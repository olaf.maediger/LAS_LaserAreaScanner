//
//--------------------------------
//  Library AdcInternal
//--------------------------------
//
#ifndef AdcInternal_h
#define AdcInternal_h
//
#include "Arduino.h"
#include "APCDefines.h"
#include "APCPinout.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
class CAdcInternal
{
  private:
  int FPinChannel0;
  int FPinChannel1;
  int FPinChannel2;
  int FPinChannel3;
  //  
  public:
  CAdcInternal(int pinchannel0, int pinchannel1);
  CAdcInternal(int pinchannel0, int pinchannel1,
               int pinchannel2, int pinchannel3);
  //
  UInt16 ReadChannel(byte channel);
  //
  Boolean Open();
  Boolean Close();
};
//
#endif // AdcInternal_h
