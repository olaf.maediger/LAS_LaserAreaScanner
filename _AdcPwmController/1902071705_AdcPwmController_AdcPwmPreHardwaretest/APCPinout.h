 //
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef APCPinout_h
#define APCPinout_h
//
#include "APCDefines.h"
//
//##########################################
//  PROCESSOR_NANOR3
//##########################################
#if defined(PROCESSOR_NANOR3)
//
// System Led 
const int PIN_LEDSYSTEM               = 13; // used from SPI
const bool LEDSYSTEM_INVERTED         = false;
//
// SPI
const int PIN_SPI_SS                  = 10;
const int PIN_SPI_SCK                 = 13; // used from LedSystem
const int PIN_SPI_MISO                = 12;
const int PIN_SPI_MOSI                = 11;
//
// I2C
const int PIN_I2C_SCL                 = 5;
const int PIN_I2C_SDA                 = 4;
//
#endif // PROCESSOR_NANOR3
//
//##########################################
//  PROCESSOR_UNOR3
//##########################################
#if defined(PROCESSOR_UNOR3)
//
// System Led 
const int PIN_LEDSYSTEM               = 13; // used from SPI
const bool LEDSYSTEM_INVERTED         = false;
//
// SPI
const int PIN_SPI_SS                  = 10;
const int PIN_SPI_SCK                 = 13; // used from LedSystem
const int PIN_SPI_MISO                = 12;
const int PIN_SPI_MOSI                = 11;
//
// I2C
const int PIN_I2C_SCL                 = 5;
const int PIN_I2C_SDA                 = 4;
//
#endif // PROCESSOR_UNOR3
//
//##########################################
//  PROCESSOR_MEGA2560
//##########################################
#if defined(PROCESSOR_MEGA2560)
//
// System Led 
const int PIN_LEDSYSTEM               = 13;
const bool LEDSYSTEM_INVERTED         = false;
//
#endif // PROCESSOR_MEGA2560
////
////##########################################
////  PROCESSOR_DUEM3
////##########################################
#if defined(PROCESSOR_DUEM3)
//
// SPI
#define PIN_SPI_CS_SDCARD               4
#define PIN_SPI_SCK                     76
#define PIN_SPI_MISO                    74
#define PIN_SPI_MOSI                    75
//
// I2C
const int PIN_I2C_SCL                 = 21;
const int PIN_I2C_SDA                 = 20;
//
// Led System
const int PIN_LEDSYSTEM               = 13;
const bool LEDSYSTEM_INVERTED         = false;
// Led Laser
const int PIN_LEDLASER                = 50;
const bool LEDLASER_INVERTED          = false;
//
// System LedLine
//const int PIN_LEDERROR              = 54;
////                                    55
//const int PIN_LEDBUSY               = 56;
////                                    57
//const int PIN_LEDMOTIONX            = 58;
//const int PIN_LEDMOTIONY            = 59;
//                                      60
//                                  
// Dac
const int PIN_DAC_CHANNEL0            = 66;
const int PIN_DAC_CHANNEL1            = 67;
// Definition for ADCInternal:
//#define DAC_INTERNAL_X
//#define DAC_INTERNAL_Y
// Definition for ADCExternalMCP4725:
#define DAC_MCP4725_X
#define DAC_MCP4725_Y

// Dac
const int PIN_DAC_CHANNEL0            = 66;
const int PIN_DAC_CHANNEL1            = 67;
//
// Trigger
const int PIN_TRIGGER_IN              = 53;
const int PIN_TRIGGER_OUT             = 51;
//
#endif // PROCESSOR_DUEM3
//
//##########################################
//  PROCESSOR_STM32F103C8
//##########################################
#if defined(PROCESSOR_STM32F103C8)
//----------------------------------------------------------------------------------
// STM32F103C8, Pins 2..16                  Pin       // Processor          - Signal
//----------------------------------------------------------------------------------
const int PIN_PC13_LEDSYSTEM                = PC13;   // 
const int PIN_PC14_OSC32OUT                 = PC14;   // 
const int PIN_PC15_OSC32IN                  = PC15;   // 
const int PIN_PA0_ADC0_UART2CTS             = PA0;    // 
const int PIN_PA1_ADC1_UART2RTS_PWM22       = PA1;    // 
const int PIN_PA2_ADC2_UART2TX_PWM23        = PA2;    // 
const int PIN_PA3_ADC3_UART2RX_PWM24        = PA3;    // 
const int PIN_PA4_ADC4_SPI1NSS_CK2          = PA4;    // 
const int PIN_PA5_ADC5_SPI1SCK              = PA5;    // 
const int PIN_PA6_ADC6_SPI1MISO_PWM31       = PA6;    // 
const int PIN_PA7_ADC7_SPI1MOSI_PWM32       = PA7;    // 
const int PIN_PB0_ADC8_PWM33                = PB0;    // 
const int PIN_PB1_ADC9_PWM34                = PB1;    // 
const int PIN_PB10_UART3TX_I2C2SCL_PWM23    = PB10;   // 
const int PIN_PB11_UART3RX_I2C2SDA_PWM24    = PB11;   // 
//----------------------------------------------------------------------------------
// STM32F103C8, Pins 21..37                 Pin       // Processor          - Signal
//----------------------------------------------------------------------------------
const int PIN_PB12_SPI2NSS                  = PB12;   // 
const int PIN_PB13_SPI2SCK_PWM11N           = PB13;   // 
const int PIN_PB14_SPI2MISO_PWM12N          = PB14;   // 
const int PIN_PB15_SPI2MOSI_PWM13N          = PB15;   // 
const int PIN_PA8_D7_PWM11                  = PA8;    // 
const int PIN_PA9_D8_UART1TX_PWM12          = PA9;    // 
const int PIN_PA10_D2_UART1RX_PWM13         = PA10;   // 
const int PIN_PA11_USBDM_CAN1RD_PWM14       = PA11;   // 
const int PIN_PA12_USBDP_CAN1TD             = PA12;   // 
const int PIN_PA15_ASPI1NSS_PWM21           = PA15;   // !!! JTDI  !!! DISABLE !!!
const int PIN_PB3_D3_ASPI1SCK_PWM22         = PB3;    // !!! JTDO  !!! DISABLE !!!
const int PIN_PB4_D5_ASPI1MISO_PWM31        = PB4;    // !!! JTRST !!! DISABLE !!!
const int PIN_PB5_D4_ASPI1MOSI_PWM32        = PB5;    // 
const int PIN_PB6_D10_AUART1TX_I2C1SCL      = PB6;    // 
const int PIN_PB7_AUART1RX_I2C1SDA          = PB7;    // 
const int PIN_PB8_D15_CAN1RD_AI2CSCL        = PB8;    // 
const int PIN_PB9_D14_CAN1TD_AI2C1SDA       = PB9;    // 
//----------------------------------------------------------------------------------
//
// --- Serial - RS232 ---
//
// --- Serial - SPI ---
// Serial - SPI - SPI_1
//const int PIN_SPI1_SCK                  = PIN_PA5_ADC5_SPI1SCK; 
//const int PIN_SPI1_MISO                 = PIN_PA6_ADC6_SPI1MISO_PWM31; 
//const int PIN_SPI1_MOSI                 = PIN_PA7_ADC7_SPI1MOSI_PWM32; 
//const int PIN_SPI1_NSS                  = PIN_PA4_ADC4_SPI1NSS_CK2; 
// Serial - SPI - SPI_2
//const int PIN_SPI2_SCK                  = PIN_PB13_SPI2SCK_PWM11N; 
//const int PIN_SPI2_MISO                 = PIN_PB14_SPI2MISO_PWM12N; 
//const int PIN_SPI2_MOSI                 = PIN_PB15_SPI2MOSI_PWM13N;
//const int PIN_SPI2_NSS                  = PIN_PB12_SPI2NSS;
//
// --- Serial - I2C ---
// Serial - I2C - I2C1
//const int PIN_I2C1_SCL                  = PIN_PB6_D10_AUART1TX_I2C1SCL;
//const int PIN_I2C1_SDA                  = PIN_PB7_AUART1RX_I2C1SDA;
// Serial - I2C - I2C2
//const int PIN_I2C2_SCL                  = PIN_PB10_UART3TX_I2C2SCL_PWM23;
//const int PIN_I2C2_SDA                  = PIN_PB11_UART3RX_I2C2SDA_PWM24;
//
// --- Analog - ADC ---
// Analog - ADC - ADConverter
//const int PIN_ADC_ANALOGIN0             = PIN_PA0_ADC0_UART2CTS;
//const int PIN_ADC_ANALOGIN1             = PIN_PA1_ADC1_UART2RTS_PWM22;
//const int PIN_ADC_ANALOGIN2             = PIN_PA2_ADC2_UART2TX_PWM23;
//const int PIN_ADC_ANALOGIN3             = PIN_PA3_ADC3_UART2RX_PWM24;
//const int PIN_ADC_ANALOGIN4             = PIN_PA4_ADC4_SPI1NSS_CK2;
//const int PIN_ADC_ANALOGIN5             = PIN_PA5_ADC5_SPI1SCK;
//const int PIN_ADC_ANALOGIN6             = PIN_PA6_ADC6_SPI1MISO_PWM31;
//const int PIN_ADC_ANALOGIN7             = PIN_PA7_ADC7_SPI1MOSI_PWM32;
//const int PIN_ADC_ANALOGIN8             = PIN_PB0_ADC8_PWM33;
//const int PIN_ADC_ANALOGIN9             = PIN_PB1_ADC9_PWM34;
//
// --- DIO - Joystick (3V3)(Analog) ---
const int PIN_JOYSTICK_SLITHORIZONTAL_POSITION  = PIN_PA0_ADC0_UART2CTS;        // X
const int PIN_JOYSTICK_SLITHORIZONTAL_WIDTH     = PIN_PA1_ADC1_UART2RTS_PWM22;  // Y
const int PIN_JOYSTICK_SLITHORIZONTAL_BUTTON    = PIN_PA2_ADC2_UART2TX_PWM23;
////
//const int PIN_JOYSTICK_SLITVERTICAL_POSITION    = PIN_PA3_ADC3_UART2RX_PWM24;    // X
//const int PIN_JOYSTICK_SLITVERTICAL_WIDTH       = PIN_PA4_ADC4_SPI1NSS_CK2;      // Y
//const int PIN_JOYSTICK_SLITVERTICAL_BUTTON      = PIN_PA5_ADC5_SPI1SCK;
////
// --- DIO - MotorDriverL298N (5V0)(Digital) ---
const int PIN_MOTOR_SLITHORIZONTAL_POSITION_INA = PIN_PB12_SPI2NSS;
const int PIN_MOTOR_SLITHORIZONTAL_POSITION_INB = PIN_PB13_SPI2SCK_PWM11N;
const int PIN_MOTOR_SLITHORIZONTAL_POSITION_PWM = PIN_PB6_D10_AUART1TX_I2C1SCL;
//
const int PIN_MOTOR_SLITHORIZONTAL_WIDTH_INA    = PIN_PB14_SPI2MISO_PWM12N;
const int PIN_MOTOR_SLITHORIZONTAL_WIDTH_INB    = PIN_PB15_SPI2MOSI_PWM13N;
const int PIN_MOTOR_SLITHORIZONTAL_WIDTH_PWM    = PIN_PB7_AUART1RX_I2C1SDA;
////
//const int PIN_MOTOR_SLITVERTICAL_POSITION_INA   = PIN_PA11_USBDM_CAN1RD_PWM14;
//const int PIN_MOTOR_SLITVERTICAL_POSITION_INB   = PIN_PA12_USBDP_CAN1TD;
//const int PIN_MOTOR_SLITVERTICAL_POSITION_PWM   = PIN_PB8_D15_CAN1RD_AI2CSCL;
////
//const int PIN_MOTOR_SLITVERTICAL_WIDTH_INA      = PIN_PA12_USBDP_CAN1TD;
//const int PIN_MOTOR_SLITVERTICAL_WIDTH_INB      = PIN_PB5_D4_ASPI1MOSI_PWM32;
//const int PIN_MOTOR_SLITVERTICAL_WIDTH_PWM      = PIN_PB9_D14_CAN1TD_AI2C1SDA;
//
// --- DIN - ABDecoder ---
const int PIN_IRQ_ABDECODER_SLITHORIZONTAL_POSITION_PULSEA  = PIN_PB0_ADC8_PWM33;
const int PIN_IRQ_ABDECODER_SLITHORIZONTAL_POSITION_PULSEB  = PIN_PB1_ADC9_PWM34;
const int PIN_IRQ_ABDECODER_SLITHORIZONTAL_WIDTH_PULSEA     = PIN_PA11_USBDM_CAN1RD_PWM14;
const int PIN_IRQ_ABDECODER_SLITHORIZONTAL_WIDTH_PULSEB     = PIN_PA12_USBDP_CAN1TD; 
//
//const int PIN_ENDSWITCH                         = 0;
//const Boolean LEVELINVERTED_ENDSWITCH           = false;
//
// I2C - I2CDisplay            I2C Address Bit[654.3210]
// I2C - I2CDisplay debug - 0x27
// I2C - I2CDisplay (IC5) - 0x20 + 0x02 : 0x22 - 0b0[010.0001]
#if defined(I2CDISPLAY_ISPLUGGED)
const Byte ADDRESS_I2C1_I2CDISPLAY            = 0x27;
#endif
//
//
// ### DIO ###
// 
// Single Led System (fixed)
const int PIN_LEDSYSTEM                       = PIN_PC13_LEDSYSTEM;  // -> Collision LedSystem & SpiClock
const bool LEDSYSTEM_INVERTED                 = TRUE;
//
// ### DAC ###
//
#endif // PROCESSOR_STM32F103C8
//
////
////##########################################
////  PROCESSOR_TEENSY32
////##########################################
#if defined(PROCESSOR_TEENSY32)
//----------------------------------------------------------------------------------
// Teensy32, Pins 0..12               Pin     // Processor          - Signal
//----------------------------------------------------------------------------------
const int PIN_0_UARTRXD1_TOUCH        = 0;    // P2  - 0/RX1/T      - UART1RXD
const int PIN_1_UARTTXD1_TOUCH        = 1;    // P3  - 1/TX1/T      - UART1TXD
const int PIN_2_NONE                  = 2;    // P4  - 2            - ADC16BUSY
const int PIN_3_CANTX_PWM             = 3;    // P5  - 3/CAN-TX-PWM - PS2DATAIN
const int PIN_4_CANRX_PWM             = 4;    // P6  - 4/CAN-RX-PWM - PS2CLOCKIN
const int PIN_5_UARTTX1_PWM           = 5;    // P7  - 5/PWM        - ADC16CLOCK
const int PIN_6_PWM                   = 6;    // P8  - 6/PWM        - ADC16DATA
const int PIN_7_UARTRX3_SPIDOUT       = 7;    // P9  - 7/RX3        - UART3RXD
const int PIN_8_UARTTX3_SPIDIN        = 8;    // P10 - 8/TX3        - UART3TXD
const int PIN_9_UARTRX2_SPICS_PWM     = 9;    // P11 - 9/RX2/PWM    - UART2RXD
const int PIN_10_UARTTX2_SPICS_PWM    = 10;   // P12 - 10/TX2/PWM   - UART2TXD
const int PIN_11_SPIMOSI              = 11;   // P13 - 11/MOSI      - SPIMOSI
const int PIN_12_SPIMISO              = 12;   // P14 - 12/MISO      - SPIMISO
//----------------------------------------------------------------------------------
// Teensy32, Pins 13..23              Pin     // Processor          - Signal
//----------------------------------------------------------------------------------
const int PIN_13_LED_SPISCK           = 13;   // P15 - 13/SCK/LED   - SPISCK
const int PIN_14_A0_SPISCK            = 14;   // P16 - 14/A0        - PS2CLOCKOUT
const int PIN_15_A1_SPICS_TOUCH       = 15;   // P17 - 15/A1/T      - PS2DATAOUT
const int PIN_16_A2_I2CSCL0_TOUCH     = 16;   // P18 - 16/A2/T      - ADC16CSN
const int PIN_17_A3_I2CSDA0_TOUCH     = 17;   // P19 - 17/A3/T      - ADC16TAG
const int PIN_18_A4_I2CSDA0_TOUCH     = 18;   // P20 - 18/A4/T/SDA0 - I2CDATA
const int PIN_19_A5_I2CSCL0_TOUCH     = 19;   // P21 - 19/A5/T/SCL0 - I2CCLOCK
const int PIN_20_A6_SPICS_PWM         = 20;   // P22 - 20/A6/PWM    - SPISLAVEB
const int PIN_21_A7_UARTRX1_SPICS_PWM = 21;   // P23 - 21/A7/PWM    - SPISLAVEA
const int PIN_22_A8_PWM_TOUCH         = 22;   // P24 - 22/A8/T/PWM  - ADC16READ
const int PIN_23_A9_PWM_TOUCH         = 23;   // P25 - 23/A9/T/PWM  - WDI
//----------------------------------------------------------------------------------
//
// ### RS232 ###
// --- Serial - RS232 ---
// Serial - RS232_1 - RS232Command
const int PIN_UART1_RXD               = PIN_0_UARTRXD1_TOUCH;     // -> RXD010V - UART1RXD
const int PIN_UART1_TXD               = PIN_1_UARTTXD1_TOUCH;     // -> TXD010V - UART1TXD
// Serial - RS232_2 - RS232Printer
const int PIN_UART2_RXD               = PIN_9_UARTRX2_SPICS_PWM;  // -> RXD010V - UART2RXD
const int PIN_UART2_TXD               = PIN_10_UARTTX2_SPICS_PWM; // -> TXD010V - UART2TXD
// Serial - RS232_3 - RS232Debug
const int PIN_UART3_RXD               = PIN_7_UARTRX3_SPIDOUT;    // -> RXD010V - UART3RXD
const int PIN_UART3_TXD               = PIN_8_UARTTX3_SPIDIN;     // -> TXD010V - UART3TXD
//
#define SERIALUSBPROGRAM SerialUSB // Serial-USBProgramming
#define SERIALPRINTER    Serial1   // Serial-Printer
#define SERIALNETWORK    Serial2   // Serial-LAN
#define SERIALCOMMAND    Serial3   // CommunicationPC
//
// ### Analog - ADC ### 
//
// Analog - ADC - ADC16
const int PIN_ADC16DATA               = PIN_6_PWM;                // -> ADC16
const int PIN_ADC16BUSY               = PIN_2_NONE;               // -> ADC16
const int PIN_ADC16CLOCK              = PIN_5_UARTTX1_PWM;        // -> ADC16
const int PIN_ADC16READ               = PIN_22_A8_PWM_TOUCH;      // -> ADC16
const int PIN_ADC16TAG                = PIN_17_A3_I2CSDA0_TOUCH;  // -> ADC16
const int PIN_ADC16CSN                = PIN_16_A2_I2CSCL0_TOUCH;  // -> ADC16
//
//
// ### Watchdog(RTC) ### 
const int PIN_WDI                     = PIN_23_A9_PWM_TOUCH;      // -> Watchdog(RTC)
//
//
// ### PS2 ###
// PS2 - Keyboard
const int PIN_PS2CLOCKOUT             = PIN_14_A0_SPISCK;         // -> PS2 Keyboard
const int PIN_PS2DATAOUT              = PIN_15_A1_SPICS_TOUCH;    // -> PS2 Keyboard
const int PIN_PS2CLOCKIN              = PIN_4_CANRX_PWM;          // -> PS2 Keyboard
const int PIN_PS2DATAIN               = PIN_3_CANTX_PWM;          // -> PS2 Keyboard
//
//
// ### SPI ###
// SPI - Common
const int PIN_SPIMISO                 = PIN_12_SPIMISO;           // -> RTC/FRAM
const int PIN_SPIMOSI                 = PIN_11_SPIMOSI;           // -> RTC/FRAM
const int PIN_SPISCK                  = PIN_13_LED_SPISCK;        // -> RTC/FRAM
//
// SPI - RTC
const int PIN_SPISLAVEA               = PIN_21_A7_UARTRX1_SPICS_PWM; // -> RTC
//
// SPI - FRAM
const int PIN_SPISLAVEB               = PIN_20_A6_SPICS_PWM;      // -> FRAM
//
// SPI - SDCard
#if defined(SDCARD_ISPLUGGED)
const int PIN_SPI_CS_SDCARD           = PIN_9_UARTRX2_SPICS_PWM;                      // -> SDCard
#endif
//
//
// ### I2C ###
//
// I2C - Common
const int PIN_I2CCLOCK                = PIN_19_A5_I2CSCL0_TOUCH;  // Common
const int PIN_I2CDATA                 = PIN_18_A4_I2CSDA0_TOUCH;  // Common
//
// I2C - ADC - MCP3424
// Analog - ADC - MCP3424 -        I2C Address Bit[7654.321]X
// I2C - ADC - MCP3424 - 0xD0 + 0x00  : 0xD0 -  0b[1101.000]X (ADR0 == ADR1 == GND)
const Byte I2CADDRESS_MCP3424_WRITE   = 0xD0;
const Byte I2CADDRESS_MCP3424_READ    = 0xD1;
// Analog - ADC - MCP3424 - Channel
const int PIN_ANALOGIN1               = 1;                        // to be adapted
const int PIN_ANALOGIN5               = 2;                        // to be adapted
const int PIN_ANALOGIN7               = 3;                        // to be adapted
const int PIN_ANALOGIN9               = 4;                        // to be adapted
//
// I2C - DIO - PCF8574            I2C Address Bit[654.3210]
// I2C - PCF8574(IC9)  - 0x20 + 0x01 : 0x21 - 0b0[010.0001]
const Byte ADDRESS_I2C1_PCF8574IC9  = 0x21;
// I2C - PCF8574(IC5)  - 0x20 + 0x02 : 0x22 - 0b0[010.0010]
const Byte ADDRESS_I2C1_PCF8574IC5  = 0x22;
// I2C - PCF8574(IC6)  - 0x20 + 0x03 : 0x23 - 0b0[010.0011]
const Byte ADDRESS_I2C1_PCF8574IC6  = 0x23;
// I2C - PCF8574(IC13) - 0x20 + 0x04 : 0x24 - 0b0[010.0100]
const Byte ADDRESS_I2C1_PCF8574IC13 = 0x24;
// I2C - PCF8574(IC8)  - 0x20 + 0x05 : 0x25 - 0b0[010.0101]
const Byte ADDRESS_I2C1_PCF8574IC8  = 0x25;
//
// I2C - I2C1 - PCF8574 - Indices
// I2C - I2C1 - PCF8574(IC9)  - 0x20 + 0x01 : 0x21 - 0b0[010.0001]
const Byte INIT_DATAIC9                       = 0x00;
const Byte INDEX_PCF8574IC9_0_KBMROW1         = 0x00;
const Byte INDEX_PCF8574IC9_1_KBMROW2         = 0x01;
const Byte INDEX_PCF8574IC9_2_KBMROW3         = 0x02;
const Byte INDEX_PCF8574IC9_3_KBMROW4         = 0x03;
const Byte INDEX_PCF8574IC9_4_LEDRED          = 0x04;
const Byte INDEX_PCF8574IC9_5_LCDREADWRITEN   = 0x05;
const Byte INDEX_PCF8574IC9_6_LCDREGSELECT    = 0x06;
const Byte INDEX_PCF8574IC9_7_LCDENABLE       = 0x07;
// I2C - I2C1 - PCF8574(IC5)  - 0x20 + 0x02 : 0x22 - 0b0[010.0010]
const Byte INIT_DATAIC5                       = 0x00;
const Byte INDEX_PCF8574IC5_0_LCDATA0         = 0x00;
const Byte INDEX_PCF8574IC5_1_LCDATA1         = 0x01;
const Byte INDEX_PCF8574IC5_2_LCDATA2         = 0x02;
const Byte INDEX_PCF8574IC5_3_LCDATA3         = 0x03;
const Byte INDEX_PCF8574IC5_4_LCDATA4         = 0x04;
const Byte INDEX_PCF8574IC5_5_LCDATA5         = 0x05;
const Byte INDEX_PCF8574IC5_6_LCDATA6         = 0x06;
const Byte INDEX_PCF8574IC5_7_LCDATA7         = 0x07;
// I2C - I2C1 - PCF8574(IC6)  - 0x20 + 0x03 : 0x23 - 0b0[010.0011]
const Byte INIT_DATAIC6                       = 0x00;
const Byte INDEX_PCF8574IC6_0_VALVE0          = 0x00;
const Byte INDEX_PCF8574IC6_1_VALVE1          = 0x01;
const Byte INDEX_PCF8574IC6_2_VALVE2          = 0x02;
const Byte INDEX_PCF8574IC6_3_VALVE3          = 0x03;
const Byte INDEX_PCF8574IC6_4_VALVE4          = 0x04;
const Byte INDEX_PCF8574IC6_5_VALVE5          = 0x05;
const Byte INDEX_PCF8574IC6_6_VALVE6          = 0x06;
const Byte INDEX_PCF8574IC6_7_VALVE7          = 0x07;
// I2C - I2C1 - PCF8574(IC13) - 0x20 + 0x04 : 0x24 - 0b0[010.0100]
const Byte INIT_DATAIC13                      = 0x00;
const Byte INDEX_PCF8574IC13_0_VALVE8         = 0x00;
const Byte INDEX_PCF8574IC13_1_SWITCHOFF      = 0x01;
const Byte INDEX_PCF8574IC13_2_LEDYELLOW      = 0x02;
const Byte INDEX_PCF8574IC13_3_LEDGREEN       = 0x03;
const Byte INDEX_PCF8574IC13_4_TESTOKN        = 0x04;
const Byte INDEX_PCF8574IC13_5_TESTNOTOKN     = 0x05;
const Byte INDEX_PCF8574IC13_6_MOTORLEFT      = 0x06;
const Byte INDEX_PCF8574IC13_7_MOTORRIGHT     = 0x07;
// I2C - I2C1 - PCF8574(IC8)  - 0x20 + 0x05 : 0x25 - 0b0[010.0101]
const Byte INIT_DATAIC8                       = 0x00;
const Byte INDEX_PCF8574IC8_0_PROGRAMLOCK     = 0x00;
const Byte INDEX_PCF8574IC8_1_KBMCOLUMN1      = 0x01;
const Byte INDEX_PCF8574IC8_2_KBMCOLUMN2      = 0x02;
const Byte INDEX_PCF8574IC8_3_KBMCOLUMN3      = 0x03;
const Byte INDEX_PCF8574IC8_4_BUTTONSTOP      = 0x04;
const Byte INDEX_PCF8574IC8_5_BUTTONVOLUME    = 0x05;
const Byte INDEX_PCF8574IC8_6_BUTTONSTART     = 0x06;
const Byte INDEX_PCF8574IC8_7_ADC16DATA       = 0x07;
//
// I2C - I2CDisplay            I2C Address Bit[654.3210]
// I2C - I2CDisplay debug - 0x27
// I2C - I2CDisplay (IC5) - 0x20 + 0x02 : 0x22 - 0b0[010.0001]
#if defined(I2CDISPLAY_ISPLUGGED)
const Byte ADDRESS_I2C1_I2CDISPLAY            = 0x27;
#endif
//
//
// ### DIO ###
// 
// Single Led System (fixed)
const int PIN_LEDSYSTEM                       = PIN_13_LED_SPISCK;  // -> Collision LedSystem & SpiClock
const bool LEDSYSTEM_INVERTED                 = false;
//
//
// ### DAC ###
//
#endif // PROCESSOR_TEENSY32
//
////##########################################
////  PROCESSOR_TEENSY36
////##########################################
#if defined(PROCESSOR_TEENSY36)
//
// Single Led System (fixed)
const int PIN_LEDSYSTEM               = 13;
const bool LEDSYSTEM_INVERTED         = false;
//
#endif // PROCESSOR_TEENSY36
//
//--------------------------------
//  Section - Constant - Error
//--------------------------------
//
#endif // APCPinout_h
