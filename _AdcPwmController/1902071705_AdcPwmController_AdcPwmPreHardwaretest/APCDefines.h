 //
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef APCefines_h
#define APCefines_h
//
#include <stdlib.h>
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
// Init-Values - Global
#define TRUE            1
#define FALSE           0
//
#undef  WATCHDOG_ISPLUGGED
// !!! #define WATCHDOG_ISPLUGGED
// 
#undef  I2CDISPLAY_ISPLUGGED
// !!! #define I2CDISPLAY_ISPLUGGED
//
#undef  PCFDISPLAY_ISPLUGGED
// !!! #define PCFDISPLAY_ISPLUGGED
//
#undef  SDCARD_ISPLUGGED
// !!! #define SDCARD_ISPLUGGED
//
//----------------------------------
// ARDUINO NANOR3 :
// #define PROCESSOR_NANOR3
//----------------------------------
//----------------------------------
// ARDUINO UNOR3 :
// #define PROCESSOR_UNOR3
//----------------------------------
// Arduino Mega2560
// #define PROCESSOR_MEGA2560
//----------------------------------
//----------------------------------
// Arduino DueM3
// #define PROCESSOR_DUEM3
//----------------------------------
//----------------------------------
// STM32F103C8 : no DACs!
// 
#define PROCESSOR_STM32F103C8
//----------------------------------
//----------------------------------
// Teensy 3.2
// #define PROCESSOR_TEENSY32
//----------------------------------
//----------------------------------
// Teensy 3.6
// #define PROCESSOR_TEENSY36
//----------------------------------
//
//##########################################
//  PROCESSOR_NANOR3
//##########################################
#if defined(PROCESSOR_NANOR3)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_NANOR3
//
//##########################################
//  PROCESSOR_UNOR3
//##########################################
#if defined(PROCESSOR_UNOR3)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_UNOR3
//
//##########################################
//  PROCESSOR_MEGA2560
//##########################################
#if defined(PROCESSOR_MEGA2560)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_MEGA2560
//
//##########################################
//  PROCESSOR_DUEM3
//##########################################
#if defined(PROCESSOR_DUEM3)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_DUEM3
//
//##########################################
//  PROCESSOR_STM32F103C8
//##########################################
#if defined(PROCESSOR_STM32F103C8)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
#define SERIALCOMMAND Serial1
// NC #define SERIALPRINTER Serial2
// NC #define SERIALNETWORK Serial3
//
#endif // PROCESSOR_STM32F103C8
//
//
//##########################################
//  PROCESSOR_TEENSY32
//##########################################
#if defined(PROCESSOR_TEENSY32)
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Choose 72MHz and not 80MHz(Overclocked) !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_TEENSY32
//
////##########################################
////  PROCESSOR_TEENSY36
////##########################################
#if defined(PROCESSOR_TEENSY36)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
#endif // PROCESSOR_TEENSY36
//
//--------------------------------
//  Section - Global Defines
//--------------------------------
//
#endif // APCefines_h
