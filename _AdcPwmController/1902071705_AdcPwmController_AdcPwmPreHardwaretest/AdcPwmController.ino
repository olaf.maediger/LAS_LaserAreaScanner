//
#include "APCDefines.h"
#include "APCPinout.h"
#include "APCError.h"
#include "APCCommand.h"
#include "APCProcess.h"
#include "APCAutomation.h"
#include "Led.h"
#include "Serial.h"
#include "IOPin.h"
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
#include "I2CDisplay.h"
#endif
#include "AdcInternal.h"
#include "Servo.h"
//
//-----------------------------------------------------------
// Segment - Global Variables - System
//-----------------------------------------------------------
//
CError      APCError;
CCommand    APCCommand;
CProcess    APCProcess;
CAutomation APCAutomation;
//
//-----------------------------------------------------------
// Segment - Global Variables - LedSystem
//-----------------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED);
//
//-----------------------------------------------------------
// Segment - Global Variables - Serial
//-----------------------------------------------------------
//
CSerial SerialCommand(SERIALCOMMAND);
//
//
//-----------------------------------------------------------
// Segment - Global Variables - SDCard
//-----------------------------------------------------------
//
#if defined(SDCARD_ISPLUGGED)
CSDCard SDCard(PIN_SPI_CS_SDCARD);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - I2CDisplay
//-----------------------------------------------------------
//
#if defined(I2CDISPLAY_ISPLUGGED)
CI2CDisplay I2CDisplay(I2CLCD_I2CADDRESS, I2CLCD_COLCOUNT, I2CLCD_ROWCOUNT);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - IOPin
//-----------------------------------------------------------
//
#if defined(WATCHDOG_ISPLUGGED)
CWatchDog WatchDog();
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - ADConverter
//-----------------------------------------------------------
//
CAdcInternal AdcInternal(PIN_JOYSTICK_SLITHORIZONTAL_POSITION,
                         PIN_JOYSTICK_SLITHORIZONTAL_WIDTH);
//
//-----------------------------------------------------------
// Segment - Global Variables - Servo
//-----------------------------------------------------------
//
CServo ServoX(PB8);
CServo ServoY(PB9);
//
//------------------------------------------------------
// Segment - Setup
//------------------------------------------------------
//
void setup() 
{ // Disable both SWD and JTAG to free PA13, PA14, PA15, PB3 and PB4
  // afio_cfg_debug_ports(AFIO_DEBUG_NONE);
  // Disable JTAG only to free PA15, PB3 and PB4. SWD remains active
  //afio_cfg_debug_ports(AFIO_DEBUG_SW_ONLY);
  //
  //
  //----------------------------------------------------
  // Device - IOPin
  //----------------------------------------------------
#if defined(WATCHDOG_ISPLUGGED)
  WatchDog.Open(WATCHDOG_COUNTERPRESET, WATCHDOG_PRESETHIGH);
#endif
  //  
  //----------------------------------------------------
  // Device - DIN
  //----------------------------------------------------
  // 
  //----------------------------------------------------
  // Device - DOUT
  //----------------------------------------------------
  //
  //----------------------------------------------------
  // Device - Serial
  //----------------------------------------------------
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_ON);
  delay(200);
  //
  //----------------------------------------------------
  // Device - Led - LedSystem
  //----------------------------------------------------
  LedSystem.Open();
  for (int BI = 0; BI < 15; BI++)
  {
    LedSystem.SetOn();
    delay(30);
    LedSystem.SetOff();
    delay(30);
  }
  LedSystem.SetOff();
  //
  //----------------------------------------------------
  // Device - SDCard
  //----------------------------------------------------
#if defined(SDCARD_ISPLUGGED)
  SDCard.Mount();
  SDCard.Unmount();
#endif  
  //
  //----------------------------------------------------
  // Device - I2CDisplay
  //----------------------------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
  I2CDisplay.Open();
  I2CDisplay.ClearDisplay();
  I2CDisplay.SetBacklightOn();
#endif  
  //
  //----------------------------------------------------
  // Device - AdcInternal
  //----------------------------------------------------
  AdcInternal.Open();
  //
  //----------------------------------------------------
  // Device - Servo
  //----------------------------------------------------
//!!!!!!//!!!!!!//!!!!!!//!!!!!!//!!!!!!  ServoX.Open();
//!!!!!!//!!!!!!//!!!!!!//!!!!!!//!!!!!!  ServoY.Open();
  //
  for (int Angle = 30; Angle <= 150; Angle += 1)
  {
    ServoX.SetAngledeg(Angle);
    ServoY.SetAngledeg(Angle);
    delay(10);
  }
  digitalWrite(PC13, HIGH);
  for (int Angle = 150; 30 <= Angle; Angle -= 1)
  {
    ServoX.SetAngledeg(Angle);
    ServoY.SetAngledeg(Angle);
    delay(10);
  }
  digitalWrite(PC13, LOW);
  for (int Angle = 30; Angle <= 90; Angle += 1)
  {
    ServoX.SetAngledeg(Angle);
    ServoY.SetAngledeg(Angle);
    delay(10);
  }
  for (int I = 1; I < 10; I++)
  {
    digitalWrite(PC13, LOW);
    delay(100);
    digitalWrite(PC13, HIGH);
    delay(100);
  }
}  
  
  
  
void loop()
{
  //----------------------------------------------------
  // Device - Command
  //----------------------------------------------------
  APCCommand.WriteProgramHeader(SerialCommand);
  APCCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //
  //----------------------------------------------------
  // Device - Process 
  //----------------------------------------------------
  APCProcess.Open();
  APCProcess.SetState(spWelcome);
  //
  //----------------------------------------------------
  // Device - Automation
  //----------------------------------------------------
  APCAutomation.Open();  
}
//
//------------------------------------------------------
// Segment - Loop
//------------------------------------------------------
//
//void loop() 
//{     
//  APCError.Handle(SerialCommand);    
//#if defined(SDCARD_ISPLUGGED)
//  APCCommand.Handle(SerialCommand, SDCard);
//#else // without SDCard-Command-Processing:
//  APCCommand.Handle(SerialCommand);
//#endif
//  APCProcess.Handle(SerialCommand); 
//  APCAutomation.Handle(SerialCommand);
//  //  
//}
//
//


























