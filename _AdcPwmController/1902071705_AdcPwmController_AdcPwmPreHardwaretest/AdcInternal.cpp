//
//--------------------------------
//  Library AdcInternal
//--------------------------------
//
#include "AdcInternal.h"
//
CAdcInternal::CAdcInternal(int pinchannel0, int pinchannel1)
{
  FPinChannel0 = pinchannel0;
  FPinChannel1 = pinchannel1;
  FPinChannel2 = 0;
  FPinChannel3 = 0;  
}

CAdcInternal::CAdcInternal(int pinchannel0, int pinchannel1,
                           int pinchannel2, int pinchannel3)
{
  FPinChannel0 = pinchannel0;
  FPinChannel1 = pinchannel1;
  FPinChannel2 = pinchannel2;
  FPinChannel3 = pinchannel3;
}

Boolean CAdcInternal::Open()
{
  pinMode(FPinChannel0, INPUT);
  pinMode(FPinChannel1, INPUT);
  pinMode(FPinChannel2, INPUT);
  pinMode(FPinChannel3, INPUT);
  return true;
}

Boolean CAdcInternal::Close()
{
  pinMode(FPinChannel0, INPUT);
  pinMode(FPinChannel1, INPUT);
  pinMode(FPinChannel2, INPUT);
  pinMode(FPinChannel3, INPUT);
  return true;
}

UInt16 CAdcInternal::ReadChannel(byte channel)
{
  switch (channel)
  {
    case 0:
      return analogRead(FPinChannel0);
    case 1:
      return analogRead(FPinChannel1);
    case 2:
      return analogRead(FPinChannel2);
    case 3:
      return analogRead(FPinChannel3);
    default:
      return 0;
  }
}

