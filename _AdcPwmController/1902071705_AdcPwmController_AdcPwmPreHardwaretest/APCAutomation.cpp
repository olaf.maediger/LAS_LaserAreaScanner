//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "APCCommand.h"
#include "APCAutomation.h"
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
//
#if defined(WATCHDOG_ISPLUGGED)
extern CWatchDog WatchDog;
#endif
//
#include "AdcInternal.h"
#include "Servo.h"
//
extern CCommand APCCommand;
extern CSerial SerialCommand;
extern CAdcInternal AdcInternal;
extern CServo ServoX, ServoY;
//
char Buffer[32];
//
CAutomation::CAutomation()
{
  FState = saUndefined;
}

EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  FState = state;
}

Boolean CAutomation::Open()
{
  FState = saIdle;
  return true;
}

Boolean CAutomation::Close()
{  
  FState = saUndefined;
  return true;
}

void CAutomation::HandleUndefined(CSerial &serial)
{ 
  delay(100);  
}

void CAutomation::WriteEvent(CSerial &serial, String text)
{
  serial.WriteLine(text.c_str());
  serial.WritePrompt();
}

void CAutomation::WriteEvent(CSerial &serial, String mask, int value)
{
  serial.Write(mask.c_str(), value);
  serial.Write("\r\n");
  serial.WritePrompt();
}

// Analyse ProgramLock, Keys and Keyboard
void CAutomation::HandleIdle(CSerial &serial)
{
  // !!!! if (1000 < TimeDelta())
  unsigned short AdcValue0 = AdcInternal.ReadChannel(0);
  unsigned short AdcValue1 = AdcInternal.ReadChannel(1);
  sprintf(Buffer, "Adc[0][%u] Adc[1][%u]", AdcValue0, AdcValue1);
  SerialCommand.WriteLine(Buffer);
  //ADC -> Servo : 
//  digitalWrite(PC13, LOW);
//  for (int Angle = 86; Angle <= 94; Angle += 1)
//  {
//    ServoX.SetAngledeg(Angle);
//    ServoY.SetAngledeg(Angle);
//    delay(100);
//  }
//  digitalWrite(PC13, HIGH);
//  for (int Angle = 94; 86 <= Angle; Angle -= 1)
//  {
//    ServoX.SetAngledeg(Angle);
//    ServoY.SetAngledeg(Angle);
//    delay(100);
//  }
  
  delay(1000);  
}

void CAutomation::HandleSystemReset(CSerial &serial)
{
  delay(100);
  SetState(saIdle);
}

void CAutomation::HandleSystemTest(CSerial &serial)
{
  delay(100);
  SetState(saIdle);
}

void CAutomation::Handle(CSerial &serial)
{
#if defined(WATCHDOG_ISPLUGGED)
  WatchDog.Trigger();    
#endif 
  switch (GetState())
  { // Common
    case saIdle:
      HandleIdle(serial);
      break;
    case saSystemReset:
      HandleSystemReset(serial);
      break;
    case saSystemTest:
      HandleSystemTest(serial);
      break;
    default: // saUndefined
      HandleUndefined(serial);
      break;
  }  
}

