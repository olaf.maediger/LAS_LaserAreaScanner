#include "APCError.h"
#include "APCCommand.h"
//
extern CCommand APCCommand;
//
CError::CError()
{
  SetCode(INIT_ERRORCODE);
}

EErrorCode CError::GetCode()
{
  return FErrorCode;  
}
void CError::SetCode(EErrorCode errorcode)
{
  FErrorCode = errorcode;
}
  
Boolean CError::Open()
{
  return false; 
}
  
Boolean CError::Close()
{
  return false; 
}

Boolean CError::Handle(CSerial &serial)
{
  if (ecNone != FErrorCode)
  {
    serial.WriteAnswer();
    serial.Write((char*)" Error[");
    Int16ToAsciiDecimal(GetCode(), APCCommand.GetTxdBuffer()); 
    serial.Write(APCCommand.GetTxdBuffer());
    serial.Write((char*)"]: ");
    switch (GetCode())
    {
      case ecNone:
        serial.Write((char*)"No Error");
        break;
      case ecUnknown:
        serial.Write((char*)"Unknown");
        break;
      case ecInvalidCommand:
        serial.Write((char*)"Invalid Command");
        break;
      case ecCommandInvalidParameter:
        serial.Write((char*)"Command invalid Parameter");
        break;     
      case ecToManyParameters:
        serial.Write((char*)"To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.Write((char*)"Missing Target Parameter");
        break;
      case ecFailMountSDCard:
        serial.Write((char*)"Fail to mount SDCard");
        break;
      case ecFailOpenCommandFile:
        serial.Write((char*)"Fail to open CommandFile");
        break;
      case ecFailWriteCommandFile:
        serial.Write((char*)"Fail to write CommandFile");
        break;
      case ecFailCloseCommandFile:
        serial.Write((char*)"Fail to close CommandFile");
        break;
      case ecFailUnmountSDCard:
        serial.Write((char*)"Fail to unmount SDCard");
        break;
      case ecCommandFileNotOpened:
        serial.Write((char*)"CommandFile not opened");
        break;
      case ecDecoderStateImpossible:
        serial.Write((char*)"Decoder State impossible!");
      default:
        serial.Write((char*)"Unknown Error");
        break;
    }
    serial.Write((char*)"!");
    serial.WriteNewLine();
    serial.WritePrompt();
    FErrorCode = ecNone;
    return true;
  }
  return false;
}
