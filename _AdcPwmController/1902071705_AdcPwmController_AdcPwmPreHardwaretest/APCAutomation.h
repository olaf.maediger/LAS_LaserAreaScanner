//
//--------------------------------
//  Library Automation
//--------------------------------
//
#ifndef Automation_h
#define Automation_h
//
#include "Arduino.h"
#include "APCDefines.h"
#include "APCPinout.h"
#include "Serial.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define MASK_EVENTAUTOMATION "%s EVA %s"
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateAutomation
{
  saUndefined = -1,
  saIdle = 0,
  saSystemReset = 1,
  saSystemTest = 2
};
//
//--------------------------------
//  Section - CAutomation
//--------------------------------
//
class CAutomation
{
  private:
  // Field
  EStateAutomation FState;  
  //  
  void WriteEvent(CSerial &serial, String text);
  void WriteEvent(CSerial &serial, String mask, int value);
  //
  public:
  // Constructor
  CAutomation();
  // Property
  EStateAutomation GetState();
  void SetState(EStateAutomation state);
  //
  // Management
  Boolean Open();
  Boolean Close();
  private:
  // State
  void HandleUndefined(CSerial &serial);  
  void HandleIdle(CSerial &serial);  
  void HandleSystemReset(CSerial &serial);  
  void HandleSystemTest(CSerial &serial);  
  public:
  // Collector
  void Handle(CSerial &serial);
};
//
#endif
