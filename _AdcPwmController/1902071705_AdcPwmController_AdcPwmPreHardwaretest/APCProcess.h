#ifndef APCProcess_h
#define APCProcess_h
//
#include "APCDefines.h"
#include "APCUtility.h"
#include "Serial.h"
//
const UInt32 INIT_PROCESSCOUNT            =      10;  // [1]
const UInt32 INIT_PROCESAPCRIOD_MS        =    1000;  // [ms]
const UInt32 INIT_PROCESSWIDTH_US         =  500000;  // [us]
//
#define MASK_STATEPROCESS "%s STP %i %i"
#define MASK_EVENTAUTOMATION "%s EVA %s"
//
const UInt16 PULSEWIDTH_WATCHDOG_100MS    =      10;  // [ms]
// const UInt32 WATCHDOG_COUNTERPRESET    =   10000;  // [1]         // without polling KeyMatrix
// const UInt32 WATCHDOG_PRESETHIGH       =      10;  // [1] : 1/000 // without polling KeyMatrix
// OK const UInt32 WATCHDOG_COUNTERPRESET =     100;  // [1]
const UInt32 WATCHDOG_COUNTERPRESET       =  100000;  // [1]
const UInt32 WATCHDOG_PRESETHIGH          =       1;  // [1] 
//
enum EStateProcess
{ // Common
  spUndefined                     = -1,
  spIdle                          = 0,
  spWelcome                       = 1,
  spGetHelp                       = 2,
  spGetProgramHeader              = 3,
  spGetSoftwareVersion            = 4,
  spGetHardwareVersion            = 5,
  spGetProcessCount               = 6,
  spSetProcessCount               = 7,
  spGetProcessPeriod              = 8,
  spSetProcessPeriod              = 9,
  spGetProcessWidth               = 10,
  spSetProcessWidth               = 11,
  spStopProcessExecution          = 12,
  //
  // LedSystem
  spGetLedSystem                  = 21,
  spLedSystemOn                   = 22,  
  spLedSystemOff                  = 23,  
  spBlinkLedSystem                = 24,  
  //
  // System
  spResetSystem                   = 31,
#if defined(WATCHDOG_ISPLUGGED)
  spPulseWatchDog                 = 32,
#endif
  //
  // Motor
  spMoveChannelPositive           = 41,
  spMoveChannelNegative           = 42,
  spMovePositionAbsolute          = 43,
  spMovePositionRelative          = 44,
  spGetPositionTarget             = 45,
  spGetPositionActual             = 46,
  spSetPositionActual             = 47,
  spAbortChannelMotion            = 48,
  spAbortAllMotion                = 49,
  //
  // LimitSwitch
  spGetLimitSwitch                = 71,
  //
  // Serial_Network
  spWriteLineSerialNetwork        = 111,
  spReadLineSerialNetwork         = 112,
  //
  // I2CDisplay
#if defined(I2CDISPLAY_ISPLUGGED)
  spClearScreenI2CDisplay         = 131,
  spShowTextI2CDisplay            = 132,
#endif
  //
  // SDCommand
#if defined(SDCARD_ISPLUGGED)
  spOpenCommandFile               = 141,
  spWriteCommandFile              = 142,
  spCloseCommandFile              = 143,
  spExecuteCommandFile            = 144,
  spAbortCommandFile              = 145
#endif
  //
};
//
class CProcess
{ //
  //  Segment - Field
  //
  private:
  // Common
  EStateProcess FState;
  UInt8 FSubstate;
  UInt32 FStateTick;                  // [1]
  UInt32 FTimeStampms;                // [ms]
  UInt32 FTimeMarkms;                 // [ms]
  UInt64 FTimeStampus;                // [us]
  UInt64 FTimeMarkus;                 // [us]
  UInt32 FProcessCount;               // [1]
  UInt32 FProcessStep;                // [1]
  UInt32 FProcessPeriodms;            // [ms]
  UInt32 FProcessWidthus;             // [us]
  UInt32 FBreakTimems;                // [ms]
  //
#if defined(SDCARD_ISPLUGGED)  
  String FFileName;
  Boolean FIsExecutingCommandFile;
#endif
  //
  void LedSystemState();
  //
  public:
  CProcess();
  // Property
  EStateProcess GetState();
  void SetState(EStateProcess stateprocess);
  void SetState(EStateProcess stateprocess, UInt8 substate);
  // Measurement - Time
  // First: TimeStamp!!!
  inline void SetTimeStamp()
  {
    FTimeStampms = millis();
    FTimeMarkms = FTimeStampms;
    FTimeStampus = micros();
    FTimeMarkus = FTimeStampus;
  }
  // Later: TimeMarker!!!
  inline void SetTimeMark()
  {
    FTimeMarkms = millis();
    FTimeMarkus = micros();
  }
  inline UInt32 GetTimeSpanms()
  {
    return FTimeMarkms - FTimeStampms;
  }
  inline UInt32 GetTimeSpanus()
  {
    return FTimeMarkus - FTimeStampus;
  }

  inline void SetProcessCount(UInt32 count)
  {
    FProcessStep = 0;
    FProcessCount = count;
  }
  inline UInt32 GetProcessCount(void)
  {
    return FProcessCount;
  }

  inline void SetProcessStep(UInt32 stepindex)
  {
    FProcessStep = stepindex;
  }
  inline UInt32 GetProcessStep(void)
  {
    return FProcessStep;
  }

  inline Boolean IncrementProcessStep()
  {
    FProcessStep++;
    return (FProcessCount <= FProcessStep);
  }

  inline void SetProcessPeriodms(UInt32 period)
  {
    FProcessPeriodms = period;
  }
  inline UInt32 GetProcessPeriodms(void)
  {
    return FProcessPeriodms;
  }
  inline UInt32 GetProcessPeriodus(void)
  {
    return (UInt32)(1000 * FProcessPeriodms);
  }

  inline void SetProcessWidthus(UInt32 width)
  {
    FProcessWidthus = width;
  }
  inline UInt32 GetProcessWidthus(void)
  {
    return FProcessWidthus;
  }

  inline UInt32 GetBreakTimems()
  {
    return FBreakTimems;
  }
  inline void SetBreakTimems(UInt32 value)
  {
    FBreakTimems = value;
  }
  // SDCommand
#if defined(SDCARD_ISPLUGGED)  
  inline String GetFileName()
  {
    return FFileName;
  }
  inline void SetFileName(String filename)
  {
    FFileName = filename;
  } 
#endif
  // ExecutionCommandFile
#if defined(SDCARD_ISPLUGGED)  
  inline Boolean IsExecutingCommandFile()
  {
    return FIsExecutingCommandFile;
  }
  void StartExecutionCommandFile();
  void StopExecutionCommandFile();
  Boolean ExecuteCommandFile();
#endif
  // Management
  Boolean Open();
  Boolean Close();
  private:
  // Common
  void HandleUndefined(CSerial &serial);
  void HandleIdle(CSerial &serial);
  void HandleWelcome(CSerial &serial);
  void HandleGetHelp(CSerial &serial);
  void HandleGetProgramHeader(CSerial &serial);
  void HandleGetSoftwareVersion(CSerial &serial);
  void HandleGetHardwareVersion(CSerial &serial);
  void HandleGetProcessCount(CSerial &serial);
  void HandleSetProcessCount(CSerial &serial);
  void HandleGetProcessPeriod(CSerial &serial);
  void HandleSetProcessPeriod(CSerial &serial);
  void HandleGetProcessWidth(CSerial &serial);
  void HandleSetProcessWidth(CSerial &serial);
  void HandleStopProcessExecution(CSerial &serial);
  // LedSystem
  void HandleGetLedSystem(CSerial &serial);
  void HandleLedSystemOn(CSerial &serial);
  void HandleLedSystemOff(CSerial &serial);
  void HandleBlinkLedSystem(CSerial &serial);
  // System
  void HandleResetSystem(CSerial &serial);      
#if defined(WATCHDOG_ISPLUGGED)
  void HandlePulseWatchDog(CSerial &serial);      
#endif
  // Motor
  void HandleMoveChannelPositive(CSerial &serial);      
  void HandleMoveChannelNegative(CSerial &serial);      
  void HandleMovePositionAbsolute(CSerial &serial);      
  void HandleMovePositionRelative(CSerial &serial);      
  void HandleGetPositionTarget(CSerial &serial);      
  void HandleGetPositionActual(CSerial &serial);      
  void HandleSetPositionActual(CSerial &serial);      
  void HandleAbortChannelMotion(CSerial &serial);      
  void HandleAbortAllMotion(CSerial &serial);      
  // LimitSwitch
  void HandleGetLimitSwitch(CSerial &serial);      
  // Serial Network
  void HandleReadLineSerialNetwork(CSerial &serial);
  void HandleWriteLineSerialNetwork(CSerial &serial);
    // I2CDisplay
#if defined(I2CDISPLAY_ISPLUGGED)
  void HandleClearScreenI2CDisplay(CSerial &serial);
  void HandleShowTextI2CDisplay(CSerial &serial);
#endif
  // SDCommand
#if defined(SDCARD_ISPLUGGED)
  void HandleOpenCommandFile(CSerial &serial);
  void HandleWriteCommandFile(CSerial &serial);
  void HandleCloseCommandFile(CSerial &serial);
  void HandleExecuteCommandFile(CSerial &serial);
  void HandleAbortCommandFile(CSerial &serial);
#endif
  //
  public:
  // Collector
  void Handle(CSerial &serial);
};
//
#endif // APCProcess_h
