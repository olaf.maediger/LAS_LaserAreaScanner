#include "APCProcess.h"
#include "APCCommand.h"
#include "APCError.h"
#include "Led.h"
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
#include "I2CDisplay.h"
#endif
#include "AdcInternal.h"
#include "Servo.h"
//
extern CLed         LedSystem;
extern CSerial      SerialCommand;
extern CError       APCError;
extern CProcess     APCProcess;
extern CCommand     APCCommand;
#if defined(WATCHDOG_ISPLUGGED)
extern CWatchDog WatchDog;
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
extern CI2CDisplay  I2CDisplay;
#endif
#if defined(SDCARD_ISPLUGGED)
extern CSDCard      SDCard;
#endif
extern CAdcInternal AdcInternal;
extern CServo ServoX, ServoY;
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CProcess::CProcess()
{
  FState = spUndefined;
  FStateTick = 0;
  FTimeStampms = millis();
  FTimeStampus = micros();
  FTimeMarkms = FTimeStampms;
  FTimeMarkus = FTimeStampus;
  FProcessCount = INIT_PROCESSCOUNT;
  FProcessPeriodms = INIT_PROCESAPCRIOD_MS;
  FProcessWidthus = INIT_PROCESSWIDTH_US;
  FProcessStep = 0;
#if defined(SDCARD_ISPLUGGED)    
  FIsExecutingCommandFile = false;
#endif  
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateProcess CProcess::GetState()
{
  return FState;
}

void CProcess::SetState(EStateProcess state)
{
  FStateTick = 0;
  FState = state;
  FSubstate = 0;
  // Message to Manager(PC):
  APCCommand.CallbackStateProcess(SerialCommand, FState, FSubstate);
}

void CProcess::SetState(EStateProcess state, UInt8 substate)
{
  if ((state != FState) || (substate != FSubstate))
  {     
    if (state != FState)
    {
      FStateTick = 0;
    }
    FState = state;
    FSubstate = substate;
    // Message to Manager(PC):
    APCCommand.CallbackStateProcess(SerialCommand, FState, substate);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CProcess::Open()
{
  SetState(spIdle);
  return true;
}

Boolean CProcess::Close()
{
  SetState(spUndefined);
  return true;
}
//
//----------------------------------------------------------
//  Segment - Helper - I2CDisplay
//----------------------------------------------------------
//
#if defined(PCFDISPLAY_ISPLUGGED)
void PCFDisplay_ShowWelcome()
{ //                   "01234567890123456789"
//  PCFDisplay.SetCursorPosition(0, 0);
//  PCFDisplay.WriteText("APC - CoCoSense     ");
//  PCFDisplay.SetCursorPosition(1, 0);
//  PCFDisplay.WriteText("All rights reserved:");
//  PCFDisplay.SetCursorPosition(2, 0);
//  PCFDisplay.WriteText("Laser-Laboratorium  ");
//  PCFDisplay.SetCursorPosition(3, 0);
//  PCFDisplay.WriteText("Goettingen e.V. 2018");
}
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
void I2CDisplay_ShowWelcome()
{ //                   "01234567890123456789"
  I2CDisplay.SetCursorPosition(0, 0);
  I2CDisplay.WriteText("FlowTest IT01 - APC ");
  I2CDisplay.SetCursorPosition(1, 0);
  I2CDisplay.WriteText("All rights reserved:");
  I2CDisplay.SetCursorPosition(2, 0);
  I2CDisplay.WriteText("FlowTest GmbH   2018");
  I2CDisplay.SetCursorPosition(3, 0);
  I2CDisplay.WriteText("D-99837 Berka       ");
}
#endif

#if defined(PCFDISPLAY_ISPLUGGED)
void PCFDisplay_ShowVoltageFotoResistor()
{
}
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
void I2CDisplay_ShowVoltageFotoResistor()
{
  //!!!!!!!!!!!!char Buffer[6];
  //!!!!!!!!!!!!AdcInternal.ConvertValue(CHANNEL_FOTORESISTOR);
  //!!!!!!!!!!!!sprintf(Buffer, "%04d", AdcInternal.GetValue(CHANNEL_FOTORESISTOR));
  //!!!!!!!!!!!!I2CDisplay.SetCursorPosition(1, 4);  
  //!!!!!!!!!!!!I2CDisplay.WriteText(Buffer);
}
#endif

#if defined(PCFDISPLAY_ISPLUGGED)
void PCFDisplay_ShowVoltageThermalResistor()
{
}
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
void I2CDisplay_ShowVoltageThermalResistor()
{
  //!!!!!!!!!!!!char Buffer[6];
  //!!!!!!!!!!!!AdcInternal.ConvertValue(CHANNEL_THERMALRESISTOR);
  //!!!!!!!!!!!!sprintf(Buffer, "%04d", AdcInternal.GetValue(CHANNEL_THERMALRESISTOR));
  //!!!!!!!!!!!!I2CDisplay.SetCursorPosition(1, 12);  
  //!!!!!!!!!!!!I2CDisplay.WriteText(Buffer);
}
#endif

#if defined(PCFDISPLAY_ISPLUGGED)
void PCFDisplay_ShowPressureVacuumChamber()
{  
}
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
void I2CDisplay_ShowPressureVacuumChamber()
{
  //!!!!!!!!!!!!char Buffer[6];
  //!!!!!!!!!!!!AdcInternal.ConvertValue(CHANNEL_PRESSUREVACUUMCHAPCER);
  //!!!!!!!!!!!!sprintf(Buffer, "%04d", AdcInternal.GetValue(CHANNEL_PRESSUREVACUUMCHAPCER));
  //!!!!!!!!!!!!I2CDisplay.SetCursorPosition(2, 4);
  //!!!!!!!!!!!!I2CDisplay.WriteText(Buffer);
}
#endif

#if defined(PCFDISPLAY_ISPLUGGED)
void PCFDisplay_ShowStateThermalLight()
{  
}
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
void I2CDisplay_ShowStateThermalLight()
{
  I2CDisplay.SetCursorPosition(3, 4);
//  if (tlActive == ThermalLight.GetLevel())
//  {
//    I2CDisplay.WriteText("X");
//  }
//  else
//  {
//    I2CDisplay.WriteText("_");   
//  }
}
#endif

#if defined(PCFDISPLAY_ISPLUGGED)
void PCFDisplay_ShowStateCoolerFotoResistor()
{  
}
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
void I2CDisplay_ShowStateCoolerFotoResistor()
{
  I2CDisplay.SetCursorPosition(3, 18);
//  if (tlActive == CoolerFotoResistor.GetLevel())
//  {
//    I2CDisplay.WriteText("X");
//  }
//  else
//  {
//    I2CDisplay.WriteText("_");   
//  }
}
#endif

#if defined(PCFDISPLAY_ISPLUGGED)
void PCFDisplay_ShowSystemState()
{ //                   "01234567890123456789"
}
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
void I2CDisplay_ShowSystemState()
{ //                   "01234567890123456789"
  I2CDisplay.SetCursorPosition(0, 0);
  I2CDisplay.WriteText("APC: Sensors&Actors");
  I2CDisplay.SetCursorPosition(1, 0);
  I2CDisplay.WriteText("VFR[");
  I2CDisplay_ShowVoltageFotoResistor();
  I2CDisplay.WriteText("]mV[");
  I2CDisplay_ShowVoltageThermalResistor();
  I2CDisplay.WriteText("]VTR");
  I2CDisplay.SetCursorPosition(2, 0);
  I2CDisplay.WriteText("PVC[");
  I2CDisplay_ShowPressureVacuumChamber();
  I2CDisplay.WriteText("]mbar       ");
  //  
  I2CDisplay.SetCursorPosition(3, 0);
  I2CDisplay.WriteText("STL[");
  I2CDisplay_ShowStateThermalLight();
  I2CDisplay.WriteText("]       SCFR[");
  I2CDisplay_ShowStateCoolerFotoResistor();
  I2CDisplay.WriteText("]");  
}
#endif
//
//----------------------------------------------------------
//  Segment - Common
//----------------------------------------------------------
//
void CProcess::HandleUndefined(CSerial &serial)
{
  delay(1000);
}

void CProcess::HandleIdle(CSerial &serial)
{ // do nothing
  SetTimeStamp();
}

void CProcess::HandleWelcome(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:        
#if defined(I2CDISPLAY_ISPLUGGED)
      I2CDisplay_ShowWelcome();
      I2CDisplay.SetBacklightOn();
#endif
      FStateTick++;
      break;
//    case 1: 
//      if (1000 < GetTimeSpanms())
//      {
//        SetTimeStamp();
//        FStateTick++;
//      }
//      break;
//    case 2: case 4: case 6:  
//      if (900 < GetTimeSpanms())
//      {
//#if defined(I2CDISPLAY_ISPLUGGED)
//        I2CDisplay.SetBacklightOff();
//#endif
//        SetTimeStamp();
//        FStateTick++;        
//      }
//      break;
//    case 3: case 5: case 7:
//      if (100 < GetTimeSpanms())
//      {
//#if defined(I2CDISPLAY_ISPLUGGED)
//        I2CDisplay.SetBacklightOn();
//#endif
//        SetTimeStamp();
//        FStateTick++;        
//      }
//      break;
//    case 8:  
//      if (3000 < GetTimeSpanms())
//      {   
//#if defined(I2CDISPLAY_ISPLUGGED)
//        I2CDisplay_ShowSystemState();
//#endif
//        FStateTick++;
//        SetState(spIdle);
//        SetTimeStamp();
//      }
//      break;
    default:
      SetState(spIdle);
      SetTimeStamp();
      break;    
  }
}

void CProcess::HandleGetHelp(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProgramHeader(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSoftwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetHardwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessCount(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessCount(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessPeriod(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessPeriod(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessWidth(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessWidth(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleStopProcessExecution(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - LedSystem
//----------------------------------------------------------
//
void CProcess::LedSystemState()
{
  switch (LedSystem.GetState())
  {
    case slOn:
      break;
    case slOff:
      break;
    default:
      break;
  }
  SetState(spIdle, LedSystem.GetState());
}

void CProcess::HandleGetLedSystem(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOn(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOff(CSerial &serial)
{
  LedSystemState();
}

// 1811081159 ProcessWidth/Period/Inverted
void CProcess::HandleBlinkLedSystem(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0: // Init, Pulse L -> H
      SetTimeStamp();
      LedSystem.SetOn();
      FStateTick = 1; // PulseH
      SetState(spBlinkLedSystem, 1);
      break;
    case 1: // PulseH for 0 -> PulseWidth
      if (GetProcessWidthus() <= GetTimeSpanus())
      {
        LedSystem.SetOff();
        FStateTick = 2; // PulseL
        SetState(spBlinkLedSystem, 0);
      }
      break;
    case 2: // PulseL for PulseWidth -> PulsePeriod
      if (GetProcessPeriodms() <= GetTimeSpanms())
      {
        FProcessStep++;
        if (FProcessStep < FProcessCount)
        {
          SetTimeStamp(); 
          LedSystem.SetOn();
          FStateTick = 1; // PulseH
          SetState(spBlinkLedSystem, 1);
        }
        else
        {
          SetState(spIdle);
        }
      }
      break;
  } 
}
//
//----------------------------------------------------------
// Segment - System
//----------------------------------------------------------
// 
void CProcess::HandleResetSystem(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_ACTIVE  = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_ACTIVE;
      break;
    case STATE_ACTIVE:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  

#if defined(WATCHDOG_ISPLUGGED)
void CProcess::HandlePulseWatchDog(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_ON      = 1;
  const int STATE_OFF     = 2;
  const int STATE_END     = 3;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      WatchDog.SetOff();
      SetTimeStamp();      
      FStateTick = STATE_ON;
      break;
    case STATE_ON:
      if (PULSEWIDTH_WATCHDOG_100MS < GetTimeSpanms())
      {
        SetTimeStamp();      
        WatchDog.SetOn();
        FStateTick = STATE_OFF;
      }
      break;  
    case STATE_OFF:
      if (PULSEWIDTH_WATCHDOG_100MS < GetTimeSpanms())
      {
        SetTimeStamp();      
        WatchDog.SetOff();
        FStateTick = STATE_END;
      }
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  
#endif
//
//----------------------------------------------------------
// Segment - Motor
//----------------------------------------------------------
// 
void CProcess::HandleMoveChannelPositive(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  

void CProcess::HandleMoveChannelNegative(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  

void CProcess::HandleMovePositionAbsolute(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  

void CProcess::HandleMovePositionRelative(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  

void CProcess::HandleGetPositionTarget(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  

void CProcess::HandleGetPositionActual(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  

void CProcess::HandleSetPositionActual(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  
void CProcess::HandleAbortChannelMotion(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  

void CProcess::HandleAbortAllMotion(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  
//
//----------------------------------------------------------
// Segment - LimitSwitch
//----------------------------------------------------------
// 
void CProcess::HandleGetLimitSwitch(CSerial &serial)
{ 
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}  
//
//----------------------------------------------------------
// Segment - Serial Network
//----------------------------------------------------------
// 
void CProcess::HandleWriteLineSerialNetwork(CSerial &serial)
{
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }    
}

void CProcess::HandleReadLineSerialNetwork(CSerial &serial)
{
  const int STATE_INIT    = 0;
  const int STATE_BUSY    = 1;
  const int STATE_END     = 2;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_BUSY;
      break;
    case STATE_BUSY:
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }    
}
//
//----------------------------------------------------------
// Segment - I2CDisplay
//----------------------------------------------------------
// 
#if defined(I2CDISPLAY_ISPLUGGED)
void CProcess::HandleClearScreenI2CDisplay(CSerial &serial)
{ 
  SetTimeMark();
  SetState(spIdle);
}  
#endif

#if defined(I2CDISPLAY_ISPLUGGED)
void CProcess::HandleShowTextI2CDisplay(CSerial &serial)
{ 
  SetTimeMark();
  SetState(spIdle);
}  
#endif
//
//----------------------------------------------------------
// Segment - SDCommand
//----------------------------------------------------------
// 
#if defined(SDCARD_ISPLUGGED)
void CProcess::HandleOpenCommandFile(CSerial &serial)
{
  const int STATE_INIT  = 0;
  const int STATE_MOUNT = 1;
  const int STATE_OPEN  = 2;
  const int STATE_END   = 3;
  // ocf a.cmd
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_MOUNT;
      break;
    case STATE_MOUNT:
      if (SDCard.Mount())
      {
        FStateTick = STATE_OPEN;
      }
      else
      {
        APCError.SetCode(ecFailMountSDCard);
        FStateTick = STATE_END;
      }
      break;
    case STATE_OPEN:
      if (SDCard.OpenWriteFile(FFileName))
      {
        FStateTick = STATE_END;
      }
      else
      {
        APCError.SetCode(ecFailOpenCommandFile);
        FStateTick = STATE_END;
      }
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CProcess::HandleWriteCommandFile(CSerial &serial)
{
  const int STATE_INIT  = 0;
  const int STATE_WRITE = 1;
  const int STATE_END   = 2;
  // ocf a.cmd
  Boolean Result = true;
  Byte PC = APCCommand.GetParameterCount();
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      if (SDCard.IsOpenForWriting())
      {
        FStateTick = STATE_WRITE;
      }
      else
      {
        APCError.SetCode(ecCommandFileNotOpened);
        FStateTick = STATE_END;
      }
      break;
    case STATE_WRITE:
      for (int II = 0; II < PC; II++)
      {
        if (II < PC - 1)
        {
          Result &= SDCard.WriteTextBlock(APCCommand.GetPParameters(II));
          Result &= SDCard.WriteSingleCharacter(' ');
        }
        else
        {
          Result &= SDCard.WriteTextLine(APCCommand.GetPParameters(II));
        }
      }
      if (!Result)
      {
        APCError.SetCode(ecFailWriteCommandFile);
      }
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CProcess::HandleCloseCommandFile(CSerial &serial)
{
  const int STATE_INIT    = 0;
  const int STATE_CLOSE   = 1;
  const int STATE_UNMOUNT = 2;
  const int STATE_END     = 3;
  // ccf -
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      if (SDCard.IsOpenForWriting())
      {
        FStateTick = STATE_CLOSE;
      }
      else
      {
        FStateTick = STATE_UNMOUNT;
      }
      break;
    case STATE_CLOSE:
      if (!SDCard.CloseWriteFile())
      {
        APCError.SetCode(ecFailCloseCommandFile);
      }
      FStateTick = STATE_UNMOUNT;
      break;  
    case STATE_UNMOUNT:
      if (!SDCard.Unmount())
      {
        APCError.SetCode(ecFailUnmountSDCard);
      }
      FStateTick = STATE_END;
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CProcess::HandleExecuteCommandFile(CSerial &serial)
{
  const int STATE_INIT  = 0;
  const int STATE_MOUNT = 1;
  const int STATE_OPEN  = 2;
  const int STATE_END   = 3;
  // ecf a.cmd
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      SetTimeStamp();
      FStateTick = STATE_MOUNT;
      break;
    case STATE_MOUNT:
      SDCard.Unmount();
      if (SDCard.Mount())
      {
        FStateTick = STATE_OPEN;
      }
      else
      {
        APCError.SetCode(ecFailMountSDCard);
        FStateTick = STATE_END;
      }
      break;  
    case STATE_OPEN:
      if (SDCard.OpenReadFile(FFileName))
      {
        StartExecutionCommandFile();
        FStateTick = STATE_END;
      }
      else
      {
        APCError.SetCode(ecFailOpenCommandFile);
        FStateTick = STATE_END;
      }
      break;  
    default: // STATE_END
      SetState(spIdle);
      break;
  }
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CProcess::HandleAbortCommandFile(CSerial &serial)
{
  StopExecutionCommandFile();
}
#endif
//
//----------------------------------------------------------
//  Segment - Collector
//----------------------------------------------------------
//
#if defined(SDCARD_ISPLUGGED)
void CProcess::StartExecutionCommandFile()
{
  FIsExecutingCommandFile = true;
}
#endif
  
#if defined(SDCARD_ISPLUGGED)
void CProcess::StopExecutionCommandFile()
{
  FIsExecutingCommandFile = false;
  SDCard.CloseReadFile();
  SDCard.Unmount();  
  // NC SetState(spIdle);
}
#endif

void CProcess::Handle(CSerial &serial)
{
  switch (APCProcess.GetState())
  { // Common
    case spIdle:
      HandleIdle(serial);
      break;
    case spWelcome:
      HandleWelcome(serial);
      break;
    case spGetHelp:
      HandleGetHelp(serial);
      break;
    case spGetProgramHeader:
      HandleGetProgramHeader(serial);
      break;
    case spGetSoftwareVersion:
      HandleGetSoftwareVersion(serial);
      break;
    case spGetHardwareVersion:
      HandleGetHardwareVersion(serial);
      break;
    case spGetProcessCount:
      HandleGetProcessCount(serial);
      break;
    case spSetProcessCount:
      HandleSetProcessCount(serial);
      break;
    case spGetProcessPeriod:
      HandleGetProcessPeriod(serial);
      break;
    case spSetProcessPeriod:
      HandleSetProcessPeriod(serial);
      break;
    case spGetProcessWidth:
      HandleGetProcessWidth(serial);
      break;
    case spSetProcessWidth:
      HandleSetProcessWidth(serial);
      break;
    case spStopProcessExecution:
      HandleStopProcessExecution(serial);
      break;
    // LedSystem
    case spGetLedSystem:
      HandleGetLedSystem(serial);
      break;
    case spLedSystemOn:
      HandleLedSystemOn(serial);
      break;
    case spLedSystemOff:
      HandleLedSystemOff(serial);
      break;
    case spBlinkLedSystem:
      HandleBlinkLedSystem(serial);
      break;
    // System
    case spResetSystem:
      HandleResetSystem(serial);
      break;
#if defined(WATCHDOG_ISPLUGGED)
    case spPulseWatchDog:
      HandlePulseWatchDog(serial);
      break;
#endif
    // Motor
    case spMoveChannelPositive:
      HandleMoveChannelPositive(serial);
      break;
    case spMoveChannelNegative:
      HandleMoveChannelNegative(serial);
      break;
    case spMovePositionAbsolute:
      HandleMovePositionAbsolute(serial);
      break;
    case spMovePositionRelative:
      HandleMovePositionRelative(serial);
      break;
    case spGetPositionTarget:
      HandleGetPositionTarget(serial);
      break;
    case spGetPositionActual:
      HandleGetPositionActual(serial);
      break;
    case spSetPositionActual:
      HandleSetPositionActual(serial);
      break;
    case spAbortChannelMotion:
      HandleAbortChannelMotion(serial);
      break;
    case spAbortAllMotion:
      HandleAbortAllMotion(serial);
      break;
    // LimitSwitch
    case spGetLimitSwitch:
      HandleGetLimitSwitch(serial);
      break;
    // Serial Network
    case spReadLineSerialNetwork:
      HandleReadLineSerialNetwork(serial);
      break;
    case spWriteLineSerialNetwork:
      HandleWriteLineSerialNetwork(serial);
      break;    
    // I2CDisplay
#if defined(I2CDISPLAY_ISPLUGGED)
    case spClearScreenI2CDisplay:
      HandleClearScreenI2CDisplay(serial);
      break;
    case spShowTextI2CDisplay:
      HandleShowTextI2CDisplay(serial);
      break;
#endif
#if defined(SDCARD_ISPLUGGED)
    // SDCommand
    case spOpenCommandFile:
      HandleOpenCommandFile(serial);
      break;
    case spWriteCommandFile:
      HandleWriteCommandFile(serial);
      break;
    case spCloseCommandFile:
      HandleCloseCommandFile(serial);
      break;
    case spExecuteCommandFile:
      HandleExecuteCommandFile(serial);
      break;
    case spAbortCommandFile:
      HandleAbortCommandFile(serial);
      break;
#endif
    default: // spUndefined
      HandleUndefined(serial);
      break;
  }  
}

