//
#include "APCError.h"
#include "APCProcess.h"
#include "APCCommand.h"
#include "Led.h"
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
#include "I2CDisplay.h"
#endif
#include "AdcInternal.h"
#include "Servo.h"
//
//-------------------------------------------
//  External Global Variables 
//-------------------------------------------
//  
extern CSerial  SerialCommand;
extern CError   APCError;
extern CProcess APCProcess;
extern CCommand APCCommand;
extern CLed LedSystem;
#if defined(I2CDISPLAY_ISPLUGGED)
extern CI2CDisplay I2CDisplay;
#endif
#if defined(WATCHDOG_ISPLUGGED)
extern CWatchDog WatchDog;
#endif
extern CAdcInternal AdcInternal;
extern CServo ServoX, ServoY;
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand()
{
  Init();
}

CCommand::~CCommand()
{
  Init();
}
//
//#########################################################
//  Segment - Helper
//#########################################################
// Defines Reset-Function (identically HW-Reset):
void (*PResetSystem)(void) = 0;
//
void CCommand::ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}

void CCommand::ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}

void CCommand::ZeroCommandText()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FCommandText[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Management
//#########################################################
//
void CCommand::CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate)
{
//  switch (stateprocess)
//  { // Base
//    case spUndefined:
//      break; 
//    case spIdle:
//      break; 
//    default:
//      break;
//  }  
//  serial.WriteNewLine();
//  serial.WritePrompt();
  sprintf(APCCommand.GetTxdBuffer(), MASK_STATEPROCESS, PROMPT_EVENT, stateprocess, substate);
  serial.WriteLine(APCCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::RiseEvent(CSerial &serial, String message)
{
  sprintf(APCCommand.GetTxdBuffer(), MASK_EVENTAUTOMATION, PROMPT_EVENT, message.c_str());
  serial.WriteLine(APCCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::Init()
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroCommandText();
}

Boolean CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CR:
        FRxdBuffer[FRxdBufferIndex] = ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FCommandText, FRxdBuffer);
        return true;
      case LF: // ignore
        break;
      default: 
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}
//
#if defined(SDCARD_ISPLUGGED)  
Boolean CCommand::DetectSDCardLine(CSDCard &sdcard)
{
  if (spIdle == APCProcess.GetState())
  {
    if (APCProcess.IsExecutingCommandFile())
    {
      if (sdcard.IsOpenForReading())
      {
        String Line = sdcard.ReadTextLine();
        if (0 < strlen(FCommandText))
        {
          strcpy(FCommandText, Line.c_str()); 
          return true;      
        }
        else
        { 
          APCProcess.StopExecutionCommandFile();
        }
      }
    }
  }
  return false;
}
#endif

Boolean CCommand::AnalyseCommandText(CSerial &serial)
{
  char *PTerminal = (char*)" \t\r\n";
  //char *PCommandText = FCommandText;
  FPCommand = strtok(FCommandText, PTerminal);
  if (FPCommand)
  {
    FParameterCount = 0;
    char *PParameter;
    while (0 != (PParameter = strtok(0, PTerminal)))
    {
      FPParameters[FParameterCount] = PParameter;
      FParameterCount++;
      if (COUNT_TEXTPARAMETERS < FParameterCount)
      {
        APCError.SetCode(ecToManyParameters);
        ZeroRxdBuffer();
        serial.WriteNewLine();
        serial.WritePrompt();
        return false;
      }
    }  
    ZeroRxdBuffer();
    serial.WriteNewLine();
    return true;
  }
  ZeroRxdBuffer();
  serial.WriteNewLine();
  serial.WritePrompt();    
  return false;
}
//
#if defined(SDCARD_ISPLUGGED)
Boolean CCommand::Handle(CSerial &serial, CSDCard &sdcard)
{
  if (DetectRxdLine(serial))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);     
    }
  }
  if (DetectSDCardLine(sdcard))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);
    }
  }
  return false;
}
#else
Boolean CCommand::Handle(CSerial &serial)
{
  if (DetectRxdLine(serial))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);     
    }
  }
  return false;
}
#endif
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PROJECT, ARGUMENT_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_HARDWARE, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_DATE, ARGUMENT_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_TIME, ARGUMENT_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_AUTHOR, ARGUMENT_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PORT, ARGUMENT_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PARAMETER, ARGUMENT_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer(); serial.WriteLine(MASK_ENDLINE);
}
//
void CCommand::WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
}
//
void CCommand::WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
}
//
void CCommand::WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.Write(MASK_H, SHORT_H); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPH, SHORT_GPH); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GSV, SHORT_GSV); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GHV, SHORT_GHV); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPC, SHORT_GPC); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_SPC, SHORT_SPC); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPP, SHORT_GPP); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_SPP, SHORT_SPP); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPW, SHORT_GPW); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_SPW, SHORT_SPW); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_A, SHORT_A); serial.WriteLine();
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LEDSYSTEM);
  serial.WriteAnswer(); serial.Write(MASK_GLS, SHORT_GLS); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_LSH, SHORT_LSH); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_LSL, SHORT_LSL); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_BLS, SHORT_BLS); serial.WriteLine();
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_SYSTEM);
  serial.WriteAnswer(); serial.Write(MASK_RSS, SHORT_RSS); serial.WriteLine();
#if defined(WATCHDOG_ISPLUGGED)
  serial.WriteAnswer(); serial.Write(MASK_PWD, SHORT_PWD); serial.WriteLine();
#endif
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_MOTOR); 
  serial.WriteAnswer(); serial.Write(MASK_MCP, SHORT_MCP); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_MCN, SHORT_MCN); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_MPA, SHORT_MPA); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_MPR, SHORT_MPR); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPT, SHORT_GPT); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_SPA, SHORT_SPA); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPA, SHORT_GPA); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_ACM, SHORT_ACM); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_AAM, SHORT_AAM); serial.WriteLine();
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LIMITSWITCH);
  serial.WriteAnswer(); serial.Write(MASK_GLW, SHORT_GLW); serial.WriteLine();
  //
#if defined(I2CDISPLAY_ISPLUGGED)
  serial.WriteAnswer(); serial.WriteLine(HELP_I2CDISPLAY);
  serial.WriteAnswer(); serial.Write(MASK_CLI, SHORT_CLI); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_STI, SHORT_STI); serial.WriteLine();
#endif
  //
#if defined(SDCARD_ISPLUGGED)  
  serial.WriteAnswer(); serial.WriteLine(HELP_SDCOMMAND);
  serial.WriteAnswer(); serial.Write(MASK_OCF, SHORT_OCF); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_WCF, SHORT_WCF); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_CCF, SHORT_CCF); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_ECF, SHORT_ECF); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_ACF, SHORT_ACF); serial.WriteLine();
#endif
  //
  serial.WriteAnswer(); serial.WriteLine(MASK_ENDLINE);
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
void CCommand::ExecuteGetHelp(CSerial &serial)
{
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetHelp);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), 0);
    serial.WriteLine(GetTxdBuffer());         
    WriteHelp(serial);
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
void CCommand::ExecuteGetProgramHeader(CSerial &serial)
{
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetProgramHeader);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), 0);
    serial.WriteLine(GetTxdBuffer());   
    WriteProgramHeader(serial);
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
void CCommand::ExecuteGetSoftwareVersion(CSerial &serial)
{
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetSoftwareVersion);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), COUNT_SOFTWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteSoftwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
void CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetHardwareVersion);
    // Analyse parameters: -
    // Response
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), COUNT_HARDWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteHardwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
void CCommand::ExecuteGetProcessCount(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetProcessCount);
    // Analyse parameters: -
    // Execute:
    UInt32 PC = APCProcess.GetProcessCount();    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
void CCommand::ExecuteSetProcessCount(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spSetProcessCount);
    // Analyse parameters:
    UInt32 PC = atol(GetPParameters(0));
    // Execute:
    APCProcess.SetProcessCount(PC);
    PC = APCProcess.GetProcessCount();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
void CCommand::ExecuteGetProcessPeriod(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetProcessPeriod);
    // Analyse parameters: -
    // Execute:
    UInt32 PP = APCProcess.GetProcessPeriodms();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
void CCommand::ExecuteSetProcessPeriod(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spSetProcessPeriod);
    // Analyse parameters:
    UInt32 PP = atol(GetPParameters(0));
    // Execute:
    APCProcess.SetProcessPeriodms(PP);
    PP = APCProcess.GetProcessPeriodms();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
void CCommand::ExecuteGetProcessWidth(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetProcessWidth);
    // Analyse parameters: -
    // Execute:
    UInt32 PW = APCProcess.GetProcessWidthus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), PW);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
void CCommand::ExecuteSetProcessWidth(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spSetProcessWidth);
    // Analyse parameters:
    UInt32 PW = atol(GetPParameters(0));
    // Execute:
    APCProcess.SetProcessWidthus(PW);
    PW = APCProcess.GetProcessWidthus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), PW);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
void CCommand::ExecuteAbortAll(CSerial &serial)
{ 
  APCProcess.SetProcessStep(0);
  APCProcess.SetProcessCount(0);
  APCProcess.SetState(spIdle);
  // Analyse parameters:
  // Execute:
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LedSystem
//#########################################################
//
void CCommand::ExecuteGetLedSystem(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetLedSystem);
    // Analyse parameters:
    // Execute:
    int State = LedSystem.GetState();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), State);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteLedSystemOn(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spLedSystemOn);
    // Analyse parameters:
    // Execute:
    LedSystem.SetOn();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteLedSystemOff(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spLedSystemOff);
    // Analyse parameters:
    // Execute:
    LedSystem.SetOff();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteBlinkLedSystem(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spBlinkLedSystem);
    // Analyse parameters:
    UInt32 Period = atol(GetPParameters(0)); // [us]!!!!!!!!
    UInt32 Count = atol(GetPParameters(1));  // [1] 
    // Execute:
    APCProcess.SetProcessPeriodms(Period);
    APCProcess.SetProcessCount(Count);    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %u", 
            PROMPT_RESPONSE, GetPCommand(), Period, Count);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
//#########################################################
//  Segment - Execution - System
//#########################################################
//
void CCommand::ExecuteResetSystem(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spResetSystem);
    // Analyse parameters ( RSS - ):
    // Execute:
    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
    delay(2000);
    //
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Reset System !!!
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#if (defined(PROCESSOR_NANOR3)||defined(PROCESSOR_UNOR3)||defined(PROCESSOR_MEGA2560))
    PResetSystem();
#elif defined(PROCESSOR_DUEM3)
#elif defined(PROCESSOR_STM32F103C8)
// ???????????????????????????????????????????????????????????????????????????????
    // NVIC_SystemReset();
    // SCB->AIRCR = 0x05fa0004;
//    SCB->AIRCR  = (NVIC_AIRCR_VECTKEY | (SCB->AIRCR & (0x700)) | (1<<NVIC_SYSRESETREQ)); /* Keep priority group unchanged */
//  __DSB();                                                                                 /* Ensure completion of memory access */              
//  while(1);    
//    SCB->AIRCR = AIRCR_VECTKEY_MASK | (u32)0x04;
//    asm ldr     r0,=0xE000ED0C ; SCB AIRCR
//    asm ldr     r1,=0x05FA0004
//    asm str     r1, [r0, #0]
// ???????????????????????????????????????????????????????????????????????????????
#elif (defined(PROCESSOR_TEENSY32)||defined(PROCESSOR_TEENSY36))
    // problem because of reprogramming !!! _reboot_Teensyduino_();
    // undefined _restart_Teensyduino_();
    // undefined init_pins();
    // the only possibility for this time:
    // OK (no reset!) _init_Teensyduino_internal_();
    // TOP:!!!
#define CPU_RESTART_ADDR (uint32_t *)0xE000ED0C
#define CPU_RESTART_VAL 0x5FA0004
#define CPU_RESTART (*CPU_RESTART_ADDR = CPU_RESTART_VAL);
    CPU_RESTART
    //   
#endif
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    delay(2000);
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

#if defined(WATCHDOG_ISPLUGGED)
void CCommand::ExecutePulseWatchDog(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spPulseWatchDog);
    // Analyse parameters ( PWD - ):
    // Execute:
    WatchDog.ForceTrigger();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
//#########################################################
//  Segment - Execution - Motor
//#########################################################
//
void CCommand::ExecuteMoveChannelPositive(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spMoveChannelPositive);
    // Analyse parameters ( MCP <c> ):
    Byte Channel = atoi(GetPParameters(0));
    // Execute:
    switch (Channel)
    {
      case 0:
//!!!!!!        MotorSlitHorizontalX.StartPositive();
        break;
      case 1:
//!!!!!!        MotorSlitHorizontalY.StartPositive();
        break;
      default:
        APCError.SetCode(ecCommandInvalidParameter);
        return;      
    }
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), Channel);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteMoveChannelNegative(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spMoveChannelNegative);
    // Analyse parameters ( MCN <c> ):
    Byte Channel = atoi(GetPParameters(0));
    // Execute:
    switch (Channel)
    {
      case 0:
//!!!!!!        MotorSlitHorizontalX.StartNegative();
        break;
      case 1:
//!!!!!!        MotorSlitHorizontalY.StartNegative();
        break;
    }
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), Channel);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteMovePositionAbsolute(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spMovePositionAbsolute);
    // Analyse parameters ( MPA <c> <p> ):
    Byte Channel = atoi(GetPParameters(0));
    Int32 PA = atoi(GetPParameters(1));
    // Execute:
    switch (Channel)
    {
      case 0:
//!!!!!!        MotorSlitHorizontalX.MoveAbsolute(PA);
        break;
      case 1:
//!!!!!!        MotorSlitHorizontalY.MoveAbsolute(PA);
        break;
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %i", 
                            PROMPT_RESPONSE, GetPCommand(),
                            Channel, PA);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteMovePositionRelative(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spMovePositionRelative);
    // Analyse parameters ( MPR <c> <p> ):
    Byte Channel = atoi(GetPParameters(0));
    Int32 PR = atoi(GetPParameters(1));
    // Execute:
    switch (Channel)
    {
      case 0:
//!!!!!!        MotorSlitHorizontalX.MoveRelative(PR);
        break;
      case 1:
//!!!!!!        MotorSlitHorizontalY.MoveRelative(PR);
        break;
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %i", 
            PROMPT_RESPONSE, GetPCommand(), Channel, PR);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteGetPositionTarget(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetPositionTarget);
    // Analyse parameters ( GPT <c> ):
    Byte Channel = atoi(GetPParameters(0));
    Int32 PT = 0;
    // Execute:
    switch (Channel)
    {
      case 0:
//!!!!!!        PT = MotorSlitHorizontalX.GetPositionTarget();
        break;
      case 1:
//!!!!!!        PT = MotorSlitHorizontalY.GetPositionTarget();
        break;
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %i", 
            PROMPT_RESPONSE, GetPCommand(), Channel, PT);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteGetPositionActual(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetPositionActual);
    // Analyse parameters ( GPA <c> ):
    Byte Channel = atoi(GetPParameters(0));
    Int32 PA = 0;
    // Execute:
    switch (Channel)
    {
      case 0:
//!!!!!!        PA = MotorSlitHorizontalX.GetPositionActual();
        break;
      case 1:
//!!!!!!        PA = MotorSlitHorizontalY.GetPositionActual();
        break;
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %i", 
            PROMPT_RESPONSE, GetPCommand(), Channel, PA);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteSetPositionActual(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spSetPositionActual);
    // Analyse parameters ( SPA <c> <p> ):
    Byte Channel = atoi(GetPParameters(0));
    Int32 PA = atoi(GetPParameters(1));
    // Execute:
    switch (Channel)
    {
      case 0:
//!!!!!!        MotorSlitHorizontalX.SetPositionActual(PA);
        break;
      case 1:
//!!!!!!        MotorSlitHorizontalY.SetPositionActual(PA);
        break;
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %i", 
            PROMPT_RESPONSE, GetPCommand(), Channel, PA);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteAbortChannelMotion(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spAbortChannelMotion);
    // Analyse parameters ( ACM <c> ):
    Byte Channel = atoi(GetPParameters(0));
    // Execute:
    switch (Channel)
    {
      case 0:
//!!!!!!        MotorSlitHorizontalX.StopMotion();
        break;
      case 1:
//!!!!!!        MotorSlitHorizontalY.StopMotion();
        break;
    }
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), Channel);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteAbortAllMotion(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spAbortAllMotion);
    // Analyse parameters ( AAM <-> ):
    // Execute:
//    MotorSlitHorizontalX.StopMotion();
//    MotorSlitHorizontalY.StopMotion();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
//#########################################################
//  Segment - Execution - LimitSwitch
//#########################################################
//
void CCommand::ExecuteGetLimitSwitch(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spGetLimitSwitch);
    // Analyse parameters ( GLW <c> ):
    Byte Channel = atoi(GetPParameters(0));
    // Execute:
    Byte State = 0xFF;
//    if (0 < (0x01 & Channel))
//    {
//      Channel = 0x01;
//      State = MotorAxisUX.GetLimitSwitch();
//    } else
//    if (0 < (0x02 & Channel))
//    {
//      Channel = 0x02;
//      State = MotorAxisUY.GetLimitSwitch();
//    } else
//    if (0 < (0x04 & Channel))
//    {
//      Channel = 0x04;
//      State = MotorAxisVX.GetLimitSwitch();
//    } else
//    if (0 < (0x08 & Channel))
//    {
//      Channel = 0x08;
//      State = MotorAxisVY.GetLimitSwitch();
//    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %u", 
            PROMPT_RESPONSE, GetPCommand(), Channel, State);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
//
//#########################################################
//  Segment - Execution - Serial - Network
//#########################################################
//
//void CCommand::ExecuteWriteLineSerialNetwork(CSerial &serial)
//{ 
//  if (spIdle == APCProcess.GetState())
//  {
//    APCProcess.SetState(spWriteLineSerialNetwork);
//    // Analyse parameters ( WLN <l> ):
//    String Line = GetPParameters(0);
//    // Execute:
//    SerialNetwork.WriteLine(Line);
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %s", 
//            PROMPT_RESPONSE, GetPCommand(), Line.c_str());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
//  }
//  else
//  {
//    APCError.SetCode(ecCommandTimingFailure);
//  }
//}
//
//void CCommand::ExecuteReadLineSerialNetwork(CSerial &serial)
//{ 
//  if (spIdle == APCProcess.GetState())
//  {
//    APCProcess.SetState(spReadLineSerialNetwork);
//    // Analyse parameters ( RLN - ):
//    // Execute:
//    String Line = SerialNetwork.ReadLine();
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %s", 
//            PROMPT_RESPONSE, GetPCommand(), Line.c_str());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
//  }
//  else
//  {
//    APCError.SetCode(ecCommandTimingFailure);
//  }
//}
//
//#########################################################
//  Segment - Execution - I2CDisplay
//#########################################################
//
#if defined(I2CDISPLAY_ISPLUGGED)
void CCommand::ExecuteClearScreenI2CDisplay(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spClearDisplayI2C);
    // Analyse parameters ( CLI ):
    // Execute:
    I2CDisplay.ClearDisplay();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
#endif

#if defined(I2CDISPLAY_ISPLUGGED)
void CCommand::ExecuteShowTextI2CDisplay(CSerial &serial)
{ 
  if (spIdle == APCProcess.GetState())
  {
    APCProcess.SetState(spShowTextI2C);
    // Analyse parameters ( STI <c> <r> <t> ):
    Byte R = atol(GetPParameters(0));
    Byte C = atol(GetPParameters(1));
    String T = GetPParameters(2);
    // Execute:
    I2CDisplay.SetCursorPosition(R, C);    
    I2CDisplay.WriteText(T);    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i %i %s", PROMPT_RESPONSE, GetPCommand(), R, C, T.c_str());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    APCError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
//#########################################################
//  Segment - Execution - SDCommand
//#########################################################
//
#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteOpenCommandFile(CSerial &serial)
{
  APCProcess.SetState(spOpenCommandFile);
  // Analyse parameters ( OCF <f> ):
  APCProcess.SetFileName(GetPParameters(0));
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %s", 
          PROMPT_RESPONSE, GetPCommand(), GetPParameters(0));
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteWriteCommandFile(CSerial &serial)
{
  APCProcess.SetState(spWriteCommandFile);
  // Analyse parameters ( WCF <c> <p> ):
  // Execution: -
  // Response:
  serial.Write(PROMPT_RESPONSE);
  serial.Write(' ');
  serial.Write(GetPCommand());
  serial.Write(' ');
  int PC = GetParameterCount();    
  for (int II = 0; II < PC; II++)
  {
    serial.Write(GetPParameters(II));
    serial.Write(' ');
  }
  serial.WriteLine("");
  serial.WritePrompt();
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteCloseCommandFile(CSerial &serial)
{
  APCProcess.SetState(spCloseCommandFile);
  // Analyse parameters ( CCF - ):
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", 
          PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteExecuteCommandFile(CSerial &serial)
{ // ecf a.cmd
  APCProcess.SetState(spExecuteCommandFile);
  // Analyse parameters ( ECF <f> ):
  String CF = GetPParameters(0);
  APCProcess.SetFileName(GetPParameters(0));
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", 
          PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteAbortCommandFile(CSerial &serial)
{
  APCProcess.SetState(spAbortCommandFile);
  // Analyse parameters ( ACF - ):
  // Execution:
//  Boolean Result = SDCard.CloseReadFile();
//  if (Result)
//  {
//    //... abort execution...
//  }
//  // Response:
//  sprintf(GetTxdBuffer(), "%s %s %u", 
//          PROMPT_RESPONSE, GetPCommand(), Result);
//  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
#endif
//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
Boolean CCommand::Execute(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
// debug Serial.WriteLine(TxdBuffer);
  //  Common
  if (!strcmp(SHORT_H, GetPCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPH, GetPCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GSV, GetPCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GHV, GetPCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPC, GetPCommand()))
  {
    ExecuteGetProcessCount(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPC, GetPCommand()))
  {
    ExecuteSetProcessCount(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPP, GetPCommand()))
  {
    ExecuteGetProcessPeriod(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPP, GetPCommand()))
  {
    ExecuteSetProcessPeriod(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPW, GetPCommand()))
  {
    ExecuteGetProcessWidth(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPW, GetPCommand()))
  {
    ExecuteSetProcessWidth(serial);
    return true;
  } else 
  if (!strcmp(SHORT_A, GetPCommand()))
  {
    ExecuteAbortAll(serial);
    return true;
  } else 
  // ----------------------------------
  // LedSystem
  // ---------------------------------- 
  if (!strcmp(SHORT_GLS, GetPCommand()))
  {
    ExecuteGetLedSystem(serial);
    return true;
  } else 
  if (!strcmp(SHORT_LSH, GetPCommand()))
  {
    ExecuteLedSystemOn(serial);
    return true;
  } else   
  if (!strcmp(SHORT_LSL, GetPCommand()))
  {
    ExecuteLedSystemOff(serial);
    return true;
  } else   
  if (!strcmp(SHORT_BLS, GetPCommand()))
  {
    ExecuteBlinkLedSystem(serial);
    return true;
  } else   
  // ----------------------------------
  // System
  // ----------------------------------
  if (!strcmp(SHORT_RSS, GetPCommand()))
  {
    ExecuteResetSystem(serial);
    return true;
  } else
#if defined(WATCHDOG_ISPLUGGED)
  if (!strcmp(SHORT_PWD, GetPCommand()))
  {
    ExecutePulseWatchDog(serial);
    return true;
  } else
#endif
  // ----------------------------------
  // Motor
  // ---------------------------------- 
  if (!strcmp(SHORT_MCP, GetPCommand()))
  {
    ExecuteMoveChannelPositive(serial);
    return true;
  } else   
  if (!strcmp(SHORT_MCN, GetPCommand()))
  {
    ExecuteMoveChannelNegative(serial);
    return true;
  } else   
  if (!strcmp(SHORT_MPA, GetPCommand()))
  {
    ExecuteMovePositionAbsolute(serial);
    return true;
  } else   
  if (!strcmp(SHORT_MPR, GetPCommand()))
  {
    ExecuteMovePositionRelative(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GPT, GetPCommand()))
  {
    ExecuteGetPositionTarget(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPA, GetPCommand()))
  {
    ExecuteSetPositionActual(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GPA, GetPCommand()))
  {
    ExecuteGetPositionActual(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ACM, GetPCommand()))
  {
    ExecuteAbortChannelMotion(serial);
    return true;
  } else   
  if (!strcmp(SHORT_AAM, GetPCommand()))
  {
    ExecuteAbortAllMotion(serial);
    return true;
  } else   
  // ----------------------------------
  // LimitSwitch
  // ---------------------------------- 
  if (!strcmp(SHORT_GLS, GetPCommand()))
  {
    ExecuteGetLimitSwitch(serial);
    return true;
  } else   
//  // ----------------------------------
//  // Serial Network
//  // ---------------------------------- 
//  if (!strcmp(SHORT_WLN, GetPCommand()))
//  {
//    ExecuteWriteLineSerialNetwork(serial);
//    return true;
//  } else   
//  if (!strcmp(SHORT_RLN, GetPCommand()))
//  {
//    ExecuteReadLineSerialNetwork(serial);
//    return true;
//  } else   
  // ----------------------------------
  // I2CDisplay
  // ----------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
  if (!strcmp(SHORT_CLI, GetPCommand()))
  {
    ExecuteClearScreenI2CDisplay(serial);
    return true;
  } else   
  if (!strcmp(SHORT_STI, GetPCommand()))
  {
    ExecuteShowTextI2CDisplay(serial);
    return true;
  } else  
#endif
  // ----------------------------------
  // SDCommand
  // ----------------------------------
#if defined(SDCARD_ISPLUGGED)
  if (!strcmp(SHORT_OCF, GetPCommand()))
  {
    ExecuteOpenCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_WCF, GetPCommand()))
  {
    ExecuteWriteCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_CCF, GetPCommand()))
  {
    ExecuteCloseCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ECF, GetPCommand()))
  {
    ExecuteExecuteCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ACF, GetPCommand()))
  {
    ExecuteAbortCommandFile(serial);
    return true;
  } else   
#endif
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    APCError.SetCode(ecInvalidCommand);
  }
  return false;  
}

