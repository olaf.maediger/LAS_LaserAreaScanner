﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using HWUart;
//
namespace UCUart
{
  public delegate void DOnOpenCloseChanged(Boolean opened);
  //
  public partial class CUCUart : UserControl
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    public const String INIT_PORTNAME = "";
    public const EComPort INIT_COMPORT = EComPort.cp1;
    public const EBaudrate INIT_BAUDRATE = EBaudrate.br9600;
    public const EParity INIT_PARITY = EParity.paNone;
    public const EDatabits INIT_DATABITS = EDatabits.db8;
    public const EStopbits INIT_STOPBITS = EStopbits.sb1;
    public const EHandshake INIT_HANDSHAKE = EHandshake.hsNone;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    // ??? private DOnOpenCloseChanged FOnOpenCloseChanged;
    private DOnLineReceived FOnLineReceived;
    private CUart FUart;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    public CUCUart()
    {
      InitializeComponent();
      //
      // FUCOpenClose.SetOnOpenCloseChanged(UCOpenCloseOnOpenCloseChanged);
      //
      FUart = new CUart();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      /*!!!!!
      FUCOpenClose.SetNotifier(FNotifier);
      FUCBaudrate.SetNotifier(FNotifier);
      FUCDatabits.SetNotifier(FNotifier);
      FUCHandshake.SetNotifier(FNotifier);
      FUCParity.SetNotifier(FNotifier);
      FUCStopbits.SetNotifier(FNotifier);
      FUCPortName.SetNotifier(FNotifier);
      FUCPortsInstalled.SetNotifier(FNotifier);
      FUCPortsSelectable.SetNotifier(FNotifier);
       */
    }

    public String GetName()
    {
      return FUCPortName.GetPortName();
    }
    public EComPort GetComPort()
    {
      return FUCPortsSelectable.GetPortSelected();
    }
    public EBaudrate GetBaudrate()
    {
      return FUCBaudrate.GetBaudrate();
    }
    public EParity GetParity()
    {
      return FUCParity.GetParity();
    }
    public EDatabits GetDatabits()
    {
      return FUCDatabits.GetDatabits();
    }
    public EStopbits GetStopbits()
    {
      return FUCStopbits.GetStopbits();
    }
    public EHandshake GetHandshake()
    {
      return FUCHandshake.GetHandshake();
    }

    //public void SetOnOpenCloseChanged(DOnOpenCloseChanged value)
    //{
    //  FOnOpenCloseChanged = value;
    //  FUCOpenClose.SetOnOpenCloseChanged(UCOpenCloseOnOpenCloseChanged);
    //}

    public void SetOnComPortLineReceived(DOnLineReceived value)
    {
      FOnLineReceived = value;
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUart.SetOnLineReceived(HWComPortOnLineReceived);
    }

    //
    //------------------------------------------------------------------------
    //  Section - Get/SetData
    //------------------------------------------------------------------------
    //
    //public Boolean GetUCUartData(out RUCUartData data)
    //{
    //  Boolean Result = false;
    //  data = new RUCUartData(0);
    //  Result = FUart.GetComPortData(out data.ComPortData);
    //  data.ComPortData.Name = FUCPortName.GetPortName();
    //  data.ComPortData.ComPort = FUCPortsSelectable.GetPortSelected();
    //  data.ComPortData.Baudrate = FUCBaudrate.GetBaudrate();
    //  data.ComPortData.Parity = FUCParity.GetParity();
    //  data.ComPortData.Databits = FUCDatabits.GetDatabits();
    //  data.ComPortData.Stopbits = FUCStopbits.GetStopbits();
    //  data.ComPortData.Handshake = FUCHandshake.GetHandshake();
    //  Result = FUart.SetComPortData(data.ComPortData);
    //  return Result;
    //}

    //public Boolean SetUCUartData(RUCUartData data)
    //{
    //  Boolean Result = false;
    //  FUCPortName.SetPortName(data.ComPortData.Name);
    //  FUCPortsSelectable.SetPortSelected(data.ComPortData.ComPort);
    //  FUCBaudrate.SetBaudrate(data.ComPortData.Baudrate);
    //  FUCParity.SetParity(data.ComPortData.Parity);
    //  FUCDatabits.SetDatabits(data.ComPortData.Databits);
    //  FUCStopbits.SetStopbits(data.ComPortData.Stopbits);
    //  FUCHandshake.SetHandshake(data.ComPortData.Handshake);
    //  Result = FUart.SetComPortData(data.ComPortData);
    //  return Result;
    //}
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //
    private void EnableComPortControls(Boolean enable)
    {
      FUCPortName.Enabled = enable;
      FUCPortsSelectable.Enabled = enable;
      FUCBaudrate.Enabled = enable;
      FUCParity.Enabled = enable;
      FUCDatabits.Enabled = enable;
      FUCStopbits.Enabled = enable;
      FUCHandshake.Enabled = enable;
      btnRefresh.Enabled = enable;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback
    //------------------------------------------------------------------------
    //
    //private void UCOpenCloseOnOpenCloseChanged(Boolean opened)
    //{
    //  if (opened)
    //  {
    //    OpenComPort();
    //  }
    //  else
    //  {
    //    CloseComPort();
    //  }
    //  EnableComPortControls(!FUart.IsOpen());
    //  if (FOnOpenCloseChanged is DOnOpenCloseChanged)
    //  {
    //    FOnOpenCloseChanged(opened);
    //  }
    //}

    //private void HWComPortOnLineReceived(Guid comportid, String line)
    //{
    //  if (FOnLineReceived is DOnLineReceived)
    //  {
    //    FOnLineReceived(comportid, line);
    //  }
    //}

    //
    //--------------------------------------------------------------------
    //  Segment - UartBase - Callback
    //--------------------------------------------------------------------
    //
    private void UartOnErrorDetected(Guid comportid, Int32 errorcode, String errortext)
    {
      String SError = String.Format("{Error[%d]: %s!", errorcode, errortext);
      FNotifier.Add(SError);
    }

    private void UartOnTextTransmitted(Guid comportid, String text)
    {
      // !!!!!!lbxUartBase.Items.Add("[TxText]" + text);
      FNotifier.Add("[TxText]" + text);
    }

    private void UartOnLineTransmitted(Guid comportid, String line)
    {
      //!!!lbxUartBase.Items.Add("[TxLine]" + line);
      FNotifier.Add("[TxLine]" + line);
    }

    private delegate void CBUartOnTextReceived(Guid comportid, String text);
    private void UartOnTextReceived(Guid comportid, String text)
    {
      if (this.InvokeRequired)
      {
        CBUartOnTextReceived CB = new CBUartOnTextReceived(UartOnTextReceived);
        Invoke(CB, new object[] { comportid, text });
      }
      else
      { // nothing to do -> OnLineReceived
        //!!!lbxUartBase.Items.Add("[RxText]" + text);
        FNotifier.Add("[RxText]" + text);
      }
    }

    private delegate void CBUartOnLineReceived(Guid comportid, String line);
    private void UartOnLineReceived(Guid comportid, String line)
    {
      if (this.InvokeRequired)
      {
        CBUartOnLineReceived CB = new CBUartOnLineReceived(UartOnLineReceived);
        Invoke(CB, new object[] { comportid, line });
      }
      else
      {
        //!!!lbxUartBase.Items.Add("[RxLine]" + line);
        FNotifier.Add("[RxLine]" + line);
      }
    }

    private void UartOnPinChanged(Guid comportid,
                                  Boolean pincts, Boolean pinrts,
                                  Boolean pindsr, Boolean pindtr, Boolean pindcd)
    {
      FNotifier.Add("[PinChanged]");
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event - Control
    //------------------------------------------------------------------------
    //
    private void btnRefresh_Click(object sender, EventArgs e)
    {
      RefreshComPortControls();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Public Management
    //------------------------------------------------------------------------
    //    
    public void RefreshComPortControls()
    {
      FUCPortsInstalled.RefreshPortsInstalled();
      FUCPortsSelectable.RefreshPortsSelectable();
    }

    public void RefreshOpenCloseComPort(Boolean opened)
    {
      EnableComPortControls(opened);
    }

    public Boolean OpenComPort()
    {
      try
      {
        if (FUart.IsClosed())
        {
          FUart.Open(GetName(), GetComPort(), GetBaudrate(), 
                     GetParity(), GetDatabits(), GetStopbits(), GetHandshake(), 
                     UartOnErrorDetected, UartOnPinChanged, UartOnTextTransmitted, UartOnLineTransmitted,
                     UartOnTextReceived, UartOnLineReceived);
          return FUart.IsOpen();
        }
        return false;
      }
      catch (Exception)// e)
      {
        FNotifier.Write("Error: Open ComPort");
        return false;
      }
    }


    public Boolean CloseComPort()
    {
      try
      {
        if (FUart.IsOpen())
        {
          FUart.Close();
          return FUart.IsClosed();
        }
        return false;
      }
      catch (Exception)// e)
      {
        FNotifier.Write("Error: Close ComPort");
        return false;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Program
    //------------------------------------------------------------------------
    //    
    public Boolean WriteText(String text)
    {
      try
      {
        if (FUart.IsOpen())
        {
          return FUart.WriteText(text);
        }
        return false;
      }
      catch (Exception)// e)
      {
        FNotifier.Write("Error: ComPort Write Text");
        return false;
      }
    }

  }
}
