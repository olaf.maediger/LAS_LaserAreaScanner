﻿namespace UCUart
{
  partial class CUCBaudrate
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxBaudrate = new System.Windows.Forms.ComboBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // cbxBaudrate
      // 
      this.cbxBaudrate.Dock = System.Windows.Forms.DockStyle.Right;
      this.cbxBaudrate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxBaudrate.FormattingEnabled = true;
      this.cbxBaudrate.Location = new System.Drawing.Point(85, 0);
      this.cbxBaudrate.Name = "cbxBaudrate";
      this.cbxBaudrate.Size = new System.Drawing.Size(64, 21);
      this.cbxBaudrate.TabIndex = 0;
      this.cbxBaudrate.SelectedIndexChanged += new System.EventHandler(this.cbxBaudrate_SelectedIndexChanged);
      // 
      // panel1
      // 
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(85, 4);
      this.panel1.TabIndex = 66;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label1.Location = new System.Drawing.Point(0, 4);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(85, 23);
      this.label1.TabIndex = 67;
      this.label1.Text = "Baudrate [baud]";
      // 
      // CUCBaudrate
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label1);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.cbxBaudrate);
      this.Name = "CUCBaudrate";
      this.Size = new System.Drawing.Size(149, 27);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ComboBox cbxBaudrate;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label1;
  }
}
