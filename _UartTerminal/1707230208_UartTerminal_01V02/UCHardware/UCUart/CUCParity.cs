﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using HWUart;
//
namespace UCUart
{
  public delegate void DOnParityChanged(EParity value);

  public partial class CUCParity : UserControl
  {
    private const String INIT_PARITY_TEXT = "None";

    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnParityChanged FOnParityChanged;

    public CUCParity()
    {
      InitializeComponent();
      cbxParity.Items.AddRange(CUart.PARITIES);
      cbxParity.SelectedIndex = (int)CUart.ParityTextIndex(INIT_PARITY_TEXT);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnParityChanged(DOnParityChanged value)
    {
      FOnParityChanged = value;
    }

    public EParity GetParity()
    {
      EParity Result = CUart.ParityTextEnumerator(cbxParity.Text);
      return Result;
    }
    public void SetParity(EParity value)
    {
      for (Int32 SI = 0; SI < cbxParity.Items.Count; SI++)
      {
        String SValue = (String)cbxParity.Items[SI];
        EParity PA = CUart.ParityTextEnumerator(SValue);
        if (value == PA)
        {
          cbxParity.SelectedIndex = SI;
          return;
        }
      }
    }



    private void cbxParity_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FOnParityChanged is DOnParityChanged)
      {
        FOnParityChanged(GetParity());
      }
    }


  }
}
