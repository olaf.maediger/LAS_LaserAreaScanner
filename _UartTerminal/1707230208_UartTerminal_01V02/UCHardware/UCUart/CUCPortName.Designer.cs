﻿namespace UCUart
{
  partial class CUCPortName
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel1 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.panel2 = new System.Windows.Forms.Panel();
      this.label5 = new System.Windows.Forms.Label();
      this.tbxPortName = new System.Windows.Forms.TextBox();
      this.panel3.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel1.Location = new System.Drawing.Point(201, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(10, 20);
      this.panel1.TabIndex = 64;
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.label5);
      this.panel3.Controls.Add(this.panel2);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel3.Location = new System.Drawing.Point(0, 0);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(55, 20);
      this.panel3.TabIndex = 75;
      // 
      // panel2
      // 
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(0, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(55, 3);
      this.panel2.TabIndex = 77;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label5.Location = new System.Drawing.Point(0, 3);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(55, 17);
      this.label5.TabIndex = 78;
      this.label5.Text = "PortName";
      // 
      // tbxPortName
      // 
      this.tbxPortName.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbxPortName.Location = new System.Drawing.Point(55, 0);
      this.tbxPortName.Name = "tbxPortName";
      this.tbxPortName.Size = new System.Drawing.Size(146, 20);
      this.tbxPortName.TabIndex = 76;
      this.tbxPortName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // CUCPortName
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.tbxPortName);
      this.Controls.Add(this.panel3);
      this.Controls.Add(this.panel1);
      this.Name = "CUCPortName";
      this.Size = new System.Drawing.Size(211, 20);
      this.panel3.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.TextBox tbxPortName;

  }
}
