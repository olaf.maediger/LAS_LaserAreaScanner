﻿namespace UCUart
{
  partial class CUCStopbits
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxStopbits = new System.Windows.Forms.ComboBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // cbxStopbits
      // 
      this.cbxStopbits.Dock = System.Windows.Forms.DockStyle.Right;
      this.cbxStopbits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxStopbits.FormattingEnabled = true;
      this.cbxStopbits.Location = new System.Drawing.Point(46, 0);
      this.cbxStopbits.Name = "cbxStopbits";
      this.cbxStopbits.Size = new System.Drawing.Size(39, 21);
      this.cbxStopbits.TabIndex = 4;
      // 
      // panel1
      // 
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(46, 4);
      this.panel1.TabIndex = 71;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label1.Location = new System.Drawing.Point(0, 4);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(46, 17);
      this.label1.TabIndex = 72;
      this.label1.Text = "Stopbits";
      // 
      // CUCStopbits
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label1);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.cbxStopbits);
      this.Name = "CUCStopbits";
      this.Size = new System.Drawing.Size(85, 21);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ComboBox cbxStopbits;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label1;
  }
}
