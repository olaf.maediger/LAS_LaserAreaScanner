﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using HWUart;
//
namespace UCUartBase
{
  public partial class CUCUartBase : UserControl
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    private const String NAME_OPENUART = "Open Uart";
    private const String NAME_CLOSEUART = "Close Uart";
    //
    private const String UART_NAME = "UartBase";
    private Color COLOR_CLOSED = Color.FromArgb(0, 255, 192, 192);
    private Color COLOR_OPENED = Color.FromArgb(0, 192, 255, 192);
    private const String TEXT_CLOSED = "---";

    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //		
    CUart FUart;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //		
    public CUCUartBase()
    {
      InitializeComponent();
      //
      FUart = new CUart();
      FUart.SetOnErrorDetected(UartOnErrorDetected);
      FUart.SetOnOpenCloseChanged(UartOnOpenCloseChanged);
      FUart.SetOnPinChanged(UartOnPinChanged);
      FUart.SetOnTextTransmitted(null); // UartOnTextTransmitted
      FUart.SetOnLineTransmitted(UartOnLineTransmitted);
      FUart.SetOnTextReceived(null);    // UartOnTextReceived
      FUart.SetOnLineReceived(UartOnLineReceived);
      btnOpenCloseUart.Text = NAME_OPENUART;
      btnRefreshComPorts_Click(this, null);
      lblComPort.BackColor = COLOR_CLOSED;
      lblComPort.Text = TEXT_CLOSED;
    }
    //
    //--------------------------------------------------------------------
    //  Segment - UartBase - Callback
    //--------------------------------------------------------------------
    //
    private void UartOnErrorDetected(Guid comportid, Int32 errorcode, String errortext)
    {
    }

    private delegate void CBUartOnOpenCloseChanged(Guid comportid, EComPort comport, Boolean opened);
    private void UartOnOpenCloseChanged(Guid comportid, EComPort comport, Boolean opened)
    {
      if (this.InvokeRequired)
      {
        CBUartOnOpenCloseChanged CB = new CBUartOnOpenCloseChanged(UartOnOpenCloseChanged);
        Invoke(CB, new object[] { comportid, comport, opened });
      }
      else
      {
        lblComPort.Text = CUart.ComPortEnumeratorName(comport);
        if (opened)
        {
          lblComPort.BackColor = COLOR_OPENED;
        }
        else
        {
          lblComPort.BackColor = COLOR_CLOSED;
          lblComPort.Text = TEXT_CLOSED;
        }
      }
    }

    private void UartOnTextTransmitted(Guid comportid, String text)
    {
        lbxUartBase.Items.Add("[TxText]" + text);
    }

    private void UartOnLineTransmitted(Guid comportid, String line)
    {
      lbxUartBase.Items.Add("[TxLine]" + line);
    }

    private delegate void CBUartOnTextReceived(Guid comportid, String text);
    private void UartOnTextReceived(Guid comportid, String text)
    {
      if (this.InvokeRequired)
      {
        CBUartOnTextReceived CB = new CBUartOnTextReceived(UartOnTextReceived);
        Invoke(CB, new object[] { comportid, text });
      }
      else
      { // nothing to do -> OnLineReceived
        lbxUartBase.Items.Add("[RxText]" + text);
      }
    }

    private delegate void CBUartOnLineReceived(Guid comportid, String line);
    private void UartOnLineReceived(Guid comportid, String line)
    {
      if (this.InvokeRequired)
      {
        CBUartOnLineReceived CB = new CBUartOnLineReceived(UartOnLineReceived);
        Invoke(CB, new object[] { comportid, line });
      }
      else
      {
        lbxUartBase.Items.Add("[RxLine]" + line);
      }
    }

    private void UartOnPinChanged(Guid comportid,
                                  Boolean pincts, Boolean pinrts,
                                  Boolean pindsr, Boolean pindtr, Boolean pindcd)
    {
    }
    //
    //--------------------------------------------------------------------
    //  Segment - UartBase - Event
    //--------------------------------------------------------------------
    //
    private void btnRefreshComPorts_Click(object sender, EventArgs e)
    {
      cbxPortsAvailable.Items.Clear();
      String[] SelectablePorts = CUart.GetSelectablePorts();
      if (null != SelectablePorts)
      {
        cbxPortsAvailable.Items.AddRange(SelectablePorts);
        int SI = cbxPortsAvailable.Items.Count - 1;
        cbxPortsAvailable.SelectedIndex = SI;
      }
    }

    private void btnOpenCloseUart_Click(object sender, EventArgs e)
    {
      if (FUart.IsOpen())
      { // Open -> Close
        FUart.Close();
        if (FUart.IsClosed())
        {
          btnOpenCloseUart.Text = NAME_OPENUART;
        }
      }
      else
      { // Close -> Open
        EComPort CP = CUart.ComPortTextEnumerator(cbxPortsAvailable.Text);
        FUart.Open(UART_NAME, CP, EBaudrate.br115200, EParity.paNone, 
                   EDatabits.db8, EStopbits.sb1, EHandshake.hsNone);
        if (FUart.IsOpen())
        {
          btnOpenCloseUart.Text = NAME_CLOSEUART;
        }
      }
      btnRefreshComPorts_Click(this, null);
    }

    // cbxSendLine: Enter -> Send
    private void cbxSendData_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        FUart.WriteLine(cbxTransmitText.Text);
      }
    }

    private void btnTransmit_Click(object sender, EventArgs e)
    {
      FUart.WriteLine(cbxTransmitText.Text);
    }
    //
    //--------------------------------------------------------------------
    //  Segment - UartBase - Public Management
    //--------------------------------------------------------------------
    //
    public void Close()
    {
      FUart.Close();
    }

  }
}

