﻿namespace UCUartTerminal
{
  partial class CUCUartTerminal
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.FUCMultiColorText = new UCMultiColorText.CUCMultiColorText();
      this.FUCTransmitLine = new UCUart.CUCTransmitLine();
      this.FUCUart = new UCUart.CUCUart();
      this.SuspendLayout();
      // 
      // FUCMultiColorText
      // 
      this.FUCMultiColorText.BackColor = System.Drawing.Color.White;
      this.FUCMultiColorText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCMultiColorText.Font = new System.Drawing.Font("Courier New", 9F);
      this.FUCMultiColorText.Location = new System.Drawing.Point(0, 111);
      this.FUCMultiColorText.Name = "FUCMultiColorText";
      this.FUCMultiColorText.Size = new System.Drawing.Size(613, 500);
      this.FUCMultiColorText.TabIndex = 11;
      // 
      // FUCTransmitLine
      // 
      this.FUCTransmitLine.DelayCharacter = 0;
      this.FUCTransmitLine.DelayCR = 0;
      this.FUCTransmitLine.DelayLF = 0;
      this.FUCTransmitLine.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.FUCTransmitLine.Location = new System.Drawing.Point(0, 611);
      this.FUCTransmitLine.Name = "FUCTransmitLine";
      this.FUCTransmitLine.Size = new System.Drawing.Size(613, 47);
      this.FUCTransmitLine.TabIndex = 10;
      // 
      // FUCUart
      // 
      this.FUCUart.BackColor = System.Drawing.Color.Gainsboro;
      this.FUCUart.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCUart.Location = new System.Drawing.Point(0, 0);
      this.FUCUart.Name = "FUCUart";
      this.FUCUart.Size = new System.Drawing.Size(613, 111);
      this.FUCUart.TabIndex = 8;
      // 
      // CUCUartTerminal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCMultiColorText);
      this.Controls.Add(this.FUCTransmitLine);
      this.Controls.Add(this.FUCUart);
      this.Name = "CUCUartTerminal";
      this.Size = new System.Drawing.Size(613, 658);
      this.ResumeLayout(false);

    }

    #endregion

    private UCUart.CUCUart FUCUart;
    private UCUart.CUCTransmitLine FUCTransmitLine;
    private UCMultiColorText.CUCMultiColorText FUCMultiColorText;

  }
}
