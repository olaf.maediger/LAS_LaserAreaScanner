using System;
using System.Collections.Generic;
using System.Text;

namespace HWUart
{
	public class CStreamFilter
	{	//
		//------------------------
		//	Constants
		//------------------------
		//
		private const Int32 INIT_TABLESIZE = 256;
		//
		//------------------------
		//	Members
		//------------------------
		//
		protected String[] FTable = null;
		//
		//------------------------
		//	Constructors
		//------------------------
		//
		protected CStreamFilter()
		{
			FTable = new String[INIT_TABLESIZE];
			// 1:1 Initialisierung
			for (Int32 FI = 0; FI < FTable.Length; FI++)
			{
				FTable[FI] = String.Format("{0}", (Char)FI);
			}
		}
		//
		//------------------------
		//	Management
		//------------------------
		//
		// liest gesamten Filter eines Zeichens
		public String GetValue(Byte newindex)
		{
			return FTable[newindex];
		}
		// L�scht alle Filterzeichen (Verschlucken!)
		public void ClearValue(Byte newindex)
		{
			FTable[newindex] = "";
		}
		// Setzt Filter auf ein Zeichen
		public void SetValue(Byte newindex, Char newcharacter)
		{
			FTable[newindex] = String.Format("{0}", newcharacter);
		}
		// Erweitert Filter um ein Zeichen
		public void AddValue(Byte newindex, Char newcharacter)
		{
			FTable[newindex] += String.Format("{0}", newcharacter);
		}
		//
		//------------------------
		//	Filtering
		//------------------------
		//
		public virtual String Execute(Char value)
		{
			Byte Index = (Byte)value;
			return FTable[Index];
		}
		public virtual String Execute(String value)
		{
			String Result = "";
			foreach (Char C in value)
			{
				Result += Execute(C);
			}
			return Result;
		}

		public virtual Byte Execute(Byte value)
		{
			Byte Index = (Byte)value;
			String S = FTable[Index];
			if (0 < S.Length)
			{
				return (Byte)S[0];
			}
			return 0x00;
		}
		public virtual Byte[] Execute(Byte[] buffer)
		{
			Byte[] Result = new Byte[buffer.Length];
			for (Int32 BI = 0; BI < buffer.Length; BI++)
			{
				Byte B = buffer[BI];
				Result[BI] = Execute(B);
			}
			return Result;
		}

	}
}
