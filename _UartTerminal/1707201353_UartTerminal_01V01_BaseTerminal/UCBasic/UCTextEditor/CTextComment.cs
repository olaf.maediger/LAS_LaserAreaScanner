﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
//
using Initdata;
using Programdata;
//
namespace UCTextEditor
{
  public class CTextComment
  { //
    //-----------------------------------------------
    //  Section - Constant
    //-----------------------------------------------
    //	
    public static String HEADER_SECTION = "TextComment";
    //
    private const String NAME_POSITIONX = "PositionX";
    private const String NAME_POSITIONY = "PositionY";
    private const String NAME_ALPHA = "Alpha";
    private const String NAME_RED = "Red";
    private const String NAME_GREEN = "Green";
    private const String NAME_BLUE = "Blue";
    //
    private const Int32 INIT_POSITIONX = 10; // [%]
    private const Int32 INIT_POSITIONY = 10; // [%]
    private const Byte INIT_ALPHA = 128;
    private const Byte INIT_RED = 0;
    private const Byte INIT_GREEN = 0;
    private const Byte INIT_BLUE = 0;
    //
    //-----------------------------------------------
    //  Section - Field
    //-----------------------------------------------
    //	
    // String FText -> UCTextEditor!!!
   // NC  private Int32 FPositionX; // [%]
    // NC private Int32 FPositionY; // [%]
   /* -> from Editor private Byte FAlpha;
    private Byte FRed;
    private Byte FGreen;
    private Byte FBlue;*/
    //
    //-----------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------
    //	
    public CTextComment()
    {
      // NC FPositionX = INIT_POSITIONX;
      // NC FPositionY = INIT_POSITIONY;
      /*FAlpha = INIT_ALPHA;
      FRed = INIT_RED;
      FGreen = INIT_GREEN;
      FBlue = INIT_BLUE;*/
    }
    //
    //-------------------------------
    //  Section - Load/Save Programdata
    //-------------------------------
    //
    public Boolean LoadProgramdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Result &= initdata.OpenSection(HEADER_SECTION);
      //
      /*Int32 IValue = INIT_POSITIONX;
      Result &= initdata.ReadInt32(NAME_POSITIONX, out IValue, IValue);
      FPositionX = IValue;
      //
      IValue = INIT_POSITIONY;
      Result &= initdata.ReadInt32(NAME_POSITIONY, out IValue, IValue);
      FPositionY = IValue;*/
      //
      /*Byte BValue = INIT_ALPHA;
      Result &= initdata.ReadByte(NAME_ALPHA, out BValue, BValue);
      FAlpha = BValue;
      //
      BValue = INIT_RED;
      Result &= initdata.ReadByte(NAME_RED, out BValue, BValue);
      FRed = BValue;
      //
      BValue = INIT_GREEN;
      Result &= initdata.ReadByte(NAME_GREEN, out BValue, BValue);
      FGreen = BValue;
      //
      BValue = INIT_BLUE;
      Result &= initdata.ReadByte(NAME_BLUE, out BValue, BValue);
      FBlue = BValue;*/
      //
      Result &= initdata.CloseSection();
      return Result;
    }

    public Boolean SaveProgramdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      Result &= initdata.CreateSection(HEADER_SECTION);
      //
     // NC  Result &= initdata.WriteInt32(NAME_POSITIONX, FPositionX);
      // NC Result &= initdata.WriteInt32(NAME_POSITIONY, FPositionY);
      /*Result &= initdata.WriteByte(NAME_ALPHA, FAlpha);
      Result &= initdata.WriteByte(NAME_RED, FRed);
      Result &= initdata.WriteByte(NAME_GREEN, FGreen);
      Result &= initdata.WriteByte(NAME_BLUE, FBlue);*/
      //
      Result &= initdata.CloseSection();
      return Result;
    }

    public Boolean RenderText(Graphics graphics,
                              Rectangle rectangle,
                              RTextEditorData texteditordata,
                              String text)
    {
      try
      {
        graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
        graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
        graphics.ResetTransform();
        graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
        graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
        graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
        //        
        Byte OpacityAbsolute = (Byte)((255 * texteditordata.FontOpacity) / 100);
        Int32 PX = rectangle.Width * texteditordata.FontPositionX / 100;
        Int32 PY = rectangle.Height * texteditordata.FontPositionY / 100;
        Point PointText = new Point(PX, PY);
        Color ColorText = Color.FromArgb(OpacityAbsolute,
                                         texteditordata.FontColor.R,
                                         texteditordata.FontColor.G,
                                         texteditordata.FontColor.B);
        Brush BrushText = new SolidBrush(ColorText);
        graphics.DrawString(text, texteditordata.Font, BrushText, PointText);
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
