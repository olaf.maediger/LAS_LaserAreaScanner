﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using HWUart;
//
namespace UCUart
{
  public delegate void DOnOpenCloseChanged(Boolean opened);
  //
  public partial class CUCUart : UserControl
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    public const String INIT_PORTNAME = "";
    public const EComPort INIT_COMPORT = EComPort.cp1;
    public const EBaudrate INIT_BAUDRATE = EBaudrate.br9600;
    public const EParity INIT_PARITY = EParity.paNone;
    public const EDatabits INIT_DATABITS = EDatabits.db8;
    public const EStopbits INIT_STOPBITS = EStopbits.sb1;
    public const EHandshake INIT_HANDSHAKE = EHandshake.hsNone;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnOpenCloseChanged FOnOpenCloseChanged;
    private DOnLineReceived FOnLineReceived;
    private CUart FUart;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    public CUCUart()
    {
      InitializeComponent();
      //
      FUCOpenClose.SetOnOpenCloseChanged(UCOpenCloseOnOpenCloseChanged);
      //
      FUart = new CUart();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      /*!!!!!
      FUCOpenClose.SetNotifier(FNotifier);
      FUCBaudrate.SetNotifier(FNotifier);
      FUCDatabits.SetNotifier(FNotifier);
      FUCHandshake.SetNotifier(FNotifier);
      FUCParity.SetNotifier(FNotifier);
      FUCStopbits.SetNotifier(FNotifier);
      FUCPortName.SetNotifier(FNotifier);
      FUCPortsInstalled.SetNotifier(FNotifier);
      FUCPortsSelectable.SetNotifier(FNotifier);
       */
    }

    public void SetOnOpenCloseChanged(DOnOpenCloseChanged value)
    {
      FOnOpenCloseChanged = value;
      FUCOpenClose.SetOnOpenCloseChanged(UCOpenCloseOnOpenCloseChanged);
    }

    public void SetOnComPortLineReceived(DOnLineReceived value)
    {
      FOnLineReceived = value;
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUart.SetOnLineReceived(HWComPortOnLineReceived);
    }

    //
    //------------------------------------------------------------------------
    //  Section - Get/SetData
    //------------------------------------------------------------------------
    //
    //public Boolean GetUCUartData(out RUCUartData data)
    //{
    //  Boolean Result = false;
    //  data = new RUCUartData(0);
    //  Result = FUart.GetComPortData(out data.ComPortData);
    //  data.ComPortData.Name = FUCPortName.GetPortName();
    //  data.ComPortData.ComPort = FUCPortsSelectable.GetPortSelected();
    //  data.ComPortData.Baudrate = FUCBaudrate.GetBaudrate();
    //  data.ComPortData.Parity = FUCParity.GetParity();
    //  data.ComPortData.Databits = FUCDatabits.GetDatabits();
    //  data.ComPortData.Stopbits = FUCStopbits.GetStopbits();
    //  data.ComPortData.Handshake = FUCHandshake.GetHandshake();
    //  Result = FUart.SetComPortData(data.ComPortData);
    //  return Result;
    //}

    //public Boolean SetUCUartData(RUCUartData data)
    //{
    //  Boolean Result = false;
    //  FUCPortName.SetPortName(data.ComPortData.Name);
    //  FUCPortsSelectable.SetPortSelected(data.ComPortData.ComPort);
    //  FUCBaudrate.SetBaudrate(data.ComPortData.Baudrate);
    //  FUCParity.SetParity(data.ComPortData.Parity);
    //  FUCDatabits.SetDatabits(data.ComPortData.Databits);
    //  FUCStopbits.SetStopbits(data.ComPortData.Stopbits);
    //  FUCHandshake.SetHandshake(data.ComPortData.Handshake);
    //  Result = FUart.SetComPortData(data.ComPortData);
    //  return Result;
    //}
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //
    private void EnableComPortControls(Boolean enable)
    {
      FUCPortName.Enabled = enable;
      FUCPortsSelectable.Enabled = enable;
      FUCBaudrate.Enabled = enable;
      FUCParity.Enabled = enable;
      FUCDatabits.Enabled = enable;
      FUCStopbits.Enabled = enable;
      FUCHandshake.Enabled = enable;
      btnRefresh.Enabled = enable;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback
    //------------------------------------------------------------------------
    //
    private void UCOpenCloseOnOpenCloseChanged(Boolean opened)
    {
      if (opened)
      {
        OpenComPort();
      }
      else
      {
        CloseComPort();
      }
      EnableComPortControls(!FUart.IsOpen());
      if (FOnOpenCloseChanged is DOnOpenCloseChanged)
      {
        FOnOpenCloseChanged(opened);
      }
    }

    private void HWComPortOnLineReceived(Guid comportid, String line)
    {
      if (FOnLineReceived is DOnLineReceived)
      {
        FOnLineReceived(comportid, line);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event - Control
    //------------------------------------------------------------------------
    //
    private void btnRefresh_Click(object sender, EventArgs e)
    {
      RefreshComPortControls();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Public Management
    //------------------------------------------------------------------------
    //    
    public void RefreshComPortControls()
    {
      FUCPortsInstalled.RefreshPortsInstalled();
      FUCPortsSelectable.RefreshPortsSelectable();
    }

    public void RefreshOpenCloseComPort(Boolean opened)
    {
      EnableComPortControls(opened);
    }

    public Boolean OpenComPort()
    {
      try
      {
        if (FUart.IsClosed())
        {
          //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!RUCUartData UCUartData;
          //if (GetUCUartData(out UCUartData))
          //{
          //  if (FUart.Open(UCUartData.ComPortData))
          //  {
          //    if (GetUCUartData(out UCUartData))
          //    {
          //      return UCUartData.ComPortData.IsOpen;
          //    }
          //  }
          //}
        }
        return false;
      }
      catch (Exception)// e)
      {
        FNotifier.Write("Error: Open ComPort");
        return false;
      }
    }


    public Boolean CloseComPort()
    {
      try
      {
        if (FUart.IsOpen())
        {
          if (FUart.Close())
          {
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!RUCUartData UCUartData;
            //if (GetUCUartData(out UCUartData))
            //{
            //  return !UCUartData.ComPortData.IsOpen;
            //}
          }
        }
        return false;
      }
      catch (Exception)// e)
      {
        FNotifier.Write("Error: Close ComPort");
        return false;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Program
    //------------------------------------------------------------------------
    //    
    public Boolean WriteText(String text)
    {
      try
      {
        if (FUart.IsOpen())
        {
          return FUart.WriteText(text);
        }
        return false;
      }
      catch (Exception)// e)
      {
        FNotifier.Write("Error: ComPort Write Text");
        return false;
      }
    }

  }
}
