﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using HWUart;
//
namespace UCUart
{
  public delegate void DOnDatabitsChanged(EDatabits value);

  public partial class CUCDatabits : UserControl
  {
    private const String INIT_DATABITS_TEXT = "8";


    private DOnDatabitsChanged FOnDatabitsChanged;

    public CUCDatabits()
    {
      InitializeComponent();
      cbxDatabits.Items.AddRange(CUart.DATABITS);
      cbxDatabits.SelectedIndex = (int)CUart.DatabitsTextIndex(INIT_DATABITS_TEXT);
    }

    public void SetOnDatabitsChanged(DOnDatabitsChanged value)
    {
      FOnDatabitsChanged = value;
    }

    public EDatabits GetDatabits()
    {
      EDatabits Result = CUart.DatabitsTextEnumerator(cbxDatabits.Text);
      return Result;
    }
    public void SetDatabits(EDatabits value)
    {
      for (Int32 SI = 0; SI < cbxDatabits.Items.Count; SI++)
      {
        String SValue = (String)cbxDatabits.Items[SI];
        EDatabits PA = CUart.DatabitsTextEnumerator(SValue);
        if (value == PA)
        {
          cbxDatabits.SelectedIndex = SI;
          return;
        }
      }
    }



    private void cbxDatabits_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FOnDatabitsChanged is DOnDatabitsChanged)
      {
        FOnDatabitsChanged(GetDatabits());
      }
    }
  }
}
