﻿namespace UCUart
{
  partial class CUCBaudrate
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxBaudrate = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // cbxBaudrate
      // 
      this.cbxBaudrate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxBaudrate.FormattingEnabled = true;
      this.cbxBaudrate.Location = new System.Drawing.Point(94, 3);
      this.cbxBaudrate.Name = "cbxBaudrate";
      this.cbxBaudrate.Size = new System.Drawing.Size(64, 21);
      this.cbxBaudrate.TabIndex = 0;
      this.cbxBaudrate.SelectedIndexChanged += new System.EventHandler(this.cbxBaudrate_SelectedIndexChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 6);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(89, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Baudrate [baud] :";
      // 
      // CUCBaudrate
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label1);
      this.Controls.Add(this.cbxBaudrate);
      this.Name = "CUCBaudrate";
      this.Size = new System.Drawing.Size(161, 27);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox cbxBaudrate;
    private System.Windows.Forms.Label label1;
  }
}
