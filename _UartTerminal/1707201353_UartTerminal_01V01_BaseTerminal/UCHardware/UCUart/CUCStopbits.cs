﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using HWUart;
//
namespace UCUart
{
  public delegate void DOnStopbitsChanged(EStopbits value);

  public partial class CUCStopbits : UserControl
  {
    private const String INIT_STOPBITS_TEXT = "1";


    private DOnStopbitsChanged FOnStopbitsChanged;

    public CUCStopbits()
    {
      InitializeComponent();
      cbxStopbits.Items.AddRange(CUart.STOPBITS);
      cbxStopbits.SelectedIndex = (int)CUart.StopbitsTextIndex(INIT_STOPBITS_TEXT);
    }

    public void SetOnStopbitsChanged(DOnStopbitsChanged value)
    {
      FOnStopbitsChanged = value;
    }

    public EStopbits GetStopbits()
    {
      EStopbits Result = CUart.StopbitsTextEnumerator(cbxStopbits.Text);
      return Result;
    }
    public void SetStopbits(EStopbits value)
    {
      for (Int32 SI = 0; SI < cbxStopbits.Items.Count; SI++)
      {
        String SValue = (String)cbxStopbits.Items[SI];
        EStopbits PA = CUart.StopbitsTextEnumerator(SValue);
        if (value == PA)
        {
          cbxStopbits.SelectedIndex = SI;
          return;
        }
      }
    }



    private void cbxStopbits_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FOnStopbitsChanged is DOnStopbitsChanged)
      {
        FOnStopbitsChanged(GetStopbits());
      }
    }
  }
}
