﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using HWUart;
//
namespace UartTerminal
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICETYPE = "Uart";
    private const String INIT_DEVICENAME = "OMUart";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    private const String NAME_OPENUART = "Open Uart";
    private const String NAME_CLOSEUART = "Close Uart";
    //
    private const String UART_NAME = "UartBase";
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    Random FRandom;
    CUart FUartBase; // only for UartBaseTab!!!
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FRandom = new Random((Int32)DateTime.Now.Ticks);
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      FUartBase = new CUart();
      btnOpenCloseUart.Text = NAME_OPENUART;
      btnRefreshComPorts_Click(this, null);
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FUartBase.Close();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      ////// /////////////////////////?????FUdpTextArrayServer.Close();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    // 

    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //#########################################################################
    //  Segment - Callback -
    //#########################################################################
    //
   

    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          tbcMain.SelectedIndex = 0;
          return true;
        case 1:

          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5: 
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }
    //
    //####################################################################
    //  Segment - UartBase
    //####################################################################
    //
    private void btnRefreshComPorts_Click(object sender, EventArgs e)
    {
      cbxPortsAvailable.Items.Clear();
      String[] SelectablePorts = CUart.GetSelectablePorts();
      if (null != SelectablePorts)
      {
        cbxPortsAvailable.Items.AddRange(SelectablePorts);
        int SI = cbxPortsAvailable.Items.Count - 1;
        cbxPortsAvailable.SelectedIndex = SI;
      }
    }

    private void btnOpenCloseUart_Click(object sender, EventArgs e)
    {
      if (FUartBase.IsOpen())
      { // Open -> Close
        FUartBase.Close();
        if (FUartBase.IsClosed())
        {
          btnOpenCloseUart.Text = NAME_OPENUART;
        }
      }
      else
      { // Close -> Open
        EComPort CP = CUart.ComPortTextEnumerator(cbxPortsAvailable.Text);
        FUartBase.Open(UART_NAME, CP,
                       EBaudrate.br115200, EParity.paNone, EDatabits.db8,
                       EStopbits.sb1, EHandshake.hsNone,
                       // only lines! UartOnErrorDetected, UartOnTextTransmitted, UartOnLineTransmitted,
                       // only lines! UartOnTextReceived, UartOnLineReceived, UartOnPinChanged);
                       UartOnErrorDetected, UartOnPinChanged,
                       null, UartOnLineTransmitted,
                       null, UartOnLineReceived);
        if (FUartBase.IsOpen())
        {
          btnOpenCloseUart.Text = NAME_CLOSEUART;
        }
      }
    }

    private void btnTransmit_Click(object sender, EventArgs e)
    {
      String Text = cbxTransmitText.Text;
      FUartBase.WriteLine(Text);
    }
    //
    //--------------------------------------------------------------------
    //  Segment - UartBase - Callback
    //--------------------------------------------------------------------
    //
    private void UartOnErrorDetected(Guid comportid, Int32 errorcode, String errortext)
    {
    }

    private void UartOnTextTransmitted(Guid comportid, String text)
    {
      lbxUartBase.Items.Add("[TxText]" + text);
    }

    private void UartOnLineTransmitted(Guid comportid, String line)
    {
      lbxUartBase.Items.Add("[TxLine]" + line);
    }

    private delegate void CBUartOnTextReceived(Guid comportid, String text);
    private void UartOnTextReceived(Guid comportid, String text)
    {
      if (this.InvokeRequired)
      {
        CBUartOnTextReceived CB = new CBUartOnTextReceived(UartOnTextReceived);
        Invoke(CB, new object[] { comportid, text });
      }
      else
      { // nothing to do -> OnLineReceived
        lbxUartBase.Items.Add("[RxText]" + text);
      }      
    }

    private delegate void CBUartOnLineReceived(Guid comportid, String line);
    private void UartOnLineReceived(Guid comportid, String line)
    {
      if (this.InvokeRequired)
      {
        CBUartOnLineReceived CB = new CBUartOnLineReceived(UartOnLineReceived);
        Invoke(CB, new object[] { comportid, line });
      }
      else
      {
        lbxUartBase.Items.Add("[RxLine]" + line);
      }
    }

    private void UartOnPinChanged(Guid comportid,
                                  Boolean pincts, Boolean pinrts,
                                  Boolean pindsr, Boolean pindtr, Boolean pindcd)
    {
    }
    //
    //--------------------------------------------------------------------
    //  Segment - UartBase - 
    //--------------------------------------------------------------------
    //

  }
}

    //
    //--------------------------------------------------------------------
    //  Segment - UartBase - 
    //--------------------------------------------------------------------
    //


//
//--------------------------------------------------------------------
//  Segment - 
//--------------------------------------------------------------------
//
