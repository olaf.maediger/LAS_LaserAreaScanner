﻿namespace UCUartBase
{
  partial class CUCUartBase
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlTop = new System.Windows.Forms.Panel();
      this.btnRefreshComPorts = new System.Windows.Forms.Button();
      this.label5 = new System.Windows.Forms.Label();
      this.cbxPortsAvailable = new System.Windows.Forms.ComboBox();
      this.btnOpenCloseUart = new System.Windows.Forms.Button();
      this.pnlBottom = new System.Windows.Forms.Panel();
      this.cbxTransmitText = new System.Windows.Forms.ComboBox();
      this.panel3 = new System.Windows.Forms.Panel();
      this.panel2 = new System.Windows.Forms.Panel();
      this.panel1 = new System.Windows.Forms.Panel();
      this.btnTransmit = new System.Windows.Forms.Button();
      this.lbxUartBase = new System.Windows.Forms.ListBox();
      this.pnlTop.SuspendLayout();
      this.pnlBottom.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlTop
      // 
      this.pnlTop.BackColor = System.Drawing.Color.FloralWhite;
      this.pnlTop.Controls.Add(this.btnRefreshComPorts);
      this.pnlTop.Controls.Add(this.label5);
      this.pnlTop.Controls.Add(this.cbxPortsAvailable);
      this.pnlTop.Controls.Add(this.btnOpenCloseUart);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(386, 34);
      this.pnlTop.TabIndex = 1;
      // 
      // btnRefreshComPorts
      // 
      this.btnRefreshComPorts.Location = new System.Drawing.Point(221, 5);
      this.btnRefreshComPorts.Name = "btnRefreshComPorts";
      this.btnRefreshComPorts.Size = new System.Drawing.Size(61, 23);
      this.btnRefreshComPorts.TabIndex = 58;
      this.btnRefreshComPorts.Text = "Refresh";
      this.btnRefreshComPorts.UseVisualStyleBackColor = true;
      this.btnRefreshComPorts.Click += new System.EventHandler(this.btnRefreshComPorts_Click);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(102, 11);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(52, 13);
      this.label5.TabIndex = 57;
      this.label5.Text = "ComPorts";
      // 
      // cbxPortsAvailable
      // 
      this.cbxPortsAvailable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPortsAvailable.FormattingEnabled = true;
      this.cbxPortsAvailable.Location = new System.Drawing.Point(155, 7);
      this.cbxPortsAvailable.Name = "cbxPortsAvailable";
      this.cbxPortsAvailable.Size = new System.Drawing.Size(60, 21);
      this.cbxPortsAvailable.TabIndex = 56;
      // 
      // btnOpenCloseUart
      // 
      this.btnOpenCloseUart.Location = new System.Drawing.Point(8, 6);
      this.btnOpenCloseUart.Name = "btnOpenCloseUart";
      this.btnOpenCloseUart.Size = new System.Drawing.Size(75, 23);
      this.btnOpenCloseUart.TabIndex = 0;
      this.btnOpenCloseUart.Text = "opencloseuart";
      this.btnOpenCloseUart.UseVisualStyleBackColor = true;
      this.btnOpenCloseUart.Click += new System.EventHandler(this.btnOpenCloseUart_Click);
      // 
      // pnlBottom
      // 
      this.pnlBottom.BackColor = System.Drawing.Color.SeaShell;
      this.pnlBottom.Controls.Add(this.cbxTransmitText);
      this.pnlBottom.Controls.Add(this.panel3);
      this.pnlBottom.Controls.Add(this.panel2);
      this.pnlBottom.Controls.Add(this.panel1);
      this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlBottom.Location = new System.Drawing.Point(0, 199);
      this.pnlBottom.Name = "pnlBottom";
      this.pnlBottom.Size = new System.Drawing.Size(386, 34);
      this.pnlBottom.TabIndex = 2;
      // 
      // cbxTransmitText
      // 
      this.cbxTransmitText.Dock = System.Windows.Forms.DockStyle.Top;
      this.cbxTransmitText.FormattingEnabled = true;
      this.cbxTransmitText.Items.AddRange(new object[] {
            "Hello World!",
            "FGX[123456][0AC]",
            "FGY[123460][0BD]",
            "FGZ[123470][023]"});
      this.cbxTransmitText.Location = new System.Drawing.Point(91, 7);
      this.cbxTransmitText.Name = "cbxTransmitText";
      this.cbxTransmitText.Size = new System.Drawing.Size(280, 21);
      this.cbxTransmitText.TabIndex = 5;
      // 
      // panel3
      // 
      this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel3.Location = new System.Drawing.Point(91, 0);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(280, 7);
      this.panel3.TabIndex = 4;
      // 
      // panel2
      // 
      this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel2.Location = new System.Drawing.Point(371, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(15, 34);
      this.panel2.TabIndex = 3;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.btnTransmit);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(91, 34);
      this.panel1.TabIndex = 1;
      // 
      // btnTransmit
      // 
      this.btnTransmit.Location = new System.Drawing.Point(8, 6);
      this.btnTransmit.Name = "btnTransmit";
      this.btnTransmit.Size = new System.Drawing.Size(75, 23);
      this.btnTransmit.TabIndex = 0;
      this.btnTransmit.Text = "Transmit";
      this.btnTransmit.UseVisualStyleBackColor = true;
      this.btnTransmit.Click += new System.EventHandler(this.btnTransmit_Click);
      // 
      // lbxUartBase
      // 
      this.lbxUartBase.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbxUartBase.FormattingEnabled = true;
      this.lbxUartBase.IntegralHeight = false;
      this.lbxUartBase.Location = new System.Drawing.Point(0, 34);
      this.lbxUartBase.Name = "lbxUartBase";
      this.lbxUartBase.Size = new System.Drawing.Size(386, 165);
      this.lbxUartBase.TabIndex = 3;
      // 
      // CUCUartBase
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lbxUartBase);
      this.Controls.Add(this.pnlBottom);
      this.Controls.Add(this.pnlTop);
      this.Name = "CUCUartBase";
      this.Size = new System.Drawing.Size(386, 233);
      this.pnlTop.ResumeLayout(false);
      this.pnlTop.PerformLayout();
      this.pnlBottom.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.Button btnRefreshComPorts;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.ComboBox cbxPortsAvailable;
    private System.Windows.Forms.Button btnOpenCloseUart;
    private System.Windows.Forms.Panel pnlBottom;
    private System.Windows.Forms.ComboBox cbxTransmitText;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button btnTransmit;
    private System.Windows.Forms.ListBox lbxUartBase;
  }
}
