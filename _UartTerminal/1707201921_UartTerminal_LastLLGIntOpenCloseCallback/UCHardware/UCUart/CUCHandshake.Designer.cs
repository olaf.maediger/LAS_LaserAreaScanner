﻿namespace UCUart
{
  partial class CUCHandshake
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.cbxHandshake = new System.Windows.Forms.ComboBox();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(5, 7);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(68, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "Handshake :";
      // 
      // cbxHandshake
      // 
      this.cbxHandshake.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxHandshake.FormattingEnabled = true;
      this.cbxHandshake.Location = new System.Drawing.Point(75, 5);
      this.cbxHandshake.Name = "cbxHandshake";
      this.cbxHandshake.Size = new System.Drawing.Size(72, 21);
      this.cbxHandshake.TabIndex = 4;
      // 
      // CUCHandshake
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label1);
      this.Controls.Add(this.cbxHandshake);
      this.Name = "CUCHandshake";
      this.Size = new System.Drawing.Size(150, 29);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox cbxHandshake;
  }
}
