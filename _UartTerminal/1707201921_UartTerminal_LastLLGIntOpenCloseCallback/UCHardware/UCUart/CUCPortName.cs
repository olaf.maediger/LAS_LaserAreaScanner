﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using HWUart;
//
namespace UCUart
{
  public delegate void DOnPortNameChanged(String value);

  public partial class CUCPortName : UserControl
  {
    private const String INIT_PORTNAME_TEXT = "PortName";

    private DOnPortNameChanged FOnPortNameChanged;

    public CUCPortName()
    {
      InitializeComponent();
      tbxPortName.Text = INIT_PORTNAME_TEXT;
    }

    public void SetOnPortNameChanged(DOnPortNameChanged value)
    {
      FOnPortNameChanged = value;
    }

    public String GetPortName()
    {
      String Result = INIT_PORTNAME_TEXT;
      if ("" == tbxPortName.Text)
      {
        tbxPortName.Text = Result;
      }
      Result = tbxPortName.Text;
      return Result;
    }
    public void SetPortName(String value)
    {
      if (0 < value.Length)
      {
        tbxPortName.Text = value;
      }
    }


    private void tbxPortName_TextChanged(object sender, EventArgs e)
    {
      if (FOnPortNameChanged is DOnPortNameChanged)
      {
        FOnPortNameChanged(GetPortName());
      }
    }


  }
}