﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using HWUart;
//
namespace UCUart
{
  public partial class CUCPortsInstalled : UserControl
  {
    public CUCPortsInstalled()
    {
      InitializeComponent();
    }

    public void RefreshPortsInstalled()
    {
      cbxPortsInstalled.Items.Clear();
      cbxPortsInstalled.Items.AddRange(CUart.GetInstalledPorts());
      int SI = cbxPortsInstalled.Items.Count - 1;
      cbxPortsInstalled.SelectedIndex = SI;
    }

  }
}
