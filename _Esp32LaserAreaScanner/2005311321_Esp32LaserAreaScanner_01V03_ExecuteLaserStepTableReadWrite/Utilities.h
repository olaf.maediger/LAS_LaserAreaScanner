#ifndef Utilities_h
#define Utilities_h
//
#include "Defines.h"
//
UInt8 CharacterHexadecimalToByte(Character character);
Character DigitHexadecimalToCharacter(UInt8 digit);
PCharacter UInt8ToAsciiHexadecimal(UInt8 value, PCharacter buffer);
PCharacter UInt16ToAsciiHexadecimal(UInt16 value, PCharacter buffer);
UInt8 AsciiHexadecimalToByte(PCharacter text);
PCharacter Int16ToAsciiHexadecimal(Int16 value, PCharacter buffer);
PCharacter Int16ToAsciiDecimal(Int16 value, PCharacter buffer);
PCharacter UInt16ToAsciiDecimal(Int16 value, PCharacter buffer);
PCharacter Float32ToAscii(Float32 value, PCharacter buffer);
PCharacter BooleanToAscii(Boolean value, PCharacter buffer);
//
String TemperatureDouble64String(Double64 value);
//
String BuildTime(long unsigned milliseconds);
//
Byte BcdToBin(Byte bcdvalue);
Byte BinToBcd(Byte binvalue);
//
#endif // Utilities_h
