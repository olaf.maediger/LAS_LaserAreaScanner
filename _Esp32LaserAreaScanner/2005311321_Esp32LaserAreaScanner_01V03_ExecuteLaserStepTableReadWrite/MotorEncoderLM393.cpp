//
//--------------------------------
//  Library MotorEncoderLM393
//--------------------------------
//
#include "Defines.h"
//
#if defined(MOTORENCODERLM393_ISPLUGGED)
//
//#include "MECError.h"
#include "MotorEncoderLM393.h"
//
CMotorEncoderLM393::CMotorEncoderLM393(int pinpulsea,
                                       TInterruptFunction pirqfunctionpulsea)
{
  FState = INIT_STATEMOTORENCODER;
  FPIrqFunctionPulseA = pirqfunctionpulsea;
  FPinPulseA = pinpulsea;
  FPositionActualS = INIT_ENCODERCOUNT;
  FPositionLastS = 1 + INIT_ENCODERCOUNT;
}

bool CMotorEncoderLM393::Open()
{
  SetMoveDirection(medIdle);
  attachInterrupt(digitalPinToInterrupt(FPinPulseA), FPIrqFunctionPulseA, CHANGE);
  FPositionActualS = INIT_ENCODERCOUNT;
  FPositionLastS = 1 + INIT_ENCODERCOUNT;
  return true;
}

bool CMotorEncoderLM393::Close()
{
  SetMoveDirection(medIdle);
  pinMode(FPinPulseA, INPUT_PULLDOWN);
  detachInterrupt(FPinPulseA);
  return true;
}

void CMotorEncoderLM393::SetMoveDirection(EStateMotorEncoderLM393 state)
{
  switch (state)
  {
    case medPositive:
      FState = state;
      break;
    case medNegative:
      FState = state;
      break;
    case medIdle:
      FState = state;
      break;
    default:
      FState = medError;
      break;
  }
}

bool CMotorEncoderLM393::GetPositionChanged(Int32 &positionactual, Int32 &deltaposition)
{
  positionactual = FPositionActualS;
  deltaposition = 0L;
  if (FPositionActualS != FPositionLastS)
  {
    deltaposition = FPositionActualS - FPositionLastS;
    FPositionLastS = FPositionActualS;
    return true;
  }
  return false;
}

void CMotorEncoderLM393::HandleInterrupt()
{
  if (medPositive == FState)
  {
    FPositionActualS++;
  }
  else
  if (medNegative == FState)
  {
    FPositionActualS--;
  }
  else
  {
    FErrorCount++;
  }  
}
//
//  bool LevelActualA = (0 < digitalRead(FPinPulseA));
//
#endif // MOTORENCODERLM393_ISPLUGGED
//








////                                0b0000.0010        0b0000.0001
//void CMotorEncoder::HandleLevel(bool levelactualb, bool levelactuala)
//{ //---------- State 0--0
//  //   - B1 A0 - <- B0 A0 -> + B0 A1 +
//  if (!FLevelPresetB && !FLevelPresetA)
//  { //   B0 A0 -> + B0 A1 +
//    if (!levelactualb && levelactuala)
//    { 
//      FStepCountActual++;
//      FState = smePositive;
//    }
//    else
//    // - B1 A0 - <- B0 A0
//    if (levelactualb && !levelactuala)
//    {
//      FStepCountActual--;
//      FState = smeNegative;
//    }
//    else
//    { // error
//      FState = smeError;
//      SCPError.SetCode(ecDecoderStateImpossible);
//    }
//  }
//  else //----- State 0--1
//  //   - B0 A0 - <- B0 A1 -> + B1 A1 +
//  if (!FLevelPresetB && FLevelPresetA)
//  { //   B0 A1 -> + B1 A1 +
//    if (levelactualb && levelactuala)
//    { 
//      FStepCountActual++;
//      FState = smePositive;
//    }
//    else
//    // - B0 A0 - <- B0 A1
//    if (!levelactualb && !levelactuala)
//    {
//      FStepCountActual--;
//      FState = smeNegative;
//    }
//    else
//    { // error
//      FState = smeError;
//      SCPError.SetCode(ecDecoderStateImpossible);
//    }
//  }
//  else //----- State 1--1
//  //   - B0 A1 - <- B1 A1 -> + B1 A0 +
//  if (FLevelPresetB && FLevelPresetA)
//  { //   B1 A1 -> + B1 A0 +
//    if (levelactualb && !levelactuala)
//    { 
//      FStepCountActual++;
//      FState = smePositive;
//    }
//    else
//    // - B0 A1 - <- B1 A1
//    if (!levelactualb && levelactuala)
//    { 
//      FStepCountActual--;
//      FState = smeNegative;
//    }
//    else
//    { // error
//      FState = smeError;
//      SCPError.SetCode(ecDecoderStateImpossible);
//    }
//  }
//  else //----- State 1--0
//  //   - B1 A1 - <- B1 A0 -> + B0 A0 +
//  if (FLevelPresetB && !FLevelPresetA)
//  { //   B1 A0 -> + B0 A0 +
//    if (!levelactualb && !levelactuala)
//    { 
//      FStepCountActual++;
//      FState = smePositive;
//    }
//    else
//    // - B1 A1 - <- B1 A0
//    if (levelactualb && levelactuala)
//    { 
//      FStepCountActual--;
//      FState = smeNegative;
//    }
//    else
//    { // error
//      FState = smeError;
//      SCPError.SetCode(ecDecoderStateImpossible);
//    }
//  }
//}

//-------------------------
//  Counterwise <- ++
//-------------------------
//  A-0011001100
//  B-0001100110
//-------------------------
//  AB  Time
//  00  n+0
//  10  n+1
//  11  n+2
//  01  n+3
//  00  n+4
//
//-------------------------
//  CounterClockwise -> --
//-------------------------
//  A-0001100110
//  B-0011001100
//-------------------------
//  AB  Time
//  00  n+0
//  01  n+1
//  11  n+2
//  10  n+3
//  00  n+4
//
//void CMotorEncoderLM393::HandleLevel(bool levelactuala, bool levelactualb)
//{ //---------- Actual AB[00]
//  if (!levelactuala && !levelactualb)
//  { // [00] <- [XX]
//    if (!FLevelPresetA && !FLevelPresetB)
//    { // [00] <- [00] !Error
//      FErrorCount++;
//    }
//    else
//    if (!FLevelPresetA && FLevelPresetB)
//    { // [00] <- [01] 
//      FPositionActualS++;
//    }
//    else
//    if (FLevelPresetA && !FLevelPresetB)
//    { // [00] <- [10]
//      FPositionActualS--;
//    }
//    else
//    if (FLevelPresetA && FLevelPresetB)
//    { // [00] <- [11] !Error
//      FErrorCount++;
//    }
//  }
//  else //---------- Actual AB[01]
//  if (!levelactuala && levelactualb)
//  { // [01] <- [XX]
//    if (!FLevelPresetA && !FLevelPresetB)
//    { // [01] <- [00]
//      FPositionActualS--;
//    }
//    else
//    if (!FLevelPresetA && FLevelPresetB)
//    { // [01] <- [01] !Error
//      FErrorCount++;
//    }
//    else
//    if (FLevelPresetA && !FLevelPresetB)
//    { // [01] <- [10] !Error
//      FErrorCount++;
//    }
//    else
//    if (FLevelPresetA && FLevelPresetB)
//    { // [01] <- [11]
//      FPositionActualS++;
//    }    
//  }
//  else //---------- Actual AB[10]
//  if (levelactuala && !levelactualb)
//  { // [10] <- [XX]
//    if (!FLevelPresetA && !FLevelPresetB)
//    { // [10] <- [00]
//      FPositionActualS++;
//    }
//    else
//    if (!FLevelPresetA && FLevelPresetB)
//    { // [10] <- [01] !Error
//      FErrorCount++;
//    }
//    else
//    if (FLevelPresetA && !FLevelPresetB)
//    { // [10] <- [10] !Error      
//      FErrorCount++;
//    }
//    else
//    if (FLevelPresetA && FLevelPresetB)
//    { // [10] <- [11]
//      FPositionActualS--;
//    }    
//  }
//  else //---------- Actual AB[11]
//  if (levelactuala && levelactualb)
//  { // [11] <- [XX]
//    if (!FLevelPresetA && !FLevelPresetB)
//    { // [11] <- [00] !Error
//      FErrorCount++;
//    }
//    else
//    if (!FLevelPresetA && FLevelPresetB)
//    { // [11] <- [01]
//      FPositionActualS--;
//    }
//    else
//    if (FLevelPresetA && !FLevelPresetB)
//    { // [11] <- [10]
//      FPositionActualS++;
//    }
//    else
//    if (FLevelPresetA && FLevelPresetB)
//    { // [11] <- [11] !Error
//      FErrorCount++;
//    }        
//  }
//}

//void CMotorEncoderLM393::HandleLevel(bool levelactuala)
//{
//  if (levelactuala && !FLevelPresetA)
//  { // HIGH ( L -> H )
//    FLevelPresetA = levelactuala;
//    if (smePositive == FState)
//    {
//      FPositionActualS++;
//    }
//    else
//    if (smeNegative == FState)
//    {
//      FPositionActualS--;
//    }
//  }
//  else
//  if (!levelactuala && FLevelPresetA)
//  { // LOW ( H -> L )
//    FLevelPresetA = levelactuala;
//    if (smePositive == FState)
//    {
//      FPositionActualS++;
//    }
//    else
//    if (smeNegative == FState)
//    {
//      FPositionActualS--;
//    }
//  }
//  else
//  {
//    FErrorCount++;
//  }  
//}
