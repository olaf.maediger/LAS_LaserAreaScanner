#include "Defines.h"
//
#include "Error.h"
//
#ifdef SERIALCOMMAND_ISPLUGGED
#include "Serial.h"
#endif
#ifdef MQTTCLIENT_ISPLUGGED
#include "MQTTClient.h"
#endif
//
#ifdef SERIALCOMMAND_ISPLUGGED
extern CSerial SerialCommand;
#endif
#ifdef MQTTCLIENT_ISPLUGGED
extern CMQTTClient MQTTClient;
#endif
//
CError::CError(void)
{
  SetCode(INIT_ERRORCODE);
}

EErrorCode CError::GetCode(void)
{
  return FErrorCode;  
}
void CError::SetCode(EErrorCode errorcode)
{
  FErrorCode = errorcode;
}
  
Boolean CError::Open(void)
{
  FErrorCode = ecNone;
  return true; 
}
  
Boolean CError::Close(void)
{
  FErrorCode = ecNone;
  return true; 
}

char* CError::ErrorCodeText(EErrorCode errorcode)
{
  switch (errorcode)
  {
    case ecNone:
      return "No Error";
    case ecInvalidCommand:
      return "Invalid Command";
    case ecCommandTimingFailure:
      return "Command Timing Failure";
    case ecCommandInvalidParameter:
      return "Command invalid Parameter";
    case ecToManyParameters:
      return "To Many Parameters";
    case ecNotEnoughParameters:
      return "Not Enough Parameters";
    case ecMissingTargetParameter:
      return "Missing Target Parameter";
    case ecFailMountard:
      return "Fail Mount ard";    
    case ecFailOpenCommandFile:
      return "Fail Open Command File";
    case ecFailParseCommandFile:
      return "Fail Parse Command File";
    case ecFailWriteCommandFile:
      return "Fail Write Command File";
    case ecFailCloseCommandFile:
      return "Fail Close Command File";
    case ecFailUnmountard:
      return "Fail Unmount ard";
    case ecCommandFileNotOpened:
      return "Command File Not Opened";
    case ecDecoderStateImpossible:
      return "Decoder State Impossible";
    default: // ecUnknown:
      return "Unknown Error"; 
  }
}

Boolean CError::Handle(CSerial &serial)
{
  if (ecNone != FErrorCode)
  {  
    char Buffer[48];
    sprintf(Buffer, "%s Error[%i]: %s!", TERMINAL_ERROR, FErrorCode, ErrorCodeText(FErrorCode));
#ifdef SERIALCOMMAND_ISPLUGGED
    SerialCommand.Write(Buffer);
    SerialCommand.WriteNewLine();
    SerialCommand.WritePrompt();
#endif
//
#ifdef MQTTCLIENT_ISPLUGGED
    MQTTClient.Publish("/Error", Buffer);
#endif
    FErrorCode = ecNone;
    return true;
  }
  return false;
}
