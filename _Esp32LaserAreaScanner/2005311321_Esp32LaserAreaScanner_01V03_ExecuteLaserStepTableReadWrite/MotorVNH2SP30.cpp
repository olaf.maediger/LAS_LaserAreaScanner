#include "Defines.h"
#if defined(MOTORVNH2SP30_ISPLUGGED)
//
//--------------------------------
//  Library MotorVNH2SP30
//--------------------------------
//
#include "Command.h"
#include "MotorVNH2SP30.h"
//
//--------------------------------
//  Segment - Constructor
//--------------------------------
//
CMotorVNH2SP30::CMotorVNH2SP30(Byte pwmchannel, Byte pwmresolution,
                               UInt16 pwmlow, UInt16 pwmhigh,
                               UInt32 pwmfrequency, int pinina, 
                               int pininb, int pinpwm)
{                              
  FState = smdIdle;
  FPwmChannel = pwmchannel;
  FPwmResolution = pwmresolution;
  FPwmFrequency = pwmfrequency;
  FPinINA = pinina;
  FPinINB = pininb;
  FPinPWM = pinpwm;
  FPwmLow = pwmlow;
  FPwmHigh = pwmhigh;
}
//
//--------------------------------
//  Segment - Property
//--------------------------------
//
EStateMotorVNH2SP30 CMotorVNH2SP30::GetState()
{
  return FState;
}
void CMotorVNH2SP30::SetState(EStateMotorVNH2SP30 state)
{
  if (state != FState)
  {   
    FState = state;
//// DEBUG    Serial1.print("! State[");
//// DEBUG    Serial1.print(FState);
//// DEBUG    Serial1.print("] PA[");
//// DEBUG    Serial1.print(FPositionActualS);
//// DEBUG    Serial1.print("] PT[");
//// DEBUG    Serial1.print(FPositionTargetS);
//// DEBUG    Serial1.println("]");
  }
}
//
//--------------------------------
//  Segment - Management
//--------------------------------
//
Boolean CMotorVNH2SP30::Open()
{
  pinMode(FPinINA, OUTPUT);
  digitalWrite(FPinINA, LOW);
  pinMode(FPinINB, OUTPUT);
  digitalWrite(FPinINB, LOW);
#if defined(PROCESSOR_ESP32)
  ledcSetup(FPwmChannel, FPwmFrequency, FPwmResolution);
  ledcAttachPin(FPinPWM, FPwmChannel);
  ledcWrite(FPwmChannel, PWM_ZERO);
#else // all other uCs
  pinMode(FPinPWM, PWM);
  digitalWrite(FPinPWM, LOW);
#endif  
  //
  SetState(smdIdle);
  return true;
}

Boolean CMotorVNH2SP30::Close()
{
  pinMode(FPinINA, INPUT_PULLDOWN);
  pinMode(FPinINB, INPUT_PULLDOWN);
  pinMode(FPinPWM, INPUT_PULLDOWN);
  //
  SetState(smdIdle);
  return true;
}
//
//------------------------------------------------------------
//
UInt16 CMotorVNH2SP30::GetPwmLow(void)
{
  return FPwmLow;
}
void CMotorVNH2SP30::SetPwmLow(UInt16 pwm)
{
  FPwmLow = pwm;  
}
//
UInt16 CMotorVNH2SP30::GetPwmHigh(void)
{
  return FPwmHigh;
}
void CMotorVNH2SP30::SetPwmHigh(UInt16 pwm)
{
  FPwmHigh = pwm;
}
//
//------------------------------------------------------------
//  PWM -> VA:  [definition: VLOW == 0%, VHIGH == 100%]
//
//    VA - VLOW      PWM - PWMLOW
//  ------------ = ---------------- && PWM = minmax(PWMLOW, PWM, PWMHIGH)
//  VHIGH - VLOW   PWMHIGH - PWMLOW      // here VHIGH !!!
//
//  VA = VLOW + (VHIGH - VLOW) * (PWM - PWMLOW) / (PWMHIGH - PWMLOW)
//
void CMotorVNH2SP30::SetPwm(UInt16 pwm) // [1]
{
  UInt16 P = pwm;
  P = max(FPwmLow, P);
  P = min(FPwmHigh, P);
  //
#if defined(PROCESSOR_ESP32)
    ledcWrite(FPwmChannel, P); 
#else // all other uCs    
    analogWrite(FPinPWM, P);
#endif      
}
//
void CMotorVNH2SP30::SetVelocity(Float32 velocity) // [%]
{
  Float32 V = min(VELOCITY_HIGH, velocity);
  V = max(VELOCITY_LOW, V);
  UInt16 P = (UInt16)(0.5f + (Float32)FPwmLow + (Float32)(FPwmHigh - FPwmLow) * (velocity - VELOCITY_LOW) / (VELOCITY_HIGH - VELOCITY_LOW));
  SetPwm(P);
}
//
//------------------------------------------------------------
//
void CMotorVNH2SP30::AbortMotion()
{
  SetVelocity(0.0f);
  digitalWrite(FPinINA, LOW);
  digitalWrite(FPinINB, LOW);
  SetState(smdIdle);
}
//
//------------------------------------------------------------
//
void CMotorVNH2SP30::MoveVelocityPositive(float velocity)
{
  SetVelocity(velocity);
  digitalWrite(FPinINA, HIGH);
  digitalWrite(FPinINB, LOW);
  SetState(smdMovePositive);
}

void CMotorVNH2SP30::MoveVelocityNegative(float velocity)
{
  SetVelocity(velocity);
  digitalWrite(FPinINA, LOW);
  digitalWrite(FPinINB, HIGH); 
  SetState(smdMoveNegative);
}
//
#endif // MOTORVNH2SP30_ISPLUGGED
