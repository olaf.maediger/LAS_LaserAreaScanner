//
#include "Defines.h"
//
#if defined(NTPCLIENT_ISPLUGGED)
//
#include <Arduino.h>
#include <Udp.h>
#include "NTPClient.h"
#include "Serial.h"
//
extern CSerial SerialCommand;
//
CNTPClient::CNTPClient(UDP& udp) 
{
  this->FUdp            = &udp;
}

CNTPClient::CNTPClient(UDP& udp, int timeOffset) 
{
  this->FUdp            = &udp;
  this->FTimeOffset     = timeOffset;
}

CNTPClient::CNTPClient(UDP& udp, const char* poolServerName) 
{
  this->FUdp            = &udp;
  this->FPoolServerName = poolServerName;
}

CNTPClient::CNTPClient(UDP& udp, const char* poolServerName, int timeOffset) 
{
  this->FUdp            = &udp;
  this->FTimeOffset     = timeOffset;
  this->FPoolServerName = poolServerName;
}

CNTPClient::CNTPClient(UDP& udp, const char* poolServerName, 
                       int timeOffset, unsigned long updateInterval) 
{
  this->FUdp            = &udp;
  this->FTimeOffset     = timeOffset;
  this->FPoolServerName = poolServerName;
  this->FUpdateInterval = updateInterval;
}
//
//-----------------------------------------------------------
//
bool CNTPClient::OpenWifi(void)
{
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  // Serial - Serial1 
  SerialCommand.Write(TERMINAL_COMMENT);
  SerialCommand.Write(" Connecting to: ");
  SerialCommand.Write(WIFI_SSID);  
  SerialCommand.Write(" : "); 
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WL_CONNECTED != WiFi.status()) 
  {
    delay(200);
    SerialCommand.Write(".");
  }  
  SerialCommand.WriteLine("");
  SerialCommand.Write(TERMINAL_COMMENT);
  SerialCommand.Write(" WiFi connected to IP address: ");
  IPAddress IPA = WiFi.localIP();
  char Buffer[32];
  sprintf(Buffer, "%3.3d.%3.3d.%3.3d.%3.3d", IPA[0], IPA[1], IPA[2], IPA[3]);
  SerialCommand.WriteLine(Buffer);
  SerialCommand.Write(TERMINAL_COMMENT);
  SerialCommand.WriteLine(" Setup done - starting DateTime update...");
  Update();
  return true;
}

bool CNTPClient::CloseWifi(void)
{
  SerialCommand.Write(TERMINAL_COMMENT);
  SerialCommand.WriteLine(" WiFi connection closed...");
  WiFi.disconnect();
  return true;
}
//
//-----------------------------------------------------------
//
void CNTPClient::Open(void)
{
  SetTimeOffset(NTPCLIENT_TIMEOFFSET);
  this->Open(NTP_PORT);
}

void CNTPClient::Open(int port)
{
  this->FPort = port;
  this->FUdp->begin(this->FPort);
  this->FUdpSetup = true;
  GetFormattedDateTime();
}

void CNTPClient::Close() 
{
  this->FUdp->stop();

  this->FUdpSetup = false;
}

bool CNTPClient::IsValid(byte * ntpPacket)
{
	//Perform a few validity checks on the packet
	if((ntpPacket[0] & 0b11000000) == 0b11000000)		//Check for LI=UNSYNC
		return false;		
	if((ntpPacket[0] & 0b00111000) >> 3 < 0b100)		//Check for Version >= 4
		return false;		
	if((ntpPacket[0] & 0b00000111) != 0b100)			//Check for Mode == Server
		return false;		
	if((ntpPacket[1] < 1) || (ntpPacket[1] > 15))		//Check for valid Stratum
		return false;
	if(	ntpPacket[16] == 0 && ntpPacket[17] == 0 && 
		ntpPacket[18] == 0 && ntpPacket[19] == 0 &&
		ntpPacket[20] == 0 && ntpPacket[21] == 0 &&
		ntpPacket[22] == 0 && ntpPacket[22] == 0)		//Check for ReferenceTimestamp != 0
		return false;
	return true;
}

bool CNTPClient::ForceUpdate() 
{
  #ifdef DEBUG_CNTPClient
    Serial.println("Update from NTP Server");
  #endif
  this->SendNTPPacket();
  // Wait till data is there or timeout...
  byte timeout = 0;
  int cb = 0;
  do 
  {
    delay ( 10 );
    cb = this->FUdp->parsePacket();    
    if(cb > 0)
    {
      this->FUdp->read(this->FPacketBuffer, NTP_PACKET_SIZE);
      if(!this->IsValid(this->FPacketBuffer))
        cb = 0;
    }    
    if (timeout > 100) return false; // timeout after 1000 ms
    timeout++;
  } 
  while (cb == 0);
  this->FLastUpdate = millis() - (10 * (timeout + 1)); // Account for delay in reading the time
  unsigned long highWord = word(this->FPacketBuffer[40], this->FPacketBuffer[41]);
  unsigned long lowWord = word(this->FPacketBuffer[42], this->FPacketBuffer[43]);
  // combine the four bytes (two words) into a long integer
  // this is NTP time (seconds since Jan 1 1900):
  unsigned long secsSince1900 = highWord << 16 | lowWord;
  this->FCurrentEpoc = secsSince1900 - SEVENZYYEARS;
  return true;
}

bool CNTPClient::Update() 
{
  if ((millis() - this->FLastUpdate >= this->FUpdateInterval)     // Update after FUpdateInterval
    || this->FLastUpdate == 0) 
  {                                // Update if there was no update yet.
    if (!this->FUdpSetup) this->Open();                         // setup the UDP client if needed
    return this->ForceUpdate();
  }
  return true;
}

void CNTPClient::SetTimeOffset(int timeOffset) 
{
  this->FTimeOffset = timeOffset;
}

void CNTPClient::SetUpdateInterval(unsigned long updateInterval) 
{
  this->FUpdateInterval = updateInterval;
}

void CNTPClient::SendNTPPacket() 
{ // set all bytes in the buffer to 0
  memset(this->FPacketBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  this->FPacketBuffer[0] = 0b11100011;   // LI, Version, Mode
  this->FPacketBuffer[1] = 0;     // Stratum, or type of clock
  this->FPacketBuffer[2] = 6;     // Polling Interval
  this->FPacketBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  this->FPacketBuffer[12] = 0x49;
  this->FPacketBuffer[13] = 0x4E;
  this->FPacketBuffer[14] = 0x49;
  this->FPacketBuffer[15] = 0x52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  this->FUdp->beginPacket(this->FPoolServerName, 123); //NTP requests are to port 123
  this->FUdp->write(this->FPacketBuffer, NTP_PACKET_SIZE);
  this->FUdp->endPacket();
}

unsigned long CNTPClient::GetEpochTime() 
{
  return this->FTimeOffset + // User offset
         this->FCurrentEpoc + // Epoc returned by the NTP server
         ((millis() - this->FLastUpdate) / 1000); // Time since last update
}
void CNTPClient::SetEpochTime(unsigned long secs) 
{
  this->FCurrentEpoc = secs;
}

int CNTPClient::GetWeekDay(void) 
{
  return (((this->GetEpochTime()  / 86400L) + 4 ) % 7); //0 is Sunday
}
int CNTPClient::GetHours(void)
{
  return ((this->GetEpochTime()  % 86400L) / 3600);
}
int CNTPClient::GetMinutes(void)
{
  return ((this->GetEpochTime() % 3600) / 60);
}
int CNTPClient::GetSeconds(void)
{
  return (this->GetEpochTime() % 60);
}

String CNTPClient::GetFormattedTime(unsigned long secs) 
{
  unsigned long rawTime = secs ? secs : this->GetEpochTime();
  unsigned long hours = (rawTime % 86400L) / 3600;
  String hoursStr = hours < 10 ? "0" + String(hours) : String(hours);
  unsigned long minutes = (rawTime % 3600) / 60;
  String minuteStr = minutes < 10 ? "0" + String(minutes) : String(minutes);
  unsigned long seconds = rawTime % 60;
  String secondStr = seconds < 10 ? "0" + String(seconds) : String(seconds);
  return hoursStr + ":" + minuteStr + ":" + secondStr;
}

// Based on https://github.com/PaulStoffregen/Time/blob/master/Time.cpp
// currently assumes UTC timezone, instead of using this->FTimeOffset
String CNTPClient::GetFormattedDateTime(unsigned long secs) 
{
  unsigned long rawTime = (secs ? secs : this->GetEpochTime()) / 86400L;  // in days
  unsigned long days = 0, year = 1970;
  uint8_t month;
  static const uint8_t monthDays[]={31,28,31,30,31,30,31,31,30,31,30,31};
  while((days += (LEAP_YEAR(year) ? 366 : 365)) <= rawTime)
    year++;
  rawTime -= days - (LEAP_YEAR(year) ? 366 : 365); // now it is days in this year, starting at 0
  days=0;
  for (month=0; month<12; month++) {
    uint8_t monthLength;
    if (month==1) { // february
      monthLength = LEAP_YEAR(year) ? 29 : 28;
    } else 
    {
      monthLength = monthDays[month];
    }
    if (rawTime < monthLength) break;
    rawTime -= monthLength;
  }
  String monthStr = ++month < 10 ? "0" + String(month) : String(month); // jan is month 1  
  String dayStr = ++rawTime < 10 ? "0" + String(rawTime) : String(rawTime); // day of month  
  return String(year) + "." + monthStr + "." + dayStr + "-" + this->GetFormattedTime(secs ? secs : 0);
}

bool CNTPClient::GetFormattedDateTime(String &year, String &month, String &day, 
                                      String &hour, String &minute, String &second)
{
  String DateTime = this->GetFormattedDateTime();
  // 01234567890123456789
  // 2018.12.09-11:08:53
  year = DateTime.substring(2, 4);
  month = DateTime.substring(5, 7);
  day = DateTime.substring(8, 10);
  hour = DateTime.substring(11, 13);
  minute = DateTime.substring(14, 16);
  second = DateTime.substring(17, 19);
  return true;
}

bool CNTPClient::GetDate(String &yy, String &mm, String &dd)
{
  String DateTime = this->GetFormattedDateTime();
  // 01234567890123456789
  // 2018.12.09-11:08:53
  yy = DateTime.substring(2, 4);
  mm = DateTime.substring(5, 7);
  dd = DateTime.substring(8, 10);
  return true;
}

bool CNTPClient::GetTime(String &hh, String &mm, String &ss)
{
  String DateTime = this->GetFormattedDateTime();
  // 01234567890123456789
  // 2018.12.09-11:08:53
  hh = DateTime.substring(11, 13);
  mm = DateTime.substring(14, 16);
  ss = DateTime.substring(17, 19);
  return true;
}

String CNTPClient::GetDateTimeOrdered()
{
  String DY = "";
  String DM = "";
  String DD = "";
  String TH = "";
  String TM = "";
  String TS = "";
  this->GetFormattedDateTime(DY, DM, DD, TH, TM, TS);
  return DY + "." + DM + "." + DD + "|" + TH + ":" + TM + ":" + TS;
}

bool CNTPClient::GetDateTimeSeparated(String &dtdate, String &dttime)
{
  String DY = "";
  String DM = "";
  String DD = "";
  String TH = "";
  String TM = "";
  String TS = "";
  this->GetFormattedDateTime(DY, DM, DD, TH, TM, TS);
  dtdate = DY + "." + DM + "." + DD;
  dttime = TH + ":" + TM + ":" + TS;
  return true;
}

bool CNTPClient::GetDateTimeCompressed(String &dtdate, String &dttime)
{
  String DY = "";
  String DM = "";
  String DD = "";
  String TH = "";
  String TM = "";
  String TS = "";
  this->GetFormattedDateTime(DY, DM, DD, TH, TM, TS);
  dtdate = DY + DM + DD;
  dttime = TH + TM + TS;
  return true;  
}

bool CNTPClient::GetDateTimeMillisCompressed(String &dtdate, String &dttime, String &dtmillis)
{
  String DY = "";
  String DM = "";
  String DD = "";
  String TH = "";
  String TM = "";
  String TS = "";
  char Buffer[8];
  sprintf(Buffer, "%3.3i", millis() % 1000);
  this->GetFormattedDateTime(DY, DM, DD, TH, TM, TS);
  dtdate = DY + DM + DD;
  dttime = TH + TM + TS;
  dtmillis = Buffer;
  return true;  
}
//
#endif // NTPCLIENT_ISPLUGGED
//
