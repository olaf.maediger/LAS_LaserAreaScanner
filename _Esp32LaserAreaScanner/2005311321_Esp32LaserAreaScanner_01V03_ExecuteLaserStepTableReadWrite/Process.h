//
#include "Defines.h"
//
#ifndef Process_h
#define Process_h
//
#include "Serial.h"
//
class CProcess
{ //
  private:
  // Field
  //
  public:
  // Constructor
  CProcess();
  // 
  public:
  // Management
  Boolean Open();
  Boolean Close(); 
  // Collector
  void Handle(CSerial &serial);
};
//
#endif // Process_h
//
