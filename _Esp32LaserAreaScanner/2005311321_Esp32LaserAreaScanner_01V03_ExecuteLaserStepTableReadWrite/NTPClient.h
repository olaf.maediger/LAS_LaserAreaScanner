//
#ifndef NTPClient_h
#define NTPClient_h
//
#include <Arduino.h>
#include <Udp.h>
#include <WiFiUdp.h>
#include <WiFi.h> // ESP32
//
#define IPADDRESS_NTPSERVER "de.pool.ntp.org"
//
#define SEVENZYYEARS        2208988800UL
#define NTP_PACKET_SIZE     48
#define NTP_PORT            1337
#define LEAP_YEAR(Y)        ((Y>0) && !(Y%4) && ((Y%100) || !(Y%400)))
//
class CNTPClient 
{
  private:
  UDP*          FUdp;
  bool          FUdpSetup       = false;
  const char*   FPoolServerName = "pool.ntp.org"; // Default time server
  int           FPort           = NTP_PORT;
  int           FTimeOffset     = 0;
  unsigned long FUpdateInterval = 60000;  // In ms
  unsigned long FCurrentEpoc    = 0;      // In s
  unsigned long FLastUpdate     = 0;      // In ms
  byte          FPacketBuffer[NTP_PACKET_SIZE];
  //
  bool          IsValid(byte * ntpPacket);
  void          SendNTPPacket();
  //
  public:
  //
  // Constructor
  CNTPClient(UDP& udp);
  CNTPClient(UDP& udp, const char* poolServerName);
  CNTPClient(UDP& udp, int timeOffset);
  CNTPClient(UDP& udp, const char* poolServerName, int timeOffset);
  CNTPClient(UDP& udp, const char* poolServerName, int timeOffset, unsigned long updateInterval);
  //
  // Helper Wifi
  bool OpenWifi(void);
  bool CloseWifi(void);  
  //
  // Management
  void Open(void);
  void Open(int port);
  void Close();  
  bool Update();
  bool ForceUpdate();
  int GetWeekDay();
  int GetHours();
  int GetMinutes();
  int GetSeconds();
  // Changes the time offset. Useful for changing timezones dynamically
  void SetTimeOffset(int timeOffset);
  // Set the update interval to another frequency
  void SetUpdateInterval(unsigned long updateInterval);
  // return time in seconds since Jan. 1, 1970
  unsigned long GetEpochTime();
  // Replace the NTP-fetched time with seconds since Jan. 1, 1970
  void SetEpochTime(unsigned long secs);
  // return secs argument (or 0 for current time) formatted like `hh:mm:ss`
  String GetFormattedTime(unsigned long secs = 0);
  // return secs argument (or 0 for current date) formatted to ISO 8601 like `2004-02-12T15:19:21+00:00`
  String GetFormattedDateTime(unsigned long secs = 0);
  bool GetFormattedDateTime(String &year, String &month, String &day, 
                            String &hour, String &minute, String &second);
  bool GetTime(String &hh, String &mm, String &ss);
  bool GetDate(String &yy, String &mm, String &dd);
  //                            
  String GetDateTimeOrdered();
  bool GetDateTimeSeparated(String &dtdate, String &dttime);
  bool GetDateTimeCompressed(String &dtdate, String &dttime);
  bool GetDateTimeMillisCompressed(String &dtdate, String &dttime, String &dtmillis);
};
//
#endif // NTPClient_h
//
