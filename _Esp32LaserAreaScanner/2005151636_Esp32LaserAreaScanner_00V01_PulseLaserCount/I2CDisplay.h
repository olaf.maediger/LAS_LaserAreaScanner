//
//--------------------------------
//  Library I2CDisplay
//--------------------------------
//
#include "Defines.h"
//
#ifndef I2CDisplay_h
#define I2CDisplay_h
//
#include "Pinout.h"
//
#include <Wire.h>
#include "I2CDisplayBase.h"
//
// const Byte I2CLCD_I2CADDRESS  = 0x27; // PCF8574
const Byte I2CLCD_I2CADDRESS  = 0x22;
const Byte I2CLCD_COLCOUNT    = 20; 
const Byte I2CLCD_ROWCOUNT    = 4;
//
//--------------------------------
//  Section - CI2CDisplay
//--------------------------------
//
class CI2CDisplay
{
  private:
  CI2CDisplayBase* FPI2CDisplayBase; 
  UInt8 FI2CAddress;
  UInt8 FColCount, FRowCount;
  
  public:
  CI2CDisplay(UInt8 i2caddress = I2CLCD_I2CADDRESS, 
              UInt8 colcount = I2CLCD_COLCOUNT, 
              UInt8 rowcount = I2CLCD_ROWCOUNT);
  //
  Boolean Open();
  Boolean Close();
  // LowLevel
  Byte GetI2CAddress();
  // HighLevel
  void SetBacklightOn();
  void SetBacklightOff();
  void SetHomePosition();
  void ClearDisplay();
  void SetCursorPosition(Byte row, Byte col);
  void WriteText(String text);
  void WriteByte(Byte value);
  void WriteUInt8(UInt8 value);
  void WriteInt16(Int16 value);
};
//
#endif // I2CDisplay_h
