//
#include "Defines.h"
//
#if !defined(WifiClient_h)
#define WifiClient_h
//
#include <WiFi.h>
//
class CWifiClient
{
  private:
  WiFiClient* FPWiFiClient;
  //
  public:
  CWifiClient(void);
  //
  inline WiFiClient* GetWiFiClient()
  {
    return FPWiFiClient;
  }
  //
  void Open(const char* wifissid, const char* wifipassword);
};


#endif // WifiClient_h
