//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "Defines.h"
//
#if defined(LASERSCANNER_JI)
//
#if defined(PINOUTPUT_ISPLUGGED)
#include "PinOutput.h"
#endif
#if defined(DACINTERNAL_ISPLUGGED)
#include "DacInternal.h"
#endif
#include "LaserScannerJI.h"
#include "Command.h"
//
#if defined(PINOUTPUT_ISPLUGGED)
extern CPinOutput        LaserTrigger;
#endif
#if defined(DACINTERNAL_ISPLUGGED)
extern CDacInternal      PositionX;
extern CDacInternal      PositionY;
#endif
//
extern CCommand          Command;
//
//-------------------------------------------------------------------------------
//  LaserScanner - Init
//-------------------------------------------------------------------------------
CLaserScanner::CLaserScanner(String id, Boolean messageon)
{
  FID = id;
  FState = slsZero;
  FMessageOn = messageon;
  FPulsePeriodms = INIT_PULSEPERIOD_MS;
  FPTimeRelative = new CTimeRelative("TRLS", messageon);
}

Boolean CLaserScanner::Open()
{
  FState = slsZero;
  return true;
}

Boolean CLaserScanner::Close()
{
  FState = slsZero;
  return true;
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Property
//-------------------------------------------------------------------------------
EStateLaserScanner CLaserScanner::GetState(void)
{
  return FState;
}  

PCharacter CLaserScanner::StateText(EStateLaserScanner state)
{
  switch (state)
  {
    case slsZero:
      return "Z";
    // FSM PulseLaser
    case slsInitPulseLaser:
      return "IPL";
    case slsBusyPulseLaserHigh:
      return "BPLH";
    case slsBusyPulseLaserLow:
      return "BPLL";
    // FSM MovePulse
    case slsInitMovePulse:
      return "IMP";
    case slsBusyMovePulseTarget:
      return "BMPT";
    case slsBusyMovePulseHigh:
      return "BMPH";
    case slsBusyMovePulseLow:
      return "BMPL";
  }
  return "???";
}

void CLaserScanner::SetState(EStateLaserScanner state)
{
  if (state != FState)
  {
    if (FMessageOn)
    {
      char Buffer[32];    
      sprintf(Buffer, "SLS %s -> %s", StateText(FState), StateText(state));
      Command.WriteEvent(Buffer);
    }
    FState = state;
  }
}  

void CLaserScanner::SetPulsePeriodms(Float32 periodms)
{
  FPulsePeriodms = periodms;
}
UInt32 CLaserScanner::GetPulsePeriodms(void)
{
  return FPulsePeriodms;
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Handler
//-------------------------------------------------------------------------------
Boolean CLaserScanner::PulseLaserCount(Float32 periodms, UInt32 count)
{
  if (slsZero == FState)
  {
    FPulsePeriodms = periodms;
    FPulseCountActual = 0;
    FPulseCountPreset = count;
    FState = slsInitPulseLaser;
    return true;
  }
  return false;
}

Boolean CLaserScanner::MovePositionX(UInt16 px)
{
  return false;  
}

Boolean CLaserScanner::MovePositionY(UInt16 py)
{
  return false;  
}

Boolean CLaserScanner::MovePositionPulse(UInt16 px, UInt16 py, Float32 ppms, UInt32 pc, UInt32 dm)
{
  return false;  
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Wait
//-------------------------------------------------------------------------------
//
void CLaserScanner::Abort(void)
{
  // stop all Activities !!! ...
  // FPinPulse = LOW;
  FState = slsZero;
}

Boolean CLaserScanner::IsBusy(void)
{
  return (slsZero != FState);
}

// PLC 1000 3
// PLC 100 3
// PLC 10 3
// PLC 1 3
// PLC 0.5 3
// PLC 1 1000000
// PLC 0.5 1000000
void CLaserScanner::Execute(void)
{ 
  switch (FState)
  {
    case slsZero:
      return;  
  //------------------------------------
  // FSM PulseLaser
  //------------------------------------
    case slsInitPulseLaser:
      FPulseCountActual = 0;
      FPulseCountActual++;
      LaserTrigger.WriteLevel(polHigh);
      FPTimeRelative->Wait_Start((Float32)(FPulsePeriodms / 2.0f));
      SetState(slsBusyPulseLaserHigh);
      break;
    case slsBusyPulseLaserHigh:
      if (!FPTimeRelative->Wait_Execute())
      { // Time High reached
        LaserTrigger.WriteLevel(polLow);
        FPTimeRelative->Wait_Start((Float32)(FPulsePeriodms / 2.0f));
        SetState(slsBusyPulseLaserLow);
      }
      break;
    case slsBusyPulseLaserLow:      
      if (!FPTimeRelative->Wait_Execute())
      { // Time Low reached
        if (FPulseCountActual < FPulseCountPreset)
        { 
          FPulseCountActual++;
          LaserTrigger.WriteLevel(polHigh);
          FPTimeRelative->Wait_Start((Float32)(FPulsePeriodms / 2.0f));
          SetState(slsBusyPulseLaserHigh);
        }
        else
        {
          LaserTrigger.WriteLevel(polLow);
          SetState(slsZero);
        }
      }
      break;
  //------------------------------------
  // FSM MovePulse      
  //------------------------------------
    case slsInitMovePulse:
      SetState(slsBusyMovePulseTarget);
      break;
    case slsBusyMovePulseTarget:
      SetState(slsBusyMovePulseHigh);
      break;
    case slsBusyMovePulseHigh:
      SetState(slsBusyMovePulseLow);
      break;
    case slsBusyMovePulseLow:
      SetState(slsZero);
      break;
  }  
}
//
#endif // LASERSCANNER_JI
//
