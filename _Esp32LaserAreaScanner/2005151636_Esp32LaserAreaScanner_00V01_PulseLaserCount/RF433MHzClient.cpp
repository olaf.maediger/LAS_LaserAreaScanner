//
#include "Defines.h"
//
#if defined(RF433MHZCLIENT_ISPLUGGED)
//
#include "RF433MHzClient.h"
#include "Command.h"
//
extern CCommand Command;
//
CRF433MHzClient::CRF433MHzClient(String id, int pintxd)
{
  FID = id;
  FPinTXD = pintxd;
  FPRCSwitch  = new RCSwitch();
  SetProtocol(INIT_RF433MHZ_PROTOCOL);
  SetPulseLength(INIT_RF433MHZ_PULSELENGTH_US);
}
//
void CRF433MHzClient::Open(void)
{
  FPRCSwitch->enableTransmit(FPinTXD);
  FPRCSwitch->setProtocol(INIT_RF433MHZ_PROTOCOL);
  FPRCSwitch->setPulseLength(INIT_RF433MHZ_PULSELENGTH_US);
}
//
void CRF433MHzClient::Close(void)
{
  FPRCSwitch->disableTransmit();
}
//
void CRF433MHzClient::SetProtocol(Byte protocol)
{
  FPRCSwitch->setProtocol(protocol);
}
//
void CRF433MHzClient::SetPulseLength(UInt16 pulselength)
{
  FPRCSwitch->setPulseLength(pulselength);
}
//
void CRF433MHzClient::TransmitCode(UInt32 code)
{
  FPRCSwitch->send(code, 24);
}

void CRF433MHzClient::SwitchOn()
{
//  char Buffer[16];
//  sprintf(Buffer, "SWO %s", FID);
//  Command.WriteEvent(Buffer);
  FPRCSwitch->send(4480176, 24);
}
 
void CRF433MHzClient::SwitchOff()
{
//  char Buffer[16];
//  sprintf(Buffer, "SWF %s", FID);
//  Command.WriteEvent(Buffer);
  FPRCSwitch->send(4631328, 24);
}
//
#endif // RF433MHZCLIENT_ISPLUGGED
//
  
