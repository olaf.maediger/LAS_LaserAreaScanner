//
//--------------------------------
//  Library PositionReached
//--------------------------------
//
#include "Defines.h"
//
#if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
//
#include "PositionReached.h"
#include "RRCCommand.h"
//
extern CCommand RRCCommand;
//
CPositionReached::CPositionReached(char id)
{
  FID = id;
  FState = sprZero;
  FPositionActualLeft = INIT_POSITION_STP;
  FPositionActualRight = INIT_POSITION_STP;
  FPositionTargetLeft = INIT_POSITION_STP;
  FPositionTargetRight = INIT_POSITION_STP;
  FVelocityTarget = INIT_VELOCITY_PRC;
}

EStatePositionReached CPositionReached::GetState(void)
{
  return FState;
}  

Boolean CPositionReached::Open()
{
  FState = sprZero;
  return true;
}

Boolean CPositionReached::Close()
{
  FState = sprZero;
  return true;
}

Int32 CPositionReached::GetPositionActualLeftstp(void)
{
  return FPositionActualLeft;
}

Int32 CPositionReached::GetPositionActualRightstp(void)
{
  return FPositionActualRight;
}
//
//-------------------------------------------------------------------------------
//  PositionReached - Wait
//-------------------------------------------------------------------------------
//
void CPositionReached::Wait_Start(Int32 positiontargetleft, 
                                  Int32 positiontargetright,
                                  Float32 velocitytarget)
{
//  FTimePeriodms = timeperiodms;
//  FTimeStartms = millis();
  FState = sprInit;
}

void CPositionReached::Wait_Abort(void)
{
  FState = sprZero;
}

Boolean CPositionReached::Wait_Execute(void)
{
  switch (FState)
  {
    case sprZero:
      return false;
    case sprInit:
//      FTimeStartms = millis();
//      char Buffer[16];
//      sprintf(Buffer, "WTR %c 1", FID);
//      RRCCommand.WriteEvent(Buffer);
      FState = sprBusy;
      return true;
    case sprBusy:
//      if (FTimePeriodms <= (millis() - FTimeStartms))
//      {
//        char Buffer[16];
//        sprintf(Buffer, "WTR %c 0", FID);
//        RRCCommand.WriteEvent(Buffer);
//        FState = sprZero;
//        return false;
//      }
      return true;
  }
  return false;
}
//
#endif // MOTORVNH2SP30_ISPLUGGED && MOTORENCODERLM393_ISPLUGGED
//
