//
//--------------------------------
//  Library TimeAbsolute
//--------------------------------
//
#include "Defines.h"
//
#if defined(NTPCLIENT_ISPLUGGED)
//
#ifndef TimeAbsolute_h
#define TimeAbsolute_h
//
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const Byte INIT_TIMEABSOLUTE_HH = 00; //[hh]
const Byte INIT_TIMEABSOLUTE_MM = 00; //[hh]
const Byte INIT_TIMEABSOLUTE_SS = 10; //[hh]
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateTimeAbsolute
{
  staZero = 0,
  staInit = 1,
  staBusy = 2
};
//
class CTimeAbsolute
{
  private:
  String FID;
  EStateTimeAbsolute FState;
  Boolean FMessageOn;
  Byte FTimeStampHH, FTimeStampMM, FTimeStampSS;
  Byte FTimeStartHH, FTimeStartMM, FTimeStartSS;
  Byte FTimeActualHH, FTimeActualMM, FTimeActualSS;
  //
  public:
  CTimeAbsolute(String id, Boolean messageon);
  //
  EStateTimeAbsolute GetState(void);
  //
  Boolean Open();
  Boolean Close();
  // 
  Byte GetTimeStampHH(void);
  Byte GetTimeStampMM(void);
  Byte GetTimeStampSS(void);
  Byte GetTimeStartHH(void);
  Byte GetTimeStartMM(void);
  Byte GetTimeStartSS(void);
  Byte GetTimeActualHH(void);
  Byte GetTimeActualMM(void);
  Byte GetTimeActualSS(void);
  //
  void Wait_Start(Byte timestamphh, Byte timestampmm, Byte timestampss);
  void Wait_Abort(void);
  Boolean Wait_Execute(void);
};
//
#endif // TimeAbsolute_h
//

#endif // NTPCLIENT_ISPLUGGED
//
