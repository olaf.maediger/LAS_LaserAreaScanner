//
#include "DacMCP4725.h"
//
CDacMCP4725::CDacMCP4725(String id, UInt8 i2caddress, UInt16 initlevel)
{
  FID = id;
  FI2CAddress = i2caddress;
  FLevel = initlevel;
}

void CDacMCP4725::Open(void)
{  
  Wire.begin();
  WriteLevel(FLevel);
}
 
void CDacMCP4725::Close(void)
{  
  WriteLevel(0);
  //Wire.end();
}
 
void CDacMCP4725::WriteLevel(UInt16 level)
{
  FLevel = level;
  Wire.beginTransmission(FI2CAddress);
  Wire.write(MCP4726_CMD_WRITEDAC);
  Wire.write(FLevel / 16);                   
  Wire.write((FLevel % 16) << 4);            
  Wire.endTransmission();
}
