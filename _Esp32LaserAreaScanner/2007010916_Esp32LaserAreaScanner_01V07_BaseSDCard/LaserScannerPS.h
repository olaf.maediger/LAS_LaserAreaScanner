//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "Defines.h"
//
#if defined(LASERSCANNER_PS)
//
#ifndef LaserScanner_h
#define LaserScanner_h
//
#include "Arduino.h"
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateLaserScanner
{
  srsZero = 0,
  srsInit = 1,
  srsBusy = 2
};
//
enum EStateForceSwitch
{
  sfsPassive  = 0,
  sfsForceOn  = 1,
  sfsForceOff = 2
};
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const EStateForceSwitch INIT_STATEFORCESWITCH = sfsPassive;
const Boolean INIT_SWITCHON = true;
//
//
//--------------------------------
//  Section - CLaserScanner
//--------------------------------
//
class CLaserScanner
{
  private:
  String FID;
  EStateLaserScanner FStateLaserScanner;
  EStateForceSwitch FStateForceSwitch;
  Boolean FSwitchOn;
  //
  public:
  CLaserScanner(String id);
  //
  void SetPulsePeriod(UInt32 period);
  UInt32 GetPulsePeriod(void);
  //
  void PulseLaserAbort(void);
  void PulseLaserCount(UInt32 period, UInt32 count);

  void MovePositionX(UInt16 px);
  void MovePositionY(UInt16 py);
  void MovePositionPulse(UInt16 px, UInt16 PY, UInt32 PP, UInt32 PC, UInt32 DM);

  
  EStateLaserScanner GetStateLaserScanner(void);
  EStateForceSwitch GetStateForceSwitch(void);
  Boolean GetSwitchOn(void);
  //
  Boolean Open();
  Boolean Close();
  //
  void ForceOn(UInt32 ontime); 
  void ForceOff(UInt32 offtime); 
  //
  void Execute(void);
};
//
#endif // LaserScanner_h
//
#endif // LASERSCANNER_PS
//
