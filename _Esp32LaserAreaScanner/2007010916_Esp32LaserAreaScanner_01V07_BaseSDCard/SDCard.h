//
#include "Defines.h"
//
#ifdef SDCARD_ISPLUGGED
//
#ifndef SDCard_h
#define SDCard_h
//
#include <SPI.h>
#include "FS.h"
#include "SD.h"
//
class CSDCard
{
  private:
  fs::FS* FPSD;
  Int16 FPinCS;
  File FFile;
  String FLine;
  //
  Boolean IsCharacterOfHex(Character value);
  //
  UInt32 AsciiHexUnsigned(String text);
  //
  Boolean IsCharacter(Character value);
  Boolean IsCharacterOfUnsigned(Character value);
  Boolean IsCharacterOfInt(Character value);
  Boolean IsCharacterOfFloat(Character value);
  Boolean IsCharacterOfDouble(Character value);
  //
  UInt32 AsciiUnsigned(String text);
  Int32 AsciiInt(String text);
  Float32 AsciiFloat(String text);
  Double64 AsciiDouble(String text);
  //  
  public:
  CSDCard(Int16 pincs);
  //
  Int16 Open(fs::FS* psd);
  Boolean Close(void);
  // 
  // Write
  Boolean OpenWrite(String filename); // possible Create
  Boolean CloseWrite(void);
  Boolean IsOpenForWriting(void);
  // unformatted
  Boolean Write(Byte value);
  Boolean Write(Character value);
  Boolean Write(UInt16 value);
  Boolean Write(Int16 value);
  Boolean Write(UInt32 value);
  Boolean Write(Int32 value);
  Boolean Write(Float32 value);
  Boolean Write(Double64 value);
  Boolean Write(PCharacter value);
  Boolean Write(String value);
  // formatted
  Boolean Write(String format, Byte value);
  Boolean Write(String format, Character value);
  Boolean Write(String format, UInt16 value);
  Boolean Write(String format, Int16 value);
  Boolean Write(String format, UInt32 value);
  Boolean Write(String format, Int32 value);
  Boolean Write(String format, Float32 value);
  Boolean Write(String format, Double64 value);
  Boolean Write(String format, PCharacter value);
  Boolean Write(String format, String value);
  // with Header 0x
  Boolean WriteHex(Byte value);
  Boolean WriteHex(UInt16 value);
  Boolean WriteHex(UInt32 value);
  //
  // Read
  Boolean OpenRead(String filename);
  Boolean CloseRead(void);
  Boolean IsOpenForReading(void);
  Int16 ReadAvailable(void);
  //
  Boolean ReadLine(String &line);
  //
  Boolean Read(Byte &value);
  Boolean Read(Character &value);
  Boolean Read(UInt16 &value);
  Boolean Read(Int16 &value);
  Boolean Read(UInt32 &value);
  Boolean Read(Int32 &value);
  Boolean Read(Float32 &value);
  Boolean Read(Double64 &value);
  Boolean Read(Character terminal, PCharacter value);
  Boolean Read(Character terminal, String &value);
  // with Header 0x
  Boolean ReadHex(Byte &value);
  Boolean ReadHex(UInt16 &value);
  Boolean ReadHex(UInt32 &value);  
};
//
#endif // SDCard_h
//
#endif // SDCARD_ISPLUGGED
//
