//
#include "Defines.h"
//
#if defined(SDCARD_ISPLUGGED)
//
#include "CommandFile.h"
//
// debug #include "Serial.h"
// debug extern CSerial SerialCommand;
//
bool CCommandFile::Open(fs::FS* pfs)
{
  FPFS = pfs;
  FText = "";
  FTextIndex = 0;
}
bool CCommandFile::Close(void)
{
  FText = "";
  FTextIndex = 0;
  FPFS = 0;
}
//
bool CCommandFile::IsExecuting(void)
{
  // debug SerialCommand.WriteLine(FText.length());
  return (1 < FText.length());
}
//
bool CCommandFile::ParseFile(const char* path)
{
  FText = "";
  FTextIndex = 0;
  File file = FPFS->open(path);
  if (!file) return false;  // Serial.println("Failed to open file for reading");
  while (file.available())
  {
    FText += (char)file.read();
  }
  file.close();
  return IsExecuting();
}
//
String CCommandFile::GetCommand(void)
{
  String Line = "";
  while (FTextIndex < FText.length())
  {
    Byte C = (Byte)FText[FTextIndex];
    FTextIndex++;
    switch (C)
    {
      case 0x0D:
        if (0 < Line.length()) return Line;
        break;
      case 0x0A:
        if (0 < Line.length()) return Line;
        break;
      default:
        Line += (char)C;
        break;
    }
  }
  FText = "";
  return FText;
}
//
void CCommandFile::ResetExecution(void)
{
  // debug SerialCommand.WriteLine("RE");
  FTextIndex = 0;
}
//
#endif // SDCARD_ISPLUGGED
//
