//
//--------------------------------
//  Library PinInput
//--------------------------------
//
#include "Defines.h"
//
#include "PinInput.h"
#include "Command.h"
//
//extern CCommand Command;
////
//CPinInput::CPinInput(String id, int pin)
//{
//  FID = id;
//  FPin = pin;
//  FLevel = pilLow;
//  FInverted = LEVEL_NOTINVERTED;
//}
//
//CPinInput::CPinInput(String id, int pin, bool inverted)
//{
//  FID = id;
//  FPin = pin;
//  FLevel = pilLow;
//  FInverted = inverted;
//}
//
//EPinInputLevel CPinInput::GetLevel(void)
//{
//  return FLevel;
//}  
//
//EPinInputLevel CPinInput::ReadLevel(void)
//{
//  if (digitalRead(FPin))
//  { // 3V3 / 5V0
//    if (FInverted)
//    {
//      FLevel = pilLow;
//    }
//    else
//    {
//      FLevel = pilHigh;
//    }
//  }
//  else
//  { // 0V0
//    if (FInverted)
//    {
//      FLevel = pilHigh;
//    }
//    else
//    {
//      FLevel = pilLow;
//    }
//  }
//  return FLevel;
//}  
//
//Boolean CPinInput::Open()
//{
//  pinMode(FPin, INPUT_PULLDOWN);
//  ReadLevel();
//  return true;
//}
//
//Boolean CPinInput::Close()
//{
//  pinMode(FPin, INPUT_PULLDOWN);
//  return true;
//}
////
////-------------------------------------------------------------------------------
////  PinInput - Wait
////-------------------------------------------------------------------------------
////
//EPinInputLevel CPinInput::Execute(void)
//{
//  ReadLevel();
//  return GetLevel();
//}
////
