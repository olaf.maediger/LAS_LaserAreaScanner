//
#include "Defines.h"
//
#if defined(MQTTCLIENT_ISPLUGGED)
//
#include "Serial.h"
#include "MQTTClient.h"
#include "Utilities.h"
#if defined(NTPCLIENT_ISPLUGGED)
#include "NTPClient.h"
#endif
//
extern CMQTTClient MQTTClient;
extern CSerial SerialCommand;
#if defined(NTPCLIENT_ISPLUGGED)
extern CNTPClient NTPClient;
#endif
//
CMQTTClient::CMQTTClient()
{
  FPWifiClient = new CWifiClient();
  WiFiClient* PWiFiClient = FPWifiClient->GetWiFiClient();
  FPPubSubClient = new PubSubClient(*PWiFiClient);  
}

void CMQTTClient::MQTTCallback(char* topic, byte* payload, unsigned int length) 
{
  // debug SerialCommand.Write("# Received Topic[");
  // debug SerialCommand.Write(topic);
  // debug SerialCommand.Write("] Message[");
  char Message[32];
  for (int i = 0; i < length; i++) 
  {
    Message[i] = (char)payload[i];
    Message[1 + i] = (char)0x00;
  }
  // debug SerialCommand.Write(Message);
  // debug SerialCommand.WriteLine("]");
  if (0 == strcmp(TOPIC_COMMAND, topic))
  {
    String SMessage = Message;
    MQTTClient.SetReceivedText(SMessage);
  }
}

void CMQTTClient::Initialise(const char* wifissid, const char* wifipassword)
{
  FPWifiClient->Open(wifissid, wifipassword);
}

void CMQTTClient::Open(const char* brokeripaddress, const int brokerport)
{
  FPPubSubClient->setServer(brokeripaddress, brokerport);
  FPPubSubClient->setCallback(MQTTCallback);
  if (!FPPubSubClient->connected()) 
  {
    Reconnect();
  }  
}

bool CMQTTClient::Reconnect(void)//HardwareSerial* pserial)
{
  while (!FPPubSubClient->connected()) 
  {
    SerialCommand.Write("# Try connection to MQTT-Broker: ");
    // Create a random client ID
    String clientId = "ESP32Client"; 
    clientId += String(random(0xffff), HEX);
    // try to connect to Broker
    if (FPPubSubClient->connect(clientId.c_str())) 
    {
      SerialCommand.WriteLine("OK");
      FPPubSubClient->setCallback(MQTTCallback);
      WriteEvent(EVENT_BROKERCONNECTED);
      FPPubSubClient->subscribe(TOPIC_COMMAND);
      return true;
    } 
    else 
    {
      // NC ! WriteEvent(EVENT_BROKERDISCONNECTED);
      SerialCommand.Write("failed, rc=");
      SerialCommand.Write(FPPubSubClient->state());
      SerialCommand.WriteLine("# Try will follow in 3s ...");
      delay(3000);
    }
  }  
  return false;
}

bool CMQTTClient::RefreshConnection(void)
{ 
  if (!FPPubSubClient->connected()) 
  {
    return Reconnect();
  }
  return false;
}
 
void CMQTTClient::Execute(void)
{
  FPPubSubClient->loop();
}

void CMQTTClient::Publish(const char *topic, const char* message)
{
  FPPubSubClient->publish(topic, message);
}

void CMQTTClient::Subscribe(const char *topic)
{
  FPPubSubClient->subscribe(topic);    
}
//--------------------------------------------------------------
void CMQTTClient::WritePrompt()
{  
#if defined(NTPCLIENT_ISPLUGGED)
  String Date, Time, Millis;
  NTPClient.GetDateTimeMillisCompressed(Date, Time, Millis);
  sprintf(FBuffer, "%s%s.%s.%s%s", TERMINAL_RESPONSE, Date.c_str(), Time.c_str(), Millis.c_str(), TERMINAL_INPUT);
  Publish(TOPIC_RESPONSE, FBuffer);
#else
  sprintf(FBuffer, "%s%s%s", TERMINAL_RESPONSE, BuildTime(millis()).c_str(), TERMINAL_INPUT);
  Publish(TOPIC_RESPONSE, FBuffer);
#endif  
}

void CMQTTClient::WriteLine(String line)
{
  sprintf(FBuffer, "%s", line.c_str());
  Publish(TOPIC_TEXT, FBuffer);
} 
//
void CMQTTClient::WriteEvent(String line)
{
  sprintf(FBuffer, "%s %s", TERMINAL_EVENT, line.c_str());
  Publish(TOPIC_EVENT, FBuffer);
}
//
void CMQTTClient::WriteResponse(String line)
{
  sprintf(FBuffer, "%s %s", TERMINAL_RESPONSE, line.c_str());
  Publish(TOPIC_RESPONSE, FBuffer);
}
//
void CMQTTClient::WriteComment(String line)
{
  sprintf(FBuffer, "%s %s", TERMINAL_COMMENT, line.c_str());
  Publish(TOPIC_COMMENT, FBuffer);
}
void CMQTTClient::WriteComment(String mask, String line)
{
  sprintf(FBuffer, mask.c_str(), line.c_str());
  WriteComment(FBuffer);
}
//
void CMQTTClient::WriteDebug(String line)
{
  sprintf(FBuffer, "%s %s", TERMINAL_DEBUG, line.c_str());
  Publish(TOPIC_DEBUG, FBuffer);
}
//
#endif // MQTTCLIENT_ISPLUGGED
//
