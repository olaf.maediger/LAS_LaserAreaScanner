//
//--------------------------------
//  Library TriggerInput
//--------------------------------
//
#include "Defines.h"
//
#if defined(TRIGGERINPUT_ISPLUGGED)
//
#include "TriggerInput.h"
#include "Command.h"
//
extern CCommand Command;
//
CTriggerInput::CTriggerInput(String id, int pin)
{
  FID = id;
  FPin = pin;
  FRisingEdge = INIT_RISINGEDGE;
  FState = stiZero;
}

EStateTriggerInput CTriggerInput::GetState(void)
{
  return FState;
}  

Boolean CTriggerInput::Open()
{
  pinMode(FPin, INPUT_PULLDOWN);
  FState = stiZero;
  return true;
}

Boolean CTriggerInput::Close()
{
  FState = stiZero;
  return true;
}
//
//-------------------------------------------------------------------------------
//  TriggerInput - Wait
//-------------------------------------------------------------------------------
//
void CTriggerInput::Wait_Start(Boolean risingedge)
{
  FRisingEdge = risingedge;
  FState = stiInit;
}

void CTriggerInput::Wait_Abort(void)
{
  FState = stiZero;
}

Boolean CTriggerInput::Wait_Execute(void)
{
  int PL = digitalRead(FPin);
  switch (FState)
  {
    case stiZero:
      return false;
    case stiInit:
      char Buffer[16];
      sprintf(Buffer, "WTI %s %u %i %i", FID, FPin, (int)FRisingEdge, PL);
      Command.WriteEvent(Buffer);
      FState = stiWait;
      return true;
    case stiWait:
      if (PL == (int)FRisingEdge)
      {
        char Buffer[16];
        sprintf(Buffer, "WTI %s %u %i %i", FID, FPin, (int)FRisingEdge, PL);
        Command.WriteEvent(Buffer);
        FState = stiZero;
        return false;
      }
      return true;
  }
  return false;
}
//
#endif // TRIGGERINPUT_ISPLUGGED
//
