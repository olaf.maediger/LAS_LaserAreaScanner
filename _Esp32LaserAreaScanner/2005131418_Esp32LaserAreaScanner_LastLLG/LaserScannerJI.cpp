//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "Defines.h"
//
#if defined(LASERSCANNER_JI)
//
#if defined(PINOUTPUT_ISPLUGGED)
#include "PinOutput.h"
#endif
#if defined(DACINTERNAL_ISPLUGGED)
#include "DacInternal.h"
#endif
#include "LaserScannerJI.h"
//???#include "Command.h"
//
#if defined(PINOUTPUT_ISPLUGGED)
extern CPinOutput        PinLaserTrigger;
#endif
#if defined(DACINTERNAL_ISPLUGGED)
extern CDacInternal      DacPositionX;
extern CDacInternal      DacPositionY;
#endif
//
//-------------------------------------------------------------------------------
//  LaserScanner - Init
//-------------------------------------------------------------------------------
CLaserScanner::CLaserScanner(String id)
{
  FID = id;
  FState = slsZero;
  FPulsePeriodms = INIT_PULSEPERIOD_MS;
  FTimeRelative = 
}

Boolean CLaserScanner::Open()
{
  FState = slsZero;
  return true;
}

Boolean CLaserScanner::Close()
{
  FState = slsZero;
  return true;
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Property
//-------------------------------------------------------------------------------
EStateLaserScanner CLaserScanner::GetState(void)
{
  return FState;
}  

void CLaserScanner::SetPulsePeriod(UInt32 period)
{
  FPulsePeriodms = period;
}
UInt32 CLaserScanner::GetPulsePeriod(void)
{
  return FPulsePeriodms;
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Handler
//-------------------------------------------------------------------------------
Boolean CLaserScanner::PulseLaserCount(UInt32 period, UInt32 count)
{
  if (slsZero == FState)
  {
    FPulsePeriodms = period;
    FPulseCountActual = 0;
    FPulseCountPreset = count;
    FState = slsInitPulseLaser;
    return true;
  }
  return false;
}

Boolean CLaserScanner::MovePositionX(UInt16 px)
{
  return false;  
}

Boolean CLaserScanner::MovePositionY(UInt16 py)
{
  return false;  
}

Boolean CLaserScanner::MovePositionPulse(UInt16 px, UInt16 PY, UInt32 PP, UInt32 PC, UInt32 DM)
{
  return false;  
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Wait
//-------------------------------------------------------------------------------
//
void CLaserScanner::Abort(void)
{
  // stop all Activities !!! ...
  // FPinPulse = LOW;
  FState = slsZero;
}

Boolean CLaserScanner::IsBusy(void)
{
  return (slsZero != FState);
}
void CLaserScanner::Execute(void)
{ 
  switch (FState)
  {
    case slsZero:
      return;
    case slsInitPulseLaser:
      FPulseCountActual = 0;
      FTimeRelative.Wait_Start(ontime);
      FState = slsBusyPulseLaserHigh;
      return;
    case slsBusyPulseLaserHigh:
      return;
    case slsBusyPulseLaserLow;
      return;
    case slsBusyPulseLaser:
      if  
      //PinLaserTrigger
      FPulseCountActual++;
      if (FPulseCountPreset <= FPulseCountActual)
      {        
        FState = slsZero;
        return;
      }
      return;
    case slsInitMovePulse:
      FState = slsBusyMovePulse;
      return;
    case slsBusyMovePulse:
      return;
  }  
}
//
#endif // LASERSCANNER_JI
//
