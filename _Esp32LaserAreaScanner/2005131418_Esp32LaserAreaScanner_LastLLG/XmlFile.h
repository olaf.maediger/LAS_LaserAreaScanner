//
#include "Defines.h"
//
#if defined(SDCARD_ISPLUGGED)
//
#ifndef XmlFile_h
#define XmlFile_h
//
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <tinyxml2.h>
//
using namespace tinyxml2;
//
class CXmlFile
{
  private:
  fs::FS* FPFS;
  String FText = "";
  XMLDocument FXmlDocument;
  //
  public:
  bool Open(fs::FS* pfs);
  bool Close(void);
  //
  bool ReadFile(const char* path);
  String GetText(void);
  //
  bool Parse(void);
  XMLNode* GetRoot(void);
  XMLElement* GetElement(XMLNode* pnode, const char* name);
  XMLElement* GetNextElement(XMLNode* pnode, const char* name);
};
//
#endif // XmlFile_h
//
#endif // SDCARD_ISPLUGGED
//
