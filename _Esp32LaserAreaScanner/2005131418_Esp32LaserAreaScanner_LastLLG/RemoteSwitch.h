//
//--------------------------------
//  Library RemoteSwitch
//--------------------------------
//
#include "Defines.h"
//
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
//
#ifndef RemoteSwitch_h
#define RemoteSwitch_h
//
#include "Arduino.h"
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateRemoteSwitch
{
  srsZero = 0,
  srsInit = 1,
  srsBusy = 2
};
//
enum EStateForceSwitch
{
  sfsPassive  = 0,
  sfsForceOn  = 1,
  sfsForceOff = 2
};
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const EStateForceSwitch INIT_STATEFORCESWITCH = sfsPassive;
const Boolean INIT_SWITCHON = true;
//
//
//--------------------------------
//  Section - CRemoteSwitch
//--------------------------------
//
class CRemoteSwitch
{
  private:
  String FID;
  EStateRemoteSwitch FStateRemoteSwitch;
  EStateForceSwitch FStateForceSwitch;
  Boolean FSwitchOn;
  //
  public:
  CRemoteSwitch(String id);
  //
  EStateRemoteSwitch GetStateRemoteSwitch(void);
  EStateForceSwitch GetStateForceSwitch(void);
  Boolean GetSwitchOn(void);
  //
  Boolean Open();
  Boolean Close();
  //
  void ForceOn(UInt32 ontime); 
  void ForceOff(UInt32 offtime); 
  //
  void Execute(void);
};
//
#endif // RemoteSwitch_h
//
#endif // RF433MHZCLIENT_ISPLUGGED && REMOTEWIRELESSSWITCH_ISPLUGGED
//
