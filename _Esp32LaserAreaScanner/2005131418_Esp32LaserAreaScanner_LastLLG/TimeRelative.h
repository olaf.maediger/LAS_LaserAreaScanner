//
//--------------------------------
//  Library TimeRelative
//--------------------------------
//
#ifndef TimeRelative_h
#define TimeRelative_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const UInt32 INIT_TIMERELATIVE_MS = 0; //[ms]
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateTimeRelative
{
  strZero = 0,
  strInit = 1,
  strBusy = 2
};
//
class CTimeRelative
{
  private:
  String FID;
  EStateTimeRelative FState;
  UInt32 FTimePeriodms;
  UInt32 FTimeStartms;
  //
  public:
  CTimeRelative(String id);
  //
  EStateTimeRelative GetState(void);
  //
  Boolean Open();
  Boolean Close();
  // 
  UInt32 GetTimePeriodms(void);
  UInt32 GetTimeStartms(void);
  UInt32 GetTimeActualms(void);
  //
  void Wait_Start(UInt32 timeperiodms);
  void Wait_Abort(void);
  Boolean Wait_Execute(void);
};
//
#endif // TimeRelative_h
//
