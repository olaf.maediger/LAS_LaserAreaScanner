#include "WifiClient.h"
#include "Serial.h"
//
extern CSerial SerialCommand;
//
CWifiClient::CWifiClient()
{
  FPWiFiClient = new WiFiClient();
}
void CWifiClient::Open(const char* wifissid, const char* wifipassword)
{
  WiFi.disconnect();
  delay(100);  
  WiFi.disconnect();
  delay(100);  
  SerialCommand.Write(TERMINAL_COMMENT);
  SerialCommand.Write(" Connecting to ");
  SerialCommand.Write(wifissid);
  SerialCommand.Write(" : .");
  WiFi.mode(WIFI_STA);
  WiFi.begin(wifissid, wifipassword);
  while (WL_CONNECTED != WiFi.status())
  {
    delay(500);
    SerialCommand.Write(".");
  }
  randomSeed(micros());
  SerialCommand.WriteLine("");
  //
  SerialCommand.Write(TERMINAL_COMMENT);
  SerialCommand.Write(" WiFi connected to IP address: ");
  IPAddress IPA = WiFi.localIP();
  char Buffer[32];
  sprintf(Buffer, "%3.3d.%3.3d.%3.3d.%3.3d", IPA[0], IPA[1], IPA[2], IPA[3]);
  SerialCommand.WriteLine(Buffer); 
}



  
