//
#include "Defines.h"
//
#ifdef SDCARD_ISPLUGGED
//
#include "SDCard.h"
//
CSDCard::CSDCard(Int16 pincs)
{
  FPinCS = pincs;
  FFile = (File)NULL;
  FLine = "";
}

Int16 CSDCard::Open(fs::FS* psd)
{
  FPSD = psd;  
  FLine = "";
  if (!SD.begin(FPinCS))
  {
    return -1;
  }
  UInt16 CardType = SD.cardType();
  if (CARD_NONE == CardType) 
  {
    return -2;
  }
  if (!SD.begin(FPinCS))
  {
    return -3;
  }
  return 1; 
}

Boolean CSDCard::Close(void)
{
  SD.end();
  return true;
}
//
//-----------------------------------------
// Helper - Hex
//-----------------------------------------
Boolean CSDCard::IsCharacterOfHex(Character value)
{
  switch (value)
  {
    case 'x': case 'X':
      return true;
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
    case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
    case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
      return true;
  }
  return false;
}

UInt32 CSDCard::AsciiHexUnsigned(String text)
{
  UInt32 Result = 0x00;
  Byte Mask = 0;
  Int16 L = text.length();
  for (Int16 CI = L - 1; 0 <= CI; CI--)
  {
    Character C = text[CI];      
    switch (C)
    {
      case '0':         
        break;
      case '1':
        Result += (0x00000001 << Mask);
        break;
      case '2': 
        Result += (0x00000002 << Mask);
        break;
      case '3': 
        Result += (0x00000003 << Mask);
        break;
      case '4':
        Result += (0x00000004 << Mask);
        break;
      case '5': 
        Result += (0x00000005 << Mask);
        break;
      case '6': 
        Result += (0x00000006 << Mask);
        break;
      case '7': 
        Result += (0x00000007 << Mask);
        break;
      case '8': 
        Result += (0x00000008 << Mask);
        break;
      case '9':
        Result += (0x00000009 << Mask);
        break;
      case 'a': case 'A':
        Result += (0x0000000A << Mask);
        break;
      case 'b': case 'B':
        Result += (0x0000000B << Mask);
        break;
      case 'c': case 'C':
        Result += (0x0000000C << Mask);
        break;
      case 'd': case 'D':
        Result += (0x0000000D << Mask);
        break;
      case 'e': case 'E':
        Result += (0x0000000E << Mask);
        break;
      case 'f': case 'F':
        Result += (0x0000000F << Mask);
        break;
      default: // 0x...
        return Result;      
    }
    Mask += 4;
  }
  return Result;
}
//
//-----------------------------------------
// Helper - Type
//-----------------------------------------
Boolean CSDCard::IsCharacter(Character value)
{
  return ((32 <= Byte(value)) && (Byte(value) <= 127));
}

Boolean CSDCard::IsCharacterOfUnsigned(Character value)
{
  switch (value)
  {
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
      return true;
  }
  return false;
}

Boolean CSDCard::IsCharacterOfInt(Character value)
{
  switch (value)
  {
    case '+':
      return true;
    case '-':
      return true;
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
      return true;
  }
  return false;
}

Boolean CSDCard::IsCharacterOfFloat(Character value)
{
  switch (value)
  {
    case '+':
      return true;
    case '-':
      return true;
    case '.':
      return true;
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
      return true;
  }
  return false;
}

Boolean CSDCard::IsCharacterOfDouble(Character value)
{
  switch (value)
  {
    case '+':
      return true;
    case '-':
      return true;
    case '.':
      return true;
    case 'e':
      return true;
    case 'E':
      return true;
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
      return true;
  }
  return false;
}
//
//#############################################################
//
UInt32 CSDCard::AsciiUnsigned(String text)
{
  UInt32 Result = 0x00;
  UInt32 Mask = 1;
  Int16 L = text.length();
  for (Int16 CI = L - 1; 0 <= CI; CI--)
  {
    Character C = text[CI];      
    switch (C)
    {
      case '0':
        break;
      case '1':
        Result += 1 * Mask;
        break;
      case '2': 
        Result += 2 * Mask;
        break;
      case '3': 
        Result += 3 * Mask;
        break;
      case '4':
        Result += 4 * Mask;
        break;
      case '5': 
        Result += 5 * Mask;
        break;
      case '6': 
        Result += 6 * Mask;
        break;
      case '7': 
        Result += 7 * Mask;
        break;
      case '8': 
        Result += 8 * Mask;
        break;
      case '9':
        Result += 9 * Mask;
        break;
      default: // 
        return Result;      
    }
    Mask *= 10;
  }
  return Result;
}

Int32 CSDCard::AsciiInt(String text)
{
  UInt32 Result = 0x00;
  Byte Mask = 1;
  Int16 L = text.length();
  for (Int16 CI = L - 1; 0 <= CI; CI--)
  {
    Character C = text[CI];      
    switch (C)
    {
      case '+':
        break;
      case '-':
        Result = -Result;
        return Result;
      case '0':
        break;
      case '1':
        Result += 1 * Mask;
        break;
      case '2': 
        Result += 2 * Mask;
        break;
      case '3': 
        Result += 3 * Mask;
        break;
      case '4':
        Result += 4 * Mask;
        break;
      case '5': 
        Result += 5 * Mask;
        break;
      case '6': 
        Result += 6 * Mask;
        break;
      case '7': 
        Result += 7 * Mask;
        break;
      case '8': 
        Result += 8 * Mask;
        break;
      case '9':
        Result += 9 * Mask;
        break;
      default: // 0x...
        return Result;      
    }
    Mask *= 10;
  }
  return Result;
}

Float32 CSDCard::AsciiFloat(String text)
{
  return (Float32)atof(text.c_str());
}

Double64 CSDCard::AsciiDouble(String text)
{
  return (Double64)atof(text.c_str());
}

//
//-----------------------------------------
// Write - Management
//-----------------------------------------
Boolean CSDCard::OpenWrite(String filename)
{
  FFile = SD.open(filename, FILE_WRITE);  
  return ((File)NULL != FFile);
}

Boolean CSDCard::CloseWrite(void)
{
  if ((File)NULL != FFile)
  {
    FFile.close();
    FFile = (File)NULL;
  }
  return ((File)NULL == FFile);
}
//
Boolean CSDCard::IsOpenForWriting(void)
{
  return ((File)NULL != FFile);
}
//
//-----------------------------------------
// Write - Type - unformatted
//-----------------------------------------
Boolean CSDCard::Write(Byte value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("%2.2X", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(Character value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("%c", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(Int16 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("%i", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(UInt16 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("%u", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(Int32 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("%li", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(UInt32 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("%lu", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(Float32 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("%.3f", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(Double64 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("%.3f", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(char* value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("%s", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(String value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("%s", value.c_str());
    return true;
  }  
  return false;
}
//
//-----------------------------------------
// Write - Type - formatted
//-----------------------------------------
Boolean CSDCard::Write(String format, Byte value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf(format.c_str(), value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(String format, Character value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf(format.c_str(), value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(String format, Int16 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf(format.c_str(), value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(String format, UInt16 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf(format.c_str(), value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(String format, Int32 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf(format.c_str(), value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(String format, UInt32 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf(format.c_str(), value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(String format, Float32 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf(format.c_str(), value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(String format, Double64 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf(format.c_str(), value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(String format, char* value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf(format.c_str(), value);
    return true;
  }  
  return false;
}

Boolean CSDCard::Write(String format, String value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf(format.c_str(), value.c_str());
    return true;
  }  
  return false;
}
//
//-----------------------------------------
// Write - with Header 0x
//-----------------------------------------
Boolean CSDCard::WriteHex(Byte value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("0x%2.2X", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::WriteHex(UInt16 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("0x%4.4X", value);
    return true;
  }  
  return false;
}

Boolean CSDCard::WriteHex(UInt32 value)
{
  if ((File)NULL != FFile)
  {
    FFile.printf("0x%8.8X", value);
    return true;
  }  
  return false;
}
//
//-----------------------------------------
// Read - Management
//-----------------------------------------
Boolean CSDCard::OpenRead(String filename)
{
  FFile = SD.open(filename, FILE_READ);  
  return ((File)NULL != FFile);
}

Boolean CSDCard::CloseRead(void)
{
  if ((File)NULL != FFile)
  {
    FFile.close();
    FFile = (File)NULL;
  }
  return ((File)NULL == FFile);
}
//
Boolean CSDCard::IsOpenForReading(void)
{
  return ((File)NULL != FFile);
}
//
Int16 CSDCard::ReadAvailable(void)
{
  if ((File)NULL != FFile)
  {    
    return FFile.available();
  }  
  return 0;  
}
//
//-----------------------------------------
// Read - Line
//-----------------------------------------
Boolean CSDCard::ReadLine(String &line)
{
  line = "";
  while (FFile.available())
  {
    Character C = FFile.read();
    switch (C)
    {
      case (char)0x0D: case (char)0x0A:
        if (0 < strlen(FLine.c_str()))
        {
          line = FLine;
          FLine = "";
          return true;
        }
        break;
      default:
        FLine += C;
        break;
    }
  }
  return false;
}
//
//-----------------------------------------
// Read - Type
//-----------------------------------------
Boolean CSDCard::Read(Byte &value)
{
  value = 0;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (IsCharacterOfUnsigned(C))
    { // Is Byte
      FLine += C;
    }
    else
    { // not an Byte
      if (0 < strlen(FLine.c_str()))
      {
        value = AsciiUnsigned(FLine.c_str());
        FLine = "";
        return true;
      }
    }
  }
  return false;
}

Boolean CSDCard::Read(Character &value)
{
  value = 0x00;
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (IsCharacter(C))
    { // Is Byte
      value = C;
      return true;
    }
  }
  return false;
}

Boolean CSDCard::Read(Int16 &value)
{
  value = 0;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (IsCharacterOfInt(C))
    { // Is Integer
      FLine += C;
    }
    else
    { // not an Integer
      if (0 < strlen(FLine.c_str()))
      {
        value = atoi(FLine.c_str());
        FLine = "";
        return true;
      }
    }
  }
  return false;
}

Boolean CSDCard::Read(UInt16 &value)
{
  value = 0;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (IsCharacterOfUnsigned(C))
    { 
      FLine += C;
    }
    else
    { 
      if (0 < strlen(FLine.c_str()))
      {
        value = atoi(FLine.c_str());
        FLine = "";
        return true;
      }
    }
  }
  return false;
}

Boolean CSDCard::Read(UInt32 &value)
{
  value = 0;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (IsCharacterOfUnsigned(C))
    { 
      FLine += C;
    }
    else
    { 
      if (0 < strlen(FLine.c_str()))
      {
        value = atol(FLine.c_str());
        FLine = "";
        return true;
      }
    }
  }
  return false;
}

Boolean CSDCard::Read(Int32 &value)
{
  value = 0;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (IsCharacterOfInt(C))
    { 
      FLine += C;
    }
    else
    { 
      if (0 < strlen(FLine.c_str()))
      {
        value = atol(FLine.c_str());
        FLine = "";
        return true;
      }
    }
  }
  return false;
}

Boolean CSDCard::Read(Float32 &value)
{
  value = 0.0f;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (IsCharacterOfFloat(C))
    { 
      FLine += C;
    }
    else
    { 
      if (0 < strlen(FLine.c_str()))
      {
        value = atof(FLine.c_str());
        FLine = "";
        return true;
      }
    }
  }
  return false;  
}

Boolean CSDCard::Read(Double64 &value)
{
  value = 0.0;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();    
    if (IsCharacterOfDouble(C))
    { // Is Double
      FLine += C;
    }
    else
    { // not an Double
      if (0 < strlen(FLine.c_str()))
      {
        value = atof(FLine.c_str());
        FLine = "";
        return true;
      }
    }
  }
  return false;
}

Boolean CSDCard::Read(Character terminal, PCharacter value)
{
  value[0] = 0x00;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (terminal == C)
    {
      if (0 < strlen(FLine.c_str()))
      {
        strcpy(value, FLine.c_str());
        return true;
      }
      return false;      
    }
    switch (C)
    {
      case 0x0A: case 0x0D:
        if (0 < strlen(FLine.c_str()))
        {
          strcpy(value, FLine.c_str());
          return true;
        }
        break;
      default:
        FLine += C;
        break;
    }
  }
  return false;
}

Boolean CSDCard::Read(Character terminal, String& value)
{
  value = "";
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (terminal == C)
    {
      if (0 < strlen(FLine.c_str()))
      {
        value += FLine;
        return true;
      }
      return false;      
    }
    switch (C)
    {
      case 0x0A: case 0x0D:
        if (0 < strlen(FLine.c_str()))
        {
          value += FLine;
          return true;
        }
        break;
      default:
        FLine += C;
        break;
    }
  }
  return false;
}
//
//-----------------------------------------
// Read - with Header 0x
//-----------------------------------------
Boolean CSDCard::ReadHex(Byte &value)
{
  value = 0x00;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (IsCharacterOfHex(C))
    { // Is Byte
      FLine += C;
    }
    else
    { // not an Byte
      if (0 < strlen(FLine.c_str()))
      {
        value = AsciiHexUnsigned(FLine.c_str());
        FLine = "";
        return true;
      }
    }
  }
  return false;
}

Boolean CSDCard::ReadHex(UInt16 &value)
{
  value = 0x00;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (IsCharacterOfHex(C))
    { // Is Byte
      FLine += C;
    }
    else
    { // not an Byte
      if (0 < strlen(FLine.c_str()))
      {
        value = AsciiHexUnsigned(FLine.c_str());
        FLine = "";
        return true;
      }
    }
  }
  return false;
}

Boolean CSDCard::ReadHex(UInt32 &value)
{
  value = 0x00;
  FLine = "";
  while (FFile.available())
  {    
    Character C = FFile.read();
    if (IsCharacterOfHex(C))
    { // Is Byte
      FLine += C;
    }
    else
    { // not an Byte
      if (0 < strlen(FLine.c_str()))
      {
        value = AsciiHexUnsigned(FLine.c_str());
        FLine = "";
        return true;
      }
    }
  }
  return false;
}
//
#endif // SDCARD_ISPLUGGED
//
