//
//--------------------------------
//  Library TimeRelative
//--------------------------------
//
#include "Defines.h"
//
#include "TimeRelative.h"
#include "Command.h"
//
extern CCommand Command;
//
CTimeRelative::CTimeRelative(String id)
{
  FID = id;
  FState = strZero;
  FTimePeriodms = INIT_TIMERELATIVE_MS;
  FTimeStartms = INIT_TIMERELATIVE_MS;
}

EStateTimeRelative CTimeRelative::GetState(void)
{
  return FState;
}  

Boolean CTimeRelative::Open()
{
  FState = strZero;
  return true;
}

Boolean CTimeRelative::Close()
{
  FState = strZero;
  return true;
}

UInt32 CTimeRelative::GetTimePeriodms(void)
{
  return FTimePeriodms;
}

UInt32 CTimeRelative::GetTimeStartms(void)
{
  return FTimeStartms;
}

UInt32 CTimeRelative::GetTimeActualms(void)
{
  return millis() - FTimeStartms;
}
//
//-------------------------------------------------------------------------------
//  TimeRelative - Wait
//-------------------------------------------------------------------------------
//
void CTimeRelative::Wait_Start(UInt32 timeperiodms)
{
  FTimePeriodms = timeperiodms;
  FTimeStartms = millis();
  FState = strInit;
}

void CTimeRelative::Wait_Abort(void)
{
  FState = strZero;
}

Boolean CTimeRelative::Wait_Execute(void)
{
  switch (FState)
  {
    case strZero:
      return false;
    case strInit:
      FTimeStartms = millis();
      char Buffer[16];
      sprintf(Buffer, "WTR %s 1", FID);
      Command.WriteEvent(Buffer);
      FState = strBusy;
      return true;
    case strBusy:
      if (FTimePeriodms <= (millis() - FTimeStartms))
      {
        char Buffer[16];
        sprintf(Buffer, "WTR %s 0", FID);
        Command.WriteEvent(Buffer);
        FState = strZero;
        return false;
      }
      return true;
  }
  return false;
}
