//
//--------------------------------
//  Library TimeRelative
//--------------------------------
//
#include "Defines.h"
//
#ifndef TimeRelative_h
#define TimeRelative_h
//
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const Float32 INIT_TIMEPERIOD_MS = 0.0f;  //[ms]
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateTimeRelative
{
  strZero = 0,
  strInit = 1,
  strBusy = 2
};
//
class CTimeRelative
{
  private:
  String FID;
  EStateTimeRelative FState;
  Boolean FMessageOn;
  Float32 FTimePeriodms;
  Float32 FTimeStartms;
  //
  public:
  CTimeRelative(String id, Boolean messageon);
  //
  EStateTimeRelative GetState(void);
  //
  Boolean Open();
  Boolean Close();
  // 
  void SetTimeStartActual(void);
  Float32 GetTimePeriodms(void);
  Float32 GetTimeStartms(void);
  Float32 GetTimeActualms(void);
  //
  UInt32 GetTimePeriodus(void);
  UInt32 GetTimeStartus(void);
  UInt32 GetTimeActualus(void);
  //
  void Wait_Start(Float32 timeperiodms);
  void Wait_Abort(void);
  Boolean Wait_Execute(void); // True:Active False:Passive
};
//
#endif // TimeRelative_h
//
