//
#include "Defines.h"
//
#if defined(RF433MHZCLIENT_ISPLUGGED)
//
#if !defined(RF433MHzClient_h)
#define RF433MHzClient_h
//
#include <RCSwitch.h>
//
#define INIT_RF433MHZ_PROTOCOL            4
#define INIT_RF433MHZ_PULSELENGTH_US    350 
//
class CRF433MHzClient
{
  private:
  String FID;
  int FPinTXD;
  RCSwitch* FPRCSwitch;
  //
  public:
  CRF433MHzClient(String id, int pintxd);
  //
  inline RCSwitch* GetRCSwitch(void)
  {
    return FPRCSwitch;
  }
  //
  void Open(void);
  void Close(void);
  //
  void SetProtocol(Byte protocol);
  void SetPulseLength(UInt16 pulselength);
  //
  void TransmitCode(UInt32 code);
  //
  void SwitchOn();
  void SwitchOff();
};
//
#endif // RF433MHzClient_h
//
#endif // RF433MHZCLIENT_ISPLUGGED
