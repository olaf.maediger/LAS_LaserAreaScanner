//
#include "Defines.h"
//
#if defined(MQTTCLIENT_ISPLUGGED)
//
#if !defined(MQTTClient_h)
#define MQTTClient_h
//
#include <PubSubClient.h>
#include "WifiClient.h"
//
//--------------------------------------------------------------------
//  Constant
//--------------------------------------------------------------------
//
const int SIZE_MQTTBUFFER = 64;
//
//--------------------------------------------------------------------
//  MQTTClient
//--------------------------------------------------------------------
//
class CMQTTClient
{
  private:
  const char* FBrokerIPAddress;
  CWifiClient* FPWifiClient;
  PubSubClient* FPPubSubClient; 
  String FReceivedText;
  Character FBuffer[SIZE_MQTTBUFFER];
  //
  public:
  CMQTTClient();
  //
//  inline void SetBrokerIPAddress(const char* brokeripaddress)
//  {
//    FBrokerIPAddress = brokeripaddress;
//  }
  inline PubSubClient* GetPubSubClient()
  {
    return FPPubSubClient;
  }
  //
  public:
  void Initialise(const char* wifissid, const char* wifipassword);
  void Open(const char* brokeripaddress, const int brokerport);
  bool Reconnect(void);
  bool RefreshConnection(void);
  void Execute(void);
  //
  static void MQTTCallback(char* topic, byte* payload, unsigned int length);   
  //
  void Publish(const char *topic, const char* message);
  void Subscribe(const char *topic);
  //
  inline void SetReceivedText(String text)
  {
    FReceivedText += text;
  }
  inline String ReadReceivedText()
  {
  String Result = FReceivedText;
  FReceivedText = "";
  return Result;
  }
  //------------------------------------------------------
  void WritePrompt();
  void WriteLine(String line);
  void WriteEvent(String line);
  void WriteResponse(String line);
  void WriteComment(String line);
  void WriteComment(String mask, String line);
  void WriteDebug(String line);
};
//
#endif // MQTTClient_h
//
#endif // MQTTCLIENT_ISPLUGGED
//
