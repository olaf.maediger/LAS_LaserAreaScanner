//
//--------------------------------
//  Library TriggerInput
//--------------------------------
//
#include "Defines.h"
//
#if defined(TRIGGERINPUT_ISPLUGGED)
//
#ifndef TriggerInput_h
#define TriggerInput_h
//
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const Boolean INIT_RISINGEDGE = true;
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateTriggerInput
{
  stiZero = 0,
  stiInit = 1,
  stiWait = 2
};
//
class CTriggerInput
{
  private:
  String FID;
  UInt16 FPin;
  Boolean FRisingEdge;
  EStateTriggerInput FState;
  //
  public:
  CTriggerInput(String id, int pin);
  //
  EStateTriggerInput GetState(void);
  //
  Boolean Open();
  Boolean Close();
  // 
  void Wait_Start(Boolean risingedge);
  void Wait_Abort(void);
  Boolean Wait_Execute(void);
};
//
#endif // TriggerInput_h
//
#endif // TRIGGERINPUT_ISPLUGGED
//
