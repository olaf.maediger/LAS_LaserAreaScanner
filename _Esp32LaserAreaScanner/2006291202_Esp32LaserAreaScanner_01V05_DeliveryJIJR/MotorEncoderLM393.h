#if defined(MOTORENCODERLM393_ISPLUGGED)
//
//--------------------------------
//  Library MotorEncoderLM393
//--------------------------------
//
#ifndef MotorEncoderLM393_h
#define MotorEncoderLM393_h
//
#include "Defines.h"
//#include "Pinout.h"
//
const Byte POSITION_ZERO = 0x00000000;
//
//--------------------------------
//  Section - Type
//--------------------------------
//
//
enum EStateMotorEncoderLM393
{
  medError      = -2,
  medUndefined  = -1,
  medIdle       = 0,
  medPositive   = 1,
  medNegative   = 2
};
//
const EStateMotorEncoderLM393 INIT_STATEMOTORENCODER = medUndefined;
const long signed INIT_ENCODERCOUNT = 0;
//
class CMotorEncoderLM393
{
  private:
  EStateMotorEncoderLM393 FState;
  int FPinPulseA;
  TInterruptFunction FPIrqFunctionPulseA;
  bool FLevelPresetA;
  Int32 FPositionActualS, FPositionLastS;
  UInt32 FErrorCount;
  //
  public:
  CMotorEncoderLM393(int pinpulsea,
                     TInterruptFunction pirqfunctionpulsea);
  //
  inline UInt32 GetErrorCount(void)
  {
    UInt32 EC = FErrorCount;
    FErrorCount = 0;
    return EC;
  }
  //
  inline Int32 GetPositionActual()
  {
    return FPositionActualS;
  }
  inline Int32 GetDeltaPosition()
  {
    return FPositionActualS - FPositionLastS;
  }
  inline void SetPositionActual(Int32 positionactuals)
  {
    FPositionActualS = positionactuals;
  }
  //
  bool GetPositionChanged(Int32 &positionactual, Int32 &deltaposition);
  //
  bool Open(void);
  bool Close(void);
  //
  void SetMoveDirection(EStateMotorEncoderLM393 state);
  //
  void HandleInterrupt();
};
//
#endif // MotorEncoderLM393_h
//
#endif // MOTORENCODERLM393_ISPLUGGED
