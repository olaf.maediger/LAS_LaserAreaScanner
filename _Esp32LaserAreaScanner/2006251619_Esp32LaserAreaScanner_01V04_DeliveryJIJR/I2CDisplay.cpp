#include "I2CDisplay.h"
//
CI2CDisplay::CI2CDisplay(UInt8 i2caddress, UInt8 colcount, UInt8 rowcount)
{
  FI2CAddress = i2caddress;
  FRowCount = rowcount;
  FColCount = colcount;
  FPI2CDisplayBase = new CI2CDisplayBase(FI2CAddress, FColCount, FRowCount, LCD_5x8DOTS);
}

Boolean CI2CDisplay::Open()
{
  FPI2CDisplayBase->Open();
  FPI2CDisplayBase->Clear();
  FPI2CDisplayBase->SetBacklightOn();
  return true;
}

Boolean CI2CDisplay::Close()
{
  return true;
}

// LowLevel
Byte CI2CDisplay::GetI2CAddress()
{
  return FI2CAddress;
}

// HighLevel
void CI2CDisplay::SetBacklightOn()
{
  FPI2CDisplayBase->SetBacklightOn();
}

void CI2CDisplay::SetBacklightOff()
{
  FPI2CDisplayBase->SetBacklightOff();  
}

void CI2CDisplay::SetHomePosition()
{
  FPI2CDisplayBase->Home();
}
  
void CI2CDisplay::ClearDisplay()
{
  FPI2CDisplayBase->Clear();
}

void CI2CDisplay::SetCursorPosition(Byte row, Byte col)
{
  FPI2CDisplayBase->SetCursorPosition(col, row);
}

void CI2CDisplay::WriteText(String text)
{
  FPI2CDisplayBase->print(text);
}

void CI2CDisplay::WriteByte(Byte value)
{
  FPI2CDisplayBase->print(value, HEX);
}

void CI2CDisplay::WriteUInt8(UInt8 value)
{
  FPI2CDisplayBase->print(value, DEC);
}

void CI2CDisplay::WriteInt16(Int16 value)
{
  FPI2CDisplayBase->print(value, DEC);  
}



