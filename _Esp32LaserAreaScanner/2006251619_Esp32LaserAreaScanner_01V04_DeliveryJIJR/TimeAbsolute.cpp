//
//--------------------------------
//  Library TimeAbsolute
//--------------------------------
//
#include "Defines.h"
//
#if defined(NTPCLIENT_ISPLUGGED)
//
#include "NTPClient.h"
#include "TimeAbsolute.h"
//
#include "Command.h"

#include "Serial.h"
//
extern CCommand   Command;
extern CNTPClient NTPClient;

extern CSerial SerialCommand;
//
CTimeAbsolute::CTimeAbsolute(String id, Boolean messageon)
{
  FID = id;
  FState = staZero;
  FMessageOn = messageon;
  FTimeStampHH = INIT_TIMEABSOLUTE_HH;
  FTimeStampMM = INIT_TIMEABSOLUTE_MM;
  FTimeStampSS = INIT_TIMEABSOLUTE_SS;
  FTimeStartHH = INIT_TIMEABSOLUTE_HH;
  FTimeStartMM = INIT_TIMEABSOLUTE_MM;
  FTimeStartSS = INIT_TIMEABSOLUTE_SS;
}

EStateTimeAbsolute CTimeAbsolute::GetState(void)
{
  return FState;
}  

Boolean CTimeAbsolute::Open()
{
  FState = staZero;
  return true;
}

Boolean CTimeAbsolute::Close()
{
  FState = staZero;
  return true;
}

Byte CTimeAbsolute::GetTimeStampHH(void)
{
  return FTimeStampHH;
}
Byte CTimeAbsolute::GetTimeStampMM(void)
{
  return FTimeStampMM;
}
Byte CTimeAbsolute::GetTimeStampSS(void)
{
  return FTimeStampSS;
}

Byte CTimeAbsolute::GetTimeStartHH(void)
{
  return FTimeStartHH;
}
Byte CTimeAbsolute::GetTimeStartMM(void)
{
  return FTimeStartMM;
}
Byte CTimeAbsolute::GetTimeStartSS(void)
{
  return FTimeStartSS;
}

Byte CTimeAbsolute::GetTimeActualHH(void)
{
  return 0; //NTPClient...;
}
Byte CTimeAbsolute::GetTimeActualMM(void)
{
  return 0; //NTPClient...;
}
Byte CTimeAbsolute::GetTimeActualSS(void)
{
  return 0; //NTPClient...;
}
//
//-------------------------------------------------------------------------------
//  TimeAbsolute - Waut
//-------------------------------------------------------------------------------
//
void CTimeAbsolute::Wait_Start(Byte timestamphh, Byte timestampmm, Byte timestampss)
{
  FTimeStampHH = timestamphh;
  FTimeStampMM = timestampmm;
  FTimeStampSS = timestampss;
  FState = staInit;
}

void CTimeAbsolute::Wait_Abort(void)
{
  FState = staZero;
}

Boolean CTimeAbsolute::Wait_Execute(void)
{
  switch (FState)
  {
    case staZero:
      return false;
    case staInit:
      if (FMessageOn)
      {
        char Buffer[16];
        sprintf(Buffer, "WTA %s 1", FID);
        Command.WriteEvent(Buffer);
      }
      FState = staBusy;
      return true;
    case staBusy:
      String HA, MA, SA;
      NTPClient.GetTime(HA, MA, SA);
      Byte HH = atoi(HA.c_str());
      Byte MM = atoi(MA.c_str());
      Byte SS = atoi(SA.c_str());
      UInt32 TimeActual = 3600 * HH + 60 * MM + SS;
      UInt32 TimeStamp = 3600 * FTimeStampHH + 60 * FTimeStampMM + FTimeStampSS;
      if (TimeStamp < TimeActual)
      { // wait time reached
        if (FMessageOn)
        {
          char Buffer[16];
          sprintf(Buffer, "WTA %s 0", FID);
          Command.WriteEvent(Buffer);
        }
        FState = staZero;
        return false;        
      }        
      return true;
  }
  return false;
}
//
#endif // NTPCLIENT_ISPLUGGED
//
