//
#include "Defines.h"
//
#if defined(SDCARD_ISPLUGGED)
//
#ifndef CommandFile_h
#define CommandFile_h
//
#include "FS.h"
#include "SD.h"
#include "SPI.h"
//
class CCommandFile
{
  private:
  fs::FS* FPFS;
  String FText = "";
  unsigned FTextIndex = 0;
  //
  public:
  bool Open(fs::FS* pfs);
  bool Close(void);
  //
  bool ParseFile(const char* path);
  String GetCommand(void);
  void ResetExecution(void);
  bool IsExecuting(void);
};
//
#endif // CommandFile_h
//
#endif // SDCARD_ISPLUGGED
//
