#ifndef Command_h
#define Command_h
//
#include "Defines.h"
#include "Process.h"
#include "Automation.h"
#include "Utilities.h"
#include "Serial.h"
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
#include "MQTTClient.h"
#endif
//
#define ARGUMENT_PROJECT    "LAS - Esp32LaserAreaScanner"
#define ARGUMENT_SOFTWARE   "01V04"
#define ARGUMENT_DATE       "200625"
#define ARGUMENT_TIME       "1030"
#define ARGUMENT_AUTHOR     "OMDevelop"
#define ARGUMENT_PORT       "SerialCommand (Serial-USB)"
#define ARGUMENT_PARAMETER  "115200, N, 8, 1"
//
#ifdef PROCESSOR_NANOR3
#define ARGUMENT_HARDWARE "NanoR3"
#endif
#ifdef PROCESSOR_UNOR3
#define ARGUMENT_HARDWARE "UnoR3"
#endif
#ifdef PROCESSOR_MEGA2560
#define ARGUMENT_HARDWARE "Mega2560"
#endif
#ifdef PROCESSOR_DUEM3
#define ARGUMENT_HARDWARE "DueM3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
#ifdef PROCESSOR_ESP8266
#define ARGUMENT_HARDWARE "Esp8266"
#endif
#ifdef PROCESSOR_ESP32
#define ARGUMENT_HARDWARE "Esp32"
#endif
// 
// HELP_COMMON
#define SHORT_H     "H"
#if defined(COMMAND_SYSTEMENABLED)
#define SHORT_GPH   "GPH"
#define SHORT_GSV   "GSV"
#define SHORT_GHV   "GHV"
#if defined(WATCHDOG_ISPLUGGED)
#define SHORT_PWD   "PWD"
#endif
#define SHORT_RSS   "RSS"
#define SHORT_A     "A"
#endif
#define SHORT_WTR   "WTR"     // WTR T        - WaitTimeRelative
#if defined(NTPCLIENT_ISPLUGGED)
#define SHORT_WTA   "WTA"     // WTA HH MM SS - WaitTimeAbsolute
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
#define SHORT_WTI   "WTI"     // WTI RE       - WaitInputTriggered
#endif
//
// HELP_SERIAL_COMMAND
//#define SHORT_WLC   "WLC"
//#define SHORT_RLC   "RLC"
//
// HELP_IC2DISPLAY
#if defined(I2CDISPLAY_ISPLUGGED)
#define SHORT_CLI   "CLI"
#define SHORT_STI   "STI"
#endif
//
// HELP_NTPCLIENT
#if defined(NTPCLIENT_ISPLUGGED)
#define SHORT_GNT   "GNT"
#define SHORT_GND   "GND"
#endif
//
// HELP_SDCOMMAND
#if defined(SDCARD_ISPLUGGED)
//!!!#define SHORT_OCF   "OCF"
//#define SHORT_WCF   "WCF"
//#define SHORT_CCF   "CCF"
#define SHORT_ECF   "ECF"
#define SHORT_ACF   "ACF"
#endif
//
// HELP_RFIDCLIENT
#if defined(RFIDCLIENT_ISPLUGGED)
// #define SHORT_SRL   "SRL"   // SRL I RGB C
#endif
//
// HELP_LEDSYSTEM
#if defined(COMMAND_SYSTEMENABLED)
#define SHORT_GLS   "GLS"
#define SHORT_LSH   "LSH"
#define SHORT_LSL   "LSL"
#define SHORT_BLS   "BLS"
#endif
#if defined(MULTICHANNELLED_ISPLUGGED)
#define SHORT_BL1   "BL1"
#define SHORT_BL2   "BL2"
#define SHORT_BL3   "BL3"
#define SHORT_BL4   "BL4"
#define SHORT_BL5   "BL5"
#define SHORT_BL6   "BL6"
#define SHORT_BL7   "BL7"
#define SHORT_BL8   "BL8"
#endif
//
// HELP_MOTORVNH2SP30
#if defined(MOTORVNH2SP30_ISPLUGGED)
#define SHORT_GLWL  "GLWL"   // GLWL    - GetLeftPwmLow
#define SHORT_SLWL  "SLWL"   // SLWL P
#define SHORT_GLWH  "GLWH"   // GLWH    - GetLeftPwmHigh
#define SHORT_SLWH  "SLWH"   // SLWH P
#
#define SHORT_GRWL  "GRWL"   // GRWL    - GetRightPwmLow
#define SHORT_SRWL  "SRWL"   // SRWL P
#define SHORT_GRWH  "GRWH"   // GRWH    - GetRightPwmHigh
#define SHORT_SRWH  "SRWH"   // SRWH P
//
#define SHORT_S     "S"       // S          - Stop
#define SHORT_SL    "SL"      // SL         - StopLeft
#define SHORT_SR    "SR"      // SR         - StopRight
#define SHORT_MLP   "MLP"     // MLP PL     - MoveLeftPositive
#define SHORT_MLN   "MLN"     // MLN PL     - MoveLeftNegative
#define SHORT_MRP   "MRP"     // MRP PR     - MoveRightPositive
#define SHORT_MRN   "MRN"     // MRN PR     - MoveRightNegative
#define SHORT_MBP   "MBP"     // MBP PL PR  - MoveBothPositive
#define SHORT_MBN   "MBN"     // MBN PL PR  - MoveBothNegative
#define SHORT_RBP   "RBP"     // RBP PL PR  - RotateBothPositive
#define SHORT_RBN   "RBN"     // RBN PL PR  - RotateBothNegative
//
#define SHORT_MPA   "MPA"     // MPA PL PR VT - MovePositionAbsolute
#define SHORT_MPR   "MPR"     // MPR PL PR VT - MovePositionRelative
#endif
//
// HELP_MOTORENCODERLM393
#if defined(MOTORENCODERLM393_ISPLUGGED)
#define SHORT_GEPB  "GEPB"    // GEPB          - GetEncoderPositionBoth
#define SHORT_SEPL  "SEPL"    // SEPL EP       - SetEncoderPositionLeft
#define SHORT_SEPR  "SEPR"    // SEPR EP       - SetEncoderPositionRight
#define SHORT_WPR   "WPR"     // WPR           - WaitPositionReached
#endif
//
// HELP_REMOTEWIRELESSSWITCH
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
#define SHORT_RSO  "RSO"      // RSO          - RemoteSwitchOn
#define SHORT_RSF  "RSF"      // RSF          - RemoteSwitchOff
#endif
//
// HELP_LASERSCANNER
#if defined(LASERSCANNER_JI)
#define SHORT_SPP  "SPP"      //           - SetPulsePeriod [ms]
#define SHORT_GPP  "GPP"      //           - GetPulsePeriod [ms]
#define SHORT_PLA  "PLA"      //           - PulseLaserAbort 
#define SHORT_PLC  "PLC"      //           - PulseLaserCount [1]
#define SHORT_MPX  "MPX"      //           - MovePositionX [stp]
#define SHORT_MPY  "MPY"      //           - MovePositionY [stp]
#define SHORT_MPP  "MPP"      //           - MovePositionPulse {px py pp pc dm}
#endif
//
#if defined(LASERSCANNER_PS)
#define SHORT_WTI  "WTI"      //           - WaitTriggerIn wt
#endif
//
#define MASK_ENDLINE " ###"
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
//-----------------------------------------------------------------------------------------
//
#define TITLE_LINE            "--------------------------------------------------"
#define MASK_PROJECT          "- Project:   %-35s -"
#define MASK_SOFTWARE         "- Version:   %-35s -"
#define MASK_HARDWARE         "- Hardware:  %-35s -"
#define MASK_DATE             "- Date:      %-35s -"
#define MASK_TIME             "- Time:      %-35s -"
#define MASK_AUTHOR           "- Author:    %-35s -"
#define MASK_PORT             "- Port:      %-35s -"
#define MASK_PARAMETER        "- Parameter: %-35s -"
//
//-----------------------------------------------------------------------------------------
//
#define HELP_COMMON               " Help (Common):"
#define MASK_H                    " %-3s                : This Help"
#if defined(COMMAND_SYSTEMENABLED)
#define MASK_GPH                  " %-3s                : Get Program Header"
#define MASK_GSV                  " %-3s                : Get Software-Version"
#define MASK_GHV                  " %-3s                : Get Hardware-Version"
#endif
#define MASK_RSS                  " %-3s                : Reset System"
#if defined(WATCHDOG_ISPLUGGED)
#define MASK_PWD                  " %-3s                : Pulse WatchDog"
#endif
#if defined(COMMAND_SYSTEMENABLED)
#define MASK_A                    " %-1s                  : Abort Process Execution"
#endif
#define MASK_WTR                  " %-3s <t>            : Wait Time Relative T{ms}"
#if defined(NTPCLIENT_ISPLUGGED)
#define MASK_WTA                  " %-3s <hh> <mm> <ss> : Wait Time Absolute {HH:MM:SS}"
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
#define MASK_WTI                  " %-3s <re>           : Wait Trigger Input <risingedge>{1}"
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
#define HELP_LEDSYSTEM            " Help (LedSystem):"
#define MASK_GLS                  " %-3s                : Get State LedSystem"
#define MASK_LSH                  " %-3s                : Switch LedSystem On"
#define MASK_LSL                  " %-3s                : Switch LedSystem Off"
#define MASK_BLS                  " %-3s <n> <p> <w>    : Blink LedSystem <n>times <p>eriod{ms}/<w>ith{ms}"
#endif
#if defined(MULTICHANNELLED_ISPLUGGED)
#define MASK_BL1                  " %-3s <n> <p> <w>    : Blink LedChannel1 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL2                  " %-3s <n> <p> <w>    : Blink LedChannel2 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL3                  " %-3s <n> <p> <w>    : Blink LedChannel3 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL4                  " %-3s <n> <p> <w>    : Blink LedChannel4 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL5                  " %-3s <n> <p> <w>    : Blink LedChannel5 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL6                  " %-3s <n> <p> <w>    : Blink LedChannel6 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL7                  " %-3s <n> <p> <w>    : Blink LedChannel7 <n>times <p>eriod{ms}/<w>ith{ms}"
#define MASK_BL8                  " %-3s <n> <p> <w>    : Blink LedChannel8 <n>times <p>eriod{ms}/<w>ith{ms}"
#endif
//
#if defined(I2CDISPLAY_ISPLUGGED)
#define HELP_I2CDISPLAY           " Help (I2CDisplay):"
#define MASK_CLI                  " %-3s                : Clear Display"
#define MASK_STI                  " %-3s <r> <c> <t>    : Show <t>ext at <r>ow <c>ol"
#endif
//
#if defined(NTPCLIENT_ISPLUGGED)
#define HELP_NTPCLIENT            " Help (NTPClient):"
#define MASK_GNT                  " %-3s                : Get NTPClient Time {hh:mm:ss}"
#define MASK_GND                  " %-3s                : Get NTPClient Date {yy.mm.dd}"
#endif
//
#if defined(SDCARD_ISPLUGGED)
#define HELP_SDCOMMAND            " Help (SDCommand):"
//!!!#define MASK_OCF                  " %-3s <f>            : Open Command<f>ile for writing"
//!!!#define MASK_WCF                  " %-3s <c> <p> ..     : Write <c>ommand with <p>arameter(s) to File"
//!!!#define MASK_CCF                  " %-3s                : Close Command<f>ile for writing"
#define MASK_ECF                  " %-3s <f>            : Execute Command<f>ile"
#define MASK_ACF                  " %-3s                : Abort Execution Commandfile"
#endif
//
#if defined(RFIDCLIENT_ISPLUGGED)
#define HELP_RFIDCLIENT             " Help (RFIDClient):"
//#define MASK_GID                  " %-3s              : Get RFID[xxxx]"
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
#define HELP_MOTORVNH2SP30        " Help (MotorVNH2SP30):"
#define MASK_GLWL                 " %-4s               : Get Left PwmLow[1]"
#define MASK_SLWL                 " %-4s <pwm>         : Set Left PwmLow[1]"
#define MASK_GLWH                 " %-4s               : Get Left PwmHigh[1]"
#define MASK_SLWH                 " %-4s <pwm>         : Set Left PwmHigh[1]"
//
#define MASK_GRWL                 " %-4s               : Get Right PwmLow[1]"
#define MASK_SRWL                 " %-4s <pwm>         : Set Right PwmLow[1]"
#define MASK_GRWH                 " %-4s               : Get Right PwmHigh[1]"
#define MASK_SRWH                 " %-4s <pwm>         : Set Right PwmHigh[1]"
//
#define MASK_SL                   " %-3s                : Stop Left"
#define MASK_SR                   " %-3s                : Stop Right"
#define MASK_S                    " %-3s                : Stop Both"
#define MASK_MLP                  " %-3s <vl>           : Move Left Positive <v>elocity[%%]"
#define MASK_MLN                  " %-3s <vl>           : Move Left Negative <v>elocity[%%]"
#define MASK_MRP                  " %-3s <vr>           : Move Right Positive <v>elocity[%%]"
#define MASK_MRN                  " %-3s <vr>           : Move Right Negative <v>elocity[%%]"
#define MASK_MBP                  " %-3s <vl> <vr>      : Move Both Positive <v>elocity[%%]"
#define MASK_MBN                  " %-3s <vl> <vr>      : Move Both Negative <v>elocity[%%]"
#define MASK_RBP                  " %-3s <vl> <vr>      : Rotate Both Positive <v>elocity[%%]"
#define MASK_RBN                  " %-3s <vl> <vr>      : Rotate Both Negative <v>elocity[%%]"
#define MASK_MPT                  "
//
#define MASK_MPA                  " %-3s <pl> <pr> <vt> : Move Position Absolute PL{stp} PR{stp} VT{%%}"
#define MASK_MPR                  " %-3s <pl> <pr> <vt> : Move Position Relative PL{stp} PR{stp} VT{%%}"
#endif
//
#if defined(MOTORENCODERLM393_ISPLUGGED)
#define HELP_MOTORENCODERLM393    " Help (MotorEncoderLM393):"
#define MASK_GEPB                 " %-4s               : Get EncoderPositionBoth[stp]"
#define MASK_SEPL                 " %-4s <ep>          : Set EncoderPositionLeft <ep>osition[stp]"
#define MASK_SEPR                 " %-4s <ep>          : Set EncoderPositionRight <ep>osition[stp]"
#define MASK_WPR                  " %-3s               : Wait Position Reached"
#endif
//
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
#define HELP_REMOTEWIRELESSSWITCH " Help (RemoteSwitch):"
#define MASK_RSO                  " %-3s <t>            : Remote Switch On <t>ime{ms}"
#define MASK_RSF                  " %-3s <t>            : Remote Switch Off <t>ime{ms}"
#endif
//
#if defined(LASERSCANNER_JI)
#define HELP_LASERSCANNER         " Help (LaserScanner):"
#define MASK_SPP                  " %-3s <p>                 : Set PulsePeriod{ms}"
#define MASK_GPP                  " %-3s                     : Get PulsePeriod{ms}"
#define MASK_PLA                  " %-3s                     : Pulse Laser Abort"
#define MASK_PLC                  " %-3s <p> <c>             : Pulse <p>eriod{ms} Laser<c>ount{1}"
#define MASK_MPX                  " %-3s <x>                 : Move PositionX{stp}"
#define MASK_MPY                  " %-3s <y>                 : Move PositionY{stp}"
#define MASK_MPP                  " %-3s <x> <y> <p> <c> <d> : Move PositionPulse <x><y>{stp} <p>{ms} <c>{1} <d>{ms}"
#endif
//
#if defined(LASERSCANNER_PS)
#define HELP_LASERSCANNER         " Help (LaserScanner):"
#define MASK_WTI                  " %-3s <t>         : Wait TriggerIn <t>ime{ms}"
#endif
//
#define MASK_STATEPROCESS         "STP %i %i"

//-----------------------------------------------------------------------------------------
//
const int COUNT_SOFTWAREVERSION = 1;
//
#define MASK_SOFTWAREVERSION      "Software-Version %s"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HARDWAREVERSION = 1;
//
#define MASK_HARDWAREVERSION      "Hardware-Version %s"
//
//-----------------------------------------------------------------------------------------
//
// Command-Parameter
#define SIZE_SERIALBUFFER 32
#define SIZE_RESPONSEBUFFER 32
#define SIZE_COMMANDBUFFER 64
#define COUNT_TEXTPARAMETERS 5
#define SIZE_TEXTPARAMETER   8
//
//-----------------------------------------------------------------------------------------

//
enum EStateCommand
{
  scError     = -2,
  scUndefined = -1,
  scInit      = 0,
  scIdle      = 1,
  scBusy      = 2
};
//
class CCommand
{
  private:
  EStateCommand FState;
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Int16 FRxdBufferIndex = 0;
  Character FCommandText[SIZE_RXDBUFFER];
  PCharacter FPCommand;
  PCharacter FPParameters[COUNT_TEXTPARAMETERS];
  Int16 FParameterCount = 0;

  public:
  CCommand(void);
  ~CCommand(void);
  //
  char* StateText(EStateCommand state);
  //
  EStateCommand GetState(void);
  void SetState(EStateCommand state);
  //
  Boolean Open(void);
  Boolean Close(void);
  //
  inline PCharacter GetRxdBuffer(void)
  {
    return FRxdBuffer;
  }
  inline PCharacter GetTxdBuffer(void)
  {
    return FTxdBuffer;
  }
  //
  inline PCharacter GetPCommand(void)
  {
    return FPCommand;
  }
  inline Byte GetParameterCount(void)
  {
    return FParameterCount;
  }
  inline PCharacter GetPParameters(UInt8 index)
  {
    return FPParameters[index];
  } 
  //
  void ZeroRxdBuffer(void);
  void ZeroTxdBuffer(void);
  void ZeroCommandText(void);
  //
  void AbortAll(void);
  //
  //  Segment - Execution - Common
  void WriteProgramHeader(CSerial &serial);
  void WriteSoftwareVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
  void WriteHelp(CSerial &serial);
  //
  //  Segment - Execution - Common
  void ExecuteGetHelp(CSerial &serial);
  void ExecuteGetProgramHeader(CSerial &serial);
  void ExecuteGetSoftwareVersion(CSerial &serial);
  void ExecuteGetHardwareVersion(CSerial &serial);
  //
#if defined(WATCHDOG_ISPLUGGED)
  void ExecutePulseWatchDog(CSerial &serial); 
#endif
  void ExecuteResetSystem(CSerial &serial); 
  void ExecuteAbortAll(CSerial &serial);
  void ExecuteWaitTimeRelative(CSerial &serial);
#if defined(NTPCLIENT_ISPLUGGED)
  void ExecuteWaitTimeAbsolute(CSerial &serial);
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
  void ExecuteWaitTriggerInput(CSerial &serial);
#endif  
  //
  //  Segment - Execution - LedSystem
  void ExecuteGetLedSystem(CSerial &serial);
  void ExecuteLedSystemOn(CSerial &serial);
  void ExecuteLedSystemOff(CSerial &serial);
  void ExecuteBlinkLedSystem(CSerial &serial);
#if defined(MULTICHANNELLED_ISPLUGGED)
  void ExecuteBlinkLedChannel1(CSerial &serial);
  void ExecuteBlinkLedChannel2(CSerial &serial);
  void ExecuteBlinkLedChannel3(CSerial &serial);
  void ExecuteBlinkLedChannel4(CSerial &serial);
  void ExecuteBlinkLedChannel5(CSerial &serial);
  void ExecuteBlinkLedChannel6(CSerial &serial);
  void ExecuteBlinkLedChannel7(CSerial &serial);
  void ExecuteBlinkLedChannel8(CSerial &serial);
#endif  
  //
  //  Segment - Execution - I2CDisplay
#if defined(I2CDISPLAY_ISPLUGGED)
  void ExecuteClearScreenI2CDisplay(CSerial &serial); 
  void ExecuteShowTextI2CDisplay(CSerial &serial);
#endif
  //
  //  Segment - Execution - NTPClient
#if defined(NTPCLIENT_ISPLUGGED)
  void ExecuteGetNTPClientTime(CSerial &serial);
  void ExecuteGetNTPClientDate(CSerial &serial);
#endif 
  //
  //  Segment - Execution - SDCard
#if defined(SDCARD_ISPLUGGED)
//!!!  void ExecuteOpenCommandFile(CSerial &serial);
//!!!  void ExecuteWriteCommandFile(CSerial &serial);
//!!!  void ExecuteCloseCommandFile(CSerial &serial);
  void ExecuteExecuteCommandFile(CSerial &serial);
  void ExecuteAbortCommandFile(CSerial &serial);
#endif
  //
  //  Segment - Execution - RFIDClient
#if defined(RFIDCLIENT_ISPLUGGED)
//  void ExecuteGetRFID(CSerial &serial);
#endif
  //  Segment - Execution - MotorVNH2SP30
#if defined(MOTORVNH2SP30_ISPLUGGED)
  void ExecuteGetLeftPwmLow(CSerial &serial);
  void ExecuteSetLeftPwmLow(CSerial &serial);
  void ExecuteGetLeftPwmHigh(CSerial &serial);
  void ExecuteSetLeftPwmHigh(CSerial &serial);
  //
  void ExecuteGetRightPwmLow(CSerial &serial);
  void ExecuteSetRightPwmLow(CSerial &serial);
  void ExecuteGetRightPwmHigh(CSerial &serial);
  void ExecuteSetRightPwmHigh(CSerial &serial);
  //
  void ExecuteStopBoth(CSerial &serial);
  void ExecuteStopLeft(CSerial &serial);
  void ExecuteStopRight(CSerial &serial);
  void ExecuteMoveLeftPositive(CSerial &serial);
  void ExecuteMoveLeftNegative(CSerial &serial);
  void ExecuteMoveRightPositive(CSerial &serial);
  void ExecuteMoveRightNegative(CSerial &serial);
  void ExecuteMoveBothPositive(CSerial &serial);
  void ExecuteMoveBothNegative(CSerial &serial);
  void ExecuteRotateBothPositive(CSerial &serial);
  void ExecuteRotateBothNegative(CSerial &serial);
  //
  void ExecuteMovePositionAbsolute(CSerial &serial);
  void ExecuteMovePositionRelative(CSerial &serial);
#endif
  // HELP_MOTORENCODERLM393
#if defined(MOTORENCODERLM393_ISPLUGGED)
  void ExecuteGetEncoderPositionBoth(CSerial &serial);
  void ExecuteSetEncoderPositionLeft(CSerial &serial);
  void ExecuteSetEncoderPositionRight(CSerial &serial);
  void ExecuteWaitPositionReached(CSerial &serial);
#endif
  // HELP_REMOTEWIRELESSSWITCH
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
  void ExecuteRemoteSwitchOn(CSerial &serial);
  void ExecuteRemoteSwitchOff(CSerial &serial);
#endif
// HELP_LASERSCANNER
#if defined(LASERSCANNER_JI)
  void ExecuteSetPulsePeriod(CSerial &serial);
  void ExecuteGetPulsePeriod(CSerial &serial);
  void ExecutePulseLaserAbort(CSerial &serial);
  void ExecutePulseLaserCount(CSerial &serial);
  void ExecuteMovePositionX(CSerial &serial);
  void ExecuteMovePositionY(CSerial &serial);
  void ExecuteMovePositionPulse(CSerial &serial);
#endif
#if defined(LASERSCANNER_PS)
  void ExecuteWaitTriggerIn(CSerial &serial);
#endif
  //
  // Main
  //
  Boolean AnalyseCommandText(CSerial &serial);
  //void WritePrompt(void);
  void WriteEvent(String line);
  void WriteResponse(String line);
  void WriteComment(String line);
  void WriteComment(String mask, String line);
  //  
  void DistributeCommand(void);
  //
  void Init();
#if defined(SERIALCOMMAND_ISPLUGGED) 
  Boolean DetectRxdLine(CSerial &serial);
#endif  
#if defined(SDCARD_ISPLUGGED) 
  Boolean DetectSDCardLine(CSDCard &sdcard);
#endif  
#if defined(MQTTCLIENT_ISPLUGGED) 
  Boolean DetectMQTTClientLine(CMQTTClient &mqttclient);
#endif  
  //
#if defined(SERIALCOMMAND_ISPLUGGED) 
  Boolean Handle(CSerial &serial);
  Boolean Execute(CSerial &serial);
#endif 
#if defined(SDCARD_ISPLUGGED)
  Boolean Handle(CSerial &serial, CSDCard &sdcard);
#endif  
#if defined(MQTTCLIENT_ISPLUGGED)
  Boolean Handle(CSerial &serial, CMQTTClient &mqttclient);
#endif 
};
//
#endif // Command_h
