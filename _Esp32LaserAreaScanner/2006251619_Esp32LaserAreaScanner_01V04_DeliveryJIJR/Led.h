//
//--------------------------------
//  Library Led
//--------------------------------
//
#ifndef Led_h
#define Led_h
//
#include "Arduino.h"
#include "Defines.h"
#include "Pinout.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const UInt32 INIT_PULSECOUNT    = 0;
const UInt32 INIT_PULSEPERIODMS = 1000; // [ms]
const UInt32 INIT_PULSEWIDTHMS  =  500; // [ms]
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateLed
{
  slUndefined = -1,
  slOff = 0,
  slOn = 1
};
//
enum EStateLedBlink
{
  slbZero  = 0,
  slbInit  = 1,
  slbHigh  = 2,
  slbLow   = 3
};
//
class CLed
{
  private:
  int FPin;
  Character FID;
  Boolean FInverted;
  EStateLed FState;
  char* FPBuffer;
  // Blink
  EStateLedBlink FStateBlink;
  UInt32 FPulseCountPreset;
  UInt32 FPulseCountActual;
  UInt32 FPulsePeriodms;
  UInt32 FPulseWidthms;
  UInt32 FTimeStartms;
  //
  public:
  CLed(int pin, char id);
  CLed(int pin, char id, bool inverted);
  //
  EStateLed GetState(void);
  Boolean IsInverted(void);
  //
  Boolean Open();
  Boolean Close();
  void SetOn();
  void SetOff();
  void Toggle();
  // 
  EStateLedBlink GetStateBlink(void);
  UInt32 GetPulsePeriodms(void);
  UInt32 GetPulseWidthms(void);
  UInt32 GetPulseCountPreset(void);
  UInt32 GetPulseCountActual(void);
  //
  void Blink_Start(UInt32 pulsecount, UInt32 pulseperiodms, UInt32 pulsewidthms);
  void Blink_Abort(void);
  void Blink_Execute(void);
};
//
#endif
