//
/*
BLS 1000 100 50
BLS 10 100 50
MPP 2000 2000 100 10 1000
MPP 2000 2000 100 100 0
*/
#include "Defines.h"
#include "Pinout.h"
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Automation.h"
#if defined(PININPUT_ISPLUGGED)
#include "PinInput.h"
#endif
#if defined(PINOUTPUT_ISPLUGGED)
#include "PinOutput.h"
#endif
#if defined(DACINTERNAL_ISPLUGGED)
#include "DacInternal.h"
#endif
#if defined(DACMCP4725_ISPLUGGED)
#include "DacMCP4725.h"
#endif
#if defined(DACMCP4725_ISPLUGGED)
#include "DacMCP4725.h"
#endif
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#if defined(RTCINTERNAL_ISPLUGGED)
#include "RTCInternal.h"
#endif
#include "TimeRelative.h"
#if defined(NTPCLIENT_ISPLUGGED)
#include "NTPClient.h"
#include "TimeAbsolute.h"
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
#include "TriggerInput.h"
#endif
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#include "XmlFile.h"
#include "CommandFile.h"
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
#include "I2CDisplay.h"
#include "Menu.h"
#endif
#if defined(RFIDCLIENT_ISPLUGGED)
#include "RFIDClient.h"
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
#include "MQTTClient.h"
#endif
#if defined(MOTORVNH2SP30_ISPLUGGED)
#include "MotorVNH2SP30.h"
#endif
#if defined(MOTORENCODERLM393_ISPLUGGED)
#include "MotorEncoderLM393.h"
#endif
#if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
#include "PositionReached.h";
#endif
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
#include "PinInput.h"
#include "PinOutput.h"
#include "RF433MhzClient.h"
#include "RemoteSwitch.h"
#endif
#if defined(LASERSCANNER_JI)
#include "LaserScannerJI.h"
#endif
#if defined(LASERSCANNER_PS)
#include "LaserScannerPS.h"
#endif
//
#include "IOPin.h"
#include "Serial.h"
#include "Led.h"
//
//###################################################
// Segment - Global Data - Assignment
//###################################################
// 
//------------------------------------------------------
// Segment - Global Variables 
//------------------------------------------------------
//
CError        Error;
CProcess      Process;
CCommand      Command;
#if defined(SDCARD_ISPLUGGED)
CCommandFile  CommandFile;
#endif
CAutomation   Automation;
//
String WifiSSID             = WIFI_SSID;
String WifiPASSWORD         = WIFI_PASSWORD;
String MQTTBrokerIPAddress  = MQTTBROKER_IPADDRESS;
int    MQTTBrokerIPPort     = MQTTBROKER_PORT;
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - Led
//-----------------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, 'S'); // Esp32Uno
//CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED); // Esp32small
//
#if defined(MULTICHANNELLED_ISPLUGGED)
CLed LedChannel1(PIN_LED_CHANNEL1, '1');
CLed LedChannel2(PIN_LED_CHANNEL2, '2');
CLed LedChannel3(PIN_LED_CHANNEL3, '3');
CLed LedChannel4(PIN_LED_CHANNEL4, '4');
CLed LedChannel5(PIN_LED_CHANNEL5, '5');
CLed LedChannel6(PIN_LED_CHANNEL6, '6');
CLed LedChannel7(PIN_LED_CHANNEL7, '7');
CLed LedChannel8(PIN_LED_CHANNEL8, '8');
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//-----------------------------------------------------------
//
#if defined(SERIALCOMMAND_ISPLUGGED)
CSerial SerialCommand(new HardwareSerial(SERIAL_PC), PIN_UART0_RXD, PIN_UART0_TXD);
#endif
//CSerial SerialXY(new HardwareSerial(SERIAL_XY));
//CSerial SerialZW(new HardwareSerial(SERIAL_ZW));
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - IOPin
//-----------------------------------------------------------
//
#if defined(WATCHDOG_ISPLUGGED)
CWatchDog WatchDog(PIN_23_A9_PWM_TOUCH);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - RTCInternal
//-----------------------------------------------------------
//
#if defined(RTCINTERNAL_ISPLUGGED)
CRTCInternal RTCInternal;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - Time / NTPClient
//-----------------------------------------------------------
#if defined(NTPCLIENT_ISPLUGGED)
WiFiUDP    UdpTime;
CNTPClient NTPClient(UdpTime, IPADDRESS_NTPSERVER);
#endif
//
CTimeRelative TimeRelativeSystem("TRSM", INIT_MESSAGE_ON);
#if defined(NTPCLIENT_ISPLUGGED)
CTimeAbsolute TimeAbsoluteSystem("TAS", INIT_MESSAGE_ON);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - TriggerInput
//-----------------------------------------------------------
#if defined(TRIGGERINPUT_ISPLUGGED)
CTriggerInput TriggerInputSystem("S", PIN_TRIGGERINPUT_SIGNAL);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - I2CDisplay
//-----------------------------------------------------------
//
#if defined(I2CDISPLAY_ISPLUGGED)
CI2CDisplay I2CDisplay(I2CADDRESS_I2CLCDISPLAY, 
                       I2CLCDISPLAY_COLCOUNT, 
                       I2CLCDISPLAY_ROWCOUNT);
//
CMenu MenuSystem("MS");                       
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - SDCARD
//-----------------------------------------------------------
//
#if defined(SDCARD_ISPLUGGED)
CSDCard SDCard(PIN_SPIV_CS_SDCARD);
CXmlFile XmlFile;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - RFIDClient
//-----------------------------------------------------------
//
#if defined(RFIDCLIENT_ISPLUGGED)
CRFIDClient RFIDClient(PIN_SPI_SS, PIN_SPI_RST);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - MQTTClient
//-----------------------------------------------------------
#if defined(MQTTCLIENT_ISPLUGGED)
CMQTTClient MQTTClient;
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - MotorVNH2SP30
//-----------------------------------------------------------
#if defined(MOTORVNH2SP30_ISPLUGGED)
CMotorVNH2SP30 MotorLeft(PWMCHANNEL_MOTORLEFT, PWMRESOLUTION_MOTORLEFT,
                         PWMLOW_MOTORLEFT, PWMHIGH_MOTORLEFT,
                         PWMFREQUENCY_MOTORLEFT, PIN_MOTORLEFT_INA, 
                         PIN_MOTORLEFT_INB, PIN_MOTORLEFT_PWM);
CMotorVNH2SP30 MotorRight(PWMCHANNEL_MOTORRIGHT, PWMRESOLUTION_MOTORRIGHT,
                          PWMLOW_MOTORRIGHT, PWMHIGH_MOTORRIGHT,
                          PWMFREQUENCY_MOTORRIGHT, PIN_MOTORRIGHT_INA, 
                          PIN_MOTORRIGHT_INB, PIN_MOTORRIGHT_PWM);
#endif
//
//-------------------------------------------------------------
// Segment - Global Variables - Assignment - MotorEncoderLM393
//-------------------------------------------------------------
#if defined(MOTORENCODERLM393_ISPLUGGED)
CMotorEncoderLM393  MotorEncoderLM393Left(PIN_MOTORENCODERLM393_PULSEA_LEFT,
                                          ISRMotorEncoderLM393Left);
CMotorEncoderLM393  MotorEncoderLM393Right(PIN_MOTORENCODERLM393_PULSEA_RIGHT,
                                           ISRMotorEncoderLM393Right);
#endif
//
//-------------------------------------------------------------
// Segment - Global Variables - Assignment - PositionReached
//-------------------------------------------------------------
#if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
CPositionReached MotorPositionReached('M');
#endif
//
//------------------------------------------------------
// Segment - IRQ - Callback - MotorEncoderLM393
//------------------------------------------------------
//
// IRQ - MotorEncoderLM393 - Left
//
#if defined(MOTORENCODERLM393_ISPLUGGED)
void ISRMotorEncoderLM393Left(void)
{
  // debug ok LedSystem.SetOn();
  // debug ok SerialCommand.Write("L0");
  MotorEncoderLM393Left.HandleInterrupt();
}
#endif
//
// IRQ - MotorEncoderLM393 - Right
//
#if defined(MOTORENCODERLM393_ISPLUGGED)
void ISRMotorEncoderLM393Right(void)
{
  // debug ok LedSystem.SetOff();
  // debug ok SerialCommand.Write("R0");
  MotorEncoderLM393Right.HandleInterrupt();
}
#endif
//
//-------------------------------------------------------------------------------
// Segment - Global Variables - Assignment - RF433MHzClient, RemoteWirelessSwitch
//-------------------------------------------------------------------------------
//
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
#if define(PININPUT_ISPLUGGED)
CPinInput       SignalA0("LL", PIN_GROUPA_A0, LEVEL_INVERTED);
CPinInput       SignalA1("SP", PIN_GROUPA_A1, LEVEL_INVERTED);
CPinInput       SignalA2("IE", PIN_GROUPA_A2, LEVEL_INVERTED);
CPinInput       SignalB0("SC", PIN_GROUPB_B0);
CPinInput       SignalB1("SE", PIN_GROUPB_B1);
CPinInput       SignalB2("SO", PIN_GROUPB_B2);
#endif
#if define(RF433MHZCLIENT_ISPLUGGED)
CRF433MHzClient RF433MHzTransmit("WT", PIN_RF433MHZ_TRANSMIT);
#endif
CRemoteSwitch   RemoteSwitch("RS");  
#endif
//
//-------------------------------------------------------------------------------
// Segment - Global Variables - Assignment - LaserScanner
//-------------------------------------------------------------------------------
//
#if defined(LASERSCANNER_JI)
#if defined(PINOUTPUT_ISPLUGGED)
CPinOutput      LaserTrigger("LT", PIN_LASERTRIGGER, polLow);
#endif
#if defined(DACMCP4725_ISPLUGGED)
CDacMCP4725     PositionX("PX", I2CADDRESS_POSITIONX, INIT_DAC_HALF);
CDacMCP4725     PositionY("PY", I2CADDRESS_POSITIONY, INIT_DAC_HALF);
#endif
CLaserScanner   LaserScanner("LSJI", INIT_MESSAGE_OFF);  
#endif
#if defined(LASERSCANNER_PS)
CLaserScanner   LaserScanner("LSPS");  
#endif
//
//------------------------------------------------------
// Segment - SETUP
//------------------------------------------------------
//
void setup() 
{ //
  //----------------------------------------------------
  // Device - IOPin
  //----------------------------------------------------
#if defined(WATCHDOG_ISPLUGGED)
  WatchDog.Open(WATCHDOG_COUNTERPRESET, WATCHDOG_PRESETHIGH);
#endif
  //----------------------------------------------------
  // Device - Led - LedSystem
  //----------------------------------------------------
  LedSystem.Open();
// Startpulses for Laser eleminated!
//  for (int BI = 0; BI < 10; BI++)
//  {
//    LedSystem.SetOff();
//    delay(100);
//    LedSystem.SetOn();
//    delay(30);
//  }
  LedSystem.SetOff();
  // From here LedSystem(Pin13)-Conflict with SPI-SCK!!!
  //
#if defined(MULTICHANNELLED_ISPLUGGED)
  LedChannel1.Open();
  LedChannel2.Open();
  LedChannel3.Open();
  LedChannel4.Open();
  LedChannel5.Open();
  LedChannel6.Open();
  LedChannel7.Open();
  LedChannel8.Open();  
  for (int BI = 0; BI < 10; BI++)
  {
    LedChannel1.SetOff();
    LedChannel2.SetOn();
    LedChannel3.SetOff();
    LedChannel4.SetOn();
    LedChannel5.SetOff();
    LedChannel6.SetOn();
    LedChannel7.SetOff();
    LedChannel8.SetOn();
    delay(100);
    LedChannel1.SetOn();
    LedChannel2.SetOff();
    LedChannel3.SetOn();
    LedChannel4.SetOff();
    LedChannel5.SetOn();
    LedChannel6.SetOff();
    LedChannel7.SetOn();
    LedChannel8.SetOff();
    delay(30);
  }
  LedChannel1.SetOff();
  LedChannel2.SetOff();
  LedChannel3.SetOff();
  LedChannel4.SetOff();
  LedChannel5.SetOff();
  LedChannel6.SetOff();
  LedChannel7.SetOff();
  LedChannel8.SetOff();
#endif  
  //  
  //----------------------------------------------------
  // Device - Serial
  //----------------------------------------------------
#if defined(SERIALCOMMAND_ISPLUGGED)
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_OFF);
  delay(300);
  SerialCommand.WriteLine("\r\n# Serial-Connection: [ESP32-SerialCommand]");
#endif
  //
//  SerialXY.Open(115200, PIN_UART1_RXD, PIN_UART1_TXD);
//  SerialXY.SetRxdEcho(RXDECHO_ON);
//  SerialXY.WriteLine("# SerialConnection: [ESP32Dispatcher]-[STM32AxisXY]");
//  //
//  SerialZW.Open(115200, PIN_UART2_RXD, PIN_UART2_TXD);
//  SerialZW.SetRxdEcho(RXDECHO_ON);
//  SerialZW.WriteLine("# SerialConnection: [ESP32Dispatcher]-[STM32AxisZW]");
  //
  //----------------------------------------------------
  // Device - RTCInternal
  //----------------------------------------------------
#if defined(RTCINTERNAL_ISPLUGGED)
  RTCInternal.Open();
#endif
  //
  //---------------------------------------------------------
  // Device - MQTTClient / NTPClient - TimeRelative/Absolute
  //---------------------------------------------------------
#if defined(NTPCLIENT_ISPLUGGED)
  NTPClient.OpenWifi(); 
  NTPClient.Open();
  NTPClient.Update();
  NTPClient.CloseWifi();
  //
  TimeAbsoluteSystem.Open();  
#endif
  // NC TimeRelativeSystemus.Open();  
  TimeRelativeSystem.Open();  
  //
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.Initialise(WifiSSID.c_str(), WifiPASSWORD.c_str());
  MQTTClient.Open(MQTTBrokerIPAddress.c_str(), MQTTBrokerIPPort); 
#endif 
  //
  //---------------------------------------------------------
  // Device - TriggerInput
  //---------------------------------------------------------
#if defined(TRIGGERINPUT_ISPLUGGED)
  TriggerInputSystem.Open();
#endif
  // 
  //----------------------------------------------------
  // Device - SDCARD
  //----------------------------------------------------
#if defined(SDCARD_ISPLUGGED)
  SerialCommand.WriteLine("# Initializing SDCARD ...");    
  while (SDCard.Open(&SD) < 1)
  {
    SerialCommand.WriteLine(": Initializing SDCARD failed - Insert correct SDCARD[FAT32, initfile]!");
    delay(5000);
  }
  SerialCommand.WriteLine("# Initializing SD card done.");
  //
  Boolean Result = XmlFile.Open(&SD);
  if (!Result) 
  {
    SerialCommand.WriteLine(": Error: Initfile Not Found!");
    return;
  }
  //  
  SerialCommand.Write("# Reading Initfile: ");
  SerialCommand.WriteLine(XML_INITFILE);
  Result = XmlFile.ReadFile(XML_INITFILE);
  if (!Result) 
  {
    SerialCommand.WriteLine(": Error: Reading File!");
    return;
  }
  // SerialCommand.WriteLine("# Content XmlFile:");
  // Serial.println(XmlFile.GetText());
  //
  SerialCommand.WriteLine("# Parsing Initfile");
  Result = XmlFile.Parse();
  if (!Result) 
  {
    SerialCommand.WriteLine(": Error: Parsing Xml!");
    return;
  }
  //
  SerialCommand.WriteLine("# Result Xml Query:");
  XMLNode* XmlRoot = XmlFile.GetRoot();
  if (!XmlRoot) return;
  //----------------------------------------------------------------------
  XMLElement* XmlWifi = XmlFile.GetElement(XmlRoot, "wifi");
  if (!XmlWifi) return;
  //
  XMLElement* XmlSSID = XmlFile.GetElement(XmlWifi, "ssid");
  if (!XmlSSID) return;
  SerialCommand.Write("# WifiSSID[");
  SerialCommand.Write(XmlSSID->GetText());
  SerialCommand.WriteLine("]");
  WifiSSID = XmlSSID->GetText();
  //
  XMLElement* XmlPASSWORD = XmlFile.GetElement(XmlWifi, "password");
  if (!XmlPASSWORD) return;
  SerialCommand.Write("# WifiPASSWORD[");
  SerialCommand.Write(XmlPASSWORD->GetText());
  SerialCommand.WriteLine("]");
  WifiPASSWORD = XmlPASSWORD->GetText();
  //----------------------------------------------------------------------
  XMLElement* XmlMQTTBroker = XmlFile.GetElement(XmlRoot, "mqttbroker");
  if (!XmlMQTTBroker) return;
  //
  XMLElement* XmlIPAddress = XmlFile.GetElement(XmlMQTTBroker, "ipaddress");
  if (!XmlIPAddress) return;
  SerialCommand.Write("# BrokerIPAddress[");
  SerialCommand.Write(XmlIPAddress->GetText());
  SerialCommand.WriteLine("]");
  MQTTBrokerIPAddress = XmlIPAddress->GetText();
  //
  XMLElement* XmlIPPort = XmlFile.GetElement(XmlMQTTBroker, "ipport");
  if (!XmlIPPort) return;
  SerialCommand.Write("# BrokerIPPort[");
  SerialCommand.Write(XmlIPPort->GetText());
  SerialCommand.WriteLine("]");
  MQTTBrokerIPPort = atoi(XmlIPPort->GetText());
  //----------------------------------------------------------------------
  // !!! Free Ram !!!
  XmlFile.Close();
  //----------------------------------------------------------------------
  // !!! !!! !!! !!! !!! !!!
  CommandFile.Open(&SD);
  // !!! !!! !!! !!! !!! !!!
  if (!CommandFile.ParseFile(INIT_COMMANDFILE))
  {
    SerialCommand.WriteLine(": Error: Cannot Read Commandfile!");
    return;
  }
  SerialCommand.Write("# CommandFile[");
  SerialCommand.Write(INIT_COMMANDFILE);
  SerialCommand.WriteLine("]:");
  bool CommandLoop = true;
  while (CommandLoop)
  {
    String Command = CommandFile.GetCommand();
    CommandLoop = (0 < Command.length());
    if (CommandLoop)
    {
      SerialCommand.Write("# Command[");
      SerialCommand.Write(Command.c_str());
      SerialCommand.WriteLine("]");
    }
  }
  CommandFile.Close();  
#endif  
  //
  //----------------------------------------------------
  // Device - I2CDisplay
  //----------------------------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
  I2CDisplay.Open();
  I2CDisplay.ClearDisplay();
  I2CDisplay.SetBacklightOn();
  //
  MenuSystem.Open();
#endif
  //
  //----------------------------------------------------
  // Device - RFIDClient
  //----------------------------------------------------
#if defined(RFIDCLIENT_ISPLUGGED)
  RFIDClient.Open();
#endif
  //
  //----------------------------------------------------
  // Device - MotorVNH2SP30
  //----------------------------------------------------
#if defined(MOTORVNH2SP30_ISPLUGGED)
  MotorLeft.Open();
  MotorRight.Open();
#endif
  //
  //----------------------------------------------------
  // Device - MotorEncoderLM393
  //----------------------------------------------------
#if defined(MOTORENCODERLM393_ISPLUGGED)
  MotorEncoderLM393Left.Open();
  MotorEncoderLM393Right.Open();
#endif
  //
  //----------------------------------------------------
  // Device - PositionReached
  //----------------------------------------------------
#if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
  MotorPositionReached.Open();
#endif  
  //
  //----------------------------------------------------
  // Device - RemoteWirelessSwitch
  //----------------------------------------------------
#if defined(REMOTEWIRELESSSWITCH_ISPLUGGED)
  SignalA0.Open();
  SignalA1.Open();
  SignalA2.Open();
  SignalB0.Open();
  SignalB1.Open();
  SignalB2.Open();
  RF433MHzTransmit.Open();
  RemoteSwitch.Open();
#endif
  //
  //----------------------------------------------------
  // Device - LaserScanner
  //----------------------------------------------------
#if defined(LASERSCANNER_JI)
  #if defined(PINOUTPUT_ISPLUGGED)
  LaserTrigger.Open();
  #endif
  #if defined(DACMCP4725_ISPLUGGED)
  PositionX.Open(); 
  PositionY.Open();
  #endif
  LaserScanner.Open();  
#endif
#if defined(LASERSCANNER_PS)
  LaserScanner.Open();  
#endif
  //
  //####################################################
  //
  //----------------------------------------------------
  // Device - Command
  //----------------------------------------------------
  Command.Open();
  Command.WriteProgramHeader(SerialCommand);
  Command.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //----------------------------------------------------
  // Device - Process 
  //----------------------------------------------------
  Process.Open();
  //!!!! MENU Process.SetState(spWelcome);
  //----------------------------------------------------
  // Device - Automation
  //----------------------------------------------------
  Automation.Open();
}
//
//------------------------------------------------------
// Segment - LOOP
//------------------------------------------------------
//

UInt32 LX = 0;
UInt32 LY = 0;

void loop() 
{ 
#if defined(SERIALCOMMAND_ISPLUGGED)
  if (Error.Handle(SerialCommand))
  {
    Command.SetState(scIdle);
    //SerialCommand.WritePrompt();
  }
#if defined(MQTTCLIENT_ISPLUGGED)
  Command.Handle(SerialCommand, MQTTClient);
#endif // MQTTCLIENT_ISPLUGGED
#if defined(SDCARD_ISPLUGGED)
  Command.Handle(SerialCommand, SDCard);
#endif // SDCARD_ISPLUGGED
  Command.Handle(SerialCommand);
  Process.Handle(SerialCommand); 
  Automation.Handle(SerialCommand); 
#endif // SERIALCOMMAND_ISPLUGGED
  // Preemtive Multitasking:
  //------------------------------------------------------------------------
  // NC TimeRelativeSystemus.Wait_Execute();
  TimeRelativeSystem.Wait_Execute();
#if defined(NTP_ISPLUGGED)  
  TimeAbsoluteSystem.Wait_Execute();
#endif
#if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
  MotorPositionReached.Wait_Execute();
#endif  
#if defined(TRIGGERINPUT_ISPLUGGED)  
  TriggerInputSystem.Wait_Execute();
#endif  
//  //------------------------------------------------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
  MenuSystem.Display_Execute();
#endif
  //
  LedSystem.Blink_Execute();  
#if defined(MULTICHANNELLED_ISPLUGGED)  
  LedChannel1.Blink_Execute();
  LedChannel2.Blink_Execute();
  LedChannel3.Blink_Execute();
  LedChannel4.Blink_Execute();
  LedChannel5.Blink_Execute();
  LedChannel6.Blink_Execute();
  LedChannel7.Blink_Execute();
  LedChannel8.Blink_Execute();
#endif  
  //
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // !!!! 100ms !!!!!!!!!!!!!!!!!!!!
  RemoteSwitch.Execute();
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif  
  //
#if defined(LASERSCANNER_JI)
  LaserScanner.Execute();
#endif
#if defined(LASERSCANNER_PS)
  LaserScanner.Execute();
#endif
  //
}

/*
MPP 2000 2000 100 100 0
MPP 2000 2000 100 10 1000
MPP 2000 2000 100 10 4000
 *
 */
