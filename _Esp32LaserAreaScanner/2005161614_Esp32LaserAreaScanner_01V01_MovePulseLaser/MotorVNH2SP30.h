#include "Defines.h"
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
//
//--------------------------------
//  Library MotorVNH2SP30 MPA X 2000
//--------------------------------
//
#ifndef MotorVNH2SP30_h
#define MotorVNH2SP30_h
//
#include "Arduino.h"
#include "Pinout.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const UInt16 PWM_ZERO            =     0;  // [1]
//
const Float32 VELOCITY_LOW       =   0.0f; // [%]
const Float32 VELOCITY_HIGH      = 100.0f; // [%]
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateMotorVNH2SP30
{
  smdMoveNegative = -1,
  smdIdle = 0,
  smdMovePositive = 1
};
//
//--------------------------------
//  Section - CMotorVNH2SP30
//--------------------------------
//
class CMotorVNH2SP30
{
  private:
  EStateMotorVNH2SP30 FState;
  Byte FPwmChannel;
  UInt32 FPwmFrequency;
  Byte FPwmResolution;
  int FPinINA;
  int FPinINB;
  int FPinPWM;
  UInt16 FPwmHigh, FPwmLow;
  //  
  void SetState(EStateMotorVNH2SP30 state);
  //
  public:
  CMotorVNH2SP30(Byte pwmchannel, Byte pwmresolution,
                 UInt16 pwmlow, UInt16 pwmhigh,
                 UInt32 pwmfrequency, int pinina, 
                 int pininb, int pinpwm);
  //                              
  EStateMotorVNH2SP30 GetState();
  //
  Boolean Open();
  Boolean Close();
  //  
  UInt16 GetPwmLow(void);             // [1]
  void SetPwmLow(UInt16 pwm);
  UInt16 GetPwmHigh(void);
  void SetPwmHigh(UInt16 pwm);
  //
  void SetPwm(UInt16 pwm);            // [1]
  void SetVelocity(Float32 velocity); // [%]
  //
  void AbortMotion(); 
  void MoveVelocityPositive(Float32 velocity);
  void MoveVelocityNegative(Float32 velocity);
};
//
#endif // MotorVNH2SP30_h
//
#endif // MOTORVNH2SP30_ISPLUGGED
//
