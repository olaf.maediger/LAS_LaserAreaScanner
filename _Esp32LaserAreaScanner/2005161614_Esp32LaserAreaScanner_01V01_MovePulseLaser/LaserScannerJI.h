//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "Defines.h"
//
#if defined(LASERSCANNER_JI)
//
#ifndef LaserScanner_h
#define LaserScanner_h
//
#include "TimeRelative.h"
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateLaserScanner
{
  slsZero                 = 0,
  // FSM PulseLaser
  slsInitPulseLaser       = 1,
  slsBusyPulseLaserHigh   = 2,
  slsBusyPulseLaserLow    = 3,
  // FSM MovePulse
  slsInitMovePulse        = 4,
  slsBusyMovePulseTarget  = 5,
  slsBusyMovePulseHigh    = 6,
  slsBusyMovePulseLow     = 7
};
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const Float32 INIT_PULSEPERIOD_MS = 1000; // [ms]
//
//
//--------------------------------
//  Section - CLaserScanner
//--------------------------------
//
class CLaserScanner
{
  private:
  String FID;
  EStateLaserScanner FState;
  Boolean FMessageOn;
  UInt32 FPulseCountActual;
  UInt32 FPulseCountPreset;
  Float32 FPulsePeriodms;
  Float32 FDelayMotionms;
  CTimeRelative* FPTimeRelative;
  UInt16 FPositionX, FPositionY;
  //
  public:
  // Init
  CLaserScanner(String id, Boolean messageon);
  Boolean Open(void);
  Boolean Close(void);
  // Property
  EStateLaserScanner GetState(void);
  Boolean IsBusy(void);
  void SetPulsePeriodms(Float32 periodms);
  UInt32 GetPulsePeriodms(void);
  // Helper
  PCharacter StateText(EStateLaserScanner state);
  void SetState(EStateLaserScanner state);
  // Handler
  Boolean PulseLaserCount(Float32 periodms, UInt32 count);
  Boolean MovePositionX(UInt16 px);
  Boolean MovePositionY(UInt16 py);
  Boolean MovePositionPulse(UInt16 px, UInt16 py, Float32 ppms, UInt32 pc, UInt32 dm);
  //  
  void Abort(void);
  void Execute(void);
  //
  //
  //
};
//
#endif // LaserScanner_h
//
#endif // LASERSCANNER_JI
//
