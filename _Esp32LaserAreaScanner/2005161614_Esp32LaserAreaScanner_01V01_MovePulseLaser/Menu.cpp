//
//--------------------------------
//  Library Menu
//--------------------------------
//
#include "Defines.h"
//
#if defined(I2CDISPLAY_ISPLUGGED)
//
#include "Menu.h"
#include "Command.h"
//
extern CCommand Command;
//
CMenu::CMenu(String id)
{
  FID = id;
  FState = smZero;
}

EStateMenu CMenu::GetState(void)
{
  return FState;
}  

Boolean CMenu::Open()
{
  FState = smZero;
  return true;
}

Boolean CMenu::Close()
{
  FState = smZero;
  return true;
}
//
//-------------------------------------------------------------------------------
//  Menu - Process 
//-------------------------------------------------------------------------------
//
void CMenu::Display_Start(void)
{
  FState = smInit;
}

void CMenu::Display_Abort(void)
{
  FState = smZero;
}

void CMenu::Display_Execute(void)
{
  switch (FState)
  {
    case smZero:
      break;
    case smInit:
      char Buffer[32];
      sprintf(Buffer, "SMN %s 1", FID);
      Command.WriteEvent(Buffer);
      FState = smBusy;
      break;
    case smBusy:
      if (true)//FPulseWidthms < (millis() - FTimeStartms))
      {
        char Buffer[32];
        sprintf(Buffer, "SMN %s 0", FID);
        Command.WriteEvent(Buffer);
        FState = smZero;
      }
      break;
  }
}
//
#endif // I2CDISPLAY_ISPLUGGED
//
////
////----------------------------------------------------------
////  Segment - Helper - I2CDisplay
////----------------------------------------------------------
////
//void I2CDisplay_ShowWelcome()
//{ //                   "01234567890123456789"
////  I2CDisplay.SetCursorPosition(0, 0);
////  I2CDisplay.WriteText("* Esp32LGBController");
////  I2CDisplay.SetCursorPosition(1, 0);
////  I2CDisplay.WriteText("Version 01.xx");
////  I2CDisplay.SetCursorPosition(2, 0);
////  I2CDisplay.WriteText("openhardsoftware.de ");
////  I2CDisplay.SetCursorPosition(3, 0);
////  I2CDisplay.WriteText("D-37191 Lindau");
//}
//#endif
//
////#if defined(I2CDISPLAY_ISPLUGGED)
////void I2CDisplay_ShowVoltageFotoResistor()
////{
////  //!!!!!!!!!!!!char Buffer[6];
////  //!!!!!!!!!!!!AdcInternal.ConvertValue(CHANNEL_FOTORESISTOR);
////  //!!!!!!!!!!!!sprintf(Buffer, "%04d", AdcInternal.GetValue(CHANNEL_FOTORESISTOR));
////  //!!!!!!!!!!!!I2CDisplay.SetCursorPosition(1, 4);  
////  //!!!!!!!!!!!!I2CDisplay.WriteText(Buffer);
////}
////#endif
//
////#if defined(I2CDISPLAY_ISPLUGGED)
////void I2CDisplay_ShowVoltageThermalResistor()
////{
////  //!!!!!!!!!!!!char Buffer[6];
////  //!!!!!!!!!!!!AdcInternal.ConvertValue(CHANNEL_THERMALRESISTOR);
////  //!!!!!!!!!!!!sprintf(Buffer, "%04d", AdcInternal.GetValue(CHANNEL_THERMALRESISTOR));
////  //!!!!!!!!!!!!I2CDisplay.SetCursorPosition(1, 12);  
////  //!!!!!!!!!!!!I2CDisplay.WriteText(Buffer);
////}
////#endif
//
////#if defined(I2CDISPLAY_ISPLUGGED)
////void I2CDisplay_ShowPressureVacuumChamber()
////{
////  //!!!!!!!!!!!!char Buffer[6];
////  //!!!!!!!!!!!!AdcInternal.ConvertValue(CHANNEL_PRESSUREVACUUMCHAMBER);
////  //!!!!!!!!!!!!sprintf(Buffer, "%04d", AdcInternal.GetValue(CHANNEL_PRESSUREVACUUMCHAMBER));
////  //!!!!!!!!!!!!I2CDisplay.SetCursorPosition(2, 4);
////  //!!!!!!!!!!!!I2CDisplay.WriteText(Buffer);
////}
////#endif
//
////#if defined(I2CDISPLAY_ISPLUGGED)
////void I2CDisplay_ShowStateThermalLight()
////{
////  I2CDisplay.SetCursorPosition(3, 4);
//////  if (tlActive == ThermalLight.GetLevel())
//////  {
//////    I2CDisplay.WriteText("X");
//////  }
//////  else
//////  {
//////    I2CDisplay.WriteText("_");   
//////  }
////}
////#endif
//
////#if defined(I2CDISPLAY_ISPLUGGED)
////void I2CDisplay_ShowStateCoolerFotoResistor()
////{
////  I2CDisplay.SetCursorPosition(3, 18);
//////  if (tlActive == CoolerFotoResistor.GetLevel())
//////  {
//////    I2CDisplay.WriteText("X");
//////  }
//////  else
//////  {
//////    I2CDisplay.WriteText("_");   
//////  }
////}
////#endif
//
//#if defined(I2CDISPLAY_ISPLUGGED)
//void I2CDisplay_ShowSystemState()
//{ //                   "01234567890123456789"
////  I2CDisplay.ClearDisplay();
////  I2CDisplay.SetCursorPosition(0, 0);
////  I2CDisplay.WriteText("MVE: Slit Movement  ");
////  // X
////  I2CDisplay.SetCursorPosition(1, 0);
////  I2CDisplay.WriteText("PX[");
////  I2CDisplay.WriteText("]");
////  I2CDisplay.SetCursorPosition(1, 9);
////  I2CDisplay.WriteText("DX[");
////  I2CDisplay.WriteText("]");
////  // Y
////  I2CDisplay.SetCursorPosition(2, 0);
////  I2CDisplay.WriteText("VX[");
////  I2CDisplay.WriteText("]");
////  I2CDisplay.SetCursorPosition(2, 9);
////  I2CDisplay.WriteText("PTY[");
////  I2CDisplay.WriteText("]");
//}
//#endif
////  I2CDisplay_ShowVoltageThermalResistor();
////  I2CDisplay_ShowVoltageFotoResistor();
////  I2CDisplay.WriteText("]VTR");
////  I2CDisplay.SetCursorPosition(2, 0);
////  I2CDisplay.WriteText("PVC[");
////  I2CDisplay_ShowPressureVacuumChamber();
////  I2CDisplay.WriteText("]mbar       ");
////  //  
////  I2CDisplay.SetCursorPosition(3, 0);
////  I2CDisplay.WriteText("STL[");
////  I2CDisplay_ShowStateThermalLight();
////  I2CDisplay.WriteText("]       SCFR[");
////  I2CDisplay_ShowStateCoolerFotoResistor();
////  I2CDisplay.WriteText("]");  
////}
////
//////----------------------------------------------------------
//////  Segment - Common
//////----------------------------------------------------------
//////
////void CProcess::HandleUndefined(CSerial &serial)
////{
////  delay(1000);
////}
////
////void CProcess::HandleIdle(CSerial &serial)
////{ // do nothing
////  SetTimeStamp();
////}
////
////void CProcess::HandleWelcome(CSerial &serial)
////{
////  SetTimeMark();
////  switch (FStateTick)
////  {
////    case 0:        
////#if defined(I2CDISPLAY_ISPLUGGED)
////      I2CDisplay_ShowWelcome();
////      I2CDisplay.SetBacklightOn();
////#endif
////      FStateTick++;
//// FStateTick = 999; // direct -> Idle
////      break;
////    case 1: 
////      if (2000 < GetTimeSpanms())
////      {
////        SetTimeStamp();
////        FStateTick++;
////      }
////      break;
////    case 2: case 4: case 6:  
////      if (900 < GetTimeSpanms())
////      {
////#if defined(I2CDISPLAY_ISPLUGGED)
////        I2CDisplay.SetBacklightOff();
////#endif
////        SetTimeStamp();
////        FStateTick++;        
////      }
////      break;
////    case 3: case 5: case 7:
////      if (100 < GetTimeSpanms())
////      {
////#if defined(I2CDISPLAY_ISPLUGGED)
////        I2CDisplay.SetBacklightOn();
////#endif
////        SetTimeStamp();
////        FStateTick++;        
////      }
////      break;
////    case 8:  
////      if (3000 < GetTimeSpanms())
////      {   
////#if defined(I2CDISPLAY_ISPLUGGED)
////        I2CDisplay_ShowSystemState();
////#endif
////        FStateTick++;
////        SetState(spIdle);
////        SetTimeStamp();
////      }
////      break;
////    default:
////      SetState(spIdle);
////      SetTimeStamp();
////      break;    
////  }
////}
