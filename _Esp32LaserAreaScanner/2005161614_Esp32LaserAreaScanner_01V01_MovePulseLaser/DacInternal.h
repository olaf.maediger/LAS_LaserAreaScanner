//
//--------------------------------
//  Library DacInternal
//--------------------------------
//
#include "Defines.h"
//
#if defined(DACINTERNAL_ISPLUGGED)
//
#ifndef DacInternal_h
#define DacInternal_h
//
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const UInt16 INIT_DAC_ZERO =    0;
const UInt16 INIT_DAC_HALF = 2047;
const UInt16 INIT_DAC_FULL = 4095;
//
//--------------------------------
//  Section - Type
//--------------------------------
//
class CDacInternal
{
  private:
  String FID;
  UInt8  FPin;
  UInt16 FLevel;
  //
  public:
  // Init
  CDacInternal(String id, UInt8 pin, UInt16 initlevel);
  Boolean Open();
  Boolean Close();
  // Property
  UInt16 GetLevel(void);
  void WriteLevel(UInt16 level);
  // Handler
  void Execute(void); // NC
};
//
#endif // DacInternal_h
//
#endif // DACINTERNAL_ISPLUGGED
//
