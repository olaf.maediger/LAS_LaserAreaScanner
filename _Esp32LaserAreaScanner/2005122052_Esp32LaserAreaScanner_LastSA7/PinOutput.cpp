//
//--------------------------------
//  Library PinOutput
//--------------------------------
//
#include "Defines.h"
//
#include "PinOutput.h"
//#include "Command.h"
//
//extern CCommand Command;
//
CPinOutput::CPinOutput(String id, UInt8 pin, EPinOutputLevel level)
{
  FID = id;
  FPin = pin;
  FLevel = polLow;
}

EPinOutputLevel CPinOutput::GetLevel(void)
{
  return FLevel;
}  

void CPinOutput::WriteLevel(EPinOutputLevel level)
{
  FLevel = level;
  digitalWrite(FPin, FLevel);
}  

Boolean CPinOutput::Open()
{
  pinMode(FPin, OUTPUT);
  FLevel = polLow;
  digitalWrite(FPin, FLevel);
  return true;
}

Boolean CPinOutput::Close()
{
  pinMode(FPin, INPUT_PULLUP);
  FLevel = polLow;
  return true;
}
//
//-------------------------------------------------------------------------------
//  PinOutput - Execute
//-------------------------------------------------------------------------------
EPinOutputLevel CPinOutput::Execute(void)
{
  return FLevel;
}
//
