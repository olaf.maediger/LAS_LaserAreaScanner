//
//--------------------------------
//  Library Menu
//--------------------------------
//
#include "Defines.h"
//
#if defined(I2CDISPLAY_ISPLUGGED)
//
#ifndef Menu_h
#define Menu_h
//
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
//const UInt32 INIT_PULSEPERIODMS = 1000; // [ms]
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateMenu
{
  smZero = 0,
  smInit = 1,
  smBusy = 2
};
//
class CMenu
{
  private:
  String FID;
  EStateMenu FState;
  //
  public:
  CMenu(String id);
  //
  EStateMenu GetState(void);
  //
  Boolean Open();
  Boolean Close();
  // 
  void Display_Start(void);
  void Display_Abort(void);
  void Display_Execute(void);
};
//
#endif // Menu_h
//
#endif // I2CDISPLAY_ISPLUGGED
//
