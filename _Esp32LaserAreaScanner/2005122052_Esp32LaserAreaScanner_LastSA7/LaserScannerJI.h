//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "Defines.h"
//
#if defined(LASERSCANNER_JI)
//
#ifndef LaserScanner_h
#define LaserScanner_h
//
#include "Arduino.h"
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateLaserScanner
{
  slsZero             = 0,
  slsInitPulseLaser   = 1,
  slsInitMovePulse    = 2,
  slsBusyPulseLaser   = 3,
  slsBusyMovePulse    = 4
};
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
//const EStateForceSwitch INIT_STATEFORCESWITCH = sfsPassive;
//
//
//--------------------------------
//  Section - CLaserScanner
//--------------------------------
//
class CLaserScanner
{
  private:
  String FID;
  EStateLaserScanner FState;
  UInt32 FPulsePeriodms;
  UInt32 FPulseCountActual;
  UInt32 FPulseCountPreset;
  //
  public:
  // Init
  CLaserScanner(String id);
  Boolean Open(void);
  Boolean Close(void);
  // Property
  EStateLaserScanner GetState(void);
  Boolean IsBusy(void);
  void SetPulsePeriod(UInt32 period);
  UInt32 GetPulsePeriod(void);
  // Handler
  Boolean PulseLaserCount(UInt32 period, UInt32 count);
  Boolean MovePositionX(UInt16 px);
  Boolean MovePositionY(UInt16 py);
  Boolean MovePositionPulse(UInt16 px, UInt16 PY, UInt32 PP, UInt32 PC, UInt32 DM);
  //  
  void Abort(void);
  void Execute(void);
  //
  //
  //
};
//
#endif // LaserScanner_h
//
#endif // LASERSCANNER_JI
//
