//
#include "Defines.h"
//
#include "Error.h"
//#include "Process.h"
#include "Automation.h"
#include "Command.h"
#include "CommandFile.h"
#include "Led.h"
#include "TimeRelative.h"
#if defined(SERIALCOMMAND_ISPLUGGED)
#include "Serial.h"
#endif
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#include "TimeRelative.h"
#if defined(NTPCLIENT_ISPLUGGED)
#include "TimeAbsolute.h"
#include "NTPClient.h"
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
#include "TriggerInput.h"
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
#include "I2CDisplay.h"
#endif
#if defined(ARD_ISPLUGGED)
#include "ard.h"
#endif
#if defined(RFIDCLIENT_ISPLUGGED)
#include "RFIDClient.h"
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
#include "MQTTClient.h"
#endif
#if defined(RTCINTERNAL_ISPLUGGED)
#include "RTCInternal.h"
#endif
#include "IOPin.h"
#if defined(MOTORVNH2SP30_ISPLUGGED)
#include "MotorVNH2SP30.h"
#endif
#if defined(MOTORENCODERLM393_ISPLUGGED)
#include "MotorEncoderLM393.h"
#endif
#if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
#include "PositionReached.h";
#endif
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
#include "PinInput.h"
#include "PinOutput.h"
#include "RF433MhzClient.h"
#include "RemoteSwitch.h"
#endif
#if defined(LASERSCANNER_JI)
#include "LaserScannerJI.h"
#endif
#if defined(LASERSCANNER_PS)
#include "LaserScannerPS.h"
#endif
//
//-------------------------------------------
//  External Global Variables
//-------------------------------------------
//
extern CError               Error;
extern CCommand             Command;
#if defined(SDCARD_ISPLUGGED)
extern CCommandFile         CommandFile;
#endif
extern CAutomation          Automation;
extern CTimeRelative        TimeRelativeSystem;
#if defined(NTPCLIENT_ISPLUGGED)
extern CTimeAbsolute        TimeAbsoluteSystem;
#endif
extern CLed                 LedSystem;
#if defined(MULTICHANNELLED_ISPLUGGED)
extern CLed                 LedChannel1;
extern CLed                 LedChannel2;
extern CLed                 LedChannel3;
extern CLed                 LedChannel4;
extern CLed                 LedChannel5;
extern CLed                 LedChannel6;
extern CLed                 LedChannel7;
extern CLed                 LedChannel8;
#endif
//
#if defined(SERIALCOMMAND_ISPLUGGED)
extern CSerial              SerialCommand;
#endif
//extern CSerial  SerialXY;
//extern CSerial  SerialZW;
#if defined(I2CDISPLAY_ISPLUGGED)
extern CI2CDisplay          I2CDisplay;
#endif
#if defined(WATCHDOG_ISPLUGGED)
extern CWatchDog            WatchDog;
#endif
#if defined(NTPCLIENT_ISPLUGGED)
extern CNTPClient           NTPClient;
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
extern CTriggerInput        TriggerInputSystem;
#endif
#if defined(ARD_ISPLUGGED)
extern Card              ard;
#endif
#if defined(RFIDCLIENT_ISPLUGGED)
extern CRFIDClient          RFIDClient;
#endif
#if defined(RTCINTERNAL_ISPLUGGED)
extern CRTCInternal         RTCInternal;
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
extern CMQTTClient          MQTTClient;
#endif
#if defined(MOTORVNH2SP30_ISPLUGGED)
extern CMotorVNH2SP30       MotorLeft;
extern CMotorVNH2SP30       MotorRight;
#endif
#if defined(MOTORENCODERLM393_ISPLUGGED)
extern CMotorEncoderLM393   MotorEncoderLM393Left;
extern CMotorEncoderLM393   MotorEncoderLM393Right;
#endif
#if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
extern CPositionReached     MotorPositionReached;
#endif
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
extern CPinInput       SignalA0;//("LL", PIN_GROUPA_A0, LEVEL_INVERTED);
extern CPinInput       SignalA1;//("SP", PIN_GROUPA_A1, LEVEL_INVERTED);
extern CPinInput       SignalA2;//("IE", PIN_GROUPA_A2, LEVEL_INVERTED);
extern CPinInput       SignalB0;//("SC", PIN_GROUPB_B0);
extern CPinInput       SignalB1;//("SE", PIN_GROUPB_B1);
extern CPinInput       SignalB2;//("SO", PIN_GROUPB_B2);
extern CRF433MHzClient RF433MHzTransmit;//("WT", PIN_RF433MHZ_TRANSMIT);
extern CRemoteSwitch   RemoteSwitch;//("RS");  
#endif
#if defined(LASERSCANNER_JI)
extern CLaserScanner   LaserScanner;
#endif
#if defined(LASERSCANNER_PS)
extern CLaserScannerPS LaserScanner;
#endif
//
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand(void)
{
  FState = scUndefined;
}

CCommand::~CCommand(void)
{
  FState = scUndefined;
}
//
//#########################################################
//  Segment - Property
//#########################################################
//
EStateCommand CCommand::GetState(void)
{
  return FState;
}
void CCommand::SetState(EStateCommand state)
{
  if (state != FState)
  {
    sprintf(GetTxdBuffer(), "CS %s %s", StateText(state), StateText(FState));
#ifdef SERIALCOMMAND_ISPLUGGED
    SerialCommand.WriteEvent(GetTxdBuffer());
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
    MQTTClient.WriteResponse(GetTxdBuffer());
#endif
    FState = state;
  }
}
//
//#########################################################
//  Segment - Property
//#########################################################
//
Boolean CCommand::Open(void)
{
  SetState(scInit);
  Init();
  SetState(scIdle);
}
Boolean CCommand::Close(void)
{
  SetState(scUndefined);
}
//
//#########################################################
//  Segment - Helper
//#########################################################
//
char* CCommand::StateText(EStateCommand state)
{
  switch (state)
  {
    case scError:
      return "Error";
    case scUndefined:
      return "Undefined";
    case scInit:
      return "Init";
    case scIdle:
      return "Idle";
    case scBusy:
      return "Busy";
    default: // not in list -> Undefined
      return "Unknown";
  }
}
// Defines Reset-Function (identically HW-Reset):
void (*PResetSystem)(void) = 0;
//
void CCommand::ZeroRxdBuffer(void)
{
  int CI;
  for (CI = 0; CI < SIZE_SERIALBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}
void CCommand::ZeroTxdBuffer(void)
{
  int CI;
  for (CI = 0; CI < SIZE_SERIALBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}
//
void CCommand::ZeroCommandText(void)
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FCommandText[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Helper - Analyse
//#########################################################
//
void CCommand::Init(void)
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroCommandText();
}

Boolean CCommand::AnalyseCommandText(CSerial &serial)
{
  char *PTerminal = (char*)" \t\r\n";
  FPCommand = strtok(FCommandText, PTerminal);
  if (FPCommand)
  {
    FParameterCount = 0;
    char *PParameter;
    while (0 != (PParameter = strtok(0, PTerminal)))
    {
      FPParameters[FParameterCount] = PParameter;
      FParameterCount++;
      if (COUNT_TEXTPARAMETERS < FParameterCount)
      {
        Error.SetCode(ecToManyParameters);
        ZeroRxdBuffer();
        serial.WriteNewLine();
        serial.WritePrompt();
        return false;
      }
    }  
    ZeroRxdBuffer();
    serial.WriteNewLine();
    return true;
  }
  ZeroRxdBuffer();
  serial.WriteNewLine();
  serial.WritePrompt();    
  return false;
}

Boolean CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case TERMINAL_CARRIAGERETURN:
        FRxdBuffer[FRxdBufferIndex] = TERMINAL_ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FCommandText, FRxdBuffer);
        return true;
      case TERMINAL_LINEFEED: // ignore
        break;
      default: 
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}
//
#if defined(SDCARD_ISPLUGGED)
Boolean CCommand::DetectSDCardLine(CSDCard &sdcard)
{
  if (scIdle == GetState())
  { // ready for next Command
    if (CommandFile.IsExecuting())
    { // more Commands exist and no Wait-Command:
      if (TimeRelativeSystem.Wait_Execute()) return false;
#if defined(NTPCLIENT_ISPLUGGED)
      if (TimeAbsoluteSystem.Wait_Execute()) return false;
#endif      
#if defined(TRIGGERINPUT_ISPLUGGED)
      if (TriggerInputSystem.Wait_Execute()) return false;
#endif
#if defined(MOTORVNH2SP30_ISPLUGGED) && defined(MOTORENCODERLM393_ISPLUGGED)
      if (MotorPositionReached.Wait_Execute()) return false;
#endif  
      String Command = CommandFile.GetCommand();
      if (0 < Command.length())
      { // copy CommandFile-Line (next Command) to CommandBuffer:
        strcpy(FCommandText, Command.c_str());
        return true;
      }
    }
  }
  return false;
}
#endif
/*
ecf /sdc.cmd
*/
//
#if defined(MQTTCLIENT_ISPLUGGED)
Boolean CCommand::DetectMQTTClientLine(CMQTTClient &mqttclient)
{
  if (scIdle == GetState())
  {
    String Line = mqttclient.ReadReceivedText();
    if (0 < Line.length())
    {
      strcpy(FCommandText, Line.c_str());
      return true;
    }
  }
  return false;
}
#endif
//
#if defined(SDCARD_ISPLUGGED)
Boolean CCommand::Handle(CSerial &serial, CSDCard &sdcard)
{ // Command <- SDCard
  if (DetectSDCardLine(sdcard))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);
    }
  }
  return false;
}
#endif // SDCARD_ISPLUGGED
//
#if defined(MQTTCLIENT_ISPLUGGED)
Boolean CCommand::Handle(CSerial &serial, CMQTTClient &mqttclient)
{ // Command <- Mqtt
  if (DetectMQTTClientLine(mqttclient))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);
    }
  }
  return false;
}
#endif // MQTTCLIENT_ISPLUGGED
//
Boolean CCommand::Handle(CSerial &serial)
{ // Command <- (Usb)Serial
  if (DetectRxdLine(serial))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);
    }
  }
  return false;
}
//
//#########################################################
//  Segment - Helper - Answer
//#########################################################
//
void CCommand::WriteEvent(String line)
{
#ifdef SERIALCOMMAND_ISPLUGGED
  SerialCommand.WriteEvent(line);
  SerialCommand.WritePrompt();
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteEvent(line);
#endif
//#if defined(I2CDISPLAY_ISPLUGGED)
//  I2CDisplay.WriteEvent(line);
//#endif
//#if defined(SDCARD_ISPLUGGED)
//  SDCard.WriteEvent(line);
//#endif 
}

void CCommand::WriteResponse(String line)
{
#ifdef SERIALCOMMAND_ISPLUGGED
  SerialCommand.WriteResponse(line);
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteResponse(line);
//???  MQTTClient.WritePrompt();
#endif
//#if defined(I2CDISPLAY_ISPLUGGED)
//  I2CDisplay.WriteAnswer(line);
//#endif
//#if defined(SDCARD_ISPLUGGED)
//  SDCard.WriteAnswer(line);
//#endif
}

void CCommand::WriteComment(String line)
{
#ifdef SERIALCOMMAND_ISPLUGGED
  SerialCommand.WriteComment(line);
  SerialCommand.WritePrompt();
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteComment(line);
#endif
//#if defined(I2CDISPLAY_ISPLUGGED)
//  I2CDisplay.WriteComment(line);
//#endif
//#if defined(SDCARD_ISPLUGGED)
//  SDCard.WriteComment(line);
//#endif 
}
void CCommand::WriteComment(String mask, String line)
{
  sprintf(GetTxdBuffer(), mask.c_str(), line.c_str());
  WriteComment(GetTxdBuffer());
}
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{
#if defined(SERIALCOMMAND_ISPLUGGED) 
//  serial.WriteComment();
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_PROJECT, ARGUMENT_PROJECT);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_HARDWARE, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_DATE, ARGUMENT_DATE);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_TIME, ARGUMENT_TIME);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_AUTHOR, ARGUMENT_AUTHOR);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_PORT, ARGUMENT_PORT);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(MASK_PARAMETER, ARGUMENT_PARAMETER);
  serial.WriteNewLine();
  serial.WriteComment();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteComment();
//  serial.WriteNewLine();
//  serial.WriteComment();
  serial.WriteLine(MASK_ENDLINE);
#endif
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteComment(TITLE_LINE);
  MQTTClient.WriteComment(MASK_PROJECT, ARGUMENT_PROJECT);
  MQTTClient.WriteComment(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  MQTTClient.WriteComment(MASK_HARDWARE, ARGUMENT_HARDWARE);
  MQTTClient.WriteComment(MASK_DATE, ARGUMENT_DATE);
  MQTTClient.WriteComment(MASK_TIME, ARGUMENT_TIME);
  MQTTClient.WriteComment(MASK_AUTHOR, ARGUMENT_AUTHOR);
  MQTTClient.WriteComment(MASK_PORT, ARGUMENT_PORT);
  MQTTClient.WriteComment(MASK_PARAMETER, ARGUMENT_PARAMETER);
  MQTTClient.WriteComment(TITLE_LINE);
  MQTTClient.WriteComment(MASK_ENDLINE);
#endif  
}
//
void CCommand::WriteSoftwareVersion(CSerial &serial)
{
#if defined(SERIALCOMMAND_ISPLUGGED)
  sprintf(GetTxdBuffer(), MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
  serial.WriteComment(GetTxdBuffer());
#endif
//######################################################
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteComment(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
#endif
}
//
void CCommand::WriteHardwareVersion(CSerial &serial)
{
#if defined(SERIALCOMMAND_ISPLUGGED) 
  sprintf(GetTxdBuffer(), MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
  serial.WriteComment(GetTxdBuffer());
#endif
//######################################################
#if defined(MQTTCLIENT_ISPLUGGED)
  MQTTClient.WriteComment(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
#endif
}
//
void CCommand::WriteHelp(CSerial &serial)
{
#if defined(SERIALCOMMAND_ISPLUGGED) 
    serial.WriteNewLine();
    serial.WriteComment(); serial.WriteLine(HELP_COMMON);
    serial.WriteComment(); serial.Write(MASK_H, SHORT_H); serial.WriteLine();
  #if defined(COMMAND_SYSTEMENABLED)
    serial.WriteComment(); serial.Write(MASK_GPH, SHORT_GPH); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_GSV, SHORT_GSV); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_GHV, SHORT_GHV); serial.WriteLine();
  #endif
    serial.WriteComment(); serial.Write(MASK_RSS, SHORT_RSS); serial.WriteLine();
  #if defined(WATCHDOG_ISPLUGGED)
    serial.WriteComment(); serial.Write(MASK_PWD, SHORT_PWD); serial.WriteLine();
  #endif
  #if defined(COMMAND_SYSTEMENABLED)
    serial.WriteComment(); serial.Write(MASK_A, SHORT_A); serial.WriteLine();
  #endif
    serial.WriteComment(); serial.Write(MASK_WTR, SHORT_WTR); serial.WriteLine();
  #if defined(NTPCLIENT_ISPLUGGED)
    serial.WriteComment(); serial.Write(MASK_WTA, SHORT_WTA); serial.WriteLine();
  #endif
  #if defined(TRIGGERINPUT_ISPLUGGED)  
    serial.WriteComment(); serial.Write(MASK_WTI, SHORT_WTI); serial.WriteLine();
  #endif
    //
  #if defined(COMMAND_SYSTEMENABLED)
    serial.WriteComment(); serial.WriteLine(HELP_LEDSYSTEM);
    serial.WriteComment(); serial.Write(MASK_GLS, SHORT_GLS); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_LSH, SHORT_LSH); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_LSL, SHORT_LSL); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BLS, SHORT_BLS); serial.WriteLine();
  #endif
  #if defined(MULTICHANNELLED_ISPLUGGED)
    serial.WriteComment(); serial.Write(MASK_BL1, SHORT_BL1); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL2, SHORT_BL2); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL3, SHORT_BL3); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL4, SHORT_BL4); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL5, SHORT_BL5); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL6, SHORT_BL6); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL7, SHORT_BL7); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_BL8, SHORT_BL8); serial.WriteLine();
  #endif
  //  // Serial
  //  serial.WriteComment(); serial.WriteLine(HELP_SERIAL_COMMAND);
  //  serial.WriteComment(); serial.Write(MASK_WLC, SHORT_WLC); serial.WriteLine();
  //  serial.WriteComment(); serial.Write(MASK_RLC, SHORT_RLC); serial.WriteLine();
    //
  #if defined(NTPCLIENT_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_NTPCLIENT);
    serial.WriteComment(); serial.Write(MASK_GNT, SHORT_GNT); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_GND, SHORT_GND); serial.WriteLine();
  #endif
    //
  #if defined(I2CDISPLAY_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_I2CDISPLAY);
    serial.WriteComment(); serial.Write(MASK_CLI, SHORT_CLI); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_STI, SHORT_STI); serial.WriteLine();
  #endif
    //
  #if defined(SDCARD_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_SDCOMMAND);
//!!!    serial.WriteComment(); serial.Write(MASK_OCF, SHORT_OCF); serial.WriteLine();
//!!!    serial.WriteComment(); serial.Write(MASK_WCF, SHORT_WCF); serial.WriteLine();
//!!!    serial.WriteComment(); serial.Write(MASK_CCF, SHORT_CCF); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_ECF, SHORT_ECF); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_ACF, SHORT_ACF); serial.WriteLine();
  #endif
    //
    // RFIDClient
  #if defined(RFIDCLIENT_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_RFIDCLIENT);
    //serial.WriteComment(); serial.Write(MASK_SRL, SHORT_SRL); serial.WriteLine();
  #endif  
    //
  #if defined(MQTTCLIENT_ISPLUGGED)
    // !!!!!!!!  serial.WriteAnswer(); serial.WriteLine(HELP_MQTTCLIENT);
  #endif
    //
    // MotorVNH2SP30
  #if defined(MOTORVNH2SP30_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_MOTORVNH2SP30);
    serial.WriteComment(); serial.Write(MASK_GLWL, SHORT_GLWL); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SLWL, SHORT_SLWL); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_GLWH, SHORT_GLWH); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SLWH, SHORT_SLWH); serial.WriteLine();
    //
    serial.WriteComment(); serial.Write(MASK_GRWL, SHORT_GRWL); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SRWL, SHORT_SRWL); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_GRWH, SHORT_GRWH); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SRWH, SHORT_SRWH); serial.WriteLine();
    //
    serial.WriteComment(); serial.Write(MASK_SL, SHORT_SL);   serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SR, SHORT_SR);   serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_S, SHORT_S);   serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MLP, SHORT_MLP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MLN, SHORT_MLN); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MRP, SHORT_MRP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MRN, SHORT_MRN); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MBP, SHORT_MBP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MBN, SHORT_MBN); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_RBP, SHORT_RBP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_RBN, SHORT_RBN); serial.WriteLine();
  #endif
    //
    // MotorEncoderLM393
  #if defined(MOTORENCODERLM393_ISPLUGGED)
    serial.WriteComment(); serial.WriteLine(HELP_MOTORENCODERLM393);
    serial.WriteComment(); serial.Write(MASK_GEPB, SHORT_GEPB); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SEPL, SHORT_SEPL); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_SEPR, SHORT_SEPR); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_WPR, SHORT_WPR); serial.WriteLine();
  #endif
    //
    // RF433MHzClient / RemoteWirelessSwitch
  #if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
    serial.WriteComment(); serial.WriteLine(HELP_REMOTEWIRELESSSWITCH);
    serial.WriteComment(); serial.Write(MASK_RSO, SHORT_RSO); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_RSF, SHORT_RSF); serial.WriteLine();
  #endif  
    //
    // LaserScanner
  #if defined(LASERSCANNER_JI)
    serial.WriteComment(); serial.WriteLine(HELP_LASERSCANNER);
    serial.WriteComment(); serial.Write(MASK_SPP, SHORT_SPP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_GPP, SHORT_GPP); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_PLA, SHORT_PLA); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_PLC, SHORT_PLC); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MPX, SHORT_MPX); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MPY, SHORT_MPY); serial.WriteLine();
    serial.WriteComment(); serial.Write(MASK_MPP, SHORT_MPP); serial.WriteLine();
  #endif
  #if defined(LASERSCANNER_PS)
    serial.WriteComment(); serial.WriteLine(HELP_LASERSCANNER);
    serial.WriteComment(); serial.Write(MASK_, SHORT_); serial.WriteLine();
  #endif
  //
  serial.WriteComment(); serial.WriteLine(MASK_ENDLINE);
#endif
//################################################################
#if defined(MQTTCLIENT_ISPLUGGED)
    MQTTClient.WriteComment(HELP_COMMON);
    MQTTClient.WriteComment(MASK_H, SHORT_H);
  #if defined(COMMAND_SYSTEMENABLED)
    MQTTClient.WriteComment(MASK_GPH, SHORT_GPH);
    MQTTClient.WriteComment(MASK_GSV, SHORT_GSV);
    MQTTClient.WriteComment(MASK_GHV, SHORT_GHV);
  #endif
    MQTTClient.WriteComment(MASK_RSS, SHORT_RSS);
  #if defined(WATCHDOG_ISPLUGGED)
    MQTTClient.WriteComment(MASK_PWD, SHORT_PWD);
  #endif
  #if defined(COMMAND_SYSTEMENABLED)
    MQTTClient.WriteComment(MASK_A, SHORT_A);
  #endif
  #if defined(NTPCLIENT_ISPLUGGED)
    MQTTClient.WriteComment(MASK_WTR, SHORT_WTR);
    MQTTClient.WriteComment(MASK_WTA, SHORT_WTA);
  #endif
  #if defined(TRIGGERINPUT_ISPLUGGED)
    MQTTClient.WriteComment(MASK_WTI, SHORT_WTI);
  #endif
    //
  #if defined(COMMAND_SYSTEMENABLED)
    MQTTClient.WriteComment(HELP_LEDSYSTEM);
    MQTTClient.WriteComment(MASK_GLS, SHORT_GLS);
    MQTTClient.WriteComment(MASK_LSH, SHORT_LSH);
    MQTTClient.WriteComment(MASK_LSL, SHORT_LSL);
    MQTTClient.WriteComment(MASK_BLS, SHORT_BLS);
  #endif
  #if defined(MULTICHANNELLED_ISPLUGGED)  
    MQTTClient.WriteComment(MASK_BL1, SHORT_BL1);
    MQTTClient.WriteComment(MASK_BL2, SHORT_BL2);
    MQTTClient.WriteComment(MASK_BL3, SHORT_BL3);
    MQTTClient.WriteComment(MASK_BL4, SHORT_BL4);
    MQTTClient.WriteComment(MASK_BL5, SHORT_BL5);
    MQTTClient.WriteComment(MASK_BL6, SHORT_BL6);
    MQTTClient.WriteComment(MASK_BL7, SHORT_BL7);
    MQTTClient.WriteComment(MASK_BL8, SHORT_BL8);
  #endif
  //  // Serial
  // MQTTClient.WriteComment(HELP_SERIAL_COMMAND);
  // MQTTClient.WriteComment(MASK_WLC, SHORT_WLC);
  // MQTTClient.WriteComment(MASK_RLC, SHORT_RLC);
    //
  #if defined(NTPCLIENT_ISPLUGGED)
    MQTTClient.WriteComment(HELP_NTPCLIENT);
    MQTTClient.WriteComment(MASK_GNT, SHORT_GNT);
    MQTTClient.WriteComment(MASK_GND, SHORT_GND);
  #endif
    //
  #if defined(I2CDISPLAY_ISPLUGGED)
    MQTTClient.WriteComment(HELP_I2CDISPLAY);
    MQTTClient.WriteComment(MASK_CLI, SHORT_CLI);
    MQTTClient.WriteComment(MASK_STI, SHORT_STI);
  #endif
    //
  #if defined(SDCARD_ISPLUGGED)
    MQTTClient.WriteComment(HELP_SDCOMMAND);
    // !!! MQTTClient.WriteComment(MASK_OCF, SHORT_OCF);
    // !!! MQTTClient.WriteComment(MASK_WCF, SHORT_WCF);
    // !!! MQTTClient.WriteComment(MASK_CCF, SHORT_CCF);
    MQTTClient.WriteComment(MASK_ECF, SHORT_ECF);
    MQTTClient.WriteComment(MASK_ACF, SHORT_ACF);
  #endif
    //
    // RFIDClient
  #if defined(RFIDCLIENT_ISPLUGGED)
    MQTTClient.WriteComment(HELP_RFIDCLIENT);
    // ... MQTTClient.WriteComment(MASK_SRL, SHORT_SRL); 
  #endif
    //
  #if defined(MQTTCLIENT_ISPLUGGED)
    // MQTTClient.WriteComment(HELP_MQTTCLIENT);
  #endif
    //
    // MotorVNH2SP30
  #if defined(MOTORVNH2SP30_ISPLUGGED)
    MQTTClient.WriteComment(HELP_MOTORVNH2SP30);
    MQTTClient.WriteComment(MASK_GLWL, SHORT_GLWL);
    MQTTClient.WriteComment(MASK_SLWL, SHORT_SLWL);
    MQTTClient.WriteComment(MASK_GLWH, SHORT_GLWH);
    MQTTClient.WriteComment(MASK_SLWH, SHORT_SLWH);
    //
    MQTTClient.WriteComment(MASK_GRWL, SHORT_GRWL);
    MQTTClient.WriteComment(MASK_SRWL, SHORT_SRWL);
    MQTTClient.WriteComment(MASK_GRWH, SHORT_GRWH);
    MQTTClient.WriteComment(MASK_SRWH, SHORT_SRWH);
    //
    MQTTClient.WriteComment(MASK_A,   SHORT_A);
    MQTTClient.WriteComment(MASK_SL,  SHORT_SL);
    MQTTClient.WriteComment(MASK_SR,  SHORT_SR);
    MQTTClient.WriteComment(MASK_MLP, SHORT_MLP);
    MQTTClient.WriteComment(MASK_MLN, SHORT_MLN);
    MQTTClient.WriteComment(MASK_MRP, SHORT_MRP);
    MQTTClient.WriteComment(MASK_MRN, SHORT_MRN);
    MQTTClient.WriteComment(MASK_MBP, SHORT_MBP);
    MQTTClient.WriteComment(MASK_MBN, SHORT_MBN);
    MQTTClient.WriteComment(MASK_RBP, SHORT_RBP);
    MQTTClient.WriteComment(MASK_RBN, SHORT_RBN);
  #endif
    //
    // MotorEncoderLM393
  #if defined(MOTORENCODERLM393_ISPLUGGED)
    MQTTClient.WriteComment(HELP_MOTORENCODERLM393);
    MQTTClient.WriteComment(MASK_GEPB, SHORT_GEPB);
    MQTTClient.WriteComment(MASK_SEPL, SHORT_SEPL);
    MQTTClient.WriteComment(MASK_SEPR, SHORT_SEPR);
    MQTTClient.WriteComment(MASK_WPR, SHORT_WPR);
  #endif
    //
    // RF433MHzClient / RemoteWirelessSwitch
  #if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
    MQTTClient.WriteComment(HELP_REMOTEWIRELESSSWITCH);
    MQTTClient.WriteComment(MASK_RSO, SHORT_RSO);
    MQTTClient.WriteComment(MASK_RSF, SHORT_RSF);
  #endif  
    //
    // LaserScanner
  #if defined(LASERSCANNER_JI)
    MQTTClient.WriteComment(HELP_LASERSCANNER);
    MQTTClient.WriteComment(MASK_SPP, SHORT_SPP);
    MQTTClient.WriteComment(MASK_GPP, SHORT_GPP);
    MQTTClient.WriteComment(MASK_PLA, SHORT_PLA);
    MQTTClient.WriteComment(MASK_PLC, SHORT_PLC);
    MQTTClient.WriteComment(MASK_MPX, SHORT_MPX);
    MQTTClient.WriteComment(MASK_MPY, SHORT_MPY);
    MQTTClient.WriteComment(MASK_MPP, SHORT_MPP);
  #endif
  #if defined(LASERSCANNER_PS)
    MQTTClient.WriteComment(HELP_LASERSCANNER);
    MQTTClient.WriteComment(MASK_, SHORT_);
    //
  #endif

  
    //
    MQTTClient.WriteComment(MASK_ENDLINE);   
#endif
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
void CCommand::ExecuteGetHelp(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Response:
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  WriteHelp(serial);
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt(); 
}
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetProgramHeader(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Response:
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  WriteProgramHeader(serial);
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt(); 
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetSoftwareVersion(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Response:
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %i", GetPCommand(), COUNT_SOFTWAREVERSION);  
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  WriteSoftwareVersion(serial);
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %i", GetPCommand(), COUNT_HARDWAREVERSION);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  WriteHardwareVersion(serial);
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(WATCHDOG_ISPLUGGED)
void CCommand::ExecutePulseWatchDog(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( PWD - ):
  // Execute:
  WatchDog.ForceTrigger();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
void CCommand::ExecuteResetSystem(CSerial &serial)
{
//  if (scIdle != GetState())
//  {
//    Error.SetCode(ecCommandTimingFailure);
//    return;
//  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( RSS - ):
  // Execute:
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
  delay(2000);
  //
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  // Reset System !!!
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#if (defined(PROCESSOR_NANOR3)||defined(PROCESSOR_UNOR3)||defined(PROCESSOR_MEGA2560))
  PResetSystem();
#elif defined(PROCESSOR_DUEM3)
#elif defined(PROCESSOR_STM32F103C8)
#elif (defined(PROCESSOR_TEENSY32)||defined(PROCESSOR_TEENSY36))
  // problem because of reprogramming !!! _reboot_Teensyduino_();
  // undefined _restart_Teensyduino_();
  // undefined init_pins();
  // the only possibility for this time:
  // OK (no reset!) _init_Teensyduino_internal_();
  // TOP:!!!
#define CPU_RESTART_ADDR (uint32_t *)0xE000ED0C
#define CPU_RESTART_VAL 0x5FA0004
#define CPU_RESTART (*CPU_RESTART_ADDR = CPU_RESTART_VAL);
  CPU_RESTART
  //
#endif
}
//
void CCommand::AbortAll(void)
{
#if defined(SDCARD_ISPLUGGED)
  CommandFile.Close();
#endif  
  // Close all
  TimeRelativeSystem.Wait_Abort();
#if defined(NTPCLIENT_ISPLUGGED)
  TimeAbsoluteSystem.Wait_Abort();
#endif  
#if defined(TRIGGERINPUT_ISPLUGGED)
  TriggerInputSystem.Wait_Abort();
#endif  
  //
  LedSystem.Blink_Abort();
#if defined(MULTICHANNELLED_ISPLUGGED)
  LedChannel1.Blink_Abort();
  LedChannel2.Blink_Abort();
  LedChannel3.Blink_Abort();
  LedChannel4.Blink_Abort();
  LedChannel5.Blink_Abort();
  LedChannel6.Blink_Abort();
  LedChannel7.Blink_Abort();
  LedChannel8.Blink_Abort();
#endif  
}
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteAbortAll(CSerial &serial)
{
//  if (scIdle != GetState())
//  {
//    Error.SetCode(ecCommandTimingFailure);
//    return;
//  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( A - ):
  // Execute:
  AbortAll();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
void CCommand::ExecuteWaitTimeRelative(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: { WTR <time> }
  UInt32 TR = atol(GetPParameters(0));
  // Execute:
  TimeRelativeSystem.Wait_Start(TR);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu", GetPCommand(), TR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
//
#if defined(NTPCLIENT_ISPLUGGED)
void CCommand::ExecuteWaitTimeAbsolute(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( WTA hh mm ss ):
  Byte HH = atoi(GetPParameters(0));
  Byte MM = atoi(GetPParameters(1));
  Byte SS = atoi(GetPParameters(2));
  // Execute:
  TimeAbsoluteSystem.Wait_Start(HH, MM, SS);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u %u %u", GetPCommand(), HH, MM, SS);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(TRIGGERINPUT_ISPLUGGED)
void CCommand::ExecuteWaitTriggerInput(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( WTI re ):    
  bool RE  = (0 < atoi(GetPParameters(0)));
  // Execute:
  TriggerInputSystem.Wait_Start(RE);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %i", GetPCommand(), (int)RE);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//#########################################################
//  Segment - Execution - NTPClient
//#########################################################
//
#if defined(NTPCLIENT_ISPLUGGED)
void CCommand::ExecuteGetNTPClientTime(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GNT - ):
  // Execute:
  String HH = "??";
  String MM = "??";
  String SS = "??";
  NTPClient.GetTime(HH, MM, SS);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %s %s %s", GetPCommand(), HH, MM, SS);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(NTPCLIENT_ISPLUGGED)
void CCommand::ExecuteGetNTPClientDate(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GND - ):
  // Execute:
  String YY = "??";
  String MM = "??";
  String DD = "??";
  NTPClient.GetDate(YY, MM, DD);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %s %s %s", GetPCommand(), YY, MM, DD);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//#########################################################
//  Segment - Execution - LedSystem
//#########################################################
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetLedSystem(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Execute:
  int SLS = LedSystem.GetState();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %i", GetPCommand(), SLS);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt(); 
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteLedSystemOn(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Execute:
  LedSystem.SetOn();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt(); 
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteLedSystemOff(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: -
  // Execute:
  LedSystem.SetOff();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt(); 
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteBlinkLedSystem(CSerial &serial)
{ // <BLS> <c> <p> <w>
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedSystem.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
void CCommand::ExecuteBlinkLedChannel1(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel1.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
void CCommand::ExecuteBlinkLedChannel2(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel2.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
void CCommand::ExecuteBlinkLedChannel3(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel3.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
void CCommand::ExecuteBlinkLedChannel4(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel4.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
void CCommand::ExecuteBlinkLedChannel5(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel5.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
void CCommand::ExecuteBlinkLedChannel6(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel6.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
void CCommand::ExecuteBlinkLedChannel7(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel7.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MULTICHANNELLED_ISPLUGGED)
void CCommand::ExecuteBlinkLedChannel8(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 3)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters: 
  UInt32 PC = atol(GetPParameters(0));  // [1]
  UInt32 PP = atol(GetPParameters(1));  // [ms]
  UInt32 PW = atol(GetPParameters(2));  // [ms]
  // Execute:
  LedChannel8.Blink_Start(PC, PP, PW);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu", GetPCommand(), PC, PP, PW);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//#########################################################
//  Segment - Execution - SDCommand
//#########################################################
//
//#if defined(SDCARD_ISPLUGGED)
//void CCommand::ExecuteOpenCommandFile(CSerial &serial)
//{
//  if (scIdle != GetState())
//  {
//    Error.SetCode(ecCommandTimingFailure);
//    return;
//  }
//  if (GetParameterCount() < 1)
//  {
//    Error.SetCode(ecNotEnoughParameters);
//    return;
//  }
//  // Analyse parameters ( OCF <f> ):
//  String FN = GetPParameters(0);
// // ???? Automation.SetCommandFileName(FN);
//  // Execution: -
//  // Response:
//  sprintf(GetTxdBuffer(), "%s %s", GetPCommand(), GetPParameters(0));
//  WriteResponse(GetTxdBuffer());
//}
//#endif
//
//#if defined(SDCARD_ISPLUGGED)
//void CCommand::ExecuteWriteCommandFile(CSerial &serial)
//{
//  if (scIdle != GetState())
//  {
//    Error.SetCode(ecCommandTimingFailure);
//    return;
//  }
//  if (GetParameterCount() < 2)
//  {
//    Error.SetCode(ecNotEnoughParameters);
//    return;
//  }
//  // Analyse parameters ( WCF <c> <p> ):
//  // Execution: -
//  // Response:
//  serial.Write(TERMINAL_RESPONSE);
//  serial.Write(' ');
//  serial.Write(GetPCommand());
//  serial.Write(' ');
//  int PC = GetParameterCount();
//  for (int II = 0; II < PC; II++)
//  {
//    serial.Write(GetPParameters(II));
//    serial.Write(' ');
//  }
//  serial.WriteLine("");
//  serial.WritePrompt();
//}
//#endif
//
//#if defined(SDCARD_ISPLUGGED)
//void CCommand::ExecuteCloseCommandFile(CSerial &serial)
//{
//  if (scIdle != GetState())
//  {
//    Error.SetCode(ecCommandTimingFailure);
//    return;
//  }
//  // Analyse parameters ( CCF - ):
//  // Execution: -
//  // Response:
//  sprintf(GetTxdBuffer(), "%s", GetPCommand());
//  WriteResponse(GetTxdBuffer());
//}
//#endif
//
#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteExecuteCommandFile(CSerial &serial)
{ // ecf /sdc.cmd
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( ECF <f> ):
  String CF = GetPParameters(0);
  // Execution: 
  // !!! !!! !!! !!! !!! !!!
  CommandFile.Open(&SD);
  // !!! !!! !!! !!! !!! !!!
  if (!CommandFile.ParseFile(CF.c_str()))
  {
    Error.SetCode(ecFailParseCommandFile);
    return;
  }
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", GetPCommand(), CF.c_str());
  SerialCommand.WritePrompt();
  WriteResponse(GetTxdBuffer());
  // Response:
  SerialCommand.Write("# CommandFile[");
  SerialCommand.Write(CF);
  SerialCommand.WriteLine("]:");
  bool CommandLoop = true;
  while (CommandLoop)
  {
    String Command = CommandFile.GetCommand();
    CommandLoop = (0 < Command.length());
    if (CommandLoop)
    {
      SerialCommand.Write("# Command[");
      SerialCommand.Write(Command.c_str());
      SerialCommand.WriteLine("]");
    }
  }
  // Preparation for executing Commands from CommandFile:
  CommandFile.Open(&SD);
  if (!CommandFile.ParseFile(CF.c_str()))
  {
    Error.SetCode(ecFailParseCommandFile);
    return;
  }
  // !!! !!! !!! !!! !!! !!!
  CommandFile.ResetExecution();
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();  
  // only after Command-Completition: CommandFile.Close();
}
#endif
/*
ecf /sdc.cmd 
 */
//
#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteAbortCommandFile(CSerial &serial)
{
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( ACF - ):
  // Execution:
  AbortAll();
  // Response:
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s 1", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
////
////#########################################################
////  Segment - Execution - Serial - Command
////#########################################################
////
//void CCommand::ExecuteWriteLineSerialCommand(CSerial &serial)
//{
//  if (spIdle != Process.GetState())
//  {
//    Error.SetCode(ecCommandTimingFailure);
//    return;
//  }
//  if (GetParameterCount() < 1)
//  {
//    Error.SetCode(ecNotEnoughParameters);
//    return;
//  }
//  Process.SetState(spWriteLineSerialCommand);
//  // Analyse parameters ( WLC <l> ):
//  String Line = GetPParameters(0);
//  // Execute:
//  SerialCommand.WriteLine(Line);
//  // Response:
//  sprintf(Command.GetTxdBuffer(), "%s %s", GetPCommand(), Line.c_str());
//  WriteResponse(Command.GetTxdBuffer());
//}
//
//void CCommand::ExecuteReadLineSerialCommand(CSerial &serial)
//{
//  if (spIdle != Process.GetState())
//  {
//    Error.SetCode(ecCommandTimingFailure);
//    return;
//  }
//  Process.SetState(spReadLineSerialCommand);
//  // Analyse parameters ( RLC - ):
//  // Execute:
//  String Line = SerialCommand.ReadLine();
//  // Response:
//  sprintf(Command.GetTxdBuffer(), "%s %s", GetPCommand(), Line.c_str());
//  WriteResponse(Command.GetTxdBuffer());
//}
//
//#########################################################
//  Segment - Execution - I2CDisplay
//#########################################################
//
#if defined(I2CDISPLAY_ISPLUGGED)
void CCommand::ExecuteClearScreenI2CDisplay(CSerial &serial)
{
  // Analyse parameters ( CLI - ):
  // Execute:
  I2CDisplay.ClearDisplay();
  // Response:
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif

#if defined(I2CDISPLAY_ISPLUGGED)
void CCommand::ExecuteShowTextI2CDisplay(CSerial &serial)
{
  //!!!!if (GetParameterCount() < 3)
  // Analyse parameters ( STI <c> <r> <t> ):
  Byte R = atol(GetPParameters(0));
  Byte C = atol(GetPParameters(1));
  String T = GetPParameters(2);
  // Execute:
  I2CDisplay.SetCursorPosition(R, C);
  I2CDisplay.WriteText(T);
  // Response:
  sprintf(GetTxdBuffer(), "%s %i %i %s", GetPCommand(), R, C, T.c_str());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//#########################################################
//  Segment - Execution - RFIDClient
//#########################################################
//
#if defined(RFIDCLIENT_ISPLUGGED)
void CCommand::ExecuteGetRFID(CSerial &serial)
{
  // Analyse parameters: ( GID ):
  // Execute:
  Int32 ID = -9999;
  ID = ...GetRFID();
  sprintf(GetTxdBuffer(), "%s %li", GetPCommand(), ID);
  WriteResponse(GetTxdBuffer());
  }
  else
  {
    Error.SetCode(ecCommandTimingFailure);
  }  
}
#endif
//
//#########################################################
//  Segment - Execution - MotorVNH2SP30 
//#########################################################
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteGetLeftPwmLow(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GLWL -):    
  // Execute:
  UInt16 P = MotorLeft.GetPwmLow();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteSetLeftPwmLow(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SLWL <p> ):
  UInt16 P = atoi(GetPParameters(0));
  // Execute:
  MotorLeft.SetPwmLow(P);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteGetLeftPwmHigh(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GLWH - ):
  // Execute:
  UInt16 P = MotorLeft.GetPwmHigh();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteSetLeftPwmHigh(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SLWH <p> ):
  UInt16 P = atoi(GetPParameters(0));
  // Execute:
  MotorLeft.SetPwmHigh(P);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//---------------------------------------------------------
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteGetRightPwmLow(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GRWL -):    
  // Execute:
  UInt16 P = MotorRight.GetPwmLow();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteSetRightPwmLow(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SRWL <p> ):
  UInt16 P = atoi(GetPParameters(0));
  // Execute:
  MotorRight.SetPwmLow(P);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteGetRightPwmHigh(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GRWH - ):    
  // Execute:
  UInt16 P = MotorRight.GetPwmHigh();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteSetRightPwmHigh(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SRWH <p> ):
  UInt16 P = atoi(GetPParameters(0));
  // Execute:
  MotorRight.SetPwmHigh(P);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %u", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//---------------------------------------------------------
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteStopBoth(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( S - ):
  // Execute:
  MotorLeft.AbortMotion();
  MotorRight.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)            
  MotorEncoderLM393Left.SetMoveDirection(medIdle);
  MotorEncoderLM393Right.SetMoveDirection(medIdle);    
#endif
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteStopLeft(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SL - ):
  // Execute:
  MotorLeft.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
  MotorEncoderLM393Left.SetMoveDirection(medIdle);
#endif
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED)
void CCommand::ExecuteStopRight(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SR - ):
  // Execute:
  MotorRight.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
  MotorEncoderLM393Right.SetMoveDirection(medIdle);
#endif
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//---------------------------------------------------------
//
#if defined(MOTORVNH2SP30_ISPLUGGED)  
void CCommand::ExecuteMoveLeftPositive(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( MLP <v> ):
  float VL = atof(GetPParameters(0));  // [%]
  // Execute:
  if (0 == (int)VL)
  {
    MotorLeft.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medPositive);
#endif
    MotorLeft.MoveVelocityPositive(VL);
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %f", GetPCommand(), VL);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED) 
void CCommand::ExecuteMoveLeftNegative(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( MLN vl ):
  float VL = atof(GetPParameters(0));  // [1]
  // Execute:
  if (0 == (int)VL)
  {
    MotorLeft.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medIdle);
#endif
  }
  else
  {  
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medNegative);
#endif
    MotorLeft.MoveVelocityNegative(VL);
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %f", GetPCommand(), VL);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//---------------------------------------------------------
//
#if defined(MOTORVNH2SP30_ISPLUGGED)  
void CCommand::ExecuteMoveRightPositive(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( MRP vr ):
  float VR = atof(GetPParameters(0));  // [%]
  // Execute:
  if (0 == (int)VR)
  {
    MotorRight.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medPositive);
#endif
    MotorRight.MoveVelocityPositive(VR);
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %f", GetPCommand(), VR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED) 
void CCommand::ExecuteMoveRightNegative(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( MRN vr ):
  float VR = atof(GetPParameters(0));  // [1]
  // Execute:
  if (0 == (int)VR)
  {
    MotorRight.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medNegative);
#endif
    MotorRight.MoveVelocityNegative(VR);
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %f", GetPCommand(), VR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//---------------------------------------------------------
//
#if defined(MOTORVNH2SP30_ISPLUGGED)  
void CCommand::ExecuteMoveBothPositive(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( MBP vl vr ):
  float VL = atof(GetPParameters(0));  // [%]
  float VR = atof(GetPParameters(1));  // [%]
  // Execute:
  if (0 == (int)VL)
  {
    MotorLeft.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medPositive);
#endif
    MotorLeft.MoveVelocityPositive(VL);
  }
  if (0 == (int)VR)
  {
    MotorRight.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medPositive);
#endif
    MotorRight.MoveVelocityPositive(VR);
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %f %f", GetPCommand(), VL, VR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED) 
void CCommand::ExecuteMoveBothNegative(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( MBN vl vr ):
  float VL = atof(GetPParameters(0));  // [1]
  float VR = atof(GetPParameters(1));  // [1]
  // Execute:
  if (0 == (int)VL)
  {
    MotorLeft.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medNegative);
#endif
    MotorLeft.MoveVelocityNegative(VL);
  }
  if (0 == (int)VR)
  {
    MotorRight.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medNegative);
#endif
    MotorRight.MoveVelocityNegative(VR);
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %f %f", GetPCommand(), VL, VR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//---------------------------------------------------------
//
#if defined(MOTORVNH2SP30_ISPLUGGED)  
void CCommand::ExecuteRotateBothPositive(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( RBP vl vr ):
  float VL = atof(GetPParameters(0));  // [%]
  float VR = atof(GetPParameters(1));  // [%]
  // Execute:
  if (0 == (int)VL)
  {
    MotorLeft.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medNegative);
#endif
    MotorLeft.MoveVelocityNegative(VL);
  }
  if (0 == (int)VR)
  {
    MotorRight.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medPositive);
#endif
    MotorRight.MoveVelocityPositive(VR);
  }
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %f %f", GetPCommand(), VL, VR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORVNH2SP30_ISPLUGGED) 
void CCommand::ExecuteRotateBothNegative(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( RBN vl vr ):
  float VL = atof(GetPParameters(0));  // [1]
  float VR = atof(GetPParameters(1));  // [1]
  // Execute:
  if (0 == (int)VL)
  {
    MotorLeft.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Left.SetMoveDirection(medPositive);
#endif
    MotorLeft.MoveVelocityPositive(VL);
  }
  if (0 == (int)VR)
  {
    MotorRight.AbortMotion();
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medIdle);
#endif
  }
  else
  {
#if defined(MOTORENCODERLM393_ISPLUGGED)
    MotorEncoderLM393Right.SetMoveDirection(medNegative);
#endif
    MotorRight.MoveVelocityNegative(VR);
  }    
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %f %f", GetPCommand(), VL, VR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//#########################################################
//  Segment - Execution - MotorEncoderLM393
//#########################################################
//
#if defined(MOTORENCODERLM393_ISPLUGGED)
void CCommand::ExecuteGetEncoderPositionBoth(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GEPB - ):    
  // Execute:
  Int32 EPL = MotorEncoderLM393Left.GetPositionActual();
  Int32 EPR = MotorEncoderLM393Right.GetPositionActual();
  Int32 DPL = MotorEncoderLM393Left.GetDeltaPosition();
  Int32 DPR = MotorEncoderLM393Right.GetDeltaPosition();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %li %li %i %i", GetPCommand(), EPL, EPR, (int)DPL, (int)DPR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORENCODERLM393_ISPLUGGED)
void CCommand::ExecuteSetEncoderPositionLeft(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SEPL <ep> ):
  Int32 EPL = atoi(GetPParameters(0));
  // Execute:
  MotorEncoderLM393Left.SetPositionActual(EPL);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %li", GetPCommand(), EPL);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORENCODERLM393_ISPLUGGED)
void CCommand::ExecuteSetEncoderPositionRight(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SEPR <ep> ):
  Int32 EPR = atol(GetPParameters(0));
  // Execute:
  MotorEncoderLM393Right.SetPositionActual(EPR);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %li", GetPCommand(), EPR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(MOTORENCODERLM393_ISPLUGGED)
void CCommand::ExecuteWaitPositionReached(CSerial &serial)
{ 
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( WPR pl pr vt ):
  Int32 PL = atol(GetPParameters(0));
  Int32 PR = atol(GetPParameters(1));
  float VT = atof(GetPParameters(2));
  // Execute:
  MotorPositionReached.Wait_Start(PL, PR, VT);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %li %li", GetPCommand(), PL, PR);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//##############################################################
//  Segment - Execution - RF433MHzClient / RemoteWirelessSwitch
//##############################################################
//
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
void CCommand::ExecuteRemoteSwitchOn(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( RSO <t> ):
  UInt32 T = atol(GetPParameters(0));
  // Execute:
  RemoteSwitch.ForceOn(T);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu", GetPCommand(), T);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
void CCommand::ExecuteRemoteSwitchOff(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( RSF <t> ):
  UInt32 T = atol(GetPParameters(0));
  // Execute:
  RemoteSwitch.ForceOff(T);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu", GetPCommand(), T);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//##############################################################
//  Segment - Execution - LaserScanner JI
//##############################################################
//
#if defined(LASERSCANNER_JI)
void CCommand::ExecuteSetPulsePeriod(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( SPP <p> ):
  UInt32 P = atol(GetPParameters(0));
  // Execute:
  LaserScanner.SetPulsePeriod(P);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif

#if defined(LASERSCANNER_JI)
void CCommand::ExecuteGetPulsePeriod(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( GPP - ):  
  // Execute:
  UInt32 P = LaserScanner.GetPulsePeriod();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif

#if defined(LASERSCANNER_JI)
void CCommand::ExecutePulseLaserAbort(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( PLA - ):  
  // Execute:
  LaserScanner.Abort();
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s", GetPCommand());
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif

#if defined(LASERSCANNER_JI)
void CCommand::ExecutePulseLaserCount(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 2)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( PLC <p> <c> ):
  UInt32 P = atol(GetPParameters(0));
  UInt32 C = atol(GetPParameters(1));
  // Execute:
  LaserScanner.PulseLaserCount(P, C);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu", GetPCommand(), P, C);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
#if defined(LASERSCANNER_JI)
void CCommand::ExecuteMovePositionX(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( MPX <p> ):
  UInt32 P = atol(GetPParameters(0));
  // Execute:
  LaserScanner.MovePositionX(P);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif

#if defined(LASERSCANNER_JI)
void CCommand::ExecuteMovePositionY(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( MPY <p> ):
  UInt32 P = atol(GetPParameters(0));
  // Execute:
  LaserScanner.MovePositionY(P);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu", GetPCommand(), P);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif

#if defined(LASERSCANNER_JI)
void CCommand::ExecuteMovePositionPulse(CSerial &serial)
{
  if (scIdle != GetState())
  {
    Error.SetCode(ecCommandTimingFailure);
    return;
  }
  if (GetParameterCount() < 1)
  {
    Error.SetCode(ecNotEnoughParameters);
    return;
  }
  SerialCommand.WritePrompt();
  SetState(scBusy);
  // Analyse parameters ( MPP <px> <py> <pp> <pc> <dm> ):
  UInt32 PX = atol(GetPParameters(0));
  UInt32 PY = atol(GetPParameters(1));
  UInt32 PP = atol(GetPParameters(2));
  UInt32 PC = atol(GetPParameters(3));
  UInt32 DM = atol(GetPParameters(4));
  // Execute:
  LaserScanner.MovePositionPulse(PX, PY, PP, PC, DM);
  // Response
  SerialCommand.WritePrompt();
  sprintf(GetTxdBuffer(), "%s %lu %lu %lu %lu %lu", GetPCommand(), PX, PY, PP, PC, DM);
  WriteResponse(GetTxdBuffer());
  SerialCommand.WritePrompt();
  SetState(scIdle);
  SerialCommand.WritePrompt();
}
#endif
//
//##############################################################
//  Segment - Execution - LaserScanner PS
//##############################################################
//
#if defined(LASERSCANNER_PS)
void CCommand::Execute(CSerial &serial)
#endif
//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
Boolean CCommand::Execute(CSerial &serial)
{
  // debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
  // debug Serial.WriteLine(TxdBuffer);
  //  Common
  if (!strcmp(SHORT_H, GetPCommand()))
  {
    ExecuteGetHelp(serial);
    return true;
  } else
#if defined(COMMAND_SYSTEMENABLED)
  if (!strcmp(SHORT_GPH, GetPCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GSV, GetPCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GHV, GetPCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
#endif
#if defined(WATCHDOG_ISPLUGGED)
  if (!strcmp(SHORT_PWD, GetPCommand()))
  {
    ExecutePulseWatchDog(serial);
    return true;
  } else
#endif
#if defined(COMMAND_SYSTEMENABLED)
  if (!strcmp(SHORT_RSS, GetPCommand()))
  {
    ExecuteResetSystem(serial);
    return true;
  } else
  if (!strcmp(SHORT_A, GetPCommand()))
  {
    ExecuteAbortAll(serial);
//!!!!!!!!!!    ExecuteStopBoth(serial);
    return true;
  } else
#endif
  if (!strcmp(SHORT_WTR, GetPCommand()))
  {
    ExecuteWaitTimeRelative(serial);
    return true;
  } else   
#if defined(NTPCLIENT_ISPLUGGED)
  if (!strcmp(SHORT_WTA, GetPCommand()))
  {
    ExecuteWaitTimeAbsolute(serial);
    return true;
  } else   
#endif
#if defined(TRIGGERINPUT_ISPLUGGED)
  if (!strcmp(SHORT_WTI, GetPCommand()))
  {
    ExecuteWaitTriggerInput(serial);
    return true;
  } else
#endif  
  //
#if defined(NTPCLIENT_ISPLUGGED)
  // ----------------------------------
  // NTPClient
  // ----------------------------------
  if (!strcmp(SHORT_GNT, GetPCommand()))
  {
    ExecuteGetNTPClientTime(serial);
    return true;
  } else
  if (!strcmp(SHORT_GND, GetPCommand()))
  {
    ExecuteGetNTPClientDate(serial);
    return true;
  } else
#endif 
//
#if defined(SDCARD_ISPLUGGED)
  // ----------------------------------
  // SDCard
  // ----------------------------------
//  if (!strcmp(SHORT_OCF, GetPCommand()))
//  {
//    ExecuteOpenCommandFile(serial);
//    return true;
//  } else 
//  if (!strcmp(SHORT_WCF, GetPCommand()))
//  {
//    ExecuteWriteCommandFile(serial);
//    return true;
//  } else 
//  if (!strcmp(SHORT_CCF, GetPCommand()))
//  {
//    ExecuteCloseCommandFile(serial);
//    return true;
//  } else 
  if (!strcmp(SHORT_ECF, GetPCommand()))
  {
    ExecuteExecuteCommandFile(serial);
    return true;
  } else 
  if (!strcmp(SHORT_ACF, GetPCommand()))
  {
    ExecuteAbortCommandFile(serial);
    return true;
  } else
#endif
  
  // ----------------------------------
  // I2CDisplay
  // ----------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
  if (!strcmp(SHORT_CLI, GetPCommand()))
  {
    ExecuteClearScreenI2CDisplay(serial);
    return true;
  } else if (!strcmp(SHORT_STI, GetPCommand()))
  {
    ExecuteShowTextI2CDisplay(serial);
    return true;
  } else
#endif
  // ----------------------------------
  // RFIDClient
  // ----------------------------------
#if defined(RFIDCLIENT_ISPLUGGED)
  if (!strcmp(SHORT_GID, GetPCommand()))
  {
    ExecuteGetRFID(serial);
    return true;
  } else
#endif
  //
#if defined(COMMAND_SYSTEMENABLED)
  // ----------------------------------
  // LedSystem
  // ----------------------------------
  if (!strcmp(SHORT_GLS, GetPCommand()))
  {
    ExecuteGetLedSystem(serial);
    return true;
  } else if (!strcmp(SHORT_LSH, GetPCommand()))
  {
    ExecuteLedSystemOn(serial);
    return true;
  } else if (!strcmp(SHORT_LSL, GetPCommand()))
  {
    ExecuteLedSystemOff(serial);
    return true;
  } else if (!strcmp(SHORT_BLS, GetPCommand()))
  {
    ExecuteBlinkLedSystem(serial);
    return true;
  } else
#endif  
#if defined(MULTICHANNELLED_ISPLUGGED)
  if (!strcmp(SHORT_BL1, GetPCommand()))
  {
    ExecuteBlinkLedChannel1(serial);
    return true;
  } else
  if (!strcmp(SHORT_BL2, GetPCommand()))
  {
    ExecuteBlinkLedChannel2(serial);
    return true;
  } else
  if (!strcmp(SHORT_BL3, GetPCommand()))
  {
    ExecuteBlinkLedChannel3(serial);
    return true;
  } else
  if (!strcmp(SHORT_BL4, GetPCommand()))
  {
    ExecuteBlinkLedChannel4(serial);
    return true;
  } else
  if (!strcmp(SHORT_BL5, GetPCommand()))
  {
    ExecuteBlinkLedChannel5(serial);
    return true;
  } else
  if (!strcmp(SHORT_BL6, GetPCommand()))
  {
    ExecuteBlinkLedChannel6(serial);
    return true;
  } else
  if (!strcmp(SHORT_BL7, GetPCommand()))
  {
    ExecuteBlinkLedChannel7(serial);
    return true;
  } else
  if (!strcmp(SHORT_BL8, GetPCommand()))
  {
    ExecuteBlinkLedChannel8(serial);
    return true;
  } else
#endif
  // ----------------------------------
  // MotorVNH2SP30
  // ----------------------------------
#if defined(MOTORVNH2SP30_ISPLUGGED)
  if (!strcmp(SHORT_GLWL, GetPCommand()))
  {
    ExecuteGetLeftPwmLow(serial);
    return true;
  } else
  if (!strcmp(SHORT_SLWL, GetPCommand()))
  {
    ExecuteSetLeftPwmLow(serial);
    return true;
  } else
  if (!strcmp(SHORT_GLWH, GetPCommand()))
  {
    ExecuteGetLeftPwmHigh(serial);
    return true;
  } else
  if (!strcmp(SHORT_SLWH, GetPCommand()))
  {
    ExecuteSetLeftPwmHigh(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GRWL, GetPCommand()))
  {
    ExecuteGetRightPwmLow(serial);
    return true;
  } else
  if (!strcmp(SHORT_SRWL, GetPCommand()))
  {
    ExecuteSetRightPwmLow(serial);
    return true;
  } else
  if (!strcmp(SHORT_GRWH, GetPCommand()))
  {
    ExecuteGetRightPwmHigh(serial);
    return true;
  } else
  if (!strcmp(SHORT_SRWH, GetPCommand()))
  {
    ExecuteSetRightPwmHigh(serial);
    return true;
  } else
  //  
  if (!strcmp(SHORT_S, GetPCommand()))
  {
    ExecuteStopBoth(serial);
    return true;
  } else
  if (!strcmp(SHORT_SL, GetPCommand()))
  {
    ExecuteStopLeft(serial);
    return true;
  } else
  if (!strcmp(SHORT_SR, GetPCommand()))
  {
    ExecuteStopRight(serial);
    return true;
  } else
  //  
  if (!strcmp(SHORT_MLP, GetPCommand()))
  {
    ExecuteMoveLeftPositive(serial);
    return true;
  } else
  if (!strcmp(SHORT_MLN, GetPCommand()))
  {
    ExecuteMoveLeftNegative(serial);
    return true;
  } else
  //
  if (!strcmp(SHORT_MRP, GetPCommand()))
  {
    ExecuteMoveRightPositive(serial);
    return true;
  } else
  if (!strcmp(SHORT_MRN, GetPCommand()))
  {
    ExecuteMoveRightNegative(serial);
    return true;
  } else
  //
  if (!strcmp(SHORT_MBP, GetPCommand()))
  {
    ExecuteMoveBothPositive(serial);
    return true;
  } else
  if (!strcmp(SHORT_MBN, GetPCommand()))
  {
    ExecuteMoveBothNegative(serial);
    return true;
  } else
  //
  if (!strcmp(SHORT_RBP, GetPCommand()))
  {
    ExecuteRotateBothPositive(serial);
    return true;
  } else
  if (!strcmp(SHORT_RBN, GetPCommand()))
  {
    ExecuteRotateBothNegative(serial);
    return true;
  } else
  //
  if (!strcmp(SHORT_MPA, GetPCommand()))
  {
    ExecuteMovePositionAbsolute(serial);
    return true;
  } else
  if (!strcmp(SHORT_MPR, GetPCommand()))
  {
    ExecuteMovePositionRelative(serial);
    return true;
  } else
#endif
  // ----------------------------------
  // MotorEncoderLM393
  // ----------------------------------
#if defined(MOTORENCODERLM393_ISPLUGGED)
  if (!strcmp(SHORT_GEPB, GetPCommand()))
  {
    ExecuteGetEncoderPositionBoth(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SEPL, GetPCommand()))
  {
    ExecuteSetEncoderPositionLeft(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SEPR, GetPCommand()))
  {
    ExecuteSetEncoderPositionRight(serial);
    return true;
  } else   
  if (!strcmp(SHORT_WPR, GetPCommand()))
  {
    ExecuteWaitPositionReached(serial);
    return true;
  } else   
#endif
  // ----------------------------------
  // RemoteWirelessSwitch
  // ----------------------------------
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
  if (!strcmp(SHORT_RSO, GetPCommand()))
  {
    ExecuteRemoteSwitchOn(serial);
    return true;
  } else   
  if (!strcmp(SHORT_RSF, GetPCommand()))
  {
    ExecuteRemoteSwitchOff(serial);
    return true;
  } else
#endif
  // ----------------------------------
  // LaserScanner
  // ----------------------------------
#if defined(LASERSCANNER_JI)
  if (!strcmp(SHORT_SPP, GetPCommand()))
  {
    ExecuteSetPulsePeriod(serial);
    return true;
  } else
  if (!strcmp(SHORT_GPP, GetPCommand()))
  {
    ExecuteGetPulsePeriod(serial);
    return true;
  } else
  if (!strcmp(SHORT_PLA, GetPCommand()))
  {
    ExecutePulseLaserAbort(serial);
    return true;
  } else
  if (!strcmp(SHORT_PLC, GetPCommand()))
  {
    ExecutePulseLaserCount(serial);
    return true;
  } else
  if (!strcmp(SHORT_MPX, GetPCommand()))
  {
    ExecuteMovePositionX(serial);
    return true;
  } else
  if (!strcmp(SHORT_MPY, GetPCommand()))
  {
    ExecuteMovePositionY(serial);
    return true;
  } else
  if (!strcmp(SHORT_MPP, GetPCommand()))
  {
    ExecuteMovePositionPulse(serial);
    return true;
  } else
#endif
#if defined(LASERSCANNER_PS)
  if (!strcmp(SHORT_, GetPCommand()))
  {
    Execute(serial);
    return true;
  } else
#endif
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    Error.SetCode(ecInvalidCommand);
  }
  return false;
}
