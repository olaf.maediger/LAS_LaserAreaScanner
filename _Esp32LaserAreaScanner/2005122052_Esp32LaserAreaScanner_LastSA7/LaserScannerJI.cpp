//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "Defines.h"
//
#if defined(LASERSCANNER_JI)
//
#include "PinInput.h"
#include "PinOutput.h"
#include "RF433MHzClient.h"
#include "LaserScannerJI.h"
#include "TimeRelative.h"
#include "Command.h"
//
//extern CPinInput        SignalA0;
//extern CRF433MHzClient  RF433MHzTransmit;
//extern CTimeRelative    TimeRelativeSystem;
//extern CCommand         Command;
//
//-------------------------------------------------------------------------------
//  LaserScanner - Init
//-------------------------------------------------------------------------------
CLaserScanner::CLaserScanner(String id)
{
  FID = id;
  FState = slsZero;
}

Boolean CLaserScanner::Open()
{
  FState = slsZero;
  return true;
}

Boolean CLaserScanner::Close()
{
  FState = slsZero;
  return true;
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Property
//-------------------------------------------------------------------------------
EStateLaserScanner CLaserScanner::GetState(void)
{
  return FState;
}  

void CLaserScanner::SetPulsePeriod(UInt32 period)
{
  
}

UInt32 CLaserScanner::GetPulsePeriod(void)
{
  
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Handler
//-------------------------------------------------------------------------------
Boolean CLaserScanner::PulseLaserCount(UInt32 period, UInt32 count)
{
  if (slsZero == FState)
  {
    FPulsePeriodms = period;
    FPulseCountActual = 0;
    FPulseCountPreset = count;
    FState = slsInitPulseLaser;
    return true;
  }
  return false;
}

Boolean CLaserScanner::MovePositionX(UInt16 px)
{
  return false;  
}

Boolean CLaserScanner::MovePositionY(UInt16 py)
{
  return false;  
}

Boolean CLaserScanner::MovePositionPulse(UInt16 px, UInt16 PY, UInt32 PP, UInt32 PC, UInt32 DM)
{
  return false;  
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Wait
//-------------------------------------------------------------------------------
//
void CLaserScanner::Abort(void)
{
  // stop all Activities !!! ...
  // FPinPulse = LOW;
  FState = slsZero;
}

Boolean CLaserScanner::IsBusy(void)
{
  return (slsZero != FState);
}
void CLaserScanner::Execute(void)
{ 
  switch (FState)
  {
    case slsZero:
      return;
    case slsInitPulseLaser:
      FState = slsBusyPulseLaser;
      return;
    case slsBusyPulseLaser:
      
      FPulseCountActual++;
      if (FPulseCountPreset <= FPulseCountActual)
      {        
        FState = slsZero;
        return;
      }
      return;
    case slsInitMovePulse:
      FState = slsBusyMovePulse;
      return;
    case slsBusyMovePulse:
      return;
  }  
}
//
#endif // LASERSCANNER_JI
//
