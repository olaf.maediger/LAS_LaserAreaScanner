//
#include "Defines.h"
//
#if defined(SDCARD_ISPLUGGED)
//
#include "XmlFile.h"
//
bool CXmlFile::Open(fs::FS* pfs)
{
  FPFS = pfs;
  FText = "";
}
bool CXmlFile::Close(void)
{
  FText = "";
  FPFS = 0;
}
//
bool CXmlFile::ReadFile(const char* path)
{
  FText = "";
  File file = FPFS->open(path);
  if (!file) return false;  // Serial.println("Failed to open file for reading");
  while (file.available())
  {
    FText += (char)file.read();
  }
  file.close();
  return true;//Serial.write(text.c_str());  
}

String CXmlFile::GetText(void)
{
  return FText;
}

bool CXmlFile::Parse(void)
{
  return (XML_SUCCESS == FXmlDocument.Parse(FText.c_str()));
}


XMLNode* CXmlFile::GetRoot(void)
{
  return FXmlDocument.FirstChild();
}

XMLElement* CXmlFile::GetElement(XMLNode* pnode, const char* name)
{
  if (0 != pnode)
  {
    return pnode->FirstChildElement(name);
  }
}

XMLElement* CXmlFile::GetNextElement(XMLNode* pnode, const char* name)
{
  if (0 != pnode)
  {
    return pnode->NextSiblingElement(name);
  }
}
//
#endif // SDCARD_ISPLUGGED
//
