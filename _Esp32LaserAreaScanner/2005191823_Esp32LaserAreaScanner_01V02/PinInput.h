//
//--------------------------------
//  Library TriggerInput
//--------------------------------
//
#include "Defines.h"
//
#ifndef PinInput_h
#define PinInput_h
//
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const Boolean LEVEL_INVERTED = true;
const Boolean LEVEL_NOTINVERTED = false;
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EPinInputLevel
{
  pilLow   = 0,
  pilHigh  = 1
};
//
class CPinInput
{
  private:
  String FID;
  UInt16 FPin;
  EPinInputLevel FLevel;
  Boolean FInverted;
  //
  public:
  CPinInput(String id, int pin);
  CPinInput(String id, int pin, bool inverted);
  //
  EPinInputLevel GetLevel(void);
  EPinInputLevel ReadLevel(void);
  //
  Boolean Open();
  Boolean Close();
  // 
  EPinInputLevel Execute(void);
};
//
#endif // PinInput_h
//
