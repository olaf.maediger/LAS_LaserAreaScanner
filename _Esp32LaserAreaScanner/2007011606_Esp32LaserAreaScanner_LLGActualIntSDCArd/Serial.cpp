#include "Defines.h"
//
#ifdef SERIALCOMMAND_ISPLUGGED
//
#include "Serial.h"
#include "Utilities.h"
//
#if defined(NTPCLIENT_ISPLUGGED)
#include "NTPClient.h"
#endif
//
#if defined(NTPCLIENT_ISPLUGGED)
extern CNTPClient NTPClient;
#endif
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
void CSerialBase::SetRxdEcho(bool rxdecho)
{
  FRxdEcho = rxdecho;
}
 
void CSerialBase::WriteNewLine()
{
  Write(TERMINAL_NEWLINE);  
}
  
void CSerialBase::WritePrompt()
{  
#if defined(NTPCLIENT_ISPLUGGED)
  String Date, Time, Millis;  
  NTPClient.GetDateTimeMillisCompressed(Date, Time, Millis);
  Write(TERMINAL_RESPONSE); // ???????????
  Write(Date + ".");
  Write(Time + ".");
  Write(Millis);
  Write(TERMINAL_INPUT);
#else
  Write(TERMINAL_RESPONSE);
  Write(BuildTime(millis()));
  Write(TERMINAL_INPUT);
#endif  
}
  
void CSerialBase::WriteResponse()
{
  Write(TERMINAL_RESPONSE);
}
  
void CSerialBase::WriteComment()
{
  Write(TERMINAL_COMMENT);
}
//
//----------------------------------------------------
//  Segment - CSerialH 
//----------------------------------------------------
//
#if ((defined PROCESSOR_NANOR3)||(defined PROCESSOR_UNOR3)||(defined PROCESSOR_MEGA2560)||(defined PROCESSOR_DUEM3)||(defined PROCESSOR_STM32F103C8)||(defined PROCESSOR_TEENSY32)||(defined PROCESSOR_TEENSY36)||(defined PROCESSOR_ESP8266))
CSerialH::CSerialH(HardwareSerial *serial)
{
  FPSerial = serial;
} 

bool CSerialH::Open(UInt32 baudrate)
{
  FPSerial->begin(baudrate);
  FIsOpen = true;
  return FIsOpen;
}
//
bool CSerialH::Close()
{
  FRxdEcho = false;
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}

int CSerialH::GetRxdByteCount()
{
  return FPSerial->available();
}

char CSerialH::ReadCharacter()
{
  char C = (char)FPSerial->read();
  if (FRxdEcho && (isalnum(C) || (' ' == C)))
  {
    FPSerial->write(C);
  }
  return C;
}

String CSerialH::ReadLine()
{
  return "";
}

void CSerialH::Write(const char* text)
{  
  FPSerial->write(text); 
}

void CSerialH::Write(String text)
{
  FPSerial->write(text.c_str());
} 
#endif 
//
//----------------------------------------------------
//  Segment - CSerialU
//----------------------------------------------------
//
#if ((defined PROCESSOR_TEENSY32) || (defined PROCESSOR_TEENSY36))
bool CSerialU::Open(UInt32 baudrate)
{
  FRxdEcho = INIT_RXDECHO;
  FPSerial->begin(baudrate);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialU::Close()
{
  FRxdEcho = RXDECHO_OFF;
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}
 
int CSerialU::GetRxdByteCount()
{
  return FPSerial->available();
}

char CSerialU::ReadCharacter()
{
  char C = (char)FPSerial->read();
  if (FRxdEcho && (isalnum(C) || (' ' == C)))
  {
    FPSerial->write(C);
  }
  return C;
}

String CSerialU::ReadLine()
{
  return "";
}

void CSerialU::Write(const char* text)
{  
  FPSerial->write(text); 
}

void CSerialU::Write(String text)
{
  FPSerial->write(text.c_str());
}  
#endif  
//
//----------------------------------------------------
//  Segment - CSerialS
//----------------------------------------------------
//
#if (defined PROCESSOR_ESP32)
CSerialPS::CSerialPS(HardwareSerial *serial, int pinrxd, int pintxd)
{
  FPSerial = serial;
  FPinRXD = pinrxd;
  FPinTXD = pintxd;
} 

bool CSerialPS::Open(UInt32 baudrate)
{ //                        !!!!!!!!!!
  FPSerial->begin(baudrate, SERIAL_8N1, FPinRXD, FPinTXD);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialPS::Close()
{
  FRxdEcho = false;
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}

int CSerialPS::GetRxdByteCount()
{
  return FPSerial->available();
}

char CSerialPS::ReadCharacter()
{
  char C = (char)FPSerial->read();
  if (FRxdEcho && (isalnum(C) || (' ' == C)))
  {
    FPSerial->write(C);
  }
  return C;
}

String CSerialPS::ReadLine()
{
  return "";
}

void CSerialPS::Write(const char* text)
{  
  FPSerial->write(text); 
}

void CSerialPS::Write(String text)
{
  FPSerial->write(text.c_str());
} 
#endif // PROCESSOR_ESP32

//class CSerialPS : public CSerialBase
//{
//  protected:
//  HardwareSerial* FPSerial;
//  int FPinRXD, FPinTXD;
//  //  
//  public:
//  CSerialPS(HardwareSerial *serial, int pinrxd, int pintxd);
//  bool Open(UInt32 baudrate);
//  bool Close();
//  int GetRxdByteCount();
//  char ReadCharacter();
//  String ReadLine();
//  void Write(const char* text);
//  void Write(String text);
//};
//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
#ifdef PROCESSOR_NANOR3
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif // PROCESSOR_NANOR3

#ifdef PROCESSOR_UNOR3
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif // PROCESSOR_UNOR3

#ifdef PROCESSOR_MEGA2560
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif // PROCESSOR_MEGA2560

#ifdef PROCESSOR_DUEM3
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif // PROCESSOR_DUEM3

#ifdef PROCESSOR_STM32F103C8
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif // PROCESSOR_STM32F103C8

#if (defined PROCESSOR_TEENSY32)
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(usb_serial_class *serial)
{
  CSerialU *PSerialU = (CSerialU*)new CSerialU(serial);
  FPSerial = PSerialU;
}
//
CSerial::CSerial(usb_serial_class &serial)
{
  CSerialU *PSerialU = (CSerialU*)new CSerialU(&serial);
  FPSerial = PSerialU; 
}
#endif // PROCESSOR_TEENSY32
//
#if (defined PROCESSOR_ESP8266)
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}
//
CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif // PROCESSOR_ESP8266
//
#if (defined PROCESSOR_ESP32)
CSerial::CSerial(HardwareSerial *serial, int pinrxd, int pintxd)
{
  CSerialPS *PSerialPS = (CSerialPS*)new CSerialPS(serial, pinrxd, pintxd);
  FPSerial = (CSerialBase*)PSerialPS;
}
//
//CSerial::CSerial(HardwareSerial &serial, int pinrxd, int pintxd)
//{
//  CSerialPS *PSerialPS = (CSerialPS*)new CSerialPS(&serial);
//  FPSerial = (CSerialBase*)PSerialPS;
//}
#endif // PROCESSOR_ESP32
//
//----------------------------------------------
//
#if ((defined PROCESSOR_NANOR3)||(defined PROCESSOR_UNOR3)||(defined PROCESSOR_MEGA2560)||(defined PROCESSOR_DUEM3)||(defined PROCESSOR_STM32F103C8)||(defined PROCESSOR_TEENSY32)||(defined PROCESSOR_TEENSY36)||(defined PROCESSOR_ESP8266)||(defined PROCESSOR_ESP32))
bool CSerial::Open(UInt32 baudrate)
{
  return FPSerial->Open(baudrate);
}
#endif

bool CSerial::Close()
{
  return FPSerial->Close();
}

void CSerial::SetRxdEcho(bool rxdecho)
{
  FPSerial->SetRxdEcho(rxdecho);
}
//
//----------------------------------------------
//
char CSerial::ReadCharacter()
{
  char C = (char)FPSerial->ReadCharacter();
  return C;
}

String CSerial::ReadLine()
{
  String Line = (String)FPSerial->ReadLine();
  return Line;
}

int CSerial::GetRxdByteCount()
{
  return FPSerial->GetRxdByteCount();
}
//
//----------------------------------------------
//
void CSerial::Write(char character)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%c", character);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char *text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s", text);
  FPSerial->Write(Buffer);
}

void CSerial::Write(String text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s", text.c_str());
  FPSerial->Write(Buffer);
}

void CSerial::Write(UInt8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%u", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(UInt16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%u", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(UInt32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%lu", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(Int8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%i", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(Int16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%i", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(Int32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%li", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(Float32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%f", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(Double64 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%lf", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteByte(UInt8 value)
{
  char Buffer[6];
  sprintf(Buffer, "0x%02X", value);
  FPSerial->Write(Buffer);
}

void CSerial::WriteWord(UInt16 value)
{
  char Buffer[8];
  sprintf(Buffer, "0x%04X", value);
  FPSerial->Write(Buffer);
}

void CSerial::WriteQuad(UInt32 value)
{
  char Buffer[16];
  sprintf(Buffer, "0x%08lX", value);
  FPSerial->Write(Buffer);
}
//
//----------------------------------------------
//
void CSerial::WriteLine(void)
{
  FPSerial->Write("\r\n");
}

void CSerial::WriteLine(const char *text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", text);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(String text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", text.c_str());
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(UInt8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%u\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(UInt16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%u\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(UInt32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%lu\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(Int8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%i\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(Int16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%i\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(Int32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%li\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(Float32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%f\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(Double64 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%lf\r\n", number);
  FPSerial->Write(Buffer);
}
//
//----------------------------------------------
//
void CSerial::Write(const char* mask, char character)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, character);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, const char *text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, text);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, String text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, text.c_str());
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, UInt8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, UInt16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, UInt32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, Int8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, Int16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, Int32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, Float32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, Double64 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}
//
//----------------------------------------------
//
void CSerial::WriteLine(const char* mask, const char *text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, text);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, String text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, text.c_str());
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, UInt8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, UInt16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, UInt32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, Int8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, Int16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, Int32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, Float32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, Double64 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}
//
//----------------------------------------------
//
void CSerial::WriteNewLine()
{
  FPSerial->WriteNewLine();
}

void CSerial::WritePrompt()
{
  FPSerial->WritePrompt();
}

void CSerial::WriteEvent(String line)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s %s", TERMINAL_EVENT, line.c_str());
  WriteLine(Buffer);
}

void CSerial::WriteResponse(void)
{
  FPSerial->WriteResponse();
}
void CSerial::WriteResponse(String line)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s %s", TERMINAL_RESPONSE, line.c_str());
  WriteLine(Buffer);
}

void CSerial::WriteComment()
{
  FPSerial->WriteComment();
}
void CSerial::WriteComment(String line)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s %s", TERMINAL_COMMENT, line.c_str());
  WriteLine(Buffer);
}

void CSerial::WriteDebug(String line)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s %s", TERMINAL_DEBUG, line.c_str());
  WriteLine(Buffer);
}

void CSerial::WriteCommand(String command, Character index, String parameter1, String parameter2)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s %c %s %s", command.c_str(), index, parameter1.c_str(), parameter2.c_str());
  WriteLine(Buffer);
}

void CSerial::WriteTime()
{
  long unsigned Time = millis();
  // -> Utilitis! String STime = FPSerial->BuildTime(Time);
  String STime = BuildTime(Time);
  FPSerial->Write(STime);
}
//
#endif // SERIALCOMMAND_ISPLUGGED
