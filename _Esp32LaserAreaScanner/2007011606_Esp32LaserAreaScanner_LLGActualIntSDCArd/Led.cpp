//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "Led.h"
//
// -> CB #include "Command.h"
//
// -> CB extern CCommand Command;
//
CLed::CLed(int pin, char id)
{
  FPin = pin;
  FID = id;
  FInverted = false;
  FState = slUndefined;
  FPBuffer = new char[32];
  //
  FStateBlink = slbZero;
  FPulseCountPreset = INIT_PULSECOUNT;
  FPulseCountActual = INIT_PULSECOUNT;
  FPulsePeriodms = INIT_PULSEPERIODMS;
  FPulseWidthms =  INIT_PULSEWIDTHMS;
}

CLed::CLed(int pin, char id, bool inverted)
{
  FPin = pin;
  FID = id;
  FInverted = inverted;
  FState = slUndefined;
  //
  FStateBlink = slbZero;
  FPulseCountPreset = INIT_PULSECOUNT;
  FPulseCountActual = INIT_PULSECOUNT;
  FPulsePeriodms = INIT_PULSEPERIODMS;
  FPulseWidthms =  INIT_PULSEWIDTHMS;
}

EStateLed CLed::GetState(void)
{
  return FState;
}  

Boolean CLed::IsInverted(void)
{
  return FInverted;
}

Boolean CLed::Open()
{
  pinMode(FPin, OUTPUT);
  SetOff();
  FState = slOff;
  return true;
}

Boolean CLed::Close()
{
  SetOff();
  pinMode(FPin, INPUT);
  FState = slUndefined;
  return true;
}

void CLed::SetOn()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FState = slOn;
}

void CLed::SetOff()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FState = slOff;
}

void CLed::Toggle(void)
{
  if (slOn == FState)
  {
    SetOff();
  }
  else
  {
    SetOn();
  }
}
//
//-------------------------------------------------------------------------------
//  Led - Process Blink
//-------------------------------------------------------------------------------
//
EStateLedBlink CLed::GetStateBlink(void)
{
  return FStateBlink;
}  

UInt32 CLed::GetPulsePeriodms(void)
{
  return FPulsePeriodms;
}

UInt32 CLed::GetPulseWidthms(void)
{
  return FPulseWidthms;
}

UInt32 CLed::GetPulseCountPreset(void)
{
  return FPulseCountPreset;  
}

UInt32 CLed::GetPulseCountActual(void)
{
  return FPulseCountActual;  
}

void CLed::Blink_Start(UInt32 pulsecount, UInt32 pulseperiodms, UInt32 pulsewidthms)
{ // bls 10 100 90
  FPulseCountPreset = pulsecount;
  FPulsePeriodms = pulseperiodms;
  FPulseWidthms = pulsewidthms;
  FStateBlink = slbInit;
}

void CLed::Blink_Abort(void)
{
  SetOff();
  FStateBlink = slbZero;
}
//
void CLed::Blink_Execute(void)
{
  switch (FStateBlink)
  {
    case slbZero:
      break;
    case slbInit:
      FTimeStartms = millis();
      SetOn();
      // -> CB sprintf(FPBuffer, "SBL%c Init", FID);
      // -> CB Command.WriteEvent(FPBuffer);
      //
      FPulseCountActual = 1;
      // -> CB sprintf(FPBuffer, "BL%c %lu %lu 1", FID, FPulseCountActual, FPulseCountPreset);
      // -> CB Command.WriteEvent(FPBuffer);
      FStateBlink = slbHigh;
      break;
    case slbHigh:
      if (FPulseWidthms < (millis() - FTimeStartms))
      {
        SetOff();
        // -> CB sprintf(FPBuffer, "BL%c %lu %lu 0", FID, FPulseCountActual, FPulseCountPreset);
        // -> CB Command.WriteEvent(FPBuffer);
        FStateBlink = slbLow;
      }
      break;
    case slbLow:
      if (FPulsePeriodms < (millis() - FTimeStartms))
      {
        if (FPulseCountActual < FPulseCountPreset)
        {
          FPulseCountActual++;
          SetOn();
          FTimeStartms = millis();
          // -> CB sprintf(FPBuffer, "BL%c %lu %lu 1", FID, FPulseCountActual, FPulseCountPreset);
          // -> CB Command.WriteEvent(FPBuffer);
          FStateBlink = slbHigh;
        }
        else
        {
          // -> CB sprintf(FPBuffer, "SBL%c Zero", FID);
          // -> CB Command.WriteEvent(FPBuffer);
          FStateBlink = slbZero;
        }
      }
      break;
  }
}
