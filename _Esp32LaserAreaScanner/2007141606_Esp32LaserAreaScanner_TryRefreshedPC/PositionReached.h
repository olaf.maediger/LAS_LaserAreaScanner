//
//--------------------------------
//  Library PositionReached
//--------------------------------
//
#ifndef PositionReached_h
#define PositionReached_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const Int32   INIT_POSITION_STP =  0;     //[stp]
const Float32 INIT_VELOCITY_PRC = 10.0f;  // [%]
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStatePositionReached
{
  sprZero = 0,
  sprInit = 1,
  sprBusy = 2
};
//
class CPositionReached
{
  private:
  Character FID;
  EStatePositionReached FState;
  Int32 FPositionActualLeft, FPositionActualRight;
  Int32 FPositionTargetLeft, FPositionTargetRight;
  Float32 FVelocityTarget;
  //
  public:
  CPositionReached(char id);
  //
  EStatePositionReached GetState(void);
  //
  Boolean Open();
  Boolean Close();
  // 
  Int32 GetPositionActualLeftstp(void);
  Int32 GetPositionActualRightstp(void);
  //
  void Wait_Start(Int32 positiontargetleft, 
                  Int32 positiontargetright,
                  Float32 velocitytarget);
  void Wait_Abort(void);
  Boolean Wait_Execute(void);
};
//
#endif // PositionReached_h
//
