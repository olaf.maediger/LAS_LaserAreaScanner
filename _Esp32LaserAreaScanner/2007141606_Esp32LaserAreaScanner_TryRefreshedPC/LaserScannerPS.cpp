//
//--------------------------------
//  Library LaserScanner
//--------------------------------
//
#include "Defines.h"
//
#if defined(LASERSCANNER_PS)
//
#include "PinInput.h"
#include "PinOutput.h"
#include "RF433MHzClient.h"
#include "LaserScanner.h"
#include "TimeRelative.h"
#include "Command.h"
//
//extern CPinInput        SignalA0;
//extern CRF433MHzClient  RF433MHzTransmit;
//extern CTimeRelative    TimeRelativeSystem;
//extern CCommand         Command;
//
CLaserScanner::CLaserScanner(String id)
{
  FID = id;
  FStateLaserScanner = srsZero;
  FStateForceSwitch = INIT_STATEFORCESWITCH;
  FSwitchOn = INIT_SWITCHON;
}

EStateLaserScanner CLaserScanner::GetStateLaserScanner(void)
{
  return FStateLaserScanner;
}  

EStateForceSwitch CLaserScanner::GetStateForceSwitch(void)
{
  return FStateForceSwitch;
}  

Boolean CLaserScanner::GetSwitchOn(void)
{
  return FSwitchOn;
}

Boolean CLaserScanner::Open()
{
  FStateLaserScanner = srsZero;
  FStateForceSwitch = INIT_STATEFORCESWITCH;
  return true;
}

Boolean CLaserScanner::Close()
{
  FStateLaserScanner = srsZero;
  FStateForceSwitch = INIT_STATEFORCESWITCH;
  return true;
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Handler
//-------------------------------------------------------------------------------
void CLaserScanner::ForceOn(UInt32 ontime)
{
  FStateForceSwitch = sfsForceOn;
//!!!  TimeRelativeSystem.Wait_Start(ontime);
}

void CLaserScanner::ForceOff(UInt32 offtime) 
{
  FStateForceSwitch = sfsForceOff;
//!!!  TimeRelativeSystem.Wait_Start(offtime);
}
//
//-------------------------------------------------------------------------------
//  LaserScanner - Wait
//-------------------------------------------------------------------------------
//
void CLaserScanner::Execute(void)
{ 
  switch (FStateLaserScanner)
  {
    case srsZero:
      FStateLaserScanner = srsInit;
      break;
    case srsInit:
      FStateLaserScanner = srsBusy;
      break;
    case srsBusy:
//      //--------------------------------------- 
//      // Read all Signal-Level:
//      EPinInputLevel ILA2 = SignalA2.Execute();
//      EPinInputLevel ILA1 = SignalA1.Execute();
//      EPinInputLevel ILA0 = SignalA0.Execute();
//      EPinInputLevel ILB2 = SignalB2.Execute();
//      EPinInputLevel ILB1 = SignalB1.Execute();
//      EPinInputLevel ILB0 = SignalB0.Execute();
//      //--------------------------------------- 
//      // Check GroupA if any active:
//      FSwitchOn = (pilHigh == ILA2) || (pilHigh == ILA1) || (pilHigh == ILA0);
//      // Check GroupB:  
//      FSwitchOn |= (ILB2 == ILB0); // Error: B0 must differ from B2
//      FSwitchOn |= (pilHigh == ILB2); // Shutter ON from GroupB
//      FSwitchOn |= (pilHigh == ILB1); // Error from GroupB
      //--------------------------------------- 
      // Execute Result;
      switch (FStateForceSwitch)
      {
        case sfsForceOn:
//            RF433MHzTransmit.SwitchOn();
//            if (!TimeRelativeSystem.Wait_Execute())
//            {
//              FStateForceSwitch = sfsPassive;
//            }
          break;
        case sfsForceOff:
//            RF433MHzTransmit.SwitchOff();
//            if (!TimeRelativeSystem.Wait_Execute())
//            {
//              FStateForceSwitch = sfsPassive;
//            }
          break;
        default: // sfsPassive
//          if (FSwitchOn)
//          {  
//            RF433MHzTransmit.SwitchOn();
//          }
//          else
//          {
//            RF433MHzTransmit.SwitchOff();     
//          }    
          break;
      }
      break;
  }  
}
//
#endif // LASERSCANNER_PS
//
