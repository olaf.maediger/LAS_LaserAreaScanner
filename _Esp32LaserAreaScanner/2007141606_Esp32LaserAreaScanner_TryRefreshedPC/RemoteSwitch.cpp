//
//--------------------------------
//  Library RemoteSwitch
//--------------------------------
//
#include "Defines.h"
//
#if (defined(RF433MHZCLIENT_ISPLUGGED) && defined(REMOTEWIRELESSSWITCH_ISPLUGGED))
//
#include "PinInput.h"
#include "PinOutput.h"
#include "RF433MHzClient.h"
#include "RemoteSwitch.h"
#include "TimeRelative.h"
#include "Command.h"
//
extern CPinInput        SignalA0;
extern CPinInput        SignalA1;
extern CPinInput        SignalA2;
extern CPinInput        SignalB0;
extern CPinInput        SignalB1;
extern CPinInput        SignalB2;
extern CRF433MHzClient  RF433MHzTransmit;
extern CTimeRelative    TimeRelativeSystem;
extern CCommand         Command;
//
CRemoteSwitch::CRemoteSwitch(String id)
{
  FID = id;
  FStateRemoteSwitch = srsZero;
  FStateForceSwitch = INIT_STATEFORCESWITCH;
  FSwitchOn = INIT_SWITCHON;
}

EStateRemoteSwitch CRemoteSwitch::GetStateRemoteSwitch(void)
{
  return FStateRemoteSwitch;
}  

EStateForceSwitch CRemoteSwitch::GetStateForceSwitch(void)
{
  return FStateForceSwitch;
}  

Boolean CRemoteSwitch::GetSwitchOn(void)
{
  return FSwitchOn;
}

Boolean CRemoteSwitch::Open()
{
  FStateRemoteSwitch = srsZero;
  FStateForceSwitch = INIT_STATEFORCESWITCH;
  return true;
}

Boolean CRemoteSwitch::Close()
{
  FStateRemoteSwitch = srsZero;
  FStateForceSwitch = INIT_STATEFORCESWITCH;
  return true;
}
//
//-------------------------------------------------------------------------------
//  RemoteSwitch - Handler
//-------------------------------------------------------------------------------
void CRemoteSwitch::ForceOn(UInt32 ontime)
{
  FStateForceSwitch = sfsForceOn;
  TimeRelativeSystem.Wait_Start(ontime);
}

void CRemoteSwitch::ForceOff(UInt32 offtime) 
{
  FStateForceSwitch = sfsForceOff;
  TimeRelativeSystem.Wait_Start(offtime);
}
//
//-------------------------------------------------------------------------------
//  RemoteSwitch - Wait
//-------------------------------------------------------------------------------
//
void CRemoteSwitch::Execute(void)
{ 
  switch (FStateRemoteSwitch)
  {
    case srsZero:
      FStateRemoteSwitch = srsInit;
      break;
    case srsInit:
      FStateRemoteSwitch = srsBusy;
      break;
    case srsBusy:
      //--------------------------------------- 
      // Read all Signal-Level:
      EPinInputLevel ILA2 = SignalA2.Execute();
      EPinInputLevel ILA1 = SignalA1.Execute();
      EPinInputLevel ILA0 = SignalA0.Execute();
      EPinInputLevel ILB2 = SignalB2.Execute();
      EPinInputLevel ILB1 = SignalB1.Execute();
      EPinInputLevel ILB0 = SignalB0.Execute();
      //--------------------------------------- 
      // Check GroupA if any active:
      FSwitchOn = (pilHigh == ILA2) || (pilHigh == ILA1) || (pilHigh == ILA0);
      // Check GroupB:  
      FSwitchOn |= (ILB2 == ILB0); // Error: B0 must differ from B2
      FSwitchOn |= (pilHigh == ILB2); // Shutter ON from GroupB
      FSwitchOn |= (pilHigh == ILB1); // Error from GroupB
      //--------------------------------------- 
      // Execute Result;
      switch (FStateForceSwitch)
      {
        case sfsForceOn:
            RF433MHzTransmit.SwitchOn();
            if (!TimeRelativeSystem.Wait_Execute())
            {
              FStateForceSwitch = sfsPassive;
            }
          break;
        case sfsForceOff:
            RF433MHzTransmit.SwitchOff();
            if (!TimeRelativeSystem.Wait_Execute())
            {
              FStateForceSwitch = sfsPassive;
            }
          break;
        default: // sfsPassive
          if (FSwitchOn)
          {  
            RF433MHzTransmit.SwitchOn();
          }
          else
          {
            RF433MHzTransmit.SwitchOff();     
          }    
          break;
      }
      break;
  }  
}
// BLS 10 100 50
#endif // RF433MHZCLIENT_ISPLUGGED && REMOTEWIRELESSSWITCH_ISPLUGGED
//
