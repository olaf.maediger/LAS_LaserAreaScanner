//
//--------------------------------
//  Library DacInternal
//--------------------------------
//
#include "Defines.h"
//
#if defined(DACINTERNAL_ISPLUGGED)
//
#include "DacInternal.h"
//#include "Command.h"
//
//extern CCommand Command;
//
CDacInternal::CDacInternal(String id, UInt8 pin, UInt16 initlevel)
{
  FID = id;
  FPin = pin;
  FLevel = initlevel;
}

Boolean CDacInternal::Open()
{
  WriteLevel(FLevel);
  return true;
}

Boolean CDacInternal::Close()
{
  pinMode(FPin, INPUT);
  return true;
}



UInt16 CDacInternal::GetLevel(void)
{
  return FLevel;
}  

void CDacInternal::WriteLevel(UInt16 level)
{
  FLevel = level;
  //analogWriteResolution(12);
  //analogWrite(FPin, FLevel);
  dacWrite(FPin, FLevel); 
}  

//
//-------------------------------------------------------------------------------
//  DacInternal - Execute
//-------------------------------------------------------------------------------
void CDacInternal::Execute(void)
{ // nothing...
  return;
}
//
#endif // DACINTERNAL_ISPLUGGED
//
