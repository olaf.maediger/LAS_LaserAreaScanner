//
//--------------------------------
//  Library DacInternal
//--------------------------------
//
#include "Defines.h"
//
#if defined(DACMCP4725_ISPLUGGED)
//
#ifndef DacMCP4725_h
#define DacMCP4725_h
//
#include "Arduino.h"
#include <Wire.h>
//
#define INIT_DAC_FULL 4095
#define INIT_DAC_HALF 2047
#define INIT_DAC_ZERO    0
//
#define MCP4726_CMD_WRITEDAC 0x40
//
class CDacMCP4725
{
  private:
  String FID;
  UInt8 FI2CAddress;
  UInt16 FLevel;
  //
  public:
  CDacMCP4725(String id, UInt8 i2caddress, UInt16 initlevel);
  void Open(void);
  void Close(void);
  void WriteLevel(UInt16 level);
};
//
#endif // DacMCP4725_h
//
#endif // DACMCP4725_ISPLUGGED
//
