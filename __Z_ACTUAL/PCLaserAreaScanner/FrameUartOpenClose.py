#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameUartOpenClose
# ------------------------------------------------------------------
#   Version: 02V03
#   Date   : 200617
#   Time   : 0820
#   Author : OMDevelop
#
import tkinter as tk
from tkinter import ttk
import Initdata as ID
import UartClient
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION = "FrameUartOpenClose"
NAME_OPENSERIAL = "OpenSerial"
NAME_COMPORT = "ComPort"
#
INIT_OPENSERIAL = "False"
INIT_COMPORT = "COM1"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
class CFrameUartOpenClose(tk.Frame):
    """CFrameUartOpenClose: ..."""

#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        """CFrameUartOpenClose.__init__: ..."""
        tk.Frame.__init__(self, window, width = 880, height = 520, \
                          padx = 6, pady = 6)#, background = "#77ee77")
        self.grid_propagate(0)
        #
        self.lblHeaderSerialPort = tk.Label(self, text = "FrameOCS")
        self.lblHeaderSerialPort.grid(sticky = tk.W, row = 0, column = 0, \
                                      padx = 6, pady = 6)
        #
        self.lblHeaderSerialPort = tk.Label(self, text = "Serial Ports")
        self.lblHeaderSerialPort.grid(sticky = tk.W, row = 0, column = 0, \
                                      padx = 6, pady = 6)
        self.cbxSerialPorts = ttk.Combobox(self, state="readonly")
        self.cbxSerialPorts.grid(sticky = tk.W, row = 0, column = 1, padx = 6, pady = 6)
        self.btnRefreshSerialPorts = tk.Button(self, text = "Refresh Serial Ports",\
                                               command = self.OnRefreshSerialPorts)
        self.btnRefreshSerialPorts.grid(sticky = tk.W, row = 0, column = 2, \
                                        padx = 6, pady = 6)
        #
        self.lblHeaderBaudrate = tk.Label(self, text = "Baudrate [bps]")
        self.lblHeaderBaudrate.grid(sticky = tk.W, row = 1, column = 0, \
                                    padx = 6, pady = 6)
        self.cbxBaudrates = ttk.Combobox(self, state="readonly")
        self.cbxBaudrates.grid(sticky = tk.W, row = 1, column = 1, padx = 6, pady = 6)
        #
        self.lblHeaderParity = tk.Label(self, text = "Parity []")
        self.lblHeaderParity.grid(sticky = tk.W, row = 2, column = 0, \
                                    padx = 6, pady = 6)
        self.cbxParities = ttk.Combobox(self, state="readonly")
        self.cbxParities.grid(sticky = tk.W, row = 2, column = 1, padx = 6, pady = 6)
        #
        self.lblHeaderDatabits = tk.Label(self, text = "Databits []")
        self.lblHeaderDatabits.grid(sticky = tk.W, row = 3, column = 0, \
                                    padx = 6, pady = 6)
        self.cbxDatabits = ttk.Combobox(self, state="readonly")
        self.cbxDatabits.grid(sticky = tk.W, row = 3, column = 1, \
                              padx = 6, pady = 6)
        #
        self.lblHeaderStopbits = tk.Label(self, text = "Stopbits []")
        self.lblHeaderStopbits.grid(sticky = tk.W, row = 4, column = 0, \
                                    padx = 6, pady = 6)
        self.cbxStopbits = ttk.Combobox(self, state="readonly")
        self.cbxStopbits.grid(sticky = tk.W, row = 4, column = 1, padx = 6, pady = 6)
        #
        self.lblHeaderHandshakes = tk.Label(self, text = "Handshakes []")
        self.lblHeaderHandshakes.grid(sticky = tk.W, row = 4, column = 0, \
                                      padx = 6, pady = 6)
        self.cbxHandshakes = ttk.Combobox(self, state="readonly")
        self.cbxHandshakes.grid(sticky = tk.W, row = 4, column = 1, padx = 6, pady = 6)
        #
        self.btnOpenClose = tk.Button(self, text = "Open", width = 32, \
                                      command = self.OnOpenClose)
        self.btnOpenClose.grid(row = 5, column = 0, columnspan = 3, padx = 4, pady = 4)
        #
        self.cbxBaudrates["values"] = UartClient.BAUDRATES
        self.cbxBaudrates.set(UartClient.BAUDRATES[UartClient.INDEX_BAUDRATE_DEFAULT])
        #
        self.cbxParities["values"] = UartClient.PARITIES
        self.cbxParities.set(UartClient.PARITIES[UartClient.INDEX_PARITY_DEFAULT])
        #
        self.cbxDatabits["values"] = UartClient.DATABITS
        self.cbxDatabits.set(UartClient.DATABITS[UartClient.INDEX_DATABITS_DEFAULT])
        #
        self.cbxStopbits["values"] = UartClient.STOPBITS
        self.cbxStopbits.set(UartClient.STOPBITS[UartClient.INDEX_STOPBITS_DEFAULT])
        #
        self.cbxHandshakes["values"] = UartClient.HANDSHAKES
        self.cbxHandshakes.set(UartClient.HANDSHAKES[UartClient.INDEX_HANDSHAKE_DEFAULT])
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def IsOpen(self):
        return ("Close" == self.btnOpenClose["text"])
    #
    def GetComPort(self):
        return self.cbxSerialPorts.get()
    #
    def GetBaudrate(self):
        return self.cbxBaudrates.get()
    #
    def GetParity(self):
        return self.cbxParities.get()
    #
    def GetDatabits(self):
        return self.cbxDatabits.get()
    #
    def GetStopbits(self):
        return self.cbxStopbits.get()
    #
    def GetHandshake(self):
        return self.cbxHandshakes.get()
    #
    def SetListSerialPorts(self, listports):
        """SetListSerialPorts: ..."""
        self.cbxSerialPorts["values"] = listports
        LastIndex = len(listports) - 1
        if (0 <= LastIndex):
            self.cbxSerialPorts.set(listports[LastIndex])
    #
    def SetCBOnSerialOpen(self, cbonserialopen):
        self.CBOnSerialOpen = cbonserialopen
    #
    def SetCBOnSerialClose(self, cbonserialclose):
        self.CBOnSerialClose = cbonserialclose
    #
    def SetCBOnRefreshSerialPorts(self, oncbrefreshserialports):
        self.CBOnRefreshSerialPorts = oncbrefreshserialports
    #
    #-----------------------------------------------------------------------------------
    # Callback
    #-----------------------------------------------------------------------------------
    def OnRefreshSerialPorts(self):
        if (None != self.CBOnRefreshSerialPorts):
            self.CBOnRefreshSerialPorts()
    #
    def OnOpenClose(self):
        if ("Open" == self.btnOpenClose["text"]):
            self.OnSerialOpen()
        else:
            self.OnSerialClose()
    #
    def OnSerialOpen(self):
        """OnSerialOpen: ..."""
        if (None != self.CBOnSerialOpen):
            Result = self.CBOnSerialOpen(self.GetComPort(), self.GetBaudrate(),\
                                         self.GetParity(), self.GetDatabits(),\
                                         self.GetStopbits(), self.GetHandshake())
            if (True == Result):
                self.btnOpenClose["text"] = "Close"
                # disable
                self.cbxSerialPorts["state"] = tk.DISABLED
                self.btnRefreshSerialPorts["state"] = tk.DISABLED
                self.cbxBaudrates["state"] = tk.DISABLED
                self.cbxParities["state"] = tk.DISABLED
                self.cbxDatabits["state"] = tk.DISABLED
                self.cbxStopbits["state"] = tk.DISABLED
                self.cbxHandshakes["state"] = tk.DISABLED
                #
    #
    def OnSerialClose(self):
        """OnOpenClose: ..."""
        if (None != self.CBOnSerialClose):
            Result = self.CBOnSerialClose(self.GetComPort())
            if (Result):
                self.btnOpenClose["text"] = "Open"
                # enable
                self.cbxSerialPorts["state"] = tk.NORMAL
                self.btnRefreshSerialPorts["state"] = tk.NORMAL
                self.cbxBaudrates["state"] = tk.NORMAL
                self.cbxParities["state"] = tk.NORMAL
                self.cbxDatabits["state"] = tk.NORMAL
                self.cbxStopbits["state"] = tk.NORMAL
                self.cbxHandshakes["state"] = tk.NORMAL
                #
                self.OnRefreshSerialPorts()
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        """: ..."""
        CP = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_COMPORT, INIT_COMPORT)
        BOS = ID.StringBoolean(readinitdata.ReadValueInit(INITDATA_SECTION, \
                                                          NAME_OPENSERIAL, \
                                                          INIT_OPENSERIAL))
        if (CP in self.cbxSerialPorts["values"]):
            self.cbxSerialPorts.set(CP)
        if (True == BOS):
            self.OnSerialOpen()
        else:
            self.OnSerialClose()
    #
    def WriteInitdata(self, writeinitdata):
        """: ..."""
        writeinitdata.WriteSection(INITDATA_SECTION)
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_COMPORT, \
                                 str(self.GetComPort()))
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_OPENSERIAL, \
                                 ID.BooleanString(self.IsOpen()))
    #

