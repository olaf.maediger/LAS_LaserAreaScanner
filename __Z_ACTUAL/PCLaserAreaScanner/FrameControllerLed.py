#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameControllerLed
# ------------------------------------------------------------------
#   Version: 02V03
#   Date   : 200617
#   Time   : 0820
#   Author : OMDevelop
#
"""project: ..."""
#
import tkinter as tk
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION    = "FrameControllerLed"
NAME_COUNT          = "Count"
NAME_PERIOD_MS      = "Period[ms]"
NAME_WIDTH_MS       = "Width[ms]"
#
INIT_COUNT          = "10"
INIT_PERIOD_MS      = "200"
INIT_WIDTH_MS       = "100"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameControllerLed(tk.Frame):
    #---------------------------------------------------------------------
    # Constructor
    #---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 880, height = 73, \
                          padx = 6, pady = 6, background = "#eeeeff")
        self.grid_propagate(0)
        #---------------------------------------------------------------
        self.CBOnGetStateLedSystem = None
        #
        self.btnGetStateLedSystem = tk.Button(self, width = 24, \
                                              text = "Get State LedSystem", \
                                              command = self.OnGetStateLedSystem)
        self.btnGetStateLedSystem.grid(row = 0, column = 0, \
                                        sticky = tk.W, padx = 2, pady = 2)
        #
        self.btnSwitchLedSystemOn = tk.Button(self, width = 24, \
                                              text = "Switch LedSystem On", \
                                              command = self.OnSwitchLedSystemOn)
        self.btnSwitchLedSystemOn.grid(row = 0, column = 1, columnspan = 2, \
                                       sticky = tk.W, padx = 2, pady = 2)
        #
        self.btnSwitchLedSystemOff = tk.Button(self, width = 24, \
                                               text = "Switch LedSystem Off", \
                                               command = self.OnSwitchLedSystemOff)
        self.btnSwitchLedSystemOff.grid(row = 0, column = 3, columnspan = 2, \
                                        sticky = tk.W, padx = 2, pady = 2)
        #
        self.lblColorLed = tk.Label(self, text = "---", width = 6)
        self.lblColorLed.grid(row = 0, column = 5)
        #
        self.btnBlinkLedSystem = tk.Button(self, width = 24, \
                                           text = "Blink Led System", \
                                           command = self.OnBlinkLedSystem)
        self.btnBlinkLedSystem.grid(row = 1, column = 0, \
                                    sticky = tk.W, padx = 2, pady = 2)
        #
        self.lblCount = tk.Label(self, text = "Count[1]", width = 10)
        self.lblCount.grid(row = 1, column = 1)
        self.spbCount = tk.Spinbox(self, from_ = 1, to = 1000, increment = 1, \
                                   width = 6, justify = "center")
        self.spbCount.grid(row = 1, column = 2)
        self.spbCount.delete(0, tk.END)
        self.spbCount.insert(0, INIT_COUNT)
        #
        self.lblPeriod = tk.Label(self, text = "Period[ms]", width = 10)
        self.lblPeriod.grid(row = 1, column = 3)
        self.spbPeriod = tk.Spinbox(self, from_ = 100, to = 10000, \
                                    increment = 100, width = 6, justify = "center")
        self.spbPeriod.grid(row = 1, column = 4)
        self.spbPeriod.delete(0, tk.END)
        self.spbPeriod.insert(0, INIT_PERIOD_MS)
        #
        self.lblWidth = tk.Label(self, text = "Width[ms]", width = 10)
        self.lblWidth.grid(row = 1, column = 5)
        self.spbWidth = tk.Spinbox(self, from_ = 100, to = 10000, \
                                    increment = 100, width = 6, justify = "center")
        self.spbWidth.grid(row = 1, column = 6)
        self.spbWidth.delete(0, tk.END)
        self.spbWidth.insert(0, INIT_WIDTH_MS)
        #
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def SetCBOnGetStateLedSystem(self, callback):
        self.CBOnGetStateLedSystem = callback
    #
    def SetCBOnSwitchLedSystemOn(self, callback):
        self.CBOnSwitchLedSystemOn = callback
    #
    def SetCBOnSwitchLedSystemOff(self, callback):
        self.CBOnSwitchLedSystemOff = callback
    #
    def SetCBOnBlinkLedSystem(self, callback):
        self.CBOnBlinkLedSystem = callback
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    def OnGetStateLedSystem(self):
        if (None != self.CBOnGetStateLedSystem):
            self.CBOnGetStateLedSystem()
    #
    def OnSwitchLedSystemOn(self):
        if (None != self.CBOnSwitchLedSystemOn):
            self.CBOnSwitchLedSystemOn()
    #
    def OnSwitchLedSystemOff(self):
        if (None != self.CBOnSwitchLedSystemOff):
            self.CBOnSwitchLedSystemOff()
    #
    def OnBlinkLedSystem(self):
        if (None != self.CBOnBlinkLedSystem):
            Count = str(self.spbCount.get())
            Period = str(self.spbPeriod.get())
            Width = str(self.spbWidth.get())
            self.CBOnBlinkLedSystem(Count, Period, Width)
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        BC = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_COUNT, \
                                        INIT_COUNT)
        BP = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_PERIOD_MS, \
                                        INIT_PERIOD_MS)
        BW = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_WIDTH_MS, \
                                        INIT_WIDTH_MS)
        self.spbCount.delete(0, tk.END)
        self.spbCount.insert(0, BC)
        self.spbPeriod.delete(0, tk.END)
        self.spbPeriod.insert(0, BP)
        self.spbWidth.delete(0, tk.END)
        self.spbWidth.insert(0, BW)
    #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_COUNT, \
                                 self.spbCount.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PERIOD_MS, \
                                 self.spbPeriod.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_WIDTH_MS, \
                                 self.spbWidth.get())
    #
    #---------------------------------------------------------------
    # Management
    #--------------------------------------------------------------
    def RefreshEvent(self, tokens):
        if (4 < len(tokens)):
            if ("1" == tokens[4]):
                self.lblColorLed.config(bg = "violet")
                self.lblColorLed.config(text = "ON")
            else:
                self.lblColorLed.config(bg = "grey")
                self.lblColorLed.config(text = "OFF")


        # Event = "Event: "
        # if 1 < len(tokens):
        #     Event += tokens[1]
        # if 2 < len(tokens):
        #     Event += " [" + tokens[2] + "]"
        # if 3 < len(tokens):
        #     Event += " <- [" + tokens[3] + "]"
        # #debug
        # print("++++++ REFRESH EVENT LED[" + Event + "]")
        # #self.lblEvent.config(text = Event)
    #
    # def RefreshDateTime(self, date, time, millis):
    #     """: ..."""
    #     self.lblDateTime.config(text = "D[" + date[4:6] + "." + date[2:4] + "." + \
    #                             date[0:2] + "] T[" + time[0:2] + ":" + time[2:4] + \
    #                             ":" + time[4:6] + "." + millis + "]")
    # def RefreshLocalTime(self, hours, minutes, seconds, millis):
    #     self.lblDateTime.config(text = "LT[ " + hours + " : " + minutes + " : " + \
    #                                    seconds + " . " + millis + " ]")
    # #
    #
    # def RefreshResponse(self, tokens):
    #     Response = "Response:  "
    #     # debug print(tokens)
    #     for Token in tokens[1:]:
    #         Response += " " + Token
    #     self.lblResponse.config(text = Response)
