#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameLaserMatrix
# ------------------------------------------------------------------
#   Version: 00V00
#   Date   : 200519
#   Time   : 2109
#   Author : OMDevelop
#
import tkinter as tk
#
from tkinter import filedialog as FD
import FrameStepTable as FST
import Steplist as SL
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION                = "FrameLaserMatrix"
#
NAME_PULSEPERIOD_MS             = "PulsePeriod[ms]"
NAME_PULSECOUNT                 = "PulseCount[1]"
NAME_POSITIONX_STP              = "PositionX[stp]"
NAME_POSITIONY_STP              = "PositionY[stp]"
NAME_DELAYSTEP_MS               = "DelayStep[ms]"
NAME_DELAYLINE_MS               = "DelayLine[ms]"
#
INIT_PULSEPERIOD_MS             = "1000"
INIT_PULSECOUNT                 = "1"
INIT_POSITIONX_STP              = "2000"
INIT_POSITIONY_STP              = "2000"
INIT_DELAYSTEP_MS               = "10"
INIT_DELAYLINE_MS               = "100"
#------------------------------------------------------------------
#
StateSteplist = ['sslZero', 'sslInit', 'sslBusy', 'sslWait', 'sslAbort']

CONTROL_WIDTH = 16
CONTROL_HEIGHT = 1

#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameLaserMatrix(tk.Frame):
#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 880, height = 569, \
                          padx = 2, pady = 2, background = "#ddffee")
        self.grid_propagate(0)
        #
        self.CBOnStartSteplist = None
        self.CBOnAbortSteplist = None
        #--------------------------------------------------------------
        self.FrameStepTable = FST.CFrameStepTable(self)
        self.FrameStepTable.grid(row = 0, column = 0, columnspan = 123, \
                                  sticky = tk.W, padx = 2, pady = 2)
        #---------------------------------------------------------------
        self.btnReadSteplist = tk.Button(self, text = "Read Steplist", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT, \
                                  command = self.OnReadSteplistClick)
        self.btnReadSteplist.grid(row = 1, column = 0, padx = 2, pady = 2)
        #---------------------------------------------------------------
        self.btnWriteSteplist = tk.Button(self, text = "Write Steplist", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT, \
                                  command = self.OnWriteSteplistClick)
        self.btnWriteSteplist.grid(row = 1, column = 1, padx = 2, pady = 2)
        #---------------------------------------------------------------      
        #--------------------------------------------------------------
        self.btnStartSteplist = tk.Button(self, text = "Start Steplist", \
                                          width = CONTROL_WIDTH, height = CONTROL_HEIGHT, \
                                          command = self.OnStartSteplist)
        self.btnStartSteplist.grid(row = 1, column = 2, \
                                    sticky = tk.W, padx = 2, pady = 2)
        #--------------------------------------------------------------
        self.btnAbortSteplist = tk.Button(self, text = "Abort Steplist", \
                                          width = CONTROL_WIDTH, height = CONTROL_HEIGHT, \
                                          command = self.OnAbortSteplist)
        self.btnAbortSteplist.grid(row = 1, column = 3, \
                                   sticky = tk.W, padx = 2, pady = 2)
        #--------------------------------------------------------------
        self.Steplist = SL.CSteplist()
        self.Steplist.InitSimple(3)
        self.Steplist.Debug("Steplist", "---")
        #
        self.StateSteplist = 'sslZero'
        #
        self.RefreshStepTable()
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def GetStateSteplist(self):
        return self.StateSteplist
    #
    def SetStateSteplist(self, state):
        if ('sslInit' == state):
            self.Steplist.ResetStep()
            self.StateSteplist = 'sslBusy'
            print("SSL[{0}]<-[{1}]".format(self.StateSteplist, state))
            return
        if ('sslAbort' == state):
            # abort execution...
            self.StateSteplist = 'sslZero'
            print("SSL[{0}]<-[{1}]".format(self.StateSteplist, state))
            return
        print("SSL[{0}]<-[{1}]".format(state, self.StateSteplist))
        self.StateSteplist = state
        return
        
    #
    def SetCBOnAppendCommand(self, callback):
        self.CBOnAppendCommand = callback        
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    def OnStartSteplist(self):
        print("FLM: StartSteplist")
        self.SetStateSteplist('sslInit')
        # if ('sslBusy' == self.GetState()):
        # self.SetState('sslZero')
        # if ('sslBusy' == self.State):
        # self.SetState('sslZero')
    #
    def OnAbortSteplist(self):
        print("FLM: AbortSteplist")
        self.SetStateSteplist('sslAbort')
    #
    def OnAppendCommand(self, commandtext):
        print("FLM: AppendCommand")
        if (self.CBOnAppendCommand):
            self.CBOnAppendCommand(commandtext)
    #       
    def OnWriteSteplistClick(self):        
        IF = 'MicroMachining.stt'
        T = 'Write Steplist to File'
        UD = '.\\'
        FT = (('StepTables','.stt'), ('All Files', '*.*'))
        FileEntry = FD.asksaveasfilename(initialfile = IF, title = T,
                                        initialdir = UD, filetypes = FT)
        if (FileEntry):
            SC = self.Steplist.GetStepCount()
            F = open(FileEntry, "w")
            for SI in range(0, SC):
                S = self.Steplist.GetStep(SI)
                F.write(MASK_FILE.format(S[0], S[1], S[2], S[3], S[4]))
            F.close()
    #
    def OnReadSteplistClick(self):
        T = 'Read Steplist from File'
        UD = '.\\'
        FT = (('StepTables','.stt'), ('All Files', '*.*'))
        FileEntry = FD.askopenfilename(initialdir = UD, filetypes = FT, title = T)
        if (FileEntry):
            self.Steplist.Clear()
            self.FrameStepTable.Clear()
            F = open(FileEntry, "r")
            FPE = self.GetFilePositionEnd(F)
            while (not self.IsEndOfFile(F, FPE)):
                Line = F.readline().strip()
                if (0 < len(Line)):
                    TL = Line.split()
                    Step = [int(TL[0]), int(TL[1]), int(TL[2]), float(TL[3]), float(TL[4])]
                    self.Steplist.AddStep(Step)
            F.close()
            self.RefreshStepTable()
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        PP = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_PULSEPERIOD_MS, \
                                        INIT_PULSEPERIOD_MS)
        self.FrameLaserMatrix.ReadInitdata(readinitdata)       
        #
        # self.OnSet...
        #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        self.FrameLaserMatrix.WriteInitdata(writeinitdata)
    #
    #-----------------------------------------------------------------------------------
    # Helper - File
    #-----------------------------------------------------------------------------------
    def GetFilePositionEnd(self, file):
        file.seek(0, 2)
        EP = file.tell()
        file.seek(0)
        return EP
    #
    def IsEndOfFile(self, file, filepositionend):
        return (filepositionend <= file.tell())
    #
    #---------------------------------------------------------------
    # Management - Helper
    #--------------------------------------------------------------
    def RefreshStepTable(self):
        SC = self.Steplist.GetStepCount()
        for SI in range(SC):
            Step = self.Steplist.GetIndexStep(SI)
            self.FrameStepTable.AddRow(Step[0], Step[1], Step[2], Step[3], Step[4])
        
    #
    #---------------------------------------------------------------
    # Management - Response
    #--------------------------------------------------------------
    def CommandFinnished(self):
        if ('sslWait' == self.GetStateSteplist()):
            self.SetStateSteplist('sslBusy')
    #
    #---------------------------------------------------------------
    # Management - Execute
    #--------------------------------------------------------------
    def Execute(self):
        if ('sslWait' == self.GetStateSteplist()):
            return
        if ('sslBusy' == self.GetStateSteplist()):
            StepIndex, Step = self.Steplist.GetStepIndexCommandParameter()
            self.FrameStepTable.SetStepMarked(StepIndex)
            if (Step):
                print(Step)
                CommandText = "MPP"
                for PI in range(0, len(Step)):
                    CommandText += " " + str(Step[PI])
                print(CommandText)
                self.OnAppendCommand(CommandText)
                self.SetStateSteplist('sslWait')
            else:
                self.SetStateSteplist('sslZero')







        # #--------------------------------------------------------------
        # self.lblPulsePeriod = tk.Label(self, text = "PulsePeriod[ms]", width = 14)
        # self.lblPulsePeriod.grid(row = 1, column = 0, sticky = "w", padx = 6, pady = 6)
        # #
        # self.spbPulsePeriod = tk.Spinbox(self, from_ = 1000, to = 100000, increment = 1000, \
        #                                   width = 8, justify = "center")
        # self.spbPulsePeriod.grid(row = 1, column = 1, sticky = "w", padx = 6, pady = 6)
        # self.spbPulsePeriod.delete(0, tk.END)
        # self.spbPulsePeriod.insert(0, INIT_PULSEPERIOD_MS)
        # #--------------------------------------------------------------       
        # self.lblPulseCount = tk.Label(self, text = "PulseCount[1]", width = 14)
        # self.lblPulseCount.grid(row = 1, column = 2, sticky = "w", padx = 10, pady = 6)
        # #
        # self.spbPulseCount = tk.Spinbox(self, from_ = 1, to = 10000, increment = 1, \
        #                                   width = 8, justify = "center")
        # self.spbPulseCount.grid(row = 1, column = 3, sticky = "w", padx = 6, pady = 6)
        # self.spbPulseCount.delete(0, tk.END)
        # self.spbPulseCount.insert(0, INIT_PULSECOUNT)
        # #--------------------------------------------------------------
        # self.lblDelayStep = tk.Label(self, text = "DelayStep[ms]", width = 14)
        # self.lblDelayStep.grid(row = 1, column = 4, sticky = "w", padx = 6, pady = 6)
        # #
        # self.spbDelayStep = tk.Spinbox(self, from_ = 0, to = 9999, increment = 1, \
        #                                   width = 8, justify = "center")
        # self.spbDelayStep.grid(row = 1, column = 5, sticky = "w", padx = 6, pady = 6)
        # self.spbDelayStep.delete(0, tk.END)
        # self.spbDelayStep.insert(0, INIT_DELAYSTEP_MS)
        # #--------------------------------------------------------------
        # self.lblDelayLine = tk.Label(self, text = "DelayLine[ms]", width = 14)
        # self.lblDelayLine.grid(row = 1, column = 6, sticky = "w", padx = 6, pady = 6)
        # #
        # self.spbDelayLine = tk.Spinbox(self, from_ = 0, to = 9999, increment = 1, \
        #                                   width = 8, justify = "center")
        # self.spbDelayLine.grid(row = 1, column = 7, sticky = "w", padx = 6, pady = 6)
        # self.spbDelayLine.delete(0, tk.END)
        # self.spbDelayLine.insert(0, INIT_DELAYLINE_MS)
        # #--------------------------------------------------------------
        # self.lblPositionX = tk.Label(self, text = "PositionX[stp]", width = 14)
        # self.lblPositionX.grid(row = 2, column = 0, sticky = "w", padx = 6, pady = 6)
        # #
        # self.spbPositionX = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
        #                                   width = 8, justify = "center")
        # self.spbPositionX.grid(row = 2, column = 1, sticky = "w", padx = 6, pady = 6)
        # self.spbPositionX.delete(0, tk.END)
        # self.spbPositionX.insert(0, INIT_POSITIONX_STP)



        #        
        # #--------------------------------------------------------------
        # # PulseLaser
        # #--------------------------------------------------------------
        # self.btnPulseLaserAbort = tk.Button(self, width = WIDTH_BUTTON, \
        #                                    text = "Pulse Laser Abort", \
        #                                    command = self.OnPulseLaserAbort)
        # self.btnPulseLaserAbort.grid(row = 1, column = 0, \
        #                              sticky = tk.W, padx = 2, pady = 2)
        # #
        # self.btnPulseLaserCount = tk.Button(self, width = WIDTH_BUTTON, \
        #                                     text = "Pulse Laser Count", \
        #                                     command = self.OnPulseLaserCount)
        # self.btnPulseLaserCount.grid(row = 1, column = 1, \
        #                              sticky = tk.W, padx = 2, pady = 2)
        # #
        # #--------------------------------------------------------------
        # # PulseLaserCount
        # #--------------------------------------------------------------
        # self.btnMovePositionX = tk.Button(self, width = WIDTH_BUTTON, \
        #                                   text = "Move PositionX", \
        #                                   command = self.OnMovePositionX)
        # self.btnMovePositionX.grid(row = 2, column = 1, \
        #                            sticky = tk.W, padx = 2, pady = 2)
        # #
        # self.btnMovePositionY = tk.Button(self, width = WIDTH_BUTTON, \
        #                                   text = "Move PositionY", \
        #                                   command = self.OnMovePositionY)
        # self.btnMovePositionY.grid(row = 3, column = 1, \
        #                            sticky = tk.W, padx = 2, pady = 2)
        # #
        # self.lblPositionY = tk.Label(self, text = "PositionY[stp]", width = 14)
        # self.lblPositionY.grid(row = 3, column = 2, sticky = "w", padx = 10)
        # #
        # self.spbPositionY = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
        #                                  width = 8, justify = "center")
        # self.spbPositionY.grid(row = 3, column = 3, sticky = "w", padx = 6)
        # self.spbPositionY.delete(0, tk.END)
        # self.spbPositionY.insert(0, INIT_POSITIONY_STP)
        # #
        # #--------------------------------------------------------------
        # # MovePositionPulse
        # #--------------------------------------------------------------
        # self.btnMovePositionPulse = tk.Button(self, width = WIDTH_BUTTON, \
        #                                       text = "Move Position Pulse", \
        #                                   command = self.OnMovePositionPulse)
        # self.btnMovePositionPulse.grid(row = 4, column = 1, \
        #                                sticky = tk.W, padx = 2, pady = 2)
        # #
        # self.lblLaserLed = tk.Label(self, text = "---", width = 4)
        # self.lblLaserLed.grid(row = 4, column = 4, padx = 3)


    #
    # def SetCBOnSetPulsePeriod(self, callback):
    #     self.CBOnSetPulsePeriod = callback
    # #
    # def SetCBOnGetPulsePeriod(self, callback):
    #     self.CBOnGetPulsePeriod = callback
    # #
    # def SetCBOnPulseLaserAbort(self, callback):
    #     self.CBOnPulseLaserAbort = callback
    # #
    # def SetCBOnPulseLaserCount(self, callback):
    #     self.CBOnPulseLaserCount = callback
    # #
    # def SetCBOnMovePositionX(self, callback):
    #     self.CBOnMovePositionX = callback
    # #
    # def SetCBOnMovePositionY(self, callback):
    #     self.CBOnMovePositionY = callback
    # #
    # def SetCBOnMovePositionPulse(self, callback):
    #     self.CBOnMovePositionPulse = callback


    #
    # def OnSetPulsePeriod(self):
    #     if (self.CBOnSetPulsePeriod):
    #         PP = self.spbPulsePeriod.get()
    #         self.CBOnSetPulsePeriod(PP)
    # #
    # def OnGetPulsePeriod(self):
    #     if (self.CBOnGetPulsePeriod):
    #         self.CBOnGetPulsePeriod()
    # #
    # def OnPulseLaserAbort(self):
    #     if (self.CBOnPulseLaserAbort):
    #         self.CBOnPulseLaserAbort()
    # #
    # def OnPulseLaserCount(self):
    #     if (self.CBOnPulseLaserCount):
    #         PP = self.spbPulsePeriod.get()
    #         PC = self.spbPulseCount.get()
    #         self.CBOnPulseLaserCount(PP, PC)
    # #
    # def OnMovePositionX(self):
    #     if (self.CBOnMovePositionX):
    #         PX = self.spbPositionX.get()
    #         self.CBOnMovePositionX(PX)
    # #
    # def OnMovePositionY(self):
    #     if (self.CBOnMovePositionY):
    #         PY = self.spbPositionY.get()
    #         self.CBOnMovePositionY(PY)
    # #
    # def OnMovePositionPulse(self):
    #     if (self.CBOnMovePositionPulse):
    #         PP = self.spbPulsePeriod.get()
    #         PC = self.spbPulseCount.get()
    #         PX = self.spbPositionX.get()
    #         PY = self.spbPositionY.get()
    #         DM = self.spbDelayMotion.get()
    #         self.CBOnMovePositionPulse(PX, PY, PP, PC, DM)           


        # PC = readinitdata.ReadValueInit(INITDATA_SECTION, \
        #                                 NAME_PULSECOUNT, \
        #                                 INIT_PULSECOUNT)
        # PX = readinitdata.ReadValueInit(INITDATA_SECTION, \
        #                                 NAME_POSITIONX_STP, \
        #                                 INIT_POSITIONX_STP)
        # PY = readinitdata.ReadValueInit(INITDATA_SECTION, \
        #                                 NAME_POSITIONY_STP, \
        #                                 INIT_POSITIONY_STP)
        # DM = readinitdata.ReadValueInit(INITDATA_SECTION, \
        #                                 NAME_DELAYMOTION_MS, \
        #                                 INIT_DELAYMOTION_MS)
        # self.spbPulsePeriod.delete(0, tk.END)
        # self.spbPulsePeriod.insert(0, PP)
        # self.spbPulseCount.delete(0, tk.END)
        # self.spbPulseCount.insert(0, PC)
        # self.spbPositionX.delete(0, tk.END)
        # self.spbPositionX.insert(0, PX)
        # self.spbPositionY.delete(0, tk.END)
        # self.spbPositionY.insert(0, PY)
        # self.spbDelayMotion.delete(0, tk.END)
        # self.spbDelayMotion.insert(0, DM)


        # writeinitdata.WriteValue(INITDATA_SECTION, \
        #                          NAME_PULSEPERIOD_MS, \
        #                          self.spbPulsePeriod.get())
        # writeinitdata.WriteValue(INITDATA_SECTION, \
        #                          NAME_PULSECOUNT, \
        #                          self.spbPulseCount.get())
        # writeinitdata.WriteValue(INITDATA_SECTION, \
        #                          NAME_POSITIONX_STP, \
        #                          self.spbPositionX.get())
        # writeinitdata.WriteValue(INITDATA_SECTION, \
        #                          NAME_POSITIONY_STP, \
        #                          self.spbPositionY.get())
        # writeinitdata.WriteValue(INITDATA_SECTION, \
        #                          NAME_DELAYMOTION_MS, \
        #                          self.spbDelayMotion.get())


    # def RefreshEvent(self, tokens):
    #     if (2 < len(tokens)):
    #         # <!> <MPP> <percent> 
    #         if ("MPP" == tokens[1]):
    #             PC = int(tokens[2])
    #             if (PC < 100):
    #                 self.lblLaserLed.config(bg = "magenta")
    #                 self.lblLaserLed.config(bg = "green")
    #             self.lblLaserLed.config(text = str(PC) + "%")    



    # def RefreshResponse(self, tokens):
    #     if (1 < len(tokens)):
    #         Command = tokens[1]
    #         Parameters = []
    #         for TI in range(2, len(tokens)):
    #             Parameters.append(tokens[TI])               
    #         self.RefreshCommandData(Command, Parameters)
    # #        
    # def RefreshCommandData(self, command, parameters):
    #     if (0 < len(parameters)):
    #         # PulsePeriod
    #         if ("GPP" == command) or ("SPP" == command):
    #             self.spbPulsePeriod.delete(0, tk.END)
    #             self.spbPulsePeriod.insert(0, float(parameters[0]))            
    #             return True
    #         # MovePositionX
    #         if ("MPX" == command):
    #             self.spbPositionX.delete(0, tk.END)
    #             self.spbPositionX.insert(0, int(parameters[0]))
    #             return True
    #         # MovePositionY
    #         if ("MPY" == command):
    #             self.spbPositionY.delete(0, tk.END)
    #             self.spbPositionY.insert(0, int(parameters[0]))
    #             return True
    #     if (1 < len(parameters)):
    #         # PulseLaserCount
    #         if ("PLC" == command):
    #             self.spbPulsePeriod.delete(0, tk.END)
    #             self.spbPulsePeriod.insert(0, float(parameters[0]))            
    #             self.spbPulseCount.delete(0, tk.END)
    #             self.spbPulseCount.insert(0, int(parameters[1]))
    #             return True
    #     if (4 < len(parameters)):
    #         # MovePositionPulse
    #         if ("MPP" == command):
    #             self.spbPositionX.delete(0, tk.END)
    #             self.spbPositionX.insert(0, int(parameters[0]))
    #             self.spbPositionY.delete(0, tk.END)
    #             self.spbPositionY.insert(0, int(parameters[1]))                
    #             self.spbPulsePeriod.delete(0, tk.END)
    #             self.spbPulsePeriod.insert(0, float(parameters[2]))                
    #             self.spbPulseCount.delete(0, tk.END)
    #             self.spbPulseCount.insert(0, int(parameters[3]))
    #             self.spbDelayMotion.delete(0, tk.END)
    #             self.spbDelayMotion.insert(0, int(parameters[4]))
    #             return True
    #     return False
#
# MPP 2000 2000 100.0 10 10        

