#
#------------------------------------------------------------------
#   <short> - <project>
#------------------------------------------------------------------
#   Date   : 20mmdd
#   Time   : hhmm
#   Author : OMDevelop
#
import cv2 as OCV
import sys
from matplotlib import pyplot as PLT

#

#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#


#------------------------------------------------------------------
#   Global Variable
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Function
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Callback
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Main
#------------------------------------------------------------------
#

#WindMill = OCV.imread('windmill.jpg', OCV.IMREAD_COLOR)
WindMill = OCV.imread('windmill.jpg', OCV.IMREAD_GRAYSCALE)
if WindMill is None:
    print('Cannot load image')
    sys.exit()
#
# show in MatPlotLib:
#PLT.imshow(WindMill, cmap = 'gray', interpolation = 'bicubic')
# PLT.imshow(WindMill, cmap = 'hot', interpolation = 'bicubic')
# PLT.imshow(WindMill, cmap = 'Greens', interpolation = 'bicubic')
#PLT.imshow(WindMill, cmap = 'ocean', interpolation = 'bicubic')
PLT.imshow(WindMill, cmap = 'rainbow', interpolation = 'bicubic')
PLT.show()

#
# show in OpenCV:
OCV.imshow("WindMill", WindMill)
OCV.waitKey(0)
OCV.destroyAllWindows()
