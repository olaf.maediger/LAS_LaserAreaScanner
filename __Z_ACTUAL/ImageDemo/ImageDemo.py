#
#------------------------------------------------------------------
#   <short> - <project>
#------------------------------------------------------------------
#   Date   : 20mmdd
#   Time   : hhmm
#   Author : OMDevelop
#
import cv2 as OCV
import sys
import numpy as NP
from PIL import Image
from matplotlib import pyplot as PLT

#

#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#


#------------------------------------------------------------------
#   Global Variable
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Function
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Helper
#------------------------------------------------------------------
#
def GrayBlackWhite(rowcount, colcount, image, threshold):
    for RI in range(rowcount):
        for CI in range(colcount):
            PV = Image[RI, CI]
            if PV < threshold:
                image.itemset(RI, CI, 0)
            else:
                image.itemset(RI, CI, 255)
    return image
# #
# def ColorBlackWhite(rowcount, colcount, image, thred, thgreen, thblue):
#     for RI in range(rowcount):
#         for CI in range(colcount):
#             PV = Image[RI, CI]
#             # red
#             if PV[2] < thred:
#                 Image.itemset(RI, CI, 0, 0)
#             else:
#                 Image.itemset(RI, CI, 0, 255)
#             # green
#             if PV[1] < thgreen:
#                 Image.itemset(RI, CI, 1, 0)
#             else:
#                 Image.itemset(RI, CI, 1, 255)
#             # blue
#             if PV[0] < thblue:
#                 Image.itemset(RI, CI, 2, 0)
#             else:
#                 Image.itemset(RI, CI, 2, 255)
#     return image
#
def ColorBlackWhite(rowcount, colcount, image, thred, thgreen, thblue):
    ImageData = NP.asarray(image)
    for RI in range(rowcount):
        for CI in range(colcount):
            ImageData[RI, CI, 0] = 128
            ImageData[RI, CI, 1] = 128
            ImageData[RI, CI, 2] = 255
            #PV = ImageData[RI, CI]
            # # red
            # if PV[0] < thred:
            #     ImageData[RI, CI, 0] = 0
            # else:
            #     ImageData[RI, CI, 0] = 255
            # # green
            # if PV[1] < thred:
            #     ImageData[RI, CI, 0] = 0
            # else:
            #     ImageData[RI, CI, 0] = 255
            # # blue
            # if PV[2] < thred:
            #     ImageData[RI, CI, 0] = 0
            # else:
            #     ImageData[RI, CI, 0] = 255
    #print(ImageData)
    Result = Image.fromarray(ImageData.astype('uint8'))
    return Result


#
#------------------------------------------------------------------
#   Main
#------------------------------------------------------------------
#
#
# Image = OCV.imread('windmill.jpg', OCV.IMREAD_GRAYSCALE)
# RowCount, ColCount = Image.shape
# ColorCount = 1
#
#
I = OCV.imread('simple.png', OCV.IMREAD_COLOR)
RowCount, ColCount, ColorCount = I.shape
#
#
if I is None:
    print('Cannot load image')
    sys.exit()
#
PLT.imshow(I)
PLT.show()
#
# if 3 == ColorCount:
#     I = ColorBlackWhite(RowCount, ColCount, I, 255, 0, 0)
# else:
#     I = GrayBlackWhite(RowCount, ColCount, I, 184)
#print(I)

I = ColorBlackWhite(RowCount, ColCount, I, 255, 0, 0)


PLT.imshow(I)
PLT.show()
#
# OCV.imshow('Image', I)
#OCV.waitKey(0)
# OCV.destroyAllWindows()
