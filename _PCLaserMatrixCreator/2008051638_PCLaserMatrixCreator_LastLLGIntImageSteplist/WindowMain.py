#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - WindowMainUart
# ------------------------------------------------------------------
#   Version: 02V04
#   Date   : 200625
#   Time   : 0820
#   Author : OMDevelop
#
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
#import time
#
import Defines
import Initdata as ID
#from Task import *
import FrameLaserMatrixConstant as FLMC
import FrameLaserMatrixLinear as FLML
import FrameLaserMatrixImage as FLMI
#
#   WindowMain
#       Notebook
#           FrameLaserMatrix
#               FrameLaserMatrixConstant
#                  Steplist
#               FrameLaserMatrixLinear
#                  Steplist
#               FrameLaserMatrixImage
#                  Steplist
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION = "WindowMain"
NAME_X = "X"
NAME_Y = "Y"
NAME_W = "W"
NAME_H = "H"
NAME_SELECTTABINDEX = "SelectTabIndex"
#
INIT_X = "10"
INIT_Y = "10"
INIT_W = "690"
INIT_H = "546"
INIT_SELECTTABINDEX = "0"
#
PAD_X = 1
PAD_Y = 1
FONTSIZE_LISTBOX = 11
COLOR_BACK = "#F8FEEE"
#
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
#
#------------------------------------------------------------------
#   Global Type - WindowMain
#------------------------------------------------------------------
class CWindowMain(tk.Tk):
    #---------------------------------------------------------------------
    # CWindowMain - Constructor
    #---------------------------------------------------------------------
    def __init__(self):
        super().__init__()
        super().protocol("WM_DELETE_WINDOW", self.OnDeleteWindow)
        self.title(Defines.TITLE_WINDOW)
        #self.resizable(False, False)
        #
        # Menu
        self.Menu = tk.Menu(self)
        self.config(menu = self.Menu)
        # Menu - System
        self.MenuSystem = tk.Menu(self.Menu)
        self.Menu.add_cascade(label = "System", menu = self.MenuSystem)
        self.MenuSystem.add_command(label = "Exit Application", \
                                    command = self.OnExitApplication)
        # Menu - Device
        self.MenuDevice = tk.Menu(self.Menu)
        self.Menu.add_cascade(label = "Device", menu = self.MenuDevice)
        self.MenuDevice.add_command(label = "Open/Close Uart") #, \
                                    #command = self.OnOpenCloseUart)
        self.MenuDevice.add_command(label = "Save File")#, command = self.OnSaveFile)
        self.MenuDevice.add_separator()
        self.MenuDevice.add_command(label = "Choose Color") #,\
        #                             command = self.OnChooseColor)
        # Menu - Matrix
        self.MenuMatrix = tk.Menu(self.Menu)
        self.Menu.add_cascade(label = "Matrix", menu = self.MenuMatrix)
        self.MenuMatrix.add_command(label = "Abort")#, command = self.OnShowAbout)
        self.MenuMatrix.add_command(label = "MovePulseLaser")#
        #
        # Menu - Help
        self.MenuHelp = tk.Menu(self.Menu)
        self.Menu.add_cascade(label = "Help", menu = self.MenuHelp)
        self.MenuHelp.add_command(label = "Show About", command = self.OnShowAbout)
        #
        #-------------------------------------------------------------------------------
        # Notebook - All
        #-------------------------------------------------------------------------------
        self.nbkMain = ttk.Notebook(self)
        self.nbkMain.pack(fill = tk.BOTH)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - LaserMatrixConstant
        #-------------------------------------------------------------------------------
        self.frmLaserMatrixConstant = tk.Frame(self.nbkMain)
        self.nbkMain.add(self.frmLaserMatrixConstant, text = "LaserMatrixConstant")
        self.FrameLaserMatrixConstant = \
        FLMC.CFrameLaserMatrixConstant(self.frmLaserMatrixConstant)
        self.FrameLaserMatrixConstant.pack(fill = tk.BOTH)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - LaserMatrixLinear
        #-------------------------------------------------------------------------------
        self.frmLaserMatrixLinear = tk.Frame(self.nbkMain)
        self.nbkMain.add(self.frmLaserMatrixLinear, text = "LaserMatrixLinear")
        self.FrameLaserMatrixLinear = \
        FLML.CFrameLaserMatrixLinear(self.frmLaserMatrixLinear)
        self.FrameLaserMatrixLinear.pack(fill = tk.BOTH)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - LaserMatrixImage
        #-------------------------------------------------------------------------------
        self.frmLaserMatrixImage = tk.Frame(self.nbkMain)
        self.nbkMain.add(self.frmLaserMatrixImage, text = "LaserMatrixImage")
        self.FrameLaserMatrixImage = \
        FLMI.CFrameLaserMatrixImage(self.frmLaserMatrixImage)
        self.FrameLaserMatrixImage.pack(fill = tk.BOTH)
        #
        #
        #-------------------------------------------------------------------------------
        # Init...
        #-------------------------------------------------------------------------------
        # self.Task = CTask()
        # self.Task.SetCBOnExecute(self.TaskOnExecute)
        #
        self.ReadInitdata(Defines.NAME_INITFILE)
    #---------------------------------------------------------------------
    # CWindowMain - Callback
    #---------------------------------------------------------------------
    def OnDeleteWindow(self):
        #
        self.WriteInitdata(Defines.NAME_INITFILE)
        #
        super().destroy()
    #
    #---------------------------------------------------------------------
    # CApplication - Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, filename):
        print("*** ReadInitdata: start")
        RID = ID.CReadInitdata()
        RID.Open(filename)
        X = RID.ReadValueInit(INITDATA_SECTION, NAME_X, INIT_X)
        Y = RID.ReadValueInit(INITDATA_SECTION, NAME_Y, INIT_Y)
        W = RID.ReadValueInit(INITDATA_SECTION, NAME_W, INIT_W)
        H = RID.ReadValueInit(INITDATA_SECTION, NAME_H, INIT_H)
        self.geometry('%dx%d+%d+%d' % (int(W), int(H), int(X), int(Y)))
        #
        self.FrameLaserMatrixConstant.ReadInitdata(RID)
        #
        STI = RID.ReadValueInit(INITDATA_SECTION, NAME_SELECTTABINDEX, \
                                INIT_SELECTTABINDEX)
        self.nbkMain.select(int(STI))
        #
        RID.Close()
        #
        print("*** ReadInitdata: end")
    #
    def WriteInitdata(self, filename):
        print("*** WriteInitdata: begin")
        X = self.winfo_x()
        Y = self.winfo_y()
        W = self.winfo_width()
        H = self.winfo_height()
        WID = ID.CWriteInitdata()
        WID.Open(filename)
        WID.WriteSection(INITDATA_SECTION)
        WID.WriteValue(INITDATA_SECTION, NAME_X, str(X))
        WID.WriteValue(INITDATA_SECTION, NAME_Y, str(Y))
        WID.WriteValue(INITDATA_SECTION, NAME_W, str(W))
        WID.WriteValue(INITDATA_SECTION, NAME_H, str(H))
        #
        self.FrameLaserMatrixConstant.WriteInitdata(WID)
        #
        STI = self.nbkMain.index('current')
        WID.WriteValue(INITDATA_SECTION, NAME_SELECTTABINDEX, str(STI))
        #
        WID.Close()
        print("*** WriteInitdata: end")

    #
    #---------------------------------------------------------------------
    # CWindowMain - Property
    #---------------------------------------------------------------------

    #
    #---------------------------------------------------------------------
    # CWindowMain - Event
    #---------------------------------------------------------------------
    def OnShowAbout(self):
        messagebox.showinfo("About", Defines.INFO_ABOUT)
    #
    def OnExitApplication(self):
        self.destroy()
    #
    #---------------------------------------------------------------------
    # CWindowMain - Handler
    #---------------------------------------------------------------------
    def Execute(self):
        self.mainloop()
    #
    # SC: Idle -> Wait -> Busy -> Response -> Idle
    def TaskOnExecute(self):
        return
#
