#
# ------------------------------------------------------------------
#   PCDemoTable - FrameStepTable
# ------------------------------------------------------------------
#   Version: 02V04
#   Date   : 200625
#   Time   : 0820
#   Author : OMDevelop
#
import tkinter as tk
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION                = "FrameStepTable"
#
# NAME_PULSEPERIOD_MS             = "PulsePeriod[ms]"
#
# INIT_PULSEPERIOD_MS             = "1000"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
# FrameWidth = 200
# FrameHeight = 500
FORMAT_STEP = " %8d      %6d     %6d     %9.3f      %6d     %9.3f"


class CFrameStepTable(tk.Frame):
    #
    #---------------------------------------------------------------------
    # Constructor
    #---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 610, height = 428, padx = 2, pady = 2, bg = "#F2F2F2")
        self.grid_propagate(0)
        #--------------------------------------------------------------
        # Controls...
        #--------------------------------------------------------------
        self.lblHSIN = tk.Label(self, text = " StepIndex", font = "Courier 10")
        self.lblHSIN.grid(row = 0, column = 0, padx = 2, pady = 0)
        self.lblHSIU = tk.Label(self, text = "       [1]", font = "Courier 10")
        self.lblHSIU.grid(row = 1, column = 0, padx = 2, pady = 0)
        #
        self.lblHPXN = tk.Label(self, text = " PositionX", font = "Courier 10")
        self.lblHPXN.grid(row = 0, column = 1, padx = 2, pady = 0)
        self.lblHPXU = tk.Label(self, text = "     [stp]", font = "Courier 10")
        self.lblHPXU.grid(row = 1, column = 1, padx = 2, pady = 0)
        #
        self.lblHPYN = tk.Label(self, text = " PositionY", font = "Courier 10")
        self.lblHPYN.grid(row = 0, column = 2, padx = 2, pady = 0)
        self.lblHPYU = tk.Label(self, text = "     [stp]", font = "Courier 10")
        self.lblHPYU.grid(row = 1, column = 2, padx = 2, pady = 0)
        #
        self.lblHPPN = tk.Label(self, text = " PulsePeriod", font = "Courier 10")
        self.lblHPPN.grid(row = 0, column = 3, padx = 2, pady = 0)
        self.lblHPPU = tk.Label(self, text = "        [ms]", font = "Courier 10")
        self.lblHPPU.grid(row = 1, column = 3, padx = 2, pady = 0)
        #
        self.lblHPCN = tk.Label(self, text = " PulseCount", font = "Courier 10")
        self.lblHPCN.grid(row = 0, column = 4, padx = 2, pady = 0)
        self.lblHPCU = tk.Label(self, text = "        [1]", font = "Courier 10")
        self.lblHPCU.grid(row = 1, column = 4, padx = 2, pady = 0)
        #
        self.lblHDMN = tk.Label(self, text = " DelayMotion", font = "Courier 10")
        self.lblHDMN.grid(row = 0, column = 5, padx = 2, pady = 0)
        self.lblHDMU = tk.Label(self, text = "        [ms]", font = "Courier 10")
        self.lblHDMU.grid(row = 1, column = 5, padx = 2, pady = 0)
        #
        self.lbxSteps = tk.Listbox(self, width = 100, height = 22,
                                   font = "Courier 10", activestyle = tk.NONE)
        #self.lbxSteps["selectmode"] = "extended"
        self.lbxSteps["selectmode"] = "single"
        self.lbxSteps.grid(row = 2, column = 0, columnspan = 123, padx = 2, pady = 2)
        #
        self.scbSteps = tk.Scrollbar(self)
        self.scbSteps.grid(row = 2, column = 7, sticky = "ns")
        #
        self.lbxSteps["yscrollcommand"] = self.scbSteps.set
        self.scbSteps["command"] = self.lbxSteps.yview
        #
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        # PP = readinitdata.ReadValueInit(INITDATA_SECTION, \
        #                                 NAME_PULSEPERIOD_MS, \
        #                                 INIT_PULSEPERIOD_MS)
        # self.spbPulsePeriod.delete(0, tk.END)
        # self.OnSet...
        return True
        #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        # writeinitdata.WriteValue(INITDATA_SECTION, \
        #                          NAME_PULSEPERIOD_MS, \
        #                          self.spbPulsePeriod.get())
        return True
        #

    #---------------------------------------------------------------
    # Management - Get/Set
    #--------------------------------------------------------------
    def AddRow(self, px, py, pp, pc, dm):
        NS = self.lbxSteps.size()
        self.lbxSteps.insert(tk.END, FORMAT_STEP % (NS, px, py, pp, pc, dm))
    #
    def Clear(self):
        self.lbxSteps.delete(0, tk.END)
    #
    def SetStepMarked(self, stepindex):
        Sn = self.lbxSteps.size()
        Sih = Sn - 1
        if Sih <= 0:
            return
        SI = min(stepindex, Sih)
        self.lbxSteps.selection_clear(0, tk.END)
        self.lbxSteps.select_set(SI)
        self.lbxSteps.yview(SI - 14)
    #
    def GetStepIndexMarked(self):
        MarkIndices = self.lbxSteps.curselection()
        if (0 < len(MarkIndices)):
            return MarkIndices[0]
        return -1 # None
    #
    def GetStepMarked(self):
        MarkIndices = self.lbxSteps.curselection()
        if (0 < len(MarkIndices)):
            return self.lbxSteps.get(MarkIndices[0])
        return None






