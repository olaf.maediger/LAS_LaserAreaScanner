#
SHORT_APPLICATION = "LMC"
NAME_APPLICATION = "PCLaserMatrixCreator"
TITLE_APPLICATION = SHORT_APPLICATION + " - " + NAME_APPLICATION
#
NAME_INITFILE = NAME_APPLICATION + ".ini"
#
TITLE_WINDOW = TITLE_APPLICATION

APPLICATION_VERSION = "Version: 02V05"
APPLICATION_DATE = "Date: 200629"
APPLICATION_TIME = "Time: 1204"
APPLICATION_AUTHOR = "Author: OMDevelop"
APPLICATION_ABSTRACT = "Abstract:\r\n" + \
              "- Gui for Esp32LaserAreaScanner\r\n" + \
              "- Controlling over UsbSerial\r\n" + \
              "- not included: Mqtt"

INFO_ABOUT = TITLE_WINDOW + "\r\n\r\n" + \
             APPLICATION_VERSION + "\r\n" + \
             APPLICATION_DATE + "\r\n" + \
             APPLICATION_TIME + "\r\n" + \
             APPLICATION_AUTHOR + "\r\n\r\n" + \
             APPLICATION_ABSTRACT



MASK_FILE = "{0} {1} {2} {3} {4}"



