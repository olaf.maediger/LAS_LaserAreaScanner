#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameLaserMatrix - Linear
# ------------------------------------------------------------------
#   Version: 01V02
#   Date   : 200803
#   Time   : 1119
#   Author : OMDevelop
#
import tkinter as tk
import os
#
from tkinter import filedialog as FD
import FrameStepTable as FST
import Steplist as SL
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
WIDTH_BUTTON                    = 24
#
INITDATA_SECTION                = "FrameLaserMatrixLinear"
#
NAME_PULSEPERIOD_MS             = "PulsePeriod[ms]"
NAME_DELAYMOTIONX_MS            = "DelayMotionX[ms]"
NAME_DELAYMOTIONY_MS            = "DelayMotionY[ms]"
NAME_POSITIONXL_STP             = "PositionXL[stp]"
NAME_POSITIONXH_STP             = "PositionXH[stp]"
NAME_POSITIONDX_STP             = "PositionDX[stp]"
NAME_PULSECOUNTXL               = "PulseCountXL[1]"
NAME_PULSECOUNTXH               = "PulseCountXH[1]"
NAME_PULSECOUNTDX               = "PulseCountDS[1]"
NAME_POSITIONYL_STP             = "PositionYL[stp]"
NAME_POSITIONYH_STP             = "PositionYH[stp]"
NAME_POSITIONDY_STP             = "PositionDY[stp]"
#
INIT_PULSEPERIOD_MS             = "10"
INIT_DELAYMOTIONX_MS            = "10"
INIT_DELAYMOTIONY_MS            = "20"
INIT_POSITIONXL_STP             = "1800"
INIT_POSITIONXH_STP             = "2200"
INIT_POSITIONDX_STP             = "100"
INIT_PULSECOUNTXL               = "0"
INIT_PULSECOUNTXH               = "9"
INIT_PULSECOUNTDX               = "4"
INIT_POSITIONYL_STP             = "1800"
INIT_POSITIONYH_STP             = "2200"
INIT_POSITIONDY_STP             = "100"
INIT_PULSECOUNTYL               = "1"
INIT_PULSECOUNTYH               = "1"
INIT_PULSECOUNTDY               = "0"
#------------------------------------------------------------------
#
CONTROL_WIDTH = 26
CONTROL_HEIGHT = 1
#
PAD_WIDTH_A = 2
PAD_HEIGHT_A = 2
#
PAD_WIDTH_B = 2
PAD_HEIGHT_B = 2
#
PAD_WIDTH_T = 2
PAD_HEIGHT_T = 2
#
MASK_FILE = "{0} {1} {2} {3} {4}\n"
ERROR_WRITE = "Error: Undefined Step!"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameLaserMatrixLinear(tk.Frame):
#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 840, height = 620, \
                          padx = PAD_WIDTH_A, pady = PAD_HEIGHT_A, background = "#ddffee")
        self.grid_propagate(0)
        #
        #--------------------------------------------------------------
        # Steplist - Steptable
        #--------------------------------------------------------------
        self.FrameStepTable = FST.CFrameStepTable(self, 610, 424, 22)
        self.FrameStepTable.grid(row = 0, column = 0, columnspan = 123, \
                                  sticky = tk.W, padx = PAD_WIDTH_A, pady = PAD_HEIGHT_A)
        #
        #=R1===========================================================
        #--------------------------------------------------------------
        # PulsePeriod [ms]
        #--------------------------------------------------------------
        self.lblPulsePeriod = tk.Label(self, text = "PulsePeriod[ms]", width = 14)
        self.lblPulsePeriod.grid(row = 1, column = 0, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPulsePeriod = tk.Spinbox(self, from_ = -10000.000, to = +10000.000, \
                                        increment = 1.0, width = 8, justify = "center")
        self.spbPulsePeriod.grid(row = 1, column = 1, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPulsePeriod.delete(0, tk.END)
        self.spbPulsePeriod.insert(0, INIT_PULSEPERIOD_MS)
        #
        #--------------------------------------------------------------
        # DelayMotion-X [ms]
        #--------------------------------------------------------------
        self.lblDelayMotionX = tk.Label(self, text = "DelayMotionX[ms]", width = 14)
        self.lblDelayMotionX.grid(row = 1, column = 2, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbDelayMotionX = tk.Spinbox(self, from_ = 0, to = 9999, increment = 1, \
                                         width = 8, justify = "center")
        self.spbDelayMotionX.grid(row = 1, column = 3, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbDelayMotionX.delete(0, tk.END)
        self.spbDelayMotionX.insert(0, INIT_DELAYMOTIONX_MS)
        #
        #--------------------------------------------------------------
        # DelayMotion-Y [ms]
        #--------------------------------------------------------------
        self.lblDelayMotionY = tk.Label(self, text = "DelayMotionY[ms]", width = 14)
        self.lblDelayMotionY.grid(row = 1, column = 4, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbDelayMotionY = tk.Spinbox(self, from_ = 0, to = 9999, increment = 1, \
                                         width = 8, justify = "center")
        self.spbDelayMotionY.grid(row = 1, column = 5, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbDelayMotionY.delete(0, tk.END)
        self.spbDelayMotionY.insert(0, INIT_DELAYMOTIONY_MS)
        #
        #==============================================================
        #--------------------------------------------------------------
        # PositionXL [stp]
        #--------------------------------------------------------------
        self.lblPositionXL = tk.Label(self, text = "PositionXL[stp]", width = 14)
        self.lblPositionXL.grid(row = 2, column = 0, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPositionXL = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionXL.grid(row = 2, column = 1, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPositionXL.delete(0, tk.END)
        self.spbPositionXL.insert(0, INIT_POSITIONXL_STP)
        #
        #--------------------------------------------------------------
        # PositionXH [stp]
        #--------------------------------------------------------------
        self.lblPositionXH = tk.Label(self, text = "PositionXH[stp]", width = 14)
        self.lblPositionXH.grid(row = 2, column = 2, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPositionXH = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionXH.grid(row = 2, column = 3, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPositionXH.delete(0, tk.END)
        self.spbPositionXH.insert(0, INIT_POSITIONXH_STP)
        #
        #--------------------------------------------------------------
        # PositionDX [stp]
        #--------------------------------------------------------------
        self.lblPositionDX = tk.Label(self, text = "PositionDX[stp]", width = 14)
        self.lblPositionDX.grid(row = 2, column = 4, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPositionDX = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionDX.grid(row = 2, column = 5, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPositionDX.delete(0, tk.END)
        self.spbPositionDX.insert(0, INIT_POSITIONDX_STP)
        #
        #==============================================================
        #--------------------------------------------------------------
        # PulseCount-X-PCL [1]
        #--------------------------------------------------------------
        self.lblPulseCountXL = tk.Label(self, text = "PulseCountXL[1]", width = 14)
        self.lblPulseCountXL.grid(row = 3, column = 0, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPulseCountXL = tk.Spinbox(self, from_ = 1, to = 10000, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPulseCountXL.grid(row = 3, column = 1, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPulseCountXL.delete(0, tk.END)
        self.spbPulseCountXL.insert(0, INIT_PULSECOUNTXL)
        #
        #--------------------------------------------------------------
        # PulseCount-X-PCH [1]
        #--------------------------------------------------------------
        self.lblPulseCountXH = tk.Label(self, text = "PulseCountXH[1]", width = 14)
        self.lblPulseCountXH.grid(row = 3, column = 2, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPulseCountXH = tk.Spinbox(self, from_ = 1, to = 10000, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPulseCountXH.grid(row = 3, column = 3, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPulseCountXH.delete(0, tk.END)
        self.spbPulseCountXH.insert(0, INIT_PULSECOUNTXH)
        #
        #--------------------------------------------------------------
        # PulseCount-X-DPC [1]
        #--------------------------------------------------------------
        self.lblPulseCountDX = tk.Label(self, text = "PulseCountDX[1]", width = 14)
        self.lblPulseCountDX.grid(row = 3, column = 4, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPulseCountDX = tk.Spinbox(self, from_ = 1, to = 10000, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPulseCountDX.grid(row = 3, column = 5, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPulseCountDX.delete(0, tk.END)
        self.spbPulseCountDX.insert(0, INIT_PULSECOUNTDX)
        #
        #==============================================================
        #--------------------------------------------------------------
        # PositionYL [stp]
        #--------------------------------------------------------------
        self.lblPositionYL = tk.Label(self, text = "PositionYL[stp]", width = 14)
        self.lblPositionYL.grid(row = 4, column = 0, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPositionYL = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionYL.grid(row = 4, column = 1, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPositionYL.delete(0, tk.END)
        self.spbPositionYL.insert(0, INIT_POSITIONYL_STP)
        #
        #--------------------------------------------------------------
        # PositionYH [stp]
        #--------------------------------------------------------------
        self.lblPositionYH = tk.Label(self, text = "PositionYH[stp]", width = 14)
        self.lblPositionYH.grid(row = 4, column = 2, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPositionYH = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionYH.grid(row = 4, column = 3, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPositionYH.delete(0, tk.END)
        self.spbPositionYH.insert(0, INIT_POSITIONYH_STP)
        #
        #--------------------------------------------------------------
        # PositionDY [stp]
        #--------------------------------------------------------------
        self.lblPositionDY = tk.Label(self, text = "PositionDY[stp]", width = 14)
        self.lblPositionDY.grid(row = 4, column = 4, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPositionDY = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionDY.grid(row = 4, column = 5, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPositionDY.delete(0, tk.END)
        self.spbPositionDY.insert(0, INIT_POSITIONDY_STP)
        #
        #==============================================================
        #--------------------------------------------------------------
        # PulseCount-Y-PCL [1]
        #--------------------------------------------------------------
        self.lblPulseCountYL = tk.Label(self, text = "PulseCountYL[1]", width = 14)
        self.lblPulseCountYL.grid(row = 5, column = 0, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPulseCountYL = tk.Spinbox(self, from_ = 1, to = 10000, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPulseCountYL.grid(row = 5, column = 1, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPulseCountYL.delete(0, tk.END)
        self.spbPulseCountYL.insert(0, INIT_PULSECOUNTYL)
        #
        #--------------------------------------------------------------
        # PulseCount-Y-PCH [1]
        #--------------------------------------------------------------
        self.lblPulseCountYH = tk.Label(self, text = "PulseCountYH[1]", width = 14)
        self.lblPulseCountYH.grid(row = 5, column = 2, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPulseCountYH = tk.Spinbox(self, from_ = 1, to = 10000, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPulseCountYH.grid(row = 5, column = 3, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPulseCountYH.delete(0, tk.END)
        self.spbPulseCountYH.insert(0, INIT_PULSECOUNTYH)
        #
        #--------------------------------------------------------------
        # PulseCount-Y-DPC [1]
        #--------------------------------------------------------------
        self.lblPulseCountDY = tk.Label(self, text = "PulseCountDY[1]", width = 14)
        self.lblPulseCountDY.grid(row = 5, column = 4, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPulseCountDY = tk.Spinbox(self, from_ = 1, to = 10000, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPulseCountDY.grid(row = 5, column = 5, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPulseCountDY.delete(0, tk.END)
        self.spbPulseCountDY.insert(0, INIT_PULSECOUNTDY)
        #
        #==============================================================
        #--------------------------------------------------------------
        # Steplist - Button
        #--------------------------------------------------------------
        self.btnCreateSteplist = tk.Button(self, text = "Create Steplist", fg = "#000000", \
                                  width = int(0.50 * CONTROL_WIDTH), height = CONTROL_HEIGHT, \
                                  command = self.OnCreateSteplistClick)
        self.btnCreateSteplist.grid(row = 6, column = 0, columnspan = 1,
                                    padx = PAD_WIDTH_T, pady = PAD_HEIGHT_T)
        #---------------------------------------------------------------
        self.btnWriteSteplist = tk.Button(self, text = "Write Steplist", fg = "#000000", \
                                  width = int(0.50 * CONTROL_WIDTH), height = CONTROL_HEIGHT, \
                                  command = self.OnWriteSteplistClick)
        self.btnWriteSteplist.grid(row = 7, column = 0, columnspan = 1,
                                   padx = PAD_WIDTH_T, pady = PAD_HEIGHT_T)
        #
        self.entFilename = tk.Entry(self, width = int(2.64 * CONTROL_WIDTH))
        self.entFilename.insert(0, "Steptable.stt")
        self.entFilename.grid(row = 7, column = 1, columnspan = 5,
                              padx = PAD_WIDTH_T, pady = PAD_HEIGHT_T)
        #---------------------------------------------------------------
        #
        #---------------------------------------------------------------
        # Initialisation
        #--------------------------------------------------------------
        self.Steplist = SL.CSteplist()
        self.Steplist.Clear()
        #
        #
        self.RefreshSteplist()
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def GetStateSteplist(self):
        return self.StateSteplist
    #
    def SetStateSteplist(self, state):
        if ('sslInit' == state):
            self.Steplist.ResetStep()
            self.StateSteplist = 'sslBusy'
            # debug print("SSL[{0}]<-[{1}]".format(self.StateSteplist, state))
            return
        if ('sslAbort' == state):
            # abort execution...
            self.StateSteplist = 'sslZero'
            # debug print("SSL[{0}]<-[{1}]".format(self.StateSteplist, state))
            return
        # debug print("SSL[{0}]<-[{1}]".format(state, self.StateSteplist))
        self.StateSteplist = state
        return
    #
    #---------------------------------------------------------------
    # Management - Helper
    #--------------------------------------------------------------
    def RefreshSteplist(self):
        self.FrameStepTable.Clear()
        SC = self.Steplist.GetStepCount()
        for SI in range(SC):
            Step = self.Steplist.GetIndexStep(SI)
            self.FrameStepTable.AddRow(Step[0], Step[1], Step[2], Step[3], Step[4])
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    def OnCreateSteplistClick(self):
        PP = float(self.spbPulsePeriod.get())
        DMX = float(self.spbDelayMotionX.get())
        DMY = float(self.spbDelayMotionY.get())
        PXL = int(self.spbPositionXL.get())
        PXH = int(self.spbPositionXH.get())
        PDX = int(self.spbPositionDX.get())
        PYL = int(self.spbPositionYL.get())
        PYH = int(self.spbPositionYH.get())
        PDY = int(self.spbPositionDY.get())
        PCXL = int(self.spbPulseCountXL.get())
        PCXH = int(self.spbPulseCountXH.get())
        PCDX = int(self.spbPulseCountDX.get())
        PCYL = int(self.spbPulseCountYL.get())
        PCYH = int(self.spbPulseCountYH.get())
        PCDY = int(self.spbPulseCountDY.get())
        #
        self.Steplist.Clear()
        self.FrameStepTable.Clear()
        #
        PCY = PCYL
        DM = DMY
        for PY in range(PYL, PDY + PYH, PDY):
            PCX = PCXL
            for PX in range(PXL, PDX + PXH, PDX):
                PC = PCX + PCY
                S = [PX, PY, PP, PC, DM]
                self.Steplist.AddStep(S)
                if (PCX < PCXH):
                    PCX += PCDX
                DM = DMX
            if (PCY < PCYH):
                PCY += PCDY
            DM = DMY
        self.RefreshSteplist()
    #
    def OnWriteSteplistClick(self):
        FN = self.entFilename.get()
        T = 'Write Steplist to File'
        UD = '.\\'
        FT = (('StepTables','.stt'), ('All Files', '*.*'))
        FN = self.entFilename.get()
        FN = FD.asksaveasfilename(initialfile = FN, title = T, \
                                  initialdir = UD, filetypes = FT)
        PE, FN = os.path.split(FN)
        if 0 < len(FN):
            self.entFilename.delete(0, tk.END)
            self.entFilename.insert(0, FN)
            SC = self.Steplist.GetStepCount()
            if 0 < SC:
                F = open(self.entFilename.get(), "w")
                for SI in range(0, SC):
                    S = self.Steplist.GetIndexStep(SI)
                    if 4 < len(S):
                        F.write(MASK_FILE.format(S[0], S[1], S[2], S[3], S[4]))
                    else:
                        F.write(ERROR_WRITE)
                F.close()
                return True
        return False
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        PXL = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONXL_STP, \
                                        INIT_POSITIONXL_STP)
        PXH = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONXH_STP, \
                                        INIT_POSITIONXH_STP)
        PDX = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONDX_STP, \
                                        INIT_POSITIONDX_STP)
        PYL = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONYL_STP, \
                                        INIT_POSITIONYL_STP)
        PYH = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONYH_STP, \
                                        INIT_POSITIONYH_STP)
        PDY = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONDY_STP, \
                                        INIT_POSITIONDY_STP)
        PP = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_PULSEPERIOD_MS, \
                                        INIT_PULSEPERIOD_MS)
        PC = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_PULSECOUNT, \
                                        INIT_PULSECOUNT)
        DM = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_DELAYMOTION_MS, \
                                        INIT_DELAYMOTION_MS)
        #
        self.spbPositionXL.delete(0, tk.END)
        self.spbPositionXL.insert(0, PXL)
        self.spbPositionXH.delete(0, tk.END)
        self.spbPositionXH.insert(0, PXH)
        self.spbPositionDX.delete(0, tk.END)
        self.spbPositionDX.insert(0, PDX)
        self.spbPositionYL.delete(0, tk.END)
        self.spbPositionYL.insert(0, PYL)
        self.spbPositionYH.delete(0, tk.END)
        self.spbPositionYH.insert(0, PYH)
        self.spbPositionDY.delete(0, tk.END)
        self.spbPositionDY.insert(0, PDY)
        self.spbPulsePeriod.delete(0, tk.END)
        self.spbPulsePeriod.insert(0, PP)
        self.spbPulseCount.delete(0, tk.END)
        self.spbPulseCount.insert(0, PC)
        self.spbDelayMotion.delete(0, tk.END)
        self.spbDelayMotion.insert(0, DM)
        #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONXL_STP, \
                                 self.spbPositionXL.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONXH_STP, \
                                 self.spbPositionXH.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONDX_STP, \
                                 self.spbPositionDX.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONYL_STP, \
                                 self.spbPositionYL.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONYH_STP, \
                                 self.spbPositionYH.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONDY_STP, \
                                 self.spbPositionDY.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PULSEPERIOD_MS, \
                                 self.spbPulsePeriod.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PULSECOUNT, \
                                 self.spbPulseCount.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_DELAYMOTION_MS, \
                                 self.spbDelayMotion.get())
    #
    #-----------------------------------------------------------------------------------
    # Helper - File
    #-----------------------------------------------------------------------------------
    def GetFilePositionEnd(self, file):
        file.seek(0, 2)
        EP = file.tell()
        file.seek(0)
        return EP
    #
    def IsEndOfFile(self, file, filepositionend):
        return (filepositionend <= file.tell())
    #
    #---------------------------------------------------------------
    # Management - Execute
    #--------------------------------------------------------------
    def Execute(self):
        if ('sslWait' == self.GetStateSteplist()):
            return
        if ('sslBusy' == self.GetStateSteplist()):
            StepIndex, Step = self.Steplist.GetStepIndexCommandParameter()
            self.FrameStepTable.SetStepMarked(StepIndex)
            if (Step):
                # DEBUG print(Step)
                CommandText = "MPP"
                for PI in range(0, len(Step)):
                    CommandText += " " + str(Step[PI])
                # DEBUG print(CommandText)
                self.OnAppendCommand(CommandText)
                self.SetStateSteplist('sslWait')
            else:
                self.SetStateSteplist('sslZero')



