#
# ------------------------------------------------------------------
#   LMC - PCLaserMatrixCreator
# ------------------------------------------------------------------
#   Version: 00V01
#   Date   : 200728
#   Time   : 1130
#   Author : OMDevelop
#
import WindowMain as WM
#
import Defines
#
###################################################################
# Main
###################################################################
print("*** " + Defines.TITLE_APPLICATION + ": begin")
#
WindowMain = WM.CWindowMain()
WindowMain.Execute()
#
print("*** " + Defines.TITLE_APPLICATION + ": end")
#

