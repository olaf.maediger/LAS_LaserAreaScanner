#
SHORT_APPLICATION = "LMC"
NAME_APPLICATION = "PCLaserMatrixCreator"
TITLE_APPLICATION = SHORT_APPLICATION + " - " + NAME_APPLICATION
#
NAME_INITFILE = NAME_APPLICATION + ".ini"
#
TITLE_WINDOW = TITLE_APPLICATION

APPLICATION_VERSION = "Version: 02V08"
APPLICATION_DATE = "Date: 200805"
APPLICATION_TIME = "Time: 2156"
APPLICATION_AUTHOR = "Author: OMDevelop"
APPLICATION_ABSTRACT = "Abstract:\r\n" + \
              "- Gui for Steptable-Generator\r\n" + \
              "- Input: none\r\n" + \
              "- Output: <file>.stt"

INFO_ABOUT = TITLE_WINDOW + "\r\n\r\n" + \
             APPLICATION_VERSION + "\r\n" + \
             APPLICATION_DATE + "\r\n" + \
             APPLICATION_TIME + "\r\n" + \
             APPLICATION_AUTHOR + "\r\n\r\n" + \
             APPLICATION_ABSTRACT



MASK_FILE = "{0} {1} {2} {3} {4}"



