#
#------------------------------------------------------------------
#   <short> - <project>
#------------------------------------------------------------------
#   Date   : 20mmdd
#   Time   : hhmm
#   Author : OMDevelop
#
#
#import io
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CLaserMatrix():
    #
    def __init__(self, rowcount = 3, colcount = 3):
        self.RowCount = rowcount
        self.ColCount = colcount
    #
    def GetRowCount(self):
        return self.RowCount
    #
    def GetColCount(self):
        return self.ColCount
    #
    def ExecuteConstant(self, filename):
        print('Create Steptable Constant[' + filename + ']: start')
        File = open(filename, "w")
        for RI in range(0, self.RowCount):
            for CI in range(0, self.ColCount):
                PX = int(1800.5 + 400 * CI / (self.ColCount - 1))
                PY = int(1800.5 + 400 * RI / (self.RowCount - 1))
                PP = 0.01
                PC = 1
                DM = 0.001
                Line = "{0} {1} {2} {3} {4}\n".format(PY, PX, PP, PC, DM)
                File.write(Line)
        File.flush()
        File.close()
        print('Create Steptable : end')
    #
    def ExecuteRowLinear(self, filename):
        print('Create Steptable RowLinear[' + filename + ']: start')
        File = open(filename, "w")
        for RI in range(0, self.RowCount):
            for CI in range(0, self.ColCount):
                PX = int(1800.5 + 400 * CI / (self.ColCount - 1))
                PY = int(1800.5 + 400 * RI / (self.RowCount - 1))
                PP = 0.01
                PC = CI
                DM = 0.001
                Line = "{0} {1} {2} {3} {4}\n".format(PY, PX, PP, PC, DM)
                File.write(Line)
        File.flush()
        File.close()
        print('Create Steptable : end')

#
#------------------------------------------------------------------
#   Global Variable
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Function
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Callback
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Main
#------------------------------------------------------------------
#
if '__main__' == __name__:
    print('*** LaserMatrix *** : start')
    LM = CLaserMatrix()
    print('RowCount[' + str(LM.GetRowCount()) + ']')
    print('ColCount[' + str(LM.GetColCount()) + ']')
    LM.ExecuteConstant('SteptableConstant.txt')
    LM.ExecuteRowLinear('SteptableRowLinear.txt')
    print('*** LaserMatrix *** : end')

