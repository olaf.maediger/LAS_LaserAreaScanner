#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameLaserMatrix - Image
# ------------------------------------------------------------------
#   Version: 01V01
#   Date   : 200803
#   Time   : 1452
#   Author : OMDevelop
#
import tkinter as tk
import os
#import cv2 as OCV
from PIL import Image
#
from tkinter import filedialog as FD
import FrameStepTable as FST
import Steplist as SL
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
WIDTH_BUTTON                    = 24
#
INITDATA_SECTION                = "FrameLaserMatrixImage"
#
NAME_PULSEPERIOD_MS             = "PulsePeriod[ms]"
NAME_DELAYMOTIONX_MS            = "DelayMotionX[ms]"
NAME_DELAYMOTIONY_MS            = "DelayMotionY[ms]"
NAME_PULSECOUNTL                = "PulseCountL[1]"
NAME_PULSECOUNTH                = "PulseCountH[1]"
NAME_POSITIONXL_STP             = "PositionXL[stp]"
NAME_POSITIONDX_STP             = "PositionDX[stp]"
NAME_POSITIONXH_STP             = "PositionXH[stp]"
NAME_PIXELFACTORX               = "PositionFactorX[1]"
NAME_POSITIONYL_STP             = "PositionYL[stp]"
NAME_POSITIONDY_STP             = "PositionDY[stp]"
NAME_POSITIONYH_STP             = "PositionYH[stp]"
NAME_PIXELFACTORY               = "PositionFactorY[1]"
#
INIT_PULSEPERIOD_MS             = "10"
INIT_DELAYMOTIONX_MS            = "10"
INIT_DELAYMOTIONY_MS            = "20"
INIT_PULSECOUNTL                = "0"
INIT_PULSECOUNTH                = "255"
INIT_POSITIONXL_STP             = "1800"
INIT_POSITIONDX_STP             = "1"
INIT_POSITIONXH_STP             = "2200"
INIT_PIXELFACTORX               = "1"
INIT_POSITIONYL_STP             = "1800"
INIT_POSITIONDY_STP             = "1"
INIT_POSITIONYH_STP             = "2200"
INIT_PIXELFACTORY               = "1"
#------------------------------------------------------------------
#
CONTROL_WIDTH = 26
CONTROL_HEIGHT = 1
#
WIDTH_EDT = 6
#
PAD_WIDTH_A = 2
PAD_HEIGHT_A = 2
#
PAD_WIDTH_B = 2
PAD_HEIGHT_B = 2
#
PAD_WIDTH_T = 2
PAD_HEIGHT_T = 2
#
MASK_FILE = "{0} {1} {2} {3} {4}\n"
ERROR_WRITE = "Error: Undefined Step!"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameLaserMatrixImage(tk.Frame):
#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 840, height = 620, \
                          padx = PAD_WIDTH_A, pady = PAD_HEIGHT_A, background = "#ddffee")
        self.grid_propagate(0)
        #
        #--------------------------------------------------------------
        # Steplist - Steptable
        #--------------------------------------------------------------
        self.FrameStepTable = FST.CFrameStepTable(self, 610, 394, 20)
        self.FrameStepTable.grid(row = 0, column = 0, columnspan = 123, \
                                  sticky = tk.W, padx = PAD_WIDTH_A, pady = PAD_HEIGHT_A)
        #
        #==============================================================
        #--------------------------------------------------------------
        # Steplist - Button
        #--------------------------------------------------------------
        self.btnReadImage = tk.Button(self, text = "Read Image", fg = "#000000", \
                                  width = int(0.50 * CONTROL_WIDTH), height = CONTROL_HEIGHT, \
                                  command = self.OnReadImageClick)
        self.btnReadImage.grid(row = 1, column = 0, columnspan = 1,
                                   padx = PAD_WIDTH_T, pady = PAD_HEIGHT_T)
        #
        self.entReadImage = tk.Entry(self, width = int(2.4 * CONTROL_WIDTH))
        self.entReadImage.insert(0, 'Chess5x5BW.png')
        self.entReadImage.grid(row = 1, column = 1, columnspan = 5,
                                  padx = PAD_WIDTH_T, pady = PAD_HEIGHT_T)
        #---------------------------------------------------------------
        #
        #=R1===========================================================
        #--------------------------------------------------------------
        # PulsePeriod [ms]
        #--------------------------------------------------------------
        self.lblPulsePeriod = tk.Label(self, text = "PulsePeriod[ms]", width = 14)
        self.lblPulsePeriod.grid(row = 2, column = 0, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPulsePeriod = tk.Spinbox(self, from_ = -10000.000, to = +10000.000, \
                                        increment = 1.0, width = WIDTH_EDT, justify = "center")
        self.spbPulsePeriod.grid(row = 2, column = 1, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPulsePeriod.delete(0, tk.END)
        self.spbPulsePeriod.insert(0, INIT_PULSEPERIOD_MS)
        #
        #--------------------------------------------------------------
        # DelayMotion-X [ms]
        #--------------------------------------------------------------
        self.lblDelayMotionX = tk.Label(self, text = "DelayMotionX[ms]", width = 14)
        self.lblDelayMotionX.grid(row = 2, column = 2, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbDelayMotionX = tk.Spinbox(self, from_ = 0, to = 9999, increment = 1, \
                                         width = WIDTH_EDT, justify = "center")
        self.spbDelayMotionX.grid(row = 2, column = 3, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbDelayMotionX.delete(0, tk.END)
        self.spbDelayMotionX.insert(0, INIT_DELAYMOTIONX_MS)
        #
        #--------------------------------------------------------------
        # DelayMotion-Y [ms]
        #--------------------------------------------------------------
        self.lblDelayMotionY = tk.Label(self, text = "DelayMotionY[ms]", width = 14)
        self.lblDelayMotionY.grid(row = 2, column = 4, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbDelayMotionY = tk.Spinbox(self, from_ = 0, to = 9999, increment = 1, \
                                         width = WIDTH_EDT, justify = "center")
        self.spbDelayMotionY.grid(row = 2, column = 5, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbDelayMotionY.delete(0, tk.END)
        self.spbDelayMotionY.insert(0, INIT_DELAYMOTIONY_MS)
        #
        #==============================================================
        #
        #--------------------------------------------------------------
        # PulseCount-L [1]
        #--------------------------------------------------------------
        self.lblPulseCountL = tk.Label(self, text = "PulseCountL[1]", width = 14)
        self.lblPulseCountL.grid(row = 3, column = 0, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPulseCountL = tk.Spinbox(self, from_ = 1, to = 1000, increment = 1, \
                                          width = WIDTH_EDT, justify = "center")
        self.spbPulseCountL.grid(row = 3, column = 1, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPulseCountL.delete(0, tk.END)
        self.spbPulseCountL.insert(0, INIT_PULSECOUNTL)
        #
        #--------------------------------------------------------------
        # PulseCount-H [1]
        #--------------------------------------------------------------
        self.lblPulseCountH = tk.Label(self, text = "PulseCounH[1]", width = 14)
        self.lblPulseCountH.grid(row = 3, column = 2, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPulseCountH = tk.Spinbox(self, from_ = 1, to = 10000, increment = 1, \
                                          width = WIDTH_EDT, justify = "center")
        self.spbPulseCountH.grid(row = 3, column = 3, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPulseCountH.delete(0, tk.END)
        self.spbPulseCountH.insert(0, INIT_PULSECOUNTH)
        #
        #==============================================================
        #--------------------------------------------------------------
        # PositionXL [stp]
        #--------------------------------------------------------------
        self.lblPositionXL = tk.Label(self, text = "PositionXL[stp]", width = 14)
        self.lblPositionXL.grid(row = 4, column = 0, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPositionXL = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = WIDTH_EDT, justify = "center")
        self.spbPositionXL.grid(row = 4, column = 1, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPositionXL.delete(0, tk.END)
        self.spbPositionXL.insert(0, INIT_POSITIONXL_STP)
        #
        #--------------------------------------------------------------
        # PositionDX [stp]
        #--------------------------------------------------------------
        self.lblPositionDX = tk.Label(self, text = "PositionDX[stp]", width = 14)
        self.lblPositionDX.grid(row = 4, column = 2, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPositionDX = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = WIDTH_EDT, justify = "center")
        self.spbPositionDX.grid(row = 4, column = 3, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPositionDX.delete(0, tk.END)
        self.spbPositionDX.insert(0, INIT_POSITIONDX_STP)
        # #
        # #--------------------------------------------------------------
        # # PositionXH [stp]
        # #--------------------------------------------------------------
        # self.lblPositionXH = tk.Label(self, text = "PositionXH[stp]", width = 14)
        # self.lblPositionXH.grid(row = 4, column = 4, sticky = "w",
        #                        padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        # #
        # self.spbPositionXH = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
        #                                  width = WIDTH_EDT, justify = "center")
        # self.spbPositionXH.grid(row = 4, column = 5, sticky = "w",
        #                        padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        # self.spbPositionXH.delete(0, tk.END)
        # self.spbPositionXH.insert(0, INIT_POSITIONXH_STP)
        # self.spbPositionXH.config(state = 'disabled')
        #
        #--------------------------------------------------------------
        # PixelFactorX [1]
        #--------------------------------------------------------------
        self.lblPixelFactorX = tk.Label(self, text = "PixelFactorX[1]", width = 14)
        self.lblPixelFactorX.grid(row = 4, column = 4, sticky = "w",
                                  padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPixelFactorX = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                          width = WIDTH_EDT, justify = "center")
        self.spbPixelFactorX.grid(row = 4, column = 5, sticky = "w",
                                  padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPixelFactorX.delete(0, tk.END)
        self.spbPixelFactorX.insert(0, INIT_PIXELFACTORX)
        #self.spbPixelFactorX.config(state = 'disabled')
        #
        #==============================================================
        #--------------------------------------------------------------
        # PositionYL [stp]
        #--------------------------------------------------------------
        self.lblPositionYL = tk.Label(self, text = "PositionYL[stp]", width = 14)
        self.lblPositionYL.grid(row = 5, column = 0, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPositionYL = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = WIDTH_EDT, justify = "center")
        self.spbPositionYL.grid(row = 5, column = 1, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPositionYL.delete(0, tk.END)
        self.spbPositionYL.insert(0, INIT_POSITIONYL_STP)
        #
        #--------------------------------------------------------------
        # PositionDY [stp]
        #--------------------------------------------------------------
        self.lblPositionDY = tk.Label(self, text = "PositionDY[stp]", width = 14)
        self.lblPositionDY.grid(row = 5, column = 2, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPositionDY = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = WIDTH_EDT, justify = "center")
        self.spbPositionDY.grid(row = 5, column = 3, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPositionDY.delete(0, tk.END)
        self.spbPositionDY.insert(0, INIT_POSITIONDY_STP)
        # #
        # #--------------------------------------------------------------
        # # PositionYH [stp]
        # #--------------------------------------------------------------
        # self.lblPositionYH = tk.Label(self, text = "PositionYH[stp]", width = 14)
        # self.lblPositionYH.grid(row = 5, column = 4, sticky = "w",
        #                        padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        # #
        # self.spbPositionYH = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
        #                                  width = WIDTH_EDT, justify = "center")
        # self.spbPositionYH.grid(row = 5, column = 5, sticky = "w",
        #                        padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        # self.spbPositionYH.delete(0, tk.END)
        # self.spbPositionYH.insert(0, INIT_POSITIONYH_STP)
        # self.spbPositionYH.config(state = 'disabled')
        #
        #--------------------------------------------------------------
        # PixelFactorY [1]
        #--------------------------------------------------------------
        self.lblPixelFactorY = tk.Label(self, text = "PixelFactorY[1]", width = 14)
        self.lblPixelFactorY.grid(row = 5, column = 4, sticky = "w",
                                  padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        #
        self.spbPixelFactorY = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                          width = WIDTH_EDT, justify = "center")
        self.spbPixelFactorY.grid(row = 5, column = 5, sticky = "w",
                                  padx = PAD_WIDTH_B, pady = PAD_HEIGHT_B)
        self.spbPixelFactorY.delete(0, tk.END)
        self.spbPixelFactorY.insert(0, INIT_PIXELFACTORY)
        #self.spbPixelFactorY.config(state = 'disabled')
        #
        #==============================================================
        #--------------------------------------------------------------
        # Steplist - Button
        #--------------------------------------------------------------
        self.btnWriteSteplist = tk.Button(self, text = "Write Steplist", fg = "#000000", \
                                  width = int(0.50 * CONTROL_WIDTH), height = CONTROL_HEIGHT, \
                                  command = self.OnWriteSteplistClick)
        self.btnWriteSteplist.grid(row = 8, column = 0, columnspan = 1,
                                   padx = PAD_WIDTH_T, pady = PAD_HEIGHT_T)
        #
        self.entWriteSteplist = tk.Entry(self, width = int(2.4 * CONTROL_WIDTH))
        self.entWriteSteplist.insert(0, "Steptable.stt")
        self.entWriteSteplist.grid(row = 8, column = 1, columnspan = 5,
                              padx = PAD_WIDTH_T, pady = PAD_HEIGHT_T)
        #---------------------------------------------------------------
        #
        #---------------------------------------------------------------
        # Initialisation
        #--------------------------------------------------------------
        self.PixelRowSize = 0
        self.PixelColSize = 0

        self.Steplist = SL.CSteplist()
        self.Steplist.Clear()
        #
        self.Image = None
        #
        self.RefreshSteplist()
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def GetStateSteplist(self):
        return self.StateSteplist
    #
    def SetStateSteplist(self, state):
        if ('sslInit' == state):
            self.Steplist.ResetStep()
            self.StateSteplist = 'sslBusy'
            # debug print("SSL[{0}]<-[{1}]".format(self.StateSteplist, state))
            return
        if ('sslAbort' == state):
            # abort execution...
            self.StateSteplist = 'sslZero'
            # debug print("SSL[{0}]<-[{1}]".format(self.StateSteplist, state))
            return
        # debug print("SSL[{0}]<-[{1}]".format(state, self.StateSteplist))
        self.StateSteplist = state
        return
    #
    #---------------------------------------------------------------
    # Management - Helper
    #--------------------------------------------------------------
    def RefreshSteplist(self):
        self.FrameStepTable.Clear()
        SC = self.Steplist.GetStepCount()
        for SI in range(SC):
            Step = self.Steplist.GetIndexStep(SI)
            self.FrameStepTable.AddRow(Step[0], Step[1], Step[2], Step[3], Step[4])
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    def BuildStep(self, pixelcolors, pixelrowindex, pixelcolindex, \
                  positiony, positionx, delaymotion):
        Step = []
        PulsePeriod = float(self.spbPulsePeriod.get())
        PCL = int(self.spbPulseCountL.get())
        PCH = int(self.spbPulseCountH.get())
        PDX = int(self.spbPositionDX.get())
        PFX = int(self.spbPixelFactorX.get())
        PI = pixelrowindex * self.PixelRowSize + pixelcolindex
        CRed, CGreen, CBlue = pixelcolors[PI]
        CGrey = int(0.5 + 0.3 * CRed +0.59 * CGreen + 0.11 * CBlue)
        #print(CR, CG, CB)
        PC = int(0.5 + PCL + (PCH - PCL) * CGrey / 255.0)
        Step = [positionx, positiony, PulsePeriod, PC, delaymotion]
        return Step
    #
    def BuildStepLine(self, pixelcolors, pixelrowindex, positiony):
        Steplist = []
        PXL = int(self.spbPositionXL.get())
        PDX = int(self.spbPositionDX.get())
        PFX = int(self.spbPixelFactorX.get())
        DM = float(self.spbDelayMotionY.get())
        for PCI in range(self.PixelColSize):
            for XFI in range(PFX): # Col-Repetitions
                PositionX = PXL + (PCI * PFX + XFI)* PDX 
                #print(PositionX)
                SL = self.BuildStep(pixelcolors, pixelrowindex, PCI, positiony, PositionX, DM)
                Steplist.append(SL)
                DM = float(self.spbDelayMotionX.get())
        return Steplist
    #
    def BuildStepMatrix(self, pixelcolors):
        Steplist = []
        PYL = int(self.spbPositionYL.get())
        PDY = int(self.spbPositionDY.get())
        PFY = int(self.spbPixelFactorY.get())
        for PRI in range(self.PixelRowSize):
            for YFI in range(PFY): # Row-Repetitions               
                PositionY = PYL + (PRI * PFY + YFI) * PDY
                #print(PositionY)
                SL = self.BuildStepLine(pixelcolors, PRI, PositionY)
                Steplist.extend(SL)
        return Steplist
    #
    def OnReadImageClick(self):
        #
        self.Steplist.Clear()
        self.FrameStepTable.Clear()
        #
        Filename = self.entReadImage.get()  #'Image.png'
        #Filename = 'Chess5x5BW.png'
        self.PixelRowSize = 0
        self.PixelColSize = 0
        self.Image = Image.open(Filename)
        if (self.Image is not None):
            #self.Image.show()
            self.PixelColSize, self.PixelRowSize = self.Image.size
            #print(self.PixelColSize, self.PixelRowSize)
            PixelColors = list(self.Image.getdata())
            self.Steplist.Define(self.BuildStepMatrix(PixelColors))
            #
            self.RefreshSteplist()
            return True
        return False
    #
    def OnWriteSteplistClick(self):
        FN = self.entWriteSteplist.get()
        T = 'Write Steplist to File'
        UD = '.\\'
        FT = (('StepTables','.stt'), ('All Files', '*.*'))
        FN = FD.asksaveasfilename(initialfile = FN, title = T, \
                                  initialdir = UD, filetypes = FT)
        PE, FN = os.path.split(FN)
        if 0 < len(FN):
            self.entWriteSteplist.delete(0, tk.END)
            self.entWriteSteplist.insert(0, FN)
            SC = self.Steplist.GetStepCount()
            if 0 < SC:
                F = open(FN, "w")
                for SI in range(0, SC):
                    S = self.Steplist.GetIndexStep(SI)
                    if 4 < len(S):
                        F.write(MASK_FILE.format(S[0], S[1], S[2], S[3], S[4]))
                    else:
                        F.write(ERROR_WRITE)
                F.close()
                return True
        return False
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        PP = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_PULSEPERIOD_MS, \
                                        INIT_PULSEPERIOD_MS)
        DMX = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_DELAYMOTIONX_MS, \
                                         INIT_DELAYMOTIONX_MS)
        DMY = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_DELAYMOTIONY_MS, \
                                         INIT_DELAYMOTIONY_MS)
        PCL = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_PULSECOUNTL, \
                                         INIT_PULSECOUNTL)
        PCH = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_PULSECOUNTH, \
                                         INIT_PULSECOUNTH)
        PXL = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_POSITIONXL_STP, \
                                         INIT_POSITIONXL_STP)
        PDX = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_POSITIONDX_STP, \
                                         INIT_POSITIONDX_STP)
        PXH = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_POSITIONXH_STP, \
                                         INIT_POSITIONXH_STP)
        PFX = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_PIXELFACTORX, \
                                         INIT_PIXELFACTORX)
        PYL = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_POSITIONYL_STP, \
                                         INIT_POSITIONYL_STP)
        PDY = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_POSITIONDY_STP, \
                                         INIT_POSITIONDY_STP)
        PYH = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_POSITIONYH_STP, \
                                         INIT_POSITIONYH_STP)
        PFY = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                         NAME_PIXELFACTORY, \
                                         INIT_PIXELFACTORY)
        #
        self.spbPulsePeriod.delete(0, tk.END)
        self.spbPulsePeriod.insert(0, PP)
        #
        self.spbDelayMotionX.delete(0, tk.END)
        self.spbDelayMotionX.insert(0, DMX)
        #
        self.spbDelayMotionY.delete(0, tk.END)
        self.spbDelayMotionY.insert(0, DMY)
        #
        self.spbPulseCountL.delete(0, tk.END)
        self.spbPulseCountL.insert(0, PCL)
        #
        self.spbPulseCountH.delete(0, tk.END)
        self.spbPulseCountH.insert(0, PCH)
        #
        self.spbPositionXL.delete(0, tk.END)
        self.spbPositionXL.insert(0, PXL)
        #
        self.spbPositionDX.delete(0, tk.END)
        self.spbPositionDX.insert(0, PDX)
        #
        self.spbPositionXH.delete(0, tk.END)
        self.spbPositionXH.insert(0, PXH)
        #
        self.spbPixelFactorX.delete(0, tk.END)
        self.spbPixelFactorX.insert(0, PFX)
        #
        self.spbPositionYL.delete(0, tk.END)
        self.spbPositionYL.insert(0, PYL)
        #
        self.spbPositionDY.delete(0, tk.END)
        self.spbPositionDY.insert(0, PDY)
        #
        self.spbPositionYH.delete(0, tk.END)
        self.spbPositionYH.insert(0, PYH)
        #
        self.spbPixelFactorY.delete(0, tk.END)
        self.spbPixelFactorY.insert(0, PFY)
        #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        #
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PULSEPERIOD_MS, \
                                 self.spbPulsePeriod.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_DELAYMOTIONX_MS, \
                                 self.spbDelayMotionX.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_DELAYMOTIONY_MS, \
                                 self.spbDelayMotionY.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PULSECOUNTL, \
                                 self.spbPulseCountL.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PULSECOUNTH, \
                                 self.spbPulseCountH.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONXL_STP, \
                                 self.spbPositionXL.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONDX_STP, \
                                 self.spbPositionDX.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONXH_STP, \
                                 self.spbPositionXH.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PIXELFACTORX, \
                                 self.spbPixelFactorX.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONYL_STP, \
                                 self.spbPositionYL.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONDY_STP, \
                                 self.spbPositionDY.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONYH_STP, \
                                 self.spbPositionYH.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PIXELFACTORY, \
                                 self.spbPixelFactorY.get())
    #
    #-----------------------------------------------------------------------------------
    # Helper - File
    #-----------------------------------------------------------------------------------
    def GetFilePositionEnd(self, file):
        file.seek(0, 2)
        EP = file.tell()
        file.seek(0)
        return EP
    #
    def IsEndOfFile(self, file, filepositionend):
        return (filepositionend <= file.tell())
    #
    #---------------------------------------------------------------
    # Management - Execute
    #--------------------------------------------------------------
    def Execute(self):
        if ('sslWait' == self.GetStateSteplist()):
            return
        if ('sslBusy' == self.GetStateSteplist()):
            StepIndex, Step = self.Steplist.GetStepIndexCommandParameter()
            self.FrameStepTable.SetStepMarked(StepIndex)
            if (Step):
                # DEBUG print(Step)
                CommandText = "MPP"
                for PI in range(0, len(Step)):
                    CommandText += " " + str(Step[PI])
                # DEBUG print(CommandText)
                self.OnAppendCommand(CommandText)
                self.SetStateSteplist('sslWait')
            else:
                self.SetStateSteplist('sslZero')



