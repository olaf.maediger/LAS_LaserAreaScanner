#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameLaserMatrix
# ------------------------------------------------------------------
#   Version: 02V04
#   Date   : 200625
#   Time   : 0820
#   Author : OMDevelop
#
import tkinter as tk
#
from tkinter import filedialog as FD
import FrameStepTable as FST
import Steplist as SL
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
WIDTH_BUTTON                    = 24
#
INITDATA_SECTION                = "FrameLaserMatrixConstant"
#
NAME_POSITIONXL_STP              = "PositionXL[stp]"
NAME_POSITIONXH_STP              = "PositionXH[stp]"
NAME_POSITIONDX_STP              = "PositionDX[stp]"
NAME_POSITIONYL_STP              = "PositionYL[stp]"
NAME_POSITIONYH_STP              = "PositionYH[stp]"
NAME_POSITIONDY_STP              = "PositionDY[stp]"
NAME_PULSEPERIOD_MS             = "PulsePeriod[ms]"
NAME_PULSECOUNT                 = "PulseCount[1]"
NAME_DELAYMOTION_MS             = "DelayMotion[ms]"
#
INIT_POSITIONXL_STP              = "1800"
INIT_POSITIONXH_STP              = "2200"
INIT_POSITIONDX_STP              = "200"
INIT_POSITIONYL_STP              = "1800"
INIT_POSITIONYH_STP              = "2200"
INIT_POSITIONDY_STP              = "200"
INIT_PULSEPERIOD_MS             = "1000"
INIT_PULSECOUNT                 = "1"
INIT_DELAYMOTION_MS             = "10"
#------------------------------------------------------------------
#
CONTROL_WIDTH = 26
CONTROL_HEIGHT = 1
#
PAD_WIDTH_A = 2
PATH_HEIGHT_A = 2
#
PAD_WIDTH_B = 2
PATH_HEIGHT_B = 2
#
PAD_WIDTH_T = 2
PATH_HEIGHT_T = 2
#
MASK_FILE = "{0} {1} {2} {3} {4}\n"
ERROR_WRITE = "Error: Undefined Step!"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameLaserMatrixConstant(tk.Frame):
#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 880, height = 569, \
                          padx = PAD_WIDTH_A, pady = PATH_HEIGHT_A, background = "#ddffee")
        self.grid_propagate(0)
        #
        #--------------------------------------------------------------
        # Steplist - Steptable
        #--------------------------------------------------------------
        self.FrameStepTable = FST.CFrameStepTable(self)
        self.FrameStepTable.grid(row = 0, column = 0, columnspan = 123, \
                                  sticky = tk.W, padx = PAD_WIDTH_A, pady = PATH_HEIGHT_A)
        #
        #--------------------------------------------------------------
        # PositionXL [stp]
        #--------------------------------------------------------------
        self.lblPositionXL = tk.Label(self, text = "PositionXL[stp]", width = 14)
        self.lblPositionXL.grid(row = 1, column = 0, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        #
        self.spbPositionXL = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionXL.grid(row = 1, column = 1, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        self.spbPositionXL.delete(0, tk.END)
        self.spbPositionXL.insert(0, INIT_POSITIONXL_STP)
        #
        #--------------------------------------------------------------
        # PositionXH [stp]
        #--------------------------------------------------------------
        self.lblPositionXH = tk.Label(self, text = "PositionXH[stp]", width = 14)
        self.lblPositionXH.grid(row = 1, column = 2, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        #
        self.spbPositionXH = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionXH.grid(row = 1, column = 3, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        self.spbPositionXH.delete(0, tk.END)
        self.spbPositionXH.insert(0, INIT_POSITIONXH_STP)
        #
        #--------------------------------------------------------------
        # PositionDX [stp]
        #--------------------------------------------------------------
        self.lblPositionDX = tk.Label(self, text = "PositionDX[stp]", width = 14)
        self.lblPositionDX.grid(row = 1, column = 4, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        #
        self.spbPositionDX = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionDX.grid(row = 1, column = 5, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        self.spbPositionDX.delete(0, tk.END)
        self.spbPositionDX.insert(0, INIT_POSITIONDX_STP)
        #
        #--------------------------------------------------------------
        # PositionYL [stp]
        #--------------------------------------------------------------
        self.lblPositionYL = tk.Label(self, text = "PositionYL[stp]", width = 14)
        self.lblPositionYL.grid(row = 2, column = 0, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        #
        self.spbPositionYL = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionYL.grid(row = 2, column = 1, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        self.spbPositionYL.delete(0, tk.END)
        self.spbPositionYL.insert(0, INIT_POSITIONYL_STP)
        #
        #--------------------------------------------------------------
        # PositionYH [stp]
        #--------------------------------------------------------------
        self.lblPositionYH = tk.Label(self, text = "PositionYH[stp]", width = 14)
        self.lblPositionYH.grid(row = 2, column = 2, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        #
        self.spbPositionYH = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionYH.grid(row = 2, column = 3, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        self.spbPositionYH.delete(0, tk.END)
        self.spbPositionYH.insert(0, INIT_POSITIONYH_STP)
        #
        #--------------------------------------------------------------
        # PositionDY [stp]
        #--------------------------------------------------------------
        self.lblPositionDY = tk.Label(self, text = "PositionDY[stp]", width = 14)
        self.lblPositionDY.grid(row = 2, column = 4, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        #
        self.spbPositionDY = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionDY.grid(row = 2, column = 5, sticky = "w",
                               padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        self.spbPositionDY.delete(0, tk.END)
        self.spbPositionDY.insert(0, INIT_POSITIONDY_STP)
        #
        #--------------------------------------------------------------
        # PulsePeriod [ms]
        #--------------------------------------------------------------
        self.lblPulsePeriod = tk.Label(self, text = "PulsePeriod[ms]", width = 14)
        self.lblPulsePeriod.grid(row = 3, column = 0, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        #
        self.spbPulsePeriod = tk.Spinbox(self, from_ = -10000.000, to = +10000.000, \
                                        increment = 1.0, width = 8, justify = "center")
        self.spbPulsePeriod.grid(row = 3, column = 1, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        self.spbPulsePeriod.delete(0, tk.END)
        self.spbPulsePeriod.insert(0, INIT_PULSEPERIOD_MS)
        #
        #--------------------------------------------------------------
        # PulseCount [1]
        #--------------------------------------------------------------
        self.lblPulseCount = tk.Label(self, text = "PulseCount[1]", width = 14)
        self.lblPulseCount.grid(row = 3, column = 2, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        #
        self.spbPulseCount = tk.Spinbox(self, from_ = 1, to = 10000, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPulseCount.grid(row = 3, column = 3, sticky = "w",
                                padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        self.spbPulseCount.delete(0, tk.END)
        self.spbPulseCount.insert(0, INIT_PULSECOUNT)
        #
        #--------------------------------------------------------------
        # DelayMotion [ms]
        #--------------------------------------------------------------
        self.lblDelayMotion = tk.Label(self, text = "DelayMotion[ms]", width = 14)
        self.lblDelayMotion.grid(row = 3, column = 4, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        #
        self.spbDelayMotion = tk.Spinbox(self, from_ = 0, to = 9999, increment = 1, \
                                         width = 8, justify = "center")
        self.spbDelayMotion.grid(row = 3, column = 5, sticky = "w",
                                 padx = PAD_WIDTH_B, pady = PATH_HEIGHT_B)
        self.spbDelayMotion.delete(0, tk.END)
        self.spbDelayMotion.insert(0, INIT_DELAYMOTION_MS)
        #
        #--------------------------------------------------------------
        # Steplist - Button
        #--------------------------------------------------------------
        self.btnCreateSteplist = tk.Button(self, text = "Create Steplist", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT, \
                                  command = self.OnCreateSteplistClick)
        self.btnCreateSteplist.grid(row = 4, column = 0, columnspan = 2,
                                    padx = PAD_WIDTH_T, pady = PATH_HEIGHT_T)
        #---------------------------------------------------------------
        self.btnWriteSteplist = tk.Button(self, text = "Write Steplist", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT, \
                                  command = self.OnWriteSteplistClick)
        self.btnWriteSteplist.grid(row = 4, column = 2, columnspan = 2,
                                   padx = PAD_WIDTH_T, pady = PATH_HEIGHT_T)
        #
        #---------------------------------------------------------------
        # Initialisation
        #--------------------------------------------------------------
        self.Steplist = SL.CSteplist()
        self.Steplist.Clear()
        #self.Steplist.InitSimple(3)
        # debug self.Steplist.Debug("Steplist", "---")
        #
        #
        self.RefreshSteplist()
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def GetStateSteplist(self):
        return self.StateSteplist
    #
    def SetStateSteplist(self, state):
        if ('sslInit' == state):
            self.Steplist.ResetStep()
            self.StateSteplist = 'sslBusy'
            # debug print("SSL[{0}]<-[{1}]".format(self.StateSteplist, state))
            return
        if ('sslAbort' == state):
            # abort execution...
            self.StateSteplist = 'sslZero'
            # debug print("SSL[{0}]<-[{1}]".format(self.StateSteplist, state))
            return
        # debug print("SSL[{0}]<-[{1}]".format(state, self.StateSteplist))
        self.StateSteplist = state
        return

    #
    def SetCBOnAppendCommand(self, callback):
        self.CBOnAppendCommand = callback
    #
    #---------------------------------------------------------------
    # Management - Helper
    #--------------------------------------------------------------
    def RefreshSteplist(self):
        SC = self.Steplist.GetStepCount()
        for SI in range(SC):
            Step = self.Steplist.GetIndexStep(SI)
            self.FrameStepTable.AddRow(Step[0], Step[1], Step[2], Step[3], Step[4])
    #

    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    def OnCreateSteplistClick(self):
        PXL = int(self.spbPositionXL.get())
        PXH = int(self.spbPositionXH.get())
        PDX = int(self.spbPositionDX.get())
        PYL = int(self.spbPositionYL.get())
        PYH = int(self.spbPositionYH.get())
        PDY = int(self.spbPositionDY.get())
        PP = float(self.spbPulsePeriod.get())
        PC = int(self.spbPulseCount.get())
        DM = float(self.spbDelayMotion.get())
        #
        self.Steplist.Clear()
        for PY in range(PYL, PDY + PYH, PDY):
            for PX in range(PXL, PDX + PXH, PDX):
                S = [PX, PY, PP, PC, DM]
                self.Steplist.AddStep(S)
        self.RefreshSteplist()
    #
    def OnWriteSteplistClick(self):
        IF = 'MicroMachining.stt'
        T = 'Write Steplist to File'
        UD = '.\\'
        FT = (('StepTables','.stt'), ('All Files', '*.*'))
        FileEntry = FD.asksaveasfilename(initialfile = IF, title = T,
                                        initialdir = UD, filetypes = FT)
        if (FileEntry):
            SC = self.Steplist.GetStepCount()
            if 0 < SC:
                F = open(FileEntry, "w")
                for SI in range(0, SC):
                    S = self.Steplist.GetIndexStep(SI)
                    if 4 < len(S):
                        F.write(MASK_FILE.format(S[0], S[1], S[2], S[3], S[4]))
                    else:
                        F.write(ERROR_WRITE)
                F.close()
                return True
        return False




        # IF = 'MicroMachining.stt'
        # T = 'Write Steplist to File'
        # UD = '.\\'
        # FT = (('StepTables','.stt'), ('All Files', '*.*'))
        # FileEntry = FD.asksaveasfilename(initialfile = IF, title = T,
        #                                  initialdir = UD, filetypes = FT)
        # if (FileEntry):
        #     SC = self.Steplist.GetStepCount()
        #     F = open(FileEntry, "w")
        #     for SI in range(0, SC):
        #         S = self.Steplist.GetStep(SI)
        #         if 4 < len(S):
        #             F.write(MASK_FILE.format(S[0], S[1], S[2], S[3], S[4]))
        #     F.close()
        # return

    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        PXL = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONXL_STP, \
                                        INIT_POSITIONXL_STP)
        PXH = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONXH_STP, \
                                        INIT_POSITIONXH_STP)
        PDX = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONDX_STP, \
                                        INIT_POSITIONDX_STP)
        PYL = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONYL_STP, \
                                        INIT_POSITIONYL_STP)
        PYH = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONYH_STP, \
                                        INIT_POSITIONYH_STP)
        PDY = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONDY_STP, \
                                        INIT_POSITIONDY_STP)
        PP = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_PULSEPERIOD_MS, \
                                        INIT_PULSEPERIOD_MS)
        PC = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_PULSECOUNT, \
                                        INIT_PULSECOUNT)
        DM = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_DELAYMOTION_MS, \
                                        INIT_DELAYMOTION_MS)
        #
        self.spbPositionXL.delete(0, tk.END)
        self.spbPositionXL.insert(0, PXL)
        self.spbPositionXH.delete(0, tk.END)
        self.spbPositionXH.insert(0, PXH)
        self.spbPositionDX.delete(0, tk.END)
        self.spbPositionDX.insert(0, PDX)
        self.spbPositionYL.delete(0, tk.END)
        self.spbPositionYL.insert(0, PYL)
        self.spbPositionYH.delete(0, tk.END)
        self.spbPositionYH.insert(0, PYH)
        self.spbPositionDY.delete(0, tk.END)
        self.spbPositionDY.insert(0, PDY)
        self.spbPulsePeriod.delete(0, tk.END)
        self.spbPulsePeriod.insert(0, PP)
        self.spbPulseCount.delete(0, tk.END)
        self.spbPulseCount.insert(0, PC)
        self.spbDelayMotion.delete(0, tk.END)
        self.spbDelayMotion.insert(0, DM)
        #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONXL_STP, \
                                 self.spbPositionXL.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONXH_STP, \
                                 self.spbPositionXH.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONDX_STP, \
                                 self.spbPositionDX.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONYL_STP, \
                                 self.spbPositionYL.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONYH_STP, \
                                 self.spbPositionYH.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONDY_STP, \
                                 self.spbPositionDY.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PULSEPERIOD_MS, \
                                 self.spbPulsePeriod.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PULSECOUNT, \
                                 self.spbPulseCount.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_DELAYMOTION_MS, \
                                 self.spbDelayMotion.get())
    #
    #-----------------------------------------------------------------------------------
    # Helper - File
    #-----------------------------------------------------------------------------------
    def GetFilePositionEnd(self, file):
        file.seek(0, 2)
        EP = file.tell()
        file.seek(0)
        return EP
    #
    def IsEndOfFile(self, file, filepositionend):
        return (filepositionend <= file.tell())
    #
    #---------------------------------------------------------------
    # Management - Execute
    #--------------------------------------------------------------
    def Execute(self):
        if ('sslWait' == self.GetStateSteplist()):
            return
        if ('sslBusy' == self.GetStateSteplist()):
            StepIndex, Step = self.Steplist.GetStepIndexCommandParameter()
            self.FrameStepTable.SetStepMarked(StepIndex)
            if (Step):
                # DEBUG print(Step)
                CommandText = "MPP"
                for PI in range(0, len(Step)):
                    CommandText += " " + str(Step[PI])
                # DEBUG print(CommandText)
                self.OnAppendCommand(CommandText)
                self.SetStateSteplist('sslWait')
            else:
                self.SetStateSteplist('sslZero')



