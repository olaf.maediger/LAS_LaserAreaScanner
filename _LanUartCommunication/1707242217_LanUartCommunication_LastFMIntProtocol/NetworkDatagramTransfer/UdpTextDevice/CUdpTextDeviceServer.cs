﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using Network;
using UdpTextTransfer;
//
namespace UdpTextDevice
{
  public class CUdpTextDeviceServer : CUdpTextDeviceBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpTextDeviceServer()
            : base()
    {
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public override Boolean Open()
    {
      FUdpTextTransmitter.Open(FTargetIpAddress,
                               FTargetIpPort);
      FUdpTextReceiver.Open(FSourceIpAddress,
                            FSourceIpPort);
      return true;
    }

    public override Boolean Close()
    {
      FUdpTextTransmitter.Close();
      FUdpTextReceiver.Close();
      return true;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public Function
    //--------------------------------------------------------------------------
    //
    public Boolean Preset(String message)
    {
      try
      {
        FUdpTextTransmitter.Preset(message);
        return true;      
      }
      catch (Exception)
      {
        return false;
      }
    }

//    public Boolean PresetContent(CTagTextList content)
//    {
//      try
//      {
//        //if (FUdpTextDatagramContainerTransmit.PresetContent(content))
//        //{
//        //  FUdpTextTransmitter.PresetUdpText("aa");//.//PresetUdpTextDatagramContainerTransmit(FUdpTextDatagramContainerTransmit);
//        //  FUdpTextDatagramContainerTransmit.Clear();
//        //  return true;
//        //}
//        return false;
//      }
//      catch (Exception)
//      {
//        return false;
//      }
//    }

//    public Boolean PresetMessages(String[] messages)
//    {
//      try
//      {
////        if (FUdpTextDatagramContainerTransmit.PresetMessages(messages))
// //       {
//          FUdpTextTransmitter.PresetUdpText(messages[0]);//.//PresetUdpTextDatagramContainerTransmit(FUdpTextDatagramContainerTransmit);
//          // !!! FUdpTextDatagramContainerTransmit.Clear();
//          return true;
//        //}
//        //return false;
//      }
//      catch (Exception)
//      {
//        return false;
//      }
//    }

//    public Boolean PresetVector2DDouble(Double[,] values)
//    {
//      try
//      {
//////        if (FUdpTextDatagramContainerTransmit.PresetVector2DDouble(values))
////        {
////          FUdpTextTransmitter.PresetUdpText("a");//.//PresetUdpTextDatagramContainerTransmit(FUdpTextDatagramContainerTransmit);
////          //!!! FUdpTextDatagramContainerTransmit.Clear();
////          return true;
////        }
//        return false;
//      }
//      catch (Exception)
//      {
//        return false;
//      }
//    }

  }
}
