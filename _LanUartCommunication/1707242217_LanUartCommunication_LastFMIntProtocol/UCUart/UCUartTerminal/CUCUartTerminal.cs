﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
//
using Uart;
using UCNotifier;
using TextFile;
using UCUartOpenClose;
//
namespace UCUartTerminal
{ //
  //-------------------------------
  //  Segment - Type
  //-------------------------------
  //
  public delegate void DOnTerminalOpen(String portname, EComPort comport, EBaudrate baudrate, EParity parity,
                                       EDatabits databits, EStopbits stopbits, EHandshake handshake);
  public delegate void DOnTerminalClose(EComPort comport);
  //
  //##################################################
  //  Segment - Main - CUCUartTerminal
  //##################################################
  //
  public partial class CUCUartTerminal : UserControl
  { //
    //-------------------------------
    //  Segment - Constant
    //-------------------------------
    // Color RXD
    private Byte INIT_COLORRXD_A = 0xFF;
    private Byte INIT_COLORRXD_R = 0xA9;
    private Byte INIT_COLORRXD_G = 0x22;
    private Byte INIT_COLORRXD_B = 0xAA;
    // Color TXD
    private Byte INIT_COLORTXD_A = 0xFF;
    private Byte INIT_COLORTXD_R = 0x00;
    private Byte INIT_COLORTXD_G = 0x88;
    private Byte INIT_COLORTXD_B = 0xFF;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //		
    private CNotifier FNotifier;
    
    private Color FTxdColor;
    private Color FRxdColor;
    private DOnTerminalOpen FOnTerminalOpen;
    private DOnTerminalClose FOnTerminalClose;
    private DOnTransmitText FOnTransmitText;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUCUartTerminal()
    {
      InitializeComponent();
      //
      FTxdColor = Color.FromArgb(INIT_COLORTXD_A, INIT_COLORTXD_R, INIT_COLORTXD_G, INIT_COLORTXD_B);
      FRxdColor = Color.FromArgb(INIT_COLORRXD_A, INIT_COLORRXD_R, INIT_COLORRXD_G, INIT_COLORRXD_B);
      //
      FUCTransmitLine.SetOnShowDialogOpenClose(UCTransmitLineOnShowDialogOpenClose);
      FUCTransmitLine.SetOnTransmitText(UCTransmitLineOnTransmitText);
      //
      FUCUartOpenClose.RefreshComPortControls();
      FUCUartOpenClose.SetOnUCOpenCloseChanged(UCUartOpenCloseOnUCOpenCloseChanged);
      //
      FUCMultiColorText.Dock = DockStyle.Fill;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      FUCMultiColorText.SetNotifier(notifier);
      FUCTransmitLine.SetNotifier(notifier);
      FUCUartOpenClose.SetNotifier(notifier);
    }

    public void SetOnTerminalOpen(DOnTerminalOpen value)
    {
      FOnTerminalOpen = value;
    }

    public void SetOnTerminalClose(DOnTerminalClose value)
    {
      FOnTerminalClose = value;
    }

    public void SetOnTransmitText(DOnTransmitText value)
    {
      FOnTransmitText = value;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    private void AddColoredText(Color color, String text)
    {
      FUCMultiColorText.Add(color, text);
    }

    private void AddTerminalLine(Color color, String text)
    {
      AddColoredText(color, text);
    }

    private void AddTerminalRxData(String text)
    {
      AddColoredText(FRxdColor, text);
    }

    private void AddTerminalTxData(String text)
    {
      AddColoredText(FTxdColor, text);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Callback - UCTransmitLine
    //------------------------------------------------------------------------
    //           
    private void UCUartOpenCloseOnUCOpenCloseChanged(Boolean opened)
    {
      if (opened)
      {
        String PortName = FUCUartOpenClose.GetName();
        EComPort ComPort = FUCUartOpenClose.GetComPort();
        EBaudrate Baudrate = FUCUartOpenClose.GetBaudrate();
        EParity Parity = FUCUartOpenClose.GetParity();
        EDatabits Databits = FUCUartOpenClose.GetDatabits();
        EStopbits Stopbits = FUCUartOpenClose.GetStopbits();
        EHandshake Handshake = FUCUartOpenClose.GetHandshake();
        if (FOnTerminalOpen is DOnTerminalOpen)
        {
          FOnTerminalOpen(PortName, ComPort, Baudrate, Parity, Databits, Stopbits, Handshake);
          FUCUartOpenClose.SetOpened(true);
        }
      }
      else
      {
        if (FOnTerminalClose is DOnTerminalClose)
        {
          EComPort ComPort = FUCUartOpenClose.GetComPort();
          FOnTerminalClose(ComPort);
          FUCUartOpenClose.SetOpened(false);
        }
      }
    }

    private void UCTransmitLineOnShowDialogOpenClose(Boolean visible)
    {
      FUCUartOpenClose.Visible = visible;
    }

    private void UCTransmitLineOnTransmitText(String text, Int32 delaycharacter, Int32 delaycr, Int32 delaylf)
    {
      if (FOnTransmitText is DOnTransmitText)
      {
        AddTerminalTxData("TxD>" + text);
        FOnTransmitText(text, delaycharacter, delaycr, delaylf);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public 
    //------------------------------------------------------------------------
    //
    public void TextReceived(String text)
    {
      AddTerminalRxData("RxD>" + text);// + "\r\n");
    }
    public void LineReceived(String text)
    {
      AddTerminalRxData("RxD>" + text + "\r\n");
    }
    public void TextTransmitted(String text)
    {
      AddTerminalTxData("TxD>" + text);// + "\r\n");
    }
  }
}



//private delegate void CBUartOnTextReceived(Guid comportid, String text);
//private void UartOnTextReceived(Guid comportid, String text)
//{
//  if (this.InvokeRequired)
//  {
//    CBUartOnTextReceived CB = new CBUartOnTextReceived(UartOnTextReceived);
//    Invoke(CB, new object[] { comportid, text });
//  }
//  else
//  { // nothing to do -> OnLineReceived
//    //!!!lbxUartBase.Items.Add("[RxText]" + text);
//    // ... FNotifier.Add("[RxText]" + text);
//    if (FOnTextReceived is DOnTextReceived)
//    {
//      FOnTextReceived(comportid, text);
//    }
//  }
//}












//    ////
//    ////-------------------------------
//    ////  Segment - Event
//    ////-------------------------------
//    //// cbxSendLine: Enter -> Send
//    //private void cbxSendData_KeyDown(object sender, KeyEventArgs e)
//    //{
//    //  TransmitLine(cbxTransmitLine.Text);
//    //}

//    //private Boolean LoadTransmitLines(String filename)
//    //{
//    //  if (File.Exists(filename))
//    //  {
//    //    CTextFile TextFile = new CTextFile();
//    //    //???? TextFile.SetNotifier(FNotifier);
//    //    if (TextFile.OpenRead(filename))
//    //    {
//    //      String Line;
//    //      while (!TextFile.IsEndOfFile())
//    //      {
//    //        if (TextFile.Read(out Line))
//    //        {
//    //          cbxTransmitLine.Items.Add(Line);
//    //        }
//    //      }
//    //      TextFile.Close();
//    //    }
//    //    return true;
//    //  }
//    //  return false;
//    //}

//    //private Boolean SaveTransmitLines(String filename)
//    //{
//    //  CTextFile TextFile = new CTextFile();
//    //  //?????????????TextFile.SetNotifier(FNotifier);
//    //  TextFile.OpenWrite(filename);
//    //  foreach (String Line in cbxTransmitLine.Items)
//    //  {
//    //    TextFile.Write(Line + CUart.CHARACTER_CR + CUart.CHARACTER_LF);
//    //  }
//    //  TextFile.Close();
//    //  return true;
//    //}
//    //
//    //-------------------------------
//    //  Segment - Menu
//    //-------------------------------
//    //
//    private void mitClearTerminal_Click(object sender, EventArgs e)
//    {
//      ClearTerminalText();
//    }

//    private void btnTransmitLine_Click(object sender, EventArgs e)
//    {
//      TransmitLine(cbxTransmitLine.Text);
//    }

//    private void mitSaveTransmitLines_Click(object sender, EventArgs e)
//    {
//      // NC DialogSaveTransmitLines.FileName = "";
//      if (DialogResult.OK == DialogSaveTransmitLines.ShowDialog())
//      {
//        SaveTransmitLines(DialogSaveTransmitLines.FileName);
//      }
//    }

//    private void mitLoadTransmitLines_Click(object sender, EventArgs e)
//    {
//      // NC DialogLoadTransmitLines.FileName = "";
//      if (DialogResult.OK == DialogLoadTransmitLines.ShowDialog())
//      {
//        LoadTransmitLines(DialogLoadTransmitLines.FileName);
//      }
//    }

//    private void cbxShowOpenClose_CheckedChanged(object sender, EventArgs e)
//    {
//      FUCUartOpenClose.Visible = cbxShowOpenClose.Checked;
//    }
//    //
//    //-------------------------------
//    //  Segment - Public Management
//    //-------------------------------
//    //
//    public void TransmitLine(String line)
//    {
//      String Line = line;
//      if (cbxCR.Checked)
//      {
//        Line += CUart.CHARACTER_CR;
//      }
//      if (cbxLF.Checked)
//      {
//        Line += CUart.CHARACTER_LF;
//      }
//      FUart.WriteText(Line);
//    }

//    public void ClearTerminalText()
//    {
//      FUCMultiColorText.Clear();
//    }

//    public void EnableTerminalText(Boolean enable)
//    {
//      FUCMultiColorText.EnableText(enable);
//    }


//  }
//}








////

//    public CUCTerminal()
//    {
//      InitializeComponent();
//      FTxdColor = Color.FromArgb(INIT_COLORTXD_A, INIT_COLORTXD_R, INIT_COLORTXD_G, INIT_COLORTXD_B);
//      FRxdColor = Color.FromArgb(INIT_COLORRXD_A, INIT_COLORRXD_R, INIT_COLORRXD_G, INIT_COLORRXD_B);
//      cbxSendData.Items.Clear();
//      LoadText(FILENAME_COMMANDS);
//      //
//      cbxShowOpenClose.Checked = true;
//      ////
//      //FUCUartOpenClose.SetOnUartOpenClose(TerminalOnOpenClose);
//      //FUCUartOpenClose.SetOnUartErrorDetected(TerminalOnErrorDetected);
//      //FUCUartOpenClose.SetOnUartPinChanged(TerminalOnPinChanged);
//      //FUCUartOpenClose.SetOnUartTextReceived(TerminalOnTextReceived);
//      //FUCUartOpenClose.SetOnUartLineReceived(TerminalOnLineReceived);
//      //FUCUartOpenClose.SetOnUartTextTransmitted(TerminalOnTextTransmitted);
//      //FUCUartOpenClose.SetOnUartLineTransmitted(TerminalOnLineTransmitted);
//    }
//    //
//    //-------------------------------
//    //  Segment - Property
//    //-------------------------------
//    //
//    //public void SetOnSendData(DOnSendData value)
//    //{
//    //  FOnSendData = value;
//    //}

//    public void SetNotifier(CNotifier notifier)
//    {
//      FNotifier = notifier;
//    }

//    public Int32 DelayCharacter
//    {
//      get { return (Int32)nudDelayCharacter.Value; }
//      set { nudDelayCharacter.Value = value; }
//    }
//    public Int32 DelayCR
//    {
//      get { return (Int32)nudDelayCR.Value; }
//      set { nudDelayCR.Value = value; }
//    }
//    public Int32 DelayLF
//    {
//      get { return (Int32)nudDelayLF.Value; }
//      set { nudDelayLF.Value = value; }
//    }
//    //
//    //-------------------------------
//    //  Segment - Event
//    //-------------------------------
//    // cbxSendLine: Enter -> Send
//    private void cbxSendData_KeyDown(object sender, KeyEventArgs e)
//    {
//      if (e.KeyCode == Keys.Enter)
//      {
//        btnSendData_Click(this, null);
//      }
//    }
//    //
//    //-------------------------------
//    //  Segment - Helper
//    //-------------------------------
//    //
//    private void AddColoredText(Color color, String text)
//    {
//      FUCMultiColorText.Add(color, text);
//    }


//    //
//    //-------------------------------
//    //  Segment - Menu
//    //-------------------------------
//    //




//    //
//    //--------------------------------------------------------------------
//    //  Segment - Terminal - Callback
//    //--------------------------------------------------------------------
//    //
//    private void TerminalOnOpenClose(Guid comportid, Boolean open)
//    {

//    }

//    private void TerminalOnErrorDetected(Guid comportid, Int32 errorcode, String errortext)
//    {
//      //String SError = String.Format("{Error[%d]: %s!", errorcode, errortext);
//      // NC
//    }

//    private void TerminalOnTextTransmitted(Guid comportid, String text)
//    {
//      // NC
//    }

//    private delegate void CBTerminalOnLineTransmitted(Guid comportid, String line);
//    private void TerminalOnLineTransmitted(Guid comportid, String line)
//    {
//      if (this.InvokeRequired)
//      {
//        CBTerminalOnLineTransmitted CB = new CBTerminalOnLineTransmitted(TerminalOnLineTransmitted);
//        Invoke(CB, new object[] { comportid, line });
//      }
//      else
//      {
//        FUCMultiColorText.Add(Color.Blue, line);
//      }     
//    }

//    private delegate void CBTerminalOnTextReceived(Guid comportid, String text);
//    private void TerminalOnTextReceived(Guid comportid, String text)
//    {
//      if (this.InvokeRequired)
//      {
//        CBTerminalOnTextReceived CB = new CBTerminalOnTextReceived(TerminalOnTextReceived);
//        Invoke(CB, new object[] { comportid, text });
//      }
//      else
//      {
//        // NC
//      }
//    }

//    private delegate void CBTerminalOnLineReceived(Guid comportid, String line);
//    private void TerminalOnLineReceived(Guid comportid, String line)
//    {
//      if (this.InvokeRequired)
//      {
//        CBTerminalOnLineReceived CB = new CBTerminalOnLineReceived(TerminalOnLineReceived);
//        Invoke(CB, new object[] { comportid, line });
//      }
//      else
//      {
//        FUCMultiColorText.Add(Color.Green, line);     
//      }
//    }

//    private void TerminalOnPinChanged(Guid comportid,
//                                  Boolean pincts, Boolean pinrts,
//                                  Boolean pindsr, Boolean pindtr, Boolean pindcd)
//    {
//      // NC
//    }
//    //
//    //-------------------------------
//    //  Segment - Management
//    //-------------------------------
//    //
//    public void Clear()
//    {
//      FUCMultiColorText.Clear();
//    }

//    public void EnableText(Boolean enable)
//    {
//      FUCMultiColorText.EnableText(enable);
//    }

//    public void Add(Color color, String text)
//    {
//      AddColoredText(color, text);
//    }

//    public void AddRxData(String text)
//    {
//      AddColoredText(FRxdColor, text);
//    }

//    public void AddTxData(String text)
//    {
//      AddColoredText(FTxdColor, text);
//    }

//    public void EnableControls(Boolean enable)
//    {
//      this.Enabled = enable;
//    }

//    private void cbxShowOpenClose_CheckedChanged(object sender, EventArgs e)
//    {
//      FUCUartOpenClose.Visible = cbxShowOpenClose.Checked;
//    }

//  }
//}

