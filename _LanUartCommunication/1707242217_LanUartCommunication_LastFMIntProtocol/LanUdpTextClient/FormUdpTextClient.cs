﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using Network;
using NetworkDevice;
using UdpTextTransfer;
using UdpTextDevice;
//
namespace LanUdpTextClient
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormClient : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICETYPE = "LanUdpTextClient";
    private const String INIT_DEVICENAME = "OMDevelopDebugUdpTextBinaryClient";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    private CDeviceParameter FDeviceParameter;
    private CUdpTextDeviceClient FUdpTextDeviceClient;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      //####################################################################################
      // Init - Instance - Client
      //####################################################################################
      //
      FDeviceParameter = new CDeviceParameter();
      FDeviceParameter.Type = INIT_DEVICETYPE;
      FDeviceParameter.Name = INIT_DEVICENAME;
      FDeviceParameter.ID = Guid.NewGuid();
      IPAddress IPALocal;
      CNetwork.FindIPAddressLocal(out IPALocal);
      FDeviceParameter.IPA = IPALocal;
      IPAddress IPATarget;
      CNetwork.FindIPAddressLocalBroadcast(out IPATarget);
      //
      // UdpTextDeviceClient
      FUdpTextDeviceClient = new CUdpTextDeviceClient();
      FUdpTextDeviceClient.SetNotifier(FUCNotifier);
      FUdpTextDeviceClient.SetOnDatagramTransmitted(UdpTextDeviceClientOnDatagramTransmitted);
      FUdpTextDeviceClient.SetOnDatagramReceived(UdpTextDeviceClientOnDatagramReceived);
      FUdpTextDeviceClient.SetSourceIpAddress(IPALocal);
      FUdpTextDeviceClient.SetSourceIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_CLIENT_TRANSMITTER);
      FUdpTextDeviceClient.SetTargetIpAddress(IPATarget);
      FUdpTextDeviceClient.SetTargetIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_CLIENT_RECEIVER);
      FUdpTextDeviceClient.Open();
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      // Start Parallel-Application: LanUdpTextServer.exe
      // Process.Start("LanUdpTextServer.exe");
      Process.Start("LanUartUdpTextServer.exe");
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FUdpTextDeviceClient.Close();
      // -> LanUdpTextServer
      //Process[] ProcessesLanUdpServer = Process.GetProcessesByName("LanUdpTextServer");
      //foreach (Process ProcessLanUdpServer in ProcessesLanUdpServer)
      //{
      //  ProcessLanUdpServer.Kill();
      //}
      Process[] ProcessesLanUdpServer = Process.GetProcessesByName("LanUartUdpTextServer");
      foreach (Process ProcessLanUdpServer in ProcessesLanUdpServer)
      {
        ProcessLanUdpServer.Kill();
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //#########################################################################
    //  Segment - Callback - UdpTextDeviceClient
    //#########################################################################
    //
    private void UdpTextDeviceClientOnDatagramTransmitted(IPEndPoint ipendpointtarget,
                                                           Byte[] datagram, Int32 size)
    {
      String SDatagram = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
      String Line = String.Format("TxdUdp[{0}]:<{1}>", CNetwork.IPEndPointToText(ipendpointtarget), SDatagram);
      FUCNotifier.Write(Line);
    }

    private void UdpTextDeviceClientOnDatagramReceived(IPEndPoint ipendpointsource,
                                                        Byte[] datagram, Int32 size)
    {
      String SDatagram = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
      String Line = String.Format("RxdUdp[{0}]:<{1}>", CNetwork.IPEndPointToText(ipendpointsource), SDatagram);
      FUCNotifier.Write(Line);
    }   
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          tbcMain.SelectedIndex = 1;
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:        
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          // debug 
          FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          String Message = "Hello World! (Client -> Server)";
          FUdpTextDeviceClient.Preset(Message);
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }

  }
}


      //Char[] Separators = new Char[1];
      //Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
      //String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
      //foreach (String Tag in Tags)
      //{
      //  FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
      //}

      //Char[] Separators = new Char[1];
      //Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
      //String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
      //foreach (String Tag in Tags)
      //{
      //  FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
      //}


      //
      //  ---------------------------------------------------
      //  UdpTextDeviceClient
      //  ---------------------------------------------------
      //////////////////FUdpTextDeviceClient = new CUdpTextDeviceClient();
      //////////////////// UdpTextDeviceServer - Common
      //////////////////FUdpTextDeviceClient.SetNotifier(FUCNotifier);
      //////////////////FUdpTextDeviceClient.SetOnDatagramTransmitted(UdpTextDeviceClientOnDatagramTransmitted);
      //////////////////FUdpTextDeviceClient.SetOnDatagramReceived(UdpTextDeviceClientOnDatagramReceived);
      //////////////////// UdpTextDeviceClient - Source
      //////////////////FUdpTextDeviceClient.SetSourceIpAddress(IPALocal);
      //////////////////FUdpTextDeviceClient.SetSourceIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_CLIENT_RECEIVER);
      //////////////////FUdpTextDeviceClient.SetSourceID(FDeviceParameter.ID);
      //////////////////FUdpTextDeviceClient.SetSourceType("NhdDataMessageClient");
      //////////////////FUdpTextDeviceClient.SetSourceName("PSimonOfficeDataAnalysis");
      //////////////////// UdpTextDeviceClient - Target
      //////////////////FUdpTextDeviceClient.SetTargetIpAddress(IPATarget);
      //////////////////FUdpTextDeviceClient.SetTargetIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_CLIENT_TRANSMITTER);
      //////////////////FUdpTextDeviceClient.SetTargetID(Guid.Empty);
      //////////////////FUdpTextDeviceClient.SetTargetType("NhdDataMessageClient");
      //////////////////FUdpTextDeviceClient.SetTargetName("PSimonLaborDataCollection");
      //////////////////// UdpTextDeviceClient - Open
      //////////////////FUdpTextDeviceClient.Open();
      //
      //  ---------------------------------------------------
      //  UdpTextArrayClient 
      //  ---------------------------------------------------
      ////////////////////FUdpTextArrayClient = new CUdpTextArrayClient();
      ////////////////////// UdpTextArrayClient - Common
      ////////////////////FUdpTextArrayClient.SetNotifier(FUCNotifier);
      ////////////////////FUdpTextArrayClient.SetOnDatagramTransmitted(UdpTextArrayClientOnDatagramTransmitted);
      ////////////////////FUdpTextArrayClient.SetOnDatagramReceived(UdpTextArrayClientOnDatagramReceived);
      ////////////////////// UdpTextArrayClient - Source
      ////////////////////FUdpTextArrayClient.SetSourceIpAddress(IPALocal);
      ////////////////////FUdpTextArrayClient.SetSourceIpPort(CNetworkDevice.PORT_UDPTEXT_ARRAY_CLIENT_RECEIVER);
      ////////////////////FUdpTextArrayClient.SetSourceID(FDeviceParameter.ID);
      ////////////////////FUdpTextArrayClient.SetSourceType("NhdDataArrayClient");
      ////////////////////FUdpTextArrayClient.SetSourceName("PSimonLaborDataCollection");
      ////////////////////// UdpTextArrayClient - Target
      ////////////////////FUdpTextArrayClient.SetTargetIpAddress(IPATarget);
      ////////////////////FUdpTextArrayClient.SetTargetIpPort(CNetworkDevice.PORT_UDPTEXT_ARRAY_CLIENT_TRANSMITTER);
      ////////////////////FUdpTextArrayClient.SetTargetID(Guid.Empty);
      ////////////////////FUdpTextArrayClient.SetTargetType("NhdDataArrayServer");
      ////////////////////FUdpTextArrayClient.SetTargetName("PSimonOfficeDataAnalysis");
      ////////////////////// // UdpTextArrayClient - Open
      ////////////////////FUdpTextArrayClient.Open();
      //
      //  ---------------------------------------------------
      //  UdpBinaryDeviceClient
      //  ---------------------------------------------------
      //FUdpBinaryDeviceClient = new CUdpBinaryDeviceClient();
      //// UdpBinaryDeviceServer - Common
      //FUdpBinaryDeviceClient.SetNotifier(FUCNotifier);
      //FUdpBinaryDeviceClient.SetOnDatagramTransmitted(UdpBinaryDeviceClientOnDatagramTransmitted);
      //FUdpBinaryDeviceClient.SetOnDatagramReceived(UdpBinaryDeviceClientOnDatagramReceived);
      //FUdpBinaryDeviceClient.SetOnContentReceived(UdpBinaryDeviceClientOnContentReceived);
      //FUdpBinaryDeviceClient.SetOnMessagesReceived(UdpBinaryDeviceClientOnMessagesReceived);
      //FUdpBinaryDeviceClient.SetOnVector2DDoubleReceived(UdpBinaryDeviceClientOnVector2DDoubleReceived);
      //// UdpBinaryDeviceClient - Source
      //FUdpBinaryDeviceClient.SetSourceIpAddress(IPALocal);
      //FUdpBinaryDeviceClient.SetSourceIpPort(CNetworkDevice.PORT_UDPBINARY_MESSAGE_CLIENT_RECEIVER);
      //FUdpBinaryDeviceClient.SetSourceID(FDeviceParameter.ID);
      //FUdpBinaryDeviceClient.SetSourceType("NhdClient");
      //FUdpBinaryDeviceClient.SetSourceName("PSimonOfficeDataAnalysis");
      //// UdpBinaryDeviceClient - Target
      //FUdpBinaryDeviceClient.SetTargetIpAddress(IPATarget);
      //FUdpBinaryDeviceClient.SetTargetIpPort(CNetworkDevice.PORT_UDPBINARY_MESSAGE_CLIENT_TRANSMITTER);
      //FUdpBinaryDeviceClient.SetTargetID(Guid.Empty);
      //FUdpBinaryDeviceClient.SetTargetType("NhdClient");
      //FUdpBinaryDeviceClient.SetTargetName("PSimonLaborDataCollection");
      //// UdpBinaryDeviceClient - Open
      //FUdpBinaryDeviceClient.Open();
      //////////////////////
      //////////////////////####################################################################################
      ////////////////////// Init - UserControls - Server
      //////////////////////####################################################################################
      //////////////////////
      // UCNhdUdpTextClient
      //FUCNhdUdpTextClient.SetIpAddressLocal(IPALocal);
      //FUCNhdUdpTextClient.SetIpPortTransmit(CNetworkDevice.PORT_UDPTEXT_MESSAGE_CLIENT_TRANSMITTER);
      //FUCNhdUdpTextClient.SetIpPortReceive(CNetworkDevice.PORT_UDPTEXT_MESSAGE_CLIENT_RECEIVER);
      //FUCNhdUdpTextClient.SetIpAddressTarget(IPATarget);
      //FUCNhdUdpTextClient.SetOnSendMessage(UCNhdUdpTextClientOnSendMessage);

      ////////////////////// UCNhdUdpArrayServer
      ////////////////////FUCNhdUdpArrayClient.SetIpAddressLocal(IPALocal);
      ////////////////////FUCNhdUdpArrayClient.SetIpPortTransmit(CNetworkDevice.PORT_UDPTEXT_ARRAY_CLIENT_TRANSMITTER);
      ////////////////////FUCNhdUdpArrayClient.SetIpPortReceive(CNetworkDevice.PORT_UDPTEXT_ARRAY_CLIENT_RECEIVER);
      ////////////////////FUCNhdUdpArrayClient.SetIpAddressTarget(IPATarget);
      ////////////////////FUCNhdUdpArrayClient.SetOnSendParameter(UCNhdUdpArrayClientOnSendParameter);
      ////////////////////FUCNhdUdpArrayClient.SetOnSendRequest(UCNhdUdpArrayClientOnSendRequest);
      //

///// ///////////////////////////FUdpTextDeviceClient.Close();
///// ///////////////////////////FUdpTextArrayClient.Close();
//FUdpBinaryDeviceClient.Close();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUCNhdUdpTextClient.Close();
////////////
//
//#########################################################################
//  Segment - Callback - UDP
//#########################################################################
//
//------------------------------------------------------------------------
//  Section - Callback - UdpTextDeviceClient
//------------------------------------------------------------------------
//     
//////////////////////private void UdpTextDeviceClientOnDatagramTransmitted(IPEndPoint ipendpointtarget,
//////////////////////                                                       Byte[] datagram, Int32 size)
//////////////////////{
//////////////////////  String Line = String.Format("TxdUdpTextDeviceClient[{0}]:",
//////////////////////                              CNetwork.IPEndPointToText(ipendpointtarget));
//////////////////////  FUCNotifier.Write(Line);
//////////////////////  Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
//////////////////////  Char[] Separators = new Char[1];
//////////////////////  Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//////////////////////  String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//////////////////////  foreach (String Tag in Tags)
//////////////////////  {
//////////////////////    FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//////////////////////  }
//////////////////////}

//////////////////////private void UdpTextDeviceClientOnDatagramReceived(IPEndPoint ipendpointsource,
//////////////////////                                                    Byte[] datagram, Int32 size)
//////////////////////{
//////////////////////  String Line = String.Format("RxdUdpTextDeviceClient[{0}]:",
//////////////////////                              CNetwork.IPEndPointToText(ipendpointsource));
//////////////////////  FUCNotifier.Write(Line);
//////////////////////  Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
//////////////////////  Char[] Separators = new Char[1];
//////////////////////  Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//////////////////////  String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//////////////////////  foreach (String Tag in Tags)
//////////////////////  {
//////////////////////    FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//////////////////////  }
//////////////////////}
//
//------------------------------------------------------------------------
//  Section - Callback - UdpTextArrayClient
//------------------------------------------------------------------------
//     
//private void UdpTextArrayClientOnDatagramTransmitted(IPEndPoint ipendpointtarget,
//                                                      Byte[] datagram, Int32 size)
//{
//  String Line = String.Format("TxdUdpTextArrayClient[{0}]:",
//                              CNetwork.IPEndPointToText(ipendpointtarget));
//  FUCNotifier.Write(Line);
//  Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
//  Char[] Separators = new Char[1];
//  Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//  String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//  foreach (String Tag in Tags)
//  {
//    FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//  }
//}

//private void UdpTextArrayClientOnDatagramReceived(IPEndPoint ipendpointsource,
//                                                   Byte[] datagram, Int32 size)
//{
//  String Line = String.Format("RxdUdpTextArrayClient[{0}]:",
//                              CNetwork.IPEndPointToText(ipendpointsource));
//  FUCNotifier.Write(Line);
//  Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
//  Char[] Separators = new Char[1];
//  Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//  String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//  foreach (String Tag in Tags)
//  {
//    FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//  }
//}
//
//------------------------------------------------------------------------
//  Section - Callback - UdpBinaryDeviceClient
//------------------------------------------------------------------------
//     
//private void UdpBinaryDeviceClientOnDatagramTransmitted(IPEndPoint ipendpointtarget,
//                                                         Byte[] datagram, Int32 size)
//{
//  String Line = String.Format("TxdUdpBinaryDeviceClient[{0}]:",
//                              CNetwork.IPEndPointToText(ipendpointtarget));
//  FUCNotifier.Write(Line);
//  //Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
//  //Char[] Separators = new Char[1];
//  //Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//  //String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//  //foreach (String Tag in Tags)
//  //{
//  //  FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//  //}
//}

//private void UdpBinaryDeviceClientOnDatagramReceived(IPEndPoint ipendpointsource,
//                                                      Byte[] datagram, Int32 size)
//{
//  String Line = String.Format("RxdUdpBinaryDeviceClient[{0}]:",
//                              CNetwork.IPEndPointToText(ipendpointsource));
//  FUCNotifier.Write(Line);
//  //
//  //FUdpBinaryDeviceClient.PresetByteVector(datagram, size);
//  ///////////////// CUdpBinaryDatagram UdpBinaryDatagram;
//  ////////////////if (FUdpBinaryDeviceClient.ConvertToMessages(datagram, out UdpBinaryDatagram))
//  ////////////////{
//  ////////////////  CTagBinaryList Content = UdpBinaryDatagram.GetContent();
//  ////////////////  //CTBVectorString TBVectorString = ;
//  ////////////////}


//  //Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
//  //Char[] Separators = new Char[1];
//  //Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//  //String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//  //foreach (String Tag in Tags)
//  //{
//  //  FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//  //}
//}


//private void UdpBinaryDeviceClientOnContentReceived(Guid datagramid, CTagBinaryList content)
//{
//  CStringList StringList = content.GetInfo();
//  FUCNotifier.Write("*** Received Content:");
//  foreach (String S in StringList)
//  {
//    FUCNotifier.Write(S);
//  }
//}

//private void UdpBinaryDeviceClientOnMessagesReceived(Guid datagramid, String[] messages)
//{
//  Int32 MC = messages.Length;
//  FUCNotifier.Write("*** Received Messages:");
//  for (Int32 MI = 0; MI < MC; MI++)
//  {
//    String Line = String.Format("Message[{0}/{1}]: <{2}>",
//                                1 + MI, MC, messages[MI]);
//    FUCNotifier.Write(Line);
//  }
//}

//private void UdpBinaryDeviceClientOnVector2DDoubleReceived(Guid datagramid, Double[,] vector2ddouble)
//{
//  FUCNotifier.Write("*** Received Vector2DDouble:");
//  Int32 L0 = vector2ddouble.GetLength(0);
//  Int32 L1 = vector2ddouble.GetLength(1);

//  for (Int32 I0 = 0; I0 < L0; I0++)
//  {
//    String Line = String.Format("Vector[{0}] =", I0);
//    for (Int32 I1 = 0; I1 < L1; I1++)
//    {
//      Line += String.Format(" {0}:{1:000.000}", I1, vector2ddouble[I0, I1]);
//    }
//    FUCNotifier.Write(Line);
//  }
//}
    ////------------------------------------------------------------------------
    ////  Section - Callback - UCNhdUdpTextClient
    ////------------------------------------------------------------------------
    ////     
    //private void UCNhdUdpTextClientOnSendMessage()
    //{
    //  const Int32 MESSAGECOUNT = 6;
    //  String[] Messages = BuildMessages(MESSAGECOUNT);
    //  FUdpTextDeviceClient.PresetMessages(Messages);
    //}
    ////
    ////------------------------------------------------------------------------
    ////  Section - Callback - UCNhdUdpArrayClient
    ////------------------------------------------------------------------------
    ////     
    //private void UCNhdUdpArrayClientOnSendParameter()
    //{
    //  String[] DataNames = new String[CUdpTextArrayBase.DIMENSION_VECTOR2D];
    //  DataNames[CUdpTextArrayBase.INDEX_X] = "Location";
    //  DataNames[CUdpTextArrayBase.INDEX_Y] = "Force";
    //  String[] DataUnits = new String[CUdpTextArrayBase.DIMENSION_VECTOR2D];
    //  DataUnits[CUdpTextArrayBase.INDEX_X] = "mm";
    //  DataUnits[CUdpTextArrayBase.INDEX_Y] = "kgm/s^2";
    //  FUdpTextArrayClient.PresetParameterArrayDouble2D(16, DataNames, DataUnits);
    //}

    //private void UCNhdUdpArrayClientOnSendRequest()
    //{
    //  FUdpTextArrayClient.RequestArrayDouble2D();
    //}   