﻿namespace UCLanTransfer
{
  partial class CUCIpAddress
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.nudIPAddressL = new System.Windows.Forms.NumericUpDown();
      this.nudIPAddressM = new System.Windows.Forms.NumericUpDown();
      this.nudIPAddressH = new System.Windows.Forms.NumericUpDown();
      this.nudIPAddressX = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddressL)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddressM)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddressH)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddressX)).BeginInit();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(160, 13);
      this.lblHeader.TabIndex = 4;
      this.lblHeader.Text = "IPAddress";
      // 
      // nudIPAddressL
      // 
      this.nudIPAddressL.BackColor = System.Drawing.SystemColors.Info;
      this.nudIPAddressL.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIPAddressL.Location = new System.Drawing.Point(120, 13);
      this.nudIPAddressL.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIPAddressL.Name = "nudIPAddressL";
      this.nudIPAddressL.Size = new System.Drawing.Size(40, 20);
      this.nudIPAddressL.TabIndex = 8;
      this.nudIPAddressL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudIPAddressL.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // nudIPAddressM
      // 
      this.nudIPAddressM.BackColor = System.Drawing.SystemColors.Info;
      this.nudIPAddressM.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIPAddressM.Location = new System.Drawing.Point(80, 13);
      this.nudIPAddressM.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIPAddressM.Name = "nudIPAddressM";
      this.nudIPAddressM.Size = new System.Drawing.Size(40, 20);
      this.nudIPAddressM.TabIndex = 7;
      this.nudIPAddressM.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // nudIPAddressH
      // 
      this.nudIPAddressH.BackColor = System.Drawing.SystemColors.Info;
      this.nudIPAddressH.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIPAddressH.Location = new System.Drawing.Point(40, 13);
      this.nudIPAddressH.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIPAddressH.Name = "nudIPAddressH";
      this.nudIPAddressH.Size = new System.Drawing.Size(40, 20);
      this.nudIPAddressH.TabIndex = 6;
      this.nudIPAddressH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // nudIPAddressX
      // 
      this.nudIPAddressX.BackColor = System.Drawing.SystemColors.Info;
      this.nudIPAddressX.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIPAddressX.Location = new System.Drawing.Point(0, 13);
      this.nudIPAddressX.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIPAddressX.Name = "nudIPAddressX";
      this.nudIPAddressX.Size = new System.Drawing.Size(40, 20);
      this.nudIPAddressX.TabIndex = 5;
      this.nudIPAddressX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudIPAddressX.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
      // 
      // CUCIpAddress
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.nudIPAddressL);
      this.Controls.Add(this.nudIPAddressM);
      this.Controls.Add(this.nudIPAddressH);
      this.Controls.Add(this.nudIPAddressX);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCIpAddress";
      this.Size = new System.Drawing.Size(160, 33);
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddressL)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddressM)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddressH)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddressX)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.NumericUpDown nudIPAddressL;
    private System.Windows.Forms.NumericUpDown nudIPAddressM;
    private System.Windows.Forms.NumericUpDown nudIPAddressH;
    private System.Windows.Forms.NumericUpDown nudIPAddressX;
  }
}
