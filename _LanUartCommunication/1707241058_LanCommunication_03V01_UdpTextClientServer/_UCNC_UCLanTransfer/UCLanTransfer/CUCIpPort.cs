﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
//
using Network;
//
namespace UCLanTransfer
{
  //
  //-------------------------------------------------------------------
  //  Segment - Constructor
  //-------------------------------------------------------------------
  //
  public partial class CUCIpPort : UserControl
  {
    public CUCIpPort()
    {
      InitializeComponent();
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public UInt16 GetIpPort()
    {
      return (UInt16)nudIpPort.Value;
    }

    public void SetIpPort(UInt16 ipport)
    {
      nudIpPort.Value = (Decimal)ipport;
    }

    public UInt16 IpPort
    {
      get { return GetIpPort(); }
      set { SetIpPort(value); }
    }

    public void SetIpPort(String ipport)
    {
      IPEndPoint IPE;
      CNetwork.TextToIPEndPoint(ipport, out IPE);
      nudIpPort.Value = (Decimal)IPE.Port;
    }

  }
}
