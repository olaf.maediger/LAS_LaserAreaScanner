﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
//
using Network;
//
namespace UCLanTransfer
{
  public partial class CUCIpEndPoint : UserControl
  {
    //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public CUCIpEndPoint()
    {
      InitializeComponent();
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public Byte[] GetIPAddress()
    {
      Byte[] IPA = new Byte[4];
      IPA[3] = (Byte)nudIpAddress0.Value;
      IPA[2] = (Byte)nudIpAddress1.Value;
      IPA[1] = (Byte)nudIpAddress2.Value;
      IPA[0] = (Byte)nudIpAddress3.Value;
      return IPA;
    }

    public UInt16 GetIPPort()
    {
      return (UInt16)nudIpPort.Value;
    }

    public String GetIPEndPoint()
    {
      return String.Format("{0}.{1}.{2}.{3}:{4}",
                                            (Byte)nudIpAddress3.Value,
                                            (Byte)nudIpAddress2.Value,
                                            (Byte)nudIpAddress1.Value,
                                            (Byte)nudIpAddress0.Value,
                                            (UInt16)nudIpPort.Value);
    }

    public void SetIpEndPoint(String sipendpoint)
    {
      IPEndPoint IPE;
      CNetwork.TextToIPEndPoint(sipendpoint, out IPE);
      Byte[] AB = IPE.Address.GetAddressBytes(); 
      nudIpAddress0.Value = (Decimal)AB[3];
      nudIpAddress1.Value = (Decimal)AB[2];
      nudIpAddress2.Value = (Decimal)AB[1];
      nudIpAddress3.Value = (Decimal)AB[0];
      nudIpPort.Value = (Decimal)IPE.Port;
    }

    public void SetIPEndPoint(IPAddress ipaddress, ushort ipport)
    {
      Byte[] AB = ipaddress.GetAddressBytes();
      nudIpAddress0.Value = (Decimal)AB[3];
      nudIpAddress1.Value = (Decimal)AB[2];
      nudIpAddress2.Value = (Decimal)AB[1];
      nudIpAddress3.Value = (Decimal)AB[0];
      nudIpPort.Value = (Decimal)ipport;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //


  }
}
