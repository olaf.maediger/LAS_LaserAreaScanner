﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
//
//using UdpTransfer;
//
namespace UCLanTransfer
{
  public partial class CUCIpAddress : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCIpAddress()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public Byte IPA3X
    {
      get { return (Byte)nudIPAddressX.Value; }
      set { nudIPAddressX.Value = (Decimal)value; }
    }
    public Byte IPA2H
    {
      get { return (Byte)nudIPAddressH.Value; }
      set { nudIPAddressH.Value = (Decimal)value; }
    }
    public Byte IPA1M
    {
      get { return (Byte)nudIPAddressM.Value; }
      set { nudIPAddressM.Value = (Decimal)value; }
    }
    public Byte IPA0L
    {
      get { return (Byte)nudIPAddressL.Value; }
      set { nudIPAddressL.Value = (Decimal)value; }
    }

    public Byte[] GetIpAddress()
    {
      Byte[] Result = new Byte[4];
      Result[3] = (Byte)nudIPAddressL.Value;
      Result[2] = (Byte)nudIPAddressM.Value;
      Result[1] = (Byte)nudIPAddressH.Value;
      Result[0] = (Byte)nudIPAddressX.Value;
      return Result;
    }

    public void SetIpAddress(Byte[] ipaddress)
    {
      if (ipaddress is Byte[])
      {
        if (4 == ipaddress.Length)
        {
          nudIPAddressL.Value = ipaddress[3];
          nudIPAddressM.Value = ipaddress[2];
          nudIPAddressH.Value = ipaddress[1];
          nudIPAddressX.Value = ipaddress[0];
        }
      }
    }

    public void SetIpAddress(IPAddress ipaddress)
    {
      Byte[] AB = ipaddress.GetAddressBytes();
      nudIPAddressL.Value = AB[3];
      nudIPAddressM.Value = AB[2];
      nudIPAddressH.Value = AB[1];
      nudIPAddressX.Value = AB[0];
    }


  }
}
