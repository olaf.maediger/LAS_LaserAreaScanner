﻿namespace UCLanTransfer
{
  partial class CUCIpEndPoint
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.nudIpAddress0 = new System.Windows.Forms.NumericUpDown();
      this.nudIpAddress1 = new System.Windows.Forms.NumericUpDown();
      this.nudIpAddress2 = new System.Windows.Forms.NumericUpDown();
      this.nudIpAddress3 = new System.Windows.Forms.NumericUpDown();
      this.nudIpPort = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.nudIpAddress0)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIpAddress1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIpAddress2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIpAddress3)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIpPort)).BeginInit();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(212, 13);
      this.lblHeader.TabIndex = 5;
      this.lblHeader.Text = "IPEndpoint";
      // 
      // nudIpAddress0
      // 
      this.nudIpAddress0.BackColor = System.Drawing.SystemColors.Info;
      this.nudIpAddress0.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIpAddress0.Location = new System.Drawing.Point(120, 13);
      this.nudIpAddress0.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIpAddress0.Name = "nudIpAddress0";
      this.nudIpAddress0.Size = new System.Drawing.Size(40, 20);
      this.nudIpAddress0.TabIndex = 12;
      this.nudIpAddress0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudIpAddress0.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // nudIpAddress1
      // 
      this.nudIpAddress1.BackColor = System.Drawing.SystemColors.Info;
      this.nudIpAddress1.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIpAddress1.Location = new System.Drawing.Point(80, 13);
      this.nudIpAddress1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIpAddress1.Name = "nudIpAddress1";
      this.nudIpAddress1.Size = new System.Drawing.Size(40, 20);
      this.nudIpAddress1.TabIndex = 11;
      this.nudIpAddress1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // nudIpAddress2
      // 
      this.nudIpAddress2.BackColor = System.Drawing.SystemColors.Info;
      this.nudIpAddress2.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIpAddress2.Location = new System.Drawing.Point(40, 13);
      this.nudIpAddress2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIpAddress2.Name = "nudIpAddress2";
      this.nudIpAddress2.Size = new System.Drawing.Size(40, 20);
      this.nudIpAddress2.TabIndex = 10;
      this.nudIpAddress2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // nudIpAddress3
      // 
      this.nudIpAddress3.BackColor = System.Drawing.SystemColors.Info;
      this.nudIpAddress3.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIpAddress3.Location = new System.Drawing.Point(0, 13);
      this.nudIpAddress3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIpAddress3.Name = "nudIpAddress3";
      this.nudIpAddress3.Size = new System.Drawing.Size(40, 20);
      this.nudIpAddress3.TabIndex = 9;
      this.nudIpAddress3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudIpAddress3.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
      // 
      // nudIpPort
      // 
      this.nudIpPort.BackColor = System.Drawing.SystemColors.Info;
      this.nudIpPort.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIpPort.Location = new System.Drawing.Point(160, 13);
      this.nudIpPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
      this.nudIpPort.Name = "nudIpPort";
      this.nudIpPort.Size = new System.Drawing.Size(52, 20);
      this.nudIpPort.TabIndex = 13;
      this.nudIpPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudIpPort.Value = new decimal(new int[] {
            12345,
            0,
            0,
            0});
      // 
      // CUCIpEndPoint
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.nudIpPort);
      this.Controls.Add(this.nudIpAddress0);
      this.Controls.Add(this.nudIpAddress1);
      this.Controls.Add(this.nudIpAddress2);
      this.Controls.Add(this.nudIpAddress3);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCIpEndPoint";
      this.Size = new System.Drawing.Size(212, 33);
      ((System.ComponentModel.ISupportInitialize)(this.nudIpAddress0)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIpAddress1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIpAddress2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIpAddress3)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIpPort)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.NumericUpDown nudIpAddress0;
    private System.Windows.Forms.NumericUpDown nudIpAddress1;
    private System.Windows.Forms.NumericUpDown nudIpAddress2;
    private System.Windows.Forms.NumericUpDown nudIpAddress3;
    private System.Windows.Forms.NumericUpDown nudIpPort;
  }
}
