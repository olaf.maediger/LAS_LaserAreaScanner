﻿namespace UCLanTransfer
{
  partial class CUCIpPort
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.nudIpPort = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.nudIpPort)).BeginInit();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(52, 13);
      this.lblHeader.TabIndex = 5;
      this.lblHeader.Text = "IPPort";
      // 
      // nudIpPort
      // 
      this.nudIpPort.BackColor = System.Drawing.SystemColors.Info;
      this.nudIpPort.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIpPort.Location = new System.Drawing.Point(0, 13);
      this.nudIpPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
      this.nudIpPort.Name = "nudIpPort";
      this.nudIpPort.Size = new System.Drawing.Size(52, 20);
      this.nudIpPort.TabIndex = 6;
      this.nudIpPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudIpPort.Value = new decimal(new int[] {
            12345,
            0,
            0,
            0});
      // 
      // CUCIpPort
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.nudIpPort);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCIpPort";
      this.Size = new System.Drawing.Size(52, 33);
      ((System.ComponentModel.ISupportInitialize)(this.nudIpPort)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.NumericUpDown nudIpPort;
  }
}
