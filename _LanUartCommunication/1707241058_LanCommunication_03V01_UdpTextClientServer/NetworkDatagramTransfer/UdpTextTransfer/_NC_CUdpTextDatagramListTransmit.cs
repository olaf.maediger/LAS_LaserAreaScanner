﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Network;
//
namespace UdpTextTransfer
{
  public class CUdpTextDatagramListTransmit : CUdpTextDatagramListBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const Int32 SIZE_BLOCK_CONTENT = 12; // maximal 3K for Udp
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpTextDatagramListTransmit()
    {
      FHeader.DatagramID = Guid.NewGuid();
      FContent = null;
    }

    public CUdpTextDatagramListTransmit(Guid datagramid)
    {
      FHeader.DatagramID = datagramid;
      FContent = null;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - Public Management
    //--------------------------------------------------------------------------
    //  
    public Boolean BuildDatagramList(RUdpTextDatagramHeader header,
                                     String datagramtype, String datagramkey,
                                     CTagTextList content)
    {
      try
      {
        FHeader.ProtocolType = CNetwork.PROTOCOLTYPE_UDPTEXT;
        FHeader.TimeStamp = DateTime.Now;
        FHeader.SourceIpAddress = header.SourceIpAddress;
        FHeader.SourceID = header.SourceID;
        FHeader.SourceType = header.SourceType;
        FHeader.SourceName = header.SourceName;
        FHeader.TargetIpAddress = header.TargetIpAddress;
        FHeader.TargetID = header.TargetID;
        FHeader.TargetType = header.TargetType;
        FHeader.TargetName = header.TargetName;
        FHeader.DatagramID = Guid.NewGuid();
        FHeader.BlockIndex = 1;
        FHeader.BlockCount = 1;
        FHeader.DatagramType = datagramtype;
        FHeader.DatagramKey = datagramkey;
        FContent = content;
        // Header -> ByteList
       



        // Content(CTagBinaryList) -> ByteList -> ByteVector
        CByteList ByteList = content.GetByteList();
        Byte[] ByteVector = ByteList.GetByteVector();
        Int32 SizeByteVector = ByteVector.Length;
        // Calculate BlockCount:
        FHeader.BlockCount = (Int32)(SizeByteVector / SIZE_BLOCK_CONTENT);
        Int32 BlockModulo = SizeByteVector % SIZE_BLOCK_CONTENT;
        if (0 < BlockModulo)
        {
          FHeader.BlockCount++;
        }
        // Build all Datagrams:
        Int32 ByteIndex = 0;
        for (FHeader.BlockIndex = 1; FHeader.BlockIndex <= FHeader.BlockCount; FHeader.BlockIndex++)
        {
          Int32 ByteCount = SIZE_BLOCK_CONTENT;
          if (FHeader.BlockIndex == FHeader.BlockCount)
          {
            if (0 < BlockModulo)
            {
              ByteCount = BlockModulo;
            }
          }
          Byte[] DatagramContent = BuildTagByteSequence(ByteVector, ref ByteIndex, ByteCount);
          CUdpTextDatagramTransmit Datagram = new CUdpTextDatagramTransmit(FHeader, 
                                                                           DatagramContent);
          this.Add(Datagram);
        }
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }



    public CUdpTextDatagramTransmit GetUdpTextDatagram(Int32 blockindex)
    {
      return (CUdpTextDatagramTransmit)this[blockindex];
    }



 
 

  }
}
