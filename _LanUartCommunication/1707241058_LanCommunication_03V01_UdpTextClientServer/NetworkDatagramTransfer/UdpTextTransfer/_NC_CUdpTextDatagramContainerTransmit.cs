﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Network;
//
namespace UdpTextTransfer
{ //
  //  CUdpTextDatagramContainerTransmit - CUdpTextDatagramListTransmit - CUdpTextDatagramTransmit
  //
  public class CUdpTextDatagramContainerTransmit : List<CUdpTextDatagramListTransmit>
  { //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    protected RUdpTextDatagramHeader FHeader;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpTextDatagramContainerTransmit()
    {
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public RUdpTextDatagramHeader GetHeader()
    {
      return FHeader;
    }
    public void SetHeader(RUdpTextDatagramHeader value)
    {
      FHeader = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private CUdpTextDatagramListTransmit FindUdpTextDatagramListTransmit(Guid datagramid)
    {
      foreach (CUdpTextDatagramListTransmit UTDLT in this)
      {
        //!!!!!!!!!!!!!!!!!!!!!if (datagramid == UTDLT.GetDatagramID())
        {
          return UTDLT;
        }
      }
      return null;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private CTagTextList BuildContent(String[] messages)
    {
      CTagTextList TagTextList = new CTagTextList();
      foreach (String S in messages)
      {
        CTTScalarString TSS = new CTTScalarString(S);
        TagTextList.Add(TSS);
      }
      return null;//!!!!!!!!!!!!!!!!!!!!! TagBinaryList;
    }

    private CTagTextList BuildContent(Double[,] values)
    {
      CTagTextList TagTextList = new CTagTextList();
      CTTMatrixDouble TMD = new CTTMatrixDouble(values);
      TagTextList.Add(TMD);
      return TagTextList;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public Function
    //--------------------------------------------------------------------------
    //
    public Boolean PresetContent(CTagTextList tagtextlist)
    {
      try
      {
        CUdpTextDatagramListTransmit UTDLT = new CUdpTextDatagramListTransmit();
        CTagTextList TTLContent = tagtextlist;
        UTDLT.BuildDatagramList(FHeader, "TagTextList", "", TTLContent);
        this.Add(UTDLT);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean PresetMessages(String[] messages)
    {
      try
      {
        CUdpTextDatagramListTransmit UTDLT = new CUdpTextDatagramListTransmit();
        CTagTextList TTLContent = BuildContent(messages);
        UTDLT.BuildDatagramList(FHeader, "Messages", "String[]", TTLContent);
        this.Add(UTDLT);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean PresetVector2DDouble(Double[,] values)
    {
      try
      {
        CUdpTextDatagramListTransmit UTDLT = new CUdpTextDatagramListTransmit();
        CTagTextList TTLContent = BuildContent(values);
        UTDLT.BuildDatagramList(FHeader, "Vector2D", "Double[,]", TTLContent);
        this.Add(UTDLT);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
