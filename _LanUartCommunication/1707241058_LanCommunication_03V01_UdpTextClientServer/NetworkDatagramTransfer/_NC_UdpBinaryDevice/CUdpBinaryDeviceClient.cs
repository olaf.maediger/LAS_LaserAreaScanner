﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using UCNotifier;
using Task;
using Network;
using UdpBinaryTransfer;
//
namespace UdpBinaryDevice
{
  public class CUdpBinaryDeviceClient : CUdpBinaryDeviceBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private DOnContentReceived FOnContentReceived;
    private DOnMessagesReceived FOnMessagesReceived;
    private DOnVector2DDoubleReceived FOnVector2DDoubleReceived;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpBinaryDeviceClient()
            : base()
    {
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetOnContentReceived(DOnContentReceived value)
    {
      FOnContentReceived = value;
    }

    public void SetOnMessagesReceived(DOnMessagesReceived value)
    {
      FOnMessagesReceived = value;
    }

    public void SetOnVector2DDoubleReceived(DOnVector2DDoubleReceived value)
    {
      FOnVector2DDoubleReceived = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
 
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public override Boolean Open()
    {
      FUdpBinaryTransmitter.Open(FTargetIpAddress,
                                 FTargetIpPort);
      FUdpBinaryReceiver.Open(FSourceIpAddress,
                              FSourceIpPort);
      return true;
    }

    public override Boolean Close()
    {
      FUdpBinaryTransmitter.Close();
      FUdpBinaryReceiver.Close();
      return true;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public Function
    //--------------------------------------------------------------------------
    //
    public Boolean PresetByteVector(Byte[] bytevector, Int32 size)
    {
      try
      {
        Guid DatagramID;
        Boolean AllReceived = FUdpBinaryDatagramContainerReceive.PresetByteVector(bytevector, size, out DatagramID);
        if (AllReceived)
        {
          if (FOnContentReceived is DOnContentReceived)
          {
            CTagBinaryList Content = FUdpBinaryDatagramContainerReceive.GetContent(DatagramID);
            FOnContentReceived(DatagramID, Content);
          }
          if (FOnMessagesReceived is DOnMessagesReceived)
          {
            String[] Messages = FUdpBinaryDatagramContainerReceive.GetMessages(DatagramID);
            FOnMessagesReceived(DatagramID, Messages);
          }
          if (FOnVector2DDoubleReceived is DOnVector2DDoubleReceived)
          {
            Double[,] Vector2DDouble = FUdpBinaryDatagramContainerReceive.GetVector2DDouble(DatagramID);
            FOnVector2DDoubleReceived(DatagramID, Vector2DDouble);
          }
          FUdpBinaryDatagramContainerReceive.RemoveUdpBinaryDatagramListReceive(DatagramID);
        }
        return true; 
      }
      catch (Exception)
      {
        return false;
      }
    }



  }
}







    //public Boolean ConvertToDatagram(Byte[] bytedatagram, out CUdpBinaryDatagram udpbinarydatagram)
    //{
    //  udpbinarydatagram = new CUdpBinaryDatagram();
    //  try
    //  {
    //    Int32 SourceIndex = 0;
    //    // MagicWord
    //    CTBMagicWord TMW = new CTBMagicWord(bytedatagram, ref SourceIndex);
    //    String MWR = new String(TMW.GetVectorChar());
    //    String MWC = new String(CNetwork.PROTOCOLTYPE_UDPBINARY);
    //    if (MWR != MWC)
    //    {
    //      return false;
    //    }
    //    // TimeStamp
    //    Byte TagType = bytedatagram[SourceIndex];
    //    SourceIndex++;
    //    if (CTBBinary.TYPE_SCALAR_DATETIME != TagType)
    //    {
    //      return false;
    //    }
    //    int Count = sizeof(UInt64);
    //    Byte[] BS = BuildByteSequence(bytedatagram, SourceIndex, Count);
    //    SourceIndex += Count;
    //    CTBScalarDateTime TSDT = new CTBScalarDateTime(BS);
    //    DateTime DTR = TSDT.GetDateTime();
    //    udpbinarydatagram.SetTimeStamp(DTR);
    //    //
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

    //public Boolean ConvertToMessages(Byte[] bytedatagram, out String[] messages)
    //{
    //  messages = null;
    //  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CUdpBinaryDatagram UdpBinaryDatagram = new CUdpBinaryDatagram(bytedatagram);
    //  //if (ConvertToDatagram(bytedatagram, out UdpBinaryDatagram))
    //  //{
    //  //  return true;
    //  //}
    //  return false;
    //}
