﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using UCNotifier;
using Task;
using Network;
using UdpBinaryTransfer;
//
namespace UdpBinaryDevice
{
  public class CUdpBinaryDeviceServer : CUdpBinaryDeviceBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpBinaryDeviceServer()
            : base()
    {
      FUdpBinaryDatagramContainerTransmit = new CUdpBinaryDatagramContainerTransmit();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public override Boolean Open()
    {
      FUdpBinaryTransmitter.Open(FTargetIpAddress,
                                 FTargetIpPort);
      FUdpBinaryReceiver.Open(FSourceIpAddress,
                              FSourceIpPort);
      RUdpBinaryDatagramHeader Header = new RUdpBinaryDatagramHeader();
      Header.ProtocolType = CNetwork.PROTOCOLTYPE_UDPBINARY;
      Header.TimeStamp = DateTime.Now;
      Header.SourceIpAddress = FSourceIpAddress.GetAddressBytes();
      Header.SourceID = FSourceID;
      Header.SourceType = FSourceType;
      Header.SourceName = FSourceName;
      Header.TargetIpAddress = FTargetIpAddress.GetAddressBytes();
      Header.TargetID = FTargetID;
      Header.TargetType = FTargetType;
      Header.TargetName = FTargetName;
      Header.DatagramID = Guid.Empty;
      Header.BlockIndex = 1;
      Header.BlockCount = 1;
      Header.DatagramType = "";
      Header.DatagramKey = "";
      FUdpBinaryDatagramContainerTransmit.SetHeader(Header);
      return true;
    }

    public override Boolean Close()
    {
      FUdpBinaryTransmitter.Close();
      FUdpBinaryReceiver.Close();
      return true;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public Function
    //--------------------------------------------------------------------------
    //
    public Boolean PresetContent(CTagBinaryList content)
    {
      try
      {
        if (FUdpBinaryDatagramContainerTransmit.PresetContent(content))
        {
          FUdpBinaryTransmitter.PresetUdpBinaryDatagramContainerTransmit(FUdpBinaryDatagramContainerTransmit);
          FUdpBinaryDatagramContainerTransmit.Clear();
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean PresetMessages(String[] messages)
    {
      try
      {
        if (FUdpBinaryDatagramContainerTransmit.PresetMessages(messages))
        {
          FUdpBinaryTransmitter.PresetUdpBinaryDatagramContainerTransmit(FUdpBinaryDatagramContainerTransmit);
          FUdpBinaryDatagramContainerTransmit.Clear();
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean PresetVector2DDouble(Double[,] values)
    {
      try
      {
        if (FUdpBinaryDatagramContainerTransmit.PresetVector2DDouble(values))
        {
          FUdpBinaryTransmitter.PresetUdpBinaryDatagramContainerTransmit(FUdpBinaryDatagramContainerTransmit);
          FUdpBinaryDatagramContainerTransmit.Clear();
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
