﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using UCNotifier;
using Network;
using UdpBinaryTransfer;
//
namespace UdpBinaryDevice
{
  public delegate void DOnContentReceived(Guid datagramid, CTagBinaryList content);
  public delegate void DOnMessagesReceived(Guid datagramid, String[] messages);
  public delegate void DOnVector2DDoubleReceived(Guid datagramid, Double[,] vector2ddouble);
  //
  public abstract class CUdpBinaryDeviceBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const String TOKEN_MESSAGES = "Messages";
    public const String FORMAT_MESSAGE = "Message[{0}][{1}]";
    //
    public const String FORMAT_MESSAGECOUNT = "MessageCount[{0}]";
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    protected IPAddress FSourceIpAddress, FTargetIpAddress;
    protected UInt16 FSourceIpPort, FTargetIpPort;
    protected Guid FSourceID, FTargetID;
    protected String FSourceType, FTargetType;
    protected String FSourceName, FTargetName;
    protected CUdpBinaryTransmitter FUdpBinaryTransmitter;
    protected CUdpBinaryReceiver FUdpBinaryReceiver;
    protected CUdpBinaryDatagramContainerReceive FUdpBinaryDatagramContainerReceive;
    protected CUdpBinaryDatagramContainerTransmit FUdpBinaryDatagramContainerTransmit;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpBinaryDeviceBase()
    {
      FUdpBinaryTransmitter = new CUdpBinaryTransmitter();
      FUdpBinaryReceiver = new CUdpBinaryReceiver();
      FUdpBinaryDatagramContainerReceive = new CUdpBinaryDatagramContainerReceive();
      FUdpBinaryDatagramContainerTransmit = new CUdpBinaryDatagramContainerTransmit();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FUdpBinaryTransmitter.SetNotifier(value);
      FUdpBinaryReceiver.SetNotifier(value);
    }

    public void SetOnDatagramTransmitted(DOnDatagramTransmitted value)
    {
      FUdpBinaryTransmitter.SetOnDatagramTransmitted(value);
    }
    
    public void SetOnDatagramReceived(DOnDatagramReceived value)
    {
      FUdpBinaryReceiver.SetOnDatagramReceived(value);
    }

    public void SetSourceIpAddress(IPAddress value)
    {
      FSourceIpAddress = value;
    }
    public void SetTargetIpAddress(IPAddress value)
    {
      FTargetIpAddress = value;
    }

    public void SetSourceIpPort(UInt16 value)
    {
      FSourceIpPort = value;
    }
    public void SetTargetIpPort(UInt16 value)
    {
      FTargetIpPort = value;
    }

    public void SetSourceType(String value)
    {
      FSourceType = value;
    }
    public void SetTargetType(String value)
    {
      FTargetType = value;
    }

    public void SetSourceName(String value)
    {
      FSourceName = value;
    }
    public void SetTargetName(String value)
    {
      FTargetName = value;
    }

    public void SetSourceID(Guid value)
    {
      FSourceID = value;
    }
    public void SetTargetID(Guid value)
    {
      FTargetID = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public abstract Boolean Open();
    public abstract Boolean Close();
    //
    //--------------------------------------------------------------------------
    //  Segment - 
    //--------------------------------------------------------------------------
    //

  }
}

