﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using Network;
//
namespace UdpBinaryTransfer
{
  public class CUdpBinaryDatagramTransmit : CUdpBinaryDatagramBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private CTagBinaryList FContent;  // Taglist of elementary Datatypes specific to Type/Key
    private Byte[] FByteVector;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpBinaryDatagramTransmit(DateTime timestamp,
                                      Byte[] sourceipaddress, Guid sourceid, String sourcetype, String sourcename,
                                      Byte[] targetipaddress, Guid targetid, String targettype, String targetname,
                                      Guid datagramid, Int32 blockindex, Int32 blockcount,
                                      String datagramtype, String datagramkey,
                                      Byte[] datagramcontent)
    {
      FByteVector = BuildByteVector(timestamp,
                                    sourceipaddress, sourceid, sourcetype, sourcename,
                                    targetipaddress, targetid, targettype, targetname,
                                    datagramid, blockindex, blockcount,
                                    datagramtype, datagramkey, 
                                    datagramcontent);
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public CTagBinaryList GetContent()
    {
      return FContent;
    }
    public void SetContent(CTagBinaryList value)
    {
      FContent = value;
    }

    public Byte[] GetByteVector()
    {
      return FByteVector;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private Byte[] BuildByteVector(DateTime timestamp,
                                   Byte[] sourceipaddress, Guid sourceid, String sourcetype, String sourcename,
                                   Byte[] targetipaddress, Guid targetid, String targettype, String targetname,
                                   Guid datagramid, Int32 blockindex, Int32 blockcount,
                                   String datagramtype, String datagramkey,
                                   Byte[] datagramcontent)
    {
      CByteList ByteList = new CByteList();
      // ProtocolType [6xByte]
      CTBMagicWord TMW = new CTBMagicWord(CNetwork.PROTOCOLTYPE_UDPBINARY);
      ByteList.Add(TMW.Type);
      ByteList.AddRange(TMW.Values);
      // TimeStamp [DateTime]
      CTBScalarDateTime TDT = new CTBScalarDateTime(timestamp);
      ByteList.Add(TDT.Type);
      ByteList.AddRange(TDT.Values);
      // SourceIpAddress [Byte[]]
      CTBVectorByte TVB = new CTBVectorByte(sourceipaddress);
      ByteList.Add(TVB.Type);
      ByteList.AddRange(TVB.Values);
      // SourceID
      CTBScalarGuid TSG = new CTBScalarGuid(sourceid);
      ByteList.Add(TSG.Type);
      ByteList.AddRange(TSG.Values);
      // SourceType [String]
      CTBScalarString TSS = new CTBScalarString(sourcetype);
      ByteList.Add(TSS.Type);
      ByteList.AddRange(TSS.Values);
      // SourceName [String]
      TSS = new CTBScalarString(sourcename);
      ByteList.Add(TSS.Type);
      ByteList.AddRange(TSS.Values);
      // TargetIpAddress
      TVB = new CTBVectorByte(targetipaddress);
      ByteList.Add(TVB.Type);
      ByteList.AddRange(TVB.Values);
      // TargetID [Guid]
      TSG = new CTBScalarGuid(targetid);
      ByteList.Add(TSG.Type);
      ByteList.AddRange(TSG.Values);
      // TargetType [String]
      TSS = new CTBScalarString(targettype);
      ByteList.Add(TSS.Type);
      ByteList.AddRange(TSS.Values);
      // TargetName [String]
      TSS = new CTBScalarString(targetname);
      ByteList.Add(TSS.Type);
      ByteList.AddRange(TSS.Values);
      // DatagramID [Guid]
      TSG = new CTBScalarGuid(datagramid);
      ByteList.Add(TSG.Type);
      ByteList.AddRange(TSG.Values);
      // BlockIndex
      CTBScalarInt32 TSI = new CTBScalarInt32(blockindex);
      ByteList.Add(TSI.Type);
      ByteList.AddRange(TSI.Values);
      // BlockCount [Int32]
      TSI = new CTBScalarInt32(blockcount);
      ByteList.Add(TSI.Type);
      ByteList.AddRange(TSI.Values);
      // DatagramType [String]
      TSS = new CTBScalarString(datagramtype);
      ByteList.Add(TSS.Type);
      ByteList.AddRange(TSS.Values);
      // DatagramKey [String]
      TSS = new CTBScalarString(datagramkey);
      ByteList.Add(TSS.Type);
      ByteList.AddRange(TSS.Values);
      // DatagramContent {Byte[]}
      Int32 CountBytes = datagramcontent.Length;
      for (Int32 BI = 0; BI < CountBytes; BI++)
      {
        ByteList.Add(datagramcontent[BI]);
      }
      // List -> []
      Int32 BufferCount = ByteList.Count;
      Byte[] Result = new Byte[BufferCount];
      for (Int32 BI = 0; BI < BufferCount; BI++)
      {
        Result[BI] = ByteList[BI];
      }
      return Result;
    }

  }
}

