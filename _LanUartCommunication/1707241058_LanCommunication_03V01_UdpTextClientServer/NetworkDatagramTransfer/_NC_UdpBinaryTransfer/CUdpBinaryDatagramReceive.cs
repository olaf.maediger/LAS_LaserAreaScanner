﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UdpBinaryTransfer
{
  public class CUdpBinaryDatagramReceive : CUdpBinaryDatagramBase
  {
    private Byte[] FContent;



    public CUdpBinaryDatagramReceive(Char[] protocoltype, DateTime timestamp,
                                     Byte[] sourceipaddress, Guid sourceid,
                                     String sourcetype, String sourcename,
                                     Byte[] targetipaddress, Guid targetid,
                                     String targettype, String targetname,
                                     Guid datagramid,
                                     Int32 blockindex, Int32 blockcount,
                                     String datagramtype, String datagramkey,
                                     Byte[] content)
    {
      FHeader.ProtocolType = protocoltype;
      FHeader.TimeStamp = timestamp;
      FHeader.SourceType = sourcetype;
      FHeader.SourceID = sourceid;
      FHeader.SourceType = sourcetype;
      FHeader.SourceName = sourcename;
      FHeader.TargetType = sourcetype;
      FHeader.TargetID = sourceid;
      FHeader.TargetType = sourcetype;
      FHeader.TargetName = sourcename;
      FHeader.DatagramID = datagramid;
      FHeader.BlockIndex = blockindex;
      FHeader.BlockCount = blockcount;
      FHeader.DatagramType = datagramtype;
      FHeader.DatagramKey = datagramkey;
      FContent = content;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public Byte[] GetContent()
    {
      return FContent;
    }
    public void SetContent(Byte[] value)
    {
      FContent = value;
    }
  }
}
