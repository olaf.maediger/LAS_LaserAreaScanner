﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Network;
//
namespace UdpBinaryTransfer
{ //
  //  CUdpBinaryDatagramContainerTransmit - CUdpBinaryDatagramListTransmit - CUdpBinaryDatagramTransmit
  //
  public class CUdpBinaryDatagramContainerTransmit : List<CUdpBinaryDatagramListTransmit>
  { //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    protected RUdpBinaryDatagramHeader FHeader;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpBinaryDatagramContainerTransmit()
    {
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public RUdpBinaryDatagramHeader GetHeader()
    {
      return FHeader;
    }
    public void SetHeader(RUdpBinaryDatagramHeader value)
    {
      FHeader = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private CUdpBinaryDatagramListTransmit FindUdpBinaryDatagramListTransmit(Guid datagramid)
    {
      foreach (CUdpBinaryDatagramListTransmit UBDLT in this)
      {
        if (datagramid == UBDLT.GetDatagramID())
        {
          return UBDLT;
        }
      }
      return null;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private CTagBinaryList BuildContent(String[] messages)
    {
      CTagBinaryList TagBinaryList = new CTagBinaryList();
      foreach (String S in messages)
      {
        CTBScalarString TSS = new CTBScalarString(S);
        TagBinaryList.Add(TSS);
      }
      return TagBinaryList;
    }

    private CTagBinaryList BuildContent(Double[,] values)
    {
      CTagBinaryList TagBinaryList = new CTagBinaryList();
      CTBMatrixDouble TSD = new CTBMatrixDouble(values);
      TagBinaryList.Add(TSD);  
      return TagBinaryList;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public Function
    //--------------------------------------------------------------------------
    //
    public Boolean PresetContent(CTagBinaryList tagbinarylist)
    {
      try
      {
        CUdpBinaryDatagramListTransmit UBDLT = new CUdpBinaryDatagramListTransmit();
        CTagBinaryList TBLContent = tagbinarylist;
        UBDLT.BuildDatagramList(FHeader, "TagBinaryList", "", TBLContent);
        this.Add(UBDLT);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean PresetMessages(String[] messages)
    {
      try
      {
        CUdpBinaryDatagramListTransmit UBDLT = new CUdpBinaryDatagramListTransmit();
        CTagBinaryList TBLContent = BuildContent(messages);
        UBDLT.BuildDatagramList(FHeader, "Messages", "String[]", TBLContent);
        this.Add(UBDLT);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean PresetVector2DDouble(Double[,] values)
    {
      try
      {
        CUdpBinaryDatagramListTransmit UBDLT = new CUdpBinaryDatagramListTransmit();
        CTagBinaryList TBLContent = BuildContent(values);
        UBDLT.BuildDatagramList(FHeader, "Vector2D", "Double[,]", TBLContent);
        this.Add(UBDLT);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
