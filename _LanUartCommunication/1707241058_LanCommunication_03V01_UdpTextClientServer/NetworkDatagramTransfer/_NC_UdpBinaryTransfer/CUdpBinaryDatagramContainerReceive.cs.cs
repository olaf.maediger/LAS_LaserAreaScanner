﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Network;
//
namespace UdpBinaryTransfer
{
  //
  //  CUdpBinaryDatagramContainerReceive - CUdpBinaryDatagramListReceive - CUdpBinaryDatagramReceive
  //

  public class CUdpBinaryDatagramContainerReceive : List<CUdpBinaryDatagramListReceive>
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    // NC protected RUdpBinaryDatagramHeader FHeader;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpBinaryDatagramContainerReceive()
    {
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    // 

    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private CUdpBinaryDatagramListReceive FindUdpBinaryDatagramListReceive(Guid datagramid)
    {
      foreach (CUdpBinaryDatagramListReceive UBDLR in this)
      {
        if (datagramid == UBDLR.GetDatagramID())
        {
          return UBDLR;
        }
      }
      return null;
    }

    private Boolean AnalyseDatagram(Byte[] bytevector, Int32 size,
                                    out Char[] protocoltype, out DateTime timestamp,
                                    out Byte[] sourceipaddress, out Guid sourceid,
                                    out String sourcetype, out String sourcename,
                                    out Byte[] targetipaddress, out Guid targetid,
                                    out String targettype, out String targetname,
                                    out Guid datagramid,
                                    out Int32 blockindex, out Int32 blockcount,
                                    out String datagramtype, out String datagramkey,
                                    out Byte[] content)
    {
      protocoltype = null;
      timestamp = DateTime.Now;
      sourceipaddress = null;
      sourceid = Guid.Empty;
      sourcetype = "";
      sourcename = "";
      targetipaddress = null;
      targetid = Guid.Empty;
      targettype = "";
      targetname = "";
      datagramid = Guid.Empty;
      blockindex = 0;
      blockcount = 0;
      datagramtype = "";
      datagramkey = "";
      content = null;
      try
      {
        Int32 SourceIndex = 0;
        Int32 SourceCount = 0;
        Byte[] ByteSequence;
        Byte TagType;
        Int32 CountValues;
        Int32 LengthByteVector = bytevector.Length;
        //
        // MagicWord
        CTBMagicWord TMW = new CTBMagicWord(bytevector, ref SourceIndex);
        String MWR = new String(TMW.GetVectorChar());
        String MWC = new String(CNetwork.PROTOCOLTYPE_UDPBINARY);
        if (MWR != MWC) return false;
        protocoltype = TMW.GetVectorChar();
        //
        // TimeStamp
        TagType = bytevector[SourceIndex];
        SourceIndex++;
        if (CTagBase.TYPE_SCALAR_DATETIME != TagType) return false;
        SourceCount = sizeof(UInt64);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CTBScalarDateTime TSDT = new CTBScalarDateTime(ByteSequence);
        timestamp = TSDT.GetDateTime();
        //
        // SourceIpAddress [Byte[]]
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_VECTOR_BYTE != TagType) return false;
        SourceIndex++;
        SourceCount = sizeof(UInt32);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CountValues = CUdpBinaryDatagramListBase.BuildInt32(ByteSequence);
        SourceCount = CountValues * sizeof(Byte);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        sourceipaddress = ByteSequence;
        //
        // SourceID
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_GUID != TagType) return false;
        SourceIndex++;
        SourceCount = 16;
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CTBScalarGuid TSG = new CTBScalarGuid(ByteSequence);
        sourceid = TSG.GetGuid();
        //
        // SourceType
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_STRING != TagType) return false;
        SourceIndex++;
        SourceCount = sizeof(UInt32);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CountValues = CUdpBinaryDatagramListBase.BuildInt32(ByteSequence);
        SourceCount = CountValues * sizeof(Byte);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CTBScalarString TSS = new CTBScalarString(ByteSequence);
        sourcetype = TSS.GetString();
        SourceIndex++; // trailing zero!
        //
        // SourceName
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_STRING != TagType) return false;
        SourceIndex++;
        SourceCount = sizeof(UInt32);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CountValues = CUdpBinaryDatagramListBase.BuildInt32(ByteSequence);
        SourceCount = CountValues * sizeof(Byte);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        TSS = new CTBScalarString(ByteSequence);
        sourcename = TSS.GetString();
        SourceIndex++; // trailing zero!
        //
        // TargetIpAddress
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_VECTOR_BYTE != TagType) return false;
        SourceIndex++;
        SourceCount = sizeof(UInt32);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CountValues = CUdpBinaryDatagramListBase.BuildInt32(ByteSequence);
        SourceCount = CountValues * sizeof(Byte);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        targetipaddress = ByteSequence;
        //
        // TargetID
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_GUID != TagType) return false;
        SourceIndex++;
        SourceCount = 16;
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        TSG = new CTBScalarGuid(ByteSequence);
        targetid = TSG.GetGuid();
        //
        // TargetType
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_STRING != TagType) return false;
        SourceIndex++;
        SourceCount = sizeof(UInt32);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CountValues = CUdpBinaryDatagramListBase.BuildInt32(ByteSequence);
        SourceCount = CountValues * sizeof(Byte);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        TSS = new CTBScalarString(ByteSequence);
        targettype = TSS.GetString();
        SourceIndex++; // trailing zero!
        //
        // TargetName
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_STRING != TagType) return false;
        SourceIndex++;
        SourceCount = sizeof(UInt32);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CountValues = CUdpBinaryDatagramListBase.BuildInt32(ByteSequence);
        SourceCount = CountValues * sizeof(Byte);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        TSS = new CTBScalarString(ByteSequence);
        targetname = TSS.GetString();
        SourceIndex++; // trailing zero!
        //
        // DatagramID
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_GUID != TagType) return false;
        SourceIndex++;
        SourceCount = 16;
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        TSG = new CTBScalarGuid(ByteSequence);
        datagramid = TSG.GetGuid();
        //
        // BlockIndex
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_INT32 != TagType) return false;
        SourceIndex++;
        SourceCount = sizeof(Int32);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CTBScalarInt32 TSI = new CTBScalarInt32(ByteSequence);
        blockindex = TSI.GetInt32();
        //
        // BlockCount
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_INT32 != TagType) return false;
        SourceIndex++;
        SourceCount = sizeof(Int32);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        TSI = new CTBScalarInt32(ByteSequence);
        blockcount = TSI.GetInt32();
        //
        // DatagramType
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_STRING != TagType) return false;
        SourceIndex++;
        SourceCount = sizeof(UInt32);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CountValues = CUdpBinaryDatagramListBase.BuildInt32(ByteSequence);
        SourceCount = CountValues * sizeof(Byte);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        TSS = new CTBScalarString(ByteSequence);
        datagramtype = TSS.GetString();
        SourceIndex++; // trailing zero!
        //
        // DatagramKey
        TagType = bytevector[SourceIndex];
        if (CTagBase.TYPE_SCALAR_STRING != TagType) return false;
        SourceIndex++;
        SourceCount = sizeof(UInt32);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        CountValues = CUdpBinaryDatagramListBase.BuildInt32(ByteSequence);
        SourceCount = CountValues * sizeof(Byte);
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        TSS = new CTBScalarString(ByteSequence);
        datagramkey = TSS.GetString();
        SourceIndex++; // trailing zero!
        //
        // Content
        // No tagtaype - only tail of bytedatagram!
        SourceCount = bytevector.Length - SourceIndex;
        ByteSequence = CUdpBinaryDatagramListBase.BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
        content = ByteSequence;
        // 
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public Management
    //--------------------------------------------------------------------------
    //
    public Boolean PresetByteVector(Byte[] bytevector, Int32 size, out Guid datagramid)
    {
      datagramid = Guid.Empty;
      try
      {
        Char[] ProtocolType;
        DateTime TimeStamp;
        Byte[] SourceIpAddress;
        Guid SourceID;
        String SourceType, SourceName;
        Byte[] TargetIpAddress;
        Guid TargetID;
        String TargetType, TargetName;
        // Guid DatagramID;
        Int32 BlockIndex, BlockCount;
        String DatagramType, DatagramKey;
        Byte[] Content;
        if (AnalyseDatagram(bytevector, size,
                            out ProtocolType, out TimeStamp,
                            out SourceIpAddress, out SourceID,
                            out SourceType, out SourceName,
                            out TargetIpAddress, out TargetID,
                            out TargetType, out TargetName,
                            out datagramid,
                            out BlockIndex, out BlockCount,
                            out DatagramType, out DatagramKey,
                            out Content))
        {
          CUdpBinaryDatagramReceive UBDR = new CUdpBinaryDatagramReceive(ProtocolType, TimeStamp,
                                                                         SourceIpAddress, SourceID,
                                                                         SourceType, SourceName,
                                                                         TargetIpAddress, TargetID,
                                                                         TargetType, TargetName,
                                                                         datagramid,
                                                                         BlockIndex, BlockCount,
                                                                         DatagramType, DatagramKey,
                                                                         Content);
          // Collect all received datagrams with the same DatagramID!
          CUdpBinaryDatagramListReceive UBDLR = FindUdpBinaryDatagramListReceive(datagramid);
          if (null == UBDLR)
          {
            UBDLR = new CUdpBinaryDatagramListReceive(datagramid, BlockCount);
            this.Add(UBDLR);
          }
          UBDLR.Add(UBDR);
          UBDLR.CheckDatagramReceived(BlockIndex - 1);
          Boolean AllDatagramIDsReceived = UBDLR.CheckAllReceived();
          return AllDatagramIDsReceived;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean RemoveUdpBinaryDatagramListReceive(Guid datagramid)
    {
      CUdpBinaryDatagramListReceive UBDLR = FindUdpBinaryDatagramListReceive(datagramid);
      if (UBDLR is CUdpBinaryDatagramListReceive)
      {
        return this.Remove(UBDLR);
      }
      return false;
    }

    public CTagBinaryList GetContent(Guid datagramid)
    {
      CTagBinaryList Result = null;
      CUdpBinaryDatagramListReceive UBDLR = FindUdpBinaryDatagramListReceive(datagramid);
      if (UBDLR is CUdpBinaryDatagramListReceive)
      {
        Result = UBDLR.GetContent();
      }
      return Result;
    }

    public String[] GetMessages(Guid datagramid)
    {
      String[] Result = null;
      CUdpBinaryDatagramListReceive UBDLR = FindUdpBinaryDatagramListReceive(datagramid);
      if (UBDLR is CUdpBinaryDatagramListReceive)
      {
        Result = UBDLR.GetMessages();
      }
      return Result;
    }

    public Double[,] GetVector2DDouble(Guid datagramid)
    {
      Double[,] Result = null;
      CUdpBinaryDatagramListReceive UBDLR = FindUdpBinaryDatagramListReceive(datagramid);
      if (UBDLR is CUdpBinaryDatagramListReceive)
      {
        Result = UBDLR.GetVector2DDouble();
      }
      return Result;
    }


  }
}
