﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
//
using UCNotifier;
using Task;
using Network;
//
namespace UdpBinaryTransfer
{
  public class CUdpBinaryTransmitter : CUdpBinaryBase
  {
    //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    protected DOnDatagramTransmitted FOnDatagramTransmitted;
    protected CUdpBinaryDatagramContainerTransmit FUdpBinaryDatagramContainerTransmit;
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public CUdpBinaryTransmitter()
      : base()
    {
      FTask = null;
      FOnDatagramTransmitted = null;
      FUdpBinaryDatagramContainerTransmit = new CUdpBinaryDatagramContainerTransmit();
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public void SetOnDatagramTransmitted(DOnDatagramTransmitted value)
    {
      FOnDatagramTransmitted = value;
    }

    public Boolean PresetUdpBinaryDatagramContainerTransmit(CUdpBinaryDatagramContainerTransmit container)
    {
      try
      {
        foreach (CUdpBinaryDatagramListTransmit UBDLT in container)
        {
          FUdpBinaryDatagramContainerTransmit.Add(UBDLT);
        }
        if (FTask is CTask)
        {
          if (FTask.IsFinished())
          {
            FTask = null;
          }
        }
        if (!(FTask is CTask))
        {
          FTask = new CTask("UdpBinaryTransmitter",
                            TaskOnExecutionStart,
                            TaskOnExecutionBusy,
                            TaskOnExecutionEnd,
                            TaskOnExecutionAbort);
          FTask.Start();
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //
    private void TaskOnExecutionStart(RTaskData data)
    {
    }

    private Boolean TaskOnExecutionBusy(ref RTaskData data)
    {
      try
      {
        Boolean Ready = false;
        do
        {
          if (0 < FUdpBinaryDatagramContainerTransmit.Count)
          {
            CUdpBinaryDatagramListTransmit UBDLT = FUdpBinaryDatagramContainerTransmit[0];
            if (0 < UBDLT.Count)
            {
              CUdpBinaryDatagramTransmit UBDT = (CUdpBinaryDatagramTransmit)UBDLT[0];
              Byte[] ByteVector = UBDT.GetByteVector();
              if (ByteVector is Byte[])
              {
                IPEndPoint IPETarget = new IPEndPoint(FIpEndPoint.Address, FIpEndPoint.Port);
                if (FOnDatagramTransmitted is DOnDatagramTransmitted)
                {
                  FOnDatagramTransmitted(IPETarget, ByteVector, ByteVector.Length);
                }
                FUdpClient.Send(ByteVector, ByteVector.Length);
                Thread.Sleep(100);
              }
              UBDLT.Remove(UBDT);
            }
            else
            {
              FUdpBinaryDatagramContainerTransmit.Remove(UBDLT);
            }
          }
          else
          {
            Ready = true;
          }
        }
        while (!Ready);
        return false;
      }
      catch (Exception)
      {
        FNotifier.Error("UdpBinaryTransmitter", 1, "Invalid UdpDatagram");
        return false;
      }
    }

    private void TaskOnExecutionEnd(RTaskData data)
    {
    }

    private void TaskOnExecutionAbort(RTaskData data)
    {
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Method
    //-------------------------------------------------------------------
    //
    public override Boolean Open(IPAddress ipaddresstarget, UInt16 ipporttarget)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        //
        FUdpBinaryDatagramContainerTransmit.Clear();
        FIpEndPoint = new IPEndPoint(ipaddresstarget, ipporttarget);
        FUdpClient = new UdpClient();
        FUdpClient.Connect(FIpEndPoint);
        //
        FTask = null;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Open(String sipendpointtarget)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        //
        FUdpBinaryDatagramContainerTransmit.Clear();
        CNetwork.TextToIPEndPoint(sipendpointtarget, out FIpEndPoint);
        FUdpClient = new UdpClient();
        // open UdpConnection
        FUdpClient.Connect(FIpEndPoint);
        //
        FTask = null;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Close()
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        if (FUdpClient is UdpClient)
        { // close UdpConnection
          FUdpBinaryDatagramContainerTransmit.Clear();
          FUdpClient.Close();
          FUdpClient = null;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}

