﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Network;
//
namespace UdpBinaryTransfer
{
  public class CUdpBinaryDatagramListTransmit : CUdpBinaryDatagramListBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const Int32 SIZE_BLOCK_CONTENT = 1200; // maximal 3K for Udp
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    // -> Base private Guid FDatagramID;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpBinaryDatagramListTransmit()
    {
      FHeader.DatagramID = Guid.NewGuid();
      FContent = null;
    }

    public CUdpBinaryDatagramListTransmit(Guid datagramid)
    {
      FHeader.DatagramID = datagramid;
      FContent = null;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - Public Management
    //--------------------------------------------------------------------------
    //  
    public Boolean BuildDatagramList(RUdpBinaryDatagramHeader header,
                                     String datagramtype, String datagramkey,
                                     CTagBinaryList content)
    {
      try
      {
        FHeader.ProtocolType = CNetwork.PROTOCOLTYPE_UDPBINARY;
        FHeader.TimeStamp = DateTime.Now;
        FHeader.SourceIpAddress = header.SourceIpAddress;
        FHeader.SourceID = header.SourceID;
        FHeader.SourceType = header.SourceType;
        FHeader.SourceName = header.SourceName;
        FHeader.TargetIpAddress = header.TargetIpAddress;
        FHeader.TargetID = header.TargetID;
        FHeader.TargetType = header.TargetType;
        FHeader.TargetName = header.TargetName;
        FHeader.DatagramID = Guid.NewGuid();
        FHeader.BlockIndex = 1;
        FHeader.BlockCount = 1;
        FHeader.DatagramType = datagramtype;
        FHeader.DatagramKey = datagramkey;
        FContent = content;
        // Content(CTagBinaryList) -> ByteList -> ByteVector
        CByteList ByteList = content.GetByteList();
        Byte[] ByteVector = ByteList.GetByteVector();
        Int32 SizeByteVector = ByteVector.Length;
        // Calculate BlockCount:
        FHeader.BlockCount = (Int32)(SizeByteVector / SIZE_BLOCK_CONTENT);
        Int32 BlockModulo = SizeByteVector % SIZE_BLOCK_CONTENT;
        if (0 < BlockModulo)
        {
          FHeader.BlockCount++;
        }
        // Build all Datagrams:
        Int32 ByteIndex = 0;
        for (FHeader.BlockIndex = 1; FHeader.BlockIndex <= FHeader.BlockCount; FHeader.BlockIndex++)
        {
          Int32 ByteCount = SIZE_BLOCK_CONTENT;
          if (FHeader.BlockIndex == FHeader.BlockCount)
          {
            if (0 < BlockModulo)
            {
              ByteCount = BlockModulo;
            }
          }
          Byte[] DatagramContent = BuildTagByteSequence(ByteVector, ref ByteIndex, ByteCount);
          CUdpBinaryDatagramTransmit Datagram = new CUdpBinaryDatagramTransmit(FHeader.TimeStamp,
                                                                               FHeader.SourceIpAddress,
                                                                               FHeader.SourceID,
                                                                               FHeader.SourceType,
                                                                               FHeader.SourceName,
                                                                               FHeader.TargetIpAddress,
                                                                               FHeader.TargetID,
                                                                               FHeader.TargetType,
                                                                               FHeader.TargetName,
                                                                               FHeader.DatagramID,
                                                                               FHeader.BlockIndex,
                                                                               FHeader.BlockCount,
                                                                               FHeader.DatagramType,
                                                                               FHeader.DatagramKey, 
                                                                               DatagramContent);
          this.Add(Datagram);
        }
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public CUdpBinaryDatagramTransmit GetUdpBinaryDatagram(Int32 blockindex)
    {
      return (CUdpBinaryDatagramTransmit)this[blockindex];
    } 

  }
}
