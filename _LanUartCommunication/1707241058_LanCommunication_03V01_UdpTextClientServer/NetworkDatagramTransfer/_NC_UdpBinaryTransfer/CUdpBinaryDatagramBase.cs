﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UdpBinaryTransfer
{
  public class CUdpBinaryDatagramBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
   
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    protected RUdpBinaryDatagramHeader FHeader;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public DateTime GetTimeStamp()
    {
      return FHeader.TimeStamp;
    }
    public void SetTimeStamp(DateTime value)
    {
      FHeader.TimeStamp = value;
    }

    public Byte[] GetSourceIpAddress()
    {
      return FHeader.SourceIpAddress;
    }
    public void SetSourceIpAddress(Byte[] value)
    {
      FHeader.SourceIpAddress = value;
    }

    public Guid GetSourceID()
    {
      return FHeader.SourceID;
    }
    public void SetSourceID(Guid value)
    {
      FHeader.SourceID = value;
    }

    public String GetSourceType()
    {
      return FHeader.SourceType;
    }
    public void SetSourceType(String value)
    {
      FHeader.SourceType = value;
    }

    public String GetSourceName()
    {
      return FHeader.SourceName;
    }
    public void SetSourceName(String value)
    {
      FHeader.SourceName = value;
    }

    public Byte[] GetTargetIpAddress()
    {
      return FHeader.TargetIpAddress;
    }
    public void SetTargetIpAddress(Byte[] value)
    {
      FHeader.TargetIpAddress = value;
    }

    public Guid GetTargetID()
    {
      return FHeader.TargetID;
    }
    public void SetTargetID(Guid value)
    {
      FHeader.TargetID = value;
    }

    public String GetTargetType()
    {
      return FHeader.TargetType;
    }
    public void SetTargetType(String value)
    {
      FHeader.TargetType = value;
    }

    public String GetTargetName()
    {
      return FHeader.TargetName;
    }
    public void SetTargetName(String value)
    {
      FHeader.TargetName = value;
    }

    public Guid GetDatagramID()
    {
      return FHeader.DatagramID;
    }
    public void SetDatagramID(Guid value)
    {
      FHeader.DatagramID = value;
    }

    public Int32 GetBlockIndex()
    {
      return FHeader.BlockIndex;
    }
    public void SetBlockIndex(Int32 value)
    {
      FHeader.BlockIndex = value;
    }

    public Int32 GetBlockCount()
    {
      return FHeader.BlockCount;
    }
    public void SetBlockCount(Int32 value)
    {
      FHeader.BlockCount = value;
    }

    public String GetDatagramType()
    {
      return FHeader.DatagramType;
    }
    public void SetDatagramType(String value)
    {
      FHeader.DatagramType = value;
    }

    public String GetDatagramKey()
    {
      return FHeader.DatagramKey;
    }
    public void SetDatagramKey(String value)
    {
      FHeader.DatagramKey = value;
    }

  }
}



