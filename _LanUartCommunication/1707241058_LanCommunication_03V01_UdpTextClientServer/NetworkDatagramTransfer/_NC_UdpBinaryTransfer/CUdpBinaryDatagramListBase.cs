﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Network;
//
namespace UdpBinaryTransfer
{
  public class CUdpBinaryDatagramListBase : List<CUdpBinaryDatagramBase>
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    protected RUdpBinaryDatagramHeader FHeader;
    protected CTagBinaryList FContent;  // Taglist of elementary Datatypes specific to Type/Key
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    // 
    public Guid GetDatagramID()
    {
      return FHeader.DatagramID;
    }

    public Int32 GetBlockCount()
    {
      return FHeader.BlockCount;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    // 
    public static Byte[] BuildTagByteSequence(Byte[] bytevector, ref Int32 byteindex, Int32 bytecount)
    {
      Byte[] Result = new Byte[bytecount];
      for (Int32 BI = 0; BI < bytecount; BI++)
      {
        Result[BI] = bytevector[byteindex];
        byteindex++;
      }
      return Result;
    }
    // 
    public static Int32 BuildInt32(Byte[] values)
    {
      Int32 Result;
      Result = (values[0] << 0);
      Result |= (values[1] << 8);
      Result |= (values[2] << 16);
      Result |= (values[3] << 24);
      return Result;
    }
  }
}
