﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Network;
//
namespace UdpBinaryTransfer
{
  public class CUdpBinaryDatagramListReceive : CUdpBinaryDatagramListBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    // -> Base private Guid FDatagramID;
    private CReceivedDatagramCheck FReceivedDatagramCheck;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpBinaryDatagramListReceive(Guid datagramid, Int32 blockcount)
    {
      FHeader.DatagramID = datagramid;
      FContent = null;
      FReceivedDatagramCheck = new CReceivedDatagramCheck(blockcount);
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void CheckDatagramReceived(Int32 datagramindex)
    {
      FReceivedDatagramCheck.SetCheck(datagramindex);        
    }

    public Boolean CheckAllReceived()
    {
      return FReceivedDatagramCheck.CheckAll();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private CTagBinaryList ConvertByteVectorToTagBinaryList(Byte[] bytevector)
    {
      CTagBinaryList TagBinaryList = new CTagBinaryList();
      if (bytevector is Byte[])
      {
        Int32 SizeVector = bytevector.Length;
        Int32 SourceCount;
        Byte[] ByteSequence;
        Int32 CountValues;
        if (0 < SizeVector)
        {
          Int32 SourceIndex = 0;
          while (SourceIndex < SizeVector)
          {
            Byte TagType = bytevector[SourceIndex];
            SourceIndex++;
            switch (TagType)
            { // Tag - Scalar
              case CTagBase.TYPE_SCALAR_BYTE:
                break;
              case CTagBase.TYPE_SCALAR_CHAR:
                break;
              case CTagBase.TYPE_SCALAR_UINT16:
                break;
              case CTagBase.TYPE_SCALAR_UINT32:
                break;
              case CTagBase.TYPE_SCALAR_INT16:
                break;
              case CTagBase.TYPE_SCALAR_INT32:
                break;
              case CTagBase.TYPE_SCALAR_DOUBLE:
                SourceCount = sizeof(Double);
                ByteSequence = BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
                CTBScalarDouble TSD = new CTBScalarDouble(ByteSequence);
                TagBinaryList.Add(TSD);
                break;
              case CTagBase.TYPE_SCALAR_STRING:
                SourceCount = sizeof(UInt32);
                ByteSequence = BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
                CountValues = BuildInt32(ByteSequence);
                SourceCount = CountValues * sizeof(Byte);
                ByteSequence = BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
                CTBScalarString TSS = new CTBScalarString(ByteSequence);
                TagBinaryList.Add(TSS);
                SourceIndex++; // trailing zero!
                break;
              case CTagBase.TYPE_SCALAR_DATETIME:
                break;
              case CTagBase.TYPE_SCALAR_GUID:
                break;
              // Tag - Vector
              case CTagBase.TYPE_VECTOR_BYTE:
                break;
              case CTagBase.TYPE_VECTOR_CHAR:
                break;
              case CTagBase.TYPE_VECTOR_UINT16:
                break;
              case CTagBase.TYPE_VECTOR_UINT32:
                break;
              case CTagBase.TYPE_VECTOR_INT16:
                break;
              case CTagBase.TYPE_VECTOR_INT32:
                break;
              case CTagBase.TYPE_VECTOR_DOUBLE:
                break;
              case CTagBase.TYPE_VECTOR_STRING:
                break;
              // Tag - Matrix
              case CTagBase.TYPE_MATRIX_BYTE:
                break;
              case CTagBase.TYPE_MATRIX_CHAR:
                break;
              case CTagBase.TYPE_MATRIX_UINT16:
                break;
              case CTagBase.TYPE_MATRIX_UINT32:
                break;
              case CTagBase.TYPE_MATRIX_INT16:
                break;
              case CTagBase.TYPE_MATRIX_INT32:
                break;
              case CTagBase.TYPE_MATRIX_DOUBLE:
                break;
              case CTagBase.TYPE_MATRIX_STRING:
                break;
            }
          }
        }
      }
      return TagBinaryList;
    }
    
    //    !!!!!!!!!!!!!!!! ONLY SPECIAL CASE !!!!!!!!!!!
    private CStringList ConvertByteVectorToStringList(Byte[] bytevector)
    {
      CStringList Result = new CStringList();
      if (bytevector is Byte[])
      {
        Int32 SizeVector = bytevector.Length;
        if (0 < SizeVector)
        {
          Int32 SourceIndex = 0;
          while (SourceIndex < SizeVector)
          {
            Byte TagType = bytevector[SourceIndex];
            SourceIndex++;
            if (CTagBase.TYPE_SCALAR_STRING == TagType)
            {
              Int32 SourceCount = sizeof(UInt32);
              Byte[] ByteSequence = BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
              Int32 CountValues = BuildInt32(ByteSequence);
              SourceCount = CountValues * sizeof(Byte);
              ByteSequence = BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
              CTBScalarString TSS = new CTBScalarString(ByteSequence);
              Result.Add(TSS.GetString());
              SourceIndex++; // trailing zero!
            }
          }
        }
      }
      return Result;
    }

    private Double[,] ConvertByteVectorToVector2DDouble(Byte[] bytevector)
    {
      Double[,] Vector2DDouble = null;
      if (bytevector is Byte[])
      {
        Int32 SizeVector = bytevector.Length;
        if (0 < SizeVector)
        {
          Int32 SourceIndex = 0;
          while (SourceIndex < SizeVector)
          {
            Byte TagType = bytevector[SourceIndex];
            SourceIndex++;
            if (CTagBase.TYPE_MATRIX_DOUBLE == TagType)
            {
              Int32 SourceCount = sizeof(UInt32);
              Byte[] ByteSequence = BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
              Int32 RowCount = BuildInt32(ByteSequence);
              SourceCount = sizeof(UInt32);
              ByteSequence = BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
              Int32 ColCount = BuildInt32(ByteSequence);
              Vector2DDouble = new Double[RowCount, ColCount];
              SourceCount = sizeof(Double);
              for (Int32 RI = 0; RI < RowCount; RI++)
              {
                for (Int32 CI = 0; CI < ColCount; CI++)
                {
                  ByteSequence = BuildTagByteSequence(bytevector, ref SourceIndex, SourceCount);
                  CTBScalarDouble TBD = new CTBScalarDouble(ByteSequence);
                  Vector2DDouble[RI, CI] = TBD.GetDouble();
                }
              }
            }
          }
        }
      }
      return Vector2DDouble;
    }

    // !!!!!!!!!!!!!!! ConvertByteVectorToTagBinaryList!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    // Build all contents to one contents
    private Byte[] BuildTotalContent()
    {
      Int32 DatagramCount = FReceivedDatagramCheck.GetCount();
      Byte[][] ContentVector = new Byte[DatagramCount][];
      Int32 TotalLength = 0;
      foreach (CUdpBinaryDatagramReceive UBDR in this)
      {
        Int32 BI = UBDR.GetBlockIndex() - 1;
        ContentVector[BI] = UBDR.GetContent();
        TotalLength += UBDR.GetContent().Length;
      }
      Byte[] TotalContent = new Byte[TotalLength];
      Int32 TI = 0;
      Int32 DI = 0;
      foreach (CUdpBinaryDatagramReceive UBDR in this)
      {
        Int32 CL = ContentVector[DI].Length;
        for (Int32 CI = 0; CI < CL; CI++)
        {
          TotalContent[TI] = ContentVector[DI][CI];
          TI++;
        }
        DI++;
      }
      return TotalContent;
    }



    public CTagBinaryList GetContent()
    { // have per definition all the same DatagramID!!!
      CTagBinaryList TagBinaryList = null;
      if (FReceivedDatagramCheck.CheckAll())
      {
        Byte[] TotalContent = BuildTotalContent();
        TagBinaryList = ConvertByteVectorToTagBinaryList(TotalContent);
      }
      return TagBinaryList;
    }

    public String[] GetMessages() 
    {
      String[] Messages = null;
      if (FReceivedDatagramCheck.CheckAll())
      { // Build all contents to one contents
        Byte[] TotalContent = BuildTotalContent();
        CStringList MessageList = ConvertByteVectorToStringList(TotalContent);
        Int32 SC = MessageList.Count;
        Messages = new String[SC];
        for (Int32 SI = 0; SI < SC; SI++)
        {
          Messages[SI] = MessageList[SI];
        }
      }
      return Messages;
    }

    public Double[,] GetVector2DDouble()
    {
      Double[,] Vector2DDouble = null;
      if (FReceivedDatagramCheck.CheckAll())
      { // Build all contents to one contents
        Byte[] TotalContent = BuildTotalContent();
        Vector2DDouble = ConvertByteVectorToVector2DDouble(TotalContent);
      }
      return Vector2DDouble;
    }
    //    Int32 DatagramCount = FReceivedDatagramCheck.GetCount();
    //    Byte[][] ContentVector = new Byte[DatagramCount][];
    //    Int32 TotalLength = 0;
    //    foreach (CUdpBinaryDatagramReceive UBDR in this)
    //    {
    //      Int32 BI = UBDR.GetBlockIndex() - 1;
    //      ContentVector[BI] = UBDR.GetContent();
    //      TotalLength += UBDR.GetContent().Length;
    //    }
    //    Byte[] ContentTotal = new Byte[TotalLength];
    //    Int32 TI = 0;
    //    Int32 DI = 0;
    //    foreach (CUdpBinaryDatagramReceive UBDR in this)
    //    {
    //      Int32 CL = ContentVector[DI].Length;
    //      for (Int32 CI = 0; CI < CL; CI++)
    //      {
    //        ContentTotal[TI] = ContentVector[DI][CI];
    //        TI++;
    //      }
    //      DI++;
    //    }
    //    CStringList MessageList = ConvertByteVectorToStringList(ContentTotal);
    //    Int32 SC = MessageList.Count;
    //    Messages = new String[SC];
    //    for (Int32 SI = 0; SI < SC; SI++)
    //    {
    //      Messages[SI] = MessageList[SI];
    //    }
    //  }
    //  return Messages;
    //}


    //
    //if (FContent is CTagBinaryList)
    //{
    //  CStringList StringList = new CStringList();
    //  foreach (CTBBinary TagBinary in FContent)
    //  {
    //    if (TagBinary is CTBScalarString)
    //    {
    //      String S = ((CTBScalarString)TagBinary).GetString();
    //      StringList.Add(S);
    //    }
    //  }
    //  int Size = StringList.Count;
    //  Messages = new String[Size];
    //  for (int SI = 0; SI < Size; SI++)
    //  {
    //    Messages[SI] = StringList[SI];
    //  }
    //}
    //

  }
}
