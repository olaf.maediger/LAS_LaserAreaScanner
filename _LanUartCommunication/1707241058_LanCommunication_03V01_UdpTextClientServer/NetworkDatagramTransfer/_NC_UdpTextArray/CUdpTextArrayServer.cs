﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using Network;
using UdpTextTransfer;
//
namespace UdpTextArray
{
  public delegate void DOnBuildDoubleArray2D(Int32 datasize, String[] datanames, 
                                              String[] dataunits, out Double[,] datavector);
  //
  public class CUdpTextArrayServer : CUdpTextArrayBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const Int32 INIT_TUPELCOUNT = 8;
    public const String INIT_DATANAME_X = "X";
    public const String INIT_DATANAME_Y = "V";
    public const String INIT_DATAUNIT_X = "m";
    public const String INIT_DATAUNIT_Y = "m/s";
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private Int32 FTupelCount;
    private String[] FDataNames;
    private String[] FDataUnits;
    private DOnBuildDoubleArray2D FOnBuildDoubleArray2D;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpTextArrayServer()
            : base()
    {
      FTupelCount = INIT_TUPELCOUNT;
      FDataNames = new String[DIMENSION_VECTOR2D];
      FDataNames[INDEX_X] = INIT_DATANAME_X;
      FDataNames[INDEX_Y] = INIT_DATANAME_Y;
      FDataUnits = new String[DIMENSION_VECTOR2D];
      FDataUnits[INDEX_X] = INIT_DATAUNIT_X;
      FDataUnits[INDEX_Y] = INIT_DATAUNIT_Y;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetOnBuildDoubleArray2D(DOnBuildDoubleArray2D value)
    {
      FOnBuildDoubleArray2D = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private Boolean BuildDatagramPresetArrayDouble2D(String datagramtype,
                                                      String datagramkey,
                                                      String[] datanames, String[] dataunits, Double[,] datavector,
                                                      out String sdatagram)
    {
      sdatagram = "";
      try
      { // Header
        BuildHeader(datagramtype, datagramkey, out sdatagram);
        // Content
        Int32 TC = datavector.GetLength(0);
        sdatagram += String.Format(FORMAT_TUPELCOUNT, TC);
        sdatagram += CUdpTextBase.SEPARATOR_UDPTEXT;
        sdatagram += String.Format(FORMAT_DATANAME2D, datanames);
        sdatagram += CUdpTextBase.SEPARATOR_UDPTEXT;
        sdatagram += String.Format(FORMAT_DATAUNIT2D, dataunits);
        sdatagram += CUdpTextBase.SEPARATOR_UDPTEXT;
        for (Int32 TI = 0; TI < TC; TI++)
        {
          sdatagram += String.Format(FORMAT_TUPELDOUBLE2D, TI, datavector[TI, 0], datavector[TI, 1]);
          sdatagram += CUdpTextBase.SEPARATOR_UDPTEXT;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public override Boolean Open()
    {
      FUdpTextTransmitter.Open(FTargetIpAddress,
                               FTargetIpPort);
      FUdpTextTransmitter.SetOnDatagramTransmitted(UdpTextTransmitterOnDatagramTransmitted);
      FUdpTextReceiver.Open(FSourceIpAddress,
                            FSourceIpPort);
      FUdpTextReceiver.SetOnDatagramReceived(UdpTextReceiverOnDatagramReceived);
      return true;
    }

    public override Boolean Close()
    {
      FUdpTextTransmitter.Close();
      FUdpTextTransmitter.SetOnDatagramTransmitted(null);
      FUdpTextReceiver.Close();
      FUdpTextReceiver.SetOnDatagramReceived(null);
      return true;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    // 
    private void ResponseArrayDouble2D()
    {
      Double[,] DataArray;
      if (FOnBuildDoubleArray2D is DOnBuildDoubleArray2D)
      {
        FOnBuildDoubleArray2D(FTupelCount, FDataNames, FDataUnits, out DataArray);
        PresetArrayDouble2D(FDataNames, FDataUnits, DataArray);
      }
    }

    private void AnalyseSetParameter(CTagTextList content)
    {
      foreach (CTagText Tag in content)
      { //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Transform to new Tags!!!!
        //switch (Tag.GetType())
        //{
        //  case TOKEN_TUPELCOUNT:
        //    FTupelCount = Int32.Parse(Tag.GetValue(CTagText.INDEX_VALUE0));
        //    break;
        //  case TOKEN_DATANAME2D:
        //    FDataNames = new String[DIMENSION_VECTOR2D];
        //    FDataNames[INDEX_X] = Tag.GetValue(CTagText.INDEX_VALUE0);
        //    FDataNames[INDEX_Y] = Tag.GetValue(CTagText.INDEX_VALUE1);
        //    break;
        //  case TOKEN_DATAUNIT2D:
        //    FDataUnits = new String[DIMENSION_VECTOR2D];
        //    FDataUnits[INDEX_X] = Tag.GetValue(CTagText.INDEX_VALUE0);
        //    FDataUnits[INDEX_Y] = Tag.GetValue(CTagText.INDEX_VALUE1);
        //    break;
        //}
      }
    }

    //
    //--------------------------------------------------------------------------
    //  Segment - Callback
    //--------------------------------------------------------------------------
    //
    protected override void UdpTextTransmitterOnDatagramTransmitted(IPEndPoint ipendpointtarget,
                                                                    Byte[] datagram, Int32 size)
    {          
      if (FOnDatagramTransmitted is DOnDatagramTransmitted)
      {
        FOnDatagramTransmitted(ipendpointtarget, datagram, size);
      }
    }

    protected override void UdpTextReceiverOnDatagramReceived(IPEndPoint ipendpointsource,
                                                              Byte[] datagram, Int32 size)
    {
      String Text = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
      SUdpTextDatagram Datagram;
      if (AnalyseDatagram(Text, out Datagram))
      {
        switch (Datagram.DatagramType)
        {
          case TOKEN_VECTORDOUBLE2D:
            switch (Datagram.DatagramKey)
            {
              case TOKEN_REQUESTVECTOR:
                ResponseArrayDouble2D();
                break;
              case TOKEN_SETPARAMETER:
                AnalyseSetParameter(Datagram.Content);
                break;
            }
            break;
        }
      }
      //
      if (FOnDatagramReceived is DOnDatagramReceived)
      {
        FOnDatagramReceived(ipendpointsource, datagram, size);
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public Function
    //--------------------------------------------------------------------------
    //
    public Boolean PresetArrayDouble2D(String[] datanames, String[] dataunits, Double[,] datavector)
    {
      try
      {
        String SDatagram;
        BuildDatagramPresetArrayDouble2D(TOKEN_VECTORDOUBLE2D, "",                           
                                          datanames, dataunits, datavector,
                                          out SDatagram);
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUdpTextTransmitter.PresetMessage(SDatagram);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
