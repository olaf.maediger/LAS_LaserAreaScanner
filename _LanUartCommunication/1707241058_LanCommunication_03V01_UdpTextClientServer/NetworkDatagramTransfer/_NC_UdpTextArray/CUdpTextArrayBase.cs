﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using UCNotifier;
using Network;
using UdpTextTransfer;
//
namespace UdpTextArray
{
  public abstract class CUdpTextArrayBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const Int32 DIMENSION_VECTOR2D = 2;
    public const Int32 INDEX_X = 0;
    public const Int32 INDEX_Y = 1;
    public const Int32 INDEX_Z = 2;
    //
    public const String TOKEN_VECTORDOUBLE1D = "ArrayDouble1D";
    public const String TOKEN_VECTORDOUBLE2D = "ArrayDouble2D";
    public const String TOKEN_VECTORINT321D = "ArrayInt321D";
    public const String TOKEN_VECTORINT322D = "ArrayInt322D";
    public const String TOKEN_REQUESTVECTOR = "RequestArray";
    public const String TOKEN_SETPARAMETER = "SetParameter";
    //
    public const String TOKEN_TUPELCOUNT = "TupelCount";
    public const String TOKEN_DATANAME2D = "DataName2D";
    public const String TOKEN_DATAUNIT2D = "DataUnit2D";
    public const String TOKEN_TUPELDOUBLE2D = "T";
    //
    public const String FORMAT_TUPELCOUNT = TOKEN_TUPELCOUNT + "[{0}]";
    public const String FORMAT_DATANAME2D = TOKEN_DATANAME2D + "[{0}][{1}]";
    public const String FORMAT_DATAUNIT2D = TOKEN_DATAUNIT2D + "[{0}][{1}]";
    public const String FORMAT_TUPELDOUBLE2D = TOKEN_TUPELDOUBLE2D + "[{0}][{1:0.000}][{2:0.000}]";
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    protected IPAddress FSourceIpAddress, FTargetIpAddress;
    protected UInt16 FSourceIpPort, FTargetIpPort;
    protected Guid FSourceID, FTargetID;
    protected String FSourceType, FTargetType;
    protected String FSourceName, FTargetName;
    protected CUdpTextTransmitter FUdpTextTransmitter;
    protected CUdpTextReceiver FUdpTextReceiver;
    protected DOnDatagramTransmitted FOnDatagramTransmitted;
    protected DOnDatagramReceived FOnDatagramReceived;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpTextArrayBase()
    {
      FUdpTextTransmitter = new CUdpTextTransmitter();
      FUdpTextReceiver = new CUdpTextReceiver();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FUdpTextTransmitter.SetNotifier(value);
      FUdpTextReceiver.SetNotifier(value);
    }

    public void SetOnDatagramTransmitted(DOnDatagramTransmitted value)
    {
      FOnDatagramTransmitted = value;
      FUdpTextTransmitter.SetOnDatagramTransmitted(UdpTextTransmitterOnDatagramTransmitted);
    }
    
    public void SetOnDatagramReceived(DOnDatagramReceived value)
    {
      FOnDatagramReceived = value;
      FUdpTextReceiver.SetOnDatagramReceived(UdpTextReceiverOnDatagramReceived);
    }

    public void SetSourceIpAddress(IPAddress value)
    {
      FSourceIpAddress = value;
    }
    public void SetTargetIpAddress(IPAddress value)
    {
      FTargetIpAddress = value;
    }

    public void SetSourceIpPort(UInt16 value)
    {
      FSourceIpPort = value;
    }
    public void SetTargetIpPort(UInt16 value)
    {
      FTargetIpPort = value;
    }
    
    public void SetSourceType(String value)
    {
      FSourceType = value;
    }
    public void SetTargetType(String value)
    {
      FTargetType = value;
    }

    public void SetSourceName(String value)
    {
      FSourceName = value;
    }
    public void SetTargetName(String value)
    {
      FTargetName = value;
    }

    public void SetSourceID(Guid value)
    {
      FSourceID = value;
    }
    public void SetTargetID(Guid value)
    {
      FTargetID = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    protected Boolean BuildHeader(String datagramtype,
                                  String datagramkey,
                                  out String header)
    {
      header = "";
      try
      {
        header += String.Format(CUdpTextHeader.FORMAT_PROTOCOL, CDatagram.TOKEN_UDPTEXT);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_TIMESTAMP, CDateTime.GetNow());
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_SOURCEIPADDRESS, FSourceIpAddress.ToString());
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_SOURCEID, FSourceID.ToString());
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_SOURCETYPE, FSourceType);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_SOURCENAME, FSourceName);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_TARGETIPADDRESS, FTargetIpAddress.ToString());
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_TARGETID, FTargetID);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_TARGETTYPE, FTargetType);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_TARGETNAME, FTargetName);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_DATAGRAMTYPE, datagramtype);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_DATAGRAMKEY, datagramkey);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    protected Boolean AnalyseDatagram(String text, out SUdpTextDatagram datagram)
    {
      datagram = new SUdpTextDatagram();
      datagram.Content = new CTagTextList();
      try
      {
        CTagTextList TagTextList = new CTagTextList();
        //////////////TagTextList.Build(text);
        //////////////foreach (CTagText Tag in TagList)
        //////////////{
        //////////////  switch (Tag.GetHeader())
        //////////////  {
        //////////////    case CUdpTextHeader.TOKEN_PROTOCOL:
        //////////////      datagram.Protocol = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_TIMESTAMP:
        //////////////      datagram.TimeStamp = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_SOURCEIPADDRESS:
        //////////////      datagram.SourceIpAddress = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_SOURCETYPE:
        //////////////      datagram.SourceType = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_SOURCEID:
        //////////////      datagram.SourceID = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_SOURCENAME:
        //////////////      datagram.SourceName = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_TARGETIPADDRESS:
        //////////////      datagram.TargetIpAddress = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_TARGETTYPE:
        //////////////      datagram.TargetType = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_TARGETID:
        //////////////      datagram.TargetID = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_TARGETNAME:
        //////////////      datagram.TargetName = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_DATAGRAMTYPE:
        //////////////      datagram.DatagramType = Tag.GetValue(1);
        //////////////      break;
        //////////////    case CUdpTextHeader.TOKEN_DATAGRAMKEY:
        //////////////      datagram.DatagramKey = Tag.GetValue(1);
        //////////////      break;
        //////////////    default:
        //////////////      datagram.Content.Add(Tag);
        //////////////      break;
        //////////////  }
        //////////////}
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Callback
    //--------------------------------------------------------------------------
    //
    protected abstract void UdpTextTransmitterOnDatagramTransmitted(IPEndPoint ipendpointtarget,
                                                                    Byte[] datagram, Int32 size);
    protected abstract void UdpTextReceiverOnDatagramReceived(IPEndPoint ipendpointsource,
                                                              Byte[] datagram, Int32 size);
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public abstract Boolean Open();
    public abstract Boolean Close();

  }
}
