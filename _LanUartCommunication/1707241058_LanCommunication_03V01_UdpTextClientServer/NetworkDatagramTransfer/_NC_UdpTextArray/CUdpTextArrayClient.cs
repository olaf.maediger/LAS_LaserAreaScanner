﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using Network;
using UdpTextTransfer;
//
namespace UdpTextArray
{
  public class CUdpTextArrayClient : CUdpTextArrayBase
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpTextArrayClient()
            : base()
    {
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private Boolean BuildDatagramRequestArrayDouble2D(out String sdatagram)
    {
      sdatagram = "";
      try
      { // Header
        BuildHeader(TOKEN_VECTORDOUBLE2D, TOKEN_REQUESTVECTOR, out sdatagram);
        // Content
        // ---
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean BuildDatagramPresetParameterArrayDouble2D(Int32 datavectorsize,
                                                               String[] datanames,
                                                               String[] dataunits,
                                                               out String sdatagram)
    {
      sdatagram = "";
      try
      { // Header
        BuildHeader(TOKEN_VECTORDOUBLE2D, TOKEN_SETPARAMETER, out sdatagram);
        // Content
        sdatagram += String.Format(FORMAT_TUPELCOUNT, datavectorsize);
        sdatagram += CUdpTextBase.SEPARATOR_UDPTEXT;
        sdatagram += String.Format(FORMAT_DATANAME2D, datanames);
        sdatagram += CUdpTextBase.SEPARATOR_UDPTEXT;
        sdatagram += String.Format(FORMAT_DATAUNIT2D, dataunits);
        sdatagram += CUdpTextBase.SEPARATOR_UDPTEXT;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public override Boolean Open()
    {
      FUdpTextTransmitter.Open(FTargetIpAddress,
                               FTargetIpPort);
      FUdpTextReceiver.Open(FSourceIpAddress,
                            FSourceIpPort);
      return true;
    }

    public override Boolean Close()
    {
      FUdpTextTransmitter.Close();
      FUdpTextReceiver.Close();
      return true;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Callback
    //--------------------------------------------------------------------------
    //
    protected override void UdpTextTransmitterOnDatagramTransmitted(IPEndPoint ipendpointtarget,
                                                                    Byte[] datagram, Int32 size)
    {
      if (FOnDatagramTransmitted is DOnDatagramTransmitted)
      {
        FOnDatagramTransmitted(ipendpointtarget, datagram, size);
      }
    }

    protected override void UdpTextReceiverOnDatagramReceived(IPEndPoint ipendpointsource,
                                                              Byte[] datagram, Int32 size)
    {
      if (FOnDatagramReceived is DOnDatagramReceived)
      {
        FOnDatagramReceived(ipendpointsource, datagram, size);
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public Function
    //--------------------------------------------------------------------------
    //
    public Boolean RequestArrayDouble2D()
    {
      try
      {
        String SDatagram;
        BuildDatagramRequestArrayDouble2D(out SDatagram);
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUdpTextTransmitter.PresetMessage(SDatagram);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean PresetParameterArrayDouble2D(Int32 datavectorsize,
                                                 String[] datanames,
                                                 String[] dataunits)
    {
      try
      {
        String SDatagram;
        BuildDatagramPresetParameterArrayDouble2D(datavectorsize,
                                                   datanames,
                                                   dataunits,
                                                   out SDatagram);
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUdpTextTransmitter.PresetMessage(SDatagram);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
