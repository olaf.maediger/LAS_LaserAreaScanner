﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
//
using NhdBase;
//
namespace UCNhdUdpText
{
  //
  //--------------------------------------------------------------------------
  //  Segment - Type
  //--------------------------------------------------------------------------
  //
  public delegate void DOnSendMessage(String message);
  //
  public partial class CUCNhdUdpTextClient : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private DOnActivateDatagram FOnSendMessage;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCNhdUdpTextClient()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetOnSendMessage(DOnActivateDatagram value)
    {
      FOnSendMessage = value;
    }

    public void SetIpAddressLocal(IPAddress value)
    {
      FUCIpAddressLocal.SetIpAddress(value);
    }
    public void SetIpPortTransmit(UInt16 value)
    {
      FUCIpPortTxd.SetIpPort(value);
    }
    public void SetIpPortReceive(UInt16 value)
    {
      FUCIpPortRxd.SetIpPort(value);
    }

    public void SetIpAddressTarget(IPAddress value)
    {
      FUCIpAddressTarget.SetIpAddress(value);
    }

    public String GetMessage()
    {
      return tbxMessage.Text;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //
    private void btnSendMessage_Click(object sender, EventArgs e)
    {
      if (FOnSendMessage is DOnActivateDatagram)
      {
        FOnSendMessage();
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public Management
    //--------------------------------------------------------------------------
    //
    public void Close()
    {

    }

  }
}
