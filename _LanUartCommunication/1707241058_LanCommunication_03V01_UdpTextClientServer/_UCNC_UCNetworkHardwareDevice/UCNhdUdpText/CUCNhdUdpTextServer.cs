﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
//
using NhdBase;
//
namespace UCNhdUdpText
{
  public partial class CUCNhdUdpTextServer : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private DOnActivateDatagram FOnSendMessage;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCNhdUdpTextServer()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetOnSendMessage(DOnActivateDatagram value)
    {
      FOnSendMessage = value;
    }

    public void SetSourceIpAddress(IPAddress value)
    {
      FUCIpAddressLocal.SetIpAddress(value);
    }
    public void SetSourceIpPort(UInt16 value)
    {
      FUCIpPortTxd.SetIpPort(value);
    }
    public void SetTargetIpPort(UInt16 value)
    {
      FUCIpPortRxd.SetIpPort(value);
    }

    public void SetTargetIpAddress(IPAddress value)
    {
      FUCIpAddressTarget.SetIpAddress(value);
    }

    public String GetMessage()
    {
      return tbxMessage.Text;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //
    private void btnSendMessage_Click(object sender, EventArgs e)
    {
      if (FOnSendMessage is DOnActivateDatagram)
      {
        FOnSendMessage();
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public Management
    //--------------------------------------------------------------------------
    //
    public void Close()
    {

    }
  }
}
