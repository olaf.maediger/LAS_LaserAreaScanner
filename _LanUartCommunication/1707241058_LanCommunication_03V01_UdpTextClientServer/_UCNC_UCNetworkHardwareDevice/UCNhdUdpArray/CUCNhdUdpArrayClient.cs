﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
//
using Network;
using NhdBase;
using UCNhdCommon;
using UCLanTransfer;
//
namespace UCNhdUdpArray
{
  public partial class CUCNhdUdpArrayClient : UserControl
  {
    //
    //---------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------
    //
    private DOnActivateDatagram FOnSendParameter;
    private DOnActivateDatagram FOnSendRequest;
    //
    //---------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------
    //
    public CUCNhdUdpArrayClient()
    {
      InitializeComponent();
    }
    //
    //---------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------
    //
    public void SetOnSendParameter(DOnActivateDatagram value)
    {
      FOnSendParameter = value;
    }

    public void SetOnSendRequest(DOnActivateDatagram value)
    {
      FOnSendRequest = value;
    }

    public void SetIpAddressLocal(IPAddress value)
    {
      FUCIpAddressLocal.SetIpAddress(value);
    }
    public void SetIpPortTransmit(UInt16 value)
    {
      FUCIpPortTxd.SetIpPort(value);
    }
    public void SetIpPortReceive(UInt16 value)
    {
      FUCIpPortRxd.SetIpPort(value);
    }

    public void SetIpAddressTarget(IPAddress value)
    {
      FUCIpAddressTarget.SetIpAddress(value);
    }
    //
    //---------------------------------------------------------------
    //  Segment - Event - Menu
    //---------------------------------------------------------------
    //
    private void btnSendParameter_Click(object sender, EventArgs e)
    {
      if (FOnSendParameter is DOnActivateDatagram)
      {
        FOnSendParameter();
      }
    }

    private void btnSendRequest_Click(object sender, EventArgs e)
    {
      if (FOnSendRequest is DOnActivateDatagram)
      {
        FOnSendRequest();
      }
    }

  }
}
