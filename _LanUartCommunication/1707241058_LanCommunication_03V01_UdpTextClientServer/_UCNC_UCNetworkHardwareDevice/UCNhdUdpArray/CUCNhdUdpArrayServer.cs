﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
//
using Network;
using NhdBase;
using UCNhdCommon;
using UCLanTransfer;
//
namespace UCNhdUdpArray
{
  public partial class CUCNhdUdpArrayServer : UserControl
  {
    //
    //---------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------
    //
    private DOnActivateDatagram FOnSendArray2DSmall;
    private DOnActivateDatagram FOnSendArray2DLarge;
    //
    //---------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------
    //
    public CUCNhdUdpArrayServer()
    {
      InitializeComponent();
    }
    //
    //---------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------
    //
    public void SetOnSendArray2DSmall(DOnActivateDatagram value)
    {
      FOnSendArray2DSmall = value;
    }

    public void SetOnSendArray2DLarge(DOnActivateDatagram value)
    {
      FOnSendArray2DLarge = value;
    }

    public void SetIpAddressLocal(IPAddress value)
    {
      FUCIpAddressLocal.SetIpAddress(value);
    }
    public void SetIpPortTransmit(UInt16 value)
    {
      FUCIpPortTxd.SetIpPort(value);
    }
    public void SetIpPortReceive(UInt16 value)
    {
      FUCIpPortRxd.SetIpPort(value);
    }

    public void SetIpAddressTarget(IPAddress value)
    {
      FUCIpAddressTarget.SetIpAddress(value);
    }
    //
    //---------------------------------------------------------------
    //  Segment - Event - Menu
    //---------------------------------------------------------------
    //
    private void btnSendArrayA_Click(object sender, EventArgs e)
    {
      if (FOnSendArray2DSmall is DOnActivateDatagram)
      {
        FOnSendArray2DSmall();
      }
    }

    private void btnSendArrayB_Click(object sender, EventArgs e)
    {
      if (FOnSendArray2DLarge is DOnActivateDatagram)
      {
        FOnSendArray2DLarge();
      }
    }

  }
}
