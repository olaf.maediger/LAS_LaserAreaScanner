﻿namespace UCNhdUdpArray
{
  partial class CUCNhdUdpArrayClient
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.btnSendRequest = new System.Windows.Forms.Button();
      this.btnSendParameter = new System.Windows.Forms.Button();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.FUCIpPortRxd = new UCLanTransfer.CUCIpPort();
      this.FUCIpPortTxd = new UCLanTransfer.CUCIpPort();
      this.FUCIpAddressLocal = new UCLanTransfer.CUCIpAddress();
      this.FUCIpAddressTarget = new UCLanTransfer.CUCIpAddress();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.BackColor = System.Drawing.Color.PapayaWhip;
      this.label1.Dock = System.Windows.Forms.DockStyle.Top;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(695, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "LanUdpClient - UdpArray";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnSendRequest
      // 
      this.btnSendRequest.Location = new System.Drawing.Point(596, 29);
      this.btnSendRequest.Name = "btnSendRequest";
      this.btnSendRequest.Size = new System.Drawing.Size(84, 46);
      this.btnSendRequest.TabIndex = 34;
      this.btnSendRequest.Text = "Send  Request";
      this.btnSendRequest.UseVisualStyleBackColor = true;
      this.btnSendRequest.Click += new System.EventHandler(this.btnSendRequest_Click);
      // 
      // btnSendParameter
      // 
      this.btnSendParameter.Location = new System.Drawing.Point(505, 29);
      this.btnSendParameter.Name = "btnSendParameter";
      this.btnSendParameter.Size = new System.Drawing.Size(84, 46);
      this.btnSendParameter.TabIndex = 33;
      this.btnSendParameter.Text = "Send Parameter";
      this.btnSendParameter.UseVisualStyleBackColor = true;
      this.btnSendParameter.Click += new System.EventHandler(this.btnSendParameter_Click);
      // 
      // label4
      // 
      this.label4.BackColor = System.Drawing.SystemColors.Info;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(498, 21);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(189, 62);
      this.label4.TabIndex = 32;
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label3
      // 
      this.label3.BackColor = System.Drawing.SystemColors.Info;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(314, 21);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(178, 62);
      this.label3.TabIndex = 31;
      this.label3.Text = "Target (Server)";
      this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // label2
      // 
      this.label2.BackColor = System.Drawing.SystemColors.Info;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(8, 21);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(299, 62);
      this.label2.TabIndex = 30;
      this.label2.Text = "                Local (Client)                    Tx               Rx";
      // 
      // FUCIpPortRxd
      // 
      this.FUCIpPortRxd.Enabled = false;
      this.FUCIpPortRxd.IpPort = ((ushort)(2));
      this.FUCIpPortRxd.Location = new System.Drawing.Point(245, 42);
      this.FUCIpPortRxd.Name = "FUCIpPortRxd";
      this.FUCIpPortRxd.Size = new System.Drawing.Size(52, 33);
      this.FUCIpPortRxd.TabIndex = 37;
      // 
      // FUCIpPortTxd
      // 
      this.FUCIpPortTxd.Enabled = false;
      this.FUCIpPortTxd.IpPort = ((ushort)(1));
      this.FUCIpPortTxd.Location = new System.Drawing.Point(186, 42);
      this.FUCIpPortTxd.Name = "FUCIpPortTxd";
      this.FUCIpPortTxd.Size = new System.Drawing.Size(52, 33);
      this.FUCIpPortTxd.TabIndex = 36;
      // 
      // FUCIpAddressLocal
      // 
      this.FUCIpAddressLocal.Enabled = false;
      this.FUCIpAddressLocal.IPA0L = ((byte)(1));
      this.FUCIpAddressLocal.IPA1M = ((byte)(0));
      this.FUCIpAddressLocal.IPA2H = ((byte)(0));
      this.FUCIpAddressLocal.IPA3X = ((byte)(127));
      this.FUCIpAddressLocal.Location = new System.Drawing.Point(19, 42);
      this.FUCIpAddressLocal.Name = "FUCIpAddressLocal";
      this.FUCIpAddressLocal.Size = new System.Drawing.Size(160, 33);
      this.FUCIpAddressLocal.TabIndex = 35;
      // 
      // FUCIpAddressTarget
      // 
      this.FUCIpAddressTarget.Enabled = false;
      this.FUCIpAddressTarget.IPA0L = ((byte)(1));
      this.FUCIpAddressTarget.IPA1M = ((byte)(0));
      this.FUCIpAddressTarget.IPA2H = ((byte)(0));
      this.FUCIpAddressTarget.IPA3X = ((byte)(127));
      this.FUCIpAddressTarget.Location = new System.Drawing.Point(322, 42);
      this.FUCIpAddressTarget.Name = "FUCIpAddressTarget";
      this.FUCIpAddressTarget.Size = new System.Drawing.Size(160, 33);
      this.FUCIpAddressTarget.TabIndex = 38;
      // 
      // CUCNhdUdpArrayClient
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Honeydew;
      this.Controls.Add(this.FUCIpAddressTarget);
      this.Controls.Add(this.FUCIpPortRxd);
      this.Controls.Add(this.FUCIpPortTxd);
      this.Controls.Add(this.FUCIpAddressLocal);
      this.Controls.Add(this.btnSendRequest);
      this.Controls.Add(this.btnSendParameter);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Name = "CUCNhdUdpArrayClient";
      this.Size = new System.Drawing.Size(695, 91);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btnSendRequest;
    private System.Windows.Forms.Button btnSendParameter;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private UCLanTransfer.CUCIpPort FUCIpPortRxd;
    private UCLanTransfer.CUCIpPort FUCIpPortTxd;
    private UCLanTransfer.CUCIpAddress FUCIpAddressLocal;
    private UCLanTransfer.CUCIpAddress FUCIpAddressTarget;
  }
}
