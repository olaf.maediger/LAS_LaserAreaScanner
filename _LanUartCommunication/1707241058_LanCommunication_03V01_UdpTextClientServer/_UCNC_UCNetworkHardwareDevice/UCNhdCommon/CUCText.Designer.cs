﻿namespace UCNhdCommon
{
  partial class CUCText
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblTopBorder = new System.Windows.Forms.Label();
      this.lblUnit = new System.Windows.Forms.Label();
      this.lblHeader = new System.Windows.Forms.Label();
      this.tbxText = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // lblTopBorder
      // 
      this.lblTopBorder.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTopBorder.Location = new System.Drawing.Point(57, 0);
      this.lblTopBorder.Name = "lblTopBorder";
      this.lblTopBorder.Size = new System.Drawing.Size(266, 5);
      this.lblTopBorder.TabIndex = 9;
      // 
      // lblUnit
      // 
      this.lblUnit.Dock = System.Windows.Forms.DockStyle.Right;
      this.lblUnit.Location = new System.Drawing.Point(323, 0);
      this.lblUnit.Name = "lblUnit";
      this.lblUnit.Size = new System.Drawing.Size(8, 27);
      this.lblUnit.TabIndex = 8;
      this.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblHeader
      // 
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(57, 27);
      this.lblHeader.TabIndex = 7;
      this.lblHeader.Text = "text";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // tbxText
      // 
      this.tbxText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbxText.Location = new System.Drawing.Point(57, 5);
      this.tbxText.Name = "tbxText";
      this.tbxText.Size = new System.Drawing.Size(266, 20);
      this.tbxText.TabIndex = 10;
      // 
      // CUCText
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.tbxText);
      this.Controls.Add(this.lblTopBorder);
      this.Controls.Add(this.lblUnit);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCText";
      this.Size = new System.Drawing.Size(331, 27);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblTopBorder;
    private System.Windows.Forms.Label lblUnit;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.TextBox tbxText;
  }
}
