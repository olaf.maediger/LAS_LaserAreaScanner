﻿namespace UCNhdCommon
{
  partial class CUCTime
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.nudValue = new System.Windows.Forms.NumericUpDown();
      this.lblTopBorder = new System.Windows.Forms.Label();
      this.lblUnit = new System.Windows.Forms.Label();
      this.lblHeader = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudValue)).BeginInit();
      this.SuspendLayout();
      // 
      // nudValue
      // 
      this.nudValue.DecimalPlaces = 3;
      this.nudValue.Dock = System.Windows.Forms.DockStyle.Top;
      this.nudValue.Location = new System.Drawing.Point(74, 5);
      this.nudValue.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
      this.nudValue.Name = "nudValue";
      this.nudValue.Size = new System.Drawing.Size(56, 20);
      this.nudValue.TabIndex = 14;
      this.nudValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudValue.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
      this.nudValue.ValueChanged += new System.EventHandler(this.nudValue_ValueChanged);
      // 
      // lblTopBorder
      // 
      this.lblTopBorder.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTopBorder.Location = new System.Drawing.Point(74, 0);
      this.lblTopBorder.Name = "lblTopBorder";
      this.lblTopBorder.Size = new System.Drawing.Size(56, 5);
      this.lblTopBorder.TabIndex = 13;
      // 
      // lblUnit
      // 
      this.lblUnit.Dock = System.Windows.Forms.DockStyle.Right;
      this.lblUnit.Location = new System.Drawing.Point(130, 0);
      this.lblUnit.Name = "lblUnit";
      this.lblUnit.Size = new System.Drawing.Size(22, 26);
      this.lblUnit.TabIndex = 12;
      this.lblUnit.Text = "s";
      this.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblHeader
      // 
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(74, 26);
      this.lblHeader.TabIndex = 11;
      this.lblHeader.Text = "time";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCTime
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.nudValue);
      this.Controls.Add(this.lblTopBorder);
      this.Controls.Add(this.lblUnit);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCTime";
      this.Size = new System.Drawing.Size(152, 26);
      ((System.ComponentModel.ISupportInitialize)(this.nudValue)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.NumericUpDown nudValue;
    private System.Windows.Forms.Label lblTopBorder;
    private System.Windows.Forms.Label lblUnit;
    private System.Windows.Forms.Label lblHeader;
  }
}
