﻿namespace UCNhdCommon
{
  partial class CUCVariationKind
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblTopBorder = new System.Windows.Forms.Label();
      this.lblUnit = new System.Windows.Forms.Label();
      this.lblHeader = new System.Windows.Forms.Label();
      this.cbxVariationKind = new System.Windows.Forms.ComboBox();
      this.SuspendLayout();
      // 
      // lblTopBorder
      // 
      this.lblTopBorder.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTopBorder.Location = new System.Drawing.Point(52, 0);
      this.lblTopBorder.Name = "lblTopBorder";
      this.lblTopBorder.Size = new System.Drawing.Size(90, 3);
      this.lblTopBorder.TabIndex = 22;
      // 
      // lblUnit
      // 
      this.lblUnit.Dock = System.Windows.Forms.DockStyle.Right;
      this.lblUnit.Location = new System.Drawing.Point(142, 0);
      this.lblUnit.Name = "lblUnit";
      this.lblUnit.Size = new System.Drawing.Size(10, 26);
      this.lblUnit.TabIndex = 21;
      this.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblHeader
      // 
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(52, 26);
      this.lblHeader.TabIndex = 20;
      this.lblHeader.Text = "variation";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // cbxVariationKind
      // 
      this.cbxVariationKind.Dock = System.Windows.Forms.DockStyle.Top;
      this.cbxVariationKind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxVariationKind.FormattingEnabled = true;
      this.cbxVariationKind.Items.AddRange(new object[] {
            "Constant",
            "Linear",
            "Sinus"});
      this.cbxVariationKind.Location = new System.Drawing.Point(52, 3);
      this.cbxVariationKind.Name = "cbxVariationKind";
      this.cbxVariationKind.Size = new System.Drawing.Size(90, 21);
      this.cbxVariationKind.TabIndex = 25;
      this.cbxVariationKind.SelectedIndexChanged += new System.EventHandler(this.cbxVariationKind_SelectedIndexChanged);
      // 
      // CUCVariationKind
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.cbxVariationKind);
      this.Controls.Add(this.lblTopBorder);
      this.Controls.Add(this.lblUnit);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCVariationKind";
      this.Size = new System.Drawing.Size(152, 26);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblTopBorder;
    private System.Windows.Forms.Label lblUnit;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.ComboBox cbxVariationKind;

  }
}
