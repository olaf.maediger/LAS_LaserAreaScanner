﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCNhdCommon
{
  //
  //--------------------------------------------------------------------------
  //  Segment - Type
  //--------------------------------------------------------------------------
  //
  public enum EVariationKind
  {
    Constant = 0,
    Linear = 1,
    Sinus = 2
  };

  public static class CVariationKind
  {
    public static EVariationKind TextToVariationKind(String text)
    {
      switch (text)
      {
        case "Constant":
          return EVariationKind.Constant;
        case "Linear":
          return EVariationKind.Linear;
        case "Sinus":
          return EVariationKind.Sinus;
      }
      return EVariationKind.Constant;
    }

    public static String VariationKindToText(EVariationKind variationkind)
    {
      switch (variationkind)
      {
        case EVariationKind.Constant:
          return "Constant";
        case EVariationKind.Linear:
          return "Linear";
        case EVariationKind.Sinus:
          return "Sinus";
      }
      return "Constant";
    }

  }
}
