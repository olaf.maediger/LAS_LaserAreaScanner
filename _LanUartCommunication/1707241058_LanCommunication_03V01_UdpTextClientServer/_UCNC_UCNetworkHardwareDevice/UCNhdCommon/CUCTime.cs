﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCNhdCommon
{
  public delegate void DOnTimeChanged(Double time);

  public partial class CUCTime : UserControl
  {
    //
    //------------------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------------------
    //
    private DOnTimeChanged FOnTimeChanged;
    //
    //------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------
    //
    public CUCTime()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------
    //
    public void SetOnTimeChanged(DOnTimeChanged value)
    {
      FOnTimeChanged = value;
    }

    public Double Value
    {
      get { return (Double)nudValue.Value; }
      set { nudValue.Value = (Decimal)value; }
    }

    public String Unit
    {
      get { return lblUnit.Text; }
      set { lblUnit.Text = value; }
    }

    public Int32 TopBorderHeight
    {
      get { return lblTopBorder.Height; }
      set { lblTopBorder.Height = value; }
    }

    public Int32 HeaderWidth
    {
      get { return lblHeader.Width; }
      set { lblHeader.Width = value; }
    }
    public String HeaderText
    {
      get { return lblHeader.Text; }
      set { lblHeader.Text = value; }
    }

    private void nudValue_ValueChanged(object sender, EventArgs e)
    {
      if (FOnTimeChanged is DOnTimeChanged)
      {
        FOnTimeChanged((Double)nudValue.Value);
      }
    }


  }
}
