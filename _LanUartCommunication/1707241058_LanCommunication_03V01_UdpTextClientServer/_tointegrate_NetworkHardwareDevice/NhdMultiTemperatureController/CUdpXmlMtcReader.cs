﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using UCNotifier;
using Xml;
using UdpTransfer;
using UdpXmlTransfer;
//
namespace NhdMultiTemperatureController
{
  public class CUdpXmlMtcReader : CUdpXmlReader
  {
    //
    //--------------------------------------------
    //	Segment - Constant
    //--------------------------------------------
    //	
    //
    //--------------------------------------------
    //	Segment - Field
    //--------------------------------------------
    //	
    // !!! private RRequestGetProperties FRequestGetProperties;
    // !!! private RRequestGetMeasurement FRequestGetMeasurement;
    //
    //--------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------
    //	
    public CUdpXmlMtcReader()
      : base()
    {

    }
    //
    //--------------------------------------------
    //	Segment - Property
    //--------------------------------------------
    //	
    // !!! 
    //public RRequestGetProperties GetRequestGetProperties()
    //{
    //  return FRequestGetProperties;
    //}

    // !!! 
    //public RRequestGetMeasurement GetRequestGetMeasurement()
    //{
    //  return FRequestGetMeasurement;
    //}




















    //
    //--------------------------------------------
    //	Segment - Reader - Part
    //--------------------------------------------
    //	
    //public Boolean AnalyseIPAddress(XmlNode nodebase, out RIPAddress[] ipaddress)
    //{
    //  Boolean Result = true;
    //  ipaddress = new RIPAddress[2];
    //  String Line = String.Format("<{0}>", nodebase.Name); // IPAddress
    //  IPAddress IPA;
    //  foreach (XmlNode NodeChild in nodebase.ChildNodes)
    //  {
    //    String Name = NodeChild.Name;
    //    switch (Name)
    //    {
    //      case CUdpXmlHeader.XML_SOURCE:
    //        if (CUdpBase.TextToIPAddress(NodeChild.InnerText, out IPA))
    //        {
    //          ipaddress[0].Type = CUdpXmlHeader.XML_SOURCE;
    //          ipaddress[0].Value = IPA.GetAddressBytes();
    //        }
    //        else
    //        {
    //          Result = false;
    //        }
    //        break;
    //      case CUdpXmlHeader.XML_TARGET:
    //        if (CUdpBase.TextToIPAddress(NodeChild.InnerText, out IPA))
    //        {
    //          ipaddress[1].Type = CUdpXmlHeader.XML_TARGET;
    //          ipaddress[1].Value = IPA.GetAddressBytes();
    //        }
    //        else
    //        {
    //          Result = false;
    //        }
    //        break;
    //      default:
    //        Result = false;
    //        break;
    //    }
    //  }
    //  return Result;
    //}

    //public Boolean AnalyseIPPort(XmlNode nodebase, out RIPPort[] ipport)
    //{
    //  Boolean Result = true;
    //  ipport = new RIPPort[2];
    //  String Line = String.Format("<{0}>", nodebase.Name); // IPPort
    //  UInt16 IPP;
    //  foreach (XmlNode NodeChild in nodebase.ChildNodes)
    //  {
    //    String Name = NodeChild.Name;
    //    switch (Name)
    //    {
    //      case CUdpXmlHeader.XML_FROM:
    //        if (CUdpBase.TextToIPPort(NodeChild.InnerText, out IPP))
    //        {
    //          ipport[0].Type = CUdpXmlHeader.XML_FROM;
    //          ipport[0].Value = IPP;
    //        }
    //        else
    //        {
    //          Result = false;
    //        }
    //        break;
    //      case CUdpXmlHeader.XML_BACK:
    //        if (CUdpBase.TextToIPPort(NodeChild.InnerText, out IPP))
    //        {
    //          ipport[1].Type = CUdpXmlHeader.XML_BACK;
    //          ipport[1].Value = IPP;
    //        }
    //        else
    //        {
    //          Result = false;
    //        }
    //        break;
    //      default:
    //        Result = false;
    //        break;
    //    }
    //  }
    //  return Result;
    //}


    public Boolean AnalyseRequestGetProperties(XmlNode nodebase, out RRequestGetProperties data)
    {
      Boolean Result = true;
      String Line = String.Format("<{0}>", nodebase.Name);
      Write(Line);
      WriteIncrement();
      data = new RRequestGetProperties();
      foreach (XmlNode NodeChild in nodebase.ChildNodes)
      {
        String Name = NodeChild.Name;
        switch (Name)
        {
          case CUdpXmlHeader.XML_IPADDRESS:
            //RIPAddress[] IPAddress;
            //if (AnalyseIPAddress(NodeChild, out IPAddress))
            //{
            //  data.IPAddress = IPAddress;
            //}
            //else
            {
              Result = false;
            }
            break;
          case CUdpXmlHeader.XML_IPPORT:
            //RIPPort[] IPPort;
            //if (AnalyseIPPort(NodeChild, out IPPort))
            //{
            //  data.IPPort = IPPort;
            //}
            //else
            {
              Result = false;
            }
            break;
          default:
            Result &= false;
            break;
        }
      }
      WriteDecrement();
      return false;
    }

    public Boolean AnalyseRequestGetMeasurement(XmlNode nodebase, out RRequestGetMeasurement data)
    {
      Boolean Result = true;
      String Line = String.Format("<{0}>", nodebase.Name);
      Write(Line);
      WriteIncrement();
      data = new RRequestGetMeasurement();
      foreach (XmlNode NodeChild in nodebase.ChildNodes)
      {
        String Name = NodeChild.Name;
        switch (Name)
        {
          case CUdpXmlHeader.XML_IPADDRESS:
            //RIPAddress[] IPAddress;
            //if (AnalyseIPAddress(NodeChild, out IPAddress))
            //{
            //  data.IPAddress = IPAddress;
            //}
            //else
            {
              Result = false;
            }
            break;
          case CUdpXmlHeader.XML_IPPORT:
            //RIPPort[] IPPort;
            //if (AnalyseIPPort(NodeChild, out IPPort))
            //{
            //  data.IPPort = IPPort;
            //}
            //else
            {
              Result = false;
            }
            break;
          default:
            Result &= false;
            break;
        }
      }
      WriteDecrement();
      return false;
    }







    ////////public override Boolean AnalyseAllRequests(XmlNode nodebase)
    ////////{
    ////////  Boolean Result = false;
    ////////  String Line = String.Format("<{0}>", nodebase.Name);
    ////////  Write(Line);
    ////////  WriteIncrement();
    ////////  Int32 CA = nodebase.Attributes.Count;
    ////////  if (0 < CA)
    ////////  {
    ////////    String AN = nodebase.Attributes[0].Name;
    ////////    String AV = nodebase.Attributes[0].Value;
    ////////    if (CUdpXmlHeader.XML_TYPE == AN)
    ////////    {
    ////////      switch (AV)
    ////////      {
    ////////        case CUdpXmlMtcHeader.XML_GETPROPERTIES:
    ////////          Result &= AnalyseRequestGetProperties(nodebase, out FRequestGetProperties);
    ////////          break;
    ////////        case CUdpXmlMtcHeader.XML_GETMEASUREMENT:
    ////////          Result &= AnalyseRequestGetMeasurement(nodebase, out FRequestGetMeasurement);
    ////////          break;
    ////////      }
    ////////    }
    ////////  }
    ////////  WriteDecrement();
    ////////  return Result;
    ////////}

    //
    //--------------------------------------------
    //	Segment - Reader - Command
    //--------------------------------------------
    //	
    ////////public override Boolean AnalyseUserMeasurementProtocol(String text)
    ////////{
    ////////  try
    ////////  {
    ////////    if (!base.AnalyseUserMeasurementProtocol(text))
    ////////    {
    ////////      Boolean Result = true;
    ////////      CXmlReader XMLReader = new CXmlReader();
    ////////      XMLReader.SetNotifier(FNotifier);
    ////////      XMLReader.EnableProtocol(true);
    ////////      // Root
    ////////      XmlDocument XMLDocument = new XmlDocument();
    ////////      XMLDocument.InnerXml = text;
    ////////      //
    ////////      XmlNode XMLNodeBase = XMLDocument.SelectSingleNode(CUdpXmlMtcHeader.XML_USERMEASUREMENTPROTOCOL);
    ////////      String Line = String.Format("<{0}>", XMLNodeBase.LocalName);
    ////////      Write(Line);
    ////////      WriteIncrement();
    ////////      //
    ////////      foreach (XmlNode NodeChild in XMLNodeBase.ChildNodes)
    ////////      { // debug Console.WriteLine("----> {0}", NodeChild.Name);
    ////////        switch (NodeChild.Name)
    ////////        {
    ////////          case CUdpXmlMtcHeader.XML_REQUEST:
    ////////            Result &= AnalyseAllRequests(NodeChild);
    ////////            break;
    ////////          case CUdpXmlMtcHeader.XML_COMMAND:
    ////////            //Result &= AnalyseAllCommands(NodeChild);
    ////////            break;
    ////////          case CUdpXmlMtcHeader.XML_RESPONSE:
    ////////            //Result &= AnalyseAllResponses(NodeChild);
    ////////            break;
    ////////        }
    ////////      }
    ////////      WriteDecrement();
    ////////      return true;
    ////////    }
    ////////    return false;
    ////////  }
    ////////  catch (Exception)
    ////////  {
    ////////    FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot analyse UserMeasurementProtocol");
    ////////    return false;
    ////////  }
    ////////}



    
  }
}
