﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using UCNotifier;
using Xml;
using UdpTransfer;
using UdpXmlTransfer;
//
namespace NhdMultiTemperatureController
{

  public struct RRequestGetProperties
  {
  }

  public struct RRequestGetMeasurement
  {
  }



  public class CUdpXmlMtcHeader : CUdpXmlHeader
  { //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //
    public new const String INIT_HEADER = "NhdMultiTemperatureController";
    //
    public const String XML_DEVICENAME = "MultiTemperatureController";
    public const String XML_DEVICETYPE = "MTC";
    //
    // Command
    public const String XML_GETDATA = "GetData";
    public const String XML_SETDATA = "SetData";
    public const String XML_START = "Start";
    public const String XML_STOP = "Stop";
    // Parameter
    public const String XML_CHANNEL = "Channel";
    public const String XML_TEMPERATUREMEAN = "TemperatureMean";
    public const String XML_TEMPERATUREDELTA = "TemperatureDelta";
    public const String XML_TEMPERATUREACTUAL = "TemperatureActual";
    public const String XML_TIMEPERIOD = "TimePeriod";
    public const String XML_TIMEDELTA = "TimeDelta";
    public const String XML_TIMEACTUAL = "TimeActual";
    public const String XML_VARIATIONKIND = "VariationKind";
    public const String XML_STEPPRESET = "StepPreset";
    public const String XML_STEPACTUAL = "StepActual";
    //
    //--------------------------------------------
    //	Segment - Field
    //--------------------------------------------
    //	

    //
    //--------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------
    //	

  }
}


//public const String XML_SETPROPERTIES = "SetProperties";
//public const String XML_GETPROPERTIES = "GetProperties";
//public const String XML_PRESETMEASUREMENT = "PresetMeasurement";
//public const String XML_EXECUTEMEASUREMENT = "ExecuteMeasurement";
//public const String XML_DATE = "Date";
//public const String XML_TIME = "Time";
// Data
//public const String XML_YEAR = "Year";
//public const String XML_MONTH = "Month";
//public const String XML_DAY = "Day";
//public const String XML_HOUR = "Hour";
//public const String XML_MINUTE = "Minute";
//public const String XML_SECOND = "Second";

