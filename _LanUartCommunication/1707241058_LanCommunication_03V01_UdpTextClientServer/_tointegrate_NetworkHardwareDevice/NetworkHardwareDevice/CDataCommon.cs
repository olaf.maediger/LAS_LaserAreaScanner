﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using UCNotifier;
//
namespace NetworkHardwareDevice
{
  public class CDeviceParameter
  {
    public const String INIT_DEVICENAME = "<devicename>";
    public const String INIT_DEVICETYPE = "<devicetype>";

    private String FName;
    private String FType;
    private Guid FID;
    private IPAddress FIPAddress;
    private Boolean FRequestFindAllDevicesExternal;

    public CDeviceParameter()
    {
      FName = INIT_DEVICENAME;
      FType = INIT_DEVICETYPE;
      FID = Guid.NewGuid();
      FRequestFindAllDevicesExternal = false;
    }

    public CDeviceParameter(String devicename,
                            String devicetype,
                            Guid deviceid,
                            IPAddress deviceipaddress)
    {
      FName = devicename;
      FType = devicetype;
      FID = deviceid;
      FIPAddress = deviceipaddress;
      FRequestFindAllDevicesExternal = false;
    }

    public CDeviceParameter(CDeviceParameter deviceparameter)
    {
      FName = deviceparameter.Name;
      FType = deviceparameter.Type;
      FID = deviceparameter.ID;
      FIPAddress = deviceparameter.IPA;
      FRequestFindAllDevicesExternal = deviceparameter.RequestFindAllDevicesExternal;
    }

    private String GetName()
    {
      return FName;
    }
    private void SetName(String value)
    {
      FName = value;
    }
    public String Name
    {
      get { return GetName(); }
      set { SetName(value); }
    }

    private String GetTType()
    {
      return FType;
    }
    private void SetType(String value)
    {
      FType = value;
    }
    public String Type
    {
      get { return GetTType(); }
      set { SetType(value); }
    }

    private Guid GetID()
    {
      return FID;
    }
    private void SetID(Guid value)
    {
      FID = value;
    }
    public Guid ID
    {
      get { return GetID(); }
      set { SetID(value); }
    }

    private IPAddress GetIPAddress()
    {
      return FIPAddress;
    }
    private void SetIPAddress(IPAddress value)
    {
      FIPAddress = value;
    }
    public IPAddress IPA
    {
      get { return GetIPAddress(); }
      set { SetIPAddress(value); }
    }

    private Boolean GetRequestFindAllDevicesExternal()
    {
      return FRequestFindAllDevicesExternal;
    }
    private void SetRequestFindAllDevicesExternal(Boolean value)
    {
      FRequestFindAllDevicesExternal = value;
    }
    public Boolean RequestFindAllDevicesExternal
    {
      get { return GetRequestFindAllDevicesExternal(); }
      set { SetRequestFindAllDevicesExternal(value); }
    }

    public void Info(CNotifier notifier)
    {
      notifier.Write(String.Format("DeviceParameter[{0}]:", Name));
      notifier.Write(String.Format("  Type: {0}", Type));
      notifier.Write(String.Format("  ID  : {0}", ID));
      notifier.Write(String.Format("  IPAddress: {0}", IPA));
      notifier.Write(String.Format("  RequestFADExternal: {0}", RequestFindAllDevicesExternal));
    }
  }

  public class CDeviceParameterList : List<CDeviceParameter>
  {
    private String FName;

    public CDeviceParameterList(String name)
    {
      FName = name;
    }

    public String Name
    {
      get { return FName; }
    }

    public void Info(CNotifier notifier)
    {
      String Line = "";
      if (0 < this.Count)
      {
        Line = String.Format("*** DeviceParameterList[{0}] ({1} element(s)):", FName, this.Count);
        notifier.Write(Line);
        foreach (CDeviceParameter DP in this)
        {
          DP.Info(notifier);
        }
      }
      else
      {
        Line = String.Format("*** DeviceParameterList[{0}] - no elements!", FName);
        notifier.Write(Line);
      }
    }
  }

}
