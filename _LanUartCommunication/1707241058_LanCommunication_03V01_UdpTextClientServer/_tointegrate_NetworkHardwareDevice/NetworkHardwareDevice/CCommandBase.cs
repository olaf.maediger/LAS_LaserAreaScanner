﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
//
using UCNotifier;
using Task;
using Xml;
using UdpTransfer;
using UdpXmlTransfer;
//
namespace NetworkHardwareDevice
{
  // NC public delegate void DOnDataRequestReceived(CDataRequest datarequest);
  public delegate void DOnDataCommandReceived(CDataCommand datacommand);
  public delegate void DOnDataResponseReceived(CDataResponse dataresponse);

  public abstract class CCommandBase
  {
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    protected CNotifier FNotifier;
    //
    protected CTask FTask;
    //
    protected CUdpTransmitter FUdpTransmitter;
    protected CUdpReceiver FUdpReceiver;
    //
    protected CEvent FEventTransmitted;
    protected CEvent FEventReceived;
    //
    protected DOnExecutionStart FOnExecutionStart;
    protected DOnExecutionBusy FOnExecutionBusy;
    protected DOnExecutionEnd FOnExecutionEnd;
    protected DOnExecutionAbort FOnExecutionAbort;
    //
    protected DOnExecutionStart FParentOnExecutionStart;
    protected DOnExecutionBusy FParentOnExecutionBusy;
    protected DOnExecutionEnd FParentOnExecutionEnd;
    protected DOnExecutionAbort FParentOnExecutionAbort;
    //
    protected CXmlReader FXMLReader;
    protected CXmlWriter FXMLWriter;
    //
    // NC protected DOnDataRequestReceived FOnDataRequestReceived;
    protected DOnDataResponseReceived FOnDataResponseReceived;
    protected DOnDataCommandReceived FOnDataCommandReceived;
    //
    //------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------
    //	 
    public CCommandBase()
    {
      FNotifier = null;
      FTask = null;
      FUdpTransmitter = null;
      FUdpReceiver = null;
      // NC FOnDataRequestReceived = null;
      FOnDataResponseReceived = null;
      FOnDataCommandReceived = null;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    //public void SetOnDataRequestReceived(DOnDataRequestReceived value)
    //{
    //  FOnDataRequestReceived = value;
    //}

    public void SetOnDataResponseReceived(DOnDataResponseReceived value)
    {
      FOnDataResponseReceived = value;
    }

    public void SetOnDataCommandReceived(DOnDataCommandReceived value)
    {
      FOnDataCommandReceived = value;
    }

    public Boolean IsUdpTransmitterOpen()
    {
      return (FUdpTransmitter is CUdpTransmitter);
    }

    public Boolean IsUdpReceiverOpen()
    {
      return (FUdpReceiver is CUdpReceiver);
    }

    private Boolean GetIsActive()
    {
      if (FTask is CTask)
      {
        return FTask.IsActive();
      }
      return false;
    }
    public Boolean IsActive
    {
      get { return GetIsActive(); }
    }

    private Boolean GetIsFinished()
    {
      if (FTask is CTask)
      {
        return FTask.IsFinished();
      }
      return false;
    }
    public Boolean IsFinished
    {
      get { return GetIsFinished(); }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Callback - Task
    //---------------------------------------------------------------------------
    //
    private void TaskOnExecutionStart(RTaskData data)
    {
      if (FOnExecutionStart is DOnExecutionStart)
      {
        FOnExecutionStart(data);
      }
    }
    private Boolean TaskOnExecutionBusy(ref RTaskData data)
    {
      if (FOnExecutionBusy is DOnExecutionBusy)
      {
        return FOnExecutionBusy(ref data);
      }
      return false;
    }
    private void TaskOnExecutionEnd(RTaskData data)
    {
      if (FOnExecutionEnd is DOnExecutionEnd)
      {
        FOnExecutionEnd(data);
      }
    }
    private void TaskOnExecutionAbort(RTaskData data)
    {
      if (FOnExecutionAbort is DOnExecutionAbort)
      {
        FOnExecutionAbort(data);
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Method
    //-------------------------------------------------------------------
    //
    public virtual Boolean Execute(String taskname, 
                                   DOnExecutionStart onexecutonstart,
                                   DOnExecutionBusy onexecutionbusy,
                                   DOnExecutionEnd onexecutionend,
                                   DOnExecutionAbort onexecutionabort)
    {
      try
      {
        // ??? Abort();
        FOnExecutionStart = onexecutonstart;
        FOnExecutionBusy = onexecutionbusy;
        FOnExecutionEnd = onexecutionend;
        FOnExecutionAbort = onexecutionabort;
        FTask = new CTask(taskname, 
                          TaskOnExecutionStart,
                          TaskOnExecutionBusy,
                          TaskOnExecutionEnd,
                          TaskOnExecutionAbort);
        FTask.Start();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public virtual Boolean Abort()
    {
      try
      {
        if (FTask is CTask)
        {
          CTask TT = FTask;
          FTask = null;
          TT.Abort();
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        FTask = null;
        return false;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Helper - Analyse
    //-------------------------------------------------------------------
    //
    public Boolean AnalyseIPAddresses(XmlNode nodebase, out IPAddress[] ipaddresses)
    {
      // debug FNotifier.Write("AnalyseIPAddresses");
      Boolean Result = true;
      ipaddresses = new IPAddress[2];
      // debug String Line = String.Format("<{0}>", nodebase.Name); // IPAddress
      IPAddress IPA;
      foreach (XmlNode NodeChild in nodebase.ChildNodes)
      {
        String Name = NodeChild.Name;
        switch (Name)
        {
          case CUdpXmlHeader.XML_SOURCE:
            if (CUdpBase.TextToIPAddress(NodeChild.InnerText, out IPA))
            {
              ipaddresses[0] = IPA;
            }
            else
            {
              Result = false;
            }
            break;
          case CUdpXmlHeader.XML_TARGET:
            if (CUdpBase.TextToIPAddress(NodeChild.InnerText, out IPA))
            {
              ipaddresses[1] = IPA;
            }
            else
            {
              Result = false;
            }
            break;
          default:
            Result = false;
            break;
        }
      }
      return Result;
    }

    public Boolean AnalyseIPPorts(XmlNode nodebase, out UInt16[] ipports)
    {
      // debug FNotifier.Write("AnalyseIPPorts");
      Boolean Result = true;
      ipports = new UInt16[2];
      // debug String Line = String.Format("<{0}>", nodebase.Name); // IPPort
      UInt16 IPP;
      foreach (XmlNode NodeChild in nodebase.ChildNodes)
      {
        String Name = NodeChild.Name;
        switch (Name)
        {
          case CUdpXmlHeader.XML_FROM:
            if (CUdpBase.TextToIPPort(NodeChild.InnerText, out IPP))
            {
              ipports[0] = IPP;
            }
            else
            {
              Result = false;
            }
            break;
          case CUdpXmlHeader.XML_BACK:
            if (CUdpBase.TextToIPPort(NodeChild.InnerText, out IPP))
            {
              ipports[1] = IPP;
            }
            else
            {
              Result = false;
            }
            break;
          default:
            Result = false;
            break;
        }
      }
      return Result;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Helper - Analyse - RxdCommand / RxdRequest
    //-------------------------------------------------------------------
    // 
    public Boolean AnalyseParameterList(XmlNode nodebase,
                                        out CParameterList parameterlist)
    {
      // debug FNotifier.Write("AnalyseParameterList");
      Boolean Result = true;
      // debug String Line = String.Format("<{0}>", nodebase.Name);
      // debug FXMLReader.Write(Line);
      FXMLReader.WriteIncrement();
      parameterlist = new CParameterList();
      foreach (XmlNode NodeChild in nodebase.ChildNodes)
      {
        parameterlist.Add(NodeChild.Name, NodeChild.InnerText);
      }
      FXMLReader.WriteDecrement();
      return Result;
    }






































    //public Boolean AnalyseDeviceParameter(XmlNode nodebase, out CDeviceParameter deviceparameter)
    //{
    //  FNotifier.Write("AnalyseDeviceParameter"); 
    //  Boolean Result = true;
    //  deviceparameter = new CDeviceParameter();
    //  // debug String Line = String.Format("<{0}>", nodebase.Name); // Device
    //  // debug FNotifier.Write(Line);
    //  CXmlReader XR = new CXmlReader();
    //  foreach (XmlNode NodeChild in nodebase.ChildNodes)
    //  {
    //    String Name = NodeChild.Name;
    //    switch (Name)
    //    {
    //      case CUdpXmlHeader.XML_NAME:
    //        deviceparameter.Name = NodeChild.InnerText;
    //        break;
    //      case CUdpXmlHeader.XML_TYPE:
    //        deviceparameter.Type = NodeChild.InnerText;
    //        break;
    //      case CUdpXmlHeader.XML_GUID:
    //        Guid DPID;
    //        Result &= XR.BuildGuid(NodeChild.InnerText, out DPID);
    //        deviceparameter.ID = Guid.Parse(NodeChild.InnerText);
    //        break;
    //      case CUdpXmlHeader.XML_IPADDRESS:
    //        IPAddress DPIPA;
    //        Result &= CUdpBase.TextToIPAddress(NodeChild.InnerText, out DPIPA);
    //        if (Result)
    //        {
    //          deviceparameter.IPAddress = DPIPA;
    //        }
    //        break;
    //      default:
    //        Result = false;
    //        break;
    //    }
    //  }
    //  return Result;
    //}
    ////
    ////-------------------------------------------------------------------
    ////  Segment - Helper - Build - TxdRequestFindAllDevices
    ////-------------------------------------------------------------------
    //// 
    //public Boolean BuildTxdRequestFindAllDevices(CDataRequestFindAllDevices data, out String text)
    //{
    //  text = "";
    //  try
    //  {
    //    FNotifier.Write("BuildTxdRequestFindAllDevices");
    //    CXmlWriter XMLWriter = new CXmlWriter();
    //    XMLWriter.SetNotifier(FNotifier);
    //    XMLWriter.EnableProtocol(false);//true);
    //    // Root
    //    XmlDocument XMLDocument = new XmlDocument();
    //    XmlNode XMLNodeBase;
    //    XMLWriter.AddNode(0, XMLDocument, null, out XMLNodeBase, CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
    //    // Request
    //    XmlElement XMLNodeRequest;
    //    XMLWriter.AddNodeAttribute(1, XMLDocument, XMLNodeBase, out XMLNodeRequest,
    //                               CUdpXmlHeader.XML_REQUEST, CUdpXmlHeader.XML_TYPE, CUdpXmlHeader.XML_FINDALLDEVICES);
    //    // IPAddress
    //    XmlNode XMLNodeIPAddress;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeRequest, out XMLNodeIPAddress, CUdpXmlHeader.XML_IPADDRESS);
    //    XmlNode XMLNodeSource;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeSource,
    //                      CUdpXmlHeader.XML_SOURCE, data.IPAddressSource.ToString());
    //    XmlNode XMLNodeTarget;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeTarget,
    //                      CUdpXmlHeader.XML_TARGET, data.IPAddressTarget.ToString());
    //    // IPPort
    //    XmlNode XMLNodeIPPort;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeRequest, out XMLNodeIPPort, CUdpXmlHeader.XML_IPPORT);
    //    XmlNode XMLNodeFrom;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeFrom,
    //                      CUdpXmlHeader.XML_FROM, data.IPPortFrom.ToString());
    //    XmlNode XMLNodeBack;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeBack,
    //                      CUdpXmlHeader.XML_BACK, data.IPPortBack.ToString());
    //    //
    //    text = XMLDocument.InnerXml;
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot build TxdRequestFindAllDevices");
    //    return false;
    //  }
    //}
    ////
    ////-------------------------------------------------------------------------------
    ////  Segment - Helper - Analyse - RxdRequestFindAllDevices
    ////-------------------------------------------------------------------------------
    ////
    //public Boolean AnalyseRxdRequestFindAllDevices(XmlNode nodebase, out CDataRequestFindAllDevices data)
    //{
    //  FNotifier.Write("AnalyseRxdRequestFindAllDevices");
    //  Boolean Result = true;
    //  // debug String Line = String.Format("<{0}>", nodebase.Name);
    //  // debug FXMLReader.Write(Line);
    //  FXMLReader.WriteIncrement();
    //  data = new CDataRequestFindAllDevices();
    //  foreach (XmlNode NodeChild in nodebase.ChildNodes)
    //  {
    //    String Name = NodeChild.Name;
    //    switch (Name)
    //    {
    //      case CUdpXmlHeader.XML_IPADDRESS:
    //        IPAddress[] IPAddresses;
    //        if (AnalyseIPAddresses(NodeChild, out IPAddresses))
    //        {
    //          data.IPAddressSource = IPAddresses[0];
    //          data.IPAddressTarget = IPAddresses[1];
    //        }
    //        else
    //        {
    //          Result = false;
    //        }
    //        break;
    //      case CUdpXmlHeader.XML_IPPORT:
    //        UInt16[] IPPorts;
    //        if (AnalyseIPPorts(NodeChild, out IPPorts))
    //        {
    //          data.IPPortFrom = IPPorts[0];
    //          data.IPPortBack = IPPorts[1];
    //        }
    //        else
    //        {
    //          Result = false;
    //        }
    //        break;
    //      default:
    //        Result &= false;
    //        break;
    //    }
    //  }
    //  FXMLReader.WriteDecrement();
    //  return Result;
    //}

    //public Boolean AnalyseRxdRequestFindAllDevices(String xmltext, out CDataRequestFindAllDevices data)
    //{
    //  FNotifier.Write("AnalyseRxdRequestFindAllDevices");
    //  data = new CDataRequestFindAllDevices();
    //  try
    //  {
    //    Boolean Result = true;
    //    FXMLReader = new CXmlReader();
    //    FXMLReader.SetNotifier(FNotifier);
    //    FXMLReader.EnableProtocol(false);//true);
    //    // Root
    //    XmlDocument XMLDocument = new XmlDocument();
    //    XMLDocument.InnerXml = xmltext;
    //    //
    //    XmlNode XMLNodeBase = XMLDocument.SelectSingleNode(CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
    //    if (XMLNodeBase is XmlNode)
    //    {
    //      // debug String Line = String.Format("<{0}>", XMLNodeBase.LocalName);
    //      // debug FXMLReader.Write(Line);
    //      FXMLReader.WriteIncrement();
    //      //
    //      foreach (XmlNode NodeChild in XMLNodeBase.ChildNodes)
    //      { // debug Console.WriteLine("----> {0}", NodeChild.Name);
    //        switch (NodeChild.Name)
    //        {
    //          case CUdpXmlHeader.XML_REQUEST:
    //            Int32 CA = NodeChild.Attributes.Count;
    //            if (0 < CA)
    //            {
    //              String AN = NodeChild.Attributes[0].Name;
    //              String AV = NodeChild.Attributes[0].Value;
    //              if (CUdpXmlHeader.XML_TYPE == AN)
    //              {
    //                if (CUdpXmlHeader.XML_FINDALLDEVICES == AV)
    //                {
    //                  Result &= AnalyseRxdRequestFindAllDevices(NodeChild, out data);
    //                }
    //              }
    //            }
    //            break;
    //        }
    //      }
    //      FXMLReader.WriteDecrement();
    //      return Result;
    //    }
    //    return false;
    //  }
    //  catch (Exception)
    //  {
    //    FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot analyse ....");
    //    return false;
    //  }
    //}
    //
    //-------------------------------------------------------------------
    //  Segment - Helper - Build - TxdResponseFindAllDevices
    //-------------------------------------------------------------------
    // 
    //public Boolean BuildTxdResponseFindAllDevices(CDataResponseFindAllDevices data,
    //                                              out String text)
    //{
    //  FNotifier.Write("BuildTxdResponseFindAllDevices");
    //  text = "";
    //  try
    //  {
    //    CXmlWriter XMLWriter = new CXmlWriter();
    //    XMLWriter.SetNotifier(FNotifier);
    //    XMLWriter.EnableProtocol(false);//true);
    //    // Root
    //    XmlDocument XMLDocument = new XmlDocument();
    //    XmlNode XMLNodeBase;
    //    XMLWriter.AddNode(0, XMLDocument, null, out XMLNodeBase, CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
    //    // Response
    //    XmlElement XMLNodeResponse;
    //    XMLWriter.AddNodeAttribute(1, XMLDocument, XMLNodeBase, out XMLNodeResponse,
    //                               CUdpXmlHeader.XML_RESPONSE, CUdpXmlHeader.XML_TYPE, CUdpXmlHeader.XML_FINDALLDEVICES);
    //    // IPAddress
    //    XmlNode XMLNodeIPAddress;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeResponse, out XMLNodeIPAddress, CUdpXmlHeader.XML_IPADDRESS);
    //    XmlNode XMLNodeSource;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeSource,
    //                      CUdpXmlHeader.XML_SOURCE, data.IPAddressSource.ToString());
    //    XmlNode XMLNodeTarget;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeTarget,
    //                      CUdpXmlHeader.XML_TARGET, data.IPAddressTarget.ToString());
    //    // IPPort
    //    XmlNode XMLNodeIPPort;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeResponse, out XMLNodeIPPort, CUdpXmlHeader.XML_IPPORT);
    //    XmlNode XMLNodeFrom;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeFrom,
    //                      CUdpXmlHeader.XML_FROM, data.IPPortFrom.ToString());
    //    // Device
    //    XmlNode XMLNodeDevice;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeResponse, out XMLNodeDevice, CUdpXmlHeader.XML_DEVICE);
    //    XmlNode XMLNodeName;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeDevice, out XMLNodeName,
    //                      CUdpXmlHeader.XML_NAME, data.DeviceParameter.Name);
    //    XmlNode XMLNodeType;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeDevice, out XMLNodeType,
    //                      CUdpXmlHeader.XML_TYPE, data.DeviceParameter.Type);
    //    XmlNode XMLNodeGuid;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeDevice, out XMLNodeGuid,
    //                      CUdpXmlHeader.XML_GUID, String.Format("{0}", data.DeviceParameter.ID.ToString()));
    //    XmlNode XMLNodeIPA;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeDevice, out XMLNodeIPA,
    //                      CUdpXmlHeader.XML_IPADDRESS, data.DeviceParameter.IPAddress.ToString());
    //    //
    //    text = XMLDocument.InnerXml;
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot build ResponseFindAllDevices");
    //    return false;
    //  }
    //}
    //
    //-------------------------------------------------------------------------------
    //  Segment - Helper - Analyse - RxdResponseFindAllDevices
    //-------------------------------------------------------------------------------
    //
    //public Boolean AnalyseRxdResponseFindAllDevices(XmlNode nodebase,
    //                                                out CDataResponseFindAllDevices data)
    //{
    //  FNotifier.Write("AnalyseRxdResponseFindAllDevices");
    //  Boolean Result = true;
    //  // debug String Line = String.Format("<{0}>", nodebase.Name);
    //  // debug FXMLReader.Write(Line);
    //  FXMLReader.WriteIncrement();
    //  data = new CDataResponseFindAllDevices();
    //  foreach (XmlNode NodeChild in nodebase.ChildNodes)
    //  {
    //    String Name = NodeChild.Name;
    //    switch (Name)
    //    {
    //      case CUdpXmlHeader.XML_IPADDRESS:
    //        IPAddress[] IPAddresses;
    //        if (AnalyseIPAddresses(NodeChild, out IPAddresses))
    //        {
    //          data.IPAddressSource = IPAddresses[0];
    //          data.IPAddressTarget = IPAddresses[1];
    //        }
    //        else
    //        {
    //          Result = false;
    //        }
    //        break;
    //      case CUdpXmlHeader.XML_IPPORT:
    //        UInt16[] IPPorts;
    //        if (AnalyseIPPorts(NodeChild, out IPPorts))
    //        {
    //          data.IPPortFrom = IPPorts[0];
    //          // NC !!! data.IPPortBack = IPPorts[1];
    //        }
    //        else
    //        {
    //          Result = false;
    //        }
    //        break;
    //      case CUdpXmlHeader.XML_DEVICE:
    //        CDeviceParameter DeviceParameter;
    //        Result &= AnalyseDeviceParameter(NodeChild, out DeviceParameter);
    //        data.DeviceParameter = DeviceParameter;
    //        break;
    //      default:
    //        Result &= false;
    //        break;
    //    }
    //  }
    //  FXMLReader.WriteDecrement();
    //  return Result;
    //}

    //public Boolean AnalyseRxdResponseFindAllDevices(String xmltext,
    //                                                out CDataResponseFindAllDevices data)
    //{
    //  FNotifier.Write("AnalyseRxdResponseFindAllDevices");
    //  data = new CDataResponseFindAllDevices();
    //  try
    //  {
    //    Boolean Result = true;
    //    CXmlReader XMLReader = new CXmlReader();
    //    XMLReader.SetNotifier(FNotifier);
    //    XMLReader.EnableProtocol(false);//true);
    //    // Root
    //    XmlDocument XMLDocument = new XmlDocument();
    //    XMLDocument.InnerXml = xmltext;
    //    //
    //    XmlNode XMLNodeBase = XMLDocument.SelectSingleNode(CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
    //    if (XMLNodeBase is XmlNode)
    //    {
    //      // debug String Line = String.Format("<{0}>", XMLNodeBase.LocalName);
    //      // debug XMLReader.Write(Line);
    //      XMLReader.WriteIncrement();
    //      //
    //      foreach (XmlNode NodeChild in XMLNodeBase.ChildNodes)
    //      { // debug Console.WriteLine("----> {0}", NodeChild.Name);
    //        switch (NodeChild.Name)
    //        {
    //          case CUdpXmlHeader.XML_RESPONSE:
    //            Int32 CA = NodeChild.Attributes.Count;
    //            if (0 < CA)
    //            {
    //              String AN = NodeChild.Attributes[0].Name;
    //              String AV = NodeChild.Attributes[0].Value;
    //              if (CUdpXmlHeader.XML_TYPE == AN)
    //              {
    //                if (CUdpXmlHeader.XML_FINDALLDEVICES == AV)
    //                {
    //                  Result &= AnalyseRxdResponseFindAllDevices(NodeChild, out data);
    //                }
    //              }
    //            }
    //            break;
    //        }
    //      }
    //      XMLReader.WriteDecrement();
    //      return Result;
    //    }
    //    return false;
    //  }
    //  catch (Exception)
    //  {
    //    FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot analyse UserMeasurementProtocol");
    //    return false;
    //  }
    //}


  }
}
