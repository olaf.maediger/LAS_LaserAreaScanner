﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
//
using UCNotifier;
using Task;
using Xml;
using UdpTransfer;
using UdpXmlTransfer;
//
namespace NetworkHardwareDevice
{
  public class CResponse : CCommandBase
  { //
    //------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------
    //		
    public const Int32 TIMESPAN_WAITFORRESPONSE = 3000; // [ms]
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //
    private CDataCommand FRxdCommand;
    private CDataResponse FTxdResponse;
    //
    //------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------
    //	 
    public CResponse()
      : base()
    {
      FNotifier = null;
      FUdpTransmitter = null;
      FUdpReceiver = null;
      FRxdCommand = new CDataCommand();
      FTxdResponse = new CDataResponse();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public CDataCommand RxdCommand
    {
      get { return FRxdCommand; }
      set { FRxdCommand = value; }
    }

    public CDataResponse TxdResponse
    {
      get { return FTxdResponse; }
      set { FTxdResponse = value; }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Helper - Analyse - RxdCommand
    //-------------------------------------------------------------------
    // 
    public Boolean AnalyseRxdCommand(XmlNode nodebase,
                                     ref CDataCommand data)
    {
      // debug FNotifier.Write("AnalyseRxdCommand");
      Boolean Result = true;
      // debug String Line = String.Format("<{0}>", nodebase.Name);
      // debug FXMLReader.Write(Line);
      FXMLReader.WriteIncrement();
      foreach (XmlNode NodeChild in nodebase.ChildNodes)
      {
        String Name = NodeChild.Name;
        switch (Name)
        {
          case CUdpXmlHeader.XML_IPADDRESS:
            IPAddress[] IPAddresses;
            if (AnalyseIPAddresses(NodeChild, out IPAddresses))
            {
              data.IPAddressSource = IPAddresses[0];
              data.IPAddressTarget = IPAddresses[1];
            }
            else
            {
              Result = false;
            }
            break;
          case CUdpXmlHeader.XML_IPPORT:
            UInt16[] IPPorts;
            if (AnalyseIPPorts(NodeChild, out IPPorts))
            {
              data.IPPortFrom = IPPorts[0];
              data.IPPortBack = IPPorts[1];
            }
            else
            {
              Result = false;
            }
            break;
          case CUdpXmlHeader.XML_PARAMETER:
            CParameterList ParameterList;
            Result &= AnalyseParameterList(NodeChild, out ParameterList);
            if (Result)
            {
              data.ParameterList = ParameterList;
            }
            break;
          default:
            Result &= false;
            break;
        }
      }
      FXMLReader.WriteDecrement();
      return Result;
    }

    public Boolean AnalyseRxdCommand(String xmltext,
                                     out CDataCommand data)
    {
      // debug FNotifier.Write("AnalyseRxdCommand");
      data = new CDataCommand();
      try
      {
        Boolean Result = true;
        FXMLReader = new CXmlReader();
        FXMLReader.SetNotifier(FNotifier);
        FXMLReader.EnableProtocol(false);//true);
        // Root
        XmlDocument XMLDocument = new XmlDocument();
        XMLDocument.InnerXml = xmltext;
        //
        XmlNode XMLNodeBase = XMLDocument.SelectSingleNode(CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
        if (XMLNodeBase is XmlNode)
        {
          // debug String Line = String.Format("<{0}>", XMLNodeBase.LocalName);
          // debug FXMLReader.Write(Line);
          FXMLReader.WriteIncrement();
          //
          foreach (XmlNode NodeChild in XMLNodeBase.ChildNodes)
          { // debug Console.WriteLine("----> {0}", NodeChild.Name);
            switch (NodeChild.Name)
            {
              case CUdpXmlHeader.XML_COMMAND:
                Int32 CA = NodeChild.Attributes.Count;
                if (0 < CA)
                {
                  String AN = NodeChild.Attributes[0].Name; // type = "name" -> data.Type = value
                  if (CUdpXmlHeader.XML_TYPE == AN)
                  {
                    data.Type = NodeChild.Attributes[0].Value;
                    Result &= AnalyseRxdCommand(NodeChild, ref data);
                  }
                }
                break;
            }
          }
          FXMLReader.WriteDecrement();
          if (Result)
          {
            if (FOnDataCommandReceived is DOnDataCommandReceived)
            {
              FOnDataCommandReceived(data);
            }
          }
          return Result;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot analyse RxdCommand");
        return false;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Helper - Build - TxdResponse
    //-------------------------------------------------------------------
    // 
    public Boolean BuildTxdResponse(CDataResponse data,
                                    out String text)
    {
      // debug FNotifier.Write("BuildTxdResponse");
      text = "";
      try
      {
        CXmlWriter XMLWriter = new CXmlWriter();
        XMLWriter.SetNotifier(FNotifier);
        XMLWriter.EnableProtocol(false);//true);
        // Root
        XmlDocument XMLDocument = new XmlDocument();
        XmlNode XMLNodeBase;
        XMLWriter.AddNode(0, XMLDocument, null, out XMLNodeBase, CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
        // Response
        data.Type = FRxdCommand.Type;
        XmlElement XMLNodeResponse;
        XMLWriter.AddNodeAttribute(1, XMLDocument, XMLNodeBase, out XMLNodeResponse,
                                   CUdpXmlHeader.XML_RESPONSE, CUdpXmlHeader.XML_TYPE, data.Type);
        // IPAddress
        XmlNode XMLNodeIPAddress;
        XMLWriter.AddNode(3, XMLDocument, XMLNodeResponse, out XMLNodeIPAddress, CUdpXmlHeader.XML_IPADDRESS);
        XmlNode XMLNodeSource;
        XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeSource,
                          CUdpXmlHeader.XML_SOURCE, data.IPAddressSource.ToString());
        XmlNode XMLNodeTarget;
        XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeTarget,
                          CUdpXmlHeader.XML_TARGET, data.IPAddressTarget.ToString());
        // IPPort
        XmlNode XMLNodeIPPort;
        XMLWriter.AddNode(3, XMLDocument, XMLNodeResponse, out XMLNodeIPPort, CUdpXmlHeader.XML_IPPORT);
        XmlNode XMLNodeFrom; // only From! (no response!)
        XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeFrom,
                          CUdpXmlHeader.XML_FROM, data.IPPortFrom.ToString());
        // Parameter
        XmlNode XMLNodeParameter;
        XMLWriter.AddNode(3, XMLDocument, XMLNodeResponse, out XMLNodeParameter, CUdpXmlHeader.XML_PARAMETER);
        foreach (CParameter Parameter in data.ParameterList)
        {
          XmlNode XMLNodeEntry;
          XMLWriter.AddNode(4, XMLDocument, XMLNodeParameter, out XMLNodeEntry,
                            Parameter.Name, Parameter.Value);
        }
        //
        text = XMLDocument.InnerXml;
        return true;
      }
      catch (Exception)
      {
        FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot build TxdResponse");
        return false;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback - UdpTransmitter
    //-------------------------------------------------------------------
    //
    private void UdpTransmitterOnDatagramTransmitted(IPEndPoint ipendpoint,
                                                     Byte[] datagram, Int32 size)
    {
      if (FUdpTransmitter is CUdpTransmitter)
      {
        FUdpTransmitter.Close();
        FUdpTransmitter = null;
      }
      FEventTransmitted.Pulse();
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback - UdpReceiver
    //-------------------------------------------------------------------
    //
    private void UdpReceiverOnDatagramReceived(IPEndPoint ipendpoint,
                                               Byte[] datagram, Int32 size)
    {
      String RxdFrame = System.Text.Encoding.UTF8.GetString(datagram, 0, size);
      if (AnalyseRxdCommand(RxdFrame, out FRxdCommand))
      { // RequestFindAllDevices correct detected -> Build and transmit ResponseFindAllDevices:
        CUdpXmlWriter UXW = new CUdpXmlWriter();
        IPAddress IPA;
        CUdpBase.FindIPAddressLocal(out IPA);
        FTxdResponse.IPAddressSource = IPA;
        FTxdResponse.IPAddressTarget = FRxdCommand.IPAddressSource;
        FTxdResponse.IPPortFrom = FRxdCommand.IPPortBack;
        String TxdFrame;
        if (BuildTxdResponse(FTxdResponse, out TxdFrame))
        {
          if (FUdpTransmitter is CUdpTransmitter)
          {
            FUdpTransmitter.Close();
            FUdpTransmitter = null;
          }
          FUdpTransmitter = new CUdpTransmitter();
          FUdpTransmitter.SetNotifier(FNotifier);
          FUdpTransmitter.SetOnDatagramTransmitted(UdpTransmitterOnDatagramTransmitted);
          FEventTransmitted = new CEvent();
          Byte[] DRFAD = FRxdCommand.IPAddressSource.GetAddressBytes();
          String SEPTransmitter = String.Format("{0}.{1}.{2}.{3}:{4}",
                                                DRFAD[0], DRFAD[1], DRFAD[2], DRFAD[3],
                                                FRxdCommand.IPPortBack);
          FUdpTransmitter.Open(SEPTransmitter);
          FUdpTransmitter.SetMessage(TxdFrame);
        }
      }
      FEventReceived.Pulse();
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback - Task
    //-------------------------------------------------------------------
    //
    private void TaskOnExecutionStart(RTaskData data)
    {
      String Line = String.Format("Response[{0}] - Start", FTxdResponse.Type);
      FNotifier.Write(Line);
      if (FUdpTransmitter is CUdpTransmitter)
      {
        FUdpTransmitter.Close();
        FUdpTransmitter = null;
      }
      if (FUdpReceiver is CUdpReceiver)
      {
        FUdpReceiver.Close();
        FUdpReceiver = null;
      }
      //IPAddress IPALocal;
      //if (CUdpBase.FindIPAddressLocal(out IPALocal))
      //{ // Init Receiver
      String SEPReceiver = String.Format("{0}:{1}",
                                         FRxdCommand.IPAddressSource.ToString(),
                                         FRxdCommand.IPPortFrom);
      FUdpReceiver = new CUdpReceiver();
      FUdpReceiver.SetNotifier(FNotifier);
      FUdpReceiver.SetOnDatagramReceived(UdpReceiverOnDatagramReceived);
      FEventReceived = new CEvent();
      FUdpReceiver.Open(SEPReceiver);
      if (FParentOnExecutionStart is DOnExecutionStart)
      {
        FParentOnExecutionStart(data);
      }
    }
    private Boolean TaskOnExecutionBusy(ref RTaskData data)
    {
      try
      {
        if (data.Counter <= 1)
        {
          String Line = String.Format("Response[{0}] - Busy", FTxdResponse.Type);
          FNotifier.Write(Line);
        }
        if (FParentOnExecutionBusy is DOnExecutionBusy)
        {
          FParentOnExecutionBusy(ref data);
        }
        Thread.Sleep(100);
        return true; // MUST be aborted from Parent!!!
      }
      catch (Exception)// e)
      {
        // FNotifier.Write(e.Message);
        FNotifier.Write("Response - Busy Exception");
        return false;
      }
    }
    private void TaskOnExecutionEnd(RTaskData data)
    {
      String Line = String.Format("Response[{0}] - End", FTxdResponse.Type);
      FNotifier.Write(Line);
      if (FParentOnExecutionEnd is DOnExecutionEnd)
      {
        FParentOnExecutionEnd(data);
      }
    }
    private void TaskOnExecutionAbort(RTaskData data)
    {
      String Line = String.Format("Response[{0}] - Abort", FTxdResponse.Type);
      FNotifier.Write(Line);
      if (FParentOnExecutionAbort is DOnExecutionAbort)
      {
        FParentOnExecutionAbort(data);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Public
    //------------------------------------------------------------------------
    //	
    public override Boolean Execute(String taskname,
                                    DOnExecutionStart onexecutionstart,
                                    DOnExecutionBusy onexecutionbusy,
                                    DOnExecutionEnd onexecutionend,
                                    DOnExecutionAbort onexecutionabort)
    {
      try
      { // start Task...
        FParentOnExecutionStart = onexecutionstart;
        FParentOnExecutionBusy = onexecutionbusy;
        FParentOnExecutionEnd = onexecutionend;
        FParentOnExecutionAbort = onexecutionabort;
        String Line = String.Format("Response{0}{1}", FTxdResponse.Type, taskname);
        // debug FNotifier.Write(Line);
        base.Execute(Line, 
                     TaskOnExecutionStart, TaskOnExecutionBusy,
                     TaskOnExecutionEnd, TaskOnExecutionAbort);
        return true;
      }
      catch (Exception)// e)
      {
        // FNotifier.Write(e.Message);
        // FNotifier.Write("UdpClient: Receive-Thread aborted");
        return false;
      }
    }

    public override bool Abort()
    {
      if (FUdpTransmitter is CUdpTransmitter)
      {
        FUdpTransmitter.Close();
        FUdpTransmitter = null;
      }
      if (FUdpReceiver is CUdpReceiver)
      {
        FUdpReceiver.Close();
        FUdpReceiver = null;
      }
      return base.Abort();
    }




  }
}

//public Boolean AnalyseParameterList(XmlNode nodebase,
//                                    out CParameterList parameterlist)
//{
//  FNotifier.Write("AnalyseParameterList");
//  Boolean Result = true;
//  // debug String Line = String.Format("<{0}>", nodebase.Name);
//  // debug FXMLReader.Write(Line);
//  FXMLReader.WriteIncrement();
//  parameterlist = new CParameterList();
//  foreach (XmlNode NodeChild in nodebase.ChildNodes)
//  {
//    parameterlist.Add(NodeChild.Name, NodeChild.InnerText);
//  }
//  FXMLReader.WriteDecrement();
//  return Result;
//}

