﻿using System;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using UCNotifier;
using Xml;
using UdpTransfer;
using UdpXmlTransfer;
//
namespace NetworkHardwareDevice
{
  public class CNetworkHardwareDevice
  {
  }
}
  //////////////{ //
  //////////////  ////------------------------------------------------------
  //////////////  ////  Section - Constant
  //////////////  ////------------------------------------------------------
  //////////////  ////		
  //////////////  //////// NC public const Int32 INIT_PORTTXD_FINDALLDEVICES = 20000;
  //////////////  //////// NC public const Int32 INIT_PORTRXD_FINDALLDEVICES = 20001;
  //////////////  //public const Byte INIT_BROADCAST_BYTE = 0xFF;
  //////////////  //
  //////////////  //------------------------------------------------------
  //////////////  //  Section - Field
  //////////////  //------------------------------------------------------
  //////////////  //		
  //////////////  private CNotifier FNotifier;
  //////////////  //
  //////////////  // NC private CUdpTransmitter FUdpTransmitter;
  //////////////  // NC private String FSEPTransmitter;
  ////////////// // NC  private String FTxdText;
  //////////////  //
  //////////////  // NC private CUdpReceiver FUdpReceiver;
  //////////////  // NC private String FSEPReceiver;
  //////////////  //
  //////////////  //------------------------------------------------------
  //////////////  //  Section - Constructor
  //////////////  //------------------------------------------------------
  //////////////  //	 
  //////////////  public CNetworkHardwareDevice()
  //////////////    : base()
  //////////////  {
  //////////////    FNotifier = null;
  //////////////  //  FUdpTransmitter = null;
  //////////////  //  FUdpReceiver = null;
  //////////////  }
  //////////////  //
  //////////////  //------------------------------------------------------------------------
  //////////////  //  Section - Property
  //////////////  //------------------------------------------------------------------------
  //////////////  //	
  //////////////  public void SetNotifier(CNotifier value)
  //////////////  {
  //////////////    FNotifier = value;
  //////////////  }

  //////////////  //public Boolean IsUdpTransmitterOpen()
  //////////////  //{
  //////////////  //  return (FUdpTransmitter is CUdpTransmitter);
  //////////////  //}

  //////////////  //public Boolean IsUdpReceiverOpen()
  //////////////  //{
  //////////////  //  return (FUdpReceiver is CUdpReceiver);
  //////////////  //}
  //////////////  //
  //////////////  //------------------------------------------------------------------------
  //////////////  //  Section - Callback
  //////////////  //------------------------------------------------------------------------
  //////////////  //	
  //////////////  private void UdpTransmitterOnDatagramTransmitted(IPEndPoint ipendpoint,
  //////////////                                                   Byte[] datagram, 
  //////////////                                                   Int32 size)
  //////////////  {
  //////////////  }

  //////////////  private void UdpReceiverOnDatagramReceived(IPEndPoint ipendpoint,
  //////////////                                             Byte[] datagram,
  //////////////                                             Int32 size)
  //////////////  {
  //////////////  }
  //////////////  //
  //////////////  //------------------------------------------------------------------------
  //////////////  //  Section - Public Management
  //////////////  //------------------------------------------------------------------------
  //////////////  //	
  //////////////  //public virtual Boolean Open()
  //////////////  //{
  //////////////  //  try
  //////////////  //  {
  //////////////  //    if (FNotifier is CNotifier)
  //////////////  //    {
  //////////////  //      Close();
  //////////////  //      //
  //////////////  //      FUdpTransmitter = new CUdpTransmitter();
  //////////////  //      FUdpTransmitter.SetNotifier(FNotifier);
  //////////////  //      //
  //////////////  //      FUdpReceiver = new CUdpReceiver();
  //////////////  //      FUdpReceiver.SetNotifier(FNotifier);
  //////////////  //      return true;
  //////////////  //    }
  //////////////  //    return false;
  //////////////  //  }
  //////////////  //  catch (Exception)
  //////////////  //  {
  //////////////  //    return false;
  //////////////  //  }
  //////////////  //}

  //////////////  //public virtual Boolean Close()
  //////////////  //{
  //////////////  //  try
  //////////////  //  {
  //////////////  //    if (IsUdpTransmitterOpen())
  //////////////  //    {
  //////////////  //      FUdpTransmitter.Close();
  //////////////  //      FUdpTransmitter = null;
  //////////////  //    }
  //////////////  //    if (IsUdpReceiverOpen())
  //////////////  //    {
  //////////////  //      FUdpReceiver.Close();
  //////////////  //      FUdpReceiver = null;
  //////////////  //    }
  //////////////  //    return true;
  //////////////  //  }
  //////////////  //  catch (Exception)
  //////////////  //  {
  //////////////  //    return false;
  //////////////  //  }
  //////////////  //}
  //////////////  //
  //////////////  //------------------------------------------------------------------------
  //////////////  //  Section - Helper
  //////////////  //------------------------------------------------------------------------
  //////////////  //	
  //////////////  //public Boolean UdpXmlTxdFindAllDevices(CUdpXmlWriter udpxmlwriter,
  //////////////  //                                       CUdpXmlReader udpxmlreader)
  //////////////  //{
  //////////////  //  try
  //////////////  //  {
  //////////////  //    IPAddress IPALocal;
  //////////////  //    if (CUdpBase.FindIPAddressLocal(out IPALocal))
  //////////////  //    { // Init Transmitter (Broadcast to any FMServer in LocalNet)
  //////////////  //      Byte[] AB = IPALocal.GetAddressBytes();
  //////////////  //      String SEPTransmitter = String.Format("{0}.{1}.{2}.{3}:{4}",
  //////////////  //                                            AB[0], AB[1], AB[2], 
  //////////////  //                                            CUdpXmlHeader.INIT_BROADCAST_BYTE,
  //////////////  //                                            CUdpXmlHeader.IPPORT_BROADCAST_FROM); 
  //////////////  //      udpxmlwriter.BuildRequestFindAllDevices(out FTxdText);
  //////////////  //      FUdpTransmitter.SetMessage(FTxdText);
  //////////////  //      FUdpTransmitter.SetOnDatagramTransmitted(UdpTransmitterOnDatagramTransmitted);
  //////////////  //      // Init Receiver
  //////////////  //      String SEPReceiver = String.Format("{0}:{1}", 
  //////////////  //                                         IPALocal.ToString(),
  //////////////  //                                         CUdpXmlHeader.IPPORT_BROADCAST_BACK);
  //////////////  //      //
  //////////////  //      FUdpReceiver.SetOnDatagramReceived(UdpReceiverOnDatagramReceived);
  //////////////  //      //
  //////////////  //      FUdpReceiver.Open(SEPReceiver);
  //////////////  //      FUdpTransmitter.Open(SEPTransmitter);
  //////////////  //      //
  //////////////  //      return true;
  //////////////  //    }
  //////////////  //    return false;
  //////////////  //  }
  //////////////  //  catch (Exception)
  //////////////  //  {
  //////////////  //    return false;
  //////////////  //  }
  //////////////  //}
  //////////////  //public Boolean UdpXmlRxdFindAllDevices()
  //////////////  //{
  //////////////  //  try
  //////////////  //  {
  //////////////  //    return true;
  //////////////  //  }
  //////////////  //  catch (Exception)
  //////////////  //  {
  //////////////  //    return false;
  //////////////  //  }
  //////////////  //}

  //////////////}



    //protected Boolean DetectIPAddressLocal(out IPAddress ipaddresslocal)
    //{
    //  try
    //  {

    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

