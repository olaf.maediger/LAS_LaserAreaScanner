﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
//
using UCNotifier;
using Task;
using Xml;
using UdpTransfer;
using UdpXmlTransfer;
//
namespace NetworkHardwareDevice
{
  public class CCommand : CCommandBase
  { //
    //------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------
    //		
    public const Int32 TIMESPAN_WAITFORRESPONSE = 5000; // [ms]
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //
    protected CDataCommand FTxdCommand;
    protected CDataResponse FRxdResponse;
    protected String FHeader;
    //
    //------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------
    //	   
    public CCommand()
      : base()
    {
      FHeader = "Command";
      FTxdCommand = new CDataCommand();
      FRxdResponse = new CDataResponse();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public CDataCommand TxdCommand
    {
      get { return FTxdCommand; }
      set { FTxdCommand = value; }
    }

    public CDataResponse RxdResponse
    {
      get { return FRxdResponse; }
      set { FRxdResponse = value; }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Helper - Build - TxdCommand
    //-------------------------------------------------------------------
    // 
    public Boolean BuildTxdCommand(out String text)
    {
      text = "";
      try
      {
        String Line = String.Format("BuildTxd{0}[{1}] - ParameterCount[{2}]", 
                                    FHeader, FTxdCommand.Type, FTxdCommand.ParameterList.Count);
        // debug FNotifier.Write(Line);
        CXmlWriter XMLWriter = new CXmlWriter();
        XMLWriter.SetNotifier(FNotifier);
        XMLWriter.EnableProtocol(false);//true);
        // Root
        XmlDocument XMLDocument = new XmlDocument();
        XmlNode XMLNodeBase;
        XMLWriter.AddNode(0, XMLDocument, null, out XMLNodeBase, CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
        // Command
        XmlElement XMLNodeCommand;
        XMLWriter.AddNodeAttribute(1, XMLDocument, XMLNodeBase, out XMLNodeCommand,
                                   CUdpXmlHeader.XML_COMMAND, CUdpXmlHeader.XML_TYPE, FTxdCommand.Type);
        // IPAddress
        XmlNode XMLNodeIPAddress;
        XMLWriter.AddNode(3, XMLDocument, XMLNodeCommand, out XMLNodeIPAddress, CUdpXmlHeader.XML_IPADDRESS);
        XmlNode XMLNodeSource; // Sender IP (UdpClientObserver)
        XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeSource,
                          CUdpXmlHeader.XML_SOURCE, FTxdCommand.IPAddressSource.ToString());
        XmlNode XMLNodeTarget; // Receiver IP (UdpServerDisplay)
        XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeTarget,
                          CUdpXmlHeader.XML_TARGET, FTxdCommand.IPAddressTarget.ToString());
        // IPPort
        XmlNode XMLNodeIPPort;
        XMLWriter.AddNode(3, XMLDocument, XMLNodeCommand, out XMLNodeIPPort, CUdpXmlHeader.XML_IPPORT);
        XmlNode XMLNodeFrom;
        XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeFrom,
                          CUdpXmlHeader.XML_FROM, FTxdCommand.IPPortFrom.ToString());
        XmlNode XMLNodeBack;
        XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeBack,
                          CUdpXmlHeader.XML_BACK, FTxdCommand.IPPortBack.ToString());
        // Parameter
        XmlNode XMLNodeParameterList;
        XMLWriter.AddNode(3, XMLDocument, XMLNodeCommand, out XMLNodeParameterList, CUdpXmlHeader.XML_PARAMETER);
        foreach (CParameter Parameter in FTxdCommand.ParameterList)
        {
          XmlNode XMLNodeParameter;
          XMLWriter.AddNode(4, XMLDocument, XMLNodeParameterList, out XMLNodeParameter,
                            Parameter.Name, Parameter.Value);
        }
        //
        text = XMLDocument.InnerXml;
        return true;
      }
      catch (Exception)
      {
        String Line = String.Format("Cannot build Txd{0}", FHeader);
        FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, Line);
        return false;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Helper - Analyse - RxdResponse
    //-------------------------------------------------------------------
    // 
    public Boolean AnalyseRxdResponse(XmlNode nodebase,
                                     ref CDataResponse data)
    {
      // debug FNotifier.Write("AnalyseRxdResponse");
      Boolean Result = true;
      // debug String Line = String.Format("<{0}>", nodebase.Name);
      // debug FXMLReader.Write(Line);
      FXMLReader.WriteIncrement();
      foreach (XmlNode NodeChild in nodebase.ChildNodes)
      {
        String Name = NodeChild.Name;
        switch (Name)
        {
          case CUdpXmlHeader.XML_IPADDRESS:
            IPAddress[] IPAddresses;
            if (AnalyseIPAddresses(NodeChild, out IPAddresses))
            {
              data.IPAddressSource = IPAddresses[0];
              data.IPAddressTarget = IPAddresses[1];
            }
            else
            {
              Result = false;
            }
            break;
          case CUdpXmlHeader.XML_IPPORT:
            UInt16[] IPPorts;
            if (AnalyseIPPorts(NodeChild, out IPPorts))
            {
              data.IPPortFrom = IPPorts[0];
              // NC data.IPPortBack = IPPorts[1];
            }
            else
            {
              Result = false;
            }
            break;
          case CUdpXmlHeader.XML_PARAMETER:
            CParameterList ParameterList;
            Result &= AnalyseParameterList(NodeChild, out ParameterList);
            if (Result)
            {
              data.ParameterList = ParameterList;
            }
            break;
          default:
            Result &= false;
            break;
        }
      }
      FXMLReader.WriteDecrement();
      return Result;
    }

    public Boolean AnalyseRxdResponse(String xmltext,
                                      out CDataResponse data)
    {
      // debug FNotifier.Write("AnalyseRxdResponse");
      data = new CDataResponse();
      try
      {
        Boolean Result = true;
        FXMLReader = new CXmlReader();
        FXMLReader.SetNotifier(FNotifier);
        FXMLReader.EnableProtocol(false);//true);
        // Root
        XmlDocument XMLDocument = new XmlDocument();
        XMLDocument.InnerXml = xmltext;
        //
        XmlNode XMLNodeBase = XMLDocument.SelectSingleNode(CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
        if (XMLNodeBase is XmlNode)
        {
          // debug String Line = String.Format("<{0}>", XMLNodeBase.LocalName);
          // debug FXMLReader.Write(Line);
          FXMLReader.WriteIncrement();
          //
          foreach (XmlNode NodeChild in XMLNodeBase.ChildNodes)
          { // debug Console.WriteLine("----> {0}", NodeChild.Name);
            switch (NodeChild.Name)
            {
              //////// no sense case CUdpXmlHeader.XML_COMMAND:
              //////// no sense case CUdpXmlHeader.XML_REQUEST:
              case CUdpXmlHeader.XML_RESPONSE:
                Int32 CA = NodeChild.Attributes.Count;
                if (0 < CA)
                {
                  String AN = NodeChild.Attributes[0].Name; // type = "name" -> data.Type = value
                  if (CUdpXmlHeader.XML_TYPE == AN)
                  {
                    data.Type = NodeChild.Attributes[0].Value;
                    Result &= AnalyseRxdResponse(NodeChild, ref data);
                  }
                }
                break;
             }
          }
          FXMLReader.WriteDecrement();
          return Result;
        }
        return false;
      }
      catch (Exception)
      {
        FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot analyse RxdResponse");
        return false;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback - UdpTransmitter
    //-------------------------------------------------------------------
    //
    private void UdpTransmitterOnDatagramTransmitted(IPEndPoint ipendpoint,
                                                     Byte[] datagram, Int32 size)
    {
      if (FUdpTransmitter is CUdpTransmitter)
      {
        FUdpTransmitter.Close();
        FUdpTransmitter = null;
      }
      FEventTransmitted.Pulse();
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback - UdpReceiver
    //-------------------------------------------------------------------
    //
    private void UdpReceiverOnDatagramReceived(IPEndPoint ipendpoint,
                                               Byte[] datagram, Int32 size)
    {
      String RxdText = System.Text.Encoding.UTF8.GetString(datagram, 0, size);
      // AnalyseResponse -> Collect Responses to Container...
      FXMLReader = new CUdpXmlReader();
      FXMLReader.SetNotifier(FNotifier);
      if (AnalyseRxdResponse(RxdText, out FRxdResponse))
      {
        if (FOnDataResponseReceived is DOnDataResponseReceived)
        {
          FOnDataResponseReceived(FRxdResponse);
        }
        FTxdCommand.CountReceived++;
      }
      FEventReceived.Pulse();
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Helper
    //-------------------------------------------------------------------
    //
    protected virtual String InitSEPReceiver()
    {
      IPAddress IPALocal;
      CUdpBase.FindIPAddressLocal(out IPALocal);
      String SEPReceiver = String.Format("{0}:{1}",
                                         IPALocal.ToString(),
                                         FTxdCommand.IPPortBack);
      return SEPReceiver;
    }

    protected virtual String InitSEPTransmitter()
    {
      String SEPTransmitter = String.Format("{0}:{1}",
                                            FTxdCommand.IPAddressTarget.ToString(),
                                            FTxdCommand.IPPortFrom);
      return SEPTransmitter;
    }

    protected virtual void InitTransmitIPAddressPort(ref CDataCommand datacommand)
    {
      IPAddress IPALocal;
      CUdpBase.FindIPAddressLocal(out IPALocal);
      datacommand.IPAddressSource = IPALocal;
    }

    protected virtual Boolean ExecuteCommand(String txdframe)
    {
      FTxdCommand.CountReceived = 0;
      String Line = String.Format("{0}[{1}] transmit...", FHeader, FTxdCommand.Type);
      FNotifier.Write(Line);
      FNotifier.Write("Wait for Response...");
      FUdpTransmitter.SetMessage(txdframe);
      Boolean WaitForResponse = FEventReceived.WaitFor(TIMESPAN_WAITFORRESPONSE);
      Line = String.Format("Response[{0}] received: {1}", FTxdCommand.Type, WaitForResponse);
      FNotifier.Write(Line);
      Abort();
      return false; // only one step!!!!
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback - Task
    //-------------------------------------------------------------------
    //
    private void TaskOnExecutionStart(RTaskData data)
    {
      String Line = String.Format("{0}[{1}] - Start", FHeader, FTxdCommand.Type);
      FNotifier.Write(Line);
      if (FUdpTransmitter is CUdpTransmitter)
      {
        FUdpTransmitter.Close();
        FUdpTransmitter = null;
      }
      if (FUdpReceiver is CUdpReceiver)
      {
        FUdpReceiver.Close();
        FUdpReceiver = null;
      }
      //
      // Init Receiver on BACK-Channel
      FUdpReceiver = new CUdpReceiver();
      FUdpReceiver.SetNotifier(FNotifier);
      FUdpReceiver.SetOnDatagramReceived(UdpReceiverOnDatagramReceived);
      FEventReceived = new CEvent();
      String SEPReceiver = InitSEPReceiver();
      FNotifier.Write(String.Format("SEPReceiver<{0}>", SEPReceiver));
      FUdpReceiver.Open(SEPReceiver);
      //
      // Init Transmitter on FROM-Channel      
      FUdpTransmitter = new CUdpTransmitter();
      FUdpTransmitter.SetNotifier(FNotifier);
      FUdpTransmitter.SetOnDatagramTransmitted(UdpTransmitterOnDatagramTransmitted);
      FEventTransmitted = new CEvent();
      String SEPTransmitter = InitSEPTransmitter();
      FNotifier.Write(String.Format("SEPTransmitter<{0}>", SEPTransmitter));
      FUdpTransmitter.Open(SEPTransmitter);
      //
      if (FParentOnExecutionStart is DOnExecutionStart)
      {
        FParentOnExecutionStart(data);
      }
    }
    private Boolean TaskOnExecutionBusy(ref RTaskData data)
    {
      try
      {
        String Line;
        if (data.Counter <= 1)
        {
          Line = String.Format("{0}[{1}] - Busy", FHeader, FTxdCommand.Type);
          FNotifier.Write(Line);
        }
        if (FParentOnExecutionBusy is DOnExecutionBusy)
        {
          FParentOnExecutionBusy(ref data);
        }
        CUdpXmlWriter UXW = new CUdpXmlWriter();
        UXW.SetNotifier(FNotifier);
        UXW.EnableProtocol(false);//true);
        //
        InitTransmitIPAddressPort(ref FTxdCommand);
        String TxdFrame;
        BuildTxdCommand(out TxdFrame);
        //
        return ExecuteCommand(TxdFrame);//false; // only one execution step!
      }
      catch (Exception)// e)
      {
        // FNotifier.Write(e.Message);
        // FNotifier.Write("UdpClient: Receive-Thread aborted");
        return false;
      }
    }
    private void TaskOnExecutionEnd(RTaskData data)
    {
      String Line = String.Format("{0}[{1}] - End", FHeader, FTxdCommand.Type);
      FNotifier.Write(Line);
      if (FParentOnExecutionEnd is DOnExecutionEnd)
      {
        FParentOnExecutionEnd(data);
      }
    }
    private void TaskOnExecutionAbort(RTaskData data)
    {
      String Line = String.Format("{0}[{1}] - Abort", FHeader, FTxdCommand.Type);
      FNotifier.Write(Line);
      if (FParentOnExecutionAbort is DOnExecutionAbort)
      {
        FParentOnExecutionAbort(data);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Public
    //------------------------------------------------------------------------
    //	
    public override Boolean Execute(String taskname, 
                                    DOnExecutionStart onexecutionstart,
                                    DOnExecutionBusy onexecutionbusy,
                                    DOnExecutionEnd onexecutionend,
                                    DOnExecutionAbort onexecutionabort)
    {
      try
      { 
        FParentOnExecutionStart = onexecutionstart;
        FParentOnExecutionBusy = onexecutionbusy;
        FParentOnExecutionEnd = onexecutionend;
        FParentOnExecutionAbort = onexecutionabort;
        // start Task...
        String Line = String.Format("{0}{1}{2}", FHeader, FTxdCommand.Type, taskname);
        base.Execute(Line,
                     TaskOnExecutionStart, TaskOnExecutionBusy,
                     TaskOnExecutionEnd, TaskOnExecutionAbort);
        return true;
      }
      catch (Exception)// e)
      {
        // FNotifier.Write(e.Message);
        // FNotifier.Write("UdpClient: Receive-Thread aborted");
        return false;
      }
    }

    public override bool Abort()
    {
      if (FUdpTransmitter is CUdpTransmitter)
      {
        FUdpTransmitter.Close();
        FUdpTransmitter = null;
      }
      if (FUdpReceiver is CUdpReceiver)
      {
        FUdpReceiver.Close();
        FUdpReceiver = null;
      }
      return base.Abort();
    }
    
  }
}

