﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using Network;
using UdpTextTransfer;
using NhdBase;
using UdpTextDevice;
using UdpTextArray;
using UdpBinaryTransfer;
using UdpBinaryDevice;
using UCLanTransfer;
using UCNhdCommon;
using UCNhdUdpText;
using UCNhdUdpArray;
//
namespace LanUdpServer
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormServer : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICETYPE = "LanUdpServer";
    private const String INIT_DEVICENAME = "OMDevelopDebugUdpTextBinaryServer";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    CDeviceParameter FDeviceParameter;
    Random FRandom;
    CUdpTextDeviceServer FUdpTextDeviceServer;
    ////// /////////////////////////CUdpTextArrayServer FUdpTextArrayServer;
    //////////////CUdpBinaryDeviceServer FUdpBinaryDeviceServer;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FRandom = new Random((Int32)DateTime.Now.Ticks);
      //
      //####################################################################################
      // Init - Instance - Server
      //####################################################################################
      //
      FDeviceParameter = new CDeviceParameter();
      FDeviceParameter.Type = INIT_DEVICETYPE;
      FDeviceParameter.Name = INIT_DEVICENAME;
      FDeviceParameter.ID = Guid.NewGuid();
      IPAddress IPALocal;
      CNetwork.FindIPAddressLocal(out IPALocal);
      FDeviceParameter.IPA = IPALocal;
      IPAddress IPATarget;
      CNetwork.FindIPAddressLocalBroadcast(out IPATarget);
      //
      //  ---------------------------------------------------
      //  UdpTextDeviceServer
      //  ---------------------------------------------------
      FUdpTextDeviceServer = new CUdpTextDeviceServer();
      // UdpTextDeviceServer - Common
      FUdpTextDeviceServer.SetNotifier(FUCNotifier);
      FUdpTextDeviceServer.SetOnDatagramTransmitted(UdpTextDeviceServerOnDatagramTransmitted);
      FUdpTextDeviceServer.SetOnDatagramReceived(UdpTextDeviceServerOnDatagramReceived);
      // UdpTextDeviceServer - Source
      FUdpTextDeviceServer.SetSourceIpAddress(IPALocal);
      FUdpTextDeviceServer.SetSourceIpPort(CNhdBase.PORT_UDPTEXT_MESSAGE_SERVER_RECEIVER);
      FUdpTextDeviceServer.SetSourceID(FDeviceParameter.ID);
      FUdpTextDeviceServer.SetSourceType("NhdDataMessageServer");
      FUdpTextDeviceServer.SetSourceName("PSimonLaborDataCollection");
      // UdpTextDeviceServer - Target
      FUdpTextDeviceServer.SetTargetIpAddress(IPATarget);
      FUdpTextDeviceServer.SetTargetIpPort(CNhdBase.PORT_UDPTEXT_MESSAGE_SERVER_TRANSMITTER);
      FUdpTextDeviceServer.SetTargetID(Guid.Empty);
      FUdpTextDeviceServer.SetTargetType("NhdDataMessageClient");
      FUdpTextDeviceServer.SetTargetName("PSimonOfficeDataAnalysis");
      // UdpTextDeviceServer - Open
      FUdpTextDeviceServer.Open();
      ////////////////////////
      ////////////////////////  ---------------------------------------------------
      ////////////////////////  UdpTextArrayServer 
      ////////////////////////  ---------------------------------------------------
      //////////////////////FUdpTextArrayServer = new CUdpTextArrayServer();
      //////////////////////// UdpTextArrayServer - Common
      //////////////////////FUdpTextArrayServer.SetNotifier(FUCNotifier);
      //////////////////////FUdpTextArrayServer.SetOnDatagramTransmitted(UdpTextArrayServerOnDatagramTransmitted);
      //////////////////////FUdpTextArrayServer.SetOnDatagramReceived(UdpTextArrayServerOnDatagramReceived);
      //////////////////////FUdpTextArrayServer.SetOnBuildDoubleArray2D(UdpTextArrayServerOnBuildDoubleArray2D);
      //////////////////////// UdpTextArrayServer - Source
      //////////////////////FUdpTextArrayServer.SetSourceIpAddress(IPALocal);
      //////////////////////FUdpTextArrayServer.SetSourceIpPort(CNhdBase.PORT_UDPTEXT_ARRAY_SERVER_RECEIVER);
      //////////////////////FUdpTextArrayServer.SetSourceID(FDeviceParameter.ID);
      //////////////////////FUdpTextArrayServer.SetSourceType("NhdDataArrayServer");
      //////////////////////FUdpTextArrayServer.SetSourceName("PSimonLaborDataCollection");
      //////////////////////// UdpTextArrayServer - Target
      //////////////////////FUdpTextArrayServer.SetTargetIpAddress(IPATarget);
      //////////////////////FUdpTextArrayServer.SetTargetIpPort(CNhdBase.PORT_UDPTEXT_ARRAY_SERVER_TRANSMITTER);
      //////////////////////FUdpTextArrayServer.SetTargetID(Guid.Empty);
      //////////////////////FUdpTextArrayServer.SetTargetType("NhdDataArrayClient");
      //////////////////////FUdpTextArrayServer.SetTargetName("PSimonOfficeDataAnalysis");
      //////////////////////// UdpTextArrayServer - Open
      //////////////////////FUdpTextArrayServer.Open();
      ////
      ////  ---------------------------------------------------
      ////  UdpBinaryDeviceServer
      ////  ---------------------------------------------------
      //FUdpBinaryDeviceServer = new CUdpBinaryDeviceServer();
      //// UdpTextDeviceServer - Common
      //FUdpBinaryDeviceServer.SetNotifier(FUCNotifier);
      //FUdpBinaryDeviceServer.SetOnDatagramTransmitted(UdpBinaryDeviceServerOnDatagramTransmitted);
      //FUdpBinaryDeviceServer.SetOnDatagramReceived(UdpBinaryDeviceServerOnDatagramReceived);
      //// UdpBinaryDeviceServer - Source
      //FUdpBinaryDeviceServer.SetSourceIpAddress(IPALocal);
      //FUdpBinaryDeviceServer.SetSourceIpPort(CNhdBase.PORT_UDPBINARY_MESSAGE_SERVER_RECEIVER);
      //FUdpBinaryDeviceServer.SetSourceID(FDeviceParameter.ID);
      //FUdpBinaryDeviceServer.SetSourceType("NhdDataMessageServer");
      //FUdpBinaryDeviceServer.SetSourceName("PSimonLaborDataCollection");
      //// UdpBinaryDeviceServer - Target
      //FUdpBinaryDeviceServer.SetTargetIpAddress(IPATarget);
      //FUdpBinaryDeviceServer.SetTargetIpPort(CNhdBase.PORT_UDPBINARY_MESSAGE_SERVER_TRANSMITTER);
      //FUdpBinaryDeviceServer.SetTargetID(Guid.Empty);
      //FUdpBinaryDeviceServer.SetTargetType("NhdDataMessageClient");
      //FUdpBinaryDeviceServer.SetTargetName("PSimonOfficeDataAnalysis");
      //// UdpBinaryDeviceServer - Open
      //FUdpBinaryDeviceServer.Open();
      //////////////////////////////
      //////////////////////////////####################################################################################
      ////////////////////////////// Init - UserControls - Server
      //////////////////////////////####################################################################################
      //////////////////////////////
      ////////////////////////////// UCNhdUdpTextServer
      ////////////////////////////FUCNhdUdpTextServer.SetIpAddressLocal(IPALocal);
      ////////////////////////////FUCNhdUdpTextServer.SetIpPortTransmit(CNhdBase.PORT_UDPTEXT_MESSAGE_SERVER_TRANSMITTER);
      ////////////////////////////FUCNhdUdpTextServer.SetIpPortReceive(CNhdBase.PORT_UDPTEXT_MESSAGE_SERVER_RECEIVER);
      ////////////////////////////FUCNhdUdpTextServer.SetIpAddressTarget(IPATarget);
      ////////////////////////////FUCNhdUdpTextServer.SetOnSendMessage(UCNhdUdpTextServerOnSendMessage);
      //////////////////////////////
      ////////////////////////////// UCNhdUdpArrayServer
      ////////////////////////////FUCNhdUdpArrayServer.SetIpAddressLocal(IPALocal);
      ////////////////////////////FUCNhdUdpArrayServer.SetIpPortTransmit(CNhdBase.PORT_UDPTEXT_ARRAY_SERVER_TRANSMITTER);
      ////////////////////////////FUCNhdUdpArrayServer.SetIpPortReceive(CNhdBase.PORT_UDPTEXT_ARRAY_SERVER_RECEIVER);
      ////////////////////////////FUCNhdUdpArrayServer.SetIpAddressTarget(IPATarget);
      ////////////////////////////FUCNhdUdpArrayServer.SetOnSendArray2DSmall(UCNhdUdpArrayServerOnSendArray2DSmall);
      ////////////////////////////FUCNhdUdpArrayServer.SetOnSendArray2DLarge(UCNhdUdpArrayServerOnSendArray2DLarge);
      //////////////////////////////
      //------------------------------------------------------------------------------
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      //!!!!!!!!!!!!!!!!!! Start Parallel-Application: LanUdpClient.exe
      Process.Start("LanUdpClient.exe");
      //
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FUdpTextDeviceServer.Close();
      ////// ///////////////////////// FUdpTextArrayServer.Close();
      /////////////FUdpBinaryDeviceServer.Close();
      //////////////FUdpBinaryDeviceServer.Close();
      ////////////////
      Process[] ProcessesLanUdpClient = Process.GetProcessesByName("LanUdpClient");
      foreach (Process ProcessLanUdpClient in ProcessesLanUdpClient)
      {
        ProcessLanUdpClient.Kill();
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      ////// /////////////////////////?????FUdpTextArrayServer.Close();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    // 
    ////////////private String[] BuildMessages(UInt16 messagecount)
    ////////////{ // Content Messages
    ////////////  String[] Messages = new String[messagecount];
    ////////////  for (Int32 MI = 0; MI < messagecount; MI++)
    ////////////  {
    ////////////    Messages[MI] = String.Format("#{0}/{1}<{2}>", 1 + MI, messagecount, FUCNhdUdpTextServer.GetMessage());
    ////////////  }
    ////////////  return Messages;
    ////////////}

    ////////////private Double[,] BuildArrayDouble2D(UInt16 tupelcount)
    ////////////{ // Content Array2D
    ////////////  Double[,] DataArray = new Double[tupelcount, 2];
    ////////////  for (Int32 TI = 0; TI < tupelcount; TI++)
    ////////////  {
    ////////////    DataArray[TI, 0] = TI + 0.5 - FRandom.NextDouble();
    ////////////    DataArray[TI, 1] = 0.5 + 1.0 * DataArray[TI, 0];
    ////////////  }
    ////////////  return DataArray;
    ////////////}

    //////////////private void BuildArrayDouble2D(Int32 datasize, String[] datanames,
    //////////////                                String[] dataunits, out Double[,] datavector)
    //////////////{
    //////////////  datavector = new Double[datasize, CUdpTextArrayBase.DIMENSION_VECTOR2D];
    //////////////  for (Int32 TI = 0; TI < datasize; TI++)
    //////////////  {
    //////////////    datavector[TI, 0] = TI + 0.5 - FRandom.NextDouble();
    //////////////    datavector[TI, 1] = 0.5 + 1.0 * datavector[TI, 0];
    //////////////  }
    //////////////}

    //////////////private void UdpTextArrayServerOnBuildDoubleArray2D(Int32 datasize,
    //////////////                                                    String[] datanames,
    //////////////                                                    String[] dataunits,
    //////////////                                                    out Double[,] datavector)
    //////////////{
    //////////////  BuildArrayDouble2D(datasize, datanames, dataunits, out datavector);
    //////////////}
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //#########################################################################
    //  Segment - Callback - UDP
    //#########################################################################
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UdpTextDeviceServer
    //------------------------------------------------------------------------
    // 
    private void UdpTextDeviceServerOnDatagramTransmitted(IPEndPoint ipendpointtarget,
                                                           Byte[] datagram, Int32 size)
    {
      String Line = String.Format("TxdUdpTextDeviceServer[{0}]:",
                                  CNetwork.IPEndPointToText(ipendpointtarget));
      FUCNotifier.Write(Line);
      Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
      Char[] Separators = new Char[1];
      Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
      String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
      foreach (String Tag in Tags)
      {
        FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
      }
    }

    private void UdpTextDeviceServerOnDatagramReceived(IPEndPoint ipendpointsource,
                                                        Byte[] datagram, Int32 size)
    {
      String Line = String.Format("RxdUdpTextDeviceServer[{0}]:",
                                  CNetwork.IPEndPointToText(ipendpointsource));
      FUCNotifier.Write(Line);
      Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
      Char[] Separators = new Char[1];
      Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
      String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
      foreach (String Tag in Tags)
      {
        FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
      }
    }
    ////////////////
    ////////////////------------------------------------------------------------------------
    ////////////////  Section - Callback - UdpTextArrayServer
    ////////////////------------------------------------------------------------------------
    //////////////// 
    //////////////private void UdpTextArrayServerOnDatagramTransmitted(IPEndPoint ipendpointtarget,
    //////////////                                                     Byte[] datagram, Int32 size)
    //////////////{
    //////////////  String Line = String.Format("TxdUdpTextArrayServer[{0}]:",
    //////////////                              CNetwork.IPEndPointToText(ipendpointtarget));
    //////////////  FUCNotifier.Write(Line);
    //////////////  Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
    //////////////  Char[] Separators = new Char[1];
    //////////////  Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
    //////////////  String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //////////////  foreach (String Tag in Tags)
    //////////////  {
    //////////////    FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
    //////////////  }
    //////////////}

    //////////////private void UdpTextArrayServerOnDatagramReceived(IPEndPoint ipendpointsource,
    //////////////                                                  Byte[] datagram, Int32 size)
    //////////////{
    //////////////  String Line = String.Format("RxdUdpTextArrayServer[{0}]:",
    //////////////                              CNetwork.IPEndPointToText(ipendpointsource));
    //////////////  FUCNotifier.Write(Line);
    //////////////  Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
    //////////////  Char[] Separators = new Char[1];
    //////////////  Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
    //////////////  String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //////////////  foreach (String Tag in Tags)
    //////////////  {
    //////////////    FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
    //////////////  }
    //////////////}    
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UdpBinaryDeviceServer
    //------------------------------------------------------------------------
    // 
    //////////private void UdpBinaryDeviceServerOnDatagramTransmitted(IPEndPoint ipendpointtarget,
    //////////                                                         Byte[] datagram, Int32 size)
    //////////{
    //////////  String Line = String.Format("TxdUdpBinaryDeviceServer[{0}]:",
    //////////                              CNetwork.IPEndPointToText(ipendpointtarget));
    //////////  FUCNotifier.Write(Line);
    //////////  // NOT HERE Byte[]-Analysis !!!!
    //////////  //Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
    //////////  //Char[] Separators = new Char[1];
    //////////  //Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
    //////////  //String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //////////  //foreach (String Tag in Tags)
    //////////  //{
    //////////  //  FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
    //////////  //}
    //////////}

    //////////private void UdpBinaryDeviceServerOnDatagramReceived(IPEndPoint ipendpointsource,
    //////////                                                      Byte[] datagram, Int32 size)
    //////////{
    //////////  String Line = String.Format("RxdUdpBinaryDeviceServer[{0}]:",
    //////////                              CNetwork.IPEndPointToText(ipendpointsource));
    //////////  FUCNotifier.Write(Line);
    //////////  // NOT HERE Byte[]-Analysis !!!!
    //////////  //Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
    //////////  //Char[] Separators = new Char[1];
    //////////  //Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
    //////////  //String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //////////  //foreach (String Tag in Tags)
    //////////  //{
    //////////  //  FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
    //////////  //}
    //////////}
    //
    //#########################################################################
    //  Segment - Callback - UserControl
    //#########################################################################
    ////////////////////////////////
    ////////////////////////////////------------------------------------------------------------------------
    ////////////////////////////////  Section - Callback - UCNhdUdpTextServer
    ////////////////////////////////------------------------------------------------------------------------
    //////////////////////////////// 
    //////////////////////////////private void UCNhdUdpTextServerOnSendMessage()
    //////////////////////////////{
    //////////////////////////////  const Int32 MESSAGECOUNT = 1;
    //////////////////////////////  String[] Messages = BuildMessages(MESSAGECOUNT);
    //////////////////////////////  FUdpTextDeviceServer.PresetMessages(Messages);
    //////////////////////////////}
    ////////////////////////////////
    ////////////////////////////////------------------------------------------------------------------------
    ////////////////////////////////  Section - Callback - UCNhdUdpArrayServer
    ////////////////////////////////------------------------------------------------------------------------
    //////////////////////////////// 
    //////////////////////////////private void UCNhdUdpArrayServerOnSendArray2DSmall()
    //////////////////////////////{
    //////////////////////////////  const Int32 TUPELCOUNT = 4;
    //////////////////////////////  //////////////////String[] DataNames = new String[2];
    //////////////////////////////  //////////////////DataNames[0] = "Location"; DataNames[1] = "Signal";
    //////////////////////////////  //////////////////String[] DataUnits = new String[2];
    //////////////////////////////  //////////////////DataUnits[0] = "mm"; DataUnits[1] = "V";
    //////////////////////////////  Double[,] DataArray = BuildArrayDouble2D(TUPELCOUNT);
    //////////////////////////////  //////////////////FUdpTextArrayServer.PresetArrayDouble2D(DataNames, DataUnits, DataArray);
    //////////////////////////////  FUdpTextDeviceServer.PresetVector2DDouble(DataArray);
    //////////////////////////////}

    //////////////////////////////private void UCNhdUdpArrayServerOnSendArray2DLarge()
    //////////////////////////////{
    //////////////////////////////  const Int32 TUPELCOUNT = 128;
    //////////////////////////////  ////////////String[] DataNames = new String[2];
    //////////////////////////////  ////////////DataNames[0] = "Location"; DataNames[1] = "Signal";
    //////////////////////////////  ////////////String[] DataUnits = new String[2];
    //////////////////////////////  ////////////DataUnits[0] = "mm"; DataUnits[1] = "V";
    //////////////////////////////  Double[,] DataArray = BuildArrayDouble2D(TUPELCOUNT);
    //////////////////////////////  //////////// FUdpTextArrayServer.PresetArrayDouble2D(DataNames, DataUnits, DataArray);
    //////////////////////////////  FUdpTextDeviceServer.PresetVector2DDouble(DataArray);
    //////////////////////////////}
    //}
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          tbcMain.SelectedIndex = 1;
          return true;
        case 1:
          return true;
        case 2:
          //////FUCNotifier.Write("*** UdpBinary - Transmit Content:");
          //////CTagBinaryList Content = new CTagBinaryList();
          //////Content.Add(new CTBScalarDouble(1.2345));
          //////Content.Add(new CTBScalarDouble(314E-2));
          //////Content.Add(new CTBScalarDouble(9.87654321));
          //////CStringList StringList = Content.GetInfo();
          //////foreach (String S in StringList) FUCNotifier.Write(S);
          //////FUdpBinaryDeviceServer.PresetContent(Content);
          return true;
        case 3:
          ////////FUCNotifier.Write("*** UdpBinary - Transmit Messages:");
          ////////String[] Messages = new String[3];
          ////////Messages[0] = "AAAAAAAA";
          ////////Messages[1] = "BBBBBBBB";
          ////////Messages[2] = "CCCCCCCC";
          ////////foreach (String S in Messages)
          ////////{
          ////////  FUCNotifier.Write(S);
          ////////}
          ////////FUdpBinaryDeviceServer.PresetMessages(Messages);
          return true;
        case 4:
          //////////FUCNotifier.Write("*** UdpBinary - Transmit Vector2DDouble:");
          //////////Int32 L0 = 300;
          //////////Int32 L1 = 2;
          //////////Double[,] Vector2D = new Double[L0, L1];
          //////////for (Int32 I0 = 0; I0 < L0; I0++)
          //////////{
          //////////  for (Int32 I1 = 0; I1 < L1; I1++)
          //////////  {
          //////////    Vector2D[I0, I1] = I0 + 100 + (Double)(0.123456 + I1 / 10.0);
          //////////  }
          //////////}
          //////////for (Int32 I0 = 0; I0 < L0; I0++)
          //////////{
          //////////  String Line = String.Format("Vector[{0}] =", I0); 
          //////////  for (Int32 I1 = 0; I1 < L1; I1++)
          //////////  {
          //////////    Line += String.Format(" {0}:{1:000.000}", I1, Vector2D[I0, I1]); 
          //////////  }
          //////////  FUCNotifier.Write(Line);
          //////////}
          //////////FUdpBinaryDeviceServer.PresetVector2DDouble(Vector2D);
          return true;
        case 5: // THIS NOT!!!!!!!!!!!!!!!
          ////////////////////FUCNotifier.Write("*** UdpText - Transmit Vector2DDouble:");
          ////////////////////Int32 L0 = 3;
          ////////////////////Int32 L1 = 2;
          ////////////////////Double[,] Vector2D = new Double[L0, L1];
          ////////////////////for (Int32 I0 = 0; I0 < L0; I0++)
          ////////////////////{
          ////////////////////  for (Int32 I1 = 0; I1 < L1; I1++)
          ////////////////////  {
          ////////////////////    Vector2D[I0, I1] = I0 + 100 + (Double)(0.123456 + I1 / 10.0);
          ////////////////////  }
          ////////////////////}
          ////////////////////for (Int32 I0 = 0; I0 < L0; I0++)
          ////////////////////{
          ////////////////////  String Line = String.Format("Vector[{0}] =", I0); 
          ////////////////////  for (Int32 I1 = 0; I1 < L1; I1++)
          ////////////////////  {
          ////////////////////    Line += String.Format(" {0}:{1:000.000}", I1, Vector2D[I0, I1]); 
          ////////////////////  }
          ////////////////////  FUCNotifier.Write(Line);
          ////////////////////}
          ////////////////////FUdpTextDeviceServer.PresetVector2DDouble(Vector2D);
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }


  }
}
