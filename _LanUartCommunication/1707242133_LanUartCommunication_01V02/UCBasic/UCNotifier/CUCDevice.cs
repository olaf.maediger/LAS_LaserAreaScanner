﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCNotifier
{ //
  //-----------------------------------------------------
  //  Segment - Type
  //-----------------------------------------------------
  //
  public delegate void DOnDeviceCancel(RDeviceData data);
  public delegate void DOnDeviceConfirm(RDeviceData data);
  //
  public partial class CUCDevice : UserControl
  { //
    //-----------------------------------------------------
    //  Segment - Constant
    //-----------------------------------------------------
    //
    private const String STRING_EMPTY = "";
    private Color COLOR_READONLY = SystemColors.Info;
    private Color COLOR_READWRITE = SystemColors.Window;
    //
    //-----------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------
    //
    private CDevicelist FDevicelist;
    private DOnDeviceCancel FOnDeviceCancel;
    private DOnDeviceConfirm FOnDeviceConfirm;
    private Boolean FModeDelete;
    private RDeviceData FDeviceData;
    //
    //-----------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------
    //
    public void SetDevicelist(CDevicelist value)
    {
      FDevicelist = value;
    }

    public void SetOnDeviceCancel(DOnDeviceCancel value)
    {
      FOnDeviceCancel = value;
    }

    public void SetOnDeviceConfirm(DOnDeviceConfirm value)
    {
      FOnDeviceConfirm = value;
    }

    public String Title
    {
      get { return gbxDevice.Text; }
      set { gbxDevice.Text = value; }
    }

    public String ConfirmTitle
    {
      get { return btnConfirm.Text; }
      set { btnConfirm.Text = value; }
    }

    public Boolean ModeDelete
    {
      get { return FModeDelete; }
      set { FModeDelete = value; }
    }
    //
    //-----------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------
    //
    public CUCDevice()
    {
      InitializeComponent();
      FDeviceData = new RDeviceData(0);
    }
    //
    //-----------------------------------------------------
    //  Segment - Event
    //-----------------------------------------------------
    //
    private void tbxTextBoxInput_TextChanged(object sender, EventArgs e)
    {
      if (FModeDelete)
      { // Delete - Name
        tbxName.BackColor = COLOR_READONLY;
        tbxName.Enabled = false;
        btnCopyApplicationName.Enabled = false;
        btnPasteApplicationName.Enabled = false;
        // Delete - SerialNumber
        nudSerialNumber.BackColor = COLOR_READONLY;
        nudSerialNumber.Enabled = false;
        btnCopySerialNumber.Enabled = false;
        btnPasteSerialNumber.Enabled = false;
        // Delete - User
        tbxUser.BackColor = COLOR_READONLY;
        tbxUser.Enabled = false;
        cbxDiscountUser.Enabled = false;
        btnCopyTargetUser.Enabled = false;
        btnPasteTargetUser.Enabled = false;
        // Delete - ProductKey
        tbxProductKey.BackColor = COLOR_READONLY;
        tbxProductKey.Enabled = false;
        btnCopyProductKey.Enabled = false;
        btnPasteProductKey.Enabled = false;
      } else
      { // Add / Edit - Name
        tbxName.BackColor = COLOR_READWRITE;
        tbxName.Enabled = true;
        btnCopyApplicationName.Enabled = true;
        btnPasteApplicationName.Enabled = true;
        // Add / Edit - SerialNumber
        nudSerialNumber.BackColor = COLOR_READWRITE;
        nudSerialNumber.Enabled = true;
        btnCopySerialNumber.Enabled = true;
        btnPasteSerialNumber.Enabled = true;
        // Add / Edit - User
        tbxUser.BackColor = COLOR_READWRITE;
        tbxUser.Enabled = true;
        cbxDiscountUser.Enabled = true;
        btnCopyTargetUser.Enabled = true;
        btnPasteTargetUser.Enabled = true;
        // Add / Edit - ProductKey
        tbxProductKey.BackColor = COLOR_READWRITE;
        tbxProductKey.Enabled = true;
        btnCopyProductKey.Enabled = true;
        btnPasteProductKey.Enabled = true;
      }
    }

    private void cbxDiscountUser_CheckedChanged(object sender, EventArgs e)
    {
      tbxTextBoxInput_TextChanged(this, null);
      tbxUser.Enabled = !cbxDiscountUser.Checked;
    }
    //
    //-----------------------------------------------------
    //  Segment - Helper
    //-----------------------------------------------------
    //
    public Boolean SetData(RDeviceData data)
    {
      FDeviceData = data;
      tbxName.Text = data.TransformData.Name;
      nudSerialNumber.Value = data.SerialNumber;
			cbxDiscountUser.Checked = data.TransformData.DiscountUser;
			tbxUser.Text = data.TransformData.User;
      tbxProductKey.Text = data.TransformData.ProductKey;
      return true;
    }

    public Boolean GetData(ref RDeviceData data)
    {
      data = FDeviceData;
      data.TransformData.Name = tbxName.Text;
      data.SerialNumber = (Int32)nudSerialNumber.Value;
			data.TransformData.DiscountUser = cbxDiscountUser.Checked;
			data.TransformData.User = tbxUser.Text;
      data.TransformData.ProductKey = tbxProductKey.Text;
      return true;
    }
    //
    //-----------------------------------------------------
    //  Segment - Menu
    //-----------------------------------------------------
    //
    private void btnCancel_Click(object sender, EventArgs e)
    {
      if (FOnDeviceCancel is DOnDeviceCancel)
      {
        GetData(ref FDeviceData);
        FOnDeviceCancel(FDeviceData);
      }
    }

    private void btnConfirm_Click(object sender, EventArgs e)
    {
      if (FOnDeviceCancel is DOnDeviceCancel)
      {
        GetData(ref FDeviceData);
        FOnDeviceConfirm(FDeviceData);
      }
    }

    private void btnCopyName_Click(object sender, EventArgs e)
    {
      Clipboard.SetText(tbxName.Text);
    }
    private void btnPasteName_Click(object sender, EventArgs e)
    {
      tbxName.Text = Clipboard.GetText();
    }

    private void btnCopyUser_Click(object sender, EventArgs e)
    {
      Clipboard.SetText(tbxUser.Text);
    }
    private void btnPasteUser_Click(object sender, EventArgs e)
    {
      tbxUser.Text = Clipboard.GetText();
    }

    private void btnCopySerialNumber_Click(object sender, EventArgs e)
    {
      Clipboard.SetText(nudSerialNumber.Value.ToString());
    }
    private void btnPasteSerialNumber_Click(object sender, EventArgs e)
    {
      Int32 SerialNumber = 0;
      Int32.TryParse(Clipboard.GetText(), out SerialNumber);
      nudSerialNumber.Value = SerialNumber;
    }

    private void btnCopyProductKey_Click(object sender, EventArgs e)
    {
      Clipboard.SetText(tbxProductKey.Text);
    }

    private void btnPasteProductKey_Click(object sender, EventArgs e)
    {
      tbxProductKey.Text = Clipboard.GetText();
    }
    //
    //-----------------------------------------------------
    //  Segment - Management
    //-----------------------------------------------------
    //
    public void RefreshControls()
    {
      tbxTextBoxInput_TextChanged(this, null);
    }


  }
}
