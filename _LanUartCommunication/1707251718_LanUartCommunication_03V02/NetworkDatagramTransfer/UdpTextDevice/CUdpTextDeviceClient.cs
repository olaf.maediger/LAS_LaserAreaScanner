﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UdpTextDevice
{
  public class CUdpTextDeviceClient : CUdpTextDeviceBase
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpTextDeviceClient()
            : base()
    {
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public override Boolean Open()
    {
      FUdpTextTransmitter.Open(FTargetIpAddress,
                               FTargetIpPort);
      FUdpTextReceiver.Open(FSourceIpAddress,
                            FSourceIpPort);
      return true;
    }

    public override Boolean Close()
    {
      FUdpTextTransmitter.Close();
      FUdpTextReceiver.Close();
      return true;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - 
    //--------------------------------------------------------------------------
    //


    //
    //--------------------------------------------------------------------------
    //  Segment - Public Function
    //--------------------------------------------------------------------------
    //
    public Boolean Preset(String message)
    {
      try
      {
        //String SDatagram;
        //BuildDatagram(TOKEN_MESSAGES, "", message, out SDatagram);
        //FUdpTextTransmitter.PresetMessage(SDatagram);
        FUdpTextTransmitter.Preset(message);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}



//public Boolean PresetMessages(String[] messages)
//{
//  try
//  {
//    String SDatagram;
//    BuildDatagram(TOKEN_MESSAGES, "", messages, out SDatagram);
//    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUdpTextTransmitter.PresetMessage(SDatagram);
//    return true;
//  }
//  catch (Exception)
//  {
//    return false;
//  }
//}
