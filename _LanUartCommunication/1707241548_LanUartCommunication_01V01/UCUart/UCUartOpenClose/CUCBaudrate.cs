﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using Uart;
//
namespace UCUartOpenClose
{
  public delegate void DOnBaudrateChanged(EBaudrate value);

  public partial class CUCBaudrate : UserControl
  {
    private const String INIT_BAUDRATE_TEXT = "115200";

    private CNotifier FNotifier;
    private DOnBaudrateChanged FOnBaudrateChanged;

    public CUCBaudrate()
    {
      InitializeComponent();
      cbxBaudrate.Items.AddRange(CUart.BAUDRATES);
      cbxBaudrate.SelectedIndex = (int)CUart.BaudrateTextIndex(INIT_BAUDRATE_TEXT);
    }

    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }


    public void SetOnBaudrateChanged(DOnBaudrateChanged value)
    {
      FOnBaudrateChanged = value;
    }

    public EBaudrate GetBaudrate()
    {
      EBaudrate Result = (EBaudrate)CUart.BaudrateTextEnumerator(cbxBaudrate.Text);
      Result = EBaudrate.br115200;
      return Result;
    }
    public void SetBaudrate(EBaudrate value)
    {
      for (Int32 SI = 0; SI < cbxBaudrate.Items.Count; SI++)
      {
        String SValue = (String)cbxBaudrate.Items[SI];
        EBaudrate BR = CUart.BaudrateTextEnumerator(SValue);
        if (value == BR)
        {
          cbxBaudrate.SelectedIndex = SI;
          return;
        }
      }
    }



    private void cbxBaudrate_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FOnBaudrateChanged is DOnBaudrateChanged)
      {
        FOnBaudrateChanged(GetBaudrate());
      }
    }


  }
}
