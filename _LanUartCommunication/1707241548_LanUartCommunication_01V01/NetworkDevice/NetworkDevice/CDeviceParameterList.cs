﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace NetworkDevice
{
  public class CDeviceParameterList : List<CDeviceParameter>
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private String FName;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CDeviceParameterList(String name)
    {
      FName = name;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public String Name
    {
      get { return FName; }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public void Info(CNotifier notifier)
    {
      String Line = "";
      if (0 < this.Count)
      {
        Line = String.Format("*** DeviceParameterList[{0}] ({1} element(s)):", FName, this.Count);
        notifier.Write(Line);
        foreach (CDeviceParameter DP in this)
        {
          DP.Info(notifier);
        }
      }
      else
      {
        Line = String.Format("*** DeviceParameterList[{0}] - no elements!", FName);
        notifier.Write(Line);
      }
    }
  }

}
