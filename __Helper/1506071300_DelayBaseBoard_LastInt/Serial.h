#ifndef Serial_h
#define Serial_h
//
#include "Arduino.h"
#include "Defines.h"
#include "HardwareSerial.h"
#include "USBAPI.h"
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//
#define CARRIAGE_RETURN 0x0D
#define LINE_FEED 0x0A
#define ZERO 0x00
//
#define PROMPT_NEWLINE "\r\n"
#define PROMPT_INPUT ">"
#define PROMPT_ANSWER "#"
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
class CSerialBase
{
  protected:
  Boolean FLineDetected;
  
  public:
  virtual bool Open() = 0;
  virtual void WriteCharacter(char character) = 0;
  virtual void WriteText(char *text) = 0;
  virtual void WriteLine(char *text) = 0;
  virtual int GetRxdByteCount() = 0;
  virtual char ReadCharacter() = 0;

  void WriteNewLine();
  void WritePrompt();
  void WriteAnswer();
  Boolean DetectRxdCommandLine();
  Boolean IsLineDetected();
  void ResetLineDetected();
};
//
//----------------------------------------------------
//  Segment - CSerialZ (^= Serial)
//----------------------------------------------------
//
class CSerialZ : CSerialBase
{
  protected:
  UARTClass* FPSerial;
  
  public:
  CSerialZ(UARTClass *serial)
  {
    FPSerial = serial;
  }
  
  bool Open();
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteLine(char *text);
  int GetRxdByteCount();
  char ReadCharacter();
};
//
//----------------------------------------------------
//  Segment - CSerialS (^= Serial1 / Serial2 / Serial3)
//----------------------------------------------------
//
class CSerialS : CSerialBase
{
  protected:
  USARTClass* FPSerial;
  
  public:
  CSerialS(USARTClass *serial)
  {
    FPSerial = serial;
  }
  
  bool Open();
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteLine(char *text);
  int GetRxdByteCount();
  char ReadCharacter();
};
//
//----------------------------------------------------
//  Segment - CSerialU (^=SerialUSB)
//----------------------------------------------------
//
class CSerialU : CSerialBase
{
  protected:
  Serial_* FPSerial;
  
  public:
  CSerialU(Serial_ *serial)
  {
    FPSerial = serial;
  } 

  bool Open();
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteLine(char *text);
  int GetRxdByteCount();
  char ReadCharacter();
};
//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
class CSerial
{
  protected:
  CSerialBase* FPSerial;
  
  public:
  CSerial(UARTClass *serial);
  CSerial(UARTClass &serial);
  CSerial(USARTClass *serial);
  CSerial(USARTClass &serial);
  CSerial(Serial_ *serial);
  CSerial(Serial_ &serial);
  bool Open();
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteLine(char *text);
  void WriteNewLine();
  void WriteAnswer();
  void WritePrompt();
  int GetRxdByteCount();
  char ReadCharacter();
  Boolean DetectRxdCommandLine();
};
//
#endif
