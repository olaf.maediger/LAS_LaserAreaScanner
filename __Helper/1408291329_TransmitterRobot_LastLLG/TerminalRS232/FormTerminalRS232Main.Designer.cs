﻿namespace TerminalRS232
{
  partial class FormTerminalRS232Main
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.FHelpProvider = new System.Windows.Forms.HelpProvider();
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.mitSystem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
      this.mitSQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.mitConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCDefault = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCResetToDefaultConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveAutomaticAtProgramEnd = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCStartup = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPEditParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPClearProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
      this.diagnosticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPLoadDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPSaveDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSendDiagnosticEmail = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
      this.mitAction = new System.Windows.Forms.ToolStripMenuItem();
      this.mitAOpenClose = new System.Windows.Forms.ToolStripMenuItem();
      this.mitAStartStop = new System.Windows.Forms.ToolStripMenuItem();
      this.mitAProperties = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHelp = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHContents = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHTopic = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHSearch = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
      this.mitHEnterApplicationProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterDeviceProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
      this.mitHAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.DialogSaveInitdata = new System.Windows.Forms.SaveFileDialog();
      this.tmrStartup = new System.Windows.Forms.Timer(this.components);
      this.DialogLoadInitdata = new System.Windows.Forms.OpenFileDialog();
      this.pnlProtocol = new System.Windows.Forms.Panel();
      this.splProtocol = new System.Windows.Forms.Splitter();
      this.tbcMain = new System.Windows.Forms.TabControl();
      this.tbpConfiguration = new System.Windows.Forms.TabPage();
      this.pnlCommandTerminal = new System.Windows.Forms.Panel();
      this.btnExecuteProgramTerminal = new System.Windows.Forms.Button();
      this.btnTranslateProgramTerminal = new System.Windows.Forms.Button();
      this.btnLoadProgramTerminal = new System.Windows.Forms.Button();
      this.FUCComPort = new UCComPort.CUCComPort();
      this.tbpCommunication = new System.Windows.Forms.TabPage();
      this.FUCTerminal = new UCTerminal.CUCTerminal();
      this.tbpSerialNumberProductKey = new System.Windows.Forms.TabPage();
      this.FUCSerialNumber = new UCSerialNumber.CUCSerialNumber();
      this.cbxAutomate = new System.Windows.Forms.CheckBox();
      this.btnDebugVariablelist = new System.Windows.Forms.Button();
      this.btnDebugConstantlist = new System.Windows.Forms.Button();
      this.mstMain.SuspendLayout();
      this.tbcMain.SuspendLayout();
      this.tbpConfiguration.SuspendLayout();
      this.pnlCommandTerminal.SuspendLayout();
      this.tbpCommunication.SuspendLayout();
      this.tbpSerialNumberProductKey.SuspendLayout();
      this.SuspendLayout();
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSystem,
            this.mitConfiguration,
            this.mitProtocol,
            this.mitAction,
            this.mitHelp});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(724, 24);
      this.mstMain.TabIndex = 105;
      this.mstMain.Text = "System";
      // 
      // mitSystem
      // 
      this.mitSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.mitSQuit});
      this.mitSystem.Name = "mitSystem";
      this.mitSystem.Size = new System.Drawing.Size(57, 20);
      this.mitSystem.Text = "&System";
      // 
      // toolStripMenuItem3
      // 
      this.toolStripMenuItem3.Name = "toolStripMenuItem3";
      this.toolStripMenuItem3.Size = new System.Drawing.Size(137, 6);
      // 
      // mitSQuit
      // 
      this.mitSQuit.Name = "mitSQuit";
      this.mitSQuit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
      this.mitSQuit.Size = new System.Drawing.Size(140, 22);
      this.mitSQuit.Text = "&Quit";
      // 
      // mitConfiguration
      // 
      this.mitConfiguration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitCDefault,
            this.mitCResetToDefaultConfiguration,
            this.mitCSaveAutomaticAtProgramEnd,
            this.mitCStartup,
            this.mitCLoadStartupConfiguration,
            this.mitCSaveStartupConfiguration,
            this.mitCShowEditStartupConfiguration,
            this.toolStripMenuItem7,
            this.mitCLoadSelectableConfiguration,
            this.mitCSaveSelectableConfiguration,
            this.mitCShowEditSelectableConfiguration});
      this.mitConfiguration.Name = "mitConfiguration";
      this.mitConfiguration.Size = new System.Drawing.Size(93, 20);
      this.mitConfiguration.Text = "&Configuration";
      // 
      // mitCDefault
      // 
      this.mitCDefault.Enabled = false;
      this.mitCDefault.Name = "mitCDefault";
      this.mitCDefault.Size = new System.Drawing.Size(300, 22);
      this.mitCDefault.Text = "- Default -";
      // 
      // mitCResetToDefaultConfiguration
      // 
      this.mitCResetToDefaultConfiguration.Name = "mitCResetToDefaultConfiguration";
      this.mitCResetToDefaultConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
      this.mitCResetToDefaultConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCResetToDefaultConfiguration.Text = "Reset to Default-Configuration";
      // 
      // mitCSaveAutomaticAtProgramEnd
      // 
      this.mitCSaveAutomaticAtProgramEnd.Name = "mitCSaveAutomaticAtProgramEnd";
      this.mitCSaveAutomaticAtProgramEnd.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveAutomaticAtProgramEnd.Text = "Save automatic at program end";
      // 
      // mitCStartup
      // 
      this.mitCStartup.Enabled = false;
      this.mitCStartup.Name = "mitCStartup";
      this.mitCStartup.Size = new System.Drawing.Size(300, 22);
      this.mitCStartup.Text = "- Startup -";
      // 
      // mitCLoadStartupConfiguration
      // 
      this.mitCLoadStartupConfiguration.Name = "mitCLoadStartupConfiguration";
      this.mitCLoadStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
      this.mitCLoadStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadStartupConfiguration.Text = "Load Startup-Configuration";
      // 
      // mitCSaveStartupConfiguration
      // 
      this.mitCSaveStartupConfiguration.Name = "mitCSaveStartupConfiguration";
      this.mitCSaveStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
      this.mitCSaveStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveStartupConfiguration.Text = "Save Startup-Configuration";
      // 
      // mitCShowEditStartupConfiguration
      // 
      this.mitCShowEditStartupConfiguration.Name = "mitCShowEditStartupConfiguration";
      this.mitCShowEditStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditStartupConfiguration.Text = "Show / Edit Startup-Configuration";
      // 
      // toolStripMenuItem7
      // 
      this.toolStripMenuItem7.Enabled = false;
      this.toolStripMenuItem7.Name = "toolStripMenuItem7";
      this.toolStripMenuItem7.Size = new System.Drawing.Size(300, 22);
      this.toolStripMenuItem7.Text = "- Named -";
      // 
      // mitCLoadSelectableConfiguration
      // 
      this.mitCLoadSelectableConfiguration.Name = "mitCLoadSelectableConfiguration";
      this.mitCLoadSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadSelectableConfiguration.Text = "Load Selectable-Configuration ";
      // 
      // mitCSaveSelectableConfiguration
      // 
      this.mitCSaveSelectableConfiguration.Name = "mitCSaveSelectableConfiguration";
      this.mitCSaveSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveSelectableConfiguration.Text = "Save Selectable-Configuration";
      // 
      // mitCShowEditSelectableConfiguration
      // 
      this.mitCShowEditSelectableConfiguration.Name = "mitCShowEditSelectableConfiguration";
      this.mitCShowEditSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditSelectableConfiguration.Text = "Show / Edit Selectable-Configuration";
      // 
      // mitProtocol
      // 
      this.mitProtocol.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitPEditParameter,
            this.mitPClearProtocol,
            this.mitPCopyToClipboard,
            this.toolStripMenuItem1,
            this.diagnosticToolStripMenuItem,
            this.mitPLoadDiagnosticFile,
            this.mitPSaveDiagnosticFile,
            this.mitSendDiagnosticEmail,
            this.toolStripMenuItem2});
      this.mitProtocol.Name = "mitProtocol";
      this.mitProtocol.Size = new System.Drawing.Size(64, 20);
      this.mitProtocol.Text = "&Protocol";
      // 
      // mitPEditParameter
      // 
      this.mitPEditParameter.Name = "mitPEditParameter";
      this.mitPEditParameter.Size = new System.Drawing.Size(171, 22);
      this.mitPEditParameter.Text = "Edit Parameter";
      // 
      // mitPClearProtocol
      // 
      this.mitPClearProtocol.Name = "mitPClearProtocol";
      this.mitPClearProtocol.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
      this.mitPClearProtocol.Size = new System.Drawing.Size(171, 22);
      this.mitPClearProtocol.Text = "Clear";
      // 
      // mitPCopyToClipboard
      // 
      this.mitPCopyToClipboard.Name = "mitPCopyToClipboard";
      this.mitPCopyToClipboard.Size = new System.Drawing.Size(171, 22);
      this.mitPCopyToClipboard.Text = "Copy to Clipboard";
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new System.Drawing.Size(168, 6);
      // 
      // diagnosticToolStripMenuItem
      // 
      this.diagnosticToolStripMenuItem.Enabled = false;
      this.diagnosticToolStripMenuItem.Name = "diagnosticToolStripMenuItem";
      this.diagnosticToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
      this.diagnosticToolStripMenuItem.Text = "- Diagnostic -";
      // 
      // mitPLoadDiagnosticFile
      // 
      this.mitPLoadDiagnosticFile.Name = "mitPLoadDiagnosticFile";
      this.mitPLoadDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPLoadDiagnosticFile.Text = "Load File";
      // 
      // mitPSaveDiagnosticFile
      // 
      this.mitPSaveDiagnosticFile.Name = "mitPSaveDiagnosticFile";
      this.mitPSaveDiagnosticFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
      this.mitPSaveDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPSaveDiagnosticFile.Text = "Save File";
      // 
      // mitSendDiagnosticEmail
      // 
      this.mitSendDiagnosticEmail.Name = "mitSendDiagnosticEmail";
      this.mitSendDiagnosticEmail.Size = new System.Drawing.Size(171, 22);
      this.mitSendDiagnosticEmail.Text = "Send Email";
      // 
      // toolStripMenuItem2
      // 
      this.toolStripMenuItem2.Name = "toolStripMenuItem2";
      this.toolStripMenuItem2.Size = new System.Drawing.Size(168, 6);
      // 
      // mitAction
      // 
      this.mitAction.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitAOpenClose,
            this.mitAStartStop,
            this.mitAProperties});
      this.mitAction.Enabled = false;
      this.mitAction.Name = "mitAction";
      this.mitAction.Size = new System.Drawing.Size(54, 20);
      this.mitAction.Text = "Action";
      // 
      // mitAOpenClose
      // 
      this.mitAOpenClose.Name = "mitAOpenClose";
      this.mitAOpenClose.Size = new System.Drawing.Size(132, 22);
      this.mitAOpenClose.Text = "OpenClose";
      // 
      // mitAStartStop
      // 
      this.mitAStartStop.Name = "mitAStartStop";
      this.mitAStartStop.Size = new System.Drawing.Size(132, 22);
      this.mitAStartStop.Text = "StartStop";
      // 
      // mitAProperties
      // 
      this.mitAProperties.Name = "mitAProperties";
      this.mitAProperties.Size = new System.Drawing.Size(132, 22);
      this.mitAProperties.Text = "Properties";
      // 
      // mitHelp
      // 
      this.mitHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitHContents,
            this.mitHTopic,
            this.mitHSearch,
            this.toolStripMenuItem5,
            this.mitHEnterApplicationProductKey,
            this.mitHEnterDeviceProductKey,
            this.toolStripMenuItem4,
            this.mitHAbout});
      this.mitHelp.Name = "mitHelp";
      this.mitHelp.Size = new System.Drawing.Size(44, 20);
      this.mitHelp.Text = "Help";
      // 
      // mitHContents
      // 
      this.mitHContents.Name = "mitHContents";
      this.mitHContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.mitHContents.Size = new System.Drawing.Size(234, 22);
      this.mitHContents.Text = "Contents";
      // 
      // mitHTopic
      // 
      this.mitHTopic.Name = "mitHTopic";
      this.mitHTopic.Size = new System.Drawing.Size(234, 22);
      this.mitHTopic.Text = "Topic";
      // 
      // mitHSearch
      // 
      this.mitHSearch.Name = "mitHSearch";
      this.mitHSearch.Size = new System.Drawing.Size(234, 22);
      this.mitHSearch.Text = "Search";
      // 
      // toolStripMenuItem5
      // 
      this.toolStripMenuItem5.Name = "toolStripMenuItem5";
      this.toolStripMenuItem5.Size = new System.Drawing.Size(231, 6);
      // 
      // mitHEnterApplicationProductKey
      // 
      this.mitHEnterApplicationProductKey.Name = "mitHEnterApplicationProductKey";
      this.mitHEnterApplicationProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterApplicationProductKey.Text = "Enter Application Product-Key";
      // 
      // mitHEnterDeviceProductKey
      // 
      this.mitHEnterDeviceProductKey.Name = "mitHEnterDeviceProductKey";
      this.mitHEnterDeviceProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterDeviceProductKey.Text = "Enter Device Product-Key";
      // 
      // toolStripMenuItem4
      // 
      this.toolStripMenuItem4.Name = "toolStripMenuItem4";
      this.toolStripMenuItem4.Size = new System.Drawing.Size(231, 6);
      // 
      // mitHAbout
      // 
      this.mitHAbout.Name = "mitHAbout";
      this.mitHAbout.ShortcutKeys = System.Windows.Forms.Keys.F2;
      this.mitHAbout.Size = new System.Drawing.Size(234, 22);
      this.mitHAbout.Text = "About";
      // 
      // DialogSaveInitdata
      // 
      this.DialogSaveInitdata.DefaultExt = "ini.xml";
      this.DialogSaveInitdata.FileName = "Initdata";
      this.DialogSaveInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogSaveInitdata.Title = "MDesign: Save Initdata";
      // 
      // tmrStartup
      // 
      this.tmrStartup.Interval = 1;
      // 
      // DialogLoadInitdata
      // 
      this.DialogLoadInitdata.DefaultExt = "ini.xml";
      this.DialogLoadInitdata.FileName = "Initdata";
      this.DialogLoadInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogLoadInitdata.Title = "MDesign: Load Initdata";
      // 
      // pnlProtocol
      // 
      this.pnlProtocol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlProtocol.Location = new System.Drawing.Point(0, 473);
      this.pnlProtocol.Name = "pnlProtocol";
      this.pnlProtocol.Size = new System.Drawing.Size(724, 129);
      this.pnlProtocol.TabIndex = 112;
      // 
      // splProtocol
      // 
      this.splProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splProtocol.Location = new System.Drawing.Point(0, 470);
      this.splProtocol.Name = "splProtocol";
      this.splProtocol.Size = new System.Drawing.Size(724, 3);
      this.splProtocol.TabIndex = 113;
      this.splProtocol.TabStop = false;
      // 
      // tbcMain
      // 
      this.tbcMain.Alignment = System.Windows.Forms.TabAlignment.Bottom;
      this.tbcMain.Controls.Add(this.tbpConfiguration);
      this.tbcMain.Controls.Add(this.tbpCommunication);
      this.tbcMain.Controls.Add(this.tbpSerialNumberProductKey);
      this.tbcMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcMain.Location = new System.Drawing.Point(0, 24);
      this.tbcMain.Name = "tbcMain";
      this.tbcMain.SelectedIndex = 0;
      this.tbcMain.Size = new System.Drawing.Size(724, 446);
      this.tbcMain.TabIndex = 114;
      // 
      // tbpConfiguration
      // 
      this.tbpConfiguration.Controls.Add(this.pnlCommandTerminal);
      this.tbpConfiguration.Controls.Add(this.FUCComPort);
      this.tbpConfiguration.Location = new System.Drawing.Point(4, 4);
      this.tbpConfiguration.Name = "tbpConfiguration";
      this.tbpConfiguration.Padding = new System.Windows.Forms.Padding(3);
      this.tbpConfiguration.Size = new System.Drawing.Size(716, 420);
      this.tbpConfiguration.TabIndex = 8;
      this.tbpConfiguration.Text = "Configuration";
      this.tbpConfiguration.UseVisualStyleBackColor = true;
      // 
      // pnlCommandTerminal
      // 
      this.pnlCommandTerminal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlCommandTerminal.Controls.Add(this.btnDebugVariablelist);
      this.pnlCommandTerminal.Controls.Add(this.btnDebugConstantlist);
      this.pnlCommandTerminal.Controls.Add(this.btnExecuteProgramTerminal);
      this.pnlCommandTerminal.Controls.Add(this.btnTranslateProgramTerminal);
      this.pnlCommandTerminal.Controls.Add(this.btnLoadProgramTerminal);
      this.pnlCommandTerminal.Location = new System.Drawing.Point(12, 157);
      this.pnlCommandTerminal.Name = "pnlCommandTerminal";
      this.pnlCommandTerminal.Size = new System.Drawing.Size(399, 120);
      this.pnlCommandTerminal.TabIndex = 1;
      // 
      // btnExecuteProgramTerminal
      // 
      this.btnExecuteProgramTerminal.Location = new System.Drawing.Point(19, 77);
      this.btnExecuteProgramTerminal.Name = "btnExecuteProgramTerminal";
      this.btnExecuteProgramTerminal.Size = new System.Drawing.Size(158, 23);
      this.btnExecuteProgramTerminal.TabIndex = 2;
      this.btnExecuteProgramTerminal.Text = "Execute Program Terminal";
      this.btnExecuteProgramTerminal.UseVisualStyleBackColor = true;
      this.btnExecuteProgramTerminal.Click += new System.EventHandler(this.btnExecuteProgramTerminal_Click);
      // 
      // btnTranslateProgramTerminal
      // 
      this.btnTranslateProgramTerminal.Location = new System.Drawing.Point(19, 48);
      this.btnTranslateProgramTerminal.Name = "btnTranslateProgramTerminal";
      this.btnTranslateProgramTerminal.Size = new System.Drawing.Size(158, 23);
      this.btnTranslateProgramTerminal.TabIndex = 1;
      this.btnTranslateProgramTerminal.Text = "Translate Program Terminal";
      this.btnTranslateProgramTerminal.UseVisualStyleBackColor = true;
      this.btnTranslateProgramTerminal.Click += new System.EventHandler(this.btnTranslateProgramTerminal_Click);
      // 
      // btnLoadProgramTerminal
      // 
      this.btnLoadProgramTerminal.Location = new System.Drawing.Point(19, 19);
      this.btnLoadProgramTerminal.Name = "btnLoadProgramTerminal";
      this.btnLoadProgramTerminal.Size = new System.Drawing.Size(158, 23);
      this.btnLoadProgramTerminal.TabIndex = 0;
      this.btnLoadProgramTerminal.Text = "Load Program Terminal";
      this.btnLoadProgramTerminal.UseVisualStyleBackColor = true;
      this.btnLoadProgramTerminal.Click += new System.EventHandler(this.btnLoadProgramTerminal_Click);
      // 
      // FUCComPort
      // 
      this.FUCComPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.FUCComPort.Location = new System.Drawing.Point(0, 0);
      this.FUCComPort.Name = "FUCComPort";
      this.FUCComPort.Size = new System.Drawing.Size(447, 137);
      this.FUCComPort.TabIndex = 0;
      // 
      // tbpCommunication
      // 
      this.tbpCommunication.Controls.Add(this.FUCTerminal);
      this.tbpCommunication.Location = new System.Drawing.Point(4, 4);
      this.tbpCommunication.Name = "tbpCommunication";
      this.tbpCommunication.Padding = new System.Windows.Forms.Padding(3);
      this.tbpCommunication.Size = new System.Drawing.Size(716, 420);
      this.tbpCommunication.TabIndex = 0;
      this.tbpCommunication.Text = "Communication";
      this.tbpCommunication.UseVisualStyleBackColor = true;
      // 
      // FUCTerminal
      // 
      this.FUCTerminal.DelayCharacter = 0;
      this.FUCTerminal.DelayCR = 0;
      this.FUCTerminal.DelayLF = 0;
      this.FUCTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCTerminal.Location = new System.Drawing.Point(3, 3);
      this.FUCTerminal.Name = "FUCTerminal";
      this.FUCTerminal.Size = new System.Drawing.Size(710, 414);
      this.FUCTerminal.TabIndex = 0;
      // 
      // tbpSerialNumberProductKey
      // 
      this.tbpSerialNumberProductKey.Controls.Add(this.FUCSerialNumber);
      this.tbpSerialNumberProductKey.Location = new System.Drawing.Point(4, 4);
      this.tbpSerialNumberProductKey.Name = "tbpSerialNumberProductKey";
      this.tbpSerialNumberProductKey.Padding = new System.Windows.Forms.Padding(3);
      this.tbpSerialNumberProductKey.Size = new System.Drawing.Size(716, 420);
      this.tbpSerialNumberProductKey.TabIndex = 7;
      this.tbpSerialNumberProductKey.Text = "SerialNumber/ProductKey";
      this.tbpSerialNumberProductKey.UseVisualStyleBackColor = true;
      // 
      // FUCSerialNumber
      // 
      this.FUCSerialNumber.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCSerialNumber.Location = new System.Drawing.Point(3, 3);
      this.FUCSerialNumber.Name = "FUCSerialNumber";
      this.FUCSerialNumber.Size = new System.Drawing.Size(710, 64);
      this.FUCSerialNumber.TabIndex = 0;
      // 
      // cbxAutomate
      // 
      this.cbxAutomate.AutoSize = true;
      this.cbxAutomate.Location = new System.Drawing.Point(328, 5);
      this.cbxAutomate.Name = "cbxAutomate";
      this.cbxAutomate.Size = new System.Drawing.Size(71, 17);
      this.cbxAutomate.TabIndex = 115;
      this.cbxAutomate.Text = "Automate";
      this.cbxAutomate.UseVisualStyleBackColor = true;
      // 
      // btnDebugVariablelist
      // 
      this.btnDebugVariablelist.Location = new System.Drawing.Point(183, 48);
      this.btnDebugVariablelist.Name = "btnDebugVariablelist";
      this.btnDebugVariablelist.Size = new System.Drawing.Size(158, 23);
      this.btnDebugVariablelist.TabIndex = 116;
      this.btnDebugVariablelist.Text = "Debug Variablelist";
      this.btnDebugVariablelist.UseVisualStyleBackColor = true;
      this.btnDebugVariablelist.Click += new System.EventHandler(this.btnDebugVariablelist_Click);
      // 
      // btnDebugConstantlist
      // 
      this.btnDebugConstantlist.Location = new System.Drawing.Point(183, 19);
      this.btnDebugConstantlist.Name = "btnDebugConstantlist";
      this.btnDebugConstantlist.Size = new System.Drawing.Size(158, 23);
      this.btnDebugConstantlist.TabIndex = 3;
      this.btnDebugConstantlist.Text = "Debug Constantlist";
      this.btnDebugConstantlist.UseVisualStyleBackColor = true;
      this.btnDebugConstantlist.Click += new System.EventHandler(this.btnDebugConstantlist_Click);
      // 
      // FormTerminalRS232Main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(724, 602);
      this.Controls.Add(this.cbxAutomate);
      this.Controls.Add(this.tbcMain);
      this.Controls.Add(this.splProtocol);
      this.Controls.Add(this.pnlProtocol);
      this.Controls.Add(this.mstMain);
      this.Name = "FormTerminalRS232Main";
      this.Text = "Form1";
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.tbcMain.ResumeLayout(false);
      this.tbpConfiguration.ResumeLayout(false);
      this.pnlCommandTerminal.ResumeLayout(false);
      this.tbpCommunication.ResumeLayout(false);
      this.tbpSerialNumberProductKey.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.HelpProvider FHelpProvider;
    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem mitSystem;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
    private System.Windows.Forms.ToolStripMenuItem mitSQuit;
    private System.Windows.Forms.ToolStripMenuItem mitConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCDefault;
    private System.Windows.Forms.ToolStripMenuItem mitCResetToDefaultConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveAutomaticAtProgramEnd;
    private System.Windows.Forms.ToolStripMenuItem mitCStartup;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPEditParameter;
    private System.Windows.Forms.ToolStripMenuItem mitPClearProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPCopyToClipboard;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem diagnosticToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitPLoadDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitPSaveDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitSendDiagnosticEmail;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
    private System.Windows.Forms.ToolStripMenuItem mitAction;
    private System.Windows.Forms.ToolStripMenuItem mitAOpenClose;
    private System.Windows.Forms.ToolStripMenuItem mitAStartStop;
    private System.Windows.Forms.ToolStripMenuItem mitAProperties;
    private System.Windows.Forms.ToolStripMenuItem mitHelp;
    private System.Windows.Forms.ToolStripMenuItem mitHContents;
    private System.Windows.Forms.ToolStripMenuItem mitHTopic;
    private System.Windows.Forms.ToolStripMenuItem mitHSearch;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterApplicationProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterDeviceProductKey;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
    private System.Windows.Forms.ToolStripMenuItem mitHAbout;
    private System.Windows.Forms.SaveFileDialog DialogSaveInitdata;
    private System.Windows.Forms.Timer tmrStartup;
    private System.Windows.Forms.OpenFileDialog DialogLoadInitdata;
    private System.Windows.Forms.Panel pnlProtocol;
    private System.Windows.Forms.Splitter splProtocol;
    private System.Windows.Forms.TabControl tbcMain;
    private System.Windows.Forms.TabPage tbpCommunication;
    private System.Windows.Forms.TabPage tbpSerialNumberProductKey;
    private UCSerialNumber.CUCSerialNumber FUCSerialNumber;
    private System.Windows.Forms.CheckBox cbxAutomate;
    private UCTerminal.CUCTerminal FUCTerminal;
    private System.Windows.Forms.TabPage tbpConfiguration;
    private UCComPort.CUCComPort FUCComPort;
    private System.Windows.Forms.Button btnDebugVariablelist;
    private System.Windows.Forms.Panel pnlCommandTerminal;
    private System.Windows.Forms.Button btnLoadProgramTerminal;
    private System.Windows.Forms.Button btnExecuteProgramTerminal;
    private System.Windows.Forms.Button btnTranslateProgramTerminal;
    private System.Windows.Forms.Button btnDebugConstantlist;
  }
}

