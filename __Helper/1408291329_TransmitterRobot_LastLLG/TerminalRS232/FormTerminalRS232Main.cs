﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
//
using Initdata;
using UCNotifier;
//
using HWComPort;
using UCComPort;
using IFTerminalRS232;
//
using NLCommandTerminal;
//
namespace TerminalRS232
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormTerminalRS232Main : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;
    //
    private CIFTerminalRS232 FIFTerminalRS232;
    // FUCComPort -> static instance
    // FUCTerminal -> static instance
   //?????????????????ßß private CCommandTerminal FCommandTerminal;
    //
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      // FREEWARE:
      tbcMain.Controls.Remove(tbpSerialNumberProductKey);
      //
      FIFTerminalRS232 = new CIFTerminalRS232();
      FIFTerminalRS232.SetNotifier(FUCNotifier);
      //
      FUCTerminal.SetOnSendData(UCTerminalOnSendData);
      //
      //??????????????????????FCommandTerminal = new CCommandTerminal();
      //FCommandTerminal
      //
      FUCComPort.SetOnOpenCloseChanged(UCComPortOnOpenCloseChanged);
      //
      tmrStartup.Interval = 1000;
      //
      //
      tmrStartup.Start();
      //
      FRandom = new Random();
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FIFTerminalRS232.Close();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      // ::: Int32 IValue;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //!!!!!!!!!!!Result &= FUCComPort.LoadInitdata(initdata);
      //!!!!!!!!!!!Result &= FUCTerminal.LoadInitdata(initdata);
      Result &= FIFTerminalRS232.LoadInitdata(initdata);
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //!!!!!!!!!!Result &= FUCComPort.SaveInitdata(initdata);
      //!!!!!!!!!!Result &= FUCTerminal.SaveInitdata(initdata);
      Result &= FIFTerminalRS232.SaveInitdata(initdata);
      //
      return Result;
    }



    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUCComPort
    //------------------------------------------------------------------------
    // 
    private void UCComPortOnOpenCloseChanged(Boolean opened)
    {
      if (opened)
      { // open
        RUCComPortData UCComPortData;
        FUCComPort.GetUCComPortData(out UCComPortData);
        /*// NC FIFTerminalRS232.CommandOpenSerial(UCComPortData.PortName,
                                           UCComPortData.ComPort,
                                           UCComPortData.Baudrate,
                                           UCComPortData.Parity,
                                           UCComPortData.Databits,
                                           UCComPortData.Stopbits,
                                           UCComPortData.Handshake);*/
        // NC FIFTerminalRS232.StartExecution();
      }
      else
      { // close
        // NC FIFTerminalRS232.CommandCloseSerial();
        // NC FIFTerminalRS232.StartExecution();
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUCTerminal
    //------------------------------------------------------------------------
    // 
    private void UCTerminalOnSendData(String data, Int32 delaycharacter, Int32 delaycr, Int32 delaylf)
    { // send it over IFTerminalRS232 to HWComPort
      // NC FIFTerminalRS232.CommandTransmitTextDelay(data, delaycharacter, delaycr, delaylf);
      // NC FIFTerminalRS232.StartExecution();
    }

    private delegate void CBUCTerminalAddRxData(String data);
    public void UCTerminalAddRxData(String data)
    {
      if (this.InvokeRequired)
      {
        CBUCTerminalAddRxData CB = new CBUCTerminalAddRxData(UCTerminalAddRxData);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCTerminal.AddRxData(data);
      }
    }

    private delegate void CBUCTerminalAddTxData(String data);
    public void UCTerminalAddTxData(String data)
    {
      if (this.InvokeRequired)
      {
        CBUCTerminalAddTxData CB = new CBUCTerminalAddTxData(UCTerminalAddTxData);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCTerminal.AddTxData(data);
      }
    }

    private void IFTerminalRS232OnComPortDataReceived(String data)
    {
      UCTerminalAddRxData(data);
    }

    private void IFTerminalRS232OnComPortDataTransmitted(String data)
    {
      UCTerminalAddTxData(data);
    }
    //
    //--------------------------------------------------
    //  Segment - Helper - 
    //--------------------------------------------------
    //

    //
    //--------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }

    //
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    //--------------------------------------------
    //############################################
    //--------------------------------------------
    //  Menu-Function - ...
    //--------------------------------------------
    //############################################
    //--------------------------------------------
    // 
    //
    //--------------------------------------------------------------------------
    //  Segment - FileFormat - TextFile
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - FileFormat - TaggedBinaryFile
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - FileFormat - Excel - File
    //--------------------------------------------------------------------------

    //
    //--------------------------------------------------------------------------
    //  Segment - FileFormat - Excel - Sheet
    //--------------------------------------------------------------------------
    //
    // NC private void UCBaudrateOnBaudrateChanged(EBaudrate value)
    // NC {
    // NC }

    /*private void UCHWComPortOnPortOpen(out EBaudrate baudrate,
                                     out EParity parity,
                                     out EDatabits databits,
                                     out EStopbits stopbits,
                                     out EHandshake handshake)
    {
      baudrate = FUCBaudrate.GetBaudrate();
      parity = FUCParity.GetParity();
      databits = FUCDatabits.GetDatabits();
      stopbits = FUCStopbits.GetStopbits();
      handshake = FUCHandshake.GetHandshake();
    }

    private void UCHWComPortOnPortClose()
    {
    }*/







    //
    //------------------------------------------------------------------------
    //  Section - Command
    //------------------------------------------------------------------------
    //










    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
/* ...ok          FIFTerminalRS232.CommandSerialPortsAvailable();
          FIFTerminalRS232.StartExecution();
          CVariable VariableSPA9600 = null;
          do
          {
            VariableSPA9600 = FIFTerminalRS232.GetVariable(CSerialPortsAvailable.INIT_NAME);
            Thread.Sleep(1000);
          }
          while (null == VariableSPA9600);
          if (VariableSPA9600 is CVariableTextVector)
          {
            String PortAvailable = ((CVariableTextVector)VariableSPA9600).GetIndexText(0);
            if (4 <= PortAvailable.Length)
            {
              EComPort ComPort = CHWComPort.ComPortEnumerator(PortAvailable);
              FIFTerminalRS232.CommandOpenSerial("HC-06",
                                                  ComPort,
                                                  EBaudrate.br9600,
                                                  EParity.paNone,
                                                  EDatabits.db8,
                                                  EStopbits.sb1,
                                                  EHandshake.hsNone);
              FIFTerminalRS232.CommandCheckSerialResponse("9600", "AT", "OK");
              FIFTerminalRS232.CommandCloseSerial();
              FIFTerminalRS232.StartExecution();
              return true;
            }
          }
 */
          return false;
        case 1:
          /* CVariable VariableSPA115200 = null;
          do
          {
            VariableSPA115200 = FIFTerminalRS232.GetVariable(CSerialPortsAvailable.INIT_NAME);
            Thread.Sleep(1000);
          }
          while (null == VariableSPA115200);
          if (VariableSPA115200 is CVariableTextVector)
          {
            String PortAvailable = ((CVariableTextVector)VariableSPA115200).GetIndexText(0);
            if (4 <= PortAvailable.Length)
            {
              EComPort ComPort = CHWComPort.ComPortEnumerator(PortAvailable);
              FIFTerminalRS232.CommandOpenSerial("HC-06",
                                                  ComPort,
                                                  EBaudrate.br115200,
                                                  EParity.paNone,
                                                  EDatabits.db8,
                                                  EStopbits.sb1,
                                                  EHandshake.hsNone);
              FIFTerminalRS232.CommandCheckSerialResponse("115200", "AT", "OK");
              FIFTerminalRS232.CommandCloseSerial();
              FIFTerminalRS232.StartExecution();
              return true;
            }
          }*/
          return false;
        case 2:

         // FIFTerminalRS232.

          //??????FIFTerminalRS232.AddVariableDouble("A", 12.3);
            //??????FIFTerminalRS232.AddVariableInt32("Jumbo", 345);
            //??????FIFTerminalRS232.CheckEqual("A", "Jumbo", "BlockTrue", "BlockFalse");
          //??????FIFTerminalRS232.GetVariable(CSerialPortsAvailable.INIT_NAME);

          return false;
        case 3:
          return false;
        case 4:
          //FIFTerminalRS232.CommandCheckSerialResponse("115200", "AT", "OK");
          return true;
        case 5:
          //FIFTerminalRS232.CommandCloseSerial();
          return true;
        case 6:
          //FIFTerminalRS232.StartExecution();
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        default:
          return false;
      }
    }


    private void btnLoadProgramTerminal_Click(object sender, EventArgs e)
    {
      FIFTerminalRS232.LoadProgramFromXml("Square.pgm.xml");      
    }

    private void btnTranslateProgramTerminal_Click(object sender, EventArgs e)
    {
      FIFTerminalRS232.Translate();
    }

    private void btnExecuteProgramTerminal_Click(object sender, EventArgs e)
    {
      FIFTerminalRS232.Execute();
    }

    private void btnDebugConstantlist_Click(object sender, EventArgs e)
    {
      FIFTerminalRS232.DebugConstantlist();
    }

    private void btnDebugVariablelist_Click(object sender, EventArgs e)
    {
      FIFTerminalRS232.DebugVariablelist();
    }

      /*
        do
      {
        VariableSPA9600 = FIFTerminalRS232.GetVariable(CSerialPortsAvailable.INIT_NAME);
        Thread.Sleep(1000);
      }
      while (null == VariableSPA9600);
      if (VariableSPA9600 is CVariableTextVector)
      {
        String PortAvailable = ((CVariableTextVector)VariableSPA9600).GetIndexText(0);
        if (4 <= PortAvailable.Length)
        {
          EComPort ComPort = CHWComPort.ComPortEnumerator(PortAvailable);
          FIFTerminalRS232.CommandOpenSerial("HC-06",
                                              ComPort,
                                              EBaudrate.br9600,
                                              EParity.paNone,
                                              EDatabits.db8,
                                              EStopbits.sb1,
                                              EHandshake.hsNone);
          FIFTerminalRS232.CommandCheckSerialResponse("9600", "AT", "OK");
          FIFTerminalRS232.CommandCloseSerial();
          FIFTerminalRS232.StartExecution();
          return true;
        }
      }*/
        
    
    //
    //--------------------------------------
    //	Section - Command - Common
    //--------------------------------------
    //
    //!!!!!!!!!!  FIFTerminalRS232.CommandGetHelp();


    //!!!!!!!!!!! FIFTerminalRS232.CommandGetSoftwareVersion()
    //!!!!!!!!!!! FIFTerminalRS232.CommandGetHardwareVersion()



  }
}

