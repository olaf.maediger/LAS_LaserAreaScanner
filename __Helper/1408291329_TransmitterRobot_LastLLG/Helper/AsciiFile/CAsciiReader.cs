﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace AsciiFile
{
  public class CAsciiReader
  {
		//
		//------------------------------------------
		//	Member
		//------------------------------------------
		//
		private CNotifier FNotifier;
		// `??? private CStatementBase FParent;	// only Reference!
		private Int32 FTabulator = 0;
		private Boolean FDebugEnable = true;
		//
		//-------------------------------------
		//	Constructor
		//-------------------------------------
		//
		public CAsciiReader()
		{
			// FParent = null;
		}
		//
		//------------------------------------------
		//	Segment - Property
		//------------------------------------------
		//
		public void SetNotifier(CNotifier notifier)
		{
			FNotifier = notifier;
		}
		//
		//------------------------------------------
		//	Helper - Debug
		//------------------------------------------
		//
		private void Write(String line)
		{
			if (FDebugEnable)
			{
				String Header = "";
				for (Int32 CI = 0; CI < FTabulator; CI++)
				{
					Header += "  ";
				}
				FNotifier.Write(Header + line);
			}
		}
		private void WriteIncrement()
		{
			FTabulator++;
		}
		private void WriteDecrement()
		{
			if (0 < FTabulator)
				FTabulator--;
		}

		private void DebugEnable(Boolean enable)
		{
			FDebugEnable = enable;
		}
		//
		//-----------------------------------------------
		//
		//	Build-Methods (with looking for Constants)
		//
		//-----------------------------------------------
		//

  }
}
