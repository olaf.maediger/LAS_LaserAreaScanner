﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using UCNotifier;
using NLExpression;
using NLStatement;
//
namespace XmlFile
{
  public class CXmlReader
	{
		//
		//------------------------------------------
		//	Member
		//------------------------------------------
		//
		private CNotifier FNotifier;
		private Int32 FTabulator = 0;
		private Boolean FDebugEnable = true;
    private DOnReadCommandBlock FOnReadCommandBlock;
    //
    private CConstantlist FConstantlist;
    private CVariablelist FVariablelist;
    private CExpression FExpression;
    private CStatement FStatement;
		//
		//-------------------------------------
		//	Constructor
		//-------------------------------------
		//
		public CXmlReader()
		{
			//FParent = null;
		}
		//
		//------------------------------------------
		//	Segment - Property
		//------------------------------------------
		//
		public void SetNotifier(CNotifier notifier)
		{
			FNotifier = notifier;
		}

    public void SetOnReadCommandBlock(DOnReadCommandBlock value)
    {
      FOnReadCommandBlock = value;
    }

    /*protected CStatementBase Parent
    {
      get { return FParent; }
    }*/

    protected CNotifier Notifier
    {
      get { return FNotifier; }
    }
		//
		//------------------------------------------
		//	Helper - Debug
		//------------------------------------------
		//
    public void Write(String line)
		{
			if (FDebugEnable)
			{
				String Header = "";
				for (Int32 CI = 0; CI < FTabulator; CI++)
				{
					Header += "  ";
				}
				FNotifier.Write(Header + line);
			}
		}
    public void WriteIncrement()
		{
			FTabulator++;
		}
    public void WriteDecrement()
		{
			if (0 < FTabulator)
				FTabulator--;
		}

    public void DebugEnable(Boolean enable)
		{
			FDebugEnable = enable;
		}
		//
		//-----------------------------------------------
		//
		//	Build-Methods (with looking for Constants)
		//
		//-----------------------------------------------
		//
    static public String TransformString(String value)
		{ // filtern von "\r", "\n"
			return value.Replace("\\r\\n", "\r\n");
		}
		// --- String ---
    static public String BuildString(String value)
		{
			String Result = CVariable.INIT_STRING;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueString();
			}*/
			Result = TransformString(value);
			return Result;
		}

		// --- Boolean ---
    static public Boolean BuildBoolean(String value)
		{
			Boolean Result = CVariable.INIT_BOOLEAN;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueBoolean();
			}*/
			if (Boolean.TryParse(value, out Result))
			{
				return Result;
			}
			switch (value)
			{
				case "TRUE":
					return true;
				case "True":
					return true;
				case "true":
					return true;
				case "FALSE":
					return false;
				case "False":
					return false;
				case "false":
					return false;
			}
			return Result;
		}

		// --- Byte ---
    static public Byte BuildByte(String value)
		{
			Byte Result = CVariable.INIT_BYTE;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueByte();
			}*/
			if (Byte.TryParse(value, out Result))
			{
				return Result;
			}
			return Result;
		}

		// --- Int16 ---
    static public Int16 BuildInt16(String value)
		{
			Int16 Result = CVariable.INIT_INT16;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueInt16();
			}*/
			if (Int16.TryParse(value, out Result))
			{
				return Result;
			}
			return Result;
		}

		// --- Int32 ---
    static public Int32 BuildInt32(String value)
		{
			Int32 Result = CVariable.INIT_INT32;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueInt32();
			}*/
			if (Int32.TryParse(value, out Result))
			{
				return Result;
			}
			return Result;
		}

		// --- Double ---
    static public Double BuildDouble(String value)
		{
			Double Result = CVariable.INIT_DOUBLE;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueDouble();
			}*/
			if (Double.TryParse(value, out Result))
			{
				return Result;
			}
			return Result;
		}










		private Boolean ReadVariable(XmlNode nodebase,
																 out CVariable variable)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			String Name = "";
			String Type = "";
			String Preset = "";
			variable = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				switch (NodeChild.Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CExpressionHeader.NAME_NAME:
						Name = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Name);
						Write(Line);
						break;
          case CExpressionHeader.NAME_TYPE:
						Type = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Type);
						Write(Line);
						break;
          case CExpressionHeader.NAME_PRESET:
						Preset = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Preset);
						Write(Line);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadVariable!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			Result &= (0 < Name.Length);
			Result &= (0 < Type.Length);
			if (Result)
			{
				switch (Type)
				{
					/*case EType.String.ToString():
						// variable = new CVariableString()
						break;*/
					case CVariable.NAME_DOUBLE:
						variable = new CVariableDouble(Name, Preset);
						break;
					default: // Error
						Result = false;
						break;
				}			 
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      //
      return Result;
		}





		private Boolean ReadExpression(XmlNode nodebase)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
					case CExpressionHeader.NAME_VARIABLE:
						CVariable Variable;
						Result &= ReadVariable(NodeChild, out Variable);
						if (Result)
						{
							Result &= (Variable is CVariable);
							if (Result)
							{
                //!!!!!!!!!!!!!!!!FParent.AddVariable(Variable);
							}
						}
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadExpression!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      //
      return Result;
		}











		private Boolean ReadCommandSet(XmlNode nodebase,
																	 out CCommandSet command)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
      String VName = "";
      String VType = "";
      String VExpression = "";
			command = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CExpressionHeader.NAME_NAME:
            VName = BuildString(NodeChild.InnerText);
            Line = String.Format("<{0}={1}>", NodeChild.Name, VName);
            Write(Line);
            break;
          case CExpressionHeader.NAME_TYPE:
            VType = BuildString(NodeChild.InnerText);
            Line = String.Format("<{0}={1}>", NodeChild.Name, VType);
            Write(Line);
            break;
          case CExpressionHeader.NAME_PRESET:
						VExpression = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, VExpression);
						Write(Line);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadCommandSet!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
      Result &= (0 < VName.Length);
      Result &= (0 < VType.Length);
      Result &= (0 < VExpression.Length);
			if (Result)
			{
				command = new CCommandSet(VName, VType, VExpression);
				command.SetNotifier(FNotifier);
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      //
      return Result;
		}

		private Boolean ReadCommandWriteLine(XmlNode nodebase,
																				 out CCommandWriteLine command)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			String Format = "";
			String Expression = "";
			command = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CExpressionHeader.NAME_FORMAT:
						Format = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Format);
						Write(Line);
						break;
          case CExpressionHeader.NAME_EXPRESSION:
						Expression = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Expression);
						Write(Line);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadCommandSet!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			Result &= (0 < Format.Length);
			Result &= (0 < Expression.Length);
			if (Result)
			{
        //!!!!!!!!!!!!!!!!command = new CCommandWriteLine(FParent.Variablelist, Format, Expression);
				command.SetNotifier(FNotifier);
			}
			//
			WriteDecrement();
			return Result;
		}

		private Boolean ReadCommandWhile(XmlNode nodebase,
																		 out CCommandWhile command)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			String Expression = "";
			CCommandBlock Block = null;
			command = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
					case CExpressionHeader.NAME_EXPRESSION:
						Expression = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Expression);
						Write(Line);
						break;
					case CStatementHeader.NAME_BLOCK:
						Result &= ReadCommandBlock(NodeChild, out Block);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadCommandSet!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			Result &= (0 < Expression.Length);
			Result &= (Block is CCommandBlock);
			if (Result)
			{
        //!!!!!!!!!!!!!!!!command = new CCommandWhile(FParent.Variablelist, Expression, Block);
				command.SetNotifier(FNotifier);
			}
			//
			WriteDecrement();
			return Result;
		}

		private Boolean ReadCommandIf(XmlNode nodebase,
																	out CCommandIf command)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			String Expression = "";
			CCommandBlock BlockTrue = null;
			CCommandBlock BlockFalse = null;
			command = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CExpressionHeader.NAME_EXPRESSION:
						Expression = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Expression);
						Write(Line);
						break;
          case CStatementHeader.NAME_BLOCKTRUE:
						Result &= ReadCommandBlock(NodeChild, out BlockTrue);
						break;
          case CStatementHeader.NAME_BLOCKFALSE:
						Result &= ReadCommandBlock(NodeChild, out BlockFalse);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadCommandSet!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			Result &= (0 < Expression.Length);
			Result &= ((BlockTrue is CCommandBlock) || (BlockFalse is CCommandBlock));
			if (Result)
			{
        //!!!!!!!!!!!!!!!!command = new CCommandIf(FParent.Variablelist, Expression, BlockTrue, BlockFalse);
				command.SetNotifier(FNotifier);
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      //
      return Result;
		}

		private Boolean ReadCommandFor(XmlNode nodebase,
																	 out CCommandFor command)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			String Variable = "";
			String Expression = "";
			String Condition = "";
			String Increment = "";
			CCommandBlock Block = null;
			command = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
					case CExpressionHeader.NAME_VARIABLE:
						Variable = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Variable);
						Write(Line);
						break;
          case CExpressionHeader.NAME_EXPRESSION:
						Expression = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Expression);
						Write(Line);
						break;
          case CStatementHeader.NAME_CONDITION:
						Condition = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Condition);
						Write(Line);
						break;
          case CStatementHeader.NAME_INCREMENT:
						Increment = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Increment);
						Write(Line);
						break;
          case CStatementHeader.NAME_BLOCK:
						Result &= ReadCommandBlock(NodeChild, out Block);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadCommandFor!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
      Result &= (0 < Variable.Length);
      Result &= (0 < Expression.Length);
      Result &= (0 < Condition.Length);
      Result &= (0 < Increment.Length);
      Result &= (Block is CCommandBlock);
			if (Result)
			{
				command = new CCommandFor(//FParent.Variablelist,
																	Variable, Expression, Condition, Increment, Block);
				command.SetNotifier(FNotifier);
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      //
      return Result;
		}

		private Boolean ReadCommandBlock(XmlNode nodebase,
																		 out CCommandBlock commandblock)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			commandblock = new CCommandBlock();//FParent.Variablelist);
			commandblock.SetNotifier(FNotifier);
			//
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CStatementHeader.NAME_SET:
            CCommandSet CommandSet;
            Result &= ReadCommandSet(NodeChild, out CommandSet);
            Result &= (CommandSet is CCommandSet);
            if (Result)
            {
              commandblock.AddCommand(CommandSet);
            }
            break;
          case CStatementHeader.NAME_WHILE:
            CCommandWhile CommandWhile;
            Result &= ReadCommandWhile(NodeChild, out CommandWhile);
            Result &= (CommandWhile is CCommandWhile);
            if (Result)
            {
              commandblock.AddCommand(CommandWhile);
            }
            break;
          case CStatementHeader.NAME_IF:
            CCommandIf CommandIf;
            Result &= ReadCommandIf(NodeChild, out CommandIf);
            Result &= (CommandIf is CCommandIf);
            if (Result)
            {
              commandblock.AddCommand(CommandIf);
            }
            break;
          case CStatementHeader.NAME_WRITELINE:
            CCommandWriteLine CommandWriteLine;
            Result &= ReadCommandWriteLine(NodeChild, out CommandWriteLine);
            Result &= (CommandWriteLine is CCommandWriteLine);
            if (Result)
            {
              commandblock.AddCommand(CommandWriteLine);
            }
            break;
          case CStatementHeader.NAME_FOR:
            CCommandFor CommandFor;
            Result &= ReadCommandFor(NodeChild, out CommandFor);
            Result &= (CommandFor is CCommandFor);
            if (Result)
            {
              commandblock.AddCommand(CommandFor);
            }
            break;
          case CStatementHeader.NAME_BLOCK:
						CCommandBlock CommandBlock;
						Result &= ReadCommandBlock(NodeChild, out CommandBlock);
	  				Result &= (CommandBlock is CCommandBlock);
		  			if (Result)
			  		{
							commandblock.AddCommand(CommandBlock);
						}
						break;
					default:
            Result = false;
            if (FOnReadCommandBlock is DOnReadCommandBlock)
            { //  Extensions:
              Result = FOnReadCommandBlock(NodeChild, ref commandblock);
            }
            if (!Result)
            {
              DebugEnable(true);
              Write("ERROR - ReadCommandBlock!");
              DebugEnable(false);
              Result = false;
            }
						break;
				}
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      return Result;
		}













	
		
		/*
		
		private Boolean ReadStatement(XmlNode nodebase)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
					case CXmlHeader.NAME_BLOCK:
						CCommandBlock CommandBlock;
						Result &= ReadCommandBlock(NodeChild, out CommandBlock);
						if (Result)
						{
							Result &= (CommandBlock is CCommandBlock);
							if (Result)
							{
								FParent.AddCommand(CommandBlock);
							}
						}
						break;
					case CXmlHeader.NAME_SET:
						CCommandSet CommandSet;
						Result &= ReadCommandSet(NodeChild, out CommandSet);
						if (Result)
						{
							Result &= (CommandSet is CCommandSet);
							if (Result)
							{
								FParent.AddCommand(CommandSet);
							}
						}
						break;
					case CXmlHeader.NAME_WHILE:
						CCommandWhile CommandWhile;
						Result &= ReadCommandWhile(NodeChild, out CommandWhile);
						if (Result)
						{
							Result &= (CommandWhile is CCommandWhile);
							if (Result)
							{
								FParent.AddCommand(CommandWhile);
							}
						}
						break;
					case CXmlHeader.NAME_IF:
						break;
					case CXmlHeader.NAME_WRITELINE:
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadExpression!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			//
			WriteDecrement();
			return Result;
		}

		*/
















		//
		//------------------------------------------
		//	Central Read for all Nodes
		//------------------------------------------
		//
		// Xml -> read all ojects 
		public Boolean Load(String filename,
                        ref CConstantlist constantlist,
                        ref CVariablelist variablelist,
                        ref CExpression expression,
                        ref CStatement statement)    
		{
			Boolean Result = true;
			//
      FConstantlist = constantlist;
      FVariablelist = variablelist;
      FExpression = expression;
      FStatement = statement;
      //
			DebugEnable(true);
			//
      // NC FParent = parent;
			XmlDocument XMLDocument = new XmlDocument();
			XMLDocument.Load(filename);
			//
			// Clear all Container
			//
      // NC FParent.ClearVariablelist();
      // NC FParent.ClearCommandlist();
			//
			// Detection Root
			//
      XmlNode NodeRoot = XMLDocument.SelectSingleNode(CStatementHeader.NAME_PROGRAM);
      String Line = String.Format("<{0}>", NodeRoot.LocalName);
      Write(Line);
      WriteIncrement();
			foreach (XmlNode NodeChild in NodeRoot.ChildNodes)
			{
				// debug FNotifier.Write("----> {0}", NodeChild.Name);
				switch (NodeChild.Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CExpressionHeader.NAME_EXPRESSION:
            CExpression Expression;
            Result = false;
            /*Result &= ReadExpression(NodeChild, out Expression);
            if (Result)
            {
              FExpression.AddExpression(Expression);
            }*/
						break;
          //case CStatementHeader.NAME_STATEMENT:
          case CStatementHeader.NAME_BLOCK:
            CCommandBlock CommandBlock = null;
            Result &= ReadCommandBlock(NodeChild, out CommandBlock);
            Result &= (CommandBlock is CCommandBlock);
            if (Result)
            {
              Result &= FStatement.AddCommandBlock(CommandBlock);
            }
						break;
					default:
						DebugEnable(true);
						Write(String.Format("ERROR - ReadAll: Unknown tag <{0}>!", NodeChild.Name));
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			WriteDecrement();
      Line = String.Format("</{0}>", NodeRoot.LocalName);
      Write(Line);
      return Result;
		}






	}
}
