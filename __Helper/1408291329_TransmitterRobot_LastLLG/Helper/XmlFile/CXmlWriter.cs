﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using UCNotifier;
using NLExpression;
using NLStatement;
//
namespace XmlFile
{
	public class CXmlWriter
	{
		//
		//------------------------------------------
		//	Segment - Variable
		//------------------------------------------
		//
		private CNotifier FNotifier;
		private CStatementBase FParent;	// only Reference!
		private Int32 FTabulator = 0;
		private Boolean FDebugEnable = true;
		//
		//-------------------------------------
		//	Segment - Constructor
		//-------------------------------------
		//
		public CXmlWriter()
		{
			FParent = null;
		}
		//
		//------------------------------------------
		//	Segment - Property
		//------------------------------------------
		//
		public void SetNotifier(CNotifier notifier)
		{
			FNotifier = notifier;
		}
		//
		//------------------------------------------
		//	Segment - Helper 
		//------------------------------------------
		//
		private void Write(String line)
		{
			if (FDebugEnable)
			{
				String Header = "";
				for (Int32 CI = 0; CI < FTabulator; CI++)
				{
					Header += "  ";
				}
			  FNotifier.Write(Header + line);
      }
		}
		private void WriteIncrement()
		{
			FTabulator++;
		}
		private void WriteDecrement()
		{
			if (0 < FTabulator)
				FTabulator--;
		}

		private void EnableProtocol(Boolean value)
		{
			FDebugEnable = value;
		}
		//
		//
		//------------------------------------------
		//	Write: Scalar-Datatypes
		//------------------------------------------
		//
		private Boolean WriteBoolean(XmlDocument xmldocument,
																 XmlNode nodebase,
																 String header,
																 Boolean value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value.ToString();
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteString(XmlDocument xmldocument,
																XmlNode nodebase,
																String header,
																String value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value;
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteInt16(XmlDocument xmldocument,
															 XmlNode nodebase,
															 String header,
															 Int16 value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value.ToString();
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteInt32(XmlDocument xmldocument,
															 XmlNode nodebase,
															 String header,
															 Int32 value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value.ToString();
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteDouble(XmlDocument xmldocument,
																XmlNode nodebase,
																String header,
																Double value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value.ToString();
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteByte(XmlDocument xmldocument,
															XmlNode nodebase,
															String header,
															Byte value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value.ToString();
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteEnumeration(XmlDocument xmldocument,
																		 XmlNode nodebase,
																		 String header,
																		 String value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value;
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}
		//
		//
		//----------------------------------------------
		//	Write: Basic-Struct-Level
		//----------------------------------------------
		//
		/* NC private Boolean WriteARGB(XmlDocument xmldocument,
															XmlNode nodebase,
															Byte alpha, Byte red, Byte green, Byte blue)
		{
			Boolean Result = true;
			//	Alpha
			WriteByte(xmldocument, nodebase, CXmlHeader.NAME_ALPHA, alpha);
			//	Red
			WriteByte(xmldocument, nodebase, CXmlHeader.NAME_RED, red);
			//	Green
			WriteByte(xmldocument, nodebase, CXmlHeader.NAME_GREEN, green);
			//	Blue
			WriteByte(xmldocument, nodebase, CXmlHeader.NAME_BLUE, blue);
			//
			return Result;
		}*/
		//
		//----------------------------------------------
		//	Write: High-Level
		//----------------------------------------------
		//
		private Boolean WriteVariable(XmlDocument xmldocument,
																	XmlNode nodeparent,
																	CVariable variable)
		{
			Boolean Result = true;
			// Base
      XmlNode NodeBase = xmldocument.CreateElement(CExpressionHeader.NAME_VARIABLE);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			// Name
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_NAME, variable.Name);
			// Type
      WriteEnumeration(xmldocument, NodeBase, CExpressionHeader.NAME_TYPE, variable.Type.ToString());
			// Preset
			switch (variable.Type)
			{
        case ETypeVariable.Byte:
          WriteByte(xmldocument, NodeBase, CExpressionHeader.NAME_PRESET, variable.GetPresetByte());
					break;
        case ETypeVariable.Boolean:
          WriteBoolean(xmldocument, NodeBase, CExpressionHeader.NAME_PRESET, variable.GetPresetBoolean());
					break;
        case ETypeVariable.Int16:
          WriteInt16(xmldocument, NodeBase, CExpressionHeader.NAME_PRESET, variable.GetPresetInt16());
					break;
        case ETypeVariable.Int32:
          WriteInt32(xmldocument, NodeBase, CExpressionHeader.NAME_PRESET, variable.GetPresetInt32());
					break;
        case ETypeVariable.Double:
          WriteDouble(xmldocument, NodeBase, CExpressionHeader.NAME_PRESET, variable.GetPresetDouble());
					break;
			}
			//
			WriteDecrement();
			return Result;
		}




		private Boolean WriteCommandSet(XmlDocument xmldocument,
																	  XmlNode nodeparent,
																	  CCommandSet command)
		{
			Boolean Result = true;
			// Base
			XmlNode NodeBase = xmldocument.CreateElement(command.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			//
			// Variable
			WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_VARIABLE, command.Variable);
			// Expression
			WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_EXPRESSION, command.TextExpression);
			//
			WriteDecrement();
			return Result;
		}

		private Boolean WriteCommandWriteLine(XmlDocument xmldocument,
																					XmlNode nodeparent,
																					CCommandWriteLine command)
		{
			Boolean Result = true;
			// Base
			XmlNode NodeBase = xmldocument.CreateElement(command.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			//
			// Format
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_FORMAT, command.Format);
			// Variable
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_EXPRESSION, command.Expression);
			//
			WriteDecrement();
			return Result;
		}


		private Boolean WriteCommandWhile(XmlDocument xmldocument,
																			XmlNode nodeparent,
																			CCommandWhile command)
		{
			Boolean Result = true;
			// Base
			XmlNode NodeBase = xmldocument.CreateElement(command.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			//
			WriteIncrement();
			// Expression
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_EXPRESSION, command.Expression);
			// CommandBlock
			WriteCommandBlock(xmldocument, NodeBase, command.CommandBlock);
			//
			WriteDecrement();
			return Result;
		}




		private Boolean WriteCommandIf(XmlDocument xmldocument,
																	 XmlNode nodeparent,
																	 CCommandIf command)
		{
			Boolean Result = true;
			// Base
			XmlNode NodeBase = xmldocument.CreateElement(command.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			//
			WriteIncrement();
			// Expression
			WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_EXPRESSION, command.Expression);
			// BlockTrue
			WriteCommandBlock(xmldocument, NodeBase, command.BlockTrue);
			// BlockFalse
			if (command.BlockFalse is CCommand)
			{
				WriteCommandBlock(xmldocument, NodeBase, command.BlockFalse);
			}
			//
			WriteDecrement();
			return Result;
		}

		private Boolean WriteCommandFor(XmlDocument xmldocument,
																	  XmlNode nodeparent,
																		CCommandFor command)
		{
			Boolean Result = true;
			// Base
			XmlNode NodeBase = xmldocument.CreateElement(command.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			//
			WriteIncrement();
			// Variable
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_VARIABLE, command.Variable);
			// Expression
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_EXPRESSION, command.Expression);
      // Condition
      WriteString(xmldocument, NodeBase, CStatementHeader.NAME_CONDITION, command.Condition);
      // Increment
      WriteString(xmldocument, NodeBase, CStatementHeader.NAME_INCREMENT, command.Increment);
      // Block
			WriteCommandBlock(xmldocument, NodeBase, command.CommandBlock);
			//
			WriteDecrement();
			return Result;
		}




		private Boolean WriteCommandBlock(XmlDocument xmldocument,
																			XmlNode nodeparent,
																			CCommandBlock commandblock)
		{
			Boolean Result = true;
			// Base
			XmlNode NodeBase = xmldocument.CreateElement(commandblock.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			//
			foreach (CCommand Command in commandblock.Commandlist)
			{
				switch (Command.Name)
				{
          case CStatementHeader.NAME_SET:
						Result &= WriteCommandSet(xmldocument, NodeBase, (CCommandSet)Command);
						break;
          case CStatementHeader.NAME_WRITELINE:
						Result &= WriteCommandWriteLine(xmldocument, NodeBase, (CCommandWriteLine)Command);
						break;
          case CStatementHeader.NAME_WHILE:
						Result &= WriteCommandWhile(xmldocument, NodeBase, (CCommandWhile)Command);
						break;
          case CStatementHeader.NAME_IF:
						Result &= WriteCommandIf(xmldocument, NodeBase, (CCommandIf)Command);
						break;
					/*case CXmlHeader.NAME_FOR:
						Result &= WriteCommandFor(xmldocument, NodeBase, Command);
						break;*/
					default: // Unknown Command
						Result = false;
						break;
				}
				if (!Result)
				{
					WriteDecrement();
					return Result;
				}
			}
			//
			WriteDecrement();
			return Result;
		}



		private Boolean WriteExpression(XmlDocument xmldocument,
																		XmlNode nodeparent)
		{
			Boolean Result = true;
			// Base
      XmlNode NodeBase = xmldocument.CreateElement(CExpressionHeader.NAME_EXPRESSION);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			//
			foreach (CVariable Variable in FParent.Variablelist)
			{
				Result &= WriteVariable(xmldocument, NodeBase, Variable);
				if (!Result)
				{
					WriteDecrement();
					return Result;
				}
			}
			//
			WriteDecrement();
			return Result;
		}

		private Boolean WriteStatement(XmlDocument xmldocument,
																	 XmlNode nodeparent)
		{
			Boolean Result = true;
			// Base
      XmlNode NodeBase = xmldocument.CreateElement(CStatementHeader.NAME_STATEMENT);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			//
			foreach (CCommand Command in FParent.Commandlist)
			{
				switch (Command.Name)
				{
          case CStatementHeader.NAME_SET:
						Result &= WriteCommandSet(xmldocument, NodeBase, (CCommandSet)Command);
						break;
          case CStatementHeader.NAME_WHILE:
						Result &= WriteCommandWhile(xmldocument, NodeBase, (CCommandWhile)Command);
						break;
          case CStatementHeader.NAME_IF:
						Result &= WriteCommandIf(xmldocument, NodeBase, (CCommandIf)Command);
						break;
          case CStatementHeader.NAME_FOR:
						Result &= WriteCommandFor(xmldocument, NodeBase, (CCommandFor)Command);
						break;
					default: // Unknown Command
						Result = false;
						break;
				}
				if (!Result)
				{
					WriteDecrement();
					return Result;
				}
			}
			//
			WriteDecrement();
			return Result;
		}
		//
		//------------------------------------------
		//	Central Write for all Nodes
		//------------------------------------------
		//
		// Save all Objects -> Xml
		public Boolean WriteProgram(String filename,
																CStatementBase parent)
		{	// Define Parent
			FParent = parent;
			//
			XmlDocument XMLDocument = new XmlDocument();
			//
			EnableProtocol(true);
			//
			// Root
			XmlNode NodeRoot;
      NodeRoot = XMLDocument.CreateElement(CStatementHeader.NAME_PROGRAM);
			XMLDocument.AppendChild(NodeRoot);
			String Line = String.Format("<{0}>[{1}]", NodeRoot.Name, NodeRoot.InnerText);
			Write(Line);
			// Declaration
			XmlDeclaration XMLDeclaration = XMLDocument.CreateXmlDeclaration(CXmlHeader.TITLE_VERSION,
																																			 CXmlHeader.TITLE_ENCODING,
																																			 CXmlHeader.TITLE_STANDALONE);
			XMLDocument.InsertBefore(XMLDeclaration, NodeRoot);
			// Comment
			XmlComment XMLComment = XMLDocument.CreateComment(CXmlHeader.TITLE_COMMENT1);
			XMLDocument.InsertBefore(XMLComment, NodeRoot);
			XMLComment = XMLDocument.CreateComment(CXmlHeader.TITLE_COMMENT2);
			XMLDocument.InsertBefore(XMLComment, NodeRoot);
			XMLComment = XMLDocument.CreateComment(CXmlHeader.TITLE_COMMENT3);
			XMLDocument.InsertBefore(XMLComment, NodeRoot);
			//
			WriteIncrement();
			WriteExpression(XMLDocument, NodeRoot);
			WriteStatement(XMLDocument, NodeRoot);
			WriteDecrement();
			//	
			XMLDocument.Save(filename);
			XMLDocument = null;
			return true;
		}
	}
}