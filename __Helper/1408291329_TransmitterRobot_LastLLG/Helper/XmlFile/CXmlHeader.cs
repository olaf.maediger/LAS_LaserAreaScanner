﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using UCNotifier;
using NLExpression;
using NLStatement;
//
namespace XmlFile
{
	//
	//------------------------------------------
	//  Types - PenStyle
	//------------------------------------------
	//  
	/* NC public enum EPenStyle
	{
		Solid = 0,
		Dash = 1,
		Dot = 2,
		DashDot = 3,
		DashDotDot = 4,
		Custom = 5
	}*/

  public delegate Boolean DOnReadCommandBlock(XmlNode nodebase,
                                              ref CCommandBlock commandblock);

  
	//
	//******************************************
	//  --- XmlHeader ---
	//******************************************
	//  
	public class CXmlHeader
	{
		//
		//------------------------------------------
		//	Constant
		//------------------------------------------
		//
		//	Header
		//
		public const String TOKEN_COMMENT = "#comment";
		public const String TITLE_VERSION = "1.0";
		public const String TITLE_ENCODING = "utf-8";
		public const String TITLE_STANDALONE = "yes";
		public const String TITLE_COMMENT1 = "Program := Expression | Statement";
		public const String TITLE_COMMENT2 = "Expression := Variables";
		public const String TITLE_COMMENT3 = "Statement := Commands";
		//
		/*/	Types
		//
		public const String STRING = "String";
		public const String BYTE = "Byte";
		public const String INT32 = "Int32";
		public const String DOUBLE = "Double";
		//
		//	Common
		//
		public const String NAME_PROGRAM = "Program";
		public const String NAME_EXPRESSION = "Expression";
		public const String NAME_STATEMENT = "Statement";
		public const String NAME_CONDITION = "Condition";
		public const String NAME_INCREMENT = "Increment";
		//
		//	Expression
		//
		public const String NAME_VARIABLE = "Variable";
		public const String NAME_NAME = "Name";
		public const String NAME_TYPE = "Type";
		public const String NAME_PRESET = "Preset";
		//
		//	Statement
		//
		public const String NAME_BLOCK = "Block";
		public const String NAME_BLOCKTRUE = "BlockTrue";
		public const String NAME_BLOCKFALSE = "BlockFalse";
		public const String NAME_SET = "Set";
		public const String NAME_FOR = "For";
		public const String NAME_WHILE = "While";
		public const String NAME_IF = "If";
		public const String NAME_WRITELINE = "WriteLine";
		//
		public const String NAME_FORMAT = "Format";
		//public const String NAME_FORMAT = "Format";
    //
    //	Statement - Extension (Library)
    //
    // -> external Library !!! public const String NAME_SQUARE = "Square";
	*/
		
	}
}
