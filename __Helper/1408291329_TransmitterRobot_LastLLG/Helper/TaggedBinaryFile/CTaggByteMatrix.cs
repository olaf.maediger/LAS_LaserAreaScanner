﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
//
namespace TaggedBinaryFile
{
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct STaggByteMatrix
  {
    public Byte Tagg;
    public UInt32 Size;
    public UInt32 RowCount;
    public UInt32 ColCount;
    public Byte[,] Values;
    //
    public STaggByteMatrix(Int32 init)
    {
      Tagg = CTaggByteMatrix.TAGG;
      Size = CTaggByteMatrix.SIZE;
      RowCount = CTaggByteMatrix.INIT_ROWCOUNT;
      ColCount = CTaggByteMatrix.INIT_COLCOUNT;
      Values = new Byte[RowCount, ColCount];
      Values[0, 0] = CTaggByteMatrix.INIT_VALUE;
    }
  }


  public class CTaggByteMatrix : CTagg
  {
    public const Byte TAGG = 0x57;
    public const UInt32 SIZE = 0x00000001; // Size of one Value (Total: rc * cc * 0x01)
    public const Byte INIT_ROWCOUNT = 0x0001;
    public const Byte INIT_COLCOUNT = 0x0001;
    public const Byte INIT_VALUE = 0x0000;

    public STaggByteMatrix FField;

    public CTaggByteMatrix()
    {
      FField = new STaggByteMatrix(0);
      CalculateSize();
    }

    public CTaggByteMatrix(Byte[,] values)
    {
      FField = new STaggByteMatrix(0);
      FField.RowCount = (UInt32)values.GetLength(0);
      FField.ColCount = (UInt32)values.GetLength(1);
      FField.Values = new Byte[FField.RowCount, FField.ColCount];
      for (Int32 RI = 0; RI < FField.RowCount; RI++)
      {
        for (Int32 CI = 0; CI < FField.ColCount; CI++)
        {
          FField.Values[RI, CI] = values[RI, CI];
        }
      }
      CalculateSize();
    }


    public Byte[,] Values
    {
      get { return FField.Values; }
    }




    private void CalculateSize()
    {
      FField.Size = FField.RowCount * FField.ColCount * sizeof(Byte);
    }


    public override Boolean Write(BinaryWriter binarywriter)
    {
      binarywriter.Write((Byte)FField.Tagg);
      binarywriter.Write((UInt32)FField.Size);
      binarywriter.Write((UInt32)FField.RowCount);
      binarywriter.Write((UInt32)FField.ColCount);
      for (Int32 RI = 0; RI < FField.RowCount; RI++)
      {
        for (Int32 CI = 0; CI < FField.ColCount; CI++)
        {
          binarywriter.Write((Byte)FField.Values[RI, CI]);
        }
      }
      return true;
    }

    public override Boolean Read(BinaryReader binaryreader)
    {
      FField.Tagg = binaryreader.ReadByte();
      if (TAGG == FField.Tagg)
      {
        FField.Size = binaryreader.ReadUInt32();
        FField.RowCount = binaryreader.ReadUInt32();
        FField.ColCount = binaryreader.ReadUInt32();
        FField.Values = new Byte[FField.RowCount, FField.ColCount];
        for (Int32 RI = 0; RI < FField.RowCount; RI++)
        {
          for (Int32 CI = 0; CI < FField.ColCount; CI++)
          {
            FField.Values[RI, CI] = binaryreader.ReadByte();
          }
        }
        return true;
      }
      return false;
    }

  }
}


