﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
//
namespace TaggedBinaryFile
{
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct STaggUInt16Matrix
  {
    public Byte Tagg;
    public UInt32 Size;
    public UInt32 RowCount;
    public UInt32 ColCount;
    public UInt16[,] Values;
    //
    public STaggUInt16Matrix(Int32 init)
    {
      Tagg = CTaggUInt16Matrix.TAGG;
      Size = CTaggUInt16Matrix.SIZE;
      RowCount = CTaggUInt16Matrix.INIT_ROWCOUNT;
      ColCount = CTaggUInt16Matrix.INIT_COLCOUNT;
      Values = new UInt16[RowCount, ColCount];
      Values[0, 0] = CTaggUInt16Matrix.INIT_VALUE;
    }
  }


  public class CTaggUInt16Matrix : CTagg
  {
    public const Byte TAGG = 0x58;
    public const UInt32 SIZE = 0x00000002; // Size of one Value (Total: rc * cc * 0x02)
    public const UInt16 INIT_ROWCOUNT = 0x0001;
    public const UInt16 INIT_COLCOUNT = 0x0001;
    public const UInt16 INIT_VALUE = 0x0000;

    public STaggUInt16Matrix FField;

    public CTaggUInt16Matrix()
    {
      FField = new STaggUInt16Matrix(0);
      CalculateSize();
    }

    public CTaggUInt16Matrix(UInt16[,] values)
    {
      FField = new STaggUInt16Matrix(0);
      FField.RowCount = (UInt32)values.GetLength(0);
      FField.ColCount = (UInt32)values.GetLength(1);
      FField.Values = new UInt16[FField.RowCount, FField.ColCount];
      for (Int32 RI = 0; RI < FField.RowCount; RI++)
      {
        for (Int32 CI = 0; CI < FField.ColCount; CI++)
        {
          FField.Values[RI, CI] = values[RI, CI];
        }
      }
      CalculateSize();
    }


    public UInt16[,] Values
    {
      get { return FField.Values; }
    }




    private void CalculateSize()
    {
      FField.Size = FField.RowCount * FField.ColCount * sizeof(UInt16);
    }


    public override Boolean Write(BinaryWriter binarywriter)
    {
      binarywriter.Write((Byte)FField.Tagg);
      binarywriter.Write((UInt32)FField.Size);
      binarywriter.Write((UInt32)FField.RowCount);
      binarywriter.Write((UInt32)FField.ColCount);
      for (Int32 RI = 0; RI < FField.RowCount; RI++)
      {
        for (Int32 CI = 0; CI < FField.ColCount; CI++)
        {
          binarywriter.Write((UInt16)FField.Values[RI, CI]);
        }
      }
      return true;
    }

    public override Boolean Read(BinaryReader binaryreader)
    {
      FField.Tagg = binaryreader.ReadByte();
      if (TAGG == FField.Tagg)
      {
        FField.Size = binaryreader.ReadUInt32();
        FField.RowCount = binaryreader.ReadUInt32();
        FField.ColCount = binaryreader.ReadUInt32();
        FField.Values = new UInt16[FField.RowCount, FField.ColCount];
        for (Int32 RI = 0; RI < FField.RowCount; RI++)
        {
          for (Int32 CI = 0; CI < FField.ColCount; CI++)
          {
            FField.Values[RI, CI] = binaryreader.ReadUInt16();
          }
        }
        return true;
      }
      return false;
    }

  }
}


