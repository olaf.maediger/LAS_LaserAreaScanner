﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
//
namespace TaggedBinaryFile
{
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct STaggString
  {
    public Byte Tagg;
    public UInt32 Size;
    public Char[] Values;
    //
    public STaggString(Int32 init)
    {
      Tagg = CTaggString.TAGG;
      Size = CTaggString.SIZE;
      Values = new Char[Size + 1];
      Values[0] = CTaggString.INIT_VALUE;
    }
  }


  public class CTaggString : CTagg
  {
    public const Byte TAGG = 0x72;
    public const UInt32 SIZE = 0x00000002; // Size of one Value [Byte]
    public const Char INIT_VALUE = ' ';

    public STaggString FField;

    public CTaggString()
    {
      FField = new STaggString(0);
    }

    public CTaggString(String value)
    {
      FField = new STaggString(0);
      FField.Size = (UInt32)(1 + value.Length); // Zero-End!!!
      FField.Values = new Char[(Int32)FField.Size];
      for (Int32 CI = 0; CI < FField.Size - 1; CI++)
      {
        FField.Values[CI] = (Char)value[CI];
      }
      FField.Values[FField.Size - 1] = (Char)0x00;
    }


    public Char[] Values
    {
      get { return FField.Values; }
    }

    public String Text
    {
      get 
      {
        String Result = "";
        for (Int32 CI = 0; CI < FField.Size - 1; CI++)
        {
          Result += FField.Values[CI];
        }
        return Result; 
      }
    }



    public override Boolean Write(BinaryWriter binarywriter)
    {
      binarywriter.Write((Byte)FField.Tagg);
      binarywriter.Write((UInt32)FField.Size);
      for (Int32 CI = 0; CI < FField.Size; CI++) // With Zero-End!
      {
        Char C = FField.Values[CI];
        binarywriter.Write(C);
      }
      return true;
    }

    public override Boolean Read(BinaryReader binaryreader)
    {
      FField.Tagg = binaryreader.ReadByte();
      if (TAGG == FField.Tagg)
      {
        FField.Size = binaryreader.ReadUInt32();
        FField.Values = new Char[FField.Size];
        for (Int32 CI = 0; CI < FField.Size; CI++) // With Zero-End!
        {
          FField.Values[CI] = binaryreader.ReadChar();
        }
        return true;
      }
      return false;
    }

  }
}


