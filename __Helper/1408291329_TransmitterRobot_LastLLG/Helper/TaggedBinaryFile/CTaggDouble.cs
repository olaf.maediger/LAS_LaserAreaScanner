﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
//
namespace TaggedBinaryFile
{
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct STaggDouble
  {
    public Byte Tagg;
    public Byte Size;
    public Double Value;
    //
    public STaggDouble(Int32 init)
    {
      Tagg = CTaggDouble.TAGG;
      Size = CTaggDouble.SIZE;
      Value = CTaggDouble.INIT_VALUE;
    }
  }

  public class CTaggDouble : CTagg
  {
    public const Byte TAGG = 0x45;
    public const Byte SIZE = 0x04;
    public const UInt16 INIT_VALUE = 0x00000000;

    public STaggDouble FField;

    public CTaggDouble()
    {
      FField = new STaggDouble(0);
    }

    public CTaggDouble(Double value)
    {
      FField = new STaggDouble(0);
      FField.Value = value;
    }


    public Double Value
    {
      get { return FField.Value; }
    }



    public override Boolean Write(BinaryWriter binarywriter)
    {
      binarywriter.Write((Byte)FField.Tagg);
      binarywriter.Write((Byte)FField.Size);
      binarywriter.Write((Double)FField.Value);
      return true;
    }

    public override Boolean Read(BinaryReader binaryreader)
    {
      FField.Tagg = binaryreader.ReadByte();
      if (TAGG == FField.Tagg)
      {
        FField.Size = binaryreader.ReadByte();
        FField.Value = binaryreader.ReadDouble();
        return true;
      }
      return false;
    }

  }
}
