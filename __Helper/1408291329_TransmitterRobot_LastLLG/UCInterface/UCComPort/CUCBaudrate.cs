﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using HWComPort;
//
namespace UCHWComPort
{
  public delegate void DOnBaudrateChanged(EBaudrate value);

  public partial class CUCBaudrate : UserControl
  {
    private const String INIT_BAUDRATE_TEXT = "9600";


    private DOnBaudrateChanged FOnBaudrateChanged;

    public CUCBaudrate()
    {
      InitializeComponent();
      cbxBaudrate.Items.AddRange(CHWComPort.BAUDRATES);
      cbxBaudrate.SelectedIndex = (int)CHWComPort.BaudrateTextIndex(INIT_BAUDRATE_TEXT);
    }

    public void SetOnBaudrateChanged(DOnBaudrateChanged value)
    {
      FOnBaudrateChanged = value;
    }

    public EBaudrate GetBaudrate()
    {
      EBaudrate Result = CHWComPort.BaudrateTextBaudrate(cbxBaudrate.Text);
      return Result;
    }
    public void SetBaudrate(EBaudrate value)
    {
      for (Int32 SI = 0; SI < cbxBaudrate.Items.Count; SI++)
      {
        String SValue = (String)cbxBaudrate.Items[SI];
        EBaudrate BR = CHWComPort.BaudrateTextBaudrate(SValue);
        if (value == BR)
        {
          cbxBaudrate.SelectedIndex = SI;
          return;
        }
      }
    }



    private void cbxBaudrate_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FOnBaudrateChanged is DOnBaudrateChanged)
      {
        FOnBaudrateChanged(GetBaudrate());
      }
    }


  }
}
