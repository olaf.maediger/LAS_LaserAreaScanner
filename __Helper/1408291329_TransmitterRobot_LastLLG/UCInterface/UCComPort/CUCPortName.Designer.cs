﻿namespace UCHWComPort
{
  partial class CUCPortName
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label4 = new System.Windows.Forms.Label();
      this.tbxPortName = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(5, 6);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(60, 13);
      this.label4.TabIndex = 62;
      this.label4.Text = "PortName :";
      // 
      // tbxPortName
      // 
      this.tbxPortName.Location = new System.Drawing.Point(66, 3);
      this.tbxPortName.Name = "tbxPortName";
      this.tbxPortName.Size = new System.Drawing.Size(266, 20);
      this.tbxPortName.TabIndex = 63;
      this.tbxPortName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.tbxPortName.TextChanged += new System.EventHandler(this.tbxPortName_TextChanged);
      // 
      // CUCPortName
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.tbxPortName);
      this.Controls.Add(this.label4);
      this.Name = "CUCPortName";
      this.Size = new System.Drawing.Size(337, 26);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox tbxPortName;
  }
}
