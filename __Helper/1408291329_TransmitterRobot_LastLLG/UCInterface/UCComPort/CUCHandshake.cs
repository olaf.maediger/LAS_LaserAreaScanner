﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using HWComPort;
//
namespace UCHWComPort
{
  public delegate void DOnHandshakeChanged(EHandshake value);

  public partial class CUCHandshake : UserControl
  {
    private const String INIT_HANDSHAKE_TEXT = "None";

    private DOnHandshakeChanged FOnHandshakeChanged;

    public CUCHandshake()
    {
      InitializeComponent();
      cbxHandshake.Items.AddRange(CHWComPort.HANDSHAKES);
      cbxHandshake.SelectedIndex = (int)CHWComPort.HandshakeTextIndex(INIT_HANDSHAKE_TEXT);
    }

    public void SetOnHandshakeChanged(DOnHandshakeChanged value)
    {
      FOnHandshakeChanged = value;
    }

    public EHandshake GetHandshake()
    {
      EHandshake Result = CHWComPort.HandshakeTextHandshake(cbxHandshake.Text);
      return Result;
    }
    public void SetHandshake(EHandshake value)
    {
      for (Int32 SI = 0; SI < cbxHandshake.Items.Count; SI++)
      {
        String SValue = (String)cbxHandshake.Items[SI];
        EHandshake PA = CHWComPort.HandshakeTextHandshake(SValue);
        if (value == PA)
        {
          cbxHandshake.SelectedIndex = SI;
          return;
        }
      }
    }



    private void cbxHandshake_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FOnHandshakeChanged is DOnHandshakeChanged)
      {
        FOnHandshakeChanged(GetHandshake());
      }
    }


  }
}
