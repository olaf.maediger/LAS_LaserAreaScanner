﻿namespace UCHWComPort
{
  partial class CUCPortsSelectable
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label5 = new System.Windows.Forms.Label();
      this.cbxPortsSelectable = new System.Windows.Forms.ComboBox();
      this.SuspendLayout();
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(5, 8);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(75, 13);
      this.label5.TabIndex = 55;
      this.label5.Text = "Port selected :";
      // 
      // cbxPortsSelectable
      // 
      this.cbxPortsSelectable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPortsSelectable.FormattingEnabled = true;
      this.cbxPortsSelectable.Location = new System.Drawing.Point(88, 5);
      this.cbxPortsSelectable.Name = "cbxPortsSelectable";
      this.cbxPortsSelectable.Size = new System.Drawing.Size(86, 21);
      this.cbxPortsSelectable.TabIndex = 54;
      // 
      // CUCPortsSelectable
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label5);
      this.Controls.Add(this.cbxPortsSelectable);
      this.Name = "CUCPortsSelectable";
      this.Size = new System.Drawing.Size(178, 31);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.ComboBox cbxPortsSelectable;
  }
}
