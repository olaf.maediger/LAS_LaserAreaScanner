﻿namespace UCHWComPort
{
  partial class CUCPortsInstalled
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label5 = new System.Windows.Forms.Label();
      this.cbxPortsInstalled = new System.Windows.Forms.ComboBox();
      this.SuspendLayout();
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(4, 8);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(78, 13);
      this.label5.TabIndex = 55;
      this.label5.Text = "Ports installed :";
      // 
      // cbxPortsInstalled
      // 
      this.cbxPortsInstalled.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPortsInstalled.FormattingEnabled = true;
      this.cbxPortsInstalled.Location = new System.Drawing.Point(84, 5);
      this.cbxPortsInstalled.Name = "cbxPortsInstalled";
      this.cbxPortsInstalled.Size = new System.Drawing.Size(86, 21);
      this.cbxPortsInstalled.TabIndex = 54;
      // 
      // CUCPortsInstalled
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label5);
      this.Controls.Add(this.cbxPortsInstalled);
      this.Name = "CUCPortsInstalled";
      this.Size = new System.Drawing.Size(174, 30);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.ComboBox cbxPortsInstalled;
  }
}
