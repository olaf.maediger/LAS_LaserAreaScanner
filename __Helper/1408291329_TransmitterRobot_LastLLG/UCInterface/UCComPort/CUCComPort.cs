﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using HWComPort;
//

namespace UCComPort
{
  public delegate void DOnOpenCloseChanged(Boolean opened);

  public struct RUCComPortData
  {
    public String PortName;
    public EComPort ComPort;
    public EBaudrate Baudrate;
    public EParity Parity;
    public EDatabits Databits;
    public EStopbits Stopbits;
    public EHandshake Handshake;
    //
    public RUCComPortData(Int32 init)
    {
      PortName = CUCComPort.INIT_PORTNAME;
      ComPort = CUCComPort.INIT_COMPORT;
      Baudrate = CUCComPort.INIT_BAUDRATE;
      Parity = CUCComPort.INIT_PARITY;
      Databits = CUCComPort.INIT_DATABITS;
      Stopbits = CUCComPort.INIT_STOPBITS;
      Handshake = CUCComPort.INIT_HANDSHAKE;
    }
  }

  public partial class CUCComPort : UserControl
  {
    public const String INIT_PORTNAME = "";
    public const EComPort INIT_COMPORT = EComPort.cp1;
    public const EBaudrate INIT_BAUDRATE = EBaudrate.br9600;
    public const EParity INIT_PARITY = EParity.paNone;
    public const EDatabits INIT_DATABITS = EDatabits.db8;
    public const EStopbits INIT_STOPBITS = EStopbits.sb1;
    public const EHandshake INIT_HANDSHAKE = EHandshake.hsNone;


    private DOnOpenCloseChanged FOnOpenCloseChanged;

    public CUCComPort()
    {
      InitializeComponent();
      //
      FUCOpenClose.SetOnOpenCloseChanged(UCOpenCloseOnOpenCloseChanged);
    }



    public void SetOnOpenCloseChanged(DOnOpenCloseChanged value)
    {
      FOnOpenCloseChanged = value;
    }


    public Boolean GetUCComPortData(out RUCComPortData data)
    {
      data = new RUCComPortData(0);
      data.PortName = FUCPortName.GetPortName();
      data.ComPort = FUCPortsSelectable.GetPortSelected();
      data.Baudrate = FUCBaudrate.GetBaudrate();
      data.Parity = FUCParity.GetParity();
      data.Databits = FUCDatabits.GetDatabits();
      data.Stopbits = FUCStopbits.GetStopbits();
      data.Handshake = FUCHandshake.GetHandshake();
      return true;
    }

    public Boolean SetUCComPortData(RUCComPortData data)
    {
      FUCPortName.SetPortName(data.PortName);
      FUCPortsSelectable.SetPortSelected(data.ComPort);
      FUCBaudrate.SetBaudrate(data.Baudrate);
      FUCParity.SetParity(data.Parity);
      FUCDatabits.SetDatabits(data.Databits);
      FUCStopbits.SetStopbits(data.Stopbits);
      FUCHandshake.SetHandshake(data.Handshake);
      return true;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //
    public void EnableComPortControls(Boolean enable)
    {
      FUCPortName.Enabled = enable;
      FUCPortsSelectable.Enabled = enable;
      FUCBaudrate.Enabled = enable;
      FUCParity.Enabled = enable;
      FUCDatabits.Enabled = enable;
      FUCStopbits.Enabled = enable;
      FUCHandshake.Enabled = enable;
      btnRefresh.Enabled = enable;
    }

    private void UCOpenCloseOnOpenCloseChanged(Boolean opened)
    {
      if (FOnOpenCloseChanged is DOnOpenCloseChanged)
      {
        FOnOpenCloseChanged(opened);
      }
    }

    private void btnRefresh_Click(object sender, EventArgs e)
    {
      FUCPortsInstalled.RefreshPortsInstalled();
      FUCPortsSelectable.RefreshPortsSelectable();
    }




  }
}
