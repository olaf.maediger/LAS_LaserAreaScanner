﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using UCNotifier;
using NLExpression;
using NLStatement;
using XmlFile;
//
namespace NLCommandTerminal
{
  public class CXmlReaderTerminal : CXmlReader
  {
    // ??? private DOnReadCommandBlock FOnReadCommandBlock;


    public CXmlReaderTerminal()
      : base()
    {
      //????FProgramName = Path.GetFileNameWithoutExtension(filename);
      // ???? return FXmlReader.ReadProgram(filename, this);
    }


    public new void SetNotifier(CNotifier value)
    {
      base.SetNotifier(value);
    }


    /*public void SetOnReadCommandBlock(DOnReadCommandBlock value)
    {
      base.SetOnReadCommandBlock(FOnReadCommandBlock);
    }*/


    public Boolean ReadCommandSquare(XmlNode nodebase,
                                     out CCommandSquare command)
    {
      WriteIncrement();
      WriteIncrement();
      Boolean Result = true;
      String Line = String.Format("<{0}>", nodebase.Name);
      Write(Line);
      //
      WriteIncrement();
      String Expression = "";
      command = null;
      foreach (XmlNode NodeChild in nodebase.ChildNodes)
      {
        String Name = NodeChild.Name;
        switch (Name)
        {
          case CXmlHeader.TOKEN_COMMENT:
            // erst mal negieren, später in Commentlist!
            break;
          case CExpressionHeader.NAME_EXPRESSION:
            Expression = CXmlReader.BuildString(NodeChild.InnerText);
            Line = String.Format("<{0}={1}>", NodeChild.Name, Expression);
            Write(Line);
            break;
          default:
            DebugEnable(true);
            Write("ERROR - ReadCommandSquare!");
            DebugEnable(false);
            Result = false;
            break;
        }
      }
      Result &= (0 < Expression.Length);
      if (Result)
      {
        command = new CCommandSquare(Expression);
        command.SetNotifier(Notifier);
      }
      //
      WriteDecrement();
      WriteDecrement();
      WriteDecrement();
      return Result;
    }


  }
}
