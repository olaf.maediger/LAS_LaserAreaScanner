﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
//
namespace NLStatement
{

	// <for> <commandset> <conditionboolean> <commandchange> <block>

	public class CCommandFor : CCommand
	{
		private CVariablelist FVariablelist;
		private String FVariableName;
		private String FTextExpression;
		private CExpression FExpression;
		private String FTextCondition;
		private CExpression FCondition;
		private String FTextIncrement;
		private CExpression FIncrement;
		private CCommandBlock FCommandBlock;

    public CCommandFor(//CVariablelist variablelist,
											 String variablename,
											 String textexpression,
											 String textcondition,
											 String textincrement,
											 CCommandBlock commandblock)
      : base(CStatementHeader.NAME_FOR)
    {
			FVariablelist = null;
			FVariableName = variablename;
			FTextExpression = textexpression;
			FExpression = null;
			FTextCondition = textcondition;
			FCondition = null;
			FTextIncrement = textincrement;
			FIncrement = null;
			FCommandBlock = commandblock;
    }


		public String Variable
		{
			get { return FVariableName; }
		}

		public String Expression
		{
			get { return FTextExpression; }
		}

		public String Condition
		{
			get { return FTextCondition; }
		}

		public String Increment
		{
			get { return FTextIncrement; }
		}

		public CCommandBlock CommandBlock
		{
			get { return FCommandBlock; }
		}





    public override Boolean Translate(ref CConstantlist constantlist,
                                      ref CVariablelist variablelist)
    {
			try
			{
        String Line = String.Format("Translate For[{0} = {1}; {2}; {3}] {{4}}", 
																		FVariableName, FTextExpression, FTextCondition, 
																		FTextIncrement, FCommandBlock.ToString());
        FNotifier.Write(Line);
				//
				FExpression = new CExpression(ref constantlist,
                                      ref variablelist);
				FExpression.Translate(FTextExpression);
				//
				FCondition = new CExpression(ref constantlist,
                                     ref variablelist);
				FCondition.Translate(FTextCondition);
				//
				FIncrement = new CExpression(ref constantlist,
                                     ref variablelist);
				FIncrement.Translate(FTextIncrement);
				//
				if (FCommandBlock is CCommandBlock)
				{
					FCommandBlock.Translate(ref constantlist,
                                  ref variablelist);
				}
        FVariablelist = variablelist;
        return true;
			}
			catch (Exception)
			{
				return false;
			}
    }

    public override Boolean Execute()
    {     
      String Line;
			Double DValue = 0.0;
			if (!FExpression.Calculate(ref DValue))
			{
				return false;
			}
			CVariable Variable = FVariablelist.FindName(FVariableName);
			if (!(Variable is CVariable))
			{
				return false;
			}
			Variable.Set(DValue);
			if (!FCondition.Calculate(ref DValue))
			{
				return false;
			}
			Boolean ForActive = (0 != (Int32)DValue);
			while (ForActive)
			{
				Line = String.Format("Execute For[{0} = {1}; {2}; {3}]({4}) -> {5}",
														 FVariableName, FTextExpression, FTextCondition,
														 FTextIncrement, Variable.Get(), ForActive);
				FNotifier.Write(Line);
				if (!FCommandBlock.Execute())
				{
					return false;
				}
				// Calculate next Step
				if (!FIncrement.Calculate(ref DValue))
				{
					return false;
				}
				Variable.Set(DValue);
				//
				if (!FCondition.Calculate(ref DValue))
				{
					return false;
				}
				ForActive = (0 != (Int32)DValue);
			}
      Line = String.Format("Execute For[{0} = {1}; {2}; {3}]({4}) -> {5}", 
                           FVariableName, FTextExpression, FTextCondition,
													 FTextIncrement, Variable.Get(), ForActive);
      FNotifier.Write(Line);
      return true;
    }


    public override Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines)
    {
      String Line = BuildIdentation(identation);
      Line += String.Format("for ({0} = {1}; {2}; {3} = {4})",
                            FVariableName, FTextExpression,
                            FTextCondition, FVariableName, FTextIncrement);
      lines.Add(Line);
      //
      Line = BuildIdentation(identation) + "{";
      lines.Add(Line);
      //
      identation++;
      if (FCommandBlock is CCommand)
      {
        FCommandBlock.WriteToAscii(ref identation, ref lines);
      }
      identation--;
      //
      Line = BuildIdentation(identation) + "}";
      lines.Add(Line);
      //
      return true;
    }


  }
}
