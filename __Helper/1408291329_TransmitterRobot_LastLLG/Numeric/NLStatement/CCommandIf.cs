﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
//
namespace NLStatement
{

  // <if> <expressionboolean> <then> <statementtrue> <else> <statementfalse>


  public class CCommandIf : CCommand
  {
		private CVariablelist FVariablelist;
    private String FTextExpression;
    private CExpression FExpression;
		private CCommandBlock FBlockTrue;
		private CCommandBlock FBlockFalse;

		public String Expression
		{
			get { return FTextExpression; }
		}

		public CCommandBlock BlockTrue
		{
			get { return FBlockTrue; }
		}

		public CCommandBlock BlockFalse
		{
			get { return FBlockFalse; }
		}


    public CCommandIf(CVariablelist variablelist,
											String textexpression,
                      CCommandBlock blocktrue,
											CCommandBlock blockfalse)
      : base(CStatementHeader.NAME_IF)
    {
			FVariablelist = variablelist;
      FTextExpression = textexpression;
      FExpression = null;
			FBlockTrue = blocktrue;
			if (FBlockTrue is CCommand)
			{
        FBlockTrue.SetName(CStatementHeader.NAME_BLOCKTRUE);
			}
			FBlockFalse = blockfalse;
			if (FBlockFalse is CCommand)
			{
        FBlockFalse.SetName(CStatementHeader.NAME_BLOCKFALSE);
			}
    }

    public override Boolean Translate(ref CConstantlist constantlist,
                                      ref CVariablelist variablelist)
    {
			try
			{
        String Line = String.Format("Translate If[{0}", FTextExpression);
				if (FBlockTrue is CCommand)
        {
					Line += String.Format(" then {0}", FBlockTrue.ToString());
        }
				if (FBlockFalse is CCommand)
        {
					Line += String.Format(" else {0}", FBlockFalse.ToString());
        }
        Line += "]";
        FNotifier.Write(Line);
        FExpression = new CExpression(ref constantlist,
                                      ref variablelist);
				FExpression.Translate(FTextExpression);
				//
				if (FBlockTrue is CCommand)
				{
          FBlockTrue.Translate(ref constantlist,
                                  ref variablelist);
				}
				if (FBlockFalse is CCommand)
				{
          FBlockFalse.Translate(ref constantlist,
                                  ref variablelist);
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
    }

    public override Boolean Execute()
    {
      Double DValue = 0.0;
      if (FExpression.Calculate(ref DValue))
      {
        if (0 != (Int32)DValue)
        { // Expression true ->
					if (FBlockTrue is CCommand)
					{
						String Line = String.Format("Execute If[true -> {0}]:", FBlockTrue.ToString());
						FNotifier.Write(Line);
						return FBlockTrue.Execute();
					}
        } else
        { // Expression false ->
					if (FBlockFalse is CCommand)
					{
						String Line = String.Format("Execute If[false -> {0}]:", FBlockFalse.ToString());
						FNotifier.Write(Line);
						return FBlockFalse.Execute();
					}
        }
      }
      return false;
    }


    public override Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines)
    {
      // If
      String Line = BuildIdentation(identation);
      Line += String.Format("if ({0})", FTextExpression);
      lines.Add(Line);
      // BlockTrue      
      Line = BuildIdentation(identation) + "{";
      lines.Add(Line);
      if (FBlockTrue is CCommand)
      {
        identation++;
        FBlockTrue.WriteToAscii(ref identation, ref lines);
        identation--;
      }
      // BlockFalse
      if (FBlockFalse is CCommand)
      { // } else
        Line = BuildIdentation(identation) + "} else";
        lines.Add(Line);
        // {
        Line = BuildIdentation(identation) + "{";
        lines.Add(Line);
        //
        identation++;
        FBlockFalse.WriteToAscii(ref identation, ref lines);
        identation--;
        // }
        Line = BuildIdentation(identation) + "}";
        lines.Add(Line);
      } else
      { // no else!
        Line = BuildIdentation(identation) + "}";
        lines.Add(Line);
      }
      //
      return true;
    }

  }
}
