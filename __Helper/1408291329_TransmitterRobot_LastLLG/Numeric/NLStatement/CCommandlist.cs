﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NLStatement
{
  public class CCommandlist : List<CCommand>
  {

    public Boolean ListProgram(ref Int32 identation)
    {
      foreach (CCommand Command in this)
      {
        if (!Command.List(ref identation))
        {
          return false;
        }
      }
      return true;
    }
  }
}
