﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
//
namespace NLStatement
{

  // <while> <expressionboolean> <do> <statement>

  public class CCommandWhile : CCommand
  {
		private CVariablelist FVariablelist;
    private String FTextExpression;
    private CExpression FExpression;
		private CCommandBlock FCommandBlock;

    public CCommandWhile(CVariablelist variablelist,
												 String textexpression,                      
                         CCommandBlock commandblock)
			: base(CStatementHeader.NAME_WHILE)
    {
			FVariablelist = variablelist;
      FTextExpression = textexpression;
      FExpression = null;
      FCommandBlock = commandblock;
    }



		public String Expression
		{
			get { return FTextExpression; }
		}

		public CCommandBlock CommandBlock
		{
			get { return FCommandBlock; }
		}




    public override Boolean Translate(ref CConstantlist constantlist,
                                      ref CVariablelist variablelist)
    {
			try
			{
				String Line = String.Format("Translate While['{0}' {1}]", FTextExpression, FCommandBlock.ToString());
        FNotifier.Write(Line);
        FExpression = new CExpression(ref constantlist,
                                      ref variablelist);
				FExpression.Translate(FTextExpression);
				//
				if (FCommandBlock is CCommandBlock)
				{
          FCommandBlock.Translate(ref constantlist,
                                  ref variablelist);
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
    }

    public override Boolean Execute()
    {
      Boolean WhileActive = true;
      Double DValue = 0.0;
      String Line;
      while (WhileActive)
      {
        if (!FExpression.Calculate(ref DValue))
        {
          return false;
        }
				Line = String.Format("Execute While[{0} {1}]", WhileActive, FCommandBlock.ToString());
        FNotifier.Write(Line);
        WhileActive = (0 != (Int32)DValue);
        if (WhileActive)
        { // Expression true ->
					if (!FCommandBlock.Execute())
          {
            return false;
          }
        } 
      }
			Line = String.Format("Execute While[{0}]", WhileActive);
      FNotifier.Write(Line);
      return true;
    }

    public override Boolean WriteToAscii(ref Int32 identation, ref CLinelist lines)
    {
      String Line = BuildIdentation(identation);
      Line += String.Format("while ({0})", FTextExpression);
      lines.Add(Line);
      //
      Line = BuildIdentation(identation) + "{";
      lines.Add(Line);
      //
      identation++;
      if (FCommandBlock is CCommand)
      {
        FCommandBlock.WriteToAscii(ref identation, ref lines);
      }
      identation--;
      //
      Line = BuildIdentation(identation) + "}";
      lines.Add(Line);
      //
      return true;
    }


  }
}
