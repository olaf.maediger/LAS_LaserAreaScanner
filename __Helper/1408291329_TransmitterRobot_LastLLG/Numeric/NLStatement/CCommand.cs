﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using NLExpression;
//
namespace NLStatement
{
  public abstract class CCommand
  {
    protected const Char CHARACTER_SPACE = ' ';

    private String FName;
		protected CNotifier FNotifier;

		public CCommand(String name)
		{
			FName = name;
		}

		public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

		public String Name
		{
			get { return FName; }
		}
		public void SetName(String value)
		{
			FName = value;
		}

    public abstract Boolean Translate(ref CConstantlist constantlist,
                                      ref CVariablelist variablelist);
    public abstract Boolean Execute();

    protected String BuildIdentation(Int32 identation)
    {
      return new String(CHARACTER_SPACE, 2 * identation);
    }

    public abstract Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines);

    public virtual Boolean List(ref Int32 identation)
    {
      CLinelist Lines = new CLinelist();
      WriteToAscii(ref identation, ref Lines);
      foreach (CLine Line in Lines)
      {
        FNotifier.Write(Line.Value);
      }
      return true;
    }




  }
}
