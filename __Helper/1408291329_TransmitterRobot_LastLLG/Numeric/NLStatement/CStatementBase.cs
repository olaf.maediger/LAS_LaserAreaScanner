﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
//
using Initdata;
using UCNotifier;
using NLExpression;
//
namespace NLStatement
{
  public class CStatementBase
  {
    //
    //------------------------------------------------------
    //  Section - Error
    //------------------------------------------------------
    //	
    protected enum EErrorCode
    {
      None = 0,
      InvalidAccess,
      Unknown
    };
    //
    //------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------
    //
		private const String HEADER_LIBRARY = "Statement";

		protected static readonly String[] ERRORS = 
    { 
      "None",
			"Invalid Access",
  	  "Unknown"
    };
    //
    //------------------------------------------------------
    //  Section - Variable
    //------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CConstantlist FConstants;
		private CVariablelist FVariables;
		private CCommandlist FCommands;
    private String FProgramName;
    //
    protected CXmlReader FXmlReader;
    //
    // protected DOnReadCommandBlock FOnReadCommandBlock;
    //
    //------------------------------------
    //  Segment - Constructor
    //------------------------------------
    //
		public CStatementBase(CConstantlist constantlist,
                          CVariablelist variablelist) 
    {
      FCommands = new CCommandlist();
      FConstants = constantlist;
			FVariables = variablelist;
      FProgramName = "Program";
    }
    //
    //------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------
    //
		protected void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      _Protocol("Library correct loaded.");
    }

		public CVariablelist Variablelist
		{
			get { return FVariables; }
		}
		
		public CCommandlist Commandlist
		{
			get { return FCommands; }
		}
		//
    //------------------------------------------------------
    //  Section - Protocol
    //------------------------------------------------------
    //
    private String BuildHeader()
    {
      return String.Format("{0}", HEADER_LIBRARY);
    }

		protected void _Error(EErrorCode code)
    {
      if (FNotifier is CNotifier)
      {
        Int32 Index = (Int32)code;
        if ((Index < 0) && (ERRORS.Length <= Index))
        { // Unknown Error!
          Index = ERRORS.Length - 1;
        }
        String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
        FNotifier.Write(BuildHeader(), Line);
      }
    }

		protected void _Protocol(String line)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(BuildHeader(), line);
      }
    }
		//
		//------------------------------------
		//  Helper
		//------------------------------------
		//
		public Boolean AddVariable(CVariable variable)
		{
			if (variable is CVariable)
			{
				FVariables.Add(variable);
				return true;
			}
			return false;
		}

		public Boolean AddCommand(CCommand command)
		{
			if (command is CCommand)
			{
				FCommands.Add(command);
				return true;
			}
			return false;
		}

		public Boolean SetCommandlist(CCommandBlock commandblock)
		{
			if (commandblock is CCommandBlock)
			{
				FCommands.Clear();
				foreach (CCommand Command in commandblock.Commandlist)
				{
					FCommands.Add(Command);
				}
				return true;
			}
			return false;
		}

		public void ClearCommandlist()
		{
			FCommands.Clear();
		}

		public void ClearVariablelist()
		{
			FVariables.Clear();
		}


    //
    //------------------------------------
    //  Public Management
    //------------------------------------
		//
		protected Boolean AddCommandSet(String name,
                                    String type,
																	  String expression)
		{
      String Line = String.Format("Add Command [set {0}({1}) = {2}]", name, type, expression);
      _Protocol(Line);
      CCommand Command = new CCommandSet(//FVariables, 
                                         name, type, expression);
			if (Command is CCommand)
			{
        Command.SetNotifier(FNotifier);
        FCommands.Add(Command);
				return true;
			}
			return false;
		}
		protected Boolean AddCommandSet(CCommandBlock commandblock,
																		String name,
                                    String type,
																		String expression)
		{
      String Line = String.Format("Add Command to Block [set {0}({1}) = {2}]", name, type, expression);
      _Protocol(Line);
      CCommand Command = new CCommandSet(//FVariables, 
                                         name, type, expression);
			if (Command is CCommand)
			{
        Command.SetNotifier(FNotifier);
        commandblock.AddCommand(Command);
				return true;
			}
			return false;
		}


		protected Boolean AddCommandIf(String expression,
																	 CCommandBlock blocktrue,
																	 CCommandBlock blockfalse)
		{
      String Line = String.Format("Add Command [if {0} ", expression);
      if (blocktrue is CCommand)
      {
        Line += String.Format("then {0} ", blocktrue.ToString());
      }
      if (blockfalse is CCommand)
      {
        Line += String.Format("else {0}", blockfalse.ToString());
      }
      Line += "]";
      _Protocol(Line);
      CCommand Command = new CCommandIf(FVariables, expression, blocktrue, blockfalse);
			if (Command is CCommand)
			{
        Command.SetNotifier(FNotifier);
        FCommands.Add(Command);
				return true;
			}
			return false;
		}
		protected Boolean AddCommandIf(CCommandBlock commandblock,
																	 String expression,
																	 CCommandBlock blocktrue,
																	 CCommandBlock blockfalse)
		{
      String Line = String.Format("Add Command to Block [if {0} ", expression);
			if (blocktrue is CCommand)
      {
        Line += String.Format("then {0} ", blocktrue.ToString());
      }
			if (blockfalse is CCommand)
      {
				Line += String.Format("else {0}", blockfalse.ToString());
      }
      Line += "]";
      _Protocol(Line);
			CCommand Command = new CCommandIf(FVariables, expression, blocktrue, blockfalse);
			if (Command is CCommand)
			{
        Command.SetNotifier(FNotifier);
        commandblock.AddCommand(Command);
				return true;
			}
			return false;
		}


		protected Boolean AddCommandWhile(String expression,
																			CCommandBlock commandblock)
		{
      String Line = String.Format("Add Command [while {0} do {1}]", expression, commandblock.ToString());
      _Protocol(Line);
      CCommand Command = new CCommandWhile(FVariables, expression, commandblock);
			if (Command is CCommand)
			{
        Command.SetNotifier(FNotifier);
        FCommands.Add(Command);
				return true;
			}
			return false;
		}
		protected Boolean AddCommandWhile(CCommandBlock commandblock,
																			String expression,
																			CCommandBlock whileblock)
		{
      String Line = String.Format("Add Command While to Block [while {0} do {1}]", expression, whileblock.ToString());
      _Protocol(Line);
      CCommand Command = new CCommandWhile(FVariables, expression, whileblock);
			if (Command is CCommand)
			{
        Command.SetNotifier(FNotifier);
        commandblock.AddCommand(Command);
				return true;
			}
			return false;
		}

		// for <set> <condition==expression> <block>

		/* NC ???protected Boolean AddCommandFor(CCommandSet commandset,
																		String condition,
																		CCommandSet commandchange,
																		CCommandBlock block)
		{
			String Line = String.Format("Add Command [for ({0}; {1}; {2})",
																	commandset.ToString(), condition, commandchange.ToString());
			if (block is CCommand)
			{
				Line += String.Format(" {0} ", block.ToString());
			}
			Line += "]";
			_Protocol(Line);
			CCommand Command = new CCommandFor(FVariables, commandset, condition, commandchange, block);
			if (Command is CCommand)
			{
				Command.SetNotifier(FNotifier);
				FCommands.Add(Command);
				return true;
			}
			return false;
		}
		protected Boolean AddCommandFor(CCommandBlock commandblock,
																		CCommandSet commandset,
																		String condition,
																		CCommandSet commandchange,
																		CCommandBlock block)
		{
			String Line = String.Format("Add Command to Block [for ({0}; {1}; {2})",
																	commandset.ToString(), condition, commandchange.ToString());
			if (block is CCommand)
			{
				Line += String.Format(" {0} ", block.ToString());
			}
			Line += "]";
			_Protocol(Line);
			CCommand Command = new CCommandFor(FVariables, commandset, condition, commandchange, block);
			if (Command is CCommand)
			{
				Command.SetNotifier(FNotifier);
				commandblock.AddCommand(Command);
				return true;
			}
			return false;
		}
		*/



		protected Boolean AddCommandWriteLine(String format,
																					String expression)
		{
      String Line = String.Format("Add Command [writeln '{0}' {1}]", format, expression);
      _Protocol(Line);
      CCommand Command = new CCommandWriteLine(FVariables, format, expression);
			if (Command is CCommand)
			{
				FCommands.Add(Command);
        Command.SetNotifier(FNotifier);
				return true;
			}
			return false;
		}
		protected Boolean AddCommandWriteLine(CCommandBlock commandblock,
																					String format,
																					String expression)
		{
      String Line = String.Format("Add Command to Block [writeln '{0}' {1}]", format, expression);
      _Protocol(Line);
      CCommand Command = new CCommandWriteLine(FVariables, format, expression);
			if (Command is CCommand)
			{
        Command.SetNotifier(FNotifier);
        commandblock.AddCommand(Command);
				return true;
			}
			return false;
		}


		protected CCommandBlock CreateCommandBlock()
		{
			String Line = "Create CommandBlock";
			_Protocol(Line);
			CCommandBlock CommandBlock = new CCommandBlock();//FConstants, FVariables);
			if (CommandBlock is CCommandBlock)
			{
				CommandBlock.SetNotifier(FNotifier);
				// NO ADD !!!!!!!!!!!  FCommands.Add(CommandBlock); --> add only later in Statement
				return CommandBlock;
			}
			return null;
		}

    protected CCommandBlock CreateAddCommandBlock()
    {
      String Line = "CreateAdd CommandBlock";
      _Protocol(Line);
      CCommandBlock CommandBlock = new CCommandBlock();//FConstants, FVariables);
      if (CommandBlock is CCommandBlock)
      {
        CommandBlock.SetNotifier(FNotifier);
        // additional CommandBlock!!!
        FCommands.Add(CommandBlock);
        return CommandBlock;
      }
      return null;
    }

    protected Boolean AddCommandBlock(CCommandBlock commandblock)
    {
      String Line = "Add CommandBlock";
      _Protocol(Line);
      if (commandblock is CCommandBlock)
      {
        commandblock.SetNotifier(FNotifier);
        FCommands.Add(commandblock);
        return true;
      }
      return false;
    }




    // translates all Expressions in all Commands
    protected Boolean Translate(ref CConstantlist constantlist,
                                ref CVariablelist variablelist)
    {
      String Line = String.Format("Translate Statement[{0}]:", FCommands.Count);
      _Protocol(Line);
      foreach (CCommand Command in FCommands)
      {
        if (!Command.Translate(ref constantlist,
                               ref variablelist))
        {
          return false;
        }
      }
      return true;
    }

		protected Boolean Execute()
    {
      String Line = String.Format("Execute Statement[{0}]:", FCommands.Count);
      _Protocol(Line);
      foreach (CCommand Command in FCommands)
      {
        if (!Command.Execute())
        {
          return false;
        }
      }
      return true;
    }

    /*
    protected Boolean ReadFromXml(String filename)
    {
      FXmlReader = new CXmlReader();
      FXmlReader.SetNotifier(FNotifier);
      FXmlReader.SetOnReadCommandBlock(FOnReadCommandBlock);
      FProgramName = Path.GetFileNameWithoutExtension(filename);
      return FXmlReader.ReadProgram(filename, this);
    }*/
    /*???protected Boolean ReadFromXml(String filename,
                                  DOnReadCommandBlock onreadcommandblock)
    {
      FXmlReader = new CXmlReader();
      FXmlReader.SetNotifier(FNotifier);
      FXmlReader.SetOnReadCommandBlock(onreadcommandblock);
      FProgramName = Path.GetFileNameWithoutExtension(filename);
      return FXmlReader.ReadProgram(filename, this);
    }*/
/*
    protected Boolean WriteToXml(String filename)
    {
      CXmlWriter XmlWriter = new CXmlWriter();
      XmlWriter.SetNotifier(FNotifier);
      FProgramName = Path.GetFileNameWithoutExtension(filename);
      return XmlWriter.WriteProgram(filename, this);
    }
    */
    protected Boolean ListProgram()
    {
      Int32 Identation = 0;
      String Line = String.Format("*** List Program[{0}]:", FProgramName);
      FNotifier.Write(Line);
      return FCommands.ListProgram(ref Identation);
    }



   /* protected Boolean SaveToAscii(String filename)
    {
      CAsciiWriter AsciiWriter = new CAsciiWriter();
      AsciiWriter.SetNotifier(FNotifier);
      FProgramName = Path.GetFileNameWithoutExtension(filename);
      return AsciiWriter.WriteProgram(filename, this);
    }*/

  }
}
