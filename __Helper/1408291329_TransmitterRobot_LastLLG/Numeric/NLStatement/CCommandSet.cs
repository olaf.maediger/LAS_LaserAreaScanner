﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
//
namespace NLStatement
{ //   
	//  Syntax: <set> <variablename> <expression>
  //
	public class CCommandSet : CCommand
	{
    private CConstantlist FConstantlist;
		private CVariablelist FVariablelist;
    private String FVariableName;
    private String FVariableType;
    private String FTextExpression;
    private CExpression FExpression;

		public String Variable
		{
			get { return FVariableName; }
		}

		public String TextExpression
		{
			get { return FTextExpression; }
		}


    public CCommandSet(String variablename,
                       String variabletype,
											 String textexpression)
			: base(CStatementHeader.NAME_SET)
		{
      FConstantlist = null;
			FVariablelist = null;
      FVariableName = variablename;
      FVariableType = variabletype;
      FTextExpression = textexpression;
      FExpression = null;
    }




    public override Boolean Translate(ref CConstantlist constantlist,
                                   ref CVariablelist variablelist)
    {
      FConstantlist = constantlist;
      FVariablelist = variablelist;
      String Line = String.Format("Translate Set[{0}({1}) = {2}]",
                                  FVariableName, FVariableType, FTextExpression);
      FNotifier.Write(Line);
      FExpression = new CExpression(ref FConstantlist, ref FVariablelist);
      if (FExpression.Translate(FTextExpression))
      {
        switch (FVariableType)
        {
          case "Double":
            FVariablelist.Add(new CVariableDouble(FVariableName, FTextExpression));
            return true;
        }
      }
      return false;
    }

    public override Boolean Execute()
    {
			if (!(FVariablelist is CVariablelist))
			{
				return false;
			}
			CVariable Variable = FVariablelist.FindName(FVariableName);
			if (!(Variable is CVariable))
			{
				return false;
			}
			//
      Double DValue = 0.0;
      if (!FExpression.Calculate(ref DValue))
      {
        return false;
      }
			Variable.Set(DValue);
			String Line = String.Format("Execute Set[{0} = {1}]", FVariableName, DValue);
      FNotifier.Write(Line);
      return true;
    }



    public override Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines)
    {
      CVariable Variable = FVariablelist.FindName(FVariableName);
      if (Variable is CVariable)
      {
        String Line = BuildIdentation(identation);
        if (Variable.IsDefined())
        {
          Line += String.Format("{0} = {1};", Variable.Name, FTextExpression);
        } else
        {
          Line += String.Format("{0} {1} = {2};", Variable.Type, Variable.Name, FTextExpression);
        }
        lines.Add(Line);
        return true;
      }
      return false;
    }


  }
}
