﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using NLExpression;
//
namespace NLStatement
{
  public class CStatement : CStatementBase
  {
    //
    //------------------------------------
    //  Segment - Constructor
    //------------------------------------
    //
		public CStatement(ref CConstantlist constantlist,
                      ref CVariablelist variablelist)
			: base(constantlist, variablelist)
    {
    }
		//
		//------------------------------------------------------
		//  Section - Property
		//------------------------------------------------------
		//
		public new void SetNotifier(CNotifier notifier)
		{
			base.SetNotifier(notifier);
		}

    /*???public void SetOnReadCommandBlock(DOnReadCommandBlock value)
    {
      base.FOnReadCommandBlock = value;
    }*/
    //
    //------------------------------------
    // Section - Helper
    //------------------------------------
    //
		public new Boolean AddVariable(CVariable variable)
		{
			{
				try
				{
					return base.AddVariable(variable);
				}
				catch (Exception)
				{
					_Error(EErrorCode.InvalidAccess);
					return false;
				}
			}
			
		}
    //
    //------------------------------------
    // Public Management
    //------------------------------------
    //
		public new Boolean AddCommandSet(String name,
																		 String type,
                                     String expression)
    {
      try
      {
        return base.AddCommandSet(name, type, expression);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }

		public new Boolean AddCommandSet(CCommandBlock commandblock,
																		 String name,
                                     String type,
																		 String expression)
    {
      try
      {
        return base.AddCommandSet(commandblock, name, type, expression);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }

		public new Boolean AddCommandIf(String expression,
																		CCommandBlock blocktrue,
																		CCommandBlock blockfalse)
		{
      try
      {
				return base.AddCommandIf(expression, blocktrue, blockfalse);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }

		public new Boolean AddCommandIf(CCommandBlock commandblock,
																	  String expression,
																		CCommandBlock blocktrue,
																		CCommandBlock blockfalse)
		{
      try
      {
        return base.AddCommandIf(commandblock, expression, blocktrue, blockfalse);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }

		public new Boolean AddCommandWhile(String expression,
																			 CCommandBlock commandblock)
    {
      try
      {
        return base.AddCommandWhile(expression, commandblock);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }

		public new Boolean AddCommandWhile(CCommandBlock commandblock,
																			 String expression,
																			 CCommandBlock whileblock)
		{
      try
      {
        return base.AddCommandWhile(commandblock, expression, whileblock);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }
		/* ???
		public new Boolean AddCommandFor(CCommandSet commandset,
																		 String condition,
																		 CCommandSet commandchange,
																		 CCommandBlock commandblock)
		{
			try
			{
				return base.AddCommandFor(commandset, condition, commandchange, commandblock);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean AddCommandFor(CCommandBlock commandblock,
																		 CCommandSet commandset,
																		 String condition,
																		 CCommandSet commandchange,
																		 CCommandBlock blocktrue)
		{
			try
			{
				return base.AddCommandFor(commandblock, commandset, condition, commandchange, blocktrue);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}*/

		public new Boolean AddCommandWriteLine(String format,
																					 String expression)
    {
      try
      {
        return base.AddCommandWriteLine(format, expression);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }

		public new Boolean AddCommandWriteLine(CCommandBlock commandblock,
																					 String format,
																					 String expression)
    {
      try
      {
        return base.AddCommandWriteLine(commandblock, format, expression);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }


		public new CCommandBlock CreateCommandBlock()
		{
			try
			{
				return base.CreateCommandBlock();
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return null;
			}
		}

    public new CCommandBlock CreateAddCommandBlock()
    {
      try
      {
        return base.CreateAddCommandBlock();
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return null;
      }
    }

    public new Boolean AddCommandBlock(CCommandBlock commandblock)
    {
      try
      {
        return base.AddCommandBlock(commandblock);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }



    public new Boolean Translate(ref CConstantlist constantlist,
                                 ref CVariablelist variablelist)
    {
      try
      {
        return base.Translate(ref constantlist,
                              ref variablelist);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }

    public new Boolean Execute()
    {
      try
      {
        return base.Execute();
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }

    /*
		public new Boolean ReadFromXml(String filename)
		{
			try
			{
				return base.ReadFromXml(filename);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}	*/	
    /*
		public new Boolean WriteToXml(String filename)
		{
			try
			{
				return base.WriteToXml(filename);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}*/



    public new Boolean ListProgram()
    {
      try
      {
        return base.ListProgram();
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }

    /*?????????
    public new Boolean SaveToAscii(String filename)
    {
      try
      {
        return base.SaveToAscii(filename);
      }
      catch (Exception)
      {
        _Error(EErrorCode.InvalidAccess);
        return false;
      }
    }		*/



  }
}
