﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
using UCNotifier;
//
namespace NLStatement
{ //  
  //  Syntax: <commandblock> := <command> {0, N}
  //
  public class CCommandBlock : CCommand
  {
    //private CConstantlist FConstantlist;
		//private CVariablelist FVariablelist;
		private CCommandlist FCommands;

    public CCommandBlock()
			: base(CStatementHeader.NAME_BLOCK)
		{
      //???FConstantlist = null;
			//FVariablelist = null;
			FCommands = new CCommandlist();			
    }



		public CCommandlist Commandlist
		{
			get { return FCommands; }
		}




		public Boolean AddCommand(CCommand command)
		{
			if (command is CCommand)
			{
				command.SetNotifier(FNotifier);
				FCommands.Add(command);
				return true;
			}
			return false;
		}


    public override bool Translate(ref CConstantlist constantlist,
                                   ref CVariablelist variablelist)
    {
			String Line = "Translate [CommandBlock]";
			FNotifier.Write(Line);
			foreach (CCommand Command in FCommands)
      {
        if (!Command.Translate(ref constantlist,
                               ref variablelist))
        {
          return false;
        }
      }
      return true;
    }

    public override Boolean Execute()
    {
			String Line = "Execute [CommandBlock]";
			FNotifier.Write(Line);
			foreach (CCommand Statement in FCommands)
      {
        if (!Statement.Execute())
        {
          return false;
        }
      }
			return true;
    }


    public override Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines)
    {
      foreach (CCommand Command in FCommands)
      {
        if (!Command.WriteToAscii(ref identation, ref lines))
        {
          return false;
        }
      }
      return true;
    }

  }
}
