﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
//
using UCNotifier;
//
using NLExpression;
using NLStatement;
//
using XmlFile;
//
namespace NLProgram
{
  public class CProgram
  {
    private CNotifier FNotifier;
    private CConstantlist FConstantlist;
    private CVariablelist FVariablelist;
    private CExpression FExpression;
    private CStatement FStatement;

    private String FName;

    private DOnReadCommandBlock FOnReadCommandBlock;
    // ??? private CCommandlist FCommandlist;


    public CProgram()
    {
      CreateInstances();
    }

    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnReadCommandBlock(DOnReadCommandBlock value)
    {
      FOnReadCommandBlock = value;
    }


    private void CreateInstances()
    {
      FConstantlist = new CConstantlist();
      FConstantlist.SetNotifier(FNotifier);
      FVariablelist = new CVariablelist();
      FVariablelist.SetNotifier(FNotifier);
      FExpression = new CExpression(ref FConstantlist, ref FVariablelist);
      FExpression.SetNotifier(FNotifier);
      FStatement = new CStatement(ref FConstantlist, ref FVariablelist);
      FStatement.SetNotifier(FNotifier);
    }

    public Boolean LoadFromXml(String programentry)
    {
      Boolean Result = true;
      //
      CreateInstances();
      //
      CXmlReader XmlReader = new CXmlReader();
      XmlReader.SetNotifier(FNotifier);
      XmlReader.SetOnReadCommandBlock(SelfOnReadCommandBlock);
      FName = Path.GetFileNameWithoutExtension(programentry);
      XmlReader.Load(programentry,
                     ref FConstantlist,
                     ref FVariablelist,
                     ref FExpression,
                     ref FStatement);


      // NC ???CXmlReader XmlReader = new CXmlReader(programentry);
      //return FCommandlist.LoadProgramFromXml(programentry);
      /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FExpression = new CExpression();
      FStatement = new CStatement(FExpression.Variablelist);
      FStatement.SetNotifier(FNotifier);
       */
      //????????????????????????FStatement.SetOnReadCommandBlock(SelfOnReadCommandBlock);
      //????????????return 
      return Result;
    }

    private Boolean SelfOnReadCommandBlock(XmlNode nodebase,
                                           ref CCommandBlock commandblock)
    {
      if (FOnReadCommandBlock is DOnReadCommandBlock)
      {
        return FOnReadCommandBlock(nodebase, ref commandblock);
      }
      return false;
    }

    public Boolean Translate()
    {
      FConstantlist.Clear();
      FVariablelist.Clear();
      return FStatement.Translate(ref FConstantlist, ref FVariablelist);
    }

    public Boolean Execute()
    {
      return FStatement.Execute();
    }

    public void DebugConstantlist()
    {
      FConstantlist.Debug();
    }

    public void DebugVariablelist()
    {
      FVariablelist.Debug();
    }

  }
}
