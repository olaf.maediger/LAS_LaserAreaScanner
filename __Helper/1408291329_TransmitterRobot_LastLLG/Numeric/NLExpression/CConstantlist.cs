﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace NLExpression
{
  public class CConstantlist : List<CConstant>
  {
    private CNotifier FNotifier;


    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      foreach (CConstant Constant in this)
      {
        Constant.SetNotifier(FNotifier);
      }
    }

    public CConstant FindName(String name)
    {
      foreach (CConstant Constant in this)
      {
        if (name == Constant.Name)
        {
          return Constant;
        }
      }
      return null;
    }

    public void Debug()
    {
      String Line = String.Format("*** Constantlist[{0}]:", Count);
      FNotifier.Write(Line);
      foreach (CConstant Constant in this)
      {
        Constant.SetNotifier(FNotifier);
        Constant.Debug();
      }
    }

  }
}

