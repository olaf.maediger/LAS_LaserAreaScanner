﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NLExpression
{
  public enum ESymbolKind
  {
    Undefined = 0,
    Operator = 1,
    Sign = 2,
    Identifier = 3,
    Integer = 4,
    Floating = 5,
    Reserved = 6,
    Comment = 7
  };

  public abstract class CSymbol
  {
    private String FText;
    public String Text
    {
      get { return FText; }
      set { FText = value; }
    }

    protected abstract ESymbolKind GetKind();
    public ESymbolKind Kind
    {
      get { return GetKind(); }
    }

    public CSymbol(String text)
    {
      FText = text;
    }

    public abstract Boolean Calculate(ref CStack stack);
  }


  public class COperator : CSymbol
  {
		public COperator(String text)
      : base(text)
    {
		}

    protected override ESymbolKind GetKind()
    {
      return ESymbolKind.Operator;
    }

    public override Boolean Calculate(ref CStack stack)
    {
      try
      {
        //
        //  Calculate - (Single)Operator
        //
        Double DValueA, DValueB;
        if (((Char)ETerminal.ADD).ToString() == Text)
        {
          DValueB = stack.Pop();
          DValueA = stack.Pop();
          stack.Push(DValueA + DValueB);
          return true;
        }
        if (((Char)ETerminal.SUB).ToString() == Text)
        {
          DValueB = stack.Pop();
          DValueA = stack.Pop();
          stack.Push(DValueA - DValueB);
          return true;
        }
        if (((Char)ETerminal.MUL).ToString() == Text)
        {
          DValueB = stack.Pop();
          DValueA = stack.Pop();
          stack.Push(DValueA * DValueB);
          return true;
        }
        if (((Char)ETerminal.DIV).ToString() == Text)
        {
          DValueB = stack.Pop();
          DValueA = stack.Pop();
          stack.Push(DValueA / DValueB); // /0 -> Exception!
          return true;
        }
        if (((Char)ETerminal.LOWER).ToString() == Text)
        {
          DValueB = stack.Pop();
          DValueA = stack.Pop();
          Boolean BValue = (DValueA < DValueB);
          stack.Push(BValue ? 1.0 : 0.0);
          return true;
        }
        if (((Char)ETerminal.GREATER).ToString() == Text)
        {
          DValueB = stack.Pop();
          DValueA = stack.Pop();
          Boolean BValue = (DValueA > DValueB);
          stack.Push(BValue ? 1.0 : 0.0);
          return true;
        }
        //
        //  Calculate - StringOperator ("==", "<>", "<=", ">=")
        //
        if (CParser.RESERVED[(Byte)EReserved.EQUAL] == Text)
        {
          DValueB = stack.Pop();
          DValueA = stack.Pop();
          Boolean BValue = (DValueA == DValueB);
          stack.Push(BValue ? 1.0 : 0.0);
          return true;
        }
        if (CParser.RESERVED[(Byte)EReserved.UNEQUAL] == Text)
        {
          DValueB = stack.Pop();
          DValueA = stack.Pop();
          Boolean BValue = (DValueA != DValueB);
          stack.Push(BValue ? 1.0 : 0.0);
          return true;
        }
        if (CParser.RESERVED[(Byte)EReserved.LOWEREQUAL] == Text)
        {
          DValueB = stack.Pop();
          DValueA = stack.Pop();
          Boolean BValue = (DValueA <= DValueB);
          stack.Push(BValue ? 1.0 : 0.0);
          return true;
        }
        if (CParser.RESERVED[(Byte)EReserved.GREATEREQUAL] == Text)
        {
          DValueB = stack.Pop();
          DValueA = stack.Pop();
          Boolean BValue = (DValueA >= DValueB);
          stack.Push(BValue ? 1.0 : 0.0);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
  }

  public class CSign : CSymbol
  {
    public CSign(String text)
      : base(text)
    {
    }

    protected override ESymbolKind GetKind()
    {
      return ESymbolKind.Sign;
    }

    public override Boolean Calculate(ref CStack stack)
    {
      try
      {
        Double DValue;
        if (((Char)ETerminal.SUB).ToString() == Text)
        {
          DValue = stack.Pop();
          stack.Push(-DValue);
          return true;
        } else
        if (((Char)ETerminal.ADD).ToString() == Text)
        {
          DValue = stack.Pop();
          stack.Push(+DValue);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
  }

  public class CIdentifier : CSymbol
  {
		protected CVariablelist FVariablelist;

		public CIdentifier(CVariablelist variablelist,
											 String text)
      : base(text)
    {
			FVariablelist = variablelist;
		}

    protected override ESymbolKind GetKind()
    {
      return ESymbolKind.Identifier;
    }

    public override Boolean Calculate(ref CStack stack)
    {
      try
      {
				CVariable Variable = FVariablelist.FindName(Text);
        Double DValue = Variable.Get(); // fetch Variable / Constant fromList!!!
        stack.Push(DValue);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
  }

  public class CInteger : CSymbol
  {
    public CInteger(String text)
      : base(text)
    {
    }

    protected override ESymbolKind GetKind()
    {
      return ESymbolKind.Integer;
    }

    public override Boolean Calculate(ref CStack stack)
    {
      try
      {
        Double DValue;
        DValue = Int32.Parse(Text);
        stack.Push(DValue);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
  }

  public class CFloating : CSymbol
  {
    public CFloating(String text)
      : base(text)
    {
    }

    protected override ESymbolKind GetKind()
    {
      return ESymbolKind.Floating;
    }

    public override Boolean Calculate(ref CStack stack)
    {
      try
      {
        Double DValue;
        DValue = Double.Parse(Text);
        stack.Push(DValue);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
  }

  public class CReserved : CSymbol
  {
    public CReserved(String text)
      : base(text)
    {
    }

    protected override ESymbolKind GetKind()
    {
      return ESymbolKind.Reserved;
    }
    public override Boolean Calculate(ref CStack stack)
    {
      try
      {
        Double DValue;
        if (CParser.RESERVED[(byte)EReserved.SINUS] == Text)
        {
          DValue = stack.Pop();
          stack.Push(Math.Sin(DValue));
          return true;
        }
        if (CParser.RESERVED[(byte)EReserved.COSINUS] == Text)
        {
          DValue = stack.Pop();
          stack.Push(Math.Cos(DValue));
          return true;
        }
        if (CParser.RESERVED[(byte)EReserved.TANGENS] == Text)
        {
          DValue = stack.Pop();
          stack.Push(Math.Tan(DValue));
          return true;
        }
        if (CParser.RESERVED[(byte)EReserved.EXPONENTIAL] == Text)
        {
          DValue = stack.Pop();
          stack.Push(Math.Exp(DValue));
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
  }

  public class CComment : CSymbol
  {
    public CComment(String text)
      : base(text)
    {
    }

    protected override ESymbolKind GetKind()
    {
      return ESymbolKind.Comment;
    }
    public override Boolean Calculate(ref CStack stack)
    {
      try
      {
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
  }

}
