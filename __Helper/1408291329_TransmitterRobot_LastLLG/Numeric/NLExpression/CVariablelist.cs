﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace NLExpression
{
  public class CVariablelist : List<CVariable>
  {
    private CNotifier FNotifier;

    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      foreach (CVariable Variable in this)
      {
        Variable.SetNotifier(value);
      }
    }

    public CVariable FindName(String name)
    {
      foreach (CVariable Variable in this)
      {
        if (name == Variable.Name)
        {
          return Variable;
        }
      }
      return null;
    }

    public void Debug()
    {
      String Line = String.Format("*** Variablelist[{0}]:", Count);
      FNotifier.Write(Line);
      foreach (CVariable Variable in this)
      {
        Variable.SetNotifier(FNotifier);
        Variable.Debug();
      }
    }

  }
}
