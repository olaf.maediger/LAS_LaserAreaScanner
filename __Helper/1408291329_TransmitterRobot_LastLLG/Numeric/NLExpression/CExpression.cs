﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Initdata;
using UCNotifier;
//
namespace NLExpression
{
  public class CExpression
  {
    //
    //------------------------------------------------------
    //  Section - Error
    //------------------------------------------------------
    //	
    public enum EErrorCode
    {
      None = 0,
      InvalidAccess,
      Unknown
    };
    //
    //------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------
    //
    public const String HEADER_LIBRARY = "Expression";

    public static readonly String[] ERRORS = 
    { 
      "None",
			"Invalid Access",
  	  "Unknown"
    };
    //
    //------------------------------------------------------
    //  Section - Variable
    //------------------------------------------------------
    //
    private CNotifier FNotifier;  // only reference!
    private CConstantlist FConstants; // only reference!
    private CVariablelist FVariables; // only reference!
    private CSymbollist FSymbols;
    private CSymbollist FOperations;
    //
    //------------------------------------
    //  Segment - Constructor
    //------------------------------------
    //
    public CExpression(ref CConstantlist constantlist,
                       ref CVariablelist variablelist)
    {
      FConstants = constantlist;
      FVariables = variablelist;
      FSymbols = new CSymbollist();
      FOperations = new CSymbollist();
    }
    //
    //------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }
    //
    //------------------------------------------------------
    //  Section - Protocol
    //------------------------------------------------------
    //
    private String BuildHeader()
    {
      return String.Format("{0}", HEADER_LIBRARY);
    }

    public void _Error(EErrorCode code)
    {
      if (FNotifier is CNotifier)
      {
        Int32 Index = (Int32)code;
        if ((Index < 0) && (ERRORS.Length <= Index))
        { // Unknown Error!
          Index = ERRORS.Length - 1;
        }
        String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
        FNotifier.Write(BuildHeader(), Line);
      }
    }

    public void _Protocol(String line)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(BuildHeader(), line);
      }
    }
    //
    //------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------
    //
    private Boolean CheckSymbolIndex(Int32 symbolindex)
    {
      return (symbolindex < FSymbols.Count);
    }

    private Boolean IncrementSymbolIndex(ref Int32 symbolindex)
    {
      if (1 + symbolindex < FSymbols.Count)
      {
        symbolindex++;
        return true;
      }
      return false;
    }

    private CSymbol GetSymbol(Int32 symbolindex)
    {
      if ((0 <= symbolindex) && (symbolindex < FSymbols.Count))
      {
        CSymbol Symbol = FSymbols[symbolindex];
        if (Symbol is CSymbol)
        {
          return Symbol;
        }
      }
      return null;
    }
    //
    //-----------------------------------------
    //  Conversion Symbollist -> Operationlist
    //-----------------------------------------
    //
    private Boolean ConvertFactor(ref Int32 symbolindex)
    {
      CSymbol Symbol = GetSymbol(symbolindex);
      if (!(Symbol is CSymbol))
      {
        return false;
      }
      // "("
      if (new String((Char)ETerminal.POPEN, 1) == Symbol.Text)
      {
        symbolindex++;
        if (!ConvertExpression(ref symbolindex))
        {
          return false;
        }
        Symbol = GetSymbol(symbolindex);
        if (!(Symbol is CSymbol))
        {
          return false;
        }
        // ")"
        if (new String((Char)ETerminal.PCLOSE, 1) == Symbol.Text)
        {
          symbolindex++;
          return true;
        }
        return true;
      } 
      // Floating
      if (ESymbolKind.Floating == Symbol.Kind)
      {
        CSymbol FloatingVariable = new CFloating(Symbol.Text);
        FOperations.Add(FloatingVariable);
        symbolindex++;
        return true;
      } 
      // Integer
      if (ESymbolKind.Integer == Symbol.Kind)
      {
        CSymbol IntegerVariable = new CInteger(Symbol.Text);
        FOperations.Add(IntegerVariable);
        symbolindex++;
        return true;
      } 
      // Reserved
      if (ESymbolKind.Reserved == Symbol.Kind)
      {
        CSymbol Reservered = new CReserved(Symbol.Text);
        symbolindex++;
        Symbol = GetSymbol(symbolindex);
        if (!(Symbol is CSymbol))
        {
          return false;
        }
        if (new String((Char)ETerminal.POPEN, 1) == Symbol.Text)
        {
          symbolindex++;
          if (!ConvertExpression(ref symbolindex))
          {
            return false;
          }
          Symbol = GetSymbol(symbolindex);
          if (!(Symbol is CSymbol))
          {
            return false;
          }
          if (new String((Char)ETerminal.PCLOSE, 1) == Symbol.Text)
          {
            FOperations.Add(Reservered);
            symbolindex++;
            return true;
          }
        }        
      } 
      // Identifier
      if (ESymbolKind.Identifier == Symbol.Kind)
      {
        CSymbol Identifier = new CIdentifier(FVariables, Symbol.Text);
        FOperations.Add(Identifier);
        symbolindex++;
        return true;
      }
      return true; 
    }

    private Boolean ConvertTerm(ref Int32 symbolindex)
    {
      if (!ConvertFactor(ref symbolindex))
      {
        return false;
      }
      if (!CheckSymbolIndex(symbolindex))
      {
        return true;
      }
      Boolean LoopActive = true;
      while (LoopActive)
      {
        CSymbol Symbol = GetSymbol(symbolindex);
        if (!(Symbol is CSymbol))
        {
          return false;
        }
        if (new String((Char)ETerminal.MUL, 1) == Symbol.Text)
        {
          symbolindex++;
          if (!ConvertFactor(ref symbolindex))
          {
            return false;
          }
          COperator Operator = new COperator(Symbol.Text);
          FOperations.Add(Operator);
        } else
          if (new String((Char)ETerminal.DIV, 1) == Symbol.Text)
          {
            symbolindex++;
            if (!ConvertFactor(ref symbolindex))
            {
              return false;
            }
            COperator Operator = new COperator(Symbol.Text);
            FOperations.Add(Operator);
          } else
          {
            LoopActive = false;
          }
        LoopActive &= CheckSymbolIndex(symbolindex);
      }
      return true;
    }

    private Boolean ConvertExpression(ref Int32 symbolindex)
    {
      CSymbol Symbol = GetSymbol(symbolindex);
      if (!(Symbol is CSymbol))
      {
        return false;
      }
      // Unary Operators
      Boolean SignADD = false;
      Boolean SignSUB = false;
      if (new String((Char)ETerminal.ADD, 1) == Symbol.Text)
      {
        SignADD = true;
        if (!IncrementSymbolIndex(ref symbolindex))
        {
          return true;
        }
      } else
      if (new String((Char)ETerminal.SUB, 1) == Symbol.Text)
      {
        SignSUB = true;
        if (!IncrementSymbolIndex(ref symbolindex))
        {
          return true;
        }
      }
      //
      if (!ConvertTerm(ref symbolindex))
      {
        return false;
      }
      if (CheckSymbolIndex(symbolindex))
      {
        Boolean LoopActive = true;
        while (LoopActive)
        {
          Symbol = GetSymbol(symbolindex);
          if (!(Symbol is CSymbol))
          {
            return false;
          }
          if (new String((Char)ETerminal.ADD, 1) == Symbol.Text)
          {
            if (!IncrementSymbolIndex(ref symbolindex))
            {
              return true;
            }
            if (!ConvertTerm(ref symbolindex))
            {
              return false;
            }
            COperator Operator = new COperator(Symbol.Text);
            FOperations.Add(Operator);
          } else
            if (new String((Char)ETerminal.SUB, 1) == Symbol.Text)
            {
              if (!IncrementSymbolIndex(ref symbolindex))
              {
                return true;
              }
              if (!ConvertTerm(ref symbolindex))
              {
                return false;
              }
              COperator Operator = new COperator(Symbol.Text);
              FOperations.Add(Operator);
            } else
              if (new String((Char)ETerminal.LOWER, 1) == Symbol.Text)
              {
                if (!IncrementSymbolIndex(ref symbolindex))
                {
                  return true;
                }
                if (!ConvertTerm(ref symbolindex))
                {
                  return false;
                }
                COperator Operator = new COperator(Symbol.Text);
                FOperations.Add(Operator);
              } else
                if (new String((Char)ETerminal.GREATER, 1) == Symbol.Text)
                {
                  if (!IncrementSymbolIndex(ref symbolindex))
                  {
                    return true;
                  }
                  if (!ConvertTerm(ref symbolindex))
                  {
                    return false;
                  }
                  COperator Operator = new COperator(Symbol.Text);
                  FOperations.Add(Operator);
                } else // StringOperator
                  if ((CParser.RESERVED[(Byte)EReserved.EQUAL] == Symbol.Text) ||
                      (CParser.RESERVED[(Byte)EReserved.UNEQUAL] == Symbol.Text) ||
                      (CParser.RESERVED[(Byte)EReserved.LOWEREQUAL] == Symbol.Text) ||
                      (CParser.RESERVED[(Byte)EReserved.GREATEREQUAL] == Symbol.Text))
                  {
                    if (!IncrementSymbolIndex(ref symbolindex))
                    {
                      return true;
                    }
                    if (!ConvertTerm(ref symbolindex))
                    {
                      return false;
                    }
                    COperator Operator = new COperator(Symbol.Text);
                    FOperations.Add(Operator);
                  } else
                  {
                    LoopActive = false;
                  }
          LoopActive &= CheckSymbolIndex(symbolindex);
        }
      }
      // Unary Operators
      if (SignADD)
      {
        FOperations.Add(new CSign(new String((Char)ETerminal.ADD, 1)));
      } else
        if (SignSUB)
        {
          FOperations.Add(new CSign(new String((Char)ETerminal.SUB, 1)));
        } 
     return true;
    }
    //
    //------------------------------------
    // Private Management
    //------------------------------------
    //
    private Boolean Parse(String text)
    {
      Int32 Index = 0;
      String Token = "";
      ESymbolKind SymbolKind = ESymbolKind.Comment;
      CParser Parser = new CParser();
      FSymbols.Clear();
      while (Parser.Execute(text, ref Index, ref Token, ref SymbolKind))
      {
        CSymbol Symbol = null;
        switch (SymbolKind)
        {
          case ESymbolKind.Operator:
            Symbol = new COperator(Token);
            break;
          case ESymbolKind.Identifier:
            Symbol = new CIdentifier(FVariables, Token);
            break;
          case ESymbolKind.Integer:
            Symbol = new CInteger(Token);
            break;
          case ESymbolKind.Floating:
            Symbol = new CFloating(Token);
            break;
          case ESymbolKind.Reserved:
            Symbol = new CReserved(Token);
            break;
          case ESymbolKind.Comment:
            Symbol = new CComment(Token);
            break;
          default: // Undefined
            Symbol = null;
            break;
        }
        if (Symbol is CSymbol)
        {
          FSymbols.Add(Symbol);
        }
      }
      return (0 < FSymbols.Count);
    }


    private Boolean CheckTokenSymmetry(Int32 symbolindex)
    {
      Int32 ParantheseCount = 0;
      Int32 OpenCount = 0;
      foreach (CSymbol Symbol in FSymbols)
      {
        if (new String((Char)ETerminal.POPEN, 1) == Symbol.Text)
        {
          ParantheseCount++;
          OpenCount++;
        } else
        if (new String((Char)ETerminal.PCLOSE, 1) == Symbol.Text)
        {
          ParantheseCount++;
          OpenCount--;
        } 
      }
      Boolean CountSymmetry = (0 == (FSymbols.Count - ParantheseCount - FOperations.Count));
      Boolean ParantheseSymmetry = (0 == OpenCount);
      Boolean SymbolSymmetry = (symbolindex == FSymbols.Count);
      return (CountSymmetry && ParantheseSymmetry && SymbolSymmetry);
    }


    private Boolean ConvertToOperations()
    {
      FOperations.Clear();
      Int32 SymbolIndex = 0;
      if (ConvertExpression(ref SymbolIndex))
      {
        return CheckTokenSymmetry(SymbolIndex);
      }
      return false;
    }
    //
    //------------------------------------
    // Public Management
    //------------------------------------
    //
    public Boolean AddVariableDouble(String name, String expression)
    {
      CVariableDouble Variable = new CVariableDouble(name, expression);
      if (Variable is CVariable)
      {
        FVariables.Add(Variable);
        return true;
      }
      return false;
    }


    public Boolean Translate(String text)
    {
      try
      {
        if (Parse(text))
        {
          return ConvertToOperations();
        }
        return false;
      }
      catch (Exception)
      {
        // _error
        return false;
      }
    }

    public Boolean Calculate(ref Double value)
    {
      try
      {
        CStack Stack = new CStack();
        foreach (CSymbol Operation in FOperations)
        {
          if (!Operation.Calculate(ref Stack))
          {
            return false;
          }
        }
        value = Stack.Pop();
        return true;
      }
      catch (Exception)
      {
        // _error
        return false;
      }
    }



  }
}
