﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLExpression
{
  public class CExpressionHeader
  {
		//
		//	Types
		//
		public const String STRING = "String";
		public const String BYTE = "Byte";
		public const String INT32 = "Int32";
		public const String DOUBLE = "Double";
		//
		//	Common
		//
		public const String NAME_EXPRESSION = "Expression";
		//
		//	Expression
		//
		public const String NAME_VARIABLE = "Variable";
		public const String NAME_NAME = "Name";
		public const String NAME_TYPE = "Type";
		public const String NAME_PRESET = "Preset";
    //
    public const String NAME_FORMAT = "Format";

		
	}
}

