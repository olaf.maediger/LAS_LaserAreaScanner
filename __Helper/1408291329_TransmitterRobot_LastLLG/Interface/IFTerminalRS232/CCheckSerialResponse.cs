﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  class CCheckSerialResponse : CCommand
  {
    public const String INIT_NAME = "CheckSerialResponse";

    private String FBaudrate;
    private String FCommand;
    private String FResponse;
   //??? private String FCollector;

		public CCheckSerialResponse(CCommandlist parent,
								                CNotifier notifier,
									              CDevice device,
									              CHWComPort comport,
                                String baudrate,
                                String command,
                                String response)
      : base(parent, INIT_NAME,
             String.Format("{0}", command),
             notifier, device, comport)
    {
      SetParent(this);
      SetOnCommandExecuteBegin(null);
      SetOnCommandExecuteBusy(ThreadExecuteBusy);
      SetOnCommandExecuteResponse(null);
      SetOnCommandExecuteEnd(null);
      //
      FBaudrate = baudrate;
      FCommand = command;
      FResponse = response;
      //?????FCollector = "";
    }

    protected void ThreadExecuteBusy()
    {
      HWComPort.WriteText(FCommand);
      // Add Variable
      Variable = new CVariableTextVector(3);
      Variable.Name = INIT_NAME;
      Variable.Time = CNotifier.BuildDateTimeHeader();
      ((CVariableTextVector)Variable).SetIndexText(0, FBaudrate);
      ((CVariableTextVector)Variable).SetIndexText(1, FCommand);
    }

    public override Boolean ExecuteDataReceived(String data)
    {
      ((CVariableTextVector)Variable).SetIndexText(2, data);
      return true;
    }



  }
}
