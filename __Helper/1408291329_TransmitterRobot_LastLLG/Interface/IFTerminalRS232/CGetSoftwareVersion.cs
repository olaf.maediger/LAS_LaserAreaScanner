﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using HWComPort;

namespace IFTerminalRS232
{
	public class CGetSoftwareVersion : CCommand
	{
    public const String INIT_NAME = "GetSoftwareVersion";
    public const String COMMAND_TEXT = "GSV";

		public CGetSoftwareVersion(CCommandlist parent,
											 CNotifier notifier,
											 CDevice device,
											 CHWComPort comport)
      : base(parent, INIT_NAME, COMMAND_TEXT, notifier, device, comport)
    {
      SetParent(this);
      SetOnCommandExecuteResponse(SelfOnCommandResponseReceived);
    }

    public void SelfOnCommandResponseReceived(Guid comportid,
                                              String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
			Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
			String[] Tokenlist = line.Split(' ');
			if (2 < Tokenlist.Length)
			{
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Commandlist.Library.RefreshSoftwareVersion(Tokenlist[2]);
			}
		}

	}
}
