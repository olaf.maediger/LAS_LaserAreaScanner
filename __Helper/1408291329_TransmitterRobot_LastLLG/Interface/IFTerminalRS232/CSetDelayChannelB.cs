﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  public class CSetDelayChannelB : CCommand
  {
    public const String INIT_NAME = "SetDelayChannelB";
    public const String COMMAND_TEXT = "SDB";

    public CSetDelayChannelB(CCommandlist parent,
                             Int32 delay,
                             CNotifier notifier,
                             CDevice device,
                             CHWComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, delay),
             notifier, device, comport)
    {
      SetParent(this);
      SetOnCommandExecuteResponse(SelfOnSerialResponseReceived);
    }

    public void SelfOnSerialResponseReceived(Guid comportid,
                                             String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = line.Split(' ');
      if (2 < Tokenlist.Length)
      {
        CommandlistParent.Library.RefreshDelayChannelB(Tokenlist[2]);
      }
    }

  }
}
