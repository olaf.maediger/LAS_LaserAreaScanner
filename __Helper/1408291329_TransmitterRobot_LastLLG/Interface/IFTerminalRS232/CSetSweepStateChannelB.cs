﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  public class CSetSweepStateChannelB : CCommand
  {
    public const String INIT_NAME = "SetSweepStateChannelB";
    public const String COMMAND_TEXT = "SWB";

    private ESweepState FSweepState;

    public CSetSweepStateChannelB(CCommandlist parent,
                                  ESweepState value,
                                  CNotifier notifier,
                                  CDevice device,
                                  CHWComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, (Int32)value),
             notifier, device, comport)
    {
      SetParent(this);
      FSweepState = value;
      SetOnCommandExecuteResponse(SelfOnSerialResponseReceived);
    }

    public void SelfOnSerialResponseReceived(Guid comportid,
                                             String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = line.Split(' ');
      if (2 < Tokenlist.Length)
      {
        // NC ESweepState SS = CIFTerminalRS232.StringIndexToSweepState(Tokenlist[2]);
        //!!!!!!!!!!!!!CommandlistParent.Library.RefreshSweepStateChannelB(SS);
      }
    }
  }
}
