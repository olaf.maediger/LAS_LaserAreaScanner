﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IFTerminalRS232
{
  public enum EVariableKind
  { // Scalar
    vkByte,
    vkUInt32,
    vkInt32,
    vkDouble,
    vkText,
    // Vector
    vkInt32Vector,
    vkDoubleVector,
    vkTextVector
  }

  public class CVariable
  {
    protected EVariableKind FKind;
    protected String FName;
    protected String FTime;

    public CVariable(EVariableKind kind)
    {
      FKind = kind;
    }

    public CVariable(EVariableKind kind,
                     String name)
    {
      FKind = kind;
      FName = name;
    }



    public String Name
    {
      get { return FName; }
      set { FName = value; }
    }

    public String Time
    {
      get { return FTime; }
      set { FTime = value; }
    }

    public virtual void Debug()
    {
      Console.WriteLine(String.Format(" Variable[{0}]:", Name));
      Console.WriteLine(String.Format("  Time = {0}:", Time));
    }


  }
}
