﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UCNotifier;
using HWComPort;

namespace IFTerminalRS232
{
  //
  public delegate void DOnCommandExecuteBegin();
  public delegate void DOnCommandExecuteBusy();
  public delegate void DOnCommandExecuteResponse(Guid portid, String data);
  public delegate void DOnCommandExecuteEnd();
  //
  public delegate void DParentOnExecuteEnd(CCommand command);

	public enum ECommandState 
	{ 
		Error = -1,
		Init = 0,
		Begin = 1, 
		Busy = 2, 
    Response = 3,
		Abort = 4,
		End = 5
	};

	public abstract class CCommand //: CCommandlist // local Commandlist
	{
    protected const Int32 INIT_WAITFOR = 1000;

    private CNotifier FNotifier;        // Only Reference!
		protected CNotifier Notifier
		{
			get { return FNotifier; }
		}

		private CCommandlist FCommandlistParent;  // Only Reference (Parent-Commandlist)!
    protected CCommandlist CommandlistParent
    {
			get { return FCommandlistParent; }
		}
    protected CVariablelist VariablelistParent
    {
      get { return FCommandlistParent.Variablelist; }
    }


		private CCommandlist FCommandlistLocal;  // local Commandlist (Block-Commands)
		protected CCommandlist CommandlistLocal
		{
			get { return FCommandlistLocal; }
		}

    private CCommand FParent;           // Only Reference!
    protected void SetParent(CCommand parent)
    {
      FParent = parent;
    }





    private String FName;
    protected String Name
    {
      get { return FName; }
    }

    private String FCommandText;
    protected String CommandText
    {
      get { return FCommandText; }
    }
    public void SetCommandText(String value)
    {
      FCommandText = value;
    }

    private ECommandState FState; // Threadlist-Manager!

		private void SetState(ECommandState value)
		{
			if (value != FState)
			{
				FState = value;
        //
				// Debug 
        String Line = String.Format("Command[{0}]: State[{1}]", Name, State);
        // Debug 
        Console.Write(Line);
      }
		}
		public ECommandState State
		{
			get { return FState; }
			set { SetState(value); }
		}
		protected Thread FThread;
		private CDevice FDevice;
		protected CDevice Device
		{
			get { return FDevice; }
		}

		private CHWComPort FComPort;
		protected CHWComPort HWComPort
		{
			get { return FComPort; }
		}


    private DOnCommandExecuteBegin FOnCommandExecuteBegin;
    private DOnCommandExecuteBusy FOnCommandExecuteBusy;
    private DOnCommandExecuteResponse FOnCommandExecuteResponse;
    private DOnCommandExecuteEnd FOnCommandExecuteEnd;
    //
    private DParentOnExecuteEnd FParentOnExecuteEnd;


    private CVariable FVariable;
    protected CVariable Variable
    {
      get { return FVariable;  }
      set { FVariable = value; }
    }



    private CEvent FWaitResponse;
    private Int32 FLineCount;

    public CCommand(CCommandlist commandlistparent,
										String name,
                    String commandtext,
										CNotifier notifier,
									  CDevice device,
										CHWComPort comport)
		{
      FCommandlistParent = commandlistparent;
      FCommandlistLocal = null;
      FName = name;
      FCommandText = commandtext;
      FNotifier = notifier;
			FDevice = device;
			FComPort = comport;
      FThread = null;
      FWaitResponse = new CEvent();
		}

    protected void IncrementLineCount()
    {
      FLineCount++;
    }

    protected void BaseOnLineReceived()
    {
      IncrementLineCount();
      if (1 <= FLineCount)
      {
        FWaitResponse.Set();
      }
    }

		public void Abort()
		{
			State = ECommandState.Abort;
      FThread.Abort();
			AbortExecution();
		}

		public void Join()
		{
			FThread.Join();
		}

    // -> Member!!!public abstract void SelfOnLineReceived(Guid comportid,
    //                                        String line);



    protected void SetOnCommandExecuteBegin(DOnCommandExecuteBegin value)
    {
      FOnCommandExecuteBegin = value;
    }

    protected void SetOnCommandExecuteBusy(DOnCommandExecuteBusy value)
    {
      FOnCommandExecuteBusy = value;
    }

    protected void SetOnCommandExecuteResponse(DOnCommandExecuteResponse value)
    {
      FOnCommandExecuteResponse = value;
    }

    protected void SetOnCommandExecuteEnd(DOnCommandExecuteEnd value)
    {
      FOnCommandExecuteEnd = value;
    }




    public Boolean StartExecution(DParentOnExecuteEnd value)
		{ // debug 
      try
      {
        //FNotifier.Write(String.Format("Command[{0}].Activate", this.ToString()));
        Console.WriteLine(String.Format("Command[{0}].Activate", this.ToString()));
        FParentOnExecuteEnd = value;
        if (null == FThread)
        {
          FThread = new Thread(new ThreadStart(ThreadExecute));
          // debug FNotifier.Write(String.Format("Thread[{0}].Start", this.ToString()));
          Console.WriteLine(String.Format("Thread[{0}].Start", this.ToString()));
          FThread.Start();
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        FNotifier.Write(" Cannot start Command-Execution");
        return false;
      }
    }

    public Boolean AbortExecution()
		{
      try
      {
        if (FThread is Thread)
        {
					State = ECommandState.Abort;
          return true;
				}
        return false;
      }
      catch (Exception)
      {
        FNotifier.Write(" Cannot abort Command-Execution");
        return false;
      }
    }


    //
    //---------------------------------------------------------------------------
    //  Segment - Execution
    //---------------------------------------------------------------------------
    //
    protected void SelfExecuteBegin()
    {
      Console.WriteLine(String.Format("Command[{0}].ExecuteBegin", this.ToString()));
      State = ECommandState.Begin;
      //
      if (FOnCommandExecuteBegin is DOnCommandExecuteBegin)
      {
        FOnCommandExecuteBegin();
        Thread.Sleep(1000);
      }
    }

    protected void SelfExecuteBusy()
    {
      State = ECommandState.Busy;
      Console.WriteLine(String.Format("Command[{0}].ExecuteBusy", this.ToString()));
      //
      if (FOnCommandExecuteBusy is DOnCommandExecuteBusy)
      {
        FOnCommandExecuteBusy();
        Thread.Sleep(1000);
      }
    }

    protected void SelfExecuteResponse()
    {     
      if (FOnCommandExecuteResponse is DOnCommandExecuteResponse)
      {
        State = ECommandState.Response;
        Console.WriteLine(String.Format("Command[{0}].ExecuteResponse", this.ToString()));
        //
        FLineCount = 0;
        FWaitResponse.Reset();
        //
        // debug Thread.Sleep(1);
        // not here!!! HWComPort.WriteLine(CommandText);
        if (!FWaitResponse.WaitFor(INIT_WAITFOR))
        {
          State = ECommandState.Error;
          FNotifier.Error(CIFTerminalRS232.HEADER_LIBRARY + " " + FName,
                          1, "No response of hardware");
        }
        else
        {
          FOnCommandExecuteResponse(Guid.Empty, "ABC");
        }
      }
    }

    protected void SelfExecuteEnd()
    {
      State = ECommandState.End;
      Console.WriteLine(String.Format("Command[{0}].ExecuteEnd", this.ToString()));
      //
      if (FOnCommandExecuteEnd is DOnCommandExecuteEnd)
      {
        FOnCommandExecuteEnd();
        Thread.Sleep(1000);
      }
    }

    protected void ThreadExecute()
    {
      Console.WriteLine(String.Format("Command[{0}] startet", this.ToString()));
      SelfExecuteBegin();
      SelfExecuteBusy();
      SelfExecuteResponse();
      SelfExecuteEnd();
      //
      FParentOnExecuteEnd(this);
      // debug Thread.Sleep(100);
      FThread = null;
      Console.WriteLine(String.Format("Command[{0}] finnished", this.ToString()));
      if (FVariable is CVariable)
      {
        VariablelistParent.Add(FVariable);
      }
    }



    public virtual Boolean ExecuteDataReceived(String data)
    {
      return true;
    }

  }
}
