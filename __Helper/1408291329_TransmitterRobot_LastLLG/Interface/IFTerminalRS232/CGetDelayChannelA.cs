﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  class CGetDelayChannelA : CCommand
  {
    public const String INIT_NAME = "GetDelayChannelA";
    public const String COMMAND_TEXT = "GDA";

    public CGetDelayChannelA(CCommandlist parent,
                             CNotifier notifier,
                             CDevice device,
                             CHWComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT),
             notifier, device, comport)
    {
      SetParent(this);
      SetOnCommandExecuteResponse(SelfOnSerialResponseReceived);
    }

    public void SelfOnSerialResponseReceived(Guid comportid,
                                             String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = line.Split(' ');
      if (2 < Tokenlist.Length)
      {
        CommandlistParent.Library.RefreshDelayChannelA(Tokenlist[2]);
      }
    }

  }
}
