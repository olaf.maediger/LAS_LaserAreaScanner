﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  public class CCloseSerial : CCommand
  {
    public const String INIT_NAME = "CloseSerial";

    public CCloseSerial(CCommandlist parent,
                        CNotifier notifier,
                        CDevice device,
                        CHWComPort comport)
      : base(parent, INIT_NAME,
             "",
             notifier, device, comport)
    {
      SetParent(this);
      SetOnCommandExecuteBegin(null);
      SetOnCommandExecuteBusy(ThreadExecuteBusy);
      SetOnCommandExecuteResponse(null);
      SetOnCommandExecuteEnd(null);
    }

    protected void ThreadExecuteBusy()
    {
      RComPortData ComPortData;
      Boolean Result = HWComPort.GetComPortData(out ComPortData);
      Result &= HWComPort.Close();
      //
      Variable = new CVariableTextVector(8);
      Variable.Name = INIT_NAME;
      Variable.Time = CNotifier.BuildDateTimeHeader();
      String SValue = String.Format("Closed[{0}]", HWComPort.IsClosed().ToString());
      ((CVariableTextVector)Variable).SetIndexText(0, SValue);
      ((CVariableTextVector)Variable).SetIndexText(1, ComPortData.Name);
      ((CVariableTextVector)Variable).SetIndexText(2, CHWComPort.ComPortName(ComPortData.ComPort));
      ((CVariableTextVector)Variable).SetIndexText(3, ComPortData.Baudrate.ToString());
      ((CVariableTextVector)Variable).SetIndexText(4, ComPortData.Parity.ToString());
      ((CVariableTextVector)Variable).SetIndexText(5, ComPortData.Databits.ToString());
      ((CVariableTextVector)Variable).SetIndexText(6, ComPortData.Stopbits.ToString());
      ((CVariableTextVector)Variable).SetIndexText(7, ComPortData.Handshake.ToString());
    }



  }
}
