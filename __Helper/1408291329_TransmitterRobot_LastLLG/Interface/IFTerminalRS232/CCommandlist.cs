﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UCNotifier;
using HWComPort;

namespace IFTerminalRS232
{
  // NC 	public delegate void DOnCommandExecuteEnd(CCommand command);
  // NC public delegate void DOnSerialResponseReceived(Guid comportid,
  // NC                                          String response);

  public delegate void DOnReceived(String line);

  public delegate void DOnComPortDataTransmitted(String data);
  public delegate void DOnComPortDataReceived(String data);

  public class CCommandlist : List<CCommand>
	{
    private CIFTerminalRS232Base FLibrary;
		private CNotifier FNotifier;
   // NC  private Int32 FCommandIndex;
    private CVariablelist FVariablelist;

    public CCommandlist(CIFTerminalRS232Base library)
		{
      FLibrary = library;
      FVariablelist = new CVariablelist();
		}

    public CIFTerminalRS232Base Library
    {
      get { return FLibrary; }
    }

    public CVariablelist Variablelist
    {
      get { return FVariablelist; }
    }

		public void SetNotifier(CNotifier notifier)
		{
			FNotifier = notifier;
		}

    public new Boolean Add(CCommand command)
    {
      // debug 
      FNotifier.Write("Add Command: " + command.ToString());
      base.Add(command);
      return (0 < this.Count);
    }

    public new Boolean Clear()
    {
      base.Clear();
      return true;
    }

    public CCommand GetCommandActual()
    {
      CCommand Result = null;
      if (0 < Count)
      {
        Result = base[0];
      }
      if (0 == Count)
      {
        AbortExecution();
      }
      return Result;
    }

    public Boolean StartExecution()
    { // debug Console.WriteLine("Commandlist.StartExecution");
      if (0 < Count)
      {
        CCommand Command = base[0];
        Command.StartExecution(SelfOnCommandExecuteEnd);
        return true;
      }
      return false;
    }

    public Boolean AbortExecution()
    { // debug Console.WriteLine("Commandlist.AbortExecution");
      CCommand Command = null;
			if (0 < Count)
			{
        Command = base[0];
        if (Command is CCommand)
				{
					Command.Abort();
				}
			}
      Clear();
			return true;
		}

		public void SelfOnCommandExecuteEnd(CCommand command)
		{ // prepare next command
      CCommand Command = null;
      if (0 < Count)
      {
        Command = base[0];
        base.Remove(Command);
      }
      if (0 < Count)
      {
        Command = base[0];
        if (Command is CCommand)
        {
          String Line = String.Format("ActivateCommand[{0}]", Command.ToString());
          Console.WriteLine(Line);
          Command.StartExecution(SelfOnCommandExecuteEnd);
        }
      }
		}


    public void DebugVariablelist()
    {
      FVariablelist.Debug();
    }


    /* ?????public void SetOnSerialResponseReceived(DOnSerialResponseReceived value)
    {
      FLibrary.SetOnSerialResponseReceived(value);
    }*/

    /*ublic void SetOnRXDLineReceived(DOnRXDLineReceived onrxdlinereceived)
    {
      FLibrary.SetOnRXDLineReceived(onrxdlinereceived);
    }*/

    /* NC public Boolean LoadProgramFromXml(String programentry)
    {

    }*/

	}
}
