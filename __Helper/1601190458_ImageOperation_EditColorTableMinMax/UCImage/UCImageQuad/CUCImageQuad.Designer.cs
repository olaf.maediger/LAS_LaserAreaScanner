﻿namespace UCImageQuad
{
  partial class CUCImageQuad
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlInput = new System.Windows.Forms.Panel();
      this.cbxVisibleBottomLeft = new System.Windows.Forms.CheckBox();
      this.cbxVisibleTopLeft = new System.Windows.Forms.CheckBox();
      this.cbxVisibleBottomRight = new System.Windows.Forms.CheckBox();
      this.cbxVisibleTopRight = new System.Windows.Forms.CheckBox();
      this.label3 = new System.Windows.Forms.Label();
      this.cbxAutoSizeVertical = new System.Windows.Forms.CheckBox();
      this.cbxAutoSizeHorizontal = new System.Windows.Forms.CheckBox();
      this.label1 = new System.Windows.Forms.Label();
      this.pnlTop = new System.Windows.Forms.Panel();
      this.pnlTopRight = new System.Windows.Forms.Panel();
      this.pbxImageTopRight = new System.Windows.Forms.PictureBox();
      this.splTop = new System.Windows.Forms.Splitter();
      this.pnlTopLeft = new System.Windows.Forms.Panel();
      this.pbxImageTopLeft = new System.Windows.Forms.PictureBox();
      this.splHorizontal = new System.Windows.Forms.Splitter();
      this.pnlBottom = new System.Windows.Forms.Panel();
      this.splBottom = new System.Windows.Forms.Splitter();
      this.pnlBottomRight = new System.Windows.Forms.Panel();
      this.pbxImageBottomRight = new System.Windows.Forms.PictureBox();
      this.pnlBottomLeft = new System.Windows.Forms.Panel();
      this.pbxImageBottomLeft = new System.Windows.Forms.PictureBox();
      this.pnlInput.SuspendLayout();
      this.pnlTop.SuspendLayout();
      this.pnlTopRight.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageTopRight)).BeginInit();
      this.pnlTopLeft.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageTopLeft)).BeginInit();
      this.pnlBottom.SuspendLayout();
      this.pnlBottomRight.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageBottomRight)).BeginInit();
      this.pnlBottomLeft.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageBottomLeft)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlInput
      // 
      this.pnlInput.Controls.Add(this.cbxVisibleBottomLeft);
      this.pnlInput.Controls.Add(this.cbxVisibleTopLeft);
      this.pnlInput.Controls.Add(this.cbxVisibleBottomRight);
      this.pnlInput.Controls.Add(this.cbxVisibleTopRight);
      this.pnlInput.Controls.Add(this.label3);
      this.pnlInput.Controls.Add(this.cbxAutoSizeVertical);
      this.pnlInput.Controls.Add(this.cbxAutoSizeHorizontal);
      this.pnlInput.Controls.Add(this.label1);
      this.pnlInput.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlInput.Location = new System.Drawing.Point(0, 489);
      this.pnlInput.Name = "pnlInput";
      this.pnlInput.Size = new System.Drawing.Size(575, 44);
      this.pnlInput.TabIndex = 0;
      // 
      // cbxVisibleBottomLeft
      // 
      this.cbxVisibleBottomLeft.AutoSize = true;
      this.cbxVisibleBottomLeft.Location = new System.Drawing.Point(180, 26);
      this.cbxVisibleBottomLeft.Name = "cbxVisibleBottomLeft";
      this.cbxVisibleBottomLeft.Size = new System.Drawing.Size(15, 14);
      this.cbxVisibleBottomLeft.TabIndex = 8;
      this.cbxVisibleBottomLeft.UseVisualStyleBackColor = true;
      this.cbxVisibleBottomLeft.CheckedChanged += new System.EventHandler(this.cbxVisibleBottomLeft_CheckedChanged);
      // 
      // cbxVisibleTopLeft
      // 
      this.cbxVisibleTopLeft.AutoSize = true;
      this.cbxVisibleTopLeft.Location = new System.Drawing.Point(180, 5);
      this.cbxVisibleTopLeft.Name = "cbxVisibleTopLeft";
      this.cbxVisibleTopLeft.Size = new System.Drawing.Size(15, 14);
      this.cbxVisibleTopLeft.TabIndex = 7;
      this.cbxVisibleTopLeft.UseVisualStyleBackColor = true;
      this.cbxVisibleTopLeft.CheckedChanged += new System.EventHandler(this.cbxVisibleTopLeft_CheckedChanged);
      // 
      // cbxVisibleBottomRight
      // 
      this.cbxVisibleBottomRight.AutoSize = true;
      this.cbxVisibleBottomRight.Location = new System.Drawing.Point(199, 26);
      this.cbxVisibleBottomRight.Name = "cbxVisibleBottomRight";
      this.cbxVisibleBottomRight.Size = new System.Drawing.Size(15, 14);
      this.cbxVisibleBottomRight.TabIndex = 6;
      this.cbxVisibleBottomRight.UseVisualStyleBackColor = true;
      this.cbxVisibleBottomRight.CheckedChanged += new System.EventHandler(this.cbxVisibleBottomRight_CheckedChanged);
      // 
      // cbxVisibleTopRight
      // 
      this.cbxVisibleTopRight.AutoSize = true;
      this.cbxVisibleTopRight.Location = new System.Drawing.Point(199, 5);
      this.cbxVisibleTopRight.Name = "cbxVisibleTopRight";
      this.cbxVisibleTopRight.Size = new System.Drawing.Size(15, 14);
      this.cbxVisibleTopRight.TabIndex = 5;
      this.cbxVisibleTopRight.UseVisualStyleBackColor = true;
      this.cbxVisibleTopRight.CheckedChanged += new System.EventHandler(this.cbxVisibleTopRight_CheckedChanged);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(144, 15);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(37, 13);
      this.label3.TabIndex = 4;
      this.label3.Text = "Visible";
      // 
      // cbxAutoSizeVertical
      // 
      this.cbxAutoSizeVertical.AutoSize = true;
      this.cbxAutoSizeVertical.Location = new System.Drawing.Point(54, 24);
      this.cbxAutoSizeVertical.Name = "cbxAutoSizeVertical";
      this.cbxAutoSizeVertical.Size = new System.Drawing.Size(61, 17);
      this.cbxAutoSizeVertical.TabIndex = 3;
      this.cbxAutoSizeVertical.Text = "Vertical";
      this.cbxAutoSizeVertical.UseVisualStyleBackColor = true;
      this.cbxAutoSizeVertical.CheckedChanged += new System.EventHandler(this.cbxAutoSizeVertical_CheckedChanged);
      // 
      // cbxAutoSizeHorizontal
      // 
      this.cbxAutoSizeHorizontal.AutoSize = true;
      this.cbxAutoSizeHorizontal.Location = new System.Drawing.Point(54, 4);
      this.cbxAutoSizeHorizontal.Name = "cbxAutoSizeHorizontal";
      this.cbxAutoSizeHorizontal.Size = new System.Drawing.Size(73, 17);
      this.cbxAutoSizeHorizontal.TabIndex = 1;
      this.cbxAutoSizeHorizontal.Text = "Horizontal";
      this.cbxAutoSizeHorizontal.UseVisualStyleBackColor = true;
      this.cbxAutoSizeHorizontal.CheckedChanged += new System.EventHandler(this.cbxAutoSizeHorizontal_CheckedChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(5, 14);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(49, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "AutoSize";
      // 
      // pnlTop
      // 
      this.pnlTop.BackColor = System.Drawing.SystemColors.Info;
      this.pnlTop.Controls.Add(this.pnlTopRight);
      this.pnlTop.Controls.Add(this.splTop);
      this.pnlTop.Controls.Add(this.pnlTopLeft);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(575, 238);
      this.pnlTop.TabIndex = 8;
      // 
      // pnlTopRight
      // 
      this.pnlTopRight.AutoScroll = true;
      this.pnlTopRight.Controls.Add(this.pbxImageTopRight);
      this.pnlTopRight.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlTopRight.Location = new System.Drawing.Point(286, 0);
      this.pnlTopRight.Name = "pnlTopRight";
      this.pnlTopRight.Size = new System.Drawing.Size(289, 238);
      this.pnlTopRight.TabIndex = 9;
      // 
      // pbxImageTopRight
      // 
      this.pbxImageTopRight.BackColor = System.Drawing.SystemColors.Control;
      this.pbxImageTopRight.Location = new System.Drawing.Point(0, 0);
      this.pbxImageTopRight.Name = "pbxImageTopRight";
      this.pbxImageTopRight.Size = new System.Drawing.Size(269, 218);
      this.pbxImageTopRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pbxImageTopRight.TabIndex = 8;
      this.pbxImageTopRight.TabStop = false;
      // 
      // splTop
      // 
      this.splTop.Location = new System.Drawing.Point(283, 0);
      this.splTop.Name = "splTop";
      this.splTop.Size = new System.Drawing.Size(3, 238);
      this.splTop.TabIndex = 8;
      this.splTop.TabStop = false;
      // 
      // pnlTopLeft
      // 
      this.pnlTopLeft.AutoScroll = true;
      this.pnlTopLeft.Controls.Add(this.pbxImageTopLeft);
      this.pnlTopLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlTopLeft.Location = new System.Drawing.Point(0, 0);
      this.pnlTopLeft.Name = "pnlTopLeft";
      this.pnlTopLeft.Size = new System.Drawing.Size(283, 238);
      this.pnlTopLeft.TabIndex = 7;
      // 
      // pbxImageTopLeft
      // 
      this.pbxImageTopLeft.BackColor = System.Drawing.SystemColors.Control;
      this.pbxImageTopLeft.Location = new System.Drawing.Point(0, 0);
      this.pbxImageTopLeft.Name = "pbxImageTopLeft";
      this.pbxImageTopLeft.Size = new System.Drawing.Size(269, 218);
      this.pbxImageTopLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pbxImageTopLeft.TabIndex = 8;
      this.pbxImageTopLeft.TabStop = false;
      // 
      // splHorizontal
      // 
      this.splHorizontal.Dock = System.Windows.Forms.DockStyle.Top;
      this.splHorizontal.Location = new System.Drawing.Point(0, 238);
      this.splHorizontal.Name = "splHorizontal";
      this.splHorizontal.Size = new System.Drawing.Size(575, 3);
      this.splHorizontal.TabIndex = 10;
      this.splHorizontal.TabStop = false;
      // 
      // pnlBottom
      // 
      this.pnlBottom.BackColor = System.Drawing.SystemColors.Info;
      this.pnlBottom.Controls.Add(this.splBottom);
      this.pnlBottom.Controls.Add(this.pnlBottomRight);
      this.pnlBottom.Controls.Add(this.pnlBottomLeft);
      this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlBottom.Location = new System.Drawing.Point(0, 241);
      this.pnlBottom.Name = "pnlBottom";
      this.pnlBottom.Size = new System.Drawing.Size(575, 248);
      this.pnlBottom.TabIndex = 11;
      // 
      // splBottom
      // 
      this.splBottom.Location = new System.Drawing.Point(283, 0);
      this.splBottom.Name = "splBottom";
      this.splBottom.Size = new System.Drawing.Size(3, 248);
      this.splBottom.TabIndex = 8;
      this.splBottom.TabStop = false;
      // 
      // pnlBottomRight
      // 
      this.pnlBottomRight.AutoScroll = true;
      this.pnlBottomRight.Controls.Add(this.pbxImageBottomRight);
      this.pnlBottomRight.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlBottomRight.Location = new System.Drawing.Point(283, 0);
      this.pnlBottomRight.Name = "pnlBottomRight";
      this.pnlBottomRight.Size = new System.Drawing.Size(292, 248);
      this.pnlBottomRight.TabIndex = 6;
      // 
      // pbxImageBottomRight
      // 
      this.pbxImageBottomRight.BackColor = System.Drawing.SystemColors.Control;
      this.pbxImageBottomRight.Location = new System.Drawing.Point(0, 0);
      this.pbxImageBottomRight.Name = "pbxImageBottomRight";
      this.pbxImageBottomRight.Size = new System.Drawing.Size(269, 218);
      this.pbxImageBottomRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pbxImageBottomRight.TabIndex = 10;
      this.pbxImageBottomRight.TabStop = false;
      // 
      // pnlBottomLeft
      // 
      this.pnlBottomLeft.AutoScroll = true;
      this.pnlBottomLeft.Controls.Add(this.pbxImageBottomLeft);
      this.pnlBottomLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlBottomLeft.Location = new System.Drawing.Point(0, 0);
      this.pnlBottomLeft.Name = "pnlBottomLeft";
      this.pnlBottomLeft.Size = new System.Drawing.Size(283, 248);
      this.pnlBottomLeft.TabIndex = 7;
      // 
      // pbxImageBottomLeft
      // 
      this.pbxImageBottomLeft.BackColor = System.Drawing.SystemColors.Control;
      this.pbxImageBottomLeft.Location = new System.Drawing.Point(0, 0);
      this.pbxImageBottomLeft.Name = "pbxImageBottomLeft";
      this.pbxImageBottomLeft.Size = new System.Drawing.Size(269, 218);
      this.pbxImageBottomLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pbxImageBottomLeft.TabIndex = 7;
      this.pbxImageBottomLeft.TabStop = false;
      // 
      // CUCImageQuad
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.pnlBottom);
      this.Controls.Add(this.splHorizontal);
      this.Controls.Add(this.pnlTop);
      this.Controls.Add(this.pnlInput);
      this.Name = "CUCImageQuad";
      this.Size = new System.Drawing.Size(575, 533);
      this.SizeChanged += new System.EventHandler(this.CUCImageQuad_SizeChanged);
      this.pnlInput.ResumeLayout(false);
      this.pnlInput.PerformLayout();
      this.pnlTop.ResumeLayout(false);
      this.pnlTopRight.ResumeLayout(false);
      this.pnlTopRight.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageTopRight)).EndInit();
      this.pnlTopLeft.ResumeLayout(false);
      this.pnlTopLeft.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageTopLeft)).EndInit();
      this.pnlBottom.ResumeLayout(false);
      this.pnlBottomRight.ResumeLayout(false);
      this.pnlBottomRight.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageBottomRight)).EndInit();
      this.pnlBottomLeft.ResumeLayout(false);
      this.pnlBottomLeft.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageBottomLeft)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlInput;
    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.Splitter splHorizontal;
    private System.Windows.Forms.Panel pnlBottom;
    private System.Windows.Forms.CheckBox cbxAutoSizeVertical;
    private System.Windows.Forms.CheckBox cbxAutoSizeHorizontal;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.CheckBox cbxVisibleBottomLeft;
    private System.Windows.Forms.CheckBox cbxVisibleTopLeft;
    private System.Windows.Forms.CheckBox cbxVisibleBottomRight;
    private System.Windows.Forms.CheckBox cbxVisibleTopRight;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Panel pnlTopRight;
    private System.Windows.Forms.Splitter splTop;
    private System.Windows.Forms.Panel pnlTopLeft;
    private System.Windows.Forms.PictureBox pbxImageTopRight;
    private System.Windows.Forms.PictureBox pbxImageTopLeft;
    private System.Windows.Forms.Splitter splBottom;
    private System.Windows.Forms.Panel pnlBottomRight;
    private System.Windows.Forms.PictureBox pbxImageBottomRight;
    private System.Windows.Forms.Panel pnlBottomLeft;
    private System.Windows.Forms.PictureBox pbxImageBottomLeft;


  }
}
