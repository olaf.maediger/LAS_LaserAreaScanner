﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
//
namespace UCImageQuad
{
  public partial class CUCImageQuad : UserControl
  {
    //
    //-----------------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------------
    //
    public const String FORMAT_SECTION = "UCImageQuad{0}";
    public const String NAME_AUTOSIZEHORIZONTAL = "AutoSizeHorizontal";
    public const Boolean INIT_AUTOSIZEHORIZONTAL = true;
    public const String NAME_AUTOSIZEVERTICAL = "AutoSizeVertical";
    public const Boolean INIT_AUTOSIZEVERTICAL = true;
    public const String NAME_VISIBLETOPLEFT = "VisibleTopLeft";
    public const Boolean INIT_VISIBLETOPLEFT = true;
    public const String NAME_VISIBLETOPRIGHT = "VisibleTopRight";
    public const Boolean INIT_VISIBLETOPRIGHT = true;
    public const String NAME_VISIBLEBOTTOMLEFT = "VisibleBottomLeft";
    public const Boolean INIT_VISIBLEBOTTOMLEFT = true;
    public const String NAME_VISIBLEBOTTOMRIGHT = "VisibleBottomRight";
    public const Boolean INIT_VISIBLEBOTTOMRIGHT = true;
    //
    //-----------------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------------
    //
    //
    //-----------------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------------
    //
    public CUCImageQuad()
    {
      InitializeComponent();
      InitControls();
    }
    //
    //-----------------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------------
    //
    public void SetImageTopLeft(Image image)
    {
      pbxImageTopLeft.Image = image;
    }
    public void SetImageTopRight(Image image)
    {
      pbxImageTopRight.Image = image;
    }
    public void SetImageBottomLeft(Image image)
    {
      pbxImageBottomLeft.Image = image;
    }
    public void SetImageBottomRight(Image image)
    {
      pbxImageBottomRight.Image = image;
    }
    //
    //-----------------------------------------------------------------
    //  Section - Init
    //-----------------------------------------------------------------
    //
    private void InitControls()
    {
      cbxAutoSizeHorizontal.Checked = INIT_AUTOSIZEHORIZONTAL;
      cbxAutoSizeVertical.Checked = INIT_AUTOSIZEVERTICAL;
      cbxVisibleTopLeft.Checked = INIT_VISIBLETOPLEFT;
      cbxVisibleTopRight.Checked = INIT_VISIBLETOPRIGHT;
      cbxVisibleBottomLeft.Checked = INIT_VISIBLEBOTTOMLEFT;
      cbxVisibleBottomRight.Checked = INIT_VISIBLEBOTTOMRIGHT;
    }

    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Result &= initdata.OpenSection(String.Format(FORMAT_SECTION, header));
      //
      Boolean BValue = INIT_AUTOSIZEHORIZONTAL;
      Result &= initdata.ReadBoolean(NAME_AUTOSIZEHORIZONTAL, out BValue, BValue);
      cbxAutoSizeHorizontal.Checked = BValue;
      //
      BValue = INIT_AUTOSIZEVERTICAL;
      Result &= initdata.ReadBoolean(NAME_AUTOSIZEVERTICAL, out BValue, BValue);
      cbxAutoSizeVertical.Checked = BValue;
      //
      BValue = INIT_VISIBLETOPLEFT;
      Result &= initdata.ReadBoolean(NAME_VISIBLETOPLEFT, out BValue, BValue);
      cbxVisibleTopLeft.Checked = BValue;
      //
      BValue = INIT_VISIBLETOPRIGHT;
      Result &= initdata.ReadBoolean(NAME_VISIBLETOPRIGHT, out BValue, BValue);
      cbxVisibleTopRight.Checked = BValue;
      //
      BValue = INIT_VISIBLEBOTTOMLEFT;
      Result &= initdata.ReadBoolean(NAME_VISIBLEBOTTOMLEFT, out BValue, BValue);
      cbxVisibleBottomLeft.Checked = BValue;
      //
      BValue = INIT_VISIBLEBOTTOMRIGHT;
      Result &= initdata.ReadBoolean(NAME_VISIBLEBOTTOMRIGHT, out BValue, BValue);
      cbxVisibleBottomRight.Checked = BValue;
      //
      Result &= initdata.CloseSection();
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      Result &= initdata.CreateSection(String.Format(FORMAT_SECTION, header));
      //
      Result &= initdata.WriteBoolean(NAME_AUTOSIZEHORIZONTAL, cbxAutoSizeHorizontal.Checked);
      Result &= initdata.WriteBoolean(NAME_AUTOSIZEVERTICAL, cbxAutoSizeVertical.Checked);
      Result &= initdata.WriteBoolean(NAME_VISIBLETOPLEFT, cbxVisibleTopLeft.Checked);
      Result &= initdata.WriteBoolean(NAME_VISIBLETOPRIGHT, cbxVisibleTopRight.Checked);
      Result &= initdata.WriteBoolean(NAME_VISIBLEBOTTOMLEFT, cbxVisibleBottomLeft.Checked);
      Result &= initdata.WriteBoolean(NAME_VISIBLEBOTTOMRIGHT, cbxVisibleBottomRight.Checked);
      //
      Result &= initdata.CloseSection();
      return Result;
    }
    //
    //-----------------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------------
    //
    private void FitImages()
    {
      Boolean VTL = cbxVisibleTopLeft.Checked;
      Boolean VTR = cbxVisibleTopRight.Checked;
      Boolean VBL = cbxVisibleBottomLeft.Checked;
      Boolean VBR = cbxVisibleBottomRight.Checked;
      Boolean PTV = VTL || VTR;
      Boolean PBV = VBL || VBR;
      pnlTop.Visible = PTV;
      pnlBottom.Visible = PBV;
      if (PBV)
      {
        pnlTop.Dock = DockStyle.Top;
      }
      else
      {
        pnlTop.Dock = DockStyle.Fill;
      }
      if (VTR)
      {
        pnlTopLeft.Dock = DockStyle.Left;
      }
      else
      {
        pnlTopLeft.Dock = DockStyle.Fill;
      }
      if (VBR)
      {
        pnlBottomLeft.Dock = DockStyle.Left;
      }
      else
      {
        pnlBottomLeft.Dock = DockStyle.Fill;
      }
      pnlTopLeft.Visible = VTL;
      pnlTopRight.Visible = VTR;
      pnlBottomLeft.Visible = VBL;
      pnlBottomRight.Visible = VBR;
      splTop.Visible = VTL && VTR;
      splBottom.Visible = VBL && VBR;
      splHorizontal.Visible = PTV && PBV;
    }
    //
    //-----------------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------------
    //
    private void CUCImageQuad_SizeChanged(object sender, EventArgs e)
    {
      Int32 CWH = (pnlTop.Width - splTop.Width) / 2;
      Int32 CHH = (ClientRectangle.Height - pnlInput.Height) / 2;
      if (cbxAutoSizeHorizontal.Checked)
      {
        if (pnlTopLeft.Visible && pnlTopRight.Visible)
        {
          pnlTopLeft.Width = CWH;
        }
        pnlBottomLeft.Width = CWH;
      }
      if (cbxAutoSizeVertical.Checked)
      {
        pnlTop.Height = CHH;
      }
    }

    private void cbxAutoSizeHorizontal_CheckedChanged(object sender, EventArgs e)
    {
      splTop.Enabled = !cbxAutoSizeHorizontal.Checked;
      splBottom.Enabled = !cbxAutoSizeHorizontal.Checked;
      CUCImageQuad_SizeChanged(this, null);
    }

    private void cbxAutoSizeVertical_CheckedChanged(object sender, EventArgs e)
    {
      splHorizontal.Enabled = !cbxAutoSizeVertical.Checked;
      CUCImageQuad_SizeChanged(this, null);
    }

    private void cbxVisibleTopLeft_CheckedChanged(object sender, EventArgs e)
    {
      FitImages();
    }

    private void cbxVisibleTopRight_CheckedChanged(object sender, EventArgs e)
    {
      FitImages();
    }

    private void cbxVisibleBottomLeft_CheckedChanged(object sender, EventArgs e)
    {
      FitImages();
    }

    private void cbxVisibleBottomRight_CheckedChanged(object sender, EventArgs e)
    {
      FitImages();
    }

  }
}
