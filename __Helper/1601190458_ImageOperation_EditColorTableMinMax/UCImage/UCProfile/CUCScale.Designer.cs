﻿namespace UCProfile
{
  partial class CUCScale
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.nudScaleFactor = new System.Windows.Forms.NumericUpDown();
      this.label12 = new System.Windows.Forms.Label();
      this.cbxVisibleRowRight = new System.Windows.Forms.CheckBox();
      this.cbxVisibleRowLeft = new System.Windows.Forms.CheckBox();
      this.cbxVisibleColBottom = new System.Windows.Forms.CheckBox();
      this.label11 = new System.Windows.Forms.Label();
      this.cbxVisibleColTop = new System.Windows.Forms.CheckBox();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleFactor)).BeginInit();
      this.SuspendLayout();
      // 
      // nudScaleFactor
      // 
      this.nudScaleFactor.Location = new System.Drawing.Point(168, 16);
      this.nudScaleFactor.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudScaleFactor.Name = "nudScaleFactor";
      this.nudScaleFactor.Size = new System.Drawing.Size(48, 20);
      this.nudScaleFactor.TabIndex = 13;
      this.nudScaleFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudScaleFactor.ValueChanged += new System.EventHandler(this.nudScaleFactor_ValueChanged);
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(160, 0);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(64, 13);
      this.label12.TabIndex = 12;
      this.label12.Text = "ScaleFactor";
      // 
      // cbxVisibleRowRight
      // 
      this.cbxVisibleRowRight.AutoSize = true;
      this.cbxVisibleRowRight.Location = new System.Drawing.Point(69, 39);
      this.cbxVisibleRowRight.Name = "cbxVisibleRowRight";
      this.cbxVisibleRowRight.Size = new System.Drawing.Size(73, 17);
      this.cbxVisibleRowRight.TabIndex = 11;
      this.cbxVisibleRowRight.Text = "RowRight";
      this.cbxVisibleRowRight.UseVisualStyleBackColor = true;
      this.cbxVisibleRowRight.CheckedChanged += new System.EventHandler(this.cbxVisibleRowRight_CheckedChanged);
      // 
      // cbxVisibleRowLeft
      // 
      this.cbxVisibleRowLeft.AutoSize = true;
      this.cbxVisibleRowLeft.Location = new System.Drawing.Point(1, 39);
      this.cbxVisibleRowLeft.Name = "cbxVisibleRowLeft";
      this.cbxVisibleRowLeft.Size = new System.Drawing.Size(66, 17);
      this.cbxVisibleRowLeft.TabIndex = 10;
      this.cbxVisibleRowLeft.Text = "RowLeft";
      this.cbxVisibleRowLeft.UseVisualStyleBackColor = true;
      this.cbxVisibleRowLeft.CheckedChanged += new System.EventHandler(this.cbxVisibleRowLeft_CheckedChanged);
      // 
      // cbxVisibleColBottom
      // 
      this.cbxVisibleColBottom.AutoSize = true;
      this.cbxVisibleColBottom.Location = new System.Drawing.Point(69, 16);
      this.cbxVisibleColBottom.Name = "cbxVisibleColBottom";
      this.cbxVisibleColBottom.Size = new System.Drawing.Size(74, 17);
      this.cbxVisibleColBottom.TabIndex = 9;
      this.cbxVisibleColBottom.Text = "ColBottom";
      this.cbxVisibleColBottom.UseVisualStyleBackColor = true;
      this.cbxVisibleColBottom.CheckedChanged += new System.EventHandler(this.cbxVisibleColBottom_CheckedChanged);
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(-2, 0);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(37, 13);
      this.label11.TabIndex = 8;
      this.label11.Text = "Visible";
      // 
      // cbxVisibleColTop
      // 
      this.cbxVisibleColTop.AutoSize = true;
      this.cbxVisibleColTop.Location = new System.Drawing.Point(1, 16);
      this.cbxVisibleColTop.Name = "cbxVisibleColTop";
      this.cbxVisibleColTop.Size = new System.Drawing.Size(60, 17);
      this.cbxVisibleColTop.TabIndex = 7;
      this.cbxVisibleColTop.Text = "ColTop";
      this.cbxVisibleColTop.UseVisualStyleBackColor = true;
      this.cbxVisibleColTop.CheckedChanged += new System.EventHandler(this.cbxVisibleColTop_CheckedChanged);
      // 
      // CUCScale
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.nudScaleFactor);
      this.Controls.Add(this.label12);
      this.Controls.Add(this.cbxVisibleRowRight);
      this.Controls.Add(this.cbxVisibleRowLeft);
      this.Controls.Add(this.cbxVisibleColBottom);
      this.Controls.Add(this.label11);
      this.Controls.Add(this.cbxVisibleColTop);
      this.Name = "CUCScale";
      this.Size = new System.Drawing.Size(240, 67);
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleFactor)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.NumericUpDown nudScaleFactor;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.CheckBox cbxVisibleRowRight;
    private System.Windows.Forms.CheckBox cbxVisibleRowLeft;
    private System.Windows.Forms.CheckBox cbxVisibleColBottom;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.CheckBox cbxVisibleColTop;
  }
}
