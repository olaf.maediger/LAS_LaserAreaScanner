﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLColorTable;
//
namespace UCProfile
{ //
  //---------------------------------------------------------------------
  //  Section - Definition
  //---------------------------------------------------------------------
  //
  public delegate void DOnScaleFactorChanged(Int32 scalefactor);
  public delegate void DOnVisibleColTopChanged(Boolean visible);
  public delegate void DOnVisibleColBottomChanged(Boolean visible);
  public delegate void DOnVisibleRowLeftChanged(Boolean visible);
  public delegate void DOnVisibleRowRightChanged(Boolean visible);
  //
  public partial class CUCScale : UserControl
  { //
    //---------------------------------------------------------------------
    //  Section - Constant
    //---------------------------------------------------------------------
    //
    private const String FORMAT_SECTION = "UCScale{0}";
    private const String NAME_VISIBLECOLTOP = "VisibleColTop";
    private const Boolean INIT_VISIBLECOLTOP = false;
    private const String NAME_VISIBLECOLBOTTOM = "VisibleColBottom";
    private const Boolean INIT_VISIBLECOLBOTTOM = false;
    private const String NAME_VISIBLEROWLEFT = "VisibleRowLeft";
    private const Boolean INIT_VISIBLEROWLEFT = false;
    private const String NAME_VISIBLEROWRIGHT = "VisibleRowRight";
    private const Boolean INIT_VISIBLEROWRIGHT = false;
    private const String NAME_SCALEFACTOR = "ScaleFactor";
    private const Int32 INIT_SCALEFACTOR = 4;
    //
    //---------------------------------------------------------------------
    //  Section - Field
    //---------------------------------------------------------------------
    //
    private DOnScaleFactorChanged FOnScaleFactorChanged;
    private DOnVisibleColTopChanged FOnVisibleColTopChanged;
    private DOnVisibleColBottomChanged FOnVisibleColBottomChanged;
    private DOnVisibleRowLeftChanged FOnVisibleRowLeftChanged;
    private DOnVisibleRowRightChanged FOnVisibleRowRightChanged;
    //
    //---------------------------------------------------------------------
    //  Section - Constructor
    //---------------------------------------------------------------------
    //
    public CUCScale()
    {
      InitializeComponent();
      //
      VisibleColTop = INIT_VISIBLECOLTOP;
      VisibleColBottom = INIT_VISIBLECOLBOTTOM;
      VisibleRowLeft = INIT_VISIBLEROWLEFT;
      VisibleRowRight = INIT_VISIBLEROWRIGHT;
      ScaleFactor = INIT_SCALEFACTOR;
    }
    //
    //---------------------------------------------------------------------
    //  Section - Property
    //---------------------------------------------------------------------
    //
    public void SetOnScaleFactorChanged(DOnScaleFactorChanged value)
    {
      FOnScaleFactorChanged = value;
      nudScaleFactor_ValueChanged(this, null);
    }
    public void SetOnVisibleColTopChanged(DOnVisibleColTopChanged value)
    {
      FOnVisibleColTopChanged = value;
      cbxVisibleColTop_CheckedChanged(this, null);
    }
    public void SetOnVisibleColBottomChanged(DOnVisibleColBottomChanged value)
    {
      FOnVisibleColBottomChanged = value;
      cbxVisibleColBottom_CheckedChanged(this, null);
    }
    public void SetOnVisibleRowLeftChanged(DOnVisibleRowLeftChanged value)
    {
      FOnVisibleRowLeftChanged = value;
      cbxVisibleRowLeft_CheckedChanged(this, null);
    }
    public void SetOnVisibleRowRightChanged(DOnVisibleRowRightChanged value)
    {
      FOnVisibleRowRightChanged = value;
      cbxVisibleRowRight_CheckedChanged(this, null);
    }

    public Boolean VisibleColTop
    {
      get { return cbxVisibleColTop.Checked; }
      set { cbxVisibleColTop.Checked = value; }
    }

    public Boolean VisibleColBottom
    {
      get { return cbxVisibleColBottom.Checked; }
      set { cbxVisibleColBottom.Checked = value; }
    }

    public Boolean VisibleRowLeft
    {
      get { return cbxVisibleRowLeft.Checked; }
      set { cbxVisibleRowLeft.Checked = value; }
    }

    public Boolean VisibleRowRight
    {
      get { return cbxVisibleRowRight.Checked; }
      set { cbxVisibleRowRight.Checked = value; }
    }
    
    public Int32 ScaleFactor
    {
      get { return (Int32)nudScaleFactor.Value; }
      set { nudScaleFactor.Value = (Decimal)value; }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Init
    //---------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Result &= initdata.OpenSection(String.Format(FORMAT_SECTION, header));
      //
      Boolean BValue = INIT_VISIBLECOLTOP;
      Result &= initdata.ReadBoolean(NAME_VISIBLECOLTOP, out BValue, BValue);
      VisibleColTop = BValue;
      //
      BValue = INIT_VISIBLECOLBOTTOM;
      Result &= initdata.ReadBoolean(NAME_VISIBLECOLBOTTOM, out BValue, BValue);
      VisibleColBottom = BValue;
      //
      BValue = INIT_VISIBLEROWLEFT;
      Result &= initdata.ReadBoolean(NAME_VISIBLEROWLEFT, out BValue, BValue);
      VisibleRowLeft = BValue;
      //
      BValue = INIT_VISIBLEROWRIGHT;
      Result &= initdata.ReadBoolean(NAME_VISIBLEROWRIGHT, out BValue, BValue);
      VisibleRowRight = BValue;
      //
      Int32 IValue = INIT_SCALEFACTOR;
      Result &= initdata.ReadInt32(NAME_SCALEFACTOR, out IValue, IValue);
      ScaleFactor = IValue;
      //
      Result &= initdata.CloseSection();
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      Result &= initdata.CreateSection(String.Format(FORMAT_SECTION, header));
      //
      Result &= initdata.WriteBoolean(NAME_VISIBLECOLTOP, VisibleColTop);
      Result &= initdata.WriteBoolean(NAME_VISIBLECOLBOTTOM, VisibleColBottom);
      Result &= initdata.WriteBoolean(NAME_VISIBLEROWLEFT, VisibleRowLeft);
      Result &= initdata.WriteBoolean(NAME_VISIBLEROWRIGHT, VisibleRowRight);
      Result &= initdata.WriteInt32(NAME_SCALEFACTOR, ScaleFactor);
      //
      Result &= initdata.CloseSection();
      return Result;
    }
    //
    //---------------------------------------------------------------------
    //  Section - Helper
    //---------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------
    //  Section - Event
    //---------------------------------------------------------------------
    //
    private void cbxVisibleColTop_CheckedChanged(object sender, EventArgs e)
    {
      if (FOnVisibleColTopChanged is DOnVisibleColTopChanged)
      {
        FOnVisibleColTopChanged(VisibleColTop);
      }
    }

    private void cbxVisibleColBottom_CheckedChanged(object sender, EventArgs e)
    {
      if (FOnVisibleColBottomChanged is DOnVisibleColBottomChanged)
      {
        FOnVisibleColBottomChanged(VisibleColBottom);
      }
    }

    private void cbxVisibleRowLeft_CheckedChanged(object sender, EventArgs e)
    {
      if (FOnVisibleRowLeftChanged is DOnVisibleRowLeftChanged)
      {
        FOnVisibleRowLeftChanged(VisibleRowLeft);
      }
    }

    private void cbxVisibleRowRight_CheckedChanged(object sender, EventArgs e)
    {
      if (FOnVisibleRowRightChanged is DOnVisibleRowRightChanged)
      {
        FOnVisibleRowRightChanged(VisibleRowRight);
      }
    }

    private void nudScaleFactor_ValueChanged(object sender, EventArgs e)
    {
      if (FOnScaleFactorChanged is DOnScaleFactorChanged)
      {
        FOnScaleFactorChanged(ScaleFactor);
      }
    }


  }
}
