﻿namespace UCProfile
{
  partial class CUCProfile
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlColTop = new System.Windows.Forms.Panel();
      this.label9 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.tbcProfile = new System.Windows.Forms.TabControl();
      this.tbpColorTable = new System.Windows.Forms.TabPage();
      this.FUCEditColorTable = new UCProfile.CUCEditColorTable();
      this.tbpScale = new System.Windows.Forms.TabPage();
      this.FUCScale = new UCProfile.CUCScale();
      this.tbpHide = new System.Windows.Forms.TabPage();
      this.pnlColBottom = new System.Windows.Forms.Panel();
      this.label10 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.pnlRowRight = new System.Windows.Forms.Panel();
      this.pnlRowLeft = new System.Windows.Forms.Panel();
      this.label6 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.pbxView = new System.Windows.Forms.PictureBox();
      this.cmsProfile = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mitColorTable = new System.Windows.Forms.ToolStripMenuItem();
      this.mitScale = new System.Windows.Forms.ToolStripMenuItem();
      this.pnlColTop.SuspendLayout();
      this.tbcProfile.SuspendLayout();
      this.tbpColorTable.SuspendLayout();
      this.tbpScale.SuspendLayout();
      this.pnlColBottom.SuspendLayout();
      this.pnlRowLeft.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxView)).BeginInit();
      this.cmsProfile.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlColTop
      // 
      this.pnlColTop.Controls.Add(this.label9);
      this.pnlColTop.Controls.Add(this.label8);
      this.pnlColTop.Controls.Add(this.label3);
      this.pnlColTop.Controls.Add(this.label4);
      this.pnlColTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlColTop.Location = new System.Drawing.Point(0, 0);
      this.pnlColTop.Name = "pnlColTop";
      this.pnlColTop.Size = new System.Drawing.Size(515, 20);
      this.pnlColTop.TabIndex = 2;
      // 
      // label9
      // 
      this.label9.Dock = System.Windows.Forms.DockStyle.Right;
      this.label9.Location = new System.Drawing.Point(437, 0);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(57, 20);
      this.label9.TabIndex = 5;
      this.label9.Text = "ColHigh";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label8
      // 
      this.label8.Dock = System.Windows.Forms.DockStyle.Left;
      this.label8.Location = new System.Drawing.Point(57, 0);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(57, 20);
      this.label8.TabIndex = 4;
      this.label8.Text = "ColLow";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label3
      // 
      this.label3.Dock = System.Windows.Forms.DockStyle.Left;
      this.label3.Location = new System.Drawing.Point(0, 0);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(57, 20);
      this.label3.TabIndex = 3;
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label4
      // 
      this.label4.Dock = System.Windows.Forms.DockStyle.Right;
      this.label4.Location = new System.Drawing.Point(494, 0);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(21, 20);
      this.label4.TabIndex = 2;
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // tbcProfile
      // 
      this.tbcProfile.Controls.Add(this.tbpColorTable);
      this.tbcProfile.Controls.Add(this.tbpScale);
      this.tbcProfile.Controls.Add(this.tbpHide);
      this.tbcProfile.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.tbcProfile.Location = new System.Drawing.Point(0, 294);
      this.tbcProfile.Name = "tbcProfile";
      this.tbcProfile.SelectedIndex = 0;
      this.tbcProfile.Size = new System.Drawing.Size(515, 91);
      this.tbcProfile.TabIndex = 7;
      this.tbcProfile.SelectedIndexChanged += new System.EventHandler(this.tbcProfile_SelectedIndexChanged);
      // 
      // tbpColorTable
      // 
      this.tbpColorTable.Controls.Add(this.FUCEditColorTable);
      this.tbpColorTable.Location = new System.Drawing.Point(4, 22);
      this.tbpColorTable.Name = "tbpColorTable";
      this.tbpColorTable.Size = new System.Drawing.Size(507, 65);
      this.tbpColorTable.TabIndex = 3;
      this.tbpColorTable.Text = "ColorTable";
      this.tbpColorTable.UseVisualStyleBackColor = true;
      // 
      // FUCEditColorTable
      // 
      this.FUCEditColorTable.ColorCount = 256;
      this.FUCEditColorTable.ColorGradient = NLColorTable.EColorGradient.LinearIncrease;
      this.FUCEditColorTable.ColorReference = System.Drawing.Color.Black;
      this.FUCEditColorTable.ColorTableKind = NLColorTable.EColorTableKind.Mono;
      this.FUCEditColorTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCEditColorTable.Location = new System.Drawing.Point(0, 0);
      this.FUCEditColorTable.Name = "FUCEditColorTable";
      this.FUCEditColorTable.Size = new System.Drawing.Size(507, 65);
      this.FUCEditColorTable.TabIndex = 0;
      // 
      // tbpScale
      // 
      this.tbpScale.Controls.Add(this.FUCScale);
      this.tbpScale.Location = new System.Drawing.Point(4, 22);
      this.tbpScale.Name = "tbpScale";
      this.tbpScale.Padding = new System.Windows.Forms.Padding(3);
      this.tbpScale.Size = new System.Drawing.Size(507, 65);
      this.tbpScale.TabIndex = 1;
      this.tbpScale.Text = "Scale";
      this.tbpScale.UseVisualStyleBackColor = true;
      // 
      // FUCScale
      // 
      this.FUCScale.BackColor = System.Drawing.Color.Transparent;
      this.FUCScale.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCScale.Location = new System.Drawing.Point(3, 3);
      this.FUCScale.Name = "FUCScale";
      this.FUCScale.ScaleFactor = 4;
      this.FUCScale.Size = new System.Drawing.Size(501, 59);
      this.FUCScale.TabIndex = 0;
      this.FUCScale.VisibleColBottom = false;
      this.FUCScale.VisibleColTop = false;
      this.FUCScale.VisibleRowLeft = false;
      this.FUCScale.VisibleRowRight = false;
      // 
      // tbpHide
      // 
      this.tbpHide.Location = new System.Drawing.Point(4, 22);
      this.tbpHide.Name = "tbpHide";
      this.tbpHide.Padding = new System.Windows.Forms.Padding(3);
      this.tbpHide.Size = new System.Drawing.Size(507, 65);
      this.tbpHide.TabIndex = 2;
      this.tbpHide.Text = "Hide";
      this.tbpHide.UseVisualStyleBackColor = true;
      // 
      // pnlColBottom
      // 
      this.pnlColBottom.Controls.Add(this.label10);
      this.pnlColBottom.Controls.Add(this.label7);
      this.pnlColBottom.Controls.Add(this.label2);
      this.pnlColBottom.Controls.Add(this.label1);
      this.pnlColBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlColBottom.Location = new System.Drawing.Point(0, 274);
      this.pnlColBottom.Name = "pnlColBottom";
      this.pnlColBottom.Size = new System.Drawing.Size(515, 20);
      this.pnlColBottom.TabIndex = 8;
      // 
      // label10
      // 
      this.label10.Dock = System.Windows.Forms.DockStyle.Right;
      this.label10.Location = new System.Drawing.Point(437, 0);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(57, 20);
      this.label10.TabIndex = 3;
      this.label10.Text = "ColHigh";
      this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label7
      // 
      this.label7.Dock = System.Windows.Forms.DockStyle.Left;
      this.label7.Location = new System.Drawing.Point(57, 0);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(57, 20);
      this.label7.TabIndex = 2;
      this.label7.Text = "ColLow";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Left;
      this.label2.Location = new System.Drawing.Point(0, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(57, 20);
      this.label2.TabIndex = 1;
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Right;
      this.label1.Location = new System.Drawing.Point(494, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(21, 20);
      this.label1.TabIndex = 0;
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnlRowRight
      // 
      this.pnlRowRight.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnlRowRight.Location = new System.Drawing.Point(494, 20);
      this.pnlRowRight.Name = "pnlRowRight";
      this.pnlRowRight.Size = new System.Drawing.Size(21, 254);
      this.pnlRowRight.TabIndex = 9;
      // 
      // pnlRowLeft
      // 
      this.pnlRowLeft.Controls.Add(this.label6);
      this.pnlRowLeft.Controls.Add(this.label5);
      this.pnlRowLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlRowLeft.Location = new System.Drawing.Point(0, 20);
      this.pnlRowLeft.Name = "pnlRowLeft";
      this.pnlRowLeft.Size = new System.Drawing.Size(57, 254);
      this.pnlRowLeft.TabIndex = 10;
      // 
      // label6
      // 
      this.label6.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label6.Location = new System.Drawing.Point(0, 234);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(57, 20);
      this.label6.TabIndex = 5;
      this.label6.Text = "RowLow";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Top;
      this.label5.Location = new System.Drawing.Point(0, 0);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(57, 20);
      this.label5.TabIndex = 4;
      this.label5.Text = "RowHigh";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pbxView
      // 
      this.pbxView.BackColor = System.Drawing.SystemColors.Info;
      this.pbxView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxView.Location = new System.Drawing.Point(57, 20);
      this.pbxView.Name = "pbxView";
      this.pbxView.Size = new System.Drawing.Size(437, 254);
      this.pbxView.TabIndex = 11;
      this.pbxView.TabStop = false;
      this.pbxView.Paint += new System.Windows.Forms.PaintEventHandler(this.pbxView_Paint);
      this.pbxView.Resize += new System.EventHandler(this.pbxView_SizeChanged);
      // 
      // cmsProfile
      // 
      this.cmsProfile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitColorTable,
            this.mitScale});
      this.cmsProfile.Name = "cmsProfile";
      this.cmsProfile.Size = new System.Drawing.Size(133, 48);
      // 
      // mitColorTable
      // 
      this.mitColorTable.Name = "mitColorTable";
      this.mitColorTable.Size = new System.Drawing.Size(132, 22);
      this.mitColorTable.Text = "ColorTable";
      this.mitColorTable.Click += new System.EventHandler(this.mitColorTable_Click);
      // 
      // mitScale
      // 
      this.mitScale.Name = "mitScale";
      this.mitScale.Size = new System.Drawing.Size(132, 22);
      this.mitScale.Text = "Scale";
      this.mitScale.Click += new System.EventHandler(this.mitScale_Click);
      // 
      // CUCProfile
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ContextMenuStrip = this.cmsProfile;
      this.Controls.Add(this.pbxView);
      this.Controls.Add(this.pnlRowLeft);
      this.Controls.Add(this.pnlRowRight);
      this.Controls.Add(this.pnlColBottom);
      this.Controls.Add(this.tbcProfile);
      this.Controls.Add(this.pnlColTop);
      this.Name = "CUCProfile";
      this.Size = new System.Drawing.Size(515, 385);
      this.pnlColTop.ResumeLayout(false);
      this.tbcProfile.ResumeLayout(false);
      this.tbpColorTable.ResumeLayout(false);
      this.tbpScale.ResumeLayout(false);
      this.pnlColBottom.ResumeLayout(false);
      this.pnlRowLeft.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pbxView)).EndInit();
      this.cmsProfile.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlColTop;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TabControl tbcProfile;
    private System.Windows.Forms.TabPage tbpScale;
    private System.Windows.Forms.Panel pnlColBottom;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Panel pnlRowRight;
    private System.Windows.Forms.Panel pnlRowLeft;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.PictureBox pbxView;
    private System.Windows.Forms.ContextMenuStrip cmsProfile;
    private System.Windows.Forms.ToolStripMenuItem mitColorTable;
    private System.Windows.Forms.ToolStripMenuItem mitScale;
    private System.Windows.Forms.TabPage tbpHide;
    private System.Windows.Forms.TabPage tbpColorTable;
    private CUCEditColorTable FUCEditColorTable;
    private CUCScale FUCScale;
  }
}
