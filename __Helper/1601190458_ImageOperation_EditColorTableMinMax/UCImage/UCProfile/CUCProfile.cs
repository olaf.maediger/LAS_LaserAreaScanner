﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
//
using Initdata;
using NLColorTable;
//
namespace UCProfile
{
  public partial class CUCProfile : UserControl
  { //
    //---------------------------------------------------------------------
    //  Section - Constant
    //---------------------------------------------------------------------
    //
    public const Int32 INDEX_ROW = 0;
    public const Int32 INDEX_COL = 1;
    //
    public const EColorTableKind INIT_COLORTABLEKIND = EColorTableKind.Mono;
    public const Int32 INIT_COLORCOUNT = 64;
    public Color INIT_COLORMINIMUM = Color.Black;
    public Int32 INIT_INTERVALMINIMUM = 4;
    public Color INIT_COLORMAXIMUM = Color.White;
    public Int32 INIT_INTERVALMAXIMUM = 2;
    public Color INIT_COLORREFERENCE = Color.Black;
    public const EColorGradient INIT_COLORGRADIENT = EColorGradient.LinearDecrease;
    //
    public const Int32 INIT_ROWCOUNT = 120;
    public const Int32 INIT_COLCOUNT = 160;
    public const Double INIT_MINIMUM = 0;
    public const Double INIT_MAXIMUM = 255;
    //
    private const String FORMAT_SECTION = "UCProfile{0}";
    //
    //---------------------------------------------------------------------
    //  Section - Field
    //---------------------------------------------------------------------
    //
    private Double[,] FMatrix;
    private Double FMinimum, FMaximum;
    private CColorTable FColorTable;
    private Int32[,] FColorIndices;
    private Color[,] FColors;
    private Brush[] FBrushes;
    private Int32 FLockCount;
    private Bitmap FImage;
    //
    //---------------------------------------------------------------------
    //  Section - Constructor 
    //---------------------------------------------------------------------
    //
    public CUCProfile()
    {
      InitializeComponent();
      //
      Lock();
      FUCEditColorTable.SetOnEditColorTable(UCEditColorTableOnEditColorTable);
      //
      FUCScale.SetOnScaleFactorChanged(UCScaleOnScaleFactorChanged);
      FUCScale.SetOnVisibleColTopChanged(UCScaleOnVisibleColTopChanged);
      FUCScale.SetOnVisibleColBottomChanged(UCScaleOnVisibleColBottomChanged);
      FUCScale.SetOnVisibleRowLeftChanged(UCScaleOnVisibleRowLeftChanged);
      FUCScale.SetOnVisibleRowRightChanged(UCScaleOnVisibleRowRightChanged);
      //
      SetColorTable(INIT_COLORTABLEKIND, INIT_COLORCOUNT, 
                    INIT_COLORMINIMUM, INIT_INTERVALMINIMUM,
                    INIT_COLORMAXIMUM, INIT_INTERVALMAXIMUM,
                    INIT_COLORGRADIENT, Color.Black);
      Double[,] IM = InitMatrix();
      SetMatrix(IM, INIT_MINIMUM, INIT_MAXIMUM);
      Unlock();
    }
    //
    //---------------------------------------------------------------------
    //  Section - Property
    //---------------------------------------------------------------------
    //
    public void SetColorTable(EColorTableKind colortablekind,
                              Int32 colorcount,
                              Color colorminimum, Int32 intervalminimum,
                              Color colormaximum, Int32 intervalmaximum,
                              EColorGradient colorgradient,
                              Color colorreference)
    {
      FColorTable = CColorTable.CreateColorTable(colortablekind, colorcount,
                                                 colorminimum, intervalminimum,
                                                 colormaximum, intervalmaximum,
                                                 colorgradient, colorreference);
    }

    public void SetColorTable(CColorTable colortable)
    {
      FColorTable = colortable;
    }

    public void SetMatrix(Double[,] matrix, Double minimum, Double maximum)
    {
      FMatrix = matrix;
      FMinimum = minimum;
      FMaximum = maximum;
      BuildColorsBrushesFromMatrixExtrema();
      BuildImage();
    }

    public Bitmap GetImage()
    {
      return FImage;
    }
    //
    //---------------------------------------------------------------------
    //  Section - Init
    //---------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Result &= initdata.OpenSection(String.Format(FORMAT_SECTION, header));
      Lock();
      // Child
      Result &= FUCEditColorTable.LoadInitdata(initdata, "");
      Result &= FUCScale.LoadInitdata(initdata, "");
      //
      Unlock();
      Result &= initdata.CloseSection();
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      Result &= initdata.CreateSection(String.Format(FORMAT_SECTION, header));
      // Child
      Result &= FUCEditColorTable.SaveInitdata(initdata, "");
      Result &= FUCScale.SaveInitdata(initdata, "");
      //
      Result &= initdata.CloseSection();
      return Result;
    }
    //
    //---------------------------------------------------------------------
    //  Section - Helper
    //---------------------------------------------------------------------
    //
    private Double[,] InitMatrix()
    {
      Double[,] M = new Double[INIT_ROWCOUNT, INIT_COLCOUNT];
      for (Int32 RI = 0; RI < INIT_ROWCOUNT; RI++)
      {
        for (Int32 CI = 0; CI < INIT_COLCOUNT; CI++)
        {
          M[RI, CI] = INIT_MINIMUM + CI * (INIT_MAXIMUM - INIT_MINIMUM) / INIT_COLCOUNT;
        }
      }
      return M;
    }

    private void BuildColorsBrushesFromMatrixExtrema()
    {
      if (FMatrix is Double[,])
      {
        Int32 SizeRow = FMatrix.GetLength(INDEX_ROW);
        Int32 SizeCol = FMatrix.GetLength(INDEX_COL);
        Int32 CC = FColorTable.ColorCount;
        // ColorIndices
        // Colors
        Int32 CCD = CC - 1;
        FColorIndices = new Int32[SizeRow, SizeCol];
        FColors = new Color[SizeRow, SizeCol];
        for (Int32 RI = 0; RI < SizeRow; RI++)
        {
          for (Int32 CI = 0; CI < SizeCol; CI++)
          {
            Int32 II = (Int32)(CCD * (FMatrix[RI, CI] - FMinimum) /
                                     (FMaximum - FMinimum));
            FColorIndices[RI, CI] = II;
            FColors[RI, CI] = FColorTable.GetColor(II);
          }
        }
        // Brushes
        if (FBrushes is Brush[])
        { // use old CC 
          Int32 CB = FBrushes.GetLength(0);
          for (Int32 CI = 0; CI < CB; CI++)
          {
            FBrushes[CI].Dispose();
          }
        }
        FBrushes = new Brush[CC];
        for (Int32 CI = 0; CI < CC; CI++)
        {
          Color CB = FColorTable.GetColor(CI);
          FBrushes[CI] = new SolidBrush(CB);
        }
      }
    }

    private void BuildImage()
    {
      if (FColors is Color[,])
      {
        Int32 RowCount = FColors.GetLength(INDEX_ROW);
        Int32 ColCount = FColors.GetLength(INDEX_COL);
        Int32 VW = pbxView.ClientRectangle.Width;
        Int32 VH = pbxView.ClientRectangle.Height;
        if (0 == FUCScale.ScaleFactor)
        { // 0 -> Stretch
          FImage = new Bitmap(VW, VH, PixelFormat.Format24bppRgb);
          Graphics G = Graphics.FromImage(FImage);
          float RW = (float)VW / ColCount;
          float RH = (float)VH / RowCount;
          for (Int32 RI = 0; RI < RowCount; RI++)
          {
            float RT = (float)RI * RH;
            for (Int32 CI = 0; CI < ColCount; CI++)
            {
              float RL = (float)CI * RW;
              Brush B = FBrushes[FColorIndices[RI, CI]];
              G.FillRectangle(B, RL, RT, RW, RH);
            }
          }
          G.Dispose();
        }
        else
        { // 1..10 -> SF * Pixel
          float SF = FUCScale.ScaleFactor;
          FImage = new Bitmap((Int32)(SF * ColCount), 
                              (Int32)(SF * RowCount),
                              PixelFormat.Format24bppRgb);
          Graphics G = Graphics.FromImage(FImage);
          for (Int32 RI = 0; RI < RowCount; RI++)
          {
            float RT = (float)RI * SF;// RH;
            for (Int32 CI = 0; CI < ColCount; CI++)
            {
              float RL = (float)CI * SF;// RW;
              Brush B = FBrushes[FColorIndices[RI, CI]];
              G.FillRectangle(B, RL, RT, SF, SF);
            }
          }
          G.Dispose();
        }
 
      }
    }

    private void SelectColorTable()
    {
      Lock();
      BuildColorsBrushesFromMatrixExtrema();
      BuildImage();
      Unlock();
    }
    //
    //---------------------------------------------------------------------
    //  Section - Callback - EditColorTable
    //---------------------------------------------------------------------
    //
    private void UCEditColorTableOnEditColorTable(CColorTable colortable)
    {
      Lock();
      FColorTable = colortable;
      BuildColorsBrushesFromMatrixExtrema();
      BuildImage();
      Unlock();
    }
    //
    //---------------------------------------------------------------------
    //  Section - Callback - EditColorTable
    //---------------------------------------------------------------------
    //
    private void UCScaleOnScaleFactorChanged(Int32 scaefactor)
    {
      SelectColorTable();
    }

    private void UCScaleOnVisibleColTopChanged(Boolean visible)
    {
      pnlColTop.Visible = visible;
    }

    private void UCScaleOnVisibleColBottomChanged(Boolean visible)
    {
      pnlColBottom.Visible = visible;
    }

    private void UCScaleOnVisibleRowLeftChanged(Boolean visible)
    {
      pnlRowLeft.Visible = visible;
    }

    private void UCScaleOnVisibleRowRightChanged(Boolean visible)
    {
      pnlRowRight.Visible = visible;
    }
    //
    //---------------------------------------------------------------------
    //  Section - Event
    //---------------------------------------------------------------------
    //
    private void pbxView_SizeChanged(object sender, EventArgs e)
    {
      BuildImage();
    }
    
    private void pbxView_Paint(object sender, PaintEventArgs e)
    {
      if (FImage is Image)
      {
        e.Graphics.DrawImage(FImage, 0, 0);
      }
    }

    private void mitColorTable_Click(object sender, EventArgs e)
    {
      tbcProfile.Visible = true;
      tbcProfile.SelectedTab = tbpColorTable;
    }

    private void mitScale_Click(object sender, EventArgs e)
    {
      tbcProfile.Visible = true;
      tbcProfile.SelectedTab = tbpScale;
    }

    private void tbcProfile_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (tbpHide == tbcProfile.SelectedTab)
      {
        tbcProfile.Visible = false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Public Management
    //---------------------------------------------------------------------
    //   
    public void Lock()
    {
      FLockCount++;
    }
    public void Unlock()
    {
      if (0 < FLockCount)
      {
        FLockCount--;
      }
      if (0 == FLockCount)
      {
        pbxView.Refresh();
      }
    }




  }
}
