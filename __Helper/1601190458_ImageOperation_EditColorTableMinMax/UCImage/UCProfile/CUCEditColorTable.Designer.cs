﻿namespace UCProfile
{
  partial class CUCEditColorTable
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxColorTableKind = new System.Windows.Forms.ComboBox();
      this.btnSelectColorTable = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.nudColorCount = new System.Windows.Forms.NumericUpDown();
      this.cbxColorGradient = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.lblColorReference = new System.Windows.Forms.Label();
      this.btnColorReference = new System.Windows.Forms.Button();
      this.btnColorMinimum = new System.Windows.Forms.Button();
      this.lblColorMinimum = new System.Windows.Forms.Label();
      this.nudIntervalMinimum = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.nudIntervalMaximum = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.button1 = new System.Windows.Forms.Button();
      this.lblColorMaximum = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudColorCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIntervalMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIntervalMaximum)).BeginInit();
      this.SuspendLayout();
      // 
      // cbxColorTableKind
      // 
      this.cbxColorTableKind.FormattingEnabled = true;
      this.cbxColorTableKind.Location = new System.Drawing.Point(3, 28);
      this.cbxColorTableKind.Name = "cbxColorTableKind";
      this.cbxColorTableKind.Size = new System.Drawing.Size(148, 21);
      this.cbxColorTableKind.TabIndex = 4;
      this.cbxColorTableKind.SelectedIndexChanged += new System.EventHandler(this.cbxColorTable_SelectedIndexChanged);
      // 
      // btnSelectColorTable
      // 
      this.btnSelectColorTable.Location = new System.Drawing.Point(3, 3);
      this.btnSelectColorTable.Name = "btnSelectColorTable";
      this.btnSelectColorTable.Size = new System.Drawing.Size(148, 24);
      this.btnSelectColorTable.TabIndex = 3;
      this.btnSelectColorTable.Text = "Select ColorTable";
      this.btnSelectColorTable.UseVisualStyleBackColor = true;
      this.btnSelectColorTable.Click += new System.EventHandler(this.btnSelectColorTable_Click);
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(157, 8);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(59, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "ColorCount";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudColorCount
      // 
      this.nudColorCount.Location = new System.Drawing.Point(159, 29);
      this.nudColorCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudColorCount.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.nudColorCount.Name = "nudColorCount";
      this.nudColorCount.Size = new System.Drawing.Size(51, 20);
      this.nudColorCount.TabIndex = 6;
      this.nudColorCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudColorCount.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
      this.nudColorCount.ValueChanged += new System.EventHandler(this.nudColorCount_ValueChanged);
      // 
      // cbxColorGradient
      // 
      this.cbxColorGradient.FormattingEnabled = true;
      this.cbxColorGradient.Location = new System.Drawing.Point(219, 28);
      this.cbxColorGradient.Name = "cbxColorGradient";
      this.cbxColorGradient.Size = new System.Drawing.Size(101, 21);
      this.cbxColorGradient.TabIndex = 7;
      this.cbxColorGradient.SelectedIndexChanged += new System.EventHandler(this.cbxColorGradient_SelectedIndexChanged);
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(222, 8);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(89, 13);
      this.label2.TabIndex = 8;
      this.label2.Text = "ColorGradient";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblColorReference
      // 
      this.lblColorReference.BackColor = System.Drawing.Color.White;
      this.lblColorReference.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblColorReference.Location = new System.Drawing.Point(328, 28);
      this.lblColorReference.Name = "lblColorReference";
      this.lblColorReference.Size = new System.Drawing.Size(87, 20);
      this.lblColorReference.TabIndex = 9;
      this.lblColorReference.Text = "FF FF 00 00";
      this.lblColorReference.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnColorReference
      // 
      this.btnColorReference.Location = new System.Drawing.Point(327, 2);
      this.btnColorReference.Name = "btnColorReference";
      this.btnColorReference.Size = new System.Drawing.Size(89, 23);
      this.btnColorReference.TabIndex = 10;
      this.btnColorReference.Text = "ColorReference";
      this.btnColorReference.UseVisualStyleBackColor = true;
      this.btnColorReference.Click += new System.EventHandler(this.btnColorReference_Click);
      // 
      // btnColorMinimum
      // 
      this.btnColorMinimum.Location = new System.Drawing.Point(422, 2);
      this.btnColorMinimum.Name = "btnColorMinimum";
      this.btnColorMinimum.Size = new System.Drawing.Size(89, 23);
      this.btnColorMinimum.TabIndex = 12;
      this.btnColorMinimum.Text = "ColorMinimum";
      this.btnColorMinimum.UseVisualStyleBackColor = true;
      this.btnColorMinimum.Click += new System.EventHandler(this.btnColorMinimum_Click);
      // 
      // lblColorMinimum
      // 
      this.lblColorMinimum.BackColor = System.Drawing.Color.White;
      this.lblColorMinimum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblColorMinimum.Location = new System.Drawing.Point(423, 28);
      this.lblColorMinimum.Name = "lblColorMinimum";
      this.lblColorMinimum.Size = new System.Drawing.Size(87, 20);
      this.lblColorMinimum.TabIndex = 11;
      this.lblColorMinimum.Text = "FF 00 00 00";
      this.lblColorMinimum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudIntervalMinimum
      // 
      this.nudIntervalMinimum.Location = new System.Drawing.Point(515, 28);
      this.nudIntervalMinimum.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudIntervalMinimum.Name = "nudIntervalMinimum";
      this.nudIntervalMinimum.Size = new System.Drawing.Size(46, 20);
      this.nudIntervalMinimum.TabIndex = 16;
      this.nudIntervalMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudIntervalMinimum.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(518, 7);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(42, 13);
      this.label4.TabIndex = 15;
      this.label4.Text = "Interval";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudIntervalMaximum
      // 
      this.nudIntervalMaximum.Location = new System.Drawing.Point(661, 28);
      this.nudIntervalMaximum.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudIntervalMaximum.Name = "nudIntervalMaximum";
      this.nudIntervalMaximum.Size = new System.Drawing.Size(46, 20);
      this.nudIntervalMaximum.TabIndex = 20;
      this.nudIntervalMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudIntervalMaximum.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(664, 7);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(42, 13);
      this.label3.TabIndex = 19;
      this.label3.Text = "Interval";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(568, 2);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(89, 23);
      this.button1.TabIndex = 18;
      this.button1.Text = "ColorMaximum";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // lblColorMaximum
      // 
      this.lblColorMaximum.BackColor = System.Drawing.Color.White;
      this.lblColorMaximum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblColorMaximum.Location = new System.Drawing.Point(569, 28);
      this.lblColorMaximum.Name = "lblColorMaximum";
      this.lblColorMaximum.Size = new System.Drawing.Size(87, 20);
      this.lblColorMaximum.TabIndex = 17;
      this.lblColorMaximum.Text = "FF FF FF FF";
      this.lblColorMaximum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCEditColorTable
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.nudIntervalMaximum);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.lblColorMaximum);
      this.Controls.Add(this.nudIntervalMinimum);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.btnColorMinimum);
      this.Controls.Add(this.lblColorMinimum);
      this.Controls.Add(this.btnColorReference);
      this.Controls.Add(this.lblColorReference);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.cbxColorGradient);
      this.Controls.Add(this.nudColorCount);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.cbxColorTableKind);
      this.Controls.Add(this.btnSelectColorTable);
      this.Name = "CUCEditColorTable";
      this.Size = new System.Drawing.Size(717, 52);
      ((System.ComponentModel.ISupportInitialize)(this.nudColorCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIntervalMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIntervalMaximum)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ComboBox cbxColorTableKind;
    private System.Windows.Forms.Button btnSelectColorTable;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudColorCount;
    private System.Windows.Forms.ComboBox cbxColorGradient;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lblColorReference;
    private System.Windows.Forms.Button btnColorReference;
    private System.Windows.Forms.Button btnColorMinimum;
    private System.Windows.Forms.Label lblColorMinimum;
    private System.Windows.Forms.NumericUpDown nudIntervalMinimum;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.NumericUpDown nudIntervalMaximum;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Label lblColorMaximum;

  }
}
