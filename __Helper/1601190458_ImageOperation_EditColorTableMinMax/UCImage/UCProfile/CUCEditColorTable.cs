﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLColorTable;
//
namespace UCProfile
{
  public delegate void DOnEditColorTable(CColorTable colortable);
  //
  public partial class CUCEditColorTable : UserControl
  {
    //
    //-------------------------------------------------------------------
    //  Section - Constant
    //-------------------------------------------------------------------
    //
    private const String FORMAT_SECTION = "UCEditColorTable{0}";
    //
    private const String NAME_COLORTABLEKIND = "ColorTableKind";
    private const EColorTableKind INIT_COLORTABLEKIND = EColorTableKind.Mono;
    private const String NAME_COLORCOUNT = "ColorCount";
    private const Int32 INIT_COLORCOUNT = 256;
    private const String NAME_COLORGRADIENT = "ColorGradient";
    private const EColorGradient INIT_COLORGRADIENT = EColorGradient.LinearIncrease;
    private const String NAME_COLORREFERENCE = "ColorReference";
    private Color INIT_COLORREFERENCE = Color.Black;
    private const String NAME_COLORMINIMUM = "ColorMinimum";
    private Color INIT_COLORMINIMUM = Color.Black;
    private const String NAME_INTERVALMINIMUM = "IntervalMinimum";
    private const Int32 INIT_INTERVALMINIMUM = 2;
    private const String NAME_COLORMAXIMUM = "ColorMaximum";
    private Color INIT_COLORMAXIMUM = Color.White;
    private const String NAME_INTERVALMAXIMUM = "IntervalMaximum";
    private const Int32 INIT_INTERVALMAXIMUM = 2;
    //
    //---------------------------------------------------------------------
    //  Section - Field
    //---------------------------------------------------------------------
    //
    private DOnEditColorTable FOnEditColorTable;
    //
    //-------------------------------------------------------------------
    //  Section - Constructor
    //-------------------------------------------------------------------
    //
    public CUCEditColorTable()
    {
      InitializeComponent();
      //
      InitControls();
    }
    //
    //-------------------------------------------------------------------
    //  Section - Property
    //-------------------------------------------------------------------
    //
    public void SetOnEditColorTable(DOnEditColorTable value)
    {
      FOnEditColorTable = value;
    }

    private EColorTableKind GetColorTableKind()
    {
      String CTT = cbxColorTableKind.Text;
      return CColorTable.TextColorTableKind(CTT);
    }
    private void SetColorTableKind(EColorTableKind value)
    {
      cbxColorTableKind.Text = CColorTable.ColorTableKindText(value);
    }
    public EColorTableKind ColorTableKind
    {
      get { return GetColorTableKind(); }
      set { SetColorTableKind(value); }
    }

    public Int32 ColorCount
    {
      get { return (Int32)nudColorCount.Value; }
      set { nudColorCount.Value = (Decimal)value; }
    }

    private EColorGradient GetColorGradient()
    {
      String CGT = cbxColorGradient.Text;
      return CColorTable.TextColorGradient(CGT);
    }
    private void SetColorGradient(EColorGradient value)
    {
      cbxColorGradient.Text = CColorTable.ColorGradientText(value);
    }
    public EColorGradient ColorGradient
    {
      get { return GetColorGradient(); }
      set { SetColorGradient(value); }
    }

    private Color GetColorReference()
    {
      return lblColorReference.BackColor;
    }
    private void SetColorReference(Color value)
    {
      lblColorReference.BackColor = value;
      lblColorReference.ForeColor = CColorTable.BuildComplementColor(value);
      lblColorReference.Text = String.Format("{0:X2} {1:X2} {2:X2} {3:X2}", 
                                             value.A, value.R, value.G, value.B);
    }
    public Color ColorReference
    {
      get { return GetColorReference(); }
      set { SetColorReference(value); }
    }

    private void SetColorMinimum(Color value)
    {
      lblColorMinimum.BackColor = value;
      lblColorMinimum.ForeColor = CColorTable.BuildComplementColor(value);
      lblColorMinimum.Text = String.Format("{0:X2} {1:X2} {2:X2} {3:X2}",
                                             value.A, value.R, value.G, value.B);
    }
    public Color ColorMinimum
    {
      get { return lblColorMinimum.BackColor; }
      set { SetColorMinimum(value); }
    }

    public Int32 IntervalMinimum
    {
      get { return (Int32)nudIntervalMinimum.Value; }
      set { nudIntervalMinimum.Value = (Decimal)value; }
    }

    private void SetColorMaximum(Color value)
    {
      lblColorMaximum.BackColor = value;
      lblColorMaximum.ForeColor = CColorTable.BuildComplementColor(value);
      lblColorMaximum.Text = String.Format("{0:X2} {1:X2} {2:X2} {3:X2}",
                                             value.A, value.R, value.G, value.B);
    }
    public Color ColorMaximum
    {
      get { return lblColorMaximum.BackColor; }
      set { SetColorMaximum(value); }
    }

    public Int32 IntervalMaximum
    {
      get { return (Int32)nudIntervalMaximum.Value; }
      set { nudIntervalMaximum.Value = (Decimal)value; }
    }

    //
    //---------------------------------------------------------------------
    //  Section - Init
    //---------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Result &= initdata.OpenSection(String.Format(FORMAT_SECTION, header));
      //
      String SValue =  CColorTable.ColorTableKindText(INIT_COLORTABLEKIND);
      Result &= initdata.ReadEnumeration(NAME_COLORTABLEKIND, out SValue, SValue);
      ColorTableKind =  CColorTable.TextColorTableKind(SValue);
      //
      Int32 IValue = INIT_COLORCOUNT;
      Result &= initdata.ReadInt32(NAME_COLORCOUNT, out IValue, IValue);
      ColorCount = IValue;
      //
      SValue = CColorTable.ColorGradientText(INIT_COLORGRADIENT);
      Result &= initdata.ReadEnumeration(NAME_COLORGRADIENT, out SValue, SValue);
      ColorGradient = CColorTable.TextColorGradient(SValue);
      //
      Color CValue = INIT_COLORREFERENCE;
      Result &= initdata.ReadColor(NAME_COLORREFERENCE, out CValue, CValue);
      ColorReference = CValue;
      //
      CValue = INIT_COLORMINIMUM;
      Result &= initdata.ReadColor(NAME_COLORMINIMUM, out CValue, CValue);
      ColorMinimum = CValue;
      //
      IValue = INIT_INTERVALMINIMUM;
      Result &= initdata.ReadInt32(NAME_INTERVALMINIMUM, out IValue, IValue);
      IntervalMinimum = IValue;
      //
      CValue = INIT_COLORMAXIMUM;
      Result &= initdata.ReadColor(NAME_COLORMAXIMUM, out CValue, CValue);
      ColorMaximum = CValue;
      //
      IValue = INIT_INTERVALMAXIMUM;
      Result &= initdata.ReadInt32(NAME_INTERVALMAXIMUM, out IValue, IValue);
      IntervalMaximum = IValue;
      //
      Result &= initdata.CloseSection();
      //
      btnSelectColorTable_Click(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      Result &= initdata.CreateSection(String.Format(FORMAT_SECTION, header));
      //
      Result &= initdata.WriteEnumeration(NAME_COLORTABLEKIND, 
                                          CColorTable.ColorTableKindText(ColorTableKind));
      Result &= initdata.WriteInt32(NAME_COLORCOUNT, ColorCount);
      Result &= initdata.WriteEnumeration(NAME_COLORGRADIENT, 
                                          CColorTable.ColorGradientText(ColorGradient));
      Result &= initdata.WriteColor(NAME_COLORREFERENCE, ColorReference);
      Result &= initdata.WriteColor(NAME_COLORMINIMUM, ColorMinimum);
      Result &= initdata.WriteInt32(NAME_INTERVALMINIMUM, IntervalMinimum);
      Result &= initdata.WriteColor(NAME_COLORMAXIMUM, ColorMaximum);
      Result &= initdata.WriteInt32(NAME_INTERVALMAXIMUM, IntervalMaximum);
      //
      Result &= initdata.CloseSection();
      return Result;
    }
    //
    //-------------------------------------------------------------------
    //  Section - Helper
    //-------------------------------------------------------------------
    //
    private void InitControls()
    {
      String[] CTL = CColorTable.ColorTableKindTextAll();
      cbxColorTableKind.Items.Clear();
      for (Int32 CTI = 0; CTI < CTL.Length; CTI++)
      {
        cbxColorTableKind.Items.Add(CTL[CTI]);
      }
      ColorTableKind = INIT_COLORTABLEKIND;
      //
      ColorCount = INIT_COLORCOUNT;
      //
      CTL = CColorTable.ColorGradientTextAll();
      cbxColorGradient.Items.Clear();
      for (Int32 CTI = 0; CTI < CTL.Length; CTI++)
      {
        cbxColorGradient.Items.Add(CTL[CTI]);
      }
      if (0 < cbxColorGradient.Items.Count)
      {
        cbxColorGradient.SelectedIndex = 0;
      }
      //
      ColorReference = INIT_COLORREFERENCE;
    }
    //
    //-------------------------------------------------------------------
    //  Section - Event
    //-------------------------------------------------------------------
    //
    private void btnSelectColorTable_Click(object sender, EventArgs e)
    {
      if (FOnEditColorTable is DOnEditColorTable)
      {
        CColorTable ColorTable = 
          CColorTable.CreateColorTable(ColorTableKind, ColorCount,
                                       ColorMinimum, IntervalMinimum,
                                       ColorMaximum, IntervalMaximum,
                                       ColorGradient, ColorReference);

        FOnEditColorTable(ColorTable);
      }
    }

    private void cbxColorTable_SelectedIndexChanged(object sender, EventArgs e)
    {
      // NC
    }

    private void nudColorCount_ValueChanged(object sender, EventArgs e)
    {
      // NC
    }

    private void cbxColorGradient_SelectedIndexChanged(object sender, EventArgs e)
    {
      // NC ColorGradient = CColorTable.TextColorGradient(cbxColorGradient.Text);
    }

    private void btnColorReference_Click(object sender, EventArgs e)
    {
      ColorDialog CD = new ColorDialog();
      CD.Color = ColorReference;
      if (DialogResult.OK == CD.ShowDialog())
      {
        ColorReference = CD.Color;
      }
    }

    private void btnColorMinimum_Click(object sender, EventArgs e)
    {
      ColorDialog CD = new ColorDialog();
      CD.Color = ColorMinimum;
      if (DialogResult.OK == CD.ShowDialog())
      {
        ColorMinimum = CD.Color;
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
      ColorDialog CD = new ColorDialog();
      CD.Color = ColorMaximum;
      if (DialogResult.OK == CD.ShowDialog())
      {
        ColorMaximum = CD.Color;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Section - Action
    //-------------------------------------------------------------------
    //

  }
}
