﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageOperation
{
  public static class CUtility
  {

    public static Double[,] BuildMatrixLinear(Int32 countrow, Int32 countcol)
    {
      Double[,] Result = new Double[countrow, countcol];
      for (Int32 RI = 0; RI < countrow; RI++)
      {
        for (Int32 CI = 0; CI < countcol; CI++)
        {
          Result[RI, CI] = (Double)CI / (Double)countcol + (Double)RI / (Double)countrow;
        }
      }
      return Result;
    }

    public static Double[,] BuildMatrixParboloid(Int32 countrow, Int32 countcol)
    {
      Double[,] Result = new Double[countrow, countcol];
      Double RC = countrow / 2.0;
      for (Int32 RI = 0; RI < countrow; RI++)
      {
        Double CC = countcol / 2.0;
        for (Int32 CI = 0; CI < countcol; CI++)
        {
          Result[RI, CI] = -1.0 / RC / CC * ((CI - CC) * (CI - CC) + (RI - RC) * (RI - RC));
        }
      }
      return Result;
    }

    public static Double FindMatrixMinimum(Double[,] matrix)
    {
      Double Result = 1E20;
      Double CountRow = matrix.GetLength(0);
      Double CountCol = matrix.GetLength(1);
      for (Int32 RI = 0; RI < CountRow; RI++)
      {
        for (Int32 CI = 0; CI < CountCol; CI++)
        {
          Double DValue = matrix[RI, CI];
          if (DValue < Result)
          {
            Result = DValue;
          }
        }
      }
      return Result;
    }


    public static Double FindMatrixMaximum(Double[,] matrix)
    {
      Double Result = -1E20;
      Double CountRow = matrix.GetLength(0);
      Double CountCol = matrix.GetLength(1);
      for (Int32 RI = 0; RI < CountRow; RI++)
      {
        for (Int32 CI = 0; CI < CountCol; CI++)
        {
          Double DValue = matrix[RI, CI];
          if (Result < DValue)
          {
            Result = DValue;
          }
        }
      }
      return Result;
    }

  }
}
