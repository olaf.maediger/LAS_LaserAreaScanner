﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
using NLFilter2D;
using NLColorTable;
//
namespace ImageOperation
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    private const Int32 INIT_ROWCOUNT = 120;
    private const Int32 INIT_COLCOUNT = 160;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    // NC private CRandomDouble FRandomDouble;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      Int32 IValue = INIT_AUTOINTERVAL;
      Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      Result &= FUCProfile.LoadInitdata(initdata, "");
      //Result &= FUCImageQuad.LoadInitdata(initdata, "");
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      Result &= FUCProfile.SaveInitdata(initdata, "");
      //Result &= FUCImageQuad.SaveInitdata(initdata, "");
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback 
    //------------------------------------------------------------------------
    //    

    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          FStartupState = 4;
          return true;
        case 1:
          Bitmap BTL = new Bitmap("BTL.bmp");
          FUCImageQuad.SetImageTopLeft(BTL);
          return true;
        case 2:
          Bitmap BTR = new Bitmap("BTR.bmp");
          FUCImageQuad.SetImageTopRight(BTR);
          return true;
        case 3:
          Bitmap BBL = new Bitmap("BBL.bmp");
          FUCImageQuad.SetImageBottomLeft(BBL);
          return true;
        case 4:
          Bitmap BBR = new Bitmap("BBR.bmp");
          FUCImageQuad.SetImageBottomRight(BBR);
          return true;
        case 5:
          FUCProfile.Lock();
          //Double[,] Matrix = CUtility.BuildMatrixLinear(INIT_ROWCOUNT, INIT_COLCOUNT);
          Double[,] Matrix = CUtility.BuildMatrixParboloid(INIT_ROWCOUNT, INIT_COLCOUNT);
          Double Minimum = CUtility.FindMatrixMinimum(Matrix);
          Double Maximum = CUtility.FindMatrixMaximum(Matrix);
          //FUCProfile.ScaleFactor = 6;
          //FUCProfile.SetColorTable(EColorTableKind.Mono,
          //                         256,
          //                         EColorGradient.LinearIncrease,
          //                         Color.Black);
          FUCProfile.SetMatrix(Matrix, Minimum, Maximum);
          FUCProfile.Unlock();
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
         // Close();
          return true;
        default:
          // ?! cbxAutomate.Checked = false;
          return false;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxAutomate.Checked)
      {
        FStartupState = 0;
        tmrStartup.Interval = (Int32)nudAutoInterval.Value;
      }
      tmrStartup.Enabled = cbxAutomate.Checked;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //     

    //
    //------------------------------------------------------------------------
    //  Section - Action
    //------------------------------------------------------------------------
    //


  }
}

