﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace NLColorTable
{
  public class CRgb : CColorTable
  {
    public CRgb(Int32 colorcount, 
                Color colorminimum, Int32 intervalminimum,
                Color colormaximum, Int32 intervalmaximum,
                EColorGradient colorgradient, 
                Color colorreference)
      : base(colorcount)
    {
      Int32 CCD = ColorCount - 1 - intervalmaximum;
      Byte R = colorreference.R;
      Byte G = colorreference.G;
      Byte B = colorreference.B;
      switch (colorgradient)
      {
        case EColorGradient.LinearDecrease:
          for (Int32 CI = 0; CI <= intervalminimum; CI++)
          {
            FColors[CI] = colormaximum;
          }
          for (Int32 CI = 1 + intervalminimum; CI <= CCD; CI++)
          {
            float DDI = CCD - CI;
            Byte CF = (Byte)(DDI * 255f / (float)CCD);
            Byte CBR = (Byte)(DDI * (float)R / (float)CCD);
            Byte CBG = (Byte)(DDI * (float)G / (float)CCD);
            Byte CBB = (Byte)(DDI * (float)B / (float)CCD);
            switch (CI % 3)
            {
              case 0:
                FColors[CI] = Color.FromArgb(CF, CBG, CBB);
                break;
              case 1:
                FColors[CI] = Color.FromArgb(CBR, CF, CBB);
                break;
              case 2:
                FColors[CI] = Color.FromArgb(CBR, CBG, CF);
                break;
            }
          }
          for (Int32 CI = 1 + CCD; CI <= intervalmaximum + CCD; CI++)
          {
            FColors[CI] = colorminimum;
          }
          break;
        default: // Increase
          for (Int32 CI = 0; CI <= intervalminimum; CI++)
          {
            FColors[CI] = colorminimum;
          }
          for (Int32 CI = 1 + intervalminimum; CI <= CCD; CI++)
          {
            Byte CF = (Byte)((float)CI * 255f / (float)CCD);
            Byte CBR = (Byte)((float)CI * (float)R / (float)CCD);
            Byte CBG = (Byte)((float)CI * (float)G / (float)CCD);
            Byte CBB = (Byte)((float)CI * (float)B / (float)CCD);
            switch (CI % 3)
            {
              case 0:
                FColors[CI] = Color.FromArgb(CF, CBG, CBB);
                break;
              case 1:
                FColors[CI] = Color.FromArgb(CBR, CF, CBB);
                break;
              case 2:
                FColors[CI] = Color.FromArgb(CBR, CBG, CF);
                break;
            }
          }
          for (Int32 CI = 1 + CCD; CI <= intervalmaximum + CCD; CI++)
          {
            FColors[CI] = colormaximum;
          }
          break;
      }
    }

  }
}
