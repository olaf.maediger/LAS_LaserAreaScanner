﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace NLColorTable
{
  //
  //-----------------------------------------------------------------
  //  Section - 
  //-----------------------------------------------------------------
  //
  public enum EColorTableKind
  {
    Mono = 0,
    Rgb = 1,
    RgbRgb = 2
  };

  public enum EColorGradient
  {
    LinearIncrease,
    LinearDecrease
  };

  public class CColorTable
  {
    //
    //-----------------------------------------------------------------
    //  Section - 
    //-----------------------------------------------------------------
    //
    protected Color COLOR_ZERO = Color.Black;
    private const Int32 INIT_COLORCOUNT = 256;
    //
    //-----------------------------------------------------------------
    //  Section - 
    //-----------------------------------------------------------------
    //


    protected Color[] FColors;

    //
    //-----------------------------------------------------------------
    //  Section - 
    //-----------------------------------------------------------------
    //
    public CColorTable(Int32 colorcount)
    {
      Int32 CC = colorcount;
      FColors = new Color[CC];
    }
    //
    //-----------------------------------------------------------------
    //  Section - 
    //-----------------------------------------------------------------
    //

    public Int32 ColorCount
    {
      get { return FColors.Length; }
    }

    public Color GetColor(Int32 index)
    {
      if ((0 <= index) && (index < FColors.Length))
      {
        return FColors[index];
      }
      return COLOR_ZERO;
    }
    public Byte GetRed(Int32 index)
    {
      if ((0 <= index) && (index < FColors.Length))
      {
        return FColors[index].R;
      }
      return COLOR_ZERO.R;
    }
    public Byte GetGreen(Int32 index)
    {
      if ((0 <= index) && (index < FColors.Length))
      {
        return FColors[index].G;
      }
      return COLOR_ZERO.G;
    }
    public Byte GetBlue(Int32 index)
    {
      if ((0 <= index) && (index < FColors.Length))
      {
        return FColors[index].B;
      }
      return COLOR_ZERO.B;
    }

    public static String ColorTableKindText(EColorTableKind value)
    {
      String Result = "";
      switch (value)
      {
        case EColorTableKind.Mono:
          Result = "Mono";
          break;
        case EColorTableKind.Rgb:
          Result = "Rgb";
          break;
        case EColorTableKind.RgbRgb:
          Result = "RgbRgb";
          break;
      }
      return Result;
    }

    public static EColorTableKind TextColorTableKind(String value)
    {
      if (value == "Mono")
      {
        return EColorTableKind.Mono;
      }
      if (value == "Rgb")
      {
        return EColorTableKind.Rgb;
      }
      if (value == "RgbRgb")
      {
        return EColorTableKind.RgbRgb;
      }
      return EColorTableKind.Mono;
    }

    public static String[] ColorTableKindTextAll()
    {
      String[] Result = new String[3];
      Result[0] = "Mono";
      Result[1] = "Rgb";
      Result[2] = "RgbRgb";
      return Result;
    }
    //
    //-----------------------------------------------------------------
    //  Section - 
    //-----------------------------------------------------------------
    //
    public static String ColorGradientText(EColorGradient value)
    {
      String Result = "";
      switch (value)
      {
        case EColorGradient.LinearIncrease:
          Result = "LinearIncrease";
          break;
        case EColorGradient.LinearDecrease:
          Result = "LinearDecrease";
          break;
      }
      return Result;
    }

    public static EColorGradient TextColorGradient(String value)
    {
      if (value == "LinearIncrease")
      {
        return EColorGradient.LinearIncrease;
      }
      if (value == "LinearDecrease")
      {
        return EColorGradient.LinearDecrease;
      }
      return EColorGradient.LinearIncrease;
    }

    public static String[] ColorGradientTextAll()
    {
      String[] Result = new String[2];
      Result[0] = "LinearIncrease";
      Result[1] = "LinearDecrease";
      return Result;
    }

    public static Color BuildComplementColor(Color value)
    {
      Byte R = 0x00;
      if (value.R < 0x80)
      {
        R = 0xFF; ;
      }
      Byte G = 0x00;
      if (value.G < 0x80)
      {
        G = 0xFF; ;
      }
      Byte B = 0x00;
      if (value.B < 0x80)
      {
        B = 0xFF; ;
      }
      return Color.FromArgb(0xFF, R, G, B);
    }
    //
    //-----------------------------------------------------------------
    //  Section - Static Construction
    //-----------------------------------------------------------------
    //  
    public static CColorTable CreateColorTable(EColorTableKind colortablekind,
                                               Int32 colorcount,
                                               Color colorminimum, Int32 intervalminimum,
                                               Color colormaximum, Int32 intervalmaximum,
                                               EColorGradient colorgradient,
                                               Color colorreference)
    {
      switch (colortablekind)
      {
        case EColorTableKind.Mono:
          return new CMono(colorcount, colorgradient, colorreference);
        case EColorTableKind.Rgb:
          return new CRgb(colorcount,
                          colorminimum, intervalminimum,
                          colormaximum, intervalmaximum,
                          colorgradient, colorreference);
        case EColorTableKind.RgbRgb:
          return new CRgbRgb(colorcount, colorgradient, colorreference);
      }
      return null;
    }

  }

}
