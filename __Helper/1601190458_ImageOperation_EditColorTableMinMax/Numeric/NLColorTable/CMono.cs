﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace NLColorTable
{
  public class CMono : CColorTable
  {
    public CMono(Int32 colorcount,
                 EColorGradient colorgradient, 
                 Color colorreference)
      : base(colorcount)
    {
      Int32 CCD = ColorCount - 1;
      float CBR = (float)colorreference.R / (float)CCD;
      float CBG = (float)colorreference.G / (float)CCD;
      float CBB = (float)colorreference.B / (float)CCD;
      float CFR = (float)(0xFF - colorreference.R) / (float)CCD;
      float CFG = (float)(0xFF - colorreference.G) / (float)CCD;
      float CFB = (float)(0xFF - colorreference.B) / (float)CCD;
      switch (colorgradient)
      {
        case EColorGradient.LinearDecrease:
          for (Int32 CI = 0; CI <= CCD; CI++)
          {
            Byte R = (Byte)(0.5f + (CCD - CI) * CFR + CI * CBR);
            Byte G = (Byte)(0.5f + (CCD - CI) * CFG + CI * CBG);
            Byte B = (Byte)(0.5f + (CCD - CI) * CFG + CI * CBB);
            FColors[CI] = Color.FromArgb(0xFF, R, G, B);
          }
          break;
        default:
         for (Int32 CI = 0; CI <= CCD; CI++)
          {
            Byte R = (Byte)(0.5f + CI * CFR + (CCD - CI) * CBR);
            Byte G = (Byte)(0.5f + CI * CFG + (CCD - CI) * CBG);
            Byte B = (Byte)(0.5f + CI * CFB + (CCD - CI) * CBB);
            FColors[CI] = Color.FromArgb(0xFF, R, G, B);
          }
          break;
      }
     }

  }
}
