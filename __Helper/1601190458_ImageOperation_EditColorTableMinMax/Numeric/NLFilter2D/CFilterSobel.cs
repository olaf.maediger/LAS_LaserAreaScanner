﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLFilter2D
{
  public class CFilterSobel : CFilter2D
  {
    public const Int32 ROWCOUNT = 3;
    public const Int32 COLCOUNT = 3;
    //
    public const Double GX00 = 1;
    public const Double GX01 = 0;
    public const Double GX02 = -1;
    public const Double GX10 = 2;
    public const Double GX11 = 0;
    public const Double GX12 = -2;
    public const Double GX20 = GX00;
    public const Double GX21 = GX01;
    public const Double GX22 = GX02;
    //
    public const Double GY00 = 1;
    public const Double GY01 = 2;
    public const Double GY02 = 1;
    public const Double GY10 = 0;
    public const Double GY11 = 0;
    public const Double GY12 = 0;
    public const Double GY20 = -GY00;
    public const Double GY21 = -GY01;
    public const Double GY22 = -GY02;

    public CFilterSobel()
      : base(ROWCOUNT, COLCOUNT)
    {
      FGX[0, 0] = GX00;
      FGX[0, 1] = GX01;
      FGX[0, 2] = GX02;
      FGX[1, 0] = GX10;
      FGX[1, 1] = GX11;
      FGX[1, 2] = GX12;
      FGX[2, 0] = GX20;
      FGX[2, 1] = GX21;
      FGX[2, 2] = GX22;
      FGX[0, 0] = GX00;
      //
      FGY[0, 0] = GY00;
      FGY[0, 1] = GY01;
      FGY[0, 2] = GY02;
      FGY[1, 0] = GY10;
      FGY[1, 1] = GY11;
      FGY[1, 2] = GY12;
      FGY[2, 0] = GY20;
      FGY[2, 1] = GY21;
      FGY[2, 2] = GY22;
    }

  }
}
