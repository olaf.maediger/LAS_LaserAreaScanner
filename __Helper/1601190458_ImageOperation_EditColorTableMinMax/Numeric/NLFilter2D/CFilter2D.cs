﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLFilter2D
{
  

  public class CFilter2D
  {
    protected Double[,] FGX;
    protected Double[,] FGY;

    public CFilter2D(Int32 rowcount, Int32 colcount)
    {
      Int32 RC = Math.Max(1, rowcount);
      Int32 CC = Math.Max(1, colcount);
      FGX = new Double[RC, CC];
      FGY = new Double[RC, CC];
    }

    public Double[,] OperateX(Double[,] matrix)
    {
      try
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Double[,] Result = new Double[RC, CC];
        //
        Int32 RCD = RC - 1;
        Int32 CCD = CC - 1;
        for (Int32 RI = 1; RI < RCD; RI++)
        {
          for (Int32 CI = 1; CI < CCD; CI++)
          {
            Result[RI, CI] = FGX[2, 2] * matrix[RI - 1, CI - 1] +
                             FGX[2, 1] * matrix[RI, CI - 1] +
                             FGX[2, 0] * matrix[RI + 1, CI - 1] +
                             FGX[1, 2] * matrix[RI - 1, CI] +
                             FGX[1, 1] * matrix[RI, CI] +
                             FGX[1, 0] * matrix[RI + 1, CI] +
                             FGX[0, 2] * matrix[RI - 1, CI + 1] +
                             FGX[0, 1] * matrix[RI, CI + 1] +
                             FGX[0, 0] * matrix[RI + 1, CI + 1];
          }
        }
        //
        return Result;
      }
      catch (Exception)
      {
        return null;
      }
    }

    public Double[,] OperateY(Double[,] matrix)
    {
      try
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Double[,] Result = new Double[RC, CC];
        //
        Int32 RCD = RC - 1;
        Int32 CCD = CC - 1;
        for (Int32 RI = 1; RI < RCD; RI++)
        {
          for (Int32 CI = 1; CI < CCD; CI++)
          {
            Result[RI, CI] = FGY[2, 2] * matrix[RI - 1, CI - 1] +
                             FGY[2, 1] * matrix[RI, CI - 1] +
                             FGY[2, 0] * matrix[RI + 1, CI - 1] +
                             FGY[1, 2] * matrix[RI - 1, CI] +
                             FGY[1, 1] * matrix[RI, CI] +
                             FGY[1, 0] * matrix[RI + 1, CI] +
                             FGY[0, 2] * matrix[RI - 1, CI + 1] +
                             FGY[0, 1] * matrix[RI, CI + 1] +
                             FGY[0, 0] * matrix[RI + 1, CI + 1];
          }
        }
        //
        return Result;
      }
      catch (Exception)
      {
        return null;
      }
    }

    public Double[,] OperateR(Double[,] matrix)
    {
      try
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Double[,] Result = new Double[RC, CC];
        //
        Double[,] MX = OperateX(matrix);
        Double[,] MY = OperateY(matrix);
        //
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 1; CI < CC; CI++)
          {
            Result[RI, CI] = Math.Sqrt(MX[RI, CI] * MX[RI, CI] + MY[RI, CI] * MY[RI, CI]);
          }
        }
        //
        return Result;
      }
      catch (Exception)
      {
        return null;
      }
    }

    public Double[,] OperateA(Double[,] matrix)
    {
      try
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Double[,] Result = new Double[RC, CC];
        //
        Double[,] MX = OperateX(matrix);
        Double[,] MY = OperateY(matrix);
        //
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 1; CI < CC; CI++)
          {
            if (0 == MX[RI, CI])
            {
              if (0 == MY[RI, CI])
              {
                Result[RI, CI] = 0.0;
              }
              else
                if (0 < MY[RI, CI])
                {
                  Result[RI, CI] = Math.PI / 2;
                }
                else
                {
                  Result[RI, CI] = -Math.PI / 2;
                }
            }
            else
            {
              Result[RI, CI] = Math.Atan2(MY[RI, CI], MX[RI, CI]);
            }
          }
        }
        //
        return Result;
      }
      catch (Exception)
      {
        return null;
      }
    }

  }
}
