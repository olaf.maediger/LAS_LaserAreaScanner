﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using UCNotifier;
using Task;
//
namespace ElevatorGroup
{
  //
  //------------------------------------------------------------------------
  //  Segment - Definition
  //------------------------------------------------------------------------
  //
  public delegate CController DOnGetController();
  //
  public class CController : CElevatorBase
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "Main";
    private const Int32 DELAY_MINIMUM = 1000; // [ms]
    private const Int32 DELAY_MAXIMUM = 1000; // [ms]
    private const Int32 DELAY_SCALEFACTOR = 1;  // / SF;
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CControllerState FState;
    private DOnGetUserList FOnGetUserList;
    private DOnGetLiftList FOnGetLiftList;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CController()
      : base(INIT_HEADER)
    {
      FRandom = new CRandom(DELAY_MINIMUM, DELAY_MAXIMUM, DELAY_SCALEFACTOR, 0);
      FState = new CControllerState();
    }

    public CController(String name)
      : base(name)
    {
      FRandom = new CRandom(DELAY_MINIMUM, DELAY_MAXIMUM, DELAY_SCALEFACTOR, 0);
      FState = new CControllerState();
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public override void SetNotifier(CNotifier value)
    {
      base.SetNotifier(value);
      FState.SetNotifier(value);
    }

    public void SetOnGetUserList(DOnGetUserList value)
    {
      FOnGetUserList = value;
    }

    public void SetOnGetLiftList(DOnGetLiftList value)
    {
      FOnGetLiftList = value;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    private Boolean HandleStateHalted()
    {
      return true;
    }

    // Idle : no user has any request
    private Boolean HandleStateIdle()
    {
      if (FOnGetUserList is DOnGetUserList)
      {
        CUserList UserList = FOnGetUserList();
        foreach (CUser User in UserList)
        {
          if (EUserState.Request == User.GetState())
          {
            //CLift LS = User.LiftSelected;
            //if (LS is CLift)
            //{ 
            //  Int32 PT = User.PositionTarget;
              // minimal one user is on request -> idle -> busy
            FState.SetState(EControllerState.Busy);
            //}
          }
        }
        return true;
      }
      return false;
    }

    private Boolean HandleStateBusy()
    {
      if (FOnGetUserList is DOnGetUserList)
      {
        CUserList UserList = FOnGetUserList();
        if (UserList is CUserList)
        {
          CUserList ULRTS = UserList.GetUserRequestsTimeSorted();
          // debug ULRTS.Info();
          Int32 UC = ULRTS.Count;
          if (0 < UC)
          {
            for (Int32 UI = 0; UI < UC; UI++)
            {
              CUser UR = ULRTS[UI];
              if (EUserState.Request == UR.GetState())
              {
                UR.SetStateWaiting();
                CLift ULS = UR.LiftSelected;
                if (ULS is CLift)
                {
                  ULS.SetPositionTarget(UR.PositionTarget);
                }
              }
            }
          }
          return true;
        }
      }
      return false;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Callback - Task
    //------------------------------------------------------------------------
    //
    private void TaskOnStart(RTaskData data)
    {
      Mail("TaskOnStart");
      FRandom.SetInterval(DELAY_MINIMUM, DELAY_MAXIMUM, DELAY_SCALEFACTOR);
    }
    private void TaskOnBusy(ref RTaskData data)
    {
      do
      {
        Mail("TaskOnBusy");
        Int32 DT = FRandom.NextInteger();
        if (0 < DT) Thread.Sleep(DT);
        //
        switch (FState.GetState())
        {
          case EControllerState.Halted:
            if (!HandleStateHalted())
            {
              Error("On State Halted");
              FTask.Abort();
            }
            break;
          case EControllerState.Idle:
            if (!HandleStateIdle())
            {
              Error("On State Idle");
              FTask.Abort();
            }
            break;
          case EControllerState.Busy:
            if (!HandleStateBusy())
            {
              Error("On State Busy");
              FTask.Abort();
            }
            break;
        }
      }
      while (FTask.IsActive());
    }
    private void TaskOnEnd(RTaskData data)
    {
      Mail("TaskOnEnd");
    }
    private void TaskOnAbort(RTaskData data)
    {
      Mail("TaskOnAbort");
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Callback - UserList
    //------------------------------------------------------------------------
    //
    private void UserListOnEventUserRequest(CUser user)
    {
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public Boolean Start()
    {
      if (!(FTask is CTask))
      {
        FState.Reset();
        FTask = new CTask(FHeader, TaskOnStart, TaskOnBusy, TaskOnEnd, TaskOnAbort);
        FTask.Start();
        return true;
      }
      return false;
    }

    public Boolean Stop()
    {
      if (FTask is CTask)
      {
        FTask.Abort();
        FTask = null;
        return true;
      }
      return false;
    }

    public CLift SelectLiftRandom()
    {
      if (FOnGetLiftList is DOnGetLiftList)
      {
        CLiftList LiftList = FOnGetLiftList();
        if (LiftList is CLiftList)
        {
          Int32 LC = LiftList.Count;
          Int32 LIS = FRandom.NextInteger(0, LC);
          CLift Lift = LiftList[LIS];
          return Lift;
        }
      }
      return null;
    }

    public Int32 SelectLiftPositionActualRandom(CLift lift)
    {
      if (lift is CLift)
      {
        return lift.SelectPositionActualRandom();
      }
      return 0;
    }


    public Int32 SelectLiftPositionTargetRandom(CLift lift, Int32 positionexcluded)
    {
      if (lift is CLift)
      {
        return lift.SelectPositionTargetRandom(positionexcluded);
      }
      return 0;
    }

    public Boolean RequestLiftForUser(CUser user)
    {
      if (user is CUser)
      {
        //Int32 PositionActual = user.PositionActual;
        //Int32 PositionTarget = user.PositionTarget;
        //CLift LiftSelected = user.LiftSelected;
        //if (LiftSelected is CLift)
        //{
          if (FOnGetLiftList is DOnGetLiftList)
          {
            CLiftList LiftList = FOnGetLiftList();
            if (LiftList is CLiftList)
            {
              return LiftList.RequestLift(user);//LiftSelected, user);
            }
          }
      //  }
      }
      return false;
    }

  }
}
