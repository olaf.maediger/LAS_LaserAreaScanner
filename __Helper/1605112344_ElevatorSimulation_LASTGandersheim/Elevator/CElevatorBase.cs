﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Task;
using UCNotifier;
//
namespace ElevatorGroup
{
  //
  //------------------------------------------------------------------------
  //  Segment - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnSetHeader(String header);
  //
  public class CElevatorBase
  {
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    protected Guid FID;
    protected String FHeader;
    protected DOnSetHeader FOnSetHeader;
    protected CTask FTask;
    protected CRandom FRandom;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CElevatorBase(String header)
    {
      FHeader = header;
      FID = Guid.NewGuid();
      FTask = null;
      FRandom = null;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public virtual void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    protected virtual void SetHeader(String value)
    {
      FHeader = value;
      if (FOnSetHeader is DOnSetHeader)
      {
        FOnSetHeader(FHeader);
      }
    }
    public String Header
    {
      get { return FHeader; }
      set { SetHeader(value); }
    }

    public void SetOnSetHeader(DOnSetHeader value)
    {
      FOnSetHeader = value;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    protected void Error(String error)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("ERROR: {0}[{1}][{2}]: {3}!",
                                      this.GetType().Name, FHeader, FID.ToString(), error));
      }
    }

    protected void Mail(String mail)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}][{2}]: {3}", 
                        this.GetType().Name, FHeader, FID.ToString(), mail));
      }
    }

    //
    //------------------------------------------------------------------------
    //  Segment - 
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Segment - 
    //------------------------------------------------------------------------
    //
  }
}
