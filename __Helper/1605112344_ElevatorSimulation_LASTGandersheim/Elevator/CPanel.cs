﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace ElevatorGroup
{ //
  //------------------------------------------------------------------------
  //  Segment - Type
  //------------------------------------------------------------------------
  //
  public delegate void DOnTargetIndexChanged(Int32 targetindex);
  public delegate void DOnTargetCountChanged(Int32 targetcount);
  //
  public class CPanel : CElevatorBase
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "Panel";
    private const Int32 INIT_TARGETINDEX = 0;
    private const Int32 INIT_TARGETCOUNT = CPosition.INIT_POSITIONCOUNT;
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private Int32 FTargetIndex;
    private DOnTargetIndexChanged FOnTargetIndexChanged;
    private Int32 FTargetCount;
    private DOnTargetCountChanged FOnTargetCountChanged;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CPanel()
      : base(INIT_HEADER)
    {
      FTargetIndex = INIT_TARGETINDEX;
      FTargetCount = INIT_TARGETCOUNT;
      // no Task
    }

    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetOnTargetIndexChanged(DOnTargetIndexChanged value)
    {
      FOnTargetIndexChanged = value;
    }

    public void SetOnTargetCountChanged(DOnTargetCountChanged value)
    {
      FOnTargetCountChanged = value;
    }

    private void SetTargetIndex(Int32 value)
    {
      if ((0 <= value) && (value < FTargetCount))
      {
        FTargetIndex = value;
        if (FOnTargetIndexChanged is DOnTargetIndexChanged)
        {
          FOnTargetIndexChanged(FTargetCount);
        }
      }
    }
    public Int32 TargetIndex
    {
      get { return FTargetIndex; }
      set { SetTargetIndex(value); }
    }

    private void SetTargetCount(Int32 value)
    {
      Int32 TC = Math.Max(value, INIT_TARGETCOUNT);
      FTargetCount = TC;
      if (FOnTargetCountChanged is DOnTargetCountChanged)
      {
        FOnTargetCountChanged(FTargetCount);
      }
    }
    public Int32 TargetCount
    {
      get { return FTargetCount; }
      set { SetTargetCount(value); }
    }

    //
    //------------------------------------------------------------------------
    //  Segment - 
    //------------------------------------------------------------------------
    //

  }
}
