﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace ElevatorGroup
{
  public delegate CLiftList DOnGetLiftList();

  public class CLiftList : List<CLift>
  {
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      foreach (CLift Lift in this)
      {
        Lift.SetNotifier(FNotifier);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public new Boolean Add(CLift lift)
    {
      lift.SetNotifier(FNotifier);
      base.Add(lift);
      return (0 < Count);
    }

    public void Start()
    {
      foreach (CLift Lift in this)
      {
        Lift.Start();
      }
    }
    public void Stop()
    {
      foreach (CLift Lift in this)
      {
        Lift.Stop();
      }
    }

    //public Boolean RequestLift(CLift lift, CUser user, Int32 positiontarget, Int32 positionactual)
    //{ // add position request to lift
    //  return lift.Request(user, positiontarget, positionactual);
    //}
    public Boolean RequestLift(CUser user)
    { // add position request to lift
      return user.LiftSelected.Request(user);
    }

  }
}
