﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElevatorGroup
{
  public class CPositionSelection
  {
    private const Int32 INDEX_MINIMUM = 0;
    private const Int32 INIT_POSITIONCOUNT = 2;

    private Int32 FPositionMinimum, FPositionMaximum;

    public CPositionSelection(Int32 positioncount)//Int32 targetminimum, Int32 targetmaximum)
    {
      //FTargetMinimum = targetminimum;
      //FTargetMaximum = targetmaximum;
      Int32 IH = Math.Max(INIT_POSITIONCOUNT, positioncount) - 1;
      FPositionMinimum = INDEX_MINIMUM;
      FPositionMaximum = IH;
    }

    public Int32 SelectRandom()
    {
      Random R = new Random((Int32)DateTime.Now.Ticks);
      Int32 N = R.Next(FPositionMinimum, FPositionMaximum);
      return N;
    }

  }
}
