﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace ElevatorGroup
{
  public partial class CPanelList : List<CPanel>
  {
    private const Int32 INIT_USERPANELCOUNT = 2;

    public CPanelList()
    {
      CreateUserPanelList(INIT_USERPANELCOUNT);
    }

    private void CreateUserPanelList(Int32 count)
    {
      Int32 Count = Math.Max(INIT_USERPANELCOUNT, count);
      for (Int32 PI = 0; PI < Count; PI++)
      {
        CPanel UP = new CPanel();
        UP.Header = String.Format("UserPanel[{0}]" , PI);
        PI++;
      }
    }

    
  }
}
