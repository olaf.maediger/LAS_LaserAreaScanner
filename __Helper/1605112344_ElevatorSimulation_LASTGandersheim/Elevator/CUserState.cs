﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElevatorGroup
{ //
  //------------------------------------------------------------------------
  //  Segment - Global Type
  //------------------------------------------------------------------------
  //
  public enum EUserState
  {
    Reset = 0,
    Idle = 1,
    Request = 2,
    Waiting = 3,
    Moving = 4,
    Reached = 5
  }
  //
  //------------------------------------------------------------------------
  //  Segment - Definition
  //------------------------------------------------------------------------
  //
  public class CUserState : CElevatorBase
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "UserState";
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //

    private EUserState FState;

    public CUserState()
      : base(INIT_HEADER)
    {
      FState = EUserState.Reset;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public EUserState GetState()
    {
      return FState;
    }
    public void SetState(EUserState state)
    {
      String Text = String.Format("-> New State[{0}]",
                                  CUserState.UserStateText(state));
      Mail(Text);
      FState = state;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    public static String UserStateText(EUserState state)
    {
      switch (state)
      {
        case EUserState.Reset:
          return "Reset";
        case EUserState.Idle:
          return "Idle";
        case EUserState.Request:
          return "Request";
        case EUserState.Waiting:
          return "Waiting";
        case EUserState.Moving:
          return "Moving";
        case EUserState.Reached:
          return "Reached";
      }
      return "Unknown";
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public void Reset()
    {
      Mail("Reset...");
      FState = EUserState.Reset;
      SetState(EUserState.Reset);
    }



  }
}
