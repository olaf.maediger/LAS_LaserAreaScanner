﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using Task;
using UCNotifier;
//
namespace ElevatorGroup
{ //
  //------------------------------------------------------------------------
  //  Segment - Type
  //------------------------------------------------------------------------
  //
  public delegate void DOnPositionCountChanged(Int32 positioncount);
  public delegate void DOnPositionSourceChanged(Int32 positionsource);
  public delegate void DOnPositionActualChanged(Int32 positionactual);
  public delegate void DOnPositionTargetChanged(Int32 positiontarget);
  //
  public class CLift : CElevatorBase
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "Lift";
    private const Int32 INIT_POSITIONCOUNT = 2;
    private const Int32 INIT_POSITIONSOURCE = 0;
    private const Int32 INIT_POSITIONACTUAL = 0;
    private const Int32 INIT_POSITIONTARGET = 0;
    private const Int32 DELAY_MINIMUM = 1000; // [ms]
    private const Int32 DELAY_MAXIMUM = 1000; // [ms]
    private const Int32 DELAY_SCALEFACTOR = 1;  // / SF;
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CLiftState FState;
    private Int32 FPositionCount;
    private DOnPositionCountChanged FOnPositionCountChanged;
    private Int32 FPositionSource;
    private DOnPositionSourceChanged FOnPositionSourceChanged;
    private Int32 FPositionActual;
    private DOnPositionActualChanged FOnPositionActualChanged;
    private Int32 FPositionTarget;
    private DOnPositionTargetChanged FOnPositionTargetChanged;
    private CPositionSelection FPositionSelection;
    private CRequestList FRequestList;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CLift(Int32 positioncount)
      : base(INIT_HEADER)
    {
      FPositionCount = Math.Max(INIT_POSITIONCOUNT, positioncount);
      FPositionSource = INIT_POSITIONSOURCE;
      FPositionActual = INIT_POSITIONACTUAL;
      FPositionTarget = INIT_POSITIONTARGET;
      FPositionSelection = new CPositionSelection(FPositionCount);
      FState = new CLiftState();
      FRandom = new CRandom(DELAY_MINIMUM, DELAY_MAXIMUM, DELAY_SCALEFACTOR, 0);
      FRequestList = new CRequestList();
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public new void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      FState.SetNotifier(notifier);
    }

    public void SetOnPositionCountChanged(DOnPositionCountChanged value)
    {
      FOnPositionCountChanged = value;
    }
    public void SetOnPositionSourceChanged(DOnPositionSourceChanged value)
    {
      FOnPositionSourceChanged = value;
    }
    public void SetOnPositionActualChanged(DOnPositionActualChanged value)
    {
      FOnPositionActualChanged = value;
    }
    public void SetOnPositionTargetChanged(DOnPositionTargetChanged value)
    {
      FOnPositionTargetChanged = value;
    }

    private void SetPositionCount(Int32 value)
    {
      FPositionCount = value;
      FPositionSelection = new CPositionSelection(FPositionCount);
      if (FOnPositionCountChanged is DOnPositionCountChanged)
      {
        FOnPositionCountChanged(FPositionCount);
      }
    }
    public Int32 PositionCount
    {
      get { return FPositionCount; }
      set { SetPositionCount(value); }
    }

    private void SetPositionSource(Int32 value)
    {
      Int32 PI = Math.Max(value, 0);
      PI = Math.Min(value, FPositionCount - 1);
      FPositionSource = PI;
      if (FOnPositionSourceChanged is DOnPositionSourceChanged)
      {
        FOnPositionSourceChanged(FPositionSource);
      }
    }
    public Int32 PositionSource
    {
      get { return FPositionSource; }
      set { SetPositionSource(value); }
    }

    private void SetPositionActual(Int32 value)
    {
      Int32 PI = Math.Max(value, 0);
      PI = Math.Min(value, FPositionCount - 1);
      FPositionActual = PI;
      if (FOnPositionActualChanged is DOnPositionActualChanged)
      {
        FOnPositionActualChanged(FPositionActual);
      }
    }
    public Int32 PositionActual
    {
      get { return FPositionActual; }
      set { SetPositionActual(value); }
    }

    public void SetPositionTarget(Int32 value)
    {
      Int32 PI = Math.Max(value, 0);
      PI = Math.Min(value, FPositionCount - 1);
      FPositionTarget = PI;
      if (FOnPositionTargetChanged is DOnPositionTargetChanged)
      {
        FOnPositionTargetChanged(FPositionTarget);
      }
    }
    public Int32 PositionTarget
    {
      get { return FPositionTarget; }
      // set { SetPositionTarget(value); }
    }

    public void SetOnLiftStateChanged(DOnLiftStateChanged value)
    {
      FState.SetOnLiftStateChanged(value);
    }

    public void SetState(ELiftState value)
    {
      FState.SetState(value);
    }
    public ELiftState GetState()
    {
      return FState.GetState();
    }

    public Boolean DirectionUp
    {
      get { return (FPositionSource <= FPositionTarget); }
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    private Boolean HandleStateError()
    {
      Error("HandleState: Error");
      FTask.Abort();
      return false;
    }
 
    private Boolean HandleStateIdle()
    {
      do
      { // debug         Mail("HandleState: Idle");
        if (PositionActual != PositionTarget) // == PA !!!!
        {
          SetPositionSource(PositionActual);
          FState.SetState(ELiftState.Busy);
        }
        else
        {
          Int32 DT = FRandom.NextInteger();
          // debug Mail("DelayIdle>>>" + DT);
          if (0 < DT) Thread.Sleep(DT);
        }
      }
      while (ELiftState.Idle == FState.GetState());
      return true;
    }
    private Boolean HandleStateBusy()
    {
      do
      { // debug         Mail("HandleState: Busy");
        if (PositionActual == PositionTarget)
        {
          FState.SetState(ELiftState.Halted);
        }
        else
        {
          Int32 DT = FRandom.NextInteger();
          // debug Mail("DelayBusy>>>" + DT);
          if (0 < DT) Thread.Sleep(DT);
          if (PositionActual < PositionTarget)
          {
            SetPositionActual(+1 + PositionActual);
          }
          else
          {
            SetPositionActual(-1 + PositionActual);
          }
        }
      }
      while (ELiftState.Busy == FState.GetState());
      return true;
    }
    private Boolean HandleStateHalted()
    {
      do
      { // debug         Mail("HandleState: Halted");
        Int32 DT = FRandom.NextInteger();
        // debug Mail("DelayHalted>>>" + DT);
        if (0 < DT) Thread.Sleep(DT);
        FState.SetState(ELiftState.Idle);
      }
      while (ELiftState.Halted == FState.GetState());
      return true;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Callback - Task
    //------------------------------------------------------------------------
    //
    private void TaskOnStart(RTaskData data)
    {
      Mail("TaskOnStart");
      FRandom.SetInterval(DELAY_MINIMUM, DELAY_MAXIMUM, DELAY_SCALEFACTOR);
    }
    private void TaskOnBusy(ref RTaskData data)
    {
      do
      {
        Mail("TaskOnBusy");
        Int32 DT = FRandom.NextInteger();
        if (0 < DT) Thread.Sleep(DT);
        //
        switch (FState.GetState())
        {
          case ELiftState.Error:
            HandleStateError();
            break;
          //case ELiftState.Reset:
          //  HandleStateReset();
          //  break;
          case ELiftState.Idle:
            HandleStateIdle();
            break;
          case ELiftState.Busy:
            HandleStateBusy();
            break;
          case ELiftState.Halted:
            HandleStateHalted();
            break;
        }
      }
      while (FTask.IsActive());
    }
    private void TaskOnEnd(RTaskData data)
    {
      Mail("TaskOnEnd");
    }
    private void TaskOnAbort(RTaskData data)
    {
      Mail("TaskOnAbort");
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public Int32 SelectPositionActualRandom()
    {
      return FPositionSelection.SelectRandom();
    }

    public Int32 SelectPositionTargetRandom()
    {
      return FPositionSelection.SelectRandom();
    }

    public Int32 SelectPositionTargetRandom(Int32 positionexcluded)
    {
      Int32 TR = FPositionSelection.SelectRandom();
      if (positionexcluded != TR)
      {
        return TR;
      } // positionrandom == positionexcluded
      if (0 < positionexcluded)
      {
        return positionexcluded - 1;
      }
      return positionexcluded + 1;
    }

    public Boolean Start()
    {
      if (!(FTask is CTask))
      {
        FTask = new CTask(FHeader, TaskOnStart, TaskOnBusy, TaskOnEnd, TaskOnAbort);
        //???????????????????????????????FState.SetState(ELiftState.Reset);
        FTask.Start();
        return true;
      }
      return false;
    }

    public Boolean Stop()
    {
      if (FTask is CTask)
      {
        FTask.Abort();
        FState.SetState(ELiftState.Halted);
        FTask = null;
        return true;
      }
      return false;
    }

    //public Boolean Request(CUser user, Int32 positionactual, Int32 positiontarget)
    //{ // add position request to lift
    //  if (positiontarget != positionactual)
    //  {
    //    String Text = String.Format("Lift[{0}] requested for User[{1}] from PA[{2}] to PT[{3}]",
    //                                this.Header, user.Header, this.PositionActual, this.FPositionTarget);
    //    Mail(Text);
    //    //
    //    CRequest Request = new CRequest(user, positionactual, positiontarget);
    //    FRequestList.Add(Request);
    //    return true;
    //  }
    //  return false;
    //}

    public Boolean Request(CUser user)
    { // add position request to lift
      if (user.PositionActual != user.PositionTarget)
      {
        CRequest Request = new CRequest(FNotifier, user);
        Request.Info();
        FRequestList.Add(Request);
        return true;
      }
      return false;
    }

    //    String Text = String.Format("Lift[{0}] reached User[{1}] Position[{2}]",
            //                            FLiftSelected.Header, this.Header, this.PositionActual);
            //Mail(Text);


  }
}
