﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Task;
using UCNotifier;
using ElevatorGroup;
//
namespace ElevatorGroup
{
  public delegate CUserList DOnGetUserList();

  public class CUserList : List<CUser>
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "UserList";
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private Guid FID;
    private DOnGetController FOnGetController;
    private DOnGetLiftList FOnGetLiftList;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUserList()
    {
      FID = Guid.NewGuid();
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnGetController(DOnGetController value)
    {
      FOnGetController = value;
      foreach (CUser User in this)
      {
        User.SetOnGetController(FOnGetController);
      }
    }

    public void SetOnGetLiftList(DOnGetLiftList value)
    {
      FOnGetLiftList = value;
    }

    public CUserList GetAllUserRequests()
    {
      CUserList Result = new CUserList();
      foreach (CUser User in this)
      {
        if (EUserState.Request == User.GetState())
        {
          Result.AddUser(User);
        }
      }
      return Result;
    }

    public CUserList GetLiftUserRequests(CLift lift)
    {
      CUserList Result = new CUserList();
      foreach (CUser User in this)
      {
        if (EUserState.Request == User.GetState())
        {
          if (User.LiftSelected is CLift)
          {
            Result.AddUser(User);
          }
        }
      }
      return Result;
    }

    public CUserList GetLiftUserRequestsTimeSorted(CLift lift)
    {
      CUserList Result = new CUserList();
      foreach (CUser User in this)
      {
        if (EUserState.Request == User.GetState())
        {
          if (User.LiftSelected is CLift)
          {
            Result.AddUserTimeSorted(User);
          }
        }
      }
      return Result;
    }

    public CUserList GetUserRequestsTimeSorted()
    {
      CUserList Result = new CUserList();
      foreach (CUser User in this)
      {
        if (EUserState.Request == User.GetState())
        {
          if (User.LiftSelected is CLift)
          {
            Result.AddUserTimeSorted(User);
          }
        }
      }
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    private void Mail(String mail)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}{1}: {2}", INIT_HEADER, FID.ToString(), mail));
      }
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Callback - User
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public Boolean AddUser(String name)
    {
      CUser User = new CUser(name, this.Count);
      User.SetNotifier(FNotifier);
      // NC??? User.SetOnGetLiftList(FOnGetLiftList);
      User.SetOnGetController(FOnGetController);
      this.Add(User);
      return (0 < this.Count);
    }

    public Boolean AddUser(CUser user)
    {
      user.SetNotifier(FNotifier);
      // NC??? user.SetOnGetLiftList(FOnGetLiftList);
      user.SetOnGetController(FOnGetController);
      user.AddTimeOffset(this.Count);
      this.Add(user);
      return (0 < this.Count);
    }

    public Boolean ClearUser()
    {
      Stop();
      this.Clear();
      return (0 == this.Count);
    }

    public Boolean AddUserTimeSorted(CUser user)
    { // timelow[0] .. timehigh[UC - 1]
      Int32 UC = this.Count;
      for (Int32 UI = 0; UI < UC; UI++)
      {
        CUser User = this[UI];
        if (user.TimeSelected < User.TimeSelected)
        {
          this.Insert(UI, user);
          return true;
        }
      }
      this.Add(user);
      return true;
    }

    public Boolean Start()
    {
      foreach (CUser User in this)
      {
        User.Start();
      }
      return true;
    }

    public Boolean Stop()
    {
      foreach (CUser User in this)
      {
        User.Stop();
      }
      return true;
    }

    public void Info()
    {
      String Text = String.Format("{0} C[{1}]:", INIT_HEADER, Count);
      foreach (CUser User in this)
      {
        User.Info();
      }
    }

  }
}
