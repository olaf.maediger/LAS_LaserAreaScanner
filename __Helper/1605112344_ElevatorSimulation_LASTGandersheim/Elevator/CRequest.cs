﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace ElevatorGroup
{
  public class CRequest
  {
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    protected Guid FID;
    private CUser FUser;
    private Int32 FPositionActual;
    private Int32 FPositionTarget;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CRequest(CNotifier notifier, CUser user)
    {
      FNotifier = notifier;
      FID = Guid.NewGuid();
      FUser = user;
      FPositionActual = user.PositionActual;
      FPositionTarget = user.PositionTarget;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    //public virtual void SetNotifier(CNotifier value)
    //{
    //  FNotifier = value;
    //}

    //protected virtual void SetHeader(String value)
    //{
    //  FHeader = value;
    //}
    //public String Header
    //{
    //  get { return FHeader; }
    //  set { SetHeader(value); }
    //}
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    protected void Error(String error)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("ERROR: {0}[{1}]: {2}!",
                                      this.GetType().Name, FID.ToString(), error));
      }
    }

    protected void Mail(String mail)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]: {2}",
                        this.GetType().Name, FID.ToString(), mail));
      }
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public void Info()
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]: from User[{2}]: PA[{3}] -> PT[{4}]",
                        this.GetType().Name, FID.ToString(), 
                        FUser.Header, FUser.PositionActual, FUser.PositionTarget));
      }
    }


  }
}
