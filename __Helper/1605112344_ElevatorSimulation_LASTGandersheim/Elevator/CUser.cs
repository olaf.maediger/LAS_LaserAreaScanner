﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using UCNotifier;
using Task;
//
namespace ElevatorGroup
{
  public delegate void DOnEventUserRequest(CUser user);
  //
  public class CUser : CElevatorBase
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "User";
    private const Int32 DELAY_MINIMUM = 001000; // [ms]
    private const Int32 DELAY_MAXIMUM = 001000; // [ms]
    private const Int32 DELAY_SCALEFACTOR = 1;//1000;
    private const Int32 INIT_POSITIONSOURCE = 0;
    private const Int32 INIT_POSITIONTARGET = 0;
    private const Int32 INIT_POSITIONACTUAL = 0;
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CUserState FState;
    private Int32 FTicksOffset; // important for correct Random-Management!!!
    private DateTime FTimeCreation;
    private DateTime FTimeSelected;
    private Int32 FPositionSource;
    private Int32 FPositionTarget;
    private Int32 FPositionActual;
    private CLift FLiftSelected; // only Reference!
    // NC ???? private DOnGetLiftList FOnGetLiftList;
    private DOnGetController FOnGetController;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUser(Int32 timeoffset)
      : base(INIT_HEADER)
    {
      FState = new CUserState();
      FTicksOffset = timeoffset;
      FTimeCreation = DateTime.Now.AddTicks(FTicksOffset);
      FRandom = new CRandom(DELAY_MINIMUM, DELAY_MAXIMUM, DELAY_SCALEFACTOR, FTicksOffset);
      FPositionSource = INIT_POSITIONSOURCE;
      FPositionTarget = INIT_POSITIONTARGET;
      FPositionActual = INIT_POSITIONACTUAL;
    }

    public CUser(String header, Int32 timeoffset)
      : base(header)
    {
      FState = new CUserState();
      FHeader = header;
      FTicksOffset = timeoffset;
      FTimeCreation = DateTime.Now.AddTicks(FTicksOffset);
      FRandom = new CRandom(DELAY_MINIMUM, DELAY_MAXIMUM, DELAY_SCALEFACTOR, FTicksOffset);
      FPositionSource = INIT_POSITIONSOURCE;
      FPositionTarget = INIT_POSITIONTARGET;
      FPositionActual = INIT_POSITIONACTUAL;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public new void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      FState.SetNotifier(notifier);
    }

    public void SetOnGetController(DOnGetController value)
    {
      FOnGetController = value;
    }
 
    public EUserState GetState()
    {
      return FState.GetState();
    }
    public void SetStateWaiting()
    {
      FState.SetState(EUserState.Waiting);
    }

    private void SetPositionSource(Int32 value)
    {
      FPositionSource = value;
      String Text = String.Format("SetPositionSource[{0}]", value);
      Mail(Text);
    }
    public Int32 PositionSource
    {
      get { return FPositionSource; }
      set { SetPositionSource(value); }
    }

    private void SetPositionTarget(Int32 value)
    {
      FPositionTarget = value;
      String Text = String.Format("SetPositionTarget[{0}]", FPositionTarget);
      Mail(Text);
    }
    public Int32 PositionTarget
    {
      get { return FPositionTarget; }
    }

    private void SetPositionActual(Int32 value)
    {
      FPositionActual = value;
      String Text = String.Format("SetPositionActual[{0}]", value);
      Mail(Text);
    }
    public Int32 PositionActual
    {
      get { return FPositionActual; }
      set { SetPositionActual(value); }
    }

    private void SetLiftSelected(CLift liftselected)
    {
      FLiftSelected = liftselected;
      if (FLiftSelected is CLift)
      {
        Mail(String.Format("SetLiftSelected[{0}]", FLiftSelected.Header));
      }
      else
      {
        Mail("NoLiftSelected");
      }
    }

    public CLift LiftSelected
    {
      get { return FLiftSelected; }
    }

    public DateTime TimeSelected
    {
      get { return FTimeSelected; }
    }

    public void AddTimeOffset(Int32 timeoffset)
    {
      FTimeCreation.AddTicks(timeoffset);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    //private Boolean SelectPositionTarget(Int32 positiontarget)
    //{ // is this really a new target (unequal to PA)?
    //  if (positiontarget != PositionTarget)
    //  {
    //    FPositionTarget = positiontarget; // -> IsOnRequest <- Active!!!!
    //    FTimeSelected = DateTime.Now.AddTicks(FTicksOffset);
    //    String Text = String.Format("SelectLift[{0}]: PositionTarget[{1}] <- PositionSource[{2}]",
    //                                FLiftSelected.Header, FPositionTarget, FPositionSource);
    //    Mail(Text);
    //    return true;
    //  }
    //  return false;
    //}

    //
    //------------------------------------------------------------------------
    //  Segment - Callback
    //------------------------------------------------------------------------
    //
    private void TaskOnStart(RTaskData data)
    {
      Mail("TaskOnStart");
      if (FOnGetController is DOnGetController)
      {
        CController Controller = FOnGetController();
        if (Controller is CController)
        { // first: select random lift
          SetLiftSelected(Controller.SelectLiftRandom());
          // third: select position actual for this user
          SetPositionActual(Controller.SelectLiftPositionActualRandom(LiftSelected));
        }
      }
    }

    private void HandleTaskBusyUserReset()
    {
      Mail("HandleTaskBusyUserReset");
      FState.SetState(EUserState.Idle);
    }

    private void HandleTaskBusyUserIdle()
    {
      Mail("HandleTaskBusyUserIdle");
      if (FOnGetController is DOnGetController)
      {
        CController Controller = FOnGetController();
        if (Controller is CController)
        { // first: lift is previously selected!
          // second: select target on that lift
          SetPositionTarget(Controller.SelectLiftPositionTargetRandom(LiftSelected, PositionActual));
          if (Controller.RequestLiftForUser(this))
          {
            FState.SetState(EUserState.Request);
          }
        }
      }
    }

    private void HandleTaskBusyUserRequest()
    { // User waits for the arrival of the requested lift
      if (FOnGetController is DOnGetController)
      {
        CController Controller = FOnGetController();
        if (Controller is CController)
        {
          if (FLiftSelected.PositionActual == this.FPositionActual)
          {
            String Text = String.Format("User[{0}] entries Lift[{1}] at Position[{2}]",
                                        this.Header, FLiftSelected.Header, this.PositionActual);
            Mail(Text);
          }
        }
      }
    }

    private void HandleTaskBusyUserWaiting()
    {
    }

    private void HandleTaskBusyUserMoving()
    {
    }

    private void HandleTaskBusyUserReached()
    {
    }




    private void TaskOnBusy(ref RTaskData data)
    {
      Int32 DT = FRandom.NextInteger();
      // debug Console.WriteLine(">>>" + DT);
      if (0 < DT) Thread.Sleep(DT);
      do
      {
        switch (FState.GetState())
        {
          case EUserState.Reset:
            HandleTaskBusyUserReset();
            break;
          case EUserState.Idle:
            HandleTaskBusyUserIdle();
            break;
          case EUserState.Request:
            HandleTaskBusyUserRequest();
            break;
          case EUserState.Waiting:
            HandleTaskBusyUserWaiting();
            break;
          case EUserState.Moving:
            HandleTaskBusyUserMoving();
            break;
          case EUserState.Reached:
            HandleTaskBusyUserReached();
            break;
        }
        DT = FRandom.NextInteger();
        // debug Console.WriteLine(DT);
        if (0 < DT) Thread.Sleep(DT);
      }
      while (FTask.IsActive());
    }

    private void TaskOnEnd(RTaskData data) 
    {
      Mail("TaskOnEnd");
    }

    private void TaskOnAbort(RTaskData data)
    {
      Mail("TaskOnAbort");
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public Boolean Start()
    {
      if (!(FTask is CTask))
      {
        FTask = new CTask(FHeader, TaskOnStart, TaskOnBusy, TaskOnEnd, TaskOnAbort);
        FTask.Start();
        return true;
      }
      return false;
    }

    public Boolean Stop()
    {
      if (FTask is CTask)
      {
        FTask.Abort();
        FTask = null;
        return true;
      }
      return false;
    }

    public void Info()
    {
      String Text = String.Format("{0} TO[{1}] TC[{2}] TS[{3}]", 
                                  Header, FTicksOffset, 
                                  FTimeCreation.TimeOfDay, FTimeSelected.TimeOfDay);
      Mail(Text);
    }

  }
}
