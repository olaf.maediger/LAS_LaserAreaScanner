﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace ElevatorGroup
{ //
  //------------------------------------------------------------------------
  //  Segment - Global Type
  //------------------------------------------------------------------------
  //
  public enum EControllerState
  {
    Halted = 0,
    Idle = 1,
    Busy = 2
  };
  //
  //------------------------------------------------------------------------
  //  Segment - Definition
  //------------------------------------------------------------------------
  //
  public class CControllerState : CElevatorBase
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "Main";
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private EControllerState FState;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CControllerState()
      : base(INIT_HEADER)
    {
      FState = EControllerState.Halted;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public EControllerState GetState()
    {
      return FState;
    }
    public void SetState(EControllerState state)
    {
      String Text = String.Format("-> New State[{0}]",
                                  CControllerState.ControllerStateText(state));
      Mail(Text);
      FState = state;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    public static String ControllerStateText(EControllerState state)
    {
      switch (state)
      {
        case EControllerState.Halted:
          return "Halted";
        case EControllerState.Idle:
          return "Idle";
        case EControllerState.Busy:
          return "Busy";
      }
      return "Unknown";
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public void Reset()
    {
      Mail("Reset...");
      FState = EControllerState.Halted;
      SetState(EControllerState.Idle);
    }

  }
}
