﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace ElevatorGroup
{ //
  //------------------------------------------------------------------------
  //  Segment - Type
  //------------------------------------------------------------------------
  //
  public enum ELiftState
  {
    Error = 0,
    //Reset = 1,
    Idle = 1,
    Busy = 2,
    Halted = 3
  };

  public delegate void DOnLiftStateChanged(ELiftState liftstate);
  //
  public class CLiftState : CElevatorBase
  { //
    private const String INIT_HEADER = "LiftState";
    
    
    
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private ELiftState FState;
    private DOnLiftStateChanged FOnLiftStateChanged;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CLiftState()
      : base(INIT_HEADER)
    {
      FState = ELiftState.Idle;
    }
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetOnLiftStateChanged(DOnLiftStateChanged value)
    {
      FOnLiftStateChanged = value;
    }

    public ELiftState GetState()
    {
      return FState;
    }
    public void SetState(ELiftState state)
    {
      String Text = String.Format("-> New State[{0}]",
                                  CLiftState.LiftStateText(state));
      Mail(Text);
      FState = state;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    public static String LiftStateText(ELiftState state)
    {
      switch (state)
      {
        case ELiftState.Error:
          return "Error";
        case ELiftState.Idle:
          return "Idle";
        case ELiftState.Busy:
          return "Busy";
        case ELiftState.Halted:
          return "Halted";
      }
      return "Unknown";
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
   }
}
