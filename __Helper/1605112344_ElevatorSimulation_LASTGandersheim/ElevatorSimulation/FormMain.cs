﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
using ElevatorGroup;
using UCElevatorGroup;
//
namespace ElevatorSimulation
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private CUCElevator FUCElevator;
    // debug private CUserList FUserList;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FUCElevator = new CUCElevator();
      FUCElevator.SetNotifier(FUCNotifier);
      tbpElevator.Controls.Add(FUCElevator);
      FUCElevator.Dock = DockStyle.Fill;
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
    }

    private void FreeProgrammerControls()
    {
      FUCElevator.Stop();
      //FUserList.Stop();
      //
      tmrStartup.Stop();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      //Result &= FUCProfile.LoadInitdata(initdata, "");
      ////Result &= FUCImageQuad.LoadInitdata(initdata, "");
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      ////
      //Result &= FUCProfile.SaveInitdata(initdata, "");
      ////Result &= FUCImageQuad.SaveInitdata(initdata, "");
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback 
    //------------------------------------------------------------------------
    //    

    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          FUCElevator.SetLiftCountPositionCount(1, 4);
          //FUCElevator.SetLiftCountPositionCount(4, 6);
          FUCElevator.ClearUser();
          FUCElevator.AddUser("000");
          //FUCElevator.AddUser("111");
          //FUCElevator.AddUser("222");
          //FUCElevator.AddUser("333");
          //FUCElevator.AddUser("444");
          //FUCElevator.AddUser("555");
          //FUCElevator.AddUser("666");
          //FUCElevator.AddUser("777");
          //FUCElevator.AddUser("888");
          //FUCElevator.AddUser("999");
          FUCElevator.Start();
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          return true;
        //case 12:
        //  return true;
        //case 13:
        //  return true;
        //case 14:
        //  return true;
        //case 15:
        //  return true;
        //case 16:
        //  return true;
        //case 17:
        //  return true;
        //case 18:
        //  return true; 
        default:
          cbxAutomate.Checked = false;
          FUCElevator.Stop();
          FUCElevator.InfoUserListSorted();
          // NC FUCNotifier.Write("-");
          return false;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxAutomate.Checked)
      {
        FStartupState = 0;
        tmrStartup.Start();
      }
      else
      {
        FUCElevator.Stop();
        tmrStartup.Stop();
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //     

    //
    //------------------------------------------------------------------------
    //  Section - Action
    //------------------------------------------------------------------------
    //


  }
}
