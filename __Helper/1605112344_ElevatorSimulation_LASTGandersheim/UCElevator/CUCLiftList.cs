﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using ElevatorGroup;
//
namespace UCElevatorGroup
{
  public partial class CUCLiftList : UserControl
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "Panel";
    private const Int32 INIT_LIFTCOUNT = 1;
    private const Int32 INIT_DY = 35;
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;    
    private CUCLift[] FUCLiftVector;
    private CLiftList FLiftList;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUCLiftList()
    {
      InitializeComponent();
      //
      SetLiftCountPositionCount(INIT_LIFTCOUNT, CPosition.INIT_POSITIONCOUNT);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FLiftList.SetNotifier(FNotifier);
    }

    public void SetLiftCountPositionCount(Int32 liftcount, Int32 positioncount)
    {
      Int32 LC = Math.Max(INIT_LIFTCOUNT, liftcount);
      Int32 PC = Math.Max(CPosition.INIT_POSITIONCOUNT, positioncount);
      FLiftList = new CLiftList();
      FLiftList.SetNotifier(FNotifier);
      for (Int32 LI = 0; LI < LC; LI++)
      {
        CLift Lift = new CLift(PC);
        Lift.Header = String.Format("L{0}", LI);
        FLiftList.Add(Lift);
      }
      BuildUCLiftList();
    }

    public CLiftList GetLiftList()
    {
      return FLiftList;
    }

    public Int32 LiftCount
    {
      get { return FUCLiftVector.Length; }
    }

    public Int32 PositionCount
    {
      get { return FUCLiftVector[0].PositionCount; }
    }

    public CLift GetLift(Int32 index)
    {
      CLift Lift = null;
      if ((0 <= index) && (index < LiftCount))
      {
        Lift = FUCLiftVector[index].Lift;
      }
      return Lift;
    }

    public void SetParentSize(Int32 parentwidth, Int32 parentheight)
    {
      SetLiftSizes();
      SetLiftPositions();
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    private void BuildUCLiftList()
    {
      ClearUCLiftList();
      FUCLiftVector = new CUCLift[FLiftList.Count];
      Int32 LI = 0;
      foreach (CLift Lift in FLiftList)
      {
        CUCLift UCLift = new CUCLift();
        FUCLiftVector[LI] = UCLift;
        UCLift.SetLift(Lift);
        this.Controls.Add(UCLift);
        LI++;
      }
      SetLiftSizes();
      SetLiftPositions();
    }

    private void ClearUCLiftList()
    {
      Int32 CC = Controls.Count;
      for (Int32 CI = CC - 1; 0 <= CI; CI--)
      {
        Control C = Controls[CI];
        if (C is CUCLift)
        {
          Controls.Remove(C);
          C.Dispose();
        }
      }
      FUCLiftVector = null;
    }

    private void SetLiftSizes()
    {
      Int32 LX = this.Width / LiftCount;
      Int32 LY = this.Height - this.Height / INIT_DY;
      for (Int32 LI = 0; LI < LiftCount; LI++)
      {
        FUCLiftVector[LI].Width = LX;
        FUCLiftVector[LI].Height = LY;
      }
    }

    private void SetLiftPositions()
    {
      Int32 LX = this.Width / LiftCount;
      for (Int32 LI = 0; LI < LiftCount; LI++)
      {
        FUCLiftVector[LI].Left = LX * LI;
        FUCLiftVector[LI].Top = lblHeader.Height;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public void Start()
    {
      FLiftList.Start();
    }

    public void Stop()
    {
      FLiftList.Stop();
    }

  }
}
