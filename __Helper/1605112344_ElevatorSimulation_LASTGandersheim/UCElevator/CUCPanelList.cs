﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using ElevatorGroup;
//
namespace UCElevatorGroup
{
  public partial class CUCPanelList : UserControl
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "PanelList";
    public const Int32 INIT_DX = 20;
    private const Int32 INIT_DY = 20;
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CUCPanel[] FUCPanelVector;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUCPanelList()
    {
      InitializeComponent();
      //
      SetPositionCount(CPosition.INIT_POSITIONCOUNT);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetPositionCount(Int32 positioncount)
    { // for one position one panel
      Int32 PC = Math.Max(CPosition.INIT_POSITIONCOUNT, positioncount);
      ClearUCPanels();
      FUCPanelVector = new CUCPanel[PC];
      for (Int32 PI = 0; PI < PC; PI++)
      {
        FUCPanelVector[PI] = new CUCPanel();
        this.Controls.Add(FUCPanelVector[PI]);
        FUCPanelVector[PI].SetTargetCount(PC);
      }
      SetPanelPositions();
    }

    public Int32 PanelCount
    {
      get { return FUCPanelVector.Length; }
    }

    public Int32 PositionCount
    {
      get { return FUCPanelVector.Length; }
    }

    public CPanel GetPanel(Int32 index)
    {
      CPanel Panel = null;
      if ((0 <= index) && (index < PositionCount))
      {
        Panel = FUCPanelVector[index].Panel;
      }
      return Panel;
    }

    public CUCPanel GetUCPanel(Int32 index)
    {
      CUCPanel UCPanel = null;
      if ((0 <= index) && (index < PositionCount))
      {
        UCPanel = FUCPanelVector[index];
      }
      return UCPanel;
    }

    public void SetParentSize(Int32 parentwidth, Int32 parentheight)
    {
      SetPanelPositions();
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    private void ClearUCPanels()
    {
      Int32 CC = Controls.Count;
      for (Int32 CI = CC - 1; 0 <= CI; CI--)
      {
        Control C = Controls[CI];
        if (C is CUCPanel)
        {
          Controls.Remove(C);
          C.Dispose();
        }
      }
      FUCPanelVector = null;
    }

    private void SetPanelPosition(Int32 positionactual)
    {
      CUCPanel UCPanel = GetUCPanel(positionactual);
      if (UCPanel is CUCPanel)
      {
        UCPanel.Left = Width / INIT_DX;
        Int32 DY = (Height - 2 * lblHeader.Height) / PositionCount;
        UCPanel.Top = 2 * lblHeader.Height + INIT_DY / PositionCount + positionactual * DY;
        UCPanel.Height = DY - DY / INIT_DY;
      }
    }

    private void SetPanelPositions()
    {
      Int32 PC = PositionCount;
      CUCPanel UCPanel = FUCPanelVector[0];
      Width = UCPanel.Width + 2 * UCPanel.Width / INIT_DX;
      for (Int32 PI = 0; PI < PC; PI++)
      {
        SetPanelPosition(PI);
      }
    }

  }
}
