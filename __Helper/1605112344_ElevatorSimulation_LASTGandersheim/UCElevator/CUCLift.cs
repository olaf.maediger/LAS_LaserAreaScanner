﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using ElevatorGroup;
//
namespace UCElevatorGroup
{
  public partial class CUCLift : CUCElevatorBase
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "Lift";
    public const Int32 INIT_DX = 10;
    private const Int32 INIT_DY = 50;
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUCLift()
    {
      InitializeComponent();
      //
      Lift = new CLift(CPosition.INIT_POSITIONCOUNT);
      Lift.Header = INIT_HEADER;
      Lift.SetOnPositionCountChanged(LiftOnPositionCountChanged);
      Lift.SetOnPositionActualChanged(LiftOnPositionActualChanged);
      Lift.SetOnPositionTargetChanged(LiftOnPositionTargetChanged);
      Lift.SetOnLiftStateChanged(LiftOnLiftStateChanged);
      // 
      SetSizeCage();
      SetPositionCage(Lift.PositionActual);
      SetSizeCable();
      //
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ShowLiftState(ELiftState.Reset);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public CLift Lift
    {
      get { return (CLift)ElevatorBase; }
      set { ElevatorBase = value; }
    }

    // preset: lift -> uclift
    public void SetLift(CLift lift)
    {
      Lift = lift;
      Lift.SetOnPositionCountChanged(LiftOnPositionCountChanged);
      Lift.SetOnPositionActualChanged(LiftOnPositionActualChanged);
      Lift.SetOnPositionTargetChanged(LiftOnPositionTargetChanged);
      //!!!!!!!!!!!!!!!!!!!!Lift.SetOnLiftStateChanged(LiftOnLiftStateChanged);
      // 
      SetSizeCage();
      SetPositionCage(Lift.PositionActual);
      SetSizeCable();
    }

    public Int32 PositionCount
    {
      get { return Lift.PositionCount; }
    }

    public void SetPositionCount(Int32 value)
    {
      Lift.PositionCount = value;
    }

    public void SetPositionActual(Int32 value)
    {
      Lift.PositionActual = value;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Event
    //------------------------------------------------------------------------
    //
    private void CUCLift_SizeChanged(object sender, EventArgs e)
    {
      SetSizeCage();
      SetPositionCage(Lift.PositionActual);
      SetSizeCable();
    }


    private void LiftOnPositionCountChanged(Int32 positioncount)
    {
      SetSizeCage();
      SetPositionCage(Lift.PositionActual);
      SetSizeCable();
    }

    private void LiftOnPositionActualChanged(Int32 positionactual)
    {
      SetSizeCage();
      SetPositionCage(Lift.PositionActual);
      SetSizeCable();
    }

    private void LiftOnPositionTargetChanged(Int32 positiontarget)
    {
      //!!!!RefreshLiftInfo();
    }

    private void LiftOnLiftStateChanged(ELiftState liftstate)
    {
      //!!!!!!!!!!!!!!!!!this.ShowLiftState(liftstate);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    public void SetSizeCage()
    {
      Int32 LX = Width - 2 * Width / INIT_DX;
      Int32 LY = Height - lblHeader.Height;
      LY /= Lift.PositionCount;
      pbxCage.Width = LX;
      pbxCage.Height = LY - LY / INIT_DX;
    }

    private delegate void CBSetPositionCage(Int32 positionactual);
    private void SetPositionCage(Int32 positionactual)
    {
      if (this.InvokeRequired)
      {
        CBSetPositionCage CB = new CBSetPositionCage(SetPositionCage);
        Invoke(CB, new object[] { positionactual });
      }
      else
      {
        pbxCage.Left = Width / INIT_DX;
        Int32 DY = (Height - lblHeader.Height) / Lift.PositionCount;
        pbxCage.Top = lblHeader.Height + INIT_DY / Lift.PositionCount + positionactual * DY;
      }
    }

    private delegate void CBSetSizeCable();
    private void SetSizeCable()
    {
      if (this.InvokeRequired)
      {
        CBSetSizeCable CB = new CBSetSizeCable(SetSizeCable);
        Invoke(CB, new object[] { });
      }
      else
      {
        pbxCable.Top = lblHeader.Height;
        pbxCable.Left = this.Width / 2 - pbxCable.Width / 2;
        pbxCable.Height = pbxCage.Top - lblHeader.Height;
      }
    }

    //private delegate void CBShowLiftState(ELiftState liftstate);
    //private void ShowLiftState(ELiftState liftstate)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBShowLiftState CB = new CBShowLiftState(ShowLiftState);
    //    Invoke(CB, new object[] { liftstate });
    //  }
    //  else
    //  {
    //    RefreshLiftInfo();
    //    //lblHeader.Text = String.Format("{0}: ST[{1}] PA[{2}] PT[{3}]", 
    //    //                               Lift.Header, liftstate.ToString(),
    //    //                               Lift.PositionActual, Lift.PositionTarget);
    //    //switch (liftstate)
    //    //{
    //    //  case ELiftState.Error:
    //    //    lblHeader.BackColor = Color.Magenta;
    //    //    break;
    //    //  case ELiftState.Reset:
    //    //    lblHeader.BackColor = Color.Pink;
    //    //    break;
    //    //  case ELiftState.Idle:
    //    //    lblHeader.BackColor = Color.LightGreen;
    //    //    break;
    //    //  case ELiftState.Busy:
    //    //    lblHeader.BackColor = Color.LightBlue;
    //    //    break;
    //    //}
    //  }
    //}

    //private delegate void CBRefreshLiftInfo();
    //private void RefreshLiftInfo()
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBRefreshLiftInfo CB = new CBRefreshLiftInfo(RefreshLiftInfo);
    //    Invoke(CB, new object[] { });
    //  }
    //  else
    //  {
    //    lblHeader.Text = String.Format("{0}: ST[{1}] PA[{2}] PT[{3}]",
    //                                   Lift.Header, Lift.GetState().ToString(),
    //                                   Lift.PositionActual, Lift.PositionTarget);
    //    switch (Lift.GetState())
    //    {
    //      case ELiftState.Error:
    //        lblHeader.BackColor = Color.Magenta;
    //        break;
    //      case ELiftState.Reset:
    //        lblHeader.BackColor = Color.Pink;
    //        break;
    //      case ELiftState.Idle:
    //        lblHeader.BackColor = Color.LightGreen;
    //        break;
    //      case ELiftState.Busy:
    //        lblHeader.BackColor = Color.LightBlue;
    //        break;
    //    }
    //  }
    //}
 
  }
}
