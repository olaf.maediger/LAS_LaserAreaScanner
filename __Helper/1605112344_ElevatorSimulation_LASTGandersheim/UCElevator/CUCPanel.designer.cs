﻿namespace UCElevatorGroup
{
  partial class CUCPanel
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.pnlTarget = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.lblTarget = new System.Windows.Forms.Label();
      this.btnUp = new System.Windows.Forms.Button();
      this.pnlActual = new System.Windows.Forms.Panel();
      this.label2 = new System.Windows.Forms.Label();
      this.lblActual = new System.Windows.Forms.Label();
      this.btnDown = new System.Windows.Forms.Button();
      this.pnlTarget.SuspendLayout();
      this.pnlActual.SuspendLayout();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(160)))), ((int)(((byte)(0)))));
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(118, 18);
      this.lblHeader.TabIndex = 0;
      this.lblHeader.Text = "Panel";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnlTarget
      // 
      this.pnlTarget.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlTarget.Controls.Add(this.label1);
      this.pnlTarget.Controls.Add(this.lblTarget);
      this.pnlTarget.Controls.Add(this.btnUp);
      this.pnlTarget.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTarget.Location = new System.Drawing.Point(0, 18);
      this.pnlTarget.Name = "pnlTarget";
      this.pnlTarget.Size = new System.Drawing.Size(118, 55);
      this.pnlTarget.TabIndex = 9;
      // 
      // label1
      // 
      this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
      this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(56, 19);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(60, 34);
      this.label1.TabIndex = 7;
      this.label1.Text = "0";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblTarget
      // 
      this.lblTarget.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTarget.Location = new System.Drawing.Point(56, 0);
      this.lblTarget.Name = "lblTarget";
      this.lblTarget.Size = new System.Drawing.Size(60, 19);
      this.lblTarget.TabIndex = 6;
      this.lblTarget.Text = "Target";
      this.lblTarget.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // btnUp
      // 
      this.btnUp.BackgroundImage = global::UCElevatorGroup.Properties.Resources.Up;
      this.btnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.btnUp.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnUp.Location = new System.Drawing.Point(0, 0);
      this.btnUp.Name = "btnUp";
      this.btnUp.Size = new System.Drawing.Size(56, 53);
      this.btnUp.TabIndex = 1;
      this.btnUp.UseVisualStyleBackColor = true;
      // 
      // pnlActual
      // 
      this.pnlActual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlActual.Controls.Add(this.label2);
      this.pnlActual.Controls.Add(this.lblActual);
      this.pnlActual.Controls.Add(this.btnDown);
      this.pnlActual.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlActual.Location = new System.Drawing.Point(0, 73);
      this.pnlActual.Name = "pnlActual";
      this.pnlActual.Size = new System.Drawing.Size(118, 55);
      this.pnlActual.TabIndex = 10;
      // 
      // label2
      // 
      this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
      this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(56, 20);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(60, 33);
      this.label2.TabIndex = 7;
      this.label2.Text = "0";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblActual
      // 
      this.lblActual.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblActual.Location = new System.Drawing.Point(56, 0);
      this.lblActual.Name = "lblActual";
      this.lblActual.Size = new System.Drawing.Size(60, 20);
      this.lblActual.TabIndex = 5;
      this.lblActual.Text = "Actual";
      this.lblActual.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // btnDown
      // 
      this.btnDown.BackgroundImage = global::UCElevatorGroup.Properties.Resources.Down;
      this.btnDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.btnDown.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnDown.Location = new System.Drawing.Point(0, 0);
      this.btnDown.Name = "btnDown";
      this.btnDown.Size = new System.Drawing.Size(56, 53);
      this.btnDown.TabIndex = 3;
      this.btnDown.UseVisualStyleBackColor = true;
      // 
      // CUCPanel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(200)))), ((int)(((byte)(0)))));
      this.Controls.Add(this.pnlActual);
      this.Controls.Add(this.pnlTarget);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCPanel";
      this.Size = new System.Drawing.Size(118, 133);
      this.SizeChanged += new System.EventHandler(this.CUCPanel_SizeChanged);
      this.pnlTarget.ResumeLayout(false);
      this.pnlActual.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Panel pnlTarget;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label lblTarget;
    private System.Windows.Forms.Button btnUp;
    private System.Windows.Forms.Panel pnlActual;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lblActual;
    private System.Windows.Forms.Button btnDown;
  }
}
