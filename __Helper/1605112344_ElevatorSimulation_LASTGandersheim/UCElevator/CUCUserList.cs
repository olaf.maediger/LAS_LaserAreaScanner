﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using ElevatorGroup;
//
namespace UCElevatorGroup
{
  public partial class CUCUserList : UserControl
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "UserList";
    private const Int32 INIT_USERCOUNT = 1;
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CUserList FUserList;
    private DOnGetController FOnGetController;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUCUserList()
    {
      InitializeComponent();
      //
      FUserList = new CUserList();
      //FUserList.SetOnEventUserRequest(UserListOnEventUserRequest);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FUserList.SetNotifier(FNotifier);
    }

    public void SetOnGetController(DOnGetController value)
    {
      FOnGetController = value;
      FUserList.SetOnGetController(FOnGetController);
    }

    public void SetOnGetLiftList(DOnGetLiftList value)
    {
      FUserList.SetOnGetLiftList(value);
    }

    //public CUserList GetUserList()
    //{
    //  return FUserList; 
    //}
    //public void SetUserList(CUserList value)
    //{
    //  FUserList = value;
    //}

    public CUserList UserList
    {
      get { return FUserList; }
    //  set { FUserList = value; }
    }

    //public void SetUserCount(Int32 usercount)
    //{ 
    //  Int32 UC = Math.Max(INIT_USERCOUNT, usercount);
    //  FUserList.Clear();
    //  for (Int32 UI = 0; UI < UC; UI++)
    //  {
    //    CUser User = new CUser(UI.ToString());
    //    FUserList.AddUser(User);
    //  }
    //}

    public Int32 UserCount
    {
      get { return FUserList.Count; }
    }

    public CUser GetUser(Int32 index)
    {
      CUser User = null;
      if ((0 <= index) && (index < UserCount))
      {
        User = FUserList[index];
      }
      return User;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Segment - Callback - UserList
    //------------------------------------------------------------------------
    //
    private void UserListOnEventUserRequest(CUser user)
    {

    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public Boolean AddUser(String name)
    {
      return FUserList.AddUser(name);
    }

    public Boolean AddUser(CUser user)
    {
      return FUserList.AddUser(user);
    }

    public Boolean ClearUser()
    {
      return FUserList.ClearUser();
    }

    public Boolean Start()
    {
      return FUserList.Start();
    }

    public Boolean Stop()
    {
      return FUserList.Stop();
    }

    public void Info()
    {
      FUserList.Info();
    }

    public void InfoUserListSorted()
    {
      CUserList ULRTS = UserList.GetUserRequestsTimeSorted();
      ULRTS.Info();
    }
    



  }
}
