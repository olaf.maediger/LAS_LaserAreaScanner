﻿namespace UCElevatorGroup
{
  partial class CUCLift
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pbxCable = new System.Windows.Forms.PictureBox();
      this.pbxCage = new System.Windows.Forms.PictureBox();
      this.lblHeader = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.pbxCable)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxCage)).BeginInit();
      this.SuspendLayout();
      // 
      // pbxCable
      // 
      this.pbxCable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.pbxCable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxCable.Location = new System.Drawing.Point(48, 44);
      this.pbxCable.Name = "pbxCable";
      this.pbxCable.Size = new System.Drawing.Size(5, 60);
      this.pbxCable.TabIndex = 11;
      this.pbxCable.TabStop = false;
      // 
      // pbxCage
      // 
      this.pbxCage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
      this.pbxCage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxCage.Location = new System.Drawing.Point(2, 111);
      this.pbxCage.Name = "pbxCage";
      this.pbxCage.Size = new System.Drawing.Size(95, 83);
      this.pbxCage.TabIndex = 10;
      this.pbxCage.TabStop = false;
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(160)))), ((int)(((byte)(0)))));
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(99, 18);
      this.lblHeader.TabIndex = 12;
      this.lblHeader.Text = "<lift>";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCLift
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
      this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.pbxCable);
      this.Controls.Add(this.pbxCage);
      this.Name = "CUCLift";
      this.Size = new System.Drawing.Size(99, 411);
      this.SizeChanged += new System.EventHandler(this.CUCLift_SizeChanged);
      ((System.ComponentModel.ISupportInitialize)(this.pbxCable)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxCage)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox pbxCage;
    private System.Windows.Forms.PictureBox pbxCable;
    private System.Windows.Forms.Label lblHeader;
  }
}
