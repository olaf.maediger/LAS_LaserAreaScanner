﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using ElevatorGroup;
//
namespace UCElevatorGroup
{
  public partial class CUCElevator : CUCElevatorBase
  {
    //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "Elevator";
    private const Int32 INIT_INTERVAL_DX = 10;
    private const Int32 INIT_INTERVAL_DY = 10;
    private const Int32 INIT_LIFTCOUNT = 1;
    private const Int32 INIT_POSITIONCOUNT = 4;
    private const Int32 INIT_USERCOUNT = 1;
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CUCController FUCController;
    private CUCPanelList FUCPanelList;
    private CUCLiftList FUCLiftList;
    private CUCUserList FUCUserList;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUCElevator()
    {
      InitializeComponent();
      //
      FUCController = new CUCController();
      pnlController.Controls.Add(FUCController);
      FUCController.Dock = DockStyle.Fill;
      FUCController.SetOnGetUserList(UCControllerOnGetUserList);
      FUCController.SetOnGetLiftList(UCControllerOnGetLiftList);
      //
      FUCPanelList = new CUCPanelList();
      pnlPanel.Controls.Add(FUCPanelList);
      FUCPanelList.Dock = DockStyle.Left;
      SetPanelCount(INIT_POSITIONCOUNT);
      // 
      FUCLiftList = new CUCLiftList();
      pnlLift.Controls.Add(FUCLiftList);
      FUCLiftList.Dock = DockStyle.Fill;
      SetLiftCountPositionCount(INIT_LIFTCOUNT, INIT_POSITIONCOUNT);
      //
      FUCUserList = new CUCUserList();
      //pnlUserList.Controls.Add(FUCUserList);
      //FUCUserList.Dock = DockStyle.Fill;
      //FUCUserList.SetUserCount(INIT_USERCOUNT);
      FUCUserList.SetOnGetLiftList(UCUserListOnGetLiftList);
      FUCUserList.SetOnGetController(UCUserListOnGetController);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FUCController.SetNotifier(value);
      FUCPanelList.SetNotifier(value);
      FUCLiftList.SetNotifier(value);
      FUCUserList.SetNotifier(value);
    }

    protected CElevator Elevator
    {
      get { return (CElevator)ElevatorBase; }
      set { ElevatorBase = value; }
    }

    public void SetPanelCount(Int32 positioncount)
    {
      FUCPanelList.SetPositionCount(positioncount);
    }

    public void SetLiftCountPositionCount(Int32 liftcount, Int32 positioncount)
    {
      FUCLiftList.SetLiftCountPositionCount(liftcount, positioncount);
    }

    public void SetLiftPositionCount(Int32 liftindex, Int32 positioncount)
    {
      CLift Lift = FUCLiftList.GetLift(liftindex);
      if (Lift is CLift)
      {
        Lift.PositionCount = positioncount;
      }
    }

    public void SetLiftPositionActual(Int32 liftindex, Int32 positionactual)
    {
      CLift Lift = FUCLiftList.GetLift(liftindex);
      if (Lift is CLift)
      {
        Lift.PositionActual = positionactual;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Event
    //------------------------------------------------------------------------
    //
    private void CUCElevator_SizeChanged(object sender, EventArgs e)
    {
      FUCPanelList.Left = 0;
      FUCPanelList.Top = 0;
      FUCPanelList.SetParentSize(pnlPanel.Width, pnlPanel.Height);
      // reflexive
      pnlPanel.Width = FUCPanelList.Width;
      //
      FUCLiftList.Left = 0;
      FUCLiftList.Top = 0;
      FUCLiftList.SetParentSize(pnlLift.Width, pnlLift.Height);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //


    //
    //------------------------------------------------------------------------
    //  Segment - Callback - UCController
    //------------------------------------------------------------------------
    //
    private CUserList UCControllerOnGetUserList()
    {
      return FUCUserList.UserList;
    }

    private CLiftList UCControllerOnGetLiftList()
    {
      return FUCLiftList.GetLiftList();
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Callback - UCUserList
    //------------------------------------------------------------------------
    //
    private CController UCUserListOnGetController()
    {
      return FUCController.GetController();
    }

    private CLiftList UCUserListOnGetLiftList()
    {
      return FUCLiftList.GetLiftList();
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public void AddUser(String name)
    {
      FUCUserList.AddUser(name);
    }

    public void ClearUser()
    {
      FUCUserList.ClearUser();
    }

    public void Start()
    {
      FUCController.Start();
      FUCLiftList.Start();
      FUCUserList.Start();
    }

    public void Stop()
    {
      FUCController.Stop();
      FUCLiftList.Stop();
      FUCUserList.Stop();
    }

    public void Info()
    {
      FUCUserList.Info();
    }

    public void InfoUserListSorted()
    {
      FUCUserList.InfoUserListSorted();
    }

  }
}
