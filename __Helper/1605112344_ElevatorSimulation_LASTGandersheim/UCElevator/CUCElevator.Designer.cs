﻿namespace UCElevatorGroup
{
  partial class CUCElevator
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.splController = new System.Windows.Forms.Splitter();
      this.pnlLift = new System.Windows.Forms.Panel();
      this.pnlPanel = new System.Windows.Forms.Panel();
      this.pnlController = new System.Windows.Forms.Panel();
      this.lblHeader = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // splController
      // 
      this.splController.Dock = System.Windows.Forms.DockStyle.Top;
      this.splController.Location = new System.Drawing.Point(161, 56);
      this.splController.Name = "splController";
      this.splController.Size = new System.Drawing.Size(430, 3);
      this.splController.TabIndex = 17;
      this.splController.TabStop = false;
      // 
      // pnlLift
      // 
      this.pnlLift.BackColor = System.Drawing.Color.White;
      this.pnlLift.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlLift.Location = new System.Drawing.Point(161, 56);
      this.pnlLift.Name = "pnlLift";
      this.pnlLift.Size = new System.Drawing.Size(430, 504);
      this.pnlLift.TabIndex = 16;
      // 
      // pnlPanel
      // 
      this.pnlPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
      this.pnlPanel.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlPanel.Location = new System.Drawing.Point(0, 56);
      this.pnlPanel.Name = "pnlPanel";
      this.pnlPanel.Size = new System.Drawing.Size(161, 504);
      this.pnlPanel.TabIndex = 15;
      // 
      // pnlController
      // 
      this.pnlController.BackColor = System.Drawing.Color.Linen;
      this.pnlController.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlController.Location = new System.Drawing.Point(0, 18);
      this.pnlController.Name = "pnlController";
      this.pnlController.Size = new System.Drawing.Size(591, 38);
      this.pnlController.TabIndex = 14;
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(160)))), ((int)(((byte)(0)))));
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(591, 18);
      this.lblHeader.TabIndex = 10;
      this.lblHeader.Text = "Elevator";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCElevator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Silver;
      this.Controls.Add(this.splController);
      this.Controls.Add(this.pnlLift);
      this.Controls.Add(this.pnlPanel);
      this.Controls.Add(this.pnlController);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCElevator";
      this.Size = new System.Drawing.Size(591, 560);
      this.SizeChanged += new System.EventHandler(this.CUCElevator_SizeChanged);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Panel pnlController;
    private System.Windows.Forms.Panel pnlLift;
    private System.Windows.Forms.Panel pnlPanel;
    private System.Windows.Forms.Splitter splController;

  }
}
