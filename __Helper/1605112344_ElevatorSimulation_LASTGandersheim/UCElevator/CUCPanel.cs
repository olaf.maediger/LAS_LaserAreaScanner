﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using ElevatorGroup;
//
namespace UCElevatorGroup
{
  public partial class CUCPanel : CUCElevatorBase
  { //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "Panel";
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUCPanel()
    {
      InitializeComponent();
      //
      Panel = new CPanel();
      Panel.Header = INIT_HEADER;
      //Panel.SetOnTargetIndexChanged(PanelOnTargetIndexChanged);
      // 
      //SetPositionPanel(Panel.TargetIndex);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public CPanel Panel
    {
      get { return (CPanel)ElevatorBase; }
      set { ElevatorBase = value; }
    }

    public void SetTargetIndex(Int32 value)
    {
      Panel.TargetIndex = value;
    }

    public void SetTargetCount(Int32 value)
    {
      Panel.TargetCount = value;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Event
    //------------------------------------------------------------------------
    //
    private void CUCPanel_SizeChanged(object sender, EventArgs e)
    {
      Int32 H = (this.Height - lblHeader.Height) / 2;
      pnlTarget.Height = H;
      btnUp.Width = H;
      pnlActual.Height = H;
      btnDown.Width = H;
     }

    private void LiftOnTargetIndexChanged(Int32 targetindex)
    {
      //SetPositionPanel(Panel.TargetIndex);
    }

    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
 

  }
}
