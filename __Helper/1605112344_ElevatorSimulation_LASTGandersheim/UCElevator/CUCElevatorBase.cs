﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using ElevatorGroup;
//
namespace UCElevatorGroup
{
  public partial class CUCElevatorBase : UserControl
  {
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CElevatorBase FElevatorBase;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUCElevatorBase()
    {
      InitializeComponent();

    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    protected CElevatorBase ElevatorBase
    {
      get { return FElevatorBase; }
      set { FElevatorBase = value; }
    }

    public String GetHeader()
    {
      return "";
    }
    public void SetHeader(String value)
    {
    }
    public void SetOnSetHeader(DOnSetHeader value)
    {

    }
    public String Header
    {
      get { return GetHeader(); }
      set { SetHeader(value); }
    }


  }
}
