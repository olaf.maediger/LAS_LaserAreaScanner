﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using ElevatorGroup;
//
namespace UCElevatorGroup
{
  public partial class CUCController : CUCElevatorBase
  {
    //
    //------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_HEADER = "Main"; 
    //
    //------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    //
    public CUCController()
    {
      InitializeComponent();
      // 
      Controller = new CController(INIT_HEADER);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      Controller.SetNotifier(value);
    }
    
    protected CController Controller
    {
      get { return (CController)ElevatorBase; }
      set { ElevatorBase = value; }
    }

    public CController GetController()
    {
      return this.Controller;
    }

    public void SetOnGetUserList(DOnGetUserList value)
    {
      Controller.SetOnGetUserList(value);
    }

    public void SetOnGetLiftList(DOnGetLiftList value)
    {
      Controller.SetOnGetLiftList(value);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------
    //
    public Boolean Start()
    {
      return Controller.Start();
    }

    public Boolean Stop()
    {
      return Controller.Stop();
    }
    //
    //------------------------------------------------------------------------
    //  Segment - 
    //------------------------------------------------------------------------
    //

  }
}
