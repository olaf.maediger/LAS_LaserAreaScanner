#include "Defines.h"
#include "HardwareSerial.h"
#include "USBAPI.h"

//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
class CSerialBase
{
  protected:
  
  public:
  virtual bool Open() = 0;
  virtual bool Write(char *text) = 0;
};
//
//----------------------------------------------------
//  Segment - CSerialZ (^= Serial)
//----------------------------------------------------
//
class CSerialZ : CSerialBase
{
  protected:
  UARTClass* FPSerial;
  
  public:
  CSerialZ(UARTClass *serial)
  {
    FPSerial = serial;
  }
  
  bool Open()
  {
    FPSerial->begin(115200);
  }
  
  bool Write(char *text)
  {
    FPSerial->write(text);
  }  
};
//
//----------------------------------------------------
//  Segment - CSerialS (^= Serial1 / Serial2 / Serial3)
//----------------------------------------------------
//
class CSerialS : CSerialBase
{
  protected:
  USARTClass* FPSerial;
  
  public:
  CSerialS(USARTClass *serial)
  {
    FPSerial = serial;
  }
  
  bool Open()
  {
    FPSerial->begin(115200);
  }
  
  bool Write(char *text)
  {
    FPSerial->write(text);
  }  
};
//
//----------------------------------------------------
//  Segment - CSerialU (^=SerialUSB)
//----------------------------------------------------
//
class CSerialU : CSerialBase
{
  protected:
  Serial_* FPSerial;
  
  public:
  CSerialU(Serial_ *serial)
  {
    FPSerial = serial;
  }
  
  bool Open()
  {
    FPSerial->begin(115200);
  }
  
  bool Write(char *text)
  {
    FPSerial->write(text);
  }  
};
//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
class CSerial
{
  protected:
  //CSerialBase* FPSerial;
  CSerialBase* FPSerial;
  
  public:
  CSerial(UARTClass *serial);
  CSerial(UARTClass &serial);
  CSerial(USARTClass *serial);
  CSerial(USARTClass &serial);
  CSerial(Serial_ *serial);
  CSerial(Serial_ &serial);
  bool Open();
  bool Write(char *text);
};

