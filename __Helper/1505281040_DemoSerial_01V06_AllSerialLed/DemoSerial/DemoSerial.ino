#include "Defines.h"
#include "Led.h"
#include "Serial.h"

CLed LedSystem(PIN_LEDSYSTEM);
CSerial SerialFDL1(Serial);
CSerial SerialFDL2(Serial1);
CSerial SerialFDL3(Serial2);
CSerial SerialFDL4(Serial3);
CSerial SerialPC(SerialUSB);

int Counter = 0;
char TxdBuffer[32];

void setup() 
{
  LedSystem.Open();
  SerialFDL1.Open();
  SerialFDL2.Open();
  SerialFDL3.Open();
  SerialFDL4.Open();
  SerialPC.Open();
}

void loop() 
{
  LedSystem.Toggle();
  //
  sprintf(TxdBuffer, "\r\nFDL1[%d]>", Counter);
  SerialFDL1.Write(TxdBuffer);
  delay(300);
  //
  sprintf(TxdBuffer, "\r\nFDL2[%d]>", Counter);
  SerialFDL2.Write(TxdBuffer);
  delay(300);
  //
  sprintf(TxdBuffer, "\r\nFDL3[%d]>", Counter);
  SerialFDL3.Write(TxdBuffer);
  delay(300);
  //
  sprintf(TxdBuffer, "\r\nFDL4[%d]>", Counter);
  SerialFDL4.Write(TxdBuffer);
  delay(300);
  //
  sprintf(TxdBuffer, "\r\nPC[%d]>", Counter);
  SerialPC.Write(TxdBuffer);
  delay(300);
  //
  Counter++;
}
