﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GrabberHardware
{
  public static class CDefinition
  {
    public const String CAMERANAME_UNKNOWN = "Unknown";
    //
    //--------------------------------------------
    //	Section - Constant - Common
    //--------------------------------------------
    //    
    public enum ECameraIndex
    { // WebCamera
      Dem130Ci = 0,
      DntDigitalMicroscope = 1,
      PhilipsSpc900Nc = 2,
      Usb2p0PcCamera = 3,
      // VisioSens
      VfuV024 = 4,
      // PointGrey
      Flea308Bit = 5,
      Flea310Bit = 6
    };

    public static String[] CAMERA_NAMES = 
    { // WebCamera
      "D130C-i",
      "Digital microscope",
      "Philips SPC 900NC",
      "USB2.0 PC CAMERA",
      // VisioSens
      "VFU-V024",
      // PointGrey
      "Flea308Bit",
      "Flea310Bit"
    };

    public static String[] ENTRY_NAMES = 
    { // WebCamera
      "Web D130C-i",
      "Web Digital microscope",
      "Web Philips SPC 900NC",
      "Web USB2.0 PC CAMERA",
      // VisioSens
      "VisioSens VFU-V024",
      // PointGrey
      "PointGrey Flea308Bit",
      "PointGrey Flea310Bit"
    };
    public static String[] LIBRARY_NAMES = 
    { // WebCamera
      "WebDem130C",
      "WebDntDigitalMicroscope",
      "WebPhilipsSpc900Nc",
      "WebUsb20PcCamera",
      // VisioSens
      "VisioSensVfuV024",
      // PointGrey
      "PointGreyFlea308Bit",
      "PointGreyFlea310Bit"
    };


  }
}
