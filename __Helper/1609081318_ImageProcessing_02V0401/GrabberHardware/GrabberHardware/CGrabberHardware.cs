﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
//
using IPPaletteBase;
using IPPalette256;
using IPMemory;
using IPMatrix;
using IPBitmap;
using Task;
//

//
/*
 * WICHTIG!!!!
 * Get/setProperties bei GrabberHardware
 *    Kind        Hardware Software(Emulation)
 * - Exposure        0        1
 * - TrimIntensity   0        1

 * alle abfragbar gestalten -> Programmierer kann reagieren (Funktionen enablen/disablen)!
*/



namespace GrabberHardware
{
  //
  //-------------------------------------------------------------------------------
  //  Segment - Type - Data
  //-------------------------------------------------------------------------------
  //
  public enum EAcquisitionMode
  {
    None,
    SingleSnap,
    MultipleSnap,
    Continuous
  };

  public enum EPixelResolution
  {
    Resolution08Bit,
    Resolution10Bit,
    Resolution12Bit,
    Resolution16Bit
  };

  public enum EImageFormat
  {
    Undefined,
    G8,
    R8G8B8,
    B8G8R8,
    A8R8G8B8,
    B8G8R8A8,
    A16R16G16B16,
    B16G16R16A16,
  };
  //
  public delegate void DOnErrorGrabberHardware(String namelibrary,
                                               String namefunction,
                                               String texterror);
  public delegate void DOnInfoGrabberHardware(String namelibrary,
                                              String textinfo);
  //
  public delegate void DOnOpenCloseChanged(Boolean isopen);
  public delegate void DOnAcquisitionModeChanged(EAcquisitionMode acquisitionmode);
  public delegate void DOnPixelResolutionChanged(EPixelResolution pixelresolution);
  //
  public delegate void DOnAcquisitionStart(RTaskData data);
  public delegate void DOnAcquisitionBusy(RTaskData data);
  public delegate void DOnAcquisitionEnd(RTaskData data);
  public delegate void DOnAcquisitionAbort(RTaskData data);
  public delegate void DOnFinishStart(RTaskData data);
  public delegate void DOnFinishBusy(RTaskData data);
  public delegate void DOnFinishEnd(RTaskData data);
  public delegate void DOnFinishAbort(RTaskData data);
  //
  public delegate void DOnImageByteDetected(Byte[,] matrix,
                                            Int32 width,
                                            Int32 height,
                                            EImageFormat imageformat,
                                            Int32 framecountpreset,
                                            Int32 framecountactual);
  public delegate void DOnImageWordDetected(UInt16[,] matrix,
                                            Int32 width,
                                            Int32 height,
                                            EImageFormat imageformat,
                                            Int32 framecountpreset, 
                                            Int32 framecountactual);
  public delegate void DOnBitmapDetected(Bitmap bitmap,
                                         Int32 width,
                                         Int32 height,
                                         EImageFormat imageformat,
                                         Int32 framecountpreset,
                                         Int32 framecountactual);
  // TrueColor
  public delegate void DOnCameraStateChanged(Boolean cameraisopen, Boolean cameraisbusy);
  public delegate void DOnNewFrameBitmapRgb(CBitmapRgb bitmap24bpp);
  public delegate void DOnNewFrameBitmapArgb(CBitmapArgb bitmap32bpp);
  public delegate void DOnNewFrameBitmap256(CBitmap256 bitmap256);
  public delegate void DOnNewFrameMatrix08Bit(CMatrix08Bit matrix08Bit);
  public delegate void DOnNewFrameMatrix10Bit(CMatrix10Bit matrix10bit);
  public delegate void DOnNewFrameMatrix12Bit(CMatrix12Bit matrix12bit);
  public delegate void DOnNewFrameMatrix16Bit(CMatrix16Bit matrix16bit);
  public delegate void DOnNewFrameMatrixByte(Byte[,] matrixbyte);
  public delegate void DOnNewFrameMatrixUInt10(UInt16[,] matrixuint10);
  public delegate void DOnNewFrameMatrixUInt12(UInt16[,] matrixuint12);
  public delegate void DOnNewFrameMatrixUInt16(UInt16[,] matrixuint16);
  // PaletteColor
  public delegate void DOnNewFramePalette256BitmapRgb(CPalette256 palette256, CBitmapRgb bitmap24bpp);
  public delegate void DOnNewFramePalette256BitmapArgb(CPalette256 palette256, CBitmapArgb bitmap32bpp);
  public delegate void DOnNewFramePalette256Bitmap256(CPalette256 palette256, CBitmap256 bitmap256);
  public delegate void DOnNewFramePalette256Matrix08Bit(CPalette256 palette256, CMatrix08Bit matrix08Bit);
  public delegate void DOnNewFramePalette256Matrix10Bit(CPalette256 palette256, CMatrix10Bit matrix10bit);
  public delegate void DOnNewFramePalette256Matrix12Bit(CPalette256 palette256, CMatrix12Bit matrix12bit);
  public delegate void DOnNewFramePalette256Matrix16Bit(CPalette256 palette256, CMatrix16Bit matrix16bit);
  public delegate void DOnNewFramePalette256MatrixByte(CPalette256 palette256, Byte[,] matrixbyte);
  public delegate void DOnNewFramePalette256MatrixUInt10(CPalette256 palette256, UInt16[,] matrixuint10);
  public delegate void DOnNewFramePalette256MatrixUInt12(CPalette256 palette256, UInt16[,] matrixuint12);
  public delegate void DOnNewFramePalette256MatrixUInt16(CPalette256 palette256, UInt16[,] matrixuint16);
  //
  //---------------------------------------------------------------------------
  //  Segment - GrabberData
  //---------------------------------------------------------------------------
  // 
  public struct RGrabberData
  {
    public String LibraryName;
    public String EntryName;
    public String CameraName;
    public String CameraIdentifier;
    public Int32 CountImagePreset;
    public Int32 CountImageActual;
    public Boolean TrueColorMode;
    public EPaletteKind PaletteKind;
    public DOnErrorGrabberHardware OnErrorGrabberHardware;
    public DOnInfoGrabberHardware OnInfoGrabberHardware;
    public DOnCameraStateChanged OnCameraStateChanged;
    // TrueColor
    public DOnNewFrameBitmapRgb OnNewFrameBitmapRgb;
    public DOnNewFrameBitmapArgb OnNewFrameBitmapArgb;
    public DOnNewFrameBitmap256 OnNewFrameBitmap256;
    public DOnNewFrameMatrix08Bit OnNewFrameMatrix08Bit;
    public DOnNewFrameMatrix10Bit OnNewFrameMatrix10Bit;
    public DOnNewFrameMatrix12Bit OnNewFrameMatrix12Bit;
    public DOnNewFrameMatrix16Bit OnNewFrameMatrix16Bit;
    public DOnNewFrameMatrixByte OnNewFrameMatrixByte;
    public DOnNewFrameMatrixUInt10 OnNewFrameMatrixUInt10;
    public DOnNewFrameMatrixUInt12 OnNewFrameMatrixUInt12;
    public DOnNewFrameMatrixUInt16 OnNewFrameMatrixUInt16;
    // PaletteColor
    public DOnNewFramePalette256BitmapRgb OnNewFramePalette256BitmapRgb;
    public DOnNewFramePalette256BitmapArgb OnNewFramePalette256BitmapArgb;
    public DOnNewFramePalette256Bitmap256 OnNewFramePalette256Bitmap256;
    public DOnNewFramePalette256Matrix08Bit OnNewFramePalette256Matrix08Bit;
    public DOnNewFramePalette256Matrix10Bit OnNewFramePalette256Matrix10Bit;
    public DOnNewFramePalette256Matrix12Bit OnNewFramePalette256Matrix12Bit;
    public DOnNewFramePalette256Matrix16Bit OnNewFramePalette256Matrix16Bit;
    public DOnNewFramePalette256MatrixByte OnNewFramePalette256MatrixByte;
    public DOnNewFramePalette256MatrixUInt10 OnNewFramePalette256MatrixUInt10;
    public DOnNewFramePalette256MatrixUInt12 OnNewFramePalette256MatrixUInt12;
    public DOnNewFramePalette256MatrixUInt16 OnNewFramePalette256MatrixUInt16;
    //
    public RGrabberData(Int32 init)
    {
      LibraryName = "";
      EntryName = "";
      CameraName = "";
      CameraIdentifier = "";
      CountImagePreset = 1;
      CountImageActual = 0;
      TrueColorMode = true;
      PaletteKind = EPaletteKind.GreyScale;
      OnErrorGrabberHardware = null;
      OnInfoGrabberHardware = null;
      OnCameraStateChanged = null;
      // TrueColor
      OnNewFrameBitmapRgb = null;
      OnNewFrameBitmapArgb = null;
      OnNewFrameBitmap256 = null;
      OnNewFrameMatrix08Bit = null;
      OnNewFrameMatrix10Bit = null;
      OnNewFrameMatrix12Bit = null;
      OnNewFrameMatrix16Bit = null;
      OnNewFrameMatrixByte = null;
      OnNewFrameMatrixUInt10 = null;
      OnNewFrameMatrixUInt12 = null;
      OnNewFrameMatrixUInt16 = null;
      // PaletteColor
      OnNewFramePalette256BitmapRgb = null;
      OnNewFramePalette256BitmapArgb = null;
      OnNewFramePalette256Bitmap256 = null;
      OnNewFramePalette256Matrix08Bit = null;
      OnNewFramePalette256Matrix10Bit = null;
      OnNewFramePalette256Matrix12Bit = null;
      OnNewFramePalette256Matrix16Bit = null;
      OnNewFramePalette256MatrixByte = null;
      OnNewFramePalette256MatrixUInt10 = null;
      OnNewFramePalette256MatrixUInt12 = null;
      OnNewFramePalette256MatrixUInt16 = null;
    }
  };
  //
  //---------------------------------------------------------------------------
  //  Segment - AcquisitionData
  //---------------------------------------------------------------------------
  // 
  public struct RAcquisitionData
  {
    public EImageFormat ImageFormat;
    public EPixelResolution PixelResolution;
    public EAcquisitionMode AcquisitionMode;
    //
    public RAcquisitionData(Int32 init)
    {
      ImageFormat = EImageFormat.Undefined;
      PixelResolution = EPixelResolution.Resolution08Bit;
      AcquisitionMode = EAcquisitionMode.None;
    }
  };
  //
  //---------------------------------------------------------------------------
  //  Segment - CGrabberBase - Basic-Class for ALL Hardware-Grabbers
  //---------------------------------------------------------------------------
  //
  public abstract class CGrabberHardware
  {
    protected RGrabberData FGrabberData;
    protected RAcquisitionData FAcquisitionData;

    public CGrabberHardware()
    {
      FGrabberData = new RGrabberData(0);
      FAcquisitionData = new RAcquisitionData(0);
    }

    public virtual void SetOnCameraStateChanged(DOnCameraStateChanged value)
    {
      FGrabberData.OnCameraStateChanged = value;
      if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
      {
        FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
      }
    }

    public String GetCameraName()
    {
      return FGrabberData.CameraName;
    }

    public abstract Boolean Open(DOnErrorGrabberHardware onerrorgrabberhardware,
                                 DOnInfoGrabberHardware oninfograbberhardware);
    public abstract Boolean Close();

    public abstract Boolean StartAcquisition();
    public abstract Boolean StopAcquisition();

    public abstract Boolean IsCameraOpen();
    public abstract Boolean IsCameraBusy();

    public abstract Boolean SetExposure(Int32 value);
    public abstract Boolean ShowCameraProperties(IntPtr hparent);

    public void SetOnErrorGrabberHardware(DOnErrorGrabberHardware value)
    {
      FGrabberData.OnErrorGrabberHardware = value;
    }

    public void SetOnInfoGrabberhardware(DOnInfoGrabberHardware value)
    {
      FGrabberData.OnInfoGrabberHardware = value;
    }

    public Boolean SetTrueColorMode(Boolean value)
    {
      FGrabberData.TrueColorMode = value;
      return true;
    }
    public Boolean GetTrueColorMode()
    {
      return FGrabberData.TrueColorMode;
    }

    public EPixelResolution GetPixelResolution()
    {
      return FAcquisitionData.PixelResolution;
    }

    public Boolean SetPaletteKind(EPaletteKind value)
    {
      FGrabberData.PaletteKind = value;
      return true;
    }
    public EPaletteKind GetPaletteKind()
    {
      return FGrabberData.PaletteKind;
    }
    //
    //--------------------------------------------------------------------------------
    // Callback - TrueColor
    //--------------------------------------------------------------------------------
    //
    public void SetOnNewFrameBitmapRgb(DOnNewFrameBitmapRgb value)
    {
      FGrabberData.OnNewFrameBitmapRgb = value;
    }

    public void SetOnNewFrameBitmapArgb(DOnNewFrameBitmapArgb value)
    {
      FGrabberData.OnNewFrameBitmapArgb = value;
    }

    public void SetOnNewFrameBitmap256(DOnNewFrameBitmap256 value)
    {
      FGrabberData.OnNewFrameBitmap256 = value;
    }

    public void SetOnNewFrameMatrix08Bit(DOnNewFrameMatrix08Bit value)
    {
      FGrabberData.OnNewFrameMatrix08Bit = value;
    }

    public void SetOnNewFrameMatrix10Bit(DOnNewFrameMatrix10Bit value)
    {
      FGrabberData.OnNewFrameMatrix10Bit = value;
    }

    public void SetOnNewFrameMatrix12Bit(DOnNewFrameMatrix12Bit value)
    {
      FGrabberData.OnNewFrameMatrix12Bit = value;
    }

    public void SetOnNewFrameMatrix16Bit(DOnNewFrameMatrix16Bit value)
    {
      FGrabberData.OnNewFrameMatrix16Bit = value;
    }

    public void SetOnNewFrameMatrixByte(DOnNewFrameMatrixByte value)
    {
      FGrabberData.OnNewFrameMatrixByte = value;
    }

    public void SetOnNewFrameMatrixUInt10(DOnNewFrameMatrixUInt10 value)
    {
      FGrabberData.OnNewFrameMatrixUInt10 = value;
    }

    public void SetOnNewFrameMatrixUInt12(DOnNewFrameMatrixUInt12 value)
    {
      FGrabberData.OnNewFrameMatrixUInt12 = value;
    }

    public void SetOnNewFrameMatrixUInt16(DOnNewFrameMatrixUInt16 value)
    {
      FGrabberData.OnNewFrameMatrixUInt16 = value;
    }
    //
    //--------------------------------------------------------------------------------
    // Callback - PaletteColor
    //--------------------------------------------------------------------------------
    //
    public void SetOnNewFramePalette256BitmapRgb(DOnNewFramePalette256BitmapRgb value)
    {
      FGrabberData.OnNewFramePalette256BitmapRgb = value;
    }

    public void SetOnNewFramePalette256BitmapArgb(DOnNewFramePalette256BitmapArgb value)
    {
      FGrabberData.OnNewFramePalette256BitmapArgb = value;
    }

    public void SetOnNewFramePalette256Bitmap256(DOnNewFramePalette256Bitmap256 value)
    {
      FGrabberData.OnNewFramePalette256Bitmap256 = value;
    }

    public void SetOnNewFramePalette256Matrix08Bit(DOnNewFramePalette256Matrix08Bit value)
    {
      FGrabberData.OnNewFramePalette256Matrix08Bit = value;
    }

    public void SetOnNewFramePalette256Matrix10Bit(DOnNewFramePalette256Matrix10Bit value)
    {
      FGrabberData.OnNewFramePalette256Matrix10Bit = value;
    }

    public void SetOnNewFramePalette256Matrix12Bit(DOnNewFramePalette256Matrix12Bit value)
    {
      FGrabberData.OnNewFramePalette256Matrix12Bit = value;
    }

    public void SetOnNewFramePalette256Matrix16Bit(DOnNewFramePalette256Matrix16Bit value)
    {
      FGrabberData.OnNewFramePalette256Matrix16Bit = value;
    }

    public void SetOnNewFramePalette256MatrixByte(DOnNewFramePalette256MatrixByte value)
    {
      FGrabberData.OnNewFramePalette256MatrixByte = value;
    }

    public void SetOnNewFramePalette256MatrixUInt10(DOnNewFramePalette256MatrixUInt10 value)
    {
      FGrabberData.OnNewFramePalette256MatrixUInt10 = value;
    }

    public void SetOnNewFramePalette256MatrixUInt12(DOnNewFramePalette256MatrixUInt12 value)
    {
      FGrabberData.OnNewFramePalette256MatrixUInt12 = value;
    }

    public void SetOnNewFramePalette256MatrixUInt16(DOnNewFramePalette256MatrixUInt16 value)
    {
      FGrabberData.OnNewFramePalette256MatrixUInt16 = value;
    }
    //
    //--------------------------------------------
    //	Section - Helper
    //--------------------------------------------
    //
    protected virtual void Error(String namelibrary, String namefunction, String texterror)
    {
      if (FGrabberData.OnErrorGrabberHardware is DOnErrorGrabberHardware)
      {
        FGrabberData.OnErrorGrabberHardware(namelibrary, namefunction, texterror);
      }
    }

    protected virtual void Info(String namelibrary, String textinfo)
    {
      if (FGrabberData.OnInfoGrabberHardware is DOnInfoGrabberHardware)
      {
        FGrabberData.OnInfoGrabberHardware(namelibrary, textinfo);
      }
    }

  }

}
