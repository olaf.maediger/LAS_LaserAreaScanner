﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using AForge.Video;
using AForge.Video.DirectShow;
//
using IPMemory;
using GrabberHardware;
//
namespace GHWebPhilipsSpc900Nc
{
  public class CGHWebPhilipsSpc900Nc : CGrabberHardware
  {
    //
    //--------------------------------------------
    //	Section - Constant
    //--------------------------------------------
    //    
    private const EPixelResolution PIXELRESOLUTION_08BIT = EPixelResolution.Resolution08Bit;
    //public const String CAMERA_NAME = "Philips SPC 900NC";
    //public const String ENTRY_NAME = "Web " + CAMERA_NAME;
    //public const String LIBRARY_NAME = "WebPhilipsSpc900Nc";
    //
    //--------------------------------------------
    //	Section - Field
    //--------------------------------------------
    //
    private VideoCaptureDevice FVideoCaptureDevice;
    private String FCameraMoniker;
    private Int32 FIndexResolutionFrameRate;
    private List<Int32>[] FCameraResolutionWidths;
    private List<Int32>[] FCameraResolutionHeights;
    private List<Int32>[] FCameraFrameRates;
    private Boolean FIsCameraBusy;
    //!!!!!!!!!!!!####private CBitmapBase FBitmap;
    //
    //--------------------------------------------
    //	Section - Constructor
    //--------------------------------------------
    //
    public CGHWebPhilipsSpc900Nc()
      : base()
    {
      Int32 CI = (int)CDefinition.ECameraIndex.PhilipsSpc900Nc;
      FGrabberData.LibraryName = CDefinition.LIBRARY_NAMES[CI];
      FGrabberData.EntryName = CDefinition.ENTRY_NAMES[CI];
      FGrabberData.CameraName = CDefinition.CAMERA_NAMES[CI];
      FAcquisitionData.PixelResolution = PIXELRESOLUTION_08BIT;
      FVideoCaptureDevice = null;
      FCameraMoniker = "";
      FCameraResolutionWidths = null;
      FCameraResolutionHeights = null;
      FCameraFrameRates = null;
      FIndexResolutionFrameRate = -1;
      FIsCameraBusy = false;
    }
    //
    //--------------------------------------------
    //	Section - Property
    //--------------------------------------------
    //
    public override Boolean IsCameraOpen()
    {
      return (FVideoCaptureDevice is VideoCaptureDevice);
    }

    public override Boolean IsCameraBusy()
    {
      return FIsCameraBusy;
    }
    //
    //--------------------------------------------
    //	Section - Callback
    //--------------------------------------------
    //
    private void OnNewFrame(object sender,
                            NewFrameEventArgs arguments)
    {
      try
      {
/*//!!!!!!!!!!!!####        if (FBitmap is CBitmapRgb)
        {
          Bitmap B = (Bitmap)arguments.Frame.Clone();
          ((CBitmapRgb)FBitmap).SetBitmap(B);
          if (FGrabberData.OnNewFrame is DOnNewFrame)
          {
            FGrabberData.OnNewFrame(FBitmap.GetBitmap());
          }
        }*/
      }
      catch (Exception)
      {
      }
    }
    //
    //--------------------------------------------
    //	Section - Management - Camera
    //--------------------------------------------
    //
    public override Boolean Open(DOnErrorGrabberHardware onerrorgrabberhardware,
                                 DOnInfoGrabberHardware oninfograbberhardware)
    {
      FGrabberData.OnErrorGrabberHardware = onerrorgrabberhardware;
      FGrabberData.OnInfoGrabberHardware = oninfograbberhardware;
      //
      if (IsCameraOpen())
      {
        return false;
      }
      // Refresh installed Usb-Cameras with Resolutions
      FilterInfoCollection FIC = new FilterInfoCollection(FilterCategory.VideoInputDevice);
      if (FIC is FilterInfoCollection)
      {
        List<String> CameraIdentifiers = new List<String>();
        List<String> CameraNames = new List<String>();
        for (Int32 II = 0; II < FIC.Count; II++)
        {
          FilterInfo FI = FIC[II];
          if (0 < FI.Name.Length)
          {
            CameraIdentifiers.Add(FI.MonikerString);
            CameraNames.Add(FI.Name);
          }
        }
        Int32 CountCameras = CameraNames.Count;
        if (0 < CountCameras)
        {
          List<Int32>[] CameraResolutionWidths = new List<Int32>[CountCameras];
          List<Int32>[] CameraResolutionHeights = new List<Int32>[CountCameras];
          List<Int32>[] CameraFrameRates = new List<Int32>[CountCameras];
          for (Int32 II = 0; II < CountCameras; II++)
          {
            VideoCaptureDevice VCD = new VideoCaptureDevice(CameraIdentifiers[II]);
            CameraResolutionWidths[II] = new List<Int32>();
            CameraResolutionHeights[II] = new List<Int32>();
            CameraFrameRates[II] = new List<Int32>();
            Int32 VCL = VCD.VideoCapabilities.Length;
            if (0 < VCL)
            {
              Int32 VRMaximumIndex = 0;
              Int32 VRWidthMaximum = 0;
              Int32 VRHeightMaximum = 0;
              for (Int32 VCI = 0; VCI < VCL; VCI++)
              {
                Int32 VRWidth = VCD.VideoCapabilities[VCI].FrameSize.Width;
                CameraResolutionWidths[II].Add(VRWidth);
                Int32 VRHeight = VCD.VideoCapabilities[VCI].FrameSize.Height;
                CameraResolutionHeights[II].Add(VRHeight);
                Int32 VRFrameRate = VCD.VideoCapabilities[VCI].AverageFrameRate;
                CameraFrameRates[II].Add(VRFrameRate);
                if ((VRWidthMaximum < VRWidth) && (VRHeightMaximum < VRHeight))
                {
                  VRWidthMaximum = VRWidth;
                  VRHeightMaximum = VRHeight;
                  VRMaximumIndex = VCI;
                }
              }
              // Check if this desired Camera is in refreshed list:
              for (Int32 CI = 0; CI < CountCameras; CI++)
              {
                if (FGrabberData.CameraName == CameraNames[CI])
                {
                  FCameraMoniker = CameraIdentifiers[CI];                  
                  FVideoCaptureDevice = new VideoCaptureDevice(FCameraMoniker);
                  if (FVideoCaptureDevice is VideoCaptureDevice)
                  {
                    FCameraResolutionWidths = CameraResolutionWidths;
                    FCameraResolutionHeights = CameraResolutionHeights;
                    FCameraFrameRates = CameraFrameRates;
                    // default highest Resolution / FrameRate:
                    FIndexResolutionFrameRate = VRMaximumIndex;
                    return true;
                  }
                }
              }
            }
          }
        }
      }
      return false;
    }

    public override Boolean Close()
    {
      if (IsCameraOpen())
      {
        StopAcquisition();
        FIsCameraBusy = false;
        FVideoCaptureDevice = null;
        FCameraMoniker = "";
        FCameraResolutionWidths = null;
        FCameraResolutionHeights = null;
        FCameraFrameRates = null;
        FIndexResolutionFrameRate = -1;
        return true;
      }
      return false;
    }

    public override Boolean StartAcquisition()//RAcquisitionData acquisitiondata)
    {
      try
      {
        if (IsCameraOpen())
        {
          if (!IsCameraBusy())
          {
            FVideoCaptureDevice.VideoResolution = FVideoCaptureDevice.VideoCapabilities[1];
            // RO FVideoCaptureDevice.VideoResolution.FrameRate;
//!!!!!!!!!!!!####            FBitmap = new CBitmapRgb(FVideoCaptureDevice.VideoResolution.FrameSize.Width,
            //!!!!!!!!!!!!####                                     FVideoCaptureDevice.VideoResolution.FrameSize.Height);
            FIsCameraBusy = true;
            FVideoCaptureDevice.NewFrame += new AForge.Video.NewFrameEventHandler(OnNewFrame);
            FVideoCaptureDevice.Start();
            return true; 
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean StopAcquisition()
    {
      try
      {
        if (IsCameraOpen())
        {
          if (IsCameraBusy())
          {
            FVideoCaptureDevice.SignalToStop();
            FIsCameraBusy = true;
            FVideoCaptureDevice.NewFrame -= new AForge.Video.NewFrameEventHandler(OnNewFrame);
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean SetExposure(Int32 value)
    {
     if (IsCameraOpen())
      { // Error!!!
        //FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Focus, value, CameraControlFlags.None);//.Manual);
        //FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Exposure, value, CameraControlFlags.None);//.Manual);
        //FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Zoom, value, CameraControlFlags.None);//.Manual);
        //FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Iris, value, CameraControlFlags.None);//.Manual);
        //FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Roll, value, CameraControlFlags.None);//.Manual);

        

        //
        //FVideoCaptureDevice.SetVideoProperty(VideoProcAmpProperty.Brightness,
        //    brightnessValue,
        //    VideoProcAmpFlags.Manual);

        //FVideoCaptureDevice.SetVideoProperty(
        //    VideoProcAmpProperty.Contrast,
        //    contrastValue,
        //    VideoProcAmpFlags.Manual);

        //FVideoCaptureDevice.SetVideoProperty(
        //    VideoProcAmpProperty.Saturation,
        //    saturationValue,
        //    VideoProcAmpFlags.Manual);

        //FVideoCaptureDevice.SetVideoProperty(
        //    VideoProcAmpProperty.Sharpness,
        //    sharpnessValue,
        //    VideoProcAmpFlags.Manual);

        //FVideoCaptureDevice.SetVideoProperty(
        //    VideoProcAmpProperty.WhiteBalance,
        //    whiteBalanceValue,
        //    VideoProcAmpFlags.Manual);

        //FVideoCaptureDevice.SetVideoProperty(
        //    VideoProcAmpProperty.BacklightCompensation,
        //    backlightCompensationValue,
        //    VideoProcAmpFlags.Manual);

        return true;
      }
      return false;
    }

    public override Boolean ShowCameraProperties(IntPtr hparent)
    {
      return false;
    }


  }
}
