﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using FlyCapture2Managed;
using FlyCapture2Managed.Gui;
//
using Task;
using IPPalette256;
using IPMemory;
using IPMatrix;
using IPBitmap;
using GrabberHardware;
//
namespace GHPointGreyFlea308Bit
{
  public class CGHPointGreyFlea308Bit : CGrabberHardware
  { //
    //--------------------------------------------
    //	Section - Constant
    //--------------------------------------------
    //    
    private const EPixelResolution PIXELRESOLUTION_08BIT = EPixelResolution.Resolution08Bit;
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------------------
    //
    private FlyCapture2Managed.Gui.CameraControlDialog FDialogCameraProperties;
    private ManagedCameraBase FCamera = null;
    private ManagedImage FImageRawData;
    private ManagedImage FImageBitmap;
    private CTask FTask;
    //
    //--------------------------------------------
    //	Section - Constructor
    //--------------------------------------------
    //
    public CGHPointGreyFlea308Bit()
      : base()
    {
      Int32 CI = (int)CDefinition.ECameraIndex.Flea308Bit;
      FGrabberData.LibraryName = CDefinition.LIBRARY_NAMES[CI];
      FGrabberData.EntryName = CDefinition.ENTRY_NAMES[CI];
      FGrabberData.CameraName = CDefinition.CAMERA_NAMES[CI];
      FAcquisitionData.PixelResolution = PIXELRESOLUTION_08BIT;
      FImageRawData = new ManagedImage();
      FImageBitmap = new ManagedImage();
      FDialogCameraProperties = new CameraControlDialog();
    }
    //
    //--------------------------------------------
    //	Section - Property
    //--------------------------------------------
    //
    public override Boolean IsCameraOpen()
    {
      return (FCamera is ManagedCamera);
    }

    public override Boolean IsCameraBusy()
    {
      return (FTask is CTask);
    }

    public override void SetOnCameraStateChanged(DOnCameraStateChanged value)
    {
      base.SetOnCameraStateChanged(value);
    }
    //
    //------------------------------------------------------------------------------
    //	Section - Callback - Acquisition - Bitmap <- Camera
    //------------------------------------------------------------------------------
    //
    private void OnNewFrame()
    {
      try
      {
        unsafe
        {          
          // NC Bitmap BS = (Bitmap)FImageBitmap.bitmap;//.Clone();
          Int32 BW = (Int32)FImageRawData.cols;// FImageBitmap.bitmap.Width;
          Int32 BH = (Int32)FImageRawData.rows;// FImageBitmap.bitmap.Height;
          Int32 VL = BW * BH;
          Byte[] VRD = new Byte[VL];
          Byte* PRD = (Byte*)FImageRawData.data; // RawData 8Bit!  
          for (Int32 VI = 0; VI < VL; VI++)
          {
            VRD[VI] = *PRD;
            PRD++;
          }
          //        
          if (FGrabberData.TrueColorMode)
          { //++++++++++++++++++++++++++++++++++++
            //  TrueColorMode
            //++++++++++++++++++++++++++++++++++++         
            // BS (no Palette) -> BitmapRgb
            if (FGrabberData.OnNewFrameBitmapRgb is DOnNewFrameBitmapRgb)
            { // Conversion BS -> BitmapRgb
              //CBitmapRgb BitmapRgb = new CBitmapRgb(BS);
              // OK Conversion PRD -> BitmapRgb
              CBitmapRgb BitmapRgb;
              CBVectorConversion.ConvertVectorByteToBitmapRgb(BW, BH, VRD, out BitmapRgb);
              FGrabberData.OnNewFrameBitmapRgb(BitmapRgb);
            }
            // BS (no Palette) -> BitmapArgb
            if (FGrabberData.OnNewFrameBitmapArgb is DOnNewFrameBitmapArgb)
            { // Conversion BS -> BitmapRgb
              //CBitmapArgb BitmapArgb = new CBitmapArgb(BS);
              // OK Conversion PRD -> Bitmap
              CBitmapArgb BitmapArgb;
              CBVectorConversion.ConvertVectorByteToBitmapArgb(BW, BH, VRD, out BitmapArgb);
              FGrabberData.OnNewFrameBitmapArgb(BitmapArgb);
            }
            // BS (no Palette) -> Bitmap256(with Palette)
            if (FGrabberData.OnNewFrameBitmap256 is DOnNewFrameBitmap256)
            { // Conversion BS -> BitmapRgb
              //CBitmap256 Bitmap256;
              //CPalette256 Palette256 = CPalette256.CreatePalette256(FGrabberData.PaletteKind);
              //CBitmapConversionPalette.ConvertBitmapPalette256ToBitmap256(BS, Palette256, out Bitmap256);
              // OK Conversion PRD -> Bitmap
              CPalette256 Palette256 = CPalette256.CreatePalette256(FGrabberData.PaletteKind);
              CBitmap256 Bitmap256;
              CBVectorConversion.ConvertVectorByteToBitmap256(BW, BH, Palette256, VRD, out Bitmap256);
              FGrabberData.OnNewFrameBitmap256(Bitmap256);
            }
            // BS (no Palette) -> Matrix08Bit
            if (FGrabberData.OnNewFrameMatrix08Bit is DOnNewFrameMatrix08Bit)
            { // Conversion BS -> BitmapRgb
              //CMatrix08Bit Matrix08Bit;
              //CBitmapConversion.ConvertBitmapToMatrix08Bit(BS, out Matrix08Bit);
              // Conversion PRD -> Bitmap
              CMatrix08Bit Matrix08Bit;
              CMVectorConversion.ConvertVectorByteToMatrix08Bit(BW, BH, VRD, out Matrix08Bit);
              FGrabberData.OnNewFrameMatrix08Bit(Matrix08Bit);
            }
            // BS (no Palette) -> Matrix10Bit
            if (FGrabberData.OnNewFrameMatrix10Bit is DOnNewFrameMatrix10Bit)
            { // Conversion BS -> BitmapRgb
              //CMatrix10Bit Matrix10Bit;
              //CBitmapConversion.ConvertBitmapToMatrix10Bit(BS, out Matrix10Bit);
              // Conversion PRD -> Bitmap
              CMatrix10Bit Matrix10Bit;
              CMVectorConversion.ConvertVectorByteToMatrix10Bit(BW, BH, VRD, out Matrix10Bit);
              FGrabberData.OnNewFrameMatrix10Bit(Matrix10Bit);
            }
            // BS (no Palette) -> Matrix12Bit
            if (FGrabberData.OnNewFrameMatrix12Bit is DOnNewFrameMatrix12Bit)
            { // Conversion BS -> BitmapRgb
              //CMatrix12Bit Matrix12Bit;
              //CBitmapConversion.ConvertBitmapToMatrix12Bit(BS, out Matrix12Bit);
              // Conversion PRD -> Bitmap
              CMatrix12Bit Matrix12Bit;
              CMVectorConversion.ConvertVectorByteToMatrix12Bit(BW, BH, VRD, out Matrix12Bit);
              FGrabberData.OnNewFrameMatrix12Bit(Matrix12Bit);
            }
            // BS (no Palette) -> Matrix16Bit
            if (FGrabberData.OnNewFrameMatrix16Bit is DOnNewFrameMatrix16Bit)
            { // Conversion BS -> BitmapRgb
              //CMatrix16Bit Matrix16Bit;
              //CBitmapConversion.ConvertBitmapToMatrix16Bit(BS, out Matrix16Bit);
              // Conversion PRD -> Bitmap
              CMatrix16Bit Matrix16Bit;
              CMVectorConversion.ConvertVectorByteToMatrix16Bit(BW, BH, VRD, out Matrix16Bit);
              FGrabberData.OnNewFrameMatrix16Bit(Matrix16Bit);
            }
            // BS (no Palette) -> MatrixByte
            if (FGrabberData.OnNewFrameMatrixByte is DOnNewFrameMatrixByte)
            { // Conversion BS -> BitmapRgb
              //Byte[,] MatrixByte;
              //CBitmapConversion.ConvertBitmapToMatrixByte(BS, out MatrixByte);
              // Conversion PRD -> Bitmap
              Byte[,] MatrixByte;
              CMVectorConversion.ConvertVectorByteToMatrixByte(BW, BH, VRD, out MatrixByte);
              FGrabberData.OnNewFrameMatrixByte(MatrixByte);
            }
            // BS (no Palette) -> MatrixUInt10
            if (FGrabberData.OnNewFrameMatrixUInt10 is DOnNewFrameMatrixUInt10)
            { // Conversion BS -> BitmapRgb
              //UInt16[,] MatrixUInt10;
              //CBitmapConversion.ConvertBitmapToMatrixUInt10(BS, out MatrixUInt10);
              // Conversion PRD -> Bitmap
              UInt16[,] MatrixUInt10;
              CMVectorConversion.ConvertVectorByteToMatrixUInt10(BW, BH, VRD, out MatrixUInt10);
              FGrabberData.OnNewFrameMatrixUInt10(MatrixUInt10);
            }
            // BS (no Palette) -> MatrixUInt12
            if (FGrabberData.OnNewFrameMatrixUInt12 is DOnNewFrameMatrixUInt12)
            { // Conversion BS -> BitmapRgb
              //UInt16[,] MatrixUInt12;
              //CBitmapConversion.ConvertBitmapToMatrixUInt12(BS, out MatrixUInt12);
              // Conversion PRD -> Bitmap
              UInt16[,] MatrixUInt12;
              CMVectorConversion.ConvertVectorByteToMatrixUInt12(BW, BH, VRD, out MatrixUInt12);
              FGrabberData.OnNewFrameMatrixUInt12(MatrixUInt12);
            }
            // BS (no Palette) -> MatrixUInt16
            if (FGrabberData.OnNewFrameMatrixUInt16 is DOnNewFrameMatrixUInt16)
            { // Conversion BS -> BitmapRgb
              //UInt16[,] MatrixUInt16;
              //CBitmapConversion.ConvertBitmapToMatrixUInt16(BS, out MatrixUInt16);
              // Conversion PRD -> Bitmap
              UInt16[,] MatrixUInt16;
              CMVectorConversion.ConvertVectorByteToMatrixUInt16(BW, BH, VRD, out MatrixUInt16);
              FGrabberData.OnNewFrameMatrixUInt16(MatrixUInt16);
            }
          }
          else
          { //++++++++++++++++++++++++++++++++++++
            //  PaletteColorMode
            //++++++++++++++++++++++++++++++++++++
            CPalette256 Palette256 = CPalette256.CreatePalette256(FGrabberData.PaletteKind);
            // BS + Palette256 -> BitmapRgb
            if (FGrabberData.OnNewFramePalette256BitmapRgb is DOnNewFramePalette256BitmapRgb)
            { // Conversion BS -> Bitmap
              //CBitmapRgb BitmapRgb;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToBitmapRgb(BS, Palette256, out BitmapRgb);
              // OK Conversion PRD -> Bitmap
              CBitmapRgb BitmapRgb;
              CBVectorConversionPalette.ConvertVectorBytePalette256ToBitmapRgb(BW, BH, VRD, Palette256, out BitmapRgb);
              FGrabberData.OnNewFramePalette256BitmapRgb(Palette256, BitmapRgb);
            }
            // BS + Palette256 -> BitmapArgb
            if (FGrabberData.OnNewFramePalette256BitmapArgb is DOnNewFramePalette256BitmapArgb)
            { // Conversion BS -> Bitmap
              //CBitmapArgb BitmapArgb;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToBitmapArgb(BS, Palette256, out BitmapArgb);
              // OK Conversion PRD -> Bitmap
              CBitmapArgb BitmapArgb;
              CBVectorConversionPalette.ConvertVectorBytePalette256ToBitmapArgb(BW, BH, VRD, Palette256, out BitmapArgb);
              FGrabberData.OnNewFramePalette256BitmapArgb(Palette256, BitmapArgb);
            }
            // BS + Palette256 -> Bitmap256
            if (FGrabberData.OnNewFrameBitmap256 is DOnNewFrameBitmap256)
            { // Conversion BS -> Bitmap
              //CBitmap256 Bitmap256;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToBitmap256(BS, Palette256, out Bitmap256);
              // OK Conversion PRD -> Bitmap
              CBitmap256 Bitmap256;
              CBVectorConversionPalette.ConvertVectorBytePalette256ToBitmap256(BW, BH, VRD, Palette256, out Bitmap256);
              FGrabberData.OnNewFramePalette256Bitmap256(Palette256, Bitmap256);
            } 
            // BS + Palette256 -> Matrix08Bit
            if (FGrabberData.OnNewFramePalette256Matrix08Bit is DOnNewFramePalette256Matrix08Bit)
            { // Conversion BS -> Matrix
              //CMatrix08Bit Matrix08Bit;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix08Bit(BS, Palette256, out Matrix08Bit);
              // Conversion PRD -> Matrix
              CMatrix08Bit Matrix08Bit;
              CMVectorConversionPalette.ConvertVectorBytePalette256ToMatrix08Bit(BW, BH, VRD, Palette256, out Matrix08Bit);
              FGrabberData.OnNewFramePalette256Matrix08Bit(Palette256, Matrix08Bit);
            }
            // BS + Palette256 -> Matrix10Bit
            if (FGrabberData.OnNewFramePalette256Matrix10Bit is DOnNewFramePalette256Matrix10Bit)
            { // Conversion BS -> Matrix
              //CMatrix10Bit Matrix10Bit;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix10Bit(BS, Palette256, out Matrix10Bit);
              // Conversion PRD -> Matrix
              CMatrix10Bit Matrix10Bit;
              CMVectorConversionPalette.ConvertVectorBytePalette256ToMatrix10Bit(BW, BH, Palette256, VRD, out Matrix10Bit);
              FGrabberData.OnNewFramePalette256Matrix10Bit(Palette256, Matrix10Bit);
            }
            // BS + Palette256 -> Matrix12Bit
            if (FGrabberData.OnNewFramePalette256Matrix12Bit is DOnNewFramePalette256Matrix12Bit)
            { // Conversion BS -> Matrix
              //CMatrix12Bit Matrix12Bit;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix12Bit(BS, Palette256, out Matrix12Bit);
              // Conversion PRD -> Matrix
              CMatrix12Bit Matrix12Bit;
              CMVectorConversionPalette.ConvertVectorBytePalette256ToMatrix12Bit(BW, BH, VRD, Palette256, out Matrix12Bit);
              FGrabberData.OnNewFramePalette256Matrix12Bit(Palette256, Matrix12Bit);
            }
            // BS + Palette256 -> Matrix16Bit
            if (FGrabberData.OnNewFramePalette256Matrix16Bit is DOnNewFramePalette256Matrix16Bit)
            { // Conversion BS -> Matrix
              //CMatrix16Bit Matrix16Bit;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix12Bit(BS, Palette256, out Matrix16Bit);
              // Conversion PRD -> Matrix
              CMatrix16Bit Matrix16Bit;
              CMVectorConversionPalette.ConvertVectorBytePalette256ToMatrix16Bit(BW, BH, VRD, Palette256, out Matrix16Bit);
              FGrabberData.OnNewFramePalette256Matrix16Bit(Palette256, Matrix16Bit);
            }
            // BS + Palette256 -> MatrixByte
            if (FGrabberData.OnNewFramePalette256MatrixByte is DOnNewFramePalette256MatrixByte)
            { // Conversion BS -> Matrix
              //Byte[,] MatrixByte;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixByte(BS, Palette256, out MatrixByte);
              // Conversion PRD -> Matrix
              Byte[,] MatrixByte;
              CMVectorConversionPalette.ConvertVectorBytePalette256ToMatrixByte(BW, BH, VRD, Palette256, out MatrixByte);
              FGrabberData.OnNewFramePalette256MatrixByte(Palette256, MatrixByte);
            }
            // BS + Palette256 -> MatrixUInt10
            if (FGrabberData.OnNewFramePalette256MatrixUInt10 is DOnNewFramePalette256MatrixUInt10)
            { // Conversion BS -> Matrix
              //UInt16[,] MatrixUInt10;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixUInt10(BS, Palette256, out MatrixUInt10);
              // Conversion PRD -> Matrix
              UInt16[,] MatrixUInt10;
              CMVectorConversionPalette.ConvertVectorBytePalette256ToMatrixUInt10(BW, BH, VRD, Palette256, out MatrixUInt10);
              FGrabberData.OnNewFramePalette256MatrixUInt10(Palette256, MatrixUInt10);
            }
            // BS + Palette256 -> MatrixUInt12
            if (FGrabberData.OnNewFramePalette256MatrixUInt12 is DOnNewFramePalette256MatrixUInt12)
            { // Conversion BS -> Matrix
              //UInt16[,] MatrixUInt12;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixUInt12(BS, Palette256, out MatrixUInt12);
              // Conversion PRD -> Matrix
              UInt16[,] MatrixUInt12;
              CMVectorConversionPalette.ConvertVectorBytePalette256ToMatrixUInt12(BW, BH, VRD, Palette256, out MatrixUInt12);
              FGrabberData.OnNewFramePalette256MatrixUInt12(Palette256, MatrixUInt12);
            }
            // BS + Palette256 -> MatrixUInt16
            if (FGrabberData.OnNewFramePalette256MatrixUInt16 is DOnNewFramePalette256MatrixUInt16)
            { // Conversion BS -> Matrix
              //UInt16[,] MatrixUInt16;
              //CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixUInt16(BS, Palette256, out MatrixUInt16);
              // Conversion PRD -> Matrix
              UInt16[,] MatrixUInt16;
              CMVectorConversionPalette.ConvertVectorBytePalette256ToMatrixUInt16(BW, BH, VRD, Palette256, out MatrixUInt16);
              FGrabberData.OnNewFramePalette256MatrixUInt16(Palette256, MatrixUInt16);
            }
          }
        } // unsafe
      }
      catch (Exception)
      {
        Error(FGrabberData.LibraryName, "OnNewFrame", "CopyBitmap");
      }
    }
    //
    //--------------------------------------------
    //	Section - Callback - Task
    //--------------------------------------------
    //
    private void GrabOnExecutionStart(RTaskData taskdata)
    {
    }
    private void GrabOnExecutionBusy(ref RTaskData taskdata)
    {
      while (!taskdata.IsAborted)
      {
        FCamera.RetrieveBuffer(FImageRawData); // 10Bit in 16Bit Format
        lock (this)
        {
          taskdata.Counter++;
          //FImageRawData.Convert(FlyCapture2Managed.PixelFormat.PixelFormatBgr, FImageBitmap); // BW-Bitmap 8Bit
          //Bitmap BS = (Bitmap)FImageBitmap.bitmap;//.Clone();
          OnNewFrame();
        }
      }
    }
    private void GrabOnExecutionEnd(RTaskData taskdata)
    { // never...
    }
    private void GrabOnExecutionAbort(RTaskData taskdata)
    {
    }
    //
    //--------------------------------------------
    //	Section - Management - Camera
    //--------------------------------------------
    //
    public override Boolean Open(DOnErrorGrabberHardware onerrorgrabberhardware,
                                 DOnInfoGrabberHardware oninfograbberhardware)
    {
      FGrabberData.OnErrorGrabberHardware = onerrorgrabberhardware;
      FGrabberData.OnInfoGrabberHardware = oninfograbberhardware;
      //
      if (IsCameraOpen())
      {
        Console.WriteLine("GHPointGreyFlea308Bit: Open - false");
        return false;
      }
      try
      {
        ManagedBusManager busMgr = new ManagedBusManager();
        ManagedPGRGuid MPGRID = busMgr.GetCameraFromIndex(0);
        FCamera = new ManagedCamera();
        FCamera.Connect(MPGRID);
        FDialogCameraProperties.Connect(FCamera);
        CameraInfo camInfo = FCamera.GetCameraInfo();
        Console.WriteLine("GHPointGreyFlea308Bit: Open - true");
        return true;
      }
      catch (Exception)
      {
        Console.WriteLine("Error: Failed to open GHPointGreyFlea310Bit!");
        return false;
      }
    }

    public override Boolean Close()
    {
      if (!IsCameraOpen())
      {
        Console.WriteLine("GHPointGreyFlea308Bit: Close - false");
        return false;
      }
      try
      {
        if (IsCameraBusy())
        {
          StopAcquisition();
        }
        FCamera.Disconnect();
        FCamera = null;
        Console.WriteLine("GHPointGreyFlea308Bit: Close - true");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return true;
      }
      catch (Exception)
      {
        Console.WriteLine("Error: Failed to close GHPointGreyFlea308Bit:!");
        return false;
      }
    }

    public override Boolean StartAcquisition()
    {
      try
      {
        if (IsCameraOpen())
        {
          if (!IsCameraBusy())
          {
            // Idle -> Busy
            ManagedCamera camera = (ManagedCamera)FCamera;
            Format7ImageSettings F7IS = new Format7ImageSettings();
            uint PacketSize = 1;
            float PercentSpeed = 1f;
            camera.GetFormat7Configuration(F7IS, ref PacketSize, ref PercentSpeed);
            // 08Bit -
            F7IS.pixelFormat = FlyCapture2Managed.PixelFormat.PixelFormatMono8;
            camera.SetFormat7Configuration(F7IS, PercentSpeed);
            //
            FCamera.StartCapture();
            //
            FTask = new CTask("Grab",
                              GrabOnExecutionStart, GrabOnExecutionBusy,
                              GrabOnExecutionEnd, GrabOnExecutionAbort);
            FTask.Start();
            Console.WriteLine("GHPointGreyFlea308Bit: StartAcquisition - true");
            if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
            {
              FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
            }
            return true;
          }
        }
        Console.WriteLine("GHPointGreyFlea308Bit: StartAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
      catch (Exception)
      {
        Console.WriteLine("GHPointGreyFlea308Bit: StartAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
    }

    public override Boolean StopAcquisition()
    {
      try
      {
        if (IsCameraOpen())
        {
          if (IsCameraBusy())
          { // Busy -> Idle
            FTask.Abort();
            FTask = null;
            FCamera.StopCapture();
            Console.WriteLine("GHPointGreyFlea308Bit: StopAcquisition - true");
            if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
            {
              FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        Console.WriteLine("GHPointGreyFlea308Bit:Failed to stop camera!");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
    }


    public override Boolean SetExposure(Int32 value)
    {
      //if (IsCameraOpen())
      //{
      //  return true;
      //}
      return false;
    }

    public override Boolean ShowCameraProperties(IntPtr hparent)
    {
      return false;
    }




  }
}
