﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
// ... using GHVisioSensVfuV024.dll
//
using IPMemory;
using GrabberHardware;
//
namespace GHVisioSensVfuV024
{
  public class CGHVisioSensVfuV024 : CGrabberHardware
  {
    //
    //--------------------------------------------
    //	Section - Constant
    //--------------------------------------------
    // 
    private const EPixelResolution PIXELRESOLUTION_08BIT = EPixelResolution.Resolution08Bit;
    //public const String CAMERA_NAME = "VFU-V024";
    //public const String ENTRY_NAME = "VisioSens " + CAMERA_NAME;
    //public const String LIBRARY_NAME = "VisioSensVfuV024";
    //
    //--------------------------------------------
    //	Section - Field
    //--------------------------------------------
    //    
    //
    //--------------------------------------------
    //	Section - Constructor
    //--------------------------------------------
    //
    public CGHVisioSensVfuV024()
      : base()
    {
      Int32 CI = (int)CDefinition.ECameraIndex.VfuV024;
      FGrabberData.LibraryName = CDefinition.LIBRARY_NAMES[CI];
      FGrabberData.EntryName = CDefinition.ENTRY_NAMES[CI];
      FGrabberData.CameraName = CDefinition.CAMERA_NAMES[CI];
      FAcquisitionData.PixelResolution = PIXELRESOLUTION_08BIT;
    }
    //
    //--------------------------------------------
    //	Section - Property
    //--------------------------------------------
    //
    public override Boolean IsCameraOpen()
    {
      return false;
    }

    public override Boolean IsCameraBusy()
    {
      return false;
    }
    //
    //--------------------------------------------
    //	Section - Management - Camera
    //--------------------------------------------
    //
    public override Boolean Open(DOnErrorGrabberHardware onerrorgrabberhardware,
                                 DOnInfoGrabberHardware oninfograbberhardware)
    {
      FGrabberData.OnErrorGrabberHardware = onerrorgrabberhardware;
      FGrabberData.OnInfoGrabberHardware = oninfograbberhardware;
      //
      Boolean Result = false;
      return Result;
    }

    public override Boolean Close()
    {
      Boolean Result = false;
      return Result;
    }

    public override Boolean StartAcquisition()//RAcquisitionData acquisitiondata)
    {
      Boolean Result = false;
      return Result;
    }

    public override Boolean StopAcquisition()
    {
      Boolean Result = false;
      return Result;
    }

    public override Boolean SetExposure(Int32 value)
    {
      return false;
    }

    public override Boolean ShowCameraProperties(IntPtr hparent)
    {
      return false;
    }


  }
}
