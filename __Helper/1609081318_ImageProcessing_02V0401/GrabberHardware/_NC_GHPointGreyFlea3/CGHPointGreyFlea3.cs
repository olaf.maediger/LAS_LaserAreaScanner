﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using FlyCapture2Managed;
using FlyCapture2Managed.Gui;
//
using Task;
using IPPalette256;
using IPMemory;
using IPMatrix;
using IPBitmap;
using GrabberHardware;
//
namespace GHPointGreyFlea3
{
  public class CGHPointGreyFlea3 : CGrabberHardware
  {
    //
    //--------------------------------------------
    //	Section - Constant
    //--------------------------------------------
    //    
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------------------
    //
    private FlyCapture2Managed.Gui.CameraControlDialog FDialogCameraProperties;
    private ManagedCameraBase FCamera = null;
    private ManagedImage FImageRawData;
    private ManagedImage FImageBitmap;
    private CTask FTask;
    //
    //--------------------------------------------
    //	Section - Constructor
    //--------------------------------------------
    //
    public CGHPointGreyFlea3()
      : base()
    {
      Int32 CI = (int)CDefinition.ECameraIndex.Flea3;
      FGrabberData.LibraryName = CDefinition.LIBRARY_NAMES[CI];
      FGrabberData.EntryName = CDefinition.ENTRY_NAMES[CI];
      FGrabberData.CameraName = CDefinition.CAMERA_NAMES[CI];
      FImageRawData = new ManagedImage();
      FImageBitmap = new ManagedImage();
      FDialogCameraProperties = new CameraControlDialog();
    }
    //
    //--------------------------------------------
    //	Section - Property
    //--------------------------------------------
    //
    public override Boolean IsCameraOpen()
    {
      return (FCamera is ManagedCamera);
    }

    public override Boolean IsCameraBusy()
    {
      return (FTask is CTask);
    }

    public override void SetOnCameraStateChanged(DOnCameraStateChanged value)
    {
      base.SetOnCameraStateChanged(value);
    }
    //
    //------------------------------------------------------------------------------
    //	Section - Callback - Acquisition - Bitmap <- Camera
    //------------------------------------------------------------------------------
    //
    private void OnNewFrame()
    {
      try
      {
        Bitmap BS = (Bitmap)FImageBitmap.bitmap.Clone();
        Int32 BW = BS.Width;
        Int32 BH = BS.Height;
        //        
        if (FGrabberData.TrueColorMode)
        { //++++++++++++++++++++++++++++++++++++
          //  TrueColorMode
          //++++++++++++++++++++++++++++++++++++         
          // BS (no Palette) -> BitmapRgb
          if (FGrabberData.OnNewFrameBitmapRgb is DOnNewFrameBitmapRgb)
          {
            CBitmapRgb BitmapRgb = new CBitmapRgb(BS);
            FGrabberData.OnNewFrameBitmapRgb(BitmapRgb);
          }
          // BS (no Palette) -> BitmapArgb
          if (FGrabberData.OnNewFrameBitmapArgb is DOnNewFrameBitmapArgb)
          {
            CBitmapArgb BitmapArgb = new CBitmapArgb(BS);
            FGrabberData.OnNewFrameBitmapArgb(BitmapArgb);
          }
          // BS (no Palette) -> Bitmap256(with Palette)
          if (FGrabberData.OnNewFrameBitmap256 is DOnNewFrameBitmap256)
          {
            CBitmap256 Bitmap256;
            CPalette256 Palette256 = CPalette256.CreatePalette256(FGrabberData.PaletteKind);
            CBitmapConversionPalette.ConvertBitmapPalette256ToBitmap256(BS, Palette256, out Bitmap256);
            FGrabberData.OnNewFrameBitmap256(Bitmap256);
          }
          // BS (no Palette) -> Matrix08Bit
          if (FGrabberData.OnNewFrameMatrix08Bit is DOnNewFrameMatrix08Bit)
          {
            CMatrix08Bit Matrix08Bit;
            CBitmapConversion.ConvertBitmapToMatrix08Bit(BS, out Matrix08Bit);
            FGrabberData.OnNewFrameMatrix08Bit(Matrix08Bit);
          }
          // BS (no Palette) -> Matrix10Bit
          if (FGrabberData.OnNewFrameMatrix10Bit is DOnNewFrameMatrix10Bit)
          {
            CMatrix10Bit Matrix10Bit;
            CBitmapConversion.ConvertBitmapToMatrix10Bit(BS, out Matrix10Bit);
            FGrabberData.OnNewFrameMatrix10Bit(Matrix10Bit);
          }
          // BS (no Palette) -> Matrix12Bit
          if (FGrabberData.OnNewFrameMatrix12Bit is DOnNewFrameMatrix12Bit)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmapConversion.ConvertBitmapToMatrix12Bit(BS, out Matrix12Bit);
            FGrabberData.OnNewFrameMatrix12Bit(Matrix12Bit);
          }
          // BS (no Palette) -> Matrix16Bit
          if (FGrabberData.OnNewFrameMatrix16Bit is DOnNewFrameMatrix16Bit)
          {
            CMatrix16Bit Matrix16Bit;
            CBitmapConversion.ConvertBitmapToMatrix16Bit(BS, out Matrix16Bit);
            FGrabberData.OnNewFrameMatrix16Bit(Matrix16Bit);
          }
          // BS (no Palette) -> MatrixByte
          if (FGrabberData.OnNewFrameMatrixByte is DOnNewFrameMatrixByte)
          {
            Byte[,] MatrixByte;
            CBitmapConversion.ConvertBitmapToMatrixByte(BS, out MatrixByte);
            FGrabberData.OnNewFrameMatrixByte(MatrixByte);
          }
          // BS (no Palette) -> MatrixUInt10
          if (FGrabberData.OnNewFrameMatrixUInt10 is DOnNewFrameMatrixUInt10)
          {
            UInt16[,] MatrixUInt10;
            CBitmapConversion.ConvertBitmapToMatrixUInt10(BS, out MatrixUInt10);
            FGrabberData.OnNewFrameMatrixUInt10(MatrixUInt10);
          }
          // BS (no Palette) -> MatrixUInt12
          if (FGrabberData.OnNewFrameMatrixUInt12 is DOnNewFrameMatrixUInt12)
          {
            UInt16[,] MatrixUInt12;
            CBitmapConversion.ConvertBitmapToMatrixUInt12(BS, out MatrixUInt12);
            FGrabberData.OnNewFrameMatrixUInt12(MatrixUInt12);
          }
          // BS (no Palette) -> MatrixUInt16
          if (FGrabberData.OnNewFrameMatrixUInt16 is DOnNewFrameMatrixUInt16)
          {
            UInt16[,] MatrixUInt16;
            CBitmapConversion.ConvertBitmapToMatrixUInt16(BS, out MatrixUInt16);
            FGrabberData.OnNewFrameMatrixUInt16(MatrixUInt16);
          }
        }
        else
        { //++++++++++++++++++++++++++++++++++++
          //  PaletteColorMode
          //++++++++++++++++++++++++++++++++++++
          CPalette256 Palette256 = CPalette256.CreatePalette256(FGrabberData.PaletteKind);
          // BS + Palette256 -> BitmapRgb
          if (FGrabberData.OnNewFramePalette256BitmapRgb is DOnNewFramePalette256BitmapRgb)
          {
            CBitmapRgb BitmapRgb;
            CBitmapConversionPalette.ConvertBitmapPalette256ToBitmapRgb(BS, Palette256, out BitmapRgb);
            FGrabberData.OnNewFramePalette256BitmapRgb(Palette256, BitmapRgb);
          }
          // BS + Palette256 -> BitmapArgb
          if (FGrabberData.OnNewFramePalette256BitmapArgb is DOnNewFramePalette256BitmapArgb)
          {
            CBitmapArgb BitmapArgb;
            CBitmapConversionPalette.ConvertBitmapPalette256ToBitmapArgb(BS, Palette256, out BitmapArgb);
            FGrabberData.OnNewFramePalette256BitmapArgb(Palette256, BitmapArgb);
          }
          // BS + Palette256 -> Bitmap256
          if (FGrabberData.OnNewFrameBitmap256 is DOnNewFrameBitmap256)
          {
            CBitmap256 Bitmap256;
            CBitmapConversionPalette.ConvertBitmapPalette256ToBitmap256(BS, Palette256, out Bitmap256);
            FGrabberData.OnNewFramePalette256Bitmap256(Palette256, Bitmap256);
          }
          // BS + Palette256 -> Matrix08Bit
          if (FGrabberData.OnNewFramePalette256Matrix08Bit is DOnNewFramePalette256Matrix08Bit)
          {
            CMatrix08Bit Matrix08Bit;
            CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix08Bit(BS, Palette256, out Matrix08Bit);
            FGrabberData.OnNewFramePalette256Matrix08Bit(Palette256, Matrix08Bit);
          }
          // BS + Palette256 -> Matrix10Bit
          if (FGrabberData.OnNewFramePalette256Matrix10Bit is DOnNewFramePalette256Matrix10Bit)
          {
            CMatrix10Bit Matrix10Bit;
            CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix10Bit(BS, Palette256, out Matrix10Bit);
            FGrabberData.OnNewFramePalette256Matrix10Bit(Palette256, Matrix10Bit);
          }
          // BS + Palette256 -> Matrix12Bit
          if (FGrabberData.OnNewFramePalette256Matrix12Bit is DOnNewFramePalette256Matrix12Bit)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix12Bit(BS, Palette256, out Matrix12Bit);
            FGrabberData.OnNewFramePalette256Matrix12Bit(Palette256, Matrix12Bit);
          }
          // BS + Palette256 -> Matrix16Bit
          if (FGrabberData.OnNewFramePalette256Matrix16Bit is DOnNewFramePalette256Matrix16Bit)
          {
            CMatrix16Bit Matrix16Bit;
            CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix16Bit(BS, Palette256, out Matrix16Bit);
            FGrabberData.OnNewFramePalette256Matrix16Bit(Palette256, Matrix16Bit);
          }
          // BS + Palette256 -> MatrixByte
          if (FGrabberData.OnNewFramePalette256MatrixByte is DOnNewFramePalette256MatrixByte)
          {
            Byte[,] MatrixByte;
            CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixByte(BS, Palette256, out MatrixByte);
            FGrabberData.OnNewFramePalette256MatrixByte(Palette256, MatrixByte);
          }
          // BS + Palette256 -> MatrixUInt10
          if (FGrabberData.OnNewFramePalette256MatrixUInt10 is DOnNewFramePalette256MatrixUInt10)
          {
            UInt16[,] MatrixUInt10;
            CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixUInt10(BS, Palette256, out MatrixUInt10);
            FGrabberData.OnNewFramePalette256MatrixUInt10(Palette256, MatrixUInt10);
          }
          // BS + Palette256 -> MatrixUInt12
          if (FGrabberData.OnNewFramePalette256MatrixUInt12 is DOnNewFramePalette256MatrixUInt12)
          {
            UInt16[,] MatrixUInt12;
            CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixUInt12(BS, Palette256, out MatrixUInt12);
            FGrabberData.OnNewFramePalette256MatrixUInt12(Palette256, MatrixUInt12);
          }
          // BS + Palette256 -> MatrixUInt16
          if (FGrabberData.OnNewFramePalette256MatrixUInt16 is DOnNewFramePalette256MatrixUInt16)
          {
            UInt16[,] MatrixUInt16;
            CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixUInt16(BS, Palette256, out MatrixUInt16);
            FGrabberData.OnNewFramePalette256MatrixUInt16(Palette256, MatrixUInt16);
          }
        }
      }
      catch (Exception)
      {
        Error(FGrabberData.LibraryName, "OnNewFrame", "CopyBitmap");
      }
    }
    //
    //--------------------------------------------
    //	Section - Callback - Task
    //--------------------------------------------
    //
    private void GrabOnExecutionStart(RTaskData taskdata)
    {
    }
    private void GrabOnExecutionBusy(ref RTaskData taskdata)
    {
      while (!taskdata.IsAborted)
      {
        FCamera.RetrieveBuffer(FImageRawData);
        lock (this)
        {
          taskdata.Counter++;
          FImageRawData.Convert(FlyCapture2Managed.PixelFormat.PixelFormatBgr, FImageBitmap);
          // 24bpp FImageRawData.Convert(PixelFormat.PixelFormatRgb, FImageBitmap);
          // 32bpp FImageRawData.Convert(PixelFormat.PixelFormatRgbu, FImageBitmap);
          OnNewFrame();
        }
      }
    }
    private void GrabOnExecutionEnd(RTaskData taskdata)
    { // never...
    }
    private void GrabOnExecutionAbort(RTaskData taskdata)
    {
    }
    //
    //--------------------------------------------
    //	Section - Management - Camera
    //--------------------------------------------
    //
    public override Boolean Open(DOnErrorGrabberHardware onerrorgrabberhardware,
                                 DOnInfoGrabberHardware oninfograbberhardware)
    {
      FGrabberData.OnErrorGrabberHardware = onerrorgrabberhardware;
      FGrabberData.OnInfoGrabberHardware = oninfograbberhardware;
      //
      if (IsCameraOpen())
      {
        Console.WriteLine("GHPointGreyFlea3: Open - false");
        return false;
      }
      try
      {
        ManagedBusManager busMgr = new ManagedBusManager();
        ManagedPGRGuid MPGRID = busMgr.GetCameraFromIndex(0);
        FCamera = new ManagedCamera();
        FCamera.Connect(MPGRID);
        FDialogCameraProperties.Connect(FCamera);
        CameraInfo camInfo = FCamera.GetCameraInfo();
        Console.WriteLine("GHPointGreyFlea3: Open - true");
        return true;
      }
      catch (Exception)
      {
        Console.WriteLine("Error: Failed to open GHPointGreyFlea3!");
        return false;
      }
    }

    public override Boolean Close()
    {
      if (!IsCameraOpen())
      {
        Console.WriteLine("GHPointGreyFlea3: Close - false");
        return false;
      }
      try
      {
        if (IsCameraBusy())
        {
          StopAcquisition();
        }
        FCamera.Disconnect();
        FCamera = null;
        Console.WriteLine("GHPointGreyFlea3: Close - true");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return true;
      }
      catch (Exception)
      {
        Console.WriteLine("Error: Failed to close GHPointGreyFlea3!");
        return false;
      }
    }

    public override Boolean StartAcquisition()
    {
      try
      {
        if (IsCameraOpen())
        {
          if (!IsCameraBusy())
          {
            // Idle -> Busy
            FCamera.StartCapture();
            //
            FTask = new CTask("Grab",
                              GrabOnExecutionStart, GrabOnExecutionBusy,
                              GrabOnExecutionEnd, GrabOnExecutionAbort);
            FTask.Start();
            Console.WriteLine("GHPointGreyFlea3: StartAcquisition - true");
            if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
            {
              FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
            }
            return true;
          }
        }
        Console.WriteLine("GHPointGreyFlea3: StartAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
      catch (Exception)
      {
        Console.WriteLine("GHPointGreyFlea3: StartAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
    }

    public override Boolean StopAcquisition()
    {
      try
      {
        if (IsCameraOpen())
        {
          if (IsCameraBusy())
          { // Busy -> Idle
            FTask.Abort();
            FTask = null;
            FCamera.StopCapture();
            Console.WriteLine("GHWebUsb20PcCamera: StopAcquisition - true");
            if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
            {
              FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        Console.WriteLine("Failed to stop camera!");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
    }


    public override Boolean SetExposure(Int32 value)
    {
      //if (IsCameraOpen())
      //{
      //  return true;
      //}
      return false;
    }

    public override Boolean ShowCameraProperties(IntPtr hparent)
    {
      return false;
    }




  }
}

