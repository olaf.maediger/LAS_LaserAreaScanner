﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using AForge.Video;
using AForge.Video.DirectShow;
//
// NC using Task;
using IPPalette256;
using IPMemory;
using IPMatrix;
using IPBitmap;
using GrabberHardware;
//
namespace GHWebDntDigitalMicroscope
{
  public class CGHWebDntDigitalMicroscope : CGrabberHardware
   {
    //
    //--------------------------------------------
    //	Section - Constant
    //--------------------------------------------
    //    
    private const EPixelResolution PIXELRESOLUTION_08BIT = EPixelResolution.Resolution08Bit;
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------------------
    //
    private VideoCaptureDevice FVideoCaptureDevice;
    private String FCameraMoniker;
    private Int32 FIndexResolutionFrameRate;
    private List<Int32>[] FCameraResolutionWidths;
    private List<Int32>[] FCameraResolutionHeights;
    private List<Int32>[] FCameraFrameRates;
    private Boolean FIsCameraBusy;
    //
    //--------------------------------------------
    //	Section - Constructor
    //--------------------------------------------
    //
    public CGHWebDntDigitalMicroscope()
      : base()
    {
      Int32 CI = (int)CDefinition.ECameraIndex.DntDigitalMicroscope;
      FGrabberData.LibraryName = CDefinition.LIBRARY_NAMES[CI];
      FGrabberData.EntryName = CDefinition.ENTRY_NAMES[CI];
      FGrabberData.CameraName = CDefinition.CAMERA_NAMES[CI];
      FAcquisitionData.PixelResolution = PIXELRESOLUTION_08BIT;
      FVideoCaptureDevice = null;
      FCameraMoniker = "";
      FCameraResolutionWidths = null;
      FCameraResolutionHeights = null;
      FCameraFrameRates = null;
      FIndexResolutionFrameRate = -1;
      FIsCameraBusy = false;
    }
    //
    //--------------------------------------------
    //	Section - Property
    //--------------------------------------------
    //
    public override Boolean IsCameraOpen()
    {
      return (FVideoCaptureDevice is VideoCaptureDevice);
    }

    public override Boolean IsCameraBusy()
    {
      return FIsCameraBusy;
    }
    //
    //------------------------------------------------------------------------------
    //	Section - Callback - Acquisition - Bitmap <- Camera
    //------------------------------------------------------------------------------
    //
    private void OnNewFrame(object sender,
                            NewFrameEventArgs arguments)
    {
      try
      {
        Bitmap BS = (Bitmap)arguments.Frame.Clone();
        Int32 BW = BS.Width;
        Int32 BH = BS.Height;
        //        
        if (FGrabberData.TrueColorMode)
        { //++++++++++++++++++++++++++++++++++++
          //  TrueColorMode
          //++++++++++++++++++++++++++++++++++++         
          // BS (no Palette) -> BitmapRgb
          if (FGrabberData.OnNewFrameBitmapRgb is DOnNewFrameBitmapRgb)
          {
            CBitmapRgb BitmapRgb = new CBitmapRgb(BS);
            FGrabberData.OnNewFrameBitmapRgb(BitmapRgb);
          }
          // BS (no Palette) -> BitmapArgb
          if (FGrabberData.OnNewFrameBitmapArgb is DOnNewFrameBitmapArgb)
          {
            CBitmapArgb BitmapArgb = new CBitmapArgb(BS);
            FGrabberData.OnNewFrameBitmapArgb(BitmapArgb);
          }
          // BS (no Palette) -> Bitmap256(with Palette)
          if (FGrabberData.OnNewFrameBitmap256 is DOnNewFrameBitmap256)
          {
            CBitmap256 Bitmap256;
            CPalette256 Palette256 = CPalette256.CreatePalette256(FGrabberData.PaletteKind);
            CBitmapConversionPalette.ConvertBitmapPalette256ToBitmap256(BS, Palette256, out Bitmap256);
            FGrabberData.OnNewFrameBitmap256(Bitmap256);
          }
          // BS (no Palette) -> Matrix08Bit
          if (FGrabberData.OnNewFrameMatrix08Bit is DOnNewFrameMatrix08Bit)
          {
            CMatrix08Bit Matrix08Bit;
            CBitmapConversion.ConvertBitmapToMatrix08Bit(BS, out Matrix08Bit);
            FGrabberData.OnNewFrameMatrix08Bit(Matrix08Bit);
          }
          // BS (no Palette) -> Matrix10Bit
          if (FGrabberData.OnNewFrameMatrix10Bit is DOnNewFrameMatrix10Bit)
          {
            CMatrix10Bit Matrix10Bit;
            CBitmapConversion.ConvertBitmapToMatrix10Bit(BS, out Matrix10Bit);
            FGrabberData.OnNewFrameMatrix10Bit(Matrix10Bit);
          }
          // BS (no Palette) -> Matrix12Bit
          if (FGrabberData.OnNewFrameMatrix12Bit is DOnNewFrameMatrix12Bit)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmapConversion.ConvertBitmapToMatrix12Bit(BS, out Matrix12Bit);
            FGrabberData.OnNewFrameMatrix12Bit(Matrix12Bit);
          }
          // BS (no Palette) -> Matrix16Bit
          if (FGrabberData.OnNewFrameMatrix16Bit is DOnNewFrameMatrix16Bit)
          {
            CMatrix16Bit Matrix16Bit;
            CBitmapConversion.ConvertBitmapToMatrix16Bit(BS, out Matrix16Bit);
            FGrabberData.OnNewFrameMatrix16Bit(Matrix16Bit);
          }
          // BS (no Palette) -> MatrixByte
          if (FGrabberData.OnNewFrameMatrixByte is DOnNewFrameMatrixByte)
          {
            Byte[,] MatrixByte;
            CBitmapConversion.ConvertBitmapToMatrixByte(BS, out MatrixByte);
            FGrabberData.OnNewFrameMatrixByte(MatrixByte);
          }
          // BS (no Palette) -> MatrixUInt10
          if (FGrabberData.OnNewFrameMatrixUInt10 is DOnNewFrameMatrixUInt10)
          {
            UInt16[,] MatrixUInt10;
            CBitmapConversion.ConvertBitmapToMatrixUInt10(BS, out MatrixUInt10);
            FGrabberData.OnNewFrameMatrixUInt10(MatrixUInt10);
          }
          // BS (no Palette) -> MatrixUInt12
          if (FGrabberData.OnNewFrameMatrixUInt12 is DOnNewFrameMatrixUInt12)
          {
            UInt16[,] MatrixUInt12;
            CBitmapConversion.ConvertBitmapToMatrixUInt12(BS, out MatrixUInt12);
            FGrabberData.OnNewFrameMatrixUInt12(MatrixUInt12);
          }
          // BS (no Palette) -> MatrixUInt16
          if (FGrabberData.OnNewFrameMatrixUInt16 is DOnNewFrameMatrixUInt16)
          {
            UInt16[,] MatrixUInt16;
            CBitmapConversion.ConvertBitmapToMatrixUInt16(BS, out MatrixUInt16);
            FGrabberData.OnNewFrameMatrixUInt16(MatrixUInt16);
          }
        }
          else
          { //++++++++++++++++++++++++++++++++++++
            //  PaletteColorMode
            //++++++++++++++++++++++++++++++++++++
            CPalette256 Palette256 = CPalette256.CreatePalette256(FGrabberData.PaletteKind);
            // BS + Palette256 -> BitmapRgb
            if (FGrabberData.OnNewFramePalette256BitmapRgb is DOnNewFramePalette256BitmapRgb)
            {
              CBitmapRgb BitmapRgb;
              CBitmapConversionPalette.ConvertBitmapPalette256ToBitmapRgb(BS, Palette256, out BitmapRgb);
              FGrabberData.OnNewFramePalette256BitmapRgb(Palette256, BitmapRgb);
            }
            // BS + Palette256 -> BitmapArgb
            if (FGrabberData.OnNewFramePalette256BitmapArgb is DOnNewFramePalette256BitmapArgb)
            {
              CBitmapArgb BitmapArgb;
              CBitmapConversionPalette.ConvertBitmapPalette256ToBitmapArgb(BS, Palette256, out BitmapArgb);
              FGrabberData.OnNewFramePalette256BitmapArgb(Palette256, BitmapArgb);
            }
            // BS + Palette256 -> Bitmap256
            if (FGrabberData.OnNewFrameBitmap256 is DOnNewFrameBitmap256)
            {
              CBitmap256 Bitmap256;
              CBitmapConversionPalette.ConvertBitmapPalette256ToBitmap256(BS, Palette256, out Bitmap256);
              FGrabberData.OnNewFramePalette256Bitmap256(Palette256, Bitmap256);
            }
            // BS + Palette256 -> Matrix08Bit
            if (FGrabberData.OnNewFramePalette256Matrix08Bit is DOnNewFramePalette256Matrix08Bit)
            {              
              CMatrix08Bit Matrix08Bit;
              CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix08Bit(BS, Palette256, out Matrix08Bit);
              FGrabberData.OnNewFramePalette256Matrix08Bit(Palette256, Matrix08Bit);
            }
            // BS + Palette256 -> Matrix10Bit
            if (FGrabberData.OnNewFramePalette256Matrix10Bit is DOnNewFramePalette256Matrix10Bit)
            {
              CMatrix10Bit Matrix10Bit;
              CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix10Bit(BS, Palette256, out Matrix10Bit);
              FGrabberData.OnNewFramePalette256Matrix10Bit(Palette256, Matrix10Bit);
            }
            // BS + Palette256 -> Matrix12Bit
            if (FGrabberData.OnNewFramePalette256Matrix12Bit is DOnNewFramePalette256Matrix12Bit)
            {
              CMatrix12Bit Matrix12Bit;
              CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix12Bit(BS, Palette256, out Matrix12Bit);
              FGrabberData.OnNewFramePalette256Matrix12Bit(Palette256, Matrix12Bit);
            }
            // BS + Palette256 -> Matrix16Bit
            if (FGrabberData.OnNewFramePalette256Matrix16Bit is DOnNewFramePalette256Matrix16Bit)
            {
              CMatrix16Bit Matrix16Bit;
              CBitmapConversionPalette.ConvertBitmapPalette256ToMatrix16Bit(BS, Palette256, out Matrix16Bit);
              FGrabberData.OnNewFramePalette256Matrix16Bit(Palette256, Matrix16Bit);
            }
            // BS + Palette256 -> MatrixByte
            if (FGrabberData.OnNewFramePalette256MatrixByte is DOnNewFramePalette256MatrixByte)
            {
              Byte[,] MatrixByte;
              CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixByte(BS, Palette256, out MatrixByte);
              FGrabberData.OnNewFramePalette256MatrixByte(Palette256, MatrixByte);
            }
            // BS + Palette256 -> MatrixUInt10
            if (FGrabberData.OnNewFramePalette256MatrixUInt10 is DOnNewFramePalette256MatrixUInt10)
            {
              UInt16[,] MatrixUInt10;
              CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixUInt10(BS, Palette256, out MatrixUInt10);
              FGrabberData.OnNewFramePalette256MatrixUInt10(Palette256, MatrixUInt10);
            }
            // BS + Palette256 -> MatrixUInt12
            if (FGrabberData.OnNewFramePalette256MatrixUInt12 is DOnNewFramePalette256MatrixUInt12)
            {
              UInt16[,] MatrixUInt12;
              CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixUInt12(BS, Palette256, out MatrixUInt12);
              FGrabberData.OnNewFramePalette256MatrixUInt12(Palette256, MatrixUInt12);
            }
            // BS + Palette256 -> MatrixUInt16
            if (FGrabberData.OnNewFramePalette256MatrixUInt16 is DOnNewFramePalette256MatrixUInt16)
            {
              UInt16[,] MatrixUInt16;
              CBitmapConversionPalette.ConvertBitmapPalette256ToMatrixUInt16(BS, Palette256, out MatrixUInt16);
              FGrabberData.OnNewFramePalette256MatrixUInt16(Palette256, MatrixUInt16);
            }
          }
      }
      catch (Exception)
      {
        Error(FGrabberData.LibraryName, "OnNewFrame", "CopyBitmap");
      }
    }


        //
        //if (FGrabberData.TrueColorMode)
        //{ // Bitmap24bpp -> BitmapRgb
        //  if (FGrabberData.OnNewFrameBitmapRgb is DOnNewFrameBitmapRgb)
        //  { 
        //    FGrabberData.OnNewFrameBitmapRgb(BS);
        //  }
        //  // Bitmap24bpp -> BitmapArgb
        //  if (FGrabberData.OnNewFrameBitmapArgb is DOnNewFrameBitmapArgb)
        //  { 
        //    CBitmapArgb B32 = new CBitmapArgb(BW, BH);
        //    B32.SetBitmap(BS);
        //    FGrabberData.OnNewFrameBitmapArgb(B32.GetBitmapArgb());
        //  }
        //  // Bitmap24bpp -> Matrix08
        //  if (FGrabberData.OnNewFrameMatrix08Bit is DOnNewFrameMatrix08Bit)
        //  { 
        //    Byte[,] Matrix08;
        //    if (CBitmap.CopyBitmap24bppToMatrix08Bit(BS, out Matrix08))
        //    {
        //      FGrabberData.OnNewFrameMatrix08Bit(Matrix08);
        //    }
        //  }
        //  // Bitmap24bpp -> Matrix10
        //  if (FGrabberData.OnNewFrameMatrix10Bit is DOnNewFrameMatrix10Bit)
        //  { 
        //    UInt16[,] Matrix10;
        //    if (CBitmap.CopyBitmap24bppToMatrix10bit(BS, out Matrix10))
        //    {
        //      FGrabberData.OnNewFrameMatrix10Bit(Matrix10);
        //    }
        //  }
        //}
        //else // PaletteColorMode
        //{
        //  CPalette256 Palette256 = CPalette256.CreatePalette256(FGrabberData.PaletteKind);
        //  // Bitmap24bpp + Palette256 -> Bitmap24bpp          
        //  if (FGrabberData.OnNewFramePalette256BitmapRgb is DOnNewFramePalette256BitmapRgb)
        //  {
        //    Bitmap Bitmap24bpp;
        //    if (CBitmap.CopyBitmap24bppPalette256ToBitmap24bpp(BS, Palette256, out Bitmap24bpp))
        //    {
        //      FGrabberData.OnNewFrameBitmapArgb(Bitmap24bpp);
        //    }
        //  }
        //  // Bitmap24bpp + Palette256 -> Bitmap32bpp
        //  if (FGrabberData.OnNewFramePalette256BitmapArgb is DOnNewFramePalette256BitmapArgb)
        //  {
        //    Bitmap Bitmap32bpp;
        //    if (CBitmap.CopyBitmap24bppPalette256ToBitmap32bpp(BS, Palette256, 0xFF, out Bitmap32bpp))
        //    {
        //      FGrabberData.OnNewFrameBitmapArgb(Bitmap32bpp);
        //    }
        //  }
        //  // Bitmap24bpp + Palette256 -> Matrix08
        //  if (FGrabberData.OnNewFramePalette256Matrix08Bit is DOnNewFramePalette256Matrix08Bit)
        //  { // Format24bppRgb
        //    Byte[,] Matrix08Bit;
        //    if (CBitmap.CopyBitmap24bppPalette256ToMatrix08Bit(BS, Palette256, out Matrix08Bit))
        //    {
        //      FGrabberData.OnNewFramePalette256Matrix08Bit(Palette256, Matrix08Bit);
        //    }
        //  }
        //  // Bitmap24bpp + Palette256 -> Matrix10
        //  if (FGrabberData.OnNewFramePalette256Matrix10Bit is DOnNewFramePalette256Matrix10Bit)
        //  { 
        //    UInt16[,] Matrix10bit;
        //    if (CBitmap.CopyBitmap24bppPalette256ToMatrix10bit(BS, Palette256, out Matrix10bit))
        //    {
        //      FGrabberData.OnNewFrameMatrix10Bit(Matrix10bit);
        //    }
        //  }
        //}
    //
    //--------------------------------------------
    //	Section - Management - Camera
    //--------------------------------------------
    //
    public override Boolean Open(DOnErrorGrabberHardware onerrorgrabberhardware,
                                 DOnInfoGrabberHardware oninfograbberhardware)
    {
      FGrabberData.OnErrorGrabberHardware = onerrorgrabberhardware;
      FGrabberData.OnInfoGrabberHardware = oninfograbberhardware;
      //
      if (IsCameraOpen())
      {
        Info(FGrabberData.LibraryName, "Open - false");
        return false;
      }
      // Refresh installed Usb-Cameras with Resolutions
      FilterInfoCollection FIC = new FilterInfoCollection(FilterCategory.VideoInputDevice);
      if (FIC is FilterInfoCollection)
      {
        List<String> CameraIdentifiers = new List<String>();
        List<String> CameraNames = new List<String>();
        for (Int32 II = 0; II < FIC.Count; II++)
        {
          FilterInfo FI = FIC[II];
          if (0 < FI.Name.Length)
          {
            CameraIdentifiers.Add(FI.MonikerString);
            // ERROR CameraNames.Add("Dnt " + FI.Name);
            CameraNames.Add(FI.Name);
          }
        }
        Int32 CountCameras = CameraNames.Count;
        if (0 < CountCameras)
        {
          List<Int32>[] CameraResolutionWidths = new List<Int32>[CountCameras];
          List<Int32>[] CameraResolutionHeights = new List<Int32>[CountCameras];
          List<Int32>[] CameraFrameRates = new List<Int32>[CountCameras];
          for (Int32 II = 0; II < CountCameras; II++)
          {
            VideoCaptureDevice VCD = new VideoCaptureDevice(CameraIdentifiers[II]);
            CameraResolutionWidths[II] = new List<Int32>();
            CameraResolutionHeights[II] = new List<Int32>();
            CameraFrameRates[II] = new List<Int32>();
            Int32 VCL = VCD.VideoCapabilities.Length;
            if (0 < VCL)
            {
              Int32 VRMaximumIndex = 0;
              Int32 VRWidthMaximum = 0;
              Int32 VRHeightMaximum = 0;
              for (Int32 VCI = 0; VCI < VCL; VCI++)
              {
                Int32 VRWidth = VCD.VideoCapabilities[VCI].FrameSize.Width;
                CameraResolutionWidths[II].Add(VRWidth);
                Int32 VRHeight = VCD.VideoCapabilities[VCI].FrameSize.Height;
                CameraResolutionHeights[II].Add(VRHeight);
                Int32 VRFrameRate = VCD.VideoCapabilities[VCI].AverageFrameRate;
                CameraFrameRates[II].Add(VRFrameRate);
                if ((VRWidthMaximum < VRWidth) && (VRHeightMaximum < VRHeight))
                {
                  VRWidthMaximum = VRWidth;
                  VRHeightMaximum = VRHeight;
                  VRMaximumIndex = VCI;
                }
              }
              // Check if this desired Camera is in refreshed list:
              for (Int32 CI = 0; CI < CountCameras; CI++)
              {
                if (FGrabberData.CameraName == CameraNames[CI])
                {
                  FCameraMoniker = CameraIdentifiers[CI];
                  FVideoCaptureDevice = new VideoCaptureDevice(FCameraMoniker);
                  if (FVideoCaptureDevice is VideoCaptureDevice)
                  {
                    FCameraResolutionWidths = CameraResolutionWidths;
                    FCameraResolutionHeights = CameraResolutionHeights;
                    FCameraFrameRates = CameraFrameRates;
                    // default highest Resolution / FrameRate:
                    FIndexResolutionFrameRate = VRMaximumIndex;
                    Info(FGrabberData.LibraryName, "Open - true");
                    if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
                    {
                      FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
                    }
                    return true;
                  }
                }
              }
            }
          }
        }
      }
      Info(FGrabberData.LibraryName, "Open - false");
      if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
      {
        FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
      }
      return false;
    }

    public override Boolean Close()
    {
      if (IsCameraOpen())
      {
        StopAcquisition();
        FIsCameraBusy = false;
        FVideoCaptureDevice = null;
        FCameraMoniker = "";
        FCameraResolutionWidths = null;
        FCameraResolutionHeights = null;
        FCameraFrameRates = null;
        FIndexResolutionFrameRate = -1;
        Info(FGrabberData.LibraryName, "Close - true");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return true;
      }
      Info(FGrabberData.LibraryName, "Close - false");
      if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
      {
        FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
      }
      return false;
    }

    public override Boolean StartAcquisition()//RAcquisitionData acquisitiondata)
    {
      try
      {
        if (IsCameraOpen())
        {
          if (!IsCameraBusy())
          {
            // debug FGrabberData.PaletteKind = EPaletteKind.RedSectionBright;
            FVideoCaptureDevice.VideoResolution = FVideoCaptureDevice.VideoCapabilities[0];
            // RO FVideoCaptureDevice.VideoResolution.FrameRate;
//!!!!!!!!!!!!####            FBitmap = new CBitmapRgb(FVideoCaptureDevice.VideoResolution.FrameSize.Width,
            //!!!!!!!!!!!!####FVideoCaptureDevice.VideoResolution.FrameSize.Height);
            FIsCameraBusy = true;
            FVideoCaptureDevice.NewFrame += new AForge.Video.NewFrameEventHandler(OnNewFrame);
            FVideoCaptureDevice.Start();
            Info(FGrabberData.LibraryName, "StartAcquisition - true");
            if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
            {
              FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
            }
            return true;
          }
        }
        Info(FGrabberData.LibraryName, "StartAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
      catch (Exception)
      {
        Info(FGrabberData.LibraryName, "StartAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
    }

    public override Boolean StopAcquisition()
    {
      try
      {
        if (IsCameraOpen())
        {
          if (IsCameraBusy())
          {
            FVideoCaptureDevice.SignalToStop();
            FIsCameraBusy = false;
            FVideoCaptureDevice.NewFrame -= new AForge.Video.NewFrameEventHandler(OnNewFrame);
            Info(FGrabberData.LibraryName, "StopAcquisition - true");
            if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
            {
              FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
            }
            return true;
          }
        }
        Info(FGrabberData.LibraryName, "StopAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
      catch (Exception)
      {
        Info(FGrabberData.LibraryName, "StopAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
    }


    public override Boolean SetExposure(Int32 value)
    {
      if (IsCameraOpen())
      {
        FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Focus, value, CameraControlFlags.Manual);
        FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Exposure, value, CameraControlFlags.Manual);
        FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Zoom, value, CameraControlFlags.Manual);
        FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Iris, value, CameraControlFlags.Manual);
        FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Roll, value, CameraControlFlags.Manual);
        //
        //FVideoCaptureDevice.SetVideoProperty(VideoProcAmpProperty.Brightness,
        //    brightnessValue,
        //    VideoProcAmpFlags.Manual);
        return true;
      }
      return false;
    }

    public override Boolean ShowCameraProperties(IntPtr hparent)
    {
      if (FVideoCaptureDevice is VideoCaptureDevice)
      {
        FVideoCaptureDevice.DisplayPropertyPage(hparent);
        return true;
      }
      return false;
    }


  }
}
