﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Runtime.InteropServices;
//
using GrabberHardware;
//
using IPPaletteBase;
using IPPalette256;
using IPMemory;
//
namespace GHCommon
{
  //
  //---------------------------------------------------------------------------
  //  Segment - CGrabberLibrary - Methods to share Grabber-Dlls
  //---------------------------------------------------------------------------
  //
  public class CGrabberLibrary
  {
    //
    //-----------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------
    //
    private String FNameLibrary = "";
    private String FNameClass = "";
    private Assembly FLibraryAssembly = null;
    private Object FLibraryObject = null;
    private Type FObjectType = null;
    //
    //-----------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------
    //
    public CGrabberLibrary(String namelibrary,
                           String nameclass)
    { // Load Dll and create DLL-Instance
      FNameLibrary = namelibrary;
      FNameClass = nameclass;
      String WorkingDirectory = Directory.GetCurrentDirectory();
      String FileEntry = WorkingDirectory + "\\" + FNameLibrary;
      FLibraryAssembly = Assembly.LoadFile(FileEntry);
      FLibraryObject = FLibraryAssembly.CreateInstance(FNameClass);
      if (FLibraryObject is Object)
      {
        FObjectType = FLibraryObject.GetType();
      }
    }
    //
    //-----------------------------------------------------------
    //  Segment - Public - Common
    //-----------------------------------------------------------
    //
    public String GetCameraName()
    {
      Object[] Parameters = null;
      return (String)ExecuteGetString("GetCameraName", Parameters);
    }

    public void SetOnCameraStateChanged(DOnCameraStateChanged value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnCameraStateChanged("SetOnCameraStateChanged", Parameters);
    }
    //
    //-----------------------------------------------------------
    //  Segment - Public - TrueColor
    //-----------------------------------------------------------
    //    
    public void SetOnNewFrameBitmapRgb(DOnNewFrameBitmapRgb value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameBitmapRgb", Parameters);
    }
    public void SetOnNewFrameBitmapArgb(DOnNewFrameBitmapArgb value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameBitmapArgb", Parameters);
    }
    public void SetOnNewFrameBitmap256(DOnNewFrameBitmap256 value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameBitmap256", Parameters);
    }
    public void SetOnNewFrameMatrix08Bit(DOnNewFrameMatrix08Bit value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameMatrix08Bit", Parameters);
    }
    public void SetOnNewFrameMatrix10Bit(DOnNewFrameMatrix10Bit value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameMatrix10Bit", Parameters);
    }
    public void SetOnNewFrameMatrix12Bit(DOnNewFrameMatrix12Bit value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameMatrix12Bit", Parameters);
    }
    public void SetOnNewFrameMatrix16Bit(DOnNewFrameMatrix16Bit value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameMatrix16Bit", Parameters);
    }
    public void SetOnNewFrameMatrixByte(DOnNewFrameMatrixByte value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameMatrixByte", Parameters);
    }
    public void SetOnNewFrameMatrixUInt10(DOnNewFrameMatrixUInt10 value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameMatrixUInt10", Parameters);
    }
    public void SetOnNewFrameMatrixUInt12(DOnNewFrameMatrixUInt12 value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameMatrixUInt12", Parameters);
    }
    public void SetOnNewFrameMatrixUInt16(DOnNewFrameMatrixUInt16 value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFrameMatrixUInt16", Parameters);
    }
    //
    //-----------------------------------------------------------
    //  Segment - Public - PaletteColor
    //-----------------------------------------------------------
    //    
    public void SetOnNewFramePalette256BitmapRgb(DOnNewFramePalette256BitmapRgb value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256BitmapRgb", Parameters);
    }
    public void SetOnNewFramePalette256BitmapArgb(DOnNewFramePalette256BitmapArgb value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256BitmapArgb", Parameters);
    }
    public void SetOnNewFramePalette256Bitmap256(DOnNewFramePalette256Bitmap256 value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256Bitmap256", Parameters);
    }
    public void SetOnNewFramePalette256Matrix08Bit(DOnNewFramePalette256Matrix08Bit value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256Matrix08Bit", Parameters);
    }
    public void SetOnNewFramePalette256Matrix10Bit(DOnNewFramePalette256Matrix10Bit value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256Matrix10Bit", Parameters);
    }
    public void SetOnNewFramePalette256Matrix12Bit(DOnNewFramePalette256Matrix12Bit value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256Matrix12Bit", Parameters);
    }
    public void SetOnNewFramePalette256Matrix16Bit(DOnNewFramePalette256Matrix16Bit value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256Matrix16Bit", Parameters);
    }
    public void SetOnNewFramePalette256MatrixByte(DOnNewFramePalette256MatrixByte value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256MatrixByte", Parameters);
    }
    public void SetOnNewFramePalette256MatrixUInt10(DOnNewFramePalette256MatrixUInt10 value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256MatrixUInt10", Parameters);
    }
    public void SetOnNewFramePalette256MatrixUInt12(DOnNewFramePalette256MatrixUInt12 value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256MatrixUInt12", Parameters);
    }
    public void SetOnNewFramePalette256MatrixUInt16(DOnNewFramePalette256MatrixUInt16 value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      ExecuteSetOnNewFrame("SetOnNewFramePalette256MatrixUInt16", Parameters);
    }
    //
    //-----------------------------------------------------------
    //  Segment - Helper
    //-----------------------------------------------------------
    //
    private Boolean Execute(String namefunction,
                            Object[] arguments)
    {
      try
      { // Load Method 
        MethodInfo MI = FObjectType.GetMethod(namefunction);
        if (MI is MethodInfo)
        { // Call Method with Parameter
          return (Boolean)MI.Invoke(FLibraryObject, arguments);
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private String ExecuteGetString(String namefunction,
                                    Object[] arguments) 
    {
      try
      { // Load Method 
        MethodInfo MI = FObjectType.GetMethod(namefunction);
        if (MI is MethodInfo)
        { // Call Method with Parameter
          return (String)MI.Invoke(FLibraryObject, arguments);
        }
        return "";
      }
      catch (Exception)
      {
        return "";
      }
    }


    private EPixelResolution ExecuteGetPixelResolution(String namefunction,
                                                       Object[] arguments)
    {
      try
      { // Load Method 
        MethodInfo MI = FObjectType.GetMethod(namefunction);
        if (MI is MethodInfo)
        { // Call Method with Parameter
          return (EPixelResolution)MI.Invoke(FLibraryObject, arguments);
        }
        return EPixelResolution.Resolution08Bit;
      }
      catch (Exception)
      {
        return EPixelResolution.Resolution08Bit;
      }
    }

    private EPaletteKind ExecuteGetPaletteKind(String namefunction,
                                               Object[] arguments)
    {
      try
      { // Load Method 
        MethodInfo MI = FObjectType.GetMethod(namefunction);
        if (MI is MethodInfo)
        { // Call Method with Parameter
          return (EPaletteKind)MI.Invoke(FLibraryObject, arguments);
        }
        return EPaletteKind.GreyScale;
      }
      catch (Exception)
      {
        return EPaletteKind.GreyScale;
      }
    }

    private Boolean ExecuteGetTrueColorMode(String namefunction,
                                            Object[] arguments)
    {
      try
      { // Load Method 
        MethodInfo MI = FObjectType.GetMethod(namefunction);
        if (MI is MethodInfo)
        { // Call Method with Parameter
          return (Boolean)MI.Invoke(FLibraryObject, arguments);
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private void ExecuteSetOnCameraStateChanged(String namefunction,
                                                Object[] arguments)
    {
      try
      { // Load Method 
        MethodInfo MI = FObjectType.GetMethod(namefunction);
        if (MI is MethodInfo)
        { // Call Method with Parameter
          MI.Invoke(FLibraryObject, arguments);
        }
      }
      catch (Exception)
      {
      }
    }

    private void ExecuteSetOnNewFrame(String namefunction,
                                      Object[] arguments)
    {
      try
      { // Load Method 
        MethodInfo MI = FObjectType.GetMethod(namefunction);
        if (MI is MethodInfo)
        { // Call Method with Parameter
          MI.Invoke(FLibraryObject, arguments);
        }
      }
      catch (Exception)
      {
      }
    }

    public Boolean IsCameraOpen()
    {
      Object[] Parameters = null;
      return (Boolean)Execute("IsCameraOpen", Parameters);
    }

    public Boolean IsCameraBusy()
    {
      Object[] Parameters = null;
      return (Boolean)Execute("IsCameraBusy", Parameters);
    }

    public Boolean SetExposure(Int32 value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      return (Boolean)Execute("SetExposure", Parameters);
    }

    public Boolean SetTrueColorMode(Boolean value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      return (Boolean)Execute("SetTrueColorMode", Parameters);
    }
    public Boolean GetTrueColorMode()
    {
      Object[] Parameters = null;
      return (Boolean)Execute("GetTrueColorMode", Parameters);
    }

    public EPixelResolution GetPixelResolution()
    {
      Object[] Parameters = null;
      return ExecuteGetPixelResolution("GetPixelResolution", Parameters);
    }

    public Boolean SetPaletteKind(EPaletteKind value)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = value;
      return (Boolean)Execute("SetPaletteKind", Parameters);
    }

    public EPaletteKind GetPaletteKind()
    {
      Object[] Parameters = null;
      return ExecuteGetPaletteKind("GetPaletteKind", Parameters);
    }
    //
    //-----------------------------------------------------------
    //  Segment - Public - Management
    //-----------------------------------------------------------
    //
    public Boolean Open(DOnErrorGrabberHardware onerrorgrabberhardware,
                        DOnInfoGrabberHardware oninfograbberhardware)
    {
      Object[] Parameters = new Object[2];
      Parameters[0] = onerrorgrabberhardware;
      Parameters[1] = oninfograbberhardware;
      return (Boolean)Execute("Open", Parameters);
    }

    public Boolean Close()
    {
      Object[] Parameters = null;
      return (Boolean)Execute("Close", Parameters);
    }

    public Boolean StartAcquisition()//RAcquisitionData acquisitiondata)
    {
      Object[] Parameters = null;// new Object[1];
      //Parameters[0] = acquisitiondata;
      return (Boolean)Execute("StartAcquisition", Parameters);
    }

    public Boolean StopAcquisition()
    {
      Object[] Parameters = null;
      return (Boolean)Execute("StopAcquisition", Parameters);
    }

    public Boolean ShowCameraProperties(IntPtr hparent)
    {
      Object[] Parameters = new Object[1];
      Parameters[0] = hparent;
      return (Boolean)Execute("ShowCameraProperties", Parameters);
    }

  }
}
