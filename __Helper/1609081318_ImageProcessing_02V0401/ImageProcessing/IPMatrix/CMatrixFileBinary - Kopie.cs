﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
//
namespace IPMatrix
{
  ////
  ////--------------------------------------------------
  ////	Types
  ////--------------------------------------------------
  ////
  //public struct RBinaryFileData
  //{
  //  public String Entry;
  //  public String Drive;
  //  public String Path;
  //  public String Name;
  //  public String Extension;
  //  //
  //  public RBinaryFileData(Int32 init)
  //  {
  //    Entry = CMatrixFileBinary.INIT_ENTRY;
  //    Drive = CMatrixFileBinary.INIT_DRIVE;
  //    Path = CMatrixFileBinary.INIT_PATH;
  //    Name = CMatrixFileBinary.INIT_HEADER;
  //    Extension = CMatrixFileBinary.INIT_EXTENSION;
  //  }
  //};
  //
  //public class CMatrixFileBinary
  //{
    ////	
    ////--------------------------------------------------
    ////	Section - Constant
    ////--------------------------------------------------
    ////
    //public static String HEADER_LIBRARY = "TextFile";
    //public static String SECTION_LIBRARY = HEADER_LIBRARY;
    ////
    //public const String INIT_DRIVESEPARATOR = ":";
    //public const String INIT_HEADERSEPARATOR = ".";
    //public const String INIT_DRIVE = "C";
    //public const String INIT_PATH = INIT_HEADERSEPARATOR;
    //public const String INIT_HEADER = "filename";
    //public const String INIT_EXTENSION = "txt";
    //public const String INIT_ENTRY = INIT_HEADER + INIT_HEADERSEPARATOR + INIT_EXTENSION;
    ////
    //public const String INIT_STRING = "";
    //public const Char INIT_CHAR = (Char)0x00;
    //public const Int16 INIT_INT16 = 0;
    //public const Int32 INIT_INT32 = 0;
    //public const Byte INIT_BYTE = 0x00;
    //public const UInt16 INIT_UINT10 = 0;
    //public const UInt16 INIT_UINT12 = 0;
    //public const UInt16 INIT_UINT16 = 0;
    //public const UInt32 INIT_UINT32 = 0;
    //public const Single INIT_SINGLE = 0;
    //public const Double INIT_DOUBLE = 0;
    //public const Decimal INIT_DECIMAL = 0;
    //public DateTime INIT_DATETIME = DateTime.MinValue;
    //public TimeSpan INIT_TIMESPAN = TimeSpan.MinValue;
    ////
    ////-----------------------------------------------------------
    ////  Section - Field
    ////-----------------------------------------------------------
    ////
    //private RBinaryFileData FData;
    //private BinaryWriter FBinaryWriter;
    //private BinaryReader FBinaryReader;
    ////
    ////--------------------------------------------------
    ////	Section - Constructor
    ////--------------------------------------------------
    ////
    //public CMatrixFileBinary()
    //{
    //  FData = new RBinaryFileData(0);
    //}
    ////
    ////--------------------------------------------------
    ////	Section - Get/SetData
    ////--------------------------------------------------
    ////
    //protected Boolean GetData(ref RBinaryFileData data)
    //{
    //  data = FData;
    //  return true;
    //}

    //protected Boolean SetData(RBinaryFileData data)
    //{
    //  FData = data;
    //  return true;
    //}
    ////
    ////--------------------------------------------------
    ////	Section - File-Management
    ////--------------------------------------------------
    ////
    //public Boolean OpenRead(String fileentry)
    //{
    //  if (!(FBinaryReader is BinaryReader))
    //  {
    //    if (File.Exists(fileentry))
    //    {
    //      FileStream FS = new FileStream(fileentry, FileMode.Open);
    //      if (FS is FileStream)
    //      {
    //        FBinaryReader = new BinaryReader(FS);
    //        return (FBinaryReader is BinaryReader);
    //      }
    //    }
    //  }
    //  return false;
    //}

    //public Boolean OpenWrite(String fileentry)
    //{
    //  if (!(FBinaryWriter is BinaryWriter))
    //  {
    //    FileStream FS = new FileStream(fileentry, FileMode.Create);
    //    if (FS is FileStream)
    //    {
    //      FBinaryWriter = new BinaryWriter(FS);
    //      return (FBinaryWriter is BinaryWriter);
    //    }
    //  }
    //  return false;
    //}

    //public Boolean Close()
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Close();
    //    FBinaryWriter = null;
    //  }
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    FBinaryReader.Close();
    //    FBinaryReader = null;
    //  }
    //  return true;
    //}
    ////
    ////--------------------------------------------------
    ////	Section - LowLevel-Write
    ////--------------------------------------------------
    ////
    //protected Boolean Write(String value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Write(value);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(Char value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Write(value);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(Byte value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Write(value);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(Int16 value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Write(value);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(UInt16 value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Write(value);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(Int32 value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Write(value);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(UInt32 value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Write(value);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(Single value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Write(value);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(Double value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Write(value);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(Decimal value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    FBinaryWriter.Write(value);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(DateTime value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    String Buffer = value.ToString();
    //    FBinaryWriter.Write(Buffer);
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Write(TimeSpan value)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    String Buffer = value.ToString();
    //    FBinaryWriter.Write(Buffer);
    //    return true;
    //  }
    //  return false;
    //}
    ////
    ////--------------------------------------------------
    ////	Segment - HighLevel - Write
    ////--------------------------------------------------
    ////
    //public Boolean Write(CMatrix08Bit matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 CC = matrix.ColCount;
    //    Int32 RC = matrix.RowCount;
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("08Bit");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix.GetValue08Bit(CI, RI));
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}

    //public Boolean Write(CMatrix10Bit matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 CC = matrix.ColCount;
    //    Int32 RC = matrix.RowCount;
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("10Bit");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix.GetValue10Bit(CI, RI));
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}

    //public Boolean Write(CMatrix12Bit matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 CC = matrix.ColCount;
    //    Int32 RC = matrix.RowCount;
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("12Bit");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix.GetValue12Bit(CI, RI));
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}
    ////
    ////------------------------------------------------------------
    ////
    //public Boolean Write(Byte[,] matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 CC = matrix.GetLength(0);
    //    Int32 RC = matrix.GetLength(1);
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("Byte");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix[CI, RI]);
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}

    //public Boolean Write(Int32[,] matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 RC = matrix.GetLength(0);
    //    Int32 CC = matrix.GetLength(1);
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("Int32");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix[CI, RI]);
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}

    //public Boolean Write(Double[,] matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 RC = matrix.GetLength(0);
    //    Int32 CC = matrix.GetLength(1);
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("Double");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix[CI, RI]);
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}

















    ////
    ////--------------------------------------------------
    ////	Segment - HighLevel - Write
    ////--------------------------------------------------
    ////
    //public Boolean WriteMatrix08Bit(CMatrix08Bit matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 CC = matrix.ColCount;
    //    Int32 RC = matrix.RowCount;
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("08Bit");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix.GetValue08Bit(CI, RI));
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}

    //public Boolean WriteMatrix10Bit(CMatrix10Bit matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 CC = matrix.ColCount;
    //    Int32 RC = matrix.RowCount;
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("10Bit");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix.GetValue10Bit(CI, RI));
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}

    //public Boolean WriteMatrix12Bit(CMatrix12Bit matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 CC = matrix.ColCount;
    //    Int32 RC = matrix.RowCount;
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("12Bit");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix.GetValue12Bit(CI, RI));
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}

    //public Boolean WriteMatrix16Bit(CMatrix16Bit matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 CC = matrix.ColCount;
    //    Int32 RC = matrix.RowCount;
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("16Bit");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix.GetValue16Bit(CI, RI));
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}
    ////
    ////------------------------------------------------------------
    ////
    //public Boolean WriteMatrixByte(Byte[,] matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 CC = matrix.GetLength(0);
    //    Int32 RC = matrix.GetLength(1);
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("Byte");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix[CI, RI]);
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}

    //public Boolean WriteMatrixUInt10(UInt16[,] matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 RC = matrix.GetLength(0);
    //    Int32 CC = matrix.GetLength(1);
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("UInt10");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix[CI, RI]);
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}

    //public Boolean WriteMatrixUInt12(UInt16[,] matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 RC = matrix.GetLength(0);
    //    Int32 CC = matrix.GetLength(1);
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("UInt12");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix[CI, RI]);
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}
    //public Boolean WriteMatrixUInt16(UInt16[,] matrix)
    //{
    //  if (FBinaryWriter is BinaryWriter)
    //  {
    //    Int32 RC = matrix.GetLength(0);
    //    Int32 CC = matrix.GetLength(1);
    //    FBinaryWriter.Write(CC);
    //    FBinaryWriter.Write(RC);
    //    FBinaryWriter.Write("UInt16");
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FBinaryWriter.Write(matrix[CI, RI]);
    //      }
    //    }
    //    return true;
    //  }
    //  return false;
    //}    
    ////
    ////--------------------------------------------------
    ////	Section - LowLevel-Read
    ////--------------------------------------------------
    ////
    //protected Boolean Read(out String value)
    //{
    //  value = INIT_STRING;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    value = FBinaryReader.ReadString();
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Read(out Char value)
    //{
    //  value = INIT_CHAR;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    value = FBinaryReader.ReadChar();
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Read(out Byte value)
    //{
    //  value = INIT_BYTE;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    value = FBinaryReader.ReadByte();
    //    return true;
    //  }
    //  return false;
    //}

    //public Boolean Read(out Int16 value)
    //{
    //  value = INIT_INT16;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    value = FBinaryReader.ReadInt16();
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Read(out UInt16 value)
    //{
    //  value = INIT_UINT16;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    value = FBinaryReader.ReadUInt16();
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Read(out Int32 value)
    //{
    //  value = INIT_INT32;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    value = FBinaryReader.ReadInt32();
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Read(out UInt32 value)
    //{
    //  value = INIT_UINT32;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    value = FBinaryReader.ReadUInt32();
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Read(out Single value)
    //{
    //  value = INIT_SINGLE;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    value = FBinaryReader.ReadSingle();
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Read(out Double value)
    //{
    //  value = INIT_DOUBLE;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    value = FBinaryReader.ReadDouble();
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Read(out Decimal value)
    //{
    //  value = INIT_DECIMAL;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    value = FBinaryReader.ReadDecimal();
    //    return true;
    //  }
    //  return false;
    //}

    ////protected Boolean Read(out DateTime value)
    ////{
    ////  value = INIT_DATETIME;
    ////  String Token;
    ////  if (ReadToken(out Token))
    ////  {
    ////    return DateTime.TryParse(Token, out value);
    ////  }
    ////  return false;
    ////}

    ////protected Boolean Read(out TimeSpan value)
    ////{
    ////  value = INIT_TIMESPAN;
    ////  String Token;
    ////  if (ReadToken(out Token))
    ////  {
    ////    return TimeSpan.TryParse(Token, out value);
    ////  }
    ////  return false;
    ////}

    ////protected Boolean Read(out Double[] values, out String[] tabbedtext)
    ////{
    ////  values = null;
    ////  tabbedtext = null;
    ////  String[] Tokens;
    ////  if (ReadTokensFromLine(out Tokens))
    ////  {
    ////    values = new Double[Tokens.Length];
    ////    for (Int32 TI = 0; TI < Tokens.Length; TI++)
    ////    {
    ////      //!!!!!!!!!!!!!!!!!!!!! debug String Token = Tokens[TI].Replace(',', '.');
    ////      String Token = Tokens[TI];
    ////      Double Value;
    ////      Token = Token.Replace(',', '.');
    ////      if (!Double.TryParse(Token, out Value))
    ////      {
    ////        values = null;
    ////        tabbedtext = Tokens;
    ////        return true;
    ////      }
    ////      values[TI] = Value;
    ////    }
    ////    return true;
    ////  }
    ////  return false;
    ////}


    ////private Boolean AnalyseDoubleValues(String[] tokens,
    ////                                    out Double[] doublevalues)
    ////{
    ////  doublevalues = new Double[tokens.Length];
    ////  for (Int32 TI = 0; TI < tokens.Length; TI++)
    ////  {
    ////    String Token = tokens[TI];
    ////    Token = Token.Replace(',', '.');
    ////    Double DValue;
    ////    if (!Double.TryParse(Token, out DValue))
    ////    {
    ////      doublevalues = null;
    ////      return false;
    ////    }
    ////    doublevalues[TI] = DValue;
    ////  }
    ////  return true;
    ////}


    ////private Boolean AnalyseTextValues(String[] tokens,
    ////                                  out String[] textvalues)
    ////{
    ////  textvalues = null;
    ////  if (0 < tokens.Length)
    ////  {
    ////    textvalues = new String[tokens.Length];
    ////    for (Int32 TI = 0; TI < tokens.Length; TI++)
    ////    {
    ////      String Token = tokens[TI];
    ////      textvalues[TI] = Token;
    ////    }
    ////    return true;
    ////  }
    ////  return false;
    ////}


    ////protected Boolean Read(out Double[][] values)
    ////{
    ////  values = null;
    ////  String[] Tokens;
    ////  while (ReadTokensFromLine(out Tokens))
    ////  {
    ////    Double[] Vector = new Double[Tokens.Length];
    ////    for (Int32 TI = 0; TI < Tokens.Length; TI++)
    ////    {
    ////      String Token = Tokens[TI].Replace(',', '.');
    ////      Double Value;
    ////      if (!Double.TryParse(Token, out Value))
    ////      {
    ////        values = null;
    ////        return true;
    ////      }
    ////      values[TI] = Vector;
    ////    }
    ////    return true;
    ////  }
    ////  return false;
    ////}
    ////
    ////--------------------------------------------------
    ////	Segment - HighLevel - Read
    ////--------------------------------------------------
    ////
    //public Boolean Read(out CMatrix08Bit matrix)
    //{
    //  matrix = null;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    Int32 CC, RC;
    //    String ElementType;
    //    Boolean Result = Read(out CC);
    //    Result &= Read(out RC);
    //    Result &= Read(out ElementType);
    //    if ("08Bit" == ElementType)
    //    {
    //      matrix = new CMatrix08Bit(CC, RC);
    //      for (Int32 RI = 0; RI < RC; RI++)
    //      {
    //        for (Int32 CI = 0; CI < CC; CI++)
    //        {
    //          Byte Value;
    //          Result &= Read(out Value);
    //          matrix.SetValue08Bit(CI, RI, Value);
    //        }
    //      }
    //      return Result;
    //    }
    //  }
    //  return false;
    //}
    //public Boolean Read(out CMatrix10Bit matrix)
    //{
    //  matrix = null;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    Int32 CC, RC;
    //    String ElementType;
    //    Boolean Result = Read(out CC);
    //    Result &= Read(out RC);
    //    Result &= Read(out ElementType);
    //    if ("10Bit" == ElementType)
    //    {
    //      matrix = new CMatrix10Bit(CC, RC);
    //      for (Int32 RI = 0; RI < RC; RI++)
    //      {
    //        for (Int32 CI = 0; CI < CC; CI++)
    //        {
    //          UInt16 Value;
    //          Result &= Read(out Value);
    //          matrix.SetValue10Bit(CI, RI, Value);
    //        }
    //      }
    //      return Result;
    //    }
    //  }
    //  return false;
    //}

    //public Boolean Read(out CMatrix12Bit matrix)
    //{
    //  matrix = null;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    Int32 CC, RC;
    //    String ElementType;
    //    Boolean Result = Read(out CC);
    //    Result &= Read(out RC);
    //    Result &= Read(out ElementType);
    //    if ("12Bit" == ElementType)
    //    {
    //      matrix = new CMatrix12Bit(CC, RC);
    //      for (Int32 RI = 0; RI < RC; RI++)
    //      {
    //        for (Int32 CI = 0; CI < CC; CI++)
    //        {
    //          UInt16 Value;
    //          Result &= Read(out Value);
    //          matrix.SetValue12Bit(CI, RI, Value);
    //        }
    //      }
    //      return Result;
    //    }
    //  }
    //  return false;
    //}

    //public Boolean Read(String fileentry, out CMatrixBase matrix)
    //{
    //  matrix = null;
    //  if (OpenRead(fileentry))
    //  {
    //    Int32 CC, RC;
    //    String ElementType;
    //    Boolean Result = Read(out CC);
    //    Result &= Read(out RC);
    //    Result &= Read(out ElementType);
    //    Close();
    //    switch (ElementType)
    //    {
    //      case "08Bit":
    //        this.OpenRead(fileentry);
    //        CMatrix08Bit Matrix08Bit;
    //        this.Read(out Matrix08Bit);
    //        Close();
    //        matrix = Matrix08Bit;
    //        break;
    //      case "10Bit":
    //        this.OpenRead(fileentry);
    //        CMatrix10Bit Matrix10Bit;
    //        this.Read(out Matrix10Bit);
    //        Close();
    //        matrix = Matrix10Bit;
    //        break;
    //      case "12Bit":
    //        this.OpenRead(fileentry);
    //        CMatrix12Bit Matrix12Bit;
    //        this.Read(out Matrix12Bit);
    //        Close();
    //        matrix = Matrix12Bit;
    //        break;
    //    }
    //    return true;
    //  }
    //  return false;
    //}    //
    ////------------------------------------------------------------
    ////
    //public Boolean Read(out Byte[,] matrix)
    //{
    //  matrix = null;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    UInt32 CC, RC;
    //    String ElementType;
    //    Boolean Result = Read(out CC);
    //    Result &= Read(out RC);
    //    Result &= Read(out ElementType);
    //    if ("Byte" == ElementType)
    //    {
    //      matrix = new Byte[CC, RC];
    //      for (Int32 RI = 0; RI < RC; RI++)
    //      {
    //        for (Int32 CI = 0; CI < CC; CI++)
    //        {
    //          Byte Value;
    //          Result &= Read(out Value);
    //          matrix[CI, RI] = Value;
    //        }
    //      }
    //      return Result;
    //    }
    //  }
    //  return false;
    //}

    //public Boolean Read(out Int32[,] matrix)
    //{
    //  matrix = null;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    UInt32 CC, RC;
    //    String ElementType;
    //    Boolean Result = Read(out CC);
    //    Result &= Read(out RC);
    //    Result &= Read(out ElementType);
    //    if ("UInt32" == ElementType)
    //    {
    //      matrix = new Int32[RC, CC];
    //      for (Int32 RI = 0; RI < RC; RI++)
    //      {
    //        for (Int32 CI = 0; CI < CC; CI++)
    //        {
    //          Int32 Value;
    //          Result &= Read(out Value);
    //          matrix[CI, RI] = Value;
    //        }
    //      }
    //      return Result;
    //    }
    //  }
    //  return false;
    //}

    //public Boolean Read(out Double[,] matrix)
    //{
    //  matrix = null;
    //  if (FBinaryReader is BinaryReader)
    //  {
    //    UInt32 CC, RC;
    //    String ElementType;
    //    Boolean Result = Read(out CC);
    //    Result &= Read(out RC);
    //    Result &= Read(out ElementType);
    //    if ("Double" == ElementType)
    //    {
    //      matrix = new Double[RC, CC];
    //      for (Int32 RI = 0; RI < RC; RI++)
    //      {
    //        for (Int32 CI = 0; CI < CC; CI++)
    //        {
    //          Double Value;
    //          Result &= Read(out Value);
    //          matrix[CI, RI] = Value;
    //        }
    //      }
    //      return Result;
    //    }
    //  }
    //  return false;
    //}

 // }
}
