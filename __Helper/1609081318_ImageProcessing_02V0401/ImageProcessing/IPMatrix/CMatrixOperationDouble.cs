﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMemory;
//
namespace IPMatrix
{
  public partial class CMatrixOperation
  {
    public static Boolean Add(CMatrixDouble operanda,
                              CMatrixDouble operandb,
                              out CMatrixDouble result)
    {
      result = null;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        Int32 CCB = operanda.ColCount;
        Int32 RCB = operanda.RowCount;
        result = new CMatrixDouble(CCA, RCA);
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            result.SetValueDouble(CIA, RIA,
                                 (Double)(operanda.GetValueDouble(CIA, RIA) + operandb.GetValueDouble(CIA, RIA)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Substract(CMatrixDouble operanda,
                                    CMatrixDouble operandb,
                                    out CMatrixDouble result)
    {
      result = null;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        result = new CMatrixDouble(CCA, RCA);
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            result.SetValueDouble(CIA, RIA,
                                 (Double)(operanda.GetValueDouble(CIA, RIA) - operandb.GetValueDouble(CIA, RIA)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Multiply(CMatrixDouble operanda,
                                   CMatrixDouble operandb,
                                   out CMatrixDouble result)
    {
      result = null;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        result = new CMatrixDouble(CCA, RCA);
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            result.SetValueDouble(CIA, RIA,
                                 (Double)(operanda.GetValueDouble(CIA, RIA) * operandb.GetValueDouble(CIA, RIA)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Multiply(CMatrixDouble operanda,
                                   Double operandb,
                                   out CMatrixDouble result)
    {
      result = null;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        result = new CMatrixDouble(CCA, RCA);
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            result.SetValueDouble(CIA, RIA,
                                 (Double)(operanda.GetValueDouble(CIA, RIA) * operandb));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Devide(CMatrixDouble operanda,
                                 Double operandb,
                                 out CMatrixDouble result)
    {
      result = null;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        result = new CMatrixDouble(CCA, RCA);
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            result.SetValueDouble(CIA, RIA,
                                 (Double)(operanda.GetValueDouble(CIA, RIA) / operandb));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean FindExtrema(CMatrixDouble operanda,
                                      out Double minimum,
                                      out Double maximum)
    {
      minimum = +1E10;
      maximum = -1E10;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            Double Value = operanda.GetValueDouble(CIA, RIA);
            if (Value < minimum)
            {
              minimum = Value;
            }
            if (maximum < Value)
            {
              maximum = Value;
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean FindExtrema(CMatrixDouble operanda,
                                      out Double minimum, out Int32 indexcolminimum, out Int32 indexrowminimum,
                                      out Double maximum, out Int32 indexcolmaximum, out Int32 indexrowmaximum)
    {
      minimum = +1E10;
      indexcolminimum = 0;
      indexrowminimum = 0;
      maximum = -1E10;
      indexcolmaximum = 0;
      indexrowmaximum = 0;
      try
      {
        Int32 CC = operanda.ColCount;
        Int32 RC = operanda.RowCount;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Double Value = operanda.GetValueDouble(CI, RI);
            if (Value < minimum)
            {
              minimum = Value;
              indexcolminimum = CI;
              indexrowminimum = RI;
            }
            if (maximum < Value)
            {
              maximum = Value;
              indexcolmaximum = CI;
              indexrowmaximum = RI;
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Norm(CMatrixDouble operand, 
                               Int32 zminimum, Int32 zmaximum,
                               out CMatrixDouble result)
    {
      result = null;
      try
      {
        Double Minimum, Maximum;
        if (FindExtrema(operand, out Minimum, out Maximum))
        {
          Int32 CC = operand.ColCount;
          Int32 RC = operand.RowCount;
          result = new CMatrixDouble(CC, RC);
          Double S = (zmaximum - zminimum) / (Maximum - Minimum);
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Double Value = operand.GetValueDouble(CI, RI);
              result.SetValueDouble(CI, RI, (Byte)(S * (Double)(Value - Minimum)));
            }
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }




  }
}
