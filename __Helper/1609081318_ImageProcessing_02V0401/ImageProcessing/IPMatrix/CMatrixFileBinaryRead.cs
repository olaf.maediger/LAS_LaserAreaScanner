﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
//
namespace IPMatrix
{
  //
  //--------------------------------------------------
  //	Types
  //--------------------------------------------------
  //
  public partial class CMatrixFileBinary
  {
    //
    //--------------------------------------------------
    //	Section - Helper
    //--------------------------------------------------
    //
    //private Boolean AnalyseDoubleValues(String[] tokens,
    //                                    out Double[] doublevalues)
    //{
    //  doublevalues = new Double[tokens.Length];
    //  for (Int32 TI = 0; TI < tokens.Length; TI++)
    //  {
    //    String Token = tokens[TI];
    //    Token = Token.Replace(',', '.');
    //    Double DValue;
    //    if (!Double.TryParse(Token, out DValue))
    //    {
    //      doublevalues = null;
    //      return false;
    //    }
    //    doublevalues[TI] = DValue;
    //  }
    //  return true;
    //}
    //
    //private Boolean AnalyseTextValues(String[] tokens,
    //                                  out String[] textvalues)
    //{
    //  textvalues = null;
    //  if (0 < tokens.Length)
    //  {
    //    textvalues = new String[tokens.Length];
    //    for (Int32 TI = 0; TI < tokens.Length; TI++)
    //    {
    //      String Token = tokens[TI];
    //      textvalues[TI] = Token;
    //    }
    //    return true;
    //  }
    //  return false;
    //}
    //
    //--------------------------------------------------
    //	Section - File-Management
    //--------------------------------------------------
    //
    public Boolean OpenRead(String fileentry)
    {
      if (!(FBinaryReader is BinaryReader))
      {
        if (File.Exists(fileentry))
        {
          FileStream FS = new FileStream(fileentry, FileMode.Open);
          if (FS is FileStream)
          {
            FBinaryReader = new BinaryReader(FS);
            return (FBinaryReader is BinaryReader);
          }
        }
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Section - LowLevel - Read
    //--------------------------------------------------
    //
    protected Boolean Read(out String value)
    {
      value = INIT_STRING;
      if (FBinaryReader is BinaryReader)
      {
        value = FBinaryReader.ReadString();
        return true;
      }
      return false;
    }

    protected Boolean Read(out Char value)
    {
      value = INIT_CHAR;
      if (FBinaryReader is BinaryReader)
      {
        value = FBinaryReader.ReadChar();
        return true;
      }
      return false;
    }

    protected Boolean Read(out Byte value)
    {
      value = INIT_BYTE;
      if (FBinaryReader is BinaryReader)
      {
        value = FBinaryReader.ReadByte();
        return true;
      }
      return false;
    }

    public Boolean Read(out Int16 value)
    {
      value = INIT_INT16;
      if (FBinaryReader is BinaryReader)
      {
        value = FBinaryReader.ReadInt16();
        return true;
      }
      return false;
    }

    protected Boolean Read(out UInt16 value)
    {
      value = INIT_UINT16;
      if (FBinaryReader is BinaryReader)
      {
        value = FBinaryReader.ReadUInt16();
        return true;
      }
      return false;
    }

    protected Boolean Read(out Int32 value)
    {
      value = INIT_INT32;
      if (FBinaryReader is BinaryReader)
      {
        value = FBinaryReader.ReadInt32();
        return true;
      }
      return false;
    }

    protected Boolean Read(out UInt32 value)
    {
      value = INIT_UINT32;
      if (FBinaryReader is BinaryReader)
      {
        value = FBinaryReader.ReadUInt32();
        return true;
      }
      return false;
    }

    protected Boolean Read(out Single value)
    {
      value = INIT_SINGLE;
      if (FBinaryReader is BinaryReader)
      {
        value = FBinaryReader.ReadSingle();
        return true;
      }
      return false;
    }

    protected Boolean Read(out Double value)
    {
      value = INIT_DOUBLE;
      if (FBinaryReader is BinaryReader)
      {
        value = FBinaryReader.ReadDouble();
        return true;
      }
      return false;
    }

    protected Boolean Read(out Decimal value)
    {
      value = INIT_DECIMAL;
      if (FBinaryReader is BinaryReader)
      {
        value = FBinaryReader.ReadDecimal();
        return true;
      }
      return false;
    }

    //protected Boolean Read(out DateTime value)
    //{
    //  value = INIT_DATETIME;
    //  String Token;
    //  if (ReadToken(out Token))
    //  {
    //    return DateTime.TryParse(Token, out value);
    //  }
    //  return false;
    //}

    //protected Boolean Read(out TimeSpan value)
    //{
    //  value = INIT_TIMESPAN;
    //  String Token;
    //  if (ReadToken(out Token))
    //  {
    //    return TimeSpan.TryParse(Token, out value);
    //  }
    //  return false;
    //}

    //protected Boolean Read(out Double[] values, out String[] tabbedtext)
    //{
    //  values = null;
    //  tabbedtext = null;
    //  String[] Tokens;
    //  if (ReadTokensFromLine(out Tokens))
    //  {
    //    values = new Double[Tokens.Length];
    //    for (Int32 TI = 0; TI < Tokens.Length; TI++)
    //    {
    //      //!!!!!!!!!!!!!!!!!!!!! debug String Token = Tokens[TI].Replace(',', '.');
    //      String Token = Tokens[TI];
    //      Double Value;
    //      Token = Token.Replace(',', '.');
    //      if (!Double.TryParse(Token, out Value))
    //      {
    //        values = null;
    //        tabbedtext = Tokens;
    //        return true;
    //      }
    //      values[TI] = Value;
    //    }
    //    return true;
    //  }
    //  return false;
    //}

    //protected Boolean Read(out Double[][] values)
    //{
    //  values = null;
    //  String[] Tokens;
    //  while (ReadTokensFromLine(out Tokens))
    //  {
    //    Double[] Vector = new Double[Tokens.Length];
    //    for (Int32 TI = 0; TI < Tokens.Length; TI++)
    //    {
    //      String Token = Tokens[TI].Replace(',', '.');
    //      Double Value;
    //      if (!Double.TryParse(Token, out Value))
    //      {
    //        values = null;
    //        return true;
    //      }
    //      values[TI] = Vector;
    //    }
    //    return true;
    //  }
    //  return false;
    //}
    //
    //--------------------------------------------------
    //	Segment - HighLevel - Read - Class-specific
    //--------------------------------------------------
    //
    public Boolean ReadMatrix08Bit(out CMatrix08Bit matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("08Bit" == ElementType)
        {
          matrix = new CMatrix08Bit(CC, RC);
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Byte Value;
              Result &= Read(out Value);
              matrix.SetValue08Bit(CI, RI, Value);
            }
          }
          if (FData.OnMatrix08BitRead is DOnMatrix08BitRead)
          {
            FData.OnMatrix08BitRead(matrix);
          }
          return Result;
        }
      }
      return false;
    }

    public Boolean ReadMatrix10Bit(out CMatrix10Bit matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("10Bit" == ElementType)
        {
          matrix = new CMatrix10Bit(CC, RC);
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              UInt16 Value;
              Result &= Read(out Value);
              matrix.SetValue10Bit(CI, RI, Value);
            }
          }
          if (FData.OnMatrix10BitRead is DOnMatrix10BitRead)
          {
            FData.OnMatrix10BitRead(matrix);
          }
          return Result;
        }
      }
      return false;
    }

    public Boolean ReadMatrix12Bit(out CMatrix12Bit matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("12Bit" == ElementType)
        {
          matrix = new CMatrix12Bit(CC, RC);
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              UInt16 Value;
              Result &= Read(out Value);
              matrix.SetValue12Bit(CI, RI, Value);
            }
          }
          if (FData.OnMatrix12BitRead is DOnMatrix12BitRead)
          {
            FData.OnMatrix12BitRead(matrix);
          }
          return Result;
        }
      }
      return false;
    }

    public Boolean ReadMatrix16Bit(out CMatrix16Bit matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("16Bit" == ElementType)
        {
          matrix = new CMatrix16Bit(CC, RC);
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              UInt16 Value;
              Result &= Read(out Value);
              matrix.SetValue16Bit(CI, RI, Value);
            }
          }
          if (FData.OnMatrix16BitRead is DOnMatrix16BitRead)
          {
            FData.OnMatrix16BitRead(matrix);
          }
          return Result;
        }
      }
      return false;
    }
    //
    //------------------------------------------------------------
    //
    public Boolean ReadMatrixByte(out Byte[,] matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        UInt32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("Byte" == ElementType)
        {
          matrix = new Byte[CC, RC];
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Byte Value;
              Result &= Read(out Value);
              matrix[CI, RI] = Value;
            }
          }
          if (FData.OnMatrixByteRead is DOnMatrixByteRead)
          {
            FData.OnMatrixByteRead(matrix);
          }
          return Result;
        }
      }
      return false;
    }

    public Boolean ReadMatrixUInt10(out UInt16[,] matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        UInt32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("UInt10" == ElementType)
        {
          matrix = new UInt16[CC, RC];
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Byte Value;
              Result &= Read(out Value);
              matrix[CI, RI] = Value;
            }
          }
          if (FData.OnMatrixUInt10Read is DOnMatrixUInt10Read)
          {
            FData.OnMatrixUInt10Read(matrix);
          }
          return Result;
        }
      }
      return false;
    }

    public Boolean ReadMatrixUInt12(out UInt16[,] matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        UInt32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("UInt12" == ElementType)
        {
          matrix = new UInt16[CC, RC];
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Byte Value;
              Result &= Read(out Value);
              matrix[CI, RI] = Value;
            }
          }
          if (FData.OnMatrixUInt12Read is DOnMatrixUInt12Read)
          {
            FData.OnMatrixUInt12Read(matrix);
          }
          return Result;
        }
      }
      return false;
    }

    public Boolean ReadMatrixUInt16(out UInt16[,] matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        UInt32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("UInt16" == ElementType)
        {
          matrix = new UInt16[CC, RC];
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Byte Value;
              Result &= Read(out Value);
              matrix[CI, RI] = Value;
            }
          }
          if (FData.OnMatrixUInt16Read is DOnMatrixUInt16Read)
          {
            FData.OnMatrixUInt16Read(matrix);
          }
          return Result;
        }
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Segment - HighLevel - Read - Class-independent
    //--------------------------------------------------
    //
    public Boolean Read(out CMatrix08Bit matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("08Bit" == ElementType)
        {
          matrix = new CMatrix08Bit(CC, RC);
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Byte Value;
              Result &= Read(out Value);
              matrix.SetValue08Bit(CI, RI, Value);
            }
          }
          if (FData.OnMatrix08BitRead is DOnMatrix08BitRead)
          {
            FData.OnMatrix08BitRead(matrix);
          }
          return Result;
        }
      }
      return false;
    }

    public Boolean Read(out CMatrix10Bit matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("10Bit" == ElementType)
        {
          matrix = new CMatrix10Bit(CC, RC);
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              UInt16 Value;
              Result &= Read(out Value);
              matrix.SetValue10Bit(CI, RI, Value);
            }
          }
          if (FData.OnMatrix10BitRead is DOnMatrix10BitRead)
          {
            FData.OnMatrix10BitRead(matrix);
          }
          return Result;
        }
      }
      return false;
    }

    public Boolean Read(out CMatrix12Bit matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("12Bit" == ElementType)
        {
          matrix = new CMatrix12Bit(CC, RC);
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              UInt16 Value;
              Result &= Read(out Value);
              matrix.SetValue12Bit(CI, RI, Value);
            }
          }
          if (FData.OnMatrix12BitRead is DOnMatrix12BitRead)
          {
            FData.OnMatrix12BitRead(matrix);
          }
          return Result;
        }
      }
      return false;
    }

    public Boolean Read(out CMatrix16Bit matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("16Bit" == ElementType)
        {
          matrix = new CMatrix16Bit(CC, RC);
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              UInt16 Value;
              Result &= Read(out Value);
              matrix.SetValue16Bit(CI, RI, Value);
            }
          }
          if (FData.OnMatrix16BitRead is DOnMatrix16BitRead)
          {
            FData.OnMatrix16BitRead(matrix);
          }
          return Result;
        }
      }
      return false;
    }
    //
    //------------------------------------------------------------
    //
    public Boolean Read(out Byte[,] matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        UInt32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("Byte" == ElementType)
        {
          matrix = new Byte[CC, RC];
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Byte Value;
              Result &= Read(out Value);
              matrix[CI, RI] = Value;
            }
          }
          if (FData.OnMatrixByteRead is DOnMatrixByteRead)
          {
            FData.OnMatrixByteRead(matrix);
          }
          return Result;
        }
      }
      return false;
    }

    ////// NO SENSE !!!!!!
    //////public Boolean Read(out UInt16[,] matrix)
    //////{
    //////  matrix = null;
    //////  if (FBinaryReader is BinaryReader)
    //////  {
    //////    UInt32 CC, RC;
    //////    String ElementType;
    //////    Boolean Result = Read(out CC);
    //////    Result &= Read(out RC);
    //////    Result &= Read(out ElementType);
    //////    if ("UInt10" == ElementType)
    //////    {
    //////      matrix = new UInt16[CC, RC];
    //////      for (Int32 RI = 0; RI < RC; RI++)
    //////      {
    //////        for (Int32 CI = 0; CI < CC; CI++)
    //////        {
    //////          Byte Value;
    //////          Result &= Read(out Value);
    //////          matrix[CI, RI] = Value;
    //////        }
    //////      }
    //////      if (FData.OnMatrixUInt10Read is DOnMatrixUInt10Read)
    //////      {
    //////        FData.OnMatrixUInt10Read(matrix);
    //////      }
    //////      return Result;
    //////    }
    //////  }
    //////  return false;
    //////}

    //////public Boolean Read(out UInt16[,] matrix)
    //////{
    //////  matrix = null;
    //////  if (FBinaryReader is BinaryReader)
    //////  {
    //////    UInt32 CC, RC;
    //////    String ElementType;
    //////    Boolean Result = Read(out CC);
    //////    Result &= Read(out RC);
    //////    Result &= Read(out ElementType);
    //////    if ("UInt12" == ElementType)
    //////    {
    //////      matrix = new UInt16[CC, RC];
    //////      for (Int32 RI = 0; RI < RC; RI++)
    //////      {
    //////        for (Int32 CI = 0; CI < CC; CI++)
    //////        {
    //////          Byte Value;
    //////          Result &= Read(out Value);
    //////          matrix[CI, RI] = Value;
    //////        }
    //////      }
    //////      if (FData.OnMatrixUInt12Read is DOnMatrixUInt12Read)
    //////      {
    //////        FData.OnMatrixUInt12Read(matrix);
    //////      }
    //////      return Result;
    //////    }
    //////  }
    //////  return false;
    //////}

    //////public Boolean Read(out UInt16[,] matrix)
    //////{
    //////  matrix = null;
    //////  if (FBinaryReader is BinaryReader)
    //////  {
    //////    UInt32 CC, RC;
    //////    String ElementType;
    //////    Boolean Result = Read(out CC);
    //////    Result &= Read(out RC);
    //////    Result &= Read(out ElementType);
    //////    if ("UInt16" == ElementType)
    //////    {
    //////      matrix = new UInt16[CC, RC];
    //////      for (Int32 RI = 0; RI < RC; RI++)
    //////      {
    //////        for (Int32 CI = 0; CI < CC; CI++)
    //////        {
    //////          Byte Value;
    //////          Result &= Read(out Value);
    //////          matrix[CI, RI] = Value;
    //////        }
    //////      }
    //////      if (FData.OnMatrixUInt16Read is DOnMatrixUInt16Read)
    //////      {
    //////        FData.OnMatrixUInt16Read(matrix);
    //////      }
    //////      return Result;
    //////    }
    //////  }
    //////  return false;
    //////}

    public Boolean Read(out Int32[,] matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        UInt32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("UInt32" == ElementType)
        {
          matrix = new Int32[RC, CC];
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Int32 Value;
              Result &= Read(out Value);
              matrix[CI, RI] = Value;
            }
          }
          //if (FData.OnMatrixUInt10Read is DOnMatrixUInt10Read)
          //{
          //  FData.OnMatrixUInt10Read(matrix);
          //}
          return Result;
        }
      }
      return false;
    }

    public Boolean Read(out Double[,] matrix)
    {
      matrix = null;
      if (FBinaryReader is BinaryReader)
      {
        UInt32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        if ("Double" == ElementType)
        {
          matrix = new Double[RC, CC];
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Double Value;
              Result &= Read(out Value);
              matrix[CI, RI] = Value;
            }
          }
          //if (FData.OnMatrixUInt10Read is DOnMatrixUInt10Read)
          //{
          //  FData.OnMatrixUInt10Read(matrix);
          //}
          return Result;
        }
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Segment - HighLevel - ReadFromFile
    //--------------------------------------------------
    //
    public Boolean ReadFromFile(String fileentry, out CMatrixBase matrix)
    {
      matrix = null;
      if (OpenRead(fileentry))
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        Close();
        switch (ElementType)
        {
          case "08Bit":
            this.OpenRead(fileentry);
            CMatrix08Bit Matrix08Bit;
            this.ReadMatrix08Bit(out Matrix08Bit);
            Close();
            matrix = Matrix08Bit;
            return (matrix is CMatrix08Bit);
          case "10Bit":
            this.OpenRead(fileentry);
            CMatrix10Bit Matrix10Bit;
            this.ReadMatrix10Bit(out Matrix10Bit);
            Close();
            matrix = Matrix10Bit;
            return (matrix is CMatrix10Bit);
          case "12Bit":
            this.OpenRead(fileentry);
            CMatrix12Bit Matrix12Bit;
            this.ReadMatrix12Bit(out Matrix12Bit);
            Close();
            matrix = Matrix12Bit;
            return (matrix is CMatrix12Bit);
          case "16Bit":
            this.OpenRead(fileentry);
            CMatrix16Bit Matrix16Bit;
            this.ReadMatrix16Bit(out Matrix16Bit);
            Close();
            matrix = Matrix16Bit;
            return (matrix is CMatrix16Bit);
        }
      }
      return false;
    }

    public Boolean ReadFromFile(String fileentry, out Byte[,] matrix)
    {
      matrix = null;
      if (OpenRead(fileentry))
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        Close();
        if ("Byte" == ElementType)
        {
          this.OpenRead(fileentry);
          Byte[,] MatrixByte;
          this.ReadMatrixByte(out MatrixByte);
          Close();
          matrix = MatrixByte;
          return (matrix is Byte[,]);
        }
      }
      return false;
    }

    public Boolean ReadFromFile(String fileentry, out UInt16[,] matrix)
    {
      matrix = null;
      if (OpenRead(fileentry))
      {
        Int32 CC, RC;
        String ElementType;
        Boolean Result = Read(out CC);
        Result &= Read(out RC);
        Result &= Read(out ElementType);
        Close();
        switch (ElementType)
        {
          case "UInt10":
            this.OpenRead(fileentry);
            UInt16[,] MatrixUInt10;
            this.ReadMatrixUInt10(out MatrixUInt10);
            Close();
            matrix = MatrixUInt10;
            return (matrix is UInt16[,]);
          case "UInt12":
            this.OpenRead(fileentry);
            UInt16[,] MatrixUInt12;
            this.ReadMatrixUInt12(out MatrixUInt12);
            Close();
            matrix = MatrixUInt12;
            return (matrix is UInt16[,]);
          case "UInt16":
            this.OpenRead(fileentry);
            UInt16[,] MatrixUInt16;
            this.ReadMatrixUInt16(out MatrixUInt16);
            Close();
            matrix = MatrixUInt16;
            return (matrix is UInt16[,]);
        }
      }
      return false;
    }


  }
}
