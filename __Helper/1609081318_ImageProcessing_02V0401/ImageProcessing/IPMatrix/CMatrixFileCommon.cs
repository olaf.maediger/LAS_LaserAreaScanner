﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
//
namespace IPMatrix
{
  //
  //--------------------------------------------------
  //	Types
  //--------------------------------------------------
  //
  public delegate void DOnMatrix08BitRead(CMatrix08Bit matrix);
  public delegate void DOnMatrix10BitRead(CMatrix10Bit matrix);
  public delegate void DOnMatrix12BitRead(CMatrix12Bit matrix);
  public delegate void DOnMatrix16BitRead(CMatrix16Bit matrix);
  public delegate void DOnMatrixByteRead(Byte[,] matrix);
  public delegate void DOnMatrixUInt10Read(UInt16[,] matrix);
  public delegate void DOnMatrixUInt12Read(UInt16[,] matrix);
  public delegate void DOnMatrixUInt16Read(UInt16[,] matrix);
  //
  public delegate void DOnMatrix08BitWrite(CMatrix08Bit matrix);
  public delegate void DOnMatrix10BitWrite(CMatrix10Bit matrix);
  public delegate void DOnMatrix12BitWrite(CMatrix12Bit matrix);
  public delegate void DOnMatrix16BitWrite(CMatrix16Bit matrix);
  public delegate void DOnMatrixByteWrite(Byte[,] matrix);
  public delegate void DOnMatrixUInt10Write(UInt16[,] matrix);
  public delegate void DOnMatrixUInt12Write(UInt16[,] matrix);
  public delegate void DOnMatrixUInt16Write(UInt16[,] matrix);

}
