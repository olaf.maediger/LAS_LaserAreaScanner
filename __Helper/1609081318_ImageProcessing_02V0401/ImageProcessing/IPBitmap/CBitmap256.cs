﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPaletteBase;
using IPPalette256;
using IPMatrix;
//
namespace IPBitmap
{
  public class CBitmap256 : CBitmapBase
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    protected CPalette256 FPalette256;
    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmap256(Int32 width,
                      Int32 height,
                      EPaletteKind palettekind)
      : base(width, height, PixelFormat.Format32bppArgb)
    {
      FPalette256 = CPalette256.CreatePalette256(palettekind);
    }

    public CBitmap256(Int32 width,
                      Int32 height,
                      CPalette256 palette256)
      : base(width, height, PixelFormat.Format32bppArgb)
    {
      FPalette256 = palette256;
    }

    public CBitmap256(EPaletteKind palettekind,
                      CMatrix08Bit matrix)
      : base(matrix.ColCount, matrix.RowCount, PixelFormat.Format32bppArgb)
    {
      FPalette256 = CPalette256.CreatePalette256(palettekind);
      CBitmapConversionPalette.ConvertMatrix08BitPalette256ToBitmap32bppArgb(matrix, FPalette256, out FBitmap);
    }

    public CBitmap256(EPaletteKind palettekind,
                      CMatrix10Bit matrix)
      : base(matrix.ColCount, matrix.RowCount, PixelFormat.Format32bppArgb)
    {
      FPalette256 = CPalette256.CreatePalette256(palettekind);
      CBitmapConversionPalette.ConvertMatrix10BitPalette256ToBitmap32bppArgb(matrix, FPalette256, out FBitmap);
    }

    public CBitmap256(EPaletteKind palettekind,
                      CMatrix12Bit matrix)
      : base(matrix.ColCount, matrix.RowCount, PixelFormat.Format32bppArgb)
    {
      FPalette256 = CPalette256.CreatePalette256(palettekind);
      CBitmapConversionPalette.ConvertMatrix12BitPalette256ToBitmap32bppArgb(matrix, FPalette256, out FBitmap);
    }

    public CBitmap256(EPaletteKind palettekind,
                  CMatrixBase matrix)
      : base(matrix.ColCount, matrix.RowCount, PixelFormat.Format32bppArgb)
    {
      FPalette256 = CPalette256.CreatePalette256(palettekind);
      if (matrix is CMatrix08Bit)
      {
        CBitmapConversionPalette.ConvertMatrix08BitPalette256ToBitmap32bppArgb((CMatrix08Bit)matrix, FPalette256, out FBitmap);
      }
      else
        if (matrix is CMatrix10Bit)
        {
          CBitmapConversionPalette.ConvertMatrix10BitPalette256ToBitmap32bppArgb((CMatrix10Bit)matrix, FPalette256, out FBitmap);
        }
        else
          if (matrix is CMatrix12Bit)
          {
            CBitmapConversionPalette.ConvertMatrix12BitPalette256ToBitmap32bppArgb((CMatrix12Bit)matrix, FPalette256, out FBitmap);
          }
    }

    public CBitmap256(EPaletteKind palettekind,
                      Bitmap bitmap)
      : base(bitmap.Width, bitmap.Height, PixelFormat.Format32bppArgb)
    {
      FPalette256 = CPalette256.CreatePalette256(palettekind);
      CBitmapConversionPalette.ConvertBitmapPalette256ToBitmap32bppArgb(bitmap, FPalette256, out FBitmap);
    }
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //
    public CPalette256 Palette256
    {
      get { return FPalette256; }
    }

  }
}
