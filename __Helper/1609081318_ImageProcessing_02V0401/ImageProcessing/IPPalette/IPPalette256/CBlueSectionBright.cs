﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPPaletteBase;
//
namespace IPPalette256
{
  public class CBlueSectionBright : CPalette256
  {
    //
    //------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------
    //
    protected static String NAME_PALETTE = CPalette256.PALETTEKINDS[(int)EPaletteKind.BlueSectionBright];
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CBlueSectionBright()
      : base(NAME_PALETTE, EPaletteKind.BlueSectionBright)
    {
      BuildPaletteColors();
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------
    //
    protected override Boolean BuildPaletteColors()
    {
      return BuildPaletteColorsSection(0, 0, 255, 128, 8);
    }

  }
}
