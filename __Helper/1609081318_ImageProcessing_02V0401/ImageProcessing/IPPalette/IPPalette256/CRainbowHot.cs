﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPPaletteBase;
//
namespace IPPalette256
{
  public class CRainbowHot : CPalette256
  {
    //
    //------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------
    //
    protected static String NAME_PALETTE = CPalette256.PALETTEKINDS[(int)EPaletteKind.GreyScale];
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CRainbowHot()
      : base(NAME_PALETTE, EPaletteKind.GreyScale)
    {
      BuildPaletteColors();
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------
    //
    protected override Boolean BuildPaletteColors()
    {
      FColors[0xFF] = Color.FromArgb(0xFF, 255, 248, 248);
      FColors[0xFE] = Color.FromArgb(0xFF, 254, 235, 235);
      FColors[0xFD] = Color.FromArgb(0xFF, 253, 225, 225);
      FColors[0xFC] = Color.FromArgb(0xFF, 252, 215, 215);
      FColors[0xFB] = Color.FromArgb(0xFF, 251, 205, 205); 
      FColors[0xFA] = Color.FromArgb(0xFF, 250, 195, 195); 
      FColors[0xF9] = Color.FromArgb(0xFF, 249, 185, 185); 
      FColors[0xF8] = Color.FromArgb(0xFF, 248, 175, 175); 
      FColors[0xF7] = Color.FromArgb(0xFF, 247, 160, 160); 
      FColors[0xF6] = Color.FromArgb(0xFF, 246, 150, 150); 
      FColors[0xF5] = Color.FromArgb(0xFF, 245, 140, 140); 
      FColors[0xF4] = Color.FromArgb(0xFF, 244, 130, 130); 
      FColors[0xF3] = Color.FromArgb(0xFF, 243, 120, 120); 
      FColors[0xF2] = Color.FromArgb(0xFF, 242, 110, 110); 
      FColors[0xF1] = Color.FromArgb(0xFF, 241, 100, 100); 
      FColors[0xF0] = Color.FromArgb(0xFF, 240,  90,  90); 
      //
      FColors[0xEF] = Color.FromArgb(0xFF, 238,  80,  80); 
      FColors[0xEE] = Color.FromArgb(0xFF, 236,  70,  80);
      FColors[0xED] = Color.FromArgb(0xFF, 235, 60, 80);
      FColors[0xEC] = Color.FromArgb(0xFF, 234, 58, 58);
      FColors[0xEB] = Color.FromArgb(0xFF, 233, 56, 56);
      FColors[0xEA] = Color.FromArgb(0xFF, 232, 54, 54);
      FColors[0xE9] = Color.FromArgb(0xFF, 231, 52, 52);
      FColors[0xE8] = Color.FromArgb(0xFF, 230, 50, 50);
      FColors[0xE7] = Color.FromArgb(0xFF, 229, 46, 46);
      FColors[0xE6] = Color.FromArgb(0xFF, 228, 43, 43);
      FColors[0xE5] = Color.FromArgb(0xFF, 227, 40, 40);
      FColors[0xE4] = Color.FromArgb(0xFF, 226,  38,  48);
      FColors[0xE3] = Color.FromArgb(0xFF, 225,  36,  36); 
      FColors[0xE2] = Color.FromArgb(0xFF, 224,  34,  34); 
      FColors[0xE1] = Color.FromArgb(0xFF, 223,  32,  32); 
      FColors[0xE0] = Color.FromArgb(0xFF, 220,  30,  30); 
      //
      FColors[0xDF] = Color.FromArgb(0xFF, 211,  29,  29); 
      FColors[0xDE] = Color.FromArgb(0xFF, 217,  28,  28); 
      FColors[0xDD] = Color.FromArgb(0xFF, 220,  27,  27); 
      FColors[0xDC] = Color.FromArgb(0xFF, 225,  26,  26); 
      FColors[0xDB] = Color.FromArgb(0xFF, 230,  25,  25); 
      FColors[0xDA] = Color.FromArgb(0xFF, 233,  24,  24); 
      FColors[0xD9] = Color.FromArgb(0xFF, 236,  23,  23); 
      FColors[0xD8] = Color.FromArgb(0xFF, 239,  22,  22); 
      FColors[0xD7] = Color.FromArgb(0xFF, 242,  21,  21);
      FColors[0xD6] = Color.FromArgb(0xFF, 245,  20,  20);
      FColors[0xD5] = Color.FromArgb(0xFF, 248,  19,  19);
      FColors[0xD4] = Color.FromArgb(0xFF, 250,  18,  18);
      FColors[0xD3] = Color.FromArgb(0xFF, 252,  17,  17);
      FColors[0xD2] = Color.FromArgb(0xFF, 254,  15,  15);
      FColors[0xD1] = Color.FromArgb(0xFF, 255,  20,  10);
      FColors[0xD0] = Color.FromArgb(0xFF, 255,  30,   9);
      //
      FColors[0xCF] = Color.FromArgb(0xFF, 255,  40,   8);
      FColors[0xCE] = Color.FromArgb(0xFF, 255,  50,   7);
      FColors[0xCD] = Color.FromArgb(0xFF, 255,  60,   6);
      FColors[0xCC] = Color.FromArgb(0xFF, 255,  70,   5);
      FColors[0xCB] = Color.FromArgb(0xFF, 255,  80,   4); 
      FColors[0xCA] = Color.FromArgb(0xFF, 255,  90,   3); 
      FColors[0xC9] = Color.FromArgb(0xFF, 255, 100,   2); 
      FColors[0xC8] = Color.FromArgb(0xFF, 255, 120,   1); 
      FColors[0xC7] = Color.FromArgb(0xFF, 255, 130,   0);
      FColors[0xC6] = Color.FromArgb(0xFF, 255, 135,   0);
      FColors[0xC5] = Color.FromArgb(0xFF, 255, 140,   0);
      FColors[0xC4] = Color.FromArgb(0xFF, 255, 145,   0);
      FColors[0xC3] = Color.FromArgb(0xFF, 255, 150,   0);
      FColors[0xC2] = Color.FromArgb(0xFF, 255, 154,   0);
      FColors[0xC1] = Color.FromArgb(0xFF, 255, 158,   0);
      FColors[0xC0] = Color.FromArgb(0xFF, 255, 160,   0);
      //
      FColors[0xBF] = Color.FromArgb(0xFF, 255, 162,   0); 
      FColors[0xBE] = Color.FromArgb(0xFF, 255, 164,   0); 
      FColors[0xBD] = Color.FromArgb(0xFF, 255, 166,   0); 
      FColors[0xBC] = Color.FromArgb(0xFF, 255, 168,   0); 
      FColors[0xBB] = Color.FromArgb(0xFF, 255, 170,   0);
      FColors[0xBA] = Color.FromArgb(0xFF, 255, 172,   0);
      FColors[0xB9] = Color.FromArgb(0xFF, 255, 174,   0);
      FColors[0xB8] = Color.FromArgb(0xFF, 255, 176,   0);
      FColors[0xB7] = Color.FromArgb(0xFF, 255, 178,   0); 
      FColors[0xB6] = Color.FromArgb(0xFF, 255, 180,   0); 
      FColors[0xB5] = Color.FromArgb(0xFF, 255, 182,   0); 
      FColors[0xB4] = Color.FromArgb(0xFF, 255, 184,   0); 
      FColors[0xB3] = Color.FromArgb(0xFF, 255, 188,   0); 
      FColors[0xB2] = Color.FromArgb(0xFF, 255, 192,   0); 
      FColors[0xB1] = Color.FromArgb(0xFF, 255, 196,   0); 
      FColors[0xB0] = Color.FromArgb(0xFF, 255, 199,   0); 
      //
      FColors[0xAF] = Color.FromArgb(0xFF, 254, 202,   0);
      FColors[0xAE] = Color.FromArgb(0xFF, 253, 205,   0);
      FColors[0xAD] = Color.FromArgb(0xFF, 252, 208,   0);
      FColors[0xAC] = Color.FromArgb(0xFF, 251, 211,   0);
      FColors[0xAB] = Color.FromArgb(0xFF, 250, 214,   0);
      FColors[0xAA] = Color.FromArgb(0xFF, 249, 216,   0);
      FColors[0xA9] = Color.FromArgb(0xFF, 248, 219,   0);
      FColors[0xA8] = Color.FromArgb(0xFF, 247, 221,   0);
      FColors[0xA7] = Color.FromArgb(0xFF, 246, 224,   0);
      FColors[0xA6] = Color.FromArgb(0xFF, 245, 227,   0);
      FColors[0xA5] = Color.FromArgb(0xFF, 244, 231,   0);
      FColors[0xA4] = Color.FromArgb(0xFF, 243, 234,   0);
      FColors[0xA3] = Color.FromArgb(0xFF, 242, 237,   0);
      FColors[0xA2] = Color.FromArgb(0xFF, 241, 240,   0);
      FColors[0xA1] = Color.FromArgb(0xFF, 240, 241,   0);
      FColors[0xA0] = Color.FromArgb(0xFF, 230, 241,   0);
      //
      FColors[0x9F] = Color.FromArgb(0xFF, 220, 240,   0);
      FColors[0x9E] = Color.FromArgb(0xFF, 215, 240,   0);
      FColors[0x9D] = Color.FromArgb(0xFF, 210, 239,   0);
      FColors[0x9C] = Color.FromArgb(0xFF, 205, 239,   0);
      FColors[0x9B] = Color.FromArgb(0xFF, 203, 238,   0); 
      FColors[0x9A] = Color.FromArgb(0xFF, 202, 238,   0); 
      FColors[0x99] = Color.FromArgb(0xFF, 201, 238,   0); 
      FColors[0x98] = Color.FromArgb(0xFF, 200, 238,   0); 
      FColors[0x97] = Color.FromArgb(0xFF, 190, 237,   0); 
      FColors[0x96] = Color.FromArgb(0xFF, 185, 237,   0); 
      FColors[0x95] = Color.FromArgb(0xFF, 180, 237,   0); 
      FColors[0x94] = Color.FromArgb(0xFF, 175, 237,   0); 
      FColors[0x93] = Color.FromArgb(0xFF, 170, 236,   0);
      FColors[0x92] = Color.FromArgb(0xFF, 165, 236,   0);
      FColors[0x91] = Color.FromArgb(0xFF, 160, 235,   0);
      FColors[0x90] = Color.FromArgb(0xFF, 155, 235,   2);
      //
      FColors[0x8F] = Color.FromArgb(0xFF, 150, 236,   4); 
      FColors[0x8E] = Color.FromArgb(0xFF, 145, 236,   6); 
      FColors[0x8D] = Color.FromArgb(0xFF, 140, 236,   8); 
      FColors[0x8C] = Color.FromArgb(0xFF, 135, 236,  10); 
      FColors[0x8B] = Color.FromArgb(0xFF, 125, 237,  12); 
      FColors[0x8A] = Color.FromArgb(0xFF, 120, 237,  14); 
      FColors[0x89] = Color.FromArgb(0xFF, 115, 237,  16); 
      FColors[0x88] = Color.FromArgb(0xFF, 112, 237,  18); 
      FColors[0x87] = Color.FromArgb(0xFF,  98, 238,  20); 
      FColors[0x86] = Color.FromArgb(0xFF,  94, 238,  22); 
      FColors[0x85] = Color.FromArgb(0xFF,  90, 238,  24); 
      FColors[0x84] = Color.FromArgb(0xFF,  85, 238,  28); 
      FColors[0x83] = Color.FromArgb(0xFF,  73, 239,  32); 
      FColors[0x82] = Color.FromArgb(0xFF,  70, 239,  36); 
      FColors[0x81] = Color.FromArgb(0xFF,  68, 239,  40); 
      FColors[0x80] = Color.FromArgb(0xFF,  60, 239,  46); 
      //
      FColors[0x7F] = Color.FromArgb(0xFF,  55, 236,  50); 
      FColors[0x7E] = Color.FromArgb(0xFF,  50, 233,  55); 
      FColors[0x7D] = Color.FromArgb(0xFF,  45, 230,  63); 
      FColors[0x7C] = Color.FromArgb(0xFF,  40, 229,  72); 
      FColors[0x7B] = Color.FromArgb(0xFF,  35, 226,  80); 
      FColors[0x7A] = Color.FromArgb(0xFF,  30, 225,  85); 
      FColors[0x79] = Color.FromArgb(0xFF,  25, 224,  90); 
      FColors[0x78] = Color.FromArgb(0xFF,  20, 223,  96); 
      FColors[0x77] = Color.FromArgb(0xFF,  18, 222, 100);
      FColors[0x76] = Color.FromArgb(0xFF,  16, 221, 105);
      FColors[0x75] = Color.FromArgb(0xFF,  14, 221, 110);
      FColors[0x74] = Color.FromArgb(0xFF,  12, 221, 115);
      FColors[0x73] = Color.FromArgb(0xFF,  10, 220, 120);
      FColors[0x72] = Color.FromArgb(0xFF,   8, 220, 125);
      FColors[0x71] = Color.FromArgb(0xFF,   6, 221, 130);
      FColors[0x70] = Color.FromArgb(0xFF,   4, 221, 135);
      //
      FColors[0x6F] = Color.FromArgb(0xFF,   2, 222, 141);
      FColors[0x6E] = Color.FromArgb(0xFF,   0, 222, 145);
      FColors[0x6D] = Color.FromArgb(0xFF,   0, 223, 149);
      FColors[0x6C] = Color.FromArgb(0xFF,   0, 223, 154);
      FColors[0x6B] = Color.FromArgb(0xFF,   0, 221, 159);
      FColors[0x6A] = Color.FromArgb(0xFF,   0, 219, 164);
      FColors[0x69] = Color.FromArgb(0xFF,   0, 217, 169);
      FColors[0x68] = Color.FromArgb(0xFF,   0, 213, 174);
      FColors[0x67] = Color.FromArgb(0xFF,   0, 211, 179);
      FColors[0x66] = Color.FromArgb(0xFF,   0, 208, 184);
      FColors[0x65] = Color.FromArgb(0xFF,   0, 206, 189);
      FColors[0x64] = Color.FromArgb(0xFF,   0, 202, 191);
      FColors[0x63] = Color.FromArgb(0xFF,   0, 199, 193);
      FColors[0x62] = Color.FromArgb(0xFF,   0, 198, 195);
      FColors[0x61] = Color.FromArgb(0xFF,   0, 197, 197);
      FColors[0x60] = Color.FromArgb(0xFF,   0, 196, 198);
      //
      FColors[0x5F] = Color.FromArgb(0xFF,   0, 195, 197);
      FColors[0x5E] = Color.FromArgb(0xFF,   0, 194, 196);
      FColors[0x5D] = Color.FromArgb(0xFF,   0, 193, 195);
      FColors[0x5C] = Color.FromArgb(0xFF,   0, 192, 194);
      FColors[0x5B] = Color.FromArgb(0xFF,   0, 191, 193);
      FColors[0x5A] = Color.FromArgb(0xFF,   0, 191, 192);
      FColors[0x59] = Color.FromArgb(0xFF,   0, 190, 191);
      FColors[0x58] = Color.FromArgb(0xFF,   0, 189, 190);
      FColors[0x57] = Color.FromArgb(0xFF,   0, 188, 189);
      FColors[0x56] = Color.FromArgb(0xFF,   0, 187, 188);
      FColors[0x55] = Color.FromArgb(0xFF,   0, 186, 187);
      FColors[0x54] = Color.FromArgb(0xFF,   0, 185, 186);
      FColors[0x53] = Color.FromArgb(0xFF,   0, 184, 185);
      FColors[0x52] = Color.FromArgb(0xFF,   0, 182, 184);
      FColors[0x51] = Color.FromArgb(0xFF,   0, 180, 183);
      FColors[0x50] = Color.FromArgb(0xFF,   0, 170, 184);
      //
      FColors[0x4F] = Color.FromArgb(0xFF,   1, 168, 185);
      FColors[0x4E] = Color.FromArgb(0xFF,   1, 166, 186);
      FColors[0x4D] = Color.FromArgb(0xFF,   1, 164, 187);
      FColors[0x4C] = Color.FromArgb(0xFF,   1, 162, 188);
      FColors[0x4B] = Color.FromArgb(0xFF,   1, 158, 189);
      FColors[0x4A] = Color.FromArgb(0xFF,   1, 154, 190);
      FColors[0x49] = Color.FromArgb(0xFF,   1, 150, 191);
      FColors[0x48] = Color.FromArgb(0xFF,   1, 146, 192);
      FColors[0x47] = Color.FromArgb(0xFF,   2, 138, 193);
      FColors[0x46] = Color.FromArgb(0xFF,   3, 134, 194);
      FColors[0x45] = Color.FromArgb(0xFF,   4, 130, 195);
      FColors[0x44] = Color.FromArgb(0xFF,   5, 126, 196);
      FColors[0x43] = Color.FromArgb(0xFF,   6, 122, 197);
      FColors[0x42] = Color.FromArgb(0xFF,   7, 116, 198);
      FColors[0x41] = Color.FromArgb(0xFF,   8, 112, 198);
      FColors[0x40] = Color.FromArgb(0xFF,   9, 108, 199);
      //
      FColors[0x3F] = Color.FromArgb(0xFF,  10, 104, 199);
      FColors[0x3E] = Color.FromArgb(0xFF,  11, 100, 198);
      FColors[0x3D] = Color.FromArgb(0xFF,  12,  96, 198);
      FColors[0x3C] = Color.FromArgb(0xFF,  13,  92, 198);
      FColors[0x3B] = Color.FromArgb(0xFF,  14,  88, 198);
      FColors[0x3A] = Color.FromArgb(0xFF,  15,  84, 198);
      FColors[0x39] = Color.FromArgb(0xFF,  16,  82, 199);
      FColors[0x38] = Color.FromArgb(0xFF,  18,  78, 199);
      FColors[0x37] = Color.FromArgb(0xFF,  20,  72, 198);
      FColors[0x36] = Color.FromArgb(0xFF,  21,  69, 197);
      FColors[0x35] = Color.FromArgb(0xFF,  22,  65, 196);
      FColors[0x34] = Color.FromArgb(0xFF,  24,  61, 195);
      FColors[0x33] = Color.FromArgb(0xFF,  26,  56, 194);
      FColors[0x32] = Color.FromArgb(0xFF,  27,  54, 193);
      FColors[0x31] = Color.FromArgb(0xFF,  28,  52, 192);
      FColors[0x30] = Color.FromArgb(0xFF,  29,  50, 189);
      //
      FColors[0x2F] = Color.FromArgb(0xFF,  30,  47, 186);
      FColors[0x2E] = Color.FromArgb(0xFF,  31,  43, 183);
      FColors[0x2D] = Color.FromArgb(0xFF,  32,  40, 180);
      FColors[0x2C] = Color.FromArgb(0xFF,  33,  36, 177);
      FColors[0x2B] = Color.FromArgb(0xFF,  34,  32, 174);
      FColors[0x2A] = Color.FromArgb(0xFF,  35,  28, 171);
      FColors[0x29] = Color.FromArgb(0xFF,  36,  24, 168);
      FColors[0x28] = Color.FromArgb(0xFF,  37,  20, 165);
      FColors[0x27] = Color.FromArgb(0xFF,  38,  16, 163);
      FColors[0x26] = Color.FromArgb(0xFF,  39,  14, 161);
      FColors[0x25] = Color.FromArgb(0xFF,  40,  12, 159);
      FColors[0x24] = Color.FromArgb(0xFF,  41,  10, 157);
      FColors[0x23] = Color.FromArgb(0xFF,  42,   8, 155);
      FColors[0x22] = Color.FromArgb(0xFF,  43,   6, 153);
      FColors[0x21] = Color.FromArgb(0xFF,  44,   4, 151);
      FColors[0x20] = Color.FromArgb(0xFF,  45,   2, 149);
      //
      FColors[0x1F] = Color.FromArgb(0xFF,  46,   0, 147);
      FColors[0x1E] = Color.FromArgb(0xFF,  47,   0, 145);
      FColors[0x1D] = Color.FromArgb(0xFF,  48,   0, 143);
      FColors[0x1C] = Color.FromArgb(0xFF,  49,   0, 141);
      FColors[0x1B] = Color.FromArgb(0xFF,  50,   0, 139);
      FColors[0x1A] = Color.FromArgb(0xFF,  51,   0, 137);
      FColors[0x19] = Color.FromArgb(0xFF,  52,   0, 135);
      FColors[0x18] = Color.FromArgb(0xFF,  53,   0, 133);
      FColors[0x17] = Color.FromArgb(0xFF,  54,   0, 131);
      FColors[0x16] = Color.FromArgb(0xFF,  55,   0, 129);
      FColors[0x15] = Color.FromArgb(0xFF,  56,   0, 127);
      FColors[0x14] = Color.FromArgb(0xFF,  57,   0, 125);
      FColors[0x13] = Color.FromArgb(0xFF,  58,   0, 123);
      FColors[0x12] = Color.FromArgb(0xFF,  59,   0, 121);
      FColors[0x11] = Color.FromArgb(0xFF,  60,   0, 119);
      FColors[0x10] = Color.FromArgb(0xFF,  62,   0, 117);
      //
      FColors[0x0F] = Color.FromArgb(0xFF,  64,   0, 115);
      FColors[0x0E] = Color.FromArgb(0xFF,  66,   0, 113);
      FColors[0x0D] = Color.FromArgb(0xFF,  68,   0, 111);
      FColors[0x0C] = Color.FromArgb(0xFF,  70,   0, 109);
      FColors[0x0B] = Color.FromArgb(0xFF,  72,   0, 107);
      FColors[0x0A] = Color.FromArgb(0xFF,  74,   0, 105);
      FColors[0x09] = Color.FromArgb(0xFF,  76,   0, 103);
      FColors[0x08] = Color.FromArgb(0xFF,  78,   0, 101);
      FColors[0x07] = Color.FromArgb(0xFF,  80,   0,  98);
      FColors[0x06] = Color.FromArgb(0xFF,  81,   0,  95);
      FColors[0x05] = Color.FromArgb(0xFF,  82,   0,  92);
      FColors[0x04] = Color.FromArgb(0xFF,  83,   0,  89);
      FColors[0x03] = Color.FromArgb(0xFF,  84,   0,  86);
      FColors[0x02] = Color.FromArgb(0xFF,  85,   0,  84);
      FColors[0x01] = Color.FromArgb(0xFF,  85,   0,  82);
      FColors[0x00] = Color.FromArgb(0xFF,  85,   0,  80);
      //
      return true;
    }

  }
}
