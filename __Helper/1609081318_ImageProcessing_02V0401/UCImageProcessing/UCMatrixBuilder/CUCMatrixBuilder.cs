﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
//
using IPMemory;
using IPMatrix;
using IPPaletteBase;
using IPPalette256;
using IPBitmap;
//
namespace UCMatrixBuilder
{
  public partial class CUCMatrixBuilder : UserControl
  {
    //
    //-----------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //
    CMatrixBase FMatrix;      
    //
    //-----------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------
    //
    public CUCMatrixBuilder()
    {
      InitializeComponent();
      //
      rbtText.Checked = true;
    }
    //
    //-----------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------
    //
    private Int32 BitmapWidth
    {
      get { return (Int32)nudWidth.Value; }
    }
    private Int32 BitmapHeight
    {
      get { return (Int32)nudHeight.Value; }
    }
    //
    //-----------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------
    //
    private void btnCreateMatrixConstant_Click(object sender, EventArgs e)
    {
      FMatrix = null;
      pbxView.Image = null;
      if (rbtMatrix08Bit.Checked)
      {
        CMatrix08Bit Matrix08Bit;
        CBitmap256 Bitmap256;
        if (CBitmapBuilder.CreateBitmap08BitConstant(BitmapWidth, BitmapHeight, 256 >> 1,
                                                     out Matrix08Bit, out Bitmap256))
        {
          FMatrix = Matrix08Bit;
          pbxView.Image = Bitmap256.Bitmap;
        }
      }
      else
        if (rbtMatrix10Bit.Checked)
        {
          CMatrix10Bit Matrix10Bit;
          CBitmap256 Bitmap256;
          if (CBitmapBuilder.CreateBitmap10BitConstant(BitmapWidth, BitmapHeight, 1024 >> 1,
                                                       out Matrix10Bit, out Bitmap256))
          {
            FMatrix = Matrix10Bit;
            pbxView.Image = Bitmap256.Bitmap;
          }
        }
        else
          if (rbtMatrix12Bit.Checked)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmap256 Bitmap256;
            if (CBitmapBuilder.CreateBitmap12BitConstant(BitmapWidth, BitmapHeight, 4096 >> 1,
                                                         out Matrix12Bit, out Bitmap256))
            {
              FMatrix = Matrix12Bit;
              pbxView.Image = Bitmap256.Bitmap;
            }
          }
    }

    private void btnCreateMatrixLinear_Click(object sender, EventArgs e)
    {
      FMatrix = null;
      pbxView.Image = null;
      if (rbtMatrix08Bit.Checked)
      {
        CMatrix08Bit Matrix08Bit;
        CBitmap256 Bitmap256;
        if (CBitmapBuilder.CreateBitmap08BitLinear(BitmapWidth, BitmapHeight, 0, 255, 0, 255, 
                                                   out Matrix08Bit, out Bitmap256))
        {
          FMatrix = Matrix08Bit;
          pbxView.Image = Bitmap256.Bitmap;
        }
      }
      else
        if (rbtMatrix10Bit.Checked)
        {
          CMatrix10Bit Matrix10Bit;
          CBitmap256 Bitmap256;
          if (CBitmapBuilder.CreateBitmap10BitLinear(BitmapWidth, BitmapHeight, 0, 1023, 0, 1023, 
                                                     out Matrix10Bit, out Bitmap256))
          {
            FMatrix = Matrix10Bit;
            pbxView.Image = Bitmap256.Bitmap;
          }
        }
        else
          if (rbtMatrix12Bit.Checked)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmap256 Bitmap256;
            if (CBitmapBuilder.CreateBitmap12BitLinear(BitmapWidth, BitmapHeight, 0, 4095, 0, 4095, 
                                                       out Matrix12Bit, out Bitmap256))
            {
              FMatrix = Matrix12Bit;
              pbxView.Image = Bitmap256.Bitmap;
            }
          }
    }

    private void btnCreateMatrixCircular_Click(object sender, EventArgs e)
    {
      FMatrix = null;
      pbxView.Image = null;
      if (rbtMatrix08Bit.Checked)
      {
        CMatrix08Bit Matrix08Bit;
        CBitmap256 Bitmap256;
        if (CBitmapBuilder.CreateBitmap08BitCircular(BitmapWidth, BitmapHeight, 0, 255, 
                                                     out Matrix08Bit, out Bitmap256))
        {
          FMatrix = Matrix08Bit;
          pbxView.Image = Bitmap256.Bitmap;
        }
      }
      else
        if (rbtMatrix10Bit.Checked)
        {
          CMatrix10Bit Matrix10Bit;
          CBitmap256 Bitmap256;
          if (CBitmapBuilder.CreateBitmap10BitCircular(BitmapWidth, BitmapHeight, 0, 1023, 
                                                       out Matrix10Bit, out Bitmap256))
          {
            FMatrix = Matrix10Bit;
            pbxView.Image = Bitmap256.Bitmap;
          }
        }
        else
          if (rbtMatrix12Bit.Checked)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmap256 Bitmap256;
            if (CBitmapBuilder.CreateBitmap12BitCircular(BitmapWidth, BitmapHeight, 0, 4095, 
                                                         out Matrix12Bit, out Bitmap256))
            {
              FMatrix = Matrix12Bit;
              pbxView.Image = Bitmap256.Bitmap;
            }
          }
    }

    private void btnCreateMatrixNoise_Click(object sender, EventArgs e)
    {
      FMatrix = null;
      pbxView.Image = null;
      if (rbtMatrix08Bit.Checked)
      {
        CMatrix08Bit Matrix08Bit;
        CBitmap256 Bitmap256;
        if (CBitmapBuilder.CreateBitmap08BitNoise(BitmapWidth, BitmapHeight, 0, 255,
                                                  out Matrix08Bit, out Bitmap256))
        {
          FMatrix = Matrix08Bit;
          pbxView.Image = Bitmap256.Bitmap;
        }
      }
      else
        if (rbtMatrix10Bit.Checked)
        {
          CMatrix10Bit Matrix10Bit;
          CBitmap256 Bitmap256;
          if (CBitmapBuilder.CreateBitmap10BitNoise(BitmapWidth, BitmapHeight, 0, 1023,
                                                    out Matrix10Bit, out Bitmap256))
          {
            FMatrix = Matrix10Bit;
            pbxView.Image = Bitmap256.Bitmap;
          }
        }
        else
          if (rbtMatrix12Bit.Checked)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmap256 Bitmap256;
            if (CBitmapBuilder.CreateBitmap12BitNoise(BitmapWidth, BitmapHeight, 0, 4095,
                                                      out Matrix12Bit, out Bitmap256))
            {
              FMatrix = Matrix12Bit;
              pbxView.Image = Bitmap256.Bitmap;
            }
          }
    }

    private Boolean WriteMatrixText()
    {
      try
      {
        if (FMatrix is CMatrix08Bit)
        {
          DialogSaveMatrixText.FileName = FMatrix.Name + ".txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.Write((CMatrix08Bit)FMatrix);
            MatrixFileText.Close();
            return true;
          }
        }
        if (FMatrix is CMatrix10Bit)
        {
          DialogSaveMatrixText.FileName = FMatrix.Name + ".txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.Write((CMatrix10Bit)FMatrix);
            MatrixFileText.Close();
            return true;
          }
        }
        if (FMatrix is CMatrix12Bit)
        {
          DialogSaveMatrixText.FileName = FMatrix.Name + ".txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.Write((CMatrix12Bit)FMatrix);
            MatrixFileText.Close();
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean WriteMatrixBinary()
    {
      try
      {
        if (FMatrix is CMatrix08Bit)
        {
          DialogSaveMatrixBinary.FileName = FMatrix.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.Write((CMatrix08Bit)FMatrix);
            MatrixFileBinary.Close();
            return true;
          }
        }
        if (FMatrix is CMatrix10Bit)
        {
          DialogSaveMatrixBinary.FileName = FMatrix.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.Write((CMatrix10Bit)FMatrix);
            MatrixFileBinary.Close();
            return true;
          }
        }
        if (FMatrix is CMatrix12Bit)
        {
          DialogSaveMatrixBinary.FileName = FMatrix.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.Write((CMatrix12Bit)FMatrix);
            MatrixFileBinary.Close();
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }


    private void btnWriteFile_Click(object sender, EventArgs e)
    {
      if (FMatrix is CMatrixBase)
      {
        if (rbtText.Checked)
        {
          WriteMatrixText();
        }
        if (rbtBinary.Checked)
        {
          WriteMatrixBinary();
        }
      }     
    }

    private void RefreshBitmapRange()
    {
      rbtMatrix08Bit.Checked = (FMatrix is CMatrix08Bit);
      rbtMatrix10Bit.Checked = (FMatrix is CMatrix10Bit);
      rbtMatrix12Bit.Checked = (FMatrix is CMatrix12Bit);
    }

    private void btnReadFile_Click(object sender, EventArgs e)
    {
      if (rbtText.Checked)
      {
        if (DialogResult.OK == DialogLoadMatrixText.ShowDialog())
        {
          CMatrixFileText MatrixFileText = new CMatrixFileText();
          MatrixFileText.Read(DialogLoadMatrixText.FileName, out FMatrix);
          String SName = Path.GetFileNameWithoutExtension(DialogLoadMatrixText.FileName);
          FMatrix.Name = SName;
          MatrixFileText.Close();
          RefreshBitmapRange();
          CBitmap256 Bitmap256 = new CBitmap256(EPaletteKind.RGBSection, FMatrix);
          pbxView.Image = Bitmap256.Bitmap;
        }
      }
      else
        if (rbtBinary.Checked)
        { 
          if (DialogResult.OK == DialogLoadMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.ReadFromFile(DialogLoadMatrixBinary.FileName, out FMatrix);
            String SName = Path.GetFileNameWithoutExtension(DialogLoadMatrixText.FileName);
            FMatrix.Name = SName;
            MatrixFileBinary.Close();
            RefreshBitmapRange();
            CBitmap256 Bitmap256 = new CBitmap256(EPaletteKind.RGBSection, FMatrix);
            pbxView.Image = Bitmap256.Bitmap;
          }
        }
    }
    //
    //-----------------------------------------------------------
    //  Section - Callback
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Public Method
    //-----------------------------------------------------------
    //

  }
}
