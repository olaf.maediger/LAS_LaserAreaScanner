﻿namespace UCMatrixBuilder
{
  partial class CUCMatrixOperator
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlUser = new System.Windows.Forms.Panel();
      this.nudArgument1 = new System.Windows.Forms.NumericUpDown();
      this.nudArgument0 = new System.Windows.Forms.NumericUpDown();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.rbtMatrix08Bit = new System.Windows.Forms.RadioButton();
      this.rbtMatrix12Bit = new System.Windows.Forms.RadioButton();
      this.rbtMatrix10Bit = new System.Windows.Forms.RadioButton();
      this.pbxMatrix01 = new System.Windows.Forms.PictureBox();
      this.pbxMatrix02 = new System.Windows.Forms.PictureBox();
      this.cbxMatrix01 = new System.Windows.Forms.ComboBox();
      this.cbxMatrix02 = new System.Windows.Forms.ComboBox();
      this.cbxMatrixOperation = new System.Windows.Forms.ComboBox();
      this.pbxMatrix03 = new System.Windows.Forms.PictureBox();
      this.cbxMatrix03 = new System.Windows.Forms.ComboBox();
      this.pnlUser.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudArgument1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudArgument0)).BeginInit();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix01)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix02)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix03)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlUser
      // 
      this.pnlUser.BackColor = System.Drawing.SystemColors.Control;
      this.pnlUser.Controls.Add(this.nudArgument1);
      this.pnlUser.Controls.Add(this.nudArgument0);
      this.pnlUser.Controls.Add(this.groupBox2);
      this.pnlUser.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlUser.Location = new System.Drawing.Point(0, 533);
      this.pnlUser.Name = "pnlUser";
      this.pnlUser.Size = new System.Drawing.Size(630, 54);
      this.pnlUser.TabIndex = 1;
      // 
      // nudArgument1
      // 
      this.nudArgument1.Location = new System.Drawing.Point(274, 24);
      this.nudArgument1.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudArgument1.Name = "nudArgument1";
      this.nudArgument1.Size = new System.Drawing.Size(61, 20);
      this.nudArgument1.TabIndex = 14;
      this.nudArgument1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudArgument1.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
      // 
      // nudArgument0
      // 
      this.nudArgument0.Location = new System.Drawing.Point(198, 24);
      this.nudArgument0.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudArgument0.Name = "nudArgument0";
      this.nudArgument0.Size = new System.Drawing.Size(61, 20);
      this.nudArgument0.TabIndex = 13;
      this.nudArgument0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.rbtMatrix08Bit);
      this.groupBox2.Controls.Add(this.rbtMatrix12Bit);
      this.groupBox2.Controls.Add(this.rbtMatrix10Bit);
      this.groupBox2.Location = new System.Drawing.Point(4, 6);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(162, 42);
      this.groupBox2.TabIndex = 12;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = " MatrixType ";
      // 
      // rbtMatrix08Bit
      // 
      this.rbtMatrix08Bit.Checked = true;
      this.rbtMatrix08Bit.Location = new System.Drawing.Point(6, 19);
      this.rbtMatrix08Bit.Name = "rbtMatrix08Bit";
      this.rbtMatrix08Bit.Size = new System.Drawing.Size(51, 18);
      this.rbtMatrix08Bit.TabIndex = 11;
      this.rbtMatrix08Bit.TabStop = true;
      this.rbtMatrix08Bit.Text = "08Bit";
      this.rbtMatrix08Bit.UseVisualStyleBackColor = true;
      // 
      // rbtMatrix12Bit
      // 
      this.rbtMatrix12Bit.Location = new System.Drawing.Point(108, 19);
      this.rbtMatrix12Bit.Name = "rbtMatrix12Bit";
      this.rbtMatrix12Bit.Size = new System.Drawing.Size(49, 17);
      this.rbtMatrix12Bit.TabIndex = 13;
      this.rbtMatrix12Bit.Text = "12Bit";
      this.rbtMatrix12Bit.UseVisualStyleBackColor = true;
      // 
      // rbtMatrix10Bit
      // 
      this.rbtMatrix10Bit.Location = new System.Drawing.Point(57, 19);
      this.rbtMatrix10Bit.Name = "rbtMatrix10Bit";
      this.rbtMatrix10Bit.Size = new System.Drawing.Size(54, 17);
      this.rbtMatrix10Bit.TabIndex = 12;
      this.rbtMatrix10Bit.Text = "10Bit";
      this.rbtMatrix10Bit.UseVisualStyleBackColor = true;
      // 
      // pbxMatrix01
      // 
      this.pbxMatrix01.BackColor = System.Drawing.SystemColors.Info;
      this.pbxMatrix01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxMatrix01.Location = new System.Drawing.Point(4, 30);
      this.pbxMatrix01.Name = "pbxMatrix01";
      this.pbxMatrix01.Size = new System.Drawing.Size(300, 220);
      this.pbxMatrix01.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pbxMatrix01.TabIndex = 2;
      this.pbxMatrix01.TabStop = false;
      // 
      // pbxMatrix02
      // 
      this.pbxMatrix02.BackColor = System.Drawing.SystemColors.Info;
      this.pbxMatrix02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxMatrix02.Location = new System.Drawing.Point(4, 283);
      this.pbxMatrix02.Name = "pbxMatrix02";
      this.pbxMatrix02.Size = new System.Drawing.Size(300, 220);
      this.pbxMatrix02.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pbxMatrix02.TabIndex = 3;
      this.pbxMatrix02.TabStop = false;
      // 
      // cbxMatrix01
      // 
      this.cbxMatrix01.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxMatrix01.FormattingEnabled = true;
      this.cbxMatrix01.Items.AddRange(new object[] {
            "Load from File",
            "Create Constant",
            "Create Linear",
            "Create Circular",
            "Create Noise"});
      this.cbxMatrix01.Location = new System.Drawing.Point(4, 3);
      this.cbxMatrix01.Name = "cbxMatrix01";
      this.cbxMatrix01.Size = new System.Drawing.Size(119, 21);
      this.cbxMatrix01.TabIndex = 5;
      this.cbxMatrix01.SelectedIndexChanged += new System.EventHandler(this.cbxMatrix01_SelectedIndexChanged);
      // 
      // cbxMatrix02
      // 
      this.cbxMatrix02.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxMatrix02.FormattingEnabled = true;
      this.cbxMatrix02.Items.AddRange(new object[] {
            "Load from File",
            "Create Constant",
            "Create Linear",
            "Create Circular",
            "Create Noise"});
      this.cbxMatrix02.Location = new System.Drawing.Point(4, 508);
      this.cbxMatrix02.Name = "cbxMatrix02";
      this.cbxMatrix02.Size = new System.Drawing.Size(119, 21);
      this.cbxMatrix02.TabIndex = 6;
      this.cbxMatrix02.SelectedIndexChanged += new System.EventHandler(this.cbxMatrix02_SelectedIndexChanged);
      // 
      // cbxMatrixOperation
      // 
      this.cbxMatrixOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxMatrixOperation.FormattingEnabled = true;
      this.cbxMatrixOperation.Items.AddRange(new object[] {
            "add",
            "subtract",
            "multiply",
            "divide"});
      this.cbxMatrixOperation.Location = new System.Drawing.Point(4, 256);
      this.cbxMatrixOperation.Name = "cbxMatrixOperation";
      this.cbxMatrixOperation.Size = new System.Drawing.Size(119, 21);
      this.cbxMatrixOperation.TabIndex = 7;
      this.cbxMatrixOperation.SelectedIndexChanged += new System.EventHandler(this.cbxMatrixOperation_SelectedIndexChanged);
      // 
      // pbxMatrix03
      // 
      this.pbxMatrix03.BackColor = System.Drawing.SystemColors.Info;
      this.pbxMatrix03.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxMatrix03.Location = new System.Drawing.Point(316, 148);
      this.pbxMatrix03.Name = "pbxMatrix03";
      this.pbxMatrix03.Size = new System.Drawing.Size(300, 220);
      this.pbxMatrix03.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pbxMatrix03.TabIndex = 8;
      this.pbxMatrix03.TabStop = false;
      // 
      // cbxMatrix03
      // 
      this.cbxMatrix03.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxMatrix03.FormattingEnabled = true;
      this.cbxMatrix03.Items.AddRange(new object[] {
            "Save Text",
            "Save Binary",
            "Norm",
            "Scale"});
      this.cbxMatrix03.Location = new System.Drawing.Point(316, 374);
      this.cbxMatrix03.Name = "cbxMatrix03";
      this.cbxMatrix03.Size = new System.Drawing.Size(119, 21);
      this.cbxMatrix03.TabIndex = 9;
      this.cbxMatrix03.SelectedIndexChanged += new System.EventHandler(this.cbxMatrix03_SelectedIndexChanged);
      // 
      // CUCMatrixOperator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.cbxMatrix03);
      this.Controls.Add(this.pbxMatrix03);
      this.Controls.Add(this.cbxMatrixOperation);
      this.Controls.Add(this.cbxMatrix02);
      this.Controls.Add(this.cbxMatrix01);
      this.Controls.Add(this.pbxMatrix02);
      this.Controls.Add(this.pbxMatrix01);
      this.Controls.Add(this.pnlUser);
      this.Name = "CUCMatrixOperator";
      this.Size = new System.Drawing.Size(630, 587);
      this.pnlUser.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudArgument1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudArgument0)).EndInit();
      this.groupBox2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix01)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix02)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix03)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlUser;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.RadioButton rbtMatrix08Bit;
    private System.Windows.Forms.RadioButton rbtMatrix12Bit;
    private System.Windows.Forms.RadioButton rbtMatrix10Bit;
    private System.Windows.Forms.PictureBox pbxMatrix01;
    private System.Windows.Forms.PictureBox pbxMatrix02;
    private System.Windows.Forms.ComboBox cbxMatrix01;
    private System.Windows.Forms.ComboBox cbxMatrix02;
    private System.Windows.Forms.ComboBox cbxMatrixOperation;
    private System.Windows.Forms.PictureBox pbxMatrix03;
    private System.Windows.Forms.ComboBox cbxMatrix03;
    private System.Windows.Forms.NumericUpDown nudArgument1;
    private System.Windows.Forms.NumericUpDown nudArgument0;
  }
}
