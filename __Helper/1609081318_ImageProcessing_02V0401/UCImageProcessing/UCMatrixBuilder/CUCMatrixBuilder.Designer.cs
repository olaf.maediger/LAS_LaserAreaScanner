﻿namespace UCMatrixBuilder
{
  partial class CUCMatrixBuilder
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlUser = new System.Windows.Forms.Panel();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.btnCreateMatrixCircular = new System.Windows.Forms.Button();
      this.btnCreateMatrixConstant = new System.Windows.Forms.Button();
      this.btnCreateMatrixNoise = new System.Windows.Forms.Button();
      this.btnCreateMatrixLinear = new System.Windows.Forms.Button();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.rbtMatrix08Bit = new System.Windows.Forms.RadioButton();
      this.rbtMatrix12Bit = new System.Windows.Forms.RadioButton();
      this.rbtMatrix10Bit = new System.Windows.Forms.RadioButton();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.rbtBinary = new System.Windows.Forms.RadioButton();
      this.rbtText = new System.Windows.Forms.RadioButton();
      this.btnReadFile = new System.Windows.Forms.Button();
      this.btnWriteFile = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.nudHeight = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.nudWidth = new System.Windows.Forms.NumericUpDown();
      this.pbxView = new System.Windows.Forms.PictureBox();
      this.DialogLoadMatrixText = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveMatrixText = new System.Windows.Forms.SaveFileDialog();
      this.DialogLoadMatrixBinary = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveMatrixBinary = new System.Windows.Forms.SaveFileDialog();
      this.pnlUser.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudHeight)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudWidth)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxView)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlUser
      // 
      this.pnlUser.Controls.Add(this.groupBox3);
      this.pnlUser.Controls.Add(this.groupBox2);
      this.pnlUser.Controls.Add(this.groupBox1);
      this.pnlUser.Controls.Add(this.label3);
      this.pnlUser.Controls.Add(this.nudHeight);
      this.pnlUser.Controls.Add(this.label2);
      this.pnlUser.Controls.Add(this.nudWidth);
      this.pnlUser.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlUser.Location = new System.Drawing.Point(0, 388);
      this.pnlUser.Name = "pnlUser";
      this.pnlUser.Size = new System.Drawing.Size(697, 81);
      this.pnlUser.TabIndex = 0;
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.btnCreateMatrixCircular);
      this.groupBox3.Controls.Add(this.btnCreateMatrixConstant);
      this.groupBox3.Controls.Add(this.btnCreateMatrixNoise);
      this.groupBox3.Controls.Add(this.btnCreateMatrixLinear);
      this.groupBox3.Location = new System.Drawing.Point(200, 20);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(258, 50);
      this.groupBox3.TabIndex = 13;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = " Create Matrix ";
      // 
      // btnCreateMatrixCircular
      // 
      this.btnCreateMatrixCircular.Location = new System.Drawing.Point(68, 19);
      this.btnCreateMatrixCircular.Name = "btnCreateMatrixCircular";
      this.btnCreateMatrixCircular.Size = new System.Drawing.Size(58, 24);
      this.btnCreateMatrixCircular.TabIndex = 4;
      this.btnCreateMatrixCircular.Text = "Circular";
      this.btnCreateMatrixCircular.UseVisualStyleBackColor = true;
      this.btnCreateMatrixCircular.Click += new System.EventHandler(this.btnCreateMatrixCircular_Click);
      // 
      // btnCreateMatrixConstant
      // 
      this.btnCreateMatrixConstant.Location = new System.Drawing.Point(133, 19);
      this.btnCreateMatrixConstant.Name = "btnCreateMatrixConstant";
      this.btnCreateMatrixConstant.Size = new System.Drawing.Size(58, 24);
      this.btnCreateMatrixConstant.TabIndex = 0;
      this.btnCreateMatrixConstant.Text = "Constant";
      this.btnCreateMatrixConstant.UseVisualStyleBackColor = true;
      this.btnCreateMatrixConstant.Click += new System.EventHandler(this.btnCreateMatrixConstant_Click);
      // 
      // btnCreateMatrixNoise
      // 
      this.btnCreateMatrixNoise.Location = new System.Drawing.Point(195, 19);
      this.btnCreateMatrixNoise.Name = "btnCreateMatrixNoise";
      this.btnCreateMatrixNoise.Size = new System.Drawing.Size(58, 24);
      this.btnCreateMatrixNoise.TabIndex = 1;
      this.btnCreateMatrixNoise.Text = "Noise";
      this.btnCreateMatrixNoise.UseVisualStyleBackColor = true;
      this.btnCreateMatrixNoise.Click += new System.EventHandler(this.btnCreateMatrixNoise_Click);
      // 
      // btnCreateMatrixLinear
      // 
      this.btnCreateMatrixLinear.Location = new System.Drawing.Point(6, 19);
      this.btnCreateMatrixLinear.Name = "btnCreateMatrixLinear";
      this.btnCreateMatrixLinear.Size = new System.Drawing.Size(58, 24);
      this.btnCreateMatrixLinear.TabIndex = 3;
      this.btnCreateMatrixLinear.Text = "Linear";
      this.btnCreateMatrixLinear.UseVisualStyleBackColor = true;
      this.btnCreateMatrixLinear.Click += new System.EventHandler(this.btnCreateMatrixLinear_Click);
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.rbtMatrix08Bit);
      this.groupBox2.Controls.Add(this.rbtMatrix12Bit);
      this.groupBox2.Controls.Add(this.rbtMatrix10Bit);
      this.groupBox2.Location = new System.Drawing.Point(12, 28);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(162, 42);
      this.groupBox2.TabIndex = 12;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = " MatrixType ";
      // 
      // rbtMatrix08Bit
      // 
      this.rbtMatrix08Bit.Checked = true;
      this.rbtMatrix08Bit.Location = new System.Drawing.Point(6, 19);
      this.rbtMatrix08Bit.Name = "rbtMatrix08Bit";
      this.rbtMatrix08Bit.Size = new System.Drawing.Size(51, 18);
      this.rbtMatrix08Bit.TabIndex = 11;
      this.rbtMatrix08Bit.TabStop = true;
      this.rbtMatrix08Bit.Text = "08Bit";
      this.rbtMatrix08Bit.UseVisualStyleBackColor = true;
      // 
      // rbtMatrix12Bit
      // 
      this.rbtMatrix12Bit.Location = new System.Drawing.Point(108, 19);
      this.rbtMatrix12Bit.Name = "rbtMatrix12Bit";
      this.rbtMatrix12Bit.Size = new System.Drawing.Size(49, 17);
      this.rbtMatrix12Bit.TabIndex = 13;
      this.rbtMatrix12Bit.Text = "12Bit";
      this.rbtMatrix12Bit.UseVisualStyleBackColor = true;
      // 
      // rbtMatrix10Bit
      // 
      this.rbtMatrix10Bit.Location = new System.Drawing.Point(57, 19);
      this.rbtMatrix10Bit.Name = "rbtMatrix10Bit";
      this.rbtMatrix10Bit.Size = new System.Drawing.Size(54, 17);
      this.rbtMatrix10Bit.TabIndex = 12;
      this.rbtMatrix10Bit.Text = "10Bit";
      this.rbtMatrix10Bit.UseVisualStyleBackColor = true;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.rbtBinary);
      this.groupBox1.Controls.Add(this.rbtText);
      this.groupBox1.Controls.Add(this.btnReadFile);
      this.groupBox1.Controls.Add(this.btnWriteFile);
      this.groupBox1.Location = new System.Drawing.Point(464, 20);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(156, 49);
      this.groupBox1.TabIndex = 11;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = " File ";
      // 
      // rbtBinary
      // 
      this.rbtBinary.AutoSize = true;
      this.rbtBinary.Location = new System.Drawing.Point(6, 28);
      this.rbtBinary.Name = "rbtBinary";
      this.rbtBinary.Size = new System.Drawing.Size(54, 17);
      this.rbtBinary.TabIndex = 12;
      this.rbtBinary.Text = "Binary";
      this.rbtBinary.UseVisualStyleBackColor = true;
      // 
      // rbtText
      // 
      this.rbtText.AutoSize = true;
      this.rbtText.Checked = true;
      this.rbtText.Location = new System.Drawing.Point(6, 12);
      this.rbtText.Name = "rbtText";
      this.rbtText.Size = new System.Drawing.Size(46, 17);
      this.rbtText.TabIndex = 11;
      this.rbtText.TabStop = true;
      this.rbtText.Text = "Text";
      this.rbtText.UseVisualStyleBackColor = true;
      // 
      // btnReadFile
      // 
      this.btnReadFile.Location = new System.Drawing.Point(107, 17);
      this.btnReadFile.Name = "btnReadFile";
      this.btnReadFile.Size = new System.Drawing.Size(42, 24);
      this.btnReadFile.TabIndex = 10;
      this.btnReadFile.Text = "Read";
      this.btnReadFile.UseVisualStyleBackColor = true;
      this.btnReadFile.Click += new System.EventHandler(this.btnReadFile_Click);
      // 
      // btnWriteFile
      // 
      this.btnWriteFile.Location = new System.Drawing.Point(61, 17);
      this.btnWriteFile.Name = "btnWriteFile";
      this.btnWriteFile.Size = new System.Drawing.Size(42, 24);
      this.btnWriteFile.TabIndex = 9;
      this.btnWriteFile.Text = "Write";
      this.btnWriteFile.UseVisualStyleBackColor = true;
      this.btnWriteFile.Click += new System.EventHandler(this.btnWriteFile_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(105, 7);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(38, 13);
      this.label3.TabIndex = 8;
      this.label3.Text = "Height";
      // 
      // nudHeight
      // 
      this.nudHeight.Location = new System.Drawing.Point(143, 4);
      this.nudHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudHeight.Name = "nudHeight";
      this.nudHeight.Size = new System.Drawing.Size(51, 20);
      this.nudHeight.TabIndex = 7;
      this.nudHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudHeight.Value = new decimal(new int[] {
            480,
            0,
            0,
            0});
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 8);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(35, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "Width";
      // 
      // nudWidth
      // 
      this.nudWidth.Location = new System.Drawing.Point(42, 5);
      this.nudWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudWidth.Name = "nudWidth";
      this.nudWidth.Size = new System.Drawing.Size(51, 20);
      this.nudWidth.TabIndex = 5;
      this.nudWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudWidth.Value = new decimal(new int[] {
            640,
            0,
            0,
            0});
      // 
      // pbxView
      // 
      this.pbxView.BackColor = System.Drawing.SystemColors.Info;
      this.pbxView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxView.Location = new System.Drawing.Point(0, 0);
      this.pbxView.Name = "pbxView";
      this.pbxView.Size = new System.Drawing.Size(697, 388);
      this.pbxView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pbxView.TabIndex = 1;
      this.pbxView.TabStop = false;
      // 
      // DialogLoadMatrixText
      // 
      this.DialogLoadMatrixText.DefaultExt = "txt";
      this.DialogLoadMatrixText.FileName = "Matrix";
      this.DialogLoadMatrixText.Filter = "Matrix Text Files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogLoadMatrixText.Title = "Load Matrix";
      // 
      // DialogSaveMatrixText
      // 
      this.DialogSaveMatrixText.DefaultExt = "txt";
      this.DialogSaveMatrixText.FileName = "Matrix";
      this.DialogSaveMatrixText.Filter = "Matrix Text Files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogSaveMatrixText.Title = "Save Matrix";
      // 
      // DialogLoadMatrixBinary
      // 
      this.DialogLoadMatrixBinary.DefaultExt = "bin";
      this.DialogLoadMatrixBinary.FileName = "Matrix";
      this.DialogLoadMatrixBinary.Filter = "Matrix Binary Files (*.bin)|*.bin|All files (*.*)|*.*";
      this.DialogLoadMatrixBinary.Title = "Load Matrix";
      // 
      // DialogSaveMatrixBinary
      // 
      this.DialogSaveMatrixBinary.DefaultExt = "bin";
      this.DialogSaveMatrixBinary.FileName = "Matrix";
      this.DialogSaveMatrixBinary.Filter = "Matrix Binary Files (*.bin)|*.bin|All files (*.*)|*.*";
      this.DialogSaveMatrixBinary.Title = "Save Matrix";
      // 
      // CUCMatrixBuilder
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pbxView);
      this.Controls.Add(this.pnlUser);
      this.Name = "CUCMatrixBuilder";
      this.Size = new System.Drawing.Size(697, 469);
      this.pnlUser.ResumeLayout(false);
      this.pnlUser.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudHeight)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudWidth)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxView)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlUser;
    private System.Windows.Forms.Button btnCreateMatrixConstant;
    private System.Windows.Forms.Button btnCreateMatrixCircular;
    private System.Windows.Forms.Button btnCreateMatrixLinear;
    private System.Windows.Forms.Button btnCreateMatrixNoise;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown nudHeight;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown nudWidth;
    private System.Windows.Forms.PictureBox pbxView;
    private System.Windows.Forms.Button btnWriteFile;
    private System.Windows.Forms.OpenFileDialog DialogLoadMatrixText;
    private System.Windows.Forms.SaveFileDialog DialogSaveMatrixText;
    private System.Windows.Forms.Button btnReadFile;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.RadioButton rbtBinary;
    private System.Windows.Forms.RadioButton rbtText;
    private System.Windows.Forms.OpenFileDialog DialogLoadMatrixBinary;
    private System.Windows.Forms.SaveFileDialog DialogSaveMatrixBinary;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.RadioButton rbtMatrix08Bit;
    private System.Windows.Forms.RadioButton rbtMatrix12Bit;
    private System.Windows.Forms.RadioButton rbtMatrix10Bit;
    private System.Windows.Forms.GroupBox groupBox3;
  }
}
