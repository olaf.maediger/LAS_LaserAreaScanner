﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using IPMemory;
using IPMatrix;
using IPPaletteBase;
using IPPalette256;
using IPBitmap;
//
namespace UCMatrixBuilder
{
  public partial class CUCMatrixOperator : UserControl
  { //
    //-----------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------
    //
    public const Int32 BITMAPWIDTH = 270;
    public const Int32 BITMAPHEIGHT = 210;
    //
    public const String COMMAND_LOADFROMFILE = "Load from File";
    public const String COMMAND_SAVETOTEXT = "Save to Text";
    public const String COMMAND_SAVETOBINARY = "Save to Binary";
    public const String COMMAND_NORM = "Norm";
    public const String COMMAND_SCALE = "Scale";
    public const String COMMAND_CREATECONSTANT = "Create Constant";
    public const String COMMAND_CREATELINEAR = "Create Linear";
    public const String COMMAND_CREATECIRCULAR = "Create Circular";
    public const String COMMAND_CREATENOISE = "Create Noise";
    public const String COMMAND_ADD = "Add";
    public const String COMMAND_SUBSTRACT = "Substract";
    public const String COMMAND_MULTIPLY = "Multiply";
    public const String COMMAND_DIVIDE = "Divide";
    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //
    private EPaletteKind FPaletteKind;
    private CMatrixBase FMatrix01;
    private CMatrixBase FMatrix02;
    private CMatrixBase FMatrix03;
    //
    //-----------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------
    //
    public CUCMatrixOperator()
    {
      InitializeComponent();
      //
      cbxMatrix01.Items.Clear();
      cbxMatrix01.Items.Add(COMMAND_LOADFROMFILE);
      cbxMatrix01.Items.Add(COMMAND_NORM);
      cbxMatrix01.Items.Add(COMMAND_SCALE);
      cbxMatrix01.Items.Add(COMMAND_CREATECONSTANT);
      cbxMatrix01.Items.Add(COMMAND_CREATELINEAR);
      cbxMatrix01.Items.Add(COMMAND_CREATECIRCULAR);
      cbxMatrix01.Items.Add(COMMAND_CREATENOISE);
      //
      cbxMatrix02.Items.Clear();
      cbxMatrix02.Items.Add(COMMAND_LOADFROMFILE);
      cbxMatrix02.Items.Add(COMMAND_NORM);
      cbxMatrix02.Items.Add(COMMAND_SCALE);
      cbxMatrix02.Items.Add(COMMAND_CREATECONSTANT);
      cbxMatrix02.Items.Add(COMMAND_CREATELINEAR);
      cbxMatrix02.Items.Add(COMMAND_CREATECIRCULAR);
      cbxMatrix02.Items.Add(COMMAND_CREATENOISE);
      //
      cbxMatrixOperation.Items.Clear();
      cbxMatrixOperation.Items.Add(COMMAND_ADD);
      cbxMatrixOperation.Items.Add(COMMAND_SUBSTRACT);
      cbxMatrixOperation.Items.Add(COMMAND_MULTIPLY);
      cbxMatrixOperation.Items.Add(COMMAND_DIVIDE);
      //
      cbxMatrix03.Items.Clear();
      cbxMatrix03.Items.Add(COMMAND_SAVETOTEXT);
      cbxMatrix03.Items.Add(COMMAND_SAVETOBINARY);
      cbxMatrix03.Items.Add(COMMAND_NORM);
      cbxMatrix03.Items.Add(COMMAND_SCALE);
    }
    //
    //-----------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------
    //
    private void CreateMatrixConstant(Int32 zconstant,
                                      ref CMatrixBase matrix, ref PictureBox picturebox)
    {
      matrix = null;
      picturebox.Image = null;
      if (rbtMatrix08Bit.Checked)
      {
        CMatrix08Bit Matrix08Bit;
        CBitmap256 Bitmap256;
        if (CBitmapBuilder.CreateBitmap08BitConstant(BITMAPWIDTH, BITMAPHEIGHT,
                                                     zconstant,
                                                     out Matrix08Bit, out Bitmap256))
        {
          matrix = Matrix08Bit;
          picturebox.Image = Bitmap256.Bitmap;
        }
      }
      else
        if (rbtMatrix10Bit.Checked)
        {
          CMatrix10Bit Matrix10Bit;
          CBitmap256 Bitmap256;
          if (CBitmapBuilder.CreateBitmap10BitConstant(BITMAPWIDTH, BITMAPHEIGHT,
                                                       zconstant,
                                                       out Matrix10Bit, out Bitmap256))
          {
            matrix = Matrix10Bit;
            picturebox.Image = Bitmap256.Bitmap;
          }
        }
        else
          if (rbtMatrix12Bit.Checked)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmap256 Bitmap256;
            if (CBitmapBuilder.CreateBitmap12BitConstant(BITMAPWIDTH, BITMAPHEIGHT,
                                                         zconstant,
                                                         out Matrix12Bit, out Bitmap256))
            {
              matrix = Matrix12Bit;
              picturebox.Image = Bitmap256.Bitmap;
            }
          }
    }

    private void CreateMatrixLinear(Int32 zminimum, Int32 zmaximum, 
                                    ref CMatrixBase matrix, ref PictureBox picturebox)
    {
      matrix = null;
      picturebox.Image = null;
      if (rbtMatrix08Bit.Checked)
      {
        CMatrix08Bit Matrix08Bit;
        CBitmap256 Bitmap256;
        if (CBitmapBuilder.CreateBitmap08BitLinear(BITMAPWIDTH, BITMAPHEIGHT,
                                                   zminimum, zmaximum, zminimum, zmaximum,
                                                   out Matrix08Bit, out Bitmap256))
        {
          matrix = Matrix08Bit;
          picturebox.Image = Bitmap256.Bitmap;
        }
      }
      else
        if (rbtMatrix10Bit.Checked)
        {
          CMatrix10Bit Matrix10Bit;
          CBitmap256 Bitmap256;
          if (CBitmapBuilder.CreateBitmap10BitLinear(BITMAPWIDTH, BITMAPHEIGHT, 
                                                     zminimum, zmaximum, zminimum, zmaximum,
                                                     out Matrix10Bit, out Bitmap256))
          {
            matrix = Matrix10Bit;
            picturebox.Image = Bitmap256.Bitmap;
          }
        }
        else
          if (rbtMatrix12Bit.Checked)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmap256 Bitmap256;
            if (CBitmapBuilder.CreateBitmap12BitLinear(BITMAPWIDTH, BITMAPHEIGHT, 
                                                       zminimum, zmaximum, zminimum, zmaximum,
                                                       out Matrix12Bit, out Bitmap256))
            {
              matrix = Matrix12Bit;
              picturebox.Image = Bitmap256.Bitmap;
            }
          }
    }

    private void CreateMatrixCircular(Int32 zminimum, Int32 zmaximum, 
                                      ref CMatrixBase matrix, ref PictureBox picturebox)
    {
      matrix = null;
      picturebox.Image = null;
      if (rbtMatrix08Bit.Checked)
      {
        CMatrix08Bit Matrix08Bit;
        CBitmap256 Bitmap256;
        if (CBitmapBuilder.CreateBitmap08BitCircular(BITMAPWIDTH, BITMAPHEIGHT, 0, 255,
                                                     out Matrix08Bit, out Bitmap256))
        {
          matrix = Matrix08Bit;
          picturebox.Image = Bitmap256.Bitmap;
        }
      }
      else
        if (rbtMatrix10Bit.Checked)
        {
          CMatrix10Bit Matrix10Bit;
          CBitmap256 Bitmap256;
          if (CBitmapBuilder.CreateBitmap10BitCircular(BITMAPWIDTH, BITMAPHEIGHT, 0, 1023,
                                                       out Matrix10Bit, out Bitmap256))
          {
            matrix = Matrix10Bit;
            picturebox.Image = Bitmap256.Bitmap;
          }
        }
        else
          if (rbtMatrix12Bit.Checked)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmap256 Bitmap256;
            if (CBitmapBuilder.CreateBitmap12BitCircular(BITMAPWIDTH, BITMAPHEIGHT, 0, 4095,
                                                         out Matrix12Bit, out Bitmap256))
            {
              matrix = Matrix12Bit;
              picturebox.Image = Bitmap256.Bitmap;
            }
          }
    }

    private void CreateMatrixNoise(Int32 zminimum, Int32 zmaximum,
                                   ref CMatrixBase matrix, ref PictureBox picturebox)
    {
      matrix = null;
      picturebox.Image = null;
      if (rbtMatrix08Bit.Checked)
      {
        CMatrix08Bit Matrix08Bit;
        CBitmap256 Bitmap256;
        if (CBitmapBuilder.CreateBitmap08BitNoise(BITMAPWIDTH, BITMAPHEIGHT, 
                                                  zminimum, zmaximum,
                                                  out Matrix08Bit, out Bitmap256))
        {
          matrix = Matrix08Bit;
          picturebox.Image = Bitmap256.Bitmap;
        }
      }
      else
        if (rbtMatrix10Bit.Checked)
        {
          CMatrix10Bit Matrix10Bit;
          CBitmap256 Bitmap256;
          if (CBitmapBuilder.CreateBitmap10BitNoise(BITMAPWIDTH, BITMAPHEIGHT,
                                                    zminimum, zmaximum,
                                                    out Matrix10Bit, out Bitmap256))
          {
            matrix = Matrix10Bit;
            picturebox.Image = Bitmap256.Bitmap;
          }
        }
        else
          if (rbtMatrix12Bit.Checked)
          {
            CMatrix12Bit Matrix12Bit;
            CBitmap256 Bitmap256;
            if (CBitmapBuilder.CreateBitmap12BitNoise(BITMAPWIDTH, BITMAPHEIGHT, 
                                                      zminimum, zmaximum,
                                                      out Matrix12Bit, out Bitmap256))
            {
              matrix = Matrix12Bit;
              picturebox.Image = Bitmap256.Bitmap;
            }
          }
    }

    private void Matrix01Add02To03()
    { // Matrix01
      Boolean M01B08 = (FMatrix01 is CMatrix08Bit);
      Boolean M01B10 = (FMatrix01 is CMatrix10Bit);
      Boolean M01B12 = (FMatrix01 is CMatrix12Bit);
      CMatrixDouble MD01 = null;
      if (M01B08)
      {
        CMatrixConversion.Convert08BitToDouble((CMatrix08Bit)FMatrix01, out MD01);
      }
      else
        if (M01B08)
        {
          CMatrixConversion.Convert10BitToDouble((CMatrix10Bit)FMatrix01, out MD01);
        }
        else
          if (M01B08)
          {
            CMatrixConversion.Convert12BitToDouble((CMatrix12Bit)FMatrix01, out MD01);
          }
      // FMatrix02
      Boolean M02B08 = (FMatrix02 is CMatrix08Bit);
      Boolean M02B10 = (FMatrix02 is CMatrix10Bit);
      Boolean M02B12 = (FMatrix02 is CMatrix12Bit);
      CMatrixDouble MD02 = null;
      if (M02B08)
      {
        CMatrixConversion.Convert08BitToDouble((CMatrix08Bit)FMatrix02, out MD02);
      }
      else
        if (M02B08)
        {
          CMatrixConversion.Convert10BitToDouble((CMatrix10Bit)FMatrix02, out MD02);
        }
        else
          if (M02B08)
          {
            CMatrixConversion.Convert12BitToDouble((CMatrix12Bit)FMatrix02, out MD02);
          }
      // Double Operation
      CMatrixDouble MDResult;
      CMatrixOperation.Add(MD01, MD02, out MDResult);
      // Conversion to preset BitCount
      if (rbtMatrix08Bit.Checked)
      {
        CMatrix08Bit Matrix08Bit;
        CMatrixConversion.ConvertDoubleTo08Bit(MDResult, out Matrix08Bit);
        FMatrix03 = Matrix08Bit;
        CBitmap256 Bitmap256 = new CBitmap256(FPaletteKind, Matrix08Bit);
        pbxMatrix03.Image = Bitmap256.Bitmap;
      }
      else
        if (rbtMatrix10Bit.Checked)
        {
          CMatrix10Bit Matrix10Bit;
          CMatrixConversion.ConvertDoubleTo10Bit(MDResult, out Matrix10Bit);
          FMatrix03 = Matrix10Bit;
          CBitmap256 Bitmap256 = new CBitmap256(FPaletteKind, Matrix10Bit);
          pbxMatrix03.Image = Bitmap256.Bitmap;
        }
        else
          if (rbtMatrix12Bit.Checked)
          {
            CMatrix12Bit Matrix12Bit;
            CMatrixConversion.ConvertDoubleTo12Bit(MDResult, out Matrix12Bit);
            FMatrix03 = Matrix12Bit;
            CBitmap256 Bitmap256 = new CBitmap256(FPaletteKind, Matrix12Bit);
            pbxMatrix03.Image = Bitmap256.Bitmap;
          }
    }

    private void Matrix01Substract02To03()
    { // Matrix01
      Boolean M01B08 = (FMatrix01 is CMatrix08Bit);
      Boolean M01B10 = (FMatrix01 is CMatrix10Bit);
      Boolean M01B12 = (FMatrix01 is CMatrix12Bit);
      CMatrixDouble MD01 = null;
      if (M01B08)
      {
        CMatrixConversion.Convert08BitToDouble((CMatrix08Bit)FMatrix01, out MD01);
      }
      else
        if (M01B08)
        {
          CMatrixConversion.Convert10BitToDouble((CMatrix10Bit)FMatrix01, out MD01);
        }
        else
          if (M01B08)
          {
            CMatrixConversion.Convert12BitToDouble((CMatrix12Bit)FMatrix01, out MD01);
          }
      // FMatrix02
      Boolean M02B08 = (FMatrix02 is CMatrix08Bit);
      Boolean M02B10 = (FMatrix02 is CMatrix10Bit);
      Boolean M02B12 = (FMatrix02 is CMatrix12Bit);
      CMatrixDouble MD02 = null;
      if (M02B08)
      {
        CMatrixConversion.Convert08BitToDouble((CMatrix08Bit)FMatrix02, out MD02);
      }
      else
        if (M02B08)
        {
          CMatrixConversion.Convert10BitToDouble((CMatrix10Bit)FMatrix02, out MD02);
        }
        else
          if (M02B08)
          {
            CMatrixConversion.Convert12BitToDouble((CMatrix12Bit)FMatrix02, out MD02);
          }
      // Double Operation
      CMatrixDouble MDResult;
      CMatrixOperation.Substract(MD01, MD02, out MDResult);
      // Conversion to preset BitCount
      if (rbtMatrix08Bit.Checked)
      {
        CMatrix08Bit Matrix08Bit;
        CMatrixConversion.ConvertDoubleTo08Bit(MDResult, out Matrix08Bit);
        FMatrix03 = Matrix08Bit;
        CBitmap256 Bitmap256 = new CBitmap256(FPaletteKind, Matrix08Bit);
        pbxMatrix03.Image = Bitmap256.Bitmap;
      }
      else
        if (rbtMatrix10Bit.Checked)
        {
          CMatrix10Bit Matrix10Bit;
          CMatrixConversion.ConvertDoubleTo10Bit(MDResult, out Matrix10Bit);
          FMatrix03 = Matrix10Bit;
          CBitmap256 Bitmap256 = new CBitmap256(FPaletteKind, Matrix10Bit);
          pbxMatrix03.Image = Bitmap256.Bitmap;
        }
        else
          if (rbtMatrix12Bit.Checked)
          {
            CMatrix12Bit Matrix12Bit;
            CMatrixConversion.ConvertDoubleTo12Bit(MDResult, out Matrix12Bit);
            FMatrix03 = Matrix12Bit;
            CBitmap256 Bitmap256 = new CBitmap256(FPaletteKind, Matrix12Bit);
            pbxMatrix03.Image = Bitmap256.Bitmap;
          }
    }

    private Boolean NormMatrix(ref CMatrixBase matrix)
    {
      Int32 ZL = (Int32)nudArgument0.Value;
      Int32 ZH = (Int32)nudArgument1.Value;
      if (matrix is CMatrix08Bit)
      {
        CMatrix08Bit M08Bit = (CMatrix08Bit)matrix;
        CMatrixDouble MDSource, MDTarget;
        if (CMatrixConversion.Convert08BitToDouble(M08Bit, out MDSource))
        {
          if (CMatrixOperation.Norm(MDSource, ZL, ZH, out MDTarget))
          {
            CMatrix08Bit M08BitResult;
            if (CMatrixConversion.ConvertDoubleTo08Bit(MDTarget, out M08BitResult))
            {
              matrix = M08BitResult;
              return true;
            }
          }
        }
      }
      if (matrix is CMatrixDouble)
      {
        CMatrixDouble MDSource = (CMatrixDouble)matrix;
        CMatrixDouble MDTarget;
        if (CMatrixOperation.Norm(MDSource, ZL, ZH, out MDTarget))
        {
          matrix = MDTarget;
          return true;
        }
      }
      return false;
    }


    //
    //-----------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------
    //
    private void cbxMatrix01_SelectedIndexChanged(object sender, EventArgs e)
    {
      switch (cbxMatrix01.Text)
      {
        case COMMAND_LOADFROMFILE:           
          break;
        case COMMAND_NORM:
          NormMatrix(ref FMatrix01);
          CBitmap256 B01 = new CBitmap256(FPaletteKind, FMatrix01);
          pbxMatrix01.Image = B01.Bitmap;
          break;
        case COMMAND_SCALE:
          break;
        case COMMAND_CREATECONSTANT:
          Int32 C = (Int32)((nudArgument0.Value + nudArgument1.Value) / 2);
          CreateMatrixConstant(C, ref FMatrix01, ref pbxMatrix01);
          break;
        case COMMAND_CREATELINEAR:
          Int32 LZL = (Int32)nudArgument0.Value;
          Int32 LZH = (Int32)nudArgument1.Value;
          CreateMatrixLinear(LZL, LZH, ref FMatrix01, ref pbxMatrix01);
          break;
        case COMMAND_CREATECIRCULAR:
          Int32 CZL = (Int32)nudArgument0.Value;
          Int32 CZH = (Int32)nudArgument1.Value;
          CreateMatrixCircular(CZL, CZH, ref FMatrix01, ref pbxMatrix01);
          break;
        case COMMAND_CREATENOISE:
          Int32 NZL = (Int32)nudArgument0.Value;
          Int32 NZH = (Int32)nudArgument1.Value;
          CreateMatrixNoise(NZL, NZH, ref FMatrix01, ref pbxMatrix01);
          break;
      }
    }

    private void cbxMatrix02_SelectedIndexChanged(object sender, EventArgs e)
    {
      switch (cbxMatrix02.Text)
      {
        case COMMAND_LOADFROMFILE:
          break;
        case COMMAND_NORM:
          NormMatrix(ref FMatrix02);
          CBitmap256 B02 = new CBitmap256(FPaletteKind, FMatrix02);
          pbxMatrix02.Image = B02.Bitmap;
          break;
        case COMMAND_SCALE:
          break;
        case COMMAND_CREATECONSTANT:
          Int32 C = (Int32)((nudArgument0.Value + nudArgument1.Value) / 2);
          CreateMatrixConstant(C, ref FMatrix02, ref pbxMatrix02);
          break;
        case COMMAND_CREATELINEAR:
          Int32 LZL = (Int32)nudArgument0.Value;
          Int32 LZH = (Int32)nudArgument1.Value;
          CreateMatrixLinear(LZL, LZH, ref FMatrix02, ref pbxMatrix02);
          break;
        case COMMAND_CREATECIRCULAR:
          Int32 CZL = (Int32)nudArgument0.Value;
          Int32 CZH = (Int32)nudArgument1.Value;
          CreateMatrixCircular(CZL, CZH, ref FMatrix02, ref pbxMatrix02);
          break;
        case COMMAND_CREATENOISE:
          Int32 NZL = (Int32)nudArgument0.Value;
          Int32 NZH = (Int32)nudArgument1.Value;
          CreateMatrixNoise(NZL, NZH, ref FMatrix02, ref pbxMatrix02);
          break;
      }
    }

    private void cbxMatrixOperation_SelectedIndexChanged(object sender, EventArgs e)
    {
      switch (cbxMatrixOperation.Text)
      {
        case COMMAND_ADD:
          Matrix01Add02To03();
          break;
        case COMMAND_SUBSTRACT:
          Matrix01Substract02To03();
          break;
        case COMMAND_MULTIPLY:
          break;
        case COMMAND_DIVIDE:
          break;
      }
    }

    private void cbxMatrix03_SelectedIndexChanged(object sender, EventArgs e)
    {
      switch (cbxMatrix03.Text)
      {
        case COMMAND_SAVETOTEXT:
          break;
        case COMMAND_SAVETOBINARY:
          break;
        case COMMAND_NORM:
          NormMatrix(ref FMatrix03);
          CBitmap256 B03 = new CBitmap256(FPaletteKind, FMatrix03);
          pbxMatrix03.Image = B03.Bitmap;
          break;
        case COMMAND_SCALE:
          break;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Remote Control
    //---------------------------------------------------------------------------------
    //
    public void SelectMatrix08Bit()
    {
      rbtMatrix08Bit.Checked = true;
    }
    public void SelectMatrix10Bit()
    {
      rbtMatrix10Bit.Checked = true;
    }
    public void SelectMatrix12Bit()
    {
      rbtMatrix12Bit.Checked = true;
    }

    public void SetPaletteKind(EPaletteKind palettekind)
    {
      FPaletteKind = palettekind;
    }

    public void HideMatrix01()
    {
      FMatrix01 = null;
      pbxMatrix01.Image = null;
    }
    public void HideMatrix02()
    {
      FMatrix02 = null;
      pbxMatrix02.Image = null;
    }
    public void HideMatrix03()
    {
      FMatrix03 = null;
      pbxMatrix03.Image = null;
    }

    private void SelectCommandMatrix01(String command)
    {
      for (Int32 CI = 0; CI <= cbxMatrix01.Items.Count; CI++)
      {
        if (command == (String)(cbxMatrix01.Items[CI]))
        {
          cbxMatrix01.SelectedIndex = CI;
          return;
        }
      }
    }
    private void SelectCommandMatrix02(String command)
    {
      for (Int32 CI = 0; CI <= cbxMatrix02.Items.Count; CI++)
      {
        if (command == (String)(cbxMatrix02.Items[CI]))
        {
          cbxMatrix02.SelectedIndex = CI;
          return;
        }
      }
    }
    private void SelectCommandMatrix03(String command)
    {
      for (Int32 CI = 0; CI <= cbxMatrix03.Items.Count; CI++)
      {
        if (command == (String)(cbxMatrix03.Items[CI]))
        {
          cbxMatrix03.SelectedIndex = CI;
          return;
        }
      }
    }
    private void SelectOperation(String operation)
    {
      for (Int32 CI = 0; CI <= cbxMatrixOperation.Items.Count; CI++)
      {
        if (operation == (String)(cbxMatrixOperation.Items[CI]))
        {
          cbxMatrixOperation.SelectedIndex = CI;
          return;
        }
      }
    }

    public void SelectMatrix01(String command, String argument0, String argument1)
    {
      Decimal DA0 = 0;
      Decimal.TryParse(argument0, out DA0);
      nudArgument0.Value = DA0;
      //
      Decimal DA1 = 255;
      Decimal.TryParse(argument1, out DA1);
      nudArgument1.Value = DA1;
      //
      SelectCommandMatrix01(command);
    }

    public void SelectMatrix02(String command, String argument0, String argument1)
    {
      Decimal DA0 = 0;
      Decimal.TryParse(argument0, out DA0);
      nudArgument0.Value = DA0;
      //
      Decimal DA1 = 255;
      Decimal.TryParse(argument1, out DA1);
      nudArgument1.Value = DA1;
      //
      SelectCommandMatrix02(command);
    }

    public void SelectMatrix03(String command, String argument0, String argument1)
    {
      Decimal DA0 = 0;
      Decimal.TryParse(argument0, out DA0);
      nudArgument0.Value = DA0;
      //
      Decimal DA1 = 255;
      Decimal.TryParse(argument1, out DA1);
      nudArgument1.Value = DA1;
      //
      SelectCommandMatrix03(command);
    }

    public void AddMatrix010203()
    {
      cbxMatrixOperation.SelectedItem = null;
      cbxMatrixOperation.SelectedItem = "Add";
    }

    public void SubstractMatrix010203()
    {
      cbxMatrixOperation.SelectedItem = null;
      cbxMatrixOperation.SelectedItem = "Substract";
    }

    public void MultiplyMatrix010203()
    {
      cbxMatrixOperation.SelectedItem = null;
      cbxMatrixOperation.SelectedItem = "Multiply";
    }

    public void DivideMatrix010203()
    {
      cbxMatrixOperation.SelectedItem = null;
      cbxMatrixOperation.SelectedItem = "Devide";
    }



  }
}
