﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
//
using IPMemory;
using IPMatrix;
using IPPaletteBase;
using IPPalette256;
using IPBitmap;
//
namespace UCImageControls
{
  public struct RDataReadWriteImage
  {
    public DOnBitmapRead OnBitmapRead;
    public DOnBitmapRgbRead OnBitmapRgbRead;
    public DOnBitmapArgbRead OnBitmapArgbRead;
    public DOnBitmap256Read OnBitmap256Read;
    //
    public DOnBitmapWrite OnBitmapWrite;
    public DOnBitmapRgbWrite OnBitmapRgbWrite;
    public DOnBitmapArgbWrite OnBitmapArgbWrite;
    public DOnBitmap256Write OnBitmap256Write;
    //
    public RDataReadWriteImage(Int32 init)
    {
      OnBitmapRead = null;
      OnBitmapRgbRead = null;
      OnBitmapArgbRead = null;
      OnBitmap256Read = null;
      //
      OnBitmapWrite = null;
      OnBitmapRgbWrite = null;
      OnBitmapArgbWrite = null;
      OnBitmap256Write = null;
    }
  }
  //
  public partial class CUCReadWriteBitmap : UserControl
  {
    //
    //-----------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------
    //
    private const Int32 INDEX_BMP = 0;
    private const Int32 INDEX_GIF = 1;
    private const Int32 INDEX_JPG = 2;
    private const Int32 INDEX_PNG = 3;
    private const Int32 INDEX_TIF = 4;
    private const Int32 INDEX_ICON = 5;
    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //
    private Bitmap FBitmap;
    private CBitmapRgb FBitmapRgb;
    private CBitmapArgb FBitmapArgb;
    private CBitmap256 FBitmap256;
    //
    private RDataReadWriteImage FData;
    //
    //-----------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------
    //
    public CUCReadWriteBitmap()
    {
      InitializeComponent();
      cbxBitmapFormat.SelectedIndex = INDEX_BMP;
    }
    //
    //-----------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------
    //
    public Boolean GetData(out RDataReadWriteImage data)
    {
      data = FData;
      return true;
    }

    public Boolean SetData(RDataReadWriteImage data)
    {
      FData = data;
      return true;
    }

    public void SetBitmap(Bitmap bitmap)
    {
      FBitmap = bitmap;
      FBitmapRgb = null;
      FBitmapArgb = null;
      FBitmap256 = null;
    }

    public void SetBitmapRgb(CBitmapRgb bitmaprgb)
    {
      FBitmap = null;
      FBitmapRgb = bitmaprgb;
      FBitmapArgb = null;
      FBitmap256 = null;
    }

    public void SetBitmapArgb(CBitmapArgb bitmapargb)
    {
      FBitmap = null;
      FBitmapRgb = null;
      FBitmapArgb = bitmapargb;
      FBitmap256 = null;
    }

    public void SetBitmap256(CBitmap256 bitmap256)
    {
      FBitmap = null;
      FBitmapRgb = null;
      FBitmapArgb = null;
      FBitmap256 = bitmap256;
    }
    //
    //-----------------------------------------------------------
    //  Section - Helper - Write
    //-----------------------------------------------------------
    //
    private String GetSaveSelectedExtension()
    {
      switch (cbxBitmapFormat.SelectedIndex)
      {
        case INDEX_GIF:
          return "gif";
        case INDEX_JPG:
          return "jpg";
        case INDEX_PNG:
          return "png";
        case INDEX_TIF:
          return "tif";
        case INDEX_ICON:
          return "ico";
        default: // INDEX_BMP
          return "bmp";
      }
    }

    private ImageFormat GetSaveSelectedImageFormat()
    {
      switch (cbxBitmapFormat.SelectedIndex)
      {
        case INDEX_GIF:
          return ImageFormat.Gif;
        case INDEX_JPG:
          return ImageFormat.Jpeg;
        case INDEX_PNG:
          return ImageFormat.Png;
        case INDEX_TIF:
          return ImageFormat.Tiff;
        case INDEX_ICON:
          return ImageFormat.Icon;
        default: // INDEX_BMP
          return ImageFormat.Bmp;
      }
    }

    private Boolean WriteBitmap()
    {
      try
      {
        if (FBitmap is Bitmap)
        {
          DialogSaveImage.DefaultExt = GetSaveSelectedExtension();
          if (DialogResult.OK == DialogSaveImage.ShowDialog())
          {
            FBitmap.Save(DialogSaveImage.FileName, GetSaveSelectedImageFormat());
            return true;
          }
        }
        if (FBitmapRgb is CBitmapRgb)
        {
          DialogSaveImage.DefaultExt = GetSaveSelectedExtension();
          if (DialogResult.OK == DialogSaveImage.ShowDialog())
          {
            FBitmapRgb.Bitmap.Save(DialogSaveImage.FileName, GetSaveSelectedImageFormat());
            return true;
          }
        }
        if (FBitmapArgb is CBitmapArgb)
        {
          DialogSaveImage.DefaultExt = GetSaveSelectedExtension();
          if (DialogResult.OK == DialogSaveImage.ShowDialog())
          {
            FBitmapArgb.Bitmap.Save(DialogSaveImage.FileName, GetSaveSelectedImageFormat());
            return true;
          }
        }
        if (FBitmap256 is CBitmap256)
        {
          DialogSaveImage.DefaultExt = GetSaveSelectedExtension();
          if (DialogResult.OK == DialogSaveImage.ShowDialog())
          {
            FBitmapArgb.Bitmap.Save(DialogSaveImage.FileName, GetSaveSelectedImageFormat());
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------
    //  Section - Helper - Read
    //-----------------------------------------------------------
    //
    private Boolean ReadBitmap()
    {
      try
      {
        DialogLoadImage.DefaultExt = GetSaveSelectedExtension();
        if (DialogResult.OK == DialogLoadImage.ShowDialog())
        {
          if (File.Exists(DialogLoadImage.FileName))
          {
            Bitmap BitmapRead = new Bitmap(DialogLoadImage.FileName);
            if (BitmapRead is Bitmap)
            {
              SetBitmap(BitmapRead);
              if (FData.OnBitmapRead is DOnBitmapRead)
              {
                FData.OnBitmapRead(FBitmap);
              }
              return true;
            }
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    //
    //-----------------------------------------------------------
    //  Section - Event - Write
    //-----------------------------------------------------------
    //
    private void btnWriteFile_Click(object sender, EventArgs e)
    {
      WriteBitmap();
    }
    //
    //-----------------------------------------------------------
    //  Section - Event - Read
    //-----------------------------------------------------------
    //
    private void btnReadFile_Click(object sender, EventArgs e)
    {
      ReadBitmap();
    }




  }
}
