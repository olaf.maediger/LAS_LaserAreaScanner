﻿namespace UCImageControls
{
  partial class CUCImageDimension
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label3 = new System.Windows.Forms.Label();
      this.nudHeight = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.nudWidth = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.nudHeight)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudWidth)).BeginInit();
      this.SuspendLayout();
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(115, 5);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(41, 13);
      this.label3.TabIndex = 12;
      this.label3.Text = "-Height";
      // 
      // nudHeight
      // 
      this.nudHeight.Location = new System.Drawing.Point(156, 3);
      this.nudHeight.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudHeight.Name = "nudHeight";
      this.nudHeight.Size = new System.Drawing.Size(51, 20);
      this.nudHeight.TabIndex = 11;
      this.nudHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudHeight.Value = new decimal(new int[] {
            480,
            0,
            0,
            0});
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(0, 5);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(67, 13);
      this.label2.TabIndex = 10;
      this.label2.Text = "Image-Width";
      // 
      // nudWidth
      // 
      this.nudWidth.Location = new System.Drawing.Point(66, 3);
      this.nudWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudWidth.Name = "nudWidth";
      this.nudWidth.Size = new System.Drawing.Size(51, 20);
      this.nudWidth.TabIndex = 9;
      this.nudWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudWidth.Value = new decimal(new int[] {
            640,
            0,
            0,
            0});
      // 
      // CUCImageDimension
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.nudHeight);
      this.Controls.Add(this.nudWidth);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Name = "CUCImageDimension";
      this.Size = new System.Drawing.Size(210, 26);
      ((System.ComponentModel.ISupportInitialize)(this.nudHeight)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudWidth)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown nudHeight;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown nudWidth;
  }
}
