﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
//
using IPMemory;
using IPMatrix;
using IPPaletteBase;
using IPPalette256;
using IPBitmap;
//
namespace UCImageControls
{
  //
  //-----------------------------------------------------------
  //  Section - Type
  //-----------------------------------------------------------
  //
  public struct RDataReadWriteMatrix
  {
    public DOnMatrix08BitRead OnMatrix08BitRead;
    public DOnMatrix10BitRead OnMatrix10BitRead;
    public DOnMatrix12BitRead OnMatrix12BitRead;
    public DOnMatrix16BitRead OnMatrix16BitRead;
    public DOnMatrixByteRead OnMatrixByteRead;
    public DOnMatrixUInt10Read OnMatrixUInt10Read;
    public DOnMatrixUInt12Read OnMatrixUInt12Read;
    public DOnMatrixUInt16Read OnMatrixUInt16Read;
    //
    public DOnMatrix08BitWrite OnMatrix08BitWrite;
    public DOnMatrix10BitWrite OnMatrix10BitWrite;
    public DOnMatrix12BitWrite OnMatrix12BitWrite;
    public DOnMatrix16BitWrite OnMatrix16BitWrite;
    public DOnMatrixByteWrite OnMatrixByteWrite;
    public DOnMatrixUInt10Write OnMatrixUInt10Write;
    public DOnMatrixUInt12Write OnMatrixUInt12Write;
    public DOnMatrixUInt16Write OnMatrixUInt16Write;
    //
    public RDataReadWriteMatrix(Int32 init)
    {
      OnMatrix08BitRead = null;
      OnMatrix10BitRead = null;
      OnMatrix12BitRead = null;
      OnMatrix16BitRead = null;
      OnMatrixByteRead = null;
      OnMatrixUInt10Read = null;
      OnMatrixUInt12Read = null;
      OnMatrixUInt16Read = null;
      //
      OnMatrix08BitWrite = null;
      OnMatrix10BitWrite = null;
      OnMatrix12BitWrite = null;
      OnMatrix16BitWrite = null;
      OnMatrixByteWrite = null;
      OnMatrixUInt10Write = null;
      OnMatrixUInt12Write = null;
      OnMatrixUInt16Write = null;
    }
  }
  //
  public partial class CUCReadWriteMatrix : UserControl
  {
    //
    //-----------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------
    //
    private const Int32 INDEX_TEXT = 0;
    private const Int32 INDEX_BINARY = 1;
    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //
    private CMatrix08Bit FMatrix08Bit;
    private CMatrix10Bit FMatrix10Bit;
    private CMatrix12Bit FMatrix12Bit;
    private CMatrix16Bit FMatrix16Bit;
    private Byte[,] FMatrixByte;
    private UInt16[,] FMatrixUInt10;
    private UInt16[,] FMatrixUInt12;
    private UInt16[,] FMatrixUInt16;
    //
    private RDataReadWriteMatrix FData; 
    //
    //-----------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------
    //
    public CUCReadWriteMatrix()
    {
      InitializeComponent();
      cbxMatrixFormat.SelectedIndex = INDEX_TEXT;
    }
    //
    //-----------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------
    //
    public Boolean GetData(out RDataReadWriteMatrix data)
    {
      data = FData;
      return true;
    }

    public Boolean SetData(RDataReadWriteMatrix data)
    {
      FData = data;
      return true;
    }

    public void SetMatrix08Bit(CMatrix08Bit matrix08bit)
    {
      FMatrix08Bit = matrix08bit;
      FMatrix10Bit = null;
      FMatrix12Bit = null;
      FMatrix16Bit = null;
      FMatrixByte = null;
      FMatrixUInt10 = null;
      FMatrixUInt12 = null;
      FMatrixUInt16 = null;
    }

    public void SetMatrix10Bit(CMatrix10Bit matrix10bit)
    {
      FMatrix08Bit = null;
      FMatrix10Bit = matrix10bit; 
      FMatrix12Bit = null;
      FMatrix16Bit = null;
      FMatrixByte = null;
      FMatrixUInt10 = null;
      FMatrixUInt12 = null;
      FMatrixUInt16 = null;
    }
    public void SetMatrix12Bit(CMatrix12Bit matrix12bit)
    {
      FMatrix08Bit = null;
      FMatrix10Bit = null;
      FMatrix12Bit = matrix12bit; 
      FMatrix16Bit = null;
      FMatrixByte = null;
      FMatrixUInt10 = null;
      FMatrixUInt12 = null;
      FMatrixUInt16 = null;
    }
    public void SetMatrix16Bit(CMatrix16Bit matrix16bit)
    {
      FMatrix08Bit = null;
      FMatrix10Bit = null;
      FMatrix12Bit = null;
      FMatrix16Bit = matrix16bit;
      FMatrixByte = null;
      FMatrixUInt10 = null;
      FMatrixUInt12 = null;
      FMatrixUInt16 = null;
    }
    public void SetMatrixByte(Byte[,] matrixbyte)
    {
      FMatrix08Bit = null;
      FMatrix10Bit = null;
      FMatrix12Bit = null;
      FMatrix16Bit = null;
      FMatrixByte = matrixbyte;
      FMatrixUInt10 = null;
      FMatrixUInt12 = null;
      FMatrixUInt16 = null;
    }
    public void SetMatrixUInt10(UInt16[,] matrixuint10)
    {
      FMatrix08Bit = null;
      FMatrix10Bit = null;
      FMatrix12Bit = null;
      FMatrix16Bit = null;
      FMatrixByte = null;
      FMatrixUInt10 = matrixuint10;
      FMatrixUInt12 = null;
      FMatrixUInt16 = null;
    }
    public void SetMatrixUInt12(UInt16[,] matrixuint12)
    {
      FMatrix08Bit = null;
      FMatrix10Bit = null;
      FMatrix12Bit = null;
      FMatrix16Bit = null;
      FMatrixByte = null;
      FMatrixUInt10 = null;
      FMatrixUInt12 = matrixuint12;
      FMatrixUInt16 = null;
    }
    public void SetMatrixUInt16(UInt16[,] matrixuint16)
    {
      FMatrix08Bit = null;
      FMatrix10Bit = null;
      FMatrix12Bit = null;
      FMatrix16Bit = null;
      FMatrixByte = null;
      FMatrixUInt10 = null;
      FMatrixUInt12 = null;
      FMatrixUInt16 = matrixuint16;
    }
    //
    //-----------------------------------------------------------
    //  Section - Helper - Write
    //-----------------------------------------------------------
    //
    private Boolean WriteMatrixText()
    {
      try
      {
        if (FMatrix08Bit is CMatrix08Bit)
        {
          DialogSaveMatrixText.FileName = FMatrix08Bit.Name + ".txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.WriteMatrix08Bit(FMatrix08Bit);
            MatrixFileText.Close();
            return true;
          }
        }
        if (FMatrix10Bit is CMatrix10Bit)
        {
          DialogSaveMatrixText.FileName = FMatrix10Bit.Name + ".txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.WriteMatrix10Bit(FMatrix10Bit);
            MatrixFileText.Close();
            return true;
          }
        }
        if (FMatrix12Bit is CMatrix12Bit)
        {
          DialogSaveMatrixText.FileName = FMatrix12Bit.Name + ".txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.WriteMatrix12Bit(FMatrix12Bit);
            MatrixFileText.Close();
            return true;
          }
        }
        if (FMatrix16Bit is CMatrix16Bit)
        {
          DialogSaveMatrixText.FileName = FMatrix16Bit.Name + ".txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.WriteMatrix16Bit(FMatrix16Bit);
            MatrixFileText.Close();
            return true;
          }
        }
        if (FMatrixByte is Byte[,])
        {
          DialogSaveMatrixText.FileName = "MatrixByte.txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.WriteMatrixByte(FMatrixByte);
            MatrixFileText.Close();
            return true;
          }
        }
        if (FMatrixUInt10 is UInt16[,])
        {
          DialogSaveMatrixText.FileName = "MatrixUInt10.txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.WriteMatrixUInt10(FMatrixUInt10);
            MatrixFileText.Close();
            return true;
          }
        }
        if (FMatrixUInt12 is UInt16[,])
        {
          DialogSaveMatrixText.FileName = "MatrixUInt12.txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.WriteMatrixUInt12(FMatrixUInt12);
            MatrixFileText.Close();
            return true;
          }
        }
        if (FMatrixUInt10 is UInt16[,])
        {
          DialogSaveMatrixText.FileName = "MatrixUInt10.txt";
          if (DialogResult.OK == DialogSaveMatrixText.ShowDialog())
          {
            CMatrixFileText MatrixFileText = new CMatrixFileText();
            MatrixFileText.OpenWrite(DialogSaveMatrixText.FileName);
            MatrixFileText.WriteMatrixUInt16(FMatrixUInt16);
            MatrixFileText.Close();
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean WriteMatrixBinary()
    {
      try
      {
        if (FMatrix08Bit is CMatrix08Bit)
        {
          DialogSaveMatrixBinary.FileName = FMatrix08Bit.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.WriteMatrix08Bit(FMatrix08Bit);
            MatrixFileBinary.Close();
            return true;
          }
        }
        if (FMatrix10Bit is CMatrix10Bit)
        {
          DialogSaveMatrixBinary.FileName = FMatrix10Bit.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.WriteMatrix10Bit(FMatrix10Bit);
            MatrixFileBinary.Close();
            return true;
          }
        }
        if (FMatrix12Bit is CMatrix12Bit)
        {
          DialogSaveMatrixBinary.FileName = FMatrix12Bit.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.WriteMatrix12Bit(FMatrix12Bit);
            MatrixFileBinary.Close();
            return true;
          }
        }
        if (FMatrix16Bit is CMatrix16Bit)
        {
          DialogSaveMatrixBinary.FileName = FMatrix12Bit.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.WriteMatrix16Bit(FMatrix16Bit);
            MatrixFileBinary.Close();
            return true;
          }
        }
        if (FMatrixByte is Byte[,])
        {
          DialogSaveMatrixBinary.FileName = FMatrix12Bit.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.WriteMatrixByte(FMatrixByte);
            MatrixFileBinary.Close();
            return true;
          }
        }
        if (FMatrixUInt10 is UInt16[,])
        {
          DialogSaveMatrixBinary.FileName = FMatrix12Bit.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.WriteMatrixUInt10(FMatrixUInt10);
            MatrixFileBinary.Close();
            return true;
          }
        }
        if (FMatrixUInt12 is UInt16[,])
        {
          DialogSaveMatrixBinary.FileName = FMatrix12Bit.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.WriteMatrixUInt12(FMatrixUInt12);
            MatrixFileBinary.Close();
            return true;
          }
        }
        if (FMatrixUInt16 is UInt16[,])
        {
          DialogSaveMatrixBinary.FileName = FMatrix12Bit.Name + ".bin";
          if (DialogResult.OK == DialogSaveMatrixBinary.ShowDialog())
          {
            CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
            MatrixFileBinary.OpenWrite(DialogSaveMatrixBinary.FileName);
            MatrixFileBinary.WriteMatrixUInt16(FMatrixUInt16);
            MatrixFileBinary.Close();
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------
    //  Section - Callback - MatrixTextFile - Read
    //-----------------------------------------------------------
    //
    private void TextFileDataOnMatrix08BitRead(CMatrix08Bit matrix)
    {
      if (FData.OnMatrix08BitRead is DOnMatrix08BitRead)
      {
        FData.OnMatrix08BitRead(matrix);
      }
    }
    private void TextFileDataOnMatrix10BitRead(CMatrix10Bit matrix)
    {
      if (FData.OnMatrix10BitRead is DOnMatrix10BitRead)
      {
        FData.OnMatrix10BitRead(matrix);
      }
    }
    private void TextFileDataOnMatrix12BitRead(CMatrix12Bit matrix)
    {
      if (FData.OnMatrix12BitRead is DOnMatrix12BitRead)
      {
        FData.OnMatrix12BitRead(matrix);
      }
    }
    private void TextFileDataOnMatrix16BitRead(CMatrix16Bit matrix)
    {
      if (FData.OnMatrix16BitRead is DOnMatrix16BitRead)
      {
        FData.OnMatrix16BitRead(matrix);
      }
    }
    private void TextFileDataOnMatrixByteRead(Byte[,] matrix)
    {
      if (FData.OnMatrixByteRead is DOnMatrixByteRead)
      {
        FData.OnMatrixByteRead(matrix);
      }
    }
    private void TextFileDataOnMatrixUInt10Read(UInt16[,] matrix)
    {
      if (FData.OnMatrixUInt10Read is DOnMatrixUInt10Read)
      {
        FData.OnMatrixUInt10Read(matrix);
      }
    }
    private void TextFileDataOnMatrixUInt12Read(UInt16[,] matrix)
    {
      if (FData.OnMatrixUInt12Read is DOnMatrixUInt12Read)
      {
        FData.OnMatrixUInt12Read(matrix);
      }
    }
    private void TextFileDataOnMatrixUInt16Read(UInt16[,] matrix)
    {
      if (FData.OnMatrixUInt16Read is DOnMatrixUInt16Read)
      {
        FData.OnMatrixUInt16Read(matrix);
      }
    }
    //
    //-----------------------------------------------------------
    //  Section - Callback - MatrixBinaryFile - Read
    //-----------------------------------------------------------
    //
    private void BinaryFileDataOnMatrix08BitRead(CMatrix08Bit matrix)
    {
      if (FData.OnMatrix08BitRead is DOnMatrix08BitRead)
      {
        FData.OnMatrix08BitRead(matrix);
      }
    }
    private void BinaryFileDataOnMatrix10BitRead(CMatrix10Bit matrix)
    {
      if (FData.OnMatrix10BitRead is DOnMatrix10BitRead)
      {
        FData.OnMatrix10BitRead(matrix);
      }
    }
    private void BinaryFileDataOnMatrix12BitRead(CMatrix12Bit matrix)
    {
      if (FData.OnMatrix12BitRead is DOnMatrix12BitRead)
      {
        FData.OnMatrix12BitRead(matrix);
      }
    }
    private void BinaryFileDataOnMatrix16BitRead(CMatrix16Bit matrix)
    {
      if (FData.OnMatrix16BitRead is DOnMatrix16BitRead)
      {
        FData.OnMatrix16BitRead(matrix);
      }
    }
    private void BinaryFileDataOnMatrixByteRead(Byte[,] matrix)
    {
      if (FData.OnMatrixByteRead is DOnMatrixByteRead)
      {
        FData.OnMatrixByteRead(matrix);
      }
    }
    private void BinaryFileDataOnMatrixUInt10Read(UInt16[,] matrix)
    {
      if (FData.OnMatrixUInt10Read is DOnMatrixUInt10Read)
      {
        FData.OnMatrixUInt10Read(matrix);
      }
    }
    private void BinaryFileDataOnMatrixUInt12Read(UInt16[,] matrix)
    {
      if (FData.OnMatrixUInt12Read is DOnMatrixUInt12Read)
      {
        FData.OnMatrixUInt12Read(matrix);
      }
    }
    private void BinaryFileDataOnMatrixUInt16Read(UInt16[,] matrix)
    {
      if (FData.OnMatrixUInt16Read is DOnMatrixUInt16Read)
      {
        FData.OnMatrixUInt16Read(matrix);
      }
    }
    //
    //-----------------------------------------------------------
    //  Section - Helper - Read
    //-----------------------------------------------------------
    //
    private Boolean ReadMatrixText()
    {
      try
      {
        if (DialogResult.OK == DialogLoadMatrixText.ShowDialog())
        {
          CMatrixFileText MatrixFileText = new CMatrixFileText();
          RTextFileData TextFileData;
          MatrixFileText.GetData(out TextFileData);
          TextFileData.OnMatrix08BitRead = TextFileDataOnMatrix08BitRead;
          TextFileData.OnMatrix10BitRead = TextFileDataOnMatrix10BitRead;
          TextFileData.OnMatrix12BitRead = TextFileDataOnMatrix12BitRead;
          TextFileData.OnMatrix16BitRead = TextFileDataOnMatrix16BitRead;
          TextFileData.OnMatrixByteRead = TextFileDataOnMatrixByteRead;
          TextFileData.OnMatrixUInt10Read = TextFileDataOnMatrixUInt10Read;
          TextFileData.OnMatrixUInt12Read = TextFileDataOnMatrixUInt12Read;
          TextFileData.OnMatrixUInt16Read = TextFileDataOnMatrixUInt16Read;
          MatrixFileText.SetData(TextFileData);
          //
          CMatrixBase MatrixBase;
          if (MatrixFileText.Read(DialogLoadMatrixText.FileName, out MatrixBase))
          {
            //String SName = Path.GetFileNameWithoutExtension(DialogLoadMatrixText.FileName);
            //MatrixBase.Name = SName;
          }          
          MatrixFileText.Close();
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean ReadMatrixBinary()
    {
      try
      {
        DialogLoadMatrixBinary.FileName = "";
        if (DialogResult.OK == DialogLoadMatrixBinary.ShowDialog())
        {
          String FileName = DialogLoadMatrixBinary.FileName;// +".bin";
          //
          CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
          RBinaryFileData BinaryFileData;
          MatrixFileBinary.GetData(out BinaryFileData);
          BinaryFileData.OnMatrix08BitRead = BinaryFileDataOnMatrix08BitRead;
          BinaryFileData.OnMatrix10BitRead = BinaryFileDataOnMatrix10BitRead;
          BinaryFileData.OnMatrix12BitRead = BinaryFileDataOnMatrix12BitRead;
          BinaryFileData.OnMatrix16BitRead = BinaryFileDataOnMatrix16BitRead;
          BinaryFileData.OnMatrixByteRead = BinaryFileDataOnMatrixByteRead;
          BinaryFileData.OnMatrixUInt10Read = BinaryFileDataOnMatrixUInt10Read;
          BinaryFileData.OnMatrixUInt12Read = BinaryFileDataOnMatrixUInt12Read;
          BinaryFileData.OnMatrixUInt16Read = BinaryFileDataOnMatrixUInt16Read;
          MatrixFileBinary.SetData(BinaryFileData);
          //
          Boolean Result = false;
          CMatrixBase MatrixBase;
          if (MatrixFileBinary.ReadFromFile(FileName, out MatrixBase))
          {
            Result = true;
          }
          else
          {
            Byte[,] MatrixByte;
            if (MatrixFileBinary.ReadFromFile(DialogLoadMatrixText.FileName, out MatrixByte))
            {
              Result = true;
            }
            else
            {
              UInt16[,] MatrixUIntXX;
              if (MatrixFileBinary.ReadFromFile(DialogLoadMatrixText.FileName, out MatrixUIntXX))
              {
                Result = true;
              }
            }
          }
          MatrixFileBinary.Close();
          return Result;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

            //String SName = Path.GetFileNameWithoutExtension(DialogLoadMatrixText.FileName);
            //MatrixBase.Name = SName;
    //CMatrixFileBinary MatrixFileBinary = new CMatrixFileBinary();
    //MatrixFileBinary.Read(DialogLoadMatrixBinary.FileName, out FMatrix);
    //String SName = Path.GetFileNameWithoutExtension(DialogLoadMatrixText.FileName);
    //FMatrix.Name = SName;
    
    //
    //-----------------------------------------------------------
    //  Section - Event - Write
    //-----------------------------------------------------------
    //
    private void btnWriteFile_Click(object sender, EventArgs e)
    {
      if (INDEX_TEXT == cbxMatrixFormat.SelectedIndex)
      {
        WriteMatrixText();
      }
      else
        if (INDEX_BINARY == cbxMatrixFormat.SelectedIndex)
        {
          WriteMatrixBinary();
        }
    }
    //
    //-----------------------------------------------------------
    //  Section - Event - Read
    //-----------------------------------------------------------
    //
    private void btnReadFile_Click(object sender, EventArgs e)
    {
      if (INDEX_TEXT == cbxMatrixFormat.SelectedIndex)
      {
        ReadMatrixText();
      }
      else
        if (INDEX_BINARY == cbxMatrixFormat.SelectedIndex)
        {
          ReadMatrixBinary();
        }
    }

  }
}
