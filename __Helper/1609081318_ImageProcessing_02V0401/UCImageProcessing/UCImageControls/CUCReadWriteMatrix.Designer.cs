﻿namespace UCImageControls
{
  partial class CUCReadWriteMatrix
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gbxReadWriteMatrix = new System.Windows.Forms.GroupBox();
      this.cbxMatrixFormat = new System.Windows.Forms.ComboBox();
      this.btnReadFile = new System.Windows.Forms.Button();
      this.btnWriteFile = new System.Windows.Forms.Button();
      this.DialogSaveMatrixText = new System.Windows.Forms.SaveFileDialog();
      this.DialogLoadMatrixText = new System.Windows.Forms.OpenFileDialog();
      this.DialogLoadMatrixBinary = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveMatrixBinary = new System.Windows.Forms.SaveFileDialog();
      this.gbxReadWriteMatrix.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbxReadWriteMatrix
      // 
      this.gbxReadWriteMatrix.Controls.Add(this.cbxMatrixFormat);
      this.gbxReadWriteMatrix.Controls.Add(this.btnReadFile);
      this.gbxReadWriteMatrix.Controls.Add(this.btnWriteFile);
      this.gbxReadWriteMatrix.Location = new System.Drawing.Point(7, 0);
      this.gbxReadWriteMatrix.Name = "gbxReadWriteMatrix";
      this.gbxReadWriteMatrix.Size = new System.Drawing.Size(157, 42);
      this.gbxReadWriteMatrix.TabIndex = 12;
      this.gbxReadWriteMatrix.TabStop = false;
      this.gbxReadWriteMatrix.Text = " File Matrix ";
      // 
      // cbxMatrixFormat
      // 
      this.cbxMatrixFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxMatrixFormat.FormattingEnabled = true;
      this.cbxMatrixFormat.Items.AddRange(new object[] {
            "Text",
            "Binary"});
      this.cbxMatrixFormat.Location = new System.Drawing.Point(6, 15);
      this.cbxMatrixFormat.Name = "cbxMatrixFormat";
      this.cbxMatrixFormat.Size = new System.Drawing.Size(53, 21);
      this.cbxMatrixFormat.TabIndex = 12;
      // 
      // btnReadFile
      // 
      this.btnReadFile.Location = new System.Drawing.Point(110, 13);
      this.btnReadFile.Name = "btnReadFile";
      this.btnReadFile.Size = new System.Drawing.Size(42, 24);
      this.btnReadFile.TabIndex = 10;
      this.btnReadFile.Text = "Read";
      this.btnReadFile.UseVisualStyleBackColor = true;
      this.btnReadFile.Click += new System.EventHandler(this.btnReadFile_Click);
      // 
      // btnWriteFile
      // 
      this.btnWriteFile.Location = new System.Drawing.Point(64, 13);
      this.btnWriteFile.Name = "btnWriteFile";
      this.btnWriteFile.Size = new System.Drawing.Size(42, 24);
      this.btnWriteFile.TabIndex = 9;
      this.btnWriteFile.Text = "Write";
      this.btnWriteFile.UseVisualStyleBackColor = true;
      this.btnWriteFile.Click += new System.EventHandler(this.btnWriteFile_Click);
      // 
      // DialogSaveMatrixText
      // 
      this.DialogSaveMatrixText.DefaultExt = "txt";
      this.DialogSaveMatrixText.FileName = "Matrix";
      this.DialogSaveMatrixText.Filter = "Matrix Text Files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogSaveMatrixText.Title = "Save Matrix";
      // 
      // DialogLoadMatrixText
      // 
      this.DialogLoadMatrixText.DefaultExt = "txt";
      this.DialogLoadMatrixText.FileName = "Matrix";
      this.DialogLoadMatrixText.Filter = "Matrix Text Files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogLoadMatrixText.Title = "Load Matrix";
      // 
      // DialogLoadMatrixBinary
      // 
      this.DialogLoadMatrixBinary.DefaultExt = "bin";
      this.DialogLoadMatrixBinary.FileName = "Matrix";
      this.DialogLoadMatrixBinary.Filter = "Matrix Binary Files (*.bin)|*.bin|All files (*.*)|*.*";
      this.DialogLoadMatrixBinary.Title = "Load Matrix";
      // 
      // DialogSaveMatrixBinary
      // 
      this.DialogSaveMatrixBinary.DefaultExt = "bin";
      this.DialogSaveMatrixBinary.FileName = "Matrix";
      this.DialogSaveMatrixBinary.Filter = "Matrix Binary Files (*.bin)|*.bin|All files (*.*)|*.*";
      this.DialogSaveMatrixBinary.Title = "Save Matrix";
      // 
      // CUCReadWriteMatrix
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.gbxReadWriteMatrix);
      this.Name = "CUCReadWriteMatrix";
      this.Size = new System.Drawing.Size(166, 46);
      this.gbxReadWriteMatrix.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox gbxReadWriteMatrix;
    private System.Windows.Forms.Button btnReadFile;
    private System.Windows.Forms.Button btnWriteFile;
    private System.Windows.Forms.SaveFileDialog DialogSaveMatrixText;
    private System.Windows.Forms.OpenFileDialog DialogLoadMatrixText;
    private System.Windows.Forms.OpenFileDialog DialogLoadMatrixBinary;
    private System.Windows.Forms.SaveFileDialog DialogSaveMatrixBinary;
    private System.Windows.Forms.ComboBox cbxMatrixFormat;
  }
}
