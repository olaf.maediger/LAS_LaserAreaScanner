﻿namespace UCImageControls
{
  partial class CUCReadWriteBitmap
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gbxReadWriteImage = new System.Windows.Forms.GroupBox();
      this.cbxBitmapFormat = new System.Windows.Forms.ComboBox();
      this.btnReadFile = new System.Windows.Forms.Button();
      this.btnWriteFile = new System.Windows.Forms.Button();
      this.DialogSaveImage = new System.Windows.Forms.SaveFileDialog();
      this.DialogLoadImage = new System.Windows.Forms.OpenFileDialog();
      this.gbxReadWriteImage.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbxReadWriteImage
      // 
      this.gbxReadWriteImage.Controls.Add(this.cbxBitmapFormat);
      this.gbxReadWriteImage.Controls.Add(this.btnReadFile);
      this.gbxReadWriteImage.Controls.Add(this.btnWriteFile);
      this.gbxReadWriteImage.Location = new System.Drawing.Point(7, 0);
      this.gbxReadWriteImage.Name = "gbxReadWriteImage";
      this.gbxReadWriteImage.Size = new System.Drawing.Size(157, 42);
      this.gbxReadWriteImage.TabIndex = 13;
      this.gbxReadWriteImage.TabStop = false;
      this.gbxReadWriteImage.Text = " File Image ";
      // 
      // cbxBitmapFormat
      // 
      this.cbxBitmapFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxBitmapFormat.FormattingEnabled = true;
      this.cbxBitmapFormat.Items.AddRange(new object[] {
            "Bmp",
            "Gif",
            "Jpg",
            "Png",
            "Tif",
            "Icon"});
      this.cbxBitmapFormat.Location = new System.Drawing.Point(6, 15);
      this.cbxBitmapFormat.Name = "cbxBitmapFormat";
      this.cbxBitmapFormat.Size = new System.Drawing.Size(53, 21);
      this.cbxBitmapFormat.TabIndex = 11;
      // 
      // btnReadFile
      // 
      this.btnReadFile.Location = new System.Drawing.Point(110, 13);
      this.btnReadFile.Name = "btnReadFile";
      this.btnReadFile.Size = new System.Drawing.Size(42, 24);
      this.btnReadFile.TabIndex = 10;
      this.btnReadFile.Text = "Read";
      this.btnReadFile.UseVisualStyleBackColor = true;
      this.btnReadFile.Click += new System.EventHandler(this.btnReadFile_Click);
      // 
      // btnWriteFile
      // 
      this.btnWriteFile.Location = new System.Drawing.Point(64, 13);
      this.btnWriteFile.Name = "btnWriteFile";
      this.btnWriteFile.Size = new System.Drawing.Size(42, 24);
      this.btnWriteFile.TabIndex = 9;
      this.btnWriteFile.Text = "Write";
      this.btnWriteFile.UseVisualStyleBackColor = true;
      this.btnWriteFile.Click += new System.EventHandler(this.btnWriteFile_Click);
      // 
      // DialogSaveImage
      // 
      this.DialogSaveImage.DefaultExt = "bmp";
      this.DialogSaveImage.FileName = "Image";
      this.DialogSaveImage.Filter = "BMP files (*.bmp)|*.bmp|GIF Files (*.gif)|*.gif|JPG files (*.jpg)|*.jpg|PNG files" +
    " (*.png)|*.png|TIF files (*.tif)|*.tif|Icon files (*.ico)|*.ico";
      this.DialogSaveImage.Title = "Save Matrix";
      // 
      // DialogLoadImage
      // 
      this.DialogLoadImage.DefaultExt = "bmp";
      this.DialogLoadImage.FileName = "Image";
      this.DialogLoadImage.Filter = "BMP files (*.bmp)|*.bmp|GIF Files (*.gif)|*.gif|JPG files (*.jpg)|*.jpg|PNG files" +
    " (*.png)|*.png|TIF files (*.tif)|*.tif|Icon files (*.ico)|*.ico";
      this.DialogLoadImage.Title = "Load Image";
      // 
      // CUCReadWriteBitmap
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.gbxReadWriteImage);
      this.Name = "CUCReadWriteBitmap";
      this.Size = new System.Drawing.Size(167, 46);
      this.gbxReadWriteImage.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox gbxReadWriteImage;
    private System.Windows.Forms.Button btnReadFile;
    private System.Windows.Forms.Button btnWriteFile;
    private System.Windows.Forms.ComboBox cbxBitmapFormat;
    private System.Windows.Forms.SaveFileDialog DialogSaveImage;
    private System.Windows.Forms.OpenFileDialog DialogLoadImage;
  }
}
