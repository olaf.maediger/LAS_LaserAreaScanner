﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
using UCMatrixBuilder;
using IPPaletteBase;
//
namespace DemoImageProcessing
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    //private const String TEXT_LEFTOPEN = "Open CameraLeft";
    //
    //private const String NAME_AUTOLOADIMAGE = "AutoLoadImage";
    //private const Boolean INIT_AUTOLOADIMAGE = true;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Int32 FLeftPreset, FTopPreset;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      //
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      //// important for correct z-order of child-forms
      //this.AddOwnedForm(FUCCamera.UCViewImage.DialogReal);
      //this.AddOwnedForm(FUCCamera.UCViewImage.DialogGrid);
      //this.AddOwnedForm(FUCCamera.UCViewImage.DialogTitle);
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      //
      //FUCCamera.SetOnErrorUCCamera(OnErrorUCCamera);
      //FUCCamera.SetOnInfoUCCamera(OnInfoUCCamera);
      //FUCCamera.SetOnErrorGrabberHardware(OnErrorGrabberHardware);
      //FUCCamera.SetOnInfoGrabberHardware(OnInfoGrabberHardware);
      //FUCCamera.SetViewImageOnDialogRealChanged(UCCameraViewImageOnDialogRealChanged);
      //FUCCamera.SetViewImageOnDialogGridChanged(UCCameraViewImageOnDialogGridChanged);
      //FUCCamera.SetViewImageOnDialogTitleChanged(UCCameraViewImageOnDialogTitleChanged);
      ////
      FLeftPreset = Left;
      FTopPreset = Top;
      //
      // FUCCamera.SetNotifier(FUCNotifier);
      // FUCCamera.Dock = DockStyle.Fill;
      // FUCCamera.SetOnCameraLeftStateChanged(UCLaserLabyrinthOnCameraLeftStateChanged);
      //
      tmrStartup.Interval = 1000;
      tmrStartup.Start();
      //
      // NC FRandomDouble = new CRandomDouble();
    }

    private void FreeProgrammerControls()
    {
      FUCCamera.CloseCamera();
      //
      tmrStartup.Stop();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //!!!!!Boolean BValue;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //!!!!!Result &= FUCCamera.LoadInitdata(initdata, "UCCamera");
      //
      //BValue = INIT_AUTOLOADIMAGE;
      //Result &= initdata.ReadBoolean(NAME_AUTOLOADIMAGE, out BValue, BValue);
      //cbxAutoLoadImage.Checked = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //!!!!!Result &= FUCCamera.SaveInitdata(initdata, "UCCamera");
      //
      //!!!!!initdata.WriteBoolean(NAME_AUTOLOADIMAGE, cbxAutoLoadImage.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCCamera
    //------------------------------------------------------------------------
    //    
    //private void OnErrorUCCamera(String namefunction,
    //                             String texterror)
    //{
    //  String SError = String.Format("UCCamera-{0}: Error: {1}!", namefunction, texterror);
    //  FUCNotifier.Write(SError);
    //}

    //private void OnInfoUCCamera(String textinfo)
    //{
    //  String SInfo = String.Format("UCCamera: {0}", textinfo);
    //  FUCNotifier.Write(SInfo);
    //}

    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private void SelectImageGeneration()
    {
      tbcMain.SelectTab(tbpImageGeneration);
    }

    private void SelectImageProcessing()
    {
      tbcMain.SelectTab(tbpImageProcessing);
    }

    private void SelectImageAcquisition()
    {
      tbcMain.SelectTab(tbpImageAcquisition);
    }


    //private Boolean OnStartupTick(Int32 startupstate)
    //{
    //  switch (FStartupState)
    //  {
    //    case 0:
    //      SelectImageGeneration();
    //      return true;
    //    case 1:
    //      SelectImageProcessing();
    //      FStartupState = 18;
    //      return true;
    //    case 2:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RGBSectionBright);
    //      FUCMatrixOperator.SelectMatrix01(CUCMatrixOperator.COMMAND_CREATELINEAR, "0", "250");
    //      return true;
    //    case 3:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RGBSectionBright);
    //      FUCMatrixOperator.SelectMatrix02(CUCMatrixOperator.COMMAND_CREATENOISE, "0", "5");
    //      return true;
    //    case 4:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.AddMatrix010203();
    //      return true;
    //    case 5:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RGBSectionBright);
    //      FUCMatrixOperator.SelectMatrix03(CUCMatrixOperator.COMMAND_NORM, "0", "255");
    //      return true;
    //    case 6:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RGBSectionBright);
    //      FUCMatrixOperator.SelectMatrix02(CUCMatrixOperator.COMMAND_NORM, "200", "255");
    //      return true;
    //    case 7:
    //      return true;
    //    case 8:
    //      return true;
    //    case 9:
    //      FUCMatrixOperator.HideMatrix01();
    //      FUCMatrixOperator.HideMatrix02();
    //      FUCMatrixOperator.HideMatrix03();
    //      return true;
    //    case 10:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RGBSectionBright);
    //      FUCMatrixOperator.SelectMatrix01(CUCMatrixOperator.COMMAND_CREATECIRCULAR, "0", "240");
    //      return true;
    //    case 11:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RGBSectionBright);
    //      FUCMatrixOperator.SelectMatrix02(CUCMatrixOperator.COMMAND_CREATENOISE, "0", "255");
    //      return true;
    //    case 12:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RGBSectionBright);
    //      FUCMatrixOperator.SelectMatrix02(CUCMatrixOperator.COMMAND_NORM, "0", "15");
    //      return true;
    //    case 13:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.AddMatrix010203();
    //      return true;
    //    case 14:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RGBSectionBright);
    //      FUCMatrixOperator.SelectMatrix02(CUCMatrixOperator.COMMAND_NORM, "127", "255");
    //      return true;
    //    case 15:
    //      return true;
    //    case 16:
    //      return true;
    //    case 17:
    //      return true;
    //    case 18:
    //      return true;
    //    case 19:
    //      FUCMatrixOperator.HideMatrix01();
    //      FUCMatrixOperator.HideMatrix02();
    //      FUCMatrixOperator.HideMatrix03();
    //      return true;
    //    case 20:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RainbowHot);
    //      FUCMatrixOperator.SelectMatrix01(CUCMatrixOperator.COMMAND_CREATENOISE, "0", "255");
    //      return true;
    //    case 21:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RainbowHot);
    //      FUCMatrixOperator.SelectMatrix01(CUCMatrixOperator.COMMAND_NORM, "0", "120");
    //      return true;
    //    case 22:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RainbowHot);
    //      FUCMatrixOperator.SelectMatrix02(CUCMatrixOperator.COMMAND_CREATENOISE, "0", "255");
    //      return true;
    //    case 23:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RainbowHot);
    //      FUCMatrixOperator.SelectMatrix02(CUCMatrixOperator.COMMAND_NORM, "20", "100");
    //      return true;
    //    case 24:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RGBSectionBright);
    //      FUCMatrixOperator.SubstractMatrix010203();
    //      return true;
    //    case 25:
    //      return true;
    //    case 26:
    //      FUCMatrixOperator.SelectMatrix08Bit();
    //      FUCMatrixOperator.SetPaletteKind(EPaletteKind.RGBSectionBright);
    //      FUCMatrixOperator.SelectMatrix03(CUCMatrixOperator.COMMAND_NORM, "0", "255");
    //      return true;
    //    case 27:
    //      return true;
    //    case 28:
    //      return true;
    //    case 29:
    //      //FUCMatrixOperator.HideMatrix01();
    //      //FUCMatrixOperator.HideMatrix02();
    //      //FUCMatrixOperator.HideMatrix03();
    //      return true;
    //    default:
    //      return false;
    //  }
    //}
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          SelectImageAcquisition();
          return true;
        default:
          return false;
      }
    }

    //
    //########################################################################
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void FormMain_Move(object sender, EventArgs e)
    { // Synchronize positions of all Childs: 
      Int32 LeftActual = Left;
      Int32 TopActual = Top;
      Int32 DL = LeftActual - FLeftPreset;
      Int32 DT = TopActual - FTopPreset;
      FLeftPreset = LeftActual;
      FTopPreset = TopActual;
      //
      //!!!!!FUCCamera.MoveDialogs(DL, DT);
    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }

    private void cbxAutoLoadImage_CheckedChanged(object sender, EventArgs e)
    {
      //!!!!!tmrLoadImage.Enabled = cbxAutoLoadImage.Checked;
    }

    private void cbxShowDialogEditReal_CheckedChanged(object sender, EventArgs e)
    {
      // !!!! FUCCamera.ShowDialogEditReal(cbxShowDialogEditReal.Checked);
    }

    private void cbxShowDialogEditGrid_CheckedChanged(object sender, EventArgs e)
    {
      // !!!! FUCCamera.ShowDialogEditGrid(cbxShowDialogEditGrid.Checked);
    }

    private void cbxShowDialogEditTitle_CheckedChanged(object sender, EventArgs e)
    {
      // !!!! FUCCamera.ShowDialogEditTitle(cbxShowDialogEditTitle.Checked);
    }

  

  }
}

