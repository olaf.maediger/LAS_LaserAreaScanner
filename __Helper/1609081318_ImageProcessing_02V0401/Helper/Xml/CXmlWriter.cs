﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
namespace Xml
{
  public class CXmlWriter : CXmlBase
  {
    //
    //------------------------------------------
    //	Segment - Write - Scalar-Datatypes
    //------------------------------------------
    //
    public Boolean WriteBoolean(XmlDocument xmldocument,
                                XmlNode nodebase,
                                String header,
                                Boolean value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteString(XmlDocument xmldocument,
                               XmlNode nodebase,
                               String header,
                               String value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value;
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteInt16(XmlDocument xmldocument,
                              XmlNode nodebase,
                              String header,
                              Int16 value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteInt32(XmlDocument xmldocument,
                              XmlNode nodebase,
                              String header,
                              Int32 value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteDouble(XmlDocument xmldocument,
                               XmlNode nodebase,
                               String header,
                               Double value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteByte(XmlDocument xmldocument,
                             XmlNode nodebase,
                             String header,
                             Byte value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }
    //
    //----------------------------------------------
    //	Segment - Write - Common - High-Level
    //----------------------------------------------
    //
    public Boolean WriteBegin(out XmlDocument xmldocument, 
                              out XmlNode xmlnode,
                              Boolean enableprotocol)
    {
      Boolean Result = true;
      EnableProtocol(enableprotocol);
      // Root
      xmldocument = new XmlDocument();
      xmlnode = xmldocument.CreateElement(CXmlHeader.MAIN);
      xmldocument.AppendChild(xmlnode);
      String Line = String.Format("<{0}>[{1}]", xmlnode.Name, xmlnode.InnerText);
      Write(Line);
      // Declaration
      XmlDeclaration XMLDeclaration = xmldocument.CreateXmlDeclaration(CXmlHeader.TITLEVERSION,
                                                                       CXmlHeader.TITLEENCODING,
                                                                       CXmlHeader.TITLESTANDALONE);
      xmldocument.InsertBefore(XMLDeclaration, xmlnode);
      // Comment
      XmlComment XMLComment = xmldocument.CreateComment(CXmlHeader.COMMENT);
      xmldocument.InsertBefore(XMLComment, xmlnode);
      //
      WriteIncrement();
      //	
      return Result;
    }

    public Boolean WriteHeader(XmlDocument xmldocument,
                               XmlNode xmlnode,
                               RXmlHeader data)
    {
      Boolean Result = true;
      // Base
      XmlNode NodeBase = xmldocument.CreateElement(CXmlHeader.HEADER);
      xmlnode.AppendChild(NodeBase);
      String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
      Write(Line);
      WriteIncrement();
      // Project
      WriteString(xmldocument, NodeBase, CXmlHeader.PROJECT, data.Project);
      // Version
      WriteString(xmldocument, NodeBase, CXmlHeader.VERSION, data.Version);
      // Year
      WriteInt32(xmldocument, NodeBase, CXmlHeader.YEAR, data.Year);
      // Month
      WriteInt32(xmldocument, NodeBase, CXmlHeader.MONTH, data.Month);
      // Day
      WriteInt32(xmldocument, NodeBase, CXmlHeader.DAY, data.Day);
      // Hour
      WriteInt32(xmldocument, NodeBase, CXmlHeader.HOUR, data.Hour);
      // Minute
      WriteInt32(xmldocument, NodeBase, CXmlHeader.MINUTE, data.Minute);
      // Second
      WriteInt32(xmldocument, NodeBase, CXmlHeader.SECOND, data.Second);
      // Author
      WriteString(xmldocument, NodeBase, CXmlHeader.AUTHOR, data.Author);
      //
      WriteDecrement();
      return Result;
    }

    public Boolean WriteEnd(XmlDocument xmldocument)
    {
      Boolean Result = true;
      WriteDecrement();
      return Result;
    }
    //
    //
    //----------------------------------------------
    //	Segment - Write - Basic-Tools
    //----------------------------------------------
    //


  }
}
