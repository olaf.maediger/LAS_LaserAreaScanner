﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using UCNotifier;
//
namespace Xml
{
  //
  //------------------------------------------
  //	Segment - Definition
  //------------------------------------------
  //
  public abstract class CXmlBase
  {
    //
    //------------------------------------------
    //	Segment - Constant
    //------------------------------------------
    //
    public const Boolean INIT_PROTOCOLENABLED = false;
    //
    //------------------------------------------
    //	Segment - Field
    //------------------------------------------
    //
    protected CNotifier FNotifier;
    //protected XmlDocument FXMLDocument;
    //protected XmlNode FNodeRoot;
    protected Int32 FTabulator = 0;
    protected Boolean FProtocolEnabled;
    //
    //------------------------------------------
    //	Segment - Constructor
    //------------------------------------------
    //
    public CXmlBase()
    {
      FProtocolEnabled = INIT_PROTOCOLENABLED;
    }
    //
    //------------------------------------------
    //	Segment - Property
    //------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void EnableProtocol(Boolean enable)
    {
      FProtocolEnabled = enable;
    }
    //
    //------------------------------------------
    //	Segment - Helper
    //------------------------------------------
    //
    protected void WriteIncrement()
    {
      FTabulator++;
    }

    protected void WriteDecrement()
    {
      if (0 < FTabulator)
        FTabulator--;
    }

    protected void Write(String line)
    {
      if (FNotifier is CNotifier)
      {
        if (FProtocolEnabled)
        {
          String Header = "";
          for (Int32 CI = 0; CI < FTabulator; CI++)
          {
            Header += "  ";
          }
          FNotifier.Write(Header + line);
        }
      }
    }
    ////
    ////------------------------------------------
    ////	Segment - Write - Scalar-Datatypes
    ////------------------------------------------
    ////
    //private Boolean WriteBoolean(XmlNode nodebase,
    //                             String header,
    //                             Boolean value)
    //{
    //  XmlNode NodeChild = FXMLDocument.CreateElement(header);
    //  NodeChild.InnerText = value.ToString();
    //  nodebase.AppendChild(NodeChild);
    //  String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
    //  Write(Line);
    //  return true;
    //}

    //private Boolean WriteString(XmlNode nodebase,
    //                            String header,
    //                            String value)
    //{
    //  XmlNode NodeChild = FXMLDocument.CreateElement(header);
    //  NodeChild.InnerText = value;
    //  nodebase.AppendChild(NodeChild);
    //  String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
    //  Write(Line);
    //  return true;
    //}

    //private Boolean WriteInt16(XmlNode nodebase,
    //                           String header,
    //                           Int16 value)
    //{
    //  XmlNode NodeChild = FXMLDocument.CreateElement(header);
    //  NodeChild.InnerText = value.ToString();
    //  nodebase.AppendChild(NodeChild);
    //  String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
    //  Write(Line);
    //  return true;
    //}

    //private Boolean WriteInt32(XmlNode nodebase,
    //                           String header,
    //                           Int32 value)
    //{
    //  XmlNode NodeChild = FXMLDocument.CreateElement(header);
    //  NodeChild.InnerText = value.ToString();
    //  nodebase.AppendChild(NodeChild);
    //  String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
    //  Write(Line);
    //  return true;
    //}

    //private Boolean WriteDouble(XmlNode nodebase,
    //                            String header,
    //                            Double value)
    //{
    //  XmlNode NodeChild = FXMLDocument.CreateElement(header);
    //  NodeChild.InnerText = value.ToString();
    //  nodebase.AppendChild(NodeChild);
    //  String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
    //  Write(Line);
    //  return true;
    //}

    //private Boolean WriteByte(XmlNode nodebase,
    //                          String header,
    //                          Byte value)
    //{
    //  XmlNode NodeChild = FXMLDocument.CreateElement(header);
    //  NodeChild.InnerText = value.ToString();
    //  nodebase.AppendChild(NodeChild);
    //  String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
    //  Write(Line);
    //  return true;
    //}
    ////
    ////----------------------------------------------
    ////	Segment - Write - Basic-Struct-Level
    ////----------------------------------------------
    ////
    //private Boolean WriteARGB(XmlNode nodebase,
    //                          Byte alpha, Byte red, Byte green, Byte blue)
    //{
    //  Boolean Result = true;
    //  //	Alpha
    //  WriteByte(nodebase, CXmlHeader.ALPHA, alpha);
    //  //	Red
    //  WriteByte(nodebase, CXmlHeader.RED, red);
    //  //	Green
    //  WriteByte(nodebase, CXmlHeader.GREEN, green);
    //  //	Blue
    //  WriteByte(nodebase, CXmlHeader.BLUE, blue);
    //  //
    //  return Result;
    //}

    //private Boolean WriteGuid(XmlNode nodebase,
    //                          Guid guid)
    //{
    //  Boolean Result = true;
    //  //	Guid
    //  XmlNode NodeChild = FXMLDocument.CreateElement(CXmlHeader.GUID);
    //  NodeChild.InnerText = guid.ToString();
    //  nodebase.AppendChild(NodeChild);
    //  String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
    //  Write(Line);
    //  //
    //  return Result;
    //}

    //private Boolean WritePenData(XmlNode nodeparent,
    //                             RPenData data)
    //{
    //  Boolean Result = true;
    //  XmlNode NodeBase = FXMLDocument.CreateElement(CXmlHeader.PEN);
    //  nodeparent.AppendChild(NodeBase);
    //  String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
    //  Write(Line);
    //  WriteIncrement();
    //  // Alpha, Red, Green, Blue
    //  WriteARGB(NodeBase, data.Color.A, data.Color.R, data.Color.G, data.Color.B);
    //  //	Style
    //  WriteString(NodeBase, CXmlHeader.STYLE, data.DashStyle.ToString());
    //  //	Width
    //  WriteInt32(NodeBase, CXmlHeader.WIDTH, data.Width);
    //  //
    //  WriteDecrement();
    //  return Result;
    //}

    //private Boolean WriteBrushData(XmlNode nodeparent,
    //                               RBrushData data)
    //{
    //  Boolean Result = true;
    //  XmlNode NodeBase = FXMLDocument.CreateElement(CXmlHeader.BRUSH);
    //  nodeparent.AppendChild(NodeBase);
    //  String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
    //  Write(Line);
    //  WriteIncrement();
    //  // Alpha, Red, Green, Blue
    //  WriteARGB(NodeBase, data.Color.A, data.Color.R, data.Color.G, data.Color.B);
    //  //	Style
    //  // later WriteString(NodeBase, CXmlHeader.STYLE, data.BrushStyle.ToString());
    //  //
    //  WriteDecrement();
    //  return Result;
    //}

    //private Boolean WriteFontData(XmlNode nodeparent,
    //                              RFontData data)
    //{
    //  Boolean Result = true;
    //  XmlNode NodeBase = FXMLDocument.CreateElement(CXmlHeader.FONT);
    //  nodeparent.AppendChild(NodeBase);
    //  String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
    //  Write(Line);
    //  WriteIncrement();
    //  //	Name
    //  WriteString(NodeBase, CXmlHeader.NAME, data.Name);
    //  //	Size
    //  WriteInt32(NodeBase, CXmlHeader.SIZE, (Int32)data.Size);
    //  //	Style
    //  WriteString(NodeBase, CXmlHeader.STYLE, data.FontStyle.ToString());
    //  // Alpha, Red, Green, Blue
    //  WriteARGB(NodeBase, data.Color.A, data.Color.R, data.Color.G, data.Color.B);
    //  //
    //  WriteDecrement();
    //  return Result;
    //}







  }
}
