﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCUdpInterface
{
  public partial class CUCUdpInterface : UserControl
  {
    public CUCUdpInterface()
    {
      InitializeComponent();
    }

    private void CUCUdpInterface_Resize(object sender, EventArgs e)
    {
      cbxSend.Width = this.Width - btnSend.Left - btnSend.Width - pnlBorder.Width;
    }
  }
}
