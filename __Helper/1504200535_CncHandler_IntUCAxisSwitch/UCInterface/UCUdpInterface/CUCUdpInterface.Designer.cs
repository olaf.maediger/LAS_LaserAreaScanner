﻿namespace UCUdpInterface
{
  partial class CUCUdpInterface
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlTop = new System.Windows.Forms.Panel();
      this.btnSend = new System.Windows.Forms.Button();
      this.cbxOpenClose = new System.Windows.Forms.CheckBox();
      this.pnlBorderTop = new System.Windows.Forms.Panel();
      this.pnlBorder = new System.Windows.Forms.Panel();
      this.pnlProtocol = new System.Windows.Forms.Panel();
      this.cbxSend = new System.Windows.Forms.ComboBox();
      this.pnlTop.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlTop
      // 
      this.pnlTop.Controls.Add(this.cbxSend);
      this.pnlTop.Controls.Add(this.pnlBorder);
      this.pnlTop.Controls.Add(this.pnlBorderTop);
      this.pnlTop.Controls.Add(this.cbxOpenClose);
      this.pnlTop.Controls.Add(this.btnSend);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlTop.Location = new System.Drawing.Point(0, 426);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(685, 30);
      this.pnlTop.TabIndex = 114;
      // 
      // btnSend
      // 
      this.btnSend.Location = new System.Drawing.Point(92, 4);
      this.btnSend.Name = "btnSend";
      this.btnSend.Size = new System.Drawing.Size(78, 22);
      this.btnSend.TabIndex = 2;
      this.btnSend.Text = "Send";
      this.btnSend.UseVisualStyleBackColor = true;
      // 
      // cbxOpenClose
      // 
      this.cbxOpenClose.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxOpenClose.Location = new System.Drawing.Point(3, 4);
      this.cbxOpenClose.Name = "cbxOpenClose";
      this.cbxOpenClose.Size = new System.Drawing.Size(78, 22);
      this.cbxOpenClose.TabIndex = 3;
      this.cbxOpenClose.Text = "Open/Close";
      this.cbxOpenClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxOpenClose.UseVisualStyleBackColor = true;
      // 
      // pnlBorderTop
      // 
      this.pnlBorderTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlBorderTop.Location = new System.Drawing.Point(0, 0);
      this.pnlBorderTop.Name = "pnlBorderTop";
      this.pnlBorderTop.Size = new System.Drawing.Size(685, 5);
      this.pnlBorderTop.TabIndex = 7;
      // 
      // pnlBorder
      // 
      this.pnlBorder.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnlBorder.Location = new System.Drawing.Point(673, 5);
      this.pnlBorder.Name = "pnlBorder";
      this.pnlBorder.Size = new System.Drawing.Size(12, 25);
      this.pnlBorder.TabIndex = 8;
      // 
      // pnlProtocol
      // 
      this.pnlProtocol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlProtocol.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlProtocol.Location = new System.Drawing.Point(0, 0);
      this.pnlProtocol.Name = "pnlProtocol";
      this.pnlProtocol.Size = new System.Drawing.Size(685, 426);
      this.pnlProtocol.TabIndex = 115;
      // 
      // cbxSend
      // 
      this.cbxSend.FormattingEnabled = true;
      this.cbxSend.Location = new System.Drawing.Point(176, 5);
      this.cbxSend.Name = "cbxSend";
      this.cbxSend.Size = new System.Drawing.Size(462, 21);
      this.cbxSend.TabIndex = 9;
      this.cbxSend.Text = "Hello World!";
      // 
      // CUCUdpInterface
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlProtocol);
      this.Controls.Add(this.pnlTop);
      this.Name = "CUCUdpInterface";
      this.Size = new System.Drawing.Size(685, 456);
      this.Resize += new System.EventHandler(this.CUCUdpInterface_Resize);
      this.pnlTop.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.Panel pnlBorder;
    private System.Windows.Forms.Panel pnlBorderTop;
    private System.Windows.Forms.CheckBox cbxOpenClose;
    private System.Windows.Forms.Button btnSend;
    private System.Windows.Forms.Panel pnlProtocol;
    private System.Windows.Forms.ComboBox cbxSend;
  }
}
