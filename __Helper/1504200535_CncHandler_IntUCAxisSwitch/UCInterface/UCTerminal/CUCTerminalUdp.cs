﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
//
using TextFile;
//
namespace UCTerminal
{
  public partial class CUCTerminalUdp : UserControl
  {
    //
    //--------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------
    //
    // NC const String EXTENSION_COMMANDLIST = "cmd.txt";
    // NC const String FILENAME_COMMANDLIST = "Commandlist" + "." + EXTENSION_COMMANDLIST;
    //
    //--------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------
    //
    private DOnPortOpened FOnPortOpened;
    private DOnPortClosed FOnPortClosed;
    private DOnSendText FOnSendText;
    private DOnSendBinary FOnSendBinary;
    //
    //--------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------
    //
    public CUCTerminalUdp()
    {
      InitializeComponent();
    }
    //
    //-------------------------------
    //  Segment - Property
    //-------------------------------
    //
    public void SetOnPortOpened(DOnPortOpened value)
    {
      FOnPortOpened = value;
    }
    public void SetOnPortClosed(DOnPortClosed value)
    {
      FOnPortClosed = value;
    }
    public void SetOnSendText(DOnSendText value)
    {
      FOnSendText = value;
    }
    public void SetOnSendBinary(DOnSendBinary value)
    {
      FOnSendBinary = value;
    }
    //
    //-------------------------------
    //  Segment - Helper
    //-------------------------------
    //
    private void AddColoredLine(Color color, String line)
    {
      FUCMultiColorList.Add(color, line);
    }

    private Boolean LoadCommandText(String filename)
    {
      try
      {
        if (File.Exists(filename))
        {
          CTextFile TextFile = new CTextFile();
          if (TextFile.OpenRead(filename))
          {
            String Line;
            while (!TextFile.IsEndOfFile())
            {
              if (TextFile.Read(out Line))
              {
                cbxSend.Items.Add(Line);
              }
            }
            TextFile.Close();
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean SaveCommandText(String filename)
    {
      try
      {
        CTextFile TextFile = new CTextFile();
        TextFile.OpenWrite(filename);
        foreach (String Line in cbxSend.Items)
        {
          TextFile.Write(Line + '\r' + '\n');
        }
        TextFile.Close();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------------
    //  Segment - Menu - Protocol
    //--------------------------------------------------------------
    //
    private void mitClearProtocol_Click(object sender, EventArgs e)
    {
      FUCMultiColorList.Clear();
    }

    private void mitLoadProtocol_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadCommandText.ShowDialog())
      {
        FUCMultiColorList.LoadText(Color.Green, DialogLoadCommandText.FileName, true);
      }
    }

    private void mitSaveProtocol_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogSaveCommandText.ShowDialog())
      {
        FUCMultiColorList.SaveText(DialogSaveCommandText.FileName);
      }
    }
    //

    //--------------------------------------------------------------
    //  Segment - Menu - Command
    //--------------------------------------------------------------
    //
    private void mitClearCommandlist_Click(object sender, EventArgs e)
    {
      cbxSend.Items.Clear();
    }

    private void mitLoadCommandText_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadCommandText.ShowDialog())
      {
        LoadCommandText(DialogLoadCommandText.FileName);
      }
    }

    private void mitSaveCommandText_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogSaveCommandText.ShowDialog())
      {
        SaveCommandText(DialogSaveCommandText.FileName);
      }
    }

    private void cbxOpenClose_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxOpenClose.Checked)
      { // Close -> Open
        if (FOnPortOpened is DOnPortOpened)
        {
          if (FOnPortOpened())
          {
            cbxOpenClose.Text = "Close";
          }
        }
      }
      else
      { // Open -> Close
        if (FOnPortClosed is DOnPortClosed)
        {
          // ????!!! if (FOnPortClosed())
          {
            cbxOpenClose.Text = "Open";
          }
        }
      }
    }

    private void btnSend_Click(object sender, EventArgs e)
    {
      String Text = cbxSend.Text;
      if (FOnSendText is DOnSendText)
      {
        FOnSendText(Text);
      }
      // add (if new) to ComboBox
      if (!cbxSend.Items.Contains(Text))
      {
        cbxSend.Items.Add(Text);
      }
    }
    //
    //--------------------------------------------------------------
    //  Segment - Public Management
    //--------------------------------------------------------------
    //
    public void Open()
    {
      cbxOpenClose.Checked = true;
    }

    public void Close()
    {
      cbxOpenClose.Checked = false;
    }

    public void PresetText(String text)
    {
      cbxSend.Text = text;
    }

    public void SendText(String text)
    {
      cbxSend.Text = text;
      btnSend_Click(this, null);
    }

    public Boolean AddTextTransmitted(String data)
    {
      FUCMultiColorList.Add(Color.Blue, data);
      return true;
    }

    public Boolean AddTextReceived(String data)
    {
      FUCMultiColorList.Add(Color.Red, data);
      return true;
    }



  }
}
