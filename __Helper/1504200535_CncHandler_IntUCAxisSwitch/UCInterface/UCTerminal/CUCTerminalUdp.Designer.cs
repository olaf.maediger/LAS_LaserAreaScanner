﻿namespace UCTerminal
{
  partial class CUCTerminalUdp
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.cmsTxData = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.protocolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitClearProtocolText = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSaveProtocolText = new System.Windows.Forms.ToolStripMenuItem();
      this.mitLoadProtocolText = new System.Windows.Forms.ToolStripMenuItem();
      this.commandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitClearCommandText = new System.Windows.Forms.ToolStripMenuItem();
      this.mitLoadCommandText = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSaveCommandText = new System.Windows.Forms.ToolStripMenuItem();
      this.pnlInput = new System.Windows.Forms.Panel();
      this.cbxSend = new System.Windows.Forms.ComboBox();
      this.btnSend = new System.Windows.Forms.Button();
      this.cbxOpenClose = new System.Windows.Forms.CheckBox();
      this.FUCMultiColorList = new UCMultiColorList.CUCMultiColorList();
      this.DialogLoadProtocol = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveProtocol = new System.Windows.Forms.SaveFileDialog();
      this.DialogSaveCommandText = new System.Windows.Forms.SaveFileDialog();
      this.DialogLoadCommandText = new System.Windows.Forms.OpenFileDialog();
      this.cmsTxData.SuspendLayout();
      this.pnlInput.SuspendLayout();
      this.SuspendLayout();
      // 
      // cmsTxData
      // 
      this.cmsTxData.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.protocolToolStripMenuItem,
            this.mitClearProtocolText,
            this.mitSaveProtocolText,
            this.mitLoadProtocolText,
            this.commandToolStripMenuItem,
            this.mitClearCommandText,
            this.mitLoadCommandText,
            this.mitSaveCommandText});
      this.cmsTxData.Name = "cmsMultiColorList";
      this.cmsTxData.Size = new System.Drawing.Size(148, 180);
      // 
      // protocolToolStripMenuItem
      // 
      this.protocolToolStripMenuItem.Name = "protocolToolStripMenuItem";
      this.protocolToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.protocolToolStripMenuItem.Text = "- Protocol -";
      // 
      // mitClearProtocolText
      // 
      this.mitClearProtocolText.Name = "mitClearProtocolText";
      this.mitClearProtocolText.Size = new System.Drawing.Size(147, 22);
      this.mitClearProtocolText.Text = "Clear";
      this.mitClearProtocolText.Click += new System.EventHandler(this.mitClearProtocol_Click);
      // 
      // mitSaveProtocolText
      // 
      this.mitSaveProtocolText.Name = "mitSaveProtocolText";
      this.mitSaveProtocolText.Size = new System.Drawing.Size(147, 22);
      this.mitSaveProtocolText.Text = "Save";
      this.mitSaveProtocolText.Click += new System.EventHandler(this.mitSaveProtocol_Click);
      // 
      // mitLoadProtocolText
      // 
      this.mitLoadProtocolText.Name = "mitLoadProtocolText";
      this.mitLoadProtocolText.Size = new System.Drawing.Size(147, 22);
      this.mitLoadProtocolText.Text = "Load";
      this.mitLoadProtocolText.Click += new System.EventHandler(this.mitLoadProtocol_Click);
      // 
      // commandToolStripMenuItem
      // 
      this.commandToolStripMenuItem.Name = "commandToolStripMenuItem";
      this.commandToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
      this.commandToolStripMenuItem.Text = "- Command -";
      // 
      // mitClearCommandText
      // 
      this.mitClearCommandText.Name = "mitClearCommandText";
      this.mitClearCommandText.Size = new System.Drawing.Size(147, 22);
      this.mitClearCommandText.Text = "Clear";
      this.mitClearCommandText.Click += new System.EventHandler(this.mitClearCommandlist_Click);
      // 
      // mitLoadCommandText
      // 
      this.mitLoadCommandText.Name = "mitLoadCommandText";
      this.mitLoadCommandText.Size = new System.Drawing.Size(147, 22);
      this.mitLoadCommandText.Text = "Load";
      // 
      // mitSaveCommandText
      // 
      this.mitSaveCommandText.Name = "mitSaveCommandText";
      this.mitSaveCommandText.Size = new System.Drawing.Size(147, 22);
      this.mitSaveCommandText.Text = "Save";
      // 
      // pnlInput
      // 
      this.pnlInput.Controls.Add(this.cbxSend);
      this.pnlInput.Controls.Add(this.btnSend);
      this.pnlInput.Controls.Add(this.cbxOpenClose);
      this.pnlInput.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlInput.Location = new System.Drawing.Point(0, 304);
      this.pnlInput.Name = "pnlInput";
      this.pnlInput.Size = new System.Drawing.Size(354, 21);
      this.pnlInput.TabIndex = 4;
      // 
      // cbxSend
      // 
      this.cbxSend.BackColor = System.Drawing.SystemColors.Info;
      this.cbxSend.ContextMenuStrip = this.cmsTxData;
      this.cbxSend.Dock = System.Windows.Forms.DockStyle.Fill;
      this.cbxSend.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbxSend.ForeColor = System.Drawing.Color.Blue;
      this.cbxSend.FormattingEnabled = true;
      this.cbxSend.Items.AddRange(new object[] {
            "GHV",
            "GSV",
            "H"});
      this.cbxSend.Location = new System.Drawing.Point(112, 0);
      this.cbxSend.Name = "cbxSend";
      this.cbxSend.Size = new System.Drawing.Size(242, 23);
      this.cbxSend.TabIndex = 11;
      this.cbxSend.Text = "Hello World!";
      // 
      // btnSend
      // 
      this.btnSend.ContextMenuStrip = this.cmsTxData;
      this.btnSend.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnSend.Location = new System.Drawing.Point(56, 0);
      this.btnSend.Name = "btnSend";
      this.btnSend.Size = new System.Drawing.Size(56, 21);
      this.btnSend.TabIndex = 10;
      this.btnSend.Text = "Send";
      this.btnSend.UseVisualStyleBackColor = true;
      this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
      // 
      // cbxOpenClose
      // 
      this.cbxOpenClose.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxOpenClose.Dock = System.Windows.Forms.DockStyle.Left;
      this.cbxOpenClose.Location = new System.Drawing.Point(0, 0);
      this.cbxOpenClose.Name = "cbxOpenClose";
      this.cbxOpenClose.Size = new System.Drawing.Size(56, 21);
      this.cbxOpenClose.TabIndex = 9;
      this.cbxOpenClose.Text = "Open";
      this.cbxOpenClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxOpenClose.UseVisualStyleBackColor = true;
      this.cbxOpenClose.CheckedChanged += new System.EventHandler(this.cbxOpenClose_CheckedChanged);
      // 
      // FUCMultiColorList
      // 
      this.FUCMultiColorList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.FUCMultiColorList.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCMultiColorList.Location = new System.Drawing.Point(0, 0);
      this.FUCMultiColorList.Name = "FUCMultiColorList";
      this.FUCMultiColorList.Size = new System.Drawing.Size(354, 304);
      this.FUCMultiColorList.TabIndex = 5;
      // 
      // DialogLoadProtocol
      // 
      this.DialogLoadProtocol.DefaultExt = "txt";
      this.DialogLoadProtocol.FileName = "Protocol";
      this.DialogLoadProtocol.Filter = "Text files (*.ptc.txt)|*.ptc.txt|All files (*.*)|*.*";
      this.DialogLoadProtocol.Title = "Load Protocol";
      // 
      // DialogSaveProtocol
      // 
      this.DialogSaveProtocol.DefaultExt = "txt";
      this.DialogSaveProtocol.FileName = "Protocol";
      this.DialogSaveProtocol.Filter = "Text files (*.ptc.txt)|*.ptc.txt|All files (*.*)|*.*";
      this.DialogSaveProtocol.Title = "Save Protocol";
      // 
      // DialogSaveCommandText
      // 
      this.DialogSaveCommandText.DefaultExt = "txt";
      this.DialogSaveCommandText.FileName = "CommandText";
      this.DialogSaveCommandText.Filter = "Command files (*.cmd.txt)|*.cmd.txt|All files (*.*)|*.*";
      this.DialogSaveCommandText.Title = "Save CommandText";
      // 
      // DialogLoadCommandText
      // 
      this.DialogLoadCommandText.DefaultExt = "txt";
      this.DialogLoadCommandText.FileName = "CommandText";
      this.DialogLoadCommandText.Filter = "Command files (*.cmd.txt)|*.cmd.txt|All files (*.*)|*.*";
      this.DialogLoadCommandText.Title = "Load CommandText";
      // 
      // CUCTerminalUdp
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCMultiColorList);
      this.Controls.Add(this.pnlInput);
      this.Name = "CUCTerminalUdp";
      this.Size = new System.Drawing.Size(354, 325);
      this.cmsTxData.ResumeLayout(false);
      this.pnlInput.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ContextMenuStrip cmsTxData;
    private System.Windows.Forms.ToolStripMenuItem mitSaveProtocolText;
    private System.Windows.Forms.ToolStripMenuItem mitClearProtocolText;
    private System.Windows.Forms.ToolStripMenuItem mitLoadProtocolText;
    private UCMultiColorList.CUCMultiColorList FUCMultiColorList;
    private System.Windows.Forms.Panel pnlInput;
    private System.Windows.Forms.ToolStripMenuItem protocolToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem commandToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitClearCommandText;
    private System.Windows.Forms.ToolStripMenuItem mitLoadCommandText;
    private System.Windows.Forms.ToolStripMenuItem mitSaveCommandText;
    private System.Windows.Forms.OpenFileDialog DialogLoadProtocol;
    private System.Windows.Forms.SaveFileDialog DialogSaveProtocol;
    private System.Windows.Forms.SaveFileDialog DialogSaveCommandText;
    private System.Windows.Forms.OpenFileDialog DialogLoadCommandText;
    private System.Windows.Forms.CheckBox cbxOpenClose;
    private System.Windows.Forms.ComboBox cbxSend;
    private System.Windows.Forms.Button btnSend;
  }
}
