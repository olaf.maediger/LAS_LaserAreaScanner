﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
//!!!!!!!!!!!using TextFile;
using UCNotifier;
//
namespace UCTerminal
{ //
  //-------------------------------
  //  Segment - Type
  //-------------------------------
  //

  //
  //##################################################
  //  Segment - Main - CUCTerminal
  //##################################################
  //
  public partial class CUCTerminal : UserControl
  { //
    //-------------------------------
    //  Segment - Constant
    //-------------------------------
    // Color RXD
    private Byte INIT_COLORRXD_A = 0xFF;
    private Byte INIT_COLORRXD_R = 0xFF;
    private Byte INIT_COLORRXD_G = 0x00;
    private Byte INIT_COLORRXD_B = 0x00;
    // Color TXD
    private Byte INIT_COLORTXD_A = 0xFF;
    private Byte INIT_COLORTXD_R = 0x00;
    private Byte INIT_COLORTXD_G = 0x00;
    private Byte INIT_COLORTXD_B = 0xFF;
    //
    private String FILENAME_COMMANDS = "TerminalCommands.txt";
    //
    //-------------------------------
    //  Segment - Field
    //-------------------------------
    //
    private CNotifier FNotifier;
    private DOnSendText FOnSendText;
    private DOnSendBinary FOnSendBinary;
    //
    //-------------------------------
    //  Segment - Constructor
    //-------------------------------
    //
    public CUCTerminal()
    {
      InitializeComponent();
      cbxSendLine.Items.Clear();
      LoadText(FILENAME_COMMANDS);
    }
    //
    //-------------------------------
    //  Segment - Property
    //-------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

    public void SetOnSendText(DOnSendText value)
    {
      FOnSendText = value;
    }

    public void SetOnSendBinary(DOnSendBinary value)
    {
      FOnSendBinary = value;
    }
    //
    //-------------------------------
    //  Segment - Event
    //-------------------------------
    // cbxSendLine: Enter -> Send
    private void cbxSendLine_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        btnSend_Click(this, null);
      }
    }
    //
    //-------------------------------
    //  Segment - Helper
    //-------------------------------
    //
    private void AddColoredLine(Color color, String line)
    {
      FUCMultiColorList.Add(color, line);
    }

   /* NC !!!!!!! now with Callback !!! 
    * private void SendColoredLine(Color color, String line)
    {
      AddColoredLine(color, ">" + line);
      if (FOnSendLine is DOnSendLine)
      {
        FOnSendLine(line);
      }
    }*/

    private Boolean LoadText(String filename)
    {
      if (File.Exists(filename))
      {
        /*/!!!!!!!!!!!CTextFile TextFile = new CTextFile(FNotifier);
        if (TextFile.OpenRead(filename))
        {
          String Line;
          while (!TextFile.IsEndOfFile())
          {
            if (TextFile.Read(out Line))
            {
              cbxSendLine.Items.Add(Line);
            }
          }
          TextFile.Close();
        }
        return true;
         */
      }
      return false;
    }

    private Boolean SaveText(String filename)
    {
      /*CTextFile TextFile = new CTextFile(FNotifier);
      TextFile.OpenWrite(filename);
      foreach (String Line in cbxSendLine.Items)
      {
        TextFile.Write(Line + '\r' + '\n');
      }
      TextFile.Close();
      return true;*/
      return false;
    }
    //
    //-------------------------------
    //  Segment - Menu
    //-------------------------------
    //
    private void btnSend_Click(object sender, EventArgs e)
    {
      String Text = cbxSendLine.Text;
      // send it over ComPort
      if (FOnSendText is DOnSendText)
      {
        FOnSendText(Text);
      }
      // add (if new) to ComboBox
      if (!cbxSendLine.Items.Contains(Text))
      {
        cbxSendLine.Items.Add(Text);
      }
    }

    private void mitSaveText_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogSaveCommandText.ShowDialog())
      {
        SaveText(DialogSaveCommandText.FileName);
      }
    }

    private void mitClearAllLines_Click(object sender, EventArgs e)
    {
      cbxSendLine.Items.Clear();
    }

    private void mitLoadText_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadCommandText.ShowDialog())
      {
        LoadText(DialogLoadCommandText.FileName);
      }
    }
    //
    //-------------------------------
    //  Segment - Management
    //-------------------------------
    //

    public void Clear()
    {
      FUCMultiColorList.Clear();
    }

    public void Add(Color color, String line)
    {
      AddColoredLine(color, line);
    }

    public void AddRxData(String line)
    {
      Color C = Color.FromArgb(INIT_COLORRXD_A, INIT_COLORRXD_R, INIT_COLORRXD_G, INIT_COLORRXD_B);
      AddColoredLine(C, line);
    }

    public void AddTxData(String line)
    {
      Color C = Color.FromArgb(INIT_COLORTXD_A, INIT_COLORTXD_R, INIT_COLORTXD_G, INIT_COLORTXD_B);
      AddColoredLine(C, line);
    }



  }
}
