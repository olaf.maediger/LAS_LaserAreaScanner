﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UdpInterface
{
  public delegate void DOnTextReceived(String data);
  public delegate void DOnBinaryReceived(Byte[] data);

  public class CUdpInterface
  {
    //
    //---------------------------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------------------------
    //
    public const Int32 PORT_UDP_01 = 8081;
    public const Int32 PORT_UDP_02 = 8082;
    public const Int32 PORT_UDP_03 = 8083;
    public const Int32 PORT_UDP_04 = 8084;
    public const Int32 PORT_UDP_05 = 8085;
    public const Int32 PORT_UDP_06 = 8086;
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //
  }
}
