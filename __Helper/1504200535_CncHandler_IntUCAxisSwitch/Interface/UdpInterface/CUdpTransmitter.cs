﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
//
namespace UdpInterface
{
  public class CUdpTransmitter : CUdpBase
  { //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    //
    private String FHostAddress;
    private Int32 FPortAddress;
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CUdpTransmitter()
    {
      FUdpClient = new UdpClient();
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------------------------
    //
    public Boolean Open(String hostaddress, Int32 portaddress)
    {
      try
      {
        FHostAddress = hostaddress;
        FPortAddress = portaddress;
        FUdpClient.Connect(FHostAddress, FPortAddress);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Close()
    {
      try
      { // Send EndMessage to stop the Receiving-Thread (not necessary!)
        Byte[] TxData = Encoding.ASCII.GetBytes("");
        if (FUdpClient is UdpClient)
        {
          FUdpClient.Send(TxData, TxData.Length);
          FUdpClient.Close();
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean SendText(String data)
    {
      try
      {
        Byte[] TxData = Encoding.ASCII.GetBytes(data);
        if (FUdpClient is UdpClient)
        {
          FUdpClient.Send(TxData, TxData.Length);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean SendBinary(Byte[] data)
    {
      try
      {
        if (FUdpClient is UdpClient)
        {
          FUdpClient.Send(data, data.Length);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
