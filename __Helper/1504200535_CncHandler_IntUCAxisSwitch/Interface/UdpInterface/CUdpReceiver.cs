﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
//
using UdpInterface;
//
namespace UdpInterface
{
  public class CUdpReceiver : CUdpBase
  {
    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    //
    private Thread FThread;
    private Boolean FThreadActive = false;
    private String FNameClient;
    private String FHostAddress;
    private Int32 FPortAddress;
    private DOnTextReceived FOnTextReceived;
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CUdpReceiver()
    {
      FThread = null;
      FUdpClient = null;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    public void SetOnTextReceived(DOnTextReceived value)
    {
      FOnTextReceived = value;
    }

    public Boolean IsOpen
    {
      get { return (FThread is Thread); }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------------------
    //
    private void CloseChildProcesses()
    {
      
      Process[] PC = Process.GetProcessesByName(FNameClient);
      if (PC is Process[])
      {
        foreach (Process PI in PC)
        {
          PI.Kill();
        }
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Thread - Callback
    //---------------------------------------------------------------------------
    //
    private void ThreadExecution()
    {
      FThreadActive = true;
      FNotifier.Write("UdpReceiver: ThreadExecution: Begin");
      FUdpClient = new UdpClient(FPortAddress); // "127.0.0.1" Problem 
      IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
      Int32 L = 1;
      Int32 NotifierCounter = 0;
      Boolean Loop = true;
      while (Loop)
      {
        if (FUdpClient is UdpClient)
        {
          if (0 < FUdpClient.Available)
          {
            FNotifier.Write("UdpReceiver: ThreadExecution: Received");
            Byte[] receiveBytes = FUdpClient.Receive(ref RemoteIpEndPoint);
            L = receiveBytes.Length;
            String RxData = Encoding.ASCII.GetString(receiveBytes);
            // debug
            String Message = RemoteIpEndPoint.Address.ToString() + ":" + RxData.ToString();
            // debug
            FNotifier.Write(Message);
            if (FOnTextReceived is DOnTextReceived)
            {
              FOnTextReceived(RxData);
            }
          }
          else
          {
            NotifierCounter++;
            if (100 <= NotifierCounter)
            {
              NotifierCounter = 0;
              // debug FNotifier.Write("UdpReceiver: ThreadExecution: Waiting");
            }
            else
            {
              Thread.Sleep(10);
            }
          }
          Loop = (System.Threading.ThreadState.Running == FThread.ThreadState);
          Loop &= (0 < L);
          Loop &= (FThreadActive);
        }
        else
        {
          Loop = false;
        }
      }
      FNotifier.Write("UdpReceiver: ThreadExecution: End");
      CloseChildProcesses();
      if (FUdpClient is UdpClient)
      {
        FUdpClient.Close();
      }
      FThread = null;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------------------------
    //
    public Boolean Open(String hostaddress, Int32 portaddress, String nameclient)
    {
      if (IsOpen)
      {
        return false;
      }
      FNameClient = nameclient;
      try
      {
        FHostAddress = hostaddress;
        FPortAddress = portaddress;
        if (0 < nameclient.Length)
        {
          ProcessStartInfo PSI = new ProcessStartInfo();
          PSI.CreateNoWindow = false;
          PSI.UseShellExecute = false;
          PSI.FileName = FNameClient + ".exe";
          PSI.WindowStyle = ProcessWindowStyle.Normal;
          PSI.Arguments = String.Format("{0}:{1}", FHostAddress, FPortAddress);
          Process.Start(PSI);
        }
        //
        if (FThread is Thread)
        {
          FThreadActive = false;
          FThread = null;
        }
        FNotifier.Write("UdpReceiver: Open");
        FThread = new Thread(ThreadExecution);
        FThread.Start();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Close()
    {
      try
      {
        CloseChildProcesses();
        FThreadActive = false;
        //
        // Send EndMessage to stop the Receiving-Thread (not necessary!)
        // ??? 
        Byte[] TxData = Encoding.ASCII.GetBytes("");
        if (FUdpClient is UdpClient)
        {
          FUdpClient.Send(TxData, TxData.Length);
          FUdpClient.Close();
        }
        FNotifier.Write("UdpReceiver: Close");
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Write(String text)
    {
      try
      {
        FNotifier.Write("UdpReceiver: Write");
        Byte[] TxData = Encoding.ASCII.GetBytes(text);
        if (FUdpClient is UdpClient)
        {
          FUdpClient.Send(TxData, TxData.Length);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}


