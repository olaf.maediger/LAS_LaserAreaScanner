﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageInterface
{
  public class CImageParameter
  {
    private Int32 FPixelDX, FPixelDY;
    private Double FWorldX0, FWorldX1;
    private Double FWorldY0, FWorldY1;

    public CImageParameter()
    {
      FPixelDX = 100;
      FPixelDY = 100;
      FWorldX0 =  0.0;
      FWorldX1 = 10.0;
      FWorldY0 =  0.0;
      FWorldY1 = 10.0;
    }

    public void SetPixelExtrema(Int32 pixeldx, Int32 pixeldy)
    {
      FPixelDX = pixeldx;
      FPixelDY = pixeldy;
    }

    public void SetWorldExtrema(Double worldx0, Double worldx1, 
                                Double worldy0, Double worldy1)
    {
      FWorldX0 = worldx0;
      FWorldX1 = worldx1;
      FWorldY0 = worldy0;
      FWorldY1 = worldy1;
    }

    public Int32 WorldPixelX(Double x)
    {
      return (Int32)(FPixelDX * (x - FWorldX0) / (FWorldX1 - FWorldX0));
    }
    public Int32 WorldPixelY(Double y)
    {
      return (Int32)(FPixelDY * (y - FWorldY0) / (FWorldY1 - FWorldY0));
    }

  }
}
