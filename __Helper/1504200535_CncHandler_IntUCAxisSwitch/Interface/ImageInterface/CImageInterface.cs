﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace ImageInterface
{
  public class CImageInterface
  {
    private CImageParameter FImageParameter;
    private CDrawList FDrawList;


    public CImageInterface()
    {
      FImageParameter = new CImageParameter();
      FDrawList = new CDrawList();
    }


    public void GetImageParameter(out CImageParameter value)
    {
      value = FImageParameter;
    }
    public void SetImageParameter(CImageParameter value)
    {
      FImageParameter = value;
    }


    public void Draw(Graphics graphics)
    {
      FDrawList.Draw(graphics, FImageParameter);
    }

    public void Add(CDrawBase drawbase)
    {
      FDrawList.Add(drawbase);
    }

  }
}
