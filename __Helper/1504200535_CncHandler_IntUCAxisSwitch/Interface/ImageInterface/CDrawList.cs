﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace ImageInterface
{
  public class CDrawList : List<CDrawBase>
  {

    public void Draw(Graphics graphics, CImageParameter imageparameter)
    {
      Int32 GC = this.Count;
      for (Int32 GI = 0; GI < GC; GI++)
      {
        CDrawBase DB = this[GI];
        DB.Draw(graphics, imageparameter);
      }
    }

  }
}
