﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace ImageInterface
{
  public abstract class CDrawBase
  {

    public abstract void Draw(Graphics graphics, CImageParameter imageparameter);
  }
}
