﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace ImageInterface
{
  public class CLine : CDrawBase
  {
    private Pen FPen;
    private Double FX0, FX1;
    private Double FY0, FY1;

    public CLine(Pen pen, 
                 Double x0, Double y0,
                 Double x1, Double y1)
    {
      FPen = pen;
      FX0 = x0;
      FY0 = y0;
      FX1 = x1;
      FY1 = y1;
    }

    public override void Draw(Graphics graphics, CImageParameter imageparameter)
    {
      Int32 PX0 = imageparameter.WorldPixelX(FX0);
      Int32 PY0 = imageparameter.WorldPixelY(FY0);
      Int32 PX1 = imageparameter.WorldPixelX(FX1);
      Int32 PY1 = imageparameter.WorldPixelY(FY1);
      graphics.DrawLine(FPen, PX0, PY0, PX1, PY1);
    }
  }
}
