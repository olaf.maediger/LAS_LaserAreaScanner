﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
//
using Initdata;
using UCNotifier;
//
using Hardware;
using Command;
using GCode;
using Cnc;
using UdpInterface;
using Simulation;
//
namespace CncSimulator
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormCncSimulator : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String NAME_STARTPROCESS_CNCTRANSLATOR = "StartProcessCncTranslator";
    private const Boolean INIT_STARTPROCESS_CNCTRANSLATOR = true;
    private const String NAME_STARTPROCESS_CNCCONTROLLER = "StartProcessCncController";
    private const Boolean INIT_STARTPROCESS_CNCCONTROLLER = true;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;
    private CUdpReceiver FUdpReceiverTranslator;
    private CUdpTransmitter FUdpTransmitterTranslator;
    private CCnc FCnc;
    private CSimulation FSimulation;
    private CSystemParameter FSystemParameter;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    /*
    private void CncGetSystemParameter(out RSystemData systemdata)
    {
      systemdata = FSystemData;
    }
    private void CncSetSystemData(RSystemData systemdata)
    {
      FSystemData = systemdata;
    }

    private void SimulationGetSystemData(out RSystemData systemdata)
    {
      systemdata = FSystemData;
    }
    private void SimulationSetSystemData(RSystemData systemdata)
    {
      FSystemData = systemdata;
    }*/

    private CSystemParameter SimulationGetSystemParameter()
    {
      return FSystemParameter;
    }
    private void SimulationSetSystemParameter(CSystemParameter value)
    {
      FSystemParameter = value;
    }

    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FUCTerminalUdpTranslator.SetOnSendText(UCTerminalUdpTranslatorOnSendText);
      FUCTerminalUdpTranslator.SetOnPortOpened(UCTerminalUdpTranslatorOnPortOpened);
      FUCTerminalUdpTranslator.SetOnPortClosed(UCTerminalUdpTranslatorOnPortClosed);
      FUCTerminalUdpTranslator.PresetText("");
      //
      FUdpTransmitterTranslator = new CUdpTransmitter();
      FUdpTransmitterTranslator.SetNotifier(FUCNotifier);
      //
      FUdpReceiverTranslator = new CUdpReceiver();
      FUdpReceiverTranslator.SetNotifier(FUCNotifier);
      FUdpReceiverTranslator.SetOnTextReceived(UdpReceiverTranslatorOnTextReceived);
      //
      FSystemParameter = new CSystemParameter(FUCNotifier);
      FSimulation = new CSimulation(FUCNotifier,
                                    SimulationGetSystemParameter,
                                    SimulationSetSystemParameter);
      FUCSimulation.SetOnSimulationHalted(UCSimulationOnSimulationHalted);
      FUCSimulation.SetOnSimulationResumed(UCSimulationOnSimulationResumed);
      FUCSimulation.SetOnTimeResolutionChanged(UCSimulationOnTimeResolutionChanged);
      FUCSimulation.TimeResolution = FSimulation.TimeResolution;
      //
      FCnc = new CCnc();//CncGetSystemData, CncSetSystemData);
      FCnc.SetNotifier(FUCNotifier);
      FCnc.SetOnResponse(CncOnResponse);
      FCnc.SetOnLimitSwitchState(CncOnLimitSwitchState);
      //
      FRandom = new Random();
      //
      tmrStartup.Interval = 1;
      tmrStartup.Start();
    }

    private void FreeProgrammerControls()
    {
      CloseAllChildProcesses();
      //
      tmrStartup.Stop();
      //
      FUCTerminalUdpTranslator.Close();
      //
      FSimulation.Abort();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      Boolean BValue;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      BValue = INIT_STARTPROCESS_CNCTRANSLATOR;
      Result &= initdata.ReadBoolean(NAME_STARTPROCESS_CNCTRANSLATOR, out BValue, BValue);
      if (BValue)
      {
        // 
        StartProcessCncTranslator();
      }
      //
      BValue = INIT_STARTPROCESS_CNCCONTROLLER;
      Result &= initdata.ReadBoolean(NAME_STARTPROCESS_CNCCONTROLLER, out BValue, BValue);
      if (BValue)
      {
        //!!!StartProcessCncController();
      }
      //
      FUCTerminalUdpTranslator.Open();
      //
      FSimulation.Start();
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //
    private void StartProcessCncTranslator()
    {
      ProcessStartInfo PSI = new ProcessStartInfo();
      PSI.FileName = "CncTranslator.exe";
      PSI.Arguments = "";
      PSI.UseShellExecute = true;
      PSI.CreateNoWindow = false;
      PSI.WindowStyle = ProcessWindowStyle.Normal;
      Process.Start(PSI);
    }

    private void StartProcessCncController()
    {
      ProcessStartInfo PSI = new ProcessStartInfo();
      PSI.FileName = "CncController.exe";
      PSI.Arguments = "";
      PSI.UseShellExecute = true;
      PSI.CreateNoWindow = false;
      PSI.WindowStyle = ProcessWindowStyle.Normal;
      Process.Start(PSI);
    }

    private void CloseAllChildProcesses()
    {
      Process[] ChildProcesses = Process.GetProcessesByName("CncTranslator");
      foreach (Process ChildProcess in ChildProcesses)
      {
        ChildProcess.CloseMainWindow();
      }
      ChildProcesses = Process.GetProcessesByName("CncController");
      foreach (Process ChildProcess in ChildProcesses)
      {
        ChildProcess.CloseMainWindow();
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        default:
          return false;
      }
    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }
    //
    //--------------------------------------------------
    //  Segment - Helper - 
    //--------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback - Cnc
    //------------------------------------------------------------------------
    //
    private void CncOnResponse(String response)
    {
      FUCTerminalUdpTranslator.SendText(response);
    }

    private void CncOnLimitSwitchState(RCommandParameter systemdata)
    {

    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCTerminal - Translator
    //------------------------------------------------------------------------
    //
    private Boolean UCTerminalUdpTranslatorOnPortOpened()
    {
      Boolean Result = true;
      Result &= FUdpTransmitterTranslator.Open("127.0.0.1", CUdpInterface.PORT_UDP_02);
      Result &= FUdpReceiverTranslator.Open("127.0.0.1", CUdpInterface.PORT_UDP_01, "");
      return Result;
    }

    private Boolean UCTerminalUdpTranslatorOnPortClosed()
    {
      Boolean Result = true;
      Result &= FUdpTransmitterTranslator.Close();
      Result &= FUdpReceiverTranslator.Close();
      return Result;
    }

    private Boolean UCTerminalUdpTranslatorOnSendText(String data)
    { // UCTerminal -> Send-Button -> here -> UdpTransmitter.Send & UCTerminal.Add
      Boolean Result = false;
      if (FUdpTransmitterTranslator.SendText(data))
      {
        Result = FUCTerminalUdpTranslator.AddTextTransmitted(data);
      }
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UdpTransmitter - Translator
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback - UdpReceiver - Translator
    //------------------------------------------------------------------------
    //
    private delegate void CBUdpReceiverTranslatorOnTextReceived(String data);
    private void UdpReceiverTranslatorOnTextReceived(String data)
    {
      if (this.InvokeRequired)
      {
        CBUdpReceiverTranslatorOnTextReceived CB =
          new CBUdpReceiverTranslatorOnTextReceived(UdpReceiverTranslatorOnTextReceived);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCTerminalUdpTranslator.AddTextReceived(data);
        if (FCnc.TranslateProgram(data))
        {
          if (FCnc.ExecuteProgram(ref FSystemParameter))
          {
          }
        }
        else
        { // data empty -> shutdown
          Application.Exit();
        }
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Simulation
    //--------------------------------------------------------------------------
    //
    private void UCSimulationOnSimulationHalted()
    {
      FSimulation.Halt();
    }

    private void UCSimulationOnSimulationResumed()
    {
      FSimulation.Resume();
    }

    private void UCSimulationOnTimeResolutionChanged(Int32 timeresolution)
    {
      FSimulation.TimeResolution = timeresolution;
    }


  }
}

