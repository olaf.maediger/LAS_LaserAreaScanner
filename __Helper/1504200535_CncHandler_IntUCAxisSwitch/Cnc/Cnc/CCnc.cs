﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Initdata;
using UCNotifier;
//
using Command;
using GCode;
using Hardware;
//
namespace Cnc
{
  public class CCnc
  {
    private CNotifier FNotifier;
    private CGCode FGCode;

    public CCnc()
    {
      FGCode = new CGCode();
    }

    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FGCode.SetNotifier(value);
    }

    public void SetOnResponse(DOnResponse value)
    {
      FGCode.SetOnResponse(value);
    }

    public void SetOnExecuteCommand(DOnExecuteCommand value)
    {
      FGCode.SetOnExecuteCommand(value);
    }

    public void SetOnLimitSwitchState(DOnLimitSwitchState value)
    {
      FGCode.SetOnLimitSwitchState(value);
    }



    public Boolean TranslateProgram(String text)
    {
      return FGCode.TranslateProgram(text);
    }

    public Boolean ExecuteProgram(ref CSystemParameter systemparameter)
    {
      return FGCode.ExecuteProgram(ref systemparameter);
    }

    public Boolean ExecuteSequence(Boolean reset, Int32 commandcount,
                                   ref CSystemParameter systemparameter)
    {
      return FGCode.ExecuteSequence(reset, commandcount, ref systemparameter);
    }

  }
}
