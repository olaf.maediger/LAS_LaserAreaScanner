﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Initdata;
using UCNotifier;
using Command;
using Hardware;
//
namespace GCode
{
  public class CStringlist : List<String>
  {
  }
 
  public class CGCode
  {
    //  <space> : Separator / <tab> : ignore
    //  <%> - Program Start
    //  { 
    //    <;><x><CR>|<LF> - Comment
    //    <(><x><)> - Comment |
    //    <characterA..Z><space>(0,n)<integer> |
    //    <characterA..Z><space>(0,n)<float> 
    //  }
    //  <%> - Program End 

    private CNotifier FNotifier;
    private CStringlist FTokenlist;
    private Int32 FIndexToken;
    private CCommandlist FCommandlist;
    private DOnResponse FOnResponse;
    private DOnExecuteCommand FOnExecuteCommand;
    private DOnLimitSwitchState FOnLimitSwitchState;
    private RCommandParameter FCommandParameter;
    //private DGetSystemData FGetSystemData;
    //private DSetSystemData FSetSystemData;

    public CGCode()//DGetSystemData getsystemdata,
                  //DSetSystemData setsystemdata)
    {
      FCommandlist = new CCommandlist();
      FCommandParameter = new RCommandParameter(0);
      //FGetSystemData = getsystemdata;
      //FSetSystemData = setsystemdata;
    }

    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnResponse(DOnResponse value)
    {
      FOnResponse = value;
    }

    public void SetOnExecuteCommand(DOnExecuteCommand value)
    {
      FOnExecuteCommand = value;
    }

    public void SetOnLimitSwitchState(DOnLimitSwitchState value)
    {
      FOnLimitSwitchState = value;
    }

    private Boolean TranslateInt32(string text, out Int32 value)
    {
      try
      {
        return Int32.TryParse(text, out value);
      }
      catch (Exception)
      {
        value = 0;
        return false;
      }
    }

    private Boolean TranslateDouble(string text, out Double value)
    {
      try
      {
        return Double.TryParse(text, out value);
      }
      catch (Exception)
      {
        value = 0.0;
        return false;
      }
    }

    private Boolean TranslateInt32Double(String axis, out Double value)
    {
      String Token = FTokenlist[FIndexToken];
      FIndexToken++;
      String SValue = Token.Substring(1);
      Int32 IValue;
      if (TranslateInt32(SValue, out IValue))
      {
        FNotifier.Write(axis + SValue);
        value = (Double)IValue;
        return true;
      }
      Double DValue;
      if (TranslateDouble(SValue, out DValue))
      {
        FNotifier.Write(axis + SValue);
        value = DValue;
        return true;
      }
      FNotifier.Write(String.Format("Error: {0}-Code <{1}>", axis, SValue));
      value = 0.0;
      return false;
    }



    private Boolean TranslateG()
    {
      String Token = FTokenlist[FIndexToken];
      FIndexToken++;
      String SValue = "";
      if (Token.Length <= 3)
      {
        SValue = Token.Substring(1);
      }
      else
      { // more than 3 characters
        SValue = Token.Substring(1, 2);
      }
      switch (SValue)
      {
        case "00":
        case "0":
          FNotifier.Write("G00");
          FCommandlist.Add(new CCommandG00(FNotifier));
          break;
        case "01":
        case "1":
          FNotifier.Write("G01");
          FCommandlist.Add(new CCommandG01(FNotifier));
          break;
        case "02":
        case "2":
          FNotifier.Write("G02");
          FCommandlist.Add(new CCommandG02(FNotifier));
          break;
        case "03":
        case "3":
          FNotifier.Write("G" + SValue);
          break;
        case "04":
        case "4":
          FNotifier.Write("G" + SValue);
          break;
        case "05":
        case "5":
          FNotifier.Write("G" + SValue);
          break;
        case "05.1":
        case "5.1":
          FNotifier.Write("G" + SValue);
          break;
        case "06.1":
        case "6.1":
          FNotifier.Write("G" + SValue);
          break;
        case "07":
        case "7":
          FNotifier.Write("G" + SValue);
          break;
        case "09":
        case "9":
          FNotifier.Write("G" + SValue);
          break;
        case "10":
          FNotifier.Write("G" + SValue);
          break;
        case "11":
          FNotifier.Write("G" + SValue);
          break;
        case "12":
          FNotifier.Write("G" + SValue);
          break;
        case "13":
          FNotifier.Write("G" + SValue);
          break;
        case "17":
          FNotifier.Write("G" + SValue);
          FCommandlist.Add(new CCommandG17(FNotifier));
          break;
        case "18":
          FNotifier.Write("G" + SValue);
          break;
        case "19":
          FNotifier.Write("G" + SValue);
          break;
        case "20":
          FNotifier.Write("G" + SValue);
          FCommandlist.Add(new CCommandG20(FNotifier));
          break;
        case "21":
          FNotifier.Write("G" + SValue);
          FCommandlist.Add(new CCommandG21(FNotifier));
          break;
        case "28":
          FNotifier.Write("G" + SValue);
          break;
        case "30":
          FNotifier.Write("G" + SValue);
          break;
        case "31":
          FNotifier.Write("G" + SValue);
          break;
        case "32":
          FNotifier.Write("G" + SValue);
          break;
        case "33":
          FNotifier.Write("G" + SValue);
          break;
        case "34":
          FNotifier.Write("G" + SValue);
          break;
        case "40":
          FNotifier.Write("G" + SValue);
          FCommandlist.Add(new CCommandG40(FNotifier));
          break;
        case "41":
          FNotifier.Write("G" + SValue);
          break;
        case "42":
          FNotifier.Write("G" + SValue);
          break;
        case "43":
          FNotifier.Write("G" + SValue);
          break;
        case "44":
          FNotifier.Write("G" + SValue);
          break;
        case "45":
          FNotifier.Write("G" + SValue);
          break;
        case "46":
          FNotifier.Write("G" + SValue);
          break;
        case "47":
          FNotifier.Write("G" + SValue);
          break;
        case "48":
          FNotifier.Write("G" + SValue);
          break;
        case "49":
          FNotifier.Write("G" + SValue);
          break;
        case "50":
          FNotifier.Write("G" + SValue);
          break;
        case "52":
          FNotifier.Write("G" + SValue);
          break;
        case "53":
          FNotifier.Write("G" + SValue);
          break;
        case "54":
          FNotifier.Write("G54");
          FCommandlist.Add(new CCommandG54(FNotifier));
          break;
        case "55":
          FNotifier.Write("G" + SValue);
          break;
        case "56":
          FNotifier.Write("G" + SValue);
          break;
        case "57":
          FNotifier.Write("G" + SValue);
          break;
        case "58":
          FNotifier.Write("G" + SValue);
          break;
        case "59":
          FNotifier.Write("G" + SValue);
          break;
        case "61":
          FNotifier.Write("G" + SValue);
          break;
        case "62":
          FNotifier.Write("G" + SValue);
          break;
        case "64":
          FNotifier.Write("G" + SValue);
          break;
        case "70":
          FNotifier.Write("G" + SValue);
          break;
        case "71":
          FNotifier.Write("G" + SValue);
          break;
        case "72":
          FNotifier.Write("G" + SValue);
          break;
        case "73":
          FNotifier.Write("G" + SValue);
          break;
        case "74":
          FNotifier.Write("G" + SValue);
          break;
        case "75":
          FNotifier.Write("G" + SValue);
          break;
        case "76":
          FNotifier.Write("G" + SValue);
          break;
        case "80":
          FNotifier.Write("G" + SValue);
          break;
        case "81":
          FNotifier.Write("G" + SValue);
          break;
        case "82":
          FNotifier.Write("G" + SValue);
          break;
        case "83":
          FNotifier.Write("G" + SValue);
          break;
        case "84":
          FNotifier.Write("G" + SValue);
          break;
        case "84.2":
          FNotifier.Write("G" + SValue);
          break;
        case "84.3":
          FNotifier.Write("G" + SValue);
          break;
        case "85":
          FNotifier.Write("G" + SValue);
          break;
        case "86":
          FNotifier.Write("G" + SValue);
          break;
        case "87":
          FNotifier.Write("G" + SValue);
          break;
        case "88":
          FNotifier.Write("G" + SValue);
          break;
        case "89":
          FNotifier.Write("G" + SValue);
          break;
        case "90":
          FNotifier.Write("G90");
          FCommandlist.Add(new CCommandG90(FNotifier));
          break;
        case "91":
          String SParameter = Token.Substring(3);
          FNotifier.Write("G" + SValue + SParameter);
          Double DValue;
          if (TranslateDouble(SParameter, out DValue))
          {
            FCommandlist.Add(new CCommandG91(FNotifier, DValue));
            return true;
          } 
          FNotifier.Write("Error - G-Code" + SValue);
          return false;
        case "92":
          FNotifier.Write("G" + SValue);
          break;
        case "94":
          FNotifier.Write("G94");
          FCommandlist.Add(new CCommandG94(FNotifier));
          break;
        case "95":
          FNotifier.Write("G" + SValue);
          break;
        case "96":
          FNotifier.Write("G" + SValue);
          break;
        case "97":
          FNotifier.Write("G" + SValue);
          break;
        case "98":
          FNotifier.Write("G" + SValue);
          break;
        case "99":
          FNotifier.Write("G" + SValue);
          break;
        default:
          FNotifier.Write("Error - G-Code" + SValue);
          return false;
      }
      return true;
    }

    private Boolean TranslateL()
    {
      String Token = FTokenlist[FIndexToken];
      FIndexToken++;
      String SValue = Token.Substring(1);
      Int32 IValue;
      if (Int32.TryParse(SValue, out IValue))
      {
        FNotifier.Write("L" + SValue);
        FCommandlist.Add(new CCommandL(FNotifier, IValue));
        return true;
      }
      FNotifier.Write("Error - L-Code " + SValue);
      return false;
    }

    private Boolean TranslateM()
    {
      String Token = FTokenlist[FIndexToken];
      FIndexToken++;
      String SValue = Token.Substring(1);
      switch (SValue)
      {
        case "00":
        case "0":
          FNotifier.Write("M00");
          FCommandlist.Add(new CCommandM00(FNotifier));
          break;
        case "01":
        case "1":
          FNotifier.Write("M01");
          FCommandlist.Add(new CCommandM01(FNotifier));
          break;
        case "02":
        case "2":
          FNotifier.Write("M02");
          FCommandlist.Add(new CCommandM02(FNotifier));
          break;
        case "03":
        case "3":
          FNotifier.Write("M03");
          FCommandlist.Add(new CCommandM03(FNotifier));
          break;
        case "04":
        case "4":
          FNotifier.Write("M04");
          FCommandlist.Add(new CCommandM04(FNotifier));
          break;
        case "05":
        case "5":
          FNotifier.Write("M05");
          FCommandlist.Add(new CCommandM05(FNotifier));
          break;
        case "06":
        case "6":
          FNotifier.Write("M06");
          FCommandlist.Add(new CCommandM06(FNotifier));
          break;
        case "07":
        case "7":
          FNotifier.Write("M07");
          FCommandlist.Add(new CCommandM07(FNotifier));
          break;
        case "08":
        case "8":
          FNotifier.Write("M08");
          FCommandlist.Add(new CCommandM08(FNotifier));
          break;
        case "09":
        case "9":
          FNotifier.Write("M09");
          FCommandlist.Add(new CCommandM09(FNotifier));
          break;
        case "30":
          FNotifier.Write("M30");
          FCommandlist.Add(new CCommandM30(FNotifier));
          break;
        case "98":
          FNotifier.Write("M98");
          FCommandlist.Add(new CCommandM98(FNotifier));
          break;
        case "99":
          FNotifier.Write("M99");
          FCommandlist.Add(new CCommandM99(FNotifier));
          break;
        case "119":
          FNotifier.Write("M119");
          FCommandlist.Add(new CCommandM119(FNotifier));
          break;
        default:
          FNotifier.Write("Error - M-Code " + SValue);
          return false;
      }
      return true;
    }

    private Boolean TranslateN()
    {
      String Token = FTokenlist[FIndexToken];
      FIndexToken++;
      String SValue = Token.Substring(1);
      Int32 IValue;
      if (Int32.TryParse(SValue, out IValue))
      {
        FNotifier.Write("N" + SValue);
        FCommandlist.Add(new CCommandN(FNotifier, IValue));
        return true;
      }
      FNotifier.Write("Error - N-Code " + SValue);
      return false;
    }

    private Boolean TranslateO()
    {
      String Token = FTokenlist[FIndexToken];
      FIndexToken++;
      String SValue = Token.Substring(1);
      Int32 IValue;
      if (Int32.TryParse(SValue, out IValue))
      {
        FNotifier.Write("O" + SValue);
        FCommandlist.Add(new CCommandO(FNotifier, IValue));
        return true;
      }
      FNotifier.Write("Error - O-Code " + SValue);
      return false;
    }

    private Boolean TranslateP()
    {
      String Token = FTokenlist[FIndexToken];
      FIndexToken++;
      String SValue = Token.Substring(1);
      if (0 < SValue.Length)
      {
        FNotifier.Write("P" + SValue);
        FCommandlist.Add(new CCommandP(FNotifier, SValue));
        return true;
      }
      FNotifier.Write("Error - P-Code " + SValue);
      return false;
    }

    private Boolean TranslateS()
    {
      String Token = FTokenlist[FIndexToken];
      FIndexToken++;
      String SValue = Token.Substring(1);
      Int32 IValue;
      if (Int32.TryParse(SValue, out IValue))
      {
        FNotifier.Write("S" + SValue);
        FCommandlist.Add(new CCommandS(FNotifier, IValue));
        return true;
      }
      FNotifier.Write("Error - S-Code " + SValue);
      return false;
    }

    private Boolean ReduceVectorToken(String[] vectortoken)
    {
      Int32 TC = vectortoken.Length;
      FTokenlist = new CStringlist();
      Int32 TI = 0; 
      while (TI < TC)
      {
        String Token = vectortoken[TI];
        switch (Token.ToUpper())
        {
          case "A":
          case "B":
          case "C":
          case "D":
          case "F":
          case "G":
          case "I":
          case "J":
          case "K":
          case "L":
          case "M":
          case "R":
          case "X":
          case "Y":
          case "Z":
            TI++;
            FTokenlist.Add(Token + vectortoken[TI]);
            break;
          default:
            FTokenlist.Add(Token);
            break;
        }
        TI++;
      }
      return (0 < FTokenlist.Count);
    }

    private Boolean AnalyseResponse()
    {
      String Token = FTokenlist[FIndexToken];
      // LimitSwitch X
      if (7 == Token.Length)
      {
        String TokenHeader = Token.Substring(0, 6);
        String TokenArgument = Token.Substring(6, 1);
        if ("x_min:" == TokenHeader)
        {
          if ("H" == TokenArgument)
          {
            //!!!!!!!!!!FCommandParameter.LimitSwitchX.Low = ESwitchState.High;
            FNotifier.Write("M119: Switch XLow H");
            FCommandlist.Add(new CResponseM119(FNotifier, "XLow", "H"));
          }
          else
            if ("L" == TokenArgument)
            {
              //!!!!!!!!!!FCommandParameter.LimitSwitchX.Low = ESwitchState.Low;
              FNotifier.Write("M119: Switch XLow L");
              FCommandlist.Add(new CResponseM119(FNotifier, "XLow", "L"));
            }
          FIndexToken++;
          return true;
        }
        if ("x_ref:" == TokenHeader)
        {
          if ("H" == TokenArgument)
          {
            //!!!!!!!!!!FSystParameter.LimitSwitchX.Reference = ESwitchState.High;
            FNotifier.Write("M119: Switch XReference H");
            FCommandlist.Add(new CResponseM119(FNotifier, "XReference", "H"));
          }
          else
            if ("L" == TokenArgument)
            {
              //!!!!!!!!!!FSystemData.LimitSwitchX.Reference = ESwitchState.Low;
              FNotifier.Write("M119: Switch XReferenceL");
              FCommandlist.Add(new CResponseM119(FNotifier, "XReference", "L"));
            }
          FIndexToken++;
          return true;
        }
        if ("x_max:" == TokenHeader)
        {
          if ("H" == TokenArgument)
          {
            //!!!!!!!!!!FSystemData.LimitSwitchX.High = ESwitchState.High;
            FNotifier.Write("M119: Switch XHigh H");
            FCommandlist.Add(new CResponseM119(FNotifier, "XHigh", "H"));
          }
          else
            if ("L" == TokenArgument)
            {
              //!!!!!!!!!!FSystemData.LimitSwitchX.High = ESwitchState.Low;
              FNotifier.Write("M119: Switch XHigh L");
              FCommandlist.Add(new CResponseM119(FNotifier, "XHigh", "L"));
            }
          FIndexToken++;
          return true;
        }
        // LimitSwitch Y
        if ("y_min:" == TokenHeader)
        {
          if ("H" == TokenArgument)
          {
            //!!!!!!!!!!FSystemData.LimitSwitchY.Low = ESwitchState.High;
            FNotifier.Write("M119: Switch YLow H");
            FCommandlist.Add(new CResponseM119(FNotifier, "YLow", "H"));
          }
          else
            if ("L" == TokenArgument)
            {
              //!!!!!!!!!!FSystemData.LimitSwitchY.Low = ESwitchState.Low;
              FNotifier.Write("M119: Switch YLow L");
              FCommandlist.Add(new CResponseM119(FNotifier, "YLow", "L"));
            }
          FIndexToken++;
          return true;
        }
        if ("y_ref:" == TokenHeader)
        {
          if ("H" == TokenArgument)
          {
            //!!!!!!!!!!FSystemData.LimitSwitchY.Reference = ESwitchState.High;
            FNotifier.Write("M119: Switch YReference H");
            FCommandlist.Add(new CResponseM119(FNotifier, "YReference", "H"));
          }
          else
            if ("L" == TokenArgument)
            {
              //!!!!!!!!!!FSystemData.LimitSwitchY.Reference = ESwitchState.Low;
              FNotifier.Write("M119: Switch YReference L");
              FCommandlist.Add(new CResponseM119(FNotifier, "YReference", "L"));
            }
          FIndexToken++;
          return true;
        }
        if ("y_max:" == TokenHeader)
        {
          if ("H" == TokenArgument)
          {
            //!!!!!!!!!!FSystemData.LimitSwitchY.High = ESwitchState.High;
            FNotifier.Write("M119: Switch YHigh H");
            FCommandlist.Add(new CResponseM119(FNotifier, "YHigh", "H"));
          }
          else
            if ("L" == TokenArgument)
            {
              //!!!!!!!!!!FSystemData.LimitSwitchY.High = ESwitchState.Low;
              FNotifier.Write("M119: Switch YHigh L");
              FCommandlist.Add(new CResponseM119(FNotifier, "YHigh", "L"));
            }
          FIndexToken++;
          return true;
        }
        // LimitSwitch Z
        if ("z_min:" == TokenHeader)
        {
          if ("H" == TokenArgument)
          {
            //!!!!!!!!!!FSystemData.LimitSwitchZ.Low = ESwitchState.High;
            FNotifier.Write("M119: Switch ZLow H");
            FCommandlist.Add(new CResponseM119(FNotifier, "ZLow", "H"));
          }
          else
            if ("L" == TokenArgument)
            {
              //!!!!!!!!!!FSystemData.LimitSwitchZ.Low = ESwitchState.Low;
              FNotifier.Write("M119: Switch ZLow L");
              FCommandlist.Add(new CResponseM119(FNotifier, "ZLow", "L"));
            }
          FIndexToken++;
          return true;
        }
        if ("z_ref:" == TokenHeader)
        {
          if ("H" == TokenArgument)
          {
            //!!!!!!!!!!FSystemData.LimitSwitchZ.Reference = ESwitchState.High;
            FNotifier.Write("M119: Switch ZReference H");
            FCommandlist.Add(new CResponseM119(FNotifier, "ZReference", "H"));
          }
          else
            if ("L" == TokenArgument)
            {
              //!!!!!!!!!!FSystemData.LimitSwitchZ.Reference = ESwitchState.Low;
              FNotifier.Write("M119: Switch ZReference L");
              FCommandlist.Add(new CResponseM119(FNotifier, "ZReference", "L"));
            }
          FIndexToken++;
          return true;
        }
        if ("z_max:" == TokenHeader)
        {
          if ("H" == TokenArgument)
          {
            //!!!!!!!!!!FSystemData.LimitSwitchZ.High = ESwitchState.High;
            FNotifier.Write("M119: Switch ZHigh H");
            FCommandlist.Add(new CResponseM119(FNotifier, "ZHigh", "H"));
          }
          else
            if ("L" == TokenArgument)
            {
              //!!!!!!!!!!FSystemData.LimitSwitchZ.High = ESwitchState.Low;
              FNotifier.Write("M119: Switch ZHigh L");
              FCommandlist.Add(new CResponseM119(FNotifier, "ZHigh", "L"));
            }
          FIndexToken++;
          return true;
        }
      }
      return false;
    }

    public Boolean TranslateProgram(String text)
    {
      FNotifier.Write("*** Translate Program:");
      String[] VectorToken = text.Split(new Char[] {' ', '\t', '\r', '\n'}, 
                                        StringSplitOptions.RemoveEmptyEntries);
      FIndexToken = 0;
      if (!(FCommandlist is CCommandlist))
      {
        FCommandlist = new CCommandlist();
      }
      else
      {
        FCommandlist.Clear();
      }
      //
      if (!ReduceVectorToken(VectorToken))
      {
        FNotifier.Write("--- Result: Error");
        return false;
      }
      //
      Boolean ProgramActive = false;
      //
      Boolean CommentActive = false;
      String CommentBuffer = "";
      //
      Boolean Result = true;
      Boolean Loop = true;
      do
      {
        String Token = FTokenlist[FIndexToken];
        //######################################################################
        //  Analyse Response 'x_min:'
        //######################################################################
        if (!AnalyseResponse())
        {
          Char FirstCharacter = Token[0];
          //######################################################################
          //  Comment '(' .. ')'
          //######################################################################
          if ('(' == FirstCharacter)
          {
            CommentActive = true;
            CommentBuffer = "Comment<";
            if (Token.Contains(')'))
            {
              CommentActive = false;
              FNotifier.Write(Token + ">");
            }
            else
            {
              CommentBuffer += Token;
            }
            FIndexToken++;
          }
          else
            if (Token.Contains(')'))
            {
              CommentActive = false;
              FNotifier.Write(CommentBuffer + " " + Token + ">");
              FIndexToken++;
            }
            else
              if (CommentActive)
              {
                CommentBuffer += " " + Token;
                FIndexToken++;
              }
              else
              {
                //######################################################################
                //  No Response / No Comment
                //######################################################################
                Double DValue;
                switch (Char.ToUpper(FirstCharacter))
                {
                  case '%':
                    if (ProgramActive)
                    {
                      FNotifier.Write("% - Program End");
                      FNotifier.Write("--- Result: Success");
                      FCommandlist.Add(new CCommandEnd(FNotifier));
                      ProgramActive = false;
                      return true;
                    }
                    else
                    {
                      FNotifier.Write("% - Program Start");
                      FCommandlist.Add(new CCommandBegin(FNotifier));
                      ProgramActive = true;
                    }
                    FIndexToken++;
                    break;
                  case 'A':
                    FNotifier.Error("GCode", 1, "Unknown Command A");
                    Result = false;
                    break;
                  case 'B':
                    FNotifier.Error("GCode", 1, "Unknown Command B");
                    Result = false;
                    break;
                  case 'C':
                    FNotifier.Error("GCode", 1, "Unknown Command C");
                    Result = false;
                    break;
                  case 'D':
                    FNotifier.Error("GCode", 1, "Unknown Command D");
                    Result = false;
                    break;
                  case 'E':
                    FNotifier.Error("GCode", 1, "Unknown Command E");
                    Result = false;
                    break;
                  case 'F':
                    if (TranslateInt32Double("F", out DValue))
                    {
                      CCommand Command = new CCommandF(FNotifier, DValue);
                      FCommandlist.Add(Command);
                    }
                    else
                    {
                      Result = false;
                    }
                    break;
                  case 'G':
                    Result &= TranslateG();
                    break;
                  case 'H':
                    FNotifier.Error("GCode", 1, "Unknown Command H");
                    Result = false;
                    break;
                  case 'I':
                    if (TranslateInt32Double("I", out DValue))
                    {
                      CCommand Command = new CCommandI(FNotifier, DValue);
                      FCommandlist.Add(Command);
                    }
                    else
                    {
                      Result = false;
                    }
                    break;
                  case 'J':
                    if (TranslateInt32Double("J", out DValue))
                    {
                      CCommand Command = new CCommandJ(FNotifier, DValue);
                      FCommandlist.Add(Command);
                    }
                    else
                    {
                      Result = false;
                    }
                    break;
                  case 'K':
                    if (TranslateInt32Double("K", out DValue))
                    {
                      CCommand Command = new CCommandK(FNotifier,DValue);
                      FCommandlist.Add(Command);
                    }
                    else
                    {
                      Result = false;
                    }
                    break;
                  case 'L':
                    Result &= TranslateL();
                    break;
                  case 'M':
                    Result &= TranslateM();
                    break;
                  case 'N':
                    Result &= TranslateN();
                    break;
                  case 'O':
                    Result &= TranslateO();
                    break;
                  case 'P':
                    Result &= TranslateP();
                    break;
                  case 'Q':
                    Result = false;
                    break;
                  case 'R':
                    if (TranslateInt32Double("R", out DValue))
                    {
                      CCommand Command = new CCommandR(FNotifier, DValue);
                      FCommandlist.Add(Command);
                    }
                    else
                    {
                      Result = false;
                    }
                    break;
                  case 'S':
                    Result &= TranslateS();
                    break;
                  case 'T':
                    Result = false;
                    break;
                  case 'U':
                    Result = false;
                    break;
                  case 'V':
                    Result = false;
                    break;
                  case 'W':
                    Result = false;
                    break;
                  case 'X':
                    if (TranslateInt32Double("X", out DValue))
                    {
                      CCommand Command = new CCommandX(FNotifier, DValue);
                      FCommandlist.Add(Command);
                    }
                    else
                    {
                      Result = false;
                    }
                    break;
                  case 'Y':
                    if (TranslateInt32Double("Y", out DValue))
                    {
                      CCommand Command = new CCommandY(FNotifier, DValue);
                      FCommandlist.Add(Command);
                    }
                    else
                    {
                      Result = false;
                    }
                    break;
                  case 'Z':
                    if (TranslateInt32Double("Z", out DValue))
                    {
                      CCommand Command = new CCommandZ(FNotifier, DValue);
                      FCommandlist.Add(Command);
                    }
                    else
                    {
                      Result = false;
                    }
                    break;
                }
              }
        }
        if (FTokenlist.Count <= FIndexToken)
        {
          // no % required FNotifier.Error("GCode", 2, "Missing Program End <%>");
          Loop = false;
        }
      }
      //while (ProgramActive && Result);
      // no % required
      while (Result && Loop);
      if (!Result)
      {
        FNotifier.Write("--- Result: Error");
        return false;
      } 
      FNotifier.Write("--- Result: Success");
      return true;
    }

    private void SystemDataOnResponse(String response)
    {
      if (FOnResponse is DOnResponse)
      {
        FOnResponse(response);
      }
    }

    private void SystemDataOnExecuteCommand(CCommand command, RCommandParameter systemdata)
    {
      if (FOnExecuteCommand is DOnExecuteCommand)
      {
        FOnExecuteCommand(command, systemdata);
      }
    }

    private void SystemDataOnLimitSwitchState(RCommandParameter systemdata)
    {
      if (FOnLimitSwitchState is DOnLimitSwitchState)
      {
        FOnLimitSwitchState(systemdata);
      }
    }

    public Boolean ExecuteProgram(ref CSystemParameter systemparameter)
    {
      FNotifier.Write("*** Execute Program:");
      FCommandParameter = new RCommandParameter(0);
      FCommandParameter.CommandIndex = 0;
      FCommandParameter.CommandCountPreset = 0;
      FCommandParameter.CommandCountActual = 0;
      FCommandParameter.OnResponse = SystemDataOnResponse;
      FCommandParameter.OnExecuteCommand = SystemDataOnExecuteCommand;
      FCommandParameter.OnLimitSwitchState = SystemDataOnLimitSwitchState;
      //
      if (FCommandlist.Execute(ref FCommandParameter, ref systemparameter))
      {
        FNotifier.Write("--- Result: Success");
      }
      else
      {
        FNotifier.Write("--- Result: Error");
      }
      return true;
    }

    public Boolean ExecuteSequence(Boolean reset, Int32 commandcount,
                                   ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("*** Execute Sequence[{0}]:", commandcount));
      if (reset)
      {
        FCommandParameter.CommandIndex = 0;
      }
      // AutoReset:
      if (0 < FCommandlist.Count)
      {
        CCommand Command = FCommandlist[FCommandParameter.CommandIndex];
        if (Command is CCommandM02)
        {
          FCommandParameter = new RCommandParameter(0);
          FCommandParameter.CommandIndex = 0;
        }
      }
      FCommandParameter.CommandCountPreset = commandcount;
      FCommandParameter.CommandCountActual = 0;
      if (FCommandlist.Execute(ref FCommandParameter, ref systemparameter))
      {
        FNotifier.Write("--- Result: Success");
      }
      else
      {
        FNotifier.Write("--- Result: Error");
      }
      return true;
    }


  }
}
