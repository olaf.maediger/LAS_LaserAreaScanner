﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using Task;
using UCNotifier;
using Hardware;
//
namespace Simulation
{



  public delegate CSystemParameter DGetSystemParameter();
  public delegate void DSetSystemParameter(CSystemParameter value);

  public enum ESimulationState
  {
    Error,
    Init,
    Idle,
    Move,
    LeaveLimit,
    Shutdown,
    Dead
  }

  public struct RSimulationData
  {
    public const Int32 INIT_TIMERESOLUTION = 200;
    public const Int32 INIT_COUNTINTERVAL = 0;
    //
    public ESimulationState State;
    public Int32 TimeResolution;
    public Int32 CountInterval;
    //
    public RSimulationData(Int32 init)
    {
      State = ESimulationState.Error;
      TimeResolution = INIT_TIMERESOLUTION;
      CountInterval = INIT_COUNTINTERVAL;
    }
  }

  public class CSimulation
  {
    private CNotifier FNotifier;
    private RSimulationData FData;
    private CTask FTask;
    private DGetSystemParameter FGetSystemParameter;
    private DSetSystemParameter FSetSystemParameter;
   
    public CSimulation(CNotifier notifier,
                       DGetSystemParameter getsystemparameter,
                       DSetSystemParameter setsystemparameter)
    {
      FNotifier = notifier;
      FGetSystemParameter = getsystemparameter;
      FSetSystemParameter = setsystemparameter;
      FData = new RSimulationData(0);
      FTask = new CTask("Simulation", 
                        OnExecutionStart, 
                        OnExecutionBusy, 
                        OnExecutionEnd, 
                        OnExecutionAbort);
      FData.State = ESimulationState.Error;
      // ERROR !!! FTask.Start(); external!!! 
    }


    private void SetSimulationState(ESimulationState value)
    {
      if (value != FData.State)
      {
        String Line = String.Format("--- Simulation: State[{0}] -> [{1}]", FData.State, value);
        FData.State = value;
        FNotifier.Write(Line);
        FData.CountInterval = 0;
      }
    }

    public ESimulationState State
    {
      get { return FData.State; }
      set { SetSimulationState(value); }
    }

    private void SetTimeResolution(Int32 timeresolution)
    {
      FData.TimeResolution = timeresolution;
      CSystemParameter SP = FGetSystemParameter();
      foreach (CAxis Axis in SP.Axes)
      {
        RAxisData AD = Axis.GetData();
        AD.TimeResolution = timeresolution;
        Axis.SetData(AD);
      }
      FSetSystemParameter(SP);
    }
    public Int32 TimeResolution
    {
      get { return FData.TimeResolution; }
      set { SetTimeResolution(value); }
    }
    //
    //-------------------------------------------------------------
    //  Segment - Helper
    //-------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------
    //  Segment - Callback - Commands
    //-------------------------------------------------------------
    //

    //
    //----------------------------------------------------------
    //  Section - State Execution
    //----------------------------------------------------------
    //
    private void OnError()
    {
      FNotifier.Write("--- Simulation: OnError...");
      State = ESimulationState.Error;
      // only new start !!!
      while (true)
      {
        Thread.Sleep(FData.TimeResolution);
      }
    }

    private void OnError(Int32 errorcode, String errortext)
    {
      FNotifier.Error("", errorcode, errortext);
      OnError();
    }

    private void OnClosed()
    {
      FNotifier.Write("--- Simulation: OnClosed...");
      Thread.Sleep(FData.TimeResolution);
    }

    private void OnInit()
    {
      FNotifier.Write("--- Simulation: OnInit...");
      //
      CSystemParameter SP = FGetSystemParameter();
      SP.Axes.Clear();
      SP.Axes.Add(new CAxis(FNotifier, EAxis.X, EAxisKind.Translation, FData.TimeResolution));
      SP.Axes.Add(new CAxis(FNotifier, EAxis.Y, EAxisKind.Translation, FData.TimeResolution));
      SP.Axes.Add(new CAxis(FNotifier, EAxis.Z, EAxisKind.Translation, FData.TimeResolution));
      //
      if (SP.AllAxesIdle())
      {
        FSetSystemParameter(SP);
        State = ESimulationState.Idle;
      }
      else
      {
        FSetSystemParameter(SP);
        State = ESimulationState.LeaveLimit;
      }
    }

    private void OnLeaveLimit()
    {
      FNotifier.Write(String.Format("--- Simulation: OnLeaveLimit[{0}/10]...", FData.CountInterval));
      CSystemParameter SP = FGetSystemParameter();
      if (SP.AllAxesIdle())
      {
        State = ESimulationState.Idle;
      }
      else
      {
        Thread.Sleep(FData.TimeResolution);
        //
        FData.CountInterval++;
        if (10 <= FData.CountInterval)
        {
          OnError(1, "Initialisation of Axis failed");
        }
      }
      FSetSystemParameter(SP);
    }

    private void OnIdle()
    {
      // debug FNotifier.Write("--- Simulation: OnIdle...");
      //
      Thread.Sleep(FData.TimeResolution);
    }

    private void OnMove()
    {
      FNotifier.Write("--- Simulation: OnMove...");
      //
      Thread.Sleep(FData.TimeResolution);
    }

    private void OnShutdown()
    {
      FNotifier.Write("--- Simulation: OnShutdown...");
      Thread.Sleep(FData.TimeResolution);
      //
      FData.CountInterval++;
      if (1 < FData.CountInterval)
      {
        State = ESimulationState.Dead;
      }
    }
    //
    //----------------------------------------------------------
    //  Section - Task - Callback
    //----------------------------------------------------------
    //
    private void OnExecutionStart(ref RTaskData data)
    {
      String Line = String.Format("*** OnExcutionStart");
      State = ESimulationState.Init;
      FNotifier.Write(Line);
    }

    private void  OnExecutionBusy(ref RTaskData data)
    {
      String Line = String.Format("*** OnExcutionBusy");
      FNotifier.Write(Line);
      Boolean Busy = true;
      while (Busy)
      {
        if (!data.Halt)
        {
          switch (State)
          {
            case ESimulationState.Init:
              OnInit();
              break;
            case ESimulationState.Idle:
              OnIdle();
              break;
            case ESimulationState.Move:
              OnMove();
              break;
            case ESimulationState.LeaveLimit:
              OnLeaveLimit();
              break;
            case ESimulationState.Error:
              OnError();
              break;
            case ESimulationState.Shutdown:
              OnShutdown();
              break;
            default:
              OnError(2, "Invalid Simulation State");
              break;
          }
        }
        Thread.Sleep(FData.TimeResolution);
      }
    } 

    private void  OnExecutionEnd(ref RTaskData data)
    {
      String Line = String.Format("*** OnExcutionEnd");
      FNotifier.Write(Line);
      OnShutdown();
    }

    private void  OnExecutionAbort(ref RTaskData data)
    {
      String Line = String.Format("*** OnExcutionAbort");
      FNotifier.Write(Line);
      OnShutdown();
    }
    //
    //----------------------------------------------------------
    //  Section - Task - Public
    //----------------------------------------------------------
    //
    public Boolean Start()
    {
      return FTask.Start();
    }

    public Boolean Halt()
    {
      Boolean Result = true;
      CSystemParameter SP = FGetSystemParameter();
      foreach (CAxis Axis in SP.Axes)
      {
        Result &= Axis.Halt();
      }
      Result &= FTask.Halt();
      FSetSystemParameter(SP);
      return Result;
    }

    public Boolean Resume()
    {
      Boolean Result = true;
      CSystemParameter SP = FGetSystemParameter();
      foreach (CAxis Axis in SP.Axes)
      {
        Result &= Axis.Resume();
      }
      Result &= FTask.Resume();
      FSetSystemParameter(SP);
      return Result;
    }

    public Boolean Abort()
    {
      CSystemParameter SP = FGetSystemParameter();
      Boolean Result = SP.Abort();
      Result &= FTask.Abort();
      FSetSystemParameter(SP);
      return Result;
    }


  }
}
