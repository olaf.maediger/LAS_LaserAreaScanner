﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CResponseM119 : CCommand
  {
    private String FHeader = "";
    private String FState = "";

    public CResponseM119(CNotifier notifier,
                         String header, 
                         String state)
      : base(notifier)
    {
      FNotifier = notifier;
      FHeader = header;
      FState = state;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("LimitSwitch[{0}]: State={1}", FHeader, FState)); 
      // "XLow/XReference/XHigh:=L/H"
      /*RSystemData SP;
      FGetSystemData(out SP);
      ESwitchState SS = ESwitchState.Low;
      if ("H" == FState.Substring(0, 1))
      {
        SS = ESwitchState.High;
      }
      switch (FHeader.Substring(0, 2).ToUpper())
      {
        case "XL":
          SP.LimitSwitchX.Low = SS;
          break;
        case "XR":
          SP.LimitSwitchX.Reference = SS;
          break;
        case "XH":
          SP.LimitSwitchX.High = SS;
          break;
        case "YL":
          SP.LimitSwitchY.Low = SS;
          break;
        case "YR":
          SP.LimitSwitchY.Reference = SS;
          break;
        case "YH":
          SP.LimitSwitchY.High = SS;
          break;
        case "ZL":
          SP.LimitSwitchZ.Low = SS;
          break;
        case "ZR":
          SP.LimitSwitchZ.Reference = SS;
          break;
        case "ZH":
          SP.LimitSwitchZ.High = SS;
          break;
      }
      if (commandparameter.OnLimitSwitchState is DOnLimitSwitchState)
      {
        commandparameter.OnLimitSwitchState(commandparameter);
      } */
      return true;
    }

  }
}



