﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandX : CCommand
  {
    private Double FPosition;

    public CCommandX(CNotifier notifier,
                     Double position)
      : base(notifier)
    {
      FNotifier = notifier;
      FPosition = position;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      RAxisData AD = systemparameter.AxisDataX;
      AD.PositionData.Target = FPosition;
      String S0 = String.Format(AD.PositionData.Format, AD.PositionData.Actual);
      String S1 = String.Format(AD.PositionData.Format, AD.PositionData.Target);
      FNotifier.Write(String.Format("X: Move X-Axis from {0} to {1} {2}",
                                    S0, S1, commandparameter.MeasurementUnit));
      systemparameter.AxisDataX = AD;
      return true;
    }

  }
}
