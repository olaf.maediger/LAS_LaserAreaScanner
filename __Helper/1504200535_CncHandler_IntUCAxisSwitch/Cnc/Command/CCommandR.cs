﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandR : CCommand
  {
    private Double FRadius;

    public CCommandR(CNotifier notifier,
                     Double radius)
      : base(notifier)
    {
      FNotifier = notifier;
      FRadius = radius;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      commandparameter.Radius = FRadius;
      FNotifier.Write(String.Format("R: Define Arc Radius: {0} {1}",
                                    String.Format(commandparameter.FormatRadius, commandparameter.Radius),
                                    commandparameter.MeasurementUnit));
      return true;
    }

  }
}
