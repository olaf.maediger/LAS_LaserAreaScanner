﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
/*
Formula to determine feed rate

This formula[10] can be used to figure out the feed rate that the cutter travels into or around the work. This would apply to cutters on a milling machine, drill press and a number of other machine tools. This is not to be used on the lathe for turning operations, as the feed rate on a lathe is given as feed per revolution.

FR = RPM x T x CL
 [FR] = 1/min x 1 x sizeunit

Where:
    FR = the calculated feed rate in inches per minute or mm per minute.
    RPM = is the calculated speed for the cutter.
    T = Number of teeth on the cutter.
    CL = The chip load or feed per tooth. This is the size of chip that each tooth of the cutter takes.
 
 sizeunit   Millimeter    Inch
 [FR]       mm/min        in/min
 
*/


namespace Command
{
  public class CCommandF : CCommand
  {
    private Double FFeedrate; // [sizeunit/min]

    public CCommandF(CNotifier notifier,
                     Double feedrate)
      : base(notifier)
    {
      FNotifier = notifier;
      FFeedrate = feedrate;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      /*RSystemData SP;
      FGetSystemData(out SP);
      FNotifier.Write(String.Format("F: Define Feed Rate: {0} {1}/min", 
                                    FFeedrate, commandparameter.MeasurementUnit));
      if (EMeasurementUnit.Millimeter == commandparameter.MeasurementUnit)
      { // mm/min -> mm/sec
        Double DX2 = (SP.PositionX.Target - SP.PositionX.Actual) * (SP.PositionX.Target - SP.PositionX.Actual);
        Double DY2 = (SP.PositionY.Target - SP.PositionY.Actual) * (SP.PositionY.Target - SP.PositionY.Actual);
        Double DZ2 = (SP.PositionZ.Target - SP.PositionZ.Actual) * (SP.PositionZ.Target - SP.PositionZ.Actual);
        Double DR2 = DX2 + DY2 + DZ2;
        if (0 < DR2)
        {
          SP.VelocityX.Target = Math.Sqrt(DX2) * FFeedrate / 60.0 / Math.Sqrt(DR2);
          SP.VelocityY.Target = Math.Sqrt(DY2) * FFeedrate / 60.0 / Math.Sqrt(DR2);
          SP.VelocityZ.Target = Math.Sqrt(DZ2) * FFeedrate / 60.0 / Math.Sqrt(DR2);
        }
      }
      else
        if (EMeasurementUnit.Millimeter == commandparameter.MeasurementUnit)
        { // inch/min -> mm/sec
          SP.VelocityX.Target = FFeedrate / 60 * 25.4;
        }
      FSetSystemData(SP);        */
      return true;
    }

  }
}
