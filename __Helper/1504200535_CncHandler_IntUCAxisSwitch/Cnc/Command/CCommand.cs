﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Initdata;
using Hardware;
using UCNotifier;
//
namespace Command
{
  public enum EMeasurementUnit
  {
    Millimeter = 0,
    Inch = 1
  }

  public enum EToolRadiusCompensation
  {
    Off = 0,
    Left = 1,
    Right = 2
  }

  public delegate void DOnResponse(String response);

  public delegate void DOnLimitSwitchState(RCommandParameter systemdata);


  public struct RCommandParameter
  {
    public Int32 CommandIndex;
    public Int32 CommandCountPreset;
    public Int32 CommandCountActual;
    public EMeasurementUnit MeasurementUnit;
    public String FormatLineNumber;
    public Int32 IParameter;
    public String SParameter;
    public String FormatRadius;
    public Double Radius;
    public Int32 LoopCount;
    public EToolRadiusCompensation ToolRadiusCompensation;
    //
    public DOnResponse OnResponse;
    public DOnExecuteCommand OnExecuteCommand;
    public DOnLimitSwitchState OnLimitSwitchState;
    //
    public RCommandParameter(Int32 init)
    {
      CommandIndex = 0;
      CommandCountPreset = 0;
      CommandCountActual = 0;
      MeasurementUnit = EMeasurementUnit.Millimeter;
      FormatLineNumber = "{0:00000}";
      IParameter = 0;
      SParameter = "";
      FormatRadius = "{0:+000.000}";
      Radius = 1.0;
      LoopCount = 0;
      ToolRadiusCompensation = EToolRadiusCompensation.Off;
      //
      OnResponse = null;
      OnExecuteCommand = null;
      OnLimitSwitchState = null;
    }
  }



  public abstract class CCommand
  {
    protected CNotifier FNotifier;
    //protected DGetSystemData FGetSystemData;
    //protected DSetSystemData FSetSystemData;

    public CCommand(CNotifier value)//,
      //DGetSystemData getsystemdata,
        //            DSetSystemData setsystemdata)
    {
      FNotifier = value;
      //FGetSystemData = getsystemdata;
      //FSetSystemData = setsystemdata;
    }

    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public abstract Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter); 
  }


}
