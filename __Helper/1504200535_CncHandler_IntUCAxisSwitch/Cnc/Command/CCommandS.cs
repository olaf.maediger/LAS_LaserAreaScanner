﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandS : CCommand
  {
    private Int32 FSpeed; // [rpm]

    public CCommandS(CNotifier notifier,
                     Int32 speed)
      : base(notifier)
    {
      FNotifier = notifier;
      FSpeed = speed;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("S: Define Spindle Speed: {0} rpm", FSpeed));
      return true;
    }

  }
}
