﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandG17 : CCommand
  {
    public CCommandG17(CNotifier notifier)//,
      //DGetSystemData getsystemdata,
      //DSetSystemData setsystemdata)
      : base(notifier)//, getsystemdata, setsystemdata)
    {
      FNotifier = notifier;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("G17: XY Plane Selection"));
      return true;
    }

  }
}
