﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandG02 : CCommand
  {
    public CCommandG02(CNotifier notifier)//,
      //DGetSystemData getsystemdata,
      //DSetSystemData setsystemdata)
      : base(notifier)//, getsystemdata, setsystemdata)
    {
      FNotifier = notifier;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("G02: Circular Interpolation"));
      return true;
    }

  }
}
