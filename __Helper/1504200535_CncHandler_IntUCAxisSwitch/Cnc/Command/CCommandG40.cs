﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandG40 : CCommand
  {
    public CCommandG40(CNotifier notifier)//,
      //DGetSystemData getsystemdata,
      //DSetSystemData setsystemdata)
      : base(notifier)//, getsystemdata, setsystemdata)
    {
      FNotifier = notifier;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      commandparameter.ToolRadiusCompensation = EToolRadiusCompensation.Off;
      FNotifier.Write(String.Format("G40: Tool Radius Compensation: Off"));
      return true;
    }

  }
}

