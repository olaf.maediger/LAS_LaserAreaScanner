﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Hardware;
//
namespace Command
{
  public delegate void DOnExecuteCommand(CCommand command, RCommandParameter systemdata);

  public class CIndexlist : List<Int32>
  {
  }



  public class CCommandlist : List<CCommand>
  {
    private CIndexlist FReturnlist;

    public CCommandlist()
    {
      FReturnlist = new CIndexlist();
    }

    private Boolean FindCommandIndex(Int32 programnumber, out Int32 commandindex)
    {
      Int32 CC = this.Count;
      for (Int32 CI = 0; CI < CC; CI++)
      {
        CCommand Command = this[CI];
        if (Command is CCommandO)
        {
          if (programnumber == ((CCommandO)Command).ProgramNumber)
          {
            commandindex = CI;
            return true;
          }
        }
      }
      commandindex = -1;
      return false;
    }

    public Boolean Execute(ref RCommandParameter commandparameter,
                           ref CSystemParameter systemparameter)
    {
      Boolean Result = (0 < this.Count);
      Boolean Busy = true;
      while (Result && Busy)
      {
        CCommand Command = this[commandparameter.CommandIndex];
        Result &= Command.Execute(ref commandparameter, ref systemparameter);
        if (commandparameter.OnExecuteCommand is DOnExecuteCommand)
        {
          commandparameter.OnExecuteCommand(Command, commandparameter);
        }
        //
        commandparameter.CommandCountActual++;
        if (0 < commandparameter.CommandCountPreset)
        {
          if (commandparameter.CommandCountPreset <= commandparameter.CommandCountActual)
          {
            FReturnlist.Clear();
            Busy = false;
          }
        }
        //
        if (Command is CCommandM02)
        { // End Program          
          FReturnlist.Clear();
          Busy = false;
        }
        else
          if (Command is CCommandM30)
          { // End Program
            FReturnlist.Clear();
            commandparameter.CommandIndex = 0;
            Busy = false;
          }
          else
            if (Command is CCommandM99)
            { // End Subprogram
              // restore indexcommand before calling subroutine
              Int32 ReturnIndex = FReturnlist[FReturnlist.Count - 1];
              FReturnlist.Remove(FReturnlist.Count - 1);
              commandparameter.CommandIndex = ReturnIndex;
            }
            else
              if (commandparameter.CommandIndex < this.Count - 1)
              {
                commandparameter.CommandIndex++;
                if (Command is CCommandM98)
                { // Call Supprogram
                  Command = this[commandparameter.CommandIndex];
                  if (Command is CCommandP)
                  { // secure Backstep from subroutine
                    FReturnlist.Add(1 + commandparameter.CommandIndex);
                    // call to subroutine
                    Int32 ProgramNumber = ((CCommandP)Command).ProgramNumber;
                    Int32 ProgramIndex;
                    if (FindCommandIndex(ProgramNumber, out ProgramIndex))
                    {
                      commandparameter.CommandIndex = ProgramIndex;
                    }
                  }
                }
              }
              else
              {
                Busy = false;
              }
      }
      return Result;
    }
  }

}