﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandG42 : CCommand
  {
    public CCommandG42(CNotifier notifier)//,
      //DGetSystemData getsystemdata,
      //DSetSystemData setsystemdata)
      : base(notifier)//, getsystemdata, setsystemdata)
    {
      FNotifier = notifier;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      commandparameter.ToolRadiusCompensation = EToolRadiusCompensation.Right;
      FNotifier.Write(String.Format("G42: Tool Radius Compensation: Right"));
      return true;
    }

  }
}

