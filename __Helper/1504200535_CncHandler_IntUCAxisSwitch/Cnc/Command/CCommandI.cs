﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandI : CCommand
  {
    private Double FPosition;

    public CCommandI(CNotifier notifier,
                     Double position)
      : base(notifier)
    {
      FNotifier = notifier;
      FPosition = position;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      /*RSystemData SP;
      FGetSystemData(out SP);
      FNotifier.Write(String.Format("I: Define Arc Center X: {0} {1}",
                                    String.Format(SP.FormatPosition, FPosition), 
                                    commandparameter.MeasurementUnit));*/
      return true;
    }

  }
}
