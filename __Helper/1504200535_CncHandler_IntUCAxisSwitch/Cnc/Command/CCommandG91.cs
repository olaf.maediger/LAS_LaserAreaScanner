﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandG91 : CCommand
  {
    private Double FParameter;

    public CCommandG91(CNotifier notifier,
                       Double parameter)
      : base(notifier)
    {
      FNotifier = notifier;
      FParameter = parameter;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("G91: Incremental Programming: {0:0.0}", FParameter));
      return true;
    }

  }
}
