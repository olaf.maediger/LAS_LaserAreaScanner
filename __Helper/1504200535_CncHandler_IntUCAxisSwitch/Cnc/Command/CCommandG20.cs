﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandG20 : CCommand
  {
    public CCommandG20(CNotifier notifier)//,
      //DGetSystemData getsystemdata,
      //DSetSystemData setsystemdata)
      : base(notifier)//, getsystemdata, setsystemdata)
    {
      FNotifier = notifier;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      commandparameter.MeasurementUnit = EMeasurementUnit.Inch;
      FNotifier.Write(String.Format("G20: Programming in Inches"));
      return true;
    }

  }
}

