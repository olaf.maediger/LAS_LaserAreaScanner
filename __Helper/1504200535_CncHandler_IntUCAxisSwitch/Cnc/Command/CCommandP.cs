﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandP : CCommand
  {
    private String FParameter;

    public CCommandP(CNotifier notifier,
                     String parameter)
      : base(notifier)
    {
      FNotifier = notifier;
      FParameter = parameter;
    }

    public CCommandP(CNotifier notifier, 
                     Int32 parameter)
      : base(notifier)
    {
      FNotifier = notifier;
      FParameter = parameter.ToString();
    }

    public Int32 ProgramNumber
    {
      get
      {
        try
        {
          Int32 PN = Int32.Parse(FParameter);
          return PN;
        }
        catch (Exception)
        {
          return 0;
        }
      }
    }

    public String Parameter
    {
      get { return FParameter; }
    }


    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      commandparameter.IParameter = ProgramNumber;
      commandparameter.SParameter = FParameter;
      FNotifier.Write(String.Format("P: Definition Parameter: {0}", FParameter));
      return true;
    }

  }
}
