﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandY : CCommand
  {
    private Double FPosition;

    public CCommandY(CNotifier notifier,
                     Double position)    
      : base(notifier)
    {
      FNotifier = notifier;
      FPosition = position;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      RAxisData AD = systemparameter.AxisDataY;
      AD.PositionData.Target = FPosition;
      String S0 = String.Format(AD.PositionData.Format, AD.PositionData.Actual);
      String S1 = String.Format(AD.PositionData.Format, AD.PositionData.Target);
      FNotifier.Write(String.Format("Y: Move Y-Axis from {0} to {1} {2}",
                                    S0, S1, commandparameter.MeasurementUnit));
      systemparameter.AxisDataY = AD;
      return true;
    }

  }
}
