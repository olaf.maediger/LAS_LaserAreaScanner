﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandM08 : CCommand
  {
    public CCommandM08(CNotifier notifier)
      : base(notifier)
    {
      FNotifier = notifier;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("M08: Coolant On (Flood)"));
      return true;
    }

  }
}

