﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandM00 : CCommand
  {
    public CCommandM00(CNotifier notifier)
      : base(notifier)
    {
      FNotifier = notifier;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("M00: Compulsory Stop"));
      return true;
    }

  }
}

