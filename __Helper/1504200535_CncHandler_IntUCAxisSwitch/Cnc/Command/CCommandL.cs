﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandL : CCommand
  {
    private Int32 FParameter;

    public CCommandL(CNotifier notifier,
                     Int32 parameter)
      : base(notifier)                     
    {
      FNotifier = notifier;
      FParameter = parameter;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      commandparameter.LoopCount = FParameter;
      FNotifier.Write(String.Format("L: Fixed Cycle Loop Count: {0}", commandparameter.LoopCount));
      return true;
    }

  }
}
