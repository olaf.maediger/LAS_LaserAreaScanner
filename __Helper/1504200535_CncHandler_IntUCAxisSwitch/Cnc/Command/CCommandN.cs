﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandN : CCommand
  {
    private Int32 FLineNumber;

    public CCommandN(CNotifier notifier,
                     Int32 linenumber)
      : base(notifier)
    {
      FNotifier = notifier;
      FLineNumber = linenumber;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("N: LineNumber: {0}",
                                    String.Format(commandparameter.FormatLineNumber, FLineNumber)));
      return true;
    }

  }
}
