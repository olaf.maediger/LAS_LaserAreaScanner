﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandO : CCommand
  {
    private Int32 FProgramNumber;

    public CCommandO(CNotifier notifier,
                     Int32 programnumber)
      : base(notifier)
    {
      FNotifier = notifier;
      FProgramNumber = programnumber;
    }

    public Int32 ProgramNumber
    {
      get { return FProgramNumber; }
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("O: ProgramNumber: {0}", FProgramNumber));
      return true;
    }

  }
}
