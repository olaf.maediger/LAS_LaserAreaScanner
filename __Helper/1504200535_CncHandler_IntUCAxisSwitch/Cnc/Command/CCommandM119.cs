﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Hardware;
//
namespace Command
{
  public class CCommandM119 : CCommand
  {
    public CCommandM119(CNotifier notifier)
      : base(notifier)
    {
      FNotifier = notifier;
    }

    public override Boolean Execute(ref RCommandParameter commandparameter,
                                    ref CSystemParameter systemparameter)
    {
      FNotifier.Write(String.Format("M119: Read Status of Limit Switches"));
      if (commandparameter.OnResponse is DOnResponse)
      {
        /*RSystemData SP;
        FGetSystemData(out SP);
        String SXL = String.Format("x_min:{0}", RLimitSwitch.SwitchStateText(SP.LimitSwitchX.Low));
        String SXR = String.Format("x_ref:{0}", RLimitSwitch.SwitchStateText(SP.LimitSwitchX.Reference));
        String SXH = String.Format("x_max:{0}", RLimitSwitch.SwitchStateText(SP.LimitSwitchX.High));
        String SYL = String.Format("y_min:{0}", RLimitSwitch.SwitchStateText(SP.LimitSwitchY.Low));
        String SYR = String.Format("y_ref:{0}", RLimitSwitch.SwitchStateText(SP.LimitSwitchY.Reference));
        String SYH = String.Format("y_max:{0}", RLimitSwitch.SwitchStateText(SP.LimitSwitchY.High));
        String SZL = String.Format("z_min:{0}", RLimitSwitch.SwitchStateText(SP.LimitSwitchZ.Low));
        String SZR = String.Format("z_ref:{0}", RLimitSwitch.SwitchStateText(SP.LimitSwitchZ.Reference));
        String SZH = String.Format("z_max:{0}", RLimitSwitch.SwitchStateText(SP.LimitSwitchZ.High));
        String Response = SXL + " " + SXR + " " + SXH + " " + SYL + " " + SYR + " " + SYH + " " + SZL + " " + SZR + " " + SZH;
        commandparameter.OnResponse(Response);
         */
      }
      if (commandparameter.OnLimitSwitchState is DOnLimitSwitchState)
      {
        commandparameter.OnLimitSwitchState(commandparameter);
      }
      return true;
    }

  }
}



