﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace Hardware
{
  public delegate void DGetSystemParameter(out CSystemParameter data);
  public delegate void DSetSystemParameter(ref CSystemParameter data);

  public class CSystemParameter
  { //
    //-------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------
    //
    public const Int32 COUNT_AXES = 3;
    //
    public const Int32 INDEX_X = 0;
    public const Int32 INDEX_Y = 1;
    public const Int32 INDEX_Z = 2;
    //
    //-------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CAxisList FAxes;
    //
    //-------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------
    //
    public CSystemParameter(CNotifier notifier)
    {
      FNotifier = notifier;
      FAxes = new CAxisList();
    }
    //
    //-------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------
    //
    private RAxisData GetAxisData(Int32 index)
    {
      if ((INDEX_X == index) && (0 < FAxes.Count))
      {
        return FAxes[INDEX_X].GetData();
      }
      if ((INDEX_Y == index) && (1 < FAxes.Count))
      {
        return FAxes[INDEX_Y].GetData();
      }
      if ((INDEX_Z == index) && (2 < FAxes.Count))
      {
        return FAxes[INDEX_Z].GetData();
      }
      return new RAxisData(0);
    }
    private void SetAxisData(Int32 index, RAxisData axisdata)
    {
      if ((INDEX_X == index) && (0 < FAxes.Count))
      {
        FAxes[INDEX_X].SetData(axisdata);
      }
      if ((INDEX_Y == index) && (1 < FAxes.Count))
      {
        FAxes[INDEX_Y].SetData(axisdata);
      }
      if ((INDEX_Z == index) && (2 < FAxes.Count))
      {
        FAxes[INDEX_Z].SetData(axisdata);
      }
    }

    public RAxisData AxisDataX
    {
      get { return GetAxisData(INDEX_X); }
      set { SetAxisData(INDEX_X, value); }
    }
    public RAxisData AxisDataY
    {
      get { return GetAxisData(INDEX_Y); }
      set { SetAxisData(INDEX_Y, value); }
    }
    public RAxisData AxisDataZ
    {
      get { return GetAxisData(INDEX_Z); }
      set { SetAxisData(INDEX_Z, value); }
    }

    public Boolean AllAxesIdle()
    {
      Boolean Result = true;
      foreach (CAxis Axis in FAxes)
      {
        Result &= (EAxisState.Idle == Axis.State);
      }
      return Result;
    }

    public CAxisList Axes
    {
      get { return FAxes; }
    }
    //
    //-------------------------------------------------------------
    //  Segment - Helper
    //-------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------
    //  Segment - Public 
    //-------------------------------------------------------------
    //
    public Boolean Abort()
    {
      Boolean Result = true;
      foreach (CAxis Axis in FAxes)
      {
        Result &= Axis.Abort();
      }
      return Result;
    }

  }

}