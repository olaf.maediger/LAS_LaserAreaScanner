﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using Task;
using UCNotifier;
using Hardware;
//
namespace Hardware
{
  //
  //---------------------------------------------------------------------
  //  Segment - 
  //---------------------------------------------------------------------
  //
  public class CAxisList : List<CAxis>
  {
    public CAxis FindAxis(EAxis axis)
    {      
      foreach (CAxis Axis in this)
      {
        if (axis == Axis.Axis)
        {
          return Axis;
        }
      }
      return null;
    }
  }

  //
  //---------------------------------------------------------------------
  //  Segment - 
  //---------------------------------------------------------------------
  //
  public class CAxis
  {
    //
    //---------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private RAxisData FData;
    private CLimitSwitchList FLimitSwitches;
    private CTask FTask;
    private Double FRampLength;
    //
    //---------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------
    //
    public CAxis(CNotifier notifier, EAxis axis, EAxisKind kind, Int32 timeresolution)
    {
      FNotifier = notifier;
      FData = new RAxisData(0);
      FData.Axis = axis;
      FData.Kind = kind;
      FData.State = EAxisState.Error;
      FLimitSwitches = new CLimitSwitchList();
      FLimitSwitches.Add(new CLimitSwitch(axis, ESwitchKind.Low, ESwitchState.Low,
                                          RAxisData.POSITION_LOW, RAxisData.POSITION_ERROR));
      FLimitSwitches.Add(new CLimitSwitch(axis, ESwitchKind.Reference, ESwitchState.Low,
                                          RAxisData.POSITION_REFERENCE, RAxisData.POSITION_ERROR));
      FLimitSwitches.Add(new CLimitSwitch(axis, ESwitchKind.High, ESwitchState.Low,
                                          RAxisData.POSITION_HIGH, RAxisData.POSITION_ERROR));
      FData.TimeResolution = timeresolution;
      FTask = new CTask(GetName(),
                        OnExecutionStart,
                        OnExecutionBusy,
                        OnExecutionEnd,
                        OnExecutionAbort);
      FTask.Start(); // !!!!
    }
    //
    //---------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------
    //
    public RAxisData GetData()
    {
      return FData;
    }
    public void SetData(RAxisData value)
    {
      FData = value;
    }

    public String GetName()
    {
      return "Axis" + RAxisData.AxisText(FData.Axis);
    }

    private void SetAxisState(EAxisState value)
    {
      if (value != FData.State)
      {
        String Line = String.Format("~~~ [{0}]: State[{1}] -> [{2}]", GetName(), FData.State, value);
        FData.State = value;
        FNotifier.Write(Line);
      }
    }
    public EAxisState State
    {
      get { return FData.State; }
      set { SetAxisState(value); }
    }

    public EAxis Axis
    {
      get { return FData.Axis; }
    }
    //
    //-------------------------------------------------------------
    //  Segment - Helper
    //-------------------------------------------------------------
    //
    private Boolean IsLimitSwitchLowActive()
    {
      // NC RAxisData AD = FGetAxisData();
      return (FData.PositionData.Actual <= FLimitSwitches[RAxisData.INDEX_LSL].Position -
                                           FLimitSwitches[RAxisData.INDEX_LSL].Error);
    }

    private Boolean IsLimitSwitchReferenceActive()
    {
      // NC RAxisData AD = FGetAxisData();
      RLimitSwitchData LSD = FLimitSwitches[RAxisData.INDEX_LSR].GetData();
      Boolean LL = ((FLimitSwitches[RAxisData.INDEX_LSR].Position -
                     FLimitSwitches[RAxisData.INDEX_LSR].Error) <= FData.PositionData.Actual);
      Boolean LH = ((FData.PositionData.Actual <= FLimitSwitches[RAxisData.INDEX_LSR].Position + 
                     FLimitSwitches[RAxisData.INDEX_LSR].Error));
      return (LL || LH);              
    }

    private Boolean IsLimitSwitchHighActive()
    {
      // NC RAxisData AD = FGetAxisData();
      return (FLimitSwitches[RAxisData.INDEX_LSH].Position <= FData.PositionData.Actual);
    }

    private void UpdateStateFromLimitSwitches(EAxisState axisstate)
    {
      if (IsLimitSwitchLowActive())
      {
        State = EAxisState.LimitLow;
      }
      else
        if (IsLimitSwitchHighActive())
        {
          State = EAxisState.LimitHigh;
        }
        else
        { // no limitswitches active
          State = axisstate;
        }
    }
    //
    //-------------------------------------------------------------
    //  Segment - State Execution
    //-------------------------------------------------------------
    //
    private void OnError()
    {
      FNotifier.Write(String.Format("~~~ [{0}]: OnError...", GetName()));
      State = EAxisState.Error;
      // only new start !!!
      while (true)
      {
        Thread.Sleep(FData.TimeResolution);
      }
    }
    private void OnError(Int32 errorcode, String errortext)
    {
      String Header = String.Format("~~~ [{0}]", GetName());
      FNotifier.Error(Header, errorcode, errortext);
      OnError();
    }

    private void OnInit()
    {
      FNotifier.Write(String.Format("~~~ [{0}]: OnInit...", GetName()));
      UpdateStateFromLimitSwitches(EAxisState.Idle);
    }

    private void OnIdle()
    {
      // debug String PA = String.Format(FData.PositionData.Format, FData.PositionData.Actual);
      // debug String PT = String.Format(FData.PositionData.Format, FData.PositionData.Target);
      // debug FNotifier.Write(String.Format("!!!OI -> PAX:{0} PTX:{1}", PA, PT));
      FData.VelocityData.Actual = 0.0;
      FData.AccelerationData.Actual = 0.0;
      if (FData.PositionData.Actual < FData.PositionData.Target - FData.PositionData.Resolution)
      {
        FData.MoveDirection = EMoveDirection.Positive;
        FData.PositionData.Preset = FData.PositionData.Actual;
        FData.AccelerationData.Actual = FData.AccelerationData.Target;
        State = EAxisState.RampUp;
      }
      else
        if (FData.PositionData.Resolution + FData.PositionData.Target < FData.PositionData.Actual)
        {
          FData.MoveDirection = EMoveDirection.Negative;
          FData.PositionData.Preset = FData.PositionData.Actual;
          FData.AccelerationData.Actual = FData.AccelerationData.Target;
          State = EAxisState.RampUp;
        }
        else
        {
          FData.MoveDirection = EMoveDirection.Halt;
        }
    }

    private void OnRampUp()
    {
      Double PH = (FData.PositionData.Target + FData.PositionData.Preset) / 2.0;
      if (EMoveDirection.Positive == FData.MoveDirection)
      { // -> 
        if (PH <= FData.PositionData.Actual)
        {
          State = EAxisState.RampDown;
        }
        else
          if (FData.VelocityData.Target <= FData.VelocityData.Actual)
          { 
            FRampLength = FData.PositionData.Actual - FData.PositionData.Preset;
            FData.VelocityData.Actual = FData.VelocityData.Target;
            State = EAxisState.Move;
          }
          else
          {
            FData.VelocityData.Actual += FData.AccelerationData.Actual * FData.TimeResolution / 1000.0;
            FData.PositionData.Actual += FData.VelocityData.Actual * FData.TimeResolution / 1000.0;           
          }
        String PA = String.Format(FData.PositionData.Format, FData.PositionData.Actual);
        String VA = String.Format(FData.VelocityData.Format, FData.VelocityData.Actual);
        FNotifier.Write(String.Format("{0}: RU -> PA:{1} VA:{2}", GetName(), PA, VA));
      }
      else
        if (EMoveDirection.Negative == FData.MoveDirection)
        { // <-
          if (FData.PositionData.Actual <= PH)
          {
            State = EAxisState.RampDown;
          }
          else
            if (FData.VelocityData.Target <= FData.VelocityData.Actual)
            {
              FRampLength = FData.PositionData.Preset - FData.PositionData.Actual;
              FData.VelocityData.Actual = FData.VelocityData.Target;
              State = EAxisState.Move;
            }
            else
            {
              FData.VelocityData.Actual += FData.AccelerationData.Target * FData.TimeResolution / 1000.0;
              FData.PositionData.Actual -= FData.VelocityData.Actual * FData.TimeResolution / 1000.0;
            }
          String PA = String.Format(FData.PositionData.Format, FData.PositionData.Actual);
          String VA = String.Format(FData.VelocityData.Format, FData.VelocityData.Actual);
          FNotifier.Write(String.Format("{0}: RU <- PA{1} VA:{2}", GetName(), PA, VA));
        }
        else
        { // Halt == MoveDirection
          State = EAxisState.Idle;
        }         
    }

    private void OnMove()
    {
      if (EMoveDirection.Positive == FData.MoveDirection)
      {
        FData.PositionData.Actual += FData.VelocityData.Target * FData.TimeResolution / 1000.0;
        String PA = String.Format(FData.PositionData.Format, FData.PositionData.Actual);
        String VA = String.Format(FData.VelocityData.Format, FData.VelocityData.Actual);
        FNotifier.Write(String.Format("{0}: M -> PA{1} VA:{2}", GetName(), PA, VA));
        if (FData.PositionData.Target - FRampLength <= FData.PositionData.Actual)
        {
          State = EAxisState.RampDown;
        }
      }
      else
        if (EMoveDirection.Negative == FData.MoveDirection)
        {
          FData.PositionData.Actual -= FData.VelocityData.Target * FData.TimeResolution / 1000.0;
          String PA = String.Format(FData.PositionData.Format, FData.PositionData.Actual);
          String VA = String.Format(FData.VelocityData.Format, FData.VelocityData.Actual);
          FNotifier.Write(String.Format("{0}: M <- PA{1}:{1} VA:{2}", GetName(), PA, VA));
          if (FData.PositionData.Actual - FRampLength <= FData.PositionData.Target)
          {
            State = EAxisState.RampDown;
          }
        }
        else
        {
          State = EAxisState.Idle;
        }
    }

    private void OnRampDown()
    {
      if (EMoveDirection.Positive == FData.MoveDirection)
      { // -> 
        FData.VelocityData.Actual -= FData.AccelerationData.Actual * FData.TimeResolution / 1000.0;
        FData.VelocityData.Actual = Math.Max(FData.VelocityData.Leave, FData.VelocityData.Actual);
        FData.PositionData.Actual += FData.VelocityData.Actual * FData.TimeResolution / 1000.0;           
        String PA = String.Format(FData.PositionData.Format, FData.PositionData.Actual);
        String VA = String.Format(FData.VelocityData.Format, FData.VelocityData.Actual);
        FNotifier.Write(String.Format("{0}: RD -> PA:{1} VA:{2}", GetName(), PA, VA));
        if (FData.PositionData.Target <= FData.PositionData.Actual + FData.PositionData.Resolution)
        {
          FData.PositionData.Target = FData.PositionData.Actual;
          FData.VelocityData.Actual = 0.0;
          State = EAxisState.Idle;
        }
      } 
      else
        if (EMoveDirection.Negative == FData.MoveDirection)
        { // <-
          FData.VelocityData.Actual -= FData.AccelerationData.Actual * FData.TimeResolution / 1000.0;
          FData.VelocityData.Actual = Math.Max(FData.VelocityData.Leave, FData.VelocityData.Actual);
          FData.PositionData.Actual -= FData.VelocityData.Actual * FData.TimeResolution / 1000.0;
          String PA = String.Format(FData.PositionData.Format, FData.PositionData.Actual);
          String VA = String.Format(FData.VelocityData.Format, FData.VelocityData.Actual);
          FNotifier.Write(String.Format("{0}: RD <- PA:{1} VA:{2}", GetName(), PA, VA));
          if (FData.PositionData.Actual <= FData.PositionData.Target + FData.PositionData.Resolution)
          {
            FData.PositionData.Target = FData.PositionData.Actual;
            FData.VelocityData.Actual = 0.0;
            State = EAxisState.Idle;
          }
        }           
    }

    private void OnLimitLow()
    {
    }

    private void OnLimitHigh()
    {
    }

    private void OnBlocked()
    {
      FNotifier.Write(String.Format("~~~ [{0}]: OnBlocked...", GetName()));
      State = EAxisState.Blocked;
      // only new start !!!
      while (true)
      {
        Thread.Sleep(FData.TimeResolution);
      }
    }
    //
    //----------------------------------------------------------
    //  Section - Task - Callback
    //----------------------------------------------------------
    //
    private void OnExecutionStart(ref RTaskData data)
    {
      FNotifier.Write(String.Format("~~~ [{0}]: OnExcutionStart", GetName()));
      State = EAxisState.Init;
    }

    private void OnExecutionBusy(ref RTaskData data)
    {
      FNotifier.Write(String.Format("~~~ [{0}]: OnExcutionBusy", GetName()));      
      Boolean Busy = true;
      while (Busy)
      {
        if (!data.Halt)
        {
          switch (State)
          {
            case EAxisState.Init:
              OnInit();
              break;
            case EAxisState.LimitLow:
              OnLimitLow();
              break;
            case EAxisState.LimitHigh:
              OnLimitHigh();
              break;
            case EAxisState.RampUp:
              OnRampUp();
              break;
            case EAxisState.Idle:
              OnIdle();
              break;
            case EAxisState.Move:
              OnMove();
              break;
            case EAxisState.RampDown:
              OnRampDown();
              break;
            case EAxisState.Blocked:
              OnBlocked();
              break;
            case EAxisState.Error:
              OnError();
              break;
            default:
              OnError(2, "Invalid Axis State");
              break;
          }
        }
        Thread.Sleep(FData.TimeResolution);
      }
    }

    private void OnExecutionEnd(ref RTaskData data)
    {
      FNotifier.Write(String.Format("~~~ [{0}]: OnExcutionEnd", GetName()));
      // Hangsup!!! OnBlocked();
    }

    private void OnExecutionAbort(ref RTaskData data)
    {
      FNotifier.Write(String.Format("~~~ [{0}]: OnExcutionAbort", GetName()));
      // Hangsup!!! OnBlocked();
    }
    //
    //----------------------------------------------------------
    //  Section - Task - Public
    //----------------------------------------------------------
    //
    public Boolean Start()
    {
      return FTask.Start();
    }

    public Boolean Halt()
    {
      return FTask.Halt();
    }

    public Boolean Resume()
    {
      return FTask.Resume();
    }

    public Boolean Abort()
    {
      return FTask.Abort();
    }


  }
}
