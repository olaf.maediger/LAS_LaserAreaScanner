﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace Hardware
{

  //
  //---------------------------------------------------------------------
  //  Segment - 
  //---------------------------------------------------------------------
  //
  public class CLimitSwitchList : List<CLimitSwitch>
  {
  }

  //
  //---------------------------------------------------------------------
  //  Segment - 
  //---------------------------------------------------------------------
  //
  public class CLimitSwitch
  {
    //
    //---------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------
    //
    private EAxis FAxis; 
    private RLimitSwitchData FData;
    //
    //---------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------
    //
    public CLimitSwitch(EAxis axis,
                        ESwitchKind switchkind, 
                        ESwitchState switchstate,
                        Double position,
                        Double error)
    {
      FAxis = axis;
      FData = new RLimitSwitchData(0);
      FData.SwitchKind = switchkind;      
      FData.SwitchState = switchstate;
      FData.Position = position;
      FData.Error = error;
    }
    //
    //---------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------
    //
    public RLimitSwitchData GetData()
    {
      return FData;
    }
    public void SetData(RLimitSwitchData value)
    {
      FData = value;
    }

    public String GetName()
    {
      return "LimitSwitch" + RLimitSwitchData.SwitchKindText(FData.SwitchKind);
    }

    public Double Position
    {
      get { return FData.Position; }
      set { FData.Position = value; }
    }

    public Double Error
    {
      get { return FData.Error; }
      set { FData.Error = value; }
    }
    
  }
}
