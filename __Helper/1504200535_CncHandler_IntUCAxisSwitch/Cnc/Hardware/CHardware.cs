﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace Hardware
{ //
  //--------------------------------------------------------
  //  Segment - Definition - Switch
  //--------------------------------------------------------
  //
  public enum ESwitchKind
  {
    Low = 0,
    Reference = 1,
    High = 2
  }

  public enum ESwitchState
  {
    Low = 0,
    High = 1
  }
  //
  //--------------------------------------------------------
  //  Segment - Definition - Axis
  //--------------------------------------------------------
  //
  public enum EAxis
  {
    X,
    Y,
    Z,
    W
  }

  public enum EAxisKind
  {
    Translation,  // "Translationstrieb"
    Rotation,     // "Rotationstrieb"
    Geneva        // "Malteserkreuzgetriebe"
  }

  public enum EAxisState
  {
    Error,
    Init,
    Idle,
    LimitLow,
    LimitHigh,
    RampUp,
    Move,
    RampDown,
    Blocked
  }

  public enum EMoveDirection
  {
    Halt,
    Positive,
    Negative
  }
  //
  //--------------------------------------------------------
  //  Segment - Definition - LimitSwitch
  //--------------------------------------------------------
  //
  public delegate RLimitSwitchData DGetLimitSwitchData();
  public delegate void DSetLimitSwitchData(RLimitSwitchData data);

  public struct RLimitSwitchData
  {
    private const Double INIT_POSITION = 0.0;
    private const Double INIT_ERROR = 0.010;  // 10um
    //
    public ESwitchKind SwitchKind;
    public ESwitchState SwitchState;
    public Double Position;
    public Double Error;
    //
    public RLimitSwitchData(Int32 init)
    {
      SwitchKind = ESwitchKind.Low;
      SwitchState = ESwitchState.Low;
      Position = INIT_POSITION;
      Error = INIT_ERROR;
    }
    // Helper
    public static String SwitchStateText(ESwitchState value)
    {
      switch (value)
      {
        case ESwitchState.High:
          return "H";
        default: //ESwitchKind.L
          return "L";
      }
    }

    public static String SwitchKindText(ESwitchKind value)
    {
      switch (value)
      {
        case ESwitchKind.Reference:
          return "R";
        case ESwitchKind.High:
          return "H";
        default: //ESwitchKind.Low
          return "L";
      }
    }
  }
  //
  //--------------------------------------------------------
  //  Segment - Definition - Position
  //--------------------------------------------------------
  //
  public struct RPositionData
  {
    public Double Actual;
    public Double Preset;
    public Double Target;
    public Double Resolution;
    public Double Error;
    public String Format;
    //
    public RPositionData(Int32 init)
    { // [m]
      Actual = 0.0;
      Preset = 0.0;
      Target = 0.0;
      Resolution = 0.001;  // mm
      Error =      0.001;  // mm
      Format = "{0:+000.000;-000.000;+000.000}";
    }
  }
  //
  //--------------------------------------------------------
  //  Segment - Definition - Velocity
  //--------------------------------------------------------
  //
  public struct RVelocityData
  {
    public Double Actual;
    public Double Preset;
    public Double Target;
    public Double Resolution;
    public Double Error;
    public Double Leave;
    public String Format;
    //
    public RVelocityData(Int32 init)
    { // [m/s]
      Actual = 0.0;
      Preset = 0.0;
      Target = 10.00000;  // mm/s
      Resolution = 0.100100;  // mm/s
      Error =      0.100100;  // mm/s
      Leave = 0.5000; // 
      Format = "{0:+000.000;-000.000;+000.000}";
    }
  }
  //
  //--------------------------------------------------------
  //  Segment - Definition - Acceleration
  //--------------------------------------------------------
  //
  public struct RAccelerationData
  {
    public Double Actual;
    public Double Preset;
    public Double Target;
    public Double Resolution;
    public Double Error;
    public String Format;
    //
    public RAccelerationData(Int32 init)
    { // [m/s2]
      Actual = 0.0;
      Preset = 0.0;
      Target = 1.01000;  // 1mm/s2
      Resolution = 0.101000;  // 1mm/s2
      Error =      0.101000;  // 1mm/s2
      Format = "{0:+000.000;-000.000;+000.000}";
    }
  }
  //
  //--------------------------------------------------------
  //  Segment - Definition - AxisParameter
  //--------------------------------------------------------
  //
  public delegate RAxisData DGetAxisData();
  public delegate void DSetAxisData(RAxisData axis);

  public struct RAxisData
  {
    public const Int32 POSITION_MINIMUM = 0;//-2147483647;
    public const Int32 POSITION_MAXIMUM = 1000;//+2147483647;
    public const Int32 POSITION_ZERO = 0;
    public const Int32 POSITION_LOW = 0;//-1000000;
    public const Int32 POSITION_REFERENCE = 100;//0;
    public const Int32 POSITION_HIGH = 200;//1000000;
    public const Int32 POSITION_ERROR = 5;//1000;
    //
    private const Int32 COUNT_LIMITSWITCH = 3;
    //
    public const Int32 INDEX_LSL = 0;
    public const Int32 INDEX_LSR = 1;
    public const Int32 INDEX_LSH = 2;
    //
    private const Double INIT_LSL_POSITION = POSITION_LOW + 20;
    private const Double INIT_LSR_POSITION = POSITION_LOW + 100;
    private const Double INIT_LSH_POSITION = POSITION_HIGH - 20;
    private const Int32 INIT_TIMERESOLUTION = 1000; // [ms]
    //
    public EAxis Axis;
    public EAxisKind Kind;
    public EAxisState State;
    public Int32 TimeResolution;
    public EMoveDirection MoveDirection;
    public RPositionData PositionData;
    public RVelocityData VelocityData;
    public RAccelerationData AccelerationData;
    public RLimitSwitchData[] LimitSwitchData;
    //
    public RAxisData(Int32 init)
    {
      Axis = EAxis.X;
      Kind = EAxisKind.Translation;
      State = EAxisState.Error;
      TimeResolution = INIT_TIMERESOLUTION;
      //
      MoveDirection = EMoveDirection.Halt;
      PositionData = new RPositionData(0);
      VelocityData = new RVelocityData(0);
      AccelerationData = new RAccelerationData(0);
      //
      LimitSwitchData = new RLimitSwitchData[COUNT_LIMITSWITCH];
      //
      LimitSwitchData[INDEX_LSL] = new RLimitSwitchData(0);
      LimitSwitchData[INDEX_LSL].SwitchKind = ESwitchKind.Low;
      LimitSwitchData[INDEX_LSL].SwitchState = ESwitchState.Low;
      LimitSwitchData[INDEX_LSL].Position = INIT_LSL_POSITION;
      //
      LimitSwitchData[INDEX_LSR] = new RLimitSwitchData(0);
      LimitSwitchData[INDEX_LSR].SwitchKind = ESwitchKind.Reference;
      LimitSwitchData[INDEX_LSR].SwitchState = ESwitchState.Low;
      LimitSwitchData[INDEX_LSR].Position = INIT_LSR_POSITION;
      //
      LimitSwitchData[INDEX_LSH] = new RLimitSwitchData(0);
      LimitSwitchData[INDEX_LSH].SwitchKind = ESwitchKind.High;
      LimitSwitchData[INDEX_LSH].SwitchState = ESwitchState.Low;
      LimitSwitchData[INDEX_LSH].Position = INIT_LSH_POSITION;
    }
    // Helper
    public static String AxisText(EAxis value)
    {
      switch (value)
      {
        case EAxis.Y:
          return "Y";
        case EAxis.Z:
          return "Z";
        default: //ESwitchAxis.X
          return "X";
      }
    }

    public static String AxisKindText(EAxisKind value)
    {
      switch (value)
      {
        case EAxisKind.Geneva:
          return "Geneva";
        case EAxisKind.Rotation:
          return "Rotation";
        default: // Translation
          return "Translation";
      }
    }
  }

}
