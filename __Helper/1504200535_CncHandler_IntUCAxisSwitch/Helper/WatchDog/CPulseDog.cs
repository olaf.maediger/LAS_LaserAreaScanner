﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
//
namespace WatchDog
{ //
  //--------------------------------------------------------------
  //  Segment - Type
  //--------------------------------------------------------------
  //
  public delegate void DOnPulseEvent(); 
  //
  //################################################################
  //  Segment - CPulseDog
  //################################################################
  //
  public class CPulseDog : CWatchBase
  { //
    //--------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------
    //
    private Boolean FRetriggered;
    private DOnPulseEvent FOnPulseEvent;
    //
    //--------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------
    //
    public CPulseDog(Int32 refreshtime,
                     DOnPulseEvent onpulseevent)
      : base(refreshtime)
    {
      FOnPulseEvent = onpulseevent;
      FRetriggered = false;
    }
    //
    //--------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------
    //
    protected override void OnTimerTick(object sender, EventArgs arguments)
    {
      if (!FRetriggered)
      {
        Stop();
      }
      FRetriggered = false;
      //
      if (FOnPulseEvent is DOnPulseEvent)
      {
        FOnPulseEvent();
      }
    }
    //
    //--------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------
    //  Segment - Public Management
    //--------------------------------------------------------------
    //
    public void Trigger()
    {
      FRetriggered = true;
      Start();
    }

    public void Halt()
    {
      Stop();
    }
  }
}
