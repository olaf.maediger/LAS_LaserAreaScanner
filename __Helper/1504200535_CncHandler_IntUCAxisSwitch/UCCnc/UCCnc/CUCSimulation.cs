﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCCnc
{
  public delegate void DOnSimulationHalted();
  public delegate void DOnSimulationResumed();
  public delegate void DOnTimeResolutionChanged(Int32 timeresolution);

  public partial class CUCSimulation : UserControl
  {
    private const String TEXT_HALT = "Halt";
    private const String TEXT_RESUME = "Resume";

    private DOnSimulationHalted FOnSimulationHalted;
    private DOnSimulationResumed FOnSimulationResumed;
    private DOnTimeResolutionChanged FOnTimeResolutionChanged;

    public CUCSimulation()
    {
      InitializeComponent();
      //
      cbxHaltResume.Checked = true;
      cbxHaltResume.Checked = false;
    }


    public void SetOnSimulationHalted(DOnSimulationHalted value)
    {
      FOnSimulationHalted = value;
    }
    public void SetOnSimulationResumed(DOnSimulationResumed value)
    {
      FOnSimulationResumed = value;
    }
    public void SetOnTimeResolutionChanged(DOnTimeResolutionChanged value)
    {
      FOnTimeResolutionChanged = value;
    }

    private Int32 GetTimeResolution()
    {
      return (Int32)nudTimeResolution.Value;
    }
    private void SetTimeResolution(Int32 value)
    {
      nudTimeResolution.Value = Math.Max(1, Math.Min(1000, value));
    }
    public Int32 TimeResolution
    {
      get { return GetTimeResolution(); }
      set { SetTimeResolution(value); }
    }


    private void cbxHaltResume_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxHaltResume.Checked)
      {
        if (FOnSimulationHalted is DOnSimulationHalted)
        {
          FOnSimulationHalted();
        }
        cbxHaltResume.Text = TEXT_RESUME;
      }
      else
      {
        if (FOnSimulationResumed is DOnSimulationResumed)
        {
          FOnSimulationResumed();
        }
        cbxHaltResume.Text = TEXT_HALT;
      }
    }

    private void nudTimeResolution_ValueChanged(object sender, EventArgs e)
    {
      if (FOnTimeResolutionChanged is DOnTimeResolutionChanged)
      {
        FOnTimeResolutionChanged((Int32)nudTimeResolution.Value);
      }
    }

  }
}
