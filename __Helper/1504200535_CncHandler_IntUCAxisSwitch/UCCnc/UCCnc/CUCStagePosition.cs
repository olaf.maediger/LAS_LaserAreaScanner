﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCCnc
{
  public partial class CUCStagePosition : UserControl
  {
    private Int32 FPosition;

    public CUCStagePosition()
    {
      InitializeComponent();
      FPosition = 100; 
    }

    private void SetPosition(Int32 value)
    {
      if (value != FPosition)
      {
        FPosition = value;
        Invalidate();
      }
    }
    public Int32 Position
    {
      get { return FPosition; }
      set { SetPosition(value); }
    }

    private void CUCStagePosition_Paint(object sender, PaintEventArgs e)
    {
      Graphics G = e.Graphics;
      Int32 W = ClientRectangle.Width;
      Int32 WH = W / 2;
      Int32 H = ClientRectangle.Height;
      Int32 HH = H / 2;
      Int32 O = 6;
      Int32 OD = 2 * O;
      Color CF = Color.FromArgb(0xFF, 0xCC, 0xAA, 0xAA);
      Brush BF = new SolidBrush(CF);
      G.FillRectangle(BF, 0, 0, W, H);
      //
      Int32 WS = OD;
      Color CS = Color.FromArgb(0xFF, 0x55, 0x44, 0x66);
      Brush BS = new SolidBrush(CS);
      G.FillRectangle(BS, FPosition - O, O, OD, H - OD);
      //
      BF.Dispose();
      BS.Dispose();
    }

  }
}
