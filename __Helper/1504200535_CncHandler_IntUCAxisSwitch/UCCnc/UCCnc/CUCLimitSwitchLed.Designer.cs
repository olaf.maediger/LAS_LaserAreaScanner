﻿namespace UCCnc
{
  partial class CUCLimitSwitchLed
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.pnlMain = new System.Windows.Forms.Panel();
      this.lblState = new System.Windows.Forms.Label();
      this.pnlMain.SuspendLayout();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(97, 13);
      this.lblHeader.TabIndex = 0;
      this.lblHeader.Text = "<header>";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnlMain
      // 
      this.pnlMain.BackColor = System.Drawing.SystemColors.Info;
      this.pnlMain.Controls.Add(this.lblState);
      this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlMain.Location = new System.Drawing.Point(0, 13);
      this.pnlMain.Name = "pnlMain";
      this.pnlMain.Size = new System.Drawing.Size(97, 74);
      this.pnlMain.TabIndex = 1;
      this.pnlMain.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMain_Paint);
      // 
      // lblState
      // 
      this.lblState.BackColor = System.Drawing.Color.Transparent;
      this.lblState.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblState.Location = new System.Drawing.Point(0, 0);
      this.lblState.Name = "lblState";
      this.lblState.Size = new System.Drawing.Size(97, 74);
      this.lblState.TabIndex = 1;
      this.lblState.Text = "<state>";
      this.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // UCLimitSwitch
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.Controls.Add(this.pnlMain);
      this.Controls.Add(this.lblHeader);
      this.Name = "UCLimitSwitch";
      this.Size = new System.Drawing.Size(97, 87);
      this.pnlMain.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Panel pnlMain;
    private System.Windows.Forms.Label lblState;
  }
}
