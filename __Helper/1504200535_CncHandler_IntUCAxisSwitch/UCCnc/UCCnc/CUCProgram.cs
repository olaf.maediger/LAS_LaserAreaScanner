﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCCnc
{
  public partial class CUCProgram : UserControl
  {
    public CUCProgram()
    {
      InitializeComponent();
    }

    private void btnTranslateProgram_Click(object sender, EventArgs e)
    {
      // CB FCnc.TranslateProgram(FUCTextEditor.GetText());
    }

    private void btnExecuteProgram_Click(object sender, EventArgs e)
    {
      // CB FCnc.SetOnExecuteCommand(CncOnExecuteCommand);
      // CB FCnc.ExecuteProgram();
    }

    private void btnExecuteSequence_Click(object sender, EventArgs e)
    {
      Boolean Reset = cbxReset.Checked;
      Int32 CommandCount = (Int32)nudCommandCount.Value;
      // CB FCnc.ExecuteSequence(Reset, CommandCount);
    }

  }
}
