﻿namespace UCCnc
{
  partial class CUCLimitSwitches
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.FUCLimitSwitchZH = new UCCnc.CUCLimitSwitchLed();
      this.FUCLimitSwitchZR = new UCCnc.CUCLimitSwitchLed();
      this.FUCLimitSwitchZL = new UCCnc.CUCLimitSwitchLed();
      this.FUCLimitSwitchYH = new UCCnc.CUCLimitSwitchLed();
      this.FUCLimitSwitchYR = new UCCnc.CUCLimitSwitchLed();
      this.FUCLimitSwitchYL = new UCCnc.CUCLimitSwitchLed();
      this.FUCLimitSwitchXH = new UCCnc.CUCLimitSwitchLed();
      this.FUCLimitSwitchXR = new UCCnc.CUCLimitSwitchLed();
      this.FUCLimitSwitchXL = new UCCnc.CUCLimitSwitchLed();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(188)))));
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(252, 15);
      this.lblHeader.TabIndex = 0;
      this.lblHeader.Text = "LimitSwitches";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FUCLimitSwitchZH
      // 
      this.FUCLimitSwitchZH.BackColor = System.Drawing.SystemColors.Control;
      this.FUCLimitSwitchZH.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCLimitSwitchZH.Header = "ZH";
      this.FUCLimitSwitchZH.Location = new System.Drawing.Point(224, 15);
      this.FUCLimitSwitchZH.Name = "FUCLimitSwitchZH";
      this.FUCLimitSwitchZH.Position = 0D;
      this.FUCLimitSwitchZH.Resolution = 0D;
      this.FUCLimitSwitchZH.Size = new System.Drawing.Size(28, 40);
      this.FUCLimitSwitchZH.TabIndex = 17;
      // 
      // FUCLimitSwitchZR
      // 
      this.FUCLimitSwitchZR.BackColor = System.Drawing.SystemColors.Control;
      this.FUCLimitSwitchZR.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCLimitSwitchZR.Header = "ZR";
      this.FUCLimitSwitchZR.Location = new System.Drawing.Point(196, 15);
      this.FUCLimitSwitchZR.Name = "FUCLimitSwitchZR";
      this.FUCLimitSwitchZR.Position = 0D;
      this.FUCLimitSwitchZR.Resolution = 0D;
      this.FUCLimitSwitchZR.Size = new System.Drawing.Size(28, 40);
      this.FUCLimitSwitchZR.TabIndex = 16;
      // 
      // FUCLimitSwitchZL
      // 
      this.FUCLimitSwitchZL.BackColor = System.Drawing.SystemColors.Control;
      this.FUCLimitSwitchZL.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCLimitSwitchZL.Header = "ZL";
      this.FUCLimitSwitchZL.Location = new System.Drawing.Point(168, 15);
      this.FUCLimitSwitchZL.Name = "FUCLimitSwitchZL";
      this.FUCLimitSwitchZL.Position = 0D;
      this.FUCLimitSwitchZL.Resolution = 0D;
      this.FUCLimitSwitchZL.Size = new System.Drawing.Size(28, 40);
      this.FUCLimitSwitchZL.TabIndex = 15;
      // 
      // FUCLimitSwitchYH
      // 
      this.FUCLimitSwitchYH.BackColor = System.Drawing.SystemColors.Control;
      this.FUCLimitSwitchYH.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCLimitSwitchYH.Header = "YH";
      this.FUCLimitSwitchYH.Location = new System.Drawing.Point(140, 15);
      this.FUCLimitSwitchYH.Name = "FUCLimitSwitchYH";
      this.FUCLimitSwitchYH.Position = 0D;
      this.FUCLimitSwitchYH.Resolution = 0D;
      this.FUCLimitSwitchYH.Size = new System.Drawing.Size(28, 40);
      this.FUCLimitSwitchYH.TabIndex = 14;
      // 
      // FUCLimitSwitchYR
      // 
      this.FUCLimitSwitchYR.BackColor = System.Drawing.SystemColors.Control;
      this.FUCLimitSwitchYR.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCLimitSwitchYR.Header = "YR";
      this.FUCLimitSwitchYR.Location = new System.Drawing.Point(112, 15);
      this.FUCLimitSwitchYR.Name = "FUCLimitSwitchYR";
      this.FUCLimitSwitchYR.Position = 0D;
      this.FUCLimitSwitchYR.Resolution = 0D;
      this.FUCLimitSwitchYR.Size = new System.Drawing.Size(28, 40);
      this.FUCLimitSwitchYR.TabIndex = 13;
      // 
      // FUCLimitSwitchYL
      // 
      this.FUCLimitSwitchYL.BackColor = System.Drawing.SystemColors.Control;
      this.FUCLimitSwitchYL.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCLimitSwitchYL.Header = "YL";
      this.FUCLimitSwitchYL.Location = new System.Drawing.Point(84, 15);
      this.FUCLimitSwitchYL.Name = "FUCLimitSwitchYL";
      this.FUCLimitSwitchYL.Position = 0D;
      this.FUCLimitSwitchYL.Resolution = 0D;
      this.FUCLimitSwitchYL.Size = new System.Drawing.Size(28, 40);
      this.FUCLimitSwitchYL.TabIndex = 12;
      // 
      // FUCLimitSwitchXH
      // 
      this.FUCLimitSwitchXH.BackColor = System.Drawing.SystemColors.Control;
      this.FUCLimitSwitchXH.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCLimitSwitchXH.Header = "XH";
      this.FUCLimitSwitchXH.Location = new System.Drawing.Point(56, 15);
      this.FUCLimitSwitchXH.Name = "FUCLimitSwitchXH";
      this.FUCLimitSwitchXH.Position = 0D;
      this.FUCLimitSwitchXH.Resolution = 0D;
      this.FUCLimitSwitchXH.Size = new System.Drawing.Size(28, 40);
      this.FUCLimitSwitchXH.TabIndex = 11;
      // 
      // FUCLimitSwitchXR
      // 
      this.FUCLimitSwitchXR.BackColor = System.Drawing.SystemColors.Control;
      this.FUCLimitSwitchXR.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCLimitSwitchXR.Header = "XR";
      this.FUCLimitSwitchXR.Location = new System.Drawing.Point(28, 15);
      this.FUCLimitSwitchXR.Name = "FUCLimitSwitchXR";
      this.FUCLimitSwitchXR.Position = 0D;
      this.FUCLimitSwitchXR.Resolution = 0D;
      this.FUCLimitSwitchXR.Size = new System.Drawing.Size(28, 40);
      this.FUCLimitSwitchXR.TabIndex = 10;
      // 
      // FUCLimitSwitchXL
      // 
      this.FUCLimitSwitchXL.BackColor = System.Drawing.SystemColors.Control;
      this.FUCLimitSwitchXL.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCLimitSwitchXL.Header = "XL";
      this.FUCLimitSwitchXL.Location = new System.Drawing.Point(0, 15);
      this.FUCLimitSwitchXL.Name = "FUCLimitSwitchXL";
      this.FUCLimitSwitchXL.Position = 0D;
      this.FUCLimitSwitchXL.Resolution = 0D;
      this.FUCLimitSwitchXL.Size = new System.Drawing.Size(28, 40);
      this.FUCLimitSwitchXL.TabIndex = 9;
      // 
      // UCLimitSwitches
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCLimitSwitchZH);
      this.Controls.Add(this.FUCLimitSwitchZR);
      this.Controls.Add(this.FUCLimitSwitchZL);
      this.Controls.Add(this.FUCLimitSwitchYH);
      this.Controls.Add(this.FUCLimitSwitchYR);
      this.Controls.Add(this.FUCLimitSwitchYL);
      this.Controls.Add(this.FUCLimitSwitchXH);
      this.Controls.Add(this.FUCLimitSwitchXR);
      this.Controls.Add(this.FUCLimitSwitchXL);
      this.Controls.Add(this.lblHeader);
      this.Name = "UCLimitSwitches";
      this.Size = new System.Drawing.Size(252, 55);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private CUCLimitSwitchLed FUCLimitSwitchZH;
    private CUCLimitSwitchLed FUCLimitSwitchZR;
    private CUCLimitSwitchLed FUCLimitSwitchZL;
    private CUCLimitSwitchLed FUCLimitSwitchYH;
    private CUCLimitSwitchLed FUCLimitSwitchYR;
    private CUCLimitSwitchLed FUCLimitSwitchYL;
    private CUCLimitSwitchLed FUCLimitSwitchXH;
    private CUCLimitSwitchLed FUCLimitSwitchXR;
    private CUCLimitSwitchLed FUCLimitSwitchXL;

  }
}
