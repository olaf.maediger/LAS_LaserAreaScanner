﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCCnc
{
  public partial class CUCAxis : UserControl
  {
    private CUCLimitSwitchLed FLimitSwitchLow;
    private CUCLimitSwitchLed FLimitSwitchReference;
    private CUCLimitSwitchLed FLimitSwitchHigh;
    //
    public CUCAxis()
    {
      InitializeComponent();
      //
      FLimitSwitchLow = new CUCLimitSwitchLed();
      this.Controls.Add(FLimitSwitchLow);
      FLimitSwitchReference = new CUCLimitSwitchLed();
      this.Controls.Add(FLimitSwitchReference);
      FLimitSwitchHigh = new CUCLimitSwitchLed();
      this.Controls.Add(FLimitSwitchHigh);
    }

  }
}
