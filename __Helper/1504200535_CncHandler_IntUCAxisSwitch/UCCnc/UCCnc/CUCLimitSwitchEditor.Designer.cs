﻿namespace UCCnc
{
  partial class CUCLimitSwitchEditor
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel1 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.panel4 = new System.Windows.Forms.Panel();
      this.panel5 = new System.Windows.Forms.Panel();
      this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
      this.panel2 = new System.Windows.Forms.Panel();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.label3);
      this.panel1.Controls.Add(this.numericUpDown2);
      this.panel1.Controls.Add(this.textBox1);
      this.panel1.Controls.Add(this.label2);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Controls.Add(this.numericUpDown1);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(0, 255);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(338, 134);
      this.panel1.TabIndex = 0;
      // 
      // panel3
      // 
      this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel3.Location = new System.Drawing.Point(253, 0);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(85, 255);
      this.panel3.TabIndex = 2;
      // 
      // panel4
      // 
      this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel4.Location = new System.Drawing.Point(0, 0);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(80, 255);
      this.panel4.TabIndex = 3;
      // 
      // panel5
      // 
      this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel5.Location = new System.Drawing.Point(80, 22);
      this.panel5.Name = "panel5";
      this.panel5.Size = new System.Drawing.Size(173, 233);
      this.panel5.TabIndex = 5;
      // 
      // numericUpDown1
      // 
      this.numericUpDown1.DecimalPlaces = 3;
      this.numericUpDown1.Location = new System.Drawing.Point(177, 47);
      this.numericUpDown1.Name = "numericUpDown1";
      this.numericUpDown1.Size = new System.Drawing.Size(143, 36);
      this.numericUpDown1.TabIndex = 0;
      this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 47);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(169, 29);
      this.label1.TabIndex = 1;
      this.label1.Text = "Position [mm]";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 11);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(81, 29);
      this.label2.TabIndex = 2;
      this.label2.Text = "Name";
      // 
      // textBox1
      // 
      this.textBox1.Location = new System.Drawing.Point(99, 6);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(221, 36);
      this.textBox1.TabIndex = 3;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 89);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(197, 29);
      this.label3.TabIndex = 5;
      this.label3.Text = "Hysteresis [mm]";
      // 
      // numericUpDown2
      // 
      this.numericUpDown2.DecimalPlaces = 3;
      this.numericUpDown2.Location = new System.Drawing.Point(210, 89);
      this.numericUpDown2.Name = "numericUpDown2";
      this.numericUpDown2.Size = new System.Drawing.Size(110, 36);
      this.numericUpDown2.TabIndex = 4;
      this.numericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // panel2
      // 
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(80, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(173, 22);
      this.panel2.TabIndex = 4;
      // 
      // CUCLimitSwitchArrow
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 29F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel5);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.panel4);
      this.Controls.Add(this.panel3);
      this.Controls.Add(this.panel1);
      this.Name = "CUCLimitSwitchArrow";
      this.Size = new System.Drawing.Size(338, 389);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown numericUpDown2;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown numericUpDown1;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.Panel panel5;
    private System.Windows.Forms.Panel panel2;
  }
}
