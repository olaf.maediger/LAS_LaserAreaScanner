﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Hardware;
using Command;
//
namespace UCCnc
{
  public partial class CUCLimitSwitches : UserControl
  {
    public CUCLimitSwitches()
    {
      InitializeComponent();
      //
      // X - Axis
      FUCLimitSwitchXL.Axis = EAxis.X;
      FUCLimitSwitchXR.Axis = EAxis.X;
      FUCLimitSwitchXH.Axis = EAxis.X;
      // X - Kind
      FUCLimitSwitchXL.SwitchKind = ESwitchKind.Low;
      FUCLimitSwitchXR.SwitchKind = ESwitchKind.Reference;
      FUCLimitSwitchXH.SwitchKind = ESwitchKind.High;
      //
      // Y - Axis
      FUCLimitSwitchYL.Axis = EAxis.Y;
      FUCLimitSwitchYR.Axis = EAxis.Y;
      FUCLimitSwitchYH.Axis = EAxis.Y;
      // Y - Kind
      FUCLimitSwitchYL.SwitchKind = ESwitchKind.Low;
      FUCLimitSwitchYR.SwitchKind = ESwitchKind.Reference;
      FUCLimitSwitchYH.SwitchKind = ESwitchKind.High;
      //
      // Z - Axis
      FUCLimitSwitchZL.Axis = EAxis.Z;
      FUCLimitSwitchZR.Axis = EAxis.Z;
      FUCLimitSwitchZH.Axis = EAxis.Z;
      // X - Kind
      FUCLimitSwitchZL.SwitchKind = ESwitchKind.Low;
      FUCLimitSwitchZR.SwitchKind = ESwitchKind.Reference;
      FUCLimitSwitchZH.SwitchKind = ESwitchKind.High;
      //
    }

    //
    //---------------------------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------------------------
    /*???/
    public void SynchronizeSystemData(ref RSystemData systemdata)
    { // X
      FUCLimitSwitchXL.SwitchState = systemdata.LimitSwitchX.Low;
      FUCLimitSwitchXR.SwitchState = systemdata.LimitSwitchX.Reference;
      FUCLimitSwitchXH.SwitchState = systemdata.LimitSwitchX.High;
      // Y
      FUCLimitSwitchYL.SwitchState = systemdata.LimitSwitchY.Low;
      FUCLimitSwitchYR.SwitchState = systemdata.LimitSwitchY.Reference;
      FUCLimitSwitchYH.SwitchState = systemdata.LimitSwitchY.High;
      // Z
      FUCLimitSwitchZL.SwitchState = systemdata.LimitSwitchZ.Low;
      FUCLimitSwitchZR.SwitchState = systemdata.LimitSwitchZ.Reference;
      FUCLimitSwitchZH.SwitchState = systemdata.LimitSwitchZ.High;
    }*/

  }
}
