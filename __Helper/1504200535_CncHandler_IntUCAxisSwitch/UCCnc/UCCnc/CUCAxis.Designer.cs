﻿namespace UCCnc
{
  partial class CUCAxis
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel1 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.pnlHeader = new System.Windows.Forms.Panel();
      this.FUCLimitSwitchLine = new UCCnc.CUCLimitSwitchLine();
      this.cucStagePosition1 = new UCCnc.CUCStagePosition();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(1500, 86);
      this.panel1.TabIndex = 4;
      // 
      // panel3
      // 
      this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel3.Location = new System.Drawing.Point(1373, 86);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(127, 215);
      this.panel3.TabIndex = 6;
      // 
      // pnlHeader
      // 
      this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlHeader.Location = new System.Drawing.Point(0, 86);
      this.pnlHeader.Name = "pnlHeader";
      this.pnlHeader.Size = new System.Drawing.Size(127, 215);
      this.pnlHeader.TabIndex = 8;
      // 
      // FUCLimitSwitchLine
      // 
      this.FUCLimitSwitchLine.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.FUCLimitSwitchLine.Location = new System.Drawing.Point(127, 234);
      this.FUCLimitSwitchLine.Name = "FUCLimitSwitchLine";
      this.FUCLimitSwitchLine.Size = new System.Drawing.Size(1246, 67);
      this.FUCLimitSwitchLine.TabIndex = 10;
      // 
      // cucStagePosition1
      // 
      this.cucStagePosition1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.cucStagePosition1.Location = new System.Drawing.Point(127, 86);
      this.cucStagePosition1.Name = "cucStagePosition1";
      this.cucStagePosition1.Position = 50;
      this.cucStagePosition1.Size = new System.Drawing.Size(1246, 148);
      this.cucStagePosition1.TabIndex = 11;
      // 
      // CUCAxis
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 29F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.cucStagePosition1);
      this.Controls.Add(this.FUCLimitSwitchLine);
      this.Controls.Add(this.pnlHeader);
      this.Controls.Add(this.panel3);
      this.Controls.Add(this.panel1);
      this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
      this.Name = "CUCAxis";
      this.Size = new System.Drawing.Size(1500, 301);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Panel pnlHeader;
    private CUCLimitSwitchLine FUCLimitSwitchLine;
    private CUCStagePosition cucStagePosition1;

  }
}
