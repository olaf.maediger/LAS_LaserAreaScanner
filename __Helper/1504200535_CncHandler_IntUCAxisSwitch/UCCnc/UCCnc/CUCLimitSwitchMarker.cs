﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCCnc
{
  public partial class CUCLimitSwitchMarker : UserControl
  {
    public CUCLimitSwitchMarker()
    {
      InitializeComponent();
    }

    private void CUCLimitSwitchMarker_Paint(object sender, PaintEventArgs e)
    {
      Graphics G = e.Graphics;
      Int32 W = ClientRectangle.Width;
      Int32 WH = W / 2;
      Int32 H = ClientRectangle.Height;
      Int32 HH = H / 2;
      Int32 O = 6;
      Int32 OD = 2 * O;
      Color CF = Color.FromArgb(0xFF, 0xCC, 0xAA, 0xAA);
      Brush B = new SolidBrush(CF);
      G.FillRectangle(B, 0, 0, W, H);
      Color CP = Color.FromArgb(0xFF, 0x55, 0x44, 0x66);
      Pen P = new Pen(CP, 5);
      G.DrawLine(P, WH, O, WH, H - O);
      G.DrawLine(P, WH - OD, OD, WH, O);
      G.DrawLine(P, WH + OD, OD, WH, O);
      P.Dispose();
      B.Dispose();
    }
  }
}
