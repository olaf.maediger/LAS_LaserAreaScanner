﻿namespace UCCnc
{
  partial class CUCProgram
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnpProgram = new System.Windows.Forms.Panel();
      this.cbxReset = new System.Windows.Forms.CheckBox();
      this.nudCommandCount = new System.Windows.Forms.NumericUpDown();
      this.btnExecuteSequence = new System.Windows.Forms.Button();
      this.btnExecuteProgram = new System.Windows.Forms.Button();
      this.btnTranslateProgram = new System.Windows.Forms.Button();
      this.pnpProgram.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudCommandCount)).BeginInit();
      this.SuspendLayout();
      // 
      // pnpProgram
      // 
      this.pnpProgram.Controls.Add(this.cbxReset);
      this.pnpProgram.Controls.Add(this.nudCommandCount);
      this.pnpProgram.Controls.Add(this.btnExecuteSequence);
      this.pnpProgram.Controls.Add(this.btnExecuteProgram);
      this.pnpProgram.Controls.Add(this.btnTranslateProgram);
      this.pnpProgram.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnpProgram.Location = new System.Drawing.Point(0, 0);
      this.pnpProgram.Name = "pnpProgram";
      this.pnpProgram.Size = new System.Drawing.Size(443, 29);
      this.pnpProgram.TabIndex = 2;
      // 
      // cbxReset
      // 
      this.cbxReset.AutoSize = true;
      this.cbxReset.Checked = true;
      this.cbxReset.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbxReset.Location = new System.Drawing.Point(389, 8);
      this.cbxReset.Name = "cbxReset";
      this.cbxReset.Size = new System.Drawing.Size(54, 17);
      this.cbxReset.TabIndex = 4;
      this.cbxReset.Text = "Reset";
      this.cbxReset.UseVisualStyleBackColor = true;
      // 
      // nudCommandCount
      // 
      this.nudCommandCount.Location = new System.Drawing.Point(334, 5);
      this.nudCommandCount.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudCommandCount.Name = "nudCommandCount";
      this.nudCommandCount.Size = new System.Drawing.Size(51, 20);
      this.nudCommandCount.TabIndex = 3;
      this.nudCommandCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudCommandCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // btnExecuteSequence
      // 
      this.btnExecuteSequence.Location = new System.Drawing.Point(223, 3);
      this.btnExecuteSequence.Name = "btnExecuteSequence";
      this.btnExecuteSequence.Size = new System.Drawing.Size(108, 23);
      this.btnExecuteSequence.TabIndex = 2;
      this.btnExecuteSequence.Text = "Execute Sequence";
      this.btnExecuteSequence.UseVisualStyleBackColor = true;
      // 
      // btnExecuteProgram
      // 
      this.btnExecuteProgram.Location = new System.Drawing.Point(113, 3);
      this.btnExecuteProgram.Name = "btnExecuteProgram";
      this.btnExecuteProgram.Size = new System.Drawing.Size(108, 23);
      this.btnExecuteProgram.TabIndex = 1;
      this.btnExecuteProgram.Text = "Execute Program";
      this.btnExecuteProgram.UseVisualStyleBackColor = true;
      // 
      // btnTranslateProgram
      // 
      this.btnTranslateProgram.Location = new System.Drawing.Point(3, 3);
      this.btnTranslateProgram.Name = "btnTranslateProgram";
      this.btnTranslateProgram.Size = new System.Drawing.Size(108, 23);
      this.btnTranslateProgram.TabIndex = 0;
      this.btnTranslateProgram.Text = "Translate Program";
      this.btnTranslateProgram.UseVisualStyleBackColor = true;
      // 
      // CUCProgram
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnpProgram);
      this.Name = "CUCProgram";
      this.Size = new System.Drawing.Size(443, 29);
      this.pnpProgram.ResumeLayout(false);
      this.pnpProgram.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudCommandCount)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnpProgram;
    private System.Windows.Forms.CheckBox cbxReset;
    private System.Windows.Forms.NumericUpDown nudCommandCount;
    private System.Windows.Forms.Button btnExecuteSequence;
    private System.Windows.Forms.Button btnExecuteProgram;
    private System.Windows.Forms.Button btnTranslateProgram;
  }
}
