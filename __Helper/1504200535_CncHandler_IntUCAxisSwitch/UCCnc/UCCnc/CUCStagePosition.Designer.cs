﻿namespace UCCnc
{
  partial class CUCStagePosition
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.SuspendLayout();
      // 
      // CUCStagePosition
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(15F, 29F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Name = "CUCStagePosition";
      this.Size = new System.Drawing.Size(884, 150);
      this.Paint += new System.Windows.Forms.PaintEventHandler(this.CUCStagePosition_Paint);
      this.ResumeLayout(false);

    }

    #endregion
  }
}
