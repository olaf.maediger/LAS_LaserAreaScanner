﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Hardware;
//
namespace UCCnc
{
  public partial class CUCLimitSwitchLed : UserControl
  { //
    //---------------------------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------------------------
    //
    Color COLOR_LOW = Color.FromArgb(0xC8, 0x80, 0xFF, 0x80);
    Color COLOR_HIGH = Color.FromArgb(0xC8, 0xFF, 0x80, 0xB0);
    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    //
    private EAxis FAxis;
    private ESwitchKind FSwitchKind;
    private ESwitchState FSwitchState;
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CUCLimitSwitchLed()
    {
      InitializeComponent();
      //
      FAxis = EAxis.X;
      FSwitchKind = ESwitchKind.Low;
      RefreshHeader();
      FSwitchState = ESwitchState.Low;
      RefreshState(); 
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    public String Header
    {
      get { return lblHeader.Text; }
      set { lblHeader.Text = value; }
    }

    private void SetAxis(EAxis value)
    {
      FAxis = value;
      RefreshHeader();
    }
    public EAxis Axis
    {
      get { return FAxis; }
      set { SetAxis(value); }
    }

    private void SetSwitchKind(ESwitchKind value)
    {
      FSwitchKind = value;
      RefreshHeader();
    }
    public ESwitchKind SwitchKind
    {
      get { return FSwitchKind; }
      set { SetSwitchKind(value); }
    }

    private void SetSwitchState(ESwitchState value)
    {
      FSwitchState = value;
      RefreshState();
    }
    public ESwitchState SwitchState
    {
      get { return FSwitchState; }
      set { SetSwitchState(value); }
    }

    private Double FPosition;
    public Double Position
    {
      get { return FPosition; }
      set { FPosition = value; }
    }

    private Double FResolution;
    public Double Resolution
    {
      get { return FResolution; }
      set { FResolution = value; }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Local Helper
    //---------------------------------------------------------------------------
    //
    private void RefreshHeader()
    {
      lblHeader.Text = RAxisData.AxisText(FAxis) + RLimitSwitchData.SwitchKindText(FSwitchKind);
    }

    private void RefreshState()
    {
      lblState.Text = RLimitSwitchData.SwitchStateText(FSwitchState);
      pnlMain.Invalidate();
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Event
    //---------------------------------------------------------------------------
    //
    private void pnlMain_Paint(object sender, PaintEventArgs e)
    {
      const Int32 BORDER = 1;
      Graphics G = e.Graphics;
      Int32 L = BORDER - 1;
      Int32 T = BORDER;
      Int32 W = pnlMain.ClientRectangle.Width - 2 * BORDER;
      Int32 H = pnlMain.ClientRectangle.Height - 2 * BORDER;
      Brush B;
      switch (FSwitchState)
      {
        case ESwitchState.High:
          B = new SolidBrush(COLOR_HIGH);
          break;
        default: // Low
          B = new SolidBrush(COLOR_LOW);
          break;
      }
      G.FillEllipse(B, L, T, W, H);
      B.Dispose();
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------------------------
    //

  }
}
