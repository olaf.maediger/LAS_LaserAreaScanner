﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCCnc
{
  public partial class CUCLimitSwitchLine : UserControl
  {
    private const Int32 COUNT_MARKER = 3;
    private const Int32 INDEX_L = 0;
    private const Int32 INDEX_R = 1;
    private const Int32 INDEX_H = 2;

    private CUCLimitSwitchMarker[] FMarker;
    private Int32[] FPosition;

    public CUCLimitSwitchLine()
    {
      InitializeComponent();
      //
      FPosition = new Int32[COUNT_MARKER];
      FPosition[INDEX_L] = 0;
      FPosition[INDEX_R] = 100;
      FPosition[INDEX_H] = 400;
      //
      FMarker = new CUCLimitSwitchMarker[COUNT_MARKER];
      FMarker[INDEX_L] = new CUCLimitSwitchMarker();
      this.Controls.Add(FMarker[INDEX_L]);
      FMarker[INDEX_L].Left = FPosition[INDEX_L] - FMarker[INDEX_L].Width / 2;
      //
      FMarker[INDEX_R] = new CUCLimitSwitchMarker();
      this.Controls.Add(FMarker[INDEX_R]);
      FMarker[INDEX_R].Left = FPosition[INDEX_R] - FMarker[INDEX_R].Width / 2;
      //
      FMarker[INDEX_H] = new CUCLimitSwitchMarker();
      this.Controls.Add(FMarker[INDEX_H]);
      FMarker[INDEX_H].Left = FPosition[INDEX_H] - FMarker[INDEX_H].Width / 2;
    }

  


    public void SetPosition(Int32 index, Int32 value)
    {
      if ((0 <= index) && (index < COUNT_MARKER))
      {
        if (value != FPosition[index])
        {
          FPosition[index] = value;
          FMarker[index].Left = value - FMarker[index].Width / 2;
          Invalidate();
        }
      }
    }


    private void CUCLimitSwitchLine_Paint(object sender, PaintEventArgs e)
    {
      
    }


  }
}
