﻿namespace UCCnc
{
  partial class CUCSimulation
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.nudTimeResolution = new System.Windows.Forms.NumericUpDown();
      this.cbxHaltResume = new System.Windows.Forms.CheckBox();
      this.label1 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudTimeResolution)).BeginInit();
      this.SuspendLayout();
      // 
      // nudTimeResolution
      // 
      this.nudTimeResolution.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudTimeResolution.Location = new System.Drawing.Point(181, 5);
      this.nudTimeResolution.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudTimeResolution.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudTimeResolution.Name = "nudTimeResolution";
      this.nudTimeResolution.Size = new System.Drawing.Size(51, 20);
      this.nudTimeResolution.TabIndex = 5;
      this.nudTimeResolution.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTimeResolution.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudTimeResolution.ValueChanged += new System.EventHandler(this.nudTimeResolution_ValueChanged);
      // 
      // cbxHaltResume
      // 
      this.cbxHaltResume.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxHaltResume.Location = new System.Drawing.Point(3, 3);
      this.cbxHaltResume.Name = "cbxHaltResume";
      this.cbxHaltResume.Size = new System.Drawing.Size(74, 23);
      this.cbxHaltResume.TabIndex = 6;
      this.cbxHaltResume.Text = "HaltResume";
      this.cbxHaltResume.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxHaltResume.UseVisualStyleBackColor = true;
      this.cbxHaltResume.CheckedChanged += new System.EventHandler(this.cbxHaltResume_CheckedChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(79, 8);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(102, 13);
      this.label1.TabIndex = 7;
      this.label1.Text = "TimeResolution [ms]";
      // 
      // CUCSimulation
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label1);
      this.Controls.Add(this.cbxHaltResume);
      this.Controls.Add(this.nudTimeResolution);
      this.Name = "CUCSimulation";
      this.Size = new System.Drawing.Size(235, 29);
      ((System.ComponentModel.ISupportInitialize)(this.nudTimeResolution)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.NumericUpDown nudTimeResolution;
    private System.Windows.Forms.CheckBox cbxHaltResume;
    private System.Windows.Forms.Label label1;
  }
}
