﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
//
using Initdata;
using UCNotifier;
//
using Command;
using Cnc;
using UdpInterface;
using Hardware;
//
namespace CncTranslator
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormCncTranslator : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String NAME_STARTPROCESS_CNCSIMULATOR = "StartProcessCncSimulator";
    private const Boolean INIT_STARTPROCESS_CNCSIMULATOR = true;
    private const String NAME_STARTPROCESS_CNCCONTROLLER = "StartProcessCncController";
    private const Boolean INIT_STARTPROCESS_CNCCONTROLLER = true;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;
    private CUdpReceiver FUdpReceiverSimulator;
    private CUdpTransmitter FUdpTransmitterSimulator;
    private CCnc FCnc;
    private CUdpReceiver FUdpReceiverController;
    private CUdpTransmitter FUdpTransmitterController;
    //???private RSystemData FSystemData;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 NC!!!!
    /*private void CncGetSystemData(out RSystemData systemdata)
    {
      systemdata = FSystemData;
    }

    private void CncSetSystemData(RSystemData systemdata)
    {
      FSystemData = systemdata;
    }*/

    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      //  Simulator
      FUCTerminalUdpSimulator.SetOnSendText(UCTerminalUdpSimulatorOnSendText);
      FUCTerminalUdpSimulator.SetOnPortOpened(UCTerminalUdpSimulatorOnPortOpened);
      FUCTerminalUdpSimulator.SetOnPortClosed(UCTerminalUdpSimulatorOnPortClosed);
      //FUCTerminalUdpSimulator.PresetText("M119");
      //FUCTerminalUdpSimulator.PresetText("G21 X100 F600");
      //FUCTerminalUdpSimulator.PresetText("G21 X100 F2000");
      //FUCTerminalUdpSimulator.PresetText("G21 X110 Y160 F600");
      FUCTerminalUdpSimulator.PresetText("G21 X20 F600");
      //
      FUdpTransmitterSimulator = new CUdpTransmitter();
      FUdpTransmitterSimulator.SetNotifier(FUCNotifier);
      //
      FUdpReceiverSimulator = new CUdpReceiver();
      FUdpReceiverSimulator.SetNotifier(FUCNotifier);
      FUdpReceiverSimulator.SetOnTextReceived(UdpReceiverSimulatorOnTextReceived);
      //
      //???FSystemData = new RSystemData(0);
      FCnc = new CCnc();//CncGetSystemData, CncSetSystemData);
      FCnc.SetNotifier(FUCNotifier);
      FCnc.SetOnResponse(null);// not here!CncOnResponse);
      FCnc.SetOnLimitSwitchState(CncOnLimitSwitchState);
      //
      //  Controller
      FUCTerminalUdpController.SetOnSendText(UCTerminalUdpControllerOnSendText);
      FUCTerminalUdpController.SetOnPortOpened(UCTerminalUdpControllerOnPortOpened);
      FUCTerminalUdpController.SetOnPortClosed(UCTerminalUdpControllerOnPortClosed);
      //
      FUdpReceiverController = new CUdpReceiver();
      FUdpReceiverController.SetNotifier(FUCNotifier);
      FUdpReceiverController.SetOnTextReceived(UdpReceiverControllerOnTextReceived);
      //
      FUdpTransmitterController = new CUdpTransmitter();
      FUdpTransmitterController.SetNotifier(FUCNotifier);
      //
      tmrStartup.Interval = 1000;
      //
      tmrStartup.Start();

      FRandom = new Random();
    }

    private void FreeProgrammerControls()
    {
      CloseAllChildProcesses();
      //
      tmrStartup.Stop();
      //
      FUdpTransmitterSimulator.Close();
      FUdpReceiverSimulator.Close();
      FUdpTransmitterController.Close();
      FUdpReceiverController.Close();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      Boolean BValue;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      BValue = INIT_STARTPROCESS_CNCSIMULATOR;
      Result &= initdata.ReadBoolean(NAME_STARTPROCESS_CNCSIMULATOR, out BValue, BValue);
      if (BValue)
      {
        //StartProcessCncSimulator();
      }
      //
      BValue = INIT_STARTPROCESS_CNCCONTROLLER;
      Result &= initdata.ReadBoolean(NAME_STARTPROCESS_CNCCONTROLLER, out BValue, BValue);
      if (BValue)
      {
        //!!!StartProcessCncController();
      }
      //
      FUCTerminalUdpSimulator.Open();
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //
    private void CloseAllChildProcesses()
    {
      Process[] ChildProcesses = Process.GetProcessesByName("CncSimulator");
      foreach (Process ChildProcess in ChildProcesses)
      {
        ChildProcess.CloseMainWindow();
      }
      ChildProcesses = Process.GetProcessesByName("CncController");
      foreach (Process ChildProcess in ChildProcesses)
      {
        ChildProcess.CloseMainWindow();
      }
    }

    private void StartProcessCncSimulator()
    {
      ProcessStartInfo PSI = new ProcessStartInfo();
      PSI.FileName = "CncSimulator.exe";
      PSI.Arguments = "";
      PSI.UseShellExecute = true;
      PSI.CreateNoWindow = false;
      PSI.WindowStyle = ProcessWindowStyle.Normal;
      Process.Start(PSI);
    }

    private void StartProcessCncController()
    {
      ProcessStartInfo PSI = new ProcessStartInfo();
      PSI.FileName = "CncController.exe";
      PSI.Arguments = "";
      PSI.UseShellExecute = true;
      PSI.CreateNoWindow = false;
      PSI.WindowStyle = ProcessWindowStyle.Normal;
      Process.Start(PSI);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        default:
          return false;
      }
    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }  
    //
    //--------------------------------------------------
    //  Segment - Helper - 
    //--------------------------------------------------
    //

    //########################################################################
    //------------------------------------------------------------------------
    //  Section - Callback - Cnc
    //------------------------------------------------------------------------
    //
    private void CncOnLimitSwitchState(RCommandParameter systemdata)
    {

    }
    //########################################################################
    //------------------------------------------------------------------------
    //  Section - Callback - UCTerminal - Simulator
    //------------------------------------------------------------------------
    //
    private Boolean UCTerminalUdpSimulatorOnPortOpened()
    {
      Boolean Result = true;
      //
      Result &= FUdpTransmitterSimulator.Open("127.0.0.1", CUdpInterface.PORT_UDP_01);
      Result &= FUdpReceiverSimulator.Open("127.0.0.1", CUdpInterface.PORT_UDP_02, "");
      //
      return Result;
    }

    private Boolean UCTerminalUdpSimulatorOnPortClosed()
    {
      Boolean Result = true;
      //
      Result &= FUdpTransmitterSimulator.Close();
      Result &= FUdpReceiverSimulator.Close();
      //
      return Result;
    }

    private Boolean UCTerminalUdpSimulatorOnSendText(String data)
    { // UCTerminal -> Send-Button -> here -> UdpTransmitter.Send & UCTerminal.Add
      Boolean Result = false;
      if (FUdpTransmitterSimulator.SendText(data))
      {
        Result = FUCTerminalUdpSimulator.AddTextTransmitted(data);
      }
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UdpTransmitter - Simulator
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback - UdpReceiver - Simulator
    //------------------------------------------------------------------------
    //
    private delegate void CBUdpReceiverSimulatorOnTextReceived(String data);
    private void UdpReceiverSimulatorOnTextReceived(String data)
    {
      if (this.InvokeRequired)
      {
        CBUdpReceiverSimulatorOnTextReceived CB =
          new CBUdpReceiverSimulatorOnTextReceived(UdpReceiverSimulatorOnTextReceived);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCTerminalUdpSimulator.AddTextReceived(data);
        // Only for Responses!!!!
        if (FCnc.TranslateProgram(data))
        {
          // !!!! not in Translator!!! if (FCnc.ExecuteProgram(ref FSy))
          {
          }
        }
      }
    }
    //########################################################################
    //------------------------------------------------------------------------
    //  Section - Callback - UCTerminal - Controller
    //------------------------------------------------------------------------
    //
    private Boolean UCTerminalUdpControllerOnPortOpened()
    {
      Boolean Result = true;
      //
      Result &= FUdpTransmitterController.Open("127.0.0.1", CUdpInterface.PORT_UDP_03);
      Result &= FUdpReceiverController.Open("127.0.0.1", CUdpInterface.PORT_UDP_04, "");
      //
      return Result;
    }

    private Boolean UCTerminalUdpControllerOnPortClosed()
    {
      Boolean Result = true;
      //
      Result &= FUdpTransmitterController.Close();
      Result &= FUdpReceiverController.Close();
      //
      return Result;
    }

    private Boolean UCTerminalUdpControllerOnSendText(String data)
    { // UCTerminal -> Send-Button -> here -> UdpTransmitter.Send & UCTerminal.Add
      Boolean Result = false;
      if (FUdpTransmitterController.SendText(data))
      {
        Result = FUCTerminalUdpController.AddTextTransmitted(data);
      }
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UdpTransmitter - Controller
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback - UdpReceiver - Controller
    //------------------------------------------------------------------------
    //
    private delegate void CBUdpReceiverControllerOnTextReceived(String data);
    private void UdpReceiverControllerOnTextReceived(String data)
    {
      if (this.InvokeRequired)
      {
        CBUdpReceiverControllerOnTextReceived CB =
          new CBUdpReceiverControllerOnTextReceived(UdpReceiverControllerOnTextReceived);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCTerminalUdpController.AddTextReceived(data);
      }
    }
    //########################################################################
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    // 


  }
}

