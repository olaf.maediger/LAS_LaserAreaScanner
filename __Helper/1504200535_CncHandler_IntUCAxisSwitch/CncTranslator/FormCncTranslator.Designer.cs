﻿namespace CncTranslator
{
  partial class FormCncTranslator
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.FHelpProvider = new System.Windows.Forms.HelpProvider();
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.mitSystem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
      this.mitSQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.mitConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCDefault = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCResetToDefaultConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveAutomaticAtProgramEnd = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCStartup = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPEditParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPClearProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
      this.diagnosticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPLoadDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPSaveDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSendDiagnosticEmail = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
      this.mitAction = new System.Windows.Forms.ToolStripMenuItem();
      this.mitAOpenClose = new System.Windows.Forms.ToolStripMenuItem();
      this.mitAStartStop = new System.Windows.Forms.ToolStripMenuItem();
      this.mitAProperties = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHelp = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHContents = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHTopic = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHSearch = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
      this.mitHEnterApplicationProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterDeviceProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
      this.mitHAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.DialogSaveInitdata = new System.Windows.Forms.SaveFileDialog();
      this.tmrStartup = new System.Windows.Forms.Timer(this.components);
      this.DialogLoadInitdata = new System.Windows.Forms.OpenFileDialog();
      this.pnlProtocol = new System.Windows.Forms.Panel();
      this.FUCSerialNumber = new UCSerialNumber.CUCSerialNumber();
      this.splProtocol = new System.Windows.Forms.Splitter();
      this.cbxAutomate = new System.Windows.Forms.CheckBox();
      this.pnlMain = new System.Windows.Forms.Panel();
      this.panel1 = new System.Windows.Forms.Panel();
      this.cbxReset = new System.Windows.Forms.CheckBox();
      this.nudCommandCount = new System.Windows.Forms.NumericUpDown();
      this.btnExecuteSequence = new System.Windows.Forms.Button();
      this.btnExecuteProgram = new System.Windows.Forms.Button();
      this.btnTranslateProgram = new System.Windows.Forms.Button();
      this.tbcView = new System.Windows.Forms.TabControl();
      this.tbpTerminal = new System.Windows.Forms.TabPage();
      this.FUCTerminalUdpController = new UCTerminal.CUCTerminalUdp();
      this.splitter2 = new System.Windows.Forms.Splitter();
      this.FUCTerminalUdpSimulator = new UCTerminal.CUCTerminalUdp();
      this.tbpDraw = new System.Windows.Forms.TabPage();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.FUCTextEditor = new UCTextEditor.CUCEditTextEditor();
      this.mstMain.SuspendLayout();
      this.pnlProtocol.SuspendLayout();
      this.pnlMain.SuspendLayout();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudCommandCount)).BeginInit();
      this.tbcView.SuspendLayout();
      this.tbpTerminal.SuspendLayout();
      this.tbpDraw.SuspendLayout();
      this.SuspendLayout();
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSystem,
            this.mitConfiguration,
            this.mitProtocol,
            this.mitAction,
            this.mitHelp});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(920, 24);
      this.mstMain.TabIndex = 104;
      this.mstMain.Text = "System";
      // 
      // mitSystem
      // 
      this.mitSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.mitSQuit});
      this.mitSystem.Name = "mitSystem";
      this.mitSystem.Size = new System.Drawing.Size(57, 20);
      this.mitSystem.Text = "&System";
      // 
      // toolStripMenuItem3
      // 
      this.toolStripMenuItem3.Name = "toolStripMenuItem3";
      this.toolStripMenuItem3.Size = new System.Drawing.Size(137, 6);
      // 
      // mitSQuit
      // 
      this.mitSQuit.Name = "mitSQuit";
      this.mitSQuit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
      this.mitSQuit.Size = new System.Drawing.Size(140, 22);
      this.mitSQuit.Text = "&Quit";
      // 
      // mitConfiguration
      // 
      this.mitConfiguration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitCDefault,
            this.mitCResetToDefaultConfiguration,
            this.mitCSaveAutomaticAtProgramEnd,
            this.mitCStartup,
            this.mitCLoadStartupConfiguration,
            this.mitCSaveStartupConfiguration,
            this.mitCShowEditStartupConfiguration,
            this.toolStripMenuItem7,
            this.mitCLoadSelectableConfiguration,
            this.mitCSaveSelectableConfiguration,
            this.mitCShowEditSelectableConfiguration});
      this.mitConfiguration.Name = "mitConfiguration";
      this.mitConfiguration.Size = new System.Drawing.Size(93, 20);
      this.mitConfiguration.Text = "&Configuration";
      // 
      // mitCDefault
      // 
      this.mitCDefault.Enabled = false;
      this.mitCDefault.Name = "mitCDefault";
      this.mitCDefault.Size = new System.Drawing.Size(300, 22);
      this.mitCDefault.Text = "- Default -";
      // 
      // mitCResetToDefaultConfiguration
      // 
      this.mitCResetToDefaultConfiguration.Name = "mitCResetToDefaultConfiguration";
      this.mitCResetToDefaultConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
      this.mitCResetToDefaultConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCResetToDefaultConfiguration.Text = "Reset to Default-Configuration";
      // 
      // mitCSaveAutomaticAtProgramEnd
      // 
      this.mitCSaveAutomaticAtProgramEnd.Name = "mitCSaveAutomaticAtProgramEnd";
      this.mitCSaveAutomaticAtProgramEnd.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveAutomaticAtProgramEnd.Text = "Save automatic at program end";
      // 
      // mitCStartup
      // 
      this.mitCStartup.Enabled = false;
      this.mitCStartup.Name = "mitCStartup";
      this.mitCStartup.Size = new System.Drawing.Size(300, 22);
      this.mitCStartup.Text = "- Startup -";
      // 
      // mitCLoadStartupConfiguration
      // 
      this.mitCLoadStartupConfiguration.Name = "mitCLoadStartupConfiguration";
      this.mitCLoadStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
      this.mitCLoadStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadStartupConfiguration.Text = "Load Startup-Configuration";
      // 
      // mitCSaveStartupConfiguration
      // 
      this.mitCSaveStartupConfiguration.Name = "mitCSaveStartupConfiguration";
      this.mitCSaveStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
      this.mitCSaveStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveStartupConfiguration.Text = "Save Startup-Configuration";
      // 
      // mitCShowEditStartupConfiguration
      // 
      this.mitCShowEditStartupConfiguration.Name = "mitCShowEditStartupConfiguration";
      this.mitCShowEditStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditStartupConfiguration.Text = "Show / Edit Startup-Configuration";
      // 
      // toolStripMenuItem7
      // 
      this.toolStripMenuItem7.Enabled = false;
      this.toolStripMenuItem7.Name = "toolStripMenuItem7";
      this.toolStripMenuItem7.Size = new System.Drawing.Size(300, 22);
      this.toolStripMenuItem7.Text = "- Named -";
      // 
      // mitCLoadSelectableConfiguration
      // 
      this.mitCLoadSelectableConfiguration.Name = "mitCLoadSelectableConfiguration";
      this.mitCLoadSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadSelectableConfiguration.Text = "Load Selectable-Configuration ";
      // 
      // mitCSaveSelectableConfiguration
      // 
      this.mitCSaveSelectableConfiguration.Name = "mitCSaveSelectableConfiguration";
      this.mitCSaveSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveSelectableConfiguration.Text = "Save Selectable-Configuration";
      // 
      // mitCShowEditSelectableConfiguration
      // 
      this.mitCShowEditSelectableConfiguration.Name = "mitCShowEditSelectableConfiguration";
      this.mitCShowEditSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditSelectableConfiguration.Text = "Show / Edit Selectable-Configuration";
      // 
      // mitProtocol
      // 
      this.mitProtocol.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitPEditParameter,
            this.mitPClearProtocol,
            this.mitPCopyToClipboard,
            this.toolStripMenuItem1,
            this.diagnosticToolStripMenuItem,
            this.mitPLoadDiagnosticFile,
            this.mitPSaveDiagnosticFile,
            this.mitSendDiagnosticEmail,
            this.toolStripMenuItem2});
      this.mitProtocol.Name = "mitProtocol";
      this.mitProtocol.Size = new System.Drawing.Size(64, 20);
      this.mitProtocol.Text = "&Protocol";
      // 
      // mitPEditParameter
      // 
      this.mitPEditParameter.Name = "mitPEditParameter";
      this.mitPEditParameter.Size = new System.Drawing.Size(171, 22);
      this.mitPEditParameter.Text = "Edit Parameter";
      // 
      // mitPClearProtocol
      // 
      this.mitPClearProtocol.Name = "mitPClearProtocol";
      this.mitPClearProtocol.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
      this.mitPClearProtocol.Size = new System.Drawing.Size(171, 22);
      this.mitPClearProtocol.Text = "Clear";
      // 
      // mitPCopyToClipboard
      // 
      this.mitPCopyToClipboard.Name = "mitPCopyToClipboard";
      this.mitPCopyToClipboard.Size = new System.Drawing.Size(171, 22);
      this.mitPCopyToClipboard.Text = "Copy to Clipboard";
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new System.Drawing.Size(168, 6);
      // 
      // diagnosticToolStripMenuItem
      // 
      this.diagnosticToolStripMenuItem.Enabled = false;
      this.diagnosticToolStripMenuItem.Name = "diagnosticToolStripMenuItem";
      this.diagnosticToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
      this.diagnosticToolStripMenuItem.Text = "- Diagnostic -";
      // 
      // mitPLoadDiagnosticFile
      // 
      this.mitPLoadDiagnosticFile.Name = "mitPLoadDiagnosticFile";
      this.mitPLoadDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPLoadDiagnosticFile.Text = "Load File";
      // 
      // mitPSaveDiagnosticFile
      // 
      this.mitPSaveDiagnosticFile.Name = "mitPSaveDiagnosticFile";
      this.mitPSaveDiagnosticFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
      this.mitPSaveDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPSaveDiagnosticFile.Text = "Save File";
      // 
      // mitSendDiagnosticEmail
      // 
      this.mitSendDiagnosticEmail.Name = "mitSendDiagnosticEmail";
      this.mitSendDiagnosticEmail.Size = new System.Drawing.Size(171, 22);
      this.mitSendDiagnosticEmail.Text = "Send Email";
      // 
      // toolStripMenuItem2
      // 
      this.toolStripMenuItem2.Name = "toolStripMenuItem2";
      this.toolStripMenuItem2.Size = new System.Drawing.Size(168, 6);
      // 
      // mitAction
      // 
      this.mitAction.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitAOpenClose,
            this.mitAStartStop,
            this.mitAProperties});
      this.mitAction.Enabled = false;
      this.mitAction.Name = "mitAction";
      this.mitAction.Size = new System.Drawing.Size(54, 20);
      this.mitAction.Text = "Action";
      // 
      // mitAOpenClose
      // 
      this.mitAOpenClose.Name = "mitAOpenClose";
      this.mitAOpenClose.Size = new System.Drawing.Size(132, 22);
      this.mitAOpenClose.Text = "OpenClose";
      // 
      // mitAStartStop
      // 
      this.mitAStartStop.Name = "mitAStartStop";
      this.mitAStartStop.Size = new System.Drawing.Size(132, 22);
      this.mitAStartStop.Text = "StartStop";
      // 
      // mitAProperties
      // 
      this.mitAProperties.Name = "mitAProperties";
      this.mitAProperties.Size = new System.Drawing.Size(132, 22);
      this.mitAProperties.Text = "Properties";
      // 
      // mitHelp
      // 
      this.mitHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitHContents,
            this.mitHTopic,
            this.mitHSearch,
            this.toolStripMenuItem5,
            this.mitHEnterApplicationProductKey,
            this.mitHEnterDeviceProductKey,
            this.toolStripMenuItem4,
            this.mitHAbout});
      this.mitHelp.Name = "mitHelp";
      this.mitHelp.Size = new System.Drawing.Size(44, 20);
      this.mitHelp.Text = "Help";
      // 
      // mitHContents
      // 
      this.mitHContents.Name = "mitHContents";
      this.mitHContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.mitHContents.Size = new System.Drawing.Size(234, 22);
      this.mitHContents.Text = "Contents";
      // 
      // mitHTopic
      // 
      this.mitHTopic.Name = "mitHTopic";
      this.mitHTopic.Size = new System.Drawing.Size(234, 22);
      this.mitHTopic.Text = "Topic";
      // 
      // mitHSearch
      // 
      this.mitHSearch.Name = "mitHSearch";
      this.mitHSearch.Size = new System.Drawing.Size(234, 22);
      this.mitHSearch.Text = "Search";
      // 
      // toolStripMenuItem5
      // 
      this.toolStripMenuItem5.Name = "toolStripMenuItem5";
      this.toolStripMenuItem5.Size = new System.Drawing.Size(231, 6);
      // 
      // mitHEnterApplicationProductKey
      // 
      this.mitHEnterApplicationProductKey.Name = "mitHEnterApplicationProductKey";
      this.mitHEnterApplicationProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterApplicationProductKey.Text = "Enter Application Product-Key";
      // 
      // mitHEnterDeviceProductKey
      // 
      this.mitHEnterDeviceProductKey.Name = "mitHEnterDeviceProductKey";
      this.mitHEnterDeviceProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterDeviceProductKey.Text = "Enter Device Product-Key";
      // 
      // toolStripMenuItem4
      // 
      this.toolStripMenuItem4.Name = "toolStripMenuItem4";
      this.toolStripMenuItem4.Size = new System.Drawing.Size(231, 6);
      // 
      // mitHAbout
      // 
      this.mitHAbout.Name = "mitHAbout";
      this.mitHAbout.ShortcutKeys = System.Windows.Forms.Keys.F2;
      this.mitHAbout.Size = new System.Drawing.Size(234, 22);
      this.mitHAbout.Text = "About";
      // 
      // DialogSaveInitdata
      // 
      this.DialogSaveInitdata.DefaultExt = "ini.xml";
      this.DialogSaveInitdata.FileName = "Initdata";
      this.DialogSaveInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogSaveInitdata.Title = "MDesign: Save Initdata";
      // 
      // tmrStartup
      // 
      this.tmrStartup.Interval = 1;
      // 
      // DialogLoadInitdata
      // 
      this.DialogLoadInitdata.DefaultExt = "ini.xml";
      this.DialogLoadInitdata.FileName = "Initdata";
      this.DialogLoadInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogLoadInitdata.Title = "MDesign: Load Initdata";
      // 
      // pnlProtocol
      // 
      this.pnlProtocol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlProtocol.Controls.Add(this.FUCSerialNumber);
      this.pnlProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlProtocol.Location = new System.Drawing.Point(0, 712);
      this.pnlProtocol.Name = "pnlProtocol";
      this.pnlProtocol.Size = new System.Drawing.Size(920, 141);
      this.pnlProtocol.TabIndex = 111;
      // 
      // FUCSerialNumber
      // 
      this.FUCSerialNumber.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCSerialNumber.Location = new System.Drawing.Point(0, 0);
      this.FUCSerialNumber.Name = "FUCSerialNumber";
      this.FUCSerialNumber.Size = new System.Drawing.Size(918, 64);
      this.FUCSerialNumber.TabIndex = 110;
      this.FUCSerialNumber.Visible = false;
      // 
      // splProtocol
      // 
      this.splProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splProtocol.Location = new System.Drawing.Point(0, 709);
      this.splProtocol.Name = "splProtocol";
      this.splProtocol.Size = new System.Drawing.Size(920, 3);
      this.splProtocol.TabIndex = 112;
      this.splProtocol.TabStop = false;
      // 
      // cbxAutomate
      // 
      this.cbxAutomate.AutoSize = true;
      this.cbxAutomate.Location = new System.Drawing.Point(320, 4);
      this.cbxAutomate.Name = "cbxAutomate";
      this.cbxAutomate.Size = new System.Drawing.Size(71, 17);
      this.cbxAutomate.TabIndex = 116;
      this.cbxAutomate.Text = "Automate";
      this.cbxAutomate.UseVisualStyleBackColor = true;
      // 
      // pnlMain
      // 
      this.pnlMain.Controls.Add(this.panel1);
      this.pnlMain.Controls.Add(this.tbcView);
      this.pnlMain.Controls.Add(this.splitter1);
      this.pnlMain.Controls.Add(this.FUCTextEditor);
      this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlMain.Location = new System.Drawing.Point(0, 24);
      this.pnlMain.Name = "pnlMain";
      this.pnlMain.Size = new System.Drawing.Size(920, 685);
      this.pnlMain.TabIndex = 117;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.cbxReset);
      this.panel1.Controls.Add(this.nudCommandCount);
      this.panel1.Controls.Add(this.btnExecuteSequence);
      this.panel1.Controls.Add(this.btnExecuteProgram);
      this.panel1.Controls.Add(this.btnTranslateProgram);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(417, 656);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(503, 29);
      this.panel1.TabIndex = 5;
      // 
      // cbxReset
      // 
      this.cbxReset.AutoSize = true;
      this.cbxReset.Checked = true;
      this.cbxReset.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbxReset.Location = new System.Drawing.Point(418, 8);
      this.cbxReset.Name = "cbxReset";
      this.cbxReset.Size = new System.Drawing.Size(54, 17);
      this.cbxReset.TabIndex = 4;
      this.cbxReset.Text = "Reset";
      this.cbxReset.UseVisualStyleBackColor = true;
      // 
      // nudCommandCount
      // 
      this.nudCommandCount.Location = new System.Drawing.Point(363, 5);
      this.nudCommandCount.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudCommandCount.Name = "nudCommandCount";
      this.nudCommandCount.Size = new System.Drawing.Size(51, 20);
      this.nudCommandCount.TabIndex = 3;
      this.nudCommandCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudCommandCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // btnExecuteSequence
      // 
      this.btnExecuteSequence.Location = new System.Drawing.Point(249, 3);
      this.btnExecuteSequence.Name = "btnExecuteSequence";
      this.btnExecuteSequence.Size = new System.Drawing.Size(112, 23);
      this.btnExecuteSequence.TabIndex = 2;
      this.btnExecuteSequence.Text = "Execute Sequence";
      this.btnExecuteSequence.UseVisualStyleBackColor = true;
      // 
      // btnExecuteProgram
      // 
      this.btnExecuteProgram.Location = new System.Drawing.Point(126, 3);
      this.btnExecuteProgram.Name = "btnExecuteProgram";
      this.btnExecuteProgram.Size = new System.Drawing.Size(112, 23);
      this.btnExecuteProgram.TabIndex = 1;
      this.btnExecuteProgram.Text = "Execute Program";
      this.btnExecuteProgram.UseVisualStyleBackColor = true;
      // 
      // btnTranslateProgram
      // 
      this.btnTranslateProgram.Location = new System.Drawing.Point(3, 3);
      this.btnTranslateProgram.Name = "btnTranslateProgram";
      this.btnTranslateProgram.Size = new System.Drawing.Size(112, 23);
      this.btnTranslateProgram.TabIndex = 0;
      this.btnTranslateProgram.Text = "Translate Program";
      this.btnTranslateProgram.UseVisualStyleBackColor = true;
      // 
      // tbcView
      // 
      this.tbcView.Alignment = System.Windows.Forms.TabAlignment.Bottom;
      this.tbcView.Controls.Add(this.tbpTerminal);
      this.tbcView.Controls.Add(this.tbpDraw);
      this.tbcView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcView.Location = new System.Drawing.Point(417, 0);
      this.tbcView.Name = "tbcView";
      this.tbcView.SelectedIndex = 0;
      this.tbcView.Size = new System.Drawing.Size(503, 685);
      this.tbcView.TabIndex = 8;
      // 
      // tbpTerminal
      // 
      this.tbpTerminal.Controls.Add(this.FUCTerminalUdpController);
      this.tbpTerminal.Controls.Add(this.splitter2);
      this.tbpTerminal.Controls.Add(this.FUCTerminalUdpSimulator);
      this.tbpTerminal.Location = new System.Drawing.Point(4, 4);
      this.tbpTerminal.Name = "tbpTerminal";
      this.tbpTerminal.Padding = new System.Windows.Forms.Padding(3);
      this.tbpTerminal.Size = new System.Drawing.Size(495, 659);
      this.tbpTerminal.TabIndex = 1;
      this.tbpTerminal.Text = "Terminal";
      this.tbpTerminal.UseVisualStyleBackColor = true;
      // 
      // FUCTerminalUdpController
      // 
      this.FUCTerminalUdpController.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCTerminalUdpController.Location = new System.Drawing.Point(3, 339);
      this.FUCTerminalUdpController.Name = "FUCTerminalUdpController";
      this.FUCTerminalUdpController.Size = new System.Drawing.Size(489, 317);
      this.FUCTerminalUdpController.TabIndex = 2;
      // 
      // splitter2
      // 
      this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
      this.splitter2.Location = new System.Drawing.Point(3, 336);
      this.splitter2.Name = "splitter2";
      this.splitter2.Size = new System.Drawing.Size(489, 3);
      this.splitter2.TabIndex = 1;
      this.splitter2.TabStop = false;
      // 
      // FUCTerminalUdpSimulator
      // 
      this.FUCTerminalUdpSimulator.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCTerminalUdpSimulator.Location = new System.Drawing.Point(3, 3);
      this.FUCTerminalUdpSimulator.Name = "FUCTerminalUdpSimulator";
      this.FUCTerminalUdpSimulator.Size = new System.Drawing.Size(489, 333);
      this.FUCTerminalUdpSimulator.TabIndex = 0;
      // 
      // tbpDraw
      // 
      //this.tbpDraw.Controls.Add(this.FUCDraw);
      this.tbpDraw.Location = new System.Drawing.Point(4, 4);
      this.tbpDraw.Name = "tbpDraw";
      this.tbpDraw.Padding = new System.Windows.Forms.Padding(3);
      this.tbpDraw.Size = new System.Drawing.Size(495, 659);
      this.tbpDraw.TabIndex = 0;
      this.tbpDraw.Text = "Draw";
      this.tbpDraw.UseVisualStyleBackColor = true;
      // 
      // FUCDraw
      // 
      //this.FUCDraw.Dock = System.Windows.Forms.DockStyle.Fill;
      //this.FUCDraw.Location = new System.Drawing.Point(3, 3);
      //this.FUCDraw.Name = "FUCDraw";
      //this.FUCDraw.Size = new System.Drawing.Size(489, 653);
      //this.FUCDraw.TabIndex = 5;
      // 
      // splitter1
      // 
      this.splitter1.Location = new System.Drawing.Point(414, 0);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(3, 685);
      this.splitter1.TabIndex = 7;
      this.splitter1.TabStop = false;
      // 
      // FUCTextEditor
      // 
      this.FUCTextEditor.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCTextEditor.FileName = "";
      this.FUCTextEditor.IsReadOnly = false;
      this.FUCTextEditor.LoadFileExtension = "gcode.txt";
      this.FUCTextEditor.LoadFileFilter = "GCode-Files (*.gcode.txt)|*.gcode.txt|Textfiles (*.txt)|*.txt|All files (*.*)|*.*" +
    "";
      this.FUCTextEditor.LoadFileName = "";
      this.FUCTextEditor.Location = new System.Drawing.Point(0, 0);
      this.FUCTextEditor.Name = "FUCTextEditor";
      this.FUCTextEditor.SaveFileExtension = "gcode.txt";
      this.FUCTextEditor.SaveFileFilter = "GCode-Files (*.gcode.txt)|*.gcode.txt|Textfiles (*.txt)|*.txt|All files (*.*)|*.*" +
    "";
      this.FUCTextEditor.SaveFileName = "";
      this.FUCTextEditor.Size = new System.Drawing.Size(414, 685);
      this.FUCTextEditor.TabIndex = 6;
      this.FUCTextEditor.WorkingDirectory = "";
      // 
      // FormCncTranslator
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(920, 853);
      this.Controls.Add(this.pnlMain);
      this.Controls.Add(this.cbxAutomate);
      this.Controls.Add(this.splProtocol);
      this.Controls.Add(this.pnlProtocol);
      this.Controls.Add(this.mstMain);
      this.Name = "FormCncTranslator";
      this.Text = "Form1";
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.pnlProtocol.ResumeLayout(false);
      this.pnlMain.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudCommandCount)).EndInit();
      this.tbcView.ResumeLayout(false);
      this.tbpTerminal.ResumeLayout(false);
      this.tbpDraw.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.HelpProvider FHelpProvider;
    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem mitSystem;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
    private System.Windows.Forms.ToolStripMenuItem mitSQuit;
    private System.Windows.Forms.ToolStripMenuItem mitConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCDefault;
    private System.Windows.Forms.ToolStripMenuItem mitCResetToDefaultConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveAutomaticAtProgramEnd;
    private System.Windows.Forms.ToolStripMenuItem mitCStartup;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPEditParameter;
    private System.Windows.Forms.ToolStripMenuItem mitPClearProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPCopyToClipboard;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem diagnosticToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitPLoadDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitPSaveDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitSendDiagnosticEmail;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
    private System.Windows.Forms.ToolStripMenuItem mitAction;
    private System.Windows.Forms.ToolStripMenuItem mitAOpenClose;
    private System.Windows.Forms.ToolStripMenuItem mitAStartStop;
    private System.Windows.Forms.ToolStripMenuItem mitAProperties;
    private System.Windows.Forms.ToolStripMenuItem mitHelp;
    private System.Windows.Forms.ToolStripMenuItem mitHContents;
    private System.Windows.Forms.ToolStripMenuItem mitHTopic;
    private System.Windows.Forms.ToolStripMenuItem mitHSearch;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterApplicationProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterDeviceProductKey;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
    private System.Windows.Forms.ToolStripMenuItem mitHAbout;
    private System.Windows.Forms.SaveFileDialog DialogSaveInitdata;
    private System.Windows.Forms.Timer tmrStartup;
    private System.Windows.Forms.OpenFileDialog DialogLoadInitdata;
    private System.Windows.Forms.Panel pnlProtocol;
    private System.Windows.Forms.Splitter splProtocol;
    private System.Windows.Forms.CheckBox cbxAutomate;
    private UCSerialNumber.CUCSerialNumber FUCSerialNumber;
    private System.Windows.Forms.Panel pnlMain;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.CheckBox cbxReset;
    private System.Windows.Forms.NumericUpDown nudCommandCount;
    private System.Windows.Forms.Button btnExecuteSequence;
    private System.Windows.Forms.Button btnExecuteProgram;
    private System.Windows.Forms.Button btnTranslateProgram;
    private System.Windows.Forms.TabControl tbcView;
    private System.Windows.Forms.TabPage tbpTerminal;
    private UCTerminal.CUCTerminalUdp FUCTerminalUdpSimulator;
    private System.Windows.Forms.TabPage tbpDraw;
    private System.Windows.Forms.Splitter splitter1;
    private UCTextEditor.CUCEditTextEditor FUCTextEditor;
    private UCTerminal.CUCTerminalUdp FUCTerminalUdpController;
    private System.Windows.Forms.Splitter splitter2;
  }
}

