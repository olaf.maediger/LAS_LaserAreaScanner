using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Drawing;
using System.IO;

namespace Initdata
{
  public class CInitdata : CInitdataBase
  {
    // 
    //-------------------------------
    //  Section - Constant
    //-------------------------------
    //
    public const String Initfile = "Initdata.xml";
    public const String NodeBase = "Configuration";
		//
    public const String NAME_MAINWINDOW = "MainWindow";
    public const String NAME_TITLE = "Title";
    public const String NAME_WINDOWSTATE = "WindowState";
    public const String NAME_VISIBLE = "Visible"; // not for MainWindow!!!
    public const String NAME_ENABLE = "Enable";
    public const String NAME_LEFT = "Left";
    public const String NAME_TOP = "Top";
    public const String NAME_WIDTH = "Width";
    public const String NAME_HEIGHT = "Height";
    public const String NAME_ABOUTDIALOG = "AboutDialog";
    public const String NAME_SEPARATOR = "Separator";
    // 
    //-------------------------------
    //  Section - Member
    //-------------------------------
    //
		private DialogAbout FDialogAbout = null;
		// 
    //-------------------------------
    //  Section - Constructor
	  //-------------------------------
    //
    public CInitdata()
    {
			FDialogAbout = new DialogAbout();
		}
    //
    //---------------------------------------------------------
    //  Section - Static Helper
    //---------------------------------------------------------
    //
    public static void Init()
    {
      CDefines.Init();
    }

    public static void BuildPresentableMainForm(ref Int32 leftactual, Int32 leftinit,
                                                ref Int32 topactual, Int32 topinit,
                                                ref Int32 widthactual, Int32 widthinit,
                                                ref Int32 heightactual, Int32 heightinit)
    {
      Screen[] AS = Screen.AllScreens;
      Boolean[] Results = new Boolean[AS.Length];
      for (Int32 SI = 0; SI < AS.Length; SI++)
      {
        Screen S = AS[SI];
        Results[SI] = ((S.Bounds.Left <= leftactual) && (leftactual <= S.Bounds.Right));
        Results[SI] &= ((S.Bounds.Top <= topactual) && (topactual <= S.Bounds.Bottom));
        Results[SI] &= (widthactual <= S.Bounds.Width);
        Results[SI] &= (heightactual <= S.Bounds.Height);
      }
      Boolean IsInOneScreen = false;
      foreach (Boolean Result in Results)
      {
        IsInOneScreen |= Result;
      }
      if (!IsInOneScreen)
      {
        leftactual = leftinit;
        topactual = topinit;
        widthactual = widthinit;
        heightactual = heightinit;
      }
    }


    public static Boolean LoadWindowStateBorder(CInitdataReader initdatareader,
                                                Form form,
                                                FormWindowState initwindowstate,
                                                Int32 initleft, Int32 inittop,
                                                Int32 initwidth, Int32 initheight)
    {
      String SValue = CInitdata.WindowStateToText(initwindowstate);
      Boolean Result = initdatareader.ReadEnumeration(CInitdata.NAME_WINDOWSTATE,
                                                      out SValue, SValue);
      FormWindowState WindowStatePreset = CInitdata.TextToWindowState(SValue);
      Int32 IL, IT, IW, IH;
      Result &= initdatareader.ReadInt32(CInitdata.NAME_LEFT, out IL, initleft);
      Result &= initdatareader.ReadInt32(CInitdata.NAME_TOP, out IT, inittop);
      Result &= initdatareader.ReadInt32(CInitdata.NAME_WIDTH, out IW, initwidth);
      Result &= initdatareader.ReadInt32(CInitdata.NAME_HEIGHT, out IH, initheight);
      switch (WindowStatePreset)
      {
        case FormWindowState.Normal:
          form.WindowState = FormWindowState.Normal;
          CInitdata.BuildPresentableMainForm(ref IL, initleft, ref IT, inittop,
                                             ref IW, initwidth, ref IH, initheight);
          form.Left = IL;
          form.Top = IT;
          form.Width = IW;
          form.Height = IH;
          break;
        case FormWindowState.Maximized:
          form.WindowState = FormWindowState.Normal;
          CInitdata.BuildPresentableMainForm(ref IL, initleft, ref IT, inittop,
                                             ref IW, initwidth, ref IH, initheight);
          form.Left = IL;
          form.Top = IT;
          form.Width = IW;
          form.Height = IH;
          form.WindowState = WindowStatePreset;
          break;
        case FormWindowState.Minimized:
          form.WindowState = FormWindowState.Normal;
          CInitdata.BuildPresentableMainForm(ref IL, initleft, ref IT, inittop,
                                             ref IW, initwidth, ref IH, initheight);
          form.Left = IL;
          form.Top = IT;
          form.Width = IW;
          form.Height = IH;
          form.WindowState = WindowStatePreset;
          break;
        default:
          Result = false;
          break;
      }
      return Result;
    }

    public static Boolean SaveWindowStateBorder(CInitdataWriter initdatawriter,
                                                Form form)
    {
      Boolean Result = true;
      Result &= initdatawriter.WriteEnumeration(CInitdata.NAME_WINDOWSTATE,
                                                CInitdata.WindowStateToText(form.WindowState));
      switch (form.WindowState)
      {
        case FormWindowState.Minimized:
          form.WindowState = FormWindowState.Normal;
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_LEFT, form.Left);
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_TOP, form.Top);
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_WIDTH, form.Width);
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_HEIGHT, form.Height);
          form.WindowState = FormWindowState.Minimized;
          break;
        case FormWindowState.Maximized:
          form.WindowState = FormWindowState.Normal;
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_LEFT, form.Left);
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_TOP, form.Top);
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_WIDTH, form.Width);
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_HEIGHT, form.Height);
          form.WindowState = FormWindowState.Maximized;
          break;
        case FormWindowState.Normal:
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_LEFT, form.Left);
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_TOP, form.Top);
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_WIDTH, form.Width);
          Result &= initdatawriter.WriteInt32(CInitdata.NAME_HEIGHT, form.Height);
          break;
        default:
          Result = false;
          break;
      }
      return Result;
    }

    public static FormWindowState TextToWindowState(String value)
    {
      if (value == FormWindowState.Minimized.ToString())
      {
        return FormWindowState.Minimized;
      }
      if (value == FormWindowState.Maximized.ToString())
      {
        return FormWindowState.Maximized;
      }
      return FormWindowState.Normal;
    }
    public static String WindowStateToText(FormWindowState value)
    {
      return value.ToString();
    }

    public static Boolean BuildInitdataDirectory(String subdirectorytarget,
                                                 String filenamepreset,
                                                 String fileextensionpreset,
                                                 out String initdatadirectory,
                                                 out String initdatafilename)
    {
      initdatadirectory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
      initdatadirectory = Path.Combine(initdatadirectory, subdirectorytarget);
      initdatafilename = filenamepreset + fileextensionpreset;
      try
      {
        // Initdata
        // Test if "All User\...company\Project" is present
        if (!Directory.Exists(initdatadirectory))
        { // not -> create
          Directory.CreateDirectory(initdatadirectory);
        }
        // Test All User\...company\Project\project.ini.xml
        String InitdataEntry = Path.Combine(initdatadirectory, initdatafilename);
        if (!File.Exists(InitdataEntry))
        {
          // Try to copy default project.ini.xml from local _Exe
          if (File.Exists(initdatafilename))
          { // Copy default project.ini.xml -> All User\...company\Project\project.ini.xml
            File.Copy(initdatafilename, InitdataEntry);
            return true;
          }
          return false;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean DeleteInitdataFile(String subdirectorytarget,
                                             String filenamepreset,
                                             String fileextensionpreset)
    {
      String InitdataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
      InitdataDirectory = Path.Combine(InitdataDirectory, subdirectorytarget);
      String InitdataFilename = filenamepreset + fileextensionpreset;
      try
      {
        // Initdata
        // Test if "All User\...company\Project" is present
        if (Directory.Exists(InitdataDirectory))
        { // Test All User\...company\Project\project.ini.xml
          String InitdataEntry = Path.Combine(InitdataDirectory, InitdataFilename);
          if (File.Exists(InitdataEntry))
          {
            File.Delete(InitdataEntry);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-------------------------------
    //  Segment - Conversions
    //-------------------------------
    //
    static public String HexadecimalText(UInt32 value,
                                         Byte size)
    {
      String F = "{0:X" + size.ToString() + "}";
      return String.Format(F, value);
    }

    static public UInt32 TextHexadecimal(String value)
    {
      return Convert.ToUInt32(value, 16);
    }
    // 
    //-------------------------------
    //  Section - Dialog About
    //-------------------------------
    //
    public void SetApplicationData(String application,
                                   String version,
                                   String date)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetApplicationData(application,
                                        version,
                                        date);
      }
    }

    public void SetDescriptionData(String description,
                                   String point1,
                                   String point2,
                                   String point3)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetDescriptionData(description,
                                        point1,
                                        point2,
                                        point3);
      }
    }

    public void SetCompanySourceData(String organisationtop,
                                     String organisationbottom,
                                     String street,
                                     String city)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetCompanySourceData(organisationtop,
                                          organisationbottom,
                                          street,
                                          city);
      }
    }

    public void SetCompanyTargetData(String organisationtop,
                                     String organisationbottom,
                                     String street,
                                     String city)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetCompanyTargetData(organisationtop,
                                          organisationbottom,
                                          street,
                                          city);
      }
    }

    public void SetIcon(Icon icon)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetIcon(icon);
      }
    }

    public void SetProductKey(String productkey)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetProductKey(productkey);
      }
    }

    public DialogResult ShowModalDialogAbout()
    {
      return FDialogAbout.ShowDialog();
    }

	}
}
