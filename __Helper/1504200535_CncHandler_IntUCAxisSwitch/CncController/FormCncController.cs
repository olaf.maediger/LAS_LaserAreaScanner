﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
//
using UdpInterface;
//
namespace CncController
{
  public partial class FormCncController : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;
    private CUdpReceiver FUdpReceiver;
    private CUdpTransmitter FUdpTransmitter;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FUCTerminalUdp.SetOnSendText(UCTerminalUdpOnSendText);
      FUCTerminalUdp.SetOnPortOpened(UCTerminalUdpOnPortOpened);
      FUCTerminalUdp.SetOnPortClosed(UCTerminalUdpOnPortClosed);
      //
      FUdpTransmitter = new CUdpTransmitter();
      FUdpTransmitter.SetNotifier(FUCNotifier);
      //
      FUdpReceiver = new CUdpReceiver();
      FUdpReceiver.SetNotifier(FUCNotifier);
      FUdpReceiver.SetOnTextReceived(UdpReceiverOnTextReceived);
      //
      tmrStartup.Interval = 1000;
      //
      tmrStartup.Start();

      FRandom = new Random();
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      // ... Int32 IValue;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      FUCTerminalUdp.PresetText("From Main: Hello World!");
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUC...
    //------------------------------------------------------------------------
    // 
    // NC private void UCPeakDetectionOnProcessStateChanged(EProcessState statepreset, 
    // NC                                                   EProcessState stateactual)
    // NC {
    // NC }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        default:
          return false;
      }
    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }
    //
    //--------------------------------------------------
    //  Segment - Helper - 
    //--------------------------------------------------
    //
    private void StartProcessCncSimulator()
    {
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCTerminal
    //------------------------------------------------------------------------
    //
    private Boolean UCTerminalUdpOnPortOpened()
    {
      Boolean Result = true;
      Result &= FUdpTransmitter.Open("127.0.0.1", CUdpInterface.PORT_UDP_04);
      Result &= FUdpReceiver.Open("127.0.0.1", CUdpInterface.PORT_UDP_03, "");
      return Result;
    }

    private Boolean UCTerminalUdpOnPortClosed()
    {
      Boolean Result = true;
      Result &= FUdpTransmitter.Close();
      Result &= FUdpReceiver.Close();
      return Result;
    }

    private Boolean UCTerminalUdpOnSendText(String data)
    { // UCTerminal -> Send-Button -> here -> UdpTransmitter.Send & UCTerminal.Add
      Boolean Result = false;
      if (FUdpTransmitter.SendText(data))
      {
        Result = FUCTerminalUdp.AddTextTransmitted(data);
      }
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UdpTransmitter
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback - UdpReceiver
    //------------------------------------------------------------------------
    //
    private delegate void CBUdpReceiverOnTextReceived(String data);
    private void UdpReceiverOnTextReceived(String data)
    {
      if (this.InvokeRequired)
      {
        CBUdpReceiverOnTextReceived CB = new CBUdpReceiverOnTextReceived(UdpReceiverOnTextReceived);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCTerminalUdp.AddTextReceived(data);
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Menu
    //--------------------------------------------------------------------------
    //


  }
}

