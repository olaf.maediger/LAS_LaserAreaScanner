﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCRotator
{
  public class CPointInt32
  {
    private Int32 FX;
    private Int32 FY;

    public CPointInt32(Int32 x, Int32 y)
    {
      FX = x;
      FY = y;
    }

    public Int32 X
    {
      get { return FX; }
      set { FX = value; }
    }
    public Int32 Y
    {
      get { return FY; }
      set { FY = value; }
    }

  }
}
