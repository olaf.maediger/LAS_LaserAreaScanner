﻿namespace UCRotator
{
  partial class CUCEditDouble
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblUnit = new System.Windows.Forms.Label();
      this.nudValue = new System.Windows.Forms.NumericUpDown();
      this.lblHeader = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudValue)).BeginInit();
      this.SuspendLayout();
      // 
      // lblUnit
      // 
      this.lblUnit.BackColor = System.Drawing.SystemColors.Info;
      this.lblUnit.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblUnit.Location = new System.Drawing.Point(208, 0);
      this.lblUnit.Name = "lblUnit";
      this.lblUnit.Size = new System.Drawing.Size(47, 21);
      this.lblUnit.TabIndex = 5;
      this.lblUnit.Text = "<unit>";
      this.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudValue
      // 
      this.nudValue.DecimalPlaces = 3;
      this.nudValue.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudValue.Location = new System.Drawing.Point(138, 0);
      this.nudValue.Maximum = new decimal(new int[] {
            1874919424,
            2328306,
            0,
            0});
      this.nudValue.Minimum = new decimal(new int[] {
            1874919424,
            2328306,
            0,
            -2147483648});
      this.nudValue.Name = "nudValue";
      this.nudValue.Size = new System.Drawing.Size(70, 20);
      this.nudValue.TabIndex = 4;
      this.nudValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudValue.ValueChanged += new System.EventHandler(this.nudValue_ValueChanged);
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(138, 21);
      this.lblHeader.TabIndex = 3;
      this.lblHeader.Text = "<header>";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCEditDouble
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.lblUnit);
      this.Controls.Add(this.nudValue);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCEditDouble";
      this.Size = new System.Drawing.Size(255, 21);
      ((System.ComponentModel.ISupportInitialize)(this.nudValue)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblUnit;
    private System.Windows.Forms.NumericUpDown nudValue;
    private System.Windows.Forms.Label lblHeader;

  }
}
