﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCRotator
{
  public partial class CUCRotator : UserControl
  {
    protected const Double INIT_ANGLE = 0.0;

    private Double FAngle;
    private Double FCosinus;
    private Double FSinus;

    public CUCRotator()
    {
      InitializeComponent();
      //
      SetRotation(INIT_ANGLE);
    }


    protected void SetRotation(Double angle)
    {
      FAngle = angle;
      FCosinus = Math.Cos(FAngle);
      FSinus = Math.Sin(FAngle);
    }
    protected Int32 RotateX(Int32 x, Int32 y)
    {
      return (Int32)Math.Round(x * FCosinus - y * FSinus);
    }
    protected Int32 RotateY(Int32 x, Int32 y)
    {
      return (Int32)Math.Round(x * FSinus + y * FCosinus);
    }

    protected Point[] Rotate(Point[] points, Double angle)
    {
      Point[] Points = new Point[points.Length];
      SetRotation(angle);
      for (Int32 PI = 0; PI < points.Length; PI++)
      {
        Points[PI].X = RotateX(points[PI].X, points[PI].Y);
        Points[PI].Y = RotateY(points[PI].X, points[PI].Y);
      }
      return Points;
    }

    protected Point[] Translate(Point[] points, Int32 dx, Int32 dy)
    {
      Point[] Points = new Point[points.Length];
      for (Int32 PI = 0; PI < points.Length; PI++)
      {
        Points[PI].X = points[PI].X - dx;
        Points[PI].Y = points[PI].Y - dy;
      }
      return Points;
    }

  }
}
