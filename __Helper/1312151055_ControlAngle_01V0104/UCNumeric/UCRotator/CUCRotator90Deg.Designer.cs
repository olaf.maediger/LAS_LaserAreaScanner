﻿namespace UCRotator
{
  partial class CUCRotator90Deg
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.FUCEditDoublePreset = new UCRotator.CUCEditDouble();
      this.FUCEditDoubleActual = new UCRotator.CUCEditDouble();
      this.lblHeader = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // FUCEditDoublePreset
      // 
      this.FUCEditDoublePreset.BackColor = System.Drawing.SystemColors.Info;
      this.FUCEditDoublePreset.DecimalPlaces = ((uint)(6u));
      this.FUCEditDoublePreset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCEditDoublePreset.Header = "Preset";
      this.FUCEditDoublePreset.HeaderWidth = 72;
      this.FUCEditDoublePreset.Location = new System.Drawing.Point(15, 59);
      this.FUCEditDoublePreset.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
      this.FUCEditDoublePreset.Name = "FUCEditDoublePreset";
      this.FUCEditDoublePreset.Size = new System.Drawing.Size(233, 21);
      this.FUCEditDoublePreset.TabIndex = 0;
      this.FUCEditDoublePreset.Unit = "deg";
      this.FUCEditDoublePreset.Value = 0D;
      this.FUCEditDoublePreset.ValueDelta = 1D;
      this.FUCEditDoublePreset.ValueMaximum = 1E+16D;
      this.FUCEditDoublePreset.ValueMinimum = -1E+16D;
      this.FUCEditDoublePreset.ValueWidth = 120;
      // 
      // FUCEditDoubleActual
      // 
      this.FUCEditDoubleActual.BackColor = System.Drawing.SystemColors.Info;
      this.FUCEditDoubleActual.DecimalPlaces = ((uint)(6u));
      this.FUCEditDoubleActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCEditDoubleActual.Header = "Actual";
      this.FUCEditDoubleActual.HeaderWidth = 72;
      this.FUCEditDoubleActual.Location = new System.Drawing.Point(15, 86);
      this.FUCEditDoubleActual.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
      this.FUCEditDoubleActual.Name = "FUCEditDoubleActual";
      this.FUCEditDoubleActual.Size = new System.Drawing.Size(233, 21);
      this.FUCEditDoubleActual.TabIndex = 1;
      this.FUCEditDoubleActual.Unit = "deg";
      this.FUCEditDoubleActual.Value = 0D;
      this.FUCEditDoubleActual.ValueDelta = 1D;
      this.FUCEditDoubleActual.ValueMaximum = 1E+16D;
      this.FUCEditDoubleActual.ValueMinimum = -1E+16D;
      this.FUCEditDoubleActual.ValueWidth = 120;
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.Color.Transparent;
      this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(326, 34);
      this.lblHeader.TabIndex = 2;
      this.lblHeader.Text = "Angle";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCRotator90Deg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.FUCEditDoubleActual);
      this.Controls.Add(this.FUCEditDoublePreset);
      this.MinimumSize = new System.Drawing.Size(200, 200);
      this.Name = "CUCRotator90Deg";
      this.Size = new System.Drawing.Size(326, 250);
      this.ResumeLayout(false);

    }

    #endregion

    private CUCEditDouble FUCEditDoublePreset;
    private CUCEditDouble FUCEditDoubleActual;
    private System.Windows.Forms.Label lblHeader;
  }
}
