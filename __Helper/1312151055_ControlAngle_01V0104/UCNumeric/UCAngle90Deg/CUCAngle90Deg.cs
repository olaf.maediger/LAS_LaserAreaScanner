﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;
//
namespace UCAngle90Deg
{
  //
  //----------------------------------------------------------
  //  Segment - Type
  //----------------------------------------------------------
  //
  public delegate void DOnValueChanged(Double valueactual, 
                                       Double valuepreset,
                                       Double valuedelta);

  public partial class CUCAngle90Deg : UserControl
  {
    //
    //----------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------
    //
    private Double FAngle;
    private Double FDelta;
    private String FFormat;
    private DOnValueChanged FOnValueChanged;
    //
    //----------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------
    //
    public CUCAngle90Deg()
    {
      InitializeComponent();
      //
      CInitdata.Init();
      //
      FFormat = "{0:0.0}";
      SetAngle(0.0);
      SetDelta(1.0);
    }
    //
    //----------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------
    //
    public void SetOnValueChanged(DOnValueChanged value)
    {
      FOnValueChanged = value;
    }

    public Double GetAngle()
    {
      return FAngle;
    }
    private delegate void CBSetAngle(Double value);
    public void SetAngle(Double value)
    {
      if (this.InvokeRequired)
      {
        CBSetAngle CB = new CBSetAngle(SetAngle);
        Invoke(CB, new object[] { value });
      }
      else
      {
        Double AnglePreset = Math.Min(90.0, Math.Max(0.0, value));
        if (AnglePreset != FAngle)
        {
          trbAngle.Value = (Int32)Math.Round(AnglePreset);
          lblAngle.Text = String.Format(FFormat, AnglePreset);
          if (FOnValueChanged is DOnValueChanged)
          {
            FOnValueChanged(AnglePreset, Angle, FDelta);
          }
          FAngle = AnglePreset;
        }
      }
    }
    public Double Angle
    {
      get { return GetAngle(); }
      set { SetAngle(value); }
    }


    public Double GetDelta()
    {
      return FDelta;
    }
    public void SetDelta(Double value)
    {
      FDelta = Math.Min(90.0, Math.Max(0.001, value));
    }
    public Double Delta
    {
      get { return GetDelta(); }
      set { SetDelta(value); }
    }
    //
    //----------------------------------------------------------
    //  Segment - Event
    //----------------------------------------------------------
    //
    private void trbAngle_ValueChanged(object sender, EventArgs e)
    {
      SetAngle(trbAngle.Value);
    }
    //
    //----------------------------------------------------------
    //  Segment - KeyUp
    //----------------------------------------------------------
    //
    private void btnStepUp_Click(object sender, EventArgs e)
    {
      tmrKeyDown.Enabled = false;
      tmrKeyUp.Enabled = !tmrKeyUp.Enabled;
      if (tmrKeyUp.Enabled)
      {
        SetAngle(FAngle + FDelta);
      }
    }

    private void tmrKeyUp_Tick(object sender, EventArgs e)
    {
      if (Angle < 90)
      {
        SetAngle(Angle + Delta);
      }
      else
      {
        tmrKeyUp.Enabled = false;
      }
    }
    //
    //----------------------------------------------------------
    //  Segment - KeyDown
    //----------------------------------------------------------
    //
    private void btnStepDown_Click(object sender, EventArgs e)
    {
      tmrKeyUp.Enabled = false;
      tmrKeyDown.Enabled = !tmrKeyDown.Enabled;
      if (tmrKeyDown.Enabled)
      {
        SetAngle(Angle - Delta);
      }
    }

    private void tmrKeyDown_Tick(object sender, EventArgs e)
    {
      if (0 < Angle)
      {
        SetAngle(Angle - Delta);
      }
      else
      {
        tmrKeyDown.Enabled = false;
      }
    }




  }
}
