﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
//
using Initdata;
using UCNotifier;
using UCSerialNumber;
using UCRotator;
//
namespace ControlAngle
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  {
    //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    // NC private CUCRotator90Deg FUCRotator90Deg;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //
      // NC FUCRotator90Deg = new CUCRotator90Deg();
      // NC tbpRotator90Deg.Controls.Add(FUCRotator90Deg);
      FUCRotator90Deg.Dock = DockStyle.Fill;
      FUCRotator90Deg.SetOnAnglePresetChanged(UCRotator90DegOnAnglePresetChanged);
      FUCRotator90Deg.SetOnAngleActualChanged(UCRotator90DegOnAngleActualChanged);
      //
      // 
      tmrStartup.Interval = 1500;
      //tmrStartup.Start();
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - 
    //------------------------------------------------------------------------
    // 
    private void UCRotator90DegOnAngleActualChanged(Double angle)
    {
    }

    private void UCRotator90DegOnAnglePresetChanged(Double angle)
    {
    }
    //
    //----------------------------------------
    //  Section - Startup
    //----------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          return true;
        case 1:
          FUCRotator90Deg.SetAnglePreset(90);
          FUCRotator90Deg.SetAngleActual(0);
          return true;
        case 2:
          //FUCRotator90Deg.SetAnglePreset(10);
          FUCRotator90Deg.SetAngleActual(10);
          return true;
        case 3:
          //FUCRotator90Deg.SetAnglePreset(20);
          FUCRotator90Deg.SetAngleActual(20);
          return true;
        case 4:
          //FUCRotator90Deg.SetAnglePreset(30);
          FUCRotator90Deg.SetAngleActual(30);
          return true;
        case 5:
          //FUCRotator90Deg.SetAnglePreset(40);
          FUCRotator90Deg.SetAngleActual(40);
          return true;
        case 6:
          //FUCRotator90Deg.SetAnglePreset(50);
          FUCRotator90Deg.SetAngleActual(50);
          return true;
        case 7:
          //FUCRotator90Deg.SetAnglePreset(60);
          FUCRotator90Deg.SetAngleActual(60);
          return true;
        case 8:
          //FUCRotator90Deg.SetAnglePreset(70);
          FUCRotator90Deg.SetAngleActual(70);
          return true;
        case 9:
          //FUCRotator90Deg.SetAnglePreset(80);
          FUCRotator90Deg.SetAngleActual(80);
          return true;
        case 10:
          //FUCRotator90Deg.SetAnglePreset(90);
          FUCRotator90Deg.SetAngleActual(90);
          return true;
        case 11:
          FUCRotator90Deg.SetAnglePreset(0);
          FUCRotator90Deg.SetAngleActual(80);
          return true;
        case 12:
          //FUCRotator90Deg.SetAnglePreset(70);
          FUCRotator90Deg.SetAngleActual(70);
          return true;
        case 13:
          //FUCRotator90Deg.SetAnglePreset(60);
          FUCRotator90Deg.SetAngleActual(60);
          return true;
        case 14:
          //FUCRotator90Deg.SetAnglePreset(50);
          FUCRotator90Deg.SetAngleActual(50);
          return true;
        case 15:
          //FUCRotator90Deg.SetAnglePreset(40);
          FUCRotator90Deg.SetAngleActual(40);
          return true;
        case 16:
          //FUCRotator90Deg.SetAnglePreset(30);
          FUCRotator90Deg.SetAngleActual(30);
          return true;
        case 17:
          //FUCRotator90Deg.SetAnglePreset(20);
          FUCRotator90Deg.SetAngleActual(20);
          return true;
        case 18:
          //FUCRotator90Deg.SetAnglePreset(10);
          FUCRotator90Deg.SetAngleActual(10);
          return true;
        case 19:
          //FUCRotator90Deg.SetAnglePreset(0);
          FUCRotator90Deg.SetAngleActual(0);
          FStartupState = 0;
          return true;
        default:
          return false;
      }

    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    //
    private void cbxAuto_CheckedChanged(object sender, EventArgs e)
    {
      FStartupState = 0;
      tmrStartup.Enabled = cbxAuto.Checked;
    }

  }
}
