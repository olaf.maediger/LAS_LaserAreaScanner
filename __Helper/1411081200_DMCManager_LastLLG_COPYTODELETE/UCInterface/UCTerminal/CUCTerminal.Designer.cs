﻿namespace UCTerminal
{
  partial class CUCTerminal
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlInput = new System.Windows.Forms.Panel();
      this.cbxSendLine = new System.Windows.Forms.ComboBox();
      this.cmsTxData = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mitSaveText = new System.Windows.Forms.ToolStripMenuItem();
      this.mitClearAllLines = new System.Windows.Forms.ToolStripMenuItem();
      this.mitLoadText = new System.Windows.Forms.ToolStripMenuItem();
      this.btnSend = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.FUCMultiColorList = new UCMultiColorList.CUCMultiColorList();
      this.DialogLoadCommandText = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveCommandText = new System.Windows.Forms.SaveFileDialog();
      this.pnlInput.SuspendLayout();
      this.cmsTxData.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlInput
      // 
      this.pnlInput.Controls.Add(this.cbxSendLine);
      this.pnlInput.Controls.Add(this.btnSend);
      this.pnlInput.Controls.Add(this.label1);
      this.pnlInput.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlInput.Location = new System.Drawing.Point(0, 228);
      this.pnlInput.Name = "pnlInput";
      this.pnlInput.Size = new System.Drawing.Size(291, 21);
      this.pnlInput.TabIndex = 0;
      // 
      // cbxSendLine
      // 
      this.cbxSendLine.BackColor = System.Drawing.SystemColors.Info;
      this.cbxSendLine.ContextMenuStrip = this.cmsTxData;
      this.cbxSendLine.Dock = System.Windows.Forms.DockStyle.Fill;
      this.cbxSendLine.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbxSendLine.ForeColor = System.Drawing.Color.Blue;
      this.cbxSendLine.FormattingEnabled = true;
      this.cbxSendLine.Items.AddRange(new object[] {
            "GHV",
            "GSV",
            "H"});
      this.cbxSendLine.Location = new System.Drawing.Point(56, 0);
      this.cbxSendLine.Name = "cbxSendLine";
      this.cbxSendLine.Size = new System.Drawing.Size(179, 23);
      this.cbxSendLine.TabIndex = 4;
      this.cbxSendLine.Text = "H";
      this.cbxSendLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbxSendLine_KeyDown);
      // 
      // cmsTxData
      // 
      this.cmsTxData.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSaveText,
            this.mitClearAllLines,
            this.mitLoadText});
      this.cmsTxData.Name = "cmsMultiColorList";
      this.cmsTxData.Size = new System.Drawing.Size(147, 70);
      // 
      // mitSaveText
      // 
      this.mitSaveText.Name = "mitSaveText";
      this.mitSaveText.Size = new System.Drawing.Size(146, 22);
      this.mitSaveText.Text = "Save Text";
      this.mitSaveText.Click += new System.EventHandler(this.mitSaveText_Click);
      // 
      // mitClearAllLines
      // 
      this.mitClearAllLines.Name = "mitClearAllLines";
      this.mitClearAllLines.Size = new System.Drawing.Size(146, 22);
      this.mitClearAllLines.Text = "Clear all Lines";
      this.mitClearAllLines.Click += new System.EventHandler(this.mitClearAllLines_Click);
      // 
      // mitLoadText
      // 
      this.mitLoadText.Name = "mitLoadText";
      this.mitLoadText.Size = new System.Drawing.Size(146, 22);
      this.mitLoadText.Text = "Load Text";
      this.mitLoadText.Click += new System.EventHandler(this.mitLoadText_Click);
      // 
      // btnSend
      // 
      this.btnSend.ContextMenuStrip = this.cmsTxData;
      this.btnSend.Dock = System.Windows.Forms.DockStyle.Right;
      this.btnSend.Location = new System.Drawing.Point(235, 0);
      this.btnSend.Name = "btnSend";
      this.btnSend.Size = new System.Drawing.Size(56, 21);
      this.btnSend.TabIndex = 3;
      this.btnSend.Text = "Send";
      this.btnSend.UseVisualStyleBackColor = true;
      this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
      // 
      // label1
      // 
      this.label1.ContextMenuStrip = this.cmsTxData;
      this.label1.Dock = System.Windows.Forms.DockStyle.Left;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(56, 21);
      this.label1.TabIndex = 1;
      this.label1.Text = "TXD";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FUCMultiColorList
      // 
      this.FUCMultiColorList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.FUCMultiColorList.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCMultiColorList.Location = new System.Drawing.Point(0, 0);
      this.FUCMultiColorList.Name = "FUCMultiColorList";
      this.FUCMultiColorList.Size = new System.Drawing.Size(291, 228);
      this.FUCMultiColorList.TabIndex = 3;
      // 
      // DialogLoadCommandText
      // 
      this.DialogLoadCommandText.DefaultExt = "txt";
      this.DialogLoadCommandText.FileName = "TerminalCommands";
      this.DialogLoadCommandText.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogLoadCommandText.Title = "Load Terminal Commands";
      // 
      // DialogSaveCommandText
      // 
      this.DialogSaveCommandText.DefaultExt = "txt";
      this.DialogSaveCommandText.FileName = "TerminalCommands";
      this.DialogSaveCommandText.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogSaveCommandText.Title = "Save Terminal Commands";
      // 
      // CUCTerminal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCMultiColorList);
      this.Controls.Add(this.pnlInput);
      this.Name = "CUCTerminal";
      this.Size = new System.Drawing.Size(291, 249);
      this.pnlInput.ResumeLayout(false);
      this.cmsTxData.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlInput;
    private System.Windows.Forms.Button btnSend;
    private System.Windows.Forms.Label label1;
    private UCMultiColorList.CUCMultiColorList FUCMultiColorList;
    private System.Windows.Forms.ComboBox cbxSendLine;
    private System.Windows.Forms.ContextMenuStrip cmsTxData;
    private System.Windows.Forms.ToolStripMenuItem mitSaveText;
    private System.Windows.Forms.ToolStripMenuItem mitClearAllLines;
    private System.Windows.Forms.ToolStripMenuItem mitLoadText;
    private System.Windows.Forms.OpenFileDialog DialogLoadCommandText;
    private System.Windows.Forms.SaveFileDialog DialogSaveCommandText;
  }
}
