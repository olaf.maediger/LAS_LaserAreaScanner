﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCNotifier
{
  public abstract class CTransformBaselist : List<CTransformBase>
  {

    public CTransformBase Find(String name,
                               Int32 productnumber,
                               String productkey)
    {
      foreach (CTransformBase TransformBase in this)
      {
        Boolean Result = (name == TransformBase.Name);
        Result &= (productnumber == TransformBase.ProductNumber);
        Result &= (productkey == TransformBase.ProductKey);
        if (Result)
        {
          return TransformBase;
        }
      }
      return null;
    }

    public CTransformBase FindByName(String name)
    {
      foreach (CTransformBase TransformBase in this)
      {
        if (name == TransformBase.Name)
        {
          return TransformBase;
        }
      }
      return null;
    }
    public CTransformBase FindByProductNumber(Int32 productnumber)
    {
      foreach (CTransformBase TransformBase in this)
      {
        if (productnumber == TransformBase.ProductNumber)
        {
          return TransformBase;
        }
      }
      return null;
    }
    public CTransformBase FindByProductKey(String productkey)
    {
      foreach (CTransformBase TransformBase in this)
      {
        if (productkey == TransformBase.ProductKey)
        {
          return TransformBase;
        }
      }
      return null;
    }

    public Boolean Delete(String name,
                          Int32 productnumber,
                          String productkey)
    {
      CTransformBase TransformBase = Find(name, productnumber, productkey);
      if (TransformBase is CTransformBase)
      {
        return Remove(TransformBase);
      }
      return false;
    }
    public Boolean DeleteByName(String name)
    {
      CTransformBase TransformBase = FindByName(name);
      if (TransformBase is CTransformBase)
      {
        return Remove(TransformBase);
      }
      return false;
    }
    public Boolean DeleteByProductNumber(String productnumber)
    {
      CTransformBase TransformBase = FindByName(productnumber);
      if (TransformBase is CTransformBase)
      {
        return Remove(TransformBase);
      }
      return false;
    }
    public Boolean DeleteByProductKey(String productkey)
    {
      CTransformBase TransformBase = FindByProductKey(productkey);
      if (TransformBase is CTransformBase)
      {
        return Remove(TransformBase);
      }
      return false;
    }



  }
}
