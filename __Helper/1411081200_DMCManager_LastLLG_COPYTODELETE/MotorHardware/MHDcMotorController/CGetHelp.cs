﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;

namespace HWDcMotorController
{
	class CGetHelp : CCommand
	{
		private const String INIT_NAME = "GetHelp";
    public const String COMMAND_TEXT = "H";

    public CGetHelp(CHardwareDevice hardwaredevice)
      : base(hardwaredevice, INIT_NAME, COMMAND_TEXT)
		{
      SetParent(this);
    }

    public override void OnSerialDataReceived(String data)
    {
      base.OnSerialDataReceived(data);
      if (18 <= FSerialStringList.Count)
      {
        TriggerWaitResponse();
        String Line = String.Format("{0}: {1}", Name, FSerialStringList[0]);
        Notifier.Write(CHWDcMotorController.HEADER_LIBRARY, Line);
      }
			/*/ Analyse Response to refresh Gui
      String[] Tokenlist = data.Split(' ');
      if (1 < Tokenlist.Length)
			{
        //Commandlist.Library.RefreshHelp();//Tokenlist);
			}*/
		}

	}
}


