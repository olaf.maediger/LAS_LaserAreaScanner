﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCStartStop
{
  public delegate void DOnStartStopChanged(Boolean active);

  public partial class CUCStartStop : UserControl
  {
    private const String INIT_TITLEPASSIVE = "Start";
    private const String INIT_TITLEACTIVE = "Stop";

    private Boolean FActive;
    private DOnStartStopChanged FOnStartStopChanged;
    private String FTitleActive;
    private String FTitlePassive;




    public void SetOnStartStopChanged(DOnStartStopChanged value)
    {
      FOnStartStopChanged = value;
    }

    public CUCStartStop()
    {
      InitializeComponent();
      Init();
    }

    private Color ColorPassive
    {
      get { return Color.FromArgb(0xFF, 0xFF, 0xE4, 0xB5); }
    }
    private Color ColorActive
    {
      get { return Color.FromArgb(0xFF, 0xFF, 0xE4, 0xE1); }
    }

    public String TitleActive
    {
      get { return FTitleActive; }
      set { FTitleActive = value; }
    }
    public String TitlePassive
    {
      get { return FTitlePassive; }
      set { FTitlePassive = value; }
    }

    private void Init()
    {
      FTitlePassive = INIT_TITLEPASSIVE;
      FTitleActive = INIT_TITLEACTIVE;
      Active = true;
      Active = false;
    }

    private void SetActive(Boolean value)
    {
      if (value != Active)
      {
        FActive = value;
        if (Active)
        {
          this.BackColor = ColorActive;
          btnStartStop.BackColor = ColorActive;
          btnStartStop.Text = TitleActive;
        }
        else
        {
          this.BackColor = ColorPassive;
          btnStartStop.BackColor = ColorPassive;
          btnStartStop.Text = TitlePassive;
        }
        if (FOnStartStopChanged is DOnStartStopChanged)
        {
          FOnStartStopChanged(Active);
        }
      }
    }
    public Boolean Active
    {
      get { return FActive; }
      set { SetActive(value); }
    }


    private void btnStartStop_Click(object sender, EventArgs e)
    {
      Active = !Active;
    }
  }
}
