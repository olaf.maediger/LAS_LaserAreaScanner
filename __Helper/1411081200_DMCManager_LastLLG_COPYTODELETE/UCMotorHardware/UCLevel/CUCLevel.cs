﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCLevel
{
  public delegate void DOnLevelChanged(Double level);


  public partial class CUCLevel : UserControl
  {
    //
    //----------------------------------------
    //  Section - Field
    //----------------------------------------
    //
    private DOnLevelChanged FOnLevelChanged;
    //
    //----------------------------------------
    //  Section - Constructor
    //----------------------------------------
    //
    public CUCLevel()
    {
      InitializeComponent();
      //      
      nudLevel.Minimum = (Decimal)(-10.0);
      nudLevel.Maximum = (Decimal)(+10.0);
      //
      hsbLevel.Minimum = -100;
      hsbLevel.Maximum = +100;
      //
      Level = 0.0;
    }
    //
    //----------------------------------------
    //  Section - Helper
    //----------------------------------------
    //
    private Double ScrollBarValueToDouble()
    {
      Double DValue = hsbLevel.Value - hsbLevel.Minimum;
      DValue *= Maximum - Minimum;
      DValue /= hsbLevel.Maximum - hsbLevel.Minimum;
      DValue += Minimum;
      return DValue;
    }

    private Int32 NumericUpDownValueToInt32()
    {     
      Double DValue = (Double)nudLevel.Value - Minimum;
      DValue /= Maximum - Minimum;
      DValue *= hsbLevel.Maximum - hsbLevel.Minimum;
      DValue += hsbLevel.Minimum;
      Int32 IValue = (Int32)DValue;
      return IValue; 
    }
    //
    //----------------------------------------
    //  Section - Property
    //----------------------------------------
    //
    public void SetOnLevelChanged(DOnLevelChanged value)
    {
      FOnLevelChanged = value;
    }

    public String Title
    {
      get { return lblTitle.Text; }
      set { lblTitle.Text = value; }
    }

    public Double Minimum
    {
      get { return (Double)nudLevel.Minimum; }
      set { nudLevel.Minimum = (Decimal)value; }
    }

    public Double Maximum
    {
      get { return (Double)nudLevel.Maximum; }
      set { nudLevel.Maximum = (Decimal)value; }
    }

    private Double GetLevel()
    {
      return (Double)nudLevel.Value;
    }
    private void SetLevel(Double value)
    {
      if (value < Minimum)
      {
        nudLevel.Value = (Decimal)Minimum;
      } else
        if (Maximum < value)
        {
          nudLevel.Value = (Decimal)Maximum;
        } else
        {
          nudLevel.Value = (Decimal)value;
        }
      Int32 IValue = NumericUpDownValueToInt32();
      if (IValue != hsbLevel.Value)
      {
        if (IValue < hsbLevel.Minimum)
        {
          hsbLevel.Value = hsbLevel.Minimum;
        } else
          if (hsbLevel.Maximum < IValue)
          {
            hsbLevel.Value = hsbLevel.Maximum;
          } else
          {
            hsbLevel.Value = IValue;
          }
      }
    }
    public Double Level
    {
      get { return GetLevel(); }
      set { SetLevel(value); }
    }
    private void SetLevel(Int32 value)
    {
      Double NUDValue = ScrollBarValueToDouble();
      SetLevel(NUDValue);
    }
    //
    //----------------------------------------
    //  Section - Event
    //----------------------------------------
    //
    private void hsbLevel_ValueChanged(object sender, EventArgs e)
    {
      SetLevel((Int32)hsbLevel.Value);
      if (FOnLevelChanged is DOnLevelChanged)
      {
        FOnLevelChanged(Level);
      }
    }

    private void nudLevel_ValueChanged(object sender, EventArgs e)
    {
      SetLevel((Double)nudLevel.Value);
      if (FOnLevelChanged is DOnLevelChanged)
      {
        FOnLevelChanged(Level);
      }
    }

    
   

  }
}
