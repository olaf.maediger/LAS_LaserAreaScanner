﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using IT01SerialPort;
using UCNotifier;
//
namespace IT01SerialPort
{
  public delegate void DOnDataReceived(String rxdata);

  public class CIT01SerialPort
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    private SerialPort FSerialPort;
    private DOnDataReceived FOnDataReceived;

    public CIT01SerialPort()
    {
      FSerialPort = new SerialPort();
    }

    //
    //-----------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

    public String GetPortName()
    {
      return FSerialPort.PortName;
    }

    public void SetOnDataReceived(DOnDataReceived value)
    {
      FOnDataReceived = value;
    }


    private void OnDataReceived(object sender, SerialDataReceivedEventArgs arguments)
    {
      String DataReceived = FSerialPort.ReadExisting();
      if (FOnDataReceived is DOnDataReceived)
      {
        FOnDataReceived(DataReceived);
      }
    }

    public Boolean IsOpen()
    {
      return (FSerialPort.IsOpen);
    }

    public Boolean Open(String portname)
    {
      try
      {
        if (FNotifier is CNotifier)
        {
          FNotifier.Write("Open ComPort[" + portname + "]");
        }        
        FSerialPort.PortName = portname;
        FSerialPort.BaudRate = 115200;
        FSerialPort.Parity = Parity.None;
        FSerialPort.DataBits = 8;
        FSerialPort.StopBits = StopBits.One;
        FSerialPort.DataReceived += new SerialDataReceivedEventHandler(OnDataReceived);
        FSerialPort.Open();
        return FSerialPort.IsOpen;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Close()
    {
      try
      {
        if (FNotifier is CNotifier)
        {
          FNotifier.Write("Close ComPort[" + FSerialPort.PortName + "]");
        }
        FSerialPort.Close();
        return !FSerialPort.IsOpen;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean WriteByte(Byte value)
    {
      try
      {
        if (FSerialPort.IsOpen)
        {
          Byte[] Buffer = new Byte[1];
          Buffer[0] = value;
          FSerialPort.Write(Buffer, 0, 1);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean WriteCharacter(Char character)
    {
      try
      {
        if (FSerialPort.IsOpen)
        {
          Byte[] Buffer = new Byte[1];
          Buffer[0] = (Byte)character;
          FSerialPort.Write(Buffer, 0, 1);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean WriteText(String text)
    {
      try
      {
        if (FSerialPort.IsOpen)
        {
          if (0 < text.Length)
          {
            Byte[] Buffer = new Byte[text.Length];
            for (Int32 BI = 0; BI < Buffer.Length; BI++)
            {
              Buffer[BI] = (Byte)text[BI];
            }
            FSerialPort.Write(Buffer, 0, Buffer.Length);
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean WriteBytes(Byte[] values)
    {
      try
      {
        if (FSerialPort.IsOpen)
        {
          FSerialPort.Write(values, 0, values.Length);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
