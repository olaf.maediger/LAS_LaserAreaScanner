﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
namespace Task
{
  public delegate Boolean DOnExecutionStart(RTaskData data);
  public delegate Boolean DOnExecutionBusy(RTaskData data);
  public delegate Boolean DOnExecutionEnd(RTaskData data);
  public delegate Boolean DOnExecutionAbort(RTaskData data);

  public struct RTaskData
  {
    public String Name;
    public Int32 Counter;
    public Boolean IsActive;
    public Boolean IsAborted;
    public DOnExecutionStart OnExecutionStart;
    public DOnExecutionBusy OnExecutionBusy;
    public DOnExecutionEnd OnExecutionEnd;
    public DOnExecutionAbort OnExecutionAbort;
  }

  public class CTask
  {
    private const String INIT_THREADNAME = "Task";
    private RTaskData FData;
    private Thread FThread;

    public CTask(DOnExecutionStart onexecutionstart,
                 DOnExecutionBusy onexecutionbusy,
                 DOnExecutionEnd onexecutionend,
                 DOnExecutionAbort onexecutionabort)
    {
      FData.Name = INIT_THREADNAME + FData.Counter.ToString();;
      FData.OnExecutionStart = onexecutionstart;
      FData.OnExecutionBusy = onexecutionbusy;
      FData.OnExecutionEnd = onexecutionend;
      FData.OnExecutionAbort = onexecutionabort;
      FData.Counter = 0;
      FData.IsAborted = false;
      FData.IsActive = false;
      FThread = null;
    }

    public Boolean IsActive()
    {
      return FData.IsActive;
    }

    public Boolean IsAborted()
    {
      return FData.IsAborted;
    }

    private void OnExecute()
    {
      String ThreadName = FThread.Name;
      FData.IsActive = true;
      // Console.WriteLine("---Start[" + ThreadName + "]");// ThreadState: " + FThread.ThreadState.ToString());
      if (FData.OnExecutionStart is DOnExecutionStart)
      {
        FData.OnExecutionStart(FData);
      }
      if (FData.OnExecutionBusy is DOnExecutionBusy)
      {
        Boolean Result = FData.OnExecutionBusy(FData);
        while (Result)
        {
          FData.Counter++;
          Result = FData.OnExecutionBusy(FData);
        }
      }
      if (FData.OnExecutionEnd is DOnExecutionEnd)
      {
        FData.OnExecutionEnd(FData);
      }
      // Console.WriteLine("---End[" + ThreadName + "]");// ThreadState: " + FThread.ThreadState.ToString());
      FThread = null;
      FData.IsActive = false;
    }

    public Boolean Start()
    {
      // Console.WriteLine("### CTask.Start - S");
      FData.Counter += 1;
      FData.Name = INIT_THREADNAME + FData.Counter.ToString();
      FThread = new Thread(OnExecute);
      FThread.Name = FData.Name;
      FThread.Start();
      // Console.WriteLine("### CTask.Start - E");
      return true;
    }

    public Boolean Abort()
    {
      try
      {
        if (FThread is Thread)
        {
          FThread.Abort();
          FData.IsAborted = true;
          FData.IsActive = false;
          if (FData.OnExecutionAbort is DOnExecutionAbort)
          {
            FData.OnExecutionAbort(FData);
          }
          if (ThreadState.Aborted == FThread.ThreadState)
          {
            FThread = null;
            return true;
          }
        }
        FThread = null;
        return false;
      }
      catch (Exception)
      {
        FThread = null;
        return false;
      }
    }

  }
}
