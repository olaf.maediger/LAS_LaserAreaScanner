﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;
using IT01BBAProtocol;
using IT01SerialPort;
using IT01System;
using UCNotifier;
//
//
//------------------------------------------------
//  PC - Uart Ports
//------------------------------------------------
// PC COM2 - IT01SCPTerminal - FSerialPortSCPDebug
// PC COM3 - IT01DKCTerminal - FSerialPortDKCDebug
// PC COM4 - IT01ADPTerminal - FSerialPortADPDebug
//
//------------------------------------------------
//  USBizi - Uart Ports
//------------------------------------------------
//  DEBUG     [SCP - IT01SCPTerminal(PC) - Communication]
//            - SCPRXD0   - USBizi COM1 RXD Pin99  - CON1 Pin2
//            - SCPTXD0   - USBizi COM1 TXD Pin98  - CON1 Pin3
//
//  EXTERN    [SERIAL0 ??? - EXTERNAL PRINTER Alternative ???]
//            - LPCRXD1   - USBizi COM2 RXD Pin74  - RXD0.10V - ST3 Pin5 SERIAL0 TXD
//            - LPCTXD1   - USBizi COM2 TXD Pin75  - TXD0.10V - ST3 Pin3 SERIAL0 RXD
//
//  DKC       [SCP - DKC - Communication]
//            - LPCRXD2   - USBizi COM3 RXD Pin49  
//            - LPCTXD2   - USBizi COM3 TXD Pin48  
//
//  ADP       [SCP - ADP - Communication]
//            - LPCRXD3   - USBizi COM4 RXD Pin85
//            - LPCTXD3   - USBizi COM4 TXD Pin82
//
//------------------------------------------------
//  DKC - Uart Ports
//------------------------------------------------
// 
//  LPCMASTER [DKC - SPC - Communication]
//  INTERFACE - DKC TXD P1(6) - LPCRXD2 - USBizi COM3 Pin49
//            - DKC RXD P1(4) - LPCTXD2 - USBizi COM3 Pin48
// 
//  EXTERNAL  [DKC - EXTERNAL PRINTER] {Direct Connection over Level-Shifter!}
//  PRINTER   - DKC TXD P1(3) - DKCTXD1 - TXD1.10V - ST3 Pin7 EXTERNAL PRINTER DTR
//            - DKC RXD P1(2) - DKCRXD1 - RXD1.10V - ST3 Pin2 EXTERNAL PRINTER DSR
//
//  DEBUG     [DKC - IT01DKCTerminal(PC) - Communication]
//            - DKC TXD P2(7) - DKCDEBUGTXD - X7 Pin13 - MCP - DKC-Debug-Port
//            - DKC RXD P0(1) - DKCDEBUGRXD - X7 Pin11 - MCP - DKC-Debug-Port
//
//------------------------------------------------
//  ADP - Uart Ports
//------------------------------------------------
// 
//  LPCMASTER [ADP - SPC - Communication]
//  INTERFACE - ADP TXD P1(3) - LPCRXD3 - USBizi COM4 Pin85
//            - ADP RXD P1(2) - LPCTXD3 - USBizi COM4 Pin82
// 
//  DEBUG     [ADP - IT01ADPTerminal(PC) - Communication]
//            - ADP TXD P1(6) - ADPDEBUGTXD - X7 Pin14 - MCP - ADP-Debug-Port
//            - ADP RXD P1(4) - ADPDEBUGRXD - X7 Pin12 - MCP - ADP-Debug-Port
// 
// 
//------------------------------------------------
//  USBizi - SPI Ports
//------------------------------------------------
//
//  RTC       - SPICS     - SPI1 Pin66
//            - SPISCK    - SPI1 Pin62
//            - SPIMISO   - SPI1 Pin61
//            - SPIMOSI   - SPI1 Pin60
//
//  FRAM      - SPI2CS    - SPI2 Pin79
//            - SPI2SCK   - SPI2 Pin78
//            - SPI2MOSI  - SPI2 Pin76
//            - SPI2MISO  - SPI2 Pin77
//
namespace IT01SCPTerminal
{
  public partial class FormMain : Form
  {
    //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String NAME_COMSCP = "COMSCP";
    private const String NAME_HEIGHTSCP = "HeightSCP";
    private const String NAME_HEIGHTDKC = "HeightDKC";
    //
    private const String INIT_COMSCP = "COM2";
    private const Int32 INIT_HEIGHTSCP = 200;
    private const Int32 INIT_HEIGHTDKC = 200;
    //
    //------------------------------------------------------------------------
    //  Section - Member
    //------------------------------------------------------------------------
    //		
    private CIT01SerialPort FSerialPortScpDebug;
    private CBBAProtocol FBBAProtocolScpDebug;
    private CIT01System FIT01System;
    //
    //------------------------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      FSerialPortScpDebug = new CIT01SerialPort();
      FSerialPortScpDebug.SetNotifier(FUCNotifier);
      //
      FBBAProtocolScpDebug = new CBBAProtocol();
      FBBAProtocolScpDebug.SetNotifier(FUCNotifier);
      //
      FIT01System = new CIT01System();
      FIT01System.SetNotifier(FUCNotifier);
      FIT01System.SetOnDatagramReceived(IT01SystemOnDatagramReceived);
      FIT01System.SetOnSystemStateChanged(IT01SystemOnSystemStateChanged);
      //
      String[] SS = FIT01System.GetSystemStates();
      Int32[] SV = (Int32[])FIT01System.GetSystemValues();
      for (Int32 SI = 0; SI < SS.Length; SI++)
      {
        String Line = String.Format("{0} - {1}", SV[SI].ToString(), SS[SI]);
        lbxSystemStates.Items.Add(Line);
      }
      //
      Init();
    }

    private void FreeProgrammerControls()
    {
      // ???????????????????????????? FIT01System.Close();
      FBBAProtocolScpDebug.Close();
      FSerialPortScpDebug.Close();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private void Init()
    { //
      //--------------------------------------------------------------------
      // Main
      //--------------------------------------------------------------------
      //
    }

    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      String SValue = INIT_COMSCP;
      initdata.ReadString(NAME_COMSCP, out SValue, SValue);
      FSerialPortScpDebug.Open(SValue);
      FBBAProtocolScpDebug.Open(FSerialPortScpDebug);
      FIT01System.Open(FSerialPortScpDebug, FBBAProtocolScpDebug);
      //
      Int32 IValue = INIT_HEIGHTSCP;
      initdata.ReadInt32(NAME_HEIGHTSCP, out IValue, IValue);
      pnlSCPDebug.Height = IValue;
      //
      IValue = INIT_HEIGHTDKC;
      initdata.ReadInt32(NAME_HEIGHTDKC, out IValue, IValue);
      pnlDKCDebug.Height = IValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      initdata.WriteString(NAME_COMSCP, FSerialPortScpDebug.GetPortName());
      initdata.WriteInt32(NAME_HEIGHTSCP, pnlSCPDebug.Height);
      initdata.WriteInt32(NAME_HEIGHTDKC, pnlDKCDebug.Height);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper - DKCDebug
    //------------------------------------------------------------------------
    //
    private delegate void CBBuildDKCDebugKeyPressedFromDatagram(Byte datagramsize,
                                                                Byte[] datagram);
    private void BuildDKCDebugKeyPressedFromDatagram(Byte datagramsize,
                                                     Byte[] datagram)
    {
      if (this.InvokeRequired)
      {
        CBBuildDKCDebugKeyPressedFromDatagram CB =
          new CBBuildDKCDebugKeyPressedFromDatagram(BuildDKCDebugKeyPressedFromDatagram);
        Invoke(CB, new object[] { datagramsize, datagram });
      }
      else
      {
        String Line = "Key(s) pressed:";
        Byte KeyCode = 0x00;
        Boolean KeyState = false;
        for (Byte DI = 0; DI < datagramsize; DI++)
        {
          if (KeyState)
          {
            if (0 < datagram[DI])
            {
              Line += String.Format(" [0x{0:X2}]", KeyCode);
            }
          }
          else
          {
            KeyCode = datagram[DI];
          }
          KeyState = !KeyState;
        }
        cbxDKCKeyboardFront.Items.Add(Line);
        cbxDKCKeyboardFront.SelectedIndex = cbxDKCKeyboardFront.Items.Count - 1;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - IT01System
    //------------------------------------------------------------------------
    //
    private void IT01SystemOnDatagramReceived(Byte datagramid,
                                              Byte datagramsize,
                                              Byte[] datagram)
    {
      switch (datagramid)
      { // SCP -> DisplayFront
        case CBBAProtocol.DATAGRAMID_SYSTEMSTATE:
          FIT01System.SystemState.BuildFromDatagram(datagramsize, datagram);
          break;
          // !!! case CBBAProtocol.DATAGRAMID_DISPLAYFRONT:
          // ...FUCIT01Display.WriteDatagram(datagramsize, datagram);
        // !!! break;
        case CBBAProtocol.DATAGRAMID_KEYBOARDFRONT:
          BuildDKCDebugKeyPressedFromDatagram(datagramsize, datagram);
          break;
        // SCP -> External Printer???
      }
    }


    private delegate void CBIT01SystemOnSystemStateChanged(ESystemState systemstate);
    private void IT01SystemOnSystemStateChanged(ESystemState systemstate)
    {
      if (this.InvokeRequired)
      {
        CBIT01SystemOnSystemStateChanged CB =
          new CBIT01SystemOnSystemStateChanged(IT01SystemOnSystemStateChanged);
        Invoke(CB, new object[] { systemstate });
      }
      else
      {
        lbxSystemStates.SelectedIndex = (Byte)systemstate;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    //



  }
}
