﻿namespace IT01SCPTerminal
{
  partial class FormMain
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.mitSystem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
      this.mitSQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.mitConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCDefault = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCResetToDefaultConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveAutomaticAtProgramEnd = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCStartup = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPEditParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPClearProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
      this.diagnosticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPLoadDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPSaveDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSendDiagnosticEmail = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
      this.mitCommand = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHelp = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHContents = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHTopic = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHSearch = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
      this.mitHEnterApplicationProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterDeviceProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
      this.mitHAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.DialogLoadInitdata = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveInitdata = new System.Windows.Forms.SaveFileDialog();
      this.FHelpProvider = new System.Windows.Forms.HelpProvider();
      this.tmrStartup = new System.Windows.Forms.Timer(this.components);
      this.pnlProtocol = new System.Windows.Forms.Panel();
      this.splProtocol = new System.Windows.Forms.Splitter();
      this.pnlMain = new System.Windows.Forms.Panel();
      this.pnlADPDebug = new System.Windows.Forms.Panel();
      this.label3 = new System.Windows.Forms.Label();
      this.splADPDebug = new System.Windows.Forms.Splitter();
      this.pnlDKCDebug = new System.Windows.Forms.Panel();
      this.cbxDKCKeyboardFront = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.splDKCDebug = new System.Windows.Forms.Splitter();
      this.pnlSCPDebug = new System.Windows.Forms.Panel();
      this.lbxSystemStates = new System.Windows.Forms.ListBox();
      this.label1 = new System.Windows.Forms.Label();
      this.fontDialog1 = new System.Windows.Forms.FontDialog();
      this.mstMain.SuspendLayout();
      this.pnlMain.SuspendLayout();
      this.pnlADPDebug.SuspendLayout();
      this.pnlDKCDebug.SuspendLayout();
      this.pnlSCPDebug.SuspendLayout();
      this.SuspendLayout();
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSystem,
            this.mitConfiguration,
            this.mitProtocol,
            this.mitCommand,
            this.mitHelp});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(634, 24);
      this.mstMain.TabIndex = 82;
      this.mstMain.Text = "System";
      // 
      // mitSystem
      // 
      this.mitSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.mitSQuit});
      this.mitSystem.Name = "mitSystem";
      this.mitSystem.Size = new System.Drawing.Size(57, 20);
      this.mitSystem.Text = "&System";
      // 
      // toolStripMenuItem3
      // 
      this.toolStripMenuItem3.Name = "toolStripMenuItem3";
      this.toolStripMenuItem3.Size = new System.Drawing.Size(137, 6);
      // 
      // mitSQuit
      // 
      this.mitSQuit.Name = "mitSQuit";
      this.mitSQuit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
      this.mitSQuit.Size = new System.Drawing.Size(140, 22);
      this.mitSQuit.Text = "&Quit";
      // 
      // mitConfiguration
      // 
      this.mitConfiguration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitCDefault,
            this.mitCResetToDefaultConfiguration,
            this.mitCSaveAutomaticAtProgramEnd,
            this.mitCStartup,
            this.mitCLoadStartupConfiguration,
            this.mitCSaveStartupConfiguration,
            this.mitCShowEditStartupConfiguration,
            this.toolStripMenuItem7,
            this.mitCLoadSelectableConfiguration,
            this.mitCSaveSelectableConfiguration,
            this.mitCShowEditSelectableConfiguration});
      this.mitConfiguration.Name = "mitConfiguration";
      this.mitConfiguration.Size = new System.Drawing.Size(93, 20);
      this.mitConfiguration.Text = "&Configuration";
      // 
      // mitCDefault
      // 
      this.mitCDefault.Enabled = false;
      this.mitCDefault.Name = "mitCDefault";
      this.mitCDefault.Size = new System.Drawing.Size(300, 22);
      this.mitCDefault.Text = "- Default -";
      // 
      // mitCResetToDefaultConfiguration
      // 
      this.mitCResetToDefaultConfiguration.Name = "mitCResetToDefaultConfiguration";
      this.mitCResetToDefaultConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
      this.mitCResetToDefaultConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCResetToDefaultConfiguration.Text = "Reset to Default-Configuration";
      // 
      // mitCSaveAutomaticAtProgramEnd
      // 
      this.mitCSaveAutomaticAtProgramEnd.Name = "mitCSaveAutomaticAtProgramEnd";
      this.mitCSaveAutomaticAtProgramEnd.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveAutomaticAtProgramEnd.Text = "Save automatic at program end";
      // 
      // mitCStartup
      // 
      this.mitCStartup.Enabled = false;
      this.mitCStartup.Name = "mitCStartup";
      this.mitCStartup.Size = new System.Drawing.Size(300, 22);
      this.mitCStartup.Text = "- Startup -";
      // 
      // mitCLoadStartupConfiguration
      // 
      this.mitCLoadStartupConfiguration.Name = "mitCLoadStartupConfiguration";
      this.mitCLoadStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
      this.mitCLoadStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadStartupConfiguration.Text = "Load Startup-Configuration";
      // 
      // mitCSaveStartupConfiguration
      // 
      this.mitCSaveStartupConfiguration.Name = "mitCSaveStartupConfiguration";
      this.mitCSaveStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
      this.mitCSaveStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveStartupConfiguration.Text = "Save Startup-Configuration";
      // 
      // mitCShowEditStartupConfiguration
      // 
      this.mitCShowEditStartupConfiguration.Name = "mitCShowEditStartupConfiguration";
      this.mitCShowEditStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditStartupConfiguration.Text = "Show / Edit Startup-Configuration";
      // 
      // toolStripMenuItem7
      // 
      this.toolStripMenuItem7.Enabled = false;
      this.toolStripMenuItem7.Name = "toolStripMenuItem7";
      this.toolStripMenuItem7.Size = new System.Drawing.Size(300, 22);
      this.toolStripMenuItem7.Text = "- Named -";
      // 
      // mitCLoadSelectableConfiguration
      // 
      this.mitCLoadSelectableConfiguration.Name = "mitCLoadSelectableConfiguration";
      this.mitCLoadSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadSelectableConfiguration.Text = "Load Selectable-Configuration ";
      // 
      // mitCSaveSelectableConfiguration
      // 
      this.mitCSaveSelectableConfiguration.Name = "mitCSaveSelectableConfiguration";
      this.mitCSaveSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveSelectableConfiguration.Text = "Save Selectable-Configuration";
      // 
      // mitCShowEditSelectableConfiguration
      // 
      this.mitCShowEditSelectableConfiguration.Name = "mitCShowEditSelectableConfiguration";
      this.mitCShowEditSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditSelectableConfiguration.Text = "Show / Edit Selectable-Configuration";
      // 
      // mitProtocol
      // 
      this.mitProtocol.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitPEditParameter,
            this.mitPClearProtocol,
            this.mitPCopyToClipboard,
            this.toolStripMenuItem1,
            this.diagnosticToolStripMenuItem,
            this.mitPLoadDiagnosticFile,
            this.mitPSaveDiagnosticFile,
            this.mitSendDiagnosticEmail,
            this.toolStripMenuItem2});
      this.mitProtocol.Name = "mitProtocol";
      this.mitProtocol.Size = new System.Drawing.Size(64, 20);
      this.mitProtocol.Text = "&Protocol";
      // 
      // mitPEditParameter
      // 
      this.mitPEditParameter.Name = "mitPEditParameter";
      this.mitPEditParameter.Size = new System.Drawing.Size(171, 22);
      this.mitPEditParameter.Text = "Edit Parameter";
      // 
      // mitPClearProtocol
      // 
      this.mitPClearProtocol.Name = "mitPClearProtocol";
      this.mitPClearProtocol.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
      this.mitPClearProtocol.Size = new System.Drawing.Size(171, 22);
      this.mitPClearProtocol.Text = "Clear";
      // 
      // mitPCopyToClipboard
      // 
      this.mitPCopyToClipboard.Name = "mitPCopyToClipboard";
      this.mitPCopyToClipboard.Size = new System.Drawing.Size(171, 22);
      this.mitPCopyToClipboard.Text = "Copy to Clipboard";
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new System.Drawing.Size(168, 6);
      // 
      // diagnosticToolStripMenuItem
      // 
      this.diagnosticToolStripMenuItem.Enabled = false;
      this.diagnosticToolStripMenuItem.Name = "diagnosticToolStripMenuItem";
      this.diagnosticToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
      this.diagnosticToolStripMenuItem.Text = "- Diagnostic -";
      // 
      // mitPLoadDiagnosticFile
      // 
      this.mitPLoadDiagnosticFile.Name = "mitPLoadDiagnosticFile";
      this.mitPLoadDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPLoadDiagnosticFile.Text = "Load File";
      // 
      // mitPSaveDiagnosticFile
      // 
      this.mitPSaveDiagnosticFile.Name = "mitPSaveDiagnosticFile";
      this.mitPSaveDiagnosticFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
      this.mitPSaveDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPSaveDiagnosticFile.Text = "Save File";
      // 
      // mitSendDiagnosticEmail
      // 
      this.mitSendDiagnosticEmail.Name = "mitSendDiagnosticEmail";
      this.mitSendDiagnosticEmail.Size = new System.Drawing.Size(171, 22);
      this.mitSendDiagnosticEmail.Text = "Send Email";
      // 
      // toolStripMenuItem2
      // 
      this.toolStripMenuItem2.Name = "toolStripMenuItem2";
      this.toolStripMenuItem2.Size = new System.Drawing.Size(168, 6);
      // 
      // mitCommand
      // 
      this.mitCommand.Name = "mitCommand";
      this.mitCommand.Size = new System.Drawing.Size(61, 20);
      this.mitCommand.Text = "Grabber";
      // 
      // mitHelp
      // 
      this.mitHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitHContents,
            this.mitHTopic,
            this.mitHSearch,
            this.toolStripMenuItem5,
            this.mitHEnterApplicationProductKey,
            this.mitHEnterDeviceProductKey,
            this.toolStripMenuItem4,
            this.mitHAbout});
      this.mitHelp.Name = "mitHelp";
      this.mitHelp.Size = new System.Drawing.Size(44, 20);
      this.mitHelp.Text = "Help";
      // 
      // mitHContents
      // 
      this.mitHContents.Name = "mitHContents";
      this.mitHContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.mitHContents.Size = new System.Drawing.Size(234, 22);
      this.mitHContents.Text = "Contents";
      // 
      // mitHTopic
      // 
      this.mitHTopic.Name = "mitHTopic";
      this.mitHTopic.Size = new System.Drawing.Size(234, 22);
      this.mitHTopic.Text = "Topic";
      // 
      // mitHSearch
      // 
      this.mitHSearch.Name = "mitHSearch";
      this.mitHSearch.Size = new System.Drawing.Size(234, 22);
      this.mitHSearch.Text = "Search";
      // 
      // toolStripMenuItem5
      // 
      this.toolStripMenuItem5.Name = "toolStripMenuItem5";
      this.toolStripMenuItem5.Size = new System.Drawing.Size(231, 6);
      // 
      // mitHEnterApplicationProductKey
      // 
      this.mitHEnterApplicationProductKey.Name = "mitHEnterApplicationProductKey";
      this.mitHEnterApplicationProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterApplicationProductKey.Text = "Enter Application Product-Key";
      // 
      // mitHEnterDeviceProductKey
      // 
      this.mitHEnterDeviceProductKey.Name = "mitHEnterDeviceProductKey";
      this.mitHEnterDeviceProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterDeviceProductKey.Text = "Enter Device Product-Key";
      // 
      // toolStripMenuItem4
      // 
      this.toolStripMenuItem4.Name = "toolStripMenuItem4";
      this.toolStripMenuItem4.Size = new System.Drawing.Size(231, 6);
      // 
      // mitHAbout
      // 
      this.mitHAbout.Name = "mitHAbout";
      this.mitHAbout.ShortcutKeys = System.Windows.Forms.Keys.F2;
      this.mitHAbout.Size = new System.Drawing.Size(234, 22);
      this.mitHAbout.Text = "About";
      // 
      // DialogLoadInitdata
      // 
      this.DialogLoadInitdata.DefaultExt = "ini.xml";
      this.DialogLoadInitdata.FileName = "Initdata";
      this.DialogLoadInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogLoadInitdata.Title = "MDesign: Load Initdata";
      // 
      // DialogSaveInitdata
      // 
      this.DialogSaveInitdata.DefaultExt = "ini.xml";
      this.DialogSaveInitdata.FileName = "Initdata";
      this.DialogSaveInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogSaveInitdata.Title = "MDesign: Save Initdata";
      // 
      // tmrStartup
      // 
      this.tmrStartup.Interval = 1000;
      // 
      // pnlProtocol
      // 
      this.pnlProtocol.BackColor = System.Drawing.Color.White;
      this.pnlProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlProtocol.Location = new System.Drawing.Point(0, 550);
      this.pnlProtocol.Name = "pnlProtocol";
      this.pnlProtocol.Size = new System.Drawing.Size(634, 158);
      this.pnlProtocol.TabIndex = 84;
      // 
      // splProtocol
      // 
      this.splProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splProtocol.Location = new System.Drawing.Point(0, 547);
      this.splProtocol.Name = "splProtocol";
      this.splProtocol.Size = new System.Drawing.Size(634, 3);
      this.splProtocol.TabIndex = 85;
      this.splProtocol.TabStop = false;
      // 
      // pnlMain
      // 
      this.pnlMain.Controls.Add(this.pnlADPDebug);
      this.pnlMain.Controls.Add(this.splADPDebug);
      this.pnlMain.Controls.Add(this.pnlDKCDebug);
      this.pnlMain.Controls.Add(this.splDKCDebug);
      this.pnlMain.Controls.Add(this.pnlSCPDebug);
      this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlMain.Location = new System.Drawing.Point(0, 24);
      this.pnlMain.Name = "pnlMain";
      this.pnlMain.Size = new System.Drawing.Size(634, 523);
      this.pnlMain.TabIndex = 86;
      // 
      // pnlADPDebug
      // 
      this.pnlADPDebug.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlADPDebug.Controls.Add(this.label3);
      this.pnlADPDebug.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlADPDebug.Location = new System.Drawing.Point(0, 381);
      this.pnlADPDebug.Name = "pnlADPDebug";
      this.pnlADPDebug.Size = new System.Drawing.Size(634, 142);
      this.pnlADPDebug.TabIndex = 12;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(6, 6);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(135, 20);
      this.label3.TabIndex = 2;
      this.label3.Text = "ADP Channel Out";
      // 
      // splADPDebug
      // 
      this.splADPDebug.Dock = System.Windows.Forms.DockStyle.Top;
      this.splADPDebug.Location = new System.Drawing.Point(0, 378);
      this.splADPDebug.Name = "splADPDebug";
      this.splADPDebug.Size = new System.Drawing.Size(634, 3);
      this.splADPDebug.TabIndex = 11;
      this.splADPDebug.TabStop = false;
      // 
      // pnlDKCDebug
      // 
      this.pnlDKCDebug.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlDKCDebug.Controls.Add(this.cbxDKCKeyboardFront);
      this.pnlDKCDebug.Controls.Add(this.label2);
      this.pnlDKCDebug.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlDKCDebug.Location = new System.Drawing.Point(0, 231);
      this.pnlDKCDebug.Name = "pnlDKCDebug";
      this.pnlDKCDebug.Size = new System.Drawing.Size(634, 147);
      this.pnlDKCDebug.TabIndex = 8;
      // 
      // cbxDKCKeyboardFront
      // 
      this.cbxDKCKeyboardFront.FormattingEnabled = true;
      this.cbxDKCKeyboardFront.Location = new System.Drawing.Point(164, 6);
      this.cbxDKCKeyboardFront.Name = "cbxDKCKeyboardFront";
      this.cbxDKCKeyboardFront.Size = new System.Drawing.Size(221, 21);
      this.cbxDKCKeyboardFront.TabIndex = 2;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(6, 6);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(155, 20);
      this.label2.TabIndex = 1;
      this.label2.Text = "DKC Keyboard Front";
      // 
      // splDKCDebug
      // 
      this.splDKCDebug.Dock = System.Windows.Forms.DockStyle.Top;
      this.splDKCDebug.Location = new System.Drawing.Point(0, 228);
      this.splDKCDebug.Name = "splDKCDebug";
      this.splDKCDebug.Size = new System.Drawing.Size(634, 3);
      this.splDKCDebug.TabIndex = 5;
      this.splDKCDebug.TabStop = false;
      // 
      // pnlSCPDebug
      // 
      this.pnlSCPDebug.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlSCPDebug.Controls.Add(this.lbxSystemStates);
      this.pnlSCPDebug.Controls.Add(this.label1);
      this.pnlSCPDebug.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlSCPDebug.Location = new System.Drawing.Point(0, 0);
      this.pnlSCPDebug.Name = "pnlSCPDebug";
      this.pnlSCPDebug.Size = new System.Drawing.Size(634, 228);
      this.pnlSCPDebug.TabIndex = 4;
      // 
      // lbxSystemStates
      // 
      this.lbxSystemStates.FormattingEnabled = true;
      this.lbxSystemStates.Location = new System.Drawing.Point(10, 29);
      this.lbxSystemStates.Name = "lbxSystemStates";
      this.lbxSystemStates.Size = new System.Drawing.Size(357, 108);
      this.lbxSystemStates.TabIndex = 7;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(6, 6);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(137, 20);
      this.label1.TabIndex = 0;
      this.label1.Text = "SCP SystemState";
      // 
      // FormMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Control;
      this.ClientSize = new System.Drawing.Size(634, 708);
      this.Controls.Add(this.pnlMain);
      this.Controls.Add(this.splProtocol);
      this.Controls.Add(this.pnlProtocol);
      this.Controls.Add(this.mstMain);
      this.KeyPreview = true;
      this.Name = "FormMain";
      this.Text = "<it01scpterminal>";
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.pnlMain.ResumeLayout(false);
      this.pnlADPDebug.ResumeLayout(false);
      this.pnlADPDebug.PerformLayout();
      this.pnlDKCDebug.ResumeLayout(false);
      this.pnlDKCDebug.PerformLayout();
      this.pnlSCPDebug.ResumeLayout(false);
      this.pnlSCPDebug.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem mitSystem;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
    private System.Windows.Forms.ToolStripMenuItem mitSQuit;
    private System.Windows.Forms.ToolStripMenuItem mitConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCDefault;
    private System.Windows.Forms.ToolStripMenuItem mitCResetToDefaultConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveAutomaticAtProgramEnd;
    private System.Windows.Forms.ToolStripMenuItem mitCStartup;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPEditParameter;
    private System.Windows.Forms.ToolStripMenuItem mitPClearProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPCopyToClipboard;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem diagnosticToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitPLoadDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitPSaveDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitSendDiagnosticEmail;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
    private System.Windows.Forms.ToolStripMenuItem mitCommand;
    private System.Windows.Forms.ToolStripMenuItem mitHelp;
    private System.Windows.Forms.ToolStripMenuItem mitHContents;
    private System.Windows.Forms.ToolStripMenuItem mitHTopic;
    private System.Windows.Forms.ToolStripMenuItem mitHSearch;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterApplicationProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterDeviceProductKey;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
    private System.Windows.Forms.ToolStripMenuItem mitHAbout;
    private System.Windows.Forms.OpenFileDialog DialogLoadInitdata;
    private System.Windows.Forms.SaveFileDialog DialogSaveInitdata;
    private System.Windows.Forms.HelpProvider FHelpProvider;
    private System.Windows.Forms.Timer tmrStartup;
    private System.Windows.Forms.Panel pnlProtocol;
    private System.Windows.Forms.Splitter splProtocol;
    private System.Windows.Forms.Panel pnlMain;
    private System.Windows.Forms.Panel pnlSCPDebug;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.FontDialog fontDialog1;
    private System.Windows.Forms.ListBox lbxSystemStates;
    private System.Windows.Forms.Panel pnlADPDebug;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Splitter splADPDebug;
    private System.Windows.Forms.Panel pnlDKCDebug;
    private System.Windows.Forms.ComboBox cbxDKCKeyboardFront;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Splitter splDKCDebug;
  }
}

