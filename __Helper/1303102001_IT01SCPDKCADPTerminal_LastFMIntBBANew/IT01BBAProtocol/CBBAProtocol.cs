﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Convert;
using IT01SerialPort;
using UCNotifier;
//
namespace IT01BBAProtocol
{
  //
  //-------------------------------------------------------------
  //  BinaryBlockAsciiProtocol - DatagramByteAscii
  //  Index BSize   Format    Volume
  //    0     1     Byte      STX
  //    1     2     AsciiByte DatagramID
  //    2     2     AsciiByte DatagramKind
  //    3     2     AsciiByte Length Datagram of AsciiBytes L
  //    4     2     AsciiByte Data0
  //    5     2     AsciiByte ...
  //    6     2     AsciiByte DataL-1
  //    7     2     AsciiByte Checksum
  //    8     1     Byte      ETX
  //    9     1     Byte      CR
  //    10    1     Byte      LF
  //-------------------------------------------------------------
  //
  //  Datagram Length[bytes] = 1{STX} + 2{DID} + 2{DK} + 2 * L + 2{CS} + 1{ETX} + 1{CR} + 1{LF}
  //                         = 2 * L + 10
  //
  //
  //-------------------------------------------------------------
  //  BinaryBlockAsciiProtocol - DatagramTextAscii
  //  Index BSize   Format    Volume
  //    0     1     Byte      STX
  //    1     2     AsciiByte DatagramID
  //    1     2     AsciiByte DatagramKind
  //    2     2     AsciiByte Length Datagram of TextBytes L
  //    3     1     Byte Character0
  //    4     1     Byte ...
  //    5     1     Byte CharacterN-1
  //    6     1     AsciiByte Checksum
  //    7     1     Byte      ETX
  //    8     1     Byte      CR
  //    9     1     Byte      LF
  //-------------------------------------------------------------
  //
  //  Datagram Length[bytes] = 1{STX} + 2{DID} + 2{DK} + N + 2{CS} + 1{ETX} + 1{CR} + 1{LF}
  //                         = N + 10
  //
  public delegate void DOnDatagramReceived(Byte datagramid,
                                           Byte datagramsize,
                                           Byte[] datagram);

  public enum EDatagramKind
  {
    ByteAscii = 0x55,
    TextAscii = 0xAA
  }

  public enum EDetectionState
  {
    DatagramActive,
    DatagramPassive
  }

  public class CBBAProtocol
  { //
    //-----------------------------------------------------------
    //  Segment - Constant
    //-----------------------------------------------------------
    //
    public const Byte TOKEN_STX = 0x02; // detect this on receiving!
    public const Byte TOKEN_ETX = 0x03; // detect this on receiving!
    public const Byte TOKEN_CR = 0x0D;  // ignore this on receiving!
    public const Byte TOKEN_LF = 0x0A;  // ignore this on receiving!
    //
    public const Byte INIT_CHECKSUMPRESET = 0x00;
    //
    public const Byte INDEX_STX = 0x00;
    public const Byte INDEX_DATAGRAMID = 0x01;
    public const Byte INDEX_DATAGRAMKIND = 0x01;
    public const Byte INDEX_DATAGRAMSIZE = 0x05;
    public const Byte INDEX_DATAGRAM = 0x07;
    //
    //-----------------------------------------------------------
    //  Segment - Constant - DatagramID
    //-----------------------------------------------------------
    public const Byte DATAGRAMID_SYSTEMSTATE = 0x11;
    public const Byte DATAGRAMID_SCP = 0x21;
    public const Byte DATAGRAMID_DKC = 0x31;
    public const Byte DATAGRAMID_ADC = 0x41;
    public const Byte DATAGRAMID_DISPLAYFRONT01 = 0x50;
    public const Byte DATAGRAMID_DISPLAYFRONT23 = 0x52;
    public const Byte DATAGRAMID_KEYBOARDFRONT = 0x61;
    public const Byte DATAGRAMID_KEYBOARDEXTERNAL = 0x71;
    //
    //-----------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CIT01SerialPort FSerialPort;
    private DOnDatagramReceived FOnDatagramReceived;
    private String FRxdLine;
    private EDetectionState FDetectionState;
    //
    //-----------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------
    //
    public CBBAProtocol()
    {
      FSerialPort = null;
      FOnDatagramReceived = null;
      FRxdLine = "";
      FDetectionState = EDetectionState.DatagramPassive;
    }
    //
    //-----------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnDatagramReceived(DOnDatagramReceived value)
    {
      FOnDatagramReceived = value;
    }
    //
    //-----------------------------------------------------------
    //  Segment - Callback
    //-----------------------------------------------------------
    //
    private void UartOnDataReceived(String rxdata)
    {
      for (int CI = 0; CI < rxdata.Length; CI++)
      {
        Char RxdCharacter = rxdata[CI]; 
        switch (RxdCharacter)
        {
          case (Char)TOKEN_STX:
            FRxdLine = "";
            FDetectionState = EDetectionState.DatagramActive;
            break;
          case (Char)TOKEN_ETX:
            String Line = String.Format("L[RXDL] = {0}", rxdata.Length);
            Console.WriteLine(Line);
            AnalyseDatagram(FRxdLine);
            FDetectionState = EDetectionState.DatagramPassive;
            break;
          default:
            if (EDetectionState.DatagramActive == FDetectionState)
            {
              FRxdLine += RxdCharacter;
            }
            break;
        }
      }
    }
    //
    //-----------------------------------------------------------
    //  Segment - Helper
    //-----------------------------------------------------------
    // 130310
    private Boolean AnalyseDatagram(String rxdline)
    {
      Byte DatagramID = 0x00;
      EDatagramKind DatagramKind = EDatagramKind.ByteAscii;
      Int32 DatagramSize = (rxdline.Length - 4) / 2;
      Byte ChecksumActual = INIT_CHECKSUMPRESET;
      // Check
      if (DatagramSize < 1)
      {
        return false;
      }
      if (TOKEN_STX != rxdline[INDEX_STX])
      {
        return false;
      }
      //
      // Analyse DatagramID
      DatagramID = CConvert.AsciiByteToByte((Char)rxdline[0 + INDEX_DATAGRAMID],
                                            (Char)rxdline[1 + INDEX_DATAGRAMID]);
      ChecksumActual ^= DatagramID;
      //
      // Analyse DatagramKind
      DatagramKind = (EDatagramKind)CConvert.AsciiByteToByte((Char)rxdline[0 + INDEX_DATAGRAMKIND],
                                                             (Char)rxdline[1 + INDEX_DATAGRAMKIND]);
      ChecksumActual ^= DatagramID;
      //
      // Analyse DatagramSize
      DatagramSize = CConvert.AsciiByteToByte((Char)rxdline[0 + INDEX_DATAGRAMSIZE],
                                              (Char)rxdline[1 + INDEX_DATAGRAMSIZE]);
      ChecksumActual ^= (Byte)DatagramSize;
      //
      // Analyse Datagram
      Byte[] Datagram = new Byte[DatagramSize];
      Byte DatagramIndex = INDEX_DATAGRAM;
      switch (DatagramKind)
      {
        case EDatagramKind.ByteAscii:
          for (Int32 SI = 0x00; SI < DatagramSize; SI++)
          {
            Char NibbleLow = rxdline[0 + DatagramIndex];
            DatagramIndex++;
            Char NibbleHigh = rxdline[1 + DatagramIndex];
            DatagramIndex++;
            Datagram[SI] = CConvert.AsciiByteToByte(NibbleLow, NibbleHigh);
          }
          break;
        case EDatagramKind.TextAscii:
          for (Int32 SI = 0x00; SI < DatagramSize; SI++)
          {
            Char Character = rxdline[DatagramIndex];
            DatagramIndex++;
            Datagram[SI] = (Byte)Character;
          }
          break;
        default:
          return false;
      }
      //
      // Analyse Checksum
      Byte ChecksumPreset = CConvert.AsciiByteToByte((Char)rxdline[0 + DatagramIndex],
                                                     (Char)rxdline[1 + DatagramIndex]);
      if (ChecksumPreset != ChecksumActual)
      {
        // .... Error !!!!
      }
      // Notify Parent with detected Datagram
      if (FOnDatagramReceived is DOnDatagramReceived)
      {
        FOnDatagramReceived(DatagramID, (Byte)DatagramSize, Datagram);
      }
      return true;
    }
    //
    //-----------------------------------------------------------
    //  Segment - Management
    //-----------------------------------------------------------
    //
    public Boolean IsOpen()
    {
      if (FSerialPort is CIT01SerialPort)
      {
        return (FSerialPort.IsOpen());
      }
      return false;
    }

    public Boolean Open(CIT01SerialPort serialport)
    { // uart MUST be opened! (Main)
      if (null == FSerialPort)
      {
        if (serialport is CIT01SerialPort)
        {
          FSerialPort = serialport;
          FSerialPort.SetOnDataReceived(UartOnDataReceived);
          FRxdLine = "";
          return FSerialPort.IsOpen();
        }
      }
      return false;
    }

    public Boolean Close()
    {
      if (FSerialPort is CIT01SerialPort)
      {
        FSerialPort.Close();
        FSerialPort = null;
        return true;
      }
      return false;
    }
    //
    //-----------------------------------------------------------
    //  Segment - Low Level
    //-----------------------------------------------------------
    //
    private Boolean WriteByteBinary(Byte value)
    {
      try
      {
        return FSerialPort.WriteByte(value);
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean WriteByteAscii(Byte value)
    {
      try
      {
        String Text = CConvert.ByteToHexadecimalString(value);
        return FSerialPort.WriteText(Text);
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------
    //  Segment - High Level
    //-----------------------------------------------------------
    //
    public Boolean WriteDatagramByteAscii(Byte datagramid,
                                          Byte datagramsize,
                                          Byte[] datagram)
    {
      Byte Checksum = INIT_CHECKSUMPRESET;
      //
      // STX
      WriteByteBinary(TOKEN_STX);
      //
      // DatagramID
      Checksum ^= datagramid;
      WriteByteAscii(datagramid);
      //
      // DatagramKind
      Byte DatagramKind = (Byte)EDatagramKind.ByteAscii;
      Checksum ^= DatagramKind;
      WriteByteAscii(DatagramKind);
      //
      // DatagramSize
      Checksum ^= datagramsize;
      WriteByteAscii(datagramsize);
      //
      // Datagram
      if (datagram is Byte[])
      {
        for (Byte DI = 0; DI < datagram.Length; DI++)
        {
          Checksum ^= datagram[DI];
          WriteByteAscii(datagram[DI]);
        }
      }
      //
      // Checksum
      WriteByteAscii(Checksum);
      //
      // ETX, CR, LF
      WriteByteBinary(TOKEN_ETX);
      WriteByteBinary(TOKEN_CR);
      WriteByteBinary(TOKEN_LF);
      //    
      return true;
    }

    public Boolean WriteDatagramTextAscii(Byte datagramid,
                                          Byte datagramsize,
                                          Byte[] datagram)
    {
      Byte Checksum = INIT_CHECKSUMPRESET;
      //
      // STX
      WriteByteBinary(TOKEN_STX);
      //
      // DatagramID
      Checksum ^= datagramid;
      WriteByteAscii(datagramid);
      //
      // DatagramKind
      Byte DatagramKind = (Byte)EDatagramKind.TextAscii;
      Checksum ^= DatagramKind;
      WriteByteAscii(DatagramKind);
      //
      // DatagramSize
      Checksum ^= datagramsize;
      WriteByteAscii(datagramsize);
      //
      // Datagram
      if (datagram is Byte[])
      {
        for (Byte DI = 0; DI < datagram.Length; DI++)
        {
          Checksum ^= datagram[DI];
          WriteByteBinary(datagram[DI]);
        }
      }
      //
      // Checksum
      WriteByteAscii(Checksum);
      //
      // ETX, CR, LF
      WriteByteBinary(TOKEN_ETX);
      WriteByteBinary(TOKEN_CR);
      WriteByteBinary(TOKEN_LF);
      //    
      return true;
    }

  }
}
