﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;
using UCNotifier;
using IT01SerialPort;
using IT01BBAProtocol;
//
namespace IT01DKCTerminal
{ //
  //##########################################################################
  //	Section - Main - Extended
  //##########################################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------------------------
    //  Section - Member
    //------------------------------------------------------------------------
    //		
    private CIT01SerialPort FSerialPortDKCDebug;
    private CBBAProtocol FBBAProtocol;
    //
    //------------------------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      FSerialPortDKCDebug = new CIT01SerialPort();
      FSerialPortDKCDebug.Open("COM3");
      //
      FBBAProtocol = new CBBAProtocol();
      FBBAProtocol.SetOnDatagramReceived(BBAProtocolOnDatagramReceived);
      FBBAProtocol.Open(FSerialPortDKCDebug);
      //
      FUCIT01KeyboardFront.SetOnKeyMatrixChanged(IT01KeyboardFrontOnKeyMatrixChanged);
    }

    private void FreeProgrammerControls()
    {
      // FUCGrabberHardware.Close(true);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      // FUCGrabberHardware.LoadInitdata(initdata);
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      // FUCGrabberHardware.SaveInitdata(initdata);
      //
      return Result;
    }

    //
    //------------------------------------------------------------------------
    //  Section - Callback - ...
    //------------------------------------------------------------------------
    //
    // SCP -> LPCR/TXD2 -> DKC LPCMASTER -> DKC DEBUG-Port (MCP) -> THIS - IT01DKCTerminal(PC)
    private void BBAProtocolOnDatagramReceived(Byte datagramid,
                                               Byte datagramsize,
                                               Byte[] datagram)
    {
      switch (datagramid)
      { // SCP -> DisplayFront
        case CBBAProtocol.DATAGRAMID_DISPLAYFRONT01:
          FUCIT01Display.WriteDatagram01(datagramsize, datagram);
          break;
        case CBBAProtocol.DATAGRAMID_DISPLAYFRONT23:
          FUCIT01Display.WriteDatagram23(datagramsize, datagram);
          break;
        // SCP -> External Printer???
      }
    }

    // Key -> THIS - IT01DKCTerminal(PC) -> DKC DEBUG-Port (MCP) -> DKC LPCMASTER -> SCP -> LPCR/TXD2
    private void IT01KeyboardFrontOnKeyMatrixChanged(Byte[] keycodes, Byte[] keystates)
    {
      Byte DatagramID = CBBAProtocol.DATAGRAMID_KEYBOARDFRONT;
      Byte DatagramSize = (Byte)(keycodes.Length + keystates.Length);
      Byte[] Datagram = new Byte[DatagramSize];
      Byte DI = 0;
      for (Byte KI = 0; KI < keycodes.Length; KI++)
      {
        Datagram[DI] = keycodes[KI];
        DI++;
        Datagram[DI] = keystates[KI];
        DI++;
      }
      FBBAProtocol.WriteDatagramByteAscii(DatagramID, DatagramSize, Datagram);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    //


  }
}
