﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Initdata;
using UCNotifier;
using UCTextEditor;
//
namespace IT01DKCTerminal
{
  //
  //------------------------------------------------------
  //  Section - Error
  //------------------------------------------------------
  //	
  public enum EErrorCode
  {
    None = 0,
    InvalidLibraryCall = 1,
    InvalidInitialization,
    InvalidAcquistion,
    Invalid
  };
  //
  //#########################################################
  //	Section - Main - Basic
  //#########################################################
  //
  public partial class FormMain : Form
  {
    //
    //------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------
    //		
    private const String HEADER_LIBRARY = "Main";
    private const String INIT_SEGMENTBASE = "Application";
    //
    private const String APPLICATION_TITLE = "IT01DKCTerminal";
    private const Int32 APPLICATION_COUNT = 1;
    private const String APPLICATION_NAME = APPLICATION_TITLE;
    private const String APPLICATION_VERSION = "01V0104";
    private const String APPLICATION_DATE = "4. March 2013";
    private const Boolean APPLICATION_DISCOUNTUSER = false;
    private const String APPLICATION_USER = "OM";
    //
    private const String FORMAT_MODULE_NAME = "DemoPlugin{0}";
    private const String MODULE_VERSION = APPLICATION_VERSION;
    private const String MODULE_DATE = APPLICATION_DATE;
    private const Boolean MODULE_DISCOUNTUSER = false;
    private const String MODULE_USER = APPLICATION_USER;
    //
    private const Int32 DEVICE_COUNT_MINIMUM = 0;
    private const String DEVICE_NAME = "MCD";
    private const String DEVICE_VERSION = "01V0101";
    private const String DEVICE_DATE = "01. February 2012";
    private const Boolean DEVICE_DISCOUNTUSER = false;
    private const String DEVICE_USER = "LLG";
    //
    private const String MAIN_HELPFILE = APPLICATION_TITLE + "Help.chm";
    //
    public const String DESCRIPTION = APPLICATION_TITLE;
    public const String POINT1 = " - ";
    public const String POINT2 = " - ";
    public const String POINT3 = " - ";
    //
    //
    /*public const String COMPANYTARGETTOP = "Laser-Laboratorium";
    public const String COMPANYTARGETBOTTOM = "Göttingen e.V.";
    public const String STREETTARGET = "Hans-Adolf-Krebs-Weg 1";
    public const String CITYTARGET = "D - 37077 Göttingen";
    //
    public const String COMPANYSOURCETOP = "Laser-Laboratorium";
    public const String COMPANYSOURCEBOTTOM = "Göttingen e.V.";
    public const String STREETSOURCE = "Hans-Adolf-Krebs-Weg 1";
    public const String CITYSOURCE = "D - 37077 Göttingen";
    //
    private const String SUBDIRECTORY_TARGET =
      "LLG\\" + APPLICATION_TITLE + APPLICATION_VERSION;*/
    //
    public const String COMPANYTARGETTOP = "Maediger";
    public const String COMPANYTARGETBOTTOM = "Programming & Development";
    public const String STREETTARGET = "Bundesstrasse 37";
    public const String CITYTARGET = "D - 37191 Katlenburg-Lindau";
    //
    public const String COMPANYSOURCETOP = "Maediger";
    public const String COMPANYSOURCEBOTTOM = "Programming & Development";
    public const String STREETSOURCE = "Bundesstrasse 37";
    public const String CITYSOURCE = "D - Katlenburg-Lindau";
    //
    private const String SUBDIRECTORY_TARGET =
      "MWare\\" + APPLICATION_TITLE + APPLICATION_VERSION;

    //
    public const String INIT_FILENAME = APPLICATION_TITLE;
    public const String INIT_FILEEXTENSION = ".ini.xml";
    public const String INIT_TITLE = APPLICATION_TITLE;
    //
    private const String NAME_HEIGHTPROTOCOL = "HeightProtocol";
    private const String NAME_SAVEAUTOMATICATPROGRAMEND = "SaveAutomaticAtProgramEnd";
    //
    private const FormWindowState INIT_WINDOWSTATE = FormWindowState.Normal;
    private const Int32 INIT_LEFT = 10;
    private const Int32 INIT_TOP = 10;
    private const Int32 INIT_WIDTH = 700;
    private const Int32 INIT_HEIGHT = 980;
    //
    private const Int32 INIT_HEIGHTPROTOCOL = 300;
    // PSimon private const Boolean INIT_SAVEAUTOMATICATPROGRAMEND = true; 
    // OMaediger 
    private const Boolean INIT_SAVEAUTOMATICATPROGRAMEND = false; 
    //
    //
    public static readonly String[] ERRORS = 
    { 
      "None",
      "Invalid Library Call",
      "Invalid Initialization",
      "Invalid Acquistion",
  	  "Invalid"
    };
    //
    //------------------------------------------------------
    //  Section - Member
    //------------------------------------------------------
    //		
    private String FInitdataFilename = INIT_FILENAME + INIT_FILEEXTENSION;
    private String FInitdataDirectory;
    private CUCNotifier FUCNotifier;
   // private Int32 FStartupState;
    //
    //------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------
    //		
    public FormMain()
    {	// DecimalPoint 
      CInitdata.Init();
      // DecimalPoint
      InitializeComponent();
      StartPosition = FormStartPosition.Manual;
      //
      this.Load += FormMain_Load;
      this.FormClosing += FormMain_FormClosing;
      //
      //----------------------------
      // Instantiation UCNotifier
      //----------------------------
      FUCNotifier = new CUCNotifier();
      FUCNotifier.SetProtocolParameter(SUBDIRECTORY_TARGET,
                                       APPLICATION_NAME, APPLICATION_VERSION,
                                       COMPANYSOURCETOP,
                                       COMPANYSOURCEBOTTOM,
                                       APPLICATION_USER);
      tbpProtocol.Controls.Add(FUCNotifier);
      FUCNotifier.Dock = DockStyle.Fill;
      _Protocol("Creating all Controls");
      //
      //----------------------------
      // Instantiation Help
      //----------------------------
      FHelpProvider.HelpNamespace = Path.Combine(Directory.GetCurrentDirectory(), MAIN_HELPFILE);
      //
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      // Instantiate Programmer-Controls:
      InstantiateProgrammerControls();
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      //
      InitControls();
      //
      //----------------------------
      // Instantiation Initdata
      //----------------------------
      if (!CInitdata.BuildInitdataDirectory(SUBDIRECTORY_TARGET,
                                            INIT_FILENAME,
                                            INIT_FILEEXTENSION,
                                            out FInitdataDirectory,
                                            out FInitdataFilename))
      { // No initfile present -> build and save project.ini.xml with INIT_-values:
        LoadInitdata(FInitdataDirectory, FInitdataFilename);
        SaveInitdata(FInitdataDirectory, FInitdataFilename);
      }
      _Error(EErrorCode.None);
      _Protocol("Construction successful.");
    }
    //
    //------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Form-Events
    //------------------------------------------------------
    //
    private void FormMain_Load(object sender, EventArgs e)
    {
      LoadInitdata(FInitdataDirectory, FInitdataFilename);
      // FStartupState = 0;
      tmrStartup.Interval = 1000;
      tmrStartup.Enabled = true;
      tmrStartup.Tick += new System.EventHandler(tmrStartup_Tick);
    }

    private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
    { // 
      // Close all Sub-Devices...
      FreeProgrammerControls();
      //
      FUCNotifier.SaveOnClosing();
      //
      if (mitCSaveAutomaticAtProgramEnd.Checked)
      {
        SaveInitdata(FInitdataDirectory, FInitdataFilename);
      }
    }

    private void InitdataOnNotify(String module, String line)
    {
      FUCNotifier.Write(module, line);
    }


    //
    //----------------------------
    //  Section - Init
    //----------------------------
    //
    private void InitApplicationTitle()
    {
      Text = APPLICATION_TITLE + "  Version " + APPLICATION_VERSION + " | " + COMPANYSOURCETOP;    
    }

    private void InitControls()
    {
      _Protocol("Initialisation of Controls.");
      InitApplicationTitle();
      //
      //  Event - System
      mitSQuit.Click += new EventHandler(mitSQuit_Click);
      //
      //  Event - Configuration
      mitCResetToDefaultConfiguration.Click += new EventHandler(mitCResetToDefaultConfiguration_Click);
      mitCSaveAutomaticAtProgramEnd.Click += new EventHandler(mitCSaveAutomaticAtProgramEnd_Click);
      mitCLoadStartupConfiguration.Click += new EventHandler(mitCLoadStartupConfiguration_Click);
      mitCSaveStartupConfiguration.Click += new EventHandler(mitCSaveStartupConfiguration_Click);
      mitCShowEditStartupConfiguration.Click += new EventHandler(mitCShowEditStartupConfiguration_Click);
      mitCLoadSelectableConfiguration.Click += new EventHandler(mitCLoadSelectableConfiguration_Click);
      mitCSaveSelectableConfiguration.Click += new EventHandler(mitCSaveSelectableConfiguration_Click);
      mitCShowEditSelectableConfiguration.Click += new EventHandler(mitCShowEditSelectableConfiguration_Click);
      //
      //  Event - 
      mitPEditParameter.Click += new EventHandler(mitPEditParameter_Click);
      mitPClearProtocol.Click += new EventHandler(mitPClearProtocol_Click);
      mitPCopyToClipboard.Click += new EventHandler(mitPCopyToClipboard_Click);
      mitPLoadDiagnosticFile.Click += new EventHandler(mitPLoadDiagnosticFile_Click);
      mitPSaveDiagnosticFile.Click += new EventHandler(mitPSaveDiagnosticFile_Click);
      mitSendDiagnosticEmail.Click += new EventHandler(mitSendDiagnosticEmail_Click);
      //
      //  Event - Help
      mitHTopic.Click += new EventHandler(mitHTopic_Click);
      mitHContents.Click += new EventHandler(mitHContents_Click);
      mitHSearch.Click += new EventHandler(mitHSearch_Click);
      mitHEnterApplicationProductKey.Click += new EventHandler(mitHEnterApplicationProductKey_Click);
      mitHEnterDeviceProductKey.Click += new EventHandler(mitHEnterDeviceProductKey_Click);
      mitHAbout.Click += new EventHandler(mitHAbout_Click);
    }

    private Boolean LoadInitdata(String directory, String filename)
    {
      Boolean Result = true;
      // Open     
      _Protocol(String.Format("Directory..: '{0}'", directory));
      _Protocol(String.Format("FileName...: '{0}'", filename));
      String FileEntry = Path.Combine(directory, filename);
      //
      CInitdataReader IDR = new CInitdataReader();
      // with Protocol: IDR.Open(FileEntry, INIT_SEGMENTBASE, InitdataOnNotify);
      // without Protocol: 
      IDR.Open(FileEntry, INIT_SEGMENTBASE, null);
      //
      // MainWindow
      IDR.OpenSection(CInitdata.NAME_MAINWINDOW);
      //
      Int32 IValue;
      String SValue = this.Text;
      // Main - WindowText
      Result &= IDR.ReadString(CInitdata.NAME_WINDOWTEXT, out SValue, SValue);
      this.Text = SValue;
      //
      // Main - WindowState / Left, Top, Width, Height
      CInitdata.LoadWindowStateBorder(IDR, this,
                                      INIT_WINDOWSTATE, 
                                      INIT_LEFT, INIT_TOP,
                                      INIT_WIDTH, INIT_HEIGHT);
      //
      // MainWindow - Autosave
      Boolean BValue = INIT_SAVEAUTOMATICATPROGRAMEND;
      IDR.ReadBoolean(NAME_SAVEAUTOMATICATPROGRAMEND, out BValue, BValue);
      mitCSaveAutomaticAtProgramEnd.Checked = BValue;
      //
      //-----------------------------------
      // Load/Create Notifier-Applications
      //-----------------------------------
      Int32 ApplicationCount = APPLICATION_COUNT;
      IDR.ReadInt32(CUCNotifier.NAME_APPLICATIONCOUNT, out ApplicationCount, ApplicationCount);
      ApplicationCount = Math.Max(APPLICATION_COUNT, ApplicationCount);
      for (Int32 AI = 0; AI < ApplicationCount; AI++)
      {
        String Name = String.Format(CUCNotifier.FORMAT_APPLICATIONPRODUCTKEY, AI);
        SValue = CUCNotifier.INIT_APPLICATIONPRODUCTKEY;
        IDR.ReadString(Name, out SValue, SValue);
        if (0 == AI)
        {
          Result &= FUCNotifier.AddApplication(APPLICATION_NAME, APPLICATION_VERSION, APPLICATION_DATE,
                                               COMPANYTARGETTOP, STREETTARGET, CITYTARGET,
                                               APPLICATION_DISCOUNTUSER, APPLICATION_USER, SValue);
        } else
        {
          String ModuleName = String.Format(FORMAT_MODULE_NAME, AI.ToString());
          Result &= FUCNotifier.AddApplication(ModuleName, MODULE_VERSION, MODULE_DATE,
                                               COMPANYTARGETTOP, STREETTARGET, CITYTARGET,
                                               MODULE_DISCOUNTUSER, MODULE_USER, SValue);
        }
      }
      Result &= (APPLICATION_COUNT == FUCNotifier.GetApplicationCount());
      //
      //-----------------------------------
      // Load/Create Notifier-Devices
      //-----------------------------------
      Int32 DeviceCount = DEVICE_COUNT_MINIMUM;
      IDR.ReadInt32(CUCNotifier.NAME_DEVICECOUNT, out DeviceCount, DeviceCount);
      DeviceCount = Math.Max(DEVICE_COUNT_MINIMUM, DeviceCount);
      for (Int32 DI = 0; DI < DeviceCount; DI++)
      {
        String Name = String.Format(CUCNotifier.FORMAT_DEVICESERIALNUMBER, DI);
        IValue = CUCNotifier.INIT_DEVICESERIALNUMBER;
        IDR.ReadInt32(Name, out IValue, IValue);
        //
        Name = String.Format(CUCNotifier.FORMAT_DEVICEPRODUCTKEY, DI);
        SValue = CUCNotifier.INIT_DEVICEPRODUCTKEY;
        IDR.ReadString(Name, out SValue, SValue);
        //
        Result &= FUCNotifier.AddDevice(DEVICE_NAME, DEVICE_VERSION, DEVICE_DATE,
                                        DEVICE_DISCOUNTUSER, DEVICE_USER,
                                        IValue, SValue);
      }
      Result &= (DEVICE_COUNT_MINIMUM <= FUCNotifier.GetDeviceCount());
      //
      // MainWindow
      IDR.CloseSection();
      //
      //
      // Notifier
      Result &= FUCNotifier.LoadInitdata(IDR);
      //
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      // LoadInitdata Programmer-Controls:
      LoadInitdataProgrammerControls(IDR);
      //
      // Close
      IDR.Close();
      IDR = null;
      //
      return Result;
    }

    private Boolean SaveInitdata(String directory, String filename)
    { // Open
      _Protocol(String.Format("Directory..: '{0}'", directory));
      _Protocol(String.Format("FileName...: '{0}'", filename));
      String FileEntry = Path.Combine(directory, filename);
      //
      Boolean Result = true;
      CInitdataWriter IDW = new CInitdataWriter();
      // with Protocol: IDW.Open(FileEntry, INIT_SEGMENTBASE, InitdataOnNotify);
      // without Protocol: 
      IDW.Open(FileEntry, INIT_SEGMENTBASE, null);
      //
      // MainWindow
      IDW.CreateSection(CInitdata.NAME_MAINWINDOW);
      //
      // Main - WindowText
      InitApplicationTitle();
      IDW.WriteString(CInitdata.NAME_TITLE, this.Text);
      //
      // Main - WindowState / Left, Top, Width, Height
      CInitdata.SaveWindowStateBorder(IDW, this);
      //
      // MainWindow - Autosave
      IDW.WriteBoolean(NAME_SAVEAUTOMATICATPROGRAMEND, mitCSaveAutomaticAtProgramEnd.Checked);      
      //
      //--------------------------------------------------------------
      // Save Notifier-Applications
      //--------------------------------------------------------------
      Int32 ApplicationCount = FUCNotifier.GetApplicationCount();
      ApplicationCount = Math.Max(APPLICATION_COUNT, ApplicationCount);
      IDW.WriteInt32(CUCNotifier.NAME_APPLICATIONCOUNT, ApplicationCount);
      for (Int32 AI = 0; AI < ApplicationCount; AI++)
      {
        String Name = String.Format(CUCNotifier.FORMAT_APPLICATIONPRODUCTKEY, AI);
        IDW.WriteString(Name, FUCNotifier.GetApplicationProductKey(AI));
      }
      //
      //-----------------------------------
      // Save Notifier-Devices
      //-----------------------------------
      Int32 DeviceCount = FUCNotifier.GetDeviceCount();
      DeviceCount = Math.Max(DEVICE_COUNT_MINIMUM, DeviceCount);
      IDW.WriteInt32(CUCNotifier.NAME_DEVICECOUNT, DeviceCount);
      for (Int32 DI = 0; DI < DeviceCount; DI++)
      {
        String Name = String.Format(CUCNotifier.FORMAT_DEVICESERIALNUMBER, DI);
        IDW.WriteInt32(Name, FUCNotifier.GetDeviceSerialNumber(DI));
        //
        Name = String.Format(CUCNotifier.FORMAT_DEVICEPRODUCTKEY, DI);
        IDW.WriteString(Name, FUCNotifier.GetDeviceProductKey(DI));
      }
      //
      // MainWindow
      IDW.CloseSection();
      //
      //
      // Notifier
      Result &= FUCNotifier.SaveInitdata(IDW);
      //
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      // SaveInitdata Programmer-Controls:
      SaveInitdataProgrammerControls(IDW);
      //
      // Close
      IDW.Close();
      IDW = null;
      return Result;
    }
    //
    //----------------------------------------
    //  Section - Private Helper
    //----------------------------------------
    //

    //
    //------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------
    //
    private String BuildHeader()
    {
      return String.Format("{0}", HEADER_LIBRARY);
    }

    public void _Error(EErrorCode code)
    {
      if (FUCNotifier is CUCNotifier)
      {
        Int32 Index = (Int32)code;
        if ((Index < 0) && (ERRORS.Length <= Index))
        {
          Index = ERRORS.Length - 1;
        }
        String Line = String.Format("Error[{0}]: {1}!", Index, ERRORS[Index]);
        FUCNotifier.Write(BuildHeader(), Line);
      }
    }

    public void _Error(EErrorCode code, String text)
    {
      if (FUCNotifier is CUCNotifier)
      {
        String Line = String.Format("ERROR[{0}]: {1}!", code, text);
        FUCNotifier.Write(BuildHeader(), Line);
      }
    }

    public void _Protocol(String line)
    {
      if (FUCNotifier is CUCNotifier)
      {
        FUCNotifier.Write(BuildHeader(), line);
      }
    }

    //
    //----------------------------------------
    //  Section - Private Helper
    //----------------------------------------
    //

    //
    //----------------------------------------
    //  Section - Startup
    //----------------------------------------
    //
    private void tmrStartup_Tick(object sender, EventArgs e)
    {
      /*switch (FStartupState)
      {
        case 0:
          //...
          break;
        case 1:
          break;
        case 2:
          break;
        case 3:
          break;
        case 4:
          break;
        case 5:
          break;
        case 6:
          break;
        case 7:
          break;
        case 8:
          break;
        case 9:
          break;
        case 10:
          break;
        default:
          tmrStartup.Enabled = false;
          break;
      }
      FStartupState++;
       */
    }
    //
    //----------------------------------------
    //  Menu-Event-Methods : Help
    //----------------------------------------
    //
    private void mitHContents_Click(object sender, EventArgs e)
    {
      Help.ShowHelp(this, MAIN_HELPFILE, HelpNavigator.TableOfContents);
    }

    private void mitHTopic_Click(object sender, EventArgs e)
    {
      Help.ShowHelp(this, MAIN_HELPFILE, HelpNavigator.Topic, "./Main/MIndex.html");
    }

    private void mitHSearch_Click(object sender, EventArgs e)
    {
      Help.ShowHelp(this, MAIN_HELPFILE, HelpNavigator.KeywordIndex);
    }

    private void mitHEnterApplicationProductKey_Click(object sender, EventArgs e)
    {
      if (FUCNotifier.ShowApplicationProductKeyModal())
      {
        if (FUCNotifier.ShowSaveQuestionModal())
        {
          SaveInitdata(FInitdataDirectory, FInitdataFilename);
          Close();
        }
      }
    }

    private void mitHEnterDeviceProductKey_Click(object sender, EventArgs e)
    {
      if (FUCNotifier.ShowDeviceProductKeyModal())
      {
        if (FUCNotifier.ShowSaveQuestionModal())
        {
          SaveInitdata(FInitdataDirectory, FInitdataFilename);
          Close();
        }
      }
    }

    private void mitHAbout_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = ":";
      _Protocol(Line);
      _Protocol("Call About-Dialog");
      CInitdata ID = new CInitdata();
      ID.SetApplicationData(APPLICATION_NAME, APPLICATION_VERSION, APPLICATION_DATE);
      ID.SetDescriptionData(DESCRIPTION, POINT1, POINT2, POINT3);
      ID.SetCompanySourceData(COMPANYSOURCETOP, COMPANYSOURCEBOTTOM,
                              STREETSOURCE, CITYSOURCE);
      ID.SetCompanyTargetData(COMPANYTARGETTOP, COMPANYTARGETBOTTOM, STREETTARGET, CITYTARGET);
      RNotifierData NotifierData;
      FUCNotifier.GetData(out NotifierData);
      ID.SetProductKey(FUCNotifier.GetApplicationProductKey(0));
      ID.SetIcon(this.Icon);
      ID.ShowModalDialogAbout();
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }
    //
    //----------------------------------------
    //  Menu-Event-Methods : System
    //----------------------------------------
    //
    private void mitSQuit_Click(object sender, EventArgs e)
    {
      _Protocol("Exit Application");
      Close();
    }
    //
    //--------------------------------------------------------------
    //  Section - Menu-Event-Methods: Protocol
    //--------------------------------------------------------------
    //
    private void mitPEditParameter_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Edit Protocol-Parameter:";
      _Protocol(Line);
      Result &= FUCNotifier.EditParameterModal();
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitPClearProtocol_Click(object sender, EventArgs e)
    {
      String Line = "Clear Protocol.";
      _Protocol(Line);
      FUCNotifier.Clear();
    }

    private void mitPCopyToClipboard_Click(object sender, EventArgs e)
    {
      String Line = "Copy Protocol to Clipboard.";
      _Protocol(Line);
      FUCNotifier.CopyToClipboard();
    }

    private void mitPLoadDiagnosticFile_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Load Diagnostic-File with Dialog:";
      _Protocol(Line);
      Result &= FUCNotifier.LoadDiagnosticFileModal();
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitPSaveDiagnosticFile_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Save Diagnostic-File with Dialog:";
      _Protocol(Line);
      Result &= FUCNotifier.SaveDiagnosticFileModal();
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitSendDiagnosticEmail_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Send Diagnostic-Email:";
      _Protocol(Line);
      Result &= FUCNotifier.SendDiagnosticEmail();
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }
    //
    //--------------------------------------------------------------
    //  Section - Menu-Event-Methods : Configuration
    //--------------------------------------------------------------
    //
    private void mitCResetToDefaultConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Reset to Default-Configuration:";
      _Protocol(Line);
      Result &= CInitdata.DeleteInitdataFile(SUBDIRECTORY_TARGET,
                                             INIT_FILENAME,
                                             INIT_FILEEXTENSION);
      Result &= LoadInitdata(FInitdataDirectory, FInitdataFilename);
      Result &= SaveInitdata(FInitdataDirectory, FInitdataFilename);
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitCSaveAutomaticAtProgramEnd_Click(object sender, EventArgs e)
    {
      mitCSaveAutomaticAtProgramEnd.Checked = !mitCSaveAutomaticAtProgramEnd.Checked;
      String Line = String.Format("Save automatic at program end: -> {0}",
                                  mitCSaveAutomaticAtProgramEnd.Checked);
      _Protocol(Line);
    }

    private void mitCLoadStartupConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Load Startup-Configuration:";
      _Protocol(Line);
      Result &= LoadInitdata(FInitdataDirectory, FInitdataFilename);
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitCSaveStartupConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Save Startup-Configuration:";
      _Protocol(Line);
      Result &= SaveInitdata(FInitdataDirectory, FInitdataFilename);
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitCShowEditStartupConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Show / Edit Startup-Configuration:";
      _Protocol(Line);
      String FileEntry = Path.Combine(FInitdataDirectory, FInitdataFilename);
      CDialogTextEditor DialogTextEditor = new CDialogTextEditor();
      DialogTextEditor.Text = Line; // Title!
      DialogTextEditor.Width = 800;
      DialogTextEditor.Height = 600;
      DialogTextEditor.TextReadOnly = true;
      DialogTextEditor.LoadFileExtension = "ini.xml";
      DialogTextEditor.LoadFileName = APPLICATION_NAME;
      DialogTextEditor.LoadFileFilter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      DialogTextEditor.SaveFileExtension = "ini.xml";
      DialogTextEditor.SaveFileName = APPLICATION_NAME;
      DialogTextEditor.SaveFileFilter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      Result &= DialogTextEditor.LoadFromFile(FileEntry);
      if (Result)
      {
        if (DialogResult.OK == DialogTextEditor.ShowDialog())
        {
          if (DialogTextEditor.TextIsChanged)
          {
            Result &= DialogTextEditor.SaveToFile(FileEntry);
          }
        }
      }
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitCLoadSelectableConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Load Selectable-Configuration:";
      _Protocol(Line);
      DialogSaveInitdata.InitialDirectory = FInitdataDirectory;
      DialogSaveInitdata.FileName = FInitdataFilename;
      Result &= (DialogResult.OK == DialogLoadInitdata.ShowDialog());
      if (Result)
      {
        String Directory = Path.GetDirectoryName(DialogLoadInitdata.FileName);
        String FileName = Path.GetFileName(DialogLoadInitdata.FileName);
        Result &= LoadInitdata(Directory, FileName);
      }
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitCSaveSelectableConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Save Selectable-Configuration:";
      _Protocol(Line);
      DialogSaveInitdata.InitialDirectory = FInitdataDirectory;
      DialogSaveInitdata.FileName = FInitdataFilename;
      Result &= (DialogResult.OK == DialogSaveInitdata.ShowDialog());
      if (Result)
      {
        String Directory = Path.GetDirectoryName(DialogSaveInitdata.FileName);
        String FileName = Path.GetFileName(DialogSaveInitdata.FileName);
        Result &= SaveInitdata(Directory, FileName);
      }
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitCShowEditSelectableConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = false;
      String Line = "Show / Edit Selectable-Configuration:";
      _Protocol(Line);
      //
      DialogSaveInitdata.InitialDirectory = FInitdataDirectory;
      DialogSaveInitdata.FileName = FInitdataFilename;
      if (DialogResult.OK == DialogLoadInitdata.ShowDialog())
      {
        CDialogTextEditor DialogTextEditor = new CDialogTextEditor();
        DialogTextEditor.Text = Line; // Title!
        DialogTextEditor.Width = 800;
        DialogTextEditor.Height = 600;
        DialogTextEditor.TextReadOnly = true;
        DialogTextEditor.LoadFileExtension = "ini.xml";
        DialogTextEditor.LoadFileName = Path.GetFileNameWithoutExtension(DialogLoadInitdata.FileName);
        DialogTextEditor.LoadFileFilter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
        DialogTextEditor.SaveFileExtension = "ini.xml";
        DialogTextEditor.SaveFileName = Path.GetFileNameWithoutExtension(DialogLoadInitdata.FileName);
        DialogTextEditor.SaveFileFilter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
        Result = DialogTextEditor.LoadFromFile(DialogLoadInitdata.FileName);
        if (Result)
        {
          if (DialogResult.OK == DialogTextEditor.ShowDialog())
          {
            if (DialogTextEditor.TextIsChanged)
            {
              Result &= DialogTextEditor.SaveToFile(DialogLoadInitdata.FileName);
            }
          }
        }
      }
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }
  }
}
