﻿using UCKeyFrame;

namespace UCIT01KeyboardFront
{
  partial class CUCIT01KeyboardFront
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.FUCKeyBackstep = new UCKeyFrame.CUCKeySpecial();
      this.FUCKeyStop = new UCKeyFrame.CUCKeySpecial();
      this.FUCKeyStart = new UCKeyFrame.CUCKeySpecial();
      this.FUCKeyVolume = new UCKeyFrame.CUCKeySpecial();
      this.FUCKeyOn = new UCKeyFrame.CUCKeySpecial();
      this.FUCKeyOff = new UCKeyFrame.CUCKeySpecial();
      this.FUCKeyEnter = new UCKeyFrame.CUCKeySpecial();
      this.FUCKeyFunction = new UCKeyFrame.CUCKeySpecial();
      this.FUCKey0 = new UCKeyFrame.CUCKeyFront();
      this.FUCKey9 = new UCKeyFrame.CUCKeyFront();
      this.FUCKey8 = new UCKeyFrame.CUCKeyFront();
      this.FUCKey6 = new UCKeyFrame.CUCKeyFront();
      this.FUCKey5 = new UCKeyFrame.CUCKeyFront();
      this.FUCKey4 = new UCKeyFrame.CUCKeyFront();
      this.FUCKey7 = new UCKeyFrame.CUCKeyFront();
      this.FUCKey3 = new UCKeyFrame.CUCKeyFront();
      this.FUCKey2 = new UCKeyFrame.CUCKeyFront();
      this.FUCKey1 = new UCKeyFrame.CUCKeyFront();
      this.SuspendLayout();
      // 
      // FUCKeyBackstep
      // 
      this.FUCKeyBackstep.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKeyBackstep.BorderColor = System.Drawing.Color.Black;
      this.FUCKeyBackstep.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKeyBackstep.BorderWidth = 1;
      this.FUCKeyBackstep.CornerRadius = 6;
      this.FUCKeyBackstep.FillColor = System.Drawing.Color.DarkGray;
      this.FUCKeyBackstep.FillColorFocus = System.Drawing.Color.Gray;
      this.FUCKeyBackstep.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCKeyBackstep.KeyCharacter = '\r';
      this.FUCKeyBackstep.KeyCode = ((byte)(13));
      this.FUCKeyBackstep.KeyCommand = "CR";
      this.FUCKeyBackstep.Location = new System.Drawing.Point(1, 17);
      this.FUCKeyBackstep.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKeyBackstep.Name = "FUCKeyBackstep";
      this.FUCKeyBackstep.PressColor = System.Drawing.Color.Red;
      this.FUCKeyBackstep.Size = new System.Drawing.Size(80, 65);
      this.FUCKeyBackstep.TabIndex = 17;
      this.FUCKeyBackstep.TextColor = System.Drawing.Color.Black;
      this.FUCKeyBackstep.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKeyBackstep.Title = "BS";
      // 
      // FUCKeyStop
      // 
      this.FUCKeyStop.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKeyStop.BorderColor = System.Drawing.Color.Black;
      this.FUCKeyStop.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKeyStop.BorderWidth = 1;
      this.FUCKeyStop.CornerRadius = 6;
      this.FUCKeyStop.FillColor = System.Drawing.Color.Orange;
      this.FUCKeyStop.FillColorFocus = System.Drawing.Color.Orange;
      this.FUCKeyStop.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCKeyStop.KeyCharacter = '\r';
      this.FUCKeyStop.KeyCode = ((byte)(13));
      this.FUCKeyStop.KeyCommand = "CR";
      this.FUCKeyStop.Location = new System.Drawing.Point(0, 392);
      this.FUCKeyStop.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKeyStop.Name = "FUCKeyStop";
      this.FUCKeyStop.PressColor = System.Drawing.Color.Red;
      this.FUCKeyStop.Size = new System.Drawing.Size(80, 65);
      this.FUCKeyStop.TabIndex = 16;
      this.FUCKeyStop.TextColor = System.Drawing.Color.Black;
      this.FUCKeyStop.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKeyStop.Title = "STOP";
      // 
      // FUCKeyStart
      // 
      this.FUCKeyStart.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKeyStart.BorderColor = System.Drawing.Color.Black;
      this.FUCKeyStart.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKeyStart.BorderWidth = 1;
      this.FUCKeyStart.CornerRadius = 6;
      this.FUCKeyStart.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(165)))), ((int)(((byte)(70)))));
      this.FUCKeyStart.FillColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(165)))), ((int)(((byte)(70)))));
      this.FUCKeyStart.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCKeyStart.KeyCharacter = '\r';
      this.FUCKeyStart.KeyCode = ((byte)(13));
      this.FUCKeyStart.KeyCommand = "CR";
      this.FUCKeyStart.Location = new System.Drawing.Point(0, 253);
      this.FUCKeyStart.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKeyStart.Name = "FUCKeyStart";
      this.FUCKeyStart.PressColor = System.Drawing.Color.Red;
      this.FUCKeyStart.Size = new System.Drawing.Size(80, 65);
      this.FUCKeyStart.TabIndex = 15;
      this.FUCKeyStart.TextColor = System.Drawing.Color.Black;
      this.FUCKeyStart.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKeyStart.Title = "START";
      // 
      // FUCKeyVolume
      // 
      this.FUCKeyVolume.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKeyVolume.BorderColor = System.Drawing.Color.Black;
      this.FUCKeyVolume.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKeyVolume.BorderWidth = 1;
      this.FUCKeyVolume.CornerRadius = 6;
      this.FUCKeyVolume.FillColor = System.Drawing.Color.DarkGray;
      this.FUCKeyVolume.FillColorFocus = System.Drawing.Color.Gray;
      this.FUCKeyVolume.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCKeyVolume.KeyCharacter = '\r';
      this.FUCKeyVolume.KeyCode = ((byte)(13));
      this.FUCKeyVolume.KeyCommand = "CR";
      this.FUCKeyVolume.Location = new System.Drawing.Point(464, 392);
      this.FUCKeyVolume.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKeyVolume.Name = "FUCKeyVolume";
      this.FUCKeyVolume.PressColor = System.Drawing.Color.Red;
      this.FUCKeyVolume.Size = new System.Drawing.Size(80, 65);
      this.FUCKeyVolume.TabIndex = 14;
      this.FUCKeyVolume.TextColor = System.Drawing.Color.Black;
      this.FUCKeyVolume.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKeyVolume.Title = "VOL.";
      // 
      // FUCKeyOn
      // 
      this.FUCKeyOn.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKeyOn.BorderColor = System.Drawing.Color.Black;
      this.FUCKeyOn.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKeyOn.BorderWidth = 1;
      this.FUCKeyOn.CornerRadius = 6;
      this.FUCKeyOn.FillColor = System.Drawing.Color.DarkGray;
      this.FUCKeyOn.FillColorFocus = System.Drawing.Color.Gray;
      this.FUCKeyOn.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCKeyOn.KeyCharacter = '\r';
      this.FUCKeyOn.KeyCode = ((byte)(13));
      this.FUCKeyOn.KeyCommand = "CR";
      this.FUCKeyOn.Location = new System.Drawing.Point(304, 392);
      this.FUCKeyOn.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKeyOn.Name = "FUCKeyOn";
      this.FUCKeyOn.PressColor = System.Drawing.Color.Red;
      this.FUCKeyOn.Size = new System.Drawing.Size(80, 65);
      this.FUCKeyOn.TabIndex = 13;
      this.FUCKeyOn.TextColor = System.Drawing.Color.Black;
      this.FUCKeyOn.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKeyOn.Title = "ON";
      // 
      // FUCKeyOff
      // 
      this.FUCKeyOff.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKeyOff.BorderColor = System.Drawing.Color.Black;
      this.FUCKeyOff.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKeyOff.BorderWidth = 1;
      this.FUCKeyOff.CornerRadius = 6;
      this.FUCKeyOff.FillColor = System.Drawing.Color.DarkGray;
      this.FUCKeyOff.FillColorFocus = System.Drawing.Color.Gray;
      this.FUCKeyOff.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCKeyOff.KeyCharacter = '\r';
      this.FUCKeyOff.KeyCode = ((byte)(13));
      this.FUCKeyOff.KeyCommand = "CR";
      this.FUCKeyOff.Location = new System.Drawing.Point(144, 392);
      this.FUCKeyOff.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKeyOff.Name = "FUCKeyOff";
      this.FUCKeyOff.PressColor = System.Drawing.Color.Red;
      this.FUCKeyOff.Size = new System.Drawing.Size(80, 65);
      this.FUCKeyOff.TabIndex = 12;
      this.FUCKeyOff.TextColor = System.Drawing.Color.Black;
      this.FUCKeyOff.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKeyOff.Title = "OFF";
      // 
      // FUCKeyEnter
      // 
      this.FUCKeyEnter.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKeyEnter.BorderColor = System.Drawing.Color.Black;
      this.FUCKeyEnter.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKeyEnter.BorderWidth = 1;
      this.FUCKeyEnter.CornerRadius = 6;
      this.FUCKeyEnter.FillColor = System.Drawing.Color.DarkGray;
      this.FUCKeyEnter.FillColorFocus = System.Drawing.Color.Gray;
      this.FUCKeyEnter.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCKeyEnter.KeyCharacter = '\r';
      this.FUCKeyEnter.KeyCode = ((byte)(13));
      this.FUCKeyEnter.KeyCommand = "CR";
      this.FUCKeyEnter.Location = new System.Drawing.Point(450, 301);
      this.FUCKeyEnter.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKeyEnter.Name = "FUCKeyEnter";
      this.FUCKeyEnter.PressColor = System.Drawing.Color.Red;
      this.FUCKeyEnter.Size = new System.Drawing.Size(110, 65);
      this.FUCKeyEnter.TabIndex = 11;
      this.FUCKeyEnter.TextColor = System.Drawing.Color.Black;
      this.FUCKeyEnter.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKeyEnter.Title = "ENTER";
      // 
      // FUCKeyFunction
      // 
      this.FUCKeyFunction.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKeyFunction.BorderColor = System.Drawing.Color.Black;
      this.FUCKeyFunction.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKeyFunction.BorderWidth = 1;
      this.FUCKeyFunction.CornerRadius = 6;
      this.FUCKeyFunction.FillColor = System.Drawing.Color.DarkGray;
      this.FUCKeyFunction.FillColorFocus = System.Drawing.Color.Gray;
      this.FUCKeyFunction.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCKeyFunction.KeyCharacter = '\r';
      this.FUCKeyFunction.KeyCode = ((byte)(13));
      this.FUCKeyFunction.KeyCommand = "CR";
      this.FUCKeyFunction.Location = new System.Drawing.Point(129, 301);
      this.FUCKeyFunction.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKeyFunction.Name = "FUCKeyFunction";
      this.FUCKeyFunction.PressColor = System.Drawing.Color.Red;
      this.FUCKeyFunction.Size = new System.Drawing.Size(110, 65);
      this.FUCKeyFunction.TabIndex = 10;
      this.FUCKeyFunction.TextColor = System.Drawing.Color.Black;
      this.FUCKeyFunction.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKeyFunction.Title = "FUNCTION";
      // 
      // FUCKey0
      // 
      this.FUCKey0.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKey0.BorderColor = System.Drawing.Color.Black;
      this.FUCKey0.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKey0.BorderWidth = 1;
      this.FUCKey0.CornerRadius = 6;
      this.FUCKey0.FillColor = System.Drawing.Color.White;
      this.FUCKey0.FillColorFocus = System.Drawing.Color.White;
      this.FUCKey0.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCKey0.KeyCharacter = '0';
      this.FUCKey0.KeyCode = ((byte)(48));
      this.FUCKey0.KeyCommand = "0";
      this.FUCKey0.Location = new System.Drawing.Point(272, 301);
      this.FUCKey0.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKey0.Name = "FUCKey0";
      this.FUCKey0.PressColor = System.Drawing.Color.Red;
      this.FUCKey0.Size = new System.Drawing.Size(151, 71);
      this.FUCKey0.TabIndex = 9;
      this.FUCKey0.TextColor = System.Drawing.Color.Black;
      this.FUCKey0.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKey0.Title = "0";
      // 
      // FUCKey9
      // 
      this.FUCKey9.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKey9.BorderColor = System.Drawing.Color.Black;
      this.FUCKey9.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKey9.BorderWidth = 1;
      this.FUCKey9.CornerRadius = 6;
      this.FUCKey9.FillColor = System.Drawing.Color.White;
      this.FUCKey9.FillColorFocus = System.Drawing.Color.White;
      this.FUCKey9.Font = new System.Drawing.Font("Arial", 15.75F);
      this.FUCKey9.KeyCharacter = '0';
      this.FUCKey9.KeyCode = ((byte)(48));
      this.FUCKey9.KeyCommand = "0";
      this.FUCKey9.Location = new System.Drawing.Point(432, 201);
      this.FUCKey9.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKey9.Name = "FUCKey9";
      this.FUCKey9.PressColor = System.Drawing.Color.Red;
      this.FUCKey9.Size = new System.Drawing.Size(151, 71);
      this.FUCKey9.TabIndex = 8;
      this.FUCKey9.TextColor = System.Drawing.Color.Black;
      this.FUCKey9.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKey9.Title = "9";
      // 
      // FUCKey8
      // 
      this.FUCKey8.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKey8.BorderColor = System.Drawing.Color.Black;
      this.FUCKey8.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKey8.BorderWidth = 1;
      this.FUCKey8.CornerRadius = 6;
      this.FUCKey8.FillColor = System.Drawing.Color.White;
      this.FUCKey8.FillColorFocus = System.Drawing.Color.White;
      this.FUCKey8.Font = new System.Drawing.Font("Arial", 15.75F);
      this.FUCKey8.KeyCharacter = '0';
      this.FUCKey8.KeyCode = ((byte)(48));
      this.FUCKey8.KeyCommand = "0";
      this.FUCKey8.Location = new System.Drawing.Point(272, 201);
      this.FUCKey8.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKey8.Name = "FUCKey8";
      this.FUCKey8.PressColor = System.Drawing.Color.Red;
      this.FUCKey8.Size = new System.Drawing.Size(151, 71);
      this.FUCKey8.TabIndex = 7;
      this.FUCKey8.TextColor = System.Drawing.Color.Black;
      this.FUCKey8.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKey8.Title = "8";
      // 
      // FUCKey6
      // 
      this.FUCKey6.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKey6.BorderColor = System.Drawing.Color.Black;
      this.FUCKey6.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKey6.BorderWidth = 1;
      this.FUCKey6.CornerRadius = 6;
      this.FUCKey6.FillColor = System.Drawing.Color.White;
      this.FUCKey6.FillColorFocus = System.Drawing.Color.White;
      this.FUCKey6.Font = new System.Drawing.Font("Arial", 15.75F);
      this.FUCKey6.KeyCharacter = '0';
      this.FUCKey6.KeyCode = ((byte)(48));
      this.FUCKey6.KeyCommand = "0";
      this.FUCKey6.Location = new System.Drawing.Point(432, 101);
      this.FUCKey6.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKey6.Name = "FUCKey6";
      this.FUCKey6.PressColor = System.Drawing.Color.Red;
      this.FUCKey6.Size = new System.Drawing.Size(151, 71);
      this.FUCKey6.TabIndex = 6;
      this.FUCKey6.TextColor = System.Drawing.Color.Black;
      this.FUCKey6.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKey6.Title = "6";
      // 
      // FUCKey5
      // 
      this.FUCKey5.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKey5.BorderColor = System.Drawing.Color.Black;
      this.FUCKey5.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKey5.BorderWidth = 1;
      this.FUCKey5.CornerRadius = 6;
      this.FUCKey5.FillColor = System.Drawing.Color.White;
      this.FUCKey5.FillColorFocus = System.Drawing.Color.White;
      this.FUCKey5.Font = new System.Drawing.Font("Arial", 15.75F);
      this.FUCKey5.KeyCharacter = '0';
      this.FUCKey5.KeyCode = ((byte)(48));
      this.FUCKey5.KeyCommand = "0";
      this.FUCKey5.Location = new System.Drawing.Point(272, 101);
      this.FUCKey5.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKey5.Name = "FUCKey5";
      this.FUCKey5.PressColor = System.Drawing.Color.Red;
      this.FUCKey5.Size = new System.Drawing.Size(151, 71);
      this.FUCKey5.TabIndex = 5;
      this.FUCKey5.TextColor = System.Drawing.Color.Black;
      this.FUCKey5.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKey5.Title = "5";
      // 
      // FUCKey4
      // 
      this.FUCKey4.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKey4.BorderColor = System.Drawing.Color.Black;
      this.FUCKey4.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKey4.BorderWidth = 1;
      this.FUCKey4.CornerRadius = 6;
      this.FUCKey4.FillColor = System.Drawing.Color.White;
      this.FUCKey4.FillColorFocus = System.Drawing.Color.White;
      this.FUCKey4.Font = new System.Drawing.Font("Arial", 15.75F);
      this.FUCKey4.KeyCharacter = '0';
      this.FUCKey4.KeyCode = ((byte)(48));
      this.FUCKey4.KeyCommand = "0";
      this.FUCKey4.Location = new System.Drawing.Point(112, 101);
      this.FUCKey4.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKey4.Name = "FUCKey4";
      this.FUCKey4.PressColor = System.Drawing.Color.Red;
      this.FUCKey4.Size = new System.Drawing.Size(151, 71);
      this.FUCKey4.TabIndex = 4;
      this.FUCKey4.TextColor = System.Drawing.Color.Black;
      this.FUCKey4.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKey4.Title = "4";
      // 
      // FUCKey7
      // 
      this.FUCKey7.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKey7.BorderColor = System.Drawing.Color.Black;
      this.FUCKey7.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKey7.BorderWidth = 1;
      this.FUCKey7.CornerRadius = 6;
      this.FUCKey7.FillColor = System.Drawing.Color.White;
      this.FUCKey7.FillColorFocus = System.Drawing.Color.White;
      this.FUCKey7.Font = new System.Drawing.Font("Arial", 15.75F);
      this.FUCKey7.KeyCharacter = '0';
      this.FUCKey7.KeyCode = ((byte)(48));
      this.FUCKey7.KeyCommand = "0";
      this.FUCKey7.Location = new System.Drawing.Point(112, 201);
      this.FUCKey7.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKey7.Name = "FUCKey7";
      this.FUCKey7.PressColor = System.Drawing.Color.Red;
      this.FUCKey7.Size = new System.Drawing.Size(151, 71);
      this.FUCKey7.TabIndex = 3;
      this.FUCKey7.TextColor = System.Drawing.Color.Black;
      this.FUCKey7.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKey7.Title = "7";
      // 
      // FUCKey3
      // 
      this.FUCKey3.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKey3.BorderColor = System.Drawing.Color.Black;
      this.FUCKey3.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKey3.BorderWidth = 1;
      this.FUCKey3.CornerRadius = 6;
      this.FUCKey3.FillColor = System.Drawing.Color.White;
      this.FUCKey3.FillColorFocus = System.Drawing.Color.White;
      this.FUCKey3.Font = new System.Drawing.Font("Arial", 15.75F);
      this.FUCKey3.KeyCharacter = '0';
      this.FUCKey3.KeyCode = ((byte)(48));
      this.FUCKey3.KeyCommand = "0";
      this.FUCKey3.Location = new System.Drawing.Point(432, 1);
      this.FUCKey3.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKey3.Name = "FUCKey3";
      this.FUCKey3.PressColor = System.Drawing.Color.Red;
      this.FUCKey3.Size = new System.Drawing.Size(151, 71);
      this.FUCKey3.TabIndex = 2;
      this.FUCKey3.TextColor = System.Drawing.Color.Black;
      this.FUCKey3.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKey3.Title = "3";
      // 
      // FUCKey2
      // 
      this.FUCKey2.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKey2.BorderColor = System.Drawing.Color.Black;
      this.FUCKey2.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKey2.BorderWidth = 1;
      this.FUCKey2.CornerRadius = 6;
      this.FUCKey2.FillColor = System.Drawing.Color.White;
      this.FUCKey2.FillColorFocus = System.Drawing.Color.White;
      this.FUCKey2.Font = new System.Drawing.Font("Arial", 15.75F);
      this.FUCKey2.KeyCharacter = '0';
      this.FUCKey2.KeyCode = ((byte)(48));
      this.FUCKey2.KeyCommand = "0";
      this.FUCKey2.Location = new System.Drawing.Point(272, 1);
      this.FUCKey2.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKey2.Name = "FUCKey2";
      this.FUCKey2.PressColor = System.Drawing.Color.Red;
      this.FUCKey2.Size = new System.Drawing.Size(151, 71);
      this.FUCKey2.TabIndex = 1;
      this.FUCKey2.TextColor = System.Drawing.Color.Black;
      this.FUCKey2.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKey2.Title = "2";
      // 
      // FUCKey1
      // 
      this.FUCKey1.BackColor = System.Drawing.SystemColors.Desktop;
      this.FUCKey1.BorderColor = System.Drawing.Color.Black;
      this.FUCKey1.BorderColorFocus = System.Drawing.Color.Black;
      this.FUCKey1.BorderWidth = 1;
      this.FUCKey1.CornerRadius = 6;
      this.FUCKey1.FillColor = System.Drawing.Color.White;
      this.FUCKey1.FillColorFocus = System.Drawing.Color.White;
      this.FUCKey1.Font = new System.Drawing.Font("Arial", 15.75F);
      this.FUCKey1.KeyCharacter = '0';
      this.FUCKey1.KeyCode = ((byte)(48));
      this.FUCKey1.KeyCommand = "0";
      this.FUCKey1.Location = new System.Drawing.Point(112, 1);
      this.FUCKey1.Margin = new System.Windows.Forms.Padding(6);
      this.FUCKey1.Name = "FUCKey1";
      this.FUCKey1.PressColor = System.Drawing.Color.Red;
      this.FUCKey1.Size = new System.Drawing.Size(151, 71);
      this.FUCKey1.TabIndex = 0;
      this.FUCKey1.TextColor = System.Drawing.Color.Black;
      this.FUCKey1.TextColorFocus = System.Drawing.Color.Black;
      this.FUCKey1.Title = "1";
      // 
      // CUCIT01KeyboardFront
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Desktop;
      this.Controls.Add(this.FUCKeyBackstep);
      this.Controls.Add(this.FUCKeyStop);
      this.Controls.Add(this.FUCKeyStart);
      this.Controls.Add(this.FUCKeyVolume);
      this.Controls.Add(this.FUCKeyOn);
      this.Controls.Add(this.FUCKeyOff);
      this.Controls.Add(this.FUCKeyEnter);
      this.Controls.Add(this.FUCKeyFunction);
      this.Controls.Add(this.FUCKey0);
      this.Controls.Add(this.FUCKey9);
      this.Controls.Add(this.FUCKey8);
      this.Controls.Add(this.FUCKey6);
      this.Controls.Add(this.FUCKey5);
      this.Controls.Add(this.FUCKey4);
      this.Controls.Add(this.FUCKey7);
      this.Controls.Add(this.FUCKey3);
      this.Controls.Add(this.FUCKey2);
      this.Controls.Add(this.FUCKey1);
      this.Name = "CUCIT01KeyboardFront";
      this.Size = new System.Drawing.Size(586, 458);
      this.ResumeLayout(false);

    }

    #endregion

    private CUCKeyFront FUCKey1;
    private CUCKeyFront FUCKey2;
    private CUCKeyFront FUCKey7;
    private CUCKeyFront FUCKey3;
    private CUCKeyFront FUCKey8;
    private CUCKeyFront FUCKey6;
    private CUCKeyFront FUCKey5;
    private CUCKeyFront FUCKey4;
    private CUCKeyFront FUCKey9;
    private CUCKeyFront FUCKey0;
    private CUCKeySpecial FUCKeyFunction;
    private CUCKeySpecial FUCKeyEnter;
    private CUCKeySpecial FUCKeyOff;
    private CUCKeySpecial FUCKeyOn;
    private CUCKeySpecial FUCKeyVolume;
    private CUCKeySpecial FUCKeyStop;
    private CUCKeySpecial FUCKeyStart;
    private CUCKeySpecial FUCKeyBackstep;
  }
}
