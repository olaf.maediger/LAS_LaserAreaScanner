﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UCNotifier;
using UCKeyFrame;
// --> Main, Callback using IT01System;
//
namespace UCIT01KeyboardFront
{
  public delegate void DOnKeyMatrixChanged(Byte[] keycodes, Byte[] keystates);

  public partial class CUCIT01KeyboardFront : UserControl
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    private DOnKeyMatrixChanged FOnKeyMatrixChanged;
    private Byte[] FKeyCodes;
    private Byte[] FKeyStates;

    public CUCIT01KeyboardFront()
    {
      InitializeComponent();
      //
      FKeyCodes = new Byte[18];
      FKeyStates = new Byte[18];
      //      
      FUCKey0.SetOnKeyDown(OnKeyDown);
      FUCKey0.SetOnKeyUp(OnKeyUp);
      FUCKey0.SetNotifier(FNotifier);
      FUCKey0.KeyCode = (Byte)'0';
      FUCKey0.KeyCharacter = '0';
      FUCKey0.KeyCommand = "0";
      FKeyCodes[0x00] = FUCKey0.KeyCode;
      //
      FUCKey1.SetOnKeyDown(OnKeyDown);
      FUCKey1.SetOnKeyUp(OnKeyUp);
      FUCKey1.SetNotifier(FNotifier);
      FUCKey1.KeyCode = (Byte)'1';
      FUCKey1.KeyCharacter = '1';
      FUCKey1.KeyCommand = "1";
      FKeyCodes[0x01] = FUCKey1.KeyCode;
      //
      FUCKey2.SetOnKeyDown(OnKeyDown);
      FUCKey2.SetOnKeyUp(OnKeyUp);
      FUCKey2.SetNotifier(FNotifier);
      FUCKey2.KeyCode = (Byte)'2';
      FUCKey2.KeyCharacter = '2';
      FUCKey2.KeyCommand = "2";
      FKeyCodes[0x02] = FUCKey2.KeyCode;
      //
      FUCKey3.SetOnKeyDown(OnKeyDown);
      FUCKey3.SetOnKeyUp(OnKeyUp);
      FUCKey3.SetNotifier(FNotifier);
      FUCKey3.KeyCode = (Byte)'3';
      FUCKey3.KeyCharacter = '3';
      FUCKey3.KeyCommand = "3";
      FKeyCodes[0x03] = FUCKey3.KeyCode;
      //
      FUCKey4.SetOnKeyDown(OnKeyDown);
      FUCKey4.SetOnKeyUp(OnKeyUp);
      FUCKey4.SetNotifier(FNotifier);
      FUCKey4.KeyCode = (Byte)'4';
      FUCKey4.KeyCharacter = '4';
      FUCKey4.KeyCommand = "4";
      FKeyCodes[0x04] = FUCKey4.KeyCode;
      //
      FUCKey5.SetOnKeyDown(OnKeyDown);
      FUCKey5.SetOnKeyUp(OnKeyUp);
      FUCKey5.SetNotifier(FNotifier);
      FUCKey5.KeyCode = (Byte)'5';
      FUCKey5.KeyCharacter = '5';
      FUCKey5.KeyCommand = "5";
      FKeyCodes[0x05] = FUCKey5.KeyCode;
      //
      FUCKey6.SetOnKeyDown(OnKeyDown);
      FUCKey6.SetOnKeyUp(OnKeyUp);
      FUCKey6.SetNotifier(FNotifier);
      FUCKey6.KeyCode = (Byte)'6';
      FUCKey6.KeyCharacter = '6';
      FUCKey6.KeyCommand = "6";
      FKeyCodes[0x06] = FUCKey6.KeyCode;
      //
      FUCKey7.SetOnKeyDown(OnKeyDown);
      FUCKey7.SetOnKeyUp(OnKeyUp);
      FUCKey7.SetNotifier(FNotifier);
      FUCKey7.KeyCode = (Byte)'7';
      FUCKey7.KeyCharacter = '7';
      FUCKey7.KeyCommand = "7";
      FKeyCodes[0x07] = FUCKey7.KeyCode;
      //
      FUCKey8.SetOnKeyDown(OnKeyDown);
      FUCKey8.SetOnKeyUp(OnKeyUp);
      FUCKey8.SetNotifier(FNotifier);
      FUCKey8.KeyCode = (Byte)'8';
      FUCKey8.KeyCharacter = '8';
      FUCKey8.KeyCommand = "8";
      FKeyCodes[0x08] = FUCKey8.KeyCode;
      //
      FUCKey9.SetOnKeyDown(OnKeyDown);
      FUCKey9.SetOnKeyUp(OnKeyUp);
      FUCKey9.SetNotifier(FNotifier);
      FUCKey9.KeyCode = (Byte)'9';
      FUCKey9.KeyCharacter = '9';
      FUCKey9.KeyCommand = "9";
      FKeyCodes[0x09] = FUCKey9.KeyCode;
      //
      FUCKeyEnter.SetOnKeyDown(OnKeyDown);
      FUCKeyEnter.SetOnKeyUp(OnKeyUp);
      FUCKeyEnter.SetNotifier(FNotifier);
      FUCKeyEnter.KeyCode = 0x0D;
      FUCKeyEnter.KeyCharacter = (Char)0x0D;
      FUCKeyEnter.KeyCommand = "CR";
      FKeyCodes[0x0A] = FUCKeyEnter.KeyCode;
      //
      FUCKeyFunction.SetOnKeyDown(OnKeyDown);
      FUCKeyFunction.SetOnKeyUp(OnKeyUp);
      FUCKeyFunction.SetNotifier(FNotifier);
      FUCKeyFunction.KeyCode = 0x06;
      FUCKeyFunction.KeyCharacter = (Char)0x06;
      FUCKeyFunction.KeyCommand = "FUNC";
      FKeyCodes[0x0B] = FUCKeyFunction.KeyCode;
      //
      FUCKeyOff.SetOnKeyDown(OnKeyDown);
      FUCKeyOff.SetOnKeyUp(OnKeyUp);
      FUCKeyOff.SetNotifier(FNotifier);
      FUCKeyOff.KeyCode = 0x0F;
      FUCKeyOff.KeyCharacter = (Char)0x0F;
      FUCKeyOff.KeyCommand = "OFF";
      FKeyCodes[0x0C] = FUCKeyOff.KeyCode;
      //
      FUCKeyOn.SetOnKeyDown(OnKeyDown);
      FUCKeyOn.SetOnKeyUp(OnKeyUp);
      FUCKeyOn.SetNotifier(FNotifier);
      FUCKeyOn.KeyCode = 0x0E;
      FUCKeyOn.KeyCharacter = (Char)0x0E;
      FUCKeyOn.KeyCommand = "ON";
      FKeyCodes[0x0D] = FUCKeyOn.KeyCode;
      //
      FUCKeyStart.SetOnKeyDown(OnKeyDown);
      FUCKeyStart.SetOnKeyUp(OnKeyUp);
      FUCKeyStart.SetNotifier(FNotifier);
      FUCKeyStart.KeyCode = 0x1F;
      FUCKeyStart.KeyCharacter = (Char)0x1F;
      FUCKeyStart.KeyCommand = "START";
      FKeyCodes[0x0E] = FUCKeyStart.KeyCode;
      //
      FUCKeyStop.SetOnKeyDown(OnKeyDown);
      FUCKeyStop.SetOnKeyUp(OnKeyUp);
      FUCKeyStop.SetNotifier(FNotifier);
      FUCKeyStop.KeyCode = 0x7C;
      FUCKeyStop.KeyCharacter = (Char)0x7C;
      FUCKeyStop.KeyCommand = "STOP";
      FKeyCodes[0x0F] = FUCKeyStop.KeyCode;
      //
      FUCKeyVolume.SetOnKeyDown(OnKeyDown);
      FUCKeyVolume.SetOnKeyUp(OnKeyUp);
      FUCKeyVolume.SetNotifier(FNotifier);
      FUCKeyVolume.KeyCode = 0x7E;
      FUCKeyVolume.KeyCharacter = (Char)0x7E;
      FUCKeyVolume.KeyCommand = "VOL";
      FKeyCodes[0x10] = FUCKeyVolume.KeyCode;
      //
      FUCKeyBackstep.SetOnKeyDown(OnKeyDown);
      FUCKeyBackstep.SetOnKeyUp(OnKeyUp);
      FUCKeyBackstep.SetNotifier(FNotifier);
      FUCKeyBackstep.KeyCode = 0x08;
      FUCKeyBackstep.KeyCharacter = (Char)0x08;
      FUCKeyBackstep.KeyCommand = "BS";
      FKeyCodes[0x11] = FUCKeyBackstep.KeyCode;
    }


    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

    /*// --> Main, Callback public void SetIT01System(CIT01System value)
    {
      FIT01System = value;
    }*/

    public void SetOnKeyMatrixChanged(DOnKeyMatrixChanged value)
    {
      FOnKeyMatrixChanged = value;
    }



    private Boolean IncrementKeyCode(Byte keycode)
    {
      for (Byte KI = 0; KI < FKeyCodes.Length; KI++)
      {
        if (keycode == FKeyCodes[KI])
        {
          FKeyStates[KI]++;
          return true;
        }
      }
      return false;
    }
    private Boolean DecrementKeyCode(Byte keycode)
    {
      for (Byte KI = 0; KI < FKeyCodes.Length; KI++)
      {
        if (keycode == FKeyCodes[KI])
        {
          FKeyStates[KI]--;
          return true;
        }
      }
      return false;
    }


    private void OnKeyDown(Byte keycode, 
                           Char keycharacter, 
                           String keycommand)
    {
      if (FOnKeyMatrixChanged is DOnKeyMatrixChanged)
      {
        IncrementKeyCode(keycode);
        FOnKeyMatrixChanged(FKeyCodes, FKeyStates);
      }
    }

    private void OnKeyUp(Byte keycode,
                         Char keycharacter,
                         String keycommand)
    {
      if (FOnKeyMatrixChanged is DOnKeyMatrixChanged)
      {
        DecrementKeyCode(keycode);
        FOnKeyMatrixChanged(FKeyCodes, FKeyStates);
      }
    }

    public void PressKey(Char keycharacter)
    {
      switch (keycharacter)
      {
        case (Char)0x08:
          FUCKeyBackstep.PressKey();
          break;
        case (Char)48:
          FUCKey0.PressKey();
          break;
        case (Char)49:
          FUCKey1.PressKey();
          break;
        case (Char)50:
          FUCKey2.PressKey();
          break;
        case (Char)51:
          FUCKey3.PressKey();
          break;
        case (Char)52:
          FUCKey4.PressKey();
          break;
        case (Char)53:
          FUCKey5.PressKey();
          break;
        case (Char)54:
          FUCKey6.PressKey();
          break;
        case (Char)55:
          FUCKey7.PressKey();
          break;
        case (Char)56:
          FUCKey8.PressKey();
          break;
        case (Char)57:
          FUCKey9.PressKey();
          break;
        case (Char)70: // F - Function
          FUCKeyFunction.PressKey();
          break;
        case (Char)13: // CR - Enter
          FUCKeyEnter.PressKey();
          break;
        case (Char)78: // N - On
          FUCKeyOn.PressKey();
          break;
        case (Char)79: // O - Off
          FUCKeyOff.PressKey();
          break;
        case (Char)84: // T - Start
          FUCKeyStart.PressKey();
          break;
        case (Char)80: // P - Stop
          FUCKeyStop.PressKey();
          break;
        case (Char)86: // V - Volume
          FUCKeyVolume.PressKey();
          break;
      }
    }

    public void PressKey(KeyEventArgs arguments)
    {
      PressKey((Char)arguments.KeyValue);      
    }
  }
}
