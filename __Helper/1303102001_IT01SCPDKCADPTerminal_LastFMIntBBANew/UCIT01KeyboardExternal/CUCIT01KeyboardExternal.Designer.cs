﻿namespace UCIT01KeyboardExternal
{
  partial class CUCIT01KeyboardExternal
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlStatus = new System.Windows.Forms.Panel();
      this.rbtCapsLock = new System.Windows.Forms.RadioButton();
      this.panel1 = new System.Windows.Forms.Panel();
      this.btn3 = new System.Windows.Forms.Button();
      this.button13 = new System.Windows.Forms.Button();
      this.button14 = new System.Windows.Forms.Button();
      this.button17 = new System.Windows.Forms.Button();
      this.button18 = new System.Windows.Forms.Button();
      this.button19 = new System.Windows.Forms.Button();
      this.button20 = new System.Windows.Forms.Button();
      this.button9 = new System.Windows.Forms.Button();
      this.button10 = new System.Windows.Forms.Button();
      this.button7 = new System.Windows.Forms.Button();
      this.button8 = new System.Windows.Forms.Button();
      this.button5 = new System.Windows.Forms.Button();
      this.button6 = new System.Windows.Forms.Button();
      this.button3 = new System.Windows.Forms.Button();
      this.button4 = new System.Windows.Forms.Button();
      this.btnF1 = new System.Windows.Forms.Button();
      this.button1 = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.button25 = new System.Windows.Forms.Button();
      this.button11 = new System.Windows.Forms.Button();
      this.button26 = new System.Windows.Forms.Button();
      this.btnBackslash = new System.Windows.Forms.Button();
      this.btn0 = new System.Windows.Forms.Button();
      this.btn9 = new System.Windows.Forms.Button();
      this.btn8 = new System.Windows.Forms.Button();
      this.btn7 = new System.Windows.Forms.Button();
      this.btn6 = new System.Windows.Forms.Button();
      this.btn5 = new System.Windows.Forms.Button();
      this.btn4 = new System.Windows.Forms.Button();
      this.btn33 = new System.Windows.Forms.Button();
      this.btn2 = new System.Windows.Forms.Button();
      this.btn1 = new System.Windows.Forms.Button();
      this.panel3 = new System.Windows.Forms.Panel();
      this.button46 = new System.Windows.Forms.Button();
      this.button16 = new System.Windows.Forms.Button();
      this.button21 = new System.Windows.Forms.Button();
      this.button22 = new System.Windows.Forms.Button();
      this.btnP = new System.Windows.Forms.Button();
      this.btnO = new System.Windows.Forms.Button();
      this.btnI = new System.Windows.Forms.Button();
      this.btnU = new System.Windows.Forms.Button();
      this.btnY = new System.Windows.Forms.Button();
      this.btnT = new System.Windows.Forms.Button();
      this.btnR = new System.Windows.Forms.Button();
      this.btnE = new System.Windows.Forms.Button();
      this.btnW = new System.Windows.Forms.Button();
      this.btnQ = new System.Windows.Forms.Button();
      this.button45 = new System.Windows.Forms.Button();
      this.panel4 = new System.Windows.Forms.Panel();
      this.button47 = new System.Windows.Forms.Button();
      this.button48 = new System.Windows.Forms.Button();
      this.button49 = new System.Windows.Forms.Button();
      this.btnSubtract = new System.Windows.Forms.Button();
      this.btnL = new System.Windows.Forms.Button();
      this.btnK = new System.Windows.Forms.Button();
      this.btnJ = new System.Windows.Forms.Button();
      this.btnH = new System.Windows.Forms.Button();
      this.btnG = new System.Windows.Forms.Button();
      this.btnF = new System.Windows.Forms.Button();
      this.btnD = new System.Windows.Forms.Button();
      this.btnS = new System.Windows.Forms.Button();
      this.btnA = new System.Windows.Forms.Button();
      this.btnCapsLock = new System.Windows.Forms.Button();
      this.panel6 = new System.Windows.Forms.Panel();
      this.button75 = new System.Windows.Forms.Button();
      this.button76 = new System.Windows.Forms.Button();
      this.btnShiftRight = new System.Windows.Forms.Button();
      this.button78 = new System.Windows.Forms.Button();
      this.btnDecimalPoint = new System.Windows.Forms.Button();
      this.btnComma = new System.Windows.Forms.Button();
      this.btnM = new System.Windows.Forms.Button();
      this.btnN = new System.Windows.Forms.Button();
      this.btnB = new System.Windows.Forms.Button();
      this.btnV = new System.Windows.Forms.Button();
      this.btnC = new System.Windows.Forms.Button();
      this.btnX = new System.Windows.Forms.Button();
      this.btnZ = new System.Windows.Forms.Button();
      this.btnShiftLeft = new System.Windows.Forms.Button();
      this.panel5 = new System.Windows.Forms.Panel();
      this.button66 = new System.Windows.Forms.Button();
      this.button65 = new System.Windows.Forms.Button();
      this.button64 = new System.Windows.Forms.Button();
      this.button63 = new System.Windows.Forms.Button();
      this.button62 = new System.Windows.Forms.Button();
      this.button61 = new System.Windows.Forms.Button();
      this.btnSpace = new System.Windows.Forms.Button();
      this.button70 = new System.Windows.Forms.Button();
      this.button71 = new System.Windows.Forms.Button();
      this.button73 = new System.Windows.Forms.Button();
      this.button74 = new System.Windows.Forms.Button();
      this.panel7 = new System.Windows.Forms.Panel();
      this.radioButton1 = new System.Windows.Forms.RadioButton();
      this.radioButton2 = new System.Windows.Forms.RadioButton();
      this.panel8 = new System.Windows.Forms.Panel();
      this.radioButton3 = new System.Windows.Forms.RadioButton();
      this.panel9 = new System.Windows.Forms.Panel();
      this.radioButton4 = new System.Windows.Forms.RadioButton();
      this.panel10 = new System.Windows.Forms.Panel();
      this.pnlStatus.SuspendLayout();
      this.panel1.SuspendLayout();
      this.panel2.SuspendLayout();
      this.panel3.SuspendLayout();
      this.panel4.SuspendLayout();
      this.panel6.SuspendLayout();
      this.panel5.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlStatus
      // 
      this.pnlStatus.Controls.Add(this.radioButton4);
      this.pnlStatus.Controls.Add(this.panel10);
      this.pnlStatus.Controls.Add(this.radioButton3);
      this.pnlStatus.Controls.Add(this.panel9);
      this.pnlStatus.Controls.Add(this.radioButton2);
      this.pnlStatus.Controls.Add(this.panel8);
      this.pnlStatus.Controls.Add(this.radioButton1);
      this.pnlStatus.Controls.Add(this.panel7);
      this.pnlStatus.Controls.Add(this.rbtCapsLock);
      this.pnlStatus.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlStatus.Location = new System.Drawing.Point(0, 0);
      this.pnlStatus.Name = "pnlStatus";
      this.pnlStatus.Size = new System.Drawing.Size(647, 25);
      this.pnlStatus.TabIndex = 2;
      // 
      // rbtCapsLock
      // 
      this.rbtCapsLock.AutoSize = true;
      this.rbtCapsLock.Dock = System.Windows.Forms.DockStyle.Left;
      this.rbtCapsLock.Enabled = false;
      this.rbtCapsLock.Location = new System.Drawing.Point(0, 0);
      this.rbtCapsLock.Name = "rbtCapsLock";
      this.rbtCapsLock.Size = new System.Drawing.Size(76, 25);
      this.rbtCapsLock.TabIndex = 2;
      this.rbtCapsLock.TabStop = true;
      this.rbtCapsLock.Text = "Caps Lock";
      this.rbtCapsLock.UseVisualStyleBackColor = true;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.btn3);
      this.panel1.Controls.Add(this.button13);
      this.panel1.Controls.Add(this.button14);
      this.panel1.Controls.Add(this.button17);
      this.panel1.Controls.Add(this.button18);
      this.panel1.Controls.Add(this.button19);
      this.panel1.Controls.Add(this.button20);
      this.panel1.Controls.Add(this.button9);
      this.panel1.Controls.Add(this.button10);
      this.panel1.Controls.Add(this.button7);
      this.panel1.Controls.Add(this.button8);
      this.panel1.Controls.Add(this.button5);
      this.panel1.Controls.Add(this.button6);
      this.panel1.Controls.Add(this.button3);
      this.panel1.Controls.Add(this.button4);
      this.panel1.Controls.Add(this.btnF1);
      this.panel1.Controls.Add(this.button1);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 25);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(647, 36);
      this.panel1.TabIndex = 48;
      // 
      // btn3
      // 
      this.btn3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btn3.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn3.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn3.Location = new System.Drawing.Point(593, 0);
      this.btn3.Name = "btn3";
      this.btn3.Size = new System.Drawing.Size(49, 36);
      this.btn3.TabIndex = 43;
      this.btn3.Tag = "200";
      this.btn3.Text = "Call";
      this.btn3.UseVisualStyleBackColor = false;
      // 
      // button13
      // 
      this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button13.Dock = System.Windows.Forms.DockStyle.Left;
      this.button13.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button13.Location = new System.Drawing.Point(556, 0);
      this.button13.Name = "button13";
      this.button13.Size = new System.Drawing.Size(37, 36);
      this.button13.TabIndex = 42;
      this.button13.Tag = "144";
      this.button13.Text = "Num";
      this.button13.UseVisualStyleBackColor = false;
      // 
      // button14
      // 
      this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button14.Dock = System.Windows.Forms.DockStyle.Left;
      this.button14.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button14.Location = new System.Drawing.Point(518, 0);
      this.button14.Name = "button14";
      this.button14.Size = new System.Drawing.Size(38, 36);
      this.button14.TabIndex = 41;
      this.button14.Tag = "42";
      this.button14.Text = "Print";
      this.button14.UseVisualStyleBackColor = false;
      // 
      // button17
      // 
      this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button17.Dock = System.Windows.Forms.DockStyle.Left;
      this.button17.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button17.Location = new System.Drawing.Point(481, 0);
      this.button17.Name = "button17";
      this.button17.Size = new System.Drawing.Size(37, 36);
      this.button17.TabIndex = 40;
      this.button17.Tag = "124";
      this.button17.Text = "F13";
      this.button17.UseVisualStyleBackColor = false;
      // 
      // button18
      // 
      this.button18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button18.Dock = System.Windows.Forms.DockStyle.Left;
      this.button18.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button18.Location = new System.Drawing.Point(444, 0);
      this.button18.Name = "button18";
      this.button18.Size = new System.Drawing.Size(37, 36);
      this.button18.TabIndex = 39;
      this.button18.Tag = "123";
      this.button18.Text = "F12";
      this.button18.UseVisualStyleBackColor = false;
      // 
      // button19
      // 
      this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button19.Dock = System.Windows.Forms.DockStyle.Left;
      this.button19.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button19.Location = new System.Drawing.Point(407, 0);
      this.button19.Name = "button19";
      this.button19.Size = new System.Drawing.Size(37, 36);
      this.button19.TabIndex = 38;
      this.button19.Tag = "122";
      this.button19.Text = "F11";
      this.button19.UseVisualStyleBackColor = false;
      // 
      // button20
      // 
      this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button20.Dock = System.Windows.Forms.DockStyle.Left;
      this.button20.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button20.Location = new System.Drawing.Point(370, 0);
      this.button20.Name = "button20";
      this.button20.Size = new System.Drawing.Size(37, 36);
      this.button20.TabIndex = 37;
      this.button20.Tag = "121";
      this.button20.Text = "F10";
      this.button20.UseVisualStyleBackColor = false;
      // 
      // button9
      // 
      this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button9.Dock = System.Windows.Forms.DockStyle.Left;
      this.button9.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button9.Location = new System.Drawing.Point(333, 0);
      this.button9.Name = "button9";
      this.button9.Size = new System.Drawing.Size(37, 36);
      this.button9.TabIndex = 30;
      this.button9.Tag = "120";
      this.button9.Text = "F9";
      this.button9.UseVisualStyleBackColor = false;
      // 
      // button10
      // 
      this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button10.Dock = System.Windows.Forms.DockStyle.Left;
      this.button10.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button10.Location = new System.Drawing.Point(296, 0);
      this.button10.Name = "button10";
      this.button10.Size = new System.Drawing.Size(37, 36);
      this.button10.TabIndex = 29;
      this.button10.Tag = "119";
      this.button10.Text = "F8";
      this.button10.UseVisualStyleBackColor = false;
      // 
      // button7
      // 
      this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button7.Dock = System.Windows.Forms.DockStyle.Left;
      this.button7.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button7.Location = new System.Drawing.Point(259, 0);
      this.button7.Name = "button7";
      this.button7.Size = new System.Drawing.Size(37, 36);
      this.button7.TabIndex = 28;
      this.button7.Tag = "118";
      this.button7.Text = "F7";
      this.button7.UseVisualStyleBackColor = false;
      // 
      // button8
      // 
      this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button8.Dock = System.Windows.Forms.DockStyle.Left;
      this.button8.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button8.Location = new System.Drawing.Point(222, 0);
      this.button8.Name = "button8";
      this.button8.Size = new System.Drawing.Size(37, 36);
      this.button8.TabIndex = 27;
      this.button8.Tag = "117";
      this.button8.Text = "F6";
      this.button8.UseVisualStyleBackColor = false;
      // 
      // button5
      // 
      this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button5.Dock = System.Windows.Forms.DockStyle.Left;
      this.button5.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button5.Location = new System.Drawing.Point(185, 0);
      this.button5.Name = "button5";
      this.button5.Size = new System.Drawing.Size(37, 36);
      this.button5.TabIndex = 26;
      this.button5.Tag = "116";
      this.button5.Text = "F5";
      this.button5.UseVisualStyleBackColor = false;
      // 
      // button6
      // 
      this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button6.Dock = System.Windows.Forms.DockStyle.Left;
      this.button6.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button6.Location = new System.Drawing.Point(148, 0);
      this.button6.Name = "button6";
      this.button6.Size = new System.Drawing.Size(37, 36);
      this.button6.TabIndex = 25;
      this.button6.Tag = "115";
      this.button6.Text = "F4";
      this.button6.UseVisualStyleBackColor = false;
      // 
      // button3
      // 
      this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button3.Dock = System.Windows.Forms.DockStyle.Left;
      this.button3.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button3.Location = new System.Drawing.Point(111, 0);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(37, 36);
      this.button3.TabIndex = 24;
      this.button3.Tag = "114";
      this.button3.Text = "F3";
      this.button3.UseVisualStyleBackColor = false;
      // 
      // button4
      // 
      this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button4.Dock = System.Windows.Forms.DockStyle.Left;
      this.button4.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button4.Location = new System.Drawing.Point(74, 0);
      this.button4.Name = "button4";
      this.button4.Size = new System.Drawing.Size(37, 36);
      this.button4.TabIndex = 23;
      this.button4.Tag = "113";
      this.button4.Text = "F2";
      this.button4.UseVisualStyleBackColor = false;
      // 
      // btnF1
      // 
      this.btnF1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnF1.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnF1.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btnF1.Location = new System.Drawing.Point(37, 0);
      this.btnF1.Name = "btnF1";
      this.btnF1.Size = new System.Drawing.Size(37, 36);
      this.btnF1.TabIndex = 22;
      this.btnF1.Tag = "112";
      this.btnF1.Text = "F1";
      this.btnF1.UseVisualStyleBackColor = false;
      // 
      // button1
      // 
      this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button1.Dock = System.Windows.Forms.DockStyle.Left;
      this.button1.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button1.Location = new System.Drawing.Point(0, 0);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(37, 36);
      this.button1.TabIndex = 21;
      this.button1.Tag = "27";
      this.button1.Text = "Esc";
      this.button1.UseVisualStyleBackColor = false;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.button25);
      this.panel2.Controls.Add(this.button11);
      this.panel2.Controls.Add(this.button26);
      this.panel2.Controls.Add(this.btnBackslash);
      this.panel2.Controls.Add(this.btn0);
      this.panel2.Controls.Add(this.btn9);
      this.panel2.Controls.Add(this.btn8);
      this.panel2.Controls.Add(this.btn7);
      this.panel2.Controls.Add(this.btn6);
      this.panel2.Controls.Add(this.btn5);
      this.panel2.Controls.Add(this.btn4);
      this.panel2.Controls.Add(this.btn33);
      this.panel2.Controls.Add(this.btn2);
      this.panel2.Controls.Add(this.btn1);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(0, 61);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(647, 37);
      this.panel2.TabIndex = 52;
      // 
      // button25
      // 
      this.button25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button25.Dock = System.Windows.Forms.DockStyle.Left;
      this.button25.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button25.Location = new System.Drawing.Point(582, 0);
      this.button25.Name = "button25";
      this.button25.Size = new System.Drawing.Size(60, 37);
      this.button25.TabIndex = 55;
      this.button25.Tag = "36";
      this.button25.Text = "Home";
      this.button25.UseVisualStyleBackColor = false;
      // 
      // button11
      // 
      this.button11.BackColor = System.Drawing.SystemColors.Info;
      this.button11.Dock = System.Windows.Forms.DockStyle.Left;
      this.button11.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button11.Location = new System.Drawing.Point(538, 0);
      this.button11.Name = "button11";
      this.button11.Size = new System.Drawing.Size(44, 37);
      this.button11.TabIndex = 54;
      this.button11.Tag = "255";
      this.button11.Text = "<==";
      this.button11.UseVisualStyleBackColor = false;
      // 
      // button26
      // 
      this.button26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button26.Dock = System.Windows.Forms.DockStyle.Left;
      this.button26.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.button26.Location = new System.Drawing.Point(494, 0);
      this.button26.Name = "button26";
      this.button26.Size = new System.Drawing.Size(44, 37);
      this.button26.TabIndex = 52;
      this.button26.Tag = "187";
      this.button26.Text = "= +";
      this.button26.UseVisualStyleBackColor = false;
      // 
      // btnBackslash
      // 
      this.btnBackslash.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnBackslash.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnBackslash.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btnBackslash.Location = new System.Drawing.Point(450, 0);
      this.btnBackslash.Name = "btnBackslash";
      this.btnBackslash.Size = new System.Drawing.Size(44, 37);
      this.btnBackslash.TabIndex = 51;
      this.btnBackslash.Tag = "220";
      this.btnBackslash.Text = "\\";
      this.btnBackslash.UseVisualStyleBackColor = false;
      // 
      // btn0
      // 
      this.btn0.BackColor = System.Drawing.SystemColors.Info;
      this.btn0.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn0.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn0.Location = new System.Drawing.Point(406, 0);
      this.btn0.Name = "btn0";
      this.btn0.Size = new System.Drawing.Size(44, 37);
      this.btn0.TabIndex = 48;
      this.btn0.Tag = "48";
      this.btn0.Text = "0";
      this.btn0.UseVisualStyleBackColor = false;
      // 
      // btn9
      // 
      this.btn9.BackColor = System.Drawing.SystemColors.Info;
      this.btn9.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn9.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn9.Location = new System.Drawing.Point(362, 0);
      this.btn9.Name = "btn9";
      this.btn9.Size = new System.Drawing.Size(44, 37);
      this.btn9.TabIndex = 47;
      this.btn9.Tag = "57";
      this.btn9.Text = "9";
      this.btn9.UseVisualStyleBackColor = false;
      // 
      // btn8
      // 
      this.btn8.BackColor = System.Drawing.SystemColors.Info;
      this.btn8.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn8.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn8.Location = new System.Drawing.Point(318, 0);
      this.btn8.Name = "btn8";
      this.btn8.Size = new System.Drawing.Size(44, 37);
      this.btn8.TabIndex = 46;
      this.btn8.Tag = "56";
      this.btn8.Text = "8";
      this.btn8.UseVisualStyleBackColor = false;
      // 
      // btn7
      // 
      this.btn7.BackColor = System.Drawing.SystemColors.Info;
      this.btn7.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn7.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn7.Location = new System.Drawing.Point(274, 0);
      this.btn7.Name = "btn7";
      this.btn7.Size = new System.Drawing.Size(44, 37);
      this.btn7.TabIndex = 45;
      this.btn7.Tag = "55";
      this.btn7.Text = "7";
      this.btn7.UseVisualStyleBackColor = false;
      // 
      // btn6
      // 
      this.btn6.BackColor = System.Drawing.SystemColors.Info;
      this.btn6.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn6.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn6.Location = new System.Drawing.Point(230, 0);
      this.btn6.Name = "btn6";
      this.btn6.Size = new System.Drawing.Size(44, 37);
      this.btn6.TabIndex = 44;
      this.btn6.Tag = "54";
      this.btn6.Text = "6";
      this.btn6.UseVisualStyleBackColor = false;
      // 
      // btn5
      // 
      this.btn5.BackColor = System.Drawing.SystemColors.Info;
      this.btn5.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn5.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn5.Location = new System.Drawing.Point(186, 0);
      this.btn5.Name = "btn5";
      this.btn5.Size = new System.Drawing.Size(44, 37);
      this.btn5.TabIndex = 43;
      this.btn5.Tag = "53";
      this.btn5.Text = "5";
      this.btn5.UseVisualStyleBackColor = false;
      // 
      // btn4
      // 
      this.btn4.BackColor = System.Drawing.SystemColors.Info;
      this.btn4.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn4.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn4.Location = new System.Drawing.Point(142, 0);
      this.btn4.Name = "btn4";
      this.btn4.Size = new System.Drawing.Size(44, 37);
      this.btn4.TabIndex = 42;
      this.btn4.Tag = "52";
      this.btn4.Text = "4";
      this.btn4.UseVisualStyleBackColor = false;
      // 
      // btn33
      // 
      this.btn33.BackColor = System.Drawing.SystemColors.Info;
      this.btn33.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn33.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn33.Location = new System.Drawing.Point(98, 0);
      this.btn33.Name = "btn33";
      this.btn33.Size = new System.Drawing.Size(44, 37);
      this.btn33.TabIndex = 41;
      this.btn33.Tag = "51";
      this.btn33.Text = "3";
      this.btn33.UseVisualStyleBackColor = false;
      // 
      // btn2
      // 
      this.btn2.BackColor = System.Drawing.SystemColors.Info;
      this.btn2.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn2.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn2.Location = new System.Drawing.Point(54, 0);
      this.btn2.Name = "btn2";
      this.btn2.Size = new System.Drawing.Size(44, 37);
      this.btn2.TabIndex = 40;
      this.btn2.Tag = "50";
      this.btn2.Text = "2";
      this.btn2.UseVisualStyleBackColor = false;
      // 
      // btn1
      // 
      this.btn1.BackColor = System.Drawing.SystemColors.Info;
      this.btn1.Dock = System.Windows.Forms.DockStyle.Left;
      this.btn1.Font = new System.Drawing.Font("Arial", 7.3F, System.Drawing.FontStyle.Bold);
      this.btn1.Location = new System.Drawing.Point(0, 0);
      this.btn1.Name = "btn1";
      this.btn1.Size = new System.Drawing.Size(54, 37);
      this.btn1.TabIndex = 39;
      this.btn1.Tag = "49";
      this.btn1.Text = "1";
      this.btn1.UseVisualStyleBackColor = false;
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.button46);
      this.panel3.Controls.Add(this.button16);
      this.panel3.Controls.Add(this.button21);
      this.panel3.Controls.Add(this.button22);
      this.panel3.Controls.Add(this.btnP);
      this.panel3.Controls.Add(this.btnO);
      this.panel3.Controls.Add(this.btnI);
      this.panel3.Controls.Add(this.btnU);
      this.panel3.Controls.Add(this.btnY);
      this.panel3.Controls.Add(this.btnT);
      this.panel3.Controls.Add(this.btnR);
      this.panel3.Controls.Add(this.btnE);
      this.panel3.Controls.Add(this.btnW);
      this.panel3.Controls.Add(this.btnQ);
      this.panel3.Controls.Add(this.button45);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel3.Location = new System.Drawing.Point(0, 98);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(647, 37);
      this.panel3.TabIndex = 53;
      // 
      // button46
      // 
      this.button46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button46.Dock = System.Windows.Forms.DockStyle.Left;
      this.button46.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button46.Location = new System.Drawing.Point(607, 0);
      this.button46.Name = "button46";
      this.button46.Size = new System.Drawing.Size(35, 37);
      this.button46.TabIndex = 56;
      this.button46.Tag = "33";
      this.button46.Text = "Pg Up";
      this.button46.UseVisualStyleBackColor = false;
      // 
      // button16
      // 
      this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button16.Dock = System.Windows.Forms.DockStyle.Left;
      this.button16.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button16.Location = new System.Drawing.Point(567, 0);
      this.button16.Name = "button16";
      this.button16.Size = new System.Drawing.Size(40, 37);
      this.button16.TabIndex = 55;
      this.button16.Tag = "220";
      this.button16.Text = "| \\";
      this.button16.UseVisualStyleBackColor = false;
      // 
      // button21
      // 
      this.button21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button21.Dock = System.Windows.Forms.DockStyle.Left;
      this.button21.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button21.Location = new System.Drawing.Point(526, 0);
      this.button21.Name = "button21";
      this.button21.Size = new System.Drawing.Size(41, 37);
      this.button21.TabIndex = 54;
      this.button21.Tag = "221";
      this.button21.Text = "} ]";
      this.button21.UseVisualStyleBackColor = false;
      // 
      // button22
      // 
      this.button22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button22.Dock = System.Windows.Forms.DockStyle.Left;
      this.button22.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button22.Location = new System.Drawing.Point(486, 0);
      this.button22.Name = "button22";
      this.button22.Size = new System.Drawing.Size(40, 37);
      this.button22.TabIndex = 52;
      this.button22.Tag = "219";
      this.button22.Text = "{ [";
      this.button22.UseVisualStyleBackColor = false;
      // 
      // btnP
      // 
      this.btnP.BackColor = System.Drawing.SystemColors.Info;
      this.btnP.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnP.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnP.Location = new System.Drawing.Point(441, 0);
      this.btnP.Name = "btnP";
      this.btnP.Size = new System.Drawing.Size(45, 37);
      this.btnP.TabIndex = 51;
      this.btnP.Tag = "80";
      this.btnP.Text = "P";
      this.btnP.UseVisualStyleBackColor = false;
      // 
      // btnO
      // 
      this.btnO.BackColor = System.Drawing.SystemColors.Info;
      this.btnO.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnO.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnO.Location = new System.Drawing.Point(396, 0);
      this.btnO.Name = "btnO";
      this.btnO.Size = new System.Drawing.Size(45, 37);
      this.btnO.TabIndex = 48;
      this.btnO.Tag = "79";
      this.btnO.Text = "O";
      this.btnO.UseVisualStyleBackColor = false;
      // 
      // btnI
      // 
      this.btnI.BackColor = System.Drawing.SystemColors.Info;
      this.btnI.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnI.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnI.Location = new System.Drawing.Point(351, 0);
      this.btnI.Name = "btnI";
      this.btnI.Size = new System.Drawing.Size(45, 37);
      this.btnI.TabIndex = 47;
      this.btnI.Tag = "73";
      this.btnI.Text = "I";
      this.btnI.UseVisualStyleBackColor = false;
      // 
      // btnU
      // 
      this.btnU.BackColor = System.Drawing.SystemColors.Info;
      this.btnU.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnU.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnU.Location = new System.Drawing.Point(306, 0);
      this.btnU.Name = "btnU";
      this.btnU.Size = new System.Drawing.Size(45, 37);
      this.btnU.TabIndex = 46;
      this.btnU.Tag = "85";
      this.btnU.Text = "U";
      this.btnU.UseVisualStyleBackColor = false;
      // 
      // btnY
      // 
      this.btnY.BackColor = System.Drawing.SystemColors.Info;
      this.btnY.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnY.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnY.Location = new System.Drawing.Point(261, 0);
      this.btnY.Name = "btnY";
      this.btnY.Size = new System.Drawing.Size(45, 37);
      this.btnY.TabIndex = 45;
      this.btnY.Tag = "89";
      this.btnY.Text = "Y";
      this.btnY.UseVisualStyleBackColor = false;
      // 
      // btnT
      // 
      this.btnT.BackColor = System.Drawing.SystemColors.Info;
      this.btnT.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnT.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnT.Location = new System.Drawing.Point(216, 0);
      this.btnT.Name = "btnT";
      this.btnT.Size = new System.Drawing.Size(45, 37);
      this.btnT.TabIndex = 44;
      this.btnT.Tag = "84";
      this.btnT.Text = "T";
      this.btnT.UseVisualStyleBackColor = false;
      // 
      // btnR
      // 
      this.btnR.BackColor = System.Drawing.SystemColors.Info;
      this.btnR.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnR.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnR.Location = new System.Drawing.Point(171, 0);
      this.btnR.Name = "btnR";
      this.btnR.Size = new System.Drawing.Size(45, 37);
      this.btnR.TabIndex = 43;
      this.btnR.Tag = "82";
      this.btnR.Text = "R";
      this.btnR.UseVisualStyleBackColor = false;
      // 
      // btnE
      // 
      this.btnE.BackColor = System.Drawing.SystemColors.Info;
      this.btnE.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnE.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnE.Location = new System.Drawing.Point(126, 0);
      this.btnE.Name = "btnE";
      this.btnE.Size = new System.Drawing.Size(45, 37);
      this.btnE.TabIndex = 42;
      this.btnE.Tag = "69";
      this.btnE.Text = "E";
      this.btnE.UseVisualStyleBackColor = false;
      // 
      // btnW
      // 
      this.btnW.BackColor = System.Drawing.SystemColors.Info;
      this.btnW.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnW.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnW.Location = new System.Drawing.Point(81, 0);
      this.btnW.Name = "btnW";
      this.btnW.Size = new System.Drawing.Size(45, 37);
      this.btnW.TabIndex = 41;
      this.btnW.Tag = "87";
      this.btnW.Text = "W";
      this.btnW.UseVisualStyleBackColor = false;
      // 
      // btnQ
      // 
      this.btnQ.BackColor = System.Drawing.SystemColors.Info;
      this.btnQ.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnQ.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnQ.Location = new System.Drawing.Point(35, 0);
      this.btnQ.Name = "btnQ";
      this.btnQ.Size = new System.Drawing.Size(46, 37);
      this.btnQ.TabIndex = 40;
      this.btnQ.Tag = "81";
      this.btnQ.Text = "Q";
      this.btnQ.UseVisualStyleBackColor = false;
      // 
      // button45
      // 
      this.button45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button45.Dock = System.Windows.Forms.DockStyle.Left;
      this.button45.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button45.Location = new System.Drawing.Point(0, 0);
      this.button45.Name = "button45";
      this.button45.Size = new System.Drawing.Size(35, 37);
      this.button45.TabIndex = 39;
      this.button45.Tag = "9";
      this.button45.Text = "Tab";
      this.button45.UseVisualStyleBackColor = false;
      // 
      // panel4
      // 
      this.panel4.Controls.Add(this.button47);
      this.panel4.Controls.Add(this.button48);
      this.panel4.Controls.Add(this.button49);
      this.panel4.Controls.Add(this.btnSubtract);
      this.panel4.Controls.Add(this.btnL);
      this.panel4.Controls.Add(this.btnK);
      this.panel4.Controls.Add(this.btnJ);
      this.panel4.Controls.Add(this.btnH);
      this.panel4.Controls.Add(this.btnG);
      this.panel4.Controls.Add(this.btnF);
      this.panel4.Controls.Add(this.btnD);
      this.panel4.Controls.Add(this.btnS);
      this.panel4.Controls.Add(this.btnA);
      this.panel4.Controls.Add(this.btnCapsLock);
      this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel4.Location = new System.Drawing.Point(0, 135);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(647, 37);
      this.panel4.TabIndex = 54;
      // 
      // button47
      // 
      this.button47.BackColor = System.Drawing.SystemColors.Info;
      this.button47.Dock = System.Windows.Forms.DockStyle.Left;
      this.button47.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button47.Location = new System.Drawing.Point(582, 0);
      this.button47.Name = "button47";
      this.button47.Size = new System.Drawing.Size(60, 37);
      this.button47.TabIndex = 55;
      this.button47.Tag = "13";
      this.button47.Text = "Enter";
      this.button47.UseVisualStyleBackColor = false;
      // 
      // button48
      // 
      this.button48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button48.Dock = System.Windows.Forms.DockStyle.Left;
      this.button48.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button48.Location = new System.Drawing.Point(538, 0);
      this.button48.Name = "button48";
      this.button48.Size = new System.Drawing.Size(44, 37);
      this.button48.TabIndex = 54;
      this.button48.Text = "<-";
      this.button48.UseVisualStyleBackColor = false;
      // 
      // button49
      // 
      this.button49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button49.Dock = System.Windows.Forms.DockStyle.Left;
      this.button49.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button49.Location = new System.Drawing.Point(494, 0);
      this.button49.Name = "button49";
      this.button49.Size = new System.Drawing.Size(44, 37);
      this.button49.TabIndex = 52;
      this.button49.Tag = "107";
      this.button49.Text = "+ =";
      this.button49.UseVisualStyleBackColor = false;
      // 
      // btnSubtract
      // 
      this.btnSubtract.BackColor = System.Drawing.SystemColors.Info;
      this.btnSubtract.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnSubtract.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnSubtract.Location = new System.Drawing.Point(450, 0);
      this.btnSubtract.Name = "btnSubtract";
      this.btnSubtract.Size = new System.Drawing.Size(44, 37);
      this.btnSubtract.TabIndex = 51;
      this.btnSubtract.Tag = "109";
      this.btnSubtract.Text = "-";
      this.btnSubtract.UseVisualStyleBackColor = false;
      // 
      // btnL
      // 
      this.btnL.BackColor = System.Drawing.SystemColors.Info;
      this.btnL.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnL.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnL.Location = new System.Drawing.Point(406, 0);
      this.btnL.Name = "btnL";
      this.btnL.Size = new System.Drawing.Size(44, 37);
      this.btnL.TabIndex = 48;
      this.btnL.Tag = "76";
      this.btnL.Text = "L";
      this.btnL.UseVisualStyleBackColor = false;
      // 
      // btnK
      // 
      this.btnK.BackColor = System.Drawing.SystemColors.Info;
      this.btnK.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnK.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnK.Location = new System.Drawing.Point(362, 0);
      this.btnK.Name = "btnK";
      this.btnK.Size = new System.Drawing.Size(44, 37);
      this.btnK.TabIndex = 47;
      this.btnK.Tag = "75";
      this.btnK.Text = "K";
      this.btnK.UseVisualStyleBackColor = false;
      // 
      // btnJ
      // 
      this.btnJ.BackColor = System.Drawing.SystemColors.Info;
      this.btnJ.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnJ.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnJ.Location = new System.Drawing.Point(318, 0);
      this.btnJ.Name = "btnJ";
      this.btnJ.Size = new System.Drawing.Size(44, 37);
      this.btnJ.TabIndex = 46;
      this.btnJ.Tag = "74";
      this.btnJ.Text = "J";
      this.btnJ.UseVisualStyleBackColor = false;
      // 
      // btnH
      // 
      this.btnH.BackColor = System.Drawing.SystemColors.Info;
      this.btnH.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnH.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnH.Location = new System.Drawing.Point(274, 0);
      this.btnH.Name = "btnH";
      this.btnH.Size = new System.Drawing.Size(44, 37);
      this.btnH.TabIndex = 45;
      this.btnH.Tag = "72";
      this.btnH.Text = "H";
      this.btnH.UseVisualStyleBackColor = false;
      // 
      // btnG
      // 
      this.btnG.BackColor = System.Drawing.SystemColors.Info;
      this.btnG.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnG.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnG.Location = new System.Drawing.Point(230, 0);
      this.btnG.Name = "btnG";
      this.btnG.Size = new System.Drawing.Size(44, 37);
      this.btnG.TabIndex = 44;
      this.btnG.Tag = "71";
      this.btnG.Text = "G";
      this.btnG.UseVisualStyleBackColor = false;
      // 
      // btnF
      // 
      this.btnF.BackColor = System.Drawing.SystemColors.Info;
      this.btnF.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnF.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnF.Location = new System.Drawing.Point(186, 0);
      this.btnF.Name = "btnF";
      this.btnF.Size = new System.Drawing.Size(44, 37);
      this.btnF.TabIndex = 43;
      this.btnF.Tag = "70";
      this.btnF.Text = "F";
      this.btnF.UseVisualStyleBackColor = false;
      // 
      // btnD
      // 
      this.btnD.BackColor = System.Drawing.SystemColors.Info;
      this.btnD.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnD.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnD.Location = new System.Drawing.Point(142, 0);
      this.btnD.Name = "btnD";
      this.btnD.Size = new System.Drawing.Size(44, 37);
      this.btnD.TabIndex = 42;
      this.btnD.Tag = "68";
      this.btnD.Text = "D";
      this.btnD.UseVisualStyleBackColor = false;
      // 
      // btnS
      // 
      this.btnS.BackColor = System.Drawing.SystemColors.Info;
      this.btnS.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnS.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnS.Location = new System.Drawing.Point(98, 0);
      this.btnS.Name = "btnS";
      this.btnS.Size = new System.Drawing.Size(44, 37);
      this.btnS.TabIndex = 41;
      this.btnS.Tag = "83";
      this.btnS.Text = "S";
      this.btnS.UseVisualStyleBackColor = false;
      // 
      // btnA
      // 
      this.btnA.BackColor = System.Drawing.SystemColors.Info;
      this.btnA.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnA.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnA.Location = new System.Drawing.Point(54, 0);
      this.btnA.Name = "btnA";
      this.btnA.Size = new System.Drawing.Size(44, 37);
      this.btnA.TabIndex = 40;
      this.btnA.Tag = "65";
      this.btnA.Text = "A";
      this.btnA.UseVisualStyleBackColor = false;
      // 
      // btnCapsLock
      // 
      this.btnCapsLock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnCapsLock.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnCapsLock.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnCapsLock.Location = new System.Drawing.Point(0, 0);
      this.btnCapsLock.Name = "btnCapsLock";
      this.btnCapsLock.Size = new System.Drawing.Size(54, 37);
      this.btnCapsLock.TabIndex = 39;
      this.btnCapsLock.Tag = "20";
      this.btnCapsLock.Text = "Caps Lock";
      this.btnCapsLock.UseVisualStyleBackColor = false;
      // 
      // panel6
      // 
      this.panel6.Controls.Add(this.button75);
      this.panel6.Controls.Add(this.button76);
      this.panel6.Controls.Add(this.btnShiftRight);
      this.panel6.Controls.Add(this.button78);
      this.panel6.Controls.Add(this.btnDecimalPoint);
      this.panel6.Controls.Add(this.btnComma);
      this.panel6.Controls.Add(this.btnM);
      this.panel6.Controls.Add(this.btnN);
      this.panel6.Controls.Add(this.btnB);
      this.panel6.Controls.Add(this.btnV);
      this.panel6.Controls.Add(this.btnC);
      this.panel6.Controls.Add(this.btnX);
      this.panel6.Controls.Add(this.btnZ);
      this.panel6.Controls.Add(this.btnShiftLeft);
      this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel6.Location = new System.Drawing.Point(0, 172);
      this.panel6.Name = "panel6";
      this.panel6.Size = new System.Drawing.Size(647, 37);
      this.panel6.TabIndex = 55;
      // 
      // button75
      // 
      this.button75.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button75.Dock = System.Windows.Forms.DockStyle.Left;
      this.button75.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button75.Location = new System.Drawing.Point(604, 0);
      this.button75.Name = "button75";
      this.button75.Size = new System.Drawing.Size(38, 37);
      this.button75.TabIndex = 56;
      this.button75.Tag = "35";
      this.button75.Text = "End";
      this.button75.UseVisualStyleBackColor = false;
      // 
      // button76
      // 
      this.button76.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button76.Dock = System.Windows.Forms.DockStyle.Left;
      this.button76.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button76.Location = new System.Drawing.Point(567, 0);
      this.button76.Name = "button76";
      this.button76.Size = new System.Drawing.Size(37, 37);
      this.button76.TabIndex = 55;
      this.button76.Text = "Up";
      this.button76.UseVisualStyleBackColor = false;
      // 
      // btnShiftRight
      // 
      this.btnShiftRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnShiftRight.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnShiftRight.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnShiftRight.Location = new System.Drawing.Point(508, 0);
      this.btnShiftRight.Name = "btnShiftRight";
      this.btnShiftRight.Size = new System.Drawing.Size(59, 37);
      this.btnShiftRight.TabIndex = 54;
      this.btnShiftRight.Tag = "16";
      this.btnShiftRight.Text = "Shift";
      this.btnShiftRight.UseVisualStyleBackColor = false;
      // 
      // button78
      // 
      this.button78.BackColor = System.Drawing.SystemColors.Info;
      this.button78.Dock = System.Windows.Forms.DockStyle.Left;
      this.button78.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button78.Location = new System.Drawing.Point(464, 0);
      this.button78.Name = "button78";
      this.button78.Size = new System.Drawing.Size(44, 37);
      this.button78.TabIndex = 52;
      this.button78.Tag = "219";
      this.button78.Text = "? /";
      this.button78.UseVisualStyleBackColor = false;
      // 
      // btnDecimalPoint
      // 
      this.btnDecimalPoint.BackColor = System.Drawing.SystemColors.Info;
      this.btnDecimalPoint.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnDecimalPoint.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnDecimalPoint.Location = new System.Drawing.Point(420, 0);
      this.btnDecimalPoint.Name = "btnDecimalPoint";
      this.btnDecimalPoint.Size = new System.Drawing.Size(44, 37);
      this.btnDecimalPoint.TabIndex = 51;
      this.btnDecimalPoint.Tag = "190";
      this.btnDecimalPoint.Text = "> .";
      this.btnDecimalPoint.UseVisualStyleBackColor = false;
      // 
      // btnComma
      // 
      this.btnComma.BackColor = System.Drawing.SystemColors.Info;
      this.btnComma.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnComma.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnComma.Location = new System.Drawing.Point(376, 0);
      this.btnComma.Name = "btnComma";
      this.btnComma.Size = new System.Drawing.Size(44, 37);
      this.btnComma.TabIndex = 48;
      this.btnComma.Tag = "188";
      this.btnComma.Text = "< ,";
      this.btnComma.UseVisualStyleBackColor = false;
      // 
      // btnM
      // 
      this.btnM.BackColor = System.Drawing.SystemColors.Info;
      this.btnM.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnM.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnM.Location = new System.Drawing.Point(332, 0);
      this.btnM.Name = "btnM";
      this.btnM.Size = new System.Drawing.Size(44, 37);
      this.btnM.TabIndex = 47;
      this.btnM.Tag = "77";
      this.btnM.Text = "M";
      this.btnM.UseVisualStyleBackColor = false;
      // 
      // btnN
      // 
      this.btnN.BackColor = System.Drawing.SystemColors.Info;
      this.btnN.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnN.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnN.Location = new System.Drawing.Point(288, 0);
      this.btnN.Name = "btnN";
      this.btnN.Size = new System.Drawing.Size(44, 37);
      this.btnN.TabIndex = 46;
      this.btnN.Tag = "78";
      this.btnN.Text = "N";
      this.btnN.UseVisualStyleBackColor = false;
      // 
      // btnB
      // 
      this.btnB.BackColor = System.Drawing.SystemColors.Info;
      this.btnB.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnB.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnB.Location = new System.Drawing.Point(244, 0);
      this.btnB.Name = "btnB";
      this.btnB.Size = new System.Drawing.Size(44, 37);
      this.btnB.TabIndex = 45;
      this.btnB.Tag = "66";
      this.btnB.Text = "B";
      this.btnB.UseVisualStyleBackColor = false;
      // 
      // btnV
      // 
      this.btnV.BackColor = System.Drawing.SystemColors.Info;
      this.btnV.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnV.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnV.Location = new System.Drawing.Point(200, 0);
      this.btnV.Name = "btnV";
      this.btnV.Size = new System.Drawing.Size(44, 37);
      this.btnV.TabIndex = 44;
      this.btnV.Tag = "86";
      this.btnV.Text = "V";
      this.btnV.UseVisualStyleBackColor = false;
      // 
      // btnC
      // 
      this.btnC.BackColor = System.Drawing.SystemColors.Info;
      this.btnC.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnC.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnC.Location = new System.Drawing.Point(156, 0);
      this.btnC.Name = "btnC";
      this.btnC.Size = new System.Drawing.Size(44, 37);
      this.btnC.TabIndex = 43;
      this.btnC.Tag = "67";
      this.btnC.Text = "C";
      this.btnC.UseVisualStyleBackColor = false;
      // 
      // btnX
      // 
      this.btnX.BackColor = System.Drawing.SystemColors.Info;
      this.btnX.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnX.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnX.Location = new System.Drawing.Point(112, 0);
      this.btnX.Name = "btnX";
      this.btnX.Size = new System.Drawing.Size(44, 37);
      this.btnX.TabIndex = 42;
      this.btnX.Tag = "88";
      this.btnX.Text = "X";
      this.btnX.UseVisualStyleBackColor = false;
      // 
      // btnZ
      // 
      this.btnZ.BackColor = System.Drawing.SystemColors.Info;
      this.btnZ.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnZ.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnZ.Location = new System.Drawing.Point(68, 0);
      this.btnZ.Name = "btnZ";
      this.btnZ.Size = new System.Drawing.Size(44, 37);
      this.btnZ.TabIndex = 41;
      this.btnZ.Tag = "90";
      this.btnZ.Text = "Z";
      this.btnZ.UseVisualStyleBackColor = false;
      // 
      // btnShiftLeft
      // 
      this.btnShiftLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.btnShiftLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnShiftLeft.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnShiftLeft.Location = new System.Drawing.Point(0, 0);
      this.btnShiftLeft.Name = "btnShiftLeft";
      this.btnShiftLeft.Size = new System.Drawing.Size(68, 37);
      this.btnShiftLeft.TabIndex = 40;
      this.btnShiftLeft.Tag = "16";
      this.btnShiftLeft.Text = "Shift";
      this.btnShiftLeft.UseVisualStyleBackColor = false;
      // 
      // panel5
      // 
      this.panel5.Controls.Add(this.button66);
      this.panel5.Controls.Add(this.button65);
      this.panel5.Controls.Add(this.button64);
      this.panel5.Controls.Add(this.button63);
      this.panel5.Controls.Add(this.button62);
      this.panel5.Controls.Add(this.button61);
      this.panel5.Controls.Add(this.btnSpace);
      this.panel5.Controls.Add(this.button70);
      this.panel5.Controls.Add(this.button71);
      this.panel5.Controls.Add(this.button73);
      this.panel5.Controls.Add(this.button74);
      this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel5.Location = new System.Drawing.Point(0, 209);
      this.panel5.Name = "panel5";
      this.panel5.Size = new System.Drawing.Size(647, 37);
      this.panel5.TabIndex = 56;
      // 
      // button66
      // 
      this.button66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button66.Dock = System.Windows.Forms.DockStyle.Left;
      this.button66.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button66.Location = new System.Drawing.Point(600, 0);
      this.button66.Name = "button66";
      this.button66.Size = new System.Drawing.Size(42, 37);
      this.button66.TabIndex = 58;
      this.button66.Tag = "39";
      this.button66.Text = "Right";
      this.button66.UseVisualStyleBackColor = false;
      // 
      // button65
      // 
      this.button65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button65.Dock = System.Windows.Forms.DockStyle.Left;
      this.button65.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button65.Location = new System.Drawing.Point(558, 0);
      this.button65.Name = "button65";
      this.button65.Size = new System.Drawing.Size(42, 37);
      this.button65.TabIndex = 57;
      this.button65.Tag = "40";
      this.button65.Text = "Down";
      this.button65.UseVisualStyleBackColor = false;
      // 
      // button64
      // 
      this.button64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button64.Dock = System.Windows.Forms.DockStyle.Left;
      this.button64.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button64.Location = new System.Drawing.Point(525, 0);
      this.button64.Name = "button64";
      this.button64.Size = new System.Drawing.Size(33, 37);
      this.button64.TabIndex = 56;
      this.button64.Tag = "37";
      this.button64.Text = "Left";
      this.button64.UseVisualStyleBackColor = false;
      // 
      // button63
      // 
      this.button63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button63.Dock = System.Windows.Forms.DockStyle.Left;
      this.button63.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button63.Location = new System.Drawing.Point(479, 0);
      this.button63.Name = "button63";
      this.button63.Size = new System.Drawing.Size(46, 37);
      this.button63.TabIndex = 47;
      this.button63.Tag = "17";
      this.button63.Text = "Ctrl";
      this.button63.UseVisualStyleBackColor = false;
      // 
      // button62
      // 
      this.button62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button62.Dock = System.Windows.Forms.DockStyle.Left;
      this.button62.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button62.Location = new System.Drawing.Point(441, 0);
      this.button62.Name = "button62";
      this.button62.Size = new System.Drawing.Size(38, 37);
      this.button62.TabIndex = 46;
      this.button62.Tag = "201";
      this.button62.Text = "~ `";
      this.button62.UseVisualStyleBackColor = false;
      // 
      // button61
      // 
      this.button61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button61.Dock = System.Windows.Forms.DockStyle.Left;
      this.button61.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button61.Location = new System.Drawing.Point(394, 0);
      this.button61.Name = "button61";
      this.button61.Size = new System.Drawing.Size(47, 37);
      this.button61.TabIndex = 45;
      this.button61.Tag = "18";
      this.button61.Text = "Alt Gr";
      this.button61.UseVisualStyleBackColor = false;
      // 
      // btnSpace
      // 
      this.btnSpace.BackColor = System.Drawing.SystemColors.Info;
      this.btnSpace.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnSpace.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.btnSpace.Location = new System.Drawing.Point(196, 0);
      this.btnSpace.Name = "btnSpace";
      this.btnSpace.Size = new System.Drawing.Size(198, 37);
      this.btnSpace.TabIndex = 44;
      this.btnSpace.Tag = "32";
      this.btnSpace.Text = "Space";
      this.btnSpace.UseVisualStyleBackColor = false;
      // 
      // button70
      // 
      this.button70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button70.Dock = System.Windows.Forms.DockStyle.Left;
      this.button70.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button70.Location = new System.Drawing.Point(151, 0);
      this.button70.Name = "button70";
      this.button70.Size = new System.Drawing.Size(45, 37);
      this.button70.TabIndex = 43;
      this.button70.Tag = "18";
      this.button70.Text = "Alt";
      this.button70.UseVisualStyleBackColor = false;
      // 
      // button71
      // 
      this.button71.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button71.Dock = System.Windows.Forms.DockStyle.Left;
      this.button71.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button71.Location = new System.Drawing.Point(106, 0);
      this.button71.Name = "button71";
      this.button71.Size = new System.Drawing.Size(45, 37);
      this.button71.TabIndex = 42;
      this.button71.Tag = "17";
      this.button71.Text = "Ctrl";
      this.button71.UseVisualStyleBackColor = false;
      // 
      // button73
      // 
      this.button73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button73.Dock = System.Windows.Forms.DockStyle.Left;
      this.button73.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button73.Location = new System.Drawing.Point(55, 0);
      this.button73.Name = "button73";
      this.button73.Size = new System.Drawing.Size(51, 37);
      this.button73.TabIndex = 40;
      this.button73.Tag = "203";
      this.button73.Text = "Screen-";
      this.button73.UseVisualStyleBackColor = false;
      // 
      // button74
      // 
      this.button74.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
      this.button74.Dock = System.Windows.Forms.DockStyle.Left;
      this.button74.Font = new System.Drawing.Font("Arial", 7.35F, System.Drawing.FontStyle.Bold);
      this.button74.Location = new System.Drawing.Point(0, 0);
      this.button74.Name = "button74";
      this.button74.Size = new System.Drawing.Size(55, 37);
      this.button74.TabIndex = 39;
      this.button74.Tag = "202";
      this.button74.Text = "Screen+";
      this.button74.UseVisualStyleBackColor = false;
      // 
      // panel7
      // 
      this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel7.Location = new System.Drawing.Point(76, 0);
      this.panel7.Name = "panel7";
      this.panel7.Size = new System.Drawing.Size(22, 25);
      this.panel7.TabIndex = 3;
      // 
      // radioButton1
      // 
      this.radioButton1.AutoSize = true;
      this.radioButton1.Dock = System.Windows.Forms.DockStyle.Left;
      this.radioButton1.Enabled = false;
      this.radioButton1.Location = new System.Drawing.Point(98, 0);
      this.radioButton1.Name = "radioButton1";
      this.radioButton1.Size = new System.Drawing.Size(46, 25);
      this.radioButton1.TabIndex = 4;
      this.radioButton1.TabStop = true;
      this.radioButton1.Text = "Shift";
      this.radioButton1.UseVisualStyleBackColor = true;
      // 
      // radioButton2
      // 
      this.radioButton2.AutoSize = true;
      this.radioButton2.Dock = System.Windows.Forms.DockStyle.Left;
      this.radioButton2.Enabled = false;
      this.radioButton2.Location = new System.Drawing.Point(166, 0);
      this.radioButton2.Name = "radioButton2";
      this.radioButton2.Size = new System.Drawing.Size(40, 25);
      this.radioButton2.TabIndex = 6;
      this.radioButton2.TabStop = true;
      this.radioButton2.Text = "Ctrl";
      this.radioButton2.UseVisualStyleBackColor = true;
      // 
      // panel8
      // 
      this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel8.Location = new System.Drawing.Point(144, 0);
      this.panel8.Name = "panel8";
      this.panel8.Size = new System.Drawing.Size(22, 25);
      this.panel8.TabIndex = 5;
      // 
      // radioButton3
      // 
      this.radioButton3.AutoSize = true;
      this.radioButton3.Dock = System.Windows.Forms.DockStyle.Left;
      this.radioButton3.Enabled = false;
      this.radioButton3.Location = new System.Drawing.Point(228, 0);
      this.radioButton3.Name = "radioButton3";
      this.radioButton3.Size = new System.Drawing.Size(37, 25);
      this.radioButton3.TabIndex = 8;
      this.radioButton3.TabStop = true;
      this.radioButton3.Text = "Alt";
      this.radioButton3.UseVisualStyleBackColor = true;
      // 
      // panel9
      // 
      this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel9.Location = new System.Drawing.Point(206, 0);
      this.panel9.Name = "panel9";
      this.panel9.Size = new System.Drawing.Size(22, 25);
      this.panel9.TabIndex = 7;
      // 
      // radioButton4
      // 
      this.radioButton4.AutoSize = true;
      this.radioButton4.Dock = System.Windows.Forms.DockStyle.Left;
      this.radioButton4.Enabled = false;
      this.radioButton4.Location = new System.Drawing.Point(287, 0);
      this.radioButton4.Name = "radioButton4";
      this.radioButton4.Size = new System.Drawing.Size(51, 25);
      this.radioButton4.TabIndex = 10;
      this.radioButton4.TabStop = true;
      this.radioButton4.Text = "Alt Gr";
      this.radioButton4.UseVisualStyleBackColor = true;
      // 
      // panel10
      // 
      this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel10.Location = new System.Drawing.Point(265, 0);
      this.panel10.Name = "panel10";
      this.panel10.Size = new System.Drawing.Size(22, 25);
      this.panel10.TabIndex = 9;
      // 
      // CUCIT01KeyboardExternal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel5);
      this.Controls.Add(this.panel6);
      this.Controls.Add(this.panel4);
      this.Controls.Add(this.panel3);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.pnlStatus);
      this.Name = "CUCIT01KeyboardExternal";
      this.Size = new System.Drawing.Size(647, 250);
      this.pnlStatus.ResumeLayout(false);
      this.pnlStatus.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.panel3.ResumeLayout(false);
      this.panel4.ResumeLayout(false);
      this.panel6.ResumeLayout(false);
      this.panel5.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlStatus;
    private System.Windows.Forms.RadioButton rbtCapsLock;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button btn3;
    private System.Windows.Forms.Button button13;
    private System.Windows.Forms.Button button14;
    private System.Windows.Forms.Button button17;
    private System.Windows.Forms.Button button18;
    private System.Windows.Forms.Button button19;
    private System.Windows.Forms.Button button20;
    private System.Windows.Forms.Button button9;
    private System.Windows.Forms.Button button10;
    private System.Windows.Forms.Button button7;
    private System.Windows.Forms.Button button8;
    private System.Windows.Forms.Button button5;
    private System.Windows.Forms.Button button6;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.Button button4;
    private System.Windows.Forms.Button btnF1;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Button button25;
    private System.Windows.Forms.Button button11;
    private System.Windows.Forms.Button button26;
    private System.Windows.Forms.Button btnBackslash;
    private System.Windows.Forms.Button btn0;
    private System.Windows.Forms.Button btn9;
    private System.Windows.Forms.Button btn8;
    private System.Windows.Forms.Button btn7;
    private System.Windows.Forms.Button btn6;
    private System.Windows.Forms.Button btn5;
    private System.Windows.Forms.Button btn4;
    private System.Windows.Forms.Button btn33;
    private System.Windows.Forms.Button btn2;
    private System.Windows.Forms.Button btn1;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Button button46;
    private System.Windows.Forms.Button button16;
    private System.Windows.Forms.Button button21;
    private System.Windows.Forms.Button button22;
    private System.Windows.Forms.Button btnP;
    private System.Windows.Forms.Button btnO;
    private System.Windows.Forms.Button btnI;
    private System.Windows.Forms.Button btnU;
    private System.Windows.Forms.Button btnY;
    private System.Windows.Forms.Button btnT;
    private System.Windows.Forms.Button btnR;
    private System.Windows.Forms.Button btnE;
    private System.Windows.Forms.Button btnW;
    private System.Windows.Forms.Button btnQ;
    private System.Windows.Forms.Button button45;
    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.Button button47;
    private System.Windows.Forms.Button button48;
    private System.Windows.Forms.Button button49;
    private System.Windows.Forms.Button btnSubtract;
    private System.Windows.Forms.Button btnL;
    private System.Windows.Forms.Button btnK;
    private System.Windows.Forms.Button btnJ;
    private System.Windows.Forms.Button btnH;
    private System.Windows.Forms.Button btnG;
    private System.Windows.Forms.Button btnF;
    private System.Windows.Forms.Button btnD;
    private System.Windows.Forms.Button btnS;
    private System.Windows.Forms.Button btnA;
    private System.Windows.Forms.Button btnCapsLock;
    private System.Windows.Forms.Panel panel6;
    private System.Windows.Forms.Button button75;
    private System.Windows.Forms.Button button76;
    private System.Windows.Forms.Button btnShiftRight;
    private System.Windows.Forms.Button button78;
    private System.Windows.Forms.Button btnDecimalPoint;
    private System.Windows.Forms.Button btnComma;
    private System.Windows.Forms.Button btnM;
    private System.Windows.Forms.Button btnN;
    private System.Windows.Forms.Button btnB;
    private System.Windows.Forms.Button btnV;
    private System.Windows.Forms.Button btnC;
    private System.Windows.Forms.Button btnX;
    private System.Windows.Forms.Button btnZ;
    private System.Windows.Forms.Button btnShiftLeft;
    private System.Windows.Forms.Panel panel5;
    private System.Windows.Forms.Button button66;
    private System.Windows.Forms.Button button65;
    private System.Windows.Forms.Button button64;
    private System.Windows.Forms.Button button63;
    private System.Windows.Forms.Button button62;
    private System.Windows.Forms.Button button61;
    private System.Windows.Forms.Button btnSpace;
    private System.Windows.Forms.Button button70;
    private System.Windows.Forms.Button button71;
    private System.Windows.Forms.Button button73;
    private System.Windows.Forms.Button button74;
    private System.Windows.Forms.RadioButton radioButton4;
    private System.Windows.Forms.Panel panel10;
    private System.Windows.Forms.RadioButton radioButton3;
    private System.Windows.Forms.Panel panel9;
    private System.Windows.Forms.RadioButton radioButton2;
    private System.Windows.Forms.Panel panel8;
    private System.Windows.Forms.RadioButton radioButton1;
    private System.Windows.Forms.Panel panel7;
  }
}
