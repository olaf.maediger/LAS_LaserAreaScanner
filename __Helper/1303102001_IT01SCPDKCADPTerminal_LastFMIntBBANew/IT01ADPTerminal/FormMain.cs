﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;
using UCNotifier;
//
namespace ITAnalogDigitalController
{ //
  //##########################################################################
  //	Section - Main - Extended
  //##########################################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------------------------
    //  Section - Member
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      // FUCGrabberHardware.SetNotifier(FUCNotifier);
    }

    private void FreeProgrammerControls()
    {
      // FUCGrabberHardware.Close(true);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      // FUCGrabberHardware.LoadInitdata(initdata);
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      // FUCGrabberHardware.SaveInitdata(initdata);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - ...
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    //


  }
}
