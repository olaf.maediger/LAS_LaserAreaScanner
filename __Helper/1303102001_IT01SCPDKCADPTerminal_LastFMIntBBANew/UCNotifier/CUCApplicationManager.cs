﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCNotifier
{
  public partial class CUCApplicationManager : UserControl
  { //
    //-----------------------------------------------------
    //  Segment - Const
    //-----------------------------------------------------
    //
    private const Int32 SPACEX = 4;
    private const Int32 SPACEX2 = SPACEX / 2;
    private const Int32 WIDTH_BUTTON = 76;
    private const Int32 WIDTH_BUTTON2 = WIDTH_BUTTON / 2;
    //
    private const String APPLICATIONPRODUCTKEY_EMPTY = "0-0-0-0";
    private const String APPLICATIONNAME_EMPTY = " ";
    private const String APPLICATIONVERSION_EMPTY = "0.0.0";
    private const String APPLICATIONDATE_EMPTY = "000000";
    private const String APPLICATIONCOMPANY_EMPTY = " ";
    private const String APPLICATIONSTREET_EMPTY = " ";
    private const String APPLICATIONCITY_EMPTY = " ";
    private const String APPLICATIONUSER_EMPTY = " ";

    //
    //-----------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------
    //
    private CApplicationlist FApplicationlist;
    //
    //-----------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------
    //
    public CUCApplicationManager()
    {
      InitializeComponent();
      //
      FApplicationlist = new CApplicationlist();
      //
      FUCAddApplication.SetApplicationlist(FApplicationlist);
      FUCAddApplication.SetOnApplicationCancel(OnAddApplicationCancel);
      FUCAddApplication.SetOnApplicationConfirm(OnAddApplicationConfirm);
      //
      FUCEditApplication.SetApplicationlist(FApplicationlist);
      FUCEditApplication.SetOnApplicationCancel(OnEditApplicationCancel);
      FUCEditApplication.SetOnApplicationConfirm(OnEditApplicationConfirm);
      //
      FUCDeleteApplication.SetApplicationlist(FApplicationlist);
      FUCDeleteApplication.SetOnApplicationCancel(OnDeleteApplicationCancel);
      FUCDeleteApplication.SetOnApplicationConfirm(OnDeleteApplicationConfirm);
      //
      Init();
      pnlKeys_Resize(this, null);
    }

    public Int32 GetTransformApplicationCount()
    {
      return FApplicationlist.Count;
    }

    public CTransformApplication GetTransformApplication(Int32 index)
    {
      if ((0 <= index) && (index < FApplicationlist.Count))
      {
        CTransformBase TransformBase = FApplicationlist[index];
        if (TransformBase is CTransformApplication)
        {
          return (CTransformApplication)TransformBase;
        }
      }
      return null;
    }
    //
    //-----------------------------------------------------
    //  Segment - Event
    //-----------------------------------------------------
    //
    private void pnlKeys_Resize(object sender, EventArgs e)
    {
      Int32 CX = pnlKeys.Width / 2;
      if (btnClose.Visible)
      {
        btnCancel.Visible = true;
        btnDeleteApplication.Left = CX - WIDTH_BUTTON2;
        btnEditApplication.Left = CX - WIDTH_BUTTON2 - 1 * WIDTH_BUTTON - 1 * SPACEX;
        btnAddApplication.Left = CX - WIDTH_BUTTON2 - 2 * WIDTH_BUTTON - 2 * SPACEX;
        btnClose.Left = CX + WIDTH_BUTTON2 + SPACEX;
        btnCancel.Left = CX + WIDTH_BUTTON2 + WIDTH_BUTTON + 2 * SPACEX;
      } else
      {
        btnCancel.Visible = false;
        btnEditApplication.Left = CX - btnEditApplication.Width / 2;
        btnAddApplication.Left = btnEditApplication.Left - btnAddApplication.Width - SPACEX;
        btnDeleteApplication.Left = CX + btnEditApplication.Width / 2 + SPACEX;
      }
    }

    private void FUCListBoxApplications_SelectedIndexChanged(object sender, EventArgs e)
    {
      Int32 SI = FUCListBoxApplications.SelectedIndex;
      CTransformApplication TA = (CTransformApplication)FApplicationlist[SI];
      if (TA is CTransformApplication)
      {
        RApplicationData ApplicationData;
        TA.GetData(out ApplicationData);
        // NC FApplicationData.TransformData.Name = ExtractName();
        /*ApplicationData.TransformData.Version = ExtractVersion();
        ApplicationData.TransformData.Date = ExtractDate();
        ApplicationData.Company = ExtractCompany();
        ApplicationData.Street = ExtractStreet();
        ApplicationData.City = ExtractCity();
        ApplicationData.TransformData.DiscountUser = ExtractDiscountUser();
        ApplicationData.TransformData.User = ExtractUser();
        ApplicationData.TransformData.ProductNumber = ExtractProductNumber();*/
        // ApplicationData.TransformData.ProductKey = ExtractProductKey();
        //
        FUCAddApplication.SetData(ApplicationData);
        FUCEditApplication.SetData(ApplicationData);
        FUCDeleteApplication.SetData(ApplicationData);
      }
    }
    //
    //-----------------------------------------------------
    //  Segment - Initialisation
    //-----------------------------------------------------
    //
    private void Init()
    {
      FUCAddApplication.Visible = false;
      FUCEditApplication.Visible = false;
      FUCDeleteApplication.Visible = false;
      btnClose.Visible = true;
      //
      btnAddApplication.Width = WIDTH_BUTTON;
      btnEditApplication.Width = WIDTH_BUTTON;
      btnDeleteApplication.Width = WIDTH_BUTTON;
      btnClose.Left = WIDTH_BUTTON;
    }
    //
    //---------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------
    //
    private Boolean RefreshUCApplicationManager()
    {
      ClearLines();
      foreach (CTransformApplication TA in FApplicationlist)
      {
        AddLine(TA);
      }
      return true;
    }

    public void ForceResize()
    {
      pnlKeys_Resize(this, null);
    }

    private CTransformApplication ExtractTransformApplication()
    {
      if ((0 <= FUCListBoxApplications.SelectedIndex) &&
					(FUCListBoxApplications.SelectedIndex < FApplicationlist.Count))
      {
        CTransformBase TransformBase = FApplicationlist[FUCListBoxApplications.SelectedIndex];
        if (TransformBase is CTransformApplication)
        {
          return (CTransformApplication)TransformBase;
        }
      }
      return null;
    }

    private String ExtractName()
    {
      CTransformApplication TransformApplication = ExtractTransformApplication();
      if (TransformApplication is CTransformApplication)
      {
        return TransformApplication.Name;
      }
      return APPLICATIONNAME_EMPTY;
    }
    private String ExtractVersion()
    {
      CTransformApplication TransformApplication = ExtractTransformApplication();
      if (TransformApplication is CTransformApplication)
      {
        return TransformApplication.Version;
      }
      return APPLICATIONVERSION_EMPTY;
    }
    private String ExtractDate()
    {
      CTransformApplication TransformApplication = ExtractTransformApplication();
      if (TransformApplication is CTransformApplication)
      {
        return TransformApplication.Date;
      }
      return APPLICATIONDATE_EMPTY;
    }
    private String ExtractCompany()
    {
      CTransformApplication TransformApplication = ExtractTransformApplication();
      if (TransformApplication is CTransformApplication)
      {
        return TransformApplication.Company;
      }
      return APPLICATIONCOMPANY_EMPTY;
    }
    private String ExtractStreet()
    {
      CTransformApplication TransformApplication = ExtractTransformApplication();
      if (TransformApplication is CTransformApplication)
      {
        return TransformApplication.Street;
      }
      return APPLICATIONSTREET_EMPTY;
    }
    private String ExtractCity()
    {
      CTransformApplication TransformApplication = ExtractTransformApplication();
      if (TransformApplication is CTransformApplication)
      {
        return TransformApplication.City;
      }
      return APPLICATIONCITY_EMPTY;
    }
    private Boolean ExtractDiscountUser()
    {
      CTransformApplication TransformApplication = ExtractTransformApplication();
      if (TransformApplication is CTransformApplication)
      {
        return TransformApplication.DiscountUser;
      }
      return false;
    }
    private String ExtractUser()
    {
      CTransformApplication TransformApplication = ExtractTransformApplication();
      if (TransformApplication is CTransformApplication)
      {
        return TransformApplication.User;
      }
      return APPLICATIONUSER_EMPTY;
    }
    private Int32 ExtractProductNumber()
    {
      CTransformApplication TransformApplication = ExtractTransformApplication();
      if (TransformApplication is CTransformApplication)
      {
        return TransformApplication.ProductNumber;
      }
      return 0;
    }
    private String ExtractProductKey()
    {
      CTransformApplication TransformApplication = ExtractTransformApplication();
      if (TransformApplication is CTransformApplication)
      {
        return TransformApplication.ProductKey;
      }
      return APPLICATIONPRODUCTKEY_EMPTY;
    }
    //
    //---------------------------------------------------------------
    //  Segment - Management - Application
    //---------------------------------------------------------------
    //
    public Int32 GetApplicationCount()
    {
      return FApplicationlist.Count;
    }

    private Boolean CheckIndex(Int32 index)
    {
      return ((0 <= index) && (index < FApplicationlist.Count));
    }

    public String GetApplicationProductKey(Int32 index)
    {
      if (CheckIndex(index))
      {
        CTransformApplication TA = (CTransformApplication)FApplicationlist[index];
        if (TA is CTransformApplication)
        {
          return TA.ProductKey;
        }
      }
      return APPLICATIONPRODUCTKEY_EMPTY;
    }

    public Boolean AddApplication(String name,
                                  String version,
                                  String date,
                                  String company,
                                  String street,
                                  String city,
                                  Boolean discountuser,
                                  String user)
    {
      CTransform Transform = new CTransform();
      Int32 ProductNumber = Transform.BuildApplication(name, version, date,
                                                     company, street, city,
                                                     discountuser, user);
      String ProductKey = Transform.BuildApplication(ProductNumber, 0);
      if (FApplicationlist.Add(name, version, date,
                               company, street, city, discountuser, user,
                               ProductNumber, ProductKey))
      {
        return RefreshUCApplicationManager();
      }
      return false;
    }

    public Boolean AddApplication(String name,
                                  String version,
                                  String date,
                                  String company,
                                  String street,
                                  String city,
                                  Boolean discountuser,
                                  String user,
                                  String productkey)
    {
      if (FApplicationlist.Add(name, version, date,
                               company, street, city, discountuser, user,
                               productkey))
      {
        return RefreshUCApplicationManager();
      }
      return false;
    }

    public Boolean AddApplication(String name,
                                  String version,
                                  String date,
                                  String company,
                                  String street,
                                  String city,
                                  Boolean discountuser,
                                  String user,
                                  Int32 productnumber,
                                  String productkey)
    {
      if (FApplicationlist.Add(name, version, date,
                               company, street, city, discountuser, user,
                               productnumber, productkey))
      {
        return RefreshUCApplicationManager();
      }
      return false;
    }

    public Boolean AddApplication(RApplicationData data)
    {
      if (FApplicationlist.Add(data))
      {
        return RefreshUCApplicationManager();
      }
      return false;
    }

    public Boolean DeleteApplication(String name,
                                     Int32 productnumber,
                                     String productkey)
    {
      if (FApplicationlist.Delete(name, productnumber, productkey))
      {
        return RefreshUCApplicationManager();
      }
      return false;
    }

    public Boolean ClearApplicationlist()
    {
      FApplicationlist.Clear();
      return (0 == FApplicationlist.Count);
    }

    public Boolean CheckAllApplications(Int32 count)
    {
      Int32 ApplicationCount = 0;
      Boolean Result = (0 < count);
      foreach (CTransformApplication TA in FApplicationlist)
      {
        ApplicationCount++;
        CTransform Transform = new CTransform();
        Int32 ProductNumber = Transform.BuildApplication(TA.Name, TA.Version, TA.Date,
                                                         TA.Company, TA.Street, TA.City,
                                                         TA.DiscountUser, TA.User);
        String ProductKey = Transform.BuildProductKey(ProductNumber);
        Result &= (ProductKey == TA.ProductKey);
      }
      Result &= (ApplicationCount == count);
      return Result;
    }
    //
    //-----------------------------------------------------
    //  Segment - Management - Line
    //-----------------------------------------------------
    //
    public Boolean ClearLines()
    {
      FUCListBoxApplications.Items.Clear();
      return true;
    }

    public Boolean AddLine(CTransformApplication application)
    {
      String Line = String.Format(" {0,-31} {1}", application.Name, application.ProductKey);
      FUCListBoxApplications.Items.Add(Line);
      FUCListBoxApplications.SelectedIndex = FUCListBoxApplications.Items.Count - 1;
      return true;
    }
    //
    //-----------------------------------------------------
    //  Segment - Menu - Add
    //-----------------------------------------------------
    //
    private void btnAddApplication_Click(object sender, EventArgs e)
    {
      pnlKeys.Visible = false;
      FUCAddApplication.Visible = true;
      FUCEditApplication.Visible = false;
      FUCDeleteApplication.Visible = false;
      FUCAddApplication.RefreshControls();
    }

    private void OnAddApplicationCancel(RApplicationData data)
    {
      FUCAddApplication.Visible = false;
      pnlKeys.Visible = true;
    }

    private void OnAddApplicationConfirm(RApplicationData data)
    {
      if ((0 < data.TransformData.Name.Length) &&
          // !!! not here !!! (0 < data.TransformData.ProductNumber) &&
          (0 < data.TransformData.ProductKey.Length))
      {
        FApplicationlist.Add(data);
        //
        FUCAddApplication.Visible = false;
        pnlKeys.Visible = true;
        // 
        RefreshUCApplicationManager();
        FUCListBoxApplications.SelectedIndex = FUCListBoxApplications.Items.Count - 1;
      }
    }
    //
    //-----------------------------------------------------
    //  Segment - Menu - Edit
    //-----------------------------------------------------
    //
    private void btnEditApplication_Click(object sender, EventArgs e)
    {
      pnlKeys.Visible = false;
      FUCAddApplication.Visible = false;
      FUCEditApplication.Visible = true;
      FUCDeleteApplication.Visible = false;
      FUCEditApplication.RefreshControls();
    }

    private void OnEditApplicationCancel(RApplicationData data)
    {
      FUCEditApplication.Visible = false;
      pnlKeys.Visible = true;
    }

    private void OnEditApplicationConfirm(RApplicationData data)
    {
      if ((0 < data.TransformData.Name.Length) &&
          (0 < data.TransformData.ProductNumber) &&
          (0 < data.TransformData.ProductKey.Length))
      {
        FApplicationlist.Change(FUCListBoxApplications.SelectedIndex, data);
        //
        FUCEditApplication.Visible = false;
        pnlKeys.Visible = true;
        // 
        RefreshUCApplicationManager();
        FUCListBoxApplications.SelectedIndex = FUCListBoxApplications.Items.Count - 1;
      }
    }
    //
    //-----------------------------------------------------
    //  Segment - Menu - Delete
    //-----------------------------------------------------
    //
    private void btnDeleteApplication_Click(object sender, EventArgs e)
    {
      pnlKeys.Visible = false;
      FUCAddApplication.Visible = false;
      FUCEditApplication.Visible = false;
      FUCDeleteApplication.Visible = true;
      FUCDeleteApplication.RefreshControls();
    }

    private void OnDeleteApplicationCancel(RApplicationData data)
    {
      FUCDeleteApplication.Visible = false;
      pnlKeys.Visible = true;
    }

    private void OnDeleteApplicationConfirm(RApplicationData data)
    {
      FApplicationlist.Delete(data.TransformData.Name,
                              data.TransformData.ProductNumber,
                              data.TransformData.ProductKey);
      //
      FUCDeleteApplication.Visible = false;
      pnlKeys.Visible = true;
      // 
      RefreshUCApplicationManager();
      FUCListBoxApplications.SelectedIndex = FUCListBoxApplications.Items.Count - 1;
    }
    //
    //-----------------------------------------------------
    //  Segment - Button-Event - Common
    //-----------------------------------------------------
    //
    private void btnClose_Click(object sender, EventArgs e)
    {
      //...
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      //...
    }

    //
    //-----------------------------------------------------
    //  Segment - 
    //-----------------------------------------------------
    //
  }
}
