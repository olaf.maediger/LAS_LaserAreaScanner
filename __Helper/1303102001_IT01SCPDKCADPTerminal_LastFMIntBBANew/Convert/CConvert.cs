﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace Convert
{
  public class CConvert
  {
    public static Byte AsciiNibbleToByte(Char nibble)
    {
      switch (nibble)
      {
        case '0':
          return 0x00;
        case '1':
          return 0x01;
        case '2':
          return 0x02;
        case '3':
          return 0x03;
        case '4':
          return 0x04;
        case '5':
          return 0x05;
        case '6':
          return 0x06;
        case '7':
          return 0x07;
        case '8':
          return 0x08;
        case '9':
          return 0x09;
        case 'A':
          return 0x0A;
        case 'B':
          return 0x0B;
        case 'C':
          return 0x0C;
        case 'D':
          return 0x0D;
        case 'E':
          return 0x0E;
        case 'F':
          return 0x0F;
      }
      return 0x00;
    }

    public static Byte AsciiByteToByte(Byte index,
                                       Byte[] values)
    {
      Char CNibble = (Char)values[0 + index];
      Byte Result = (Byte)(AsciiNibbleToByte(CNibble) << 4);
      CNibble = (Char)values[1 + index];
      Result += (Byte)(AsciiNibbleToByte(CNibble) << 0);
      return Result;
    }

    public static Byte AsciiByteToByte(Char nibblehigh,
                                       Char nibblelow)
    {
      Byte Result = (Byte)(AsciiNibbleToByte(nibblehigh) << 4);
      Result += (Byte)(AsciiNibbleToByte(nibblelow) << 0);
      return Result;
    }

    public static String NibbleToHexadecimalString(Byte value)
    {
      switch (0x0F & value)
      {
        case 0x01:
          return "1";
        case 0x02:
          return "2";
        case 0x03:
          return "3";
        case 0x04:
          return "4";
        case 0x05:
          return "5";
        case 0x06:
          return "6";
        case 0x07:
          return "7";
        case 0x08:
          return "8";
        case 0x09:
          return "9";
        case 0x0A:
          return "A";
        case 0x0B:
          return "B";
        case 0x0C:
          return "C";
        case 0x0D:
          return "D";
        case 0x0E:
          return "E";
        case 0x0F:
          return "F";
      }
      // 0x00:
      return "0";
    }

    public static String ByteToHexadecimalString(Byte value)
    {
      Byte Nibble = (Byte)(value >> 4);
      String Result = NibbleToHexadecimalString(Nibble);
      Nibble = (Byte)(0x0F & value);
      Result += NibbleToHexadecimalString(Nibble);
      return Result;
    }

    public static void Int32AdjustRight(Int32 ivalue,
                                        Int32 textsize,
                                        out String text)
    {
      text = ivalue.ToString();
      String Header = "";
      for (Int32 CI = text.Length; CI < textsize; CI++)
      {
        Header += " ";
      }
      text = Header + text;
    }

  }
}
