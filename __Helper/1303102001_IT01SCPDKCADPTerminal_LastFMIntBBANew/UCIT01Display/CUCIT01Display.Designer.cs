﻿namespace UCIT01Display
{
  partial class CUCIT01Display
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.FUCDisplay = new UCIT01Display.CUCDisplay();
      this.SuspendLayout();
      // 
      // FUCDisplay
      // 
      this.FUCDisplay.BackColor = System.Drawing.Color.YellowGreen;
      this.FUCDisplay.Location = new System.Drawing.Point(42, 39);
      this.FUCDisplay.Name = "FUCDisplay";
      this.FUCDisplay.Size = new System.Drawing.Size(462, 101);
      this.FUCDisplay.TabIndex = 1;
      // 
      // CUCIT01Display
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Desktop;
      this.Controls.Add(this.FUCDisplay);
      this.Name = "CUCIT01Display";
      this.Size = new System.Drawing.Size(558, 174);
      this.SizeChanged += new System.EventHandler(this.CUCIT01Display_SizeChanged);
      this.Paint += new System.Windows.Forms.PaintEventHandler(this.CUCIT01Display_Paint);
      this.ResumeLayout(false);

    }

    #endregion

    private CUCDisplay FUCDisplay;
  }
}
