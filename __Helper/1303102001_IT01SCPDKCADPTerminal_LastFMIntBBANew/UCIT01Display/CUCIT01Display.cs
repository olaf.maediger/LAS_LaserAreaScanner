﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UCKeyFrame;
// ??? using IT01System;
using UCNotifier;
//
namespace UCIT01Display
{
  public partial class CUCIT01Display : UserControl
  {
    //
    //-----------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    // ??? private CIT01System FIT01System;
    // private String FEscapePrior;
    private CFrameRounded FFrameOuter; // White
    private CFrameRounded FFrameInner; // Black
    //
    //-----------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------
    //
    public CUCIT01Display()
    {
      InitializeComponent();
      //
      FFrameOuter = new CFrameRounded();
      FFrameOuter.CornerRadius = 8;
      FFrameOuter.BorderWidth = 4;
      FFrameOuter.FillColor = Color.White;
      //
      FFrameInner = new CFrameRounded();
      FFrameInner.CornerRadius = 6;
      FFrameInner.BorderWidth = 0;
      FFrameInner.FillColor = Color.Black;
      //
      // FEscapePrior = "";
    }
    //
    //-----------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

    /*// ??? public void SetIT01System(CIT01System value)
    {
      FIT01System = value;
    }*/
    //
    //-----------------------------------------------------------------
    //  Segment - Event
    //-----------------------------------------------------------------
    //
    private void CUCIT01Display_Paint(object sender, PaintEventArgs e)
    {
      FFrameOuter.Draw(e.Graphics, false, false, 0, 0, Width, Height, "", Font);
      FFrameInner.Draw(e.Graphics, false, false, 20, 20, Width - 20, Height - 20, "", Font);
    }

    private void CUCIT01Display_SizeChanged(object sender, EventArgs e)
    {
      FUCDisplay.Left = 9 * Width / 100;
      FUCDisplay.Top = 20 * Height / 100;
      FUCDisplay.Width = 82 * Width / 100;
      FUCDisplay.Height = 60 * Height / 100;
    }
    //
    //-----------------------------------------------------------------
    //  Segment - Event - Serial
    //-----------------------------------------------------------------
    /* OK, but not needed any longer
    // Esc-Sequences to interprete:
    //    0  1 2 3 4 5
    //  "ESC [ 2 J"           - ClearScreen
    //  "ESC [ 1 ; 1 H"       - SetCursorHome
    //  "ESC [ srow ; scol H" - SetCursorPosition srow scol
    private Boolean HandleEscapeSequence(ref String rxdata)
    {
      //const Char SEQUENCE_ESCAPE = (Char)0x1B;
      const Char SEQUENCE_BRACKETOPEN = (Char)0x5B;
      const Char SEQUENCE_SEMICOLON = ';';
      //
      if (0 < FEscapePrior.Length)
      {
        if (FNotifier is CNotifier)
        {
          FNotifier.Write("EP>" + FEscapePrior + "<CS>" + FEscapePrior + rxdata + "<");
        }
      }
      String CheckSequence = FEscapePrior + rxdata;
      if (0 < CheckSequence.Length)
      {
        if (SEQUENCE_BRACKETOPEN == CheckSequence[0])
        { // Escape-Sequence detected
          if (3 <= CheckSequence.Length)
          {
            if (('2' == CheckSequence[1]) && ('J' == CheckSequence[2]))
            {
              ClearScreen();
              if (FNotifier is CNotifier)
              {
                FNotifier.Write("### ClearScreen");
              }
              if (3 < CheckSequence.Length)
              {
                CheckSequence = CheckSequence.Substring(3, CheckSequence.Length - 3);
              }
              else
              {
                CheckSequence = "";
              }
              rxdata = CheckSequence;
              FEscapePrior = "";
              return true;
            }
            else
              if (5 <= CheckSequence.Length)
              { // /ESC [ rc(1,1,n) ; cc(1,1,n) H // 1 <= n <= 3
                Char[] Separators = new Char[]{ // SEQUENCE_ESCAPE, 
                                                SEQUENCE_BRACKETOPEN, 
                                                SEQUENCE_SEMICOLON 
                                              };
                String[] Tokens = CheckSequence.Split(Separators);
                if (3 <= Tokens.Length)
                {
                  String TokenH = Tokens[2];
                  if (2 <= TokenH.Length)
                  { // "1H" .. "123H" -> 0 .. 3
                    Int32 IndexH = TokenH.IndexOf('H');
                    if ((1 <= IndexH) && (IndexH <= 3))
                    { // H correct existent -> TokenH -> cc
                      TokenH = TokenH.Substring(0, IndexH);
                      Int32 SizeSequence = 3 + Tokens[1].Length + TokenH.Length;
                      Int32 Row;
                      Int32 Col;
                      Int32.TryParse(Tokens[1], out Row);
                      Int32.TryParse(TokenH, out Col);
                      SetCursorPosition(Row - 1, Col - 1);
                      if (FNotifier is CNotifier)
                      {
                        FNotifier.Write("### SetCursorPosition " + Tokens[1] + TokenH);
                      }
                      if (SizeSequence < CheckSequence.Length)
                      {
                        CheckSequence = CheckSequence.Substring(SizeSequence, CheckSequence.Length - SizeSequence);
                        if (FNotifier is CNotifier)
                        {
                          FNotifier.Write("Rest : >" + CheckSequence + "<");
                        }
                      }
                      else
                      {
                        CheckSequence = "";
                      }
                      rxdata = CheckSequence;
                      FEscapePrior = "";
                      return true;
                    }
                  }
                }
              }
          }
          FEscapePrior += rxdata;
          if (FNotifier is CNotifier)
          {
            FNotifier.Write("**PriorSequence on End***>" + FEscapePrior + "<*****");
          }
          rxdata = "";
        }
      }
      return false;
    }*/
    /* -> IT01System
    private void IT01SerialPortOnDataReceived(String rxdata)
    {
      Char[] Separators = new Char[]{ CIT };
      String[] Tokens = rxdata.Split(Separators);
      for (Int32 TI = 0; TI < Tokens.Length; TI++)
      {
        String Token = Tokens[TI];
        if (0 < Token.Length)
        {
          HandleEscapeSequence(ref Token);
          if (0 < Token.Length)
          {
            // NC Separators = new Char[] { SEQUENCE_CR, SEQUENCE_LF };
            // NC String[] Lines = Token.Split(Separators);
            // NCfor (Int32 LI = 0; LI < Lines.Length; LI++)
            // NC{
            String Line = Token;
            if (0 < Line.Length)
            {
              if ('[' == Line[0])
              {
                FNotifier.Write("?????>" + Line + "<????");
              }
              else
              {
                WriteText(Line);
              }
            }
            // NC}
          }
        }
      }
    }*/
    //
    //-----------------------------------------------------------------
    //  Segment - Management
    //-----------------------------------------------------------------
    //
    public void ClearScreen()
    {
      String Line = String.Format("*** UCIT01Display.ClearScreen");
      FNotifier.Write(Line);
      FUCDisplay.ClearScreen();
    }

    public void SetCursorPosition(Int32 row, Int32 col)
    {
      String Line = String.Format("*** UCIT01Display.SetCursorPosition[{0}, {1}]", row, col);
      FNotifier.Write(Line);
      FUCDisplay.SetCursorPosition(row, col);
    }

    public void WriteText(String text)
    {
      String Line = String.Format("*** UCIT01Display.WriteText[{0}]", text);
      FNotifier.Write(Line);
      FUCDisplay.WriteText(text);
    }

    public void WriteText(Int32 row, Int32 col, String text)
    {
      String Line = String.Format("*** UCIT01Display.WriteText[{0}, {1}][{2}]",
                                  row, col, text);
      FNotifier.Write(Line);
      FUCDisplay.WriteText(row, col, text);
    }

    public void WriteLine(String text)
    {
      String Line = String.Format("*** UCIT01Display.WriteLine[{0}]", text);
      FNotifier.Write(Line);
      FUCDisplay.WriteLine(text);
    }

    public void WriteDatagram01(Byte datagramsize,
                                Byte[] datagram)
    {
      FUCDisplay.WriteDatagram01(datagramsize, datagram);
    }
    public void WriteDatagram23(Byte datagramsize,
                                Byte[] datagram)
    {
      FUCDisplay.WriteDatagram23(datagramsize, datagram);
    }

  }
}
