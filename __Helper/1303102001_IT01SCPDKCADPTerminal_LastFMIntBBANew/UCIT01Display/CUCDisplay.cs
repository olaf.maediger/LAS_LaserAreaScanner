﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCIT01Display
{
  public partial class CUCDisplay : UserControl
  {
    private const Int32 INIT_ROWSIZE = 4;
    private const Int32 INIT_COLSIZE = 20;
    private const Char INIT_CHARACTER = '.';
    private const Int32 INIT_CURSORROW = 0;
    private const Int32 INIT_CURSORCOL = 0;
    private const Int32 INIT_DISPLAYOFFSET_ROW = 4;
    private const Int32 INIT_DISPLAYOFFSET_COL = 1;

    private Char[,] FValues;
    private Int32 FCursorRow;
    private Int32 FCursorCol;

    public CUCDisplay()
    {
      InitializeComponent();
      FValues = new Char[INIT_ROWSIZE, INIT_COLSIZE];
      Init();
    }

    public Int32 RowSize
    {
      get { return FValues.GetLength(0); }
    }
    public Int32 ColSize
    {
      get { return FValues.GetLength(1); }
    }

    private Int32 CIL
    {
      get { return 0; }
    }
    private Int32 CIH
    {
      get { return ColSize - 1; }
    }
    private Int32 RIL
    {
      get { return 0; }
    }
    private Int32 RIH
    {
      get { return RowSize - 1; }
    }

    private void CUCDisplay_Paint(object sender, PaintEventArgs e)
    {
      Brush B = new SolidBrush(Color.FromArgb(0x60, 0x60, 0x60));
      Font F = new Font("Arial", 18f, FontStyle.Bold);
      StringFormat SF = new StringFormat();
      SF.Alignment = StringAlignment.Center;
      SF.LineAlignment = StringAlignment.Center;
     // debug Pen RP = new Pen(Color.Red);
      Int32 DX = this.Width / 21;
      Int32 DY = this.Height / 5;
      Int32 YS = DY / 2;
      for (Int32 RI = 0; RI < RowSize; RI++)
      {
        Int32 XS = DX / 2;
        for (Int32 CI = 0; CI < ColSize; CI++)
        {
          Rectangle R = new Rectangle(XS, YS, DX, DY);
          String SC = new String(FValues[RI, CI], 1);
          // only debug !!! e.Graphics.DrawRectangle(RP, R);
          R.Offset(INIT_DISPLAYOFFSET_COL, INIT_DISPLAYOFFSET_ROW);
          e.Graphics.DrawString(SC, F, B, R, SF);
          XS += DX;
        }
        YS += DY;
      }
    }

    private void Init()
    {
      ClearScreen();
    }

    private void IncrementCursorPosition()
    {
      if (FCursorCol < CIH)
      {
        FCursorCol++;
      }
      else
      {
        if (FCursorRow < RIH)
        {
          FCursorRow++;
          FCursorCol = CIL;
        }
        else
        {
          // nothing...
        }
      }
    }

    private void IncrementCursorRow()
    {
      if (FCursorRow < RIH)
      {
        FCursorRow++;
        FCursorCol = CIL;
      }
    }


    public void ClearScreen()
    {
      SetCursorPosition(RIL, CIL);
      for (Int32 RI = 0; RI < RowSize; RI++)
      {
        for (Int32 CI = 0; CI < ColSize; CI++)
        {
          FValues[RI, CI] = INIT_CHARACTER;
        }
      }
    }

    public void SetCursorPosition(Int32 row, Int32 col)
    {
      if ((CIL <= col) && (col <= CIH))
      {
        if ((RIL <= row) && (row <= RIH))
        {
          FCursorRow = row;
          FCursorCol = col;
        }
      }
    }

    public void WriteText(String text)
    {
      for (Int32 CI = 0; CI < text.Length; CI++)
      {
        FValues[FCursorRow, FCursorCol] = text[CI];
        IncrementCursorPosition();
      }
      Invalidate();
    }

    public void WriteText(Int32 row, Int32 col, String text)
    {
      for (Int32 CI = 0; CI < text.Length; CI++)
      {
        FValues[FCursorRow, FCursorCol] = text[CI];
        IncrementCursorPosition();
      }
      Invalidate();
    }

    public void WriteLine(String text)
    {
      for (Int32 CI = 0; CI < text.Length; CI++)
      {
        FValues[FCursorRow, FCursorCol] = text[CI];
        IncrementCursorPosition();
      }
      IncrementCursorRow();
      Invalidate();
    }

    public void WriteDatagram01(Byte datagramsize,
                                Byte[] datagram)
    {
      // NC ClearScreen();
      SetCursorPosition(0, 0);
      for (Int32 CI = 0; CI < datagram.Length; CI++)
      {
        FValues[FCursorRow, FCursorCol] = (Char)datagram[CI];
        IncrementCursorPosition();
      }
      Invalidate();
    }

    public void WriteDatagram23(Byte datagramsize,
                                Byte[] datagram)
    {
      SetCursorPosition(2, 0);
      for (Int32 CI = 0; CI < datagram.Length; CI++)
      {
        FValues[FCursorRow, FCursorCol] = (Char)datagram[CI];
        IncrementCursorPosition();
      }
      Invalidate();
    }

  }
}
