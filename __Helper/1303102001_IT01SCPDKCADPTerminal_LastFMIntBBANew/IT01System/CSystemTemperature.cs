﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IT01System
{
  public class CSystemTemperature
  {
    public const Byte DATAGRAMSIZE_SYSTEMTEMPERATURE = 0x1A;
    //
    public const Byte DATAGRAMINDEX_TEMPERATUREPRESET = 0x16;
    public const Byte DATAGRAMINDEX_TEMPERATUREACTUAL = 0x18;
    //
    public const Byte INIT_TEMPERATUREACTUAL_HIGH = 0x12;
    public const Byte INIT_TEMPERATUREACTUAL_LOW = 0x34;
    //
    public const Byte INIT_TEMPERATUREPRESET_HIGH = 0x45;
    public const Byte INIT_TEMPERATUREPRESET_LOW = 0x67;
    //
  }
}
