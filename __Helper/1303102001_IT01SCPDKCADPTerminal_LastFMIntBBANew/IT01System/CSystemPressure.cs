﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IT01System
{
  public class CSystemPressure
  {
    public const Byte DATAGRAMSIZE_SYSTEMPRESSURE = 0x1A;
    //
    public const Byte DATAGRAMINDEX_PRESSUREPRESET = 0x12;
    public const Byte DATAGRAMINDEX_PRESSUREACTUAL = 0x14;
    //
    public const Byte INIT_PRESSUREACTUAL_HIGH = 0x12;
    public const Byte INIT_PRESSUREACTUAL_LOW = 0x34;
    //
  }
}
