﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Convert;
//
namespace IT01System
{
  public delegate void DOnSystemStateChanged(ESystemState systemstate);

  public class CSystemState
  {
    public const Byte DATAGRAMSIZE_SYSTEMSTATE = 0x01;
    //
    public const Byte DATAGRAMINDEX_SYSTEMSTATE = 0x00;
    //
    private ESystemState FState;
    private DOnSystemStateChanged FOnSystemStateChanged;
    //
    public CSystemState()
    {
      FState = ESystemState.Main;
    }

    public ESystemState GetState()
    {
      return FState;
    }
    public void SetState(ESystemState value)
    {
      FState = value;
      if (FOnSystemStateChanged is DOnSystemStateChanged)
      {
        FOnSystemStateChanged(value);
      }
    }

    public void SetOnSystemStateChanged(DOnSystemStateChanged value)
    {
      FOnSystemStateChanged = value;
    }

    public Boolean BuildFromDatagram(Byte datagramsize,
                                     Byte[] datagram)
    {
      if (datagram is Byte[])
      {
        if (datagramsize == datagram.Length)
        {
          if (DATAGRAMSIZE_SYSTEMSTATE == datagramsize)
          {
            SetState((ESystemState)datagram[DATAGRAMINDEX_SYSTEMSTATE]);
            return true;
          }
        }
      }
      return false;
    }
	
    public Boolean ConvertToDatagram(out Byte datagramsize,
                                     out Byte[] datagram)
    {
      datagramsize = DATAGRAMSIZE_SYSTEMSTATE;
      datagram = new Byte[datagramsize];
      datagram[DATAGRAMINDEX_SYSTEMSTATE] = (Byte)FState;
      return true;
    }
	
  }
}
