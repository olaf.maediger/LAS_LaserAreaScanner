﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using Convert;
using IT01SerialPort;
using IT01BBAProtocol;
//
namespace IT01System
{
  public enum ESystemState
  {
    Undefined = 0,
    Terminated = 1,
    //
    Main = 2,
    BlockSelection = 3,
    ProgramSelection = 4,
    //
    WaterIntrusionTest = 5,
    DiffusionTest = 6,
    PressureDropTest = 7,
    WaterIntrusionTestBig = 8,
    DiffusionBubbleTest = 9,
    //
    Function = 10,
    //
    DefineVolumeCheck = 11,
    VolumeCheckFilterSystem = 12,
    //
    WaterIntrusionTestSelectPressure = 13,
    WaterIntrusionTestSelectTime = 14,
    WaterIntrusionTestFillVolume = 15,
    WaterIntrusionTestResult = 16,
    //
    DiffusionTestSelectPressure = 17,
    DiffusionTestSelectTime = 18,
    DiffusionTestFillVolume = 19,
    DiffusionTestResult = 20,
    //
    PressureDropTestSelectPressure = 21,
    PressureDropTestSelectTime = 22,
    PressureDropTestFillVolume = 23,
    PressureDropTestResult = 24,
    //
    WaterIntrusionTestBigSelectPressure = 25,
    WaterIntrusionTestBigSelectTime = 26,
    WaterIntrusionTestBigFillVolume = 27,
    WaterIntrusionTestBigResult = 28,
    // 
    DiffusionBubbleTestSelectPressure = 29,
    DiffusionBubbleTestSelectTemperature = 30,
    DiffusionBubbleTestExecution = 31,
    DiffusionBubbleTestResult = 32
  };
  
  public delegate Boolean DOnExecuteCommand(Byte deviceid, Byte command, Byte[] parameters);

  public class CIT01System
  {
    //
    //--------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------
    //
    // NC  public const Byte TOKEN_STX = 0x02; // detect this on receiving!
    // NC public const Byte TOKEN_ETX = 0x03; // detect this on receiving!
    // NC public const Byte TOKEN_CR = 0x0D;  // ignore this on receiving!
    // NC public const Byte TOKEN_LF = 0x0A;  // ignore this on receiving!
    //
    public const Int32 SIZE_COMMANDBUFFER_MINIMAL = 3; // STX DID CMD CS ETX 
    public const Byte INIT_CHECKSUMPRESET = 0x00;
    //
    //
    // FTAProtocol - Source DeviceIDs
    // NC public const Byte DEVICEID_DEBUG = 0x01;
    // NC public const Byte DEVICEID_SCP = 0x11;  // SystemControlProcessor
    // NC public const Byte DEVICEID_ADC = 0x21;  // AnalogDigitalController
    // NC public const Byte DEVICEID_DKC = 0x31;  // DisplayKeyboardController
    //
    // Command SCP - System
    // NC public const Byte COMMAND_SETSYSTEMSTATE = 0xF1;
    // Command DKC - Display
    // NC public const Byte COMMAND_CLEARSCREEN = 0x60;
    // NC public const Byte COMMAND_SETCURSORHOME = 0x61;
    // NC public const Byte COMMAND_SETCURSORPOSITION = 0x62;
    // NC public const Byte COMMAND_WRITETEXT = 0x63;
    // NC public const Byte COMMAND_WRITELINE = 0x64;
    // Command DKC - KeyboardFront
    // NC public const Byte COMMAND_KEYPRESSED = 0x91;
    // Command DKC - KeyboardExternal
    // Command ADC - ADConverter
    // Command ADC - DAConverter
    //
    //--------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CIT01SerialPort FIT01SerialPort;  // Only Reference!!!!
    private CBBAProtocol FBBAProtocol;        // Only Reference!!!!
    private DOnExecuteCommand FOnExecuteCommand;
    private Byte[] FBufferPreset;
    private DOnDatagramReceived FOnDatagramReceived;
    private CSystemState FSystemState;
    //
    //--------------------------------------------------------------------
    //  Segment - 
    //--------------------------------------------------------------------
    //
    public CIT01System()
    {
      FBufferPreset = new Byte[0];
      FSystemState = new CSystemState();
    }
    //
    //--------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnExecuteCommand(DOnExecuteCommand value)
    {
      FOnExecuteCommand = value;
    }

    public void SetOnDatagramReceived(DOnDatagramReceived value)
    {
      FOnDatagramReceived = value;
    }

    public void SetOnSystemStateChanged(DOnSystemStateChanged value)
    {
      FSystemState.SetOnSystemStateChanged(value);
    }

    public CSystemState SystemState
    {
      get { return FSystemState; }
    }

    public String[] GetSystemStates()
    {
      return Enum.GetNames(typeof(ESystemState));
    }

    public Array GetSystemValues()
    {
      return Enum.GetValues(typeof(ESystemState));
    }
    //
    //--------------------------------------------------------------------
    //  Segment - Local Helper - Command
    //--------------------------------------------------------------------
    //
    /*private Boolean AnalyseCommand(Byte[] buffer,
                                   ref Byte deviceid,
                                   ref Byte command,
                                   ref Byte[] parameters)
    {
      Int32 BIL = 0;
      Int32 BIH = buffer.Length - 1;
      if ((TOKEN_STX == buffer[BIL]) && (TOKEN_ETX == buffer[BIH]))
      { // Convert ByteAscii-Buffer -> Byte-Buffer
        Int32 BL = buffer.Length;
        Int32 BI = 1;
        Int32 BBL = (BL - 2) >> 1;
        Byte[] BufferByte = new Byte[BBL];
        for (Int32 BBI = 0; BBI < BBL; BBI++)
        {
          Char CharacterHigh = (Char)buffer[BI];
          BI++;
          Char CharacterLow = (Char)buffer[BI];
          BI++;
          Byte BValue = Transform.CTransform.AsciiByteToByte(CharacterHigh, CharacterLow);
          BufferByte[BBI] = BValue;
        }
        if (SIZE_COMMANDBUFFER_MINIMAL <= BBL)
        {
          Byte Checksum = INIT_CHECKSUMPRESET;
          //
          Int32 BufferIndex = 0;
          deviceid = BufferByte[BufferIndex];
          Checksum ^= deviceid;
          //
          BufferIndex++;
          command = BufferByte[BufferIndex];
          Checksum ^= command;
          //
          parameters = new Byte[BBL - SIZE_COMMANDBUFFER_MINIMAL];
          for (Int32 PI = 0; PI < parameters.Length; PI++)
          {
            BufferIndex++;
            parameters[PI] = BufferByte[BufferIndex];
            Checksum ^= parameters[PI];
          }
          //
          BufferIndex++;
          Byte ChecksumReceived = BufferByte[BufferIndex];
          //
          //!!!!!!!!!!!!!!!!!!!!!!!!!!! return (Checksum == ChecksumReceived);
          return true;
        }
      }
      return false;
    }*/

    /*private Boolean ExecuteCommand(Byte deviceid,
                                   Byte command,
                                   Byte[] parameters)
    { / * / Debug:
      String Line = String.Format("RxdCommand x{0:X2} [DeviceID: x{1:X2}] Parameters[",
                                  command, deviceid);
      for (Int32 PI = 0; PI < parameters.Length; PI++)
      {
        Line += String.Format("x{0:X2}", parameters[PI]);
      }
      Line += "]";
      FNotifier.Write(Line);* /
      // Execute: -> Main
      if (FOnExecuteCommand is DOnExecuteCommand)
      {
        FOnExecuteCommand(deviceid, command, parameters);
      }
      //
      return false;
    }*/
    //
    //--------------------------------------------------------------------
    //  Segment - Callback - Serial
    //--------------------------------------------------------------------
    //
    // Main <- BBAProtocol <- LPCTXD2 <- SCP
    private void BBAProtocolOnDatagramReceived(Byte datagramid,
                                               Byte datagramsize,
                                               Byte[] datagram)
    { // no local execution -> Main!!!
      if (FOnDatagramReceived is DOnDatagramReceived)
      {
        FOnDatagramReceived(datagramid, datagramsize, datagram);
      }
    }

 
    //
    //--------------------------------------------------------------------
    //  Segment - Static Helper
    //--------------------------------------------------------------------
    //
    public static ESystemState TextToSystemState(String text)
    {
      try
      {
        ESystemState Result = (ESystemState)Enum.Parse(typeof(ESystemState), text);
        return Result;
      }
      catch (Exception)
      {
        return ESystemState.Main;
      }
    }
    //
    //--------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------
    //
    public Boolean IsOpen()
    {
      return FBBAProtocol.IsOpen();
    }

    public Boolean Open(CIT01SerialPort serialport,
                        CBBAProtocol bbaprotocol)
    {
      if (serialport.IsOpen() && bbaprotocol.IsOpen())
      {
        FIT01SerialPort = serialport;
        FBBAProtocol = bbaprotocol;
        FBBAProtocol.SetOnDatagramReceived(BBAProtocolOnDatagramReceived);
        return true;
      }
      return false;
    }

    public Boolean Close()
    {
      if (FIT01SerialPort.IsOpen())
      {
        return FIT01SerialPort.Close();
      }
      if (FBBAProtocol.IsOpen())
      {
        return FBBAProtocol.Close();
      }
      return false;
    }
    //
    //--------------------------------------------------------------------
    //  Segment - Public Method
    //--------------------------------------------------------------------
    //
    //
    //-----------------------------------------------------------
    //  Segment - High Level
    //-----------------------------------------------------------
    //
    public Boolean WriteDatagramByteAscii(Byte datagramid,
                                          Byte datagramsize,
                                          Byte[] datagram)
    {
      return FBBAProtocol.WriteDatagramByteAscii(datagramid, datagramsize, datagram);
    }

    public Boolean WriteDatagram(Byte datagramid,
                                 Byte datagramsize,
                                 Byte[] datagram)
    {
      return FBBAProtocol.WriteDatagramTextAscii(datagramid, datagramsize, datagram);
    }

    public Boolean ExecuteDatagram(Byte datagramsize,
                                   Byte[] datagram)
    {
      if (FSystemState.BuildFromDatagram(datagramsize, datagram))
      {
        return true;
      }
      return false;
    }

  }
}
