﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IT01System
{
  public class CSystemDateTime
  {
    public const Byte DATAGRAMSIZE_SYSTEMDATETIME = 0x1A;
    //
    public const Byte DATAGRAMINDEX_YEAR = 0x02;
    public const Byte DATAGRAMINDEX_MONTH = 0x04;
    public const Byte DATAGRAMINDEX_DAY = 0x06;
    public const Byte DATAGRAMINDEX_DAYWEEK = 0x08;
    public const Byte DATAGRAMINDEX_HOURS = 0x0A;
    public const Byte DATAGRAMINDEX_MINUTES = 0x0C;
    public const Byte DATAGRAMINDEX_SECONDS = 0x0E;
    public const Byte DATAGRAMINDEX_MILLIS10 = 0x10;
    //
  }
}
