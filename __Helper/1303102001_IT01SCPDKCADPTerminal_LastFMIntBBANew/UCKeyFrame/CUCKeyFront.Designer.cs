﻿namespace UCKeyFrame
{
  partial class CUCKeyFront
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.tmrKeyPressed = new System.Windows.Forms.Timer(this.components);
      this.SuspendLayout();
      // 
      // tmrKeyPressed
      // 
      this.tmrKeyPressed.Interval = 333;
      // 
      // CUCKeyFront
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
      this.Name = "CUCKeyFront";
      this.Size = new System.Drawing.Size(150, 69);
      this.Title = "keyfront";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Timer tmrKeyPressed;
  }
}
