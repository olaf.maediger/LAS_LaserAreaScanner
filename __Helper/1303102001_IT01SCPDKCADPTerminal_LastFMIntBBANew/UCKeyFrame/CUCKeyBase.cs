﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Task;
using UCNotifier;
//
namespace UCKeyFrame
{
  /*public delegate void DOnKeyPressed(Byte keycode,
                                     Char keycharacter,
                                     String keycommand);*/
  public delegate void DOnKeyDown(Byte keycode,
                                  Char keycharacter,
                                  String keycommand);
  public delegate void DOnKeyUp(Byte keycode,
                                Char keycharacter,
                                String keycommand);

  public partial class CUCKeyBase : UserControl
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Constant
    //---------------------------------------------------------------------------------
    //
    private Byte INIT_KEYCODE = 0x30;
    private Char INIT_KEYCHARACTER = '0';
    private String INIT_KEYCOMMAND = "0";
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    protected CFrameRounded FFramePad;
    //protected DOnKeyPressed FOnKeyPressed;
    protected DOnKeyDown FOnKeyDown;
    protected DOnKeyUp FOnKeyUp;
    protected Byte FKeyCode;
    protected Char FKeyCharacter;
    protected String FKeyCommand;
    protected Boolean FIsKeyPressed;
    private CTask FTaskKeyPressed;
    //
    //---------------------------------------------------------------------------------
    // Segment - Constructor
    //---------------------------------------------------------------------------------
    //
    public CUCKeyBase()
    {
      InitializeComponent();
      FIsKeyPressed = false;
      // tmrKeyPressed.Tag = 0;
      //
      SetStyle(ControlStyles.Selectable, true);
      DoubleBuffered = true;
      //
      Resize += new EventHandler(ThisOnResize);
      Paint += new PaintEventHandler(ThisOnPaint);
      Enter += new EventHandler(ThisOnEnter);
      Leave += new EventHandler(ThisOnLeave);
      FTaskKeyPressed = null;
      MouseClick += new MouseEventHandler(ThisOnMouseClick);
      //
      FFramePad = new CFrameRounded();
      FFramePad.EnableKeyPressed = true;
      //
      Title = "keybase";
      Font = new Font("Arial", 12);
      TextColor = Color.Black;
      TextColorFocus = Color.Black;
      CornerRadius = 8;
      BorderWidth = 1;
      BorderColor = Color.Black;
      BorderColorFocus = Color.Black;
      FillColor = Color.White;
      FillColorFocus = Color.White;// debug Color.Yellow;
      PressColor = Color.Red;
      //
      KeyCode = INIT_KEYCODE;
      KeyCharacter = INIT_KEYCHARACTER;
      KeyCommand = INIT_KEYCOMMAND;
      //
      Width = 100;
      Height = 50;
    }
    //
    //---------------------------------------------------------------------------------
    // Segment - Property
    //---------------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

    /*public void SetOnKeyPressed(DOnKeyPressed value)
    {
      FOnKeyPressed = value;
    }*/
    public void SetOnKeyDown(DOnKeyDown value)
    {
      FOnKeyDown = value;
    }
    public void SetOnKeyUp(DOnKeyUp value)
    {
      FOnKeyUp = value;
    }

    public Byte KeyCode
    {
      get { return FKeyCode; }
      set { FKeyCode = value; }
    }

    public Char KeyCharacter
    {
      get { return FKeyCharacter; }
      set { FKeyCharacter = value; }
    }

    public String KeyCommand
    {
      get { return FKeyCommand; }
      set { FKeyCommand = value; }
    }

    public String Title
    {
      get { return base.Text; }
      set { base.Text = value; Invalidate(); }
    }
    //
    // TextColor
    //
    private void SetTextColor(Color value)
    {
      FFramePad.TextColor = value;
      Invalidate();
    }
    public Color TextColor
    {
      get { return FFramePad.TextColor; }
      set { SetTextColor(value); }
    }
    //
    // TextColorFocus
    //
    private void SetTextColorFocus(Color value)
    {
      FFramePad.TextColorFocus = value;
      Invalidate();
    }
    public Color TextColorFocus
    {
      get { return FFramePad.TextColorFocus; }
      set { SetTextColorFocus(value); }
    }
    //
    // ConerRadius
    //
    private void SetCornerRadius(Int32 value)
    {
      FFramePad.CornerRadius = value;
      Invalidate();
    }
    public Int32 CornerRadius
    {
      get { return FFramePad.CornerRadius; }
      set { SetCornerRadius(value); }
    }
    //
    // BorderWidth
    //
    private void SetBorderWidth(Int32 value)
    {
      FFramePad.BorderWidth = value;
      Invalidate();
    }
    public Int32 BorderWidth
    {
      get { return FFramePad.BorderWidth; }
      set { SetBorderWidth(value); }
    }
    //
    // BorderColor
    //
    private void SetBorderColor(Color value)
    {
      FFramePad.BorderColor = value;
      Invalidate();
    }
    public Color BorderColor
    {
      get { return FFramePad.BorderColor; }
      set { SetBorderColor(value); }
    }
    //
    // BorderColorFocus
    //
    private void SetBorderColorFocus(Color value)
    {
      FFramePad.BorderColorFocus = value;
      Invalidate();
    }
    public Color BorderColorFocus
    {
      get { return FFramePad.BorderColorFocus; }
      set { SetBorderColorFocus(value); }
    }
    //
    // FillColor
    //
    private void SetFillColor(Color value)
    {
      FFramePad.FillColor = value;
      Invalidate();
    }
    public Color FillColor
    {
      get { return FFramePad.FillColor; }
      set { SetFillColor(value); }
    }
    //
    // FillColorFocus
    //
    private void SetFillColorFocus(Color value)
    {
      FFramePad.FillColorFocus = value;
      Invalidate();
    }
    public Color FillColorFocus
    {
      get { return FFramePad.FillColorFocus; }
      set { SetFillColorFocus(value); }
    }
    //
    // PressColorFocus
    //
    private void SetPressColor(Color value)
    {
      FFramePad.PressColor = value;
      Invalidate();
    }
    public Color PressColor
    {
      get { return FFramePad.PressColor; }
      set { SetPressColor(value); }
    }
    //
    //---------------------------------------------------------------------------------
    // Segment - Event
    //---------------------------------------------------------------------------------
    //
    private void ThisOnResize(object sender, EventArgs e)
    {
      Invalidate();
    }

    protected void ThisOnPaint(object sender, PaintEventArgs e)
    {
      FFramePad.Draw(e.Graphics,
                     Focused, (FTaskKeyPressed is CTask),
                     0, 0, Width, Height,
                     Text, Font);
      // debug e.Graphics.DrawRectangle(new Pen(Color.Green), 0, 0, Width - 1, Height - 1);
    }

    private void ThisOnEnter(object sender, EventArgs e)
    {
      Invalidate();
    }

    private void ThisOnLeave(object sender, EventArgs e)
    {
      Invalidate();
    }

    private void ThisOnMouseClick(object sender, MouseEventArgs arguments)
    {
      PressKey();
    }

    public void PressKey()
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write("PressKey");
      }
      FIsKeyPressed = true;
      FTaskKeyPressed = new CTask(OnTaskStart, 
                                  OnTaskExecute, 
                                  OnTaskEnd,
                                  OnExecutionAbort);
      FTaskKeyPressed.Start();
      Invalidate();
      /*if (FOnKeyPressed is DOnKeyPressed)
      {
        FOnKeyPressed(FKeyCode, FKeyCharacter, FKeyCommand);
      }*/
    }



    private Boolean OnTaskStart(RTaskData data)
    {
      // debug 
      if (FNotifier is CNotifier)
      {
        FNotifier.Write("KeyBase: OnTaskStart");
      }
      if (FOnKeyDown is DOnKeyDown)
      {
        FOnKeyDown(FKeyCode, FKeyCharacter, FKeyCommand);
      }
      return true;
    }
    private Boolean OnTaskExecute(RTaskData data)
    {
      // debug 
      if (FNotifier is CNotifier)
      {
        FNotifier.Write("KeyBase: OnTaskExecute[" + data.Counter + "]");
      }
      Thread.Sleep(500);
      return false;
    }
    private Boolean OnTaskEnd(RTaskData data)
    {
      // debug 
      if (FNotifier is CNotifier)
      {
        FNotifier.Write("KeyBase: OnTaskEnd");
      }
      FIsKeyPressed = false;
      FTaskKeyPressed = null;
      Invalidate();
      if (FOnKeyUp is DOnKeyUp)
      {
        FOnKeyUp(FKeyCode, FKeyCharacter, FKeyCommand);
      }
      return true;
    }

    private Boolean OnExecutionAbort(RTaskData data)
    {
      return true;
    }
  }
}
