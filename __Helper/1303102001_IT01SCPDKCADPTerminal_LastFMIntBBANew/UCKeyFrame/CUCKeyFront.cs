﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCKeyFrame
{
  public partial class CUCKeyFront : CUCKeyBase
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //
    private CFrameRounded FFrameBottom;
    //
    //---------------------------------------------------------------------------------
    // Segment - Constructor
    //---------------------------------------------------------------------------------
    //
    public CUCKeyFront() 
    {
      InitializeComponent();
      tmrKeyPressed.Tag = false;
      //
      FFrameBottom = new CFrameRounded();
      FFrameBottom.EnableKeyPressed = false;
      //
      base.Paint -= base.ThisOnPaint;
      Paint += ThisOnPaint;
      //
      Title = "keyfront";
      Font = new Font("Arial", 12);
      TextColor = Color.Black;
      TextColorFocus = Color.Black;
      CornerRadius = 8;
      BorderWidth = 1;
      BorderColor = Color.Black;
      BorderColorFocus = Color.Black;
      FillColor = Color.White;
      FillColorFocus = Color.White;// debug Color.Yellow;
      PressColor = Color.Red;
      //
      Width = 80;
      Height = 50;
    }
    //
    //---------------------------------------------------------------------------------
    // Segment - Property
    //---------------------------------------------------------------------------------
    //
    //
    // ConerRadius
    //
    private void SetCornerRadius(Int32 value)
    {
      FFrameBottom.CornerRadius = value;
      base.CornerRadius = value;
    }
    public new Int32 CornerRadius
    {
      get { return base.CornerRadius; }
      set { SetCornerRadius(value); }
    }
    //
    // BorderWidth
    //
    private void SetBorderWidth(Int32 value)
    {
      FFrameBottom.BorderWidth = value;
      base.BorderWidth = value;
    }
    public new Int32 BorderWidth
    {
      get { return base.BorderWidth; }
      set { SetBorderWidth(value); }
    }
    //
    // BorderColor
    //
    private void SetBorderColor(Color value)
    {
      FFrameBottom.BorderColor = value;
      base.BorderColor = value;
    }
    public new Color BorderColor
    {
      get { return base.BorderColor; }
      set { SetBorderColor(value); }
    }
    //
    // FillColor
    //
    private void SetFillColor(Color value)
    {
      FFrameBottom.FillColor = value;
      base.FillColor = value;
    }
    public new Color FillColor
    {
      get { return base.FillColor; }
      set { SetFillColor(value); }
    }
    //
    //---------------------------------------------------------------------------------
    // Segment - Event
    //---------------------------------------------------------------------------------
    //
    private new void ThisOnPaint(object sender, PaintEventArgs e)
    {
      FFrameBottom.Draw(e.Graphics,
                        Focused, FIsKeyPressed, 
                        0, 30 * Height / 100, Width, Height,
                        "", Font);
      FFramePad.Draw(e.Graphics,
                Focused, FIsKeyPressed, 
                24 * Width / 100, 0, 77 * Width / 100, 70 * Height / 100,
                Text, Font);
      // debug       e.Graphics.DrawRectangle(new Pen(Color.Green), 0, 0, Width - 1, Height - 1);
    }


  }
}
