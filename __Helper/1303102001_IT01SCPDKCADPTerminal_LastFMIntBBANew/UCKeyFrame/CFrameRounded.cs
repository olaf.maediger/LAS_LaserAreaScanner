﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
//
namespace UCKeyFrame
{
  public class CFrameRounded
  {
    private Boolean FEnableKeyPressed;
    private Color FTextColor;
    private Color FTextColorFocus;
    private Int32 FCornerRadius;
    private Int32 FBorderWidth;
    private Color FBorderColor;
    private Color FBorderColorFocus;
    private Color FFillColor;
    private Color FFillColorFocus;
    private Color FPressColor;

    public CFrameRounded()
    {
      FEnableKeyPressed = true;
      FTextColor = Color.Black;
      FTextColorFocus = Color.Black;
      FCornerRadius = 20;
      FBorderWidth = 6;
      FBorderColor = Color.Black;
      FBorderColorFocus = Color.Black;
      FFillColor = Color.White;
      FFillColorFocus = Color.White;
      FPressColor = Color.Red;
    }

    public Boolean EnableKeyPressed
    {
      get { return FEnableKeyPressed; }
      set { FEnableKeyPressed = value; }
    }

    private void SetTextColor(Color value)
    {
      FTextColor = value;
    }
    public Color TextColor
    {
      get { return FTextColor; }
      set { SetTextColor(value); }
    }

    private void SetTextColorFocus(Color value)
    {
      FTextColorFocus = value;
    }
    public Color TextColorFocus
    {
      get { return FTextColorFocus; }
      set { SetTextColorFocus(value); }
    }

    private void SetCornerRadius(Int32 value)
    {
      FCornerRadius = value;
    }
    public Int32 CornerRadius
    {
      get { return FCornerRadius; }
      set { SetCornerRadius(value); }
    }

    private void SetBorderWidth(Int32 value)
    {
      FBorderWidth = value;
    }
    public Int32 BorderWidth
    {
      get { return FBorderWidth; }
      set { SetBorderWidth(value); }
    }

    private void SetBorderColor(Color value)
    {
      FBorderColor = value;
    }
    public Color BorderColor
    {
      get { return FBorderColor; }
      set { SetBorderColor(value); }
    }

    private void SetBorderColorFocus(Color value)
    {
      FBorderColorFocus = value;
    }
    public Color BorderColorFocus
    {
      get { return FBorderColorFocus; }
      set { SetBorderColorFocus(value); }
    }

    private void SetFillColor(Color value)
    {
      FFillColor = value;
    }
    public Color FillColor
    {
      get { return FFillColor; }
      set { SetFillColor(value); }
    }

    private void SetFillColorFocus(Color value)
    {
      FFillColorFocus = value;
    }
    public Color FillColorFocus
    {
      get { return FFillColorFocus; }
      set { SetFillColorFocus(value); }
    }

    private void SetPressColor(Color value)
    {
      FPressColor = value;
    }
    public Color PressColor
    {
      get { return FPressColor; }
      set { SetPressColor(value); }
    }




    public void Draw(Graphics graphics, 
                     Boolean focused, Boolean keypressed,
                     Int32 x0, Int32 y0,
                     Int32 width, Int32 height,
                     String text, Font font)
    {
      Brush BBorder = null;
      Brush BFill = null;
      Brush BText = null;
      StringFormat SF = new StringFormat();
      SF.Alignment = StringAlignment.Center;
      SF.LineAlignment = StringAlignment.Center;
      graphics.SmoothingMode = SmoothingMode.AntiAlias;
      graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
      graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
      //
      if (focused)
      {
        BBorder = new SolidBrush(FBorderColorFocus);
        BFill = new SolidBrush(FFillColorFocus);
        BText = new SolidBrush(FTextColorFocus);
      }
      else
      {
        BBorder = new SolidBrush(FBorderColor);
        BFill = new SolidBrush(FFillColor);
        BText = new SolidBrush(FTextColor);
      }
      if (keypressed && FEnableKeyPressed)
      {
        BFill = new SolidBrush(FPressColor);
      }
      //
      Int32 PX0 = x0;
      Int32 PY0 = y0;
      Int32 PDX = width - 1;
      Int32 PDY = height - 1;
      //
      Int32 RO = Math.Max(1, FCornerRadius);
      RO = Math.Min(RO, height / 2);
      Int32 RI = Math.Max(1, 3 * RO - FBorderWidth);
      //    
      // Rectangle - Top
      Rectangle RB = new Rectangle(PX0 + 3 * RO / 2, PY0, 1 + PDX - 3 * RO - PX0, FBorderWidth);
      graphics.FillRectangle(BBorder, RB);
      // Rectangle - Bottom
      RB = new Rectangle(PX0 + 3 * RO / 2, PDY - FBorderWidth, 1 + PDX - 3 * RO - PX0, FBorderWidth);
      graphics.FillRectangle(BBorder, RB);
      // Rectangle - Left
      RB = new Rectangle(PX0, PY0 + 3 * RO / 2, FBorderWidth, 1 + PDY - 3 * RO - PY0);
      graphics.FillRectangle(BBorder, RB);
      // Rectangle - Right
      RB = new Rectangle(PDX - FBorderWidth, PY0 + 3 * RO / 2, FBorderWidth, 1 + PDY - 3 * RO - PY0);
      graphics.FillRectangle(BBorder, RB);
      //
      // Draw Round Corners
      //
      GraphicsPath GP = new GraphicsPath();
      GP.AddArc(PX0 + FBorderWidth, PY0 + FBorderWidth, RI, RI, 180, 90);
      GP.AddArc(PDX - 3 * RO, PY0 + FBorderWidth, RI, RI, 270, 90);
      GP.AddArc(PDX - 3 * RO, PDY - 3 * RO, RI, RI, 0, 90);
      GP.AddArc(PX0 + FBorderWidth, PDY - 3 * RO, RI, RI, 90, 90);
      GP.CloseFigure();
      graphics.FillPath(BFill, GP);
      //
      // RoundCorner - Left Top
      GP = new GraphicsPath();
      GP.AddArc(PX0, PY0, 3 * RO, 3 * RO, 270, -90);
      GP.AddArc(PX0 + FBorderWidth, PY0 + FBorderWidth, RI, RI, 180, 90);
      GP.CloseFigure();
      graphics.FillPath(BBorder, GP);
      // RoundCorner - Right Top
      GP = new GraphicsPath();
      GP.AddArc(PDX - 3 * RO, PY0, 3 * RO, 3 * RO, 270, 90);
      GP.AddArc(PDX - 3 * RO, PY0 + FBorderWidth, RI, RI, 0, -90);
      GP.CloseFigure();
      graphics.FillPath(BBorder, GP);
      // RoundCorner - Right Bottom
      GP = new GraphicsPath();
      GP.AddArc(PX0, PDY - 3 * RO, 3 * RO, 3 * RO, 90, 90);
      GP.AddArc(PX0 + FBorderWidth, PDY - 3 * RO, RI, RI, 180, -90);
      GP.CloseFigure();
      graphics.FillPath(BBorder, GP);
      // RoundCorner - Left Bottom
      GP = new GraphicsPath();
      GP.AddArc(PDX - 3 * RO, PDY - 3 * RO, 3 * RO, 3 * RO, 0, 90);
      GP.AddArc(PDX - 3 * RO, PDY - 3 * RO, RI, RI, 90, -90);
      GP.CloseFigure();
      graphics.FillPath(BBorder, GP);
      //
      // Text
      Rectangle RT = new Rectangle(PX0, PY0, PDX - PX0, PDY - PY0);
      // debug graphics.DrawRectangle(new Pen(Color.Red), RT); 
      graphics.DrawString(text, font, BText, RT, SF);
      //
      BBorder.Dispose();
      BFill.Dispose();
      BText.Dispose();
    }
  }
}
