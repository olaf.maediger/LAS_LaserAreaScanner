﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCKeyFrame
{
  public partial class CUCKeyExternal : CUCKeyBase
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------------
    // Segment - Constructor
    //---------------------------------------------------------------------------------
    //
    public CUCKeyExternal()
    {
      InitializeComponent();
      //
      Title = "keyexternal";
      Font = new Font("Arial", 12);
      TextColor = Color.Black;
      TextColorFocus = Color.Black;
      CornerRadius = 8;
      BorderWidth = 1;
      BorderColor = Color.Black;
      BorderColorFocus = Color.Black;
      FillColor = Color.White;
      FillColorFocus = Color.White;
      PressColor = Color.Red;
      //
      Width = 150;
      Height = 50;
    }
    //
    //---------------------------------------------------------------------------------
    // Segment - Property
    //---------------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------------
    // Segment - Event
    //---------------------------------------------------------------------------------
    //


  }
}
