using System;
//
namespace IT01FTAProtocol
{
  public class CKeyTable
  {
    private const String INIT_VALUE = "?";

    private String[] FValues;

    public CKeyTable()
    {
      FValues = new String[128];
      for (Int32 BI = 0; BI < Size; BI++)
      {
        FValues[BI] = ((Char)(BI)).ToString();
      }
      FValues[0x00] = "NUL";    // <- Strg @
      FValues[0x01] = "SOH";    // <- Strg A
      FValues[0x02] = "STX";    // <- Strg B
      FValues[0x03] = "ETX";    // <- Strg C
      FValues[0x04] = "EOT";    // <- Strg D
      FValues[0x05] = "ENQ";    // <- Strg E
      FValues[0x06] = "FUNC";   // <- "ACK" <- Strg F
      FValues[0x07] = "BEL";    // <- Strg G
      FValues[0x08] = "BS";     // <- Strg H
      FValues[0x09] = "TAB";    // <- Strg I
      FValues[0x0A] = "LF";     // <- Strg J
      FValues[0x0B] = "VT";     // <- Strg K
      FValues[0x0C] = "FF";     // <- Strg L
      FValues[0x0D] = "CR";     // <- Strg M
      FValues[0x0E] = "ON";     // <- "SO" <- Strg N
      FValues[0x0F] = "OFF";    // <- "SI" <- Strg O
      //
      FValues[0x10] = "DLE";    // <- Strg P
      FValues[0x11] = "DC1";    // <- Strg Q
      FValues[0x12] = "DC2";    // <- Strg R
      FValues[0x13] = "STOP";   // <- "DC3" <- Strg S
      FValues[0x14] = "DC4";    // <- Strg T
      FValues[0x15] = "NAK";    // <- Strg U
      FValues[0x16] = "VOL";    // <- "SYN" <- Strg V
      FValues[0x17] = "ETB";    // <- Strg W
      FValues[0x18] = "CAN";    // <- Strg X
      FValues[0x19] = "EM";     // <- Strg Y
      FValues[0x1A] = "SUB";    // <- Strg Z
      FValues[0x1B] = "ESC";    // <- Strg ?
      FValues[0x1C] = "FS";     // <- Strg ?
      FValues[0x1D] = "GS";     // <- Strg ?
      FValues[0x1E] = "RS";     // <- Strg ?
      FValues[0x1F] = "START";  // <- "US" <- Strg ?
      //
      FValues[0x7C] = "STOP";   // <- '|'
      FValues[0x7D] = "FUNC";   // <- '}'
      FValues[0x7E] = "VOL";    // <- '~'
      FValues[0x7F] = "DEL";
    }

    public Int32 Size
    {
      get { return FValues.Length; }
    }

    public String ToString(Byte index)
    {
      if ((0 <= index) && (index < Size))
      {
        return FValues[index];
      }
      return INIT_VALUE;
    }
  }
}
