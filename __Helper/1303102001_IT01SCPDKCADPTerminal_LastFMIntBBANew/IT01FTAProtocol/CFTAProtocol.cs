﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transform;
using IT01SerialPort;
using UCNotifier;
//
namespace IT01FTAProtocol
{
  //
  //-------------------------------------------------------------
  //  FlowtestAsciiProtocol
  //  Index Length  Format    Volume
  //    0     1     Byte      STX
  //    1     2     AsciiByte Source DeviceID
  //    2     2     AsciiByte Command
  //    3     2*P   AsciiByte Parameter 0..P-1
  //    4     2     AsciiByte Checksum
  //    5     1     Byte      ETX
  //    6     1     Byte      CR
  //    7     1     Byte      LF
  //-------------------------------------------------------------
  //
  public class CFTAProtocol
  { //
    //-----------------------------------------------------------
    //  Segment - Constant
    //-----------------------------------------------------------
    //
    public const Byte TOKEN_STX = 0x02; // detect this on receiving!
    public const Byte TOKEN_ETX = 0x03; // detect this on receiving!
    public const Byte TOKEN_CR = 0x0D;  // ignore this on receiving!
    public const Byte TOKEN_LF = 0x0A;  // ignore this on receiving!
    //
    public const Byte INIT_CHECKSUMPRESET = 0x00;
    //
    // FTAProtocol - Source DeviceIDs
    public const Byte DEVICEID_DEBUG = 0x01;
    public const Byte DEVICEID_SCP = 0x11;  // SystemControlProcessor
    public const Byte DEVICEID_ADC = 0x21;  // AnalogDigitalController
    public const Byte DEVICEID_DKC = 0x31;  // DisplayKeyboardController
    public const Byte DEVICEID_KEYBOARDFRONT = 0x41;
    public const Byte DEVICEID_KEYBOARDEXERNAL = 0x51;
    //
    // Command SCP - System
    public const Byte COMMAND_SETSYSTEMSTATE = 0xF1;
    // Command DKC - Display
    public const Byte COMMAND_CLEARSCREEN = 0x60;
    public const Byte COMMAND_SETCURSORHOME = 0x61;
    public const Byte COMMAND_SETCURSORPOSITION = 0x62;
    public const Byte COMMAND_WRITETEXT = 0x63;
    public const Byte COMMAND_WRITELINE = 0x64;
    // Command DKC - KeyboardFront
    public const Byte COMMAND_KEYPRESSED = 0x91;
    // Command DKC - KeyboardExternal
    // Command ADC - ADConverter
    // Command ADC - DAConverter
    //
    //-----------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CIT01SerialPort FSerialPort;
    private DOnDataReceived FOnDataReceived;
    private CKeyTable FKeyTable;
    // ??? private String FRxdLine;
    //
    //-----------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------
    //
    public CFTAProtocol()
    {
      FSerialPort = null;
      FOnDataReceived = null;
      FKeyTable = new CKeyTable();
      // ??? FRxdLine = "";
    }
    //
    //-----------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnDataReceived(DOnDataReceived value)
    {
      FOnDataReceived = value;
    }
    //
    //-----------------------------------------------------------
    //  Segment - Callback
    //-----------------------------------------------------------
    //
    private void UartOnDataReceived(Byte[] rxdbuffer, Int32 rxdbuffersize)
    {
      for (int CI = 0; CI < rxdbuffersize; CI++)
      {
        Char RxdCharacter = (char)rxdbuffer[CI];
        String RxdHexadecimal = Transform.CTransform.ByteToHexadecimalString(rxdbuffer[CI]);
        String KeyCommand = FKeyTable.ToString(rxdbuffer[CI]);
        // debug
        String Line = "CA[" + RxdCharacter + "]";
        // debug 
        Line += " CX[" + RxdHexadecimal + "]";
        // debug 
        Line += " KC[" + KeyCommand + "]";
        // debug 
        Console.WriteLine(Line);
        //
        /*?????????????????????????????if (FOnKeyPressed is DOnKeyPressed)
         {
           FOnKeyPressed(rxdbuffer[CI], RxdCharacter, KeyCommand);
         }*/
        //
        /*???????????????switch (RxdCharacter)
        {
          case (char)(0x0D):
            // !!!!!!!!!!!!!!!!!!!!!!!!!! ExecuteCommand(FRxdLine);
            FRxdLine = "";
            break;
          case (char)(0x0A):
            // do nothing
            break;
          default:
            FRxdLine += RxdCharacter;
            break;
        }*/
      }
    }
    //
    //-----------------------------------------------------------
    //  Segment - Management
    //-----------------------------------------------------------
    //
    public Boolean Open(CIT01SerialPort serialport)
    { // uart MUST be opened! (Main)
      if (null == FSerialPort)
      {
        if (serialport is CIT01SerialPort)
        {
          FSerialPort = serialport;
          // ??? FRxdLine = "";
          return FSerialPort.IsOpen();
        }
      }
      return false;
    }

    public Boolean Close()
    {
      if (FSerialPort is CIT01SerialPort)
      {
        FSerialPort.Close();
        FSerialPort = null;
        return true;
      }
      return false;
    }
    //
    //-----------------------------------------------------------
    //  Segment - Low Level
    //-----------------------------------------------------------
    //
    private Boolean WriteByte(Byte value)
    {
      try
      {
        return FSerialPort.WriteByte(value);
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean WriteByteAscii(Byte value)
    {
      try
      {
        String Text = Transform.CTransform.ByteToHexadecimalString(value);
        return FSerialPort.WriteText(Text);
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------
    //  Segment - High Level
    //-----------------------------------------------------------
    //
    public Boolean WriteCommand(Byte deviceid,
                                Byte command)
    {
      Byte Checksum = INIT_CHECKSUMPRESET;
      //
      WriteByte(TOKEN_STX);
      //
      Checksum ^= deviceid;
      WriteByteAscii(deviceid);
      //
      Checksum ^= command;
      WriteByteAscii(command);
      //
      WriteByteAscii(Checksum);
      //
      WriteByte(TOKEN_ETX);
      WriteByte(TOKEN_CR);
      WriteByte(TOKEN_LF);
      //
      return true;
    }

    public Boolean WriteCommand(Byte deviceid,
                                Byte command,
                                Byte[] parameters)
    {
      Byte Checksum = INIT_CHECKSUMPRESET;
      //
      WriteByte(TOKEN_STX);
      //
      Checksum ^= deviceid;
      WriteByteAscii(deviceid);
      //
      Checksum ^= command;
      WriteByteAscii(command);
      //
      if (parameters is Byte[])
      {
        for (Byte PI = 0; PI < parameters.Length; PI++)
        {
          Checksum ^= parameters[PI];
          WriteByteAscii(parameters[PI]);
        }
      }
      //
      WriteByteAscii(Checksum);
      //
      WriteByte(TOKEN_ETX);
      WriteByte(TOKEN_CR);
      WriteByte(TOKEN_LF);
      //
      return true;
    }

  }
}
