﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
namespace IT01Command
{
  public class CXmlReader : CXmlBase
  {
    //
    //---------------------------------------------------
    //  Section - Constructor
    //---------------------------------------------------
    //
    public CXmlReader(String filename)
    {
      FFileName = filename;
    }
    //
    //---------------------------------------------------
    //  Section - Property
    //---------------------------------------------------
    //
    //
    //---------------------------------------------------
    //  Section - Management
    //---------------------------------------------------
    //
    public Boolean Open(String section)
    {
      try
      {
        FXmlDocument = new XmlDocument();
        FXmlDocument.Load(FFileName);
        FNodeBase = FXmlDocument.SelectSingleNode(section);
        FNodeParent = FNodeBase;
        String Line = String.Format("XmlReader: Open file \"{0}\"", FFileName);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        Line = String.Format("<{0}>", section);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        // !!!!!!!!!!!!!!!!! FNotifier.I.IncrementLeadCount();
        return (FNodeBase is XmlNode);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Close()
    {
      if (FNodeBase is XmlNode)
      {
        //! !!!!!!!!!!!FNotifier.DecrementLeadCount();
        String Line = String.Format("<{0}>", FNodeBase.Name);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        Line = String.Format("XmlReader: Close file \"{0}\"", FFileName);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        FNodeBase = null;
        return !(FNodeBase is XmlNode);
      }
      return false;
    }


    public Boolean OpenSection(String section)
    {
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(section);
        if (NodeChild is XmlNode)
        {
          FNodeParent = NodeChild;
          String Line = String.Format("<{0}>", section);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          // !!!!!!!!!!! FNotifier.IncrementLeadCount();
          return (FNodeParent is XmlNode);
        }
      }
      return false;
    }

    public Boolean CloseSection()
    {
      if (FNodeParent is XmlNode)
      {
        // !!!!!!!!!!!!!!! FNotifier.DecrementLeadCount();
        String Line = String.Format("</{0}>", FNodeParent.Name);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        FNodeParent = FNodeParent.ParentNode;
      }
      return (FNodeParent is XmlNode);
    }
    //
    //---------------------------------------
    //  Segment - Int32
    //---------------------------------------
    //      
    public Boolean ReadInt32(String name,
                             out Int32 value,
                             Int32 preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          return Int32.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Boolean
    //---------------------------------------
    //      
    public Boolean ReadBoolean(String name,
                               out Boolean value,
                               Boolean preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          return Boolean.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - String
    //---------------------------------------
    //      
    public Boolean ReadString(String name,
                              out String value,
                              String preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          value = NodeChild.InnerText;
          return true;
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Float
    //---------------------------------------
    //      
    public Boolean ReadFloat(String name,
                             out float value,
                             float preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          return float.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Double
    //---------------------------------------
    //      
    public Boolean ReadDouble(String name,
                             out Double value,
                             Double preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          return Double.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Enumeration
    //---------------------------------------
    //      
    public Boolean ReadEnumeration(String name,
                             out String value,
                             String preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          value = NodeChild.InnerText;
          return (0 < value.Length);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Guid
    //---------------------------------------
    //      
    public Boolean ReadGuid(String name,
                            out Guid value,
                            Guid preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          return Guid.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - DateTime
    //---------------------------------------
    //      
    public Boolean ReadDateTime(String name,
                                out DateTime value,
                                DateTime preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          return DateTime.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Byte
    //---------------------------------------
    //      
    public Boolean ReadByte(String name,
                            out Byte value,
                            Byte preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          return Byte.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - UInt16
    //---------------------------------------
    //      
    public Boolean ReadUInt16(String name,
                              out UInt16 value,
                              UInt16 preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          return UInt16.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - UInt32
    //---------------------------------------
    //      
    public Boolean ReadUInt32(String name,
                              out UInt32 value,
                              UInt32 preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
          return UInt32.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }




    public Boolean ReadCommandVectorList(ref CCommandVectorList commandvectorlist)
    {
      try
      {
        commandvectorlist = new CCommandVectorList();
        foreach (XmlNode NodeChild in FNodeBase.ChildNodes)
        {
          switch (NodeChild.Name)
          {
            case "CommandVector":
              CCommandVector CommandVector = new CCommandVector();
              CommandVector.SetNotifier(FNotifier);
              CommandVector.ReadFromXml(NodeChild);
              commandvectorlist.Add(CommandVector);
              break;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
