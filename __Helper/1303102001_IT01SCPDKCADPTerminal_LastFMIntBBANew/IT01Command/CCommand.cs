﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace IT01Command
{
  public abstract class CCommand
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    private TimeSpan FDelayBegin;
    private TimeSpan FDelayBusy;
    private TimeSpan FDelayEnd;

    public TimeSpan DelayBegin
    {
      get { return FDelayBegin; }
      set { FDelayBegin = value; }
    }
    public TimeSpan DelayBusy
    {
      get { return FDelayBusy; }
      set { FDelayBusy = value; }
    }
    public TimeSpan DelayEnd
    {
      get { return FDelayEnd; }
      set { FDelayEnd = value; }
    }

    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

    public abstract Boolean Execute();

    public abstract Boolean SaveToXml(CXmlWriter xmlwriter);

  }
}
