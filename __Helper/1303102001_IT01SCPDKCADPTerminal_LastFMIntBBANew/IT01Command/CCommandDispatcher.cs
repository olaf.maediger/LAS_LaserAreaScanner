﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading;
using IT01System;
using Task;
using UCNotifier;
//
namespace IT01Command
{

  public class CCommandDispatcher
  {
    //
    //---------------------------------------------------
    //  Section - Constant
    //---------------------------------------------------
    //
    public const string LIBRARY_HEADER = "IT01Command";
    //
    //---------------------------------------------------
    //  Section - Field
    //---------------------------------------------------
    //
    private CNotifier FNotifier;
    private CCommandMatrix FCommandMatrix;
    private CTask FTask;
    private ESystemState FSystemState;
    //
    //---------------------------------------------------
    //  Section - Constructor
    //---------------------------------------------------
    //
    public CCommandDispatcher()
    {
      FCommandMatrix = new CCommandMatrix();
      FSystemState = ESystemState.Main;
    }
    //
    //---------------------------------------------------
    //  Section - Property
    //---------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      FCommandMatrix.SetNotifier(FNotifier);
    }

    public void SetOnCommandSelectState(DOnCommandSelectState value)
    {
      FCommandMatrix.SetOnCommandSelectState(value);
    }

    public void SetOnCommandPressKey(DOnCommandPressKey value)
    {
      FCommandMatrix.SetOnCommandPressKey(value);
    }


    public Boolean AddCommand(ESystemState menustate, CCommand command)
    {
      CCommandVector CommandVector = FCommandMatrix.FindCommandVector(menustate);
      if (CommandVector is CCommandVector)
      {
        CommandVector.Add(command);
        return true;
      }
      return false;
    }

    public Boolean AddCommandVector(CCommandVector commandvector)
    {
      if (commandvector is CCommandVector)
      {
        return FCommandMatrix.AddCommandVector(commandvector);
      }
      return false;
    }


    public void SetSystemState(ESystemState systemstate)
    {
      String Line = String.Format("*** CommandDispatcher: SetSystemState[{0}] -> {1}",
                                  FSystemState.ToString(), systemstate.ToString());
      FNotifier.Write(Line);
      FSystemState = systemstate;
    }

    public Boolean ResetCommandExecution()
    {
      FSystemState = ESystemState.Main;
      return FCommandMatrix.ResetCommandExecution();
    }


    private Boolean OnTaskStart(RTaskData data)
    { // debug       FNotifier.Write("CommandDispatcher: OnTaskStart");
      FSystemState = ESystemState.Main;
      return true;
    }
    private Boolean OnTaskExecute(RTaskData data)
    { // debug       FNotifier.Write("CommandDispatcher: OnTaskExecute[" + stepcount + "]");
      FCommandMatrix.Execute(FSystemState);
      Thread.Sleep(1000);
      return (data.Counter < 10);
    }
    private Boolean OnTaskEnd(RTaskData data)
    { // debug       FNotifier.Write("CommandDispatcher: OnTaskEnd");
      FTask = null;
      return true;
    }

    private Boolean OnTaskAbort(RTaskData data)
    { // debug       FNotifier.Write("CommandDispatcher: OnTaskEnd");
      FTask = null;
      return true;
    }

    public Boolean StartCommandExecution()
    {
      if (!(FTask is CTask))
      {
        FTask = new CTask(OnTaskStart, OnTaskExecute, OnTaskEnd, OnTaskAbort);
        return FTask.Start();
      }
      return false;
    }

    public Boolean StopCommandExecution()
    {
      if (FTask is CTask)
      {
        FTask.Abort();
        FTask = null;
        return true;
      }
      return false;
    }

    public Boolean SaveToXml(string filename)
    {
      try
      {
        CXmlWriter XmlWriter = new CXmlWriter(filename);
        XmlWriter.SetNotifier(FNotifier);
        XmlWriter.Open("CommandMatrix");
        FCommandMatrix.SaveToXml(XmlWriter);
        XmlWriter.Close();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean ReadFromXml(string filename)
    {
      try
      {
        return FCommandMatrix.ReadFromXml(filename);
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
