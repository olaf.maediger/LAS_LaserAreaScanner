﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UCNotifier;
//
namespace IT01Command
{
  public class CXmlBase
  {
    //
    //---------------------------------------------
    //  Section - Constant
    //---------------------------------------------
    //

    //
    //---------------------------------------------
    //  Section - Field
    //---------------------------------------------
    //
    protected String FFileName;
    protected CNotifier FNotifier;
    protected XmlDocument FXmlDocument;
    protected XmlNode FNodeBase;
    protected XmlNode FNodeParent;
    //
    //---------------------------------------------
    //  Section - Property
    //---------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }
  }
}
