﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Reflection;
using UCNotifier;
//
namespace IT01Command
{
  public delegate void DOnCommandPressKey(Char key);

  public class CCommandPressKey : CCommand
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //
    private Char FKeyCharacter;
    private DOnCommandPressKey FOnCommandPressKey;

    public CCommandPressKey()
    {
      FKeyCharacter = (Char)0x00;
    }

    public CCommandPressKey(Char keycharacter)
    {
      FKeyCharacter = keycharacter;
    }

    public void SetKeyCharacter(Char value)
    {
      FKeyCharacter = value;
    }

    public void SetOnCommandPressKey(DOnCommandPressKey value)
    {
      FOnCommandPressKey = value;
    }

    public override Boolean Execute()
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write("CommandPressKey - Start");
      }
      Boolean Result = false;
      Thread.Sleep(DelayBegin);
      //
      if (FOnCommandPressKey is DOnCommandPressKey)
      {
        if (FNotifier is CNotifier)
        {
          FNotifier.Write("CommandPressKey - Busy");
        }
        FOnCommandPressKey(FKeyCharacter);
        Result = true;
      }
      //
      Thread.Sleep(DelayEnd);
      if (FNotifier is CNotifier)
      {
        FNotifier.Write("CommandPressKey - End");
      }
      return Result;
    }

    public override Boolean SaveToXml(CXmlWriter xmlwriter)
    {
      try
      {
        xmlwriter.CreateSection("Command");
        xmlwriter.WriteString("Name", this.ToString());
        xmlwriter.WriteByte("KeyCode", (Byte)FKeyCharacter);
        xmlwriter.CloseSection();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    /*
    public override Boolean ReadFromXml(CXmlReader xmlreader)
    {
      try
      {
        xmlreader.CreateSection("Command");
        xmlreader.ReadString("Name", this.ToString());
        xmlreader.WriteByte("KeyCode", (Byte)FKey);
        xmlreader.CloseSection();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
*/
  }
}
