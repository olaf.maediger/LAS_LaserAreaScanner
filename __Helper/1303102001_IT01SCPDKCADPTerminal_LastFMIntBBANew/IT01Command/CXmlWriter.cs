﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UCNotifier;
//
namespace IT01Command
{
  public class CXmlWriter : CXmlBase
  {
    //
    //---------------------------------------------------
    //  Section - Constructor
    //---------------------------------------------------
    //
    public CXmlWriter(String filename)
    {
      FFileName = filename;
      FNotifier = new CNotifier();
    }
    //
    //---------------------------------------------------
    //  Section - Property
    //---------------------------------------------------
    //

    //
    //---------------------------------------------------
    //  Section - Management
    //---------------------------------------------------
    //
    public Boolean Open(String section)
    {
      try
      {
        FXmlDocument = new XmlDocument();
        FNodeBase = FXmlDocument.CreateElement(section);
        FNodeParent = FNodeBase;
        FXmlDocument.AppendChild(FNodeBase);
        String Line = String.Format("XmlWriter: Open file \"{0}\"", FFileName);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        Line = String.Format("<{0}>", section);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        // !!!!!!!!!!! FNotifier.IncrementLeadCount();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public Boolean Close()
    {
      try
      {
        // !!!!!!!!!!! FNotifier.DecrementLeadCount();
        String Line = String.Format("<{0}>", FNodeBase.Name);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        Line = String.Format("XmlWriter: Close file \"{0}\"", FFileName);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        FXmlDocument.Save(FFileName);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean CreateSection(String section)
    {
      XmlNode Node = FXmlDocument.CreateElement(section);
      if (Node is XmlNode)
      {
        FNodeParent.AppendChild(Node);
        FNodeParent = Node;
        String Line = String.Format("<{0}>", section);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        // !!!!!!!!!!! FNotifier.IncrementLeadCount();
        return true;
      }
      return false;
    }
    public Boolean CloseSection()
    {
      // !!!!!!!!!!! FNotifier.DecrementLeadCount();
      String Line = String.Format("</{0}>", FNodeParent.Name);
      FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
      FNodeParent = FNodeParent.ParentNode;
      return (FNodeParent is XmlNode);
    }
    //
    //---------------------------------------
    //  Segment - CommonBase
    //---------------------------------------
    //      
    private Boolean WriteCommon(String name,
                                object value)
    {
      try
      {
        XmlNode Node = FXmlDocument.CreateElement(name);
        Node.InnerText = value.ToString();
        FNodeParent.AppendChild(Node);
        String Line = String.Format("<{0}>{1}</{0}>", name, Node.InnerText);
        FNotifier.Write(CCommandDispatcher.LIBRARY_HEADER, Line);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------
    //  Segment - Int32
    //---------------------------------------
    //      
    public Boolean WriteInt32(String name,
                              Int32 value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Boolean
    //---------------------------------------
    //      
    public Boolean WriteBoolean(String name,
                                Boolean value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Character
    //---------------------------------------
    //      
    public Boolean WriteCharacter(String name,
                                  Char value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - String
    //---------------------------------------
    //      
    public Boolean WriteString(String name,
                              String value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Float
    //---------------------------------------
    //      
    public Boolean WriteFloat(String name,
                              float value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Double
    //---------------------------------------
    //      
    public Boolean WriteDouble(String name,
                             Double value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Enumeration
    //---------------------------------------
    //      
    public Boolean WriteEnumeration(String name,
                                    String value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Guid
    //---------------------------------------
    //      
    public Boolean WriteGuid(String name,
                             Guid value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - DateTime
    //---------------------------------------
    //      
    public Boolean WriteDateTime(String name,
                                 DateTime value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Byte
    //---------------------------------------
    //      
    public Boolean WriteByte(String name,
                             Byte value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - UInt16
    //---------------------------------------
    //      
    public Boolean WriteUInt16(String name,
                               UInt16 value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - UInt32
    //---------------------------------------
    //      
    public Boolean WriteUInt32(String name,
                               UInt32 value)
    {
      return WriteCommon(name, value);
    }
  }
}
