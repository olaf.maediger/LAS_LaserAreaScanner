﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using IT01System;
using UCNotifier;
//
namespace IT01Command
{
  public class CCommandMatrix 
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    private CCommandVectorList FCommandVectorList;
    //private DOnCommandSelectState FOnCommandSelectState;
    //private DOnCommandPressKey FOnCommandPressKey;

    public CCommandMatrix()
    {
      FCommandVectorList = new CCommandVectorList();
    }


    public void SetOnCommandSelectState(DOnCommandSelectState value)
    {
      //FOnCommandSelectState = value;
      FCommandVectorList.SetOnCommandSelectState(value);
    }

    public void SetOnCommandPressKey(DOnCommandPressKey value)
    {
      //FOnCommandPressKey = value;
      FCommandVectorList.SetOnCommandPressKey(value);
    }


    public CCommandVector FindCommandVector(ESystemState systemstate)
    {
      foreach (CCommandVector CommandVector in FCommandVectorList)
      {
        if (systemstate == CommandVector.SystemState)
        {
          return CommandVector;
        }
      }
      return null;
    }

    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      FCommandVectorList.SetNotifier(notifier);
    }


    public Boolean AddCommandVector(CCommandVector commandvector)
    {
      if (commandvector is CCommandVector)
      {
        commandvector.SetNotifier(FNotifier);
        FCommandVectorList.Add(commandvector);
        return true;
      }
      return false;
    }

    public Boolean Execute(ESystemState systemstate)
    {
      Boolean Result = true;
      foreach (CCommandVector CommandVector in FCommandVectorList)
      {
        if (systemstate == CommandVector.SystemState)
        {
          Result &= CommandVector.Execute();
        }
      }
      return Result;
    }

    public Boolean ResetCommandExecution()
    {
      foreach (CCommandVector CommandVector in FCommandVectorList)
      {
        CommandVector.ResetCommandExecution();
      }
      return true;
    }

    public Boolean SaveToXml(CXmlWriter xmlwriter)
    {
      try
      {
        foreach (CCommandVector CommandVector in FCommandVectorList)
        {
          CommandVector.SaveToXml(xmlwriter);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean ReadFromXml(String filename)
    {
      try
      {
        CXmlReader XmlReader = new CXmlReader(filename);
        XmlReader.SetNotifier(FNotifier);
        XmlReader.Open("CommandMatrix");
        XmlReader.ReadCommandVectorList(ref FCommandVectorList);
        FCommandVectorList.SetNotifier(FNotifier);
        XmlReader.Close();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
