﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using IT01System;
using UCNotifier;
//
namespace IT01Command
{
  public delegate void DOnCommandSelectState(ESystemState systemstate);

  public class CCommandSelectState : CCommand
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //
    private ESystemState FSystemState;
    private DOnCommandSelectState FOnCommandSelectState;

    public CCommandSelectState()
    {
      FSystemState = ESystemState.Main;
    }

    public CCommandSelectState(ESystemState systemstate)
    {
      FSystemState = systemstate;
    }

    public void SetSystemState(ESystemState value)
    {
      FSystemState = value;
    }

    public void SetOnCommandSelectState(DOnCommandSelectState value)
    {
      FOnCommandSelectState = value;
    }

    public override Boolean Execute()
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write("CommandSelectState - Start");
      }
      Boolean Result = false;
      Thread.Sleep(DelayBegin);
      //
      if (FOnCommandSelectState is DOnCommandSelectState)
      {
        if (FNotifier is CNotifier)
        {
          FNotifier.Write("CommandSelectState - Busy");
        }
        FOnCommandSelectState(FSystemState);
        Result = true;
      }
      //
      Thread.Sleep(DelayEnd);
      if (FNotifier is CNotifier)
      {
        FNotifier.Write("CommandSelectState - End");
      }
      return Result;
    }

    public override Boolean SaveToXml(CXmlWriter xmlwriter)
    {
      try
      {
        xmlwriter.CreateSection("Command");
        xmlwriter.WriteString("Name", this.ToString());
        xmlwriter.WriteEnumeration("SystemState", FSystemState.ToString());
        xmlwriter.CloseSection();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
