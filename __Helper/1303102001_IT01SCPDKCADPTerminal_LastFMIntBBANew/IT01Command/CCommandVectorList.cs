﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace IT01Command
{
  public class CCommandVectorList : List<CCommandVector>
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    //private DOnCommandSelectState FOnCommandSelectState;
    //private DOnCommandPressKey FOnCommandPressKey;

    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      foreach (CCommandVector CommandVector in this)
      {
        CommandVector.SetNotifier(FNotifier);
      }
    }

    public void SetOnCommandSelectState(DOnCommandSelectState value)
    {
      //FOnCommandSelectState = value;
      foreach (CCommandVector CommandVector in this)
      {
        CommandVector.SetOnCommandSelectState(value);
      }
    }

    public void SetOnCommandPressKey(DOnCommandPressKey value)
    {
      //FOnCommandPressKey = value;
      foreach (CCommandVector CommandVector in this)
      {
        CommandVector.SetOnCommandPressKey(value);
      }
    }


    public new Boolean Add(CCommandVector commandvector)
    {
      if (commandvector is CCommandVector)
      {
        commandvector.SetNotifier(FNotifier);
        base.Add(commandvector);
        return true;
      }
      return false;
    }

  }
}
