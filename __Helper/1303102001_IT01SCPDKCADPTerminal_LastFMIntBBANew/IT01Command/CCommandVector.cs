﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using IT01System;
using System.Reflection;
using UCNotifier;
//
namespace IT01Command
{
  public class CCommandVector : List<CCommand>
  {
    //
    //---------------------------------------------------------------------------------
    // Segment - Field
    //---------------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    private ESystemState FSystemState;
    private Int32 FCommandIndex;
    //private DOnCommandSelectState FOnCommandSelectState;
    //private DOnCommandPressKey FOnCommandPressKey;

    public CCommandVector()
    {
      FSystemState = ESystemState.Main;
      //FOnCommandSelectState = null;
      //FOnCommandPressKey = null;
      ResetCommandIndex();
    }

    public CCommandVector(ESystemState systemstate)
    {
      FSystemState = systemstate;
      //FOnCommandSelectState = null;
      //FOnCommandPressKey = null;
      ResetCommandIndex();
    }


    public void SetOnCommandSelectState(DOnCommandSelectState value)
    {
      //FOnCommandSelectState = value;
      foreach (CCommand Command in this)
      {
        if (Command is CCommandSelectState)
        {
          ((CCommandSelectState)Command).SetOnCommandSelectState(value);
        }
      }
    }

    public void SetOnCommandPressKey(DOnCommandPressKey value)
    {
      //FOnCommandPressKey = value;
      foreach (CCommand Command in this)
      {
        if (Command is CCommandPressKey)
        {
          ((CCommandPressKey)Command).SetOnCommandPressKey(value);
        }
      }
    }

    public ESystemState SystemState
    {
      get { return FSystemState; }
      set { FSystemState = value; }
    }

    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      foreach (CCommand Command in this)
      {
        Command.SetNotifier(FNotifier);
      }
    }


    //
    //------------------------------------------------------------------------
    //  Section - Callback 
    //------------------------------------------------------------------------
    //
   /* private void OnCommandPressKey(Char key)
    {
      if (FOnCommandPressKey is DOnCommandPressKey)
      {
        FOnCommandPressKey(key);
      }
      // FUCNotifier.Write("Main: OnCommandPressKey: >" + key.ToString() + "<");
      // FUCIT01KeyboardFront.PressKey(key);
    }

    private void OnCommandSelectState(ESystemState systemstate)
    {
      if (FOnCommandSelectState is DOnCommandSelectState)
      {
        FOnCommandSelectState(systemstate);
      }
      // FUCNotifier.Write("Main: OnCommandSelectState: " + systemstate.ToString());
      // FCommandDispatcher.SetSystemState(systemstate);
    }*/



    private void ResetCommandIndex()
    {
      FCommandIndex = 0;
    }


    public Boolean AddCommand(CCommand command)
    {
      if (command is CCommand)
      {
        command.SetNotifier(FNotifier);
        base.Add(command);
        return true;
      }
      return false;
    }

    public Boolean Execute()
    {
      if ((0 <= FCommandIndex) && (FCommandIndex < Count))
      {
        CCommand Command = this[FCommandIndex];
        if (Command is CCommand)
        {
          if (FNotifier is CNotifier)
          {
            FNotifier.Write("CommandVector.Execute[" + FCommandIndex.ToString() + "]");
          }
          Command.Execute();
          FCommandIndex++;
        }
      }
      return false;
    }

    public Boolean ResetCommandExecution()
    {
      FCommandIndex = 0;
      return true;
    }

    public Boolean SaveToXml(CXmlWriter xmlwriter)
    {
      try
      {
        xmlwriter.CreateSection("CommandVector");
        xmlwriter.WriteEnumeration("SystemState", this.SystemState.ToString());
        foreach (CCommand Command in this)
        {
          Command.SaveToXml(xmlwriter);
        }
        xmlwriter.CloseSection();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean ReadCommand(XmlNode nodebase, out CCommand command)
    {
      command = null;
      try
      {
        foreach (XmlNode NodeChild in nodebase.ChildNodes)
        {
          switch (NodeChild.Name)
          {
            case "Name":
              command = (CCommand)Assembly.GetExecutingAssembly().CreateInstance(NodeChild.InnerText);
              if (command is CCommandSelectState)
              {
                // ((CCommandSelectState)command).SetOnCommandSelectState(OnCommandSelectState);
              } else
                if (command is CCommandPressKey)
                {
                  // ((CCommandPressKey)command).SetOnCommandPressKey(OnCommandPressKey);
                }
              break;
            case "SystemState":
              if (command is CCommandSelectState)
              {
                ESystemState SS = CIT01System.TextToSystemState(NodeChild.InnerText);
                ((CCommandSelectState)command).SetSystemState(SS);
              }
              break;
            case "KeyCode":
              if (command is CCommandPressKey)
              {
                Char KC = (Char)Byte.Parse(NodeChild.InnerText);
                ((CCommandPressKey)command).SetKeyCharacter(KC);
              }
              break;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean ReadFromXml(XmlNode nodebase)
    {
      try
      {
        foreach (XmlNode NodeChild in nodebase.ChildNodes)
        {
          switch (NodeChild.Name)
          {
            case "SystemState":
              this.SystemState = CIT01System.TextToSystemState(NodeChild.InnerText);
              break;
            case "Command":
              CCommand Command;
              ReadCommand(NodeChild, out Command);
              if (Command is CCommand)
              {
                Command.SetNotifier(FNotifier);
                Add(Command);
              }
              break;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }     
    }



  }
}
