﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Initdata;
using UCNotifier;
//
namespace UCTextEditor
{
	public delegate void DOnDataChanged();

  public struct RTextEditorData
  {
    public String FileName;
    public Boolean IsChanged;
    public Boolean IsReadOnly;
    public Int32 TabulatorWidth;
    public DOnDataChanged OnDataChanged;
  };
	
	public partial class CUCTextEditor : UserControl
	{ //
    //-------------------------------
    //	Segment - Constant
    //-------------------------------
    //
    public const String SECTION_LIBRARY = "UCTextEditor";
    public const String HEADER_LIBRARY = "UCTextEditor";
    //
    private const String NAME_FILENAME = "FileName";
    private const String NAME_ISCHANGED = "IsChanged";
    private const String NAME_ISREADONLY = "IsReadOnly";
    private const String NAME_TABULATORWIDTH = "TabulatorWidth";
    private const String NAME_LOADLISTCOUNT = "LoadListCount";
    private const String NAME_FILENAMELOAD = "FileNameLoad";
    private const String NAME_SAVELISTCOUNT = "SaveListCount";
    private const String NAME_FILENAMESAVE = "FileNameSave";
    //
    private const String INIT_FILENAME = "";
    private const Boolean INIT_ISCHANGED = false;
    private const Boolean INIT_ISREADONLY = false;
    private const Int32 INIT_TABULATORWIDTH = 4;
    private const Int32 INIT_LOADLISTCOUNT = 0;
    private const Int32 INIT_SAVELISTCOUNT = 0;
    //
    private const String HEADER_FILE = "File: ";
    private const String HEADER_PATH = "Path: ";
    private const String HEADER_TEXTMEASURE = "ABCDEFabcdef";
    private const String FORMAT_TABULATOR = "Tabulator [{0}]";
    private const String FORMAT_CURSORROW = "Row: {0}";
    private const String FORMAT_CURSORCOL = "Col: {0}";
    //
		//-------------------------------
		//	Segment - Constructor
		//-------------------------------
		//
		public CUCTextEditor()
		{
			InitializeComponent();
      Initialize();
		}
    //
    //------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------
    //
    private RTextEditorData FData;
    private CNotifier FNotifier;      // only Reference!
    //
		//-------------------------------
		//  Segment - Property
		//-------------------------------
    //
    public Boolean SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      // FChild.SetNotifier(notifier);
      return true;
    }
    //
		//	Property - FileName
		private String GetFileName()
		{
			return FData.FileName;
		}
		private void SetFileName(String value)
		{
			if (value != FileName)
			{
        FData.FileName = value;
        stsFileName.Text = HEADER_FILE + Path.GetFileName(value);
        stsPath.Text = HEADER_PATH;
        if (1 < value.Length)
        {
          stsPath.Text += Path.GetDirectoryName(value);
        }
        if (1 < value.Length)
        {
          if (!cbxLoadFromFile.Items.Contains(value))
          {
            cbxLoadFromFile.Items.Add(value);
            cbxLoadFromFile.Text = value;
          }
          if (!cbxSaveToFile.Items.Contains(value))
          {
            cbxSaveToFile.Items.Add(value);
            cbxSaveToFile.Text = value;
          }
        }
      }
		}
		public String FileName
		{
			get { return GetFileName(); }
			set { SetFileName(value); }
		}
    //
    //  Property - IsChanged
    private Boolean GetIsChanged()
    {      
      return FData.IsChanged;
    }
    private void SetIsChanged(Boolean value)
    {
      if (value != IsChanged)
      {
        FData.IsChanged = value;
        mitSave.Enabled = IsChanged;
      }
    }
    public Boolean IsChanged
    {
      get { return GetIsChanged(); }
    }
    //
    //  Property - IsReadOnly
    private Boolean GetIsReadOnly()
    {
      return FData.IsReadOnly;
    }
    private void SetIsReadOnly(Boolean value)
    {
      if (value != IsReadOnly)
      {
        FData.IsReadOnly = value;
        FUCEditor.ReadOnly = value;
        if (value)
        {
          FUCEditor.BackColor = Color.FromArgb(255, 230, 230, 230);
        } else
        {
          FUCEditor.BackColor = Color.White;
        }
        cbxReadOnly.Checked = value;
      }
    }
    public Boolean IsReadOnly
    {
      get { return GetIsReadOnly(); }
      set { SetIsReadOnly(value); }
    }
    //
    //  Property - TabulatorWidth
		private Int32 GetTabulatorWidth()
		{
			return FData.TabulatorWidth;
		}
		private void SetTabulatorWidth(Int32 value)
		{
			if (value != TabulatorWidth)
			{
        FData.TabulatorWidth = value;
				Int32 Value = Math.Max(scbTabulator.Minimum, value);
				Value = Math.Min(scbTabulator.Maximum, Value);
				scbTabulator.Value = Value;
				FUCEditor.AcceptsTab = true;

				SizeF S = new Size();
				Graphics G = Graphics.FromHwnd(FUCEditor.Handle);
				S = G.MeasureString(HEADER_TEXTMEASURE, FUCEditor.Font);
				Int32 CW = (Int32)(S.Width / 12.0f);

				Int32[] Tabulators = new Int32[10];
				for (Int32 TI = 0; TI < 10; TI++)
				{
					Tabulators[TI] = (1 + TI) * Value * CW;
				}
				FUCEditor.SelectAll();
				FUCEditor.SelectionTabs = Tabulators;
				FUCEditor.Select(0, 0);
				lblTabulator.Text = String.Format(FORMAT_TABULATOR, Value);
			}
		}
		private Int32 TabulatorWidth
		{
			get { return GetTabulatorWidth(); }
			set { SetTabulatorWidth(value); }
		}

    public String LoadFileName
    {
      get { return DialogLoadText.FileName; }
      set { DialogLoadText.FileName = value; }
    }
    public String LoadFileExtension
    {
      get { return DialogLoadText.DefaultExt; }
      set { DialogLoadText.DefaultExt = value; }
    }
    public String LoadFileFilter
    {
      get { return DialogLoadText.Filter; }
      set { DialogLoadText.Filter = value; }
    }

    public String SaveFileName
    {
      get { return DialogSaveText.FileName; }
      set { DialogSaveText.FileName = value; }
    }
    public String SaveFileExtension
    {
      get { return DialogSaveText.DefaultExt; }
      set { DialogSaveText.DefaultExt = value; }
    }
    public String SaveFileFilter
    {
      get { return DialogSaveText.Filter; }
      set { DialogSaveText.Filter = value; }
    }
    //
    //------------------------------------------------------
    //  Segment - Error-Handling
    //------------------------------------------------------
    //
    protected enum EErrorCode
    {
      None = 0,
      UnknownErrorcode
    };

    public static readonly String[] ERRORS = 
    { 
      "None",
  	  "Unknown errorcode"
    };
    //
    //----------------------------
    //  Segment - Get/SetData
    //----------------------------
    //
    public Boolean GetData(out RTextEditorData data)
    {
      data = FData;
      return true;
    }

    public Boolean SetData(RTextEditorData data)
    {
      FileName = data.FileName;
      SetIsChanged(data.IsChanged);
      IsReadOnly = data.IsReadOnly;
      TabulatorWidth = data.TabulatorWidth;
      return true;
    }
    //
    //----------------------------
    //  Segment - Callback
    //----------------------------
    //

    //
    //----------------------------
    //  Segment - Messaging
    //----------------------------
    //
    private String BuildHeader()
    {
      return String.Format("{0}", HEADER_LIBRARY);
    }

    protected Boolean _Error(EErrorCode code)
    {
      if (FNotifier is CNotifier)
      {
        Int32 Index = (Int32)code;
        if ((Index < 0) && (ERRORS.Length <= Index))
        {
          Index = ERRORS.Length - 1;
        }
        String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
        FNotifier.Write(BuildHeader(), Line);
      }
      return false;
    }

    protected void _Protocol(String line)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(BuildHeader(), line);
      }
    }
    //
    //----------------------------
    //  Segment - Init
    //----------------------------
    //
    private void Initialize()
    {
      FileName = INIT_FILENAME;
      SetIsChanged(INIT_ISCHANGED);
      IsReadOnly = INIT_ISREADONLY;
      TabulatorWidth = INIT_TABULATORWIDTH;
      FData.OnDataChanged = null;
      //
      pnlLoad.Visible = false;
      pnlSave.Visible = false;
      pnlParameter.Visible = false;
      //
      pnlLoad.Click += new EventHandler(Panel_Click);
      pnlSave.Click += new EventHandler(Panel_Click);
      pnlParameter.Click += new EventHandler(Panel_Click);
    }

    public Boolean LoadInitdata(CInitdataReader initdata)//, String sectionparent)
    {
      Int32 IValue;
      Boolean BValue;
      String SValue;
      initdata.ReadString(NAME_FILENAME, out SValue, INIT_FILENAME);
      FileName = SValue;
      initdata.ReadBoolean(NAME_ISREADONLY, out BValue, INIT_ISREADONLY);
      IsReadOnly = BValue;
      initdata.ReadBoolean(NAME_ISCHANGED, out BValue, INIT_ISCHANGED);
      SetIsChanged(BValue);
      initdata.ReadInt32(NAME_TABULATORWIDTH, out IValue, INIT_TABULATORWIDTH);
      TabulatorWidth = IValue;
      // LoadList
      initdata.ReadInt32(NAME_LOADLISTCOUNT, out IValue, INIT_LOADLISTCOUNT);
      for (Int32 LI = 0; LI < IValue; LI++)
      {
        initdata.ReadString(NAME_FILENAMELOAD + LI.ToString(), out SValue, INIT_FILENAME);
        if (File.Exists(SValue))
        {
          if (!cbxLoadFromFile.Items.Contains(SValue))
          {
            cbxLoadFromFile.Items.Add(SValue);
          }
        }
      }
      // SaveList
      initdata.ReadInt32(NAME_SAVELISTCOUNT, out IValue, INIT_SAVELISTCOUNT);
      for (Int32 LI = 0; LI < IValue; LI++)
      {
        initdata.ReadString(NAME_FILENAMESAVE + LI.ToString(), out SValue, INIT_FILENAME);
        if (!cbxSaveToFile.Items.Contains(SValue))
        {
          cbxSaveToFile.Items.Add(SValue);
        }
      }
      //
      return true;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)//, String sectionparent)
    {
      initdata.WriteString(NAME_FILENAME, FileName);
      initdata.WriteBoolean(NAME_ISCHANGED, IsChanged);
      initdata.WriteBoolean(NAME_ISREADONLY, IsReadOnly);
      initdata.WriteInt32(NAME_TABULATORWIDTH, TabulatorWidth);
      // LoadList
      initdata.WriteInt32(NAME_LOADLISTCOUNT, cbxLoadFromFile.Items.Count);
      for (Int32 LI = 0; LI < cbxLoadFromFile.Items.Count; LI++)
      {
        initdata.WriteString(NAME_FILENAMELOAD + LI.ToString(), cbxLoadFromFile.Items[LI].ToString());
      }
      // SaveList
      initdata.WriteInt32(NAME_SAVELISTCOUNT, cbxSaveToFile.Items.Count);
      for (Int32 LI = 0; LI < cbxSaveToFile.Items.Count; LI++)
      {
        initdata.WriteString(NAME_FILENAMESAVE + LI.ToString(), cbxSaveToFile.Items[LI].ToString());
      }
      //
      return true;
    }  
    //
		//-------------------------------
    //  Segment - Event - Control
		//-------------------------------
    //
    private void stsEditor_Resize(object sender, EventArgs e)
    {
      stsPath.Width = stsEditor.Width - stsRow.Width - stsCol.Width - stsFileName.Width;
    }

    private void CUCTextEditor_Resize(object sender, EventArgs e)
    {
      cbxLoadFromFile.Width = Width - cbxLoadFromFile.Left - btnClearLoadList.Width - 20;
      btnClearLoadList.Left = 10 + cbxLoadFromFile.Left + cbxLoadFromFile.Width;
      cbxSaveToFile.Width = Width - cbxSaveToFile.Left - btnClearSaveList.Width - 20;
      btnClearSaveList.Left = 10 + cbxSaveToFile.Left + cbxSaveToFile.Width;
    }

    private void cbxLoadFromFile_SelectedValueChanged(object sender, EventArgs e)
    {
      FileName = cbxLoadFromFile.Text;
      LoadFromFile();
    }

    private void cbxSaveToFile_SelectedValueChanged(object sender, EventArgs e)
    {
      FileName = cbxSaveToFile.Text;
    }

    private void Panel_Click(object sender, EventArgs e)
    {
      if (sender is Panel)
      {
        ((Panel)sender).Visible = false;
      }
    }

		private void CUCTextEditor_Load(object sender, EventArgs e)
		{
			FUCEditor.Dock = DockStyle.Fill;
		}

		private void scbTabulator_Scroll(object sender, ScrollEventArgs e)
		{
			TabulatorWidth = scbTabulator.Value;
		}

		private void FUCEditor_TextChanged(object sender, EventArgs e)
		{
			SetIsChanged(true);
		}

    private void FUCEditor_FCursorPositionChanged(object sender, EventArgs e)
    {
      stsRow.Text = String.Format(FORMAT_CURSORROW, FUCEditor.CursorRow);
      stsCol.Text = String.Format(FORMAT_CURSORCOL, FUCEditor.CursorCol);
    }
    //
    //-------------------------------
    //  Segment - Event - SubControl
    //-------------------------------
    //
    private void btnLoadFromFile_Click(object sender, EventArgs e)
    {
      LoadFromFileModal();
      pnlLoad.Visible = false;
    }

    private void btnClearLoadList_Click(object sender, EventArgs e)
    {
      cbxLoadFromFile.Items.Clear();
    }

    private void btnSaveToFile_Click(object sender, EventArgs e)
    {
      SaveToFileModal();
      pnlSave.Visible = false;
    }

    private void btnClearSaveList_Click(object sender, EventArgs e)
    {
      cbxSaveToFile.Items.Clear();
    }

    private void cbxReadOnly_CheckedChanged(object sender, EventArgs e)
    {
      IsReadOnly = cbxReadOnly.Checked;
    }

    private void btnSelectFont_Click(object sender, EventArgs e)
    {
      SelectFontModal();
    }
		//
		//-------------------------------
    //  Segment - Event - Menu
		//-------------------------------
		//
    private void mitLoad_Click(object sender, EventArgs e)
    {
      pnlSave.Visible = false;
      pnlParameter.Visible = false;
      pnlLoad.Visible = true;
    }

    private void mitSave_Click(object sender, EventArgs e)
    {
      pnlLoad.Visible = false;
      pnlParameter.Visible = false;
      pnlSave.Visible = true;
      SaveToFile();
      pnlSave.Visible = false;
    }

    private void mitSaveAs_Click(object sender, EventArgs e)
    {
      pnlLoad.Visible = false;
      pnlParameter.Visible = false;
      pnlSave.Visible = true;
    }

    private void mitParameter_Click(object sender, EventArgs e)
    {
      pnlLoad.Visible = false;
      pnlSave.Visible = false;
      pnlParameter.Visible = true;
    }
		//
		//-------------------------------
    //  Segment - Public Management
		//-------------------------------
		//
    public Boolean LoadFromFile(String fileentry)
    {
      try
      {
        FileName = fileentry;
        FUCEditor.LoadFile(FileName, RichTextBoxStreamType.PlainText);
        SetIsChanged(false);
        return true;
      }
      catch (Exception)
      {
        // Console.WriteLine(e);
        return false;
      }
    }

    public Boolean SaveToFile(String fileentry)
    {
      try
      {
        String Text = FUCEditor.Text; // Necessary, but why????
        FileName = fileentry;
        FUCEditor.Text = Text;
        SaveToFile();
        return true;
      }
      catch (Exception)
      {
        // Console.WriteLine(e);
        return false;
      }
    }

	}
}
