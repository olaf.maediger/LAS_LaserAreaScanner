﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Initdata;
using UCNotifier;
//
namespace UCTextEditor
{
  public partial class CUCTextEditor
  {
    //
    //----------------------------
    //  Segment - Local Helper
    //----------------------------
    public Boolean ClearText()
    {
      try
      {
        FUCEditor.Clear();
        return true;
      }
      catch (Exception)
      {
        // Console.WriteLine(e);
        return false;
      }
    }

    private Boolean CopyToClipboard()
    {
      try
      {
        FUCEditor.SelectAll();
        Clipboard.SetData(DataFormats.Text, FUCEditor.SelectedText);
        FUCEditor.Select(0, 0);
        return true;
      }
      catch (Exception)
      {
        // Console.WriteLine(e);
        return false;
      }
    }

    private Boolean SelectFontModal()
    {
      try
      {
        if (DialogResult.OK == DialogSelectFont.ShowDialog())
        {
          FUCEditor.Font = DialogSelectFont.Font;
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        // Console.WriteLine(e);
        return false;
      }
    }

    private Boolean LoadFromFileModal()
    {
      try
      {
        DialogLoadText.FileName = FileName;
        if (DialogResult.OK == DialogLoadText.ShowDialog())
        {
          FileName = DialogLoadText.FileName;
          FUCEditor.LoadFile(FileName, RichTextBoxStreamType.PlainText);
          SetIsChanged(false);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        // Console.WriteLine(e);
        return false;
      }
    }

    private Boolean LoadFromFile()
    {
      try
      {
        if (File.Exists(FileName))
        {
          FUCEditor.LoadFile(FileName, RichTextBoxStreamType.PlainText);
          SetIsChanged(false);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        // Console.WriteLine(e);
        return false;
      }
    }

    private Boolean SaveToFileModal()
    {
      try
      {
        DialogSaveText.FileName = FileName;
        if (DialogResult.OK == DialogSaveText.ShowDialog())
        {
          FileName = DialogSaveText.FileName;
          FUCEditor.SaveFile(FileName, RichTextBoxStreamType.PlainText);
          SetIsChanged(false);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        // Console.WriteLine(e);
        return false;
      }
    }

    private Boolean SaveToFile()
    {
      try
      {
        FUCEditor.SaveFile(FileName, RichTextBoxStreamType.PlainText);
        SetIsChanged(false);
        return true;
      }
      catch (Exception)
      {
        // Console.WriteLine(e);
        return false;
      }
    }
  }
}