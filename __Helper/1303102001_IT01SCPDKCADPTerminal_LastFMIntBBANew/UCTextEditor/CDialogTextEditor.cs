﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCTextEditor
{
  public partial class CDialogTextEditor : Form
  {
    public CDialogTextEditor()
    {
      InitializeComponent();

      CDialogTextEditor_Resize(this, null);
      FUCTextEditor.Dock = DockStyle.Fill;
    }

    private void CDialogTextEditor_Resize(object sender, EventArgs e)
    {
      btnOk.Left = pnlKey.Width / 2 - btnOk.Width  - 4;
      btnCancel.Left = pnlKey.Width / 2 + 4;
    }


    public Boolean TextIsChanged
    {
      get { return FUCTextEditor.IsChanged; }
    }

    public Boolean TextReadOnly
    {
      get { return FUCTextEditor.IsReadOnly; }
      set { FUCTextEditor.IsReadOnly = value; }
    }

    public String LoadFileExtension
    {
      get { return FUCTextEditor.LoadFileExtension; }
      set { FUCTextEditor.LoadFileExtension = value; }
    }
    public String LoadFileName
    {
      get { return FUCTextEditor.LoadFileName; }
      set { FUCTextEditor.LoadFileName = value; }
    }
    public String LoadFileFilter
    {
      get { return FUCTextEditor.LoadFileFilter; }
      set { FUCTextEditor.LoadFileFilter = value; }
    }

    public String SaveFileExtension
    {
      get { return FUCTextEditor.SaveFileExtension; }
      set { FUCTextEditor.SaveFileExtension = value; }
    }
    public String SaveFileName
    {
      get { return FUCTextEditor.SaveFileName; }
      set { FUCTextEditor.SaveFileName = value; }
    }
    public String SaveFileFilter
    {
      get { return FUCTextEditor.SaveFileFilter; }
      set { FUCTextEditor.SaveFileFilter = value; }
    }

    public Boolean LoadFromFile(String fileentry)
    {
      try
      {
        return FUCTextEditor.LoadFromFile(fileentry);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean SaveToFile(String fileentry)
    {
      try
      {
        return FUCTextEditor.SaveToFile(fileentry);
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
