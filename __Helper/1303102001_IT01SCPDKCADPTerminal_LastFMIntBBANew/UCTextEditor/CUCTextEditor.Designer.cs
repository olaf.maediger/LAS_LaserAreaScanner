﻿namespace UCTextEditor
{
	partial class CUCTextEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.components = new System.ComponentModel.Container();
      this.pnlLoad = new System.Windows.Forms.Panel();
      this.btnClearLoadList = new System.Windows.Forms.Button();
      this.btnLoad = new System.Windows.Forms.Button();
      this.cbxLoadFromFile = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.DialogSelectFont = new System.Windows.Forms.FontDialog();
      this.cmsEditor = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mitLoad = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSave = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSaveAs = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
      this.mitParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.DialogLoadText = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveText = new System.Windows.Forms.SaveFileDialog();
      this.pnlSave = new System.Windows.Forms.Panel();
      this.btnClearSaveList = new System.Windows.Forms.Button();
      this.btnSaveToFile = new System.Windows.Forms.Button();
      this.cbxSaveToFile = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.pnlParameter = new System.Windows.Forms.Panel();
      this.cbxReadOnly = new System.Windows.Forms.CheckBox();
      this.btnSelectFont = new System.Windows.Forms.Button();
      this.scbTabulator = new System.Windows.Forms.HScrollBar();
      this.lblTabulator = new System.Windows.Forms.Label();
      this.stsEditor = new System.Windows.Forms.StatusStrip();
      this.stsRow = new System.Windows.Forms.ToolStripStatusLabel();
      this.stsCol = new System.Windows.Forms.ToolStripStatusLabel();
      this.stsFileName = new System.Windows.Forms.ToolStripStatusLabel();
      this.stsPath = new System.Windows.Forms.ToolStripStatusLabel();
      this.FUCEditor = new UCTextEditor.CUCRichTextBox();
      this.pnlLoad.SuspendLayout();
      this.cmsEditor.SuspendLayout();
      this.pnlSave.SuspendLayout();
      this.pnlParameter.SuspendLayout();
      this.stsEditor.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlLoad
      // 
      this.pnlLoad.Controls.Add(this.btnClearLoadList);
      this.pnlLoad.Controls.Add(this.btnLoad);
      this.pnlLoad.Controls.Add(this.cbxLoadFromFile);
      this.pnlLoad.Controls.Add(this.label1);
      this.pnlLoad.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlLoad.Location = new System.Drawing.Point(0, 0);
      this.pnlLoad.Name = "pnlLoad";
      this.pnlLoad.Size = new System.Drawing.Size(614, 34);
      this.pnlLoad.TabIndex = 0;
      // 
      // btnClearLoadList
      // 
      this.btnClearLoadList.Location = new System.Drawing.Point(423, 6);
      this.btnClearLoadList.Name = "btnClearLoadList";
      this.btnClearLoadList.Size = new System.Drawing.Size(44, 23);
      this.btnClearLoadList.TabIndex = 10;
      this.btnClearLoadList.Text = "Clear";
      this.btnClearLoadList.UseVisualStyleBackColor = true;
      this.btnClearLoadList.Click += new System.EventHandler(this.btnClearLoadList_Click);
      // 
      // btnLoad
      // 
      this.btnLoad.Location = new System.Drawing.Point(12, 5);
      this.btnLoad.Name = "btnLoad";
      this.btnLoad.Size = new System.Drawing.Size(93, 23);
      this.btnLoad.TabIndex = 9;
      this.btnLoad.Text = "Load from File";
      this.btnLoad.UseVisualStyleBackColor = true;
      this.btnLoad.Click += new System.EventHandler(this.btnLoadFromFile_Click);
      // 
      // cbxLoadFromFile
      // 
      this.cbxLoadFromFile.BackColor = System.Drawing.SystemColors.Info;
      this.cbxLoadFromFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxLoadFromFile.FormattingEnabled = true;
      this.cbxLoadFromFile.Location = new System.Drawing.Point(170, 7);
      this.cbxLoadFromFile.Name = "cbxLoadFromFile";
      this.cbxLoadFromFile.Size = new System.Drawing.Size(247, 21);
      this.cbxLoadFromFile.Sorted = true;
      this.cbxLoadFromFile.TabIndex = 6;
      this.cbxLoadFromFile.SelectedValueChanged += new System.EventHandler(this.cbxLoadFromFile_SelectedValueChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(115, 10);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(49, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "Filename";
      // 
      // cmsEditor
      // 
      this.cmsEditor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitLoad,
            this.mitSave,
            this.mitSaveAs,
            this.toolStripMenuItem1,
            this.mitParameter});
      this.cmsEditor.Name = "cmsEditor";
      this.cmsEditor.Size = new System.Drawing.Size(136, 98);
      // 
      // mitLoad
      // 
      this.mitLoad.Name = "mitLoad";
      this.mitLoad.Size = new System.Drawing.Size(135, 22);
      this.mitLoad.Text = "Load";
      this.mitLoad.Click += new System.EventHandler(this.mitLoad_Click);
      // 
      // mitSave
      // 
      this.mitSave.Name = "mitSave";
      this.mitSave.Size = new System.Drawing.Size(135, 22);
      this.mitSave.Text = "Save";
      this.mitSave.Click += new System.EventHandler(this.mitSave_Click);
      // 
      // mitSaveAs
      // 
      this.mitSaveAs.Name = "mitSaveAs";
      this.mitSaveAs.Size = new System.Drawing.Size(135, 22);
      this.mitSaveAs.Text = "Save as";
      this.mitSaveAs.Click += new System.EventHandler(this.mitSaveAs_Click);
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new System.Drawing.Size(132, 6);
      // 
      // mitParameter
      // 
      this.mitParameter.Name = "mitParameter";
      this.mitParameter.Size = new System.Drawing.Size(135, 22);
      this.mitParameter.Text = "Parameter";
      this.mitParameter.Click += new System.EventHandler(this.mitParameter_Click);
      // 
      // DialogLoadText
      // 
      this.DialogLoadText.DefaultExt = "txt";
      this.DialogLoadText.FileName = "Data";
      this.DialogLoadText.Filter = "Textfiles (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogLoadText.Title = "Load Text";
      // 
      // DialogSaveText
      // 
      this.DialogSaveText.DefaultExt = "txt";
      this.DialogSaveText.FileName = "Data";
      this.DialogSaveText.Filter = "Textfiles (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogSaveText.Title = "Save Text";
      // 
      // pnlSave
      // 
      this.pnlSave.Controls.Add(this.btnClearSaveList);
      this.pnlSave.Controls.Add(this.btnSaveToFile);
      this.pnlSave.Controls.Add(this.cbxSaveToFile);
      this.pnlSave.Controls.Add(this.label2);
      this.pnlSave.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlSave.Location = new System.Drawing.Point(0, 34);
      this.pnlSave.Name = "pnlSave";
      this.pnlSave.Size = new System.Drawing.Size(614, 34);
      this.pnlSave.TabIndex = 3;
      // 
      // btnClearSaveList
      // 
      this.btnClearSaveList.Location = new System.Drawing.Point(423, 6);
      this.btnClearSaveList.Name = "btnClearSaveList";
      this.btnClearSaveList.Size = new System.Drawing.Size(44, 23);
      this.btnClearSaveList.TabIndex = 12;
      this.btnClearSaveList.Text = "Clear";
      this.btnClearSaveList.UseVisualStyleBackColor = true;
      this.btnClearSaveList.Click += new System.EventHandler(this.btnClearSaveList_Click);
      // 
      // btnSaveToFile
      // 
      this.btnSaveToFile.Location = new System.Drawing.Point(12, 5);
      this.btnSaveToFile.Name = "btnSaveToFile";
      this.btnSaveToFile.Size = new System.Drawing.Size(93, 23);
      this.btnSaveToFile.TabIndex = 11;
      this.btnSaveToFile.Text = "Save to File";
      this.btnSaveToFile.UseVisualStyleBackColor = true;
      this.btnSaveToFile.Click += new System.EventHandler(this.btnSaveToFile_Click);
      // 
      // cbxSaveToFile
      // 
      this.cbxSaveToFile.BackColor = System.Drawing.SystemColors.Info;
      this.cbxSaveToFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxSaveToFile.FormattingEnabled = true;
      this.cbxSaveToFile.Location = new System.Drawing.Point(170, 7);
      this.cbxSaveToFile.Name = "cbxSaveToFile";
      this.cbxSaveToFile.Size = new System.Drawing.Size(247, 21);
      this.cbxSaveToFile.Sorted = true;
      this.cbxSaveToFile.TabIndex = 6;
      this.cbxSaveToFile.SelectedValueChanged += new System.EventHandler(this.cbxSaveToFile_SelectedValueChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(115, 10);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(49, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "Filename";
      // 
      // pnlParameter
      // 
      this.pnlParameter.Controls.Add(this.cbxReadOnly);
      this.pnlParameter.Controls.Add(this.btnSelectFont);
      this.pnlParameter.Controls.Add(this.scbTabulator);
      this.pnlParameter.Controls.Add(this.lblTabulator);
      this.pnlParameter.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlParameter.Location = new System.Drawing.Point(0, 68);
      this.pnlParameter.Name = "pnlParameter";
      this.pnlParameter.Size = new System.Drawing.Size(614, 34);
      this.pnlParameter.TabIndex = 4;
      // 
      // cbxReadOnly
      // 
      this.cbxReadOnly.AutoSize = true;
      this.cbxReadOnly.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.cbxReadOnly.Location = new System.Drawing.Point(19, 9);
      this.cbxReadOnly.Name = "cbxReadOnly";
      this.cbxReadOnly.Size = new System.Drawing.Size(76, 17);
      this.cbxReadOnly.TabIndex = 6;
      this.cbxReadOnly.Text = "Read Only";
      this.cbxReadOnly.UseVisualStyleBackColor = true;
      this.cbxReadOnly.CheckedChanged += new System.EventHandler(this.cbxReadOnly_CheckedChanged);
      // 
      // btnSelectFont
      // 
      this.btnSelectFont.Location = new System.Drawing.Point(327, 6);
      this.btnSelectFont.Name = "btnSelectFont";
      this.btnSelectFont.Size = new System.Drawing.Size(93, 23);
      this.btnSelectFont.TabIndex = 5;
      this.btnSelectFont.Text = "Select Font";
      this.btnSelectFont.UseVisualStyleBackColor = true;
      this.btnSelectFont.Click += new System.EventHandler(this.btnSelectFont_Click);
      // 
      // scbTabulator
      // 
      this.scbTabulator.LargeChange = 4;
      this.scbTabulator.Location = new System.Drawing.Point(184, 10);
      this.scbTabulator.Maximum = 32;
      this.scbTabulator.Minimum = 1;
      this.scbTabulator.Name = "scbTabulator";
      this.scbTabulator.Size = new System.Drawing.Size(127, 16);
      this.scbTabulator.TabIndex = 4;
      this.scbTabulator.Value = 4;
      this.scbTabulator.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scbTabulator_Scroll);
      // 
      // lblTabulator
      // 
      this.lblTabulator.AutoSize = true;
      this.lblTabulator.Location = new System.Drawing.Point(110, 11);
      this.lblTabulator.Name = "lblTabulator";
      this.lblTabulator.Size = new System.Drawing.Size(61, 13);
      this.lblTabulator.TabIndex = 3;
      this.lblTabulator.Text = "Tabulator []";
      // 
      // stsEditor
      // 
      this.stsEditor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stsRow,
            this.stsCol,
            this.stsFileName,
            this.stsPath});
      this.stsEditor.Location = new System.Drawing.Point(0, 575);
      this.stsEditor.Name = "stsEditor";
      this.stsEditor.Size = new System.Drawing.Size(614, 22);
      this.stsEditor.SizingGrip = false;
      this.stsEditor.TabIndex = 5;
      this.stsEditor.Resize += new System.EventHandler(this.stsEditor_Resize);
      // 
      // stsRow
      // 
      this.stsRow.BackColor = System.Drawing.SystemColors.Info;
      this.stsRow.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)));
      this.stsRow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.stsRow.Name = "stsRow";
      this.stsRow.Size = new System.Drawing.Size(45, 17);
      this.stsRow.Text = "Row: 0";
      // 
      // stsCol
      // 
      this.stsCol.BackColor = System.Drawing.SystemColors.Info;
      this.stsCol.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)));
      this.stsCol.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.stsCol.Name = "stsCol";
      this.stsCol.Size = new System.Drawing.Size(39, 17);
      this.stsCol.Text = "Col: 0";
      // 
      // stsFileName
      // 
      this.stsFileName.BackColor = System.Drawing.SystemColors.Info;
      this.stsFileName.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)));
      this.stsFileName.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.stsFileName.Name = "stsFileName";
      this.stsFileName.Size = new System.Drawing.Size(41, 17);
      this.stsFileName.Text = "<file>";
      this.stsFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // stsPath
      // 
      this.stsPath.AutoSize = false;
      this.stsPath.BackColor = System.Drawing.SystemColors.Info;
      this.stsPath.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)));
      this.stsPath.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.stsPath.Name = "stsPath";
      this.stsPath.Size = new System.Drawing.Size(163, 17);
      this.stsPath.Text = "<path>";
      this.stsPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // FUCEditor
      // 
      this.FUCEditor.AcceptsTab = true;
      this.FUCEditor.ContextMenuStrip = this.cmsEditor;
      this.FUCEditor.Font = new System.Drawing.Font("Courier New", 9.75F);
      this.FUCEditor.Location = new System.Drawing.Point(12, 108);
      this.FUCEditor.Name = "FUCEditor";
      this.FUCEditor.Size = new System.Drawing.Size(581, 440);
      this.FUCEditor.TabIndex = 8;
      this.FUCEditor.Text = "";
      this.FUCEditor.CursorPositionChanged += new System.EventHandler(this.FUCEditor_FCursorPositionChanged);
      this.FUCEditor.TextChanged += new System.EventHandler(this.FUCEditor_TextChanged);
      // 
      // CUCTextEditor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCEditor);
      this.Controls.Add(this.pnlParameter);
      this.Controls.Add(this.pnlSave);
      this.Controls.Add(this.pnlLoad);
      this.Controls.Add(this.stsEditor);
      this.Name = "CUCTextEditor";
      this.Size = new System.Drawing.Size(614, 597);
      this.Load += new System.EventHandler(this.CUCTextEditor_Load);
      this.Resize += new System.EventHandler(this.CUCTextEditor_Resize);
      this.pnlLoad.ResumeLayout(false);
      this.pnlLoad.PerformLayout();
      this.cmsEditor.ResumeLayout(false);
      this.pnlSave.ResumeLayout(false);
      this.pnlSave.PerformLayout();
      this.pnlParameter.ResumeLayout(false);
      this.pnlParameter.PerformLayout();
      this.stsEditor.ResumeLayout(false);
      this.stsEditor.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

		}

		#endregion

    private System.Windows.Forms.Panel pnlLoad;
    private System.Windows.Forms.FontDialog DialogSelectFont;
    private System.Windows.Forms.Button btnLoad;
		private System.Windows.Forms.ComboBox cbxLoadFromFile;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.OpenFileDialog DialogLoadText;
		private System.Windows.Forms.SaveFileDialog DialogSaveText;
    private System.Windows.Forms.ContextMenuStrip cmsEditor;
    private System.Windows.Forms.ToolStripMenuItem mitLoad;
    private System.Windows.Forms.ToolStripMenuItem mitSave;
    private System.Windows.Forms.ToolStripMenuItem mitSaveAs;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem mitParameter;
    private System.Windows.Forms.Panel pnlSave;
    private System.Windows.Forms.Button btnSaveToFile;
    private System.Windows.Forms.ComboBox cbxSaveToFile;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Panel pnlParameter;
    private System.Windows.Forms.CheckBox cbxReadOnly;
    private System.Windows.Forms.Button btnSelectFont;
    private System.Windows.Forms.HScrollBar scbTabulator;
    private System.Windows.Forms.Label lblTabulator;
    private System.Windows.Forms.StatusStrip stsEditor;
    private System.Windows.Forms.ToolStripStatusLabel stsRow;
    private System.Windows.Forms.ToolStripStatusLabel stsCol;
    private System.Windows.Forms.ToolStripStatusLabel stsFileName;
    private System.Windows.Forms.ToolStripStatusLabel stsPath;
    private System.Windows.Forms.Button btnClearLoadList;
    private System.Windows.Forms.Button btnClearSaveList;
    private CUCRichTextBox FUCEditor;
	}
}
