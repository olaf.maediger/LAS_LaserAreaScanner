﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arithmetic
{
  //
  //*****************************************************
  //  Section - Type
  //*****************************************************
  //
  public enum EStateOperation
  {
    Error = -1,
    Idle = 0,
    Busy = 1,
    Success = 2
  };

  public delegate void DOnCompleted(Int32 code);

  public abstract class COperation
  {
    //
    //*****************************************************
    //  Section - Field
    //*****************************************************
    //
    protected EStateOperation FStateOperation;
    protected Int32 FClockStart;
    protected DOnCompleted FOnCompleted;
    //
    //*****************************************************
    //  Section - Constructor
    //*****************************************************
    //
    public COperation(DOnCompleted oncompleted)
    {
      FOnCompleted = oncompleted;
      FStateOperation = EStateOperation.Idle;
    }
    //
    //*****************************************************
    //  Section - Property
    //*****************************************************
    //
    private void SetStateOperation(EStateOperation value)
    {
      FStateOperation = value;
    }
    public EStateOperation StateOperation
    {
      get { return FStateOperation; }
      set { SetStateOperation(value); }
    }
    //
    //*****************************************************
    //  Section - Helper
    //*****************************************************
    //
    protected Int32 PresetStateClock(Int32 clockcount)
    {
      FClockStart = clockcount;
      return clockcount - FClockStart;
    }
    protected Int32 CalculateStateClock(Int32 clockcount)
    {
      return clockcount - FClockStart;
    }
    //
    //*****************************************************
    //  Section - Public Management
    //*****************************************************
    //
    public abstract Boolean Clock(Int32 clockcount);
    
   
  }
}
