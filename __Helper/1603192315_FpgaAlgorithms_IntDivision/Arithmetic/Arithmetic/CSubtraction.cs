﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arithmetic
{
  public class CSubtraction : COperation
  { //
    //*****************************************************
    //  Section - Field
    //*****************************************************
    //
    protected Int32 FMinuend;
    protected Int32 FSubtrahend;
    protected Int32 FResult;
    //
    //*****************************************************
    //  Section - Constructor
    //*****************************************************
    //
    public CSubtraction(Int32 minuend, Int32 subtrahend,
                       DOnCompleted oncompleted)
      : base(oncompleted)
    {
      FMinuend = minuend;
      FSubtrahend = subtrahend;
      FResult = 0;
      FClockStart = -1;
      FStateOperation = EStateOperation.Idle;
    }
    //
    //*****************************************************
    //  Section - Property
    //*****************************************************
    //
    public Int32 Minuend
    {
      get { return FMinuend; }
    }
    public Int32 Subtrahend
    {
      get { return FSubtrahend; }
    }
    public Int32 Result
    {
      get { return FResult; }
    }
    //
    //*****************************************************
    //  Section - Helper
    //*****************************************************
    //

    //
    //*****************************************************
    //  Section - Callback
    //*****************************************************
    //
   
    //
    //*****************************************************
    //  Section - Public Management
    //*****************************************************
    //
    public override Boolean Clock(Int32 clockcount)
    {
      try
      { // here: use only VHDL-Compatibles:
        //FResult = FSummandA + FSummandB;
        Int32 StateClock;
        switch (FStateOperation)
        {
          case EStateOperation.Idle:
            FResult = 0;
            StateOperation = EStateOperation.Busy;
            StateClock = PresetStateClock(clockcount);
            break;
          case EStateOperation.Busy:
            // Minus Operator in Vhdl:
            FResult = FMinuend - FSubtrahend;
            if (0 < (0x80000000 & FResult))
            { // Overflow
              FStateOperation = EStateOperation.Error;
              if (FOnCompleted is DOnCompleted)
              {
                FOnCompleted((int)EStateOperation.Error);
              }
            }
            else
            {
              StateOperation = EStateOperation.Success;
              if (FOnCompleted is DOnCompleted)
              {
                FOnCompleted((int)EStateOperation.Success);
              }
            }
            break;
          case EStateOperation.Success: // wait for new instantiation
            break;
          case EStateOperation.Error: // wait for new instantiation
            break;
        }
        return true;
      }
      catch (Exception)
      {
        FStateOperation = EStateOperation.Error;
        if (FOnCompleted is DOnCompleted)
        {
          FOnCompleted((int)EStateOperation.Error);
        }
        return false;
      }
    }

  }
}
