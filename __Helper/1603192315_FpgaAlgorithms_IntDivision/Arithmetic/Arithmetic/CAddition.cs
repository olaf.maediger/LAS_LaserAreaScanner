﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace Arithmetic
{
  public class CAddition : COperation
  { //
    //*****************************************************
    //  Section - Field
    //*****************************************************
    //
    protected Int32 FSummandA;
    protected Int32 FSummandB;
    protected Int32 FResult;
    //
    //*****************************************************
    //  Section - Constructor
    //*****************************************************
    //
    public CAddition(Int32 summanda, Int32 summandb, 
                DOnCompleted oncompleted)
      : base(oncompleted)
    {
      FSummandA = summanda;
      FSummandB = summandb;
      FResult = 0;
      FClockStart = -1;
      FStateOperation = EStateOperation.Idle;
    }
    //
    //*****************************************************
    //  Section - Property
    //*****************************************************
    //
    public Int32 SummandA
    {
      get { return FSummandA; }
    }
    public Int32 SummandB
    {
      get { return FSummandB; }
    }
    public Int32 Result
    {
      get { return FResult; }
    }
    //
    //*****************************************************
    //  Section - Helper
    //*****************************************************
    //

    //
    //*****************************************************
    //  Section - Callback
    //*****************************************************
    //
   
    //
    //*****************************************************
    //  Section - Public Management
    //*****************************************************
    //
    public override Boolean Clock(Int32 clockcount)
    {
      try
      { // here: use only VHDL-Compatibles:
        //FResult = FSummandA + FSummandB;
        Int32 StateClock;
        switch (FStateOperation)
        {
          case EStateOperation.Idle:
            FResult = 0;
            StateOperation = EStateOperation.Busy;
            StateClock = PresetStateClock(clockcount);
            break;
          case EStateOperation.Busy:
            // Plus Operator in Vhdl:
            FResult = FSummandA + FSummandB;
            if (0 < (0x80000000 & FResult))
            { // Overflow
              FStateOperation = EStateOperation.Error;
              if (FOnCompleted is DOnCompleted)
              {
                FOnCompleted((int)EStateOperation.Error);
              }
            }
            else
            {
              StateOperation = EStateOperation.Success;
              if (FOnCompleted is DOnCompleted)
              {
                FOnCompleted((int)EStateOperation.Success);
              }
            }
            break;
          case EStateOperation.Success: // wait for new instantiation
            break;
          case EStateOperation.Error: // wait for new instantiation
            break;
        }
        return true;
      }
      catch (Exception)
      {
        FStateOperation = EStateOperation.Error;
        if (FOnCompleted is DOnCompleted)
        {
          FOnCompleted((int)EStateOperation.Error);
        }
        return false;
      }
    }

  }
}
