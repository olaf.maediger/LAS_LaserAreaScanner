﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arithmetic
{
  public class CMultiplication : COperation
  { //
    //*****************************************************
    //  Section - Field
    //*****************************************************
    //
    protected Int32 FMultiplicand;
    protected Int32 FMultiplier;
    protected Int32 FResult;
    private Int32 FDoubleValue;
    private Int32 FHalfValue;
    //
    //*****************************************************
    //  Section - Constructor
    //*****************************************************
    //
    public CMultiplication(Int32 multiplicand, Int32 multiplier, 
                    DOnCompleted oncompleted)
      : base(oncompleted)
    {
      FMultiplicand = multiplicand;
      FMultiplier = multiplier;
      FResult = 0;
      FClockStart = -1;
      FStateOperation = EStateOperation.Idle;
    }
    //
    //*****************************************************
    //  Section - Property
    //*****************************************************
    //
    public Int32 Multiplicand
    {
      get { return FMultiplicand; }
    }
    public Int32 Multiplier
    {
      get { return FMultiplier; }
    }
    public Int32 Result
    {
      get { return FResult; }
    }
    //
    //*****************************************************
    //  Section - Helper
    //*****************************************************
    //

    //
    //*****************************************************
    //  Section - Callback
    //*****************************************************
    //
   
    //
    //*****************************************************
    //  Section - Public Management
    //*****************************************************
    //
    public override Boolean Clock(Int32 clockcount)
    {
      try
      { // here: use only VHDL-Compatibles:
        //FResult = FSummandA + FSummandB;
        Int32 StateClock;
        switch (FStateOperation)
        {
          case EStateOperation.Idle:
            FResult = 0;
            StateOperation = EStateOperation.Busy;
            StateClock = PresetStateClock(clockcount);
            // one factor equal zero?
            if ((0 == FMultiplicand) || (0 == FMultiplier))
            {
              FResult = 0;
              FStateOperation = EStateOperation.Success;
              if (FOnCompleted is DOnCompleted)
              {
                FOnCompleted((int)EStateOperation.Success);
              }
            }
            else // lower factor to FHalfValue
              if (Math.Abs(FMultiplier) < Math.Abs(FMultiplicand))
              { 
                FDoubleValue = Math.Abs(FMultiplicand);
                FHalfValue = Math.Abs(FMultiplier);
              }
              else
              {
                FDoubleValue = Math.Abs(FMultiplier);
                FHalfValue = Math.Abs(FMultiplicand);
              }
            break;
          case EStateOperation.Busy:
            if (1 <= FHalfValue)
            {
              if (0x00000000 < (0x00000001 & FHalfValue))
              { // odd (d:ungerade)
                FResult += FDoubleValue;
              }
              else
              { // even (d: gerade)
                // nothing
              }
              // preparation next step
              FDoubleValue <<= 1;
              FHalfValue >>= 1;
            } 
            else
            { // end reached
              Boolean SignNegative = ((0 < FMultiplicand) ^ (0 < FMultiplier));
              if (SignNegative)
              {
                FResult = -FResult;
              }
              StateOperation = EStateOperation.Success;
              if (FOnCompleted is DOnCompleted)
              {
                FOnCompleted((int)EStateOperation.Success);
              }
            }
            break;
          case EStateOperation.Success: // wait for new instantiation
            break;
          case EStateOperation.Error: // wait for new instantiation
            break;
        }
        return true;
      }
      catch (Exception)
      {
        FStateOperation = EStateOperation.Error;
        if (FOnCompleted is DOnCompleted)
        {
          FOnCompleted((int)EStateOperation.Error);
        }
        return false;
      }
    }

  }
}
