﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace Arithmetic
{
  public class CDivision : COperation
  { //
    //*****************************************************
    //  Section - Field
    //*****************************************************
    //
    protected Int32 FDividend;
    protected Int32 FDivisor;
    protected Int32 FQuotient;
    protected Int32 FRemainder;
    //
    //*****************************************************
    //  Section - Constructor
    //*****************************************************
    //
    public CDivision(Int32 dividend, Int32 divisor,
                     DOnCompleted oncompleted)
      : base(oncompleted)
    {
      FDividend = dividend;
      FDivisor = divisor;
      FQuotient = 0;
      FRemainder = 0;
      FClockStart = -1;
      FStateOperation = EStateOperation.Idle;
    }
    //
    //*****************************************************
    //  Section - Property
    //*****************************************************
    //
    public Int32 Dividend
    {
      get { return FDividend; }
    }
    public Int32 Divisor
    {
      get { return FDivisor; }
    }
    public Int32 Quotient
    {
      get { return FQuotient; }
    }
    public Int32 Remainder
    {
      get { return FRemainder; }
    }
    //
    //*****************************************************
    //  Section - Helper
    //*****************************************************
    //

    //
    //*****************************************************
    //  Section - Callback
    //*****************************************************
    //
    //
    //*****************************************************
    //  Section - Public Management
    //*****************************************************
    public override Boolean Clock(Int32 clockcount)
    {
      try
      { // here: use only VHDL-Compatibles:
        //FResult = FSummandA + FSummandB;
        Int32 StateClock;
        switch (FStateOperation)
        {
          case EStateOperation.Idle:
            FQuotient = 0;
            FRemainder = 0; 
            StateOperation = EStateOperation.Busy;
            StateClock = PresetStateClock(clockcount);
            // one factor equal zero?
            if (0 == FDividend) 
            {
              FQuotient = 0;
              FRemainder = 0;
              FStateOperation = EStateOperation.Success;
              if (FOnCompleted is DOnCompleted)
              {
                FOnCompleted((int)EStateOperation.Success);
              }
            }
            else
              if (0 == FDivisor) 
              {
                FQuotient = 0;
                FRemainder = 0;
                FStateOperation = EStateOperation.Error;
                if (FOnCompleted is DOnCompleted)
                {
                  FOnCompleted((int)EStateOperation.Error);
                }
              }
              else
              {
                FStateOperation = EStateOperation.Busy;
              }
            break;
          case EStateOperation.Busy:
            if (0 < (FDividend - (FQuotient * FDivisor)))
            {
             
              FDividend -= FDivisor;
              FQuotient++;
              FRemainder = FDividend;
            }
            else
            { // end reached
              StateOperation = EStateOperation.Success;
              if (FOnCompleted is DOnCompleted)
              {
                FOnCompleted((int)EStateOperation.Success);
              }
            }
            break;
          case EStateOperation.Success: // wait for new instantiation
            break;
          case EStateOperation.Error: // wait for new instantiation
            break;
        }
        return true;
      }
      catch (Exception)
      {
        FStateOperation = EStateOperation.Error;
        if (FOnCompleted is DOnCompleted)
        {
          FOnCompleted((int)EStateOperation.Error);
        }
        return false;
      }
    }


  }
}
