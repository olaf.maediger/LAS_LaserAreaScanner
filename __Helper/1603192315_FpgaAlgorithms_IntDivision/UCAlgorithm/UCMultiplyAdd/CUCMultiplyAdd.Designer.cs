﻿namespace UCMultiplyAdd
{
  partial class CUCMultiplyAdd
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxStartStop = new System.Windows.Forms.CheckBox();
      this.lblHeader = new System.Windows.Forms.Label();
      this.pbxDiagram = new System.Windows.Forms.PictureBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.nudPeriod = new System.Windows.Forms.NumericUpDown();
      this.lblProductA = new System.Windows.Forms.Label();
      this.nudMultiplier = new System.Windows.Forms.NumericUpDown();
      this.nudMultiplicand = new System.Windows.Forms.NumericUpDown();
      this.lblResult = new System.Windows.Forms.Label();
      this.nudSummand = new System.Windows.Forms.NumericUpDown();
      this.lblProductB = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.pbxDiagram)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMultiplier)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMultiplicand)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSummand)).BeginInit();
      this.SuspendLayout();
      // 
      // cbxStartStop
      // 
      this.cbxStartStop.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxStartStop.Location = new System.Drawing.Point(420, 518);
      this.cbxStartStop.Name = "cbxStartStop";
      this.cbxStartStop.Size = new System.Drawing.Size(112, 36);
      this.cbxStartStop.TabIndex = 1;
      this.cbxStartStop.Text = "<startstop>";
      this.cbxStartStop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxStartStop.UseVisualStyleBackColor = true;
      this.cbxStartStop.CheckedChanged += new System.EventHandler(this.cbxStartStop_CheckedChanged);
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(779, 21);
      this.lblHeader.TabIndex = 2;
      this.lblHeader.Text = "MultiplyAdd";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pbxDiagram
      // 
      this.pbxDiagram.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxDiagram.Image = global::UCMultiplyAdd.Properties.Resources.MultiplyAdd;
      this.pbxDiagram.Location = new System.Drawing.Point(0, 21);
      this.pbxDiagram.Name = "pbxDiagram";
      this.pbxDiagram.Size = new System.Drawing.Size(779, 592);
      this.pbxDiagram.TabIndex = 3;
      this.pbxDiagram.TabStop = false;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(51, 530);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(37, 13);
      this.label2.TabIndex = 17;
      this.label2.Text = "Period";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(197, 530);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(20, 13);
      this.label1.TabIndex = 16;
      this.label1.Text = "ms";
      // 
      // nudPeriod
      // 
      this.nudPeriod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
      this.nudPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudPeriod.Location = new System.Drawing.Point(116, 523);
      this.nudPeriod.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
      this.nudPeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPeriod.Name = "nudPeriod";
      this.nudPeriod.Size = new System.Drawing.Size(75, 26);
      this.nudPeriod.TabIndex = 15;
      this.nudPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPeriod.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // lblProductA
      // 
      this.lblProductA.BackColor = System.Drawing.SystemColors.Info;
      this.lblProductA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblProductA.Location = new System.Drawing.Point(533, 221);
      this.lblProductA.Name = "lblProductA";
      this.lblProductA.Size = new System.Drawing.Size(114, 23);
      this.lblProductA.TabIndex = 20;
      this.lblProductA.Text = "0";
      this.lblProductA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudMultiplier
      // 
      this.nudMultiplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudMultiplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudMultiplier.Location = new System.Drawing.Point(305, 220);
      this.nudMultiplier.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudMultiplier.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudMultiplier.Name = "nudMultiplier";
      this.nudMultiplier.Size = new System.Drawing.Size(113, 26);
      this.nudMultiplier.TabIndex = 19;
      this.nudMultiplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMultiplier.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      // 
      // nudMultiplicand
      // 
      this.nudMultiplicand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudMultiplicand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudMultiplicand.Location = new System.Drawing.Point(80, 220);
      this.nudMultiplicand.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudMultiplicand.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudMultiplicand.Name = "nudMultiplicand";
      this.nudMultiplicand.Size = new System.Drawing.Size(113, 26);
      this.nudMultiplicand.TabIndex = 18;
      this.nudMultiplicand.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMultiplicand.Value = new decimal(new int[] {
            123,
            0,
            0,
            0});
      // 
      // lblResult
      // 
      this.lblResult.BackColor = System.Drawing.SystemColors.Info;
      this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblResult.Location = new System.Drawing.Point(534, 409);
      this.lblResult.Name = "lblResult";
      this.lblResult.Size = new System.Drawing.Size(114, 23);
      this.lblResult.TabIndex = 23;
      this.lblResult.Text = "0";
      this.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSummand
      // 
      this.nudSummand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudSummand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudSummand.Location = new System.Drawing.Point(79, 408);
      this.nudSummand.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudSummand.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudSummand.Name = "nudSummand";
      this.nudSummand.Size = new System.Drawing.Size(113, 26);
      this.nudSummand.TabIndex = 21;
      this.nudSummand.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSummand.Value = new decimal(new int[] {
            1600,
            0,
            0,
            0});
      // 
      // lblProductB
      // 
      this.lblProductB.BackColor = System.Drawing.SystemColors.Info;
      this.lblProductB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblProductB.Location = new System.Drawing.Point(305, 409);
      this.lblProductB.Name = "lblProductB";
      this.lblProductB.Size = new System.Drawing.Size(114, 23);
      this.lblProductB.TabIndex = 24;
      this.lblProductB.Text = "0";
      this.lblProductB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCMultiplyAdd
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lblProductB);
      this.Controls.Add(this.lblResult);
      this.Controls.Add(this.nudSummand);
      this.Controls.Add(this.lblProductA);
      this.Controls.Add(this.nudMultiplier);
      this.Controls.Add(this.nudMultiplicand);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.nudPeriod);
      this.Controls.Add(this.cbxStartStop);
      this.Controls.Add(this.pbxDiagram);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCMultiplyAdd";
      this.Size = new System.Drawing.Size(779, 613);
      ((System.ComponentModel.ISupportInitialize)(this.pbxDiagram)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMultiplier)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMultiplicand)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSummand)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.CheckBox cbxStartStop;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.PictureBox pbxDiagram;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudPeriod;
    private System.Windows.Forms.Label lblProductA;
    private System.Windows.Forms.NumericUpDown nudMultiplier;
    private System.Windows.Forms.NumericUpDown nudMultiplicand;
    private System.Windows.Forms.Label lblResult;
    private System.Windows.Forms.NumericUpDown nudSummand;
    private System.Windows.Forms.Label lblProductB;
  }
}
