﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Task;
using Hardware;
using Arithmetic;
using Process;
using Algorithm;
//
namespace UCAddition
{
  public partial class CUCAddition : UserControl
  { //
    //---------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------
    //
    private const String TEXT_START = "Start";
    private const String TEXT_STOP = "Stop";
    //
    //---------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------
    //
    private CClock FClock;
    private CProcess FProcess;
    private CAddition FSum;
    //
    //---------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------
    //
    public CUCAddition()
    {
      InitializeComponent();
      cbxStartStop.Text = TEXT_START;
    }
    //
    //---------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------
    //
    public Int32 SummandA
    {
      get { return (Int32)nudSummandA.Value; }
      set { nudSummandA.Value = (Decimal)value; }
    }

    public Int32 SummandB
    {
      get { return (Int32)nudSummandB.Value; }
      set { nudSummandB.Value = (Decimal)value; }
    }

    public Int32 Result
    {
      set { lblSum.Text = String.Format("{0}", value); }
    }

    public Int32 Period
    {
      get { return (Int32)nudPeriod.Value; }
      set { nudPeriod.Value = (Decimal)value; }
    }
    //
    //---------------------------------------------------------
    //  Segment - Callback
    //---------------------------------------------------------
    //
    private void OnClockSystemStart(RTaskData taskdata)
    {
    }

    private void OnClockSystemBusy(ref RTaskData taskdata)
    {
    }

    private void OnClockSystemEnd(RTaskData taskdata)
    {
    }

    private void OnClockSystemAbort(RTaskData taskdata)
    {
    }

    private delegate void CBSumOnCompleted(Int32 code);
    private void SumOnCompleted(Int32 code)
    {
      if (this.InvokeRequired)
      {
        CBSumOnCompleted CB = new CBSumOnCompleted(SumOnCompleted);
        Invoke(CB, new object[] { code });
      }
      else
      {
        switch (code)
        {
          case (int)EStateOperation.Error:
            Console.WriteLine("Error: On building Sum!");
            break;
          case (int)EStateOperation.Success:
            Result = FSum.Result;
            Console.WriteLine(String.Format("Sum({0}, {1}) = {2}",
                                            FSum.SummandA, FSum.SummandB, FSum.Result));
            break;
        }
      }
    }
    //
    //---------------------------------------------------------
    //  Segment - Event
    //---------------------------------------------------------
    //
    private void cbxStartStop_CheckedChanged(object sender, EventArgs e)
    {
      if (!(FClock is CClock))
      { // Start
        Start();
      }
      else
      { // Stop
        Stop();
      }
    }
    //
    //---------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------
    //
    public void Start()
    {
      FProcess = new CProcess();
      //
      FClock = new CClock("ClockSystem", Period, // [ms]
                          OnClockSystemStart,
                          OnClockSystemBusy,
                          OnClockSystemEnd,
                          OnClockSystemAbort);
      FClock.AddProcess(FProcess);
      //
      FSum = new CAddition(SummandA, SummandB, SumOnCompleted);
      FProcess.SetOperation(FSum);
      //
      if (FClock.Start())
      {
        cbxStartStop.Text = TEXT_STOP;
      }
    }

    public void Stop()
    {
      if (FClock is CClock)
      {
        if (FClock.Abort())
        {
          FClock = null;
          FProcess = null;
          cbxStartStop.Text = TEXT_START;
        }
      }
    }

  }
}
