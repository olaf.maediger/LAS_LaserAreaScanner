﻿namespace UCAddition
{
  partial class CUCAddition
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.cbxStartStop = new System.Windows.Forms.CheckBox();
      this.pbxDiagram = new System.Windows.Forms.PictureBox();
      this.nudSummandA = new System.Windows.Forms.NumericUpDown();
      this.nudSummandB = new System.Windows.Forms.NumericUpDown();
      this.lblSum = new System.Windows.Forms.Label();
      this.nudPeriod = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.pbxDiagram)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSummandA)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSummandB)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).BeginInit();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(773, 21);
      this.lblHeader.TabIndex = 0;
      this.lblHeader.Text = "Addition";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // cbxStartStop
      // 
      this.cbxStartStop.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxStartStop.Location = new System.Drawing.Point(604, 275);
      this.cbxStartStop.Name = "cbxStartStop";
      this.cbxStartStop.Size = new System.Drawing.Size(114, 38);
      this.cbxStartStop.TabIndex = 1;
      this.cbxStartStop.Text = "<startstop>";
      this.cbxStartStop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxStartStop.UseVisualStyleBackColor = true;
      this.cbxStartStop.CheckedChanged += new System.EventHandler(this.cbxStartStop_CheckedChanged);
      // 
      // pbxDiagram
      // 
      this.pbxDiagram.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxDiagram.Image = global::UCAddition.Properties.Resources.Addition03;
      this.pbxDiagram.Location = new System.Drawing.Point(0, 21);
      this.pbxDiagram.Name = "pbxDiagram";
      this.pbxDiagram.Size = new System.Drawing.Size(773, 585);
      this.pbxDiagram.TabIndex = 2;
      this.pbxDiagram.TabStop = false;
      // 
      // nudSummandA
      // 
      this.nudSummandA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudSummandA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudSummandA.Location = new System.Drawing.Point(63, 289);
      this.nudSummandA.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudSummandA.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudSummandA.Name = "nudSummandA";
      this.nudSummandA.Size = new System.Drawing.Size(113, 26);
      this.nudSummandA.TabIndex = 3;
      this.nudSummandA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSummandA.Value = new decimal(new int[] {
            123,
            0,
            0,
            0});
      // 
      // nudSummandB
      // 
      this.nudSummandB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudSummandB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudSummandB.Location = new System.Drawing.Point(252, 289);
      this.nudSummandB.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudSummandB.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudSummandB.Name = "nudSummandB";
      this.nudSummandB.Size = new System.Drawing.Size(113, 26);
      this.nudSummandB.TabIndex = 4;
      this.nudSummandB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSummandB.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
      // 
      // lblSum
      // 
      this.lblSum.BackColor = System.Drawing.SystemColors.Info;
      this.lblSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblSum.Location = new System.Drawing.Point(441, 290);
      this.lblSum.Name = "lblSum";
      this.lblSum.Size = new System.Drawing.Size(114, 23);
      this.lblSum.TabIndex = 5;
      this.lblSum.Text = "0";
      this.lblSum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPeriod
      // 
      this.nudPeriod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
      this.nudPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudPeriod.Location = new System.Drawing.Point(80, 179);
      this.nudPeriod.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
      this.nudPeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPeriod.Name = "nudPeriod";
      this.nudPeriod.Size = new System.Drawing.Size(75, 26);
      this.nudPeriod.TabIndex = 12;
      this.nudPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPeriod.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // CUCAddition
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.nudPeriod);
      this.Controls.Add(this.lblSum);
      this.Controls.Add(this.nudSummandB);
      this.Controls.Add(this.nudSummandA);
      this.Controls.Add(this.cbxStartStop);
      this.Controls.Add(this.pbxDiagram);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCAddition";
      this.Size = new System.Drawing.Size(773, 606);
      ((System.ComponentModel.ISupportInitialize)(this.pbxDiagram)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSummandA)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSummandB)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.CheckBox cbxStartStop;
    private System.Windows.Forms.PictureBox pbxDiagram;
    private System.Windows.Forms.NumericUpDown nudSummandA;
    private System.Windows.Forms.NumericUpDown nudSummandB;
    private System.Windows.Forms.Label lblSum;
    private System.Windows.Forms.NumericUpDown nudPeriod;
  }
}
