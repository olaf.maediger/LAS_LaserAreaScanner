﻿namespace UCDivision
{
  partial class CUCDivision
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.nudPeriod = new System.Windows.Forms.NumericUpDown();
      this.lblQuotient = new System.Windows.Forms.Label();
      this.nudDivisor = new System.Windows.Forms.NumericUpDown();
      this.nudDividend = new System.Windows.Forms.NumericUpDown();
      this.cbxStartStop = new System.Windows.Forms.CheckBox();
      this.lblHeader = new System.Windows.Forms.Label();
      this.lblRemainder = new System.Windows.Forms.Label();
      this.pbxDiagram = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDivisor)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDividend)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxDiagram)).BeginInit();
      this.SuspendLayout();
      // 
      // nudPeriod
      // 
      this.nudPeriod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
      this.nudPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudPeriod.Location = new System.Drawing.Point(80, 179);
      this.nudPeriod.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
      this.nudPeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPeriod.Name = "nudPeriod";
      this.nudPeriod.Size = new System.Drawing.Size(75, 26);
      this.nudPeriod.TabIndex = 21;
      this.nudPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPeriod.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // lblQuotient
      // 
      this.lblQuotient.BackColor = System.Drawing.SystemColors.Info;
      this.lblQuotient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblQuotient.Location = new System.Drawing.Point(441, 290);
      this.lblQuotient.Name = "lblQuotient";
      this.lblQuotient.Size = new System.Drawing.Size(114, 23);
      this.lblQuotient.TabIndex = 20;
      this.lblQuotient.Text = "0";
      this.lblQuotient.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudDivisor
      // 
      this.nudDivisor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudDivisor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudDivisor.Location = new System.Drawing.Point(252, 289);
      this.nudDivisor.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudDivisor.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudDivisor.Name = "nudDivisor";
      this.nudDivisor.Size = new System.Drawing.Size(113, 26);
      this.nudDivisor.TabIndex = 19;
      this.nudDivisor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDivisor.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
      // 
      // nudDividend
      // 
      this.nudDividend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudDividend.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudDividend.Location = new System.Drawing.Point(63, 289);
      this.nudDividend.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudDividend.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudDividend.Name = "nudDividend";
      this.nudDividend.Size = new System.Drawing.Size(113, 26);
      this.nudDividend.TabIndex = 18;
      this.nudDividend.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDividend.Value = new decimal(new int[] {
            123,
            0,
            0,
            0});
      // 
      // cbxStartStop
      // 
      this.cbxStartStop.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxStartStop.Location = new System.Drawing.Point(604, 275);
      this.cbxStartStop.Name = "cbxStartStop";
      this.cbxStartStop.Size = new System.Drawing.Size(114, 38);
      this.cbxStartStop.TabIndex = 16;
      this.cbxStartStop.Text = "<startstop>";
      this.cbxStartStop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxStartStop.UseVisualStyleBackColor = true;
      this.cbxStartStop.CheckedChanged += new System.EventHandler(this.cbxStartStop_CheckedChanged);
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(771, 21);
      this.lblHeader.TabIndex = 15;
      this.lblHeader.Text = "Division";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblRemainder
      // 
      this.lblRemainder.BackColor = System.Drawing.SystemColors.Info;
      this.lblRemainder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblRemainder.Location = new System.Drawing.Point(441, 365);
      this.lblRemainder.Name = "lblRemainder";
      this.lblRemainder.Size = new System.Drawing.Size(114, 23);
      this.lblRemainder.TabIndex = 24;
      this.lblRemainder.Text = "0";
      this.lblRemainder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pbxDiagram
      // 
      this.pbxDiagram.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxDiagram.Image = global::UCDivision.Properties.Resources.Division03;
      this.pbxDiagram.Location = new System.Drawing.Point(0, 21);
      this.pbxDiagram.Name = "pbxDiagram";
      this.pbxDiagram.Size = new System.Drawing.Size(771, 580);
      this.pbxDiagram.TabIndex = 17;
      this.pbxDiagram.TabStop = false;
      // 
      // CUCDivision
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lblRemainder);
      this.Controls.Add(this.nudPeriod);
      this.Controls.Add(this.lblQuotient);
      this.Controls.Add(this.nudDivisor);
      this.Controls.Add(this.nudDividend);
      this.Controls.Add(this.cbxStartStop);
      this.Controls.Add(this.pbxDiagram);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCDivision";
      this.Size = new System.Drawing.Size(771, 601);
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDivisor)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDividend)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxDiagram)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.NumericUpDown nudPeriod;
    private System.Windows.Forms.Label lblQuotient;
    private System.Windows.Forms.NumericUpDown nudDivisor;
    private System.Windows.Forms.NumericUpDown nudDividend;
    private System.Windows.Forms.CheckBox cbxStartStop;
    private System.Windows.Forms.PictureBox pbxDiagram;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Label lblRemainder;
  }
}
