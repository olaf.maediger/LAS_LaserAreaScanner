﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Task;
using Hardware;
using Arithmetic;
using Process;
using Algorithm;
//
namespace UCDivision
{
  public partial class CUCDivision : UserControl
  { //
    //---------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------
    //
    private const String TEXT_START = "Start";
    private const String TEXT_STOP = "Stop";
    //
    //---------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------
    //
    private CClock FClock;
    private CProcess FProcess;
    private CDivision FDivision;
    //
    //---------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------
    //
    public CUCDivision()
    {
      InitializeComponent();
      cbxStartStop.Text = TEXT_START;
    }
    //
    //---------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------
    //
    public Int32 Dividend
    {
      get { return (Int32)nudDividend.Value; }
      set { nudDividend.Value = (Decimal)value; }
    }

    public Int32 Divisor
    {
      get { return (Int32)nudDivisor.Value; }
      set { nudDivisor.Value = (Decimal)value; }
    }

    public Int32 Quotient
    {
      set { lblQuotient.Text = String.Format("{0}", value); }
    }

    public Int32 Remainder
    {
      set { lblRemainder.Text = String.Format("{0}", value); }
    }

    public Int32 Period
    {
      get { return (Int32)nudPeriod.Value; }
      set { nudPeriod.Value = (Decimal)value; }
    }
    //
    //---------------------------------------------------------
    //  Segment - Callback
    //---------------------------------------------------------
    //
    private void OnClockSystemStart(RTaskData taskdata)
    {
    }

    private void OnClockSystemBusy(ref RTaskData taskdata)
    {
    }

    private void OnClockSystemEnd(RTaskData taskdata)
    {
    }

    private void OnClockSystemAbort(RTaskData taskdata)
    {
    }

    private delegate void CBDivisionOnCompleted(Int32 code);
    private void DivisionOnCompleted(Int32 code)
    {
      if (this.InvokeRequired)
      {
        CBDivisionOnCompleted CB = new CBDivisionOnCompleted(DivisionOnCompleted);
        Invoke(CB, new object[] { code });
      }
      else
      {
        switch (code)
        {
          case (int)EStateOperation.Error:
            Console.WriteLine("Error: On building Division!");
            break;
          case (int)EStateOperation.Success:
            Quotient = FDivision.Quotient;
            Remainder = FDivision.Remainder;
            Console.WriteLine(String.Format("Division({0}, {1}) = {2} R {3}",
                                            FDivision.Dividend, FDivision.Divisor,
                                            FDivision.Quotient, FDivision.Remainder));
            break;
        }
      }
    }
    //
    //---------------------------------------------------------
    //  Segment - Event
    //---------------------------------------------------------
    //
    private void cbxStartStop_CheckedChanged(object sender, EventArgs e)
    {
      if (!(FClock is CClock))
      { // Start
        Start();
      }
      else
      { // Stop
        Stop();
      }
    }
    //
    //---------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------
    //
    public void Start()
    {
      FProcess = new CProcess();
      //
      FClock = new CClock("ClockSystem", Period, // [ms]
                          OnClockSystemStart,
                          OnClockSystemBusy,
                          OnClockSystemEnd,
                          OnClockSystemAbort);
      FClock.AddProcess(FProcess);
      //
      FDivision = new CDivision(Dividend, Divisor, DivisionOnCompleted);
      FProcess.SetOperation(FDivision);
      //
      if (FClock.Start())
      {
        cbxStartStop.Text = TEXT_STOP;
      }
    }

    public void Stop()
    {
      if (FClock is CClock)
      {
        if (FClock.Abort())
        {
          FClock = null;
          FProcess = null;
          cbxStartStop.Text = TEXT_START;
        }
      }
    }

  }
}
