﻿namespace UCMultiplication
{
  partial class CUCMultiplication
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxStartStop = new System.Windows.Forms.CheckBox();
      this.lblHeader = new System.Windows.Forms.Label();
      this.pbxDiagram = new System.Windows.Forms.PictureBox();
      this.lblProduct = new System.Windows.Forms.Label();
      this.nudMultiplier = new System.Windows.Forms.NumericUpDown();
      this.nudMultiplicand = new System.Windows.Forms.NumericUpDown();
      this.nudPeriod = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.pbxDiagram)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMultiplier)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMultiplicand)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).BeginInit();
      this.SuspendLayout();
      // 
      // cbxStartStop
      // 
      this.cbxStartStop.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxStartStop.Location = new System.Drawing.Point(604, 275);
      this.cbxStartStop.Name = "cbxStartStop";
      this.cbxStartStop.Size = new System.Drawing.Size(113, 36);
      this.cbxStartStop.TabIndex = 1;
      this.cbxStartStop.Text = "<startstop>";
      this.cbxStartStop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxStartStop.UseVisualStyleBackColor = true;
      this.cbxStartStop.CheckedChanged += new System.EventHandler(this.cbxStartStop_CheckedChanged);
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(770, 21);
      this.lblHeader.TabIndex = 2;
      this.lblHeader.Text = "Multiplication";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pbxDiagram
      // 
      this.pbxDiagram.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxDiagram.Image = global::UCMultiplication.Properties.Resources.Multiplication03;
      this.pbxDiagram.Location = new System.Drawing.Point(0, 21);
      this.pbxDiagram.Name = "pbxDiagram";
      this.pbxDiagram.Size = new System.Drawing.Size(770, 591);
      this.pbxDiagram.TabIndex = 3;
      this.pbxDiagram.TabStop = false;
      // 
      // lblProduct
      // 
      this.lblProduct.BackColor = System.Drawing.SystemColors.Info;
      this.lblProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblProduct.Location = new System.Drawing.Point(441, 290);
      this.lblProduct.Name = "lblProduct";
      this.lblProduct.Size = new System.Drawing.Size(112, 23);
      this.lblProduct.TabIndex = 8;
      this.lblProduct.Text = "0";
      this.lblProduct.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudMultiplier
      // 
      this.nudMultiplier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudMultiplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudMultiplier.Location = new System.Drawing.Point(252, 289);
      this.nudMultiplier.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudMultiplier.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudMultiplier.Name = "nudMultiplier";
      this.nudMultiplier.Size = new System.Drawing.Size(113, 26);
      this.nudMultiplier.TabIndex = 7;
      this.nudMultiplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMultiplier.Value = new decimal(new int[] {
            3,
            0,
            0,
            -2147483648});
      // 
      // nudMultiplicand
      // 
      this.nudMultiplicand.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudMultiplicand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudMultiplicand.Location = new System.Drawing.Point(63, 289);
      this.nudMultiplicand.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudMultiplicand.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudMultiplicand.Name = "nudMultiplicand";
      this.nudMultiplicand.Size = new System.Drawing.Size(113, 26);
      this.nudMultiplicand.TabIndex = 6;
      this.nudMultiplicand.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMultiplicand.Value = new decimal(new int[] {
            123,
            0,
            0,
            0});
      // 
      // nudPeriod
      // 
      this.nudPeriod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
      this.nudPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudPeriod.Location = new System.Drawing.Point(80, 179);
      this.nudPeriod.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
      this.nudPeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPeriod.Name = "nudPeriod";
      this.nudPeriod.Size = new System.Drawing.Size(75, 26);
      this.nudPeriod.TabIndex = 9;
      this.nudPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPeriod.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // CUCMultiplication
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.nudPeriod);
      this.Controls.Add(this.lblProduct);
      this.Controls.Add(this.nudMultiplier);
      this.Controls.Add(this.nudMultiplicand);
      this.Controls.Add(this.cbxStartStop);
      this.Controls.Add(this.pbxDiagram);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCMultiplication";
      this.Size = new System.Drawing.Size(770, 612);
      ((System.ComponentModel.ISupportInitialize)(this.pbxDiagram)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMultiplier)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMultiplicand)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.CheckBox cbxStartStop;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.PictureBox pbxDiagram;
    private System.Windows.Forms.Label lblProduct;
    private System.Windows.Forms.NumericUpDown nudMultiplier;
    private System.Windows.Forms.NumericUpDown nudMultiplicand;
    private System.Windows.Forms.NumericUpDown nudPeriod;
  }
}
