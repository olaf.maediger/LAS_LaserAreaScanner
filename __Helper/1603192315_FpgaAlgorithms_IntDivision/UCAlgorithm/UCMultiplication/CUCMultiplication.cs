﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Task;
using Hardware;
using Arithmetic;
using Process;
using Algorithm;
//
namespace UCMultiplication
{
  public partial class CUCMultiplication : UserControl
  { //
    //---------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------
    //
    private const String TEXT_START = "Start";
    private const String TEXT_STOP = "Stop";
    //
    //---------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------
    //
    private CClock FClock;
    private CProcess FProcess;
    private CMultiplication FProduct;
    //
    //---------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------
    //
    public CUCMultiplication()
    {
      InitializeComponent();
      cbxStartStop.Text = TEXT_START;
    }
    //
    //---------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------
    //
    public Int32 Multiplicand
    {
      get { return (Int32)nudMultiplicand.Value; }
      set { nudMultiplicand.Value = (Decimal)value; }
    }

    public Int32 Multiplier
    {
      get { return (Int32)nudMultiplier.Value; }
      set { nudMultiplier.Value = (Decimal)value; }
    }

    public Int32 Result
    {
      set { lblProduct.Text = String.Format("{0}", value); }
    }

    public Int32 Period
    {
      get { return (Int32)nudPeriod.Value; }
      set { nudPeriod.Value = (Decimal)value; }
    }
    //
    //---------------------------------------------------------
    //  Segment - Callback
    //---------------------------------------------------------
    //
    private void OnClockSystemStart(RTaskData taskdata)
    {
    }

    private void OnClockSystemBusy(ref RTaskData taskdata)
    {
    }

    private void OnClockSystemEnd(RTaskData taskdata)
    {
    }

    private void OnClockSystemAbort(RTaskData taskdata)
    {
    }

    private delegate void CBProductOnCompleted(Int32 code);
    private void ProductOnCompleted(Int32 code)
    {
      if (this.InvokeRequired)
      {
        CBProductOnCompleted CB = new CBProductOnCompleted(ProductOnCompleted);
        Invoke(CB, new object[] { code });
      }
      else
      {
        switch (code)
        {
          case (int)EStateOperation.Error:
            Console.WriteLine("Error: On building Product!");
            break;
          case (int)EStateOperation.Success:
            Result = FProduct.Result;
            Console.WriteLine(String.Format("Product({0}, {1}) = {2}",
                                FProduct.Multiplicand, FProduct.Multiplier, FProduct.Result));
            break;
        }
      }
    }
    //
    //---------------------------------------------------------
    //  Segment - Event
    //---------------------------------------------------------
    //
    private void cbxStartStop_CheckedChanged(object sender, EventArgs e)
    {
      if (!(FClock is CClock))
      { // Start
        Start();
      }
      else
      { // Stop
        Stop();
      }
    }
    //
    //---------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------
    //
    public void Start()
    {
      FProcess = new CProcess();
      //
      FClock = new CClock("ClockSystem", Period, // [ms]
                          OnClockSystemStart,
                          OnClockSystemBusy,
                          OnClockSystemEnd,
                          OnClockSystemAbort);
      FClock.AddProcess(FProcess);
      //
      FProduct = new CMultiplication(Multiplicand, Multiplier, ProductOnCompleted);
      FProcess.SetOperation(FProduct);
      //
      if (FClock.Start())
      {
        cbxStartStop.Text = TEXT_STOP;
      }
    }

    public void Stop()
    {
      if (FClock is CClock)
      {
        if (FClock.Abort())
        {
          FClock = null;
          FProcess = null;
          cbxStartStop.Text = TEXT_START;
        }
      }
    }

  }
}

