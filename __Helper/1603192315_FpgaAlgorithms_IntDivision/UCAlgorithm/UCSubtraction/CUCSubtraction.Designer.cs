﻿namespace UCSubtraction
{
  partial class CUCSubtraction
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxStartStop = new System.Windows.Forms.CheckBox();
      this.lblHeader = new System.Windows.Forms.Label();
      this.pbxDiagram = new System.Windows.Forms.PictureBox();
      this.lblDifference = new System.Windows.Forms.Label();
      this.nudSubtrahend = new System.Windows.Forms.NumericUpDown();
      this.nudMinuend = new System.Windows.Forms.NumericUpDown();
      this.nudPeriod = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.pbxDiagram)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSubtrahend)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMinuend)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).BeginInit();
      this.SuspendLayout();
      // 
      // cbxStartStop
      // 
      this.cbxStartStop.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxStartStop.Location = new System.Drawing.Point(604, 275);
      this.cbxStartStop.Name = "cbxStartStop";
      this.cbxStartStop.Size = new System.Drawing.Size(114, 38);
      this.cbxStartStop.TabIndex = 1;
      this.cbxStartStop.Text = "<startstop>";
      this.cbxStartStop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxStartStop.UseVisualStyleBackColor = true;
      this.cbxStartStop.CheckedChanged += new System.EventHandler(this.cbxStartStop_CheckedChanged);
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(772, 21);
      this.lblHeader.TabIndex = 2;
      this.lblHeader.Text = "Subtraction";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pbxDiagram
      // 
      this.pbxDiagram.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxDiagram.Image = global::UCSubtraction.Properties.Resources.Subtraction03;
      this.pbxDiagram.Location = new System.Drawing.Point(0, 21);
      this.pbxDiagram.Name = "pbxDiagram";
      this.pbxDiagram.Size = new System.Drawing.Size(772, 582);
      this.pbxDiagram.TabIndex = 4;
      this.pbxDiagram.TabStop = false;
      // 
      // lblDifference
      // 
      this.lblDifference.BackColor = System.Drawing.SystemColors.Info;
      this.lblDifference.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDifference.Location = new System.Drawing.Point(441, 290);
      this.lblDifference.Name = "lblDifference";
      this.lblDifference.Size = new System.Drawing.Size(114, 23);
      this.lblDifference.TabIndex = 8;
      this.lblDifference.Text = "0";
      this.lblDifference.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSubtrahend
      // 
      this.nudSubtrahend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudSubtrahend.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudSubtrahend.Location = new System.Drawing.Point(252, 289);
      this.nudSubtrahend.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudSubtrahend.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudSubtrahend.Name = "nudSubtrahend";
      this.nudSubtrahend.Size = new System.Drawing.Size(113, 26);
      this.nudSubtrahend.TabIndex = 7;
      this.nudSubtrahend.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSubtrahend.Value = new decimal(new int[] {
            45,
            0,
            0,
            0});
      // 
      // nudMinuend
      // 
      this.nudMinuend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(255)))), ((int)(((byte)(222)))));
      this.nudMinuend.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudMinuend.Location = new System.Drawing.Point(63, 289);
      this.nudMinuend.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
      this.nudMinuend.Minimum = new decimal(new int[] {
            2000000,
            0,
            0,
            -2147483648});
      this.nudMinuend.Name = "nudMinuend";
      this.nudMinuend.Size = new System.Drawing.Size(113, 26);
      this.nudMinuend.TabIndex = 6;
      this.nudMinuend.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMinuend.Value = new decimal(new int[] {
            123,
            0,
            0,
            0});
      // 
      // nudPeriod
      // 
      this.nudPeriod.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(222)))), ((int)(((byte)(222)))));
      this.nudPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.nudPeriod.Location = new System.Drawing.Point(80, 179);
      this.nudPeriod.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
      this.nudPeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPeriod.Name = "nudPeriod";
      this.nudPeriod.Size = new System.Drawing.Size(75, 26);
      this.nudPeriod.TabIndex = 12;
      this.nudPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPeriod.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // CUCSubtraction
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.nudPeriod);
      this.Controls.Add(this.lblDifference);
      this.Controls.Add(this.nudSubtrahend);
      this.Controls.Add(this.nudMinuend);
      this.Controls.Add(this.cbxStartStop);
      this.Controls.Add(this.pbxDiagram);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCSubtraction";
      this.Size = new System.Drawing.Size(772, 603);
      ((System.ComponentModel.ISupportInitialize)(this.pbxDiagram)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSubtrahend)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMinuend)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.CheckBox cbxStartStop;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.PictureBox pbxDiagram;
    private System.Windows.Forms.Label lblDifference;
    private System.Windows.Forms.NumericUpDown nudSubtrahend;
    private System.Windows.Forms.NumericUpDown nudMinuend;
    private System.Windows.Forms.NumericUpDown nudPeriod;
  }
}
