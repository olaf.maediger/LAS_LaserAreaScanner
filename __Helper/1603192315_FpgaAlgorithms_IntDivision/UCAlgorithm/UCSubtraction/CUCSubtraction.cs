﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Task;
using Hardware;
using Arithmetic;
using Process;
using Algorithm;
//
namespace UCSubtraction
{
  public partial class CUCSubtraction : UserControl
  { //
    //---------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------
    //
    private const String TEXT_START = "Start";
    private const String TEXT_STOP = "Stop";
    //
    //---------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------
    //
    private CClock FClock;
    private CProcess FProcess;
    private CSubtraction FDifference;
    //
    //---------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------
    //
    public CUCSubtraction()
    {
      InitializeComponent();
      cbxStartStop.Text = TEXT_START;
    }
    //
    //---------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------
    //
    public Int32 Minuend
    {
      get { return (Int32)nudMinuend.Value; }
      set { nudMinuend.Value = (Decimal)value; }
    }

    public Int32 Subtrahend
    {
      get { return (Int32)nudSubtrahend.Value; }
      set { nudSubtrahend.Value = (Decimal)value; }
    }

    public Int32 Result
    {
      set { lblDifference.Text = String.Format("{0}", value); }
    }

    public Int32 Period
    {
      get { return (Int32)nudPeriod.Value; }
      set { nudPeriod.Value = (Decimal)value; }
    }
    //
    //---------------------------------------------------------
    //  Segment - Callback
    //---------------------------------------------------------
    //
    private void OnClockSystemStart(RTaskData taskdata)
    {
    }

    private void OnClockSystemBusy(ref RTaskData taskdata)
    {
    }

    private void OnClockSystemEnd(RTaskData taskdata)
    {
    }

    private void OnClockSystemAbort(RTaskData taskdata)
    {
    }

    private delegate void CBDifferenceOnCompleted(Int32 code);
    private void DifferenceOnCompleted(Int32 code)
    {
      if (this.InvokeRequired)
      {
        CBDifferenceOnCompleted CB = new CBDifferenceOnCompleted(DifferenceOnCompleted);
        Invoke(CB, new object[] { code });
      }
      else
      {
        switch (code)
        {
          case (int)EStateOperation.Error:
            Console.WriteLine("Error: On building Difference!");
            break;
          case (int)EStateOperation.Success:
            Result = FDifference.Result;
            Console.WriteLine(String.Format("Difference({0}, {1}) = {2}",
              FDifference.Minuend, FDifference.Subtrahend, FDifference.Result));
            break;
        }
      }
    }
    //
    //---------------------------------------------------------
    //  Segment - Event
    //---------------------------------------------------------
    //
    private void cbxStartStop_CheckedChanged(object sender, EventArgs e)
    {
      if (!(FClock is CClock))
      { // Start
        Start();
      }
      else
      { // Stop
        Stop();
      }
    }
    //
    //---------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------
    //
    public void Start()
    {
      FProcess = new CProcess();
      //
      FClock = new CClock("ClockSystem", Period, // [ms]
                          OnClockSystemStart,
                          OnClockSystemBusy,
                          OnClockSystemEnd,
                          OnClockSystemAbort);
      FClock.AddProcess(FProcess);
      //
      FDifference = new CSubtraction(Minuend, Subtrahend, DifferenceOnCompleted);
      FProcess.SetOperation(FDifference);
      //
      if (FClock.Start())
      {
        cbxStartStop.Text = TEXT_STOP;
      }
    }

    public void Stop()
    {
      if (FClock is CClock)
      {
        if (FClock.Abort())
        {
          FClock = null;
          FProcess = null;
          cbxStartStop.Text = TEXT_START;
        }
      }
    }

  }
}

