﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using Task;
using Process;
//
namespace Hardware
{
  public class CClock
  {
    private String FName;
    private Int32 FPeriod;
    private DOnExecutionStart FOnExecutionStart;
    private DOnExecutionBusy FOnExecutionBusy;
    private DOnExecutionEnd FOnExecutionEnd;
    private DOnExecutionAbort FOnExecutionAbort;
    private CTask FTask;
    private CProcessList FProcessList;
    // -> TaskData private Int32 FClockCount;

    public CClock(String name,
                  Int32 period,  // [ms]
                  DOnExecutionStart onexecutionstart,
                  DOnExecutionBusy onexecutionbusy,
                  DOnExecutionEnd onexecutionend,
                  DOnExecutionAbort onexecutionabort)
    {
      FName = name;
      FPeriod = period;
      FOnExecutionStart = onexecutionstart;
      FOnExecutionBusy = onexecutionbusy;
      FOnExecutionEnd = onexecutionend;
      FOnExecutionAbort = onexecutionabort;
      FTask = null;
      FProcessList = new CProcessList();
    }


    public Boolean IsActive
    {
      get { return (FTask is CTask); }
    }




    private void OnExecutionStart(RTaskData taskdata)
    {
      if (FOnExecutionStart is DOnExecutionStart)
      {
        FOnExecutionStart(taskdata);
      }
    }

    private void OnExecutionBusy(ref RTaskData taskdata)
    {
      do
      {
        if (FOnExecutionBusy is DOnExecutionBusy)
        {
          FOnExecutionBusy(ref taskdata);
        }
        Thread.Sleep(FPeriod);
        foreach (CProcess Process in FProcessList)
        {
          Process.Clock(taskdata.Counter);
        }
        // Loop-Correction:
        taskdata.Counter++;
      }
      while (FTask.IsActive());
    }

    private void OnExecutionEnd(RTaskData taskdata)
    {
      if (FOnExecutionEnd is DOnExecutionEnd)
      {
        FOnExecutionEnd(taskdata);
      }
    }

    private void OnExecutionAbort(RTaskData taskdata)
    {
      if (FOnExecutionAbort is DOnExecutionAbort)
      {
        FOnExecutionAbort(taskdata);
      }
    }


    public void AddProcess(CProcess process)
    {
      FProcessList.Add(process);
    }

    public Boolean Start()
    {
      FTask = new CTask(FName,
                        OnExecutionStart,
                        OnExecutionBusy,
                        OnExecutionEnd,
                        OnExecutionAbort);
      FTask.Start();
      return IsActive;
    }

    public Boolean Abort()
    {
      if (IsActive)
      {
        FTask.Abort();
        FTask = null;
        return true;
      }
      return false;
    }




  }
}
