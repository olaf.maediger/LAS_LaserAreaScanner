﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Arithmetic;
//
namespace Process
{
  public class CProcess // Error: List<COperation>
  {
    private COperation FOperation;

    public CProcess()
    {
      FOperation = null;
    }

    public void SetOperation(COperation operation)
    {
      FOperation = operation;
    }

    public Boolean Clock(Int32 clockcount)
    {
      Boolean Result = true;
      if (FOperation is COperation)
      {
        Result &= FOperation.Clock(clockcount);
      }
      return Result;
    }

    //public Boolean Clock(Int32 clockcount)
    //{
    //  Boolean Result = true;
    //  foreach (COperation Operation in this)
    //  {
    //    Result &= Operation.Clock(clockcount);
    //  }
    //  return Result;
    //}

  }
}
