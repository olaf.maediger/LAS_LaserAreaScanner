﻿namespace UCMachining
{
  partial class CUCMachiningCutter
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlUser = new System.Windows.Forms.Panel();
      this.btnStartCutter = new System.Windows.Forms.Button();
      this.pnlUser.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlUser
      // 
      this.pnlUser.BackColor = System.Drawing.SystemColors.Control;
      this.pnlUser.Controls.Add(this.btnStartCutter);
      this.pnlUser.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlUser.Location = new System.Drawing.Point(0, 256);
      this.pnlUser.Name = "pnlUser";
      this.pnlUser.Size = new System.Drawing.Size(299, 34);
      this.pnlUser.TabIndex = 6;
      // 
      // btnStartCutter
      // 
      this.btnStartCutter.Location = new System.Drawing.Point(5, 6);
      this.btnStartCutter.Name = "btnStartCutter";
      this.btnStartCutter.Size = new System.Drawing.Size(75, 23);
      this.btnStartCutter.TabIndex = 5;
      this.btnStartCutter.Text = "StartCutter";
      this.btnStartCutter.UseVisualStyleBackColor = true;
      // 
      // CUCMachiningCutter
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.pnlUser);
      this.Name = "CUCMachiningCutter";
      this.Size = new System.Drawing.Size(299, 290);
      this.SizeChanged += new System.EventHandler(this.this_SizeChanged);
      this.pnlUser.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlUser;
    private System.Windows.Forms.Button btnStartCutter;
  }
}
