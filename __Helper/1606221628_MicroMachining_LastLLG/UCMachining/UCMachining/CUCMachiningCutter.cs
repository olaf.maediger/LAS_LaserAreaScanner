﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using Machining;
using UCMachining;
//
namespace UCMachining
{
  public partial class CUCMachiningCutter : UserControl
  {
    //
    //-----------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CUCMachining FUCMachining;
    //
    //-----------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------
    //
    public CUCMachiningCutter()
      : base()
    {
      InitializeComponent();
      FUCMachining = new CUCMachining();
      FUCMachining.Dock = DockStyle.None;
      this.Controls.Add(FUCMachining);
    }
    //
    //-----------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FUCMachining.SetNotifier(value);
    }
    //
    //-----------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------
    //
    private void this_SizeChanged(object sender, EventArgs e)
    {
      this.SuspendLayout();
      FUCMachining.Left = ClientRectangle.Left;
      FUCMachining.Top = ClientRectangle.Top;
      FUCMachining.Width = ClientRectangle.Width;
      FUCMachining.Height = ClientRectangle.Height - pnlUser.Height;
      this.ResumeLayout();
    }
    //
    //-----------------------------------------------------------
    //  Section - Callback
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Public Method
    //-----------------------------------------------------------
    //
  }
}
