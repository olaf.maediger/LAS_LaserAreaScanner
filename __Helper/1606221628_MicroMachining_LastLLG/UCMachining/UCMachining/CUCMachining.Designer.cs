﻿namespace UCMachining
{
  partial class CUCMachining
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pbxView = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pbxView)).BeginInit();
      this.SuspendLayout();
      // 
      // pbxView
      // 
      this.pbxView.BackColor = System.Drawing.Color.White;
      this.pbxView.Location = new System.Drawing.Point(14, 12);
      this.pbxView.Name = "pbxView";
      this.pbxView.Size = new System.Drawing.Size(420, 259);
      this.pbxView.TabIndex = 5;
      this.pbxView.TabStop = false;
      // 
      // CUCMachining
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.Controls.Add(this.pbxView);
      this.Name = "CUCMachining";
      this.Size = new System.Drawing.Size(456, 291);
      this.SizeChanged += new System.EventHandler(this.CUCMachining_SizeChanged);
      ((System.ComponentModel.ISupportInitialize)(this.pbxView)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox pbxView;

  }
}
