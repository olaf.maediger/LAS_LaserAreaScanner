﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using Machining;
//
namespace UCMachining
{
  public partial class CUCMachining : UserControl
  {
    //
    //-----------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CMachining FMachining;
    //
    //-----------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------
    //
    public CUCMachining()
    {
      InitializeComponent();
      pbxView.Dock = DockStyle.Fill;
      //
      FMachining = new CMachining();
    }
    //
    //-----------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }
    //
    //-----------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------
    //
    private void CUCMachining_SizeChanged(object sender, EventArgs e)
    {
      FMachining.SetDimensionPixel(ClientRectangle.Width, ClientRectangle.Height);
    }
    //
    //-----------------------------------------------------------
    //  Section - Callback
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Public Method
    //-----------------------------------------------------------
    //

  }
}
