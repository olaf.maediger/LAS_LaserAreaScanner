﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machining
{
  public struct RTransformationData
  {
    public const String INIT_UNIT_PIXEL = "pxl";
    public const Int32 INIT_X0PIXEL = 0;
    public const Int32 INIT_Y0PIXEL = 0;
    public const Int32 INIT_X1PIXEL = 100;
    public const Int32 INIT_Y1PIXEL = 100;
    public const String INIT_UNIT_REAL = "mm";
    public const Double INIT_X0REAL = 0.0;
    public const Double INIT_Y0REAL = 0.0;
    public const Double INIT_X1REAL = 100.0;
    public const Double INIT_Y1REAL = 100.0;
    //
    public String UnitPixel;
    public Int32 X0Pixel, Y0Pixel;
    public Int32 X1Pixel, Y1Pixel;
    public String UnitReal;
    public Double X0Real, Y0Real;
    public Double X1Real, Y1Real;
    //
    public RTransformationData(Int32 init)
    {
      UnitPixel = INIT_UNIT_PIXEL;
      X0Pixel = INIT_X0PIXEL;
      Y0Pixel = INIT_Y0PIXEL;
      X1Pixel = INIT_X1PIXEL;
      Y1Pixel = INIT_Y1PIXEL;
      UnitReal = INIT_UNIT_REAL;
      X0Real = INIT_X0REAL;
      Y0Real = INIT_Y0REAL;
      X1Real = INIT_X1REAL;
      Y1Real = INIT_Y1REAL;
    }

    public static Double XRealPixel(Double real)
    {
      return 4.0;
    }

    //public static Double YRealPixel(Double real)
    //{
    //}

    //public static Double XPixelReal(Int32 real)
    //{
    //}

    //public static Double YPixelReal(Int32 real)
    //{
    //}

  }
}
