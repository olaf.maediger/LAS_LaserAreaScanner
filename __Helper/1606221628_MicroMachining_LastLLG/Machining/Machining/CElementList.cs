﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace Machining
{
  public class CElementList : List<CElement>
  {

    public Boolean Draw(Graphics graphics)
    {
      Boolean Result = true;
      foreach (CElement Element in this)
      {
        Result &= Element.Draw(graphics);
      }
      return Result;
    }

  }
}
