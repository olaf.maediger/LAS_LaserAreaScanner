﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machining
{
  public class CMachining
  {
    private RMachiningData FData;
    private CTransformation FTransformation;

    public CMachining()
    {
      FData = new RMachiningData(0);
      FTransformation = new CTransformation();
    }

    public void SetDimensionPixel(Int32 pixelwidth, Int32 pixelheight)
    {
      FTransformation.SetDimensionPixel(pixelwidth, pixelheight);
      //RTransformationData TD;
      //FTransformation.GetData(out TD);
      //TD.X1Pixel = pixelwidth - 1;
      //TD.Y1Pixel = pixelheight - 1;
      //FTransformation.SetData(TD);
    }
  }
}
