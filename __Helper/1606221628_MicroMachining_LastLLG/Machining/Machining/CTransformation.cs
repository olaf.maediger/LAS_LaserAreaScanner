﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Machining
{
  public class CTransformation
  {
    private RTransformationData FData;

    public CTransformation()
    {
      FData = new RTransformationData(0);
    }

    public Int32 XRealPixel(Double real)
    {
      return (Int32)(0.5 + (FData.X1Pixel - FData.X0Pixel) * (real - FData.X0Real) / (FData.X1Real - FData.X0Real));
    }

    public Int32 YRealPixel(Double real)
    {
      return (Int32)(0.5 + (FData.Y1Pixel - FData.Y0Pixel) * (real - FData.Y0Real) / (FData.Y1Real - FData.Y0Real));
    }

    public Double XPixelReal(Int32 pixel)
    {
      return (FData.X1Real - FData.X0Real) * (pixel - -FData.X0Pixel) / (FData.X1Pixel - FData.X0Pixel);
    }

    public Double YPixelReal(Int32 pixel)
    {
      return (FData.Y1Real - FData.Y0Real) * (pixel - -FData.Y0Pixel) / (FData.Y1Pixel - FData.Y0Pixel);
    }

    public void SetDimensionPixel(Int32 pixelwidth, Int32 pixelheight)
    {
      FData.X1Pixel = pixelwidth - 1;
      FData.Y1Pixel = pixelheight - 1;
    }

    public void SetDimensionXReal(Double x0real, Double x1real)
    {
      FData.X0Real = x0real;
      FData.X1Real = x1real;
    }

    public void SetDimensionYReal(Double y0real, Double y1real)
    {
      FData.Y0Real = y0real;
      FData.Y1Real = y1real;
    }

  }
}
