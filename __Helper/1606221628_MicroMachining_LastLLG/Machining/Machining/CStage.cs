﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace Machining
{
  public class CStage : CElement
  {
    private Rectangle FRectangleArea;
    private Pen FPenBorder;
    private Brush FBrushArea;

    public CStage()
    {
      FRectangleArea = new Rectangle(10, 10, 100, 100);
      FPenBorder = new Pen(Color.Black, 3);
      FBrushArea = new SolidBrush(Color.Blue);
    }

    public override Boolean Draw(Graphics graphics)
    {
      graphics.FillRectangle(FBrushArea, FRectangleArea);
      graphics.DrawRectangle(FPenBorder, FRectangleArea);
      return true;
    }
  }
}
