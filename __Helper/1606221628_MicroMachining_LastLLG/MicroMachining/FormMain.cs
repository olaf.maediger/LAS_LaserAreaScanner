﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
using TextFile;
using UCMachining;
//
namespace MicroMachining
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = false;//true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private CUCMachiningDriller FUCMachiningDriller;
    private CUCMachiningLaser FUCMachiningLaser;
    private CUCMachiningCutter FUCMachiningCutter;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FUCMachiningDriller = new CUCMachiningDriller();
      tbpMachiningDriller.Controls.Add(FUCMachiningDriller);
      FUCMachiningDriller.SetNotifier(FUCNotifier);
      FUCMachiningDriller.Dock = DockStyle.Fill;
      //
      FUCMachiningLaser = new CUCMachiningLaser();
      tbpMachiningLaser.Controls.Add(FUCMachiningLaser);
      FUCMachiningLaser.SetNotifier(FUCNotifier);
      FUCMachiningLaser.Dock = DockStyle.Fill;
      ////
      FUCMachiningCutter = new CUCMachiningCutter();
      tbpMachiningCutter.Controls.Add(FUCMachiningCutter);
      FUCMachiningCutter.SetNotifier(FUCNotifier);
      FUCMachiningCutter.Dock = DockStyle.Fill;
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
    }

    private void FreeProgrammerControls()
    {
      //FUCElevator.Stop();
      //FUserList.Stop();
      //
      tmrStartup.Stop();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      //Result &= FUCProfile.LoadInitdata(initdata, "");
      ////Result &= FUCImageQuad.LoadInitdata(initdata, "");
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      ////
      //Result &= FUCProfile.SaveInitdata(initdata, "");
      ////Result &= FUCImageQuad.SaveInitdata(initdata, "");
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback 
    //------------------------------------------------------------------------
    //    

    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          //CGCode GCode = new CGCode(FUCNotifier);
          //GCode.ReadFromFile("C.gcode");
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          //Application.Exit();
          return false;
        //case 12:
        //  return true;
        //case 13:
        //  return true;
        //case 14:
        //  return true;
        //case 15:
        //  return true;
        //case 16:
        //  return true;
        //case 17:
        //  return true;
        //case 18:
        //  return true; 
        default:
          cbxAutomate.Checked = false;
          //FUCElevator.Stop();
          //FUCElevator.InfoUserListSorted();
          // NC FUCNotifier.Write("-");
          return false;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {

    }


    private void tbcMain_SelectedIndexChanged(object sender, EventArgs e)
    {
      // debug Console.WriteLine(String.Format("TIC!!! {0}", tbcMain.SelectedIndex));
      //switch (tbcMain.SelectedIndex)
      //{
      //  case 0:
      //    break;
      //  case 1:
      //    //FUCGCodeFlow.SetGCodeText(FUCGCodeEditor.GetGCodeText());
      //    break;
      //  case 2:
      //    break;
      //  case 3:
      //    break;
      //  case 4:
      //    break;
      //}
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //     

    //
    //------------------------------------------------------------------------
    //  Section - Action
    //------------------------------------------------------------------------
    //


  }
}
