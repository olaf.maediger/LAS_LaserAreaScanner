﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
//
using UCNotifier;
using Initdata;
using HWComPort;
//
using XmlFile;
using NLExpression;
using NLStatement;
using NLProgram;
//????using NLStatementComPort;
//
namespace IFTerminalRS232
{
  /*
  public enum ESweepState
  {
    Idle = (int)0,
    LowHighLowHigh = (int)1,
    HighLowHighLow = (int)2,
    LowHighHighLow = (int)3,
    HighLowLowHigh = (int)4
  };

  public enum ESweepParameter
  {
    LimitLow = (int)0,
    LimitHigh = (int)1,
    StepDelta = (int)2,
    StepCount = (int)3,
    StepIndex = (int)4,
    RepetitionCount = (int)5,
    RepetitionIndex = (int)6
  };*/


  //
  //#######################################################################################
  //  Segment - Main - CIFTerminalRS232Base
  //#######################################################################################
  //
  public class CIFTerminalRS232Base 
  {
    //
    //--------------------------------------
    //	Section - Constant
    //--------------------------------------
    //
    static public String HEADER_LIBRARY = "MHFDL";
    static public String TITLE_LIBRARY = "MHFastDelayLine";
    static public String SECTION_LIBRARY = TITLE_LIBRARY;
    //
    // Initdata - HWComPort
    public const String SECTION_COMPORT = "ComPort";
    private const String NAME_COMPORT = "ComPort";
    // Initdata - InitValue - HWComPort
    public const EComPort INIT_COMPORT = EComPort.cp1;						// selectable over Initdata
    private const EBaudrate INIT_BAUDRATE = EBaudrate.br115200;		// fixed to Hardware
    private const EDatabits INIT_DATABITS = EDatabits.db8;				// fixed to Hardware
    private const EParity INIT_PARITY = EParity.paNone;						// fixed to Hardware
    private const EStopbits INIT_STOPBITS = EStopbits.sb1;				// fixed to Hardware
    private const Int32 INIT_BUFFERSIZE = 4096;										// fixed to Hardware
    private const Boolean INIT_TRANSMITENABLED = true;						// fixed to Hardware
    private const Int32 INIT_DELAYCARRRIAGERETURN = 0;						// fixed to Hardware
    private const Int32 INIT_DELAYLINEFEED = 0;										// fixed to Hardware
    private const Int32 INIT_DELAYCHARACTER = 0;									// fixed to Hardware
    //
    // Initdata - Common
    public const String SECTION_COMMON = "Common";
    private const String NAME_DEVICETYPE = "DeviceType";
    private const String NAME_SOFTWAREVERSION = "SoftwareVersion";
    private const String NAME_HARDWAREVERSION = "HardwareVersion";
    // Initdata - InitValue - Common
    public const String INIT_DEVICETYPE = "MHFastDelayLine";
    public const String INIT_SOFTWAREVERSION = "01V0103";
    public const String INIT_HARDWAREVERSION = "FDL02V00";
    //
    // Initdata - DelayChannelA
    public const String SECTION_CHANNELA = "ChannelA";
    public const String NAME_DELAYSTEPS10PS = "DelaySteps10ps";
    // Initdata - InitValue - DelayChannelA
    public const Int32 INIT_DELAYSTEPS10PS = 100;
    // NC public const ESweepState INIT_SWEEPSTATE = ESweepState.Idle;
    public const Int32 INIT_LIMITLOW = 100;
    public const Int32 INIT_LIMITHIGH = 900;
    public const Int32 INIT_STEPDELTA = 1;
    public const Int32 INIT_STEPCOUNT = 100;
    public const Int32 INIT_STEPINDEX = 0;
    public const Int32 INIT_REPETITIONCOUNT = 0; // infinite!
    public const Int32 INIT_REPETITIONINDEX = 0; 
    //
    // Initdata - DelayChannelB
    public const String SECTION_CHANNELB = "ChannelB";
    // Initdata - InitValue - DelayChannelB
    //
    // Initdata - Led
    public const String SECTION_LED = "Led";
    private const String NAME_LEDBUSY = "LedBusy";
    private const String NAME_LEDERROR = "LedError";
    // Initdata - InitValue - Led
    private const Boolean INIT_LEDBUSY = false;
    private const Boolean INIT_LEDERROR = false;
    public const Int32 INIT_LEDINDEX_BUSY = 3;
    public const Int32 INIT_LEDINDEX_ERROR = 7;
    //
    public const Int32 INIT_LEDINDEX = 0;
    public const Boolean INIT_LEDACTIVE = false;
    //
    private static readonly String[] ERRORS = 
    { 
      "None",
      "Access failed",
      "Port failed",
      "Open failed",
      "Close failed",
      "Read failed",
      "Write failed",
  	  "Invalid"
    };
    //
    //--------------------------------------
    //	Section - Type
    //--------------------------------------
    //
    protected enum EErrorCode
    {
      None = 0,
      AccessFailed,
      PortFailed,
      OpenFailed,
      CloseFailed,
      ReadFailed,
      WriteFailed,
      Invalid
    };
    //
    //--------------------------------------
    //	Section - Member
    //--------------------------------------
    //
    private CNotifier FNotifier;
    private CDevice FDevice;

    private CHWComPort FComPort;

    // ???? private DOnRXDLineReceived FSerialOnRXDLineReceived;  
    private DOnComPortDataTransmitted FOnComPortDataTransmitted;
    private DOnComPortDataReceived FOnComPortDataReceived;
    //???private DOnReceived FOnSerialResponseReceived;
    // ???? private DOnComPortDataReceived FOnComPorttDataReceived;
    // ???? private DOnDataTransmitted FSerialOnDataTransmitted;
    // ???? private DOnChildLineReceived FOnChildLineReceived;
    private RConfigurationData FData;
    private Random FRandom;

    // !!!!!!!!!!!!!! private CProgram FProgram;

    // ???? private CXmlReader FXmlReader;

    //
    //--------------------------------------
    //	Section - Constructor
    //--------------------------------------
    //
    public CIFTerminalRS232Base()
    {	// DecimalPoint 
      CInitdata.Init();
      //
      FNotifier = null;
      FDevice = new CDevice();
      // NC FStatementlist = new CStatementlist(this);
      //????FSerialOnRXDLineReceived = null;
      //????FSerialOnDataTransmitted = null;
      //????FOnChildLineReceived = null;
      FData = new RConfigurationData(0);
      FRandom = new Random((Int32)DateTime.Now.Ticks);

      FComPort = new CHWComPort();
      RComPortData ComPortData;
      FComPort.GetComPortData(out ComPortData);
      //!!!!!!!!!!!!!ComPortData.OnDataReceived = HWComPortOnDataReceived;
      //!!!!!!!!!!!!!ComPortData.OnDataTransmitted = HWComPortOnDataTransmitted;
      //!!!!!!!!!!!!!ComPortData.OnErrorDetected = HWComPortOnErrorDetected;
      FComPort.SetComPortData(ComPortData);


      // FStatement

      // !!!!!!!!! EnableComPortControls(FUCPortsSelectable.IsAvailable());
      /*
             FUCOpenClose.SetOnOpenCloseChanged(UCOpenCloseOnOpenCloseChanged);

      //
      FUCTerminal.SetNotifier(FUCNotifier);
      FUCTerminal.SetOnSendData(UCTerminalOnSendData);
      //
      // NC FUCBaudrate.SetOnBaudrateChanged(UCBaudrateOnBaudrateChanged);

*/

      //????????????????????????????????????????FXmlReaderTerminal = new CXmlReaderTerminal();

    }
    //
    //########################################################################
    //
    //--------------------------------------
    //	Section - Properties
    //--------------------------------------
    //
    //
    protected void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      FComPort.SetNotifier(notifier);
      FDevice.SetNotifier(notifier);
      // NC FStatementlist.SetNotifier(notifier);
      //????????????????????????????????????????FXmlReaderTerminal.SetNotifier(notifier);
      // !!!!!!!!!!!!!!!!! FProgram.SetNotifier(notifier);
    }

    public void SetOnComPortDataTransmitted(DOnComPortDataTransmitted value)
    {
      FOnComPortDataTransmitted = value;
    }

    public void SetOnComPortDataReceived(DOnComPortDataReceived value)
    {
      FOnComPortDataReceived = value;
    }

    /* // NC 
    public CVariable GetVariable(String variablename)
    {
      return FStatementlist.Variablelist.GetVariable(variablename);
    }*/

/*????    public void SetOnSerialResponseReceived(DOnSerialResponseReceived value)
    {
      FOnSerialResponseReceived = value;
    }*/
    //
    //--------------------------------------
    //	Section - Property - Common
    //--------------------------------------
    //  Property - SoftwareVersion
    //
   /* private void SetSoftwareVersion(String value)
    {
      FData.CommonData.SoftwareVersion = value;
      if (FData.CommonData.OnCommonDataChanged is DOnCommonDataChanged)
      {
        FData.CommonData.OnCommonDataChanged(FData.CommonData);
      }
    }
    private String SoftwareVersion
    {
      get { return FData.CommonData.SoftwareVersion; }
      set { SetSoftwareVersion(value); }
    }
    //
    //  Property - HardwareVersion
    //
    private void SetHardwareVersion(String value)
    {
      FData.CommonData.HardwareVersion = value;
      if (FData.CommonData.OnCommonDataChanged is DOnCommonDataChanged)
      {
        FData.CommonData.OnCommonDataChanged(FData.CommonData);
      }
    }
    private String HardwareVersion
    {
      get { return FData.CommonData.HardwareVersion; }
      set { SetHardwareVersion(value); }
    }
    //  Property - DeviceType
    private void SetDeviceType(String value)
    {
      FData.CommonData.DeviceType = value;
      if (FData.CommonData.OnCommonDataChanged is DOnCommonDataChanged)
      {
        FData.CommonData.OnCommonDataChanged(FData.CommonData);
      }
    }
    private String DeviceType
    {
      get { return FData.CommonData.DeviceType; }
      set { SetDeviceType(value); }
    }*/
    //
    //--------------------------------------
    //	Section - Property - DelayChannelA
    //--------------------------------------
    /*/
    private void SetDelayChannelA(Int32 value)
    {
      FData.ChannelDataA.Delay = value;
      if (FData.ChannelDataA.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataA.OnChannelDataChanged(FData.ChannelDataA);
      }
    }
    private Int32 DelayChannelA
    {
      get { return FData.ChannelDataA.Delay; }
      set { SetDelayChannelA(value); }
    }
    //
    //--------------------------------------
    //	Section - Property - DelayChannelB
    //--------------------------------------
    //
    private void SetDelayChannelB(Int32 value)
    {
      FData.ChannelDataB.Delay = value;
      if (FData.ChannelDataB.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataB.OnChannelDataChanged(FData.ChannelDataB);
      }
    }
    private Int32 DelayChannelB
    {
      get { return FData.ChannelDataB.Delay; }
      set { SetDelayChannelB(value); }
    }
    //
    //--------------------------------------
    //	Section - Property - Led
    //--------------------------------------
    //
    private void SetLed(Int32 index, Boolean value)
    {
      switch (index)
      {
        case INIT_LEDINDEX_BUSY:
          FData.LedData3.Index = INIT_LEDINDEX_BUSY;
          FData.LedData3.Active = value;
          if (FData.LedData3.OnLedDataChanged is DOnLedDataChanged)
          {
            FData.LedData3.OnLedDataChanged(FData.LedData3);
          }
          break;
        case INIT_LEDINDEX_ERROR:
          FData.LedData7.Index = INIT_LEDINDEX_ERROR;
          FData.LedData7.Active = value;
          if (FData.LedData7.OnLedDataChanged is DOnLedDataChanged)
          {
            FData.LedData7.OnLedDataChanged(FData.LedData7);
          }
          break;
      }
    }
    private Boolean Led3
    {
      get { return FData.LedData3.Active; }
      set { SetLed(INIT_LEDINDEX_BUSY, value); }
    }
    private Boolean Led7
    {
      get { return FData.LedData7.Active; }
      set { SetLed(INIT_LEDINDEX_ERROR, value); }
    }
    //
    //----------------------------------------------
    //	Section - Property - SweepState - ChannelA
    //----------------------------------------------
    //
    private void SetSweepStateChannelA(ESweepState value)
    {
      FData.ChannelDataA.SweepState = value;
      if (FData.ChannelDataA.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataA.OnChannelDataChanged(FData.ChannelDataA);
      }
    }
    private ESweepState SweepStateChannelA
    {
      get { return FData.ChannelDataA.SweepState; }
      set { SetSweepStateChannelA(value); }
    }    
    //
    //-----------------------------------------------
    //	Section - Property - SweepParameterChannelA
    //-----------------------------------------------
    private Int32 GetSweepParameterChannelA(Int32 index)
    {
      switch (index)
      {
        case (int)ESweepParameter.LimitLow:
          return FData.ChannelDataA.LimitLow;
        case (int)ESweepParameter.LimitHigh:
          return FData.ChannelDataA.LimitHigh;
        case (int)ESweepParameter.StepDelta:
          return FData.ChannelDataA.StepDelta;
        case (int)ESweepParameter.StepCount:
          return FData.ChannelDataA.StepCount;
        case (int)ESweepParameter.StepIndex:
          return FData.ChannelDataA.StepIndex;
        case (int)ESweepParameter.RepetitionCount:
          return FData.ChannelDataA.RepetitionCount;
        case (int)ESweepParameter.RepetitionIndex:
          return FData.ChannelDataA.RepetitionIndex;
        default:
          return 0;
      }
    }
    private void SetSweepParameterChannelA(Int32 index, Int32 value)
    {
      switch (index)
      {
        case (int)ESweepParameter.LimitLow:
          FData.ChannelDataA.LimitLow = value;
          break;
        case (int)ESweepParameter.LimitHigh:
          FData.ChannelDataA.LimitHigh = value;
          break;
        case (int)ESweepParameter.StepDelta:
          FData.ChannelDataA.StepDelta = value;
          break;
        case (int)ESweepParameter.StepCount:
          FData.ChannelDataA.StepCount = value;
          break;
        case (int)ESweepParameter.StepIndex:
          FData.ChannelDataA.StepIndex = value;
          break;
        case (int)ESweepParameter.RepetitionCount:
          FData.ChannelDataA.RepetitionCount = value;
          break;
        case (int)ESweepParameter.RepetitionIndex:
          FData.ChannelDataA.RepetitionIndex = value;
          break;
      }
      if (FData.ChannelDataA.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataA.OnChannelDataChanged(FData.ChannelDataA);
      }
    }

    private Int32 SweepLimitLowA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.LimitLow); }
      set { SetSweepParameterChannelA((int)ESweepParameter.LimitLow, value); }
    }
    private Int32 SweepLimitHighA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.LimitHigh); }
      set { SetSweepParameterChannelA((int)ESweepParameter.LimitHigh, value); }
    }
    private Int32 SweepStepDeltaA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.StepDelta); }
      set { SetSweepParameterChannelA((int)ESweepParameter.StepDelta, value); }
    }
    private Int32 SweepStepCountA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.StepCount); }
      set { SetSweepParameterChannelA((int)ESweepParameter.StepCount, value); }
    }
    private Int32 SweepStepIndexA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.StepIndex); }
      set { SetSweepParameterChannelA((int)ESweepParameter.StepIndex, value); }
    }
    private Int32 SweepRepetitionCountA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.RepetitionCount); }
      set { SetSweepParameterChannelA((int)ESweepParameter.RepetitionCount, value); }
    }
    private Int32 SweepRepetitionIndexA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.RepetitionIndex); }
      set { SetSweepParameterChannelA((int)ESweepParameter.RepetitionIndex, value); }
    }
    //
    //----------------------------------------------
    //	Section - Property - SweepState - ChannelB
    //----------------------------------------------
    //
    private void SetSweepStateChannelB(ESweepState value)
    {
      FData.ChannelDataB.SweepState = value;
      if (FData.ChannelDataB.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataB.OnChannelDataChanged(FData.ChannelDataB);
      }
    }
    private ESweepState SweepStateChannelB
    {
      get { return FData.ChannelDataB.SweepState; }
      set { SetSweepStateChannelB(value); }
    }
    //
    //-----------------------------------------------
    //	Section - Property - SweepParameterChannelB
    //-----------------------------------------------
    private Int32 GetSweepParameterChannelB(Int32 index)
    {
      switch (index)
      {
        case (int)ESweepParameter.LimitLow:
          return FData.ChannelDataB.LimitLow;
        case (int)ESweepParameter.LimitHigh:
          return FData.ChannelDataB.LimitHigh;
        case (int)ESweepParameter.StepDelta:
          return FData.ChannelDataB.StepDelta;
        case (int)ESweepParameter.StepCount:
          return FData.ChannelDataB.StepCount;
        case (int)ESweepParameter.StepIndex:
          return FData.ChannelDataB.StepIndex;
        case (int)ESweepParameter.RepetitionCount:
          return FData.ChannelDataB.RepetitionCount;
        case (int)ESweepParameter.RepetitionIndex:
          return FData.ChannelDataB.RepetitionIndex;
        default:
          return 0;
      }
    }
    private void SetSweepParameterChannelB(Int32 index, Int32 value)
    {
      switch (index)
      {
        case (int)ESweepParameter.LimitLow:
          FData.ChannelDataB.LimitLow = value;
          break;
        case (int)ESweepParameter.LimitHigh:
          FData.ChannelDataB.LimitHigh = value;
          break;
        case (int)ESweepParameter.StepDelta:
          FData.ChannelDataB.StepDelta = value;
          break;
        case (int)ESweepParameter.StepCount:
          FData.ChannelDataB.StepCount = value;
          break;
        case (int)ESweepParameter.StepIndex:
          FData.ChannelDataB.StepIndex = value;
          break;
        case (int)ESweepParameter.RepetitionCount:
          FData.ChannelDataB.RepetitionCount = value;
          break;
        case (int)ESweepParameter.RepetitionIndex:
          FData.ChannelDataB.RepetitionIndex = value;
          break;
      }
      if (FData.ChannelDataB.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataB.OnChannelDataChanged(FData.ChannelDataB);
      }
    }

    private Int32 SweepLimitLowB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.LimitLow); }
      set { SetSweepParameterChannelB((int)ESweepParameter.LimitLow, value); }
    }
    private Int32 SweepLimitHighB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.LimitHigh); }
      set { SetSweepParameterChannelB((int)ESweepParameter.LimitHigh, value); }
    }
    private Int32 SweepStepDeltaB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.StepDelta); }
      set { SetSweepParameterChannelB((int)ESweepParameter.StepDelta, value); }
    }
    private Int32 SweepStepCountB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.StepCount); }
      set { SetSweepParameterChannelB((int)ESweepParameter.StepCount, value); }
    }
    private Int32 SweepStepIndexB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.StepIndex); }
      set { SetSweepParameterChannelB((int)ESweepParameter.StepIndex, value); }
    }
    private Int32 SweepRepetitionCountB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.RepetitionCount); }
      set { SetSweepParameterChannelB((int)ESweepParameter.RepetitionCount, value); }
    }
    private Int32 SweepRepetitionIndexB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.RepetitionIndex); }
      set { SetSweepParameterChannelB((int)ESweepParameter.RepetitionIndex, value); }
    }
    /*/
		//--------------------------------------
		//	Section - Get/SetData
		//--------------------------------------
		//
    protected Boolean GetConfigurationData(ref RConfigurationData data)
    {
      RComPortData ComPortData;
      Boolean Result = FComPort.GetComPortData(out ComPortData);
      FData.ComPortData = ComPortData;
      data = FData;
      return Result;
    }
    protected Boolean SetConfigurationData(RConfigurationData data)
    {
      FData.ComPortData = data.ComPortData;
      Boolean Result = FComPort.SetComPortData(FData.ComPortData);
      FData.OnComPortDataChanged = data.OnComPortDataChanged;
      FData.CommonData = data.CommonData;
      FData.ChannelDataA = data.ChannelDataA;
      FData.ChannelDataB = data.ChannelDataB;
      FData.LedData3 = data.LedData3;
      FData.LedData7 = data.LedData7;
      return Result;
    }

		//
		//--------------------------------------
		//	Section - Helper
		//--------------------------------------
		//

		//
		//--------------------------------------
		//	Section - Error
		//--------------------------------------
		//

		//
		//--------------------------------------------------
		//	Messages
		//--------------------------------------------------
		//
		protected void _Error(EErrorCode code)
		{
			if (FNotifier is CNotifier)
			{
				Int32 Index = (Int32)code;
				if ((Index < 0) && (ERRORS.Length <= Index))
				{
					Index = ERRORS.Length - 1;
				}
				String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
        FNotifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
			}
		}

		protected void _Error(Int32 code,
													String text)
		{
			if (FNotifier is CNotifier)
			{
        FNotifier.Error(CIFTerminalRS232.HEADER_LIBRARY, code, text);
			}
		}

		protected void _Protocol(String line)
		{
			if (FNotifier is CNotifier)
			{
        FNotifier.Write(CIFTerminalRS232.HEADER_LIBRARY, line);
			}
		}
    //--------------------------------------------------------------------------------------
    //  Section - Public Management
    //--------------------------------------------------------------------------------------
    //
    public Boolean OpenSerial(String portname,
                              EComPort comport,
                              EBaudrate baudrate,
                              EParity parity,
                              EDatabits databits,
                              EStopbits stopbits,
                              EHandshake handshake)
    {
      //HWComPort.O
      return false;
    }

    public Boolean Close()
    {
      RComPortData ComPortData;
      FComPort.GetComPortData(out ComPortData);
      ComPortData.OnDataReceived = null;
      ComPortData.OnDataTransmitted = null;
      ComPortData.OnErrorDetected = null;
      FComPort.SetComPortData(ComPortData);
      //
      // !!!!!!!!!!EnableComPortControls(true);
      //
      //!!!!!!FUCOpenClose.SetOnOpenCloseChanged(null);
      //
      // Problem!!!! HWComPortClose();
      return false;
    }






		//
		//--------------------------------------
		//	Section - Management
		//--------------------------------------
		//
    /*protected Boolean OpenComPort(EComPort comport)
		{
      FlushStatementlist();
      FComPort.GetComPortData(out FData.ComPortData);
      FData.ComPortData.Name = INIT_DEVICETYPE;
      FData.ComPortData.ComPort = comport;
      FData.ComPortData.Baudrate = INIT_BAUDRATE;
      FData.ComPortData.Databits = INIT_DATABITS;
      FData.ComPortData.Parity = INIT_PARITY;
      FData.ComPortData.Stopbits = INIT_STOPBITS;
      FData.ComPortData.RXDBufferSize = INIT_BUFFERSIZE;
      FData.ComPortData.TXDBufferSize = INIT_BUFFERSIZE;
      FData.ComPortData.TransmitEnabled = INIT_TRANSMITENABLED;
      FData.ComPortData.TXDDelayCarriageReturn = INIT_DELAYCARRRIAGERETURN;
      FData.ComPortData.TXDDelayLineFeed = INIT_DELAYLINEFEED;
      FData.ComPortData.TXDDelayCharacter = INIT_DELAYCHARACTER;
      FData.ComPortData.OnChildLineReceived = HWComPortOnDataReceived //SerialOnLineReceived;
      FData.ComPortData.OnDataTransmitted = SerialOnDataTransmitted;
      FData.ComPortData.OnComPortDataChanged = SerialOnComPortDataChanged;
      Boolean Result = FComPort.Open(FData.ComPortData);
			if (Result)
			{
        String HWComPortName = CHWComPort.ComPortName(FData.ComPortData.ComPort);
				String Line = String.Format("Serial port {0} correct opened", HWComPortName);
				_Protocol(Line);
			} else
			{
				_Error(EErrorCode.OpenFailed);
			}
      // Callback-Actualization
      SerialOnComPortDataChanged(FData.ComPortData.ID, FData.ComPortData);
      //
			return Result;
		}*/

    private void SerialOnComPortDataChanged(Guid comportid, RComPortData comportdata)
    {
      FData.ComPortData = comportdata;
      if (FData.ComPortData.IsOpen)
      {
        // NC StatementSetLed(CIFTerminalRS232.INIT_LEDINDEX_BUSY);
        // NC StatementClearLed(CIFTerminalRS232.INIT_LEDINDEX_BUSY);
      }
      if (FData.OnComPortDataChanged is DOnComPortDataChanged)
      {
        FData.OnComPortDataChanged(comportdata.ID, FData.ComPortData);
      }
    }

    protected Boolean CloseComPort()
		{
      FlushStatementlist();
      Boolean Result = FComPort.Close();
			if (Result)
			{
				RComPortData ComPortData = new RComPortData();
				FComPort.GetComPortData(out ComPortData);
				String HWComPortName = CHWComPort.ComPortName(ComPortData.ComPort);						
				String Line = String.Format("Serial port {0} correct closed", HWComPortName);
				_Protocol(Line);
			} else
			{
        //!!!!!!!!!!!!!!!!!!!!!_Error(EErrorCode.CloseFailed);
			}
      // Callback-Actualization
      if (FData.ComPortData.OnComPortDataChanged is DOnComPortDataChanged)
      {
        RComPortData ComPortData = new RComPortData();
        FComPort.GetComPortData(out ComPortData);
        FData.ComPortData.OnComPortDataChanged(ComPortData.ID, ComPortData);
      }
      //
      return Result;
		}

		private Boolean MatchError(String line,
															 ref Int32 errorcode,
															 ref String errortext)
		{
			Regex RE = new Regex("(# Error)(\\[){1,1}([0-9]){1,}(\\]: ){1,1}([^!]{1,})");
			Match M = RE.Match(line);
			if (M.Success)
			{
				if (5 < M.Groups.Count)
				{
					errorcode = Int32.Parse(M.Groups[3].Value);
					errortext = M.Groups[5].Value;
					return true;
				}
			}
			return false;
		}

    /*public void SetOnChildLineReceived(DOnChildLineReceived value)
    {
      FOnChildLineReceived = value;
    }

    public void SetOnRXDLineReceived(DOnRXDLineReceived value)
    {
      FSerialOnRXDLineReceived = value;
    }

    public void SetOnDataTransmitted(DOnDataTransmitted value)
    {
      FSerialOnDataTransmitted = value;
    }

    private void SerialOnDataTransmitted(Guid comportid, String data)
    {
      if (FSerialOnDataTransmitted is DOnDataTransmitted)
      {
        FSerialOnDataTransmitted(comportid, data);
      }
    }

    private void SerialOnLineReceived(Guid comportid, String line)
    {
      // Unfiltered to Parent (UCFDL-Terminal)
      if (FSerialOnRXDLineReceived is DOnRXDLineReceived)
      {
        // Guid HWComPortID = Guid.NewGuid();
        // FOnRXDLineReceived(ComPortID, line);
        FSerialOnRXDLineReceived(line);
      }
      //
			// Error-Catching : <#><Error><[><code><]><:><!>
			// Debug Console.WriteLine(line);
			Int32 ErrorCode = 0;
			String ErrorText = "";
			if (MatchError(line, ref ErrorCode, ref ErrorText))
			{
				_Error(ErrorCode, ErrorText);
				return;
			}
      /* ********** / NC
      // Asynchronuous Message-Catching - DeviceState
      //
      EDeviceState MS = EDeviceState.Undefined;
			if (MatchDeviceState(line, ref MS))
			{
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!DeviceState = MS;
				// Callback to actual command without refresh!
        if (FOnChildLineReceived is DOnChildLineReceived)
				{
          FOnChildLineReceived(comportid, line);
				}
				return;
			}
      //
      // Asynchronuous Message-Catching - PIDAdjustment
      //
      String PT;
      String PA;
      String PD;
      String PW;
      if (MatchPIDState(line, out PT, out PA, out PD, out PW))
      {
        // debug String Line = "PT=" + PT + " PA=" + PA + " PD=" + PD + " PW=" + PW;
        // debug Console.WriteLine(Line); 
        // NC !!! RefreshVelocityTarget(PT);
        // !!!!!!!!!!!!!!!!!!!!!!RefreshVelocityActual(PA);
        // NC RefreshVelocityDelta(PT);
        //!!!!!!!!!!!!!!!!!!!!!!!!!!RefreshPWMPulseWidth(PW);
        return;
      }********* 
      //
      // All other: Callback to Child -> Statementlist -> Statement
      //
      if (FOnChildLineReceived is DOnChildLineReceived)
      {
        FOnChildLineReceived(comportid, line);
      }
    }	*/	

    protected Boolean IsComPortOpen()
		{
			return FComPort.IsOpen();
		}

    protected Boolean IsComPortClosed()
		{
			return FComPort.IsClosed();
		}
    //
    //--------------------------------------
    //	Section - Iniitdata
    //--------------------------------------
    //
		protected Boolean LoadInitdata(CInitdataReader initdata)
		{
      Boolean Result = initdata.OpenSection(CIFTerminalRS232.SECTION_LIBRARY);
      //
      String SValue;
      //         ..... Boolean BValue;
      //         ..... Int32 IValue;
      //
      //  Common
      Result &= initdata.OpenSection(CIFTerminalRS232.SECTION_COMMON);
      SValue = INIT_DEVICETYPE;
      Result &= initdata.ReadString(NAME_DEVICETYPE, out SValue, SValue);
      //RO
      Result &= initdata.CloseSection();
      //
      //  HWComPort
      Result &= initdata.OpenSection(CIFTerminalRS232.SECTION_COMPORT);
      //
      SValue = CHWComPort.ComPortName(INIT_COMPORT);
      Result &= initdata.ReadEnumeration(NAME_COMPORT, out SValue, SValue);
      FData.ComPortData.ComPort = CHWComPort.ComPortEnumerator(SValue);
      //
      Result &= initdata.CloseSection();
      /*/ -> Above
      //  DelayChannelA
      Result &= initdata.OpenSection(CIFTerminalRS232.SECTION_CHANNELA);
      //
      IValue = CIFTerminalRS232.INIT_DELAYSTEPS10PS;
      Result &= initdata.ReadInt32(CIFTerminalRS232.NAME_DELAYSTEPS10PS, out IValue, IValue);
      FData.ChannelDataA.Delay = IValue;
      //
      Result &= initdata.CloseSection();
      //
      //  DelayChannelB
      Result &= initdata.OpenSection(CIFTerminalRS232.SECTION_CHANNELB);
      //
      IValue = CIFTerminalRS232.INIT_DELAYSTEPS10PS;
      Result &= initdata.ReadInt32(CIFTerminalRS232.NAME_DELAYSTEPS10PS, out IValue, IValue);
      FData.ChannelDataB.Delay = IValue;
      //
      Result &= initdata.CloseSection();
      //
      //  Led
      Result &= initdata.OpenSection(CIFTerminalRS232.SECTION_LED);
      //  Led - Busy
      BValue = CIFTerminalRS232.INIT_LEDBUSY;
      Result &= initdata.ReadBoolean(CIFTerminalRS232.NAME_LEDBUSY, out BValue, BValue);
      FData.LedData3.Active = BValue;
      //  Led - Error
      BValue = CIFTerminalRS232.INIT_LEDERROR;
      Result &= initdata.ReadBoolean(CIFTerminalRS232.NAME_LEDERROR, out BValue, BValue);
      FData.LedData7.Active = BValue;
      //
      Result &= initdata.CloseSection();
      /*/
      //
      // Refresh all Data:
      //  HWComPort
      Result &= //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! OpenComPort(FData.ComPortData.ComPort);
      /*/  DelayChannelA
      //         ..... StatementSetDelayChannelA(FData.ChannelDataA.Delay);
      //  DelayChannelB
      //         ..... StatementSetDelayChannelB(FData.ChannelDataB.Delay);
      //  Led - Busy
      if (FData.LedData3.Active)
      {
        StatementSetLed(INIT_LEDINDEX_BUSY);
      }
      else
      {
        StatementClearLed(INIT_LEDINDEX_BUSY);
      }
      //  Led - Error
      if (FData.LedData7.Active)
      {
        StatementSetLed(INIT_LEDINDEX_ERROR);
      }
      else
      {
        StatementClearLed(INIT_LEDINDEX_ERROR);
      } */     
      //
      Result &= initdata.CloseSection();
      return Result;
		}



		protected Boolean SaveInitdata(CInitdataWriter initdata)
		{
      initdata.CreateSection(CIFTerminalRS232.SECTION_LIBRARY);
      //
      //  Common
      initdata.CreateSection(CIFTerminalRS232.SECTION_COMMON);
      //RO
      FData.CommonData.DeviceType = INIT_DEVICETYPE;
      initdata.WriteString(NAME_DEVICETYPE, FData.CommonData.DeviceType);
      initdata.CloseSection();
      //
      //  HWComPort
      initdata.CreateSection(CIFTerminalRS232.SECTION_COMPORT);
      initdata.WriteEnumeration(NAME_COMPORT, CHWComPort.ComPortName(FData.ComPortData.ComPort));
      initdata.CloseSection();
      //
      /*/  DelayChannelA
      initdata.CreateSection(CIFTerminalRS232.SECTION_CHANNELA);
      initdata.WriteInt32(CIFTerminalRS232.NAME_DELAYSTEPS10PS, FData.ChannelDataA.Delay);
      initdata.CloseSection();
      //
      //  DelayChannelB
      initdata.CreateSection(CIFTerminalRS232.SECTION_CHANNELB);
      initdata.WriteInt32(CIFTerminalRS232.NAME_DELAYSTEPS10PS, FData.ChannelDataB.Delay);
      initdata.CloseSection();
      //
      //  Led
      initdata.CreateSection(CIFTerminalRS232.SECTION_LED);
      //  Led - Busy
      initdata.WriteBoolean(CIFTerminalRS232.NAME_LEDBUSY, FData.LedData3.Active);
      //  Led - Error
      initdata.WriteBoolean(CIFTerminalRS232.NAME_LEDERROR, FData.LedData7.Active);
      initdata.CloseSection();
      /*/      
      initdata.CloseSection();
      return true;
		}
    //
    //-----------------------------------------------
    //	Section - Helper
    //-----------------------------------------------
    //
    private void FlushStatementlist()
    {
      // NC FStatementlist.AbortExecution();
    }


    //
    //-----------------------------------------------
    //	Section - Statement - Refresh - Common
    //-----------------------------------------------
    //
    /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!public void RefreshSoftwareVersion(String value)
		{
      SetSoftwareVersion(value);
		}

		public void RefreshHardwareVersion(String value)
		{
      SetHardwareVersion(value);
		}

    public void RefreshDeviceType(String value)
    {
      SetDeviceType(value);
    }*/

    public void RefreshHelp(String line)//String[] helplist)
    {
      // actual: nothing to do!
      // later!!!!!!!!!!!!!!!!!!!!!!!!
      //!!!!!!!!!!!!!!!!!!FUCConfiguration.SetHelplist(helplist);
    }
    //
    //-----------------------------------------------
    //	Section - Statement - Refresh - DeviceState
    //-----------------------------------------------
    /*/
    public void RefreshDeviceState(String value)
    {      
      switch (value)
      {
        case CDEVICESTATES.Idle:
          SetDeviceState(EDeviceState.Idle);
          break;
        case CDEVICESTATES.ADCRead:
          SetDeviceState(EDeviceState.ADCRead);
          break;
        case CDEVICESTATES.ADCWrite:
          SetDeviceState(EDeviceState.ADCWrite);
          break;
        default:
          SetDeviceState(EDeviceState.Undefined);
          break;
      }
    }
     */
    //
    //--------------------------------------------------
    //	Section - Statement - Refresh - Delay - ChannelA
    //--------------------------------------------------
    /*/
    public void RefreshDelayChannelA(String value)
    {
      try
      {
        Int32 Value = Int32.Parse(value);
        DelayChannelA = Value;
      }
      catch (Exception)
      {
      }
    }
    //
    //--------------------------------------------------
    //	Section - Statement - Refresh - Delay - ChannelB
    //--------------------------------------------------
    //
    public void RefreshDelayChannelB(String value)
    {
      try
      {
        Int32 Value = Int32.Parse(value);
        DelayChannelB = Value;
      }
      catch (Exception)
      {
      }
    }
    //
    //-----------------------------------------------
    //	Section - Statement - Refresh - Led
    //-----------------------------------------------
    //
    public void RefreshLed(String index, Boolean value)
    {
      try
      {
        Int32 Index = Int32.Parse(index);
        switch (Index)
        {
          case 3:
            Led3 = value;
            break;
          case 7:
            Led7 = value;
            break;
        }
      }
      catch (Exception)
      {
      }
    }
    //
    //-----------------------------------------------------------
    //	Section - Statement - Refresh - SweepState - ChannelA
    //-----------------------------------------------------------
    //
    public void RefreshSweepStateChannelA(ESweepState value)
    {
      try
      {
        SweepStateChannelA = value;
      }
      catch (Exception)
      {
      }
    }
    //
    //-----------------------------------------------------------
    //	Section - Statement - Refresh - SweepParameter - ChannelA
    //-----------------------------------------------------------
    //
    public void RefreshSweepParameterChannelA(Int32 index, Int32 value)
    {
      try
      {
        switch (index)
        {
          case (Int32)ESweepParameter.LimitLow:
            SweepLimitLowA = value;
            break;
          case (Int32)ESweepParameter.LimitHigh:
            SweepLimitHighA = value;
            break;
          case (Int32)ESweepParameter.StepDelta:
            SweepStepDeltaA = value;
            break;
          case (Int32)ESweepParameter.StepCount:
            SweepStepCountA = value;
            break;
          case (Int32)ESweepParameter.StepIndex:
            SweepStepIndexA = value;
            break;
          case (Int32)ESweepParameter.RepetitionCount:
            SweepRepetitionCountA = value;
            break;
          case (Int32)ESweepParameter.RepetitionIndex:
            SweepRepetitionIndexA = value;
            break;
        }
      }
      catch (Exception)
      {
      }
    }
    //
    //-----------------------------------------------------------
    //	Section - Statement - Refresh - SweepState - ChannelB
    //-----------------------------------------------------------
    //
    public void RefreshSweepStateChannelB(ESweepState value)
    {
      try
      {
        SweepStateChannelB = value;
      }
      catch (Exception)
      {
      }
    }
    //
    //-----------------------------------------------------------
    //	Section - Statement - Refresh - SweepParameter - ChannelB
    //-----------------------------------------------------------
    //
    public void RefreshSweepParameterChannelB(Int32 index, Int32 value)
    {
      try
      {
        switch (index)
        {
          case (Int32)ESweepParameter.LimitLow:
            SweepLimitLowB = value;
            break;
          case (Int32)ESweepParameter.LimitHigh:
            SweepLimitHighB = value;
            break;
          case (Int32)ESweepParameter.StepDelta:
            SweepStepDeltaB = value;
            break;
          case (Int32)ESweepParameter.StepCount:
            SweepStepCountB = value;
            break;
          case (Int32)ESweepParameter.StepIndex:
            SweepStepIndexB = value;
            break;
          case (Int32)ESweepParameter.RepetitionCount:
            SweepRepetitionCountB = value;
            break;
          case (Int32)ESweepParameter.RepetitionIndex:
            SweepRepetitionIndexB = value;
            break;
        }
      }
      catch (Exception)
      {
      }
    }*/
    //
    //--------------------------------------
    //	Section - Statement - Common
    //--------------------------------------
    /*/
    protected Boolean StatementGetHelp()
    {
      CGetHelp Statement = new CGetHelp(FStatementlist, FNotifier,
                                      FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }

    protected Boolean StatementGetSoftwareVersion()
		{
			CGetSoftwareVersion Statement = new CGetSoftwareVersion(FStatementlist, FNotifier,
																														FDevice, FComPort);
			FStatementlist.Add(Statement);
			return true;
		}

    protected Boolean StatementGetHardwareVersion()
		{
			CGetHardwareVersion Statement = new CGetHardwareVersion(FStatementlist, FNotifier,
																														FDevice, FComPort);
			FStatementlist.Add(Statement);
			return true;
		}
*/
    /*
    protected Boolean StatementGetDeviceType()
    {
      CGetDeviceType Statement = new CGetDeviceType(FStatementlist, FNotifier,
                                                FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }*/
    //
    //--------------------------------------
    //	Section - Statement - DelayChannelA
    //--------------------------------------
    //
    /*
    protected Boolean StatementGetDelayChannelA()
    {
      CGetDelayChannelA Statement = new CGetDelayChannelA(FStatementlist, FNotifier, FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }

    protected Boolean StatementSetDelayChannelA(Int32 delay)
    {
      CSetDelayChannelA Statement = new CSetDelayChannelA(FStatementlist, delay,
                                                        FNotifier, FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }*/
    //
    //--------------------------------------
    //	Section - Statement - DelayChannelB
    //--------------------------------------
    //
    /*
    protected Boolean StatementGetDelayChannelB()
    {
      CGetDelayChannelB Statement = new CGetDelayChannelB(FStatementlist, FNotifier, FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }

    protected Boolean StatementSetDelayChannelB(Int32 delay)
    {
      CSetDelayChannelB Statement = new CSetDelayChannelB(FStatementlist, delay,
                                                        FNotifier, FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }*/
    //
    //--------------------------------------
    //	Section - Statement - Led
    //--------------------------------------
    //
    /*
    protected Boolean StatementGetLed(Byte index)
    {
      CGetLed Statement = new CGetLed(FStatementlist,
                                    index,
                                    FNotifier, FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }

    protected Boolean StatementClearLed(Byte index)
    {
      CClearLed Statement = new CClearLed(FStatementlist,
                                        index,
                                        FNotifier, FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }

    protected Boolean StatementSetLed(Byte index)
    {
      CSetLed Statement = new CSetLed(FStatementlist,
                                    index, 
                                    FNotifier, FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }*/
    //
    //--------------------------------------
    //	Section - Statement - SSA
    //--------------------------------------
    //
    /*
    protected Boolean StatementSetSweepParameterChannelA(Int32 index, Int32 value)
    {
      CSetSweepParameterChannelA Statement = new CSetSweepParameterChannelA(FStatementlist,
                                                                          index,
                                                                          value,
                                                                          FNotifier,
                                                                          FDevice,
                                                                          FComPort);
      FStatementlist.Add(Statement);
      return true;
    }*/
    //
    //--------------------------------------
    //	Section - Statement - GSA
    //--------------------------------------
    //
    /*
    protected Boolean StatementGetSweepParameterChannelA(Int32 index)
    {
      CGetSweepParameterChannelA Statement = new CGetSweepParameterChannelA(FStatementlist,
                                                                          index,
                                                                          FNotifier,
                                                                          FDevice,
                                                                          FComPort);
      FStatementlist.Add(Statement);
      return true;
    }*/
    //
    //--------------------------------------
    //	Section - Statement - GWA
    //--------------------------------------
    //
    /*
    protected Boolean StatementGetSweepStateChannelA()
    {
      CGetSweepStateChannelA Statement = new CGetSweepStateChannelA(FStatementlist,
                                                                  FNotifier,
                                                                  FDevice,
                                                                  FComPort);
      FStatementlist.Add(Statement);
      return true;
    }*/
    //
    //--------------------------------------
    //	Section - Statement - SWA
    //--------------------------------------
    /*/
    protected Boolean StatementSetSweepStateChannelA(ESweepState value)
    {
      CSetSweepStateChannelA Statement = new CSetSweepStateChannelA(FStatementlist,
                                                                  value,
                                                                  FNotifier,
                                                                  FDevice,
                                                                  FComPort);
      FStatementlist.Add(Statement);
      return true;
    }
    /*/
    //--------------------------------------
    //	Section - Statement - SSB
    //--------------------------------------
    /*/
    protected Boolean StatementSetSweepParameterChannelB(Int32 index, Int32 value)
    {
      CSetSweepParameterChannelB Statement = new CSetSweepParameterChannelB(FStatementlist,
                                                                          index,
                                                                          value,
                                                                          FNotifier,
                                                                          FDevice,
                                                                          FComPort);
      FStatementlist.Add(Statement);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Statement - GSB
    //--------------------------------------
    //
    protected Boolean StatementGetSweepParameterChannelB(Int32 index)
    {
      CGetSweepParameterChannelB Statement = new CGetSweepParameterChannelB(FStatementlist,
                                                                          index,
                                                                          FNotifier,
                                                                          FDevice,
                                                                          FComPort);
      FStatementlist.Add(Statement);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Statement - GWB
    //--------------------------------------
    //
    protected Boolean StatementGetSweepStateChannelB()
    {
      CGetSweepStateChannelB Statement = new CGetSweepStateChannelB(FStatementlist,
                                                                  FNotifier,
                                                                  FDevice,
                                                                  FComPort);
      FStatementlist.Add(Statement);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Statement - SWB
    //--------------------------------------
    //
    protected Boolean StatementSetSweepStateChannelB(ESweepState value)
    {
      CSetSweepStateChannelB Statement = new CSetSweepStateChannelB(FStatementlist,
                                                                  value,
                                                                  FNotifier,
                                                                  FDevice,
                                                                  FComPort);
      FStatementlist.Add(Statement);
      return true;
    }
    /*/
    //--------------------------------------
    //	Section - Analyse
    //--------------------------------------
    /*/
    private Boolean AnalyseSendStatement(String line)
    {
      String[] Tokens = line.Split(' ');
      if (0 < Tokens.Length)
      {
        Int32 IValue;
        Int16 WIndex;
        Int16 WValue;
        Byte BIndex;
        String StatementToken = Tokens[0].ToUpper();
        switch (StatementToken)
        { // Help
          case CGetHelp.COMMAND_TEXT:
            CGetHelp CH = new CGetHelp(FStatementlist, FNotifier, FDevice, FComPort);
            FStatementlist.Add(CH);
            return true;
          // Delay Channel A
          case CSetDelayChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              IValue = Int32.Parse(Tokens[1]);
              CSetDelayChannelA Statement = new CSetDelayChannelA(FStatementlist, IValue,
                                                                FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CGetDelayChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              IValue = Byte.Parse(Tokens[1]);
              CGetDelayChannelA Statement = new CGetDelayChannelA(FStatementlist,
                                                                FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          // Delay Channel B
          case CSetDelayChannelB.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              IValue = Int32.Parse(Tokens[1]);
              CSetDelayChannelB Statement = new CSetDelayChannelB(FStatementlist, IValue,
                                                                FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CGetDelayChannelB.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              IValue = Byte.Parse(Tokens[1]);
              CGetDelayChannelB Statement = new CGetDelayChannelB(FStatementlist,
                                                                FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          // Led
          case CGetLed.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              BIndex = Byte.Parse(Tokens[1]);
              CGetLed Statement = new CGetLed(FStatementlist, BIndex,
                                            FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CSetLed.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              BIndex = Byte.Parse(Tokens[1]);
              CSetLed Statement = new CSetLed(FStatementlist, BIndex,
                                            FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CClearLed.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              BIndex = Byte.Parse(Tokens[1]);
              CClearLed Statement = new CClearLed(FStatementlist, BIndex,
                                                FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CGetSweepStateChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              CGetSweepStateChannelA Statement = new CGetSweepStateChannelA(FStatementlist,
                                                                          FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CSetSweepStateChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              ESweepState SS = (ESweepState)Int16.Parse(Tokens[1]);
              CSetSweepStateChannelA Statement = new CSetSweepStateChannelA(FStatementlist, SS,
                                                                          FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CGetSweepParameterChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              WIndex = Int16.Parse(Tokens[1]);
              CGetSweepParameterChannelA Statement = new CGetSweepParameterChannelA(FStatementlist, WIndex,
                                                                                  FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CSetSweepParameterChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              WIndex = Int16.Parse(Tokens[1]);
              WValue = Int16.Parse(Tokens[2]);
              CSetSweepParameterChannelA Statement = new CSetSweepParameterChannelA(FStatementlist, WIndex, WValue,
                                                                                  FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CGetSweepStateChannelB.COMMAND_TEXT:
            if (0 < Tokens.Length)
            {
              CGetSweepStateChannelB Statement = new CGetSweepStateChannelB(FStatementlist,
                                                                          FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CSetSweepStateChannelB.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              ESweepState SS = (ESweepState)Int16.Parse(Tokens[1]);
              CSetSweepStateChannelB Statement = new CSetSweepStateChannelB(FStatementlist, SS,
                                                                          FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CGetSweepParameterChannelB.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              WIndex = Int16.Parse(Tokens[1]);
              CGetSweepParameterChannelB Statement = new CGetSweepParameterChannelB(FStatementlist, WIndex,
                                                                                  FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
          case CSetSweepParameterChannelB.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              WIndex = Int16.Parse(Tokens[1]);
              WValue = Int16.Parse(Tokens[2]);
              CSetSweepParameterChannelB Statement = new CSetSweepParameterChannelB(FStatementlist, WIndex, WValue,
                                                                                  FNotifier, FDevice, FComPort);
              FStatementlist.Add(Statement);
              return true;
            }
            break;
        }
      }
      return false;
    }


    protected Boolean SendLine(String line)
    { // first: Analyse if command-token is in this line
      if (!AnalyseSendStatement(line))
      {
        return FComPort.WriteLine(line);
      }
      return false;
    }

    //
    //--------------------------------------
    //	Section - Statement - Synchronisation
    //--------------------------------------
    //
    public Boolean StatementsFinished()
    {
      return (0 == FStatementlist.Count);
    }


    public static ESweepState StringStateToSweepState(String textstate)
    {
      if (textstate == ESweepState.Idle.ToString())
      {
        return ESweepState.Idle;
      }
      if (textstate == ESweepState.LowHighLowHigh.ToString())
      {
        return ESweepState.LowHighLowHigh;
      }
      if (textstate == ESweepState.HighLowHighLow.ToString())
      {
        return ESweepState.HighLowHighLow;
      }
      if (textstate == ESweepState.LowHighHighLow.ToString())
      {
        return ESweepState.LowHighHighLow;
      }
      if (textstate == ESweepState.HighLowLowHigh.ToString())
      {
        return ESweepState.HighLowLowHigh;
      }
      return ESweepState.Idle;
    }

    public static ESweepState StringIndexToSweepState(String textindex)
    {
      try
      {
        ESweepState SS = (ESweepState)Int16.Parse(textindex);
        return SS;
      }
      catch (Exception)
      {
        return ESweepState.Idle;
      }
    }




    /*/
    //------------------------------------------------------------------------
    //  Section - Callback - FUCHWComPort -> Main????
    //------------------------------------------------------------------------
    /*/ 
    private void HWComPortOnDataReceived(Guid comportid, String data)
    {
      CStatement Statement = FStatementlist.GetStatementActual();
      if (Statement is CStatement)
      {
        Statement.ExecuteDataReceived(data);
      }
      if (FOnComPortDataReceived is DOnComPortDataReceived)
      {
        FOnComPortDataReceived(data);
      }
    }

    private void HWComPortOnDataTransmitted(Guid comportid, String data)
    {
      if (FOnComPortDataTransmitted is DOnComPortDataTransmitted)
      {
        FOnComPortDataTransmitted(data);
      }
    }

    private void HWComPortOnErrorDetected(Guid comportid, Int32 errorcode, String errortext)
    {
    }
    /*/
    //--------------------------------------------------------------------------------------
    //  Section - Public Management
    //--------------------------------------------------------------------------------------
    /*/
    public Boolean OpenSerial(String portname,
                              EComPort comport,
                              EBaudrate baudrate,
                              EParity parity,
                              EDatabits databits,
                              EStopbits stopbits,
                              EHandshake handshake)
    {
      //HWComPort.O
      return false;
    }

    public Boolean Close()
    {
      RComPortData ComPortData;
      FComPort.GetComPortData(out ComPortData);
      ComPortData.OnDataReceived = null;
      ComPortData.OnDataTransmitted = null;
      ComPortData.OnErrorDetected = null;
      FComPort.SetComPortData(ComPortData);
      //
      // !!!!!!!!!!EnableComPortControls(true);
      //
      //!!!!!!FUCOpenClose.SetOnOpenCloseChanged(null);
      //
      // Problem!!!! HWComPortClose();
      return false;
    }
*/






/* |||||||||||||||||||||||||||||||<<<
    private Boolean HWComPortOpen()
    {
      FUCTerminal.Clear();
      FUCTerminal.EnableText(false);
      //
      Boolean Result = false;
      RComPortData ComPortData;
      Result = FComPort.GetComPortData(out ComPortData);
      ComPortData.Name = FUCPortName.GetPortName();
      ComPortData.ComPort = FUCPortsSelectable.GetPortSelected();
      ComPortData.Baudrate = FUCBaudrate.GetBaudrate();
      ComPortData.Parity = FUCParity.GetParity();
      ComPortData.Databits = FUCDatabits.GetDatabits();
      ComPortData.Stopbits = FUCStopbits.GetStopbits();
      ComPortData.Handshake = FUCHandshake.GetHandshake();
      Result &= FComPort.Open(ComPortData);
      Result &= FComPort.IsOpen();
      if (Result)
      {
        EnableComPortControls(false);
        FUCOpenClose.SetStateOpened(true);
        FUCNotifier.Write("ComPort correct opened.");
      }
      else
      {
        FUCOpenClose.SetStateOpened(false);
        int EI = (int)EErrorCode.CannotOpenComPort;
        FUCNotifier.Error(HEADER_LIBRARY, EI, ERRORS[EI]);
      }
      return Result;
    }

    private Boolean HWComPortClose()
    {
      FUCTerminal.EnableText(true);
      //
      Boolean Result = FComPort.Close();
      if (Result)
      {
        EnableComPortControls(true);
        FUCOpenClose.SetStateOpened(false);
        FUCNotifier.Write("ComPort correct closed.");
      }
      else
      {
        FUCOpenClose.SetStateOpened(false);
        int EI = (int)EErrorCode.CannotCloseComPort;
        FUCNotifier.Error(HEADER_LIBRARY, EI, ERRORS[EI]);
      }
      return Result;
    }

    private Boolean HWComPortWriteLine(String line)
    {
      Boolean Result = FComPort.WriteLine(line);
      return Result;
    }
*/


    //
    //--------------------------------------------------------------------------------------
    //  Section - Public Management - Statement - Common
    //--------------------------------------------------------------------------------------
    /*/
    protected Boolean StatementGetHelp()
    {
      CGetHelp Statement = new CGetHelp(FStatementlist, FNotifier,
                                      FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }

    protected Boolean StatementGetSoftwareVersion()
    {
      CGetSoftwareVersion Statement = new CGetSoftwareVersion(FStatementlist, FNotifier,
                                                            FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }

    protected Boolean StatementGetHardwareVersion()
    {
      CGetHardwareVersion Statement = new CGetHardwareVersion(FStatementlist, FNotifier,
                                                            FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }


    public Boolean StatementWriteCharacter(Char character)
    {
      return false;
    }





    public Boolean StatementTransmitTextDelay(String data,
                                            Int32 delaycharacter,
                                            Int32 delaycr,
                                            Int32 delaylf)
    {
      CTransmitTextDelay Statement = new CTransmitTextDelay(FStatementlist, FNotifier,
                                                          FDevice, FComPort,
                                                          data,
                                                          delaycharacter,
                                                          delaycr,
                                                          delaylf);
      FStatementlist.Add(Statement);
      return true;
    }
    */

    /*
    public Boolean StatementOpenSerial(String portname,
                                     EComPort comport,
                                     EBaudrate baudrate,
                                     EParity parity,
                                     EDatabits databits,
                                     EStopbits stopbits,
                                     EHandshake handshake)
    {
      COpenSerial Statement = new COpenSerial(FStatementlist, FNotifier,
                                            FDevice, FComPort,
                                            portname,
                                            comport,
                                            baudrate,
                                            parity,
                                            databits,
                                            stopbits,
                                            handshake);
      FStatementlist.Add(Statement);
      return true;
    }

    public Boolean StatementCloseSerial()
    {
      CCloseSerial Statement = new CCloseSerial(FStatementlist, FNotifier,
                                              FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }


    public Boolean StatementSerialPortsAvailable()
    {
      CSerialPortsAvailable Statement = new CSerialPortsAvailable(FStatementlist, FNotifier,
                                                                FDevice, FComPort);
      FStatementlist.Add(Statement);
      return true;
    }


    public Boolean StatementCheckSerialResponse(String baudrate, String command, String response)
    {
      CCheckSerialResponse Statement = new CCheckSerialResponse(FStatementlist, FNotifier,
                                                              FDevice, FComPort,
                                                              baudrate,
                                                              command, 
                                                              response);
      FStatementlist.Add(Statement);
      return true;
    }
    */
/*
    private Boolean SelfOnReadStatementBlock(XmlNode nodechild,
                                           ref CStatementBlock commandblock)
    {
      Boolean Result = false;
      String Name = nodechild.Name;
      switch (Name)
      {
        case CXmlHeaderTerminal.NAME_SQUARE:
          CStatementSquare StatementSquare;
          Result = FXmlReaderTerminal.ReadStatementSquare(nodechild, out StatementSquare);
          if (Result)
          {
            Result &= (StatementSquare is CStatementSquare);
            if (Result)
            {
              commandblock.AddStatement(StatementSquare);
            }
          }
          break;
        default:
          Result = false;
          break;
      }
      return Result;
    }



    public Boolean LoadProgramFromXml(String programentry)
    {
      try
      {
        FProgram = new CProgram();
        FProgram.SetNotifier(FNotifier);
        FProgram.SetOnReadStatementBlock(SelfOnReadStatementBlock);
        FProgram.LoadFromXml(programentry);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Translate()
    {
      try
      {
        if (FProgram is CProgram)
        {
          return FProgram.Translate();
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Execute()
    {
      try
      {
        if (FProgram is CProgram)
        {
          return FProgram.Execute();
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }


    public void DebugConstantlist()
    {
      FProgram.DebugConstantlist();
    }

    public void DebugVariablelist()
    {
      FProgram.DebugVariablelist();
    }
    */

  }
}
