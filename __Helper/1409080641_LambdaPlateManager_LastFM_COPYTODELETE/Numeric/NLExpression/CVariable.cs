﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace NLExpression
{

  public enum ETypeVariable
  {
    String = 0,
    Boolean = 1,
    Byte = 2,
    Int16 = 3,
    Int32 = 4,
    Double = 5
  };


  public abstract class CVariable
  { //
		//	Types
		public const String NAME_STRING = "String";
		public const String INIT_STRING = "";
		public const String NAME_BOOLEAN = "Boolean";
		public const Boolean INIT_BOOLEAN = false;
		public const String NAME_BYTE = "Byte";
		public const Byte INIT_BYTE = 0x00;
		public const String NAME_INT16 = "Int16";
		public const Int32 INIT_INT16 = 0;
		public const String NAME_INT32 = "Int32";
		public const Int32 INIT_INT32 = 0;
		public const String NAME_DOUBLE = "Double";
		public const Double INIT_DOUBLE = 0.0;

    protected CNotifier FNotifier;
    private String FName;
		protected ETypeVariable FType;

    public CVariable(String name)
    {
      FName = name;
    }


    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

		public String Name
		{
			get { return FName; }
		}
		public ETypeVariable Type
		{
			get { return FType; }
		}


    public Boolean IsDefined()
    {
      return true;
    }


		public abstract Double Get();
		public abstract void Set(Double value);

		public abstract String GetValueString();
		public abstract Boolean GetValueBoolean();
		public abstract Byte GetValueByte();
		public abstract Int16 GetValueInt16();
		public abstract Int32 GetValueInt32();
		public abstract Double GetValueDouble();


		public abstract String GetPresetString();
		public abstract Boolean GetPresetBoolean();
		public abstract Byte GetPresetByte();
		public abstract Int16 GetPresetInt16();
		public abstract Int32 GetPresetInt32();
		public abstract Double GetPresetDouble();

    public abstract void Debug();
	}


  public class CVariableDouble : CVariable
  {
    private String FExpressionText;
    private CExpression FExpression;
		private Double FPreset;
		private Double FValue;

		public Double Preset
		{
			get { return FPreset; }
			set { FPreset = value; }
		}

    public CVariableDouble(String name, String expressiontext)
      : base(name)
    {
      FType = ETypeVariable.Double;
      FExpressionText = expressiontext;
      FExpression = null;
			FPreset = 0.0;
			FValue = 0.0;
		}

    public Boolean Translate(ref CConstantlist constantlist,
                             ref CVariablelist variablelist)
    {
      FExpression = new CExpression(ref constantlist, ref variablelist);
      return FExpression.Translate(FExpressionText);
    }

		public override Double Get()
		{
			return FValue;
		}
		public override void Set(double value)
		{
			FValue = value;
		}

		public override String GetValueString()
		{
			return (String)FValue.ToString();
		}
		public override Boolean GetValueBoolean()
		{
			return (Boolean)(0.0 != FValue);
		}
		public override Byte GetValueByte()
		{
			return (Byte)FValue;
		}
		public override Int16 GetValueInt16()
		{
			return (Int16)FValue;
		}
		public override Int32 GetValueInt32()
		{
			return (Int32)FValue;
		}
		public override Double GetValueDouble()
		{
			return (Double)FValue;
		}

		public override String GetPresetString()
		{
			return (String)FPreset.ToString();
		}
		public override Boolean GetPresetBoolean()
		{
			return (Boolean)(0.0 != FPreset);
		}
		public override Byte GetPresetByte()
		{
			return (Byte)FPreset;
		}
		public override Int16 GetPresetInt16()
		{
			return (Int16)FPreset;
		}
		public override Int32 GetPresetInt32()
		{
			return (Int32)FPreset;
		}
		public override Double GetPresetDouble()
		{
			return (Double)FPreset;
		}

    public override void Debug()
    {
      String Line = String.Format("  VariableDouble[{0}]: {1}", Name, GetValueDouble().ToString());
      FNotifier.Write(Line);
    }

	}

}
