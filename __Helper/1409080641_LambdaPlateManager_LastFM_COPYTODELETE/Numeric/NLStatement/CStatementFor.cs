﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
//
namespace NLStatement
{

	// <for> <commandset> <conditionboolean> <commandchange> <block>

	public class CStatementFor : CStatement
	{
		private CVariablelist FVariablelist;
		private String FVariableName;
		private String FTextExpression;
		private CExpression FExpression;
		private String FTextCondition;
		private CExpression FCondition;
		private String FTextIncrement;
		private CExpression FIncrement;
		private CStatementBlock FStatementBlock;

    public CStatementFor(//CVariablelist variablelist,
											 String variablename,
											 String textexpression,
											 String textcondition,
											 String textincrement,
											 CStatementBlock statementblock)
      : base(CStatementHeader.NAME_FOR)
    {
      //SetOnStatementExecuteBegin(null);
      //SetOnStatementExecuteBusy(ThreadExecuteBusy);
      //SetOnStatementExecuteResponse(null);
      //SetOnStatementExecuteEnd(null);
      //
      FVariablelist = null;
			FVariableName = variablename;
			FTextExpression = textexpression;
			FExpression = null;
			FTextCondition = textcondition;
			FCondition = null;
			FTextIncrement = textincrement;
			FIncrement = null;
			FStatementBlock = statementblock;
    }


		public String VariableName
		{
			get { return FVariableName; }
		}

		public String Expression
		{
			get { return FTextExpression; }
		}

		public String Condition
		{
			get { return FTextCondition; }
		}

		public String Increment
		{
			get { return FTextIncrement; }
		}

		public CStatementBlock StatementBlock
		{
			get { return FStatementBlock; }
		}





    public override Boolean Translate(ref CConstantlist constantlist,
                                         ref CVariablelist variablelist)
    {
			try
			{
        String Line = String.Format("Translate For[{0} = {1}; {2}; {3}] {{4}}", 
																		FVariableName, FTextExpression, FTextCondition, 
																		FTextIncrement, FStatementBlock.ToString());
        Notifier.Write(Line);
				//
				FExpression = new CExpression(ref constantlist,
                                      ref variablelist);
				FExpression.Translate(FTextExpression);
				//
				FCondition = new CExpression(ref constantlist,
                                     ref variablelist);
				FCondition.Translate(FTextCondition);
				//
				FIncrement = new CExpression(ref constantlist,
                                     ref variablelist);
				FIncrement.Translate(FTextIncrement);
				//
				if (FStatementBlock is CStatement)
				{
					FStatementBlock.Translate(ref constantlist,
                                  ref variablelist);
				}
        FVariablelist = variablelist;
        return true;
			}
			catch (Exception)
			{
				return false;
			}
    }

    public Boolean ThreadExecuteBusy()
    {     
      String Line;
			Double DValue = 0.0;
			if (!FExpression.Calculate(ref DValue))
			{
				return false;
			}
			CVariable Variable = FVariablelist.FindName(FVariableName);
			if (!(Variable is CVariable))
			{
				return false;
			}
			Variable.Set(DValue);
			if (!FCondition.Calculate(ref DValue))
			{
				return false;
			}
			Boolean ForActive = (0 != (Int32)DValue);
			while (ForActive)
			{
				Line = String.Format("Execute For[{0} = {1}; {2}; {3}]({4}) -> {5}",
														 FVariableName, FTextExpression, FTextCondition,
														 FTextIncrement, Variable.Get(), ForActive);
				Notifier.Write(Line);
				if (!FStatementBlock.StartExecution(null))
				{
					return false;
				}
				// Calculate next Step
				if (!FIncrement.Calculate(ref DValue))
				{
					return false;
				}
				Variable.Set(DValue);
				//
				if (!FCondition.Calculate(ref DValue))
				{
					return false;
				}
				ForActive = (0 != (Int32)DValue);
			}
      Line = String.Format("Execute For[{0} = {1}; {2}; {3}]({4}) -> {5}", 
                           FVariableName, FTextExpression, FTextCondition,
													 FTextIncrement, Variable.Get(), ForActive);
      Notifier.Write(Line);
      return true;
    }


    /*public override Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines)
    {
      String Line = BuildIdentation(identation);
      Line += String.Format("for ({0} = {1}; {2}; {3} = {4})",
                            FVariableName, FTextExpression,
                            FTextCondition, FVariableName, FTextIncrement);
      lines.Add(Line);
      //
      Line = BuildIdentation(identation) + "{";
      lines.Add(Line);
      //
      identation++;
      if (FStatementBlock is CStatement)
      {
        FStatementBlock.WriteToAscii(ref identation, ref lines);
      }
      identation--;
      //
      Line = BuildIdentation(identation) + "}";
      lines.Add(Line);
      //
      return true;
    }*/


  }
}
