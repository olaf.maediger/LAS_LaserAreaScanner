﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
//
namespace NLStatement
{

  // <while> <expressionboolean> <do> <statement>

  public class CStatementWhile : CStatement
  {
		private CVariablelist FVariablelist;
    private String FTextExpression;
    private CExpression FExpression;
		private CStatementBlock FStatementBlock;

    public CStatementWhile(CVariablelist variablelist,
												 String textexpression,                      
                         CStatementBlock statementblock)
			: base(CStatementHeader.NAME_WHILE)
    {
      //SetOnStatementExecuteBegin(null);
      //SetOnStatementExecuteBusy(ThreadExecuteBusy);
      //SetOnStatementExecuteResponse(null);
      //SetOnStatementExecuteEnd(null);
      //
      FVariablelist = variablelist;
      FTextExpression = textexpression;
      FExpression = null;
      FStatementBlock = statementblock;
    }



		public String Expression
		{
			get { return FTextExpression; }
		}

		public CStatementBlock StatementBlock
		{
			get { return FStatementBlock; }
		}




    public override Boolean Translate(ref CConstantlist constantlist,
                                      ref CVariablelist variablelist)
    {
			try
			{
				String Line = String.Format("Translate While['{0}' {1}]", FTextExpression, FStatementBlock.ToString());
        Notifier.Write(Line);
        FExpression = new CExpression(ref constantlist,
                                      ref variablelist);
				FExpression.Translate(FTextExpression);
				//
				if (FStatementBlock is CStatement)
				{
          FStatementBlock.Translate(ref constantlist,
                                  ref variablelist);
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
    }

    public Boolean ThreadExecuteBusy()
    {
      Boolean WhileActive = true;
      Double DValue = 0.0;
      String Line;
      while (WhileActive)
      {
        if (!FExpression.Calculate(ref DValue))
        {
          return false;
        }
				Line = String.Format("Execute While[{0} {1}]", WhileActive, FStatementBlock.ToString());
        Notifier.Write(Line);
        WhileActive = (0 != (Int32)DValue);
        if (WhileActive)
        { // Expression true ->
					if (!FStatementBlock.StartExecution(null))
          {
            return false;
          }
        } 
      }
			Line = String.Format("Execute While[{0}]", WhileActive);
      Notifier.Write(Line);
      return true;
    }

    /*public override Boolean WriteToAscii(ref Int32 identation, ref CLinelist lines)
    {
      String Line = BuildIdentation(identation);
      Line += String.Format("while ({0})", FTextExpression);
      lines.Add(Line);
      //
      Line = BuildIdentation(identation) + "{";
      lines.Add(Line);
      //
      identation++;
      if (FStatementBlock is CStatementBlock)
      {
        FStatementBlock.WriteToAscii(ref identation, ref lines);
      }
      identation--;
      //
      Line = BuildIdentation(identation) + "}";
      lines.Add(Line);
      //
      return true;
    }*/


  }
}
