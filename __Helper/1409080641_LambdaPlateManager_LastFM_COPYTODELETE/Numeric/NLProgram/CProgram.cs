﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
//
using UCNotifier;
//
using NLExpression;
using NLStatement;
//
using XmlFile;
//
namespace NLProgram
{
  public class CProgram
  {
    private CNotifier FNotifier;
    private String FName;
    // Program Translation -> Constant/Variable/Expression/Statement-Lists
    private CConstantlist FConstantlist;
    private CVariablelist FVariablelist;
    private CExpression FExpression;
    // Thread-controlled execution of commands (from Program)
    private CStatementBlock FStatementBlock; 


    private DOnReadStatementBlock FOnReadStatementBlock;


    public CProgram()
    {
      CreateInstances();
    }

    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      //!!!!!!!!!!!FConstantlist.SetNotifier(FNotifier);
      FVariablelist.SetNotifier(FNotifier);
      FExpression.SetNotifier(FNotifier);
      FStatementBlock.SetNotifier(FNotifier);      
    }

    public void SetOnReadStatementBlock(DOnReadStatementBlock value)
    {
      FOnReadStatementBlock = value;
    }


    private void CreateInstances()
    {
      FConstantlist = new CConstantlist();
      FConstantlist.SetNotifier(FNotifier);
      FVariablelist = new CVariablelist();
      FVariablelist.SetNotifier(FNotifier);
      FExpression = new CExpression(ref FConstantlist, ref FVariablelist);
      FExpression.SetNotifier(FNotifier);
      FStatementBlock = new CStatementBlock();// ("StatementBlock");//ref FConstantlist, ref FVariablelist);
      FStatementBlock.SetNotifier(FNotifier);
    }

    public Boolean LoadFromXml(String programentry)
    {
      Boolean Result = true;
      //
      CreateInstances();
      //
      CXmlReader XmlReader = new CXmlReader();
      XmlReader.SetNotifier(FNotifier);
      XmlReader.SetOnReadStatementBlock(SelfOnReadStatementBlock);
      FName = Path.GetFileNameWithoutExtension(programentry);
     /*!!!!!!!!!!!!!!! XmlReader.Load(programentry,
                     ref FConstantlist,
                     ref FVariablelist,
                     ref FExpression,
                     ref FStatementBlock);
      */

      // NC ???CXmlReader XmlReader = new CXmlReader(programentry);
      //return FStatementlist.LoadProgramFromXml(programentry);
      /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FExpression = new CExpression();
      FStatement = new CStatement(FExpression.Variablelist);
      FStatement.SetNotifier(FNotifier);
       */
      //????????????????????????FStatement.SetOnReadStatementBlock(SelfOnReadStatementBlock);
      //????????????return 
      return Result;
    }


    private void StatementOnDeleteOnExecutionEnd(CStatement statement)
    {
      FStatementBlock.DeleteStatement(statement);
    }

    private Boolean SelfOnReadStatementBlock(XmlNode nodebase,
                                           ref CStatement commandblock)
    {
      if (FOnReadStatementBlock is DOnReadStatementBlock)
      {
        return FOnReadStatementBlock(nodebase, ref commandblock);
      }
      return false;
    }

    public Boolean Translate()
    {
      FConstantlist.Clear();
      FVariablelist.Clear();
      return FStatementBlock.Translate(ref FConstantlist, ref FVariablelist);
    }

    /*public Boolean Execute()
    {
      return FStatement.Execute();
    }*/

    public void DebugConstantlist()
    {
      FConstantlist.Debug();
    }

    public void DebugVariablelist()
    {
      FVariablelist.Debug();
    }


    public Boolean AddStatement(CStatement statement)
    {
      statement.SetNotifier(FNotifier);
      switch (statement.ExecutionKind)
      {
        case EExecutionKind.KeepAlive:
          statement.SetOnDeleteOnExecutionEnd(null);
          break;
        case EExecutionKind.DeleteAfterExecution:
          statement.SetOnDeleteOnExecutionEnd(StatementOnDeleteOnExecutionEnd);
          break;
      }
      return FStatementBlock.AddStatement(statement);
    }

    public Boolean StartExecution()
    {
      return FStatementBlock.StartExecution(null);
    }

  }
}
