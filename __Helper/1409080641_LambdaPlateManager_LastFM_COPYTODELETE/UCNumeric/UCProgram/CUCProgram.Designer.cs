﻿namespace UCProgram
{
  partial class CUCProgram
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.btnAddDelay = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(16, 15);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(132, 25);
      this.label1.TabIndex = 0;
      this.label1.Text = "UCProgram";
      // 
      // btnAddDelay
      // 
      this.btnAddDelay.Location = new System.Drawing.Point(21, 72);
      this.btnAddDelay.Name = "btnAddDelay";
      this.btnAddDelay.Size = new System.Drawing.Size(127, 23);
      this.btnAddDelay.TabIndex = 1;
      this.btnAddDelay.Text = "Add Delay";
      this.btnAddDelay.UseVisualStyleBackColor = true;
      this.btnAddDelay.Click += new System.EventHandler(this.btnAddDelay_Click);
      // 
      // CUCProgram
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.Controls.Add(this.btnAddDelay);
      this.Controls.Add(this.label1);
      this.Name = "CUCProgram";
      this.Size = new System.Drawing.Size(693, 615);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btnAddDelay;
  }
}
