﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCRotator
{ //
  //------------------------------------------------------------------
  //  Segment - Type - Global
  //------------------------------------------------------------------
  //
  public delegate void DOnDoubleChanged(object sender, Double value);
  //
  //####################################################################
  //  Segment - Main - CUCEditDouble
  //####################################################################
  //
  public partial class CUCEditDouble : UserControl
  { //
    //------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------
    //
    private DOnDoubleChanged FOnValueChanged;
    //
    //------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------
    //
    public CUCEditDouble()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------
    //
    public String Header
    {
      get { return lblHeader.Text; }
      set { lblHeader.Text = value; }
    }
    public Int32 HeaderWidth
    {
      get { return lblHeader.Width; }
      set { lblHeader.Width = value; }
    }

    public String Unit
    {
      get { return lblUnit.Text; }
      set { lblUnit.Text = value; }
    }

    public UInt32 DecimalPlaces
    {
      get { return (UInt32)nudValue.DecimalPlaces; }
      set { nudValue.DecimalPlaces = (Int32)value; }
    }

    private void SetValue(Double value)
    {
      if (value != (Double)nudValue.Value)
      {
        if ((nudValue.Minimum <= (Decimal)value) && 
            ((Decimal)value <= nudValue.Maximum))
        {
          nudValue.Value = (Decimal)value;
        }
      }
    }
    public Double Value
    {
      get { return (Double)nudValue.Value; }
      set { SetValue(value); }
    }

    public Double ValueMinimum
    {
      get { return (Double)nudValue.Minimum; }
      set { nudValue.Minimum = (Decimal)value; }
    }

    public Double ValueMaximum
    {
      get { return (Double)nudValue.Maximum; }
      set { nudValue.Maximum = (Decimal)value; }
    }

    public Double ValueDelta
    {
      get { return (Double)nudValue.Increment; }
      set { nudValue.Increment = (Decimal)value; }
    }

    public Int32 ValueWidth
    {
      get { return nudValue.Width; }
      set { nudValue.Width = value; }
    }

    public void SetOnValueChanged(DOnDoubleChanged onvaluechanged)
    {
      FOnValueChanged = onvaluechanged;
    }
    //
    //------------------------------------------------------------------
    //  Segment - Event
    //------------------------------------------------------------------
    //
    private void nudValue_ValueChanged(object sender, EventArgs e)
    {
      if (sender is NumericUpDown)
      {
        Double DValue = (Double)((NumericUpDown)sender).Value;
        if (FOnValueChanged is DOnDoubleChanged)
        {
          FOnValueChanged(this, DValue);
        }
      }
    }


  }
}
