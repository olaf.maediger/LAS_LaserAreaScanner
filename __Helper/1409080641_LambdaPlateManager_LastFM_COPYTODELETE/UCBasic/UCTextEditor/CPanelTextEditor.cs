﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using Programdata;
using UCNotifier;
//
namespace UCTextEditor
{
  public enum EStateEditor
  {
    Hide = 0,
    Show = 1,
    Edit = 2
  }

  public partial class CPanelTextEditor : Panel
  { //
    //-----------------------------------------------
    //  Section - Constant
    //-----------------------------------------------
    //	
    public static String HEADER_LIBRARY = "PanelTextEditor";
    public static String SECTION_LIBRARY = HEADER_LIBRARY;
    //
    private const String NAME_STATEEDITOR = "StateEditor";
    //
    private const EStateEditor INIT_STATEEDITOR = EStateEditor.Hide;
    private const Boolean INIT_VISIBLE = true;
    private const Int32 INIT_LEFT = 10;
    private const Int32 INIT_TOP = 10;
    private const Int32 INIT_WIDTH = 300;
    private const Int32 INIT_WIDTH_MINIMUM = 180;
    private const Int32 INIT_HEIGHT = 200;
    private const Int32 INIT_HEIGHT_MINIMUM = 80;
    //
    private const Int32 INIT_BORDERWIDTH = 4;
    private const Int32 INIT_BORDERHEIGHT = 4;
    //
    public static readonly String[] STATESEDITOR =
    {
      "Hide",
      "Show",
      "Edit"
    };
    //
    //------------------------------------------------------------------
    //  Segment Field
    //------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private Panel FPanelTop;
    private Panel FPanelLeft;
    private Panel FPanelRight;
    private Panel FPanelBottom;
    private CUCEditTextEditor FUCTextEditor;
    private Boolean FKeyPressed;
    private Point FPointPressed;
    private DOnDisplayProjection FOnDisplayProjection;
    private DOnHide FOnHide;
    private EStateEditor FStateEditor;
    //
    //------------------------------------------------------------------
    //  Segment Constructor
    //------------------------------------------------------------------
    //
    public CPanelTextEditor()
    {
      InitializeComponent();
      //
      InitControls();
    }
    //
    //------------------------------------------------------------------
    //  Segment Property
    //------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetBorderStyle(BorderStyle value)
    {
      FUCTextEditor.BorderStyle = value;
    }

    public void SetOnDataChanged(DOnDataChanged value)
    {
      RTextEditorData TextEditorData;
      FUCTextEditor.GetData(out TextEditorData);
      TextEditorData.OnDataChanged = value;
      FUCTextEditor.SetData(TextEditorData);
    }

    public void SetOnDisplayProjection(DOnDisplayProjection value)
    {
      FOnDisplayProjection = value;
    }

    public void SetOnHide(DOnHide value)
    {
      FOnHide = value;
    }

    public EStateEditor GetStateEditor()
    {
      return FStateEditor;
    }
     
    public void SetStateEditor(EStateEditor value)
    {
      switch (value)
      {
        case EStateEditor.Hide:
          FStateEditor = EStateEditor.Hide;
          Hide();
          FUCTextEditor.IsReadOnly = true;
          break;
        case EStateEditor.Edit:
          FStateEditor = EStateEditor.Edit;
          FUCTextEditor.IsReadOnly = false;
          Show();
          break;
        case EStateEditor.Show:
          FStateEditor = EStateEditor.Show;
          FUCTextEditor.IsReadOnly = true;
          Hide();
          break;
      }
    }

    public String GetText()
    {
      return FUCTextEditor.GetText();
    }

    public Font GetFont()
    {
      return FUCTextEditor.GetFont();
    }

    public Color GetFontColor()
    {
      return FUCTextEditor.GetFontColor();
    }
    //
    //------------------------------------------------------------------
    //  Segment Initialisation
    //------------------------------------------------------------------
    //
    private void InitControls()
    {
      BackColor = Color.FromArgb(0xF5DEB3);
      //
      FUCTextEditor = new CUCEditTextEditor();
      RTextEditorData TextEditorData;
      FUCTextEditor.GetData(out TextEditorData);
      TextEditorData.OnDataChanged = null;
      TextEditorData.OnDisplayProjection = TextEditorOnDisplayProjection;
      TextEditorData.OnHide = TextEditorOnHide;
      FUCTextEditor.SetData(TextEditorData);
      //      
      FPanelTop = new Panel();
      FPanelTop.Height = INIT_BORDERHEIGHT;
      FPanelTop.BackColor = Color.Red;
      FPanelTop.MouseDown += new MouseEventHandler(CPanelTextEditor_MouseDown);
      FPanelTop.MouseMove += new MouseEventHandler(CPanelTextEditor_MouseMoveLocation);
      FPanelTop.MouseUp += new MouseEventHandler(CPanelTextEditor_MouseUpLocation);
      FPanelTop.MouseLeave += new EventHandler(CPanelTextEditor_MouseLeave);
      FPanelLeft = new Panel();
      FPanelLeft.Width = INIT_BORDERWIDTH;
      FPanelLeft.BackColor = Color.Red;
      FPanelLeft.MouseDown += new MouseEventHandler(CPanelTextEditor_MouseDown);
      FPanelLeft.MouseMove += new MouseEventHandler(CPanelTextEditor_MouseMoveLocation);
      FPanelLeft.MouseUp += new MouseEventHandler(CPanelTextEditor_MouseUpLocation);
      FPanelLeft.MouseLeave += new EventHandler(CPanelTextEditor_MouseLeave);
      FPanelRight = new Panel();
      FPanelRight.Width = INIT_BORDERWIDTH;
      FPanelRight.BackColor = Color.Red;
      FPanelRight.MouseDown += new MouseEventHandler(CPanelTextEditor_MouseDown);
      FPanelRight.MouseMove += new MouseEventHandler(CPanelTextEditor_MouseMoveWidth);
      FPanelRight.MouseUp += new MouseEventHandler(CPanelTextEditor_MouseUpWidth);
      FPanelRight.MouseLeave += new EventHandler(CPanelTextEditor_MouseLeave);
      FPanelBottom = new Panel();
      FPanelBottom.Height = INIT_BORDERHEIGHT;
      FPanelBottom.BackColor = Color.Red;
      FPanelBottom.MouseDown += new MouseEventHandler(CPanelTextEditor_MouseDown);
      FPanelBottom.MouseMove += new MouseEventHandler(CPanelTextEditor_MouseMoveHeight);
      FPanelBottom.MouseUp += new MouseEventHandler(CPanelTextEditor_MouseUpHeight);
      FPanelBottom.MouseLeave += new EventHandler(CPanelTextEditor_MouseLeave);
      //
      this.Controls.Add(FUCTextEditor);
      this.Controls.Add(FPanelBottom);
      this.Controls.Add(FPanelLeft);
      this.Controls.Add(FPanelRight);
      this.Controls.Add(FPanelTop);
      FPanelLeft.Dock = DockStyle.Left;
      FPanelRight.Dock = DockStyle.Right;
      FPanelBottom.Dock = DockStyle.Bottom;
      FPanelTop.Dock = DockStyle.Top;
      FUCTextEditor.Dock = DockStyle.Fill;
      //
      SetStateEditor(EStateEditor.Hide);
    }
    //
    //-------------------------------
    //  Section - Load/Save Programdata
    //-------------------------------
    //
    public Boolean LoadProgramdata(CInitdataReader initdata,
                                String initfilename,
                                String initworkingdirectory,
                                Int32 inittabulatorwidth,
                                Boolean initshowpanelload,
                                Boolean initshowpanelsave,
                                Boolean initshowpanelparameter,
                                Boolean initshowpanelfont,
                                Boolean initshowlegend)
    {
      Int32 IValue;
      Boolean BValue;
      String SValue;
      Boolean Result = true;
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      Result &= initdata.ReadInt32(CInitdata.NAME_LEFT, out IValue, INIT_LEFT);
      this.Left = IValue;
      Result &= initdata.ReadInt32(CInitdata.NAME_TOP, out IValue, INIT_TOP);
      this.Top = IValue;
      Result &= initdata.ReadInt32(CInitdata.NAME_WIDTH, out IValue, INIT_WIDTH);
      this.Width = IValue;
      Result &= initdata.ReadInt32(CInitdata.NAME_HEIGHT, out IValue, INIT_HEIGHT);
      this.Height = IValue;
      //
      Result &= initdata.ReadBoolean(CInitdata.NAME_VISIBLE, out BValue, INIT_VISIBLE);
      this.Visible = BValue;
      //
      SValue = StateEditorToText(INIT_STATEEDITOR);
      Result &= initdata.ReadEnumeration(NAME_STATEEDITOR, out SValue, SValue);
      SetStateEditor(TextToStateEditor(SValue));
      //
      Result &= FUCTextEditor.LoadProgramdata(initdata,
                                           initfilename,
                                           initworkingdirectory,
                                           inittabulatorwidth,
                                           initshowpanelload,
                                           initshowpanelsave,
                                           initshowpanelparameter,
                                           initshowpanelfont,
                                           initshowlegend);
      //
      // NC Result &= 
      FUCTextEditor.LoadFromFile();
      //
      Result &= initdata.CloseSection();
      return Result;
    }

    public Boolean LoadProgramdata(CInitdataReader initdata)
    {
      Int32 IValue;
      Boolean BValue;
      String SValue;
      Boolean Result = true;
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      Result &= initdata.ReadInt32(CInitdata.NAME_LEFT, out IValue, INIT_LEFT);
      this.Left = IValue;
      Result &= initdata.ReadInt32(CInitdata.NAME_TOP, out IValue, INIT_TOP);
      this.Top = IValue;
      Result &= initdata.ReadInt32(CInitdata.NAME_WIDTH, out IValue, INIT_WIDTH);
      this.Width = IValue;
      Result &= initdata.ReadInt32(CInitdata.NAME_HEIGHT, out IValue, INIT_HEIGHT);
      this.Height = IValue;
      //
      Result &= initdata.ReadBoolean(CInitdata.NAME_VISIBLE, out BValue, INIT_VISIBLE);
      this.Visible = BValue;
      //
      SValue = StateEditorToText(INIT_STATEEDITOR);
      Result &= initdata.ReadEnumeration(NAME_STATEEDITOR, out SValue, SValue);
      SetStateEditor(TextToStateEditor(SValue));
      //
      Result &= FUCTextEditor.LoadProgramdata(initdata);
      //
      Result &= initdata.CloseSection();
      return Result;
    }

    public Boolean SaveProgramdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteBoolean(CInitdata.NAME_VISIBLE, this.Visible);
      Result &= initdata.WriteInt32(CInitdata.NAME_LEFT, this.Left);
      Result &= initdata.WriteInt32(CInitdata.NAME_TOP, this.Top);
      Result &= initdata.WriteInt32(CInitdata.NAME_WIDTH, this.Width);
      Result &= initdata.WriteInt32(CInitdata.NAME_HEIGHT, this.Height);
      //
      String SValue = StateEditorToText(GetStateEditor());
      Result &= initdata.WriteEnumeration(NAME_STATEEDITOR, SValue);
      //
      Result &= FUCTextEditor.SaveProgramdata(initdata);
      //
      Result &= initdata.CloseSection();
      return Result;
    }
    //
    //------------------------------------------------------------------
    //  Segment - Get / SetData
    //------------------------------------------------------------------
    //
    public Boolean GetTextEditorData(out RTextEditorData data)
    {
      return FUCTextEditor.GetData(out data);
    }

    public Boolean SetTextEditorData(RTextEditorData data)
    {
      return FUCTextEditor.SetData(data);
    }
    //
    //------------------------------------------------------------------
    //  Segment Event - Common
    //------------------------------------------------------------------
    //
    private void CPanelTextEditor_MouseDown(Object sender,
                                            MouseEventArgs arguments)
    {
      if (MouseButtons.Left == arguments.Button)
      {
        FKeyPressed = true;
        FPointPressed = arguments.Location;
      }
    }

    private void TextEditorOnHide()
    {
      Visible = false;
      if (FOnHide is DOnHide)
      {
        FOnHide();
      }
    }

    private void CPanelTextEditor_MouseLeave(Object sender,
                                             EventArgs arguments)
    {
      if (Cursors.Arrow != Cursor)
      {
        Cursor = Cursors.Arrow;
      }
    }
    //
    //------------------------------------------------------------------
    //  Segment Event - Location
    //------------------------------------------------------------------
    //
    private void CPanelTextEditor_MouseMoveLocation(Object sender,
                                                    MouseEventArgs arguments)
    {
      if (FKeyPressed)
      {
        Point P = new Point();
        this.Left += arguments.X - FPointPressed.X;
        this.Top += P.Y = arguments.Y - FPointPressed.Y;
      }
      else
      {
        FNotifier.Write(String.Format("{0} {1}", arguments.X, arguments.Y));
      }
      if (Cursors.SizeAll != Cursor)
      {
        Cursor = Cursors.SizeAll;
      }
    }

    private void CPanelTextEditor_MouseUpLocation(Object sender,
                                                  MouseEventArgs arguments)
    {
      FKeyPressed = false;
      // Check and reduce to Parent-Border
      Control Parent = (Control)this.GetContainerControl();
      if (Parent is Control)
      {
        Rectangle RP = Parent.ClientRectangle;
        Int32 L = Math.Max(RP.Left, this.Left);
        Int32 T = Math.Max(RP.Top, this.Top);
        this.Left = Math.Min(RP.Width - this.Width, L);
        this.Top = Math.Min(RP.Height - this.Height, T);
      }
    }
    //
    //------------------------------------------------------------------
    //  Segment Event - Width
    //------------------------------------------------------------------
    //
    private void CPanelTextEditor_MouseMoveWidth(Object sender,
                                                 MouseEventArgs arguments)
    {
      if (FKeyPressed)
      {
        this.Width += arguments.X - FPointPressed.X;
      }
      if (Cursors.SizeWE != Cursor)
      {
        Cursor = Cursors.SizeWE;
      }
    }

    private void CPanelTextEditor_MouseUpWidth(Object sender,
                                               MouseEventArgs arguments)
    {
      FKeyPressed = false;
      // Check and reduce to Parent-Border
      Control Parent = (Control)this.GetContainerControl();
      if (Parent is Control)
      {
        Rectangle RP = Parent.ClientRectangle;
        Int32 W = Math.Max(INIT_WIDTH_MINIMUM, this.Width);
        W = Math.Min(RP.Width, W);
        W = Math.Min(RP.Width - this.Left, W);
        this.Width = W;
      }
    }
    //
    //------------------------------------------------------------------
    //  Segment Height
    //------------------------------------------------------------------
    //
    private void CPanelTextEditor_MouseMoveHeight(Object sender,
                                                  MouseEventArgs arguments)
    {
      if (FKeyPressed)
      {
        Point P = new Point();
        this.Height += P.Y = arguments.Y - FPointPressed.Y;
      }
      if (Cursors.SizeNS != Cursor)
      {
        Cursor = Cursors.SizeNS;
      }
    }

    private void CPanelTextEditor_MouseUpHeight(Object sender,
                                                MouseEventArgs arguments)
    {
      FKeyPressed = false;
      // Check and reduce to Parent-Border
      Control Parent = (Control)this.GetContainerControl();
      if (Parent is Control)
      {
        Rectangle RP = Parent.ClientRectangle;
        Int32 H = Math.Max(INIT_HEIGHT_MINIMUM, this.Height);
        H = Math.Min(RP.Height, H);
        H = Math.Min(RP.Height - this.Top, H);
        this.Height = H;
      }
    }
    //
    //------------------------------------------------------------------
    //  Segment Callback - TextEditor
    //------------------------------------------------------------------
    //
    private void TextEditorOnDisplayProjection(RTextEditorData data)
    {
      // show much longer TextEditor!!! // NC Visible = false;
      if (FOnDisplayProjection is DOnDisplayProjection)
      {
        FOnDisplayProjection(data);
      }
    }

    /* not here!!! public Boolean RenderText(Graphics graphics,
                              Byte alpha, Byte red, Byte green, Byte blue)
    {
      return FUCTextEditor.RenderText( (graphics, alpha, red, green, blue);
    }*/
    //
    //------------------------------------------------------------------
    //  Segment Conversion - Static
    //------------------------------------------------------------------
    //
    public static String StateEditorToText(EStateEditor value)
    {
      switch (value)
      {
        case EStateEditor.Hide:
          return STATESEDITOR[(Int32)EStateEditor.Hide];
        case EStateEditor.Show:
          return STATESEDITOR[(Int32)EStateEditor.Show];
        case EStateEditor.Edit:
          return STATESEDITOR[(Int32)EStateEditor.Edit];
      }
      return STATESEDITOR[0];
    }

    public static EStateEditor TextToStateEditor(String value)
    {
      if (STATESEDITOR[(Int32)EStateEditor.Hide] == value)
      {
        return EStateEditor.Hide;
      }
      if (STATESEDITOR[(Int32)EStateEditor.Show] == value)
      {
        return EStateEditor.Show;
      }
      if (STATESEDITOR[(Int32)EStateEditor.Edit] == value)
      {
          return EStateEditor.Edit;
      }
      return EStateEditor.Hide;
    }

  }
}
