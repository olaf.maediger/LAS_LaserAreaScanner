using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using UCNotifier;

namespace HWComPort
{
	//
	//
	//------------------------------------------------------
	//
	//  Types
	//
	//------------------------------------------------------
	//
	public enum EErrorCode
	{
		None = 0,
		Child,
		OpenFailed,
		CloseFailed,
		ReadFailed,
		WriteFailed,
		AccessFailed,
		PortFailed,
		Invalid
	};

	public enum EPortSet
	{
		Possible = 0,
		Installed = 1,
		Selectable = 2
	};

  public enum EComPort
  {
    cp1 = 0,
    cp2 = 1,
    cp3 = 2,
    cp4 = 3,
    cp5 = 4,
    cp6 = 5,
    cp7 = 6,
    cp8 = 7,
    cp9 = 8,
    cp10 = 9,
    cp11 = 10,
    cp12 = 11,
    cp13 = 12,
    cp14 = 13,
    cp15 = 14,
    cp16 = 15,
    cp17 = 16,
    cp18 = 17,
    cp19 = 18,
    cp20 = 19,
    cp21 = 20,
    cp22 = 21,
    cp23 = 22,
    cp24 = 23,
    cp25 = 24,
    cp26 = 25,
    cp27 = 26,
    cp28 = 27,
    cp29 = 28,
    cp30 = 29,
    cp31 = 30,
    cp32 = 31,
    cp33 = 32,
    cp34 = 33,
    cp35 = 34,
    cp36 = 35,
    cp37 = 36,
    cp38 = 37,
    cp39 = 38,
    cp40 = 39,
    cp41 = 40,
    cp42 = 41,
    cp43 = 42,
    cp44 = 43,
    cp45 = 44,
    cp46 = 45,
    cp47 = 46,
    cp48 = 47,
    cp49 = 48,
    cp50 = 49,
    cp51 = 50,
    cp52 = 51,
    cp53 = 52,
    cp54 = 53,
    cp55 = 54,
    cp56 = 55,
    cp57 = 56,
    cp58 = 57,
    cp59 = 58,
    cp60 = 59,
    cp61 = 60,
    cp62 = 61,
    cp63 = 62,
    cp64 = 63,
    cp65 = 64,
    cp66 = 65,
    cp67 = 66,
    cp68 = 67,
    cp69 = 68,
    cp70 = 69,
    cp71 = 70,
    cp72 = 71,
    cp73 = 72,
    cp74 = 73,
    cp75 = 74,
    cp76 = 75,
    cp77 = 76,
    cp78 = 77,
    cp79 = 78,
    cp80 = 79,
    cp81 = 80,
    cp82 = 81,
    cp83 = 82,
    cp84 = 83,
    cp85 = 84,
    cp86 = 85,
    cp87 = 86,
    cp88 = 87,
    cp89 = 88,
    cp90 = 89,
    cp91 = 90,
    cp92 = 91,
    cp93 = 92,
    cp94 = 93,
    cp95 = 94,
    cp96 = 95,
    cp97 = 96,
    cp98 = 97,
    cp99 = 98,
    cp100 = 99,
    cp101 = 100,
    cp102 = 101,
    cp103 = 102,
    cp104 = 103,
    cp105 = 104,
    cp106 = 105,
    cp107 = 106,
    cp108 = 107,
    cp109 = 108,
    cp110 = 109,
    cp111 = 100,
    cp112 = 111,
    cp113 = 112,
    cp114 = 113,
    cp115 = 114,
    cp116 = 115,
    cp117 = 116,
    cp118 = 117,
    cp119 = 118,
    cp120 = 119,
    cp121 = 120,
    cp122 = 121,
    cp123 = 122,
    cp124 = 123,
    cp125 = 124,
    cp126 = 125,
    cp127 = 126,
    cp128 = 127
  };

	public enum EBaudrate
	{
		br110 = 110, br300 = 300, br600 = 600, br1200 = 1200,
		br2400 = 2400, br4800 = 4800, br9600 = 9600, br19200 = 19200,
		br38400 = 38400, br57600 = 57600, br115200 = 115200,
		br230400 = 230400, br460800 = 460800, br921600 = 921600, br1382400
	};

	public enum EParity
	{
		paNone = Parity.None,
		paEven = Parity.Even,
		paOdd = Parity.Odd,
		paMark = Parity.Mark,
		paSpace = Parity.Space
	};

	public enum EDatabits
	{
		db5 = 5,
		db6 = 6,
		db7 = 7,
		db8 = 8
	};

	public enum EStopbits
	{
		sb0 = StopBits.None,
		sb1 = StopBits.One,
		sb15 = StopBits.OnePointFive,
		sb2 = StopBits.Two
	};

	public enum EHandshake
	{
		hsNone = Handshake.None,                    // 0
		hsXONXOFF = Handshake.XOnXOff,              // 1
		hsRTSCTS = Handshake.RequestToSend,         // 2
		hsRTSXON = Handshake.RequestToSendXOnXOff   // 3
	};

	public enum EBufferAccess
	{
		baRead = 1,
		baWrite = 2,
		baReadWrite = 3
	};

  public delegate void DOnComPortDataChanged(Guid comportid,
                                             RComPortData comportdata);
  public delegate void DOnErrorDetected(Guid comportid,
                                        Int32 errorcode,
                                        String errortext);
  public delegate void DOnDataTransmitted(Guid comportid,
                                          String data);
  public delegate void DOnDataReceived(Guid comportid,
                                       String data);
  public delegate void DOnChildLineReceived(Guid comportid,
                                            String line);
  public delegate void DOnPinChanged(Guid comportid,
                                     RPinData data);

  // Get/Open
  public struct RComPortData
  {
    public Guid ID;                                     // RO
    public String Name;                                 // RW
    public EComPort ComPort;                            // RW
    public EBaudrate Baudrate;                          // RW
    public EParity Parity;                              // RW
    public EDatabits Databits;                          // RW
    public EStopbits Stopbits;                          // RW
    public Int32 RXDBufferSize;                         // RW - [Byte]
    public Int32 TXDBufferSize;                         // RW - [Byte]
    public EHandshake Handshake;                        // RW
    public Int32 TXDDelayCharacter;                     // RW - [ms]
    public Int32 TXDDelayCarriageReturn;                // RW - [ms]
    public Int32 TXDDelayLineFeed;                      // RW - [ms]
    public Int32 RXDBytesThreshold;                     // RW - [Byte]
    public Boolean TransmitEnabled;                     // RW - Softswitch
    public Boolean IsOpen;                              // RO
    public DOnComPortDataChanged OnComPortDataChanged;  // RW
    public DOnErrorDetected OnErrorDetected;            // RW
    public DOnDataTransmitted OnDataTransmitted;        // RW
    public DOnDataReceived OnDataReceived;              // RW
    public DOnChildLineReceived OnChildLineReceived;    // RW
    public DOnPinChanged OnPinChanged;                  // RW
    //
    public RComPortData(Int32 init)
    {
      ID = Guid.Empty;
      Name = CHWComPort.INIT_COMPORT.ToString();
      ComPort = CHWComPort.INIT_COMPORT;
      Baudrate = CHWComPort.INIT_BAUDRATE;
      Parity = CHWComPort.INIT_PARITY;
      Databits = CHWComPort.INIT_DATABITS;
      Stopbits = CHWComPort.INIT_STOPBITS;
      RXDBufferSize = CHWComPort.INIT_RXDBUFFERSIZE;
      TXDBufferSize = CHWComPort.INIT_TXDBUFFERSIZE;
      Handshake = CHWComPort.INIT_HANDSHAKE;
      TXDDelayCharacter = CHWComPort.INIT_TXDDELAYCHARACTER;
      TXDDelayCarriageReturn = CHWComPort.INIT_TXDDELAYCARRIAGERETURN;
      TXDDelayLineFeed = CHWComPort.INIT_TXDDELAYLINEFEED;
      RXDBytesThreshold = CHWComPort.INIT_RXDBYTESTHRESHOLD;
      TransmitEnabled = CHWComPort.INIT_TRANSMITENABLED;
      IsOpen = CHWComPort.INIT_ISOPEN;
      OnComPortDataChanged = null;
      OnErrorDetected = null;
      OnDataTransmitted = null;
      OnDataReceived = null;
      OnChildLineReceived = null;
      OnPinChanged = null;
    }
  }
  
  // Get/Set
  public struct RPinData
  {
    public Guid ID;
    public Boolean CTS;   // RO
    public Boolean RTS;   // RW
    public Boolean DSR;   // RO
    public Boolean DTR;   // RW
    public Boolean DCD;   // RO
  }


  public class CHWComPort : CHWComPortBase
	{
    //
    //------------------------------------------------------
    //  Constant
    //------------------------------------------------------
    //
    public static readonly String[] PORTSETS = 
    { 
			"Possible",
			"Installed",
			"Selectable"
		};

    public static readonly String[] COMPORTS = 
    { 
      "COM1",
			"COM2",
			"COM3",
			"COM4",
			"COM5",
			"COM6",
			"COM7",
			"COM8",
			"COM9",
			"COM10",
			"COM11",
			"COM12",
			"COM13",
			"COM14",
			"COM15",
			"COM16",
			"COM17",
			"COM18",
			"COM19",
			"COM20",
      "COM21",
			"COM22",
			"COM23",
			"COM24",
			"COM25",
			"COM26",
			"COM27",
			"COM28",
			"COM29",
			"COM30",
      "COM31",
			"COM32",
			"COM33",
			"COM34",
			"COM35",
			"COM36",
			"COM37",
			"COM38",
			"COM39",
      "COM40",
      "COM41",
			"COM42",
			"COM43",
			"COM44",
			"COM45",
			"COM46",
			"COM47",
			"COM48",
			"COM49",
      "COM50",
      "COM51",
			"COM52",
			"COM53",
			"COM54",
			"COM55",
			"COM56",
			"COM57",
			"COM58",
			"COM59",
      "COM60",
      "COM61",
			"COM62",
			"COM63",
			"COM64",
			"COM65",
			"COM66",
			"COM67",
			"COM68",
			"COM69",
      "COM70",
      "COM71",
			"COM72",
			"COM73",
			"COM74",
			"COM75",
			"COM76",
			"COM77",
			"COM78",
			"COM79",
      "COM80",
      "COM81",
			"COM82",
			"COM83",
			"COM84",
			"COM85",
			"COM86",
			"COM87",
			"COM88",
			"COM89",
      "COM90",
      "COM91",
			"COM92",
			"COM93",
			"COM94",
			"COM95",
			"COM96",
			"COM97",
			"COM98",
			"COM99",
      "COM100",
      "COM101",
			"COM102",
			"COM103",
			"COM104",
			"COM105",
			"COM106",
			"COM107",
			"COM108",
			"COM109",
      "COM110",
      "COM111",
			"COM112",
			"COM113",
			"COM114",
			"COM115",
			"COM116",
			"COM117",
			"COM118",
			"COM119",
      "COM120",
      "COM121",
			"COM122",
			"COM123",
			"COM124",
			"COM125",
			"COM126",
			"COM127",
			"COM128"
    };

		public static Int32 ComPortIndex(String value)
		{
			for (Int32 Index = 0; Index < COMPORTS.Length; Index++)
			{
				if (value == COMPORTS[Index])
				{
					return Index;
				}
			}
			return (Int32)EComPort.cp1;
		}

		public static EComPort ComPortEnumerator(String value)
		{
			return (EComPort)ComPortIndex(value);
		}

		public static String ComPortName(EComPort comport)
		{

			switch (comport)
			{
				case EComPort.cp1:
					return CHWComPort.COMPORTS[0];
				case EComPort.cp2:
					return CHWComPort.COMPORTS[1];
				case EComPort.cp3:
					return CHWComPort.COMPORTS[2];
				case EComPort.cp4:
					return CHWComPort.COMPORTS[3];
				case EComPort.cp5:
					return CHWComPort.COMPORTS[4];
				case EComPort.cp6:
					return CHWComPort.COMPORTS[5];
				case EComPort.cp7:
					return CHWComPort.COMPORTS[6];
				case EComPort.cp8:
					return CHWComPort.COMPORTS[7];
				case EComPort.cp9:
					return CHWComPort.COMPORTS[8];
				case EComPort.cp10:
					return CHWComPort.COMPORTS[9];
				case EComPort.cp11:
					return CHWComPort.COMPORTS[10];
				case EComPort.cp12:
					return CHWComPort.COMPORTS[11];
				case EComPort.cp13:
					return CHWComPort.COMPORTS[12];
				case EComPort.cp14:
					return CHWComPort.COMPORTS[13];
				case EComPort.cp15:
					return CHWComPort.COMPORTS[14];
				case EComPort.cp16:
					return CHWComPort.COMPORTS[15];
				case EComPort.cp17:
					return CHWComPort.COMPORTS[16];
				case EComPort.cp18:
					return CHWComPort.COMPORTS[17];
				case EComPort.cp19:
					return CHWComPort.COMPORTS[18];
				case EComPort.cp20:
					return CHWComPort.COMPORTS[19];
				case EComPort.cp21:
					return CHWComPort.COMPORTS[20];
				case EComPort.cp22:
					return CHWComPort.COMPORTS[21];
				case EComPort.cp23:
					return CHWComPort.COMPORTS[22];
				case EComPort.cp24:
					return COMPORTS[23];
				case EComPort.cp25:
					return COMPORTS[24];
				case EComPort.cp26:
					return COMPORTS[25];
				case EComPort.cp27:
					return COMPORTS[26];
				case EComPort.cp28:
					return COMPORTS[27];
				case EComPort.cp29:
					return COMPORTS[28];
				case EComPort.cp30:
					return COMPORTS[29];
				case EComPort.cp31:
					return COMPORTS[30];
				case EComPort.cp32:
					return COMPORTS[31];
			}
			return "???";
		}





    public static readonly String[] BAUDRATES = 
    { 
			"110", "300", "600", "1200",
			"2400", "4800", "9600", "19200",
			"38400", "576002", "115200",
			"230400", "460800", "921600", "1382400"
		};
    public static readonly EBaudrate[] Baudrates = 
    { 
			EBaudrate.br110, EBaudrate.br300, EBaudrate.br600, 
      EBaudrate.br1200, EBaudrate.br2400, EBaudrate.br4800,
      EBaudrate.br9600, EBaudrate.br19200, EBaudrate.br38400, 
      EBaudrate.br57600, EBaudrate.br115200, EBaudrate.br230400,
      EBaudrate.br460800, EBaudrate.br921600, EBaudrate.br1382400
		};

    public static readonly String[] PARITIES = 
    { 
			"None",
			"Even",
			"Odd",
			"Mark",
			"Space"
		};
    public static readonly EParity[] Parities = 
    {
      EParity.paNone,
      EParity.paEven,
      EParity.paOdd,
      EParity.paMark,
      EParity.paSpace
    };

    public static readonly String[] DATABITS = 
    { 
			"5",
			"6",
			"7",
			"8"
		};
    public static readonly EDatabits[] Databits = 
    { 
			EDatabits.db5,
			EDatabits.db6,
			EDatabits.db7,
			EDatabits.db8
		};

    public static readonly String[] STOPBITS = 
    { 
			"0",
			"1",
			"1.5",
			"2"
		};
    public static readonly EStopbits[] Stopbits = 
    { 
			EStopbits.sb0,
			EStopbits.sb1,
			EStopbits.sb15,
			EStopbits.sb2
		};

    public static readonly String[] HANDSHAKES = 
    { 
			"None",
			"XONXOFF",
			"RTSCTS",
			"RTSXON"
		};
    public static readonly EHandshake[] Handshakes = 
    { 
			EHandshake.hsNone,
			EHandshake.hsXONXOFF,
			EHandshake.hsRTSCTS,
			EHandshake.hsRTSXON
		};
    //
    public const String INIT_PORTNAME = "COM1";
    public const EComPort INIT_COMPORT = EComPort.cp1;
    public const Int32 INIT_TXDBUFFERSIZE = 32768;
    public const Int32 INIT_RXDBUFFERSIZE = 32768;
    public const EBaudrate INIT_BAUDRATE = EBaudrate.br9600;
    public const EParity INIT_PARITY = EParity.paOdd;
    public const EDatabits INIT_DATABITS = EDatabits.db8;
    public const EStopbits INIT_STOPBITS = EStopbits.sb1;
    public const EHandshake INIT_HANDSHAKE = EHandshake.hsNone;
    //
    public const Boolean INIT_TRANSMITENABLED = true;
    public const Boolean INIT_ISOPEN = false;
    //
    public const Int32 INIT_TXDDELAYCHARACTER = 0;
    public const Int32 INIT_TXDDELAYLINEFEED = 0;
    public const Int32 INIT_TXDDELAYCARRIAGERETURN = 0;
    //
    public const Int32 INIT_RXDBYTESTHRESHOLD = 1; // jedes RXD-Zeichen l�st RXDEvent aus!
    //
    public const Boolean INIT_RTSENABLE = true;     // necessary for Terminals
    public const Boolean INIT_DTRENABLE = false;
    //
    public const Byte INIT_TERMINATOR_CR = 0x0D;
    //
    public const Char CHARACTER_ZERO = (Char)0x00;
    public const Char CHARACTER_CR = (Char)0x0D;
    public const Char CHARACTER_LF = (Char)0x0A;
    //
    //------------------------------------------------------
    //  Constructor
    //------------------------------------------------------
    //
    public CHWComPort()
      : base()
    {
    }
    //
    //------------------------------------------------------
    //  Initialisation
    //------------------------------------------------------
    //
    public override void SetNotifier(CNotifier notifier)
    {
      base.SetNotifier(notifier);
    }
    //
    //------------------------------------------------------
    //  Management
    //------------------------------------------------------
    //
    public Boolean Open(RComPortData data)
    {
      try
      {
        return base._Open(data);
      }
      catch (Exception)
      {
        _Error(EErrorCode.OpenFailed);
        return false;
      }
    }

    public Boolean Close()
    {
      try
      {
        return base._Close();
      }
      catch (Exception)
      {
        _Error(EErrorCode.OpenFailed);
        return false;
      }
    }

    public Boolean IsOpen()
    {
      try
      {
        return base._IsOpen();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean IsClosed()
    {
      try
      {
        return base._IsClosed();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //
    //-----------------------------------------
    //  Public Management: Get/SetData
    //-----------------------------------------
    //
    public Boolean GetComPortData(out RComPortData data)
    {
      try
      {
        return _GetComPortData(out data);
      }
      catch (Exception)
      {
        data = new RComPortData(0);
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    public Boolean SetComPortData(RComPortData data)
    {
      try
      {
        return _SetComPortData(data);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean GetPinData(ref RPinData data)
    {
      try
      {
        return base._GetPinData(ref data);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean SetPinData(RPinData data)
    {
      try
      {
        return base._SetPinData(data);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }


    //
    //-----------------------------------------
    //  Public Management: Buffer-Access
    //-----------------------------------------
    //
    public void PurgeBuffers(EBufferAccess access)
		{
      try
      {
        base._PurgeBuffers(access);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
      }
    }
    //
    //-----------------------------------------
    //  Public Management: Write
    //-----------------------------------------
    //
    public Boolean EnableTransmit(Boolean enable)
    {
      try
      {
        return base._EnableTransmit(enable);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean WriteByte(Byte value)
    {
      try
      {
        return base._WriteByte(value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean WriteCharacter(Char character)
    {
      try
      {
        return base._WriteCharacter(character);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean WriteLine(String line)
    {
      try
      {
        return base._WriteLine(line);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean WriteText(String text)
    {
      try
      {
        return base._WriteText(text);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean WriteBufferFilter(Byte[] buffer)
		{
      try
      {
        return base._WriteBufferFilter(buffer);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean WriteBuffer(Byte[] buffer)
		{
      try
      {
        return base._WriteBuffer(buffer);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean WriteBuffer(Byte[] buffer, 
												       Int32 offset, 
                               Int32 length)
		{
      try
      {
        return base._WriteBuffer(buffer, offset, length);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //-------------------------------------------------
    //  Public Management: Read (Polling!)
    //-------------------------------------------------
    //
    public Boolean ReadCharacter(out Char character)
    {
      try
      {
        return base._ReadCharacter(out character);
      }
      catch (Exception)
      {
        character = (Char)0x00;
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean ReadLine(out String line)
    {
      try
      {
        return base._ReadLine(out line);
      }
      catch (Exception)
      {
        line = "";
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean ReadText(out String text)
    {
      try
      {
        return base._ReadText(out text);
      }
      catch (Exception)
      {
        text = "";
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
  
	}
}
