using System;
using System.Collections.Generic;
using System.Text;

namespace HWComPort
{	//
	// prinzipiell konzipiert als 1:1-Filter
	//
	public class CPTXDStreamFilter : CPStreamFilter
	{
		//
		//------------------------
		//	Constructor
		//------------------------
		//
		public CPTXDStreamFilter():
			base()
		{ 
		}
		//
		//------------------------
		//	Task
		//------------------------
		//
		public override String Execute(Char newcharacter)
		{
			return base.Execute(newcharacter);
		}
		public override String Execute(String newtext)
		{
			return base.Execute(newtext);
		}
		public override Byte[] Execute(Byte[] buffer)
		{
			return base.Execute(buffer);
		}

	}
}
