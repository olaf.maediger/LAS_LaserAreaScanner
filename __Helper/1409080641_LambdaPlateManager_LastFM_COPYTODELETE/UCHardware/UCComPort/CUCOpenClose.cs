﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCComPort
{
  public partial class CUCOpenClose : UserControl
  {
    private const String TEXT_OPEN = "Open";
    private const String TEXT_CLOSE = "Close";

    private DOnOpenCloseChanged FOnOpenCloseChanged;

    public CUCOpenClose()
    {
      InitializeComponent();
      cbxOpenClose.Checked = false;
      cbxOpenClose.Text = TEXT_OPEN;
    }


    public void SetOnOpenCloseChanged(DOnOpenCloseChanged value)
    {
      FOnOpenCloseChanged = value;
    }

    private void cbxOpenClose_CheckedChanged(object sender, EventArgs e)
    {
      if (FOnOpenCloseChanged is DOnOpenCloseChanged)
      {
        FOnOpenCloseChanged(cbxOpenClose.Checked);
      }
      if (cbxOpenClose.Checked)
      { // opened -> "CLOSE"
        cbxOpenClose.Text = TEXT_CLOSE;
      }
      else
      { // closed -> "OPEN"
        cbxOpenClose.Text = TEXT_OPEN;
      }
    }

    public Boolean Open()
    {
      if (!cbxOpenClose.Checked)
      {
        cbxOpenClose.Checked = true;
        return true;
      }
      return false;
    }

    public Boolean Close()
    {
      if (cbxOpenClose.Checked)
      {
        cbxOpenClose.Checked = false;
        return true;
      }
      return false;
    }

    public void SetStateOpened(Boolean value)
    {
      cbxOpenClose.CheckedChanged -= cbxOpenClose_CheckedChanged;
      cbxOpenClose.Checked = value;
      if (cbxOpenClose.Checked)
      { // opened -> "CLOSE"
        cbxOpenClose.Text = TEXT_CLOSE;
      }
      else
      { // closed -> "OPEN"
        cbxOpenClose.Text = TEXT_OPEN;
      }
      cbxOpenClose.CheckedChanged += cbxOpenClose_CheckedChanged;
    }

  }
}
