﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using HWComPort;
using NLStatement;
using NLStatementComPort;
//
using UCNotifier;
//
namespace UCComPort
{
  public delegate void DOnOpenCloseChanged(Boolean opened);

  public struct RUCComPortData
  {
    // NC public String PortName;
    // NC public EComPort ComPort;
    // NC public EBaudrate Baudrate;
    // NC public EParity Parity;
    // NC public EDatabits Databits;
    // NC public EStopbits Stopbits;
    // NC public EHandshake Handshake;
    public RComPortData ComPortData;
    //
    public RUCComPortData(Int32 init)
    {
      // NC PortName = CUCComPort.INIT_PORTNAME;
      // NC ComPort = CUCComPort.INIT_COMPORT;
      // NC Baudrate = CUCComPort.INIT_BAUDRATE;
      // NC Parity = CUCComPort.INIT_PARITY;
      // NC Databits = CUCComPort.INIT_DATABITS;
      // NC Stopbits = CUCComPort.INIT_STOPBITS;
      // NC Handshake = CUCComPort.INIT_HANDSHAKE;
      ComPortData = new RComPortData(0);
    }
  }

  public partial class CUCComPort : UserControl
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    public const String INIT_PORTNAME = "";
    public const EComPort INIT_COMPORT = EComPort.cp1;
    public const EBaudrate INIT_BAUDRATE = EBaudrate.br9600;
    public const EParity INIT_PARITY = EParity.paNone;
    public const EDatabits INIT_DATABITS = EDatabits.db8;
    public const EStopbits INIT_STOPBITS = EStopbits.sb1;
    public const EHandshake INIT_HANDSHAKE = EHandshake.hsNone;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnOpenCloseChanged FOnOpenCloseChanged;
    private DOnAddStatement FOnAddStatement;
    private CHWComPort FHWComPort;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    public CUCComPort()
    {
      InitializeComponent();
      //
      FUCOpenClose.SetOnOpenCloseChanged(UCOpenCloseOnOpenCloseChanged);
      //
      FHWComPort = new CHWComPort();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      /*!!!!!
      FUCOpenClose.SetNotifier(FNotifier);
      FUCBaudrate.SetNotifier(FNotifier);
      FUCDatabits.SetNotifier(FNotifier);
      FUCHandshake.SetNotifier(FNotifier);
      FUCParity.SetNotifier(FNotifier);
      FUCStopbits.SetNotifier(FNotifier);
      FUCPortName.SetNotifier(FNotifier);
      FUCPortsInstalled.SetNotifier(FNotifier);
      FUCPortsSelectable.SetNotifier(FNotifier);
       */
    }

    public void SetOnOpenCloseChanged(DOnOpenCloseChanged value)
    {
      FOnOpenCloseChanged = value;
      FUCOpenClose.SetOnOpenCloseChanged(UCOpenCloseOnOpenCloseChanged);
    }

    public void SetOnAddStatement(DOnAddStatement value)
    {
      FOnAddStatement = value;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Get/SetData
    //------------------------------------------------------------------------
    //
    public Boolean GetUCComPortData(out RUCComPortData data)
    {
      Boolean Result = false;
      data = new RUCComPortData(0);
      Result = FHWComPort.GetComPortData(out data.ComPortData);
      data.ComPortData.Name = FUCPortName.GetPortName();
      data.ComPortData.ComPort = FUCPortsSelectable.GetPortSelected();
      data.ComPortData.Baudrate = FUCBaudrate.GetBaudrate();
      data.ComPortData.Parity = FUCParity.GetParity();
      data.ComPortData.Databits = FUCDatabits.GetDatabits();
      data.ComPortData.Stopbits = FUCStopbits.GetStopbits();
      data.ComPortData.Handshake = FUCHandshake.GetHandshake();
      Result = FHWComPort.SetComPortData(data.ComPortData);
      return Result;
    }

    public Boolean SetUCComPortData(RUCComPortData data)
    {
      Boolean Result = false;
      FUCPortName.SetPortName(data.ComPortData.Name);
      FUCPortsSelectable.SetPortSelected(data.ComPortData.ComPort);
      FUCBaudrate.SetBaudrate(data.ComPortData.Baudrate);
      FUCParity.SetParity(data.ComPortData.Parity);
      FUCDatabits.SetDatabits(data.ComPortData.Databits);
      FUCStopbits.SetStopbits(data.ComPortData.Stopbits);
      FUCHandshake.SetHandshake(data.ComPortData.Handshake);
      Result = FHWComPort.SetComPortData(data.ComPortData);
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //
    private void EnableComPortControls(Boolean enable)
    {
      FUCPortName.Enabled = enable;
      FUCPortsSelectable.Enabled = enable;
      FUCBaudrate.Enabled = enable;
      FUCParity.Enabled = enable;
      FUCDatabits.Enabled = enable;
      FUCStopbits.Enabled = enable;
      FUCHandshake.Enabled = enable;
      btnRefresh.Enabled = enable;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback
    //------------------------------------------------------------------------
    //
    private void UCOpenCloseOnOpenCloseChanged(Boolean opened)
    {
      if (opened)
      {
        OpenComPort();
      }
      else
      {
        CloseComPort();
      }
      EnableComPortControls(!FHWComPort.IsOpen());
      if (FOnOpenCloseChanged is DOnOpenCloseChanged)
      {
        FOnOpenCloseChanged(opened);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event - Control
    //------------------------------------------------------------------------
    //
    private void btnRefresh_Click(object sender, EventArgs e)
    {
      RefreshComPortControls();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Public Management
    //------------------------------------------------------------------------
    //    
    public void RefreshComPortControls()
    {
      FUCPortsInstalled.RefreshPortsInstalled();
      FUCPortsSelectable.RefreshPortsSelectable();
    }

    public void RefreshOpenCloseComPort(Boolean opened)
    {
      EnableComPortControls(opened);
    }

    public Boolean OpenComPort()
    {
      try
      {
        if (FHWComPort.IsClosed())
        {
          RUCComPortData UCComPortData;
          if (GetUCComPortData(out UCComPortData))
          {
            if (FHWComPort.Open(UCComPortData.ComPortData))
            {
              if (GetUCComPortData(out UCComPortData))
              {
                return UCComPortData.ComPortData.IsOpen;
              }
            }
          }
        }
        return false;
      }
      catch (Exception)// e)
      {
        FNotifier.Write("Error: Open ComPort");
        return false;
      }
    }


    public Boolean CloseComPort()
    {
      try
      {
        if (FHWComPort.IsOpen())
        {
          if (FHWComPort.Close())
          {
            RUCComPortData UCComPortData;
            if (GetUCComPortData(out UCComPortData))
            {
              return !UCComPortData.ComPortData.IsOpen;
            }
          }
        }
        return false;
      }
      catch (Exception)// e)
      {
        FNotifier.Write("Error: Close ComPort");
        return false;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Program
    //------------------------------------------------------------------------
    //    
    public Boolean AddStatementTransmitTextDelay(String data,
                                               Int32 delaycharacter,
                                               Int32 delaycr,
                                               Int32 delaylf)
    {
      if (FOnAddStatement is DOnAddStatement)
      {
        CStatementTransmitTextDelay Statement = new CStatementTransmitTextDelay(FHWComPort,
                                                                          data,
                                                                          delaycharacter,
                                                                          delaycr,
                                                                          delaylf);
        FOnAddStatement(Statement);
        return true;
      }
      return false;
    }

  }
}
