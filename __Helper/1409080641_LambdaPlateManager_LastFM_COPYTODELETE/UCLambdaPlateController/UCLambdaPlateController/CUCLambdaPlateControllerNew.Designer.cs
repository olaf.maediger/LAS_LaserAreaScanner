﻿namespace UCLambdaPlateController
{
  partial class CUCLambdaPlateControllerNew
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.panel1 = new System.Windows.Forms.Panel();
      this.cbxEnableMotion = new System.Windows.Forms.CheckBox();
      this.btnMoveStepDown = new System.Windows.Forms.Button();
      this.btnMoveStepUp = new System.Windows.Forms.Button();
      this.btnMoveMinimum = new System.Windows.Forms.Button();
      this.btnMove20Deg = new System.Windows.Forms.Button();
      this.btnMove40Deg = new System.Windows.Forms.Button();
      this.btnMove45Deg = new System.Windows.Forms.Button();
      this.btnMove50Deg = new System.Windows.Forms.Button();
      this.btnMove70Deg = new System.Windows.Forms.Button();
      this.btnMoveMaximum = new System.Windows.Forms.Button();
      this.FUCRotator90Deg = new UCRotator.CUCRotator90Deg();
      this.FTimerRepetition = new System.Windows.Forms.Timer(this.components);
      this.FUCPeriodHalf = new UCLambdaPlateController.CUCPeriodHalf();
      this.FUCMicroStepResolution = new UCLambdaPlateController.CUCMicroStepResolution();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.cbxEnableMotion);
      this.panel1.Controls.Add(this.btnMoveStepDown);
      this.panel1.Controls.Add(this.btnMoveStepUp);
      this.panel1.Controls.Add(this.btnMoveMinimum);
      this.panel1.Controls.Add(this.btnMove20Deg);
      this.panel1.Controls.Add(this.btnMove40Deg);
      this.panel1.Controls.Add(this.btnMove45Deg);
      this.panel1.Controls.Add(this.btnMove50Deg);
      this.panel1.Controls.Add(this.btnMove70Deg);
      this.panel1.Controls.Add(this.btnMoveMaximum);
      this.panel1.Controls.Add(this.FUCPeriodHalf);
      this.panel1.Controls.Add(this.FUCMicroStepResolution);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel1.Location = new System.Drawing.Point(602, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(168, 600);
      this.panel1.TabIndex = 14;
      // 
      // cbxEnableMotion
      // 
      this.cbxEnableMotion.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxEnableMotion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
      this.cbxEnableMotion.Dock = System.Windows.Forms.DockStyle.Top;
      this.cbxEnableMotion.Location = new System.Drawing.Point(0, 504);
      this.cbxEnableMotion.Name = "cbxEnableMotion";
      this.cbxEnableMotion.Size = new System.Drawing.Size(168, 50);
      this.cbxEnableMotion.TabIndex = 42;
      this.cbxEnableMotion.Text = "Enable Motion";
      this.cbxEnableMotion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxEnableMotion.UseVisualStyleBackColor = false;
      this.cbxEnableMotion.CheckedChanged += new System.EventHandler(this.cbxEnableMotion_CheckedChanged);
      // 
      // btnMoveStepDown
      // 
      this.btnMoveStepDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(162)))), ((int)(((byte)(178)))));
      this.btnMoveStepDown.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnMoveStepDown.Location = new System.Drawing.Point(0, 454);
      this.btnMoveStepDown.Name = "btnMoveStepDown";
      this.btnMoveStepDown.Size = new System.Drawing.Size(168, 50);
      this.btnMoveStepDown.TabIndex = 41;
      this.btnMoveStepDown.Text = "MoveStepDown";
      this.btnMoveStepDown.UseVisualStyleBackColor = false;
      this.btnMoveStepDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveStepDown_MouseDown);
      this.btnMoveStepDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMoveStepDown_MouseUp);
      // 
      // btnMoveStepUp
      // 
      this.btnMoveStepUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(162)))), ((int)(((byte)(178)))));
      this.btnMoveStepUp.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnMoveStepUp.Location = new System.Drawing.Point(0, 404);
      this.btnMoveStepUp.Name = "btnMoveStepUp";
      this.btnMoveStepUp.Size = new System.Drawing.Size(168, 50);
      this.btnMoveStepUp.TabIndex = 40;
      this.btnMoveStepUp.Text = "MoveStepUp";
      this.btnMoveStepUp.UseVisualStyleBackColor = false;
      this.btnMoveStepUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnMoveStepUp_MouseDown);
      this.btnMoveStepUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnMoveStepUp_MouseUp);
      // 
      // btnMoveMinimum
      // 
      this.btnMoveMinimum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
      this.btnMoveMinimum.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnMoveMinimum.Location = new System.Drawing.Point(0, 373);
      this.btnMoveMinimum.Name = "btnMoveMinimum";
      this.btnMoveMinimum.Size = new System.Drawing.Size(168, 31);
      this.btnMoveMinimum.TabIndex = 39;
      this.btnMoveMinimum.Text = "Move Minimum";
      this.btnMoveMinimum.UseVisualStyleBackColor = false;
      this.btnMoveMinimum.Click += new System.EventHandler(this.btnMoveMinimum_Click);
      // 
      // btnMove20Deg
      // 
      this.btnMove20Deg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(244)))), ((int)(((byte)(192)))));
      this.btnMove20Deg.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnMove20Deg.Location = new System.Drawing.Point(0, 342);
      this.btnMove20Deg.Name = "btnMove20Deg";
      this.btnMove20Deg.Size = new System.Drawing.Size(168, 31);
      this.btnMove20Deg.TabIndex = 38;
      this.btnMove20Deg.Text = "Move 20 deg";
      this.btnMove20Deg.UseVisualStyleBackColor = false;
      this.btnMove20Deg.Click += new System.EventHandler(this.btnMove20Deg_Click);
      // 
      // btnMove40Deg
      // 
      this.btnMove40Deg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(244)))), ((int)(((byte)(192)))));
      this.btnMove40Deg.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnMove40Deg.Location = new System.Drawing.Point(0, 311);
      this.btnMove40Deg.Name = "btnMove40Deg";
      this.btnMove40Deg.Size = new System.Drawing.Size(168, 31);
      this.btnMove40Deg.TabIndex = 37;
      this.btnMove40Deg.Text = "Move 40 deg";
      this.btnMove40Deg.UseVisualStyleBackColor = false;
      this.btnMove40Deg.Click += new System.EventHandler(this.btnMove40Deg_Click);
      // 
      // btnMove45Deg
      // 
      this.btnMove45Deg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(244)))), ((int)(((byte)(192)))));
      this.btnMove45Deg.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnMove45Deg.Location = new System.Drawing.Point(0, 280);
      this.btnMove45Deg.Name = "btnMove45Deg";
      this.btnMove45Deg.Size = new System.Drawing.Size(168, 31);
      this.btnMove45Deg.TabIndex = 36;
      this.btnMove45Deg.Text = "Move 45 deg";
      this.btnMove45Deg.UseVisualStyleBackColor = false;
      this.btnMove45Deg.Click += new System.EventHandler(this.btnMove45Deg_Click);
      // 
      // btnMove50Deg
      // 
      this.btnMove50Deg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(244)))), ((int)(((byte)(192)))));
      this.btnMove50Deg.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnMove50Deg.Location = new System.Drawing.Point(0, 249);
      this.btnMove50Deg.Name = "btnMove50Deg";
      this.btnMove50Deg.Size = new System.Drawing.Size(168, 31);
      this.btnMove50Deg.TabIndex = 26;
      this.btnMove50Deg.Text = "Move 50 deg";
      this.btnMove50Deg.UseVisualStyleBackColor = false;
      this.btnMove50Deg.Click += new System.EventHandler(this.btnMove50Deg_Click);
      // 
      // btnMove70Deg
      // 
      this.btnMove70Deg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(244)))), ((int)(((byte)(192)))));
      this.btnMove70Deg.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnMove70Deg.Location = new System.Drawing.Point(0, 218);
      this.btnMove70Deg.Name = "btnMove70Deg";
      this.btnMove70Deg.Size = new System.Drawing.Size(168, 31);
      this.btnMove70Deg.TabIndex = 25;
      this.btnMove70Deg.Text = "Move 70 deg";
      this.btnMove70Deg.UseVisualStyleBackColor = false;
      this.btnMove70Deg.Click += new System.EventHandler(this.btnMove70Deg_Click);
      // 
      // btnMoveMaximum
      // 
      this.btnMoveMaximum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
      this.btnMoveMaximum.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnMoveMaximum.Location = new System.Drawing.Point(0, 187);
      this.btnMoveMaximum.Name = "btnMoveMaximum";
      this.btnMoveMaximum.Size = new System.Drawing.Size(168, 31);
      this.btnMoveMaximum.TabIndex = 24;
      this.btnMoveMaximum.Text = "Move Maximum";
      this.btnMoveMaximum.UseVisualStyleBackColor = false;
      this.btnMoveMaximum.Click += new System.EventHandler(this.btnMoveMaximum_Click);
      // 
      // FUCRotator90Deg
      // 
      this.FUCRotator90Deg.AngleActual = 0D;
      this.FUCRotator90Deg.AngleDelta = 0D;
      this.FUCRotator90Deg.AnglePreset = 0D;
      this.FUCRotator90Deg.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCRotator90Deg.Location = new System.Drawing.Point(0, 0);
      this.FUCRotator90Deg.MinimumSize = new System.Drawing.Size(200, 200);
      this.FUCRotator90Deg.Name = "FUCRotator90Deg";
      this.FUCRotator90Deg.Size = new System.Drawing.Size(602, 600);
      this.FUCRotator90Deg.TabIndex = 15;
      // 
      // FTimerRepetition
      // 
      this.FTimerRepetition.Interval = 250;
      // 
      // FUCPeriodHalf
      // 
      this.FUCPeriodHalf.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCPeriodHalf.Location = new System.Drawing.Point(0, 96);
      this.FUCPeriodHalf.Name = "FUCPeriodHalf";
      this.FUCPeriodHalf.Size = new System.Drawing.Size(168, 91);
      this.FUCPeriodHalf.TabIndex = 12;
      // 
      // FUCMicroStepResolution
      // 
      this.FUCMicroStepResolution.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCMicroStepResolution.Location = new System.Drawing.Point(0, 0);
      this.FUCMicroStepResolution.Name = "FUCMicroStepResolution";
      this.FUCMicroStepResolution.Size = new System.Drawing.Size(168, 96);
      this.FUCMicroStepResolution.TabIndex = 10;
      // 
      // CUCLambdaPlateControllerNew
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCRotator90Deg);
      this.Controls.Add(this.panel1);
      this.Name = "CUCLambdaPlateControllerNew";
      this.Size = new System.Drawing.Size(770, 600);
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private CUCPeriodHalf FUCPeriodHalf;
    private CUCMicroStepResolution FUCMicroStepResolution;
    private UCRotator.CUCRotator90Deg FUCRotator90Deg;
    private System.Windows.Forms.CheckBox cbxEnableMotion;
    private System.Windows.Forms.Button btnMoveStepDown;
    private System.Windows.Forms.Button btnMoveStepUp;
    private System.Windows.Forms.Button btnMoveMinimum;
    private System.Windows.Forms.Button btnMove20Deg;
    private System.Windows.Forms.Button btnMove40Deg;
    private System.Windows.Forms.Button btnMove45Deg;
    private System.Windows.Forms.Button btnMove50Deg;
    private System.Windows.Forms.Button btnMove70Deg;
    private System.Windows.Forms.Button btnMoveMaximum;
    private System.Windows.Forms.Timer FTimerRepetition;

  }
}
