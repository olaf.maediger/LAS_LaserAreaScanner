﻿namespace UCLambdaPlateController
{
  partial class CUCAngleUnit
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.lblAngle = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(109, 76);
      this.lblHeader.TabIndex = 2;
      this.lblHeader.Text = "Angle <name> [deg]";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lblAngle
      // 
      this.lblAngle.BackColor = System.Drawing.SystemColors.Info;
      this.lblAngle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblAngle.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblAngle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblAngle.Location = new System.Drawing.Point(0, 76);
      this.lblAngle.Name = "lblAngle";
      this.lblAngle.Size = new System.Drawing.Size(109, 35);
      this.lblAngle.TabIndex = 4;
      this.lblAngle.Text = "0.0";
      this.lblAngle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCAngleUnit
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lblAngle);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCAngleUnit";
      this.Size = new System.Drawing.Size(109, 111);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Label lblAngle;
  }
}
