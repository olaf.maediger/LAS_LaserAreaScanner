﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCLambdaPlateController
{
  //
  //-----------------------------------------------------------------
  //  Segment - Public Type
  //-----------------------------------------------------------------
  //
  public delegate void DOnPeriodHalfChanged(Int32 periodhalf,
                                            Int32 period,
                                            Double frequency);

  public partial class CUCPeriodHalf : UserControl
  {
    //
    //-----------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------
    //
    private DOnPeriodHalfChanged FOnPeriodHalfChanged;
    private Double FAngleResolution;
    //
    //-----------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------
    //
    public CUCPeriodHalf()
    {
      InitializeComponent();
      // 
      FAngleResolution = 1.8;
    }
    //
    //-----------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------
    //
    public void SetOnPeriodHalfChanged(DOnPeriodHalfChanged value)
    {
      FOnPeriodHalfChanged = value;
    }

    public void SetAngleResolution(Double value)
    {
      if (value != FAngleResolution)
      {
        FAngleResolution = value;
      }
    }

    public void SetPeriodHalf(Int32 value)
    {
      if (value != (Int32)nudPeriodHalf.Value)
      {
        nudPeriodHalf.Value = value;
      }
    }
    //
    //-----------------------------------------------------------------
    //  Segment - Event
    //-----------------------------------------------------------------
    //
    private void nudPeriodHalf_ValueChanged(object sender, EventArgs e)
    {
      Int32 PH = (Int32)Math.Max(1, nudPeriodHalf.Value);
      Int32 P = 2 * PH;
      Double F = 1000.0 * FAngleResolution / 360.0 / (Double)P;
      lblPeriod.Text = String.Format("{0}", P);
      lblFrequency.Text = String.Format("{0:0.00}", F);
      if (FOnPeriodHalfChanged is DOnPeriodHalfChanged)
      {
        FOnPeriodHalfChanged(PH, P, F);
      }
    }
  }
}
