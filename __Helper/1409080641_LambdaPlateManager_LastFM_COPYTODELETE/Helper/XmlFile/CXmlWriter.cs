﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using UCNotifier;
using NLExpression;
using NLStatement;
//
namespace XmlFile
{
	public class CXmlWriter
	{
		//
		//------------------------------------------
		//	Segment - Variable
		//------------------------------------------
		//
		private CNotifier FNotifier;
		private CStatement FParent;	// only Reference!
		private Int32 FTabulator = 0;
		private Boolean FDebugEnable = true;
		//
		//-------------------------------------
		//	Segment - Constructor
		//-------------------------------------
		//
		public CXmlWriter()
		{
			FParent = null;
		}
		//
		//------------------------------------------
		//	Segment - Property
		//------------------------------------------
		//
		public void SetNotifier(CNotifier notifier)
		{
			FNotifier = notifier;
		}
		//
		//------------------------------------------
		//	Segment - Helper 
		//------------------------------------------
		//
		private void Write(String line)
		{
			if (FDebugEnable)
			{
				String Header = "";
				for (Int32 CI = 0; CI < FTabulator; CI++)
				{
					Header += "  ";
				}
			  FNotifier.Write(Header + line);
      }
		}
		private void WriteIncrement()
		{
			FTabulator++;
		}
		private void WriteDecrement()
		{
			if (0 < FTabulator)
				FTabulator--;
		}

		private void EnableProtocol(Boolean value)
		{
			FDebugEnable = value;
		}
		//
		//
		//------------------------------------------
		//	Write: Scalar-Datatypes
		//------------------------------------------
		//
		private Boolean WriteBoolean(XmlDocument xmldocument,
																 XmlNode nodebase,
																 String header,
																 Boolean value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value.ToString();
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteString(XmlDocument xmldocument,
																XmlNode nodebase,
																String header,
																String value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value;
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteInt16(XmlDocument xmldocument,
															 XmlNode nodebase,
															 String header,
															 Int16 value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value.ToString();
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteInt32(XmlDocument xmldocument,
															 XmlNode nodebase,
															 String header,
															 Int32 value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value.ToString();
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteDouble(XmlDocument xmldocument,
																XmlNode nodebase,
																String header,
																Double value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value.ToString();
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteByte(XmlDocument xmldocument,
															XmlNode nodebase,
															String header,
															Byte value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value.ToString();
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}

		private Boolean WriteEnumeration(XmlDocument xmldocument,
																		 XmlNode nodebase,
																		 String header,
																		 String value)
		{
			XmlNode NodeChild = xmldocument.CreateElement(header);
			NodeChild.InnerText = value;
			nodebase.AppendChild(NodeChild);
			String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
			Write(Line);
			return true;
		}
		//
		//
		//----------------------------------------------
		//	Write: Basic-Struct-Level
		//----------------------------------------------
		//
		/* NC private Boolean WriteARGB(XmlDocument xmldocument,
															XmlNode nodebase,
															Byte alpha, Byte red, Byte green, Byte blue)
		{
			Boolean Result = true;
			//	Alpha
			WriteByte(xmldocument, nodebase, CXmlHeader.NAME_ALPHA, alpha);
			//	Red
			WriteByte(xmldocument, nodebase, CXmlHeader.NAME_RED, red);
			//	Green
			WriteByte(xmldocument, nodebase, CXmlHeader.NAME_GREEN, green);
			//	Blue
			WriteByte(xmldocument, nodebase, CXmlHeader.NAME_BLUE, blue);
			//
			return Result;
		}*/
		//
		//----------------------------------------------
		//	Write: High-Level
		//----------------------------------------------
		//
		private Boolean WriteVariable(XmlDocument xmldocument,
																	XmlNode nodeparent,
																	CVariable variable)
		{
			Boolean Result = true;
			// Base
      XmlNode NodeBase = xmldocument.CreateElement(CExpressionHeader.NAME_VARIABLE);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			// Name
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_NAME, variable.Name);
			// Type
      WriteEnumeration(xmldocument, NodeBase, CExpressionHeader.NAME_TYPE, variable.Type.ToString());
			// Preset
			switch (variable.Type)
			{
        case ETypeVariable.Byte:
          WriteByte(xmldocument, NodeBase, CExpressionHeader.NAME_PRESET, variable.GetPresetByte());
					break;
        case ETypeVariable.Boolean:
          WriteBoolean(xmldocument, NodeBase, CExpressionHeader.NAME_PRESET, variable.GetPresetBoolean());
					break;
        case ETypeVariable.Int16:
          WriteInt16(xmldocument, NodeBase, CExpressionHeader.NAME_PRESET, variable.GetPresetInt16());
					break;
        case ETypeVariable.Int32:
          WriteInt32(xmldocument, NodeBase, CExpressionHeader.NAME_PRESET, variable.GetPresetInt32());
					break;
        case ETypeVariable.Double:
          WriteDouble(xmldocument, NodeBase, CExpressionHeader.NAME_PRESET, variable.GetPresetDouble());
					break;
			}
			//
			WriteDecrement();
			return Result;
		}




		private Boolean WriteStatementSet(XmlDocument xmldocument,
																	  XmlNode nodeparent,
																	  CStatementSet command)
		{
			Boolean Result = true;
			// Base
			XmlNode NodeBase = xmldocument.CreateElement(command.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			//
			// Variable
			WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_VARIABLE, command.VariableName);
			// Expression
			WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_EXPRESSION, command.TextExpression);
			//
			WriteDecrement();
			return Result;
		}

		private Boolean WriteStatementWriteLine(XmlDocument xmldocument,
																					XmlNode nodeparent,
																					CStatementWriteLine command)
		{
			Boolean Result = true;
			// Base
			XmlNode NodeBase = xmldocument.CreateElement(command.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			//
			// Format
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_FORMAT, command.Format);
			// Variable
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_EXPRESSION, command.Expression);
			//
			WriteDecrement();
			return Result;
		}


		private Boolean WriteStatementWhile(XmlDocument xmldocument,
																			XmlNode nodeparent,
																			CStatementWhile statementwhile)
		{
			Boolean Result = true;
			// Base
      XmlNode NodeBase = xmldocument.CreateElement(statementwhile.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			//
			WriteIncrement();
			// Expression
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_EXPRESSION, statementwhile.Expression);
			// StatementBlock
      WriteStatementBlock(xmldocument, NodeBase, statementwhile.StatementBlock);
			//
			WriteDecrement();
			return Result;
		}




		private Boolean WriteStatementIf(XmlDocument xmldocument,
																	 XmlNode nodeparent,
                                   CStatementIf statementif)
		{
			Boolean Result = true;
			// Base
      XmlNode NodeBase = xmldocument.CreateElement(statementif.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			//
			WriteIncrement();
			// Expression
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_EXPRESSION, statementif.Expression);
			// BlockTrue
      WriteStatementBlock(xmldocument, NodeBase, statementif.StatementBlockTrue);
			// BlockFalse
      if (statementif.StatementBlockFalse is CStatementBlock)
			{
        WriteStatementBlock(xmldocument, NodeBase, statementif.StatementBlockFalse);
			}
			//
			WriteDecrement();
			return Result;
		}

		private Boolean WriteStatementFor(XmlDocument xmldocument,
																	    XmlNode nodeparent,
                                      CStatementFor statementfor)
		{
			Boolean Result = true;
			// Base
      XmlNode NodeBase = xmldocument.CreateElement(statementfor.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			//
			WriteIncrement();
			// Variable
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_VARIABLE, statementfor.VariableName);
			// Expression
      WriteString(xmldocument, NodeBase, CExpressionHeader.NAME_EXPRESSION, statementfor.Expression);
      // Condition
      WriteString(xmldocument, NodeBase, CStatementHeader.NAME_CONDITION, statementfor.Condition);
      // Increment
      WriteString(xmldocument, NodeBase, CStatementHeader.NAME_INCREMENT, statementfor.Increment);
      // Block
      WriteStatementBlock(xmldocument, NodeBase, statementfor.StatementBlock);
			//
			WriteDecrement();
			return Result;
		}




		private Boolean WriteStatementBlock(XmlDocument xmldocument,
																			  XmlNode nodeparent,
                                        CStatementBlock statementblock)
		{
			Boolean Result = true;
			// Base
      XmlNode NodeBase = xmldocument.CreateElement(statementblock.Name);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			//
      foreach (CStatement Statement in statementblock.Statementlist)
			{
				switch (Statement.Name)
				{
          case CStatementHeader.NAME_SET:
						Result &= WriteStatementSet(xmldocument, NodeBase, (CStatementSet)Statement);
						break;
          case CStatementHeader.NAME_WRITELINE:
						Result &= WriteStatementWriteLine(xmldocument, NodeBase, (CStatementWriteLine)Statement);
						break;
          case CStatementHeader.NAME_WHILE:
						Result &= WriteStatementWhile(xmldocument, NodeBase, (CStatementWhile)Statement);
						break;
          case CStatementHeader.NAME_IF:
						Result &= WriteStatementIf(xmldocument, NodeBase, (CStatementIf)Statement);
						break;
					/*case CXmlHeader.NAME_FOR:
						Result &= WriteStatementFor(xmldocument, NodeBase, Statement);
						break;*/
					default: // Unknown Statement
						Result = false;
						break;
				}
				if (!Result)
				{
					WriteDecrement();
					return Result;
				}
			}
			//
			WriteDecrement();
			return Result;
		}



		private Boolean WriteExpression(XmlDocument xmldocument,
																		XmlNode nodeparent)
		{
			Boolean Result = true;
			// Base
      XmlNode NodeBase = xmldocument.CreateElement(CExpressionHeader.NAME_EXPRESSION);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			//
			foreach (CVariable Variable in FParent.Variablelist)
			{
				Result &= WriteVariable(xmldocument, NodeBase, Variable);
				if (!Result)
				{
					WriteDecrement();
					return Result;
				}
			}
			//
			WriteDecrement();
			return Result;
		}

		private Boolean WriteStatement(XmlDocument xmldocument,
																	 XmlNode nodeparent)
		{
			Boolean Result = true;
			// Base
      XmlNode NodeBase = xmldocument.CreateElement(CStatementHeader.NAME_STATEMENT);
			nodeparent.AppendChild(NodeBase);
			String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
			Write(Line);
			WriteIncrement();
			//
      /*!!!!!!!!!!!!!!!!!!!!foreach (CStatement Statement in FParent.Statementlist)
      {
        switch (Statement.Name)
        {
          case CStatementHeader.NAME_SET:
            Result &= WriteStatementSet(xmldocument, NodeBase, (CStatementSet)Statement);
            break;
          case CStatementHeader.NAME_WHILE:
            Result &= WriteStatementWhile(xmldocument, NodeBase, (CStatementWhile)Statement);
            break;
          case CStatementHeader.NAME_IF:
            Result &= WriteStatementIf(xmldocument, NodeBase, (CStatementIf)Statement);
            break;
          case CStatementHeader.NAME_FOR:
            Result &= WriteStatementFor(xmldocument, NodeBase, (CStatementFor)Statement);
            break;
          default: // Unknown Statement
            Result = false;
            break;
        }
        if (!Result)
        {
          WriteDecrement();
          return Result;
        }
      }*/
      //
			WriteDecrement();
			return Result;
		}
		//
		//------------------------------------------
		//	Central Write for all Nodes
		//------------------------------------------
		//
		// Save all Objects -> Xml
		public Boolean WriteProgram(String filename,
																CStatement parent)
		{	// Define Parent
			FParent = parent;
			//
			XmlDocument XMLDocument = new XmlDocument();
			//
			EnableProtocol(true);
			//
			// Root
			XmlNode NodeRoot;
      NodeRoot = XMLDocument.CreateElement(CStatementHeader.NAME_PROGRAM);
			XMLDocument.AppendChild(NodeRoot);
			String Line = String.Format("<{0}>[{1}]", NodeRoot.Name, NodeRoot.InnerText);
			Write(Line);
			// Declaration
			XmlDeclaration XMLDeclaration = XMLDocument.CreateXmlDeclaration(CXmlHeader.TITLE_VERSION,
																																			 CXmlHeader.TITLE_ENCODING,
																																			 CXmlHeader.TITLE_STANDALONE);
			XMLDocument.InsertBefore(XMLDeclaration, NodeRoot);
			// Comment
			XmlComment XMLComment = XMLDocument.CreateComment(CXmlHeader.TITLE_COMMENT1);
			XMLDocument.InsertBefore(XMLComment, NodeRoot);
			XMLComment = XMLDocument.CreateComment(CXmlHeader.TITLE_COMMENT2);
			XMLDocument.InsertBefore(XMLComment, NodeRoot);
			XMLComment = XMLDocument.CreateComment(CXmlHeader.TITLE_COMMENT3);
			XMLDocument.InsertBefore(XMLComment, NodeRoot);
			//
			WriteIncrement();
			WriteExpression(XMLDocument, NodeRoot);
			WriteStatement(XMLDocument, NodeRoot);
			WriteDecrement();
			//	
			XMLDocument.Save(filename);
			XMLDocument = null;
			return true;
		}
	}
}