﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using UCNotifier;
using NLExpression;
using NLStatement;
//
namespace XmlFile
{
  public class CXmlReader
	{
		//
		//------------------------------------------
		//	Member
		//------------------------------------------
		//
		private CNotifier FNotifier;
		private Int32 FTabulator = 0;
		private Boolean FDebugEnable = true;
    private DOnReadStatementBlock FOnReadStatementBlock;
    //
    private CConstantlist FConstantlist;
    private CVariablelist FVariablelist;
    private CExpression FExpression;
    private CStatement FStatement;
		//
		//-------------------------------------
		//	Constructor
		//-------------------------------------
		//
		public CXmlReader()
		{
			//FParent = null;
		}
		//
		//------------------------------------------
		//	Segment - Property
		//------------------------------------------
		//
		public void SetNotifier(CNotifier notifier)
		{
			FNotifier = notifier;
		}

    public void SetOnReadStatementBlock(DOnReadStatementBlock value)
    {
      FOnReadStatementBlock = value;
    }

    /*protected CStatementBase Parent
    {
      get { return FParent; }
    }*/

    protected CNotifier Notifier
    {
      get { return FNotifier; }
    }
		//
		//------------------------------------------
		//	Helper - Debug
		//------------------------------------------
		//
    public void Write(String line)
		{
			if (FDebugEnable)
			{
				String Header = "";
				for (Int32 CI = 0; CI < FTabulator; CI++)
				{
					Header += "  ";
				}
				FNotifier.Write(Header + line);
			}
		}
    public void WriteIncrement()
		{
			FTabulator++;
		}
    public void WriteDecrement()
		{
			if (0 < FTabulator)
				FTabulator--;
		}

    public void DebugEnable(Boolean enable)
		{
			FDebugEnable = enable;
		}
		//
		//-----------------------------------------------
		//
		//	Build-Methods (with looking for Constants)
		//
		//-----------------------------------------------
		//
    static public String TransformString(String value)
		{ // filtern von "\r", "\n"
			return value.Replace("\\r\\n", "\r\n");
		}
		// --- String ---
    static public String BuildString(String value)
		{
			String Result = CVariable.INIT_STRING;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueString();
			}*/
			Result = TransformString(value);
			return Result;
		}

		// --- Boolean ---
    static public Boolean BuildBoolean(String value)
		{
			Boolean Result = CVariable.INIT_BOOLEAN;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueBoolean();
			}*/
			if (Boolean.TryParse(value, out Result))
			{
				return Result;
			}
			switch (value)
			{
				case "TRUE":
					return true;
				case "True":
					return true;
				case "true":
					return true;
				case "FALSE":
					return false;
				case "False":
					return false;
				case "false":
					return false;
			}
			return Result;
		}

		// --- Byte ---
    static public Byte BuildByte(String value)
		{
			Byte Result = CVariable.INIT_BYTE;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueByte();
			}*/
			if (Byte.TryParse(value, out Result))
			{
				return Result;
			}
			return Result;
		}

		// --- Int16 ---
    static public Int16 BuildInt16(String value)
		{
			Int16 Result = CVariable.INIT_INT16;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueInt16();
			}*/
			if (Int16.TryParse(value, out Result))
			{
				return Result;
			}
			return Result;
		}

		// --- Int32 ---
    static public Int32 BuildInt32(String value)
		{
			Int32 Result = CVariable.INIT_INT32;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueInt32();
			}*/
			if (Int32.TryParse(value, out Result))
			{
				return Result;
			}
			return Result;
		}

		// --- Double ---
    static public Double BuildDouble(String value)
		{
			Double Result = CVariable.INIT_DOUBLE;
			/* NC CVariable Variable = FParent.Variablelist.FindName(value);
			if (Variable is CVariable)
			{
				return Variable.GetValueDouble();
			}*/
			if (Double.TryParse(value, out Result))
			{
				return Result;
			}
			return Result;
		}










		private Boolean ReadVariable(XmlNode nodebase,
																 out CVariable variable)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			String Name = "";
			String Type = "";
			String Preset = "";
			variable = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				switch (NodeChild.Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CExpressionHeader.NAME_NAME:
						Name = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Name);
						Write(Line);
						break;
          case CExpressionHeader.NAME_TYPE:
						Type = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Type);
						Write(Line);
						break;
          case CExpressionHeader.NAME_PRESET:
						Preset = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Preset);
						Write(Line);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadVariable!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			Result &= (0 < Name.Length);
			Result &= (0 < Type.Length);
			if (Result)
			{
				switch (Type)
				{
					/*case EType.String.ToString():
						// variable = new CVariableString()
						break;*/
					case CVariable.NAME_DOUBLE:
						variable = new CVariableDouble(Name, Preset);
						break;
					default: // Error
						Result = false;
						break;
				}			 
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      //
      return Result;
		}





		private Boolean ReadExpression(XmlNode nodebase)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
					case CExpressionHeader.NAME_VARIABLE:
						CVariable Variable;
						Result &= ReadVariable(NodeChild, out Variable);
						if (Result)
						{
							Result &= (Variable is CVariable);
							if (Result)
							{
                //!!!!!!!!!!!!!!!!FParent.AddVariable(Variable);
							}
						}
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadExpression!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      //
      return Result;
		}











		private Boolean ReadStatementSet(XmlNode nodebase,
																	 out CStatementSet command)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
      String VName = "";
      String VType = "";
      String VExpression = "";
			command = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CExpressionHeader.NAME_NAME:
            VName = BuildString(NodeChild.InnerText);
            Line = String.Format("<{0}={1}>", NodeChild.Name, VName);
            Write(Line);
            break;
          case CExpressionHeader.NAME_TYPE:
            VType = BuildString(NodeChild.InnerText);
            Line = String.Format("<{0}={1}>", NodeChild.Name, VType);
            Write(Line);
            break;
          case CExpressionHeader.NAME_PRESET:
						VExpression = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, VExpression);
						Write(Line);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadStatementSet!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
      Result &= (0 < VName.Length);
      Result &= (0 < VType.Length);
      Result &= (0 < VExpression.Length);
			if (Result)
			{
				command = new CStatementSet(VName, VType, VExpression);
				command.SetNotifier(FNotifier);
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      //
      return Result;
		}

		private Boolean ReadStatementWriteLine(XmlNode nodebase,
																				 out CStatementWriteLine command)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			String Format = "";
			String Expression = "";
			command = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CExpressionHeader.NAME_FORMAT:
						Format = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Format);
						Write(Line);
						break;
          case CExpressionHeader.NAME_EXPRESSION:
						Expression = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Expression);
						Write(Line);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadStatementSet!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			Result &= (0 < Format.Length);
			Result &= (0 < Expression.Length);
			if (Result)
			{
        //!!!!!!!!!!!!!!!!command = new CStatementWriteLine(FParent.Variablelist, Format, Expression);
				command.SetNotifier(FNotifier);
			}
			//
			WriteDecrement();
			return Result;
		}

		private Boolean ReadStatementWhile(XmlNode nodebase,
																		 out CStatementWhile command)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			String Expression = "";
      CStatementBlock StatementBlock = null;
			command = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
					case CExpressionHeader.NAME_EXPRESSION:
						Expression = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Expression);
						Write(Line);
						break;
					case CStatementHeader.NAME_BLOCK:
            // !!!!!!!!!!!!!!!! Result &= ReadStatementBlock(NodeChild, out StatementBlock);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadStatementSet!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			Result &= (0 < Expression.Length);
      Result &= (StatementBlock is CStatementBlock);
			if (Result)
			{
        //!!!!!!!!!!!!!!!!command = new CStatementWhile(FParent.Variablelist, Expression, Block);
				command.SetNotifier(FNotifier);
			}
			//
			WriteDecrement();
			return Result;
		}

		private Boolean ReadStatementIf(XmlNode nodebase,
																	out CStatementIf command)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			String Expression = "";
      CStatementBlock StatementBlockTrue = null;
      CStatementBlock StatementBlockFalse = null;
			command = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CExpressionHeader.NAME_EXPRESSION:
						Expression = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Expression);
						Write(Line);
						break;
          case CStatementHeader.NAME_BLOCKTRUE:
            Result &= ReadStatementBlock(NodeChild, out StatementBlockTrue);
						break;
          case CStatementHeader.NAME_BLOCKFALSE:
            Result &= ReadStatementBlock(NodeChild, out StatementBlockFalse);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadStatementSet!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			Result &= (0 < Expression.Length);
      Result &= ((StatementBlockTrue is CStatementBlock) || (StatementBlockFalse is CStatementBlock));
			if (Result)
			{
        //!!!!!!!!!!!!!!!!command = new CStatementIf(FParent.Variablelist, Expression, BlockTrue, BlockFalse);
				command.SetNotifier(FNotifier);
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      //
      return Result;
		}

    /*....
		private Boolean ReadStatementFor(XmlNode nodebase,
																	 out CStatementFor command)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			String Variable = "";
			String Expression = "";
			String Condition = "";
			String Increment = "";
			CStatementBlock Block = null;
			command = null;
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
					case CExpressionHeader.NAME_VARIABLE:
						Variable = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Variable);
						Write(Line);
						break;
          case CExpressionHeader.NAME_EXPRESSION:
						Expression = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Expression);
						Write(Line);
						break;
          case CStatementHeader.NAME_CONDITION:
						Condition = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Condition);
						Write(Line);
						break;
          case CStatementHeader.NAME_INCREMENT:
						Increment = BuildString(NodeChild.InnerText);
						Line = String.Format("<{0}={1}>", NodeChild.Name, Increment);
						Write(Line);
						break;
          case CStatementHeader.NAME_BLOCK:
						Result &= ReadStatementBlock(NodeChild, out Block);
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadStatementFor!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
      Result &= (0 < Variable.Length);
      Result &= (0 < Expression.Length);
      Result &= (0 < Condition.Length);
      Result &= (0 < Increment.Length);
      Result &= (Block is CStatementBlock);
			if (Result)
			{
				command = new CStatementFor(//FParent.Variablelist,
																	Variable, Expression, Condition, Increment, Block);
				command.SetNotifier(FNotifier);
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      //
      return Result;
		}*/

		private Boolean ReadStatementBlock(XmlNode nodebase,
                                       out CStatementBlock statementblock)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
      statementblock = new CStatementBlock();//"StatementBlock");//FParent.Variablelist);
      statementblock.SetNotifier(FNotifier);
			//
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CStatementHeader.NAME_SET:
            CStatementSet StatementSet;
            Result &= ReadStatementSet(NodeChild, out StatementSet);
            Result &= (StatementSet is CStatementSet);
            if (Result)
            {
              statementblock.AddStatement(StatementSet);
            }
            break;
          case CStatementHeader.NAME_WHILE:
            CStatementWhile StatementWhile;
            Result &= ReadStatementWhile(NodeChild, out StatementWhile);
            Result &= (StatementWhile is CStatementWhile);
            if (Result)
            {
              statementblock.AddStatement(StatementWhile);
            }
            break;
          case CStatementHeader.NAME_IF:
            CStatementIf StatementIf;
            Result &= ReadStatementIf(NodeChild, out StatementIf);
            Result &= (StatementIf is CStatementIf);
            if (Result)
            {
              statementblock.AddStatement(StatementIf);
            }
            break;
          case CStatementHeader.NAME_WRITELINE:
            CStatementWriteLine StatementWriteLine;
            Result &= ReadStatementWriteLine(NodeChild, out StatementWriteLine);
            Result &= (StatementWriteLine is CStatementWriteLine);
            if (Result)
            {
              statementblock.AddStatement(StatementWriteLine);
            }
            break;
          /*!!!!!!!!!!!!!!!case CStatementHeader.NAME_FOR:
            CStatementFor StatementFor;
            Result &= ReadStatementFor(NodeChild, out StatementFor);
            Result &= (StatementFor is CStatementFor);
            if (Result)
            {
              commandblock.AddStatement(StatementFor);
            }
            break;*/
          case CStatementHeader.NAME_BLOCK:
            // !!!!!!!!!!!!!!!!!CStatement StatementBlock;
						// !!!!!!!!!!!!!!!!! Result &= ReadStatementBlock(NodeChild, out StatementBlock);
	  				// !!!!!!!!!!!!!!!!!Result &= (StatementBlock is CStatement);
            // !!!!!!!!!!!!!!!!!if (Result)
			  		{
              // !!!!!!!!!!!!!!!!!  statementblock.AddStatement(StatementBlock);
						}
						break;
					default:
            Result = false;
            if (FOnReadStatementBlock is DOnReadStatementBlock)
            { //  Extensions:
              // !!!!!!!!!!Result = FOnReadStatementBlock(NodeChild, ref commandblock);
            }
            if (!Result)
            {
              DebugEnable(true);
              Write("ERROR - ReadStatementBlock!");
              DebugEnable(false);
              Result = false;
            }
						break;
				}
			}
			WriteDecrement();
      Line = String.Format("</{0}>", nodebase.Name);
      Write(Line);
      return Result;
		}













	
		
		/*
		
		private Boolean ReadStatement(XmlNode nodebase)
		{
			Boolean Result = true;
			String Line = String.Format("<{0}>", nodebase.Name);
			Write(Line);
			WriteIncrement();
			//
			foreach (XmlNode NodeChild in nodebase.ChildNodes)
			{
				String Name = NodeChild.Name;
				switch (Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
					case CXmlHeader.NAME_BLOCK:
						CStatementBlock StatementBlock;
						Result &= ReadStatementBlock(NodeChild, out StatementBlock);
						if (Result)
						{
							Result &= (StatementBlock is CStatementBlock);
							if (Result)
							{
								FParent.AddStatement(StatementBlock);
							}
						}
						break;
					case CXmlHeader.NAME_SET:
						CStatementSet StatementSet;
						Result &= ReadStatementSet(NodeChild, out StatementSet);
						if (Result)
						{
							Result &= (StatementSet is CStatementSet);
							if (Result)
							{
								FParent.AddStatement(StatementSet);
							}
						}
						break;
					case CXmlHeader.NAME_WHILE:
						CStatementWhile StatementWhile;
						Result &= ReadStatementWhile(NodeChild, out StatementWhile);
						if (Result)
						{
							Result &= (StatementWhile is CStatementWhile);
							if (Result)
							{
								FParent.AddStatement(StatementWhile);
							}
						}
						break;
					case CXmlHeader.NAME_IF:
						break;
					case CXmlHeader.NAME_WRITELINE:
						break;
					default:
						DebugEnable(true);
						Write("ERROR - ReadExpression!");
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			//
			WriteDecrement();
			return Result;
		}

		*/
















		//
		//------------------------------------------
		//	Central Read for all Nodes
		//------------------------------------------
		//
		// Xml -> read all ojects 
		public Boolean Load(String filename,
                        ref CConstantlist constantlist,
                        ref CVariablelist variablelist,
                        ref CExpression expression,
                        ref CStatement statement)    
		{
			Boolean Result = true;
			//
      FConstantlist = constantlist;
      FVariablelist = variablelist;
      FExpression = expression;
      FStatement = statement;
      //
			DebugEnable(true);
			//
      // NC FParent = parent;
			XmlDocument XMLDocument = new XmlDocument();
			XMLDocument.Load(filename);
			//
			// Clear all Container
			//
      // NC FParent.ClearVariablelist();
      // NC FParent.ClearStatementlist();
			//
			// Detection Root
			//
      XmlNode NodeRoot = XMLDocument.SelectSingleNode(CStatementHeader.NAME_PROGRAM);
      String Line = String.Format("<{0}>", NodeRoot.LocalName);
      Write(Line);
      WriteIncrement();
			foreach (XmlNode NodeChild in NodeRoot.ChildNodes)
			{
				// debug FNotifier.Write("----> {0}", NodeChild.Name);
				switch (NodeChild.Name)
				{
					case CXmlHeader.TOKEN_COMMENT:
						// erst mal negieren, später in Commentlist!
						break;
          case CExpressionHeader.NAME_EXPRESSION:
            //CExpression Expression;
            Result = false;
            /*Result &= ReadExpression(NodeChild, out Expression);
            if (Result)
            {
              FExpression.AddExpression(Expression);
            }*/
						break;
          //case CStatementHeader.NAME_STATEMENT:
          case CStatementHeader.NAME_BLOCK:
            CStatement StatementBlock = null;
            //Result &= ReadStatementBlock(NodeChild, out StatementBlock);
            Result &= (StatementBlock is CStatement);
            if (Result)
            {
              //Result &= FStatement.AddStatementBlock(StatementBlock);
            }
						break;
					default:
						DebugEnable(true);
						Write(String.Format("ERROR - ReadAll: Unknown tag <{0}>!", NodeChild.Name));
						DebugEnable(false);
						Result = false;
						break;
				}
			}
			WriteDecrement();
      Line = String.Format("</{0}>", NodeRoot.LocalName);
      Write(Line);
      return Result;
		}






	}
}
