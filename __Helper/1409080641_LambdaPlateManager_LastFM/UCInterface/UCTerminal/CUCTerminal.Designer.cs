﻿namespace UCTerminal
{
  partial class CUCTerminal
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlInput = new System.Windows.Forms.Panel();
      this.nudDelayLF = new System.Windows.Forms.NumericUpDown();
      this.label6 = new System.Windows.Forms.Label();
      this.nudDelayCharacter = new System.Windows.Forms.NumericUpDown();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.nudDelayCR = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.cbxLF = new System.Windows.Forms.CheckBox();
      this.cbxCR = new System.Windows.Forms.CheckBox();
      this.cbxSendData = new System.Windows.Forms.ComboBox();
      this.cmsTxData = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mitSaveText = new System.Windows.Forms.ToolStripMenuItem();
      this.mitClearAllLines = new System.Windows.Forms.ToolStripMenuItem();
      this.mitLoadText = new System.Windows.Forms.ToolStripMenuItem();
      this.btnSendData = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.FUCMultiColorText = new UCMultiColorText.CUCMultiColorText();
      this.DialogLoadStatementText = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveStatementText = new System.Windows.Forms.SaveFileDialog();
      this.pnlInput.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayLF)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayCharacter)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayCR)).BeginInit();
      this.cmsTxData.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlInput
      // 
      this.pnlInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlInput.Controls.Add(this.nudDelayLF);
      this.pnlInput.Controls.Add(this.label6);
      this.pnlInput.Controls.Add(this.nudDelayCharacter);
      this.pnlInput.Controls.Add(this.label5);
      this.pnlInput.Controls.Add(this.label4);
      this.pnlInput.Controls.Add(this.nudDelayCR);
      this.pnlInput.Controls.Add(this.label3);
      this.pnlInput.Controls.Add(this.label2);
      this.pnlInput.Controls.Add(this.cbxLF);
      this.pnlInput.Controls.Add(this.cbxCR);
      this.pnlInput.Controls.Add(this.cbxSendData);
      this.pnlInput.Controls.Add(this.btnSendData);
      this.pnlInput.Controls.Add(this.label1);
      this.pnlInput.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlInput.Location = new System.Drawing.Point(0, 202);
      this.pnlInput.Name = "pnlInput";
      this.pnlInput.Size = new System.Drawing.Size(506, 47);
      this.pnlInput.TabIndex = 0;
      // 
      // nudDelayLF
      // 
      this.nudDelayLF.Location = new System.Drawing.Point(184, 24);
      this.nudDelayLF.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudDelayLF.Name = "nudDelayLF";
      this.nudDelayLF.Size = new System.Drawing.Size(44, 20);
      this.nudDelayLF.TabIndex = 16;
      this.nudDelayLF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(166, 27);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(19, 13);
      this.label6.TabIndex = 15;
      this.label6.Text = "LF";
      // 
      // nudDelayCharacter
      // 
      this.nudDelayCharacter.Location = new System.Drawing.Point(284, 24);
      this.nudDelayCharacter.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudDelayCharacter.Name = "nudDelayCharacter";
      this.nudDelayCharacter.Size = new System.Drawing.Size(44, 20);
      this.nudDelayCharacter.TabIndex = 14;
      this.nudDelayCharacter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(330, 28);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(20, 13);
      this.label5.TabIndex = 13;
      this.label5.Text = "ms";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(233, 27);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(53, 13);
      this.label4.TabIndex = 12;
      this.label4.Text = "Character";
      // 
      // nudDelayCR
      // 
      this.nudDelayCR.Location = new System.Drawing.Point(118, 24);
      this.nudDelayCR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudDelayCR.Name = "nudDelayCR";
      this.nudDelayCR.Size = new System.Drawing.Size(44, 20);
      this.nudDelayCR.TabIndex = 11;
      this.nudDelayCR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(68, 27);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(52, 13);
      this.label3.TabIndex = 10;
      this.label3.Text = "Delay CR";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(14, 4);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(42, 13);
      this.label2.TabIndex = 9;
      this.label2.Text = "TxData";
      // 
      // cbxLF
      // 
      this.cbxLF.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxLF.Location = new System.Drawing.Point(34, 21);
      this.cbxLF.Name = "cbxLF";
      this.cbxLF.Size = new System.Drawing.Size(31, 22);
      this.cbxLF.TabIndex = 8;
      this.cbxLF.Text = "LF";
      this.cbxLF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxLF.UseVisualStyleBackColor = true;
      // 
      // cbxCR
      // 
      this.cbxCR.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxCR.Location = new System.Drawing.Point(2, 21);
      this.cbxCR.Name = "cbxCR";
      this.cbxCR.Size = new System.Drawing.Size(31, 22);
      this.cbxCR.TabIndex = 7;
      this.cbxCR.Text = "CR";
      this.cbxCR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxCR.UseVisualStyleBackColor = true;
      // 
      // cbxSendData
      // 
      this.cbxSendData.BackColor = System.Drawing.SystemColors.Info;
      this.cbxSendData.ContextMenuStrip = this.cmsTxData;
      this.cbxSendData.Dock = System.Windows.Forms.DockStyle.Fill;
      this.cbxSendData.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbxSendData.ForeColor = System.Drawing.Color.Black;
      this.cbxSendData.FormattingEnabled = true;
      this.cbxSendData.Items.AddRange(new object[] {
            "GHV",
            "GSV",
            "H"});
      this.cbxSendData.Location = new System.Drawing.Point(66, 0);
      this.cbxSendData.Name = "cbxSendData";
      this.cbxSendData.Size = new System.Drawing.Size(382, 23);
      this.cbxSendData.TabIndex = 4;
      // 
      // cmsTxData
      // 
      this.cmsTxData.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSaveText,
            this.mitClearAllLines,
            this.mitLoadText});
      this.cmsTxData.Name = "cmsMultiColorText";
      this.cmsTxData.Size = new System.Drawing.Size(147, 70);
      // 
      // mitSaveText
      // 
      this.mitSaveText.Name = "mitSaveText";
      this.mitSaveText.Size = new System.Drawing.Size(146, 22);
      this.mitSaveText.Text = "Save Text";
      this.mitSaveText.Click += new System.EventHandler(this.mitSaveText_Click);
      // 
      // mitClearAllLines
      // 
      this.mitClearAllLines.Name = "mitClearAllLines";
      this.mitClearAllLines.Size = new System.Drawing.Size(146, 22);
      this.mitClearAllLines.Text = "Clear all Lines";
      this.mitClearAllLines.Click += new System.EventHandler(this.mitClearAllLines_Click);
      // 
      // mitLoadText
      // 
      this.mitLoadText.Name = "mitLoadText";
      this.mitLoadText.Size = new System.Drawing.Size(146, 22);
      this.mitLoadText.Text = "Load Text";
      this.mitLoadText.Click += new System.EventHandler(this.mitLoadText_Click);
      // 
      // btnSendData
      // 
      this.btnSendData.ContextMenuStrip = this.cmsTxData;
      this.btnSendData.Dock = System.Windows.Forms.DockStyle.Right;
      this.btnSendData.Location = new System.Drawing.Point(448, 0);
      this.btnSendData.Name = "btnSendData";
      this.btnSendData.Size = new System.Drawing.Size(56, 45);
      this.btnSendData.TabIndex = 3;
      this.btnSendData.Text = "Send";
      this.btnSendData.UseVisualStyleBackColor = true;
      this.btnSendData.Click += new System.EventHandler(this.btnSendData_Click);
      // 
      // label1
      // 
      this.label1.ContextMenuStrip = this.cmsTxData;
      this.label1.Dock = System.Windows.Forms.DockStyle.Left;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(66, 45);
      this.label1.TabIndex = 1;
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FUCMultiColorText
      // 
      this.FUCMultiColorText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCMultiColorText.Font = new System.Drawing.Font("Courier New", 9F);
      this.FUCMultiColorText.Location = new System.Drawing.Point(0, 0);
      this.FUCMultiColorText.Name = "FUCMultiColorText";
      this.FUCMultiColorText.Size = new System.Drawing.Size(506, 202);
      this.FUCMultiColorText.TabIndex = 3;
      // 
      // DialogLoadStatementText
      // 
      this.DialogLoadStatementText.DefaultExt = "txt";
      this.DialogLoadStatementText.FileName = "TerminalStatements";
      this.DialogLoadStatementText.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogLoadStatementText.Title = "Load Terminal Statements";
      // 
      // DialogSaveStatementText
      // 
      this.DialogSaveStatementText.DefaultExt = "txt";
      this.DialogSaveStatementText.FileName = "TerminalStatements";
      this.DialogSaveStatementText.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogSaveStatementText.Title = "Save Terminal Statements";
      // 
      // CUCTerminal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCMultiColorText);
      this.Controls.Add(this.pnlInput);
      this.Name = "CUCTerminal";
      this.Size = new System.Drawing.Size(506, 249);
      this.pnlInput.ResumeLayout(false);
      this.pnlInput.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayLF)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayCharacter)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayCR)).EndInit();
      this.cmsTxData.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlInput;
    private System.Windows.Forms.Button btnSendData;
    private System.Windows.Forms.Label label1;
    private UCMultiColorText.CUCMultiColorText FUCMultiColorText;
    private System.Windows.Forms.ComboBox cbxSendData;
    private System.Windows.Forms.ContextMenuStrip cmsTxData;
    private System.Windows.Forms.ToolStripMenuItem mitSaveText;
    private System.Windows.Forms.ToolStripMenuItem mitClearAllLines;
    private System.Windows.Forms.ToolStripMenuItem mitLoadText;
    private System.Windows.Forms.OpenFileDialog DialogLoadStatementText;
    private System.Windows.Forms.SaveFileDialog DialogSaveStatementText;
    private System.Windows.Forms.NumericUpDown nudDelayCR;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.CheckBox cbxLF;
    private System.Windows.Forms.CheckBox cbxCR;
    private System.Windows.Forms.NumericUpDown nudDelayLF;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.NumericUpDown nudDelayCharacter;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
  }
}
