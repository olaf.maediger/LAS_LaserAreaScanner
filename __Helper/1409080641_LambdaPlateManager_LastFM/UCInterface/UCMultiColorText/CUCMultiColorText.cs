﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCMultiColorText
{
  //
  //##################################################
  //  Segment - Main - CUCMultiColorText
  //##################################################
  //
  public partial class CUCMultiColorText : UserControl
  { //
    //-------------------------------
    //  Segment - Constant
    //-------------------------------
    // Color Background
    private const Byte COLOR_BACKGROUND_A = 0xFF;
    private const Byte COLOR_BACKGROUND_R = 0xFF;
    private const Byte COLOR_BACKGROUND_G = 0xFF;
    private const Byte COLOR_BACKGROUND_B = 0xDD;
    // Color Focus
    private const Byte COLOR_FOCUS_A = 0xFF;
    private const Byte COLOR_FOCUS_R = 0xAA;
    private const Byte COLOR_FOCUS_G = 0xFF;
    private const Byte COLOR_FOCUS_B = 0xAA;
    //
    //-------------------------------
    //  Segment - Field
    //-------------------------------
    //
    // NC private List<Int32> FStartIndices;
    // NC private List<Int32> FStopIndices;
    //
    //-------------------------------
    //  Segment - Constructor
    //-------------------------------
    //
    public CUCMultiColorText()
    {
      InitializeComponent();
      //
      // NC FStartIndices = new List<Int32>;
      // NC FStopIndices = new List<Int32>;
      //
      //????lbxView.DrawMode = DrawMode.OwnerDrawFixed; // !!!!!!!!!!!!!!!!
      //????lbxView.DrawItem += new DrawItemEventHandler(lbxView_DrawItem);
      rtbView.BackColor = Color.FromArgb(COLOR_BACKGROUND_A,
                                         COLOR_BACKGROUND_R,
                                         COLOR_BACKGROUND_G,
                                         COLOR_BACKGROUND_B);
      


      //????lbxView.SelectedIndexChanged += new EventHandler(lbxView_SelectedIndexChanged);
      //
      // debug Add(Color.Red, "Red");
      // debug Add(Color.Green, "Green");
      // debug Add(Color.Blue, "Blue");
    }
    //
    //-------------------------------
    //  Segment - Event
    //-------------------------------
    //
   /* private void lbxView_DrawItem(object sender, DrawItemEventArgs e)
    {
      Int32 ItemIndex = e.Index;
      if (ItemIndex == lbxView.SelectedIndex)
      {
        Brush BR = new SolidBrush(Color.FromArgb(COLOR_FOCUS_A,
                                                 COLOR_FOCUS_R,
                                                 COLOR_FOCUS_G,
                                                 COLOR_FOCUS_B));
        e.Graphics.FillRectangle(BR, e.Bounds);
      } else
      {
        e.DrawBackground();
      }
      if (0 <= ItemIndex)
      {
        Color C = FColorlist[ItemIndex];
        Brush B = new SolidBrush(C);
        String Line = (String)lbxView.Items[ItemIndex];
        e.Graphics.DrawString(Line, e.Font, B, e.Bounds, StringFormat.GenericDefault);
        e.DrawFocusRectangle();
      }
    }*/

    /*private void lbxView_SelectedIndexChanged(object sender, EventArgs e)
    {
      lbxView.Invalidate();
    }*/
    //
    //-------------------------------
    //  Segment - Helper
    //-------------------------------
    //
    private void ClearAll()
    {
      rtbView.Clear();
      // NC FStartIndices.Clear();
      // NC FStopIndices.Clear();
    }

    private void AddColoredText(Color color, String text)
    {
      // NC FStartIndices.Add(rtbView.TextLength);
      //rtbView.AppendText(text);
      rtbView.SelectionColor = color;
      rtbView.SelectedText = text;
      // NC FStopIndices.Add(rtbView.TextLength - 1);
    }
    //
    //-------------------------------
    //  Segment - Menu
    //-------------------------------
    //
    private void mitClearAllLines_Click(object sender, EventArgs e)
    {
      ClearAll();
    }

    private void mitAddSpaceline_Click(object sender, EventArgs e)
    {
      Color C = Color.FromArgb(COLOR_BACKGROUND_A, COLOR_BACKGROUND_R,
                               COLOR_BACKGROUND_G, COLOR_BACKGROUND_B);
      AddColoredText(C, "");
    }
    //
    //-------------------------------
    //  Segment - Management
    //-------------------------------
    //
    public void Clear()
    {
      ClearAll();
    }

    public void Add(Color color, String text)
    {
      AddColoredText(color, text);
    }

    public void EnableText(Boolean enable)
    {
      rtbView.Enabled = enable;
    }


  }
}
