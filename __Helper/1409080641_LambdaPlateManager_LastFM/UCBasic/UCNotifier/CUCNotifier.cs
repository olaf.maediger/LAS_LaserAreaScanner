﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;

namespace UCNotifier
{
	public delegate void DWrite(String line);

	public partial class CUCNotifier : CUCNotifierBase
	{
		//
		//-------------------------------------
		//	Constant
		//-------------------------------------
		//

    //
		//-------------------------------------
		//	Constructor
		//-------------------------------------
		//
		public CUCNotifier()
		{
			InitializeComponent();
		}
    //
    //-------------------------------------
    //	Segment - Property
    //-------------------------------------

    //
    //-------------------------------------
    //	Methods - Get/SetData
    //-------------------------------------
    //
    public new Boolean GetData(out RNotifierData data)
    {
      try
      {
        return base.GetData(out data);
      }
      catch (Exception)
      {
        data = new RNotifierData();
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    public new Boolean SetData(RNotifierData data)
    {
      try
      {
        return base.SetData(data);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
		//-------------------------------------
		//	Methods - Initialisation
		//-------------------------------------
		//
		public new Boolean LoadInitdata(CInitdataReader initdata)
		{
			try
			{
				return base.LoadInitdata(initdata);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean SaveInitdata(CInitdataWriter initdata)
		{
			try
			{
				return base.SaveInitdata(initdata);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}
		//
		//
		//-------------------------------------
		//	Methods - Common
		//-------------------------------------
		//
		public new void Clear()
		{
			try
			{
				base.Clear();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
			}
		}

		public new void CopyToClipboard()
		{
			try
			{
				base.CopyToClipboard();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
			}
		}
		//
		//-------------------------------------
		//	Segment - Path-Management
		//-------------------------------------
		//   
    /* not accessible under Windows7
     * public new String GetAllUsersDesktopFolderPath()
    {
      try
      {
        return base.GetAllUsersDesktopFolderPath();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return "";
      }
    }*/

		public new String GetMyDocumentsFolderPath()
		{
			try
			{
				return base.GetMyDocumentsFolderPath();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return "";
			}
		}

		public new String BuildDateTimeFileEntry(String filenameextension)
		{
			try
			{
				return base.BuildDateTimeFileEntry(filenameextension);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return "";
			}
		}
		//
		//-------------------------------------
		//	Segment - Management Logbook
		//-------------------------------------
		//
		public new Boolean SetProtocolParameter(String targetpath,
																						String application,
																						String version,
                                            String companytop,
                                            String companysub,
                                            String user)
		{
			try
			{
				return base.SetProtocolParameter(targetpath, application, version, 
                                         companytop, companysub, user);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean EditParameterModal()
		{
			try
			{
				return base.EditParameterModal();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean LoadDiagnosticFileModal()
		{
			try
			{
				return base.LoadDiagnosticFileModal();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean SaveDiagnosticFileModal()
		{
			try
			{
				return base.SaveDiagnosticFileModal();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean SendDiagnosticEmail()
		{
			try
			{
				return base.SendDiagnosticEmail();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}
    //
    //---------------------------------------------------
    //	Segment - Management Product-Key - Application
    //---------------------------------------------------
    //
    public new Int32 GetApplicationCount()
    {
      return base.GetApplicationCount();
    }

    public new String GetApplicationProductKey(Int32 index)
    {
      return base.GetApplicationProductKey(index);
    }

    public new Boolean AddApplication(String name,
                                      String version,
                                      String date,
                                      String company,
                                      String street,
                                      String streetnumber,
                                      String postalcode,
                                      String city,
                                      Boolean discountuser,
                                      String user,
                                      String productkey)
    {
      try
      {
        return base.AddApplication(name, version, date,
                                   company, street, streetnumber,
                                   postalcode, city,
                                   discountuser, user,
                                   productkey);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean ShowApplicationProductKeyModal()
    {
      try
      {
        return base.ShowApplicationProductKeyModal();
      }
      catch (Exception)// e)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean CheckAllApplications(Int32 count)
    {
      try
      {
        return base.CheckAllApplications(count);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //---------------------------------------------------
    //	Segment - Management Product-Key - Device
    //---------------------------------------------------
    //
    public new Int32 GetDeviceCount()
    {
      return base.GetDeviceCount();
    }
    public new Int32 GetDeviceSerialNumber(Int32 index)
    {
      return base.GetDeviceSerialNumber(index);
    }
    public new String GetDeviceProductKey(Int32 index)
    {
      return base.GetDeviceProductKey(index);
    }

    public new Boolean AddDevice(String applicationkey,
                                 String name,
                                 String version,
                                 String date,
                                 Boolean discountuser,
                                 String user,
                                 Int32 serialnumber,
                                 String productkey)
    {
      try
      {
        return base.AddDevice(applicationkey,
                              name, version, date,
                              discountuser, user,
                              serialnumber, productkey);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
   
    public new Boolean ShowDeviceProductKeyModal()
    {
      try
      {
        return base.ShowDeviceProductKeyModal();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean CheckAllDevices(String applicationkey,
                                       Int32 count)
    {
      try
      {
        return base.CheckAllDevices(applicationkey, count);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean CheckDevice(String applicationkey,
                                   Int32 serialnumber)
    {
      try
      {
        return base.CheckDevice(applicationkey, serialnumber);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //---------------------------------------------------
    //	Segment - Management Product-Key - Common
    //---------------------------------------------------
    //    
    public new Boolean ShowSaveQuestionModal()
    {
      try
      {
        return base.ShowSaveQuestionModal();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

  }
}
