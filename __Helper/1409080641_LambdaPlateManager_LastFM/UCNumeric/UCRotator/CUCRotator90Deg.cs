﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCRotator
{
  public delegate void DOnAnglePresetChanged(Double anglepreset);
  public delegate void DOnAngleActualChanged(Double angleactual);
  public delegate void DOnAngleDeltaChanged(Double angledelta);

  public partial class CUCRotator90Deg : CUCRotator
  {
    private const Int32 BORDER_X = 40;
    private const Int32 BORDER_Y = 40;
    private Point[] FPointsArrowPreset;
    private Point[] FPointsArrowActual;
    private Double FAnglePreset;
    private Double FAngleActual;
    private Double FAngleDelta;
    private DOnAnglePresetChanged FOnAnglePresetChanged;
    private DOnAngleActualChanged FOnAngleActualChanged;
    private DOnAngleDeltaChanged FOnAngleDeltaChanged;

    public CUCRotator90Deg()
    {
      InitializeComponent();
      DoubleBuffered = true;
      //
      Paint += new PaintEventHandler(ThisOnPaint);
      Resize += new EventHandler(ThisOnResize);
      //
      BuildArrows();
      //
      FUCEditDoublePreset.SetOnValueChanged(UCEditDoublePresetOnValueChanged);
      FUCEditDoubleActual.SetOnValueChanged(UCEditDoubleActualOnValueChanged);
      FUCEditDoubleDelta.SetOnValueChanged(UCEditDoubleDeltaOnValueChanged);
    }



    public void SetOnAnglePresetChanged(DOnAnglePresetChanged value)
    {
      FOnAnglePresetChanged = value;
    }

    public void SetOnAngleActualChanged(DOnAngleActualChanged value)
    {
      FOnAngleActualChanged = value;
    }

    public void SetOnAngleDeltaChanged(DOnAngleDeltaChanged value)
    {
      FOnAngleDeltaChanged = value;
    }

    private void SetAnglePreset(Double angle)
    {
      if (angle != FAnglePreset)
      {
        FAnglePreset = angle;
        FUCEditDoublePreset.Value = FAnglePreset;
        if (FOnAnglePresetChanged is DOnAnglePresetChanged)
        {
          FOnAnglePresetChanged(FAnglePreset);
        }
      }
    }
    public Double AnglePreset
    {
      get { return FAnglePreset; }
      set { SetAnglePreset(value); }
    }

    private void SetAngleActual(Double angle)
    {
      if (angle != FAngleActual)
      {
        FAngleActual = angle;
        FUCEditDoubleActual.Value = FAngleActual;
        if (FOnAngleActualChanged is DOnAngleActualChanged)
        {
          FOnAngleActualChanged(FAngleActual);
        }
        Refresh();
      }
    }
    public Double AngleActual
    {
      get { return FAngleActual; }
      set { SetAngleActual(value); }
    }

    private void SetAngleDelta(Double angle)
    {
      if (angle != FAngleDelta)
      {
        FAngleDelta = angle;
        FUCEditDoubleDelta.Value = FAngleDelta;
        if (FOnAngleDeltaChanged is DOnAngleDeltaChanged)
        {
          FOnAngleDeltaChanged(FAngleDelta);
        }
      }
    }
    public Double AngleDelta
    {
      get { return FAngleDelta; }
      set { SetAngleDelta(value); }
    }

    private void BuildArrows()
    {
      Int32 L = Math.Min(ClientRectangle.Width, ClientRectangle.Height);
      // Preset
      FPointsArrowPreset = new Point[5];
      FPointsArrowPreset[0] = new Point((Int32)(L - 3.3 * BORDER_X), 0);
      FPointsArrowPreset[1] = new Point((Int32)(L - 4.3 * BORDER_X), BORDER_Y / 3);
      FPointsArrowPreset[2] = new Point((Int32)(-0.4 * BORDER_X), BORDER_Y / 3);
      FPointsArrowPreset[3] = new Point((Int32)(-0.4 * BORDER_X), -BORDER_Y / 3);
      FPointsArrowPreset[4] = new Point((Int32)(L - 4.3 * BORDER_X), -BORDER_Y / 3);
      // Actual
      FPointsArrowActual = new Point[5];
      FPointsArrowActual[0] = new Point((Int32)(L - 3.9 * BORDER_X), 0);
      FPointsArrowActual[1] = new Point((Int32)(L - 4.9 * BORDER_X), BORDER_Y / 5);
      FPointsArrowActual[2] = new Point((Int32)(-0.2 * BORDER_X), BORDER_Y / 5);
      FPointsArrowActual[3] = new Point((Int32)(-0.2 * BORDER_X), -BORDER_Y / 5);
      FPointsArrowActual[4] = new Point((Int32)(L - 4.9 * BORDER_X), -BORDER_Y / 5);
    }



    private void UCEditDoublePresetOnValueChanged(object sender, Double value)
    {
      SetAnglePreset(value);
    }
    private void UCEditDoubleActualOnValueChanged(object sender, Double value)
    {
      SetAngleActual(value);
    }
    private void UCEditDoubleDeltaOnValueChanged(object sender, Double value)
    {
      SetAngleDelta(value);
    }



    private void ThisOnResize(object sender,
                              EventArgs arguments)
    {
      Int32 L = Math.Min(ClientRectangle.Width, ClientRectangle.Height);
      lblHeader.Left = 0;
      if (ClientRectangle.Width < ClientRectangle.Height)
      {
        lblHeader.Top = (Int32)(ClientRectangle.Height - L);
        lblHeader.Width = L;
        FUCEditDoublePreset.Left = (Int32)(L - FUCEditDoublePreset.Width - 1.2 * BORDER_X);
        FUCEditDoublePreset.Top = (Int32)(ClientRectangle.Height - L + 1.2 * BORDER_Y);
      }
      else
      {
        lblHeader.Top = 0;
        lblHeader.Width = L;
        FUCEditDoublePreset.Left = (Int32)(L - FUCEditDoublePreset.Width - 1.2 * BORDER_X);
        FUCEditDoublePreset.Top = (Int32)(1.2 * BORDER_Y);
      }
      FUCEditDoubleActual.Left = FUCEditDoublePreset.Left;
      FUCEditDoubleActual.Top = (Int32)(0.2 * BORDER_Y) + FUCEditDoublePreset.Top + FUCEditDoublePreset.Height;
      FUCEditDoubleDelta.Left = FUCEditDoubleActual.Left;
      FUCEditDoubleDelta.Top = (Int32)(0.2 * BORDER_Y) + FUCEditDoubleActual.Top + FUCEditDoubleActual.Height;
      BuildArrows();
    }


    private void ThisOnPaint(object sender,
                             PaintEventArgs arguments)
    {
      Int32 L = Math.Min(ClientRectangle.Width, ClientRectangle.Height);
      //
      Graphics G = arguments.Graphics;
      G.ResetTransform();
      Matrix MS = G.Transform;
      Matrix M0 = new Matrix(1, 0, 0, -1, 0, this.ClientSize.Height);
      G.Transform = M0;
      //
      // Origin
      //
      // Rectangle
      Brush BS = new SolidBrush(Color.Bisque);
      G.FillRectangle(BS, 0, 0, L, L);
      float DX = BORDER_X;
      float DY = BORDER_Y;
      Brush BR = new SolidBrush(Color.Beige);
      Pen PR = new Pen(Color.Green, 2);
      G.FillRectangle(BR, DX, DY, L - 2 * DX, L - 2 * DY);
      G.DrawRectangle(PR, DX, DY, L - 2 * DX, L - 2 * DY);
      PR.Dispose();
      BR.Dispose();
      //
      G.TranslateTransform(DX, DY);
      //
      // ArcOuter
      float A0 = 0;
      float DA = 90;
      float RO = L;
      Pen PAO = new Pen(Color.Green, 2);
      DX = 2 * BORDER_X;
      DY = 2 * BORDER_Y;
      G.DrawArc(PAO, -RO + DX, -RO + DY, 2 * RO - 2 * DX, 2 * RO - 2 * DY, A0, DA);
      PAO.Dispose();
      //
      // ArcInner
      float RI = L - BORDER_Y;
      DX = 3 * BORDER_X;
      DY = 3 * BORDER_Y;
      Pen PAI = new Pen(Color.Green, 2);
      G.DrawArc(PAI, -RI + DX, -RI + DY, 2 * RI - 2 * DX, 2 * RI - 2 * DY, A0, DA);
      PAI.Dispose();
      //
      // Scale
      M0 = G.Transform;
      DX = L - 3.8f * BORDER_X;
      DY = L - 4.2f * BORDER_X;
      Pen PS = new Pen(Color.Green, 2);
      for (Int32 AI = 1; AI < 19; AI++)
      {
        G.RotateTransform(5f);
        if (0 == AI % 2)
        {
          G.DrawLine(PS, 0, 0, DX, 0);
        }
        else
        {
          G.DrawLine(PS, DY, 0, DX, 0);
        }
      }
      G.Transform = M0;
      // 45-Line
      G.RotateTransform(45f);
      G.DrawLine(PS, 0, 0, DX, 0);
      G.Transform = M0;
      PS.Dispose();
      //
      // Text
      G.Transform = MS;
      DX = L - 2.8f * BORDER_X;
      Int32 X, Y;
      Brush BT = new SolidBrush(Color.Green);
      Brush BBT = new SolidBrush(Color.Yellow);
      Font FT = new Font("Arial", 20, FontStyle.Bold);
      StringFormat SFT = new StringFormat();
      SFT.Alignment = StringAlignment.Center;
      SFT.LineAlignment = StringAlignment.Center;
      for (Int32 AI = 0; AI <= 90; AI += 10)
      {
        SetRotation((0 + AI) * Math.PI / 180);
        X = (Int32)(BORDER_X + RotateX((Int32)DX, 0));
        Y = (Int32)(-BORDER_Y + ClientRectangle.Height - RotateY((Int32)DX, 0));
        G.FillEllipse(BBT, X - 30, Y - 30, 60, 60);
        G.DrawString(String.Format("{0}", AI), FT, BT, X, Y, SFT);
      }
      //
      // ArrowPreset
      G.Transform = M0;
      G.RotateTransform((float)FAnglePreset);
      Pen PAP = new Pen(Color.Blue, 3);
      Brush BAP = new SolidBrush(Color.SteelBlue);
      G.FillPolygon(BAP, FPointsArrowPreset);
      G.DrawPolygon(PAP, FPointsArrowPreset);
      PAP.Dispose();
      BAP.Dispose();
      //
      // ArrowActual
      G.RotateTransform(-(float)FAnglePreset);
      G.RotateTransform((float)FAngleActual);
      Pen PAA = new Pen(Color.Red, 3);
      Brush BAA = new SolidBrush(Color.IndianRed);
      G.FillPolygon(BAA, FPointsArrowActual);
      G.DrawPolygon(PAA, FPointsArrowActual);
      PAA.Dispose();
      BAA.Dispose();
      //
      // Dispose
      MS.Dispose();
      M0.Dispose();
    }



    public void MoveStepUp()
    {
      SetAnglePreset(AnglePreset + AngleDelta);
    }

    public void MoveStepDown()
    {
      SetAnglePreset(AnglePreset - AngleDelta);
    }


  }
}
