﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
//
using Initdata;
using UCNotifier;
//
using NLExpression;
using NLStatement;
using NLProgram;
//
using NLStatementSystem;
using NLStatementComPort;
//
using XmlFile;
//
namespace UCProgram
{
  public partial class CUCProgram : UserControl
  {
    //
    //-----------------------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    //
    // Program from Xml/Ascii-File
    private CProgram FProgram;
    //
    //-----------------------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------------------
    //
    public CUCProgram()
    {
      InitializeComponent();
      //
      FProgram = new CProgram();
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FProgram.SetNotifier(FNotifier);
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Public Management
    //-----------------------------------------------------------------------
    //
    public Boolean AddStatement(CStatement statement)
    {
      return FProgram.AddStatement(statement);
    }

    public Boolean StartExecution()
    {
      return FProgram.StartExecution();
    }

    public void EnableControls(Boolean enable)
    {
      this.Enabled = enable;
    }

    private void btnAddDelay_Click(object sender, EventArgs e)
    {
      NLStatementSystem.CStatementDelay Statement = new CStatementDelay("1000");
      Statement.ExecutionKind = EExecutionKind.DeleteAfterExecution;
      FProgram.AddStatement(Statement);
      FProgram.Translate();
      FProgram.StartExecution();
    }

  }
}
