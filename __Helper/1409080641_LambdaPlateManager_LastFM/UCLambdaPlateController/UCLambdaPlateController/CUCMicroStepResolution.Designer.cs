﻿namespace UCLambdaPlateController
{
  partial class CUCMicroStepResolution
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gbxMicrostepResolution = new System.Windows.Forms.GroupBox();
      this.rbtOnePerSixteen = new System.Windows.Forms.RadioButton();
      this.rbtOnePerEight = new System.Windows.Forms.RadioButton();
      this.rbtQuarter = new System.Windows.Forms.RadioButton();
      this.rbtHalf = new System.Windows.Forms.RadioButton();
      this.rbtOnePerThirtyTwo = new System.Windows.Forms.RadioButton();
      this.rbtFull = new System.Windows.Forms.RadioButton();
      this.gbxMicrostepResolution.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbxMicrostepResolution
      // 
      this.gbxMicrostepResolution.Controls.Add(this.rbtOnePerSixteen);
      this.gbxMicrostepResolution.Controls.Add(this.rbtOnePerEight);
      this.gbxMicrostepResolution.Controls.Add(this.rbtQuarter);
      this.gbxMicrostepResolution.Controls.Add(this.rbtHalf);
      this.gbxMicrostepResolution.Controls.Add(this.rbtOnePerThirtyTwo);
      this.gbxMicrostepResolution.Controls.Add(this.rbtFull);
      this.gbxMicrostepResolution.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gbxMicrostepResolution.Location = new System.Drawing.Point(0, 0);
      this.gbxMicrostepResolution.Name = "gbxMicrostepResolution";
      this.gbxMicrostepResolution.Size = new System.Drawing.Size(134, 96);
      this.gbxMicrostepResolution.TabIndex = 7;
      this.gbxMicrostepResolution.TabStop = false;
      this.gbxMicrostepResolution.Text = " Microstep-Resolution ";
      // 
      // rbtOnePerSixteen
      // 
      this.rbtOnePerSixteen.AutoSize = true;
      this.rbtOnePerSixteen.Location = new System.Drawing.Point(78, 47);
      this.rbtOnePerSixteen.Name = "rbtOnePerSixteen";
      this.rbtOnePerSixteen.Size = new System.Drawing.Size(48, 17);
      this.rbtOnePerSixteen.TabIndex = 5;
      this.rbtOnePerSixteen.Text = "1/16";
      this.rbtOnePerSixteen.UseVisualStyleBackColor = true;
      this.rbtOnePerSixteen.CheckedChanged += new System.EventHandler(this.rbtOnePerSixteen_CheckedChanged);
      // 
      // rbtOnePerEight
      // 
      this.rbtOnePerEight.AutoSize = true;
      this.rbtOnePerEight.Location = new System.Drawing.Point(78, 24);
      this.rbtOnePerEight.Name = "rbtOnePerEight";
      this.rbtOnePerEight.Size = new System.Drawing.Size(42, 17);
      this.rbtOnePerEight.TabIndex = 4;
      this.rbtOnePerEight.Text = "1/8";
      this.rbtOnePerEight.UseVisualStyleBackColor = true;
      this.rbtOnePerEight.CheckedChanged += new System.EventHandler(this.rbtOnePerEight_CheckedChanged);
      // 
      // rbtQuarter
      // 
      this.rbtQuarter.AutoSize = true;
      this.rbtQuarter.Location = new System.Drawing.Point(16, 70);
      this.rbtQuarter.Name = "rbtQuarter";
      this.rbtQuarter.Size = new System.Drawing.Size(60, 17);
      this.rbtQuarter.TabIndex = 3;
      this.rbtQuarter.Text = "Quarter";
      this.rbtQuarter.UseVisualStyleBackColor = true;
      this.rbtQuarter.CheckedChanged += new System.EventHandler(this.rbtQuarter_CheckedChanged);
      // 
      // rbtHalf
      // 
      this.rbtHalf.AutoSize = true;
      this.rbtHalf.Location = new System.Drawing.Point(16, 47);
      this.rbtHalf.Name = "rbtHalf";
      this.rbtHalf.Size = new System.Drawing.Size(44, 17);
      this.rbtHalf.TabIndex = 2;
      this.rbtHalf.Text = "Half";
      this.rbtHalf.UseVisualStyleBackColor = true;
      this.rbtHalf.CheckedChanged += new System.EventHandler(this.rbtHalf_CheckedChanged);
      // 
      // rbtOnePerThirtyTwo
      // 
      this.rbtOnePerThirtyTwo.AutoSize = true;
      this.rbtOnePerThirtyTwo.Location = new System.Drawing.Point(78, 70);
      this.rbtOnePerThirtyTwo.Name = "rbtOnePerThirtyTwo";
      this.rbtOnePerThirtyTwo.Size = new System.Drawing.Size(48, 17);
      this.rbtOnePerThirtyTwo.TabIndex = 1;
      this.rbtOnePerThirtyTwo.Text = "1/32";
      this.rbtOnePerThirtyTwo.UseVisualStyleBackColor = true;
      this.rbtOnePerThirtyTwo.CheckedChanged += new System.EventHandler(this.rbtOnePerThirtyTwo_CheckedChanged);
      // 
      // rbtFull
      // 
      this.rbtFull.AutoSize = true;
      this.rbtFull.Checked = true;
      this.rbtFull.Location = new System.Drawing.Point(16, 24);
      this.rbtFull.Name = "rbtFull";
      this.rbtFull.Size = new System.Drawing.Size(41, 17);
      this.rbtFull.TabIndex = 0;
      this.rbtFull.TabStop = true;
      this.rbtFull.Text = "Full";
      this.rbtFull.UseVisualStyleBackColor = true;
      this.rbtFull.CheckedChanged += new System.EventHandler(this.rbtFull_CheckedChanged);
      // 
      // CUCMicroStepResolution
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.gbxMicrostepResolution);
      this.Name = "CUCMicroStepResolution";
      this.Size = new System.Drawing.Size(134, 96);
      this.gbxMicrostepResolution.ResumeLayout(false);
      this.gbxMicrostepResolution.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox gbxMicrostepResolution;
    private System.Windows.Forms.RadioButton rbtOnePerSixteen;
    private System.Windows.Forms.RadioButton rbtOnePerEight;
    private System.Windows.Forms.RadioButton rbtQuarter;
    private System.Windows.Forms.RadioButton rbtHalf;
    private System.Windows.Forms.RadioButton rbtOnePerThirtyTwo;
    private System.Windows.Forms.RadioButton rbtFull;
  }
}
