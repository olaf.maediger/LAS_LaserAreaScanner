﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UCNotifier;
//
namespace UCLambdaPlateController
{ //
  //----------------------------------------------------------------------
  //  Section - Global Type
  //----------------------------------------------------------------------
  //
  public enum EModeMicroStep
  {
    Full = 0,
    Half = 1,
    Quarter = 2,
    OnePerEight = 3,
    OnePerSixteen = 4,
    OnePerThirtyTwo = 5
  }

  public delegate void DOnModeMicroStepChanged(EModeMicroStep value);
  //
  //----------------------------------------------------------------------
  //  Section - Main
  //----------------------------------------------------------------------
  //
  public partial class CUCMicroStepResolution : UserControl
  { //
    //----------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------
    //
    public const Double RESOLUTION_FULLSTEP = 1.80000;  // [deg]
    public const Double RESOLUTION_HALFSTEP = 0.90000;  // [deg]
    public const Double RESOLUTION_QUARTERSTEP = 0.45000;  // [deg]
    public const Double RESOLUTION_ONEPEREIGHT = 0.22500;  // [deg]
    public const Double RESOLUTION_ONEPERSIXTEEN = 0.11250;  // [deg]
    public const Double RESOLUTION_ONEPERTHIRTYTWO = 0.05625;  // [deg]
    //
    //----------------------------------------------------------------------
    //  Section - Field
    //----------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnModeMicroStepChanged FOnModeMicroStepChanged;
    //
    //----------------------------------------------------------------------
    //  Section - Constructor
    //----------------------------------------------------------------------
    //
    public CUCMicroStepResolution()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnModeMicroStepChanged(DOnModeMicroStepChanged value)
    {
      FOnModeMicroStepChanged = value;
    }

    public EModeMicroStep GetModeMicroStep()
    {
      if (rbtHalf.Checked)
      {
        return EModeMicroStep.Half;
      }
      if (rbtQuarter.Checked)
      {
        return EModeMicroStep.Quarter;
      }
      if (rbtOnePerEight.Checked)
      {
        return EModeMicroStep.OnePerEight;
      }
      if (rbtOnePerSixteen.Checked)
      {
        return EModeMicroStep.OnePerSixteen;
      }
      if (rbtOnePerThirtyTwo.Checked)
      {
        return EModeMicroStep.OnePerThirtyTwo;
      }
      // if (rbtFull.Checked)
      return EModeMicroStep.Full;
    }
    public void SetModeMicroStep(EModeMicroStep value)
    {
      if (value != GetModeMicroStep())
      {
        switch (value)
        {
          case EModeMicroStep.Half:
            rbtHalf.Checked = true;
            break;
          case EModeMicroStep.Quarter:
            rbtQuarter.Checked = true;
            break;
          case EModeMicroStep.OnePerEight:
            rbtOnePerEight.Checked = true;
            break;
          case EModeMicroStep.OnePerSixteen:
            rbtOnePerSixteen.Checked = true;
            break;
          case EModeMicroStep.OnePerThirtyTwo:
            rbtOnePerThirtyTwo.Checked = true;
            break;
          default: // Full
            rbtFull.Checked = true;
            break;
        }
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Event
    //----------------------------------------------------------------------
    //
    private void rbtFull_CheckedChanged(object sender, EventArgs e)
    {
      if (rbtFull.Checked)
      {
        if (FOnModeMicroStepChanged is DOnModeMicroStepChanged)
        {
          FOnModeMicroStepChanged(EModeMicroStep.Full);
        }
      }
    }
    private void rbtHalf_CheckedChanged(object sender, EventArgs e)
    {
      if (rbtHalf.Checked)
      {
        if (FOnModeMicroStepChanged is DOnModeMicroStepChanged)
        {
          FOnModeMicroStepChanged(EModeMicroStep.Half);
        }
      }
    }

    private void rbtQuarter_CheckedChanged(object sender, EventArgs e)
    {
      if (rbtQuarter.Checked)
      {
        if (FOnModeMicroStepChanged is DOnModeMicroStepChanged)
        {
          FOnModeMicroStepChanged(EModeMicroStep.Quarter);
        }
      }
    }

    private void rbtOnePerEight_CheckedChanged(object sender, EventArgs e)
    {
      if (rbtOnePerEight.Checked)
      {
        if (FOnModeMicroStepChanged is DOnModeMicroStepChanged)
        {
          FOnModeMicroStepChanged(EModeMicroStep.OnePerEight);
        }
      }
    }

    private void rbtOnePerSixteen_CheckedChanged(object sender, EventArgs e)
    {
      if (rbtOnePerSixteen.Checked)
      {
        if (FOnModeMicroStepChanged is DOnModeMicroStepChanged)
        {
          FOnModeMicroStepChanged(EModeMicroStep.OnePerSixteen);
        }
      }
    }

    private void rbtOnePerThirtyTwo_CheckedChanged(object sender, EventArgs e)
    {
      if (rbtOnePerThirtyTwo.Checked)
      {
        if (FOnModeMicroStepChanged is DOnModeMicroStepChanged)
        {
          FOnModeMicroStepChanged(EModeMicroStep.OnePerThirtyTwo);
        }
      }
    }



    public Double MicroStepToResolution(EModeMicroStep value)
    {
      switch (value)
      {
        case EModeMicroStep.Half:
          return CUCMicroStepResolution.RESOLUTION_HALFSTEP;
        case EModeMicroStep.Quarter:
          return CUCMicroStepResolution.RESOLUTION_QUARTERSTEP;
        case EModeMicroStep.OnePerEight:
          return CUCMicroStepResolution.RESOLUTION_ONEPEREIGHT;
        case EModeMicroStep.OnePerSixteen:
          return CUCMicroStepResolution.RESOLUTION_ONEPERSIXTEEN;
        case EModeMicroStep.OnePerThirtyTwo:
          return CUCMicroStepResolution.RESOLUTION_ONEPERTHIRTYTWO;
        default:// EModeMicroStep.Full
          return CUCMicroStepResolution.RESOLUTION_FULLSTEP;
      }
    }

    public EModeMicroStep ResolutionToMicroStep(Double value)
    {
      if (value <= CUCMicroStepResolution.RESOLUTION_ONEPERTHIRTYTWO)
      {
        return EModeMicroStep.OnePerThirtyTwo;
      }
      if (value <= CUCMicroStepResolution.RESOLUTION_ONEPERSIXTEEN)
      {
        return EModeMicroStep.OnePerSixteen;
      }
      if (value <= CUCMicroStepResolution.RESOLUTION_ONEPEREIGHT)
      {
        return EModeMicroStep.OnePerEight;
      }
      if (value <= CUCMicroStepResolution.RESOLUTION_QUARTERSTEP)
      {
        return EModeMicroStep.Quarter;
      }
      if (value <= CUCMicroStepResolution.RESOLUTION_HALFSTEP)
      {
        return EModeMicroStep.Half;
      }
      // CUCMicroStepResolution.RESOLUTION_FULLSTEP
      return EModeMicroStep.Full;
    }



  }
}
