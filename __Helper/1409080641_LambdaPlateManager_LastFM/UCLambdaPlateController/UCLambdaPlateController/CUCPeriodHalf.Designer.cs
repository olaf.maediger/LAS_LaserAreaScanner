﻿namespace UCLambdaPlateController
{
  partial class CUCPeriodHalf
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gbxPeriodHalf = new System.Windows.Forms.GroupBox();
      this.nudPeriodHalf = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.lblPeriod = new System.Windows.Forms.Label();
      this.lblFrequency = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.gbxPeriodHalf.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriodHalf)).BeginInit();
      this.SuspendLayout();
      // 
      // gbxPeriodHalf
      // 
      this.gbxPeriodHalf.Controls.Add(this.lblFrequency);
      this.gbxPeriodHalf.Controls.Add(this.label5);
      this.gbxPeriodHalf.Controls.Add(this.lblPeriod);
      this.gbxPeriodHalf.Controls.Add(this.label2);
      this.gbxPeriodHalf.Controls.Add(this.label1);
      this.gbxPeriodHalf.Controls.Add(this.nudPeriodHalf);
      this.gbxPeriodHalf.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gbxPeriodHalf.Location = new System.Drawing.Point(0, 0);
      this.gbxPeriodHalf.Name = "gbxPeriodHalf";
      this.gbxPeriodHalf.Size = new System.Drawing.Size(162, 95);
      this.gbxPeriodHalf.TabIndex = 0;
      this.gbxPeriodHalf.TabStop = false;
      this.gbxPeriodHalf.Text = " PeriodHalf ";
      // 
      // nudPeriodHalf
      // 
      this.nudPeriodHalf.Location = new System.Drawing.Point(81, 19);
      this.nudPeriodHalf.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudPeriodHalf.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPeriodHalf.Name = "nudPeriodHalf";
      this.nudPeriodHalf.Size = new System.Drawing.Size(65, 20);
      this.nudPeriodHalf.TabIndex = 0;
      this.nudPeriodHalf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPeriodHalf.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPeriodHalf.ValueChanged += new System.EventHandler(this.nudPeriodHalf_ValueChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(16, 21);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(59, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Preset [ms]";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(16, 47);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(59, 13);
      this.label2.TabIndex = 3;
      this.label2.Text = "Period [ms]";
      // 
      // lblPeriod
      // 
      this.lblPeriod.BackColor = System.Drawing.SystemColors.Info;
      this.lblPeriod.Location = new System.Drawing.Point(98, 47);
      this.lblPeriod.Name = "lblPeriod";
      this.lblPeriod.Size = new System.Drawing.Size(48, 13);
      this.lblPeriod.TabIndex = 4;
      this.lblPeriod.Text = "-";
      this.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblFrequency
      // 
      this.lblFrequency.BackColor = System.Drawing.SystemColors.Info;
      this.lblFrequency.Location = new System.Drawing.Point(98, 69);
      this.lblFrequency.Name = "lblFrequency";
      this.lblFrequency.Size = new System.Drawing.Size(48, 13);
      this.lblFrequency.TabIndex = 6;
      this.lblFrequency.Text = "-";
      this.lblFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(16, 69);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(79, 13);
      this.label5.TabIndex = 5;
      this.label5.Text = "Frequency [Hz]";
      // 
      // CUCPeriodHalf
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.gbxPeriodHalf);
      this.Name = "CUCPeriodHalf";
      this.Size = new System.Drawing.Size(162, 95);
      this.gbxPeriodHalf.ResumeLayout(false);
      this.gbxPeriodHalf.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudPeriodHalf)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox gbxPeriodHalf;
    private System.Windows.Forms.Label lblFrequency;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label lblPeriod;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudPeriodHalf;
  }
}
