﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  public class CTransmitTextDelay : CStatement
  {
    public const String INIT_NAME = "TransmitTextDelay";

    private String FData;
    private Int32 FDelayCharacter;
    private Int32 FDelayCR;
    private Int32 FDelayLF;

		public CTransmitTextDelay(CStatementlist parent,									            
									            CNotifier notifier,
									            CDevice device,
									            CHWComPort comport,
                              String data,
                              Int32 delaycharacter,
                              Int32 delaycr,
                              Int32 delaylf)
      : base(parent, INIT_NAME,
             "",
             notifier, device, comport)
    {
      SetParent(this);
      //SetOnStatementExecuteBegin(null);
      //SetOnStatementExecuteBusy(ThreadExecuteBusy);
      //SetOnStatementExecuteResponse(null);
      //SetOnStatementExecuteEnd(null);
      //
      FData = data;
      FDelayCharacter = delaycharacter;
      FDelayCR = delaycr;
      FDelayLF = delaylf;
    }

    protected void ThreadExecuteBusy()
    {
      String TxdBuffer = "";
      foreach (Char C in FData)
      {
        switch (C)
        {
          case '\r':
            if (0 < TxdBuffer.Length)
            {
              HWComPort.WriteText(TxdBuffer);
              TxdBuffer = "";
            }
            HWComPort.WriteCharacter('\r');
            if (0 < FDelayCR)
            {
              Thread.Sleep(FDelayCR);
            }
            break;
          case '\n':
            if (0 < TxdBuffer.Length)
            {
              HWComPort.WriteText(TxdBuffer);
              TxdBuffer = "";
            }
            HWComPort.WriteCharacter('\n');
            if (0 < FDelayLF)
            {
              Thread.Sleep(FDelayLF);
            }
            break;
          default:
            if (0 == FDelayCharacter)
            {
              TxdBuffer += C;
            }
            else
            {
              HWComPort.WriteCharacter(C);
              Thread.Sleep(FDelayCharacter);
            }
            break;
        }
      }
      if (0 < TxdBuffer.Length)
      {
        HWComPort.WriteText(TxdBuffer);
      }
      // Add Variable
      Variable = new CVariableTextVector(2);
      Variable.Name = INIT_NAME;
      Variable.Time = CNotifier.BuildDateTimeHeader();
      ((CVariableTextVector)Variable).SetIndexText(0, FData);
    }

    /*public override Boolean ExecuteDataReceived(String data)
    {
      ((CVariableTextVector)Variable).SetIndexText(1, data);
      return true;
    }*/


  }
}
