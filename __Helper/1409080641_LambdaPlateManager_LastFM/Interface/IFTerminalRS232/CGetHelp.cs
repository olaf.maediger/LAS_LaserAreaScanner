﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using HWComPort;

namespace IFTerminalRS232
{
	class CGetHelp : CStatement
	{
		private const String INIT_NAME = "GetHelp";
    public const String COMMAND_TEXT = "H";

		public CGetHelp(CStatementlist parent,
									  CNotifier notifier,
									  CDevice shutter,
									  CHWComPort comport)
      : base(parent, INIT_NAME, COMMAND_TEXT, notifier, shutter, comport)
		{
      SetParent(this);
      //SetOnStatementExecuteResponse(SelfOnSerialResponseReceived);
    }

    public void SelfOnSerialResponseReceived(Guid comportid,
                                             String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
			Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
			String[] Tokenlist = line.Split(' ');
      if (1 < Tokenlist.Length)
			{
        //Statementlist.Library.RefreshHelp();//Tokenlist);
			}
		}

	}
}


