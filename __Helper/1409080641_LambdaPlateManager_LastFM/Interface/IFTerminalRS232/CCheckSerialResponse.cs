﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  class CCheckSerialResponse : CStatement
  {
    public const String INIT_NAME = "CheckSerialResponse";

    private String FBaudrate;
    private String FStatement;
    private String FResponse;
   //??? private String FCollector;

		public CCheckSerialResponse(CStatementlist parent,
								                CNotifier notifier,
									              CDevice device,
									              CHWComPort comport,
                                String baudrate,
                                String command,
                                String response)
      : base(parent, INIT_NAME,
             String.Format("{0}", command),
             notifier, device, comport)
    {
      SetParent(this);
      //SetOnStatementExecuteBegin(null);
      //SetOnStatementExecuteBusy(ThreadExecuteBusy);
      //SetOnStatementExecuteResponse(null);
      //SetOnStatementExecuteEnd(null);
      //
      FBaudrate = baudrate;
      FStatement = command;
      FResponse = response;
      //?????FCollector = "";
    }

    protected void ThreadExecuteBusy()
    {
      HWComPort.WriteText(FStatement);
      // Add Variable
      Variable = new CVariableTextVector(3);
      Variable.Name = INIT_NAME;
      Variable.Time = CNotifier.BuildDateTimeHeader();
      ((CVariableTextVector)Variable).SetIndexText(0, FBaudrate);
      ((CVariableTextVector)Variable).SetIndexText(1, FStatement);
    }

    /*//public override Boolean ExecuteDataReceived(String data)
    {
      //((CVariableTextVector)Variable).SetIndexText(2, data);
      return true;
    }*/



  }
}
