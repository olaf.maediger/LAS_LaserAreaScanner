﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  public class CSerialPortsAvailable : CStatement
  {
    public const String INIT_NAME = "SerialPortsAvailable";

		public CSerialPortsAvailable(CStatementlist parent,									            
									               CNotifier notifier,
									               CDevice device,
									               CHWComPort comport)
      : base(parent, INIT_NAME,
             "",
             notifier, device, comport)
    {
      SetParent(this);
      //SetOnStatementExecuteBegin(null);
      //SetOnStatementExecuteBusy(ThreadExecuteBusy);
      //SetOnStatementExecuteResponse(null);
      //SetOnStatementExecuteEnd(null);
    }

    protected void ThreadExecuteBusy()
    {
      String[] SerialPortsAvailable = CHWComPort.GetSelectablePorts();
      if (null != SerialPortsAvailable)
      {
        CVariableTextVector VariableTextVector = new CVariableTextVector(SerialPortsAvailable.Length);
        VariableTextVector.Name = INIT_NAME;
        VariableTextVector.Time = CNotifier.BuildDateTimeHeader();
        for (Int32 I = 0; I < SerialPortsAvailable.Length; I++)
        {
          VariableTextVector.SetIndexText(I, SerialPortsAvailable[I]);
        }
        VariablelistParent.Add(VariableTextVector);
      }
    }

    /*//public override Boolean ExecuteDataReceived(String data)
    {
      return true;
    }*/


  }
}
