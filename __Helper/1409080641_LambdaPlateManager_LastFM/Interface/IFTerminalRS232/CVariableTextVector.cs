﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace IFTerminalRS232
{
  public class CVariableTextVector : CVariable
  {
    //
    //------------------------------------------------------------------
    //  Segment Field
    //------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private String[] FTextVector;
    //
    //------------------------------------------------------------------
    //  Segment Constructor
    //------------------------------------------------------------------
    //
    public CVariableTextVector(Int32 size)
      : base(EVariableKind.vkTextVector)
    {
      FTextVector = new String[size];
      for (Int32 IT = 0; IT < size; IT++)
      {
        FTextVector[IT] = "";
      }
    }

    public new void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetIndexText(Int32 index, String text)
    {
      if ((0 <= index) && (index < FTextVector.Length))
      {
        FTextVector[index] = text;
      }
    }

    public String GetIndexText(Int32 index)
    {
      if ((0 <= index) && (index < FTextVector.Length))
      {
        return FTextVector[index];
      }
      return "";
    }


    public override void Debug()
    {
      base.Debug();
      FNotifier.Write(String.Format("  Kind : {0}", FKind.ToString()));
      String SValue = "  Text :";
      for (Int32 IT = 0; IT < FTextVector.Length; IT++)
      {
        SValue += String.Format(" [{0}]<{1}>", IT, FTextVector[IT]);
      }
      FNotifier.Write(SValue);
    }

  }
}
