﻿namespace UCComPort
{
  partial class CUCDatabits
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.cbxDatabits = new System.Windows.Forms.ComboBox();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(4, 6);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(52, 13);
      this.label1.TabIndex = 7;
      this.label1.Text = "Databits :";
      // 
      // cbxDatabits
      // 
      this.cbxDatabits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxDatabits.FormattingEnabled = true;
      this.cbxDatabits.Location = new System.Drawing.Point(57, 3);
      this.cbxDatabits.Name = "cbxDatabits";
      this.cbxDatabits.Size = new System.Drawing.Size(35, 21);
      this.cbxDatabits.TabIndex = 6;
      // 
      // CUCDatabits
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label1);
      this.Controls.Add(this.cbxDatabits);
      this.Name = "CUCDatabits";
      this.Size = new System.Drawing.Size(96, 28);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox cbxDatabits;
  }
}
