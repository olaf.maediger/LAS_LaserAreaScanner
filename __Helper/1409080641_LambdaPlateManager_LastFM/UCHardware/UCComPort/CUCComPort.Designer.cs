﻿namespace UCComPort
{
  partial class CUCComPort
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnRefresh = new System.Windows.Forms.Button();
      this.FUCPortsSelectable = new UCComPort.CUCPortsSelectable();
      this.FUCPortsInstalled = new UCComPort.CUCPortsInstalled();
      this.FUCOpenClose = new UCComPort.CUCOpenClose();
      this.FUCPortName = new UCComPort.CUCPortName();
      this.FUCHandshake = new UCComPort.CUCHandshake();
      this.FUCDatabits = new UCComPort.CUCDatabits();
      this.FUCStopbits = new UCComPort.CUCStopbits();
      this.FUCParity = new UCComPort.CUCParity();
      this.FUCBaudrate = new UCComPort.CUCBaudrate();
      this.SuspendLayout();
      // 
      // btnRefresh
      // 
      this.btnRefresh.Location = new System.Drawing.Point(367, 9);
      this.btnRefresh.Name = "btnRefresh";
      this.btnRefresh.Size = new System.Drawing.Size(75, 23);
      this.btnRefresh.TabIndex = 20;
      this.btnRefresh.Text = "Refresh";
      this.btnRefresh.UseVisualStyleBackColor = true;
      this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
      // 
      // FUCPortsSelectable
      // 
      this.FUCPortsSelectable.Location = new System.Drawing.Point(185, 5);
      this.FUCPortsSelectable.Name = "FUCPortsSelectable";
      this.FUCPortsSelectable.Size = new System.Drawing.Size(176, 31);
      this.FUCPortsSelectable.TabIndex = 19;
      // 
      // FUCPortsInstalled
      // 
      this.FUCPortsInstalled.Location = new System.Drawing.Point(4, 5);
      this.FUCPortsInstalled.Name = "FUCPortsInstalled";
      this.FUCPortsInstalled.Size = new System.Drawing.Size(173, 30);
      this.FUCPortsInstalled.TabIndex = 18;
      // 
      // FUCOpenClose
      // 
      this.FUCOpenClose.Location = new System.Drawing.Point(268, 95);
      this.FUCOpenClose.Name = "FUCOpenClose";
      this.FUCOpenClose.Size = new System.Drawing.Size(94, 39);
      this.FUCOpenClose.TabIndex = 17;
      // 
      // FUCPortName
      // 
      this.FUCPortName.Location = new System.Drawing.Point(3, 37);
      this.FUCPortName.Name = "FUCPortName";
      this.FUCPortName.Size = new System.Drawing.Size(359, 26);
      this.FUCPortName.TabIndex = 16;
      // 
      // FUCHandshake
      // 
      this.FUCHandshake.Location = new System.Drawing.Point(104, 99);
      this.FUCHandshake.Name = "FUCHandshake";
      this.FUCHandshake.Size = new System.Drawing.Size(152, 31);
      this.FUCHandshake.TabIndex = 15;
      // 
      // FUCDatabits
      // 
      this.FUCDatabits.Location = new System.Drawing.Point(266, 67);
      this.FUCDatabits.Name = "FUCDatabits";
      this.FUCDatabits.Size = new System.Drawing.Size(95, 28);
      this.FUCDatabits.TabIndex = 14;
      // 
      // FUCStopbits
      // 
      this.FUCStopbits.Location = new System.Drawing.Point(2, 98);
      this.FUCStopbits.Name = "FUCStopbits";
      this.FUCStopbits.Size = new System.Drawing.Size(99, 30);
      this.FUCStopbits.TabIndex = 13;
      // 
      // FUCParity
      // 
      this.FUCParity.Location = new System.Drawing.Point(164, 65);
      this.FUCParity.Name = "FUCParity";
      this.FUCParity.Size = new System.Drawing.Size(104, 30);
      this.FUCParity.TabIndex = 12;
      // 
      // FUCBaudrate
      // 
      this.FUCBaudrate.Location = new System.Drawing.Point(4, 66);
      this.FUCBaudrate.Name = "FUCBaudrate";
      this.FUCBaudrate.Size = new System.Drawing.Size(185, 27);
      this.FUCBaudrate.TabIndex = 11;
      // 
      // CUCComPort
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnRefresh);
      this.Controls.Add(this.FUCPortsSelectable);
      this.Controls.Add(this.FUCPortsInstalled);
      this.Controls.Add(this.FUCOpenClose);
      this.Controls.Add(this.FUCPortName);
      this.Controls.Add(this.FUCHandshake);
      this.Controls.Add(this.FUCDatabits);
      this.Controls.Add(this.FUCStopbits);
      this.Controls.Add(this.FUCParity);
      this.Controls.Add(this.FUCBaudrate);
      this.Name = "CUCComPort";
      this.Size = new System.Drawing.Size(447, 137);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnRefresh;
    private UCComPort.CUCPortsSelectable FUCPortsSelectable;
    private UCComPort.CUCPortsInstalled FUCPortsInstalled;
    private UCComPort.CUCOpenClose FUCOpenClose;
    private UCComPort.CUCPortName FUCPortName;
    private UCComPort.CUCHandshake FUCHandshake;
    private UCComPort.CUCDatabits FUCDatabits;
    private UCComPort.CUCStopbits FUCStopbits;
    private UCComPort.CUCParity FUCParity;
    private UCComPort.CUCBaudrate FUCBaudrate;
  }
}
