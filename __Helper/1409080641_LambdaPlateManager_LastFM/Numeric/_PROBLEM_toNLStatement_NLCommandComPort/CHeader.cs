﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLStatementComPort
{
  public class CHeader
  {
    public static String HEADER_LIBRARY = "NLCommandComPort";

    public enum EErrorCode
    {
      None = 0,
      InvalidAccess,
      Unknown
    };

    public static readonly String[] ERRORS = 
    { 
      "None",
			"Invalid Access",
  	  "Unknown"
    };

  }
}
