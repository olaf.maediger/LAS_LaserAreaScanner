﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using UCNotifier;
using HWComPort;
using Task;
using NLStatement;
using NLExpression;
//
namespace NLStatementComPort
{
  public class CStatementTransmitTextDelay : CStatement
  {
    public const String INIT_NAME = "TransmitTextDelay";

    private CHWComPort FComPort;
    private String FData;
    private Int32 FDelayCharacter;
    private Int32 FDelayCR;
    private Int32 FDelayLF;

    public CStatementTransmitTextDelay(CHWComPort comport,
                                       String data,
                                       Int32 delaycharacter,
                                       Int32 delaycr,
                                       Int32 delaylf)
      : base(INIT_NAME)
    {
      //SetOnStatementExecute(OnStatementExecute);
      //SetOnStatementExecuteBegin(ThreadExecuteBegin);
      //SetOnStatementExecuteBusy(ThreadExecuteBusy);
      //SetOnStatementExecuteResponse(ThreadExecuteResponse);
      //SetOnStatementExecuteEnd(ThreadExecuteEnd);
      //
      FComPort = comport;
      FData = data;
      FDelayCharacter = delaycharacter;
      FDelayCR = delaycr;
      FDelayLF = delaylf;
    }

    public override Boolean Translate(ref CConstantlist constantlist,
                                      ref CVariablelist variablelist)
    {
      try
      {
        /*!!!!!!!String Line = String.Format("Translate While['{0}' {1}]", FTextExpression, FStatementBlock.ToString());
        Notifier.Write(Line);
        FExpression = new CExpression(ref constantlist,
                                      ref variablelist);
        FExpression.Translate(FTextExpression);
        //
        if (FStatementBlock is CStatementBlock)
        {
          FStatementBlock.Translate(ref constantlist,
                                  ref variablelist);
        }*/
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    protected override Boolean OnStatementExecute()
    {
      return true;
    }


    /*protected Boolean ThreadExecuteBegin()
    {
      return true;
    }
    protected Boolean ThreadExecuteResponse(Guid portid, String data)
    {
      return true;
    }
    protected Boolean ThreadExecuteEnd()
    {
      return true;
    }


    protected Boolean ThreadExecuteBusy()
    {
      try
      {
        String TxdBuffer = "";
        foreach (Char C in FData)
        {
          switch (C)
          {
            case '\r':
              if (0 < TxdBuffer.Length)
              {
                FComPort.WriteText(TxdBuffer);
                TxdBuffer = "";
              }
              FComPort.WriteCharacter('\r');
              if (0 < FDelayCR)
              {
                Thread.Sleep(FDelayCR);
              }
              break;
            case '\n':
              if (0 < TxdBuffer.Length)
              {
                FComPort.WriteText(TxdBuffer);
                TxdBuffer = "";
              }
              FComPort.WriteCharacter('\n');
              if (0 < FDelayLF)
              {
                Thread.Sleep(FDelayLF);
              }
              break;
            default:
              if (0 == FDelayCharacter)
              {
                TxdBuffer += C;
              }
              else
              {
                FComPort.WriteCharacter(C);
                Thread.Sleep(FDelayCharacter);
              }
              break;
          }
        }
        if (0 < TxdBuffer.Length)
        {
          FComPort.WriteText(TxdBuffer);
        }
        // Add Variable
        / *????Variable = new CVariableTextVector(2);
        Variable.Name = INIT_NAME;
        Variable.Time = CNotifier.BuildDateTimeHeader();
        ((CVariableTextVector)Variable).SetIndexText(0, FData);
         * /
        return true;
      }
      catch (Exception)
      {
        Int32 EI = (Int32)EErrorCode.InvalidAccess;
        Notifier.Error(CHeader.HEADER_LIBRARY, EI, ERRORS[EI]);
        return false;
      }
    }*/

    /*//!!!!!!!!!!public override Boolean ExecuteDataReceived(String data)
    {
      ((CVariableTextVector)Variable).SetIndexText(1, data);
      return true;
    }*/

    /*public override Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines)
    {
      CVariable Variable = FVariablelist.FindName(FVariableName);
      if (Variable is CVariable)
      {
        String Line = BuildIdentation(identation);
        if (Variable.IsDefined())
        {
          Line += String.Format("{0} = {1};", Variable.Name, FTextExpression);
        }
        else
        {
          Line += String.Format("{0} {1} = {2};", Variable.Type, Variable.Name, FTextExpression);
        }
        lines.Add(Line);
        return true;
      }
      return false;
    }*/

  }
}
