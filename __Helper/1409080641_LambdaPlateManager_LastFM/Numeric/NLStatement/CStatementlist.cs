﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NLStatement
{
  public class CStatementlist : List<CStatement>
  {

    public Boolean ListProgram(ref Int32 identation)
    {
      foreach (CStatement Statement in this)
      {
        if (!Statement.List(ref identation))
        {
          return false;
        }
      }
      return true;
    }


    public new Boolean Add(CStatement statement)
    {
      if (statement is CStatement)
      {
        base.Add(statement);
        return true;
      }
      return false;
    }

  }
}
