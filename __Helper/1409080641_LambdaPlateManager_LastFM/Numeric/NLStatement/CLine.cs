﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NLStatement
{
  public class CLine 
  {
    private String FValue;
    public String Value
    {
      get { return FValue; }
      set { FValue = value; }
    }
  }

  public class CLinelist : List<CLine>
  {
    public void Add(String line)
    {
      CLine Line = new CLine();
      Line.Value = line;
      base.Add(Line);
    }
  }
}
