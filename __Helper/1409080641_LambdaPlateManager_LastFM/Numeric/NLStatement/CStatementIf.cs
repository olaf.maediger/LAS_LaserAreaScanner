﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
//
namespace NLStatement
{

  // <if> <expressionboolean> <then> <statementtrue> <else> <statementfalse>


  public class CStatementIf : CStatement
  {
		private CVariablelist FVariablelist;
    private String FTextExpression;
    private CExpression FExpression;
    private CStatementBlock FStatementBlockTrue;
    private CStatementBlock FStatementBlockFalse;

		public String Expression
		{
			get { return FTextExpression; }
		}

    public CStatementBlock StatementBlockTrue
		{
      get { return FStatementBlockTrue; }
		}

    public CStatementBlock StatementBlockFalse
		{
      get { return FStatementBlockFalse; }
		}


    public CStatementIf(CVariablelist variablelist,
											  String textexpression,
                        CStatementBlock statementblocktrue,
											  CStatementBlock statementblockfalse)
      : base(CStatementHeader.NAME_IF)
    {
      //SetOnStatementExecuteBegin(null);
      //SetOnStatementExecuteBusy(ThreadExecuteBusy);
      //SetOnStatementExecuteResponse(null);
      //SetOnStatementExecuteEnd(null);
      //
      FVariablelist = variablelist;
      FTextExpression = textexpression;
      FExpression = null;
      FStatementBlockTrue = statementblocktrue;
      if (FStatementBlockTrue is CStatement)
			{
        FStatementBlockTrue.Name = CStatementHeader.NAME_BLOCKTRUE;
			}
      FStatementBlockFalse = statementblockfalse;
      if (FStatementBlockFalse is CStatement)
			{
        FStatementBlockFalse.Name = CStatementHeader.NAME_BLOCKFALSE;
			}
    }

    public override Boolean Translate(ref CConstantlist constantlist,
                                      ref CVariablelist variablelist)
    {
			try
			{
        String Line = String.Format("Translate If[{0}", FTextExpression);
        if (FStatementBlockTrue is CStatementBlock)
        {
          Line += String.Format(" then {0}", FStatementBlockTrue.ToString());
        }
        if (FStatementBlockFalse is CStatementBlock)
        {
          Line += String.Format(" else {0}", FStatementBlockFalse.ToString());
        }
        Line += "]";
        Notifier.Write(Line);
        FExpression = new CExpression(ref constantlist,
                                      ref variablelist);
				FExpression.Translate(FTextExpression);
				//
        if (FStatementBlockTrue is CStatement)
				{
          FStatementBlockTrue.Translate(ref constantlist,
                                        ref variablelist);
				}
        if (FStatementBlockFalse is CStatement)
				{
          FStatementBlockFalse.Translate(ref constantlist,
                                         ref variablelist);
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
    }

    public Boolean ThreadExecuteBusy()
    {
      Double DValue = 0.0;
      if (FExpression.Calculate(ref DValue))
      {
        if (0 != (Int32)DValue)
        { // Expression true ->
          if (FStatementBlockTrue is CStatementBlock)
					{
            String Line = String.Format("Execute If[true -> {0}]:", FStatementBlockTrue.ToString());
						Notifier.Write(Line);
            //FBlockTrue.StartExecution...
            return FStatementBlockTrue.StartExecution(null);
					}
        } else
        { // Expression false ->
          if (FStatementBlockFalse is CStatementBlock)
					{
            String Line = String.Format("Execute If[false -> {0}]:", FStatementBlockFalse.ToString());
						Notifier.Write(Line);
            //FBlockFalse.StartExecution(...
            return FStatementBlockFalse.StartExecution(null);
					}
        }
      }
      return false;
    }


    /*public override Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines)
    {
      // If
      String Line = BuildIdentation(identation);
      Line += String.Format("if ({0})", FTextExpression);
      lines.Add(Line);
      // BlockTrue      
      Line = BuildIdentation(identation) + "{";
      lines.Add(Line);
      if (FBlockTrue is CStatementBlock)
      {
        identation++;
        FBlockTrue.WriteToAscii(ref identation, ref lines);
        identation--;
      }
      // BlockFalse
      if (FBlockFalse is CStatementBlock)
      { // } else
        Line = BuildIdentation(identation) + "} else";
        lines.Add(Line);
        // {
        Line = BuildIdentation(identation) + "{";
        lines.Add(Line);
        //
        identation++;
        FBlockFalse.WriteToAscii(ref identation, ref lines);
        identation--;
        // }
        Line = BuildIdentation(identation) + "}";
        lines.Add(Line);
      } else
      { // no else!
        Line = BuildIdentation(identation) + "}";
        lines.Add(Line);
      }
      //
      return true;
    }*/


  }
}
