﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLStatement
{
  class CHeader
  {
    public static String HEADER_LIBRARY = "NLStatement";

    public enum EErrorCode
    {
      None = 0,
      InvalidAccess,
      Unknown
    };

    public static readonly String[] ERRORS = 
    { 
      "None",
			"Invalid Access",
  	  "Unknown"
    };

  }
}

