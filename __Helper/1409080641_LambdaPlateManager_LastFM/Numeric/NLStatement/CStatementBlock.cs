﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Task;
using NLExpression;
using UCNotifier;
//
namespace NLStatement
{ //  
  //  Syntax: <commandblock> := <command> {0, N}
  //
  public class CStatementBlock : CStatement
  {
    //private CConstantlist FConstantlist;
		//private CVariablelist FVariablelist;
		private CStatementlist FStatements;
    private CEvent FEvent;

    public CStatementBlock()
			: base(CStatementHeader.NAME_BLOCK)
		{
      SetOnStatementExecuteBegin(null);
      SetOnStatementExecuteBusy(ThreadExecuteBusy);
      SetOnStatementExecuteResponse(null);
      SetOnStatementExecuteEnd(null);
      //
      //???FConstantlist = null;
			//FVariablelist = null;
			FStatements = new CStatementlist();			
      //
      FEvent = new CEvent();
    }



		public CStatementlist Statementlist
		{
			get { return FStatements; }
		}



    public Boolean AddStatement(CStatement command)
		{
			if (command is CStatement)
			{
				command.SetNotifier(Notifier);
				FStatements.Add(command);
				return true;
			}
			return false;
		}

    public override bool Translate(ref CConstantlist constantlist,
                                   ref CVariablelist variablelist)
    {
      String Line = "StatementBlock - Translate";
			Notifier.Write(Line);
			foreach (CStatement Statement in FStatements)
      {
        if (!Statement.Translate(ref constantlist,
                               ref variablelist))
        {
          return false;
        }
      }
      return true;
    }

    public Boolean ThreadExecuteBusy()
    {
      String Line = "StatementBlock - ExecuteBusy";
      Notifier.Write(Line);
      Boolean Result = true;
      for (Int32 SI = 0; SI < FStatements.Count; SI++)
      {
        CStatement Statement = FStatements[SI];
        Statement.StartExecution(StatementOnExecuteEnd);
        Result &= FEvent.WaitFor(20000);
        if (!Result)
        {
          Notifier.Error(CHeader.HEADER_LIBRARY, 2, "StatementBlock Execution failed");
          return false;
        }
      }
      return Result;
    }

    private void StatementOnExecuteEnd(CStatement statement)
    {
      FEvent.Pulse();
    }

    /*public override Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines)
    {
      foreach (CStatement Statement in FStatements)
      {
        // if (!Statement.WriteToAscii(ref identation, ref lines))
        {
          return false;
        }
      }
      return true;
    }*/



    public Boolean DeleteStatement(CStatement statement)
    {
      FStatements.Remove(statement);
      return (!FStatements.Contains(statement));
    }

  }
}
