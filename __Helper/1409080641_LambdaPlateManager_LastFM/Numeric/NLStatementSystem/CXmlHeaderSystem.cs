﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
using NLStatement;
using XmlFile;
//
namespace NLStatementSystem
{
	//
	//******************************************
	//  --- XmlHeader ---
	//******************************************
	//  
  public class CXmlHeaderSystem : CXmlHeader
  { //
		//------------------------------------------
		//	Constant
		//------------------------------------------
		//
    //	Statement - Extension (Library)
    //
    public const String HEADER_LIBRARY = "NLStatementSystem";
    //
    public const String NAME_SQUARE = "Square";
    public const String NAME_DELAY = "Delay";
	
		
	}
}

