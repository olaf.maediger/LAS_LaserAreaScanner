﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using NLExpression;
using NLStatement;
using XmlFile;
using UCNotifier;
//
namespace NLStatementSystem
{ //
  //  Syntax: <commanddelay> <expressionms>
  //
  public class CStatementDelay : CStatement
  { //
    //---------------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------------
    //
    private String FTextExpression;
    private CExpression FExpression;
    //
    //---------------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------------
    //
    public CStatementDelay(String textexpression)
      : base(CXmlHeaderSystem.NAME_DELAY)
    {
      FTextExpression = textexpression;
      FExpression = null;
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------------
    //
    public String TextExpression
    {
      get { return FTextExpression; }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------------------------------
    //
    public override bool Translate(ref CConstantlist constantlist,
                                   ref CVariablelist variablelist)
    {
      String Line = String.Format("Translate Delay[{0}]", FTextExpression);
      Notifier.Write(Line);
      FExpression = new CExpression(ref constantlist, ref variablelist);
      return FExpression.Translate(FTextExpression);
    }

    public Boolean ThreadExecuteBusy()
    {
      //
      Double DValue = 0.0;
      if (!FExpression.Calculate(ref DValue))
      {
        return false;
      }
      //
      Double DResult = DValue * DValue;
      //
      String Line = String.Format("Execute Delay[{0}] = {1}", DValue, DResult);
      Notifier.Write(Line);
      //
      // -> Push!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
      //
      return true;
    }


  }
}
