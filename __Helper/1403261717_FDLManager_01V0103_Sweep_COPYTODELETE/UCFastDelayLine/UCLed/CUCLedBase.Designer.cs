﻿namespace UCLed
{
  partial class CUCLedBase
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblTitle = new System.Windows.Forms.Label();
      this.pnlLed = new System.Windows.Forms.Panel();
      this.SuspendLayout();
      // 
      // lblTitle
      // 
      this.lblTitle.BackColor = System.Drawing.Color.Gold;
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(87, 23);
      this.lblTitle.TabIndex = 0;
      this.lblTitle.Text = "<led>";
      this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lblTitle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CUCLedBase_MouseClick);
      // 
      // pnlLed
      // 
      this.pnlLed.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlLed.Location = new System.Drawing.Point(0, 23);
      this.pnlLed.Name = "pnlLed";
      this.pnlLed.Size = new System.Drawing.Size(87, 61);
      this.pnlLed.TabIndex = 1;
      this.pnlLed.TabStop = true;
      this.pnlLed.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlLed_Paint);
      this.pnlLed.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CUCLedBase_MouseClick);
      // 
      // CUCLedBase
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.pnlLed);
      this.Controls.Add(this.lblTitle);
      this.Name = "CUCLedBase";
      this.Size = new System.Drawing.Size(87, 84);
      this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CUCLedBase_MouseClick);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.Panel pnlLed;
  }
}
