﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCLed
{
  public delegate void DOnActiveChanged(object sender, 
                                        COnActiveChangedData data);

  public class COnActiveChangedData : EventArgs
  {
    private Boolean FActive;
    public Boolean Active
    {
      get { return FActive; }
      set { FActive = value; }
    }

    public COnActiveChangedData(Boolean value)
    {
      Active = value;
    }
  }
}
