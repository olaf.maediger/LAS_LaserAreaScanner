﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCLed
{

  public partial class CUCLedBase : UserControl
  {
    private const Int32 INIT_BORDER = 4;


    private Boolean FActive;
    protected Color FColorBorderActive;
    protected Color FColorBorderPassive;
    protected Color FColorAreaActive;
    protected Color FColorAreaPassive;
    public event DOnActiveChanged OnActiveChanged;
  
    public CUCLedBase()
    {
      InitializeComponent();
      FColorBorderActive = Color.FromArgb(0x80, 0x99, 0x99, 0x99);
      FColorBorderPassive = Color.FromArgb(0x80, 0x44, 0x44, 0x44);
      FColorAreaActive = Color.Red;
      FColorAreaPassive = Color.Gray;
      //Active = true;
      //Active = false;
    }

    
    private void pnlLed_Paint(object sender, PaintEventArgs e)
    {
      Graphics G = e.Graphics;
      Pen P;
      Brush B;
      Int32 S = Math.Min(pnlLed.Width, pnlLed.Height);
      Int32 L = (pnlLed.Width - S)/ 2;
      Int32 T = 2;
      S -= 4;
      Rectangle R = new Rectangle(L, T, S, S);
      if (Active)
      {
        P = new Pen(FColorBorderActive);
        B = new SolidBrush(FColorAreaActive);
      }
      else
      {
        P = new Pen(FColorBorderPassive);
        B = new SolidBrush(FColorAreaPassive);
      }
      P.Width = 3;
      G.FillEllipse(B, R);
      G.DrawEllipse(P, R);
    }

    protected void CUCLedBase_MouseClick(object sender, MouseEventArgs e)
    {
      Active = !Active;
    }

    private void CUCLedBase_KeyDown(object sender, KeyEventArgs e)
    {
      if (Keys.Space == e.KeyCode)
      {
        Active = !Active;
      }
    }


    private void SetActive(Boolean value)
    {
      if (value != FActive)
      {
        FActive = value;
        pnlLed.Invalidate();
        if (OnActiveChanged is DOnActiveChanged)
        {
          OnActiveChanged(this, new COnActiveChangedData(FActive));
        }
      }
    }
    public Boolean Active
    {
      get { return FActive; }
      set { SetActive(value); }
    }


    private void SetColorBorderActive(Color value)
    {
      FColorBorderActive = value;
      pnlLed.Invalidate();
    }
    public Color ColorBorderActive
    {
      get { return FColorBorderActive; }
      set { SetColorBorderActive(value); }
    }

    private void SetColorBorderPassive(Color value)
    {
      FColorBorderPassive = value;
      pnlLed.Invalidate();
    }
    public Color ColorBorderPassive
    {
      get { return FColorBorderPassive; }
      set { SetColorBorderPassive(value); }
    }


    private void SetColorAreaActive(Color value)
    {
      FColorAreaActive = value;
      pnlLed.Invalidate();
    }
    public Color ColorAreaActive
    {
      get { return FColorAreaActive; }
      set { SetColorAreaActive(value); }
    }

    private void SetColorAreaPassive(Color value)
    {
      FColorAreaPassive = value;
      pnlLed.Invalidate();
    }
    public Color ColorAreaPassive
    {
      get { return FColorAreaPassive; }
      set { SetColorAreaPassive(value); }
    }


    private void SetTitle(String value)
    {
      lblTitle.Text = value;
    }
    public String Title
    {
      get { return lblTitle.Text; }
      set { SetTitle(value); }
    }


  }
}

