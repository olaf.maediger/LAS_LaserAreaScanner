﻿namespace UCFastDelayLine
{
  partial class CUCVersion
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.gbxVersion = new System.Windows.Forms.GroupBox();
      this.btnGetHelp = new System.Windows.Forms.Button();
      this.lblHardwareVersion = new System.Windows.Forms.Label();
      this.lblSoftwareVersion = new System.Windows.Forms.Label();
      this.btnGetVersion = new System.Windows.Forms.Button();
      this.gbxVersion.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbxVersion
      // 
      this.gbxVersion.Controls.Add(this.btnGetHelp);
      this.gbxVersion.Controls.Add(this.lblHardwareVersion);
      this.gbxVersion.Controls.Add(this.lblSoftwareVersion);
      this.gbxVersion.Controls.Add(this.btnGetVersion);
      this.gbxVersion.Location = new System.Drawing.Point(2, 2);
      this.gbxVersion.Name = "gbxVersion";
      this.gbxVersion.Size = new System.Drawing.Size(210, 81);
      this.gbxVersion.TabIndex = 86;
      this.gbxVersion.TabStop = false;
      this.gbxVersion.Text = " Firmware Version ";
      // 
      // btnGetHelp
      // 
      this.btnGetHelp.Location = new System.Drawing.Point(130, 48);
      this.btnGetHelp.Name = "btnGetHelp";
      this.btnGetHelp.Size = new System.Drawing.Size(70, 23);
      this.btnGetHelp.TabIndex = 59;
      this.btnGetHelp.Text = "Get Help";
      this.btnGetHelp.UseVisualStyleBackColor = true;
      this.btnGetHelp.Click += new System.EventHandler(this.btnGetHelp_Click);
      // 
      // lblHardwareVersion
      // 
      this.lblHardwareVersion.BackColor = System.Drawing.SystemColors.Info;
      this.lblHardwareVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblHardwareVersion.Location = new System.Drawing.Point(13, 49);
      this.lblHardwareVersion.Name = "lblHardwareVersion";
      this.lblHardwareVersion.Size = new System.Drawing.Size(106, 21);
      this.lblHardwareVersion.TabIndex = 58;
      this.lblHardwareVersion.Text = "<hardwareversion>";
      this.lblHardwareVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblSoftwareVersion
      // 
      this.lblSoftwareVersion.BackColor = System.Drawing.SystemColors.Info;
      this.lblSoftwareVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblSoftwareVersion.Location = new System.Drawing.Point(13, 22);
      this.lblSoftwareVersion.Name = "lblSoftwareVersion";
      this.lblSoftwareVersion.Size = new System.Drawing.Size(106, 21);
      this.lblSoftwareVersion.TabIndex = 58;
      this.lblSoftwareVersion.Text = "<softwareversion>";
      this.lblSoftwareVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnGetVersion
      // 
      this.btnGetVersion.Location = new System.Drawing.Point(130, 21);
      this.btnGetVersion.Name = "btnGetVersion";
      this.btnGetVersion.Size = new System.Drawing.Size(70, 23);
      this.btnGetVersion.TabIndex = 57;
      this.btnGetVersion.Text = "Get Version";
      this.btnGetVersion.UseVisualStyleBackColor = true;
      this.btnGetVersion.Click += new System.EventHandler(this.btnGetVersion_Click);
      // 
      // CUCVersion
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.gbxVersion);
      this.Name = "CUCVersion";
      this.Size = new System.Drawing.Size(214, 84);
      this.gbxVersion.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox gbxVersion;
    private System.Windows.Forms.Button btnGetHelp;
    private System.Windows.Forms.Label lblHardwareVersion;
    private System.Windows.Forms.Label lblSoftwareVersion;
    private System.Windows.Forms.Button btnGetVersion;
  }
}
