﻿namespace UCFastDelayLine
{
  partial class CUCFastDelayLineBase
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlInput = new System.Windows.Forms.Panel();
      this.gbxSweepChannelA = new System.Windows.Forms.GroupBox();
      this.rbtSweepStateHighLowLowHighChannelA = new System.Windows.Forms.RadioButton();
      this.rbtSweepStateHighLowHighLowChannelA = new System.Windows.Forms.RadioButton();
      this.rbtSweepStateLowHighHighLowChannelA = new System.Windows.Forms.RadioButton();
      this.rbtSweepStateLowHighLowHighChannelA = new System.Windows.Forms.RadioButton();
      this.rbtSweepStateIdleChannelA = new System.Windows.Forms.RadioButton();
      this.label16 = new System.Windows.Forms.Label();
      this.nudSweepRepetitionCountChannelA = new System.Windows.Forms.NumericUpDown();
      this.label17 = new System.Windows.Forms.Label();
      this.nudSweepStepCountChannelA = new System.Windows.Forms.NumericUpDown();
      this.label18 = new System.Windows.Forms.Label();
      this.nudSweepStepDeltaChannelA = new System.Windows.Forms.NumericUpDown();
      this.label19 = new System.Windows.Forms.Label();
      this.nudSweepLimitHighChannelA = new System.Windows.Forms.NumericUpDown();
      this.label20 = new System.Windows.Forms.Label();
      this.nudSweepLimitLowChannelA = new System.Windows.Forms.NumericUpDown();
      this.gbxSweepChannelB = new System.Windows.Forms.GroupBox();
      this.rbtSweepStateHighLowLowHighChannelB = new System.Windows.Forms.RadioButton();
      this.rbtSweepStateHighLowHighLowChannelB = new System.Windows.Forms.RadioButton();
      this.rbtSweepStateLowHighHighLowChannelB = new System.Windows.Forms.RadioButton();
      this.rbtSweepStateLowHighLowHighChannelB = new System.Windows.Forms.RadioButton();
      this.rbtSweepStateIdleChannelB = new System.Windows.Forms.RadioButton();
      this.label11 = new System.Windows.Forms.Label();
      this.nudSweepRepetitionCountChannelB = new System.Windows.Forms.NumericUpDown();
      this.label12 = new System.Windows.Forms.Label();
      this.nudSweepStepCountChannelB = new System.Windows.Forms.NumericUpDown();
      this.label13 = new System.Windows.Forms.Label();
      this.nudSweepStepDeltaChannelB = new System.Windows.Forms.NumericUpDown();
      this.label14 = new System.Windows.Forms.Label();
      this.nudSweepLimitHighChannelB = new System.Windows.Forms.NumericUpDown();
      this.label15 = new System.Windows.Forms.Label();
      this.nudSweepLimitLowChannelB = new System.Windows.Forms.NumericUpDown();
      this.gbxChannelDataB = new System.Windows.Forms.GroupBox();
      this.hsbDelayChannelB = new System.Windows.Forms.HScrollBar();
      this.cbxDelayChannelBUnit = new System.Windows.Forms.CheckBox();
      this.lblDelayChannelB = new System.Windows.Forms.Label();
      this.nudDelayChannelB = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.FUCVersion = new UCFastDelayLine.CUCVersion();
      this.gbxLed = new System.Windows.Forms.GroupBox();
      this.FUCLedRed3Busy = new UCLed.CUCLedRed();
      this.FUCLedRed7Error = new UCLed.CUCLedRed();
      this.gbxChannelDataA = new System.Windows.Forms.GroupBox();
      this.hsbDelayChannelA = new System.Windows.Forms.HScrollBar();
      this.cbxDelayChannelAUnit = new System.Windows.Forms.CheckBox();
      this.lblDelayChannelA = new System.Windows.Forms.Label();
      this.nudDelayChannelA = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.gbxCOMSelection = new System.Windows.Forms.GroupBox();
      this.label4 = new System.Windows.Forms.Label();
      this.lblDeviceType = new System.Windows.Forms.Label();
      this.btnOpenClosePort = new System.Windows.Forms.Button();
      this.label7 = new System.Windows.Forms.Label();
      this.cbxPortsPossible = new System.Windows.Forms.ComboBox();
      this.label5 = new System.Windows.Forms.Label();
      this.cbxPortsSelectable = new System.Windows.Forms.ComboBox();
      this.cbxPortsInstalled = new System.Windows.Forms.ComboBox();
      this.lblSelected = new System.Windows.Forms.Label();
      this.ttpHint = new System.Windows.Forms.ToolTip(this.components);
      this.tmrRefreshDelayChannelA = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshDelayChannelB = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshSweepLimitLowChannelA = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshSweepLimitLowChannelB = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshSweepLimitHighChannelA = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshSweepRepetitionCountChannelA = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshSweepStepCountChannelA = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshSweepStepDeltaChannelA = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshSweepLimitHighChannelB = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshSweepStepDeltaChannelB = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshSweepRepetitionCountChannelB = new System.Windows.Forms.Timer(this.components);
      this.tmrRefreshSweepStepCountChannelB = new System.Windows.Forms.Timer(this.components);
      this.FUCTerminal = new UCTerminal.CUCTerminal();
      this.pnlInput.SuspendLayout();
      this.gbxSweepChannelA.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepRepetitionCountChannelA)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepStepCountChannelA)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepStepDeltaChannelA)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepLimitHighChannelA)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepLimitLowChannelA)).BeginInit();
      this.gbxSweepChannelB.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepRepetitionCountChannelB)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepStepCountChannelB)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepStepDeltaChannelB)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepLimitHighChannelB)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepLimitLowChannelB)).BeginInit();
      this.gbxChannelDataB.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayChannelB)).BeginInit();
      this.gbxLed.SuspendLayout();
      this.gbxChannelDataA.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayChannelA)).BeginInit();
      this.gbxCOMSelection.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlInput
      // 
      this.pnlInput.Controls.Add(this.gbxSweepChannelA);
      this.pnlInput.Controls.Add(this.gbxSweepChannelB);
      this.pnlInput.Controls.Add(this.gbxChannelDataB);
      this.pnlInput.Controls.Add(this.FUCVersion);
      this.pnlInput.Controls.Add(this.gbxLed);
      this.pnlInput.Controls.Add(this.gbxChannelDataA);
      this.pnlInput.Controls.Add(this.gbxCOMSelection);
      this.pnlInput.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlInput.Location = new System.Drawing.Point(0, 0);
      this.pnlInput.Name = "pnlInput";
      this.pnlInput.Size = new System.Drawing.Size(694, 364);
      this.pnlInput.TabIndex = 84;
      // 
      // gbxSweepChannelA
      // 
      this.gbxSweepChannelA.Controls.Add(this.rbtSweepStateHighLowLowHighChannelA);
      this.gbxSweepChannelA.Controls.Add(this.rbtSweepStateHighLowHighLowChannelA);
      this.gbxSweepChannelA.Controls.Add(this.rbtSweepStateLowHighHighLowChannelA);
      this.gbxSweepChannelA.Controls.Add(this.rbtSweepStateLowHighLowHighChannelA);
      this.gbxSweepChannelA.Controls.Add(this.rbtSweepStateIdleChannelA);
      this.gbxSweepChannelA.Controls.Add(this.label16);
      this.gbxSweepChannelA.Controls.Add(this.nudSweepRepetitionCountChannelA);
      this.gbxSweepChannelA.Controls.Add(this.label17);
      this.gbxSweepChannelA.Controls.Add(this.nudSweepStepCountChannelA);
      this.gbxSweepChannelA.Controls.Add(this.label18);
      this.gbxSweepChannelA.Controls.Add(this.nudSweepStepDeltaChannelA);
      this.gbxSweepChannelA.Controls.Add(this.label19);
      this.gbxSweepChannelA.Controls.Add(this.nudSweepLimitHighChannelA);
      this.gbxSweepChannelA.Controls.Add(this.label20);
      this.gbxSweepChannelA.Controls.Add(this.nudSweepLimitLowChannelA);
      this.gbxSweepChannelA.Location = new System.Drawing.Point(322, 147);
      this.gbxSweepChannelA.Name = "gbxSweepChannelA";
      this.gbxSweepChannelA.Size = new System.Drawing.Size(349, 96);
      this.gbxSweepChannelA.TabIndex = 97;
      this.gbxSweepChannelA.TabStop = false;
      this.gbxSweepChannelA.Text = " Sweep-Parameter ChannelA ";
      // 
      // rbtSweepStateHighLowLowHighChannelA
      // 
      this.rbtSweepStateHighLowLowHighChannelA.AutoSize = true;
      this.rbtSweepStateHighLowLowHighChannelA.BackColor = System.Drawing.SystemColors.Info;
      this.rbtSweepStateHighLowLowHighChannelA.Location = new System.Drawing.Point(132, 20);
      this.rbtSweepStateHighLowLowHighChannelA.Name = "rbtSweepStateHighLowLowHighChannelA";
      this.rbtSweepStateHighLowLowHighChannelA.Size = new System.Drawing.Size(14, 13);
      this.rbtSweepStateHighLowLowHighChannelA.TabIndex = 106;
      this.rbtSweepStateHighLowLowHighChannelA.UseVisualStyleBackColor = false;
      this.rbtSweepStateHighLowLowHighChannelA.CheckedChanged += new System.EventHandler(this.rbtSweepStateHighLowLowHighChannelA_CheckedChanged);
      // 
      // rbtSweepStateHighLowHighLowChannelA
      // 
      this.rbtSweepStateHighLowHighLowChannelA.AutoSize = true;
      this.rbtSweepStateHighLowHighLowChannelA.BackColor = System.Drawing.SystemColors.Info;
      this.rbtSweepStateHighLowHighLowChannelA.Location = new System.Drawing.Point(69, 20);
      this.rbtSweepStateHighLowHighLowChannelA.Name = "rbtSweepStateHighLowHighLowChannelA";
      this.rbtSweepStateHighLowHighLowChannelA.Size = new System.Drawing.Size(14, 13);
      this.rbtSweepStateHighLowHighLowChannelA.TabIndex = 105;
      this.rbtSweepStateHighLowHighLowChannelA.UseVisualStyleBackColor = false;
      this.rbtSweepStateHighLowHighLowChannelA.CheckedChanged += new System.EventHandler(this.rbtSweepStateHighLowHighLowChannelA_CheckedChanged);
      // 
      // rbtSweepStateLowHighHighLowChannelA
      // 
      this.rbtSweepStateLowHighHighLowChannelA.AutoSize = true;
      this.rbtSweepStateLowHighHighLowChannelA.BackColor = System.Drawing.SystemColors.Info;
      this.rbtSweepStateLowHighHighLowChannelA.Location = new System.Drawing.Point(117, 18);
      this.rbtSweepStateLowHighHighLowChannelA.Name = "rbtSweepStateLowHighHighLowChannelA";
      this.rbtSweepStateLowHighHighLowChannelA.Size = new System.Drawing.Size(62, 17);
      this.rbtSweepStateLowHighHighLowChannelA.TabIndex = 104;
      this.rbtSweepStateLowHighHighLowChannelA.Text = "   LHHL";
      this.rbtSweepStateLowHighHighLowChannelA.UseVisualStyleBackColor = false;
      this.rbtSweepStateLowHighHighLowChannelA.CheckedChanged += new System.EventHandler(this.rbtSweepStateLowHighHighLowChannelA_CheckedChanged);
      // 
      // rbtSweepStateLowHighLowHighChannelA
      // 
      this.rbtSweepStateLowHighLowHighChannelA.AutoSize = true;
      this.rbtSweepStateLowHighLowHighChannelA.BackColor = System.Drawing.SystemColors.Info;
      this.rbtSweepStateLowHighLowHighChannelA.Location = new System.Drawing.Point(54, 18);
      this.rbtSweepStateLowHighLowHighChannelA.Name = "rbtSweepStateLowHighLowHighChannelA";
      this.rbtSweepStateLowHighLowHighChannelA.Size = new System.Drawing.Size(62, 17);
      this.rbtSweepStateLowHighLowHighChannelA.TabIndex = 103;
      this.rbtSweepStateLowHighLowHighChannelA.Text = "   LHLH";
      this.rbtSweepStateLowHighLowHighChannelA.UseVisualStyleBackColor = false;
      this.rbtSweepStateLowHighLowHighChannelA.CheckedChanged += new System.EventHandler(this.rbtSweepStateLowHighLowHighChannelA_CheckedChanged);
      // 
      // rbtSweepStateIdleChannelA
      // 
      this.rbtSweepStateIdleChannelA.AutoSize = true;
      this.rbtSweepStateIdleChannelA.BackColor = System.Drawing.SystemColors.Info;
      this.rbtSweepStateIdleChannelA.Checked = true;
      this.rbtSweepStateIdleChannelA.Location = new System.Drawing.Point(12, 18);
      this.rbtSweepStateIdleChannelA.Name = "rbtSweepStateIdleChannelA";
      this.rbtSweepStateIdleChannelA.Size = new System.Drawing.Size(42, 17);
      this.rbtSweepStateIdleChannelA.TabIndex = 102;
      this.rbtSweepStateIdleChannelA.TabStop = true;
      this.rbtSweepStateIdleChannelA.Text = "Idle";
      this.rbtSweepStateIdleChannelA.UseVisualStyleBackColor = false;
      this.rbtSweepStateIdleChannelA.CheckedChanged += new System.EventHandler(this.rbtSweepStateIdleChannelA_CheckedChanged);
      // 
      // label16
      // 
      this.label16.BackColor = System.Drawing.SystemColors.Info;
      this.label16.Location = new System.Drawing.Point(183, 16);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(83, 17);
      this.label16.TabIndex = 101;
      this.label16.Text = "Repetitions [1]";
      this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSweepRepetitionCountChannelA
      // 
      this.nudSweepRepetitionCountChannelA.Location = new System.Drawing.Point(270, 15);
      this.nudSweepRepetitionCountChannelA.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudSweepRepetitionCountChannelA.Name = "nudSweepRepetitionCountChannelA";
      this.nudSweepRepetitionCountChannelA.Size = new System.Drawing.Size(69, 20);
      this.nudSweepRepetitionCountChannelA.TabIndex = 100;
      this.nudSweepRepetitionCountChannelA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSweepRepetitionCountChannelA.ValueChanged += new System.EventHandler(this.nudSweepRepetitionCountChannelA_ValueChanged);
      // 
      // label17
      // 
      this.label17.BackColor = System.Drawing.SystemColors.Info;
      this.label17.Location = new System.Drawing.Point(183, 68);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(83, 17);
      this.label17.TabIndex = 99;
      this.label17.Text = "StepCount [1]";
      this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSweepStepCountChannelA
      // 
      this.nudSweepStepCountChannelA.Location = new System.Drawing.Point(270, 67);
      this.nudSweepStepCountChannelA.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudSweepStepCountChannelA.Name = "nudSweepStepCountChannelA";
      this.nudSweepStepCountChannelA.Size = new System.Drawing.Size(69, 20);
      this.nudSweepStepCountChannelA.TabIndex = 98;
      this.nudSweepStepCountChannelA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSweepStepCountChannelA.ValueChanged += new System.EventHandler(this.nudSweepStepCountChannelA_ValueChanged);
      // 
      // label18
      // 
      this.label18.BackColor = System.Drawing.SystemColors.Info;
      this.label18.Location = new System.Drawing.Point(11, 68);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(83, 17);
      this.label18.TabIndex = 97;
      this.label18.Text = "StepDelta [1]";
      this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSweepStepDeltaChannelA
      // 
      this.nudSweepStepDeltaChannelA.Location = new System.Drawing.Point(98, 67);
      this.nudSweepStepDeltaChannelA.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudSweepStepDeltaChannelA.Name = "nudSweepStepDeltaChannelA";
      this.nudSweepStepDeltaChannelA.Size = new System.Drawing.Size(69, 20);
      this.nudSweepStepDeltaChannelA.TabIndex = 96;
      this.nudSweepStepDeltaChannelA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSweepStepDeltaChannelA.ValueChanged += new System.EventHandler(this.nudSweepStepDeltaChannelA_ValueChanged);
      // 
      // label19
      // 
      this.label19.BackColor = System.Drawing.SystemColors.Info;
      this.label19.Location = new System.Drawing.Point(183, 42);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(85, 17);
      this.label19.TabIndex = 95;
      this.label19.Text = "LimitHigh [10ps]";
      this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSweepLimitHighChannelA
      // 
      this.nudSweepLimitHighChannelA.Location = new System.Drawing.Point(270, 41);
      this.nudSweepLimitHighChannelA.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudSweepLimitHighChannelA.Name = "nudSweepLimitHighChannelA";
      this.nudSweepLimitHighChannelA.Size = new System.Drawing.Size(69, 20);
      this.nudSweepLimitHighChannelA.TabIndex = 94;
      this.nudSweepLimitHighChannelA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSweepLimitHighChannelA.ValueChanged += new System.EventHandler(this.nudSweepLimitHighChannelA_ValueChanged);
      // 
      // label20
      // 
      this.label20.BackColor = System.Drawing.SystemColors.Info;
      this.label20.Location = new System.Drawing.Point(11, 42);
      this.label20.Name = "label20";
      this.label20.Size = new System.Drawing.Size(83, 17);
      this.label20.TabIndex = 93;
      this.label20.Text = "LimitLow [10ps]";
      this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSweepLimitLowChannelA
      // 
      this.nudSweepLimitLowChannelA.Location = new System.Drawing.Point(98, 41);
      this.nudSweepLimitLowChannelA.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudSweepLimitLowChannelA.Name = "nudSweepLimitLowChannelA";
      this.nudSweepLimitLowChannelA.Size = new System.Drawing.Size(69, 20);
      this.nudSweepLimitLowChannelA.TabIndex = 92;
      this.nudSweepLimitLowChannelA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSweepLimitLowChannelA.ValueChanged += new System.EventHandler(this.nudSweepLimitLowChannelA_ValueChanged);
      // 
      // gbxSweepChannelB
      // 
      this.gbxSweepChannelB.Controls.Add(this.rbtSweepStateHighLowLowHighChannelB);
      this.gbxSweepChannelB.Controls.Add(this.rbtSweepStateHighLowHighLowChannelB);
      this.gbxSweepChannelB.Controls.Add(this.rbtSweepStateLowHighHighLowChannelB);
      this.gbxSweepChannelB.Controls.Add(this.rbtSweepStateLowHighLowHighChannelB);
      this.gbxSweepChannelB.Controls.Add(this.rbtSweepStateIdleChannelB);
      this.gbxSweepChannelB.Controls.Add(this.label11);
      this.gbxSweepChannelB.Controls.Add(this.nudSweepRepetitionCountChannelB);
      this.gbxSweepChannelB.Controls.Add(this.label12);
      this.gbxSweepChannelB.Controls.Add(this.nudSweepStepCountChannelB);
      this.gbxSweepChannelB.Controls.Add(this.label13);
      this.gbxSweepChannelB.Controls.Add(this.nudSweepStepDeltaChannelB);
      this.gbxSweepChannelB.Controls.Add(this.label14);
      this.gbxSweepChannelB.Controls.Add(this.nudSweepLimitHighChannelB);
      this.gbxSweepChannelB.Controls.Add(this.label15);
      this.gbxSweepChannelB.Controls.Add(this.nudSweepLimitLowChannelB);
      this.gbxSweepChannelB.Location = new System.Drawing.Point(322, 254);
      this.gbxSweepChannelB.Name = "gbxSweepChannelB";
      this.gbxSweepChannelB.Size = new System.Drawing.Size(349, 96);
      this.gbxSweepChannelB.TabIndex = 96;
      this.gbxSweepChannelB.TabStop = false;
      this.gbxSweepChannelB.Text = " Sweep-Parameter ChannelB ";
      // 
      // rbtSweepStateHighLowLowHighChannelB
      // 
      this.rbtSweepStateHighLowLowHighChannelB.AutoSize = true;
      this.rbtSweepStateHighLowLowHighChannelB.BackColor = System.Drawing.SystemColors.Info;
      this.rbtSweepStateHighLowLowHighChannelB.Location = new System.Drawing.Point(132, 18);
      this.rbtSweepStateHighLowLowHighChannelB.Name = "rbtSweepStateHighLowLowHighChannelB";
      this.rbtSweepStateHighLowLowHighChannelB.Size = new System.Drawing.Size(14, 13);
      this.rbtSweepStateHighLowLowHighChannelB.TabIndex = 109;
      this.rbtSweepStateHighLowLowHighChannelB.UseVisualStyleBackColor = false;
      this.rbtSweepStateHighLowLowHighChannelB.CheckedChanged += new System.EventHandler(this.rbtSweepStateHighLowLowHighChannelB_CheckedChanged);
      // 
      // rbtSweepStateHighLowHighLowChannelB
      // 
      this.rbtSweepStateHighLowHighLowChannelB.AutoSize = true;
      this.rbtSweepStateHighLowHighLowChannelB.BackColor = System.Drawing.SystemColors.Info;
      this.rbtSweepStateHighLowHighLowChannelB.Location = new System.Drawing.Point(69, 18);
      this.rbtSweepStateHighLowHighLowChannelB.Name = "rbtSweepStateHighLowHighLowChannelB";
      this.rbtSweepStateHighLowHighLowChannelB.Size = new System.Drawing.Size(14, 13);
      this.rbtSweepStateHighLowHighLowChannelB.TabIndex = 108;
      this.rbtSweepStateHighLowHighLowChannelB.UseVisualStyleBackColor = false;
      this.rbtSweepStateHighLowHighLowChannelB.CheckedChanged += new System.EventHandler(this.rbtSweepStateHighLowHighLowChannelB_CheckedChanged);
      // 
      // rbtSweepStateLowHighHighLowChannelB
      // 
      this.rbtSweepStateLowHighHighLowChannelB.AutoSize = true;
      this.rbtSweepStateLowHighHighLowChannelB.BackColor = System.Drawing.SystemColors.Info;
      this.rbtSweepStateLowHighHighLowChannelB.Location = new System.Drawing.Point(117, 16);
      this.rbtSweepStateLowHighHighLowChannelB.Name = "rbtSweepStateLowHighHighLowChannelB";
      this.rbtSweepStateLowHighHighLowChannelB.Size = new System.Drawing.Size(62, 17);
      this.rbtSweepStateLowHighHighLowChannelB.TabIndex = 107;
      this.rbtSweepStateLowHighHighLowChannelB.Text = "   LHHL";
      this.rbtSweepStateLowHighHighLowChannelB.UseVisualStyleBackColor = false;
      this.rbtSweepStateLowHighHighLowChannelB.CheckedChanged += new System.EventHandler(this.rbtSweepStateLowHighHighLowChannelB_CheckedChanged);
      // 
      // rbtSweepStateLowHighLowHighChannelB
      // 
      this.rbtSweepStateLowHighLowHighChannelB.AutoSize = true;
      this.rbtSweepStateLowHighLowHighChannelB.BackColor = System.Drawing.SystemColors.Info;
      this.rbtSweepStateLowHighLowHighChannelB.Location = new System.Drawing.Point(54, 16);
      this.rbtSweepStateLowHighLowHighChannelB.Name = "rbtSweepStateLowHighLowHighChannelB";
      this.rbtSweepStateLowHighLowHighChannelB.Size = new System.Drawing.Size(62, 17);
      this.rbtSweepStateLowHighLowHighChannelB.TabIndex = 106;
      this.rbtSweepStateLowHighLowHighChannelB.Text = "   LHLH";
      this.rbtSweepStateLowHighLowHighChannelB.UseVisualStyleBackColor = false;
      this.rbtSweepStateLowHighLowHighChannelB.CheckedChanged += new System.EventHandler(this.rbtSweepStateLowHighLowHighChannelB_CheckedChanged);
      // 
      // rbtSweepStateIdleChannelB
      // 
      this.rbtSweepStateIdleChannelB.AutoSize = true;
      this.rbtSweepStateIdleChannelB.BackColor = System.Drawing.SystemColors.Info;
      this.rbtSweepStateIdleChannelB.Checked = true;
      this.rbtSweepStateIdleChannelB.Location = new System.Drawing.Point(10, 16);
      this.rbtSweepStateIdleChannelB.Name = "rbtSweepStateIdleChannelB";
      this.rbtSweepStateIdleChannelB.Size = new System.Drawing.Size(42, 17);
      this.rbtSweepStateIdleChannelB.TabIndex = 105;
      this.rbtSweepStateIdleChannelB.TabStop = true;
      this.rbtSweepStateIdleChannelB.Text = "Idle";
      this.rbtSweepStateIdleChannelB.UseVisualStyleBackColor = false;
      this.rbtSweepStateIdleChannelB.CheckedChanged += new System.EventHandler(this.rbtSweepStateIdleChannelB_CheckedChanged);
      // 
      // label11
      // 
      this.label11.BackColor = System.Drawing.SystemColors.Info;
      this.label11.Location = new System.Drawing.Point(183, 16);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(83, 17);
      this.label11.TabIndex = 101;
      this.label11.Text = "Repetitions [1]";
      this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSweepRepetitionCountChannelB
      // 
      this.nudSweepRepetitionCountChannelB.Location = new System.Drawing.Point(270, 15);
      this.nudSweepRepetitionCountChannelB.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudSweepRepetitionCountChannelB.Name = "nudSweepRepetitionCountChannelB";
      this.nudSweepRepetitionCountChannelB.Size = new System.Drawing.Size(69, 20);
      this.nudSweepRepetitionCountChannelB.TabIndex = 100;
      this.nudSweepRepetitionCountChannelB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSweepRepetitionCountChannelB.ValueChanged += new System.EventHandler(this.nudSweepRepetitionCountChannelB_ValueChanged);
      // 
      // label12
      // 
      this.label12.BackColor = System.Drawing.SystemColors.Info;
      this.label12.Location = new System.Drawing.Point(183, 68);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(83, 17);
      this.label12.TabIndex = 99;
      this.label12.Text = "StepCount [1]";
      this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSweepStepCountChannelB
      // 
      this.nudSweepStepCountChannelB.Location = new System.Drawing.Point(270, 67);
      this.nudSweepStepCountChannelB.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudSweepStepCountChannelB.Name = "nudSweepStepCountChannelB";
      this.nudSweepStepCountChannelB.Size = new System.Drawing.Size(69, 20);
      this.nudSweepStepCountChannelB.TabIndex = 98;
      this.nudSweepStepCountChannelB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSweepStepCountChannelB.ValueChanged += new System.EventHandler(this.nudSweepStepCountChannelB_ValueChanged);
      // 
      // label13
      // 
      this.label13.BackColor = System.Drawing.SystemColors.Info;
      this.label13.Location = new System.Drawing.Point(11, 68);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(83, 17);
      this.label13.TabIndex = 97;
      this.label13.Text = "StepDelta [1]";
      this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSweepStepDeltaChannelB
      // 
      this.nudSweepStepDeltaChannelB.Location = new System.Drawing.Point(98, 67);
      this.nudSweepStepDeltaChannelB.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudSweepStepDeltaChannelB.Name = "nudSweepStepDeltaChannelB";
      this.nudSweepStepDeltaChannelB.Size = new System.Drawing.Size(69, 20);
      this.nudSweepStepDeltaChannelB.TabIndex = 96;
      this.nudSweepStepDeltaChannelB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSweepStepDeltaChannelB.ValueChanged += new System.EventHandler(this.nudSweepStepDeltaChannelB_ValueChanged);
      // 
      // label14
      // 
      this.label14.BackColor = System.Drawing.SystemColors.Info;
      this.label14.Location = new System.Drawing.Point(183, 42);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(85, 17);
      this.label14.TabIndex = 95;
      this.label14.Text = "LimitHigh [10ps]";
      this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSweepLimitHighChannelB
      // 
      this.nudSweepLimitHighChannelB.Location = new System.Drawing.Point(270, 41);
      this.nudSweepLimitHighChannelB.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudSweepLimitHighChannelB.Name = "nudSweepLimitHighChannelB";
      this.nudSweepLimitHighChannelB.Size = new System.Drawing.Size(69, 20);
      this.nudSweepLimitHighChannelB.TabIndex = 94;
      this.nudSweepLimitHighChannelB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSweepLimitHighChannelB.ValueChanged += new System.EventHandler(this.nudSweepLimitHighChannelB_ValueChanged);
      // 
      // label15
      // 
      this.label15.BackColor = System.Drawing.SystemColors.Info;
      this.label15.Location = new System.Drawing.Point(11, 42);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(83, 17);
      this.label15.TabIndex = 93;
      this.label15.Text = "LimitLow [10ps]";
      this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudSweepLimitLowChannelB
      // 
      this.nudSweepLimitLowChannelB.Location = new System.Drawing.Point(98, 41);
      this.nudSweepLimitLowChannelB.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudSweepLimitLowChannelB.Name = "nudSweepLimitLowChannelB";
      this.nudSweepLimitLowChannelB.Size = new System.Drawing.Size(69, 20);
      this.nudSweepLimitLowChannelB.TabIndex = 92;
      this.nudSweepLimitLowChannelB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSweepLimitLowChannelB.ValueChanged += new System.EventHandler(this.nudSweepLimitLowChannelB_ValueChanged);
      // 
      // gbxChannelDataB
      // 
      this.gbxChannelDataB.Controls.Add(this.hsbDelayChannelB);
      this.gbxChannelDataB.Controls.Add(this.cbxDelayChannelBUnit);
      this.gbxChannelDataB.Controls.Add(this.lblDelayChannelB);
      this.gbxChannelDataB.Controls.Add(this.nudDelayChannelB);
      this.gbxChannelDataB.Controls.Add(this.label2);
      this.gbxChannelDataB.Location = new System.Drawing.Point(13, 254);
      this.gbxChannelDataB.Name = "gbxChannelDataB";
      this.gbxChannelDataB.Size = new System.Drawing.Size(303, 96);
      this.gbxChannelDataB.TabIndex = 95;
      this.gbxChannelDataB.TabStop = false;
      this.gbxChannelDataB.Text = " Channel B ";
      this.ttpHint.SetToolTip(this.gbxChannelDataB, "Get / Set Parameter of DigitalAnalogConverter DAC");
      // 
      // hsbDelayChannelB
      // 
      this.hsbDelayChannelB.Location = new System.Drawing.Point(12, 68);
      this.hsbDelayChannelB.Maximum = 1023;
      this.hsbDelayChannelB.Name = "hsbDelayChannelB";
      this.hsbDelayChannelB.Size = new System.Drawing.Size(269, 17);
      this.hsbDelayChannelB.TabIndex = 78;
      this.hsbDelayChannelB.ValueChanged += new System.EventHandler(this.hsbDelayChannelB_ValueChanged);
      // 
      // cbxDelayChannelBUnit
      // 
      this.cbxDelayChannelBUnit.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxDelayChannelBUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbxDelayChannelBUnit.Location = new System.Drawing.Point(245, 32);
      this.cbxDelayChannelBUnit.Name = "cbxDelayChannelBUnit";
      this.cbxDelayChannelBUnit.Size = new System.Drawing.Size(37, 30);
      this.cbxDelayChannelBUnit.TabIndex = 77;
      this.cbxDelayChannelBUnit.Text = "ps";
      this.cbxDelayChannelBUnit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxDelayChannelBUnit.UseVisualStyleBackColor = true;
      this.cbxDelayChannelBUnit.CheckedChanged += new System.EventHandler(this.cbxDelayChannelBUnit_CheckedChanged);
      // 
      // lblDelayChannelB
      // 
      this.lblDelayChannelB.BackColor = System.Drawing.SystemColors.Info;
      this.lblDelayChannelB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDelayChannelB.Location = new System.Drawing.Point(154, 36);
      this.lblDelayChannelB.Name = "lblDelayChannelB";
      this.lblDelayChannelB.Size = new System.Drawing.Size(85, 22);
      this.lblDelayChannelB.TabIndex = 76;
      this.lblDelayChannelB.Text = "0";
      this.lblDelayChannelB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudDelayChannelB
      // 
      this.nudDelayChannelB.Location = new System.Drawing.Point(75, 37);
      this.nudDelayChannelB.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudDelayChannelB.Name = "nudDelayChannelB";
      this.nudDelayChannelB.Size = new System.Drawing.Size(69, 20);
      this.nudDelayChannelB.TabIndex = 72;
      this.nudDelayChannelB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelayChannelB.ValueChanged += new System.EventHandler(this.nudDelayChannelB_ValueChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.BackColor = System.Drawing.SystemColors.Info;
      this.label2.Location = new System.Drawing.Point(9, 39);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(61, 13);
      this.label2.TabIndex = 60;
      this.label2.Text = "Delay [dec]";
      // 
      // FUCVersion
      // 
      this.FUCVersion.HardwareVersion = "<hardwareversion>";
      this.FUCVersion.Location = new System.Drawing.Point(10, 57);
      this.FUCVersion.Name = "FUCVersion";
      this.FUCVersion.Size = new System.Drawing.Size(214, 84);
      this.FUCVersion.SoftwareVersion = "<softwareversion>";
      this.FUCVersion.TabIndex = 94;
      // 
      // gbxLed
      // 
      this.gbxLed.Controls.Add(this.FUCLedRed3Busy);
      this.gbxLed.Controls.Add(this.FUCLedRed7Error);
      this.gbxLed.Location = new System.Drawing.Point(231, 60);
      this.gbxLed.Name = "gbxLed";
      this.gbxLed.Size = new System.Drawing.Size(120, 81);
      this.gbxLed.TabIndex = 87;
      this.gbxLed.TabStop = false;
      this.gbxLed.Text = " LED  ";
      this.ttpHint.SetToolTip(this.gbxLed, "Get / Set Parameter of LightEmittingDiodes LED");
      // 
      // FUCLedRed3Busy
      // 
      this.FUCLedRed3Busy.Active = false;
      this.FUCLedRed3Busy.BackColor = System.Drawing.SystemColors.Info;
      this.FUCLedRed3Busy.ColorAreaActive = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.FUCLedRed3Busy.ColorAreaPassive = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.FUCLedRed3Busy.ColorBorderActive = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
      this.FUCLedRed3Busy.ColorBorderPassive = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
      this.FUCLedRed3Busy.Location = new System.Drawing.Point(65, 17);
      this.FUCLedRed3Busy.Name = "FUCLedRed3Busy";
      this.FUCLedRed3Busy.Size = new System.Drawing.Size(44, 54);
      this.FUCLedRed3Busy.TabIndex = 73;
      this.FUCLedRed3Busy.Title = "Busy";
      // 
      // FUCLedRed7Error
      // 
      this.FUCLedRed7Error.Active = false;
      this.FUCLedRed7Error.BackColor = System.Drawing.SystemColors.Info;
      this.FUCLedRed7Error.ColorAreaActive = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.FUCLedRed7Error.ColorAreaPassive = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
      this.FUCLedRed7Error.ColorBorderActive = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
      this.FUCLedRed7Error.ColorBorderPassive = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
      this.FUCLedRed7Error.Location = new System.Drawing.Point(10, 17);
      this.FUCLedRed7Error.Name = "FUCLedRed7Error";
      this.FUCLedRed7Error.Size = new System.Drawing.Size(44, 54);
      this.FUCLedRed7Error.TabIndex = 72;
      this.FUCLedRed7Error.Title = "Error";
      // 
      // gbxChannelDataA
      // 
      this.gbxChannelDataA.Controls.Add(this.hsbDelayChannelA);
      this.gbxChannelDataA.Controls.Add(this.cbxDelayChannelAUnit);
      this.gbxChannelDataA.Controls.Add(this.lblDelayChannelA);
      this.gbxChannelDataA.Controls.Add(this.nudDelayChannelA);
      this.gbxChannelDataA.Controls.Add(this.label3);
      this.gbxChannelDataA.Location = new System.Drawing.Point(13, 147);
      this.gbxChannelDataA.Name = "gbxChannelDataA";
      this.gbxChannelDataA.Size = new System.Drawing.Size(303, 96);
      this.gbxChannelDataA.TabIndex = 86;
      this.gbxChannelDataA.TabStop = false;
      this.gbxChannelDataA.Text = " Channel A ";
      this.ttpHint.SetToolTip(this.gbxChannelDataA, "Get / Set Parameter of DigitalAnalogConverter DAC");
      // 
      // hsbDelayChannelA
      // 
      this.hsbDelayChannelA.Location = new System.Drawing.Point(12, 68);
      this.hsbDelayChannelA.Maximum = 1023;
      this.hsbDelayChannelA.Name = "hsbDelayChannelA";
      this.hsbDelayChannelA.Size = new System.Drawing.Size(270, 17);
      this.hsbDelayChannelA.TabIndex = 78;
      this.hsbDelayChannelA.ValueChanged += new System.EventHandler(this.hsbDelayChannelA_ValueChanged);
      // 
      // cbxDelayChannelAUnit
      // 
      this.cbxDelayChannelAUnit.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxDelayChannelAUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbxDelayChannelAUnit.Location = new System.Drawing.Point(244, 32);
      this.cbxDelayChannelAUnit.Name = "cbxDelayChannelAUnit";
      this.cbxDelayChannelAUnit.Size = new System.Drawing.Size(37, 30);
      this.cbxDelayChannelAUnit.TabIndex = 77;
      this.cbxDelayChannelAUnit.Text = "ps";
      this.cbxDelayChannelAUnit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxDelayChannelAUnit.UseVisualStyleBackColor = true;
      this.cbxDelayChannelAUnit.CheckedChanged += new System.EventHandler(this.cbxDelayChannelAUnit_CheckedChanged);
      // 
      // lblDelayChannelA
      // 
      this.lblDelayChannelA.BackColor = System.Drawing.SystemColors.Info;
      this.lblDelayChannelA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDelayChannelA.Location = new System.Drawing.Point(153, 36);
      this.lblDelayChannelA.Name = "lblDelayChannelA";
      this.lblDelayChannelA.Size = new System.Drawing.Size(85, 22);
      this.lblDelayChannelA.TabIndex = 76;
      this.lblDelayChannelA.Text = "0";
      this.lblDelayChannelA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudDelayChannelA
      // 
      this.nudDelayChannelA.Location = new System.Drawing.Point(75, 37);
      this.nudDelayChannelA.Maximum = new decimal(new int[] {
            1023,
            0,
            0,
            0});
      this.nudDelayChannelA.Name = "nudDelayChannelA";
      this.nudDelayChannelA.Size = new System.Drawing.Size(69, 20);
      this.nudDelayChannelA.TabIndex = 72;
      this.nudDelayChannelA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelayChannelA.ValueChanged += new System.EventHandler(this.nudDelayChannelA_ValueChanged);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.BackColor = System.Drawing.SystemColors.Info;
      this.label3.Location = new System.Drawing.Point(9, 39);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(61, 13);
      this.label3.TabIndex = 60;
      this.label3.Text = "Delay [dec]";
      // 
      // gbxCOMSelection
      // 
      this.gbxCOMSelection.Controls.Add(this.label4);
      this.gbxCOMSelection.Controls.Add(this.lblDeviceType);
      this.gbxCOMSelection.Controls.Add(this.btnOpenClosePort);
      this.gbxCOMSelection.Controls.Add(this.label7);
      this.gbxCOMSelection.Controls.Add(this.cbxPortsPossible);
      this.gbxCOMSelection.Controls.Add(this.label5);
      this.gbxCOMSelection.Controls.Add(this.cbxPortsSelectable);
      this.gbxCOMSelection.Controls.Add(this.cbxPortsInstalled);
      this.gbxCOMSelection.Controls.Add(this.lblSelected);
      this.gbxCOMSelection.Location = new System.Drawing.Point(10, 8);
      this.gbxCOMSelection.Name = "gbxCOMSelection";
      this.gbxCOMSelection.Size = new System.Drawing.Size(661, 48);
      this.gbxCOMSelection.TabIndex = 83;
      this.gbxCOMSelection.TabStop = false;
      this.gbxCOMSelection.Text = " RS232 Port ";
      this.ttpHint.SetToolTip(this.gbxCOMSelection, "Get / Set Parameter of SerialPort RS232");
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(9, 20);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(35, 13);
      this.label4.TabIndex = 60;
      this.label4.Text = "Name";
      // 
      // lblDeviceType
      // 
      this.lblDeviceType.BackColor = System.Drawing.SystemColors.Info;
      this.lblDeviceType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblDeviceType.Location = new System.Drawing.Point(44, 16);
      this.lblDeviceType.Name = "lblDeviceType";
      this.lblDeviceType.Size = new System.Drawing.Size(95, 21);
      this.lblDeviceType.TabIndex = 59;
      this.lblDeviceType.Text = "<portname>";
      this.lblDeviceType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnOpenClosePort
      // 
      this.btnOpenClosePort.Location = new System.Drawing.Point(555, 15);
      this.btnOpenClosePort.Name = "btnOpenClosePort";
      this.btnOpenClosePort.Size = new System.Drawing.Size(96, 23);
      this.btnOpenClosePort.TabIndex = 56;
      this.btnOpenClosePort.Text = "Open";
      this.btnOpenClosePort.UseVisualStyleBackColor = true;
      this.btnOpenClosePort.Click += new System.EventHandler(this.btnOpenClosePort_Click);
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(137, 20);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(46, 13);
      this.label7.TabIndex = 55;
      this.label7.Text = "Possible";
      // 
      // cbxPortsPossible
      // 
      this.cbxPortsPossible.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPortsPossible.FormattingEnabled = true;
      this.cbxPortsPossible.Location = new System.Drawing.Point(186, 17);
      this.cbxPortsPossible.Name = "cbxPortsPossible";
      this.cbxPortsPossible.Size = new System.Drawing.Size(80, 21);
      this.cbxPortsPossible.TabIndex = 54;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(277, 20);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(46, 13);
      this.label5.TabIndex = 53;
      this.label5.Text = "Installed";
      // 
      // cbxPortsSelectable
      // 
      this.cbxPortsSelectable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPortsSelectable.FormattingEnabled = true;
      this.cbxPortsSelectable.Location = new System.Drawing.Point(469, 17);
      this.cbxPortsSelectable.Name = "cbxPortsSelectable";
      this.cbxPortsSelectable.Size = new System.Drawing.Size(80, 21);
      this.cbxPortsSelectable.TabIndex = 50;
      // 
      // cbxPortsInstalled
      // 
      this.cbxPortsInstalled.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPortsInstalled.FormattingEnabled = true;
      this.cbxPortsInstalled.Location = new System.Drawing.Point(326, 17);
      this.cbxPortsInstalled.Name = "cbxPortsInstalled";
      this.cbxPortsInstalled.Size = new System.Drawing.Size(80, 21);
      this.cbxPortsInstalled.TabIndex = 50;
      // 
      // lblSelected
      // 
      this.lblSelected.AutoSize = true;
      this.lblSelected.Location = new System.Drawing.Point(417, 21);
      this.lblSelected.Name = "lblSelected";
      this.lblSelected.Size = new System.Drawing.Size(49, 13);
      this.lblSelected.TabIndex = 52;
      this.lblSelected.Text = "Selected";
      // 
      // tmrRefreshDelayChannelA
      // 
      this.tmrRefreshDelayChannelA.Interval = 200;
      this.tmrRefreshDelayChannelA.Tick += new System.EventHandler(this.tmrRefreshDelayChannelA_Tick);
      // 
      // tmrRefreshDelayChannelB
      // 
      this.tmrRefreshDelayChannelB.Interval = 200;
      this.tmrRefreshDelayChannelB.Tick += new System.EventHandler(this.tmrRefreshDelayChannelB_Tick);
      // 
      // tmrRefreshSweepLimitLowChannelA
      // 
      this.tmrRefreshSweepLimitLowChannelA.Interval = 200;
      this.tmrRefreshSweepLimitLowChannelA.Tick += new System.EventHandler(this.tmrRefreshSweepLimitLowChannelA_Tick);
      // 
      // tmrRefreshSweepLimitLowChannelB
      // 
      this.tmrRefreshSweepLimitLowChannelB.Interval = 200;
      this.tmrRefreshSweepLimitLowChannelB.Tick += new System.EventHandler(this.tmrRefreshSweepLimitLowChannelB_Tick);
      // 
      // tmrRefreshSweepLimitHighChannelA
      // 
      this.tmrRefreshSweepLimitHighChannelA.Interval = 200;
      this.tmrRefreshSweepLimitHighChannelA.Tick += new System.EventHandler(this.tmrRefreshSweepLimitHighChannelA_Tick);
      // 
      // tmrRefreshSweepRepetitionCountChannelA
      // 
      this.tmrRefreshSweepRepetitionCountChannelA.Interval = 200;
      this.tmrRefreshSweepRepetitionCountChannelA.Tick += new System.EventHandler(this.tmrRefreshSweepRepetitionCountChannelA_Tick);
      // 
      // tmrRefreshSweepStepCountChannelA
      // 
      this.tmrRefreshSweepStepCountChannelA.Interval = 200;
      this.tmrRefreshSweepStepCountChannelA.Tick += new System.EventHandler(this.tmrRefreshSweepStepCountChannelA_Tick);
      // 
      // tmrRefreshSweepStepDeltaChannelA
      // 
      this.tmrRefreshSweepStepDeltaChannelA.Interval = 200;
      this.tmrRefreshSweepStepDeltaChannelA.Tick += new System.EventHandler(this.tmrRefreshSweepStepDeltaChannelA_Tick);
      // 
      // tmrRefreshSweepLimitHighChannelB
      // 
      this.tmrRefreshSweepLimitHighChannelB.Interval = 200;
      this.tmrRefreshSweepLimitHighChannelB.Tick += new System.EventHandler(this.tmrRefreshSweepLimitHighChannelB_Tick);
      // 
      // tmrRefreshSweepStepDeltaChannelB
      // 
      this.tmrRefreshSweepStepDeltaChannelB.Interval = 200;
      this.tmrRefreshSweepStepDeltaChannelB.Tick += new System.EventHandler(this.tmrRefreshSweepStepDeltaChannelB_Tick);
      // 
      // tmrRefreshSweepRepetitionCountChannelB
      // 
      this.tmrRefreshSweepRepetitionCountChannelB.Interval = 200;
      this.tmrRefreshSweepRepetitionCountChannelB.Tick += new System.EventHandler(this.tmrRefreshSweepRepetitionCountChannelB_Tick);
      // 
      // tmrRefreshSweepStepCountChannelB
      // 
      this.tmrRefreshSweepStepCountChannelB.Interval = 200;
      this.tmrRefreshSweepStepCountChannelB.Tick += new System.EventHandler(this.tmrRefreshSweepStepCountChannelB_Tick);
      // 
      // FUCTerminal
      // 
      this.FUCTerminal.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCTerminal.Location = new System.Drawing.Point(0, 364);
      this.FUCTerminal.Name = "FUCTerminal";
      this.FUCTerminal.Size = new System.Drawing.Size(694, 326);
      this.FUCTerminal.TabIndex = 85;
      // 
      // CUCFastDelayLineBase
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCTerminal);
      this.Controls.Add(this.pnlInput);
      this.Name = "CUCFastDelayLineBase";
      this.Size = new System.Drawing.Size(694, 690);
      this.pnlInput.ResumeLayout(false);
      this.gbxSweepChannelA.ResumeLayout(false);
      this.gbxSweepChannelA.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepRepetitionCountChannelA)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepStepCountChannelA)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepStepDeltaChannelA)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepLimitHighChannelA)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepLimitLowChannelA)).EndInit();
      this.gbxSweepChannelB.ResumeLayout(false);
      this.gbxSweepChannelB.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepRepetitionCountChannelB)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepStepCountChannelB)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepStepDeltaChannelB)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepLimitHighChannelB)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSweepLimitLowChannelB)).EndInit();
      this.gbxChannelDataB.ResumeLayout(false);
      this.gbxChannelDataB.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayChannelB)).EndInit();
      this.gbxLed.ResumeLayout(false);
      this.gbxChannelDataA.ResumeLayout(false);
      this.gbxChannelDataA.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayChannelA)).EndInit();
      this.gbxCOMSelection.ResumeLayout(false);
      this.gbxCOMSelection.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlInput;
    private System.Windows.Forms.GroupBox gbxLed;
    private System.Windows.Forms.GroupBox gbxChannelDataA;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox gbxCOMSelection;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label lblDeviceType;
    private System.Windows.Forms.Button btnOpenClosePort;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.ComboBox cbxPortsPossible;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.ComboBox cbxPortsSelectable;
    private System.Windows.Forms.ComboBox cbxPortsInstalled;
    private System.Windows.Forms.Label lblSelected;
    private UCTerminal.CUCTerminal FUCTerminal;
    private UCLed.CUCLedRed FUCLedRed7Error;
    private System.Windows.Forms.NumericUpDown nudDelayChannelA;
    private System.Windows.Forms.ToolTip ttpHint;
    private System.Windows.Forms.Label lblDelayChannelA;
    private UCLed.CUCLedRed FUCLedRed3Busy;
    private CUCVersion FUCVersion;
    private System.Windows.Forms.CheckBox cbxDelayChannelAUnit;
    private System.Windows.Forms.HScrollBar hsbDelayChannelA;
    private System.Windows.Forms.Timer tmrRefreshDelayChannelA;
    private System.Windows.Forms.GroupBox gbxChannelDataB;
    private System.Windows.Forms.HScrollBar hsbDelayChannelB;
    private System.Windows.Forms.CheckBox cbxDelayChannelBUnit;
    private System.Windows.Forms.Label lblDelayChannelB;
    private System.Windows.Forms.NumericUpDown nudDelayChannelB;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Timer tmrRefreshDelayChannelB;
    private System.Windows.Forms.Timer tmrRefreshSweepLimitLowChannelA;
    private System.Windows.Forms.Timer tmrRefreshSweepLimitLowChannelB;
    private System.Windows.Forms.Timer tmrRefreshSweepLimitHighChannelA;
    private System.Windows.Forms.Timer tmrRefreshSweepRepetitionCountChannelA;
    private System.Windows.Forms.Timer tmrRefreshSweepStepCountChannelA;
    private System.Windows.Forms.Timer tmrRefreshSweepStepDeltaChannelA;
    private System.Windows.Forms.GroupBox gbxSweepChannelA;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.NumericUpDown nudSweepRepetitionCountChannelA;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.NumericUpDown nudSweepStepCountChannelA;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.NumericUpDown nudSweepStepDeltaChannelA;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.NumericUpDown nudSweepLimitHighChannelA;
    private System.Windows.Forms.Label label20;
    private System.Windows.Forms.NumericUpDown nudSweepLimitLowChannelA;
    private System.Windows.Forms.GroupBox gbxSweepChannelB;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.NumericUpDown nudSweepRepetitionCountChannelB;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.NumericUpDown nudSweepStepCountChannelB;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.NumericUpDown nudSweepStepDeltaChannelB;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.NumericUpDown nudSweepLimitHighChannelB;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.NumericUpDown nudSweepLimitLowChannelB;
    private System.Windows.Forms.Timer tmrRefreshSweepLimitHighChannelB;
    private System.Windows.Forms.Timer tmrRefreshSweepStepDeltaChannelB;
    private System.Windows.Forms.Timer tmrRefreshSweepRepetitionCountChannelB;
    private System.Windows.Forms.Timer tmrRefreshSweepStepCountChannelB;
    private System.Windows.Forms.RadioButton rbtSweepStateLowHighHighLowChannelA;
    private System.Windows.Forms.RadioButton rbtSweepStateLowHighLowHighChannelA;
    private System.Windows.Forms.RadioButton rbtSweepStateIdleChannelA;
    private System.Windows.Forms.RadioButton rbtSweepStateLowHighHighLowChannelB;
    private System.Windows.Forms.RadioButton rbtSweepStateLowHighLowHighChannelB;
    private System.Windows.Forms.RadioButton rbtSweepStateIdleChannelB;
    private System.Windows.Forms.RadioButton rbtSweepStateHighLowLowHighChannelA;
    private System.Windows.Forms.RadioButton rbtSweepStateHighLowHighLowChannelA;
    private System.Windows.Forms.RadioButton rbtSweepStateHighLowLowHighChannelB;
    private System.Windows.Forms.RadioButton rbtSweepStateHighLowHighLowChannelB;

  }
}
