﻿namespace UCLevel
{
  partial class CUCLevel
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.nudLevel = new System.Windows.Forms.NumericUpDown();
      this.lblTitle = new System.Windows.Forms.Label();
      this.hsbLevel = new System.Windows.Forms.HScrollBar();
      ((System.ComponentModel.ISupportInitialize)(this.nudLevel)).BeginInit();
      this.SuspendLayout();
      // 
      // nudLevel
      // 
      this.nudLevel.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudLevel.Location = new System.Drawing.Point(76, 0);
      this.nudLevel.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudLevel.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
      this.nudLevel.Name = "nudLevel";
      this.nudLevel.Size = new System.Drawing.Size(54, 20);
      this.nudLevel.TabIndex = 3;
      this.nudLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudLevel.ValueChanged += new System.EventHandler(this.nudLevel_ValueChanged);
      // 
      // lblTitle
      // 
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(76, 20);
      this.lblTitle.TabIndex = 2;
      this.lblTitle.Text = "<title>";
      this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // hsbLevel
      // 
      this.hsbLevel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.hsbLevel.Location = new System.Drawing.Point(130, 0);
      this.hsbLevel.Minimum = -100;
      this.hsbLevel.Name = "hsbLevel";
      this.hsbLevel.Size = new System.Drawing.Size(228, 20);
      this.hsbLevel.TabIndex = 4;
      this.hsbLevel.ValueChanged += new System.EventHandler(this.hsbLevel_ValueChanged);
      // 
      // CUCLevel
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.hsbLevel);
      this.Controls.Add(this.nudLevel);
      this.Controls.Add(this.lblTitle);
      this.Name = "CUCLevel";
      this.Size = new System.Drawing.Size(358, 20);
      ((System.ComponentModel.ISupportInitialize)(this.nudLevel)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.NumericUpDown nudLevel;
    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.HScrollBar hsbLevel;
  }
}
