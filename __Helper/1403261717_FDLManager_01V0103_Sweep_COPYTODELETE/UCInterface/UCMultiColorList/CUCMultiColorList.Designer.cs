﻿namespace UCMultiColorList
{
  partial class CUCMultiColorList
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.lbxView = new System.Windows.Forms.ListBox();
      this.cmsMultiColorList = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mitClearAllLines = new System.Windows.Forms.ToolStripMenuItem();
      this.addSpacelineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.cmsMultiColorList.SuspendLayout();
      this.SuspendLayout();
      // 
      // lbxView
      // 
      this.lbxView.ContextMenuStrip = this.cmsMultiColorList;
      this.lbxView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbxView.Font = new System.Drawing.Font("Courier New", 9F);
      this.lbxView.FormattingEnabled = true;
      this.lbxView.IntegralHeight = false;
      this.lbxView.ItemHeight = 15;
      this.lbxView.Location = new System.Drawing.Point(0, 0);
      this.lbxView.Name = "lbxView";
      this.lbxView.Size = new System.Drawing.Size(232, 232);
      this.lbxView.TabIndex = 0;
      // 
      // cmsMultiColorList
      // 
      this.cmsMultiColorList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitClearAllLines,
            this.addSpacelineToolStripMenuItem});
      this.cmsMultiColorList.Name = "cmsMultiColorList";
      this.cmsMultiColorList.Size = new System.Drawing.Size(153, 48);
      // 
      // mitClearAllLines
      // 
      this.mitClearAllLines.Name = "mitClearAllLines";
      this.mitClearAllLines.Size = new System.Drawing.Size(152, 22);
      this.mitClearAllLines.Text = "Clear all Lines";
      this.mitClearAllLines.Click += new System.EventHandler(this.mitClearAllLines_Click);
      // 
      // addSpacelineToolStripMenuItem
      // 
      this.addSpacelineToolStripMenuItem.Name = "addSpacelineToolStripMenuItem";
      this.addSpacelineToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.addSpacelineToolStripMenuItem.Text = "Add Spaceline";
      this.addSpacelineToolStripMenuItem.Click += new System.EventHandler(this.mitAddSpaceline_Click);
      // 
      // CUCMultiColorList
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lbxView);
      this.Name = "CUCMultiColorList";
      this.Size = new System.Drawing.Size(232, 232);
      this.cmsMultiColorList.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListBox lbxView;
    private System.Windows.Forms.ContextMenuStrip cmsMultiColorList;
    private System.Windows.Forms.ToolStripMenuItem mitClearAllLines;
    private System.Windows.Forms.ToolStripMenuItem addSpacelineToolStripMenuItem;
  }
}
