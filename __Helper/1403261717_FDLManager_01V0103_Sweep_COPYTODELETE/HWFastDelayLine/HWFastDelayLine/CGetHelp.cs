﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;

namespace HWFastDelayLine
{
	class CGetHelp : CCommand
	{
		private const String INIT_NAME = "GetHelp";
    public const String COMMAND_TEXT = "H";

		public CGetHelp(CCommandlist parent,
									  CNotifier notifier,
									  CDevice shutter,
									  CComPort comport)
      : base(parent, INIT_NAME, COMMAND_TEXT, notifier, shutter, comport)
		{
      SetParent(this);
    }

    public override void SelfOnLineReceived(Guid comportid,
                                            String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
			Notifier.Write(CHWFastDelayLine.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
			String[] Tokenlist = line.Split(' ');
      if (1 < Tokenlist.Length)
			{
        //Commandlist.Library.RefreshHelp();//Tokenlist);
			}
		}

	}
}


