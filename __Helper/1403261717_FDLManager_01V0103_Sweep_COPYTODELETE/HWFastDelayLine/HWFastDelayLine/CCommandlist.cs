﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UCNotifier;
using ComPort;

namespace HWFastDelayLine
{
	public class CCommandlist 
	{
    private CHWFastDelayLineBase FLibrary;
		private CNotifier FNotifier;
		private List<CCommand> FList;

    public CCommandlist(CHWFastDelayLineBase library)
		{
      FLibrary = library;
			FList = new List<CCommand>();
		}

    public CHWFastDelayLineBase Library
    {
      get { return FLibrary; }
    }

		public Int32 Count
		{
			get { return FList.Count; }
		}

		public void SetNotifier(CNotifier notifier)
		{
			FNotifier = notifier;
		}

    public Boolean Add(CCommand command)
    {
      // debug FNotifier.Write("Add Command: " + command.ToString());
      FList.Add(command);
      if (1 == Count)
      {
        command.Activate(SelfOnEnd);
      }
      return true;
    }

    public Boolean Start()
    {
      if (1 <= Count)
      {
        CCommand Command = FList[0];
        Command.Activate(SelfOnEnd);
      }
      return true;
    }

    public Boolean Abort()
		{
			CCommand Command = null;
			if (0 < Count)
			{
				Command = FList[0];
				if (Command is CCommand)
				{
					Command.Abort();
				}
			}
			FList.Clear();
			// Console.WriteLine("Abort " + FList.Count);
			return (0 == FList.Count);
		}

		public void SelfOnEnd(CCommand command)
		{
      String Line = "";
      //!!!!!!!!!!String Line = String.Format("Commandlist.SelfOnEnd[{0}]", command.ToString());
      //!!!!!!!!!!FNotifier.Write(Line);
			if (0 < Count)
			{
				if (ECommandState.End == command.State)
				{
          Line = String.Format("RemoveCommand[{0}]", command.ToString());
          FNotifier.Write(Line);
          FList.Remove(command);
					if (0 < Count)
					{
						CCommand Command = FList[0];
						if (Command is CCommand)
						{
              Line = String.Format("ActivateCommand[{0}]", Command.ToString());
              FNotifier.Write(Line);
              Command.Activate(SelfOnEnd);
            }
					}
				}
			}
		}

    public void SetOnChildLineReceived(DOnChildLineReceived onchildlinereceived)
    {
      FLibrary.SetOnChildLineReceived(onchildlinereceived);
    }

    public void SetOnRXDLineReceived(DOnRXDLineReceived onrxdlinereceived)
    {
      FLibrary.SetOnRXDLineReceived(onrxdlinereceived);
    }

	}
}
