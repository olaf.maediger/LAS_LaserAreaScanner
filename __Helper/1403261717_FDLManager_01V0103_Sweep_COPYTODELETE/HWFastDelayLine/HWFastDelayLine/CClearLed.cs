﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;
//
namespace HWFastDelayLine
{
  public class CClearLed : CCommand
  {
    private const String INIT_NAME = "ClearLed";
		public const String COMMAND_TEXT = "CLD";

		public CClearLed(CCommandlist parent,
									   Byte index, 
									   CNotifier notifier,
									   CDevice device,
									   CComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, index),
             notifier, device, comport)
    {
      SetParent(this);
    }

    public override void SelfOnLineReceived(Guid comportid,
                                            String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CHWFastDelayLine.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
			String[] Tokenlist = line.Split(' ');
      if (2 < Tokenlist.Length)
      {
        Commandlist.Library.RefreshLed(Tokenlist[2], false);
      }
    }


  }
}
