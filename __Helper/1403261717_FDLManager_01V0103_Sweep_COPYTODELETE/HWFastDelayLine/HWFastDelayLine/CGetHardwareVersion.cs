﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;

namespace HWFastDelayLine
{
	class CGetHardwareVersion : CCommand
	{
    public const String INIT_NAME = "GetHardwareVersion";
    public const String COMMAND_TEXT = "GHV";

		public CGetHardwareVersion(CCommandlist parent,
															 CNotifier notifier,
															 CDevice device,
															 CComPort comport)
      : base(parent, INIT_NAME, COMMAND_TEXT, notifier, device, comport)
    {
      SetParent(this);
    }

    public override void SelfOnLineReceived(Guid comportid,
                                            String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
			Notifier.Write(CHWFastDelayLine.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
			String[] Tokenlist = line.Split(' ');
			if (2 < Tokenlist.Length)
			{
       Commandlist.Library.RefreshHardwareVersion(Tokenlist[2]);
			}
		}

	}
}

