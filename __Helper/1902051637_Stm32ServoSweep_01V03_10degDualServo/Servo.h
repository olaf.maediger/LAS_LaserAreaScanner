//
#ifndef Servo_h
#define Servo_h
//
#include <libmaple/libmaple_types.h>
#include <libmaple/timer.h>

#include <boards.h>
#include <io.h>
#include <pwm.h>
#include <wirish_math.h>
//
const uint16 SERVO_PULSEWIDTH_MINIMUM =  544;
const uint16 SERVO_PULSEWIDTH_MAXIMUM = 2400;
const  int16 SERVO_ANGLE_MINIMUM      =    0;
const  int16 SERVO_ANGLE_MAXIMUM      =  180;
//
//
// 20 millisecond period config.  For a 1-based prescaler,
//
//    (prescaler * overflow / CYC_MSEC) msec = 1 timer cycle = 20 msec
// => prescaler * overflow = 20 * CYC_MSEC
//
// This picks the smallest prescaler that allows an overflow < 2^16.
#define MAX_OVERFLOW    ((1 << 16) - 1)
#define CYC_MSEC        (1000 * CYCLES_PER_MICROSECOND)
#define TAU_MSEC        20
#define TAU_USEC        (TAU_MSEC * 1000)
#define TAU_CYC         (TAU_MSEC * CYC_MSEC)
#define SERVO_PRESCALER (TAU_CYC / MAX_OVERFLOW + 1)
#define SERVO_OVERFLOW  ((uint16)round((double)TAU_CYC / SERVO_PRESCALER))

// Unit conversions
#define US_TO_COMPARE(us) ((uint16)map((us), 0, TAU_USEC, 0, SERVO_OVERFLOW))
#define COMPARE_TO_US(c)  ((uint32)map((c), 0, SERVO_OVERFLOW, 0, TAU_USEC))
#define ANGLE_TO_US(a)    ((uint16)(map((a), FAngleMinimum, FAngleMaximum, FPulseWidthMinimum, FPulseWidthMaximum)))
#define US_TO_ANGLE(us)   ((int16)(map((us), FPulseWidthMinimum, FPulseWidthMaximum, FAngleMinimum, FAngleMaximum)))

//

//
//
class CServo 
{
  private:
  byte FPin;
  short unsigned FPulseWidthMinimum;
  short unsigned FPulseWidthMaximum;
  short signed FAngleMinimum;
  short signed FAngleMaximum;
  //
  public:
  CServo(byte pin,
         short unsigned pulsewidthminimum = SERVO_PULSEWIDTH_MINIMUM,
         short unsigned pulsewidthmaximum = SERVO_PULSEWIDTH_MAXIMUM,
         short signed angleminimum = SERVO_ANGLE_MINIMUM,
         short signed anglemaximum = SERVO_ANGLE_MAXIMUM);
  void SetAngledeg(short signed angle);
  void SetPulseWidthus(short unsigned pulsewidth);
};
//
#endif // Servo_h

