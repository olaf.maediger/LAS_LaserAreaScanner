//
#include "Servo.h"
//
CServo::CServo(byte pin,
               short unsigned pulsewidthminimum,
               short unsigned pulsewidthmaximum,
               short signed angleminimum,
               short signed anglemaximum)
{
  FPin = pin;
  FAngleMinimum = angleminimum;
  FAngleMaximum = anglemaximum;
  FPulseWidthMinimum = pulsewidthminimum;
  FPulseWidthMaximum = pulsewidthmaximum;
  //
  timer_dev *tdev = PIN_MAP[pin].timer_device;
  //
  pinMode(FPin, PWM);
  timer_pause(tdev);
  timer_set_prescaler(tdev, SERVO_PRESCALER - 1); // prescaler is 1-based
  timer_set_reload(tdev, SERVO_OVERFLOW);
  timer_generate_update(tdev);
  timer_resume(tdev);
}

void CServo::SetAngledeg(short signed angle) 
{
  angle = constrain(angle, FAngleMinimum, FAngleMaximum);
  SetPulseWidthus(ANGLE_TO_US(angle));
}

void CServo::SetPulseWidthus(short unsigned pulsewidth) 
{
  pulsewidth = constrain(pulsewidth, FPulseWidthMinimum, FPulseWidthMaximum);
  pwmWrite(FPin, US_TO_COMPARE(pulsewidth));
}


