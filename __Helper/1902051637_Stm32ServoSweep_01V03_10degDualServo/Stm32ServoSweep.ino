//
#include "Servo.h"
//
CServo ServoX(PB8);
CServo ServoY(PB9);
//
void setup() 
{
  pinMode(PC13, OUTPUT);
  digitalWrite(PC13, LOW);
  for (int Angle = 30; Angle <= 150; Angle += 1)
  {
    ServoX.SetAngledeg(Angle);
    ServoY.SetAngledeg(Angle);
    delay(10);
  }
  digitalWrite(PC13, HIGH);
  for (int Angle = 150; 30 <= Angle; Angle -= 1)
  {
    ServoX.SetAngledeg(Angle);
    ServoY.SetAngledeg(Angle);
    delay(10);
  }
  digitalWrite(PC13, LOW);
  for (int Angle = 30; Angle <= 90; Angle += 1)
  {
    ServoX.SetAngledeg(Angle);
    ServoY.SetAngledeg(Angle);
    delay(10);
  }
  for (int I = 1; I < 10; I++)
  {
    digitalWrite(PC13, LOW);
    delay(100);
    digitalWrite(PC13, HIGH);
    delay(100);
  }
}
//
void loop()
{
  digitalWrite(PC13, LOW);
  for (int Angle = 86; Angle <= 94; Angle += 1)
  {
    ServoX.SetAngledeg(Angle);
    ServoY.SetAngledeg(Angle);
    delay(100);
  }
  digitalWrite(PC13, HIGH);
  for (int Angle = 94; 86 <= Angle; Angle -= 1)
  {
    ServoX.SetAngledeg(Angle);
    ServoY.SetAngledeg(Angle);
    delay(100);
  }
}

