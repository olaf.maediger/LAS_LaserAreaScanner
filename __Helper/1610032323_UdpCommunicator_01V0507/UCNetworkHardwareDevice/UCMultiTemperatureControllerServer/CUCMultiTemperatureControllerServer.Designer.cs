﻿namespace UCMultiTemperatureControllerServer
{
  partial class CUCMultiTemperatureControllerServer
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label2 = new System.Windows.Forms.Label();
      this.FUCChannel3 = new UCMultiTemperatureControllerServer.CUCTemperatureServer();
      this.FUCChannel2 = new UCMultiTemperatureControllerServer.CUCTemperatureServer();
      this.FUCChannel1 = new UCMultiTemperatureControllerServer.CUCTemperatureServer();
      this.FUCChannel0 = new UCMultiTemperatureControllerServer.CUCTemperatureServer();
      this.SuspendLayout();
      // 
      // label2
      // 
      this.label2.BackColor = System.Drawing.Color.PapayaWhip;
      this.label2.Dock = System.Windows.Forms.DockStyle.Top;
      this.label2.Location = new System.Drawing.Point(0, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(635, 13);
      this.label2.TabIndex = 14;
      this.label2.Text = "MultiTemperatureController - Server : Measurement Simulation";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FUCChannel3
      // 
      this.FUCChannel3.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCChannel3.Header = "Channel 3";
      this.FUCChannel3.Location = new System.Drawing.Point(474, 13);
      this.FUCChannel3.Name = "FUCChannel3";
      this.FUCChannel3.Size = new System.Drawing.Size(158, 246);
      this.FUCChannel3.TabIndex = 18;
      this.FUCChannel3.TemperatureActual = 20D;
      this.FUCChannel3.TemperatureDelta = 10D;
      this.FUCChannel3.TemperatureMean = 20D;
      this.FUCChannel3.TimeActual = 0D;
      this.FUCChannel3.TimeDelta = 1D;
      this.FUCChannel3.TimePeriod = 60D;
      this.FUCChannel3.VariationKind = UCCommon.EVariationKind.Constant;
      // 
      // FUCChannel2
      // 
      this.FUCChannel2.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCChannel2.Header = "Channel 2";
      this.FUCChannel2.Location = new System.Drawing.Point(316, 13);
      this.FUCChannel2.Name = "FUCChannel2";
      this.FUCChannel2.Size = new System.Drawing.Size(158, 246);
      this.FUCChannel2.TabIndex = 17;
      this.FUCChannel2.TemperatureActual = 20D;
      this.FUCChannel2.TemperatureDelta = 10D;
      this.FUCChannel2.TemperatureMean = 20D;
      this.FUCChannel2.TimeActual = 0D;
      this.FUCChannel2.TimeDelta = 1D;
      this.FUCChannel2.TimePeriod = 60D;
      this.FUCChannel2.VariationKind = UCCommon.EVariationKind.Constant;
      // 
      // FUCChannel1
      // 
      this.FUCChannel1.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCChannel1.Header = "Channel 1";
      this.FUCChannel1.Location = new System.Drawing.Point(158, 13);
      this.FUCChannel1.Name = "FUCChannel1";
      this.FUCChannel1.Size = new System.Drawing.Size(158, 246);
      this.FUCChannel1.TabIndex = 16;
      this.FUCChannel1.TemperatureActual = 20D;
      this.FUCChannel1.TemperatureDelta = 10D;
      this.FUCChannel1.TemperatureMean = 20D;
      this.FUCChannel1.TimeActual = 0D;
      this.FUCChannel1.TimeDelta = 1D;
      this.FUCChannel1.TimePeriod = 60D;
      this.FUCChannel1.VariationKind = UCCommon.EVariationKind.Constant;
      // 
      // FUCChannel0
      // 
      this.FUCChannel0.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCChannel0.Header = "Channel 0";
      this.FUCChannel0.Location = new System.Drawing.Point(0, 13);
      this.FUCChannel0.Name = "FUCChannel0";
      this.FUCChannel0.Size = new System.Drawing.Size(158, 246);
      this.FUCChannel0.TabIndex = 15;
      this.FUCChannel0.TemperatureActual = 20D;
      this.FUCChannel0.TemperatureDelta = 10D;
      this.FUCChannel0.TemperatureMean = 20D;
      this.FUCChannel0.TimeActual = 0D;
      this.FUCChannel0.TimeDelta = 1D;
      this.FUCChannel0.TimePeriod = 60D;
      this.FUCChannel0.VariationKind = UCCommon.EVariationKind.Constant;
      // 
      // CUCMultiTemperatureControllerServer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Lavender;
      this.Controls.Add(this.FUCChannel3);
      this.Controls.Add(this.FUCChannel2);
      this.Controls.Add(this.FUCChannel1);
      this.Controls.Add(this.FUCChannel0);
      this.Controls.Add(this.label2);
      this.Name = "CUCMultiTemperatureControllerServer";
      this.Size = new System.Drawing.Size(635, 259);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label2;
    private CUCTemperatureServer FUCChannel0;
    private CUCTemperatureServer FUCChannel1;
    private CUCTemperatureServer FUCChannel2;
    private CUCTemperatureServer FUCChannel3;
  }
}
