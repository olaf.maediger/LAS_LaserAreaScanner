﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Net;
//
using UCNotifier;
using Task;
using UdpTransfer;
using UdpXmlTransfer;
using NetworkHardwareDevice;
using NhdMultiTemperatureController;
//
namespace UCMultiTemperatureControllerServer
{
  public partial class CUCMultiTemperatureControllerServer : UserControl
  {
    //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    public CUCMultiTemperatureControllerServer()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public Boolean IsOpen
    {
      get { return FUCChannel0.IsOpen; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper 
    //------------------------------------------------------------------------
    //
    private void SetPresetMeasurement(Int32 channel, Int32 countmeasurements, Int32 timeinterval)
    {
    }
    //
    //------------------------------------------------------------------------
    //  Section - Get/SetData
    //------------------------------------------------------------------------
    //
    public Boolean GetDataTemperature(Int32 channel, out RDataTemperatureServer datatemperature)
    {
      switch (channel)
      {
        case 0:
          return FUCChannel0.GetDataTemperature(out datatemperature);
        case 1:
          return FUCChannel1.GetDataTemperature(out datatemperature);
        case 2:
          return FUCChannel2.GetDataTemperature(out datatemperature);
        case 3:
          return FUCChannel3.GetDataTemperature(out datatemperature);
      }
      FUCChannel0.GetDataTemperature(out datatemperature);
      return false;
    }

    public void SetDataTemperature(Int32 channel, RDataTemperatureServer datatemperature)
    {
      switch (channel)
      {
        case 0:
          FUCChannel0.SetDataTemperature(datatemperature);
          return;
        case 1:
          FUCChannel1.SetDataTemperature(datatemperature);
          return;
        case 2:
          FUCChannel2.SetDataTemperature(datatemperature);
          return;
        case 3:
          FUCChannel3.SetDataTemperature(datatemperature);
          return;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Management
    //------------------------------------------------------------------------
    //
     public void Start(Int32 channel)
    {
      switch (channel)
      {
        case 0:
          FUCChannel0.Start();
          break;
        case 1:
          FUCChannel1.Start();
          break;
        case 2:
          FUCChannel2.Start();
          break;
        case 3:
          FUCChannel3.Start();
          break;
      }
    }
    public void Stop(Int32 channel)
    {
      switch (channel)
      {
        case 0:
          FUCChannel0.Stop();
          break;
        case 1:
          FUCChannel1.Stop();
          break;
        case 2:
          FUCChannel2.Stop();
          break;
        case 3:
          FUCChannel3.Stop();
          break;
      }

    }


  }
}
