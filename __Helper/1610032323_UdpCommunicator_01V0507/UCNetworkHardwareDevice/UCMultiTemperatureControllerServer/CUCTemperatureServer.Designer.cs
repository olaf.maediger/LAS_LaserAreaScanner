﻿namespace UCMultiTemperatureControllerServer
{
  partial class CUCTemperatureServer
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.FUCTimeActual = new UCCommon.CUCTime();
      this.FUCTemperatureActual = new UCCommon.CUCTemperature();
      this.FUCTimeDelta = new UCCommon.CUCTime();
      this.FUCTimePeriod = new UCCommon.CUCTime();
      this.FUCVariationKind = new UCCommon.CUCVariationKind();
      this.FUCTemperatureDelta = new UCCommon.CUCTemperature();
      this.FUCTemperatureMean = new UCCommon.CUCTemperature();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.Color.PapayaWhip;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(158, 13);
      this.lblHeader.TabIndex = 15;
      this.lblHeader.Text = "<header>";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FUCTimeActual
      // 
      this.FUCTimeActual.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTimeActual.HeaderText = "TimeActual";
      this.FUCTimeActual.HeaderWidth = 74;
      this.FUCTimeActual.Location = new System.Drawing.Point(3, 18);
      this.FUCTimeActual.Name = "FUCTimeActual";
      this.FUCTimeActual.Size = new System.Drawing.Size(152, 27);
      this.FUCTimeActual.TabIndex = 22;
      this.FUCTimeActual.TopBorderHeight = 5;
      this.FUCTimeActual.Unit = "s";
      this.FUCTimeActual.Value = 0D;
      // 
      // FUCTemperatureActual
      // 
      this.FUCTemperatureActual.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTemperatureActual.HeaderText = "Tactual";
      this.FUCTemperatureActual.HeaderWidth = 74;
      this.FUCTemperatureActual.Location = new System.Drawing.Point(3, 48);
      this.FUCTemperatureActual.Name = "FUCTemperatureActual";
      this.FUCTemperatureActual.Size = new System.Drawing.Size(152, 27);
      this.FUCTemperatureActual.TabIndex = 21;
      this.FUCTemperatureActual.TopBorderHeight = 5;
      this.FUCTemperatureActual.Unit = "°C";
      this.FUCTemperatureActual.Value = 20D;
      // 
      // FUCTimeDelta
      // 
      this.FUCTimeDelta.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTimeDelta.HeaderText = "TimeDelta";
      this.FUCTimeDelta.HeaderWidth = 74;
      this.FUCTimeDelta.Location = new System.Drawing.Point(3, 178);
      this.FUCTimeDelta.Name = "FUCTimeDelta";
      this.FUCTimeDelta.Size = new System.Drawing.Size(152, 27);
      this.FUCTimeDelta.TabIndex = 20;
      this.FUCTimeDelta.TopBorderHeight = 5;
      this.FUCTimeDelta.Unit = "s";
      this.FUCTimeDelta.Value = 0.3D;
      // 
      // FUCTimePeriod
      // 
      this.FUCTimePeriod.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTimePeriod.HeaderText = "TimePeriod";
      this.FUCTimePeriod.HeaderWidth = 74;
      this.FUCTimePeriod.Location = new System.Drawing.Point(3, 148);
      this.FUCTimePeriod.Name = "FUCTimePeriod";
      this.FUCTimePeriod.Size = new System.Drawing.Size(152, 27);
      this.FUCTimePeriod.TabIndex = 19;
      this.FUCTimePeriod.TopBorderHeight = 5;
      this.FUCTimePeriod.Unit = "s";
      this.FUCTimePeriod.Value = 60D;
      // 
      // FUCVariationKind
      // 
      this.FUCVariationKind.BackColor = System.Drawing.SystemColors.Info;
      this.FUCVariationKind.HeaderText = "Variation";
      this.FUCVariationKind.HeaderWidth = 74;
      this.FUCVariationKind.Index = 0;
      this.FUCVariationKind.Location = new System.Drawing.Point(3, 211);
      this.FUCVariationKind.Name = "FUCVariationKind";
      this.FUCVariationKind.Size = new System.Drawing.Size(152, 27);
      this.FUCVariationKind.TabIndex = 18;
      this.FUCVariationKind.TopBorderHeight = 3;
      this.FUCVariationKind.Unit = "";
      this.FUCVariationKind.Value = UCCommon.EVariationKind.Constant;
      // 
      // FUCTemperatureDelta
      // 
      this.FUCTemperatureDelta.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTemperatureDelta.HeaderText = "Tdelta";
      this.FUCTemperatureDelta.HeaderWidth = 74;
      this.FUCTemperatureDelta.Location = new System.Drawing.Point(3, 114);
      this.FUCTemperatureDelta.Name = "FUCTemperatureDelta";
      this.FUCTemperatureDelta.Size = new System.Drawing.Size(152, 27);
      this.FUCTemperatureDelta.TabIndex = 17;
      this.FUCTemperatureDelta.TopBorderHeight = 5;
      this.FUCTemperatureDelta.Unit = "°C";
      this.FUCTemperatureDelta.Value = 10D;
      // 
      // FUCTemperatureMean
      // 
      this.FUCTemperatureMean.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTemperatureMean.HeaderText = "Tmean";
      this.FUCTemperatureMean.HeaderWidth = 74;
      this.FUCTemperatureMean.Location = new System.Drawing.Point(3, 84);
      this.FUCTemperatureMean.Name = "FUCTemperatureMean";
      this.FUCTemperatureMean.Size = new System.Drawing.Size(152, 27);
      this.FUCTemperatureMean.TabIndex = 16;
      this.FUCTemperatureMean.TopBorderHeight = 5;
      this.FUCTemperatureMean.Unit = "°C";
      this.FUCTemperatureMean.Value = 20D;
      // 
      // CUCTemperatureServer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCTimeActual);
      this.Controls.Add(this.FUCTemperatureActual);
      this.Controls.Add(this.FUCTimeDelta);
      this.Controls.Add(this.FUCTimePeriod);
      this.Controls.Add(this.FUCVariationKind);
      this.Controls.Add(this.FUCTemperatureDelta);
      this.Controls.Add(this.FUCTemperatureMean);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCTemperatureServer";
      this.Size = new System.Drawing.Size(158, 242);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private UCCommon.CUCTemperature FUCTemperatureMean;
    private UCCommon.CUCTemperature FUCTemperatureDelta;
    private UCCommon.CUCVariationKind FUCVariationKind;
    private UCCommon.CUCTime FUCTimePeriod;
    private UCCommon.CUCTime FUCTimeDelta;
    private UCCommon.CUCTemperature FUCTemperatureActual;
    private UCCommon.CUCTime FUCTimeActual;
  }
}
