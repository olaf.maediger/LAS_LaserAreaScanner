﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCCommon;
//
namespace UCMultiTemperatureControllerServer
{

  public partial class CUCTemperatureServer : UserControl
  { //
    //------------------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------------------
    //
 
    //
    //------------------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------------------
    //
    private CTemperatureServer FTemperatureServer;
    //
    //------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------
    //
    public CUCTemperatureServer()
    {
      InitializeComponent();
      //
      FTemperatureServer = new CTemperatureServer();
      FTemperatureServer.SetOnDataTemperatureChanged(TemperatureServerOnDataTemperatureChanged);
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      DT.TemperatureMean = FUCTemperatureMean.Value;
      DT.TemperatureDelta = FUCTemperatureDelta.Value;
      DT.TemperatureActual = FUCTemperatureActual.Value;
      DT.TimePeriod = FUCTimePeriod.Value;
      DT.TimeDelta = FUCTimeDelta.Value;
      DT.TimeActual = FUCTimeActual.Value;
      FTemperatureServer.SetData(DT);
      //
      FUCTimePeriod.SetOnTimeChanged(UCTimePeriodOnTimeChanged);
      FUCTimeDelta.SetOnTimeChanged(UCTimeDeltaOnTimeChanged);
      FUCTimeActual.SetOnTimeChanged(UCTimeActualOnTimeChanged);
      //
      FUCTemperatureActual.SetOnTemperatureChanged(UCTemperatureActualOnTemperatureChanged);
      FUCTemperatureMean.SetOnTemperatureChanged(UCTemperatureMeanOnTemperatureChanged);
      FUCTemperatureDelta.SetOnTemperatureChanged(UCTemperatureDeltaOnTemperatureChanged);
      //
      FUCVariationKind.SetOnVariationKindChanged(UCVariationKindOnVariationKindChanged);
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------
    //
    public String Header
    {
      get { return lblHeader.Text; }
      set { lblHeader.Text = value; }
    }
    //
    private Double GetTemperatureMean()
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      FUCTemperatureMean.Value = DT.TemperatureMean;
      return DT.TemperatureMean;
    }
    private void SetTemperatureMean(Double value)
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      DT.TemperatureMean = value;
      FUCTemperatureMean.Value = value;
      FTemperatureServer.SetData(DT);
    }
    public Double TemperatureMean
    {
      get { return GetTemperatureMean(); }
      set { SetTemperatureMean(value); }
    }
    //
    private Double GetTemperatureDelta()
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      FUCTemperatureDelta.Value = DT.TemperatureDelta; 
      return DT.TemperatureDelta;
    }
    private void SetTemperatureDelta(Double value)
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      DT.TemperatureDelta = value;
      FUCTemperatureDelta.Value = value;
      FTemperatureServer.SetData(DT);
    }
    public Double TemperatureDelta
    {
      get { return GetTemperatureDelta(); }
      set { SetTemperatureDelta(value); }
    }
    //
    private Double GetTemperatureActual()
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      FUCTemperatureActual.Value = DT.TemperatureActual;
      return DT.TemperatureActual;
    }
    private void SetTemperatureActual(Double value)
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      DT.TemperatureActual = value;
      FUCTemperatureActual.Value = value;
      FTemperatureServer.SetData(DT);
    }
    public Double TemperatureActual
    {
      get { return GetTemperatureActual(); }
      set { SetTemperatureActual(value); }
    }
    //
    private Double GetTimePeriod()
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      FUCTimePeriod.Value = DT.TimePeriod;
      return DT.TimePeriod;
    }
    private void SetTimePeriod(Double value)
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      DT.TimePeriod = value;
      FUCTimePeriod.Value = value;
      FTemperatureServer.SetData(DT);
    }
    public Double TimePeriod
    {
      get { return GetTimePeriod(); }
      set { SetTimePeriod(value); }
    }
    //
    private Double GetTimeDelta()
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      FUCTimeDelta.Value = DT.TimeDelta;
      return DT.TimeDelta;
    }
    private void SetTimeDelta(Double value)
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      DT.TimeDelta = value;
      FUCTimeDelta.Value = value;
      FTemperatureServer.SetData(DT);
    }
    public Double TimeDelta
    {
      get { return GetTimeDelta(); }
      set { SetTimeDelta(value); }
    }
    //
    private Double GetTimeActual()
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      FUCTimeActual.Value = DT.TimeActual;
      return DT.TimeActual;
    }
    private void SetTimeActual(Double value)
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      DT.TimeActual = value;
      FUCTimeActual.Value = value;
      FTemperatureServer.SetData(DT);
    }
    public Double TimeActual
    {
      get { return GetTimeActual(); }
      set { SetTimeActual(value); }
    }
    //
    private EVariationKind GetVariationKind()
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      FUCVariationKind.Value = DT.VariationKind;
      return DT.VariationKind;
    }
    private void SetVariationKind(EVariationKind value)
    {
      RDataTemperatureServer DT;
      FTemperatureServer.GetData(out DT);
      DT.VariationKind = value;
      FUCVariationKind.Value = value;
      FTemperatureServer.SetData(DT);
    }
    public EVariationKind VariationKind
    {
      get { return GetVariationKind(); }
      set { SetVariationKind(value); }
    }
    //
    public Boolean IsOpen
    {
      get { return FTemperatureServer.IsOpen; }
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Get/SetData
    //------------------------------------------------------------------------------------
    //
    public Boolean GetDataTemperature(out RDataTemperatureServer datatemperature)
    {
      FTemperatureServer.GetData(out datatemperature);
      return true;
    }
    public Boolean SetDataTemperature(RDataTemperatureServer datatemperature)
    {
      FUCTemperatureMean.Value = datatemperature.TemperatureMean;
      FUCTemperatureDelta.Value = datatemperature.TemperatureDelta;
      FUCTemperatureActual.Value = datatemperature.TemperatureActual;
      FUCTimePeriod.Value = datatemperature.TimePeriod;
      FUCTimeDelta.Value = datatemperature.TimeDelta;
      FUCTimeActual.Value = datatemperature.TimeActual;
      FUCVariationKind.Value = datatemperature.VariationKind;        
      FTemperatureServer.SetData(datatemperature);
      return true;
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------------------
    //  Segment - Callback - UControls
    //------------------------------------------------------------------------------------
    //
    private void UCTimePeriodOnTimeChanged(Double time)
    {
      TimePeriod = time;
    }
    private void UCTimeDeltaOnTimeChanged(Double time)
    {
      TimeDelta = time;
    }
    private void UCTimeActualOnTimeChanged(Double time)
    {
      TimeActual = time;
    }
    //
    private void UCTemperatureActualOnTemperatureChanged(Double temperature)
    {
      TemperatureActual = temperature;
    }
    private void UCTemperatureMeanOnTemperatureChanged(Double temperature)
    {
      TemperatureMean = temperature;
    }
    private void UCTemperatureDeltaOnTemperatureChanged(Double temperature)
    {
      TemperatureDelta = temperature;
    }
    //
    private void UCVariationKindOnVariationKindChanged(EVariationKind variationkind)
    {
      VariationKind = variationkind;
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Callback - TemperatureServer
    //------------------------------------------------------------------------------------
    //
    private delegate void CBTemperatureServerOnDataTemperatureChanged(RDataTemperatureServer data);
    private void TemperatureServerOnDataTemperatureChanged(RDataTemperatureServer data)
    {
      if (this.InvokeRequired)
      {
        CBTemperatureServerOnDataTemperatureChanged CB =
          new CBTemperatureServerOnDataTemperatureChanged(TemperatureServerOnDataTemperatureChanged);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCTimeActual.Value = data.TimeActual;
        FUCTimeDelta.Value = data.TimeDelta;
        FUCTimePeriod.Value = data.TimePeriod;
        FUCTemperatureActual.Value = data.TemperatureActual;
        FUCTemperatureDelta.Value = data.TemperatureDelta;
        FUCTemperatureMean.Value = data.TemperatureMean;
      }
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Management
    //------------------------------------------------------------------------------------
    //
    public Boolean Start()
    {
      return FTemperatureServer.Start();
    }
    public Boolean Stop()
    {
      return FTemperatureServer.Stop();
    }

  }
}
