﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using Task;
using UCNotifier;
using UCCommon;
//
namespace UCMultiTemperatureControllerServer
{
  public delegate void DOnDataTemperatureChanged(RDataTemperatureServer data);

  public struct RDataTemperatureServer
  {
    public Double TemperatureMean;
    public Double TemperatureDelta;
    public Double TemperatureActual;
    public Double TimePeriod;
    public Double TimeDelta;
    public Double TimeActual;
    public EVariationKind VariationKind;
    public Int32 StepPreset;
    public Int32 StepActual;
    //
    public RDataTemperatureServer(Int32 init)
    {
      TemperatureMean = CTemperatureServer.INIT_TEMPARATUREMEAN;
      TemperatureDelta = CTemperatureServer.INIT_TEMPARATUREDELTA;
      TemperatureActual = CTemperatureServer.INIT_TEMPARATUREACTUAL;
      TimePeriod = CTemperatureServer.INIT_TIMEPERIOD;
      TimeDelta = CTemperatureServer.INIT_TIMEDELTA;
      TimeActual = CTemperatureServer.INIT_TIMEACTUAL;
      VariationKind = CTemperatureServer.INIT_KINDVARIATION;
      StepPreset = CTemperatureServer.INIT_STEPPRESET;
      StepActual = CTemperatureServer.INIT_STEPACTUAL;
    }
    public void Info(CNotifier notifier, String header)
    {
      String Line = String.Format("{0} - RDataTemperatureServer:", header);
      notifier.Write(Line);
      Line = String.Format("TemperatureMean [C]: {0}", TemperatureMean);
      notifier.Write(Line);
      Line = String.Format("TemperatureDelta [C]: {0}", TemperatureDelta);
      notifier.Write(Line);
      Line = String.Format("TemperatureActual [C]: {0}", TemperatureActual);
      notifier.Write(Line);
      Line = String.Format("TimePeriod [s]: {0}", TimePeriod);
      notifier.Write(Line);
      Line = String.Format("TimeDelta [s]: {0}", TimeDelta);
      notifier.Write(Line);
      Line = String.Format("TimeActual [s]: {0}", TimeActual);
      notifier.Write(Line);
      Line = CVariationKind.VariationKindToText(VariationKind);
      Line = String.Format("VariationKind: {0}", Line);
      notifier.Write(Line);
      Line = String.Format("StepPreset [1]: {0}", StepPreset);
      notifier.Write(Line);
      Line = String.Format("StepActual [1]: {0}", StepActual);
      notifier.Write(Line);
    }
  }

  public class CTemperatureServer
  { //
    //------------------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------------------
    //
    public const Double INIT_TEMPARATUREMEAN = 20.0; // [°C]
    public const Double INIT_TEMPARATUREDELTA = 10.0; // [°C]
    public const Double INIT_TEMPARATUREACTUAL = 19.0; // [°C]
    public const Double INIT_TIMEPERIOD = 10.0; // [sec]
    public const Double INIT_TIMEDELTA = 0.1; // [sec]
    public const Double INIT_TIMEACTUAL = 0.0; // [sec]
    public const EVariationKind INIT_KINDVARIATION = EVariationKind.Constant;
    public const Int32 INIT_STEPPRESET = 0; // [1] - 0 : infinite
    public const Int32 INIT_STEPACTUAL = 0; // [1] 
    //
    //------------------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------------------
    //
    private CTask FTask;
    private RDataTemperatureServer FData;
    private DOnDataTemperatureChanged FOnDataTemperatureChanged;
    private Random FRandom;
    private Int32 FDirection;
    //
    //------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------
    //
    public CTemperatureServer()
    {
      FTask = null;
      FData = new RDataTemperatureServer(0);
      Thread.Sleep(1);
      FRandom = new Random((Int32)DateTime.Now.Ticks);
      Thread.Sleep(1); 
      FRandom = new Random(FRandom.Next(100, 200) * (Int32)DateTime.Now.Ticks);
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------
    //
    public void SetOnDataTemperatureChanged(DOnDataTemperatureChanged value)
    {
      FOnDataTemperatureChanged = value;
    }

    public void SetData(RDataTemperatureServer data)
    {
      FData.TemperatureMean = data.TemperatureMean;
      FData.TemperatureDelta = data.TemperatureDelta;
      FData.TemperatureActual = data.TemperatureActual;
      FData.TimePeriod = data.TimePeriod;
      FData.TimeDelta = data.TimeDelta;
      FData.TimeActual = data.TimeActual;
      FData.VariationKind = data.VariationKind;
      FData.StepPreset = data.StepPreset;
      FData.StepActual = data.StepActual;
    }
    public void GetData(out RDataTemperatureServer data)
    {
      data = FData;
    }

    public Boolean IsOpen
    {
      get { return (FTask is CTask); }
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Callback - Task
    //------------------------------------------------------------------------------------
    //
    private void OnExecutionStart(RTaskData data)
    {
      FDirection = +1;
    }
    private Boolean OnExecutionBusy(ref RTaskData data)
    {
      FData.TimeActual += FData.TimeDelta;
      FData.StepActual++;
      switch (FData.VariationKind)
      {
        case EVariationKind.Constant:
          FData.TemperatureActual = FData.TemperatureMean + FData.TemperatureDelta * (-0.499 + FRandom.NextDouble());
          break;
        case EVariationKind.Linear:
          Double F = 0.1;
          if (0 < FData.TimePeriod)
          {
            F = 1 / FData.TimePeriod;
          }
          if (0 <= FDirection)
          {
            FData.TemperatureActual += F * FData.TemperatureDelta * FRandom.NextDouble();
            if (FData.TemperatureMean + FData.TemperatureDelta <= FData.TemperatureActual)
            {
              FDirection = -1;
            }
          }
          else
          {
            FData.TemperatureActual -= F * FData.TemperatureDelta * FRandom.NextDouble();
            if (FData.TemperatureActual <= FData.TemperatureMean - FData.TemperatureDelta)
            {
              FDirection = +1;
            }
          }
          break;
        case EVariationKind.Sinus:
          Double Argument = 6.28 * FData.TimeActual / FData.TimePeriod;
          Double Amplitude = FData.TemperatureDelta * Math.Sin(Argument);
          Double Noise = 0.1 * FData.TemperatureDelta * FRandom.NextDouble();
          FData.TemperatureActual = FData.TemperatureMean + Amplitude + Noise;
          break;
      }
      if (FOnDataTemperatureChanged is DOnDataTemperatureChanged)
      {
        FOnDataTemperatureChanged(FData);
      }
      //
      Int32 DT = 1 + (Int32)(1000 * Math.Abs(FData.TimeDelta));// -> msec
      Thread.Sleep(DT);
      //
      if (0 == FData.StepPreset)
      { 
        return true; // continous thread infinite!!!
      }
      if (FData.StepActual < FData.StepPreset)
      {
        return true; // continous thread!
      }
      return false; // ends thread!
    }
    private void OnExecutionEnd(RTaskData data)
    {
    }
    private void OnExecutionAbort(RTaskData data)
    {
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Management
    //------------------------------------------------------------------------------------
    //
    public Boolean Start()
    {
      if (!IsOpen)
      {
        FTask = new CTask("", OnExecutionStart, OnExecutionBusy, OnExecutionEnd, OnExecutionAbort);
        return FTask.Start();
      }
      return false;
    }
    public Boolean Stop()
    {
      if (IsOpen)
      {
        FTask.Abort();
        FTask = null;
        return true;
      }
      return false;
    }


  }
}
