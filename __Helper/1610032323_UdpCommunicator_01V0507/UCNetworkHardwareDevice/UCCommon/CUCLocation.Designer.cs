﻿namespace UCCommon
{
  partial class CUCLocation
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.lblUnit = new System.Windows.Forms.Label();
      this.lblTopBorder = new System.Windows.Forms.Label();
      this.nudLocation = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.nudLocation)).BeginInit();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(57, 26);
      this.lblHeader.TabIndex = 0;
      this.lblHeader.Text = "location";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblUnit
      // 
      this.lblUnit.Dock = System.Windows.Forms.DockStyle.Right;
      this.lblUnit.Location = new System.Drawing.Point(115, 0);
      this.lblUnit.Name = "lblUnit";
      this.lblUnit.Size = new System.Drawing.Size(22, 26);
      this.lblUnit.TabIndex = 4;
      this.lblUnit.Text = "pxl";
      this.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblTopBorder
      // 
      this.lblTopBorder.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTopBorder.Location = new System.Drawing.Point(57, 0);
      this.lblTopBorder.Name = "lblTopBorder";
      this.lblTopBorder.Size = new System.Drawing.Size(58, 5);
      this.lblTopBorder.TabIndex = 5;
      // 
      // nudLocation
      // 
      this.nudLocation.Dock = System.Windows.Forms.DockStyle.Top;
      this.nudLocation.Location = new System.Drawing.Point(57, 5);
      this.nudLocation.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudLocation.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
      this.nudLocation.Name = "nudLocation";
      this.nudLocation.Size = new System.Drawing.Size(58, 20);
      this.nudLocation.TabIndex = 6;
      this.nudLocation.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // CUCLocation
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.nudLocation);
      this.Controls.Add(this.lblTopBorder);
      this.Controls.Add(this.lblUnit);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCLocation";
      this.Size = new System.Drawing.Size(137, 26);
      ((System.ComponentModel.ISupportInitialize)(this.nudLocation)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Label lblUnit;
    private System.Windows.Forms.Label lblTopBorder;
    private System.Windows.Forms.NumericUpDown nudLocation;
  }
}
