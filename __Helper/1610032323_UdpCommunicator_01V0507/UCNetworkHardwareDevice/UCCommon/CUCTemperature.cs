﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCCommon
{
  public delegate void DOnTemperatureChanged(Double temperature);

  public partial class CUCTemperature : UserControl
  {
    //
    //------------------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------------------
    //
    private DOnTemperatureChanged FOnTemperatureChanged;
    //
    //------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------
    //
    public CUCTemperature()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------
    //
    public void SetOnTemperatureChanged(DOnTemperatureChanged value)
    {
      FOnTemperatureChanged = value;
    }

    public Double Value
    {
      get { return (Double)nudValue.Value; }
      set { nudValue.Value = (Decimal)value; }
    }

    public String Unit
    {
      get { return lblUnit.Text; }
      set { lblUnit.Text = value; }
    }

    public Int32 TopBorderHeight
    {
      get { return lblTopBorder.Height; }
      set { lblTopBorder.Height = value; }
    }

    public Int32 HeaderWidth
    {
      get { return lblHeader.Width; }
      set { lblHeader.Width = value; }
    }
    public String HeaderText
    {
      get { return lblHeader.Text; }
      set { lblHeader.Text = value; }
    }

    private void nudValue_ValueChanged(object sender, EventArgs e)
    {
      if (FOnTemperatureChanged is DOnTemperatureChanged)
      {
        FOnTemperatureChanged((Double)nudValue.Value);
      }
    }


  }
}
