﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCCommon
{
  public partial class CUCText : UserControl
  {
    //
    //------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------
    //
    public CUCText()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------
    //
    public String Value
    {
      get { return tbxText.Text; }
      set { tbxText.Text = value; }
    }
    public Int32 TopBorderHeight
    {
      get { return lblTopBorder.Height; }
      set { lblTopBorder.Height = value; }
    }

    public Int32 HeaderWidth
    {
      get { return lblHeader.Width; }
      set { lblHeader.Width = value; }
    }
    public String HeaderText
    {
      get { return lblHeader.Text; }
      set { lblHeader.Text = value; }
    }

    public Int32 UnitWidth
    {
      get { return lblUnit.Width; }
      set { lblUnit.Width = value; }
    }
    public String UnitText
    {
      get { return lblUnit.Text; }
      set { lblUnit.Text = value; }
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - 
    //------------------------------------------------------------------------------------
    //
    //
    //------------------------------------------------------------------------------------
    //  Segment - 
    //------------------------------------------------------------------------------------
    //
  }
}
