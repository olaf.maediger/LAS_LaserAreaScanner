﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCCommon
{
  public delegate void DOnVariationKindChanged(EVariationKind value);

  public partial class CUCVariationKind : UserControl
  {
    //
    //------------------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------------------
    //
    private DOnVariationKindChanged FOnVariationKindChanged;
    //
    //------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------
    //
    public CUCVariationKind()
    {
      InitializeComponent();
      //
      cbxVariationKind.SelectedIndex = 0;
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------
    //
    public void SetOnVariationKindChanged(DOnVariationKindChanged value)
    {
      FOnVariationKindChanged = value;
    }

    private EVariationKind GetValue()
    {
      return IndexToVariationKind(cbxVariationKind.SelectedIndex);
    }
    private void SetValue(EVariationKind value)
    {
      switch (value)
      {
        case EVariationKind.Constant:
          cbxVariationKind.SelectedIndex = 0;
          break;
        case EVariationKind.Linear:
          cbxVariationKind.SelectedIndex = 1;
          break;
        case EVariationKind.Sinus:
          cbxVariationKind.SelectedIndex = 2;
          break;
      }
    }
    public EVariationKind Value
    {
      get { return GetValue(); }
      set { SetValue(value); }
    }

    private void SetIndex(Int32 value)
    {
      if ((0 <= value) && (value < cbxVariationKind.Items.Count))
      {
        cbxVariationKind.SelectedIndex = value;
      }
    }
    public Int32 Index
    {
      get { return cbxVariationKind.SelectedIndex; }
      set { SetIndex(value); }
    }

    public String Unit
    {
      get { return lblUnit.Text; }
      set { lblUnit.Text = value; }
    }

    public Int32 TopBorderHeight
    {
      get { return lblTopBorder.Height; }
      set { lblTopBorder.Height = value; }
    }

    public Int32 HeaderWidth
    {
      get { return lblHeader.Width; }
      set { lblHeader.Width = value; }
    }
    public String HeaderText
    {
      get { return lblHeader.Text; }
      set { lblHeader.Text = value; }
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Event
    //------------------------------------------------------------------------------------
    //
    private void cbxVariationKind_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FOnVariationKindChanged is DOnVariationKindChanged)
      {
        EVariationKind VK = IndexToVariationKind(cbxVariationKind.SelectedIndex);
        FOnVariationKindChanged(VK);
      }
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------------------
    //
    public String IndexToText()
    {
      return (String)cbxVariationKind.Items[Index];
    }
    public EVariationKind IndexToVariationKind(Int32 index)
    {
      switch (index)
      {
        case 0:
          return EVariationKind.Constant;
        case 1:
          return EVariationKind.Linear;
        case 2:
          return EVariationKind.Sinus;
      }
      return EVariationKind.Constant;
    }



  }
}
