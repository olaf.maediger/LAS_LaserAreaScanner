﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace UCDisplayServer
{
  public abstract class CElement
  {

    public abstract void Draw(Graphics graphics);

  }

  public class CElementList : List<CElement>
  {
  }

}
