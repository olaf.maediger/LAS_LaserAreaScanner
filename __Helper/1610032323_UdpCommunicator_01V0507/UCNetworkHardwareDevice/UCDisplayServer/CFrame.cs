﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NhdDisplay;
//
namespace UCDisplayServer
{
  public class CFrame : CElement
  {
      
    private Int32 FLocationX;
    private Int32 FLocationY;
    private Int32 FWidth;
    private Int32 FHeight;
    private Pen FPen;

    public CFrame(Int32 locationx, Int32 locationy, Int32 width, Int32 height)
    {
      FLocationX = locationx;
      FLocationY = locationy;
      FWidth = width;
      FHeight = height;
      FPen = new Pen(Color.Red, 3);
    }

    public override void Draw(Graphics graphics)
    {
      graphics.DrawRectangle(FPen, FLocationX, FLocationY, FWidth, FHeight);
    }


  }
}
