﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace UCDisplayServer
{
  public class CText : CElement
  {
    private Int32 FLocationX;
    private Int32 FLocationY;
    private String FText;
    private Brush FBrush;
    private Font FFont;

    public CText(Int32 locationx, Int32 locationy, String text)
    {
      FLocationX = locationx;
      FLocationY = locationy;
      FText = text;
      FBrush = new SolidBrush(Color.Black);
      FFont = new Font("Arial", 12f);
    }

    public override void Draw(Graphics graphics)
    {
      graphics.DrawString(FText, FFont, FBrush, FLocationX, FLocationY);
    }

  }
}
