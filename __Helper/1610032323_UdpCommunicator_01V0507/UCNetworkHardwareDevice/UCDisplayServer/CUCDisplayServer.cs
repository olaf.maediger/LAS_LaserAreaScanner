﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
//
using NhdDisplay;
//
namespace UCDisplayServer
{
  public partial class CUCDisplayServer : UserControl
  {
    //
    //-----------------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------------
    //
    private Bitmap FBitmap;
    private CElementList FElementList;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------------------
    //
    public CUCDisplayServer()
    {
      InitializeComponent();
      BorderStyle = BorderStyle.FixedSingle;
      BackColor = Color.Beige;
      //
      FBitmap = new Bitmap(this.Width, this.Height, PixelFormat.Format32bppArgb);
      FElementList = new CElementList();
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Helper
    //-----------------------------------------------------------------------------
    //   
    private void Redraw()
    {
      Graphics G = Graphics.FromImage(FBitmap);
      foreach (CElement Element in FElementList)
      {
        Element.Draw(G);
      }
      G.Dispose();
      Invalidate();
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Event
    //-----------------------------------------------------------------------------
    //
    private void SelfOnSizeChanged(object sender, EventArgs e)
    {
      FBitmap.Dispose();
      FBitmap = new Bitmap(this.Width, this.Height, PixelFormat.Format32bppArgb);
      Redraw();
    }

    private void CUCDisplayServer_Paint(object sender, PaintEventArgs e)
    {
      Graphics G = e.Graphics;
      G.DrawImage(FBitmap, 0, 0);
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Management
    //-----------------------------------------------------------------------------
    //
    public void ClearDisplay()
    {
      FBitmap.Dispose();
      FBitmap = new Bitmap(this.Width, this.Height, PixelFormat.Format32bppArgb);
      FElementList.Clear();
      Redraw();
    }

    public void AddText(Int32 locationx, Int32 locationy, String text)
    {
      CText Text = new CText(locationx, locationy, text);
      FElementList.Add(Text);
      Redraw();
    }

    public void AddFrame(Int32 locationx, Int32 locationy, Int32 width, Int32 height)
    {
      CFrame Frame = new CFrame(locationx, locationy, width, height);
      FElementList.Add(Frame);
      Redraw();
    }

  }
}
