﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Net;
//
using UCNotifier;
using Task;
using UdpTransfer;
using UdpXmlTransfer;
using NetworkHardwareDevice;
using NhdMultiTemperatureController;
using UCCommon;
//
namespace UCMultiTemperatureControllerClient
{
  public partial class CUCMultiTemperatureControllerClient : UserControl
  { //
    //-----------------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CDeviceParameterList FDevices;
    private CCommand FCommand;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------------------
    //
    public CUCMultiTemperatureControllerClient()
    {
      InitializeComponent();
      // Channel0 - Preset
      FUCChannel0.SetChannelIndex(0);
      FUCChannel0.SetOnGetDataTemperature(UCChannelOnGetDataTemperature);
      FUCChannel0.SetOnSetDataTemperature(UCChannelOnSetDataTemperature);
      FUCChannel0.SetOnStartMeasurement(UCChannelOnStartMeasurement);
      FUCChannel0.SetOnStopMeasurement(UCChannelOnStopMeasurement);
      FUCChannel0.TimePeriod = 12.0;
      FUCChannel0.TimeDelta = 0.1;
      // Channel1 - Preset
      FUCChannel1.SetChannelIndex(1);
      FUCChannel1.SetOnGetDataTemperature(UCChannelOnGetDataTemperature);
      FUCChannel1.SetOnSetDataTemperature(UCChannelOnSetDataTemperature);
      FUCChannel1.SetOnStartMeasurement(UCChannelOnStartMeasurement);
      FUCChannel1.SetOnStopMeasurement(UCChannelOnStopMeasurement);
      FUCChannel1.VariationKind = EVariationKind.Linear;
      // Channel2 - Preset
      FUCChannel2.SetChannelIndex(2);
      FUCChannel2.SetOnGetDataTemperature(UCChannelOnGetDataTemperature);
      FUCChannel2.SetOnSetDataTemperature(UCChannelOnSetDataTemperature);
      FUCChannel2.SetOnStartMeasurement(UCChannelOnStartMeasurement);
      FUCChannel2.SetOnStopMeasurement(UCChannelOnStopMeasurement);
      FUCChannel2.VariationKind = EVariationKind.Sinus;
      // Channel3 - Preset
      FUCChannel3.SetChannelIndex(3);
      FUCChannel3.SetOnGetDataTemperature(UCChannelOnGetDataTemperature);
      FUCChannel3.SetOnSetDataTemperature(UCChannelOnSetDataTemperature);
      FUCChannel3.SetOnStartMeasurement(UCChannelOnStartMeasurement);
      FUCChannel3.SetOnStopMeasurement(UCChannelOnStopMeasurement);
      FUCChannel3.TemperatureMean = 100.0;
      FUCChannel3.TemperatureDelta = 50.0;
      FUCChannel3.TimePeriod = 10.0;
      FUCChannel3.TimeDelta = 0.1;
      FUCChannel3.VariationKind = EVariationKind.Sinus;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

    private delegate void CBSetNetworkHardwareDevicesMTC(CDeviceParameterList devices);
    public void SetNetworkHardwareDevicesMTC(CDeviceParameterList devices)
    {
      if (this.InvokeRequired)
      {
        CBSetNetworkHardwareDevicesMTC CB = new CBSetNetworkHardwareDevicesMTC(SetNetworkHardwareDevicesMTC);
        Invoke(CB, new object[] { devices });
      }
      else
      {
        cbxDeviceSelected.Items.Clear();
        FDevices = devices;
        if (FDevices is CDeviceParameterList)
        {
          if (0 < FDevices.Count)
          {
            for (Int32 DI = 0; DI < FDevices.Count; DI++)
            {
              CDeviceParameter DP = FDevices[DI];
              String Line = String.Format("Type[{0}] Name[{1}] IPAddress[{2}] ID[{3}]",
                                          DP.Type, DP.Name, DP.IPA, DP.ID);
              cbxDeviceSelected.Items.Add(Line);
            }
            cbxDeviceSelected.SelectedIndex = 0;
          }
        }
      }
    }

    private CDeviceParameter GetDeviceSelected()
    {
      if (FDevices is CDeviceParameterList)
      {
        if (0 < FDevices.Count)
        {
          if ((0 <= cbxDeviceSelected.SelectedIndex) &&
              (cbxDeviceSelected.SelectedIndex < cbxDeviceSelected.Items.Count))
          {
            return FDevices[cbxDeviceSelected.SelectedIndex];
          }
        }
      }
      return null;
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Get/SetData
    //------------------------------------------------------------------------------------
    //
    public Boolean GetDataTemperature(Int32 channel, out RDataTemperatureClient datatemperature)
    {
      datatemperature = new RDataTemperatureClient(0);
      switch (channel)
      {
        case 0:
          return FUCChannel0.GetDataTemperature(out datatemperature);
        case 1:
          return FUCChannel1.GetDataTemperature(out datatemperature);
        case 2:
          return FUCChannel2.GetDataTemperature(out datatemperature);
        case 3:
          return FUCChannel3.GetDataTemperature(out datatemperature);
      }
      return false;
    }
    public Boolean SetDataTemperature(Int32 channel, RDataTemperatureClient datatemperature)
    {
      switch (channel)
      {
        case 0:
          return FUCChannel0.SetDataTemperature(datatemperature);
        case 1:
          return FUCChannel1.SetDataTemperature(datatemperature);
        case 2:
          return FUCChannel2.SetDataTemperature(datatemperature);
        case 3:
          return FUCChannel3.SetDataTemperature(datatemperature);
      }
      return false;
    }

    //
    //-----------------------------------------------------------------------------
    //  Segment - Callback - ...
    //-----------------------------------------------------------------------------
    //
    private void UCChannelOnGetDataTemperature(Int32 channelindex)
    {
      if (!(FCommand is CCommand))
      {
        CDeviceParameter DP = GetDeviceSelected();
        if (DP is CDeviceParameter)
        {
          FCommand = new CCommand();
          FCommand.SetNotifier(FNotifier);
          // Command Header
          FCommand.TxdCommand.Type = CUdpXmlMtcHeader.XML_GETDATA;
          IPAddress IPA;
          CUdpBase.FindIPAddressLocal(out IPA);
          FCommand.TxdCommand.IPAddressSource = IPA;
          FCommand.TxdCommand.IPAddressTarget = DP.IPA;
          FCommand.TxdCommand.IPPortFrom = CUdpXmlMtcHeader.IPPORT_COMMAND_FROM;
          FCommand.TxdCommand.IPPortBack = CUdpXmlMtcHeader.IPPORT_COMMAND_BACK;
          // Command Parameter
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_CHANNEL, channelindex.ToString());
          // execute Command
          FCommand.SetOnDataResponseReceived(CommandGetDataOnDataResponseReceived);
          FCommand.Execute("TCGD",
                           CommandOnExecutionStart, CommandOnExecutionBusy,
                           CommandOnExecutionEnd, CommandOnExecutionAbort);
        }
      }
    }
    private void UCChannelOnSetDataTemperature(Int32 channelindex, RDataTemperatureClient data)
    {
      if (!(FCommand is CCommand))
      {
        CDeviceParameter DP = GetDeviceSelected();
        if (DP is CDeviceParameter)
        {
          FCommand = new CCommand();
          FCommand.SetNotifier(FNotifier);
          // Command Header
          FCommand.TxdCommand.Type = CUdpXmlMtcHeader.XML_SETDATA;
          IPAddress IPA;
          CUdpBase.FindIPAddressLocal(out IPA);
          FCommand.TxdCommand.IPAddressSource = IPA;
          FCommand.TxdCommand.IPAddressTarget = DP.IPA;
          FCommand.TxdCommand.IPPortFrom = CUdpXmlMtcHeader.IPPORT_COMMAND_FROM;
          FCommand.TxdCommand.IPPortBack = CUdpXmlMtcHeader.IPPORT_COMMAND_BACK;
          // Command Parameter
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_CHANNEL, channelindex.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TEMPERATUREMEAN, data.TemperatureMean.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TEMPERATUREDELTA, data.TemperatureDelta.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TEMPERATUREACTUAL, data.TemperatureActual.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TIMEPERIOD, data.TimePeriod.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TIMEDELTA, data.TimeDelta.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TIMEACTUAL, data.TimeActual.ToString());
          String SVK = CVariationKind.VariationKindToText(data.VariationKind);
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_VARIATIONKIND, SVK);
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_STEPPRESET, data.StepPreset.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_STEPACTUAL, data.StepActual.ToString());
          // Execute Command
          FCommand.Execute("CST",
                           CommandOnExecutionStart, CommandOnExecutionBusy,
                           CommandOnExecutionEnd, CommandOnExecutionAbort);
        }
      }
    }
    private void UCChannelOnStartMeasurement(Int32 channelindex)
    {
      if (!(FCommand is CCommand))
      {
        CDeviceParameter DP = GetDeviceSelected();
        if (DP is CDeviceParameter)
        {
          FCommand = new CCommand();
          FCommand.SetNotifier(FNotifier);
          // Command Header
          FCommand.TxdCommand.Type = CUdpXmlMtcHeader.XML_START;
          IPAddress IPA;
          CUdpBase.FindIPAddressLocal(out IPA);
          FCommand.TxdCommand.IPAddressSource = IPA;
          FCommand.TxdCommand.IPAddressTarget = DP.IPA;
          FCommand.TxdCommand.IPPortFrom = CUdpXmlMtcHeader.IPPORT_COMMAND_FROM;
          FCommand.TxdCommand.IPPortBack = CUdpXmlMtcHeader.IPPORT_COMMAND_BACK;
          // Command Parameter
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_CHANNEL, channelindex.ToString());
          // Execute Command
          FCommand.Execute("MTCCStartMeasurement",
                           CommandOnExecutionStart, CommandOnExecutionBusy,
                           CommandOnExecutionEnd, CommandOnExecutionAbort);
        }
      }
    }
    private void UCChannelOnStopMeasurement(Int32 channelindex)
    {
      if (!(FCommand is CCommand))
      {
        CDeviceParameter DP = GetDeviceSelected();
        if (DP is CDeviceParameter)
        {
          FCommand = new CCommand();
          FCommand.SetNotifier(FNotifier);
          // Command Header
          FCommand.TxdCommand.Type = CUdpXmlMtcHeader.XML_STOP;
          IPAddress IPA;
          CUdpBase.FindIPAddressLocal(out IPA);
          FCommand.TxdCommand.IPAddressSource = IPA;
          FCommand.TxdCommand.IPAddressTarget = DP.IPA;
          FCommand.TxdCommand.IPPortFrom = CUdpXmlMtcHeader.IPPORT_COMMAND_FROM;
          FCommand.TxdCommand.IPPortBack = CUdpXmlMtcHeader.IPPORT_COMMAND_BACK;
          // Command Parameter
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_CHANNEL, channelindex.ToString());
          // Execute Command
          FCommand.Execute("MTCCStopMeasurement",
                           CommandOnExecutionStart, CommandOnExecutionBusy,
                           CommandOnExecutionEnd, CommandOnExecutionAbort);
        }
      }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Callback - Command
    //-----------------------------------------------------------------------------
    //
    private void CommandOnExecutionStart(RTaskData data)
    {
    }
    private Boolean CommandOnExecutionBusy(ref RTaskData data)
    {
      return true;
    }
    private void CommandOnExecutionEnd(RTaskData data)
    {
      if (FCommand is CCommand)
      {
        FCommand.Abort();
        FCommand = null;
      }
    }
    private void CommandOnExecutionAbort(RTaskData data)
    {
      if (FCommand is CCommand)
      {
        FCommand.Abort();
        FCommand = null;
      }
    }

    private delegate void CBCommandGetDataOnDataResponseReceived(CDataResponse data);
    private void CommandGetDataOnDataResponseReceived(CDataResponse data)
    {
      if (this.InvokeRequired)
      {
        CBCommandGetDataOnDataResponseReceived CB =
          new CBCommandGetDataOnDataResponseReceived(CommandGetDataOnDataResponseReceived);
        Invoke(CB, new object[] { data });
      }
      else
      {
        Int32 Channel;
        RDataTemperatureClient DTC;
        switch (data.Type)
        {
          case CUdpXmlMtcHeader.XML_GETDATA:
            Channel = Int32.Parse(data.ParameterList[0].Value);
            if (GetDataTemperature(Channel, out DTC))
            {
              DTC.TemperatureMean = Double.Parse(data.ParameterList[1].Value,
                                          System.Globalization.NumberStyles.AllowDecimalPoint,
                                          System.Globalization.NumberFormatInfo.InvariantInfo);
              DTC.TemperatureDelta = Double.Parse(data.ParameterList[2].Value,
                                          System.Globalization.NumberStyles.AllowDecimalPoint,
                                          System.Globalization.NumberFormatInfo.InvariantInfo);
              DTC.TemperatureActual = Double.Parse(data.ParameterList[3].Value,
                                          System.Globalization.NumberStyles.AllowDecimalPoint,
                                          System.Globalization.NumberFormatInfo.InvariantInfo);
              DTC.TimePeriod = Double.Parse(data.ParameterList[4].Value,
                                          System.Globalization.NumberStyles.AllowDecimalPoint,
                                          System.Globalization.NumberFormatInfo.InvariantInfo);
              DTC.TimeDelta = Double.Parse(data.ParameterList[5].Value,
                                          System.Globalization.NumberStyles.AllowDecimalPoint,
                                          System.Globalization.NumberFormatInfo.InvariantInfo);
              DTC.TimeActual = Double.Parse(data.ParameterList[6].Value,
                                          System.Globalization.NumberStyles.AllowDecimalPoint,
                                          System.Globalization.NumberFormatInfo.InvariantInfo);
              String SS = data.ParameterList[7].Value;
              DTC.VariationKind = CVariationKind.TextToVariationKind(SS);
              DTC.StepPreset = Int32.Parse(data.ParameterList[8].Value,
                                          System.Globalization.NumberStyles.AllowDecimalPoint,
                                          System.Globalization.NumberFormatInfo.InvariantInfo);
              DTC.StepActual = Int32.Parse(data.ParameterList[9].Value,
                                          System.Globalization.NumberStyles.AllowDecimalPoint,
                                          System.Globalization.NumberFormatInfo.InvariantInfo);
              SetDataTemperature(Channel, DTC);
              DTC.Info(FNotifier, "### SetData");
            }
            break;
        }
      }
    }


  }
}
