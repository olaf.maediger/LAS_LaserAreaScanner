﻿namespace UCMultiTemperatureControllerClient
{
  partial class CUCTemperatureClient
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.FUCTimeActual = new UCCommon.CUCTime();
      this.FUCTemperatureActual = new UCCommon.CUCTemperature();
      this.FUCTimeDelta = new UCCommon.CUCTime();
      this.FUCTimePeriod = new UCCommon.CUCTime();
      this.FUCVariationKind = new UCCommon.CUCVariationKind();
      this.FUCTemperatureDelta = new UCCommon.CUCTemperature();
      this.FUCTemperatureMean = new UCCommon.CUCTemperature();
      this.FUCStepPreset = new UCCommon.CUCStep();
      this.FUCStepActual = new UCCommon.CUCStep();
      this.btnGetData = new System.Windows.Forms.Button();
      this.btnSetData = new System.Windows.Forms.Button();
      this.btnStartStop = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.Color.PapayaWhip;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(158, 13);
      this.lblHeader.TabIndex = 15;
      this.lblHeader.Text = "<header>";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FUCTimeActual
      // 
      this.FUCTimeActual.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTimeActual.HeaderText = "TimeActual";
      this.FUCTimeActual.HeaderWidth = 74;
      this.FUCTimeActual.Location = new System.Drawing.Point(3, 18);
      this.FUCTimeActual.Name = "FUCTimeActual";
      this.FUCTimeActual.Size = new System.Drawing.Size(152, 27);
      this.FUCTimeActual.TabIndex = 22;
      this.FUCTimeActual.TopBorderHeight = 5;
      this.FUCTimeActual.Unit = "s";
      this.FUCTimeActual.Value = 0D;
      // 
      // FUCTemperatureActual
      // 
      this.FUCTemperatureActual.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTemperatureActual.HeaderText = "Tactual";
      this.FUCTemperatureActual.HeaderWidth = 74;
      this.FUCTemperatureActual.Location = new System.Drawing.Point(3, 48);
      this.FUCTemperatureActual.Name = "FUCTemperatureActual";
      this.FUCTemperatureActual.Size = new System.Drawing.Size(152, 27);
      this.FUCTemperatureActual.TabIndex = 21;
      this.FUCTemperatureActual.TopBorderHeight = 5;
      this.FUCTemperatureActual.Unit = "°C";
      this.FUCTemperatureActual.Value = 20D;
      // 
      // FUCTimeDelta
      // 
      this.FUCTimeDelta.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTimeDelta.HeaderText = "TimeDelta";
      this.FUCTimeDelta.HeaderWidth = 74;
      this.FUCTimeDelta.Location = new System.Drawing.Point(3, 204);
      this.FUCTimeDelta.Name = "FUCTimeDelta";
      this.FUCTimeDelta.Size = new System.Drawing.Size(152, 27);
      this.FUCTimeDelta.TabIndex = 20;
      this.FUCTimeDelta.TopBorderHeight = 5;
      this.FUCTimeDelta.Unit = "s";
      this.FUCTimeDelta.Value = 1D;
      // 
      // FUCTimePeriod
      // 
      this.FUCTimePeriod.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTimePeriod.HeaderText = "TimePeriod";
      this.FUCTimePeriod.HeaderWidth = 74;
      this.FUCTimePeriod.Location = new System.Drawing.Point(3, 174);
      this.FUCTimePeriod.Name = "FUCTimePeriod";
      this.FUCTimePeriod.Size = new System.Drawing.Size(152, 27);
      this.FUCTimePeriod.TabIndex = 19;
      this.FUCTimePeriod.TopBorderHeight = 5;
      this.FUCTimePeriod.Unit = "s";
      this.FUCTimePeriod.Value = 60D;
      // 
      // FUCVariationKind
      // 
      this.FUCVariationKind.BackColor = System.Drawing.SystemColors.Info;
      this.FUCVariationKind.HeaderText = "Variation";
      this.FUCVariationKind.HeaderWidth = 74;
      this.FUCVariationKind.Index = 0;
      this.FUCVariationKind.Location = new System.Drawing.Point(3, 237);
      this.FUCVariationKind.Name = "FUCVariationKind";
      this.FUCVariationKind.Size = new System.Drawing.Size(152, 27);
      this.FUCVariationKind.TabIndex = 18;
      this.FUCVariationKind.TopBorderHeight = 3;
      this.FUCVariationKind.Unit = "";
      this.FUCVariationKind.Value = UCCommon.EVariationKind.Constant;
      // 
      // FUCTemperatureDelta
      // 
      this.FUCTemperatureDelta.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTemperatureDelta.HeaderText = "Tdelta";
      this.FUCTemperatureDelta.HeaderWidth = 74;
      this.FUCTemperatureDelta.Location = new System.Drawing.Point(3, 140);
      this.FUCTemperatureDelta.Name = "FUCTemperatureDelta";
      this.FUCTemperatureDelta.Size = new System.Drawing.Size(152, 27);
      this.FUCTemperatureDelta.TabIndex = 17;
      this.FUCTemperatureDelta.TopBorderHeight = 5;
      this.FUCTemperatureDelta.Unit = "°C";
      this.FUCTemperatureDelta.Value = 10D;
      // 
      // FUCTemperatureMean
      // 
      this.FUCTemperatureMean.BackColor = System.Drawing.SystemColors.Info;
      this.FUCTemperatureMean.HeaderText = "Tmean";
      this.FUCTemperatureMean.HeaderWidth = 74;
      this.FUCTemperatureMean.Location = new System.Drawing.Point(3, 110);
      this.FUCTemperatureMean.Name = "FUCTemperatureMean";
      this.FUCTemperatureMean.Size = new System.Drawing.Size(152, 27);
      this.FUCTemperatureMean.TabIndex = 16;
      this.FUCTemperatureMean.TopBorderHeight = 5;
      this.FUCTemperatureMean.Unit = "°C";
      this.FUCTemperatureMean.Value = 20D;
      // 
      // FUCStepPreset
      // 
      this.FUCStepPreset.BackColor = System.Drawing.SystemColors.Info;
      this.FUCStepPreset.HeaderText = "StepPreset";
      this.FUCStepPreset.HeaderWidth = 74;
      this.FUCStepPreset.Location = new System.Drawing.Point(3, 270);
      this.FUCStepPreset.Name = "FUCStepPreset";
      this.FUCStepPreset.Size = new System.Drawing.Size(152, 26);
      this.FUCStepPreset.TabIndex = 25;
      this.FUCStepPreset.TopBorderHeight = 5;
      this.FUCStepPreset.Unit = "";
      this.FUCStepPreset.Value = 0D;
      // 
      // FUCStepActual
      // 
      this.FUCStepActual.BackColor = System.Drawing.SystemColors.Info;
      this.FUCStepActual.HeaderText = "StepActual";
      this.FUCStepActual.HeaderWidth = 74;
      this.FUCStepActual.Location = new System.Drawing.Point(3, 78);
      this.FUCStepActual.Name = "FUCStepActual";
      this.FUCStepActual.Size = new System.Drawing.Size(152, 26);
      this.FUCStepActual.TabIndex = 26;
      this.FUCStepActual.TopBorderHeight = 5;
      this.FUCStepActual.Unit = "";
      this.FUCStepActual.Value = 0D;
      // 
      // btnGetData
      // 
      this.btnGetData.Location = new System.Drawing.Point(3, 302);
      this.btnGetData.Name = "btnGetData";
      this.btnGetData.Size = new System.Drawing.Size(49, 23);
      this.btnGetData.TabIndex = 27;
      this.btnGetData.Text = "Get";
      this.btnGetData.UseVisualStyleBackColor = true;
      this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
      // 
      // btnSetData
      // 
      this.btnSetData.Location = new System.Drawing.Point(54, 302);
      this.btnSetData.Name = "btnSetData";
      this.btnSetData.Size = new System.Drawing.Size(49, 23);
      this.btnSetData.TabIndex = 28;
      this.btnSetData.Text = "Set";
      this.btnSetData.UseVisualStyleBackColor = true;
      this.btnSetData.Click += new System.EventHandler(this.btnSetData_Click);
      // 
      // btnStartStop
      // 
      this.btnStartStop.Location = new System.Drawing.Point(105, 302);
      this.btnStartStop.Name = "btnStartStop";
      this.btnStartStop.Size = new System.Drawing.Size(49, 23);
      this.btnStartStop.TabIndex = 29;
      this.btnStartStop.Text = "Start";
      this.btnStartStop.UseVisualStyleBackColor = true;
      this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
      // 
      // CUCTemperatureClient
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnStartStop);
      this.Controls.Add(this.btnSetData);
      this.Controls.Add(this.btnGetData);
      this.Controls.Add(this.FUCStepActual);
      this.Controls.Add(this.FUCStepPreset);
      this.Controls.Add(this.FUCTimeActual);
      this.Controls.Add(this.FUCTemperatureActual);
      this.Controls.Add(this.FUCTimeDelta);
      this.Controls.Add(this.FUCTimePeriod);
      this.Controls.Add(this.FUCVariationKind);
      this.Controls.Add(this.FUCTemperatureDelta);
      this.Controls.Add(this.FUCTemperatureMean);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCTemperatureClient";
      this.Size = new System.Drawing.Size(158, 330);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private UCCommon.CUCTemperature FUCTemperatureMean;
    private UCCommon.CUCTemperature FUCTemperatureDelta;
    private UCCommon.CUCVariationKind FUCVariationKind;
    private UCCommon.CUCTime FUCTimePeriod;
    private UCCommon.CUCTime FUCTimeDelta;
    private UCCommon.CUCTemperature FUCTemperatureActual;
    private UCCommon.CUCTime FUCTimeActual;
    private UCCommon.CUCStep FUCStepPreset;
    private UCCommon.CUCStep FUCStepActual;
    private System.Windows.Forms.Button btnGetData;
    private System.Windows.Forms.Button btnSetData;
    private System.Windows.Forms.Button btnStartStop;
  }
}
