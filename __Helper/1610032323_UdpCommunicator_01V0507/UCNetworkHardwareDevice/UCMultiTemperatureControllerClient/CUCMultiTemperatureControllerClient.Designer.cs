﻿namespace UCMultiTemperatureControllerClient
{
  partial class CUCMultiTemperatureControllerClient
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label2 = new System.Windows.Forms.Label();
      this.cbxDeviceSelected = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.pnlChannel0To4 = new System.Windows.Forms.Panel();
      this.FUCChannel3 = new UCMultiTemperatureControllerClient.CUCTemperatureClient();
      this.FUCChannel2 = new UCMultiTemperatureControllerClient.CUCTemperatureClient();
      this.FUCChannel1 = new UCMultiTemperatureControllerClient.CUCTemperatureClient();
      this.FUCChannel0 = new UCMultiTemperatureControllerClient.CUCTemperatureClient();
      this.pnlChannel0To4.SuspendLayout();
      this.SuspendLayout();
      // 
      // label2
      // 
      this.label2.BackColor = System.Drawing.Color.PapayaWhip;
      this.label2.Dock = System.Windows.Forms.DockStyle.Top;
      this.label2.Location = new System.Drawing.Point(0, 34);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(636, 13);
      this.label2.TabIndex = 17;
      this.label2.Text = "MultiTemperatureController - Client: Commands";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // cbxDeviceSelected
      // 
      this.cbxDeviceSelected.Dock = System.Windows.Forms.DockStyle.Top;
      this.cbxDeviceSelected.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxDeviceSelected.FormattingEnabled = true;
      this.cbxDeviceSelected.Location = new System.Drawing.Point(0, 13);
      this.cbxDeviceSelected.Name = "cbxDeviceSelected";
      this.cbxDeviceSelected.Size = new System.Drawing.Size(636, 21);
      this.cbxDeviceSelected.TabIndex = 16;
      // 
      // label1
      // 
      this.label1.BackColor = System.Drawing.Color.PapayaWhip;
      this.label1.Dock = System.Windows.Forms.DockStyle.Top;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(636, 13);
      this.label1.TabIndex = 15;
      this.label1.Text = "ClientMultiTemperatureController: Select Device";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnlChannel0To4
      // 
      this.pnlChannel0To4.Controls.Add(this.FUCChannel3);
      this.pnlChannel0To4.Controls.Add(this.FUCChannel2);
      this.pnlChannel0To4.Controls.Add(this.FUCChannel1);
      this.pnlChannel0To4.Controls.Add(this.FUCChannel0);
      this.pnlChannel0To4.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlChannel0To4.Location = new System.Drawing.Point(0, 47);
      this.pnlChannel0To4.Name = "pnlChannel0To4";
      this.pnlChannel0To4.Size = new System.Drawing.Size(636, 329);
      this.pnlChannel0To4.TabIndex = 18;
      // 
      // FUCChannel3
      // 
      this.FUCChannel3.Header = "Channel 3";
      this.FUCChannel3.Location = new System.Drawing.Point(474, 0);
      this.FUCChannel3.Name = "FUCChannel3";
      this.FUCChannel3.Size = new System.Drawing.Size(158, 329);
      this.FUCChannel3.TabIndex = 3;
      this.FUCChannel3.TemperatureActual = 20D;
      this.FUCChannel3.TemperatureDelta = 10D;
      this.FUCChannel3.TemperatureMean = 20D;
      this.FUCChannel3.TimeActual = 0D;
      this.FUCChannel3.TimeDelta = 1D;
      this.FUCChannel3.TimePeriod = 60D;
      this.FUCChannel3.VariationKind = UCCommon.EVariationKind.Constant;
      // 
      // FUCChannel2
      // 
      this.FUCChannel2.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCChannel2.Header = "Channel 2";
      this.FUCChannel2.Location = new System.Drawing.Point(316, 0);
      this.FUCChannel2.Name = "FUCChannel2";
      this.FUCChannel2.Size = new System.Drawing.Size(158, 329);
      this.FUCChannel2.TabIndex = 2;
      this.FUCChannel2.TemperatureActual = 20D;
      this.FUCChannel2.TemperatureDelta = 10D;
      this.FUCChannel2.TemperatureMean = 20D;
      this.FUCChannel2.TimeActual = 0D;
      this.FUCChannel2.TimeDelta = 1D;
      this.FUCChannel2.TimePeriod = 60D;
      this.FUCChannel2.VariationKind = UCCommon.EVariationKind.Constant;
      // 
      // FUCChannel1
      // 
      this.FUCChannel1.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCChannel1.Header = "Channel 1";
      this.FUCChannel1.Location = new System.Drawing.Point(158, 0);
      this.FUCChannel1.Name = "FUCChannel1";
      this.FUCChannel1.Size = new System.Drawing.Size(158, 329);
      this.FUCChannel1.TabIndex = 1;
      this.FUCChannel1.TemperatureActual = 20D;
      this.FUCChannel1.TemperatureDelta = 10D;
      this.FUCChannel1.TemperatureMean = 20D;
      this.FUCChannel1.TimeActual = 0D;
      this.FUCChannel1.TimeDelta = 1D;
      this.FUCChannel1.TimePeriod = 60D;
      this.FUCChannel1.VariationKind = UCCommon.EVariationKind.Constant;
      // 
      // FUCChannel0
      // 
      this.FUCChannel0.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCChannel0.Header = "Channel 0";
      this.FUCChannel0.Location = new System.Drawing.Point(0, 0);
      this.FUCChannel0.Name = "FUCChannel0";
      this.FUCChannel0.Size = new System.Drawing.Size(158, 329);
      this.FUCChannel0.TabIndex = 0;
      this.FUCChannel0.TemperatureActual = 20D;
      this.FUCChannel0.TemperatureDelta = 10D;
      this.FUCChannel0.TemperatureMean = 20D;
      this.FUCChannel0.TimeActual = 0D;
      this.FUCChannel0.TimeDelta = 1D;
      this.FUCChannel0.TimePeriod = 60D;
      this.FUCChannel0.VariationKind = UCCommon.EVariationKind.Constant;
      // 
      // CUCMultiTemperatureControllerClient
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Lavender;
      this.Controls.Add(this.pnlChannel0To4);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.cbxDeviceSelected);
      this.Controls.Add(this.label1);
      this.Name = "CUCMultiTemperatureControllerClient";
      this.Size = new System.Drawing.Size(636, 385);
      this.pnlChannel0To4.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.ComboBox cbxDeviceSelected;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Panel pnlChannel0To4;
    private CUCTemperatureClient FUCChannel3;
    private CUCTemperatureClient FUCChannel2;
    private CUCTemperatureClient FUCChannel1;
    private CUCTemperatureClient FUCChannel0;
  }
}
