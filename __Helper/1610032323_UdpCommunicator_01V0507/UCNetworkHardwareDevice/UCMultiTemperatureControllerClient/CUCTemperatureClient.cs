﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCCommon;
//
namespace UCMultiTemperatureControllerClient
{
  //
  public delegate void DOnGetDataTemperature(Int32 channelindex); // -> Command -> Response -> SetData(Client, here!)! // RDataTemperatureClient data);
  public delegate void DOnSetDataTemperature(Int32 channelindex, RDataTemperatureClient data); // -> Command -> SetData(Server, NOT here!)
  public delegate void DOnStartMeasurement(Int32 channelindex);
  public delegate void DOnStopMeasurement(Int32 channelindex);
  //
  public partial class CUCTemperatureClient : UserControl
  { //
    //------------------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------------------
    //
    private const String TEXT_START = "Start";
    private const String TEXT_STOP = "Stop";
    //
    //------------------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------------------
    //
    private Int32 FChannelIndex;
    private CTemperatureClient FTemperatureClient;
    private DOnGetDataTemperature FOnGetDataTemperature;
    private DOnSetDataTemperature FOnSetDataTemperature;
    private DOnStartMeasurement FOnStartMeasurement;
    private DOnStopMeasurement FOnStopMeasurement;
    //
    //------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------
    //
    public CUCTemperatureClient()
    {
      InitializeComponent();
      //
      FTemperatureClient = new CTemperatureClient();
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      DT.TemperatureMean = FUCTemperatureMean.Value;
      DT.TemperatureDelta = FUCTemperatureDelta.Value;
      DT.TemperatureActual = FUCTemperatureActual.Value;
      DT.TimePeriod = FUCTimePeriod.Value;
      DT.TimeDelta = FUCTimeDelta.Value;
      DT.TimeActual = FUCTimeActual.Value;
      FTemperatureClient.SetData(DT);
      //
      FUCTimePeriod.SetOnTimeChanged(UCTimePeriodOnTimeChanged);
      FUCTimeDelta.SetOnTimeChanged(UCTimeDeltaOnTimeChanged);
      FUCTimeActual.SetOnTimeChanged(UCTimeActualOnTimeChanged);
      //
      FUCTemperatureActual.SetOnTemperatureChanged(UCTemperatureActualOnTemperatureChanged);
      FUCTemperatureMean.SetOnTemperatureChanged(UCTemperatureMeanOnTemperatureChanged);
      FUCTemperatureDelta.SetOnTemperatureChanged(UCTemperatureDeltaOnTemperatureChanged);
      //
      FUCVariationKind.SetOnVariationKindChanged(UCVariationKindOnVariationKindChanged);
      //
      btnStartStop.Text = TEXT_START;
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------
    //
    public void SetChannelIndex(Int32 channelindex)
    {
      FChannelIndex = channelindex;
    }

    public void SetOnGetDataTemperature(DOnGetDataTemperature value)
    {
      FOnGetDataTemperature = value;
    }
    public void SetOnSetDataTemperature(DOnSetDataTemperature value)
    {
      FOnSetDataTemperature = value;
    }
    public void SetOnStartMeasurement(DOnStartMeasurement value)
    {
      FOnStartMeasurement = value;
    }
    public void SetOnStopMeasurement(DOnStopMeasurement value)
    {
      FOnStopMeasurement = value;
    }

    public String Header
    {
      get { return lblHeader.Text; }
      set { lblHeader.Text = value; }
    }
    //
    private Double GetTemperatureMean()
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      FUCTemperatureMean.Value = DT.TemperatureMean;
      return DT.TemperatureMean;
    }
    private void SetTemperatureMean(Double value)
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      DT.TemperatureMean = value;
      FUCTemperatureMean.Value = value;
      FTemperatureClient.SetData(DT);
    }
    public Double TemperatureMean
    {
      get { return GetTemperatureMean(); }
      set { SetTemperatureMean(value); }
    }
    //
    private Double GetTemperatureDelta()
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      FUCTemperatureDelta.Value = DT.TemperatureDelta; 
      return DT.TemperatureDelta;
    }
    private void SetTemperatureDelta(Double value)
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      DT.TemperatureDelta = value;
      FUCTemperatureDelta.Value = value;
      FTemperatureClient.SetData(DT);
    }
    public Double TemperatureDelta
    {
      get { return GetTemperatureDelta(); }
      set { SetTemperatureDelta(value); }
    }
    //
    private Double GetTemperatureActual()
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      FUCTemperatureActual.Value = DT.TemperatureActual;
      return DT.TemperatureActual;
    }
    private void SetTemperatureActual(Double value)
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      DT.TemperatureActual = value;
      FUCTemperatureActual.Value = value;
      FTemperatureClient.SetData(DT);
    }
    public Double TemperatureActual
    {
      get { return GetTemperatureActual(); }
      set { SetTemperatureActual(value); }
    }
    //
    private Double GetTimePeriod()
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      FUCTimePeriod.Value = DT.TimePeriod;
      return DT.TimePeriod;
    }
    private void SetTimePeriod(Double value)
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      DT.TimePeriod = value;
      FUCTimePeriod.Value = value;
      FTemperatureClient.SetData(DT);
    }
    public Double TimePeriod
    {
      get { return GetTimePeriod(); }
      set { SetTimePeriod(value); }
    }
    //
    private Double GetTimeDelta()
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      FUCTimeDelta.Value = DT.TimeDelta;
      return DT.TimeDelta;
    }
    private void SetTimeDelta(Double value)
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      DT.TimeDelta = value;
      FUCTimeDelta.Value = value;
      FTemperatureClient.SetData(DT);
    }
    public Double TimeDelta
    {
      get { return GetTimeDelta(); }
      set { SetTimeDelta(value); }
    }
    //
    private Double GetTimeActual()
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      FUCTimeActual.Value = DT.TimeActual;
      return DT.TimeActual;
    }
    private void SetTimeActual(Double value)
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      DT.TimeActual = value;
      FUCTimeActual.Value = value;
      FTemperatureClient.SetData(DT);
    }
    public Double TimeActual
    {
      get { return GetTimeActual(); }
      set { SetTimeActual(value); }
    }
    //
    private EVariationKind GetVariationKind()
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      FUCVariationKind.Value = DT.VariationKind;
      return DT.VariationKind;
    }
    private void SetVariationKind(EVariationKind value)
    {
      RDataTemperatureClient DT;
      FTemperatureClient.GetData(out DT);
      DT.VariationKind = value;
      FUCVariationKind.Value = value;
      FTemperatureClient.SetData(DT);
    }
    public EVariationKind VariationKind
    {
      get { return GetVariationKind(); }
      set { SetVariationKind(value); }
    }
    //
    //public Boolean IsOpen
    //{
    //  get { return FTemperatureClient.IsOpen; }
    //}
    //
    //------------------------------------------------------------------------------------
    //  Segment - Get/SetData
    //------------------------------------------------------------------------------------
    //
    public Boolean GetDataTemperature(out RDataTemperatureClient datatemperature)
    {
      FTemperatureClient.GetData(out datatemperature);
      return true;
    }
    public Boolean SetDataTemperature(RDataTemperatureClient datatemperature)
    {
      TemperatureMean = datatemperature.TemperatureMean;
      TemperatureDelta = datatemperature.TemperatureDelta;
      TemperatureActual = datatemperature.TemperatureActual;
      TimePeriod = datatemperature.TimePeriod;
      TimeDelta = datatemperature.TimeDelta;
      TimeActual = datatemperature.TimeActual;
      VariationKind = datatemperature.VariationKind;        
      //FTemperatureClient.SetData(datatemperature);
      return true;
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------------------
    //  Segment - Callback - UControls
    //------------------------------------------------------------------------------------
    //
    private void UCTimePeriodOnTimeChanged(Double time)
    {
      TimePeriod = time;
    }
    private void UCTimeDeltaOnTimeChanged(Double time)
    {
      TimeDelta = time;
    }
    private void UCTimeActualOnTimeChanged(Double time)
    {
      TimeActual = time;
    }
    //
    private void UCTemperatureActualOnTemperatureChanged(Double temperature)
    {
      TemperatureActual = temperature;
    }
    private void UCTemperatureMeanOnTemperatureChanged(Double temperature)
    {
      TemperatureMean = temperature;
    }
    private void UCTemperatureDeltaOnTemperatureChanged(Double temperature)
    {
      TemperatureDelta = temperature;
    }
    //
    private void UCVariationKindOnVariationKindChanged(EVariationKind variationkind)
    {
      VariationKind = variationkind;
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Callback - TemperatureClient
    //------------------------------------------------------------------------------------
    //
    private delegate void CBTemperatureClientOnDataTemperatureChanged(RDataTemperatureClient data);
    private void TemperatureClientOnDataTemperatureChanged(RDataTemperatureClient data)
    {
      if (this.InvokeRequired)
      {
        CBTemperatureClientOnDataTemperatureChanged CB =
          new CBTemperatureClientOnDataTemperatureChanged(TemperatureClientOnDataTemperatureChanged);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCTimeActual.Value = data.TimeActual;
        FUCTimeDelta.Value = data.TimeDelta;
        FUCTimePeriod.Value = data.TimePeriod;
        FUCTemperatureActual.Value = data.TemperatureActual;
        FUCTemperatureDelta.Value = data.TemperatureDelta;
        FUCTemperatureMean.Value = data.TemperatureMean;
      }
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Event
    //------------------------------------------------------------------------------------
    // GetData from Server!
    private void btnGetData_Click(object sender, EventArgs e)
    {
      if (FOnGetDataTemperature is DOnGetDataTemperature)
      {
         FOnGetDataTemperature(FChannelIndex);
      }
    }

    // SetData to Server!
    private void btnSetData_Click(object sender, EventArgs e)
    {
      if (FOnSetDataTemperature is DOnSetDataTemperature)
      {
        RDataTemperatureClient DTC;
        FTemperatureClient.GetData(out DTC);
        FOnSetDataTemperature(FChannelIndex, DTC);
      }
    }

    private void btnStartStop_Click(object sender, EventArgs e)
    {
      if (TEXT_START == btnStartStop.Text)
      { // Start...
        if (FOnStartMeasurement is DOnStartMeasurement)
        {
          FOnStartMeasurement(FChannelIndex);
        }
        btnStartStop.Text = TEXT_STOP;
      }
      else
      { // Stop...
        if (FOnStopMeasurement is DOnStopMeasurement)
        {
          FOnStopMeasurement(FChannelIndex);
        }
        btnStartStop.Text = TEXT_START;
      }
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Management
    //------------------------------------------------------------------------------------
    //

  }
}
