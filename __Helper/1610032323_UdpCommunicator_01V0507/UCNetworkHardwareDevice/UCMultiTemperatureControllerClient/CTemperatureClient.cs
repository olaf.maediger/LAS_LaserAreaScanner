﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using Task;
using UCNotifier;
using UCCommon;
//
namespace UCMultiTemperatureControllerClient
{
  public struct RDataTemperatureClient
  {
    public Double TemperatureMean;
    public Double TemperatureDelta;
    public Double TemperatureActual;
    public Double TimePeriod;
    public Double TimeDelta;
    public Double TimeActual;
    public EVariationKind VariationKind;
    public Int32 StepPreset;
    public Int32 StepActual;
    //
    public RDataTemperatureClient(Int32 init)
    {
      TemperatureMean = CTemperatureClient.INIT_TEMPARATUREMEAN;
      TemperatureDelta = CTemperatureClient.INIT_TEMPARATUREDELTA;
      TemperatureActual = CTemperatureClient.INIT_TEMPARATUREACTUAL;
      TimePeriod = CTemperatureClient.INIT_TIMEPERIOD;
      TimeDelta = CTemperatureClient.INIT_TIMEDELTA;
      TimeActual = CTemperatureClient.INIT_TIMEACTUAL;
      VariationKind = CTemperatureClient.INIT_KINDVARIATION;
      StepPreset = CTemperatureClient.INIT_STEPPRESET;
      StepActual = CTemperatureClient.INIT_STEPACTUAL;
    }
    public void Info(CNotifier notifier, String header)
    {
      String Line = String.Format("{0} - RDataTemperatureClient:", header);
      notifier.Write(Line);
      Line = String.Format("TemperatureMean [C]: {0}", TemperatureMean);
      notifier.Write(Line);
      Line = String.Format("TemperatureDelta [C]: {0}", TemperatureDelta);
      notifier.Write(Line);
      Line = String.Format("TemperatureActual [C]: {0}", TemperatureActual);
      notifier.Write(Line);
      Line = String.Format("TimePeriod [s]: {0}", TimePeriod);
      notifier.Write(Line);
      Line = String.Format("TimeDelta [s]: {0}", TimeDelta);
      notifier.Write(Line);
      Line = String.Format("TimeActual [s]: {0}", TimeActual);
      notifier.Write(Line);
      Line = CVariationKind.VariationKindToText(VariationKind);
      Line = String.Format("VariationKind: {0}", Line);
      notifier.Write(Line);
      Line = String.Format("StepPreset [1]: {0}", StepPreset);
      notifier.Write(Line);
      Line = String.Format("StepActual [1]: {0}", StepActual);
      notifier.Write(Line);
    }
  }

  public class CTemperatureClient
  { //
    //------------------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------------------
    //
    public const Double INIT_TEMPARATUREMEAN = 20.0; // [°C]
    public const Double INIT_TEMPARATUREDELTA = 10.0; // [°C]
    public const Double INIT_TEMPARATUREACTUAL = 19.0; // [°C]
    public const Double INIT_TIMEPERIOD = 10.0; // [sec]
    public const Double INIT_TIMEDELTA = 0.1; // [sec]
    public const Double INIT_TIMEACTUAL = 0.0; // [sec]
    public const EVariationKind INIT_KINDVARIATION = EVariationKind.Constant;
    public const Int32 INIT_STEPPRESET = 0; // [1] - 0 : infinite
    public const Int32 INIT_STEPACTUAL = 0; // [1] 
    //
    //------------------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------------------
    //
    private RDataTemperatureClient FData;
    //
    //------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------
    //
    public CTemperatureClient()
    {
      FData = new RDataTemperatureClient(0);
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------
    //
    public void SetData(RDataTemperatureClient data)
    {
      FData.TemperatureMean = data.TemperatureMean;
      FData.TemperatureDelta = data.TemperatureDelta;
      FData.TemperatureActual = data.TemperatureActual;
      FData.TimePeriod = data.TimePeriod;
      FData.TimeDelta = data.TimeDelta;
      FData.TimeActual = data.TimeActual;
      FData.VariationKind = data.VariationKind;
      FData.StepPreset = data.StepPreset;
      FData.StepActual = data.StepActual;
    }
    public void GetData(out RDataTemperatureClient data)
    {
      data = FData;
    }
    //
    //------------------------------------------------------------------------------------
    //  Segment - Callback -
    //------------------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------------------
    //  Segment - Management
    //------------------------------------------------------------------------------------
    //



  }
}
