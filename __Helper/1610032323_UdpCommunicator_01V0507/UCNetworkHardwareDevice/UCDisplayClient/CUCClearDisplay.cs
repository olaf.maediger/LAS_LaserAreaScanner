﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCDisplayClient
{
  public struct RDataClearDisplay
  {
    // no parameter!
  }

  public delegate void DOnClearDisplay(RDataClearDisplay data);

  public partial class CUCClearDisplay : UserControl
  {
    private DOnClearDisplay FOnClearDisplay;
    private RDataClearDisplay FData;

    public CUCClearDisplay()
    {
      InitializeComponent();
      FData = new RDataClearDisplay();
    }

    public void SetOnClearDisplay(DOnClearDisplay value)
    {
      FOnClearDisplay = value;
    }

    private void btnRun_Click(object sender, EventArgs e)
    {
      if (FOnClearDisplay is DOnClearDisplay)
      {
        FOnClearDisplay(FData);
      }
    }
  }
}
