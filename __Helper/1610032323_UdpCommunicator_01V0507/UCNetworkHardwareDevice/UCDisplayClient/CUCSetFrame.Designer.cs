﻿namespace UCDisplayClient
{
  partial class CUCSetFrame
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblTitle = new System.Windows.Forms.Label();
      this.btnRun = new System.Windows.Forms.Button();
      this.FUCY = new UCCommon.CUCLocation();
      this.FUCX = new UCCommon.CUCLocation();
      this.FUCDY = new UCCommon.CUCLocation();
      this.FUCDX = new UCCommon.CUCLocation();
      this.SuspendLayout();
      // 
      // lblTitle
      // 
      this.lblTitle.BackColor = System.Drawing.Color.PapayaWhip;
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(498, 13);
      this.lblTitle.TabIndex = 1;
      this.lblTitle.Text = "SetFrame";
      this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnRun
      // 
      this.btnRun.Location = new System.Drawing.Point(3, 17);
      this.btnRun.Name = "btnRun";
      this.btnRun.Size = new System.Drawing.Size(75, 23);
      this.btnRun.TabIndex = 3;
      this.btnRun.Text = "Run";
      this.btnRun.UseVisualStyleBackColor = true;
      this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
      // 
      // FUCY
      // 
      this.FUCY.BackColor = System.Drawing.SystemColors.Info;
      this.FUCY.HeaderText = "Y";
      this.FUCY.HeaderWidth = 24;
      this.FUCY.Location = new System.Drawing.Point(182, 14);
      this.FUCY.Name = "FUCY";
      this.FUCY.Size = new System.Drawing.Size(101, 26);
      this.FUCY.TabIndex = 6;
      this.FUCY.TopBorderHeight = 5;
      this.FUCY.UnitText = "pxl";
      this.FUCY.UnitWidth = 22;
      this.FUCY.Value = 0;
      // 
      // FUCX
      // 
      this.FUCX.BackColor = System.Drawing.SystemColors.Info;
      this.FUCX.HeaderText = "X";
      this.FUCX.HeaderWidth = 24;
      this.FUCX.Location = new System.Drawing.Point(78, 14);
      this.FUCX.Name = "FUCX";
      this.FUCX.Size = new System.Drawing.Size(101, 26);
      this.FUCX.TabIndex = 5;
      this.FUCX.TopBorderHeight = 5;
      this.FUCX.UnitText = "pxl";
      this.FUCX.UnitWidth = 22;
      this.FUCX.Value = 0;
      // 
      // FUCDY
      // 
      this.FUCDY.BackColor = System.Drawing.SystemColors.Info;
      this.FUCDY.HeaderText = "DY";
      this.FUCDY.HeaderWidth = 24;
      this.FUCDY.Location = new System.Drawing.Point(392, 14);
      this.FUCDY.Name = "FUCDY";
      this.FUCDY.Size = new System.Drawing.Size(101, 26);
      this.FUCDY.TabIndex = 8;
      this.FUCDY.TopBorderHeight = 5;
      this.FUCDY.UnitText = "pxl";
      this.FUCDY.UnitWidth = 22;
      this.FUCDY.Value = 40;
      // 
      // FUCDX
      // 
      this.FUCDX.BackColor = System.Drawing.SystemColors.Info;
      this.FUCDX.HeaderText = "DX";
      this.FUCDX.HeaderWidth = 24;
      this.FUCDX.Location = new System.Drawing.Point(288, 14);
      this.FUCDX.Name = "FUCDX";
      this.FUCDX.Size = new System.Drawing.Size(101, 26);
      this.FUCDX.TabIndex = 7;
      this.FUCDX.TopBorderHeight = 5;
      this.FUCDX.UnitText = "pxl";
      this.FUCDX.UnitWidth = 22;
      this.FUCDX.Value = 60;
      // 
      // CUCSetFrame
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.FUCDY);
      this.Controls.Add(this.FUCDX);
      this.Controls.Add(this.FUCY);
      this.Controls.Add(this.FUCX);
      this.Controls.Add(this.btnRun);
      this.Controls.Add(this.lblTitle);
      this.Name = "CUCSetFrame";
      this.Size = new System.Drawing.Size(498, 46);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.Button btnRun;
    private UCCommon.CUCLocation FUCY;
    private UCCommon.CUCLocation FUCX;
    private UCCommon.CUCLocation FUCDY;
    private UCCommon.CUCLocation FUCDX;
  }
}
