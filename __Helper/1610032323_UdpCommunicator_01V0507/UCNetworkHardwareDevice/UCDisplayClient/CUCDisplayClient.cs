﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Net;
//
using UCNotifier;
using Task;
using UdpTransfer;
using UdpXmlTransfer;
using NetworkHardwareDevice;
using NhdDisplay;
//
namespace UCDisplayClient
{
  public partial class CUCDisplayClient : UserControl
  { //
    //-----------------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private CDeviceParameterList FDevices;
    private CCommand FCommand;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------------------
    //
    public CUCDisplayClient()
    {
      InitializeComponent();
      //
      FUCClearDisplay.SetOnClearDisplay(UCClearDisplayOnClearDisplay);
      FUCSetText.SetOnSetText(UCSetTextOnSetText);
      FUCSetFrame.SetOnSetFrame(UCSetFrameOnSetFrame);
      //
      FUCSetText.SetX(12);
      FUCSetText.SetY(16);
      FUCSetText.SetText("Hello World!");
      //
      FUCSetFrame.SetX(3);
      FUCSetFrame.SetY(4);
      FUCSetFrame.SetDX(40);
      FUCSetFrame.SetDY(60);
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

    private delegate void CBSetNetworkHardwareDevicesDIS(CDeviceParameterList devices);
    public void SetNetworkHardwareDevicesDIS(CDeviceParameterList devices)
    {
      if (this.InvokeRequired)
      {
        CBSetNetworkHardwareDevicesDIS CB = new CBSetNetworkHardwareDevicesDIS(SetNetworkHardwareDevicesDIS);
        Invoke(CB, new object[] { devices });
      }
      else
      {
        cbxDeviceSelected.Items.Clear();
        FDevices = devices;
        if (FDevices is CDeviceParameterList)
        {
          if (0 < FDevices.Count)
          {
            for (Int32 DI = 0; DI < FDevices.Count; DI++)
            {
              CDeviceParameter DP = FDevices[DI];
              String Line = String.Format("Type[{0}] Name[{1}] IPAddress[{2}] ID[{3}]",
                                          DP.Type, DP.Name, DP.IPA, DP.ID);
              cbxDeviceSelected.Items.Add(Line);
            }
            cbxDeviceSelected.SelectedIndex = 0;
          }
        }
      }
    }

    private CDeviceParameter GetDeviceSelected()
    {
      if (FDevices is CDeviceParameterList)
      {
        if (0 < FDevices.Count)
        {
          if ((0 <= cbxDeviceSelected.SelectedIndex) &&
              (cbxDeviceSelected.SelectedIndex < cbxDeviceSelected.Items.Count))
          {
            return FDevices[cbxDeviceSelected.SelectedIndex];
          }
        }
      }
      return null;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Callback - Command
    //-----------------------------------------------------------------------------
    //
    private void CommandOnExecutionStart(RTaskData data)
    {
    }
    private Boolean CommandOnExecutionBusy(ref RTaskData data)
    {
      return true;
    }
    private void CommandOnExecutionEnd(RTaskData data)
    {
      if (FCommand is CCommand)
      {
        FCommand.Abort();
        FCommand = null;
      }
    }
    private void CommandOnExecutionAbort(RTaskData data)
    {
      if (FCommand is CCommand)
      {
        FCommand.Abort();
        FCommand = null;
      }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Callback - Command
    //-----------------------------------------------------------------------------
    //
    private void UCClearDisplayOnClearDisplay(RDataClearDisplay data)
    {
      if (!(FCommand is CCommand))
      {
        CDeviceParameter DP = GetDeviceSelected();
        if (DP is CDeviceParameter)
        {
          FCommand = new CCommand();
          FCommand.SetNotifier(FNotifier);
          FCommand.TxdCommand.Type = CUdpXmlDisHeader.XML_CLEARDISPLAY;
          IPAddress IPA;
          CUdpBase.FindIPAddressLocal(out IPA);
          FCommand.TxdCommand.IPAddressSource = IPA;
          FCommand.TxdCommand.IPAddressTarget = DP.IPA;
          FCommand.TxdCommand.IPPortFrom = CUdpXmlDisHeader.IPPORT_COMMAND_FROM;
          FCommand.TxdCommand.IPPortBack = CUdpXmlDisHeader.IPPORT_COMMAND_BACK;
          FCommand.Execute("CCD",
                           CommandOnExecutionStart, CommandOnExecutionBusy,
                           CommandOnExecutionEnd, CommandOnExecutionAbort);
        }
      }
    }

    private void UCSetTextOnSetText(RDataSetText data)
    {
      if (!(FCommand is CCommand))
      {
        CDeviceParameter DP = GetDeviceSelected();
        if (DP is CDeviceParameter)
        {
          FCommand = new CCommand();
          FCommand.SetNotifier(FNotifier);
          FCommand.TxdCommand.Type = CUdpXmlDisHeader.XML_SETTEXT;
          IPAddress IPA;
          CUdpBase.FindIPAddressLocal(out IPA);
          FCommand.TxdCommand.IPAddressSource = IPA;
          FCommand.TxdCommand.IPAddressTarget = DP.IPA;
          FCommand.TxdCommand.IPPortFrom = CUdpXmlDisHeader.IPPORT_COMMAND_FROM;
          FCommand.TxdCommand.IPPortBack = CUdpXmlDisHeader.IPPORT_COMMAND_BACK;
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlDisHeader.XML_LOCATIONX, data.LocationX.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlDisHeader.XML_LOCATIONY, data.LocationY.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlDisHeader.XML_TEXT, data.Text);
          FCommand.Execute("CST",
                           CommandOnExecutionStart, CommandOnExecutionBusy,
                           CommandOnExecutionEnd, CommandOnExecutionAbort);
        }
      }
    }

    private void UCSetFrameOnSetFrame(RDataSetFrame data)
    {
      if (!(FCommand is CCommand))
      {
        CDeviceParameter DP = GetDeviceSelected();
        if (DP is CDeviceParameter)
        {
          FCommand = new CCommand();
          FCommand.SetNotifier(FNotifier);
          FCommand.TxdCommand.Type = CUdpXmlDisHeader.XML_SETFRAME;
          IPAddress IPA;
          CUdpBase.FindIPAddressLocal(out IPA);
          FCommand.TxdCommand.IPAddressSource = IPA;
          FCommand.TxdCommand.IPAddressTarget = DP.IPA;
          FCommand.TxdCommand.IPPortFrom = CUdpXmlDisHeader.IPPORT_COMMAND_FROM;
          FCommand.TxdCommand.IPPortBack = CUdpXmlDisHeader.IPPORT_COMMAND_BACK;
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlDisHeader.XML_X, data.X.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlDisHeader.XML_Y, data.Y.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlDisHeader.XML_DX, data.DX.ToString());
          FCommand.TxdCommand.ParameterList.Add(CUdpXmlDisHeader.XML_DY, data.DY.ToString());
          FCommand.Execute("CSF",
                           CommandOnExecutionStart, CommandOnExecutionBusy,
                           CommandOnExecutionEnd, CommandOnExecutionAbort);
        }
      }
    }



    // FCommand = new CCommand();
    // FCommand.SetNotifier(FUCNotifier);
    // //
    // FCommand.TxdCommand.Type = CUdpXmlDisHeader.XML_SETTEXT;
    // FCommand.TxdCommand.IPAddressSource = FDeviceParameter.IPAddress; // ERROR!!! later correct TargetAddress from Display!!!!
    // FCommand.TxdCommand.IPAddressTarget = FDeviceParameter.IPAddress; // ERROR!!! later correct TargetAddress from Display!!!!
    // FCommand.TxdCommand.IPPortFrom = CUdpXmlHeader.IPPORT_REQUEST_FROM;
    // FCommand.TxdCommand.IPPortBack = CUdpXmlHeader.IPPORT_REQUEST_BACK;
    // CParameterList ParameterList = new CParameterList();
    // ParameterList.Add("LocationX", "12");
    // ParameterList.Add("LocationY", "16");
    // ParameterList.Add("Text", "Hello Display!");
    ////////////////////////////////////////!!!!!!!!!!!!!!!!!!!!!!!!! FCommand.TxdCommand ("SetText", ParameterList);
    // FCommand.Execute("CST",
    //                   CommandOnExecutionStart,
    //                   CommandOnExecutionBusy,
    //                   CommandOnExecutionEnd,
    //                   CommandOnExecutionAbort);






    //
    //-----------------------------------------------------------------------------
    //  Segment - 
    //-----------------------------------------------------------------------------
    //

  }
}
