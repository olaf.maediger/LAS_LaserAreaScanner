﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCDisplayClient
{
  public struct RDataSetFrame
  {
    public Int32 X;
    public Int32 Y;
    public Int32 DX;
    public Int32 DY;
  }

  public delegate void DOnSetFrame(RDataSetFrame data);

  public partial class CUCSetFrame : UserControl
  {
    private DOnSetFrame FOnSetFrame;
    private RDataSetFrame FData;

    public CUCSetFrame()
    {
      InitializeComponent();
    }

    public void SetOnSetFrame(DOnSetFrame value)
    {
      FOnSetFrame = value;
    }


    public Int32 GetX()
    {
      return FUCX.Value;
    }
    public void SetX(Int32 value)
    {
      FUCX.Value = value;
    }

    public Int32 GetY()
    {
      return FUCY.Value;
    }
    public void SetY(Int32 value)
    {
      FUCY.Value = value;
    }

    public Int32 GetDX()
    {
      return FUCDX.Value;
    }
    public void SetDX(Int32 value)
    {
      FUCDX.Value = value;
    }

    public Int32 GetDY()
    {
      return FUCDY.Value;
    }
    public void SetDY(Int32 value)
    {
      FUCDY.Value = value;
    }



    private void btnRun_Click(object sender, EventArgs e)
    {
      if (FOnSetFrame is DOnSetFrame)
      {
        FData.X = GetX();
        FData.Y = GetY();
        FData.DX = GetDX();
        FData.DY = GetDY();
        FOnSetFrame(FData);
      }
    }

  }
}


