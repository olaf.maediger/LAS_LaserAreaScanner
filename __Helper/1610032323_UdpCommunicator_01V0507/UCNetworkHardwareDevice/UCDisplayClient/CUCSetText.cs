﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCDisplayClient
{
  public struct RDataSetText
  {
    public Int32 LocationX;
    public Int32 LocationY;
    public String Text;
  }

  public delegate void DOnSetText(RDataSetText data);

  public partial class CUCSetText : UserControl
  {
    private DOnSetText FOnSetText;
    private RDataSetText FData;

    public CUCSetText()
    {
      InitializeComponent();
    }

    public void SetOnSetText(DOnSetText value)
    {
      FOnSetText = value;
    }

    public Int32 GetX()
    {
      return FUCLocationX.Value;
    }
    public void SetX(Int32 value)
    {
      FUCLocationX.Value = value;
    }

    public Int32 GetY()
    {
      return FUCLocationY.Value;
    }
    public void SetY(Int32 value)
    {
      FUCLocationY.Value = value;
    }

    public String GetText()
    {
      if (null == FUCText.Value)
      {
        return "";
      }
      return FUCText.Value;
    }
    public void SetText(String value)
    {
      FUCText.Value = value;
    }

    private void btnRun_Click(object sender, EventArgs e)
    {
      if (FOnSetText is DOnSetText)
      {
        FData.LocationX = GetX();
        FData.LocationY = GetY();
        FData.Text = GetText();
        FOnSetText(FData);
      }
    }
  }
}

