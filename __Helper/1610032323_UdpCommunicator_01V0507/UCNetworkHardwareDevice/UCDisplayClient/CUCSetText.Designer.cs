﻿namespace UCDisplayClient
{
  partial class CUCSetText
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblTitle = new System.Windows.Forms.Label();
      this.btnRun = new System.Windows.Forms.Button();
      this.FUCText = new UCCommon.CUCText();
      this.FUCLocationY = new UCCommon.CUCLocation();
      this.FUCLocationX = new UCCommon.CUCLocation();
      this.SuspendLayout();
      // 
      // lblTitle
      // 
      this.lblTitle.BackColor = System.Drawing.Color.PapayaWhip;
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(539, 13);
      this.lblTitle.TabIndex = 1;
      this.lblTitle.Text = "SetText";
      this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnRun
      // 
      this.btnRun.Location = new System.Drawing.Point(3, 17);
      this.btnRun.Name = "btnRun";
      this.btnRun.Size = new System.Drawing.Size(75, 23);
      this.btnRun.TabIndex = 2;
      this.btnRun.Text = "Run";
      this.btnRun.UseVisualStyleBackColor = true;
      this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
      // 
      // FUCText
      // 
      this.FUCText.HeaderText = "Text";
      this.FUCText.HeaderWidth = 36;
      this.FUCText.Location = new System.Drawing.Point(286, 14);
      this.FUCText.Name = "FUCText";
      this.FUCText.Size = new System.Drawing.Size(243, 27);
      this.FUCText.TabIndex = 5;
      this.FUCText.TopBorderHeight = 5;
      this.FUCText.UnitText = "";
      this.FUCText.UnitWidth = 8;
      this.FUCText.Value = "";
      // 
      // FUCLocationY
      // 
      this.FUCLocationY.BackColor = System.Drawing.SystemColors.Info;
      this.FUCLocationY.HeaderText = "Y";
      this.FUCLocationY.HeaderWidth = 24;
      this.FUCLocationY.Location = new System.Drawing.Point(182, 14);
      this.FUCLocationY.Name = "FUCLocationY";
      this.FUCLocationY.Size = new System.Drawing.Size(101, 26);
      this.FUCLocationY.TabIndex = 4;
      this.FUCLocationY.TopBorderHeight = 5;
      this.FUCLocationY.UnitText = "pxl";
      this.FUCLocationY.UnitWidth = 22;
      this.FUCLocationY.Value = 0;
      // 
      // FUCLocationX
      // 
      this.FUCLocationX.BackColor = System.Drawing.SystemColors.Info;
      this.FUCLocationX.HeaderText = "X";
      this.FUCLocationX.HeaderWidth = 24;
      this.FUCLocationX.Location = new System.Drawing.Point(78, 14);
      this.FUCLocationX.Name = "FUCLocationX";
      this.FUCLocationX.Size = new System.Drawing.Size(101, 26);
      this.FUCLocationX.TabIndex = 3;
      this.FUCLocationX.TopBorderHeight = 5;
      this.FUCLocationX.UnitText = "pxl";
      this.FUCLocationX.UnitWidth = 22;
      this.FUCLocationX.Value = 0;
      // 
      // CUCSetText
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.FUCText);
      this.Controls.Add(this.FUCLocationY);
      this.Controls.Add(this.FUCLocationX);
      this.Controls.Add(this.btnRun);
      this.Controls.Add(this.lblTitle);
      this.Name = "CUCSetText";
      this.Size = new System.Drawing.Size(539, 44);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.Button btnRun;
    private UCCommon.CUCLocation FUCLocationX;
    private UCCommon.CUCLocation FUCLocationY;
    private UCCommon.CUCText FUCText;
  }
}
