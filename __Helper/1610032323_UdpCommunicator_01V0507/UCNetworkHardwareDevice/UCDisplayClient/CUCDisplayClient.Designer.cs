﻿namespace UCDisplayClient
{
  partial class CUCDisplayClient
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblTitle = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.cbxDeviceSelected = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.FUCSetFrame = new UCDisplayClient.CUCSetFrame();
      this.FUCSetText = new UCDisplayClient.CUCSetText();
      this.FUCClearDisplay = new UCDisplayClient.CUCClearDisplay();
      this.SuspendLayout();
      // 
      // lblTitle
      // 
      this.lblTitle.BackColor = System.Drawing.Color.PapayaWhip;
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(553, 13);
      this.lblTitle.TabIndex = 2;
      this.lblTitle.Text = "ServerDisplay: Selected Display Device";
      this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label1
      // 
      this.label1.BackColor = System.Drawing.Color.PapayaWhip;
      this.label1.Dock = System.Windows.Forms.DockStyle.Top;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(553, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "ServerDisplay: Select Device";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // cbxDeviceSelected
      // 
      this.cbxDeviceSelected.Dock = System.Windows.Forms.DockStyle.Top;
      this.cbxDeviceSelected.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxDeviceSelected.FormattingEnabled = true;
      this.cbxDeviceSelected.Location = new System.Drawing.Point(0, 13);
      this.cbxDeviceSelected.Name = "cbxDeviceSelected";
      this.cbxDeviceSelected.Size = new System.Drawing.Size(553, 21);
      this.cbxDeviceSelected.TabIndex = 6;
      // 
      // label2
      // 
      this.label2.BackColor = System.Drawing.Color.PapayaWhip;
      this.label2.Dock = System.Windows.Forms.DockStyle.Top;
      this.label2.Location = new System.Drawing.Point(0, 34);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(553, 13);
      this.label2.TabIndex = 10;
      this.label2.Text = "Display Device: Commands";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FUCSetFrame
      // 
      this.FUCSetFrame.BackColor = System.Drawing.SystemColors.Info;
      this.FUCSetFrame.Location = new System.Drawing.Point(7, 148);
      this.FUCSetFrame.Name = "FUCSetFrame";
      this.FUCSetFrame.Size = new System.Drawing.Size(539, 46);
      this.FUCSetFrame.TabIndex = 9;
      // 
      // FUCSetText
      // 
      this.FUCSetText.BackColor = System.Drawing.SystemColors.Info;
      this.FUCSetText.Location = new System.Drawing.Point(6, 100);
      this.FUCSetText.Name = "FUCSetText";
      this.FUCSetText.Size = new System.Drawing.Size(540, 44);
      this.FUCSetText.TabIndex = 8;
      // 
      // FUCClearDisplay
      // 
      this.FUCClearDisplay.BackColor = System.Drawing.SystemColors.Info;
      this.FUCClearDisplay.Location = new System.Drawing.Point(6, 53);
      this.FUCClearDisplay.Name = "FUCClearDisplay";
      this.FUCClearDisplay.Size = new System.Drawing.Size(80, 43);
      this.FUCClearDisplay.TabIndex = 7;
      // 
      // CUCDisplayClient
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Lavender;
      this.Controls.Add(this.label2);
      this.Controls.Add(this.FUCSetFrame);
      this.Controls.Add(this.FUCSetText);
      this.Controls.Add(this.FUCClearDisplay);
      this.Controls.Add(this.cbxDeviceSelected);
      this.Controls.Add(this.label1);
      this.Name = "CUCDisplayClient";
      this.Size = new System.Drawing.Size(553, 203);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox cbxDeviceSelected;
    private CUCSetFrame FUCSetFrame;
    private CUCSetText FUCSetText;
    private CUCClearDisplay FUCClearDisplay;
    private System.Windows.Forms.Label label2;
  }
}
