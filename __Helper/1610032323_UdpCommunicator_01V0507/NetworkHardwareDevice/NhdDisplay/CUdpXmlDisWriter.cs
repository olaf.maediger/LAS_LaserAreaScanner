﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using UCNotifier;
using Xml;
using UdpTransfer;
using UdpXmlTransfer;
using NetworkHardwareDevice;
//
namespace NhdDisplay
{
  public class CUdpXmlDisWriter : CUdpXmlWriter
  { //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //
 
    //
    //--------------------------------------------
    //	Segment - Field
    //--------------------------------------------
    //	
    
    //
    //--------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------
    //	
    public CUdpXmlDisWriter()
      : base()
    {
    }
    //
    //--------------------------------------------
    //	Segment - Property
    //--------------------------------------------
    //	

    //
    //--------------------------------------------
    //	Segment - Writer
    //--------------------------------------------
    //	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //public Boolean BuildRequestGetProperties(IPAddress ipaddresstarget,
    //                                         out String text)
    //{
    //  text = "";
    //  try
    //  {
    //    CXmlWriter XMLWriter = new CXmlWriter();
    //    XMLWriter.SetNotifier(FNotifier);
    //    XMLWriter.EnableProtocol(true);
    //    // Root
    //    XmlDocument XMLDocument = new XmlDocument();
    //    XmlNode XMLNodeBase;
    //    XMLWriter.AddNode(0, XMLDocument, null, out XMLNodeBase, CUdpXmlMtcHeader.XML_USERMEASUREMENTPROTOCOL);
    //    // Request
    //    XmlNode XMLNodeRequest;
    //    XMLWriter.AddNode(1, XMLDocument, XMLNodeBase, out XMLNodeRequest, CUdpXmlMtcHeader.XML_REQUEST);
    //    // Type
    //    XmlNode XMLNodeType;
    //    XMLWriter.AddNode(2, XMLDocument, XMLNodeRequest, out XMLNodeType,
    //                      CUdpXmlMtcHeader.XML_TYPE, CUdpXmlMtcHeader.XML_GETPROPERTIES);
    //    // IPAddress
    //    XmlNode XMLNodeIPAddress;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeRequest, out XMLNodeIPAddress, CUdpXmlMtcHeader.XML_IPADDRESS);
    //    XmlNode XMLNodeSource;
    //    IPAddress IPAL;
    //    CUdpBase.FindIPAddressLocal(out IPAL);
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeSource,
    //                      CUdpXmlMtcHeader.XML_SOURCE, IPAL.ToString());
    //    XmlNode XMLNodeTarget;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeTarget,
    //                      CUdpXmlMtcHeader.XML_TARGET, ipaddresstarget.ToString());
    //    // IPPort
    //    XmlNode XMLNodeIPPort;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeRequest, out XMLNodeIPPort, CUdpXmlMtcHeader.XML_IPPORT);
    //    XmlNode XMLNodeFrom;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeFrom,
    //                      CUdpXmlMtcHeader.XML_FROM, CUdpXmlMtcHeader.IPPORT_REQUEST_FROM.ToString());
    //    XmlNode XMLNodeBack;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeBack,
    //                      CUdpXmlMtcHeader.XML_BACK, CUdpXmlMtcHeader.IPPORT_REQUEST_BACK.ToString());
    //    //
    //    text = XMLDocument.InnerXml;
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    FNotifier.Error(CUdpXmlMtcHeader.INIT_HEADER, 1, "Cannot build RequestGetProperties");
    //    return false;
    //  }
    //}

    //public Boolean BuildRequestGetMeasurement(IPAddress ipaddresstarget,
    //                                          out String text)
    //{
    //  text = "";
    //  try
    //  {
    //    CXmlWriter XMLWriter = new CXmlWriter();
    //    XMLWriter.SetNotifier(FNotifier);
    //    XMLWriter.EnableProtocol(true);
    //    // Root
    //    XmlDocument XMLDocument = new XmlDocument();
    //    XmlNode XMLNodeBase;
    //    XMLWriter.AddNode(0, XMLDocument, null, out XMLNodeBase, CUdpXmlMtcHeader.XML_USERMEASUREMENTPROTOCOL);
    //    // Request
    //    XmlNode XMLNodeRequest;
    //    XMLWriter.AddNode(1, XMLDocument, XMLNodeBase, out XMLNodeRequest, CUdpXmlMtcHeader.XML_REQUEST);
    //    // Type
    //    XmlNode XMLNodeType;
    //    XMLWriter.AddNode(2, XMLDocument, XMLNodeRequest, out XMLNodeType,
    //                      CUdpXmlMtcHeader.XML_TYPE, CUdpXmlMtcHeader.XML_GETMEASUREMENT);
    //    // IPAddress
    //    XmlNode XMLNodeIPAddress;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeRequest, out XMLNodeIPAddress, CUdpXmlMtcHeader.XML_IPADDRESS);
    //    XmlNode XMLNodeSource;
    //    IPAddress IPAL;
    //    CUdpBase.FindIPAddressLocal(out IPAL);
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeSource,
    //                      CUdpXmlMtcHeader.XML_SOURCE, IPAL.ToString());
    //    XmlNode XMLNodeTarget;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeTarget,
    //                      CUdpXmlMtcHeader.XML_TARGET, ipaddresstarget.ToString());
    //    // IPPort
    //    XmlNode XMLNodeIPPort;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeRequest, out XMLNodeIPPort, CUdpXmlMtcHeader.XML_IPPORT);
    //    XmlNode XMLNodeFrom;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeFrom,
    //                      CUdpXmlMtcHeader.XML_FROM, CUdpXmlMtcHeader.IPPORT_REQUEST_FROM.ToString());
    //    XmlNode XMLNodeBack;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeBack,
    //                      CUdpXmlMtcHeader.XML_BACK, CUdpXmlMtcHeader.IPPORT_REQUEST_BACK.ToString());
    //    //
    //    text = XMLDocument.InnerXml;
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    FNotifier.Error(CUdpXmlMtcHeader.INIT_HEADER, 1, "Cannot build RequestGetMeasurement");
    //    return false;
    //  }
    //}

    //public Boolean BuildCommandSetText(IPAddress ipaddresstarget,
    //                                   CDeviceParameter deviceparameter,
    //                                   out String text)
    //{
    //  text = "";
    //  try
    //  {
    //    CXmlWriter XMLWriter = new CXmlWriter();
    //    XMLWriter.SetNotifier(FNotifier);
    //    XMLWriter.EnableProtocol(true);
    //    // Root
    //    XmlDocument XMLDocument = new XmlDocument();
    //    XmlNode XMLNodeBase;
    //    XMLWriter.AddNode(0, XMLDocument, null, out XMLNodeBase, CUdpXmlDisHeader.XML_USERMEASUREMENTPROTOCOL);
    //    // Command
    //    XmlNode XMLNodeCommand;
    //    XMLWriter.AddNode(1, XMLDocument, XMLNodeBase, out XMLNodeCommand, CUdpXmlDisHeader.XML_COMMAND);
    //    // Type
    //    XmlNode XMLNodeType;
    //    XMLWriter.AddNode(2, XMLDocument, XMLNodeCommand, out XMLNodeType,
    //                      CUdpXmlDisHeader.XML_TYPE, CUdpXmlDisHeader.XML_SETTEXT);
    //    // IPAddress
    //    XmlNode XMLNodeIPAddress;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeCommand, out XMLNodeIPAddress, CUdpXmlMtcHeader.XML_IPADDRESS);
    //    XmlNode XMLNodeSource;
    //    IPAddress IPAL;
    //    CUdpBase.FindIPAddressLocal(out IPAL);
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeSource,
    //                      CUdpXmlMtcHeader.XML_SOURCE, IPAL.ToString());
    //    XmlNode XMLNodeTarget;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeTarget,
    //                      CUdpXmlMtcHeader.XML_TARGET, ipaddresstarget.ToString());
    //    // IPPort
    //    XmlNode XMLNodeIPPort;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeCommand, out XMLNodeIPPort, CUdpXmlMtcHeader.XML_IPPORT);
    //    XmlNode XMLNodeFrom;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeFrom,
    //                      CUdpXmlMtcHeader.XML_FROM, CUdpXmlMtcHeader.IPPORT_REQUEST_FROM.ToString());
    //    XmlNode XMLNodeBack;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeBack,
    //                      CUdpXmlMtcHeader.XML_BACK, CUdpXmlMtcHeader.IPPORT_REQUEST_BACK.ToString());
    //    // Device
    //    XmlNode XMLNodeDevice;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeCommand, out XMLNodeDevice, CUdpXmlMtcHeader.XML_DEVICE);
    //    XmlNode XMLNodeName;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeDevice, out XMLNodeName,
    //                      CUdpXmlMtcHeader.XML_NAME, CUdpXmlMtcHeader.XML_DEVICENAME);
    //    //XmlNode XMLNodeType;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeDevice, out XMLNodeType,
    //                      CUdpXmlMtcHeader.XML_TYPE, CUdpXmlMtcHeader.XML_DEVICETYPE);
    //    XmlNode XMLNodeGuid;
    //    XMLWriter.AddNode(4, XMLDocument, XMLNodeDevice, out XMLNodeGuid,
    //                      CUdpXmlMtcHeader.XML_GUID, String.Format("{0}", Guid.Empty.ToString()));
    //    // Date
    //    XmlNode XMLNodeDate;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeCommand, out XMLNodeDate, CUdpXmlMtcHeader.XML_DATE);
    //    ////// XmlNode XMLNodeYear;
    //    //XMLWriter.AddNode(4, XMLDocument, XMLNodeDate, out XMLNodeYear,
    //    //                  CUdpXmlMtcHeader.XML_YEAR, data.Date.Year);
    //    //XmlNode XMLNodeMonth;
    //    //XMLWriter.AddNode(4, XMLDocument, XMLNodeDate, out XMLNodeMonth,
    //    //                  CUdpXmlMtcHeader.XML_MONTH, data.Date.Month);
    //    //XmlNode XMLNodeDay;
    //    //XMLWriter.AddNode(4, XMLDocument, XMLNodeDate, out XMLNodeDay,
    //    //                  CUdpXmlMtcHeader.XML_DAY, data.Date.Day);
    //    // Time
    //    XmlNode XMLNodeTime;
    //    XMLWriter.AddNode(3, XMLDocument, XMLNodeCommand, out XMLNodeTime, CUdpXmlMtcHeader.XML_TIME);
    //    ///// XmlNode XMLNodeHour;
    //    //XMLWriter.AddNode(4, XMLDocument, XMLNodeTime, out XMLNodeHour,
    //    //                  CUdpXmlMtcHeader.XML_HOUR, data.Time.Hour);
    //    //XmlNode XMLNodeMinute;
    //    //XMLWriter.AddNode(4, XMLDocument, XMLNodeTime, out XMLNodeMinute,
    //    //                  CUdpXmlMtcHeader.XML_MINUTE, data.Time.Minute);
    //    //XmlNode XMLNodeSecond;
    //    //XMLWriter.AddNode(4, XMLDocument, XMLNodeTime, out XMLNodeSecond,
    //    //                  CUdpXmlMtcHeader.XML_SECOND, data.Time.Second);
    //    //
    //    text = XMLDocument.InnerXml;
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    FNotifier.Error(CUdpXmlMtcHeader.INIT_HEADER, 1, "Cannot build CommandSetProperties");
    //    return false;
    //  }
    //}




  }
}
