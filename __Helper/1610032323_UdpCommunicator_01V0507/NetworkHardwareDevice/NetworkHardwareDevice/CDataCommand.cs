﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
namespace NetworkHardwareDevice
{
  public class CDataCommand
  {
    private String FType;
    private IPAddress FIPAddressSource;
    private IPAddress FIPAddressTarget;
    private UInt16 FIPPortFrom;
    private UInt16 FIPPortBack;
    private CParameterList FParameterList;
    private Int32 FCountReceived;

    public CDataCommand()
    {
      FType = "";
      FIPAddressSource = null;
      FIPAddressTarget = null;
      FIPPortFrom = 0;
      FIPPortBack = 0;
      FParameterList = new CParameterList();
      FCountReceived = 0;
    }

    public String Type
    {
      get { return FType; }
      set { FType = value; }
    }

    public IPAddress IPAddressSource
    {
      get { return FIPAddressSource; }
      set { FIPAddressSource = value; }
    }

    public IPAddress IPAddressTarget
    {
      get { return FIPAddressTarget; }
      set { FIPAddressTarget = value; }
    }

    public UInt16 IPPortFrom
    {
      get { return FIPPortFrom; }
      set { FIPPortFrom = value; }
    }

    public UInt16 IPPortBack
    {
      get { return FIPPortBack; }
      set { FIPPortBack = value; }
    }

    public CParameterList ParameterList
    {
      get { return FParameterList; }
      set { FParameterList = value; }
    }

    public Int32 CountReceived
    {
      get { return FCountReceived; }
      set { FCountReceived = value; }
    }
  }

}


