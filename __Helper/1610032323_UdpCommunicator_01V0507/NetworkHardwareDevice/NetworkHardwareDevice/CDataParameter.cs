﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkHardwareDevice
{
  public class CParameter
  {
    private String FName;
    private String FValue;

    public CParameter(String name, String value)
    {
      FName = name;
      FValue = value;
    }

    public String Name
    {
      get { return FName; }
      set { FName = value; }
    }

    public String Value
    {
      get { return FValue; }
      set { FValue = value; }
    }
  }

  public class CParameterList : List<CParameter>
  {
    public new void Add(CParameter parameter)
    {
      base.Add(parameter);
    }
    public void Add(String name, String value)
    {
      CParameter Parameter = new CParameter(name, value);
      base.Add(Parameter);
    }
  }

}
