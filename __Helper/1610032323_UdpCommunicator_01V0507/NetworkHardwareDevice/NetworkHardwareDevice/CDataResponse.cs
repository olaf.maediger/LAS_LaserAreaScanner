﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
namespace NetworkHardwareDevice
{
  public class CDataResponse
  {
    private String FType;
    private IPAddress FIPAddressSource;
    private IPAddress FIPAddressTarget;
    private UInt16 FIPPortFrom;
    private CParameterList FParameterList;

    public CDataResponse()
    {
      FType = "";
      FIPAddressSource = null;
      FIPAddressTarget = null;
      FIPPortFrom = 0;
      FParameterList = new CParameterList();
    }

    public String Type
    {
      get { return FType; }
      set { FType = value; }
    }

    public IPAddress IPAddressSource
    {
      get { return FIPAddressSource; }
      set { FIPAddressSource = value; }
    }

    public IPAddress IPAddressTarget
    {
      get { return FIPAddressTarget; }
      set { FIPAddressTarget = value; }
    }

    public UInt16 IPPortFrom
    {
      get { return FIPPortFrom; }
      set { FIPPortFrom = value; }
    }

    public CParameterList ParameterList
    {
      get { return FParameterList; }
      set { FParameterList = value; }
    }

  }

}
