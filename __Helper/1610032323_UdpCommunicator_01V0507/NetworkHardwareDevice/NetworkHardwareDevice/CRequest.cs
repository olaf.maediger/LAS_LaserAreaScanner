﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
//
using UCNotifier;
using Task;
using Xml;
using UdpTransfer;
using UdpXmlTransfer;
//
namespace NetworkHardwareDevice
{
  public class CRequest: CCommand // !!!!!!!!!!!!!
  { //
    //------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------
    //		
    
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //

    //
    //------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------
    //	   
    public CRequest()
      : base()
    {
      FHeader = "Request";
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Helper - Build - TxdRequest
    //-------------------------------------------------------------------
    // 

    //
    //-------------------------------------------------------------------
    //  Segment - Helper - Analyse - RxdResponse
    //-------------------------------------------------------------------
    // 

    //
    //-------------------------------------------------------------------
    //  Segment - Callback - UdpTransmitter
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Callback - UdpReceiver
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Helper
    //-------------------------------------------------------------------
    //
    protected override String InitSEPReceiver()
    {
      IPAddress IPALocal;
      CUdpBase.FindIPAddressLocal(out IPALocal);
      String SEPReceiver = String.Format("{0}:{1}",
                                         IPALocal.ToString(),
                                         CUdpXmlHeader.IPPORT_REQUEST_BACK);
      return SEPReceiver;
    }

    protected override String InitSEPTransmitter()
    {
      IPAddress IPALocal;
      CUdpBase.FindIPAddressLocal(out IPALocal);
      Byte[] AB = IPALocal.GetAddressBytes();
      String SEPTransmitter = String.Format("{0}.{1}.{2}.{3}:{4}",
                                            AB[0], AB[1], AB[2],
                                            CUdpXmlHeader.INIT_BROADCAST_BYTE,
                                            CUdpXmlHeader.IPPORT_REQUEST_FROM);
      return SEPTransmitter;
    }

    protected override void InitTransmitIPAddressPort(ref CDataCommand datacommand)
    {
      IPAddress IPALocal;
      CUdpBase.FindIPAddressLocal(out IPALocal);
      datacommand.IPAddressSource = IPALocal;
      IPAddress IPABroadcast;
      CUdpBase.FindIPAddressLocal(out IPABroadcast);
      datacommand.IPAddressTarget = IPABroadcast;
      datacommand.IPPortFrom = CUdpXmlHeader.IPPORT_REQUEST_FROM;
      datacommand.IPPortBack = CUdpXmlHeader.IPPORT_REQUEST_BACK;
    }

    protected override Boolean ExecuteCommand(String txdframe)
    {
      FTxdCommand.CountReceived = 0;
      FUdpTransmitter.SetMessage(txdframe);
      String Line = String.Format("{0}[{1}] transmitted", FHeader, FTxdCommand.Type);
      FNotifier.Write(Line);
      Boolean NotifyWait = true;
      Boolean WaitForResponses = true;
      do
      {
        if (NotifyWait)
        {
          FNotifier.Write("Wait for Response...");
          NotifyWait = false;
        }
        WaitForResponses = FEventReceived.WaitFor(TIMESPAN_WAITFORRESPONSE);
      }
      while (WaitForResponses);
      Line = String.Format("{0} Response(s) received!", FTxdCommand.CountReceived);
      FNotifier.Write(Line);
      Abort();
      return false;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback - Task
    //-------------------------------------------------------------------
    //


  }
}
