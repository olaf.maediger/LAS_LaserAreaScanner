﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
//
using Task;
//
namespace NetworkHardwareDevice
{
  public class CCommandPool
  {
    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    //
    private Queue FQueue;
    private CTask FTask;
    private CCommandBase FCommand;
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CCommandPool()
    {
      FQueue = new Queue();
      FTask = null;
      FCommand = null;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Callback - Task
    //---------------------------------------------------------------------------
    //
    private void TaskOnExecutionStart(RTaskData data)
    {
    }
    private Boolean TaskOnExecutionBusy(ref RTaskData data)
    {
      if (FCommand is CCommandBase)
      {
        if (FCommand.IsActive)
        {
          return true;
        }
      }
      if (!(FCommand is CCommandBase))
      {
        //FCommand = 
      }
      Thread.Sleep(1000);
      return false;
    }
    private void TaskOnExecutionEnd(RTaskData data)
    {
    }
    private void TaskOnExecutionAbort(RTaskData data)
    {
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management
    //---------------------------------------------------------------------------
    //
    public void ClearAllCommands()
    {
      if (FTask is CTask)
      {
        FTask.Abort();
        FTask = null;
      }
      foreach (CCommandBase Command in FQueue)
      {
        if (Command is CCommandBase)
        {
          Command.Abort();
        }
      }
      FQueue.Clear();
    }

    public void AddCommand(CCommandBase command)
    {
      FQueue.Enqueue(command);
      if (!(FTask is CTask))
      {
        FTask = new CTask("CPT",
                  TaskOnExecutionStart,
                  TaskOnExecutionBusy,
                  TaskOnExecutionEnd,
                  TaskOnExecutionAbort);
        FTask.Start();
      }
    }


  }
}
