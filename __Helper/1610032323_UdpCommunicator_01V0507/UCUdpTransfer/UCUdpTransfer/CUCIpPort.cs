﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UdpTransfer;
//
namespace UCUdpTransfer
{
  //
  //-------------------------------------------------------------------
  //  Segment - Constructor
  //-------------------------------------------------------------------
  //
  public partial class CUCIpPort : UserControl
  {
    public CUCIpPort()
    {
      InitializeComponent();
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //

    public UInt16 GetIpPort()
    {
      return (UInt16)nudIpPort.Value;
    }

    //public void SetIpPort(String sipendpoint)
    //{
    //  IPEndPoint IPE;
    //  CUdpBase.TextToIpEndPoint(sipendpoint, out IPE);
    //  Byte[] AB = IPE.Address.GetAddressBytes();
    //  nudIpAddress0.Value = (Decimal)AB[3];
    //  nudIpAddress1.Value = (Decimal)AB[2];
    //  nudIpAddress2.Value = (Decimal)AB[1];
    //  nudIpAddress3.Value = (Decimal)AB[0];
    //  nudIpPort.Value = (Decimal)IPE.Port;
    //}

  }
}
