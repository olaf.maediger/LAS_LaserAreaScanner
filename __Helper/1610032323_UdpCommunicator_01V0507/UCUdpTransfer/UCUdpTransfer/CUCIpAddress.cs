﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UdpTransfer;
//
namespace UCUdpTransfer
{
  public partial class CUCIpAddress : UserControl
  {
    public CUCIpAddress()
    {
      InitializeComponent();
    }

    public void SetIPAddress(Byte[] ipaddress)
    {
      if (ipaddress is Byte[])
      {
        if (4 == ipaddress.Length)
        {
          nudIPAddress0.Value = ipaddress[3];
          nudIPAddress1.Value = ipaddress[2];
          nudIPAddress2.Value = ipaddress[1];
          nudIPAddress3.Value = ipaddress[0];
        }
      }
    }

  }
}
