﻿namespace UCUdpTransfer
{
  partial class CUCIpAddress
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.nudIPAddress0 = new System.Windows.Forms.NumericUpDown();
      this.nudIPAddress1 = new System.Windows.Forms.NumericUpDown();
      this.nudIPAddress2 = new System.Windows.Forms.NumericUpDown();
      this.nudIPAddress3 = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddress0)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddress1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddress2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddress3)).BeginInit();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(160, 13);
      this.lblHeader.TabIndex = 4;
      this.lblHeader.Text = "IPAddress";
      // 
      // nudIPAddress0
      // 
      this.nudIPAddress0.BackColor = System.Drawing.SystemColors.Info;
      this.nudIPAddress0.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIPAddress0.Location = new System.Drawing.Point(120, 13);
      this.nudIPAddress0.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIPAddress0.Name = "nudIPAddress0";
      this.nudIPAddress0.Size = new System.Drawing.Size(40, 20);
      this.nudIPAddress0.TabIndex = 8;
      this.nudIPAddress0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudIPAddress0.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // nudIPAddress1
      // 
      this.nudIPAddress1.BackColor = System.Drawing.SystemColors.Info;
      this.nudIPAddress1.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIPAddress1.Location = new System.Drawing.Point(80, 13);
      this.nudIPAddress1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIPAddress1.Name = "nudIPAddress1";
      this.nudIPAddress1.Size = new System.Drawing.Size(40, 20);
      this.nudIPAddress1.TabIndex = 7;
      this.nudIPAddress1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // nudIPAddress2
      // 
      this.nudIPAddress2.BackColor = System.Drawing.SystemColors.Info;
      this.nudIPAddress2.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIPAddress2.Location = new System.Drawing.Point(40, 13);
      this.nudIPAddress2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIPAddress2.Name = "nudIPAddress2";
      this.nudIPAddress2.Size = new System.Drawing.Size(40, 20);
      this.nudIPAddress2.TabIndex = 6;
      this.nudIPAddress2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // nudIPAddress3
      // 
      this.nudIPAddress3.BackColor = System.Drawing.SystemColors.Info;
      this.nudIPAddress3.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudIPAddress3.Location = new System.Drawing.Point(0, 13);
      this.nudIPAddress3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudIPAddress3.Name = "nudIPAddress3";
      this.nudIPAddress3.Size = new System.Drawing.Size(40, 20);
      this.nudIPAddress3.TabIndex = 5;
      this.nudIPAddress3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudIPAddress3.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
      // 
      // CUCIpAddress
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.nudIPAddress0);
      this.Controls.Add(this.nudIPAddress1);
      this.Controls.Add(this.nudIPAddress2);
      this.Controls.Add(this.nudIPAddress3);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCIpAddress";
      this.Size = new System.Drawing.Size(160, 33);
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddress0)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddress1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddress2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudIPAddress3)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.NumericUpDown nudIPAddress0;
    private System.Windows.Forms.NumericUpDown nudIPAddress1;
    private System.Windows.Forms.NumericUpDown nudIPAddress2;
    private System.Windows.Forms.NumericUpDown nudIPAddress3;
  }
}
