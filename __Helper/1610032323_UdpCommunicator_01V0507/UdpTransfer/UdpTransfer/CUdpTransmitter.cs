﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
//
using UCNotifier;
using Task;
//
namespace UdpTransfer
{
  public delegate void DOnDatagramTransmitted(IPEndPoint ipendpoint,
                                              Byte[] datagram, Int32 size);
  //
  public class CUdpTransmitter : CUdpBase
  {
    //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //
    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    private DOnDatagramTransmitted FOnDatagramTransmitted;
    private String FMessage;
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public CUdpTransmitter()
      : base()
    {
      FMessage = null;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public void SetOnDatagramTransmitted(DOnDatagramTransmitted value)
    {
      FOnDatagramTransmitted = value;
    }

    public void SetMessage(String message)
    {
      FMessage = message;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //
    private void TaskOnExecutionStart(RTaskData data)
    {
    }
    private Boolean TaskOnExecutionBusy(ref RTaskData data)
    {
      try
      {
        if (FMessage is String)
        {
          IPEndPoint IPE = FIpEndPoint;
          Byte[] UdpDatagram = Encoding.ASCII.GetBytes(FMessage);
          FMessage = null;
          if (UdpDatagram is Byte[])
          {
            String Line = String.Format("Txd[{0}]|{1}|",
                          IPEndPointToText(FIpEndPoint),
                          System.Text.Encoding.UTF8.GetString(UdpDatagram, 0, UdpDatagram.Length));
            FNotifier.Write(Line);
            FUdpClient.Send(UdpDatagram, UdpDatagram.Length);
            if (FOnDatagramTransmitted is DOnDatagramTransmitted)
            {
              FOnDatagramTransmitted(IPE, UdpDatagram, UdpDatagram.Length);
            }
          }
          else
          {
            Thread.Sleep(100);
          }
        }
        return true;
      }
      catch (Exception)
      {
        // ???? FNotifier.Error("UdpServer", 1, "Invalid UdpDatagram");
        return false;
      }
    }
    private void TaskOnExecutionEnd(RTaskData data)
    {
    }
 
    private void TaskOnExecutionAbort(RTaskData data)
    {
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Method
    //-------------------------------------------------------------------
    //
    public Boolean Open(IPAddress ipaddress, UInt16 ipport)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        //
        FIpEndPoint = new IPEndPoint(ipaddress, ipport);
        FUdpClient = new UdpClient();
        FUdpClient.Connect(FIpEndPoint);
        //
        FTask = new CTask("UdpServer",
                          TaskOnExecutionStart,
                          TaskOnExecutionBusy,
                          TaskOnExecutionEnd,
                          TaskOnExecutionAbort);
        FTask.Start();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Open(String sipendpoint)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        //
        CUdpBase.TextToIPEndPoint(sipendpoint, out FIpEndPoint);
        FUdpClient = new UdpClient();
        FUdpClient.Connect(FIpEndPoint);
        //
        FTask = new CTask("UdpServer",
                          TaskOnExecutionStart,
                          TaskOnExecutionBusy,
                          TaskOnExecutionEnd,
                          TaskOnExecutionAbort);
        FTask.Start();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Close()
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        if (FUdpClient is UdpClient)
        {
          FUdpClient.Close();
          FUdpClient = null;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
