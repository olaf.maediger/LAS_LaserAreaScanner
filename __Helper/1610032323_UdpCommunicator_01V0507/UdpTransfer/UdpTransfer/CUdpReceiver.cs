﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Net;
using System.Net.Sockets;
//
using Task;
using UCNotifier;
//
namespace UdpTransfer
{
  public delegate void DOnDatagramReceived(IPEndPoint ipendpoint,
                                           Byte[] datagram, Int32 size);
  //
  public class CUdpReceiver : CUdpBase
  { //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    private DOnDatagramReceived FOnDatagramReceived;
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public CUdpReceiver()
      : base()
    {
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public void SetOnDatagramReceived(DOnDatagramReceived value)
    {
      FOnDatagramReceived = value;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //
    private void TaskOnExecutionStart(RTaskData data)
    {
    }
    private Boolean TaskOnExecutionBusy(ref RTaskData data)
    {
      try
      {
        IPEndPoint IPE = FIpEndPoint;
        Byte[] UdpDatagram;
        UdpDatagram = FUdpClient.Receive(ref IPE);// OK !!! 
        if (UdpDatagram is Byte[])
        {
          String Text = System.Text.Encoding.UTF8.GetString(UdpDatagram, 0, UdpDatagram.Length);
          String Line = String.Format("Rxd[{0}]|{1}|",
                        IPEndPointToText(IPE), // IPEndpoint from sender!
                        Text);
          FNotifier.Write(Line);
          if (FOnDatagramReceived is DOnDatagramReceived)
          {
            FOnDatagramReceived(IPE, UdpDatagram, UdpDatagram.Length);
          }
        }
        return true;
      }
      catch (Exception e)
      {
        FNotifier.Write(e.Message);
        FNotifier.Write("UdpClient: Receive-Thread aborted");
        return false;
      }
    }
    private void TaskOnExecutionEnd(RTaskData data)
    {
    }
    private void TaskOnExecutionAbort(RTaskData data)
    {
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Method
    //-------------------------------------------------------------------
    //
    public Boolean Open(IPAddress ipaddress, UInt16 ipport)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        FIpEndPoint = new IPEndPoint(ipaddress, ipport);
        // !!! OK FIpEndPoint = new IPEndPoint(IPAddress.Any, ipport);
        FUdpClient = new UdpClient(FIpEndPoint);
        //
        FTask = new CTask("UdpClient",
                          TaskOnExecutionStart,
                          TaskOnExecutionBusy,
                          TaskOnExecutionEnd,
                          TaskOnExecutionAbort);
        FTask.Start();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Open(String sipendpoint)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        CUdpBase.TextToIPEndPoint(sipendpoint, out FIpEndPoint);
        FUdpClient = new UdpClient(FIpEndPoint);
        //
        FTask = new CTask("UdpClient",
                          TaskOnExecutionStart,
                          TaskOnExecutionBusy,
                          TaskOnExecutionEnd,
                          TaskOnExecutionAbort);
        FTask.Start();
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    
    public override Boolean Close()
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        if (FUdpClient is UdpClient)
        {
          FUdpClient.Close();
          FUdpClient = null;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
