﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using UCNotifier;
using Xml;
using UdpTransfer;
//
namespace UdpXmlTransfer
{



  public class CUdpXmlHeader : CXmlHeader
  { //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //
    public const String INIT_HEADER = "UdpXmlTransfer";
    // Command
    public const String XML_USERMEASUREMENTPROTOCOL = "UserMeasurementProtocol";
    public const String XML_REQUEST = "Request";
    public const String XML_COMMAND = "Command";
    public const String XML_RESPONSE = "Response";
    public const String XML_TYPE = "Type";
    public const String XML_FINDALLDEVICES = "FindAllDevices";
    public const String XML_IPADDRESS = "IPAddress";
    public const String XML_SOURCE = "Source";
    public const String XML_TARGET = "Target";
    public const String XML_IPPORT = "IPPort";
    public const String XML_FROM = "From";
    public const String XML_BACK = "Back";
    public const String XML_DEVICE = "Device";
    public const String XML_NAME = "Name";
    public const String XML_GUID = "Guid";
    public const String XML_PARAMETER = "Parameter";
    public const String XML_DATA = "Data";
    public Guid XML_DEVICEGUID = Guid.Empty;
    // Port
    //////public const Int32 IPPORT_BROADCAST_FROM = 20000;
    //////public const Int32 IPPORT_BROADCAST_BACK = 20001;
    public const Int32 IPPORT_REQUEST_FROM = 20000;
    public const Int32 IPPORT_REQUEST_BACK = 20001;
    //////////// NC public const Int32 IPPORT_COMMAND2_FROM = 22000; // SetText - MIST ÄNDERN!!!!
    //////////// NC public const Int32 IPPORT_COMMAND2_BACK = 22001;
    //////////// NC public const Int32 IPPORT_COMMAND3_FROM = 23000; // SetFrame - MIST ÄNDERN!!!!
    //////////// NC public const Int32 IPPORT_COMMAND3_BACK = 23001;
    public const Int32 IPPORT_COMMAND_FROM = 21000; // New - for ALL Commands!!! eleminate COMMAND2/3
    public const Int32 IPPORT_COMMAND_BACK = 21001;
    //
    public const Byte INIT_BROADCAST_BYTE = 0xFF;
    //
    //--------------------------------------------
    //	Segment - Field
    //--------------------------------------------
    //	
    protected CNotifier FNotifier;
    //
    //--------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------
    //	
    public CUdpXmlHeader()
    {
    }
    //
    //--------------------------------------------
    //	Segment - Property
    //--------------------------------------------
    //	
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }
  }
}
