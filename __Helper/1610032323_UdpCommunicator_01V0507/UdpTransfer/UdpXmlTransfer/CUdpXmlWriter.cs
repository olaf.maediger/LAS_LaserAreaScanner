﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using UCNotifier;
using Xml;
using UdpTransfer;
//
namespace UdpXmlTransfer
{
  public class CUdpXmlWriter : CXmlWriter
  { //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //

    //
    //--------------------------------------------
    //	Segment - Field
    //--------------------------------------------
    //	
    
    //
    //--------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------
    //	
    public CUdpXmlWriter()
      : base()
    {
    }
    //
    //--------------------------------------------
    //	Segment - Property
    //--------------------------------------------
    //	
    
    //
    //--------------------------------------------
    //	Segment - Writer
    //--------------------------------------------
    //	
    ////////////public Boolean BuildRequestFindAllDevices(out String text)
    ////////////{
    ////////////  text = "";
    ////////////  try
    ////////////  {
    ////////////    CXmlWriter XMLWriter = new CXmlWriter();
    ////////////    XMLWriter.SetNotifier(FNotifier);
    ////////////    XMLWriter.EnableProtocol(true);
    ////////////    // Root
    ////////////    XmlDocument XMLDocument = new XmlDocument();
    ////////////    XmlNode XMLNodeBase;
    ////////////    XMLWriter.AddNode(0, XMLDocument, null, out XMLNodeBase, CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
    ////////////    // Request
    ////////////    XmlElement XMLNodeRequest;
    ////////////    XMLWriter.AddNodeAttribute(1, XMLDocument, XMLNodeBase, out XMLNodeRequest,
    ////////////                               CUdpXmlHeader.XML_REQUEST, CUdpXmlHeader.XML_TYPE, CUdpXmlHeader.XML_FINDALLDEVICES);
    ////////////    // IPAddress
    ////////////    XmlNode XMLNodeIPAddress;
    ////////////    XMLWriter.AddNode(3, XMLDocument, XMLNodeRequest, out XMLNodeIPAddress, CUdpXmlHeader.XML_IPADDRESS);
    ////////////    XmlNode XMLNodeSource;
    ////////////    IPAddress IPAL;
    ////////////    CUdpBase.FindIPAddressLocal(out IPAL);
    ////////////    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeSource,
    ////////////                      CUdpXmlHeader.XML_SOURCE, IPAL.ToString());
    ////////////    XmlNode XMLNodeTarget;
    ////////////    IPAddress IPAT;
    ////////////    CUdpBase.FindIPAddressLocalBroadcast(out IPAT);
    ////////////    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeTarget,
    ////////////                      CUdpXmlHeader.XML_TARGET, IPAT.ToString());
    ////////////    // IPPort
    ////////////    XmlNode XMLNodeIPPort;
    ////////////    XMLWriter.AddNode(3, XMLDocument, XMLNodeRequest, out XMLNodeIPPort, CUdpXmlHeader.XML_IPPORT);
    ////////////    XmlNode XMLNodeFrom;
    ////////////    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeFrom,
    ////////////                      CUdpXmlHeader.XML_FROM, CUdpXmlHeader.IPPORT_BROADCAST_FROM.ToString());
    ////////////    XmlNode XMLNodeBack;
    ////////////    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeBack,
    ////////////                      CUdpXmlHeader.XML_BACK, CUdpXmlHeader.IPPORT_BROADCAST_BACK.ToString());
    ////////////    //
    ////////////    text = XMLDocument.InnerXml;
    ////////////    return true;
    ////////////  }
    ////////////  catch (Exception)
    ////////////  {
    ////////////    FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot build RequestFindAllDevices");
    ////////////    return false;
    ////////////  }
    ////////////}

    ////////////public Boolean BuildResponseFindAllDevices(RDataFindAllDevices data,
    ////////////                                           String devicename,
    ////////////                                           String devicetype,
    ////////////                                           Int32 deviceindex,
    ////////////                                           out String text)
    ////////////{
    ////////////  text = "";
    ////////////  try
    ////////////  {
    ////////////    CXmlWriter XMLWriter = new CXmlWriter();
    ////////////    XMLWriter.SetNotifier(FNotifier);
    ////////////    XMLWriter.EnableProtocol(true);
    ////////////    // Root
    ////////////    XmlDocument XMLDocument = new XmlDocument();
    ////////////    XmlNode XMLNodeBase;
    ////////////    XMLWriter.AddNode(0, XMLDocument, null, out XMLNodeBase, CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
    ////////////    // Response
    ////////////    XmlElement XMLNodeResponse;
    ////////////    XMLWriter.AddNodeAttribute(1, XMLDocument, XMLNodeBase, out XMLNodeResponse,
    ////////////                               CUdpXmlHeader.XML_RESPONSE, CUdpXmlHeader.XML_TYPE, CUdpXmlHeader.XML_FINDALLDEVICES);
    ////////////    // IPAddress
    ////////////    XmlNode XMLNodeIPAddress;
    ////////////    XMLWriter.AddNode(3, XMLDocument, XMLNodeResponse, out XMLNodeIPAddress, CUdpXmlHeader.XML_IPADDRESS);
    ////////////    XmlNode XMLNodeSource;
    ////////////    IPAddress IPAL;
    ////////////    CUdpBase.FindIPAddressLocal(out IPAL);
    ////////////    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeSource,
    ////////////                      CUdpXmlHeader.XML_SOURCE, IPAL.ToString());
    ////////////    XmlNode XMLNodeTarget;
    ////////////    IPAddress IPAT = new IPAddress(data.IPAddress[0].Value);
    ////////////    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeTarget,
    ////////////                      CUdpXmlHeader.XML_TARGET, IPAT.ToString());
    ////////////    // IPPort
    ////////////    XmlNode XMLNodeIPPort;
    ////////////    XMLWriter.AddNode(3, XMLDocument, XMLNodeResponse, out XMLNodeIPPort, CUdpXmlHeader.XML_IPPORT);
    ////////////    XmlNode XMLNodeFrom;
    ////////////    XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeFrom,
    ////////////                      CUdpXmlHeader.XML_FROM, data.IPPort[1].Value.ToString());
    ////////////    // Device
    ////////////    XmlNode XMLNodeDevice;
    ////////////    XMLWriter.AddNode(3, XMLDocument, XMLNodeResponse, out XMLNodeDevice, CUdpXmlHeader.XML_DEVICE);
    ////////////    XmlNode XMLNodeName;
    ////////////    XMLWriter.AddNode(4, XMLDocument, XMLNodeDevice, out XMLNodeName,
    ////////////                      CUdpXmlHeader.XML_NAME, devicename);
    ////////////    XmlNode XMLNodeType;
    ////////////    XMLWriter.AddNode(4, XMLDocument, XMLNodeDevice, out XMLNodeType,
    ////////////                      CUdpXmlHeader.XML_TYPE, devicetype);
    ////////////    XmlNode XMLNodeIndex;
    ////////////    XMLWriter.AddNode(4, XMLDocument, XMLNodeDevice, out XMLNodeIndex,
    ////////////                      CUdpXmlHeader.XML_INDEX, String.Format("{0:00}", deviceindex));
    ////////////    //
    ////////////    text = XMLDocument.InnerXml;
    ////////////    return true;
    ////////////  }
    ////////////  catch (Exception)
    ////////////  {
    ////////////    FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot build ResponseFindAllDevices");
    ////////////    return false;
    ////////////  }
    ////////////}

  }
}
