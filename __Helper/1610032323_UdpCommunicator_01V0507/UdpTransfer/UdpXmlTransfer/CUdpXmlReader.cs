﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using UCNotifier;
using Xml;
using UdpTransfer;
//
namespace UdpXmlTransfer
{
  public class CUdpXmlReader : CXmlReader
  { //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //

    //
    //--------------------------------------------
    //	Segment - Field
    //--------------------------------------------
    //	
    // private RDataFindAllDevices FRequestFindAllDevices;
    //
    //--------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------
    //	
    public CUdpXmlReader()
      : base()
    {
    }
    //
    //--------------------------------------------
    //	Segment - Property
    //--------------------------------------------
    //	
    ////////public RDataFindAllDevices GetRequestFindAllDevices()
    ////////{
    ////////  return FRequestFindAllDevices;
    ////////}
    //
    //--------------------------------------------
    //	Segment - Reader - Part
    //--------------------------------------------
    //	
    ////////public Boolean AnalyseIPAddress(XmlNode nodebase, out RIPAddress[] ipaddress)
    ////////{
    ////////  Boolean Result = true;
    ////////  ipaddress = new RIPAddress[2];
    ////////  String Line = String.Format("<{0}>", nodebase.Name); // IPAddress
    ////////  IPAddress IPA;
    ////////  foreach (XmlNode NodeChild in nodebase.ChildNodes)
    ////////  {
    ////////    String Name = NodeChild.Name;
    ////////    switch (Name)
    ////////    {
    ////////      case CUdpXmlHeader.XML_SOURCE:
    ////////        if (CUdpBase.TextToIPAddress(NodeChild.InnerText, out IPA))
    ////////        {
    ////////          ipaddress[0].Type = CUdpXmlHeader.XML_SOURCE;
    ////////          ipaddress[0].Value = IPA.GetAddressBytes();
    ////////        }
    ////////        else
    ////////        {
    ////////          Result = false;
    ////////        }
    ////////        break;
    ////////      case CUdpXmlHeader.XML_TARGET:
    ////////        if (CUdpBase.TextToIPAddress(NodeChild.InnerText, out IPA))
    ////////        {
    ////////          ipaddress[1].Type = CUdpXmlHeader.XML_TARGET;
    ////////          ipaddress[1].Value = IPA.GetAddressBytes();
    ////////        }
    ////////        else
    ////////        {
    ////////          Result = false;
    ////////        }
    ////////        break;
    ////////      default:
    ////////        Result = false;
    ////////        break;
    ////////    }
    ////////  }
    ////////  return Result;
    ////////}

    ////////public Boolean AnalyseIPPort(XmlNode nodebase, out RIPPort[] ipport)
    ////////{
    ////////  Boolean Result = true;
    ////////  ipport = new RIPPort[2];
    ////////  String Line = String.Format("<{0}>", nodebase.Name); // IPPort
    ////////  UInt16 IPP;
    ////////  foreach (XmlNode NodeChild in nodebase.ChildNodes)
    ////////  {
    ////////    String Name = NodeChild.Name;
    ////////    switch (Name)
    ////////    {
    ////////      case CUdpXmlHeader.XML_FROM:
    ////////        if (CUdpBase.TextToIPPort(NodeChild.InnerText, out IPP))
    ////////        {
    ////////          ipport[0].Type = CUdpXmlHeader.XML_FROM;
    ////////          ipport[0].Value = IPP;
    ////////        }
    ////////        else
    ////////        {
    ////////          Result = false;
    ////////        }
    ////////        break;
    ////////      case CUdpXmlHeader.XML_BACK:
    ////////        if (CUdpBase.TextToIPPort(NodeChild.InnerText, out IPP))
    ////////        {
    ////////          ipport[1].Type = CUdpXmlHeader.XML_BACK;
    ////////          ipport[1].Value = IPP;
    ////////        }
    ////////        else
    ////////        {
    ////////          Result = false;
    ////////        }
    ////////        break;
    ////////      default:
    ////////        Result = false;
    ////////        break;
    ////////    }
    ////////  }
    ////////  return Result;
    ////////}


    ////////public Boolean AnalyseRequestFindAllDevices(XmlNode nodebase, ref RDataFindAllDevices data)
    ////////{
    ////////  Boolean Result = true;
    ////////  String Line = String.Format("<{0}>", nodebase.Name);
    ////////  Write(Line);
    ////////  WriteIncrement();
    ////////  data = new RDataFindAllDevices();
    ////////  foreach (XmlNode NodeChild in nodebase.ChildNodes)
    ////////  {
    ////////    String Name = NodeChild.Name;
    ////////    switch (Name)
    ////////    {
    ////////      case CUdpXmlHeader.XML_IPADDRESS:
    ////////        RIPAddress[] IPAddress;
    ////////        if (AnalyseIPAddress(NodeChild, out IPAddress))
    ////////        {
    ////////          data.IPAddress = IPAddress;
    ////////        }
    ////////        else
    ////////        {
    ////////          Result = false;
    ////////        }
    ////////        break;
    ////////      case CUdpXmlHeader.XML_IPPORT:
    ////////        RIPPort[] IPPort;
    ////////        if (AnalyseIPPort(NodeChild, out IPPort))
    ////////        {
    ////////          data.IPPort = IPPort;
    ////////        }
    ////////        else
    ////////        {
    ////////          Result = false;
    ////////        }
    ////////        break;
    ////////      default:
    ////////        Result &= false;
    ////////        break;
    ////////    }
    ////////  }
    ////////  WriteDecrement();
    ////////  return Result;
    ////////}
    ////////public Boolean AnalyseRequestFindAllDevices(String xmltext, ref RDataFindAllDevices data)
    ////////{
    ////////  try
    ////////  {
    ////////    Boolean Result = true;
    ////////    CXmlReader XMLReader = new CXmlReader();
    ////////    XMLReader.SetNotifier(FNotifier);
    ////////    XMLReader.EnableProtocol(true);
    ////////    // Root
    ////////    XmlDocument XMLDocument = new XmlDocument();
    ////////    XMLDocument.InnerXml = xmltext;
    ////////    //
    ////////    XmlNode XMLNodeBase = XMLDocument.SelectSingleNode(CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
    ////////    if (XMLNodeBase is XmlNode)
    ////////    {
    ////////      String Line = String.Format("<{0}>", XMLNodeBase.LocalName);
    ////////      Write(Line);
    ////////      WriteIncrement();
    ////////      //
    ////////      foreach (XmlNode NodeChild in XMLNodeBase.ChildNodes)
    ////////      { // debug Console.WriteLine("----> {0}", NodeChild.Name);
    ////////        switch (NodeChild.Name)
    ////////        {
    ////////          case CUdpXmlHeader.XML_REQUEST:
    ////////            Int32 CA = NodeChild.Attributes.Count;
    ////////            if (0 < CA)
    ////////            {
    ////////              String AN = NodeChild.Attributes[0].Name;
    ////////              String AV = NodeChild.Attributes[0].Value;
    ////////              if (CUdpXmlHeader.XML_TYPE == AN)
    ////////              {
    ////////                if (CUdpXmlHeader.XML_FINDALLDEVICES == AV)
    ////////                {
    ////////                  Result &= AnalyseRequestFindAllDevices(NodeChild, ref data);
    ////////                }
    ////////              }
    ////////            }
    ////////            break;
    ////////         }
    ////////      }
    ////////      WriteDecrement();
    ////////      return Result;
    ////////    }
    ////////    return false;
    ////////  }
    ////////  catch (Exception)
    ////////  {
    ////////    FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot analyse UserMeasurementProtocol");
    ////////    return false;
    ////////  }
    ////////}







    ////////public Boolean AnalyseResponseFindAllDevices(String xmltext, ref CDeviceParameter deviceparameter)
    ////////{
    ////////  try
    ////////  {
    ////////    Boolean Result = true;
    ////////    CXmlReader XMLReader = new CXmlReader();
    ////////    XMLReader.SetNotifier(FNotifier);
    ////////    XMLReader.EnableProtocol(true);
    ////////    // Root
    ////////    XmlDocument XMLDocument = new XmlDocument();
    ////////    XMLDocument.InnerXml = xmltext;
    ////////    //
    ////////    XmlNode XMLNodeBase = XMLDocument.SelectSingleNode(CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
    ////////    if (XMLNodeBase is XmlNode)
    ////////    {
    ////////      String Line = String.Format("<{0}>", XMLNodeBase.LocalName);
    ////////      Write(Line);
    ////////      WriteIncrement();
    ////////      //
    ////////      foreach (XmlNode NodeChild in XMLNodeBase.ChildNodes)
    ////////      { // debug Console.WriteLine("----> {0}", NodeChild.Name);
    ////////        switch (NodeChild.Name)
    ////////        {
    ////////          case CUdpXmlHeader.XML_REQUEST:
    ////////            Int32 CA = NodeChild.Attributes.Count;
    ////////            if (0 < CA)
    ////////            {
    ////////              String AN = NodeChild.Attributes[0].Name;
    ////////              String AV = NodeChild.Attributes[0].Value;
    ////////              if (CUdpXmlHeader.XML_TYPE == AN)
    ////////              {
    ////////                if (CUdpXmlHeader.XML_FINDALLDEVICES == AV)
    ////////                {
    ////////                  Result &= AnalyseRequestFindAllDevices(NodeChild, ref data);
    ////////                }
    ////////              }
    ////////            }
    ////////            break;
    ////////        }
    ////////      }
    ////////      WriteDecrement();
    ////////      return Result;
    ////////    }
    ////////    return false;
    ////////  }
    ////////  catch (Exception)
    ////////  {
    ////////    FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot analyse UserMeasurementProtocol");
    ////////    return false;
    ////////  }
    ////////}









    ////////public virtual Boolean AnalyseAllRequests(XmlNode nodebase)
    ////////{
    ////////  Boolean Result = false;
    ////////  String Line = String.Format("<{0}>", nodebase.Name);
    ////////  Write(Line);
    ////////  WriteIncrement();
    ////////  Int32 CA = nodebase.Attributes.Count;
    ////////  if (0 < CA)
    ////////  {
    ////////    String AN = nodebase.Attributes[0].Name;
    ////////    String AV = nodebase.Attributes[0].Value;
    ////////    if (CUdpXmlHeader.XML_TYPE == AN)
    ////////    {
    ////////      switch (AV)
    ////////      {
    ////////        case CUdpXmlHeader.XML_FINDALLDEVICES:
    ////////          Result &= AnalyseRequestFindAllDevices(nodebase, out FRequestFindAllDevices);
    ////////          break;
    ////////      }
    ////////    }
    ////////  }
    ////////  WriteDecrement();
    ////////  return Result;
    ////////}
    //
    //--------------------------------------------
    //	Segment - Reader - Command
    //--------------------------------------------
    //	
    //////////////public virtual Boolean AnalyseUserMeasurementProtocol(String text)
    //////////////{
    //////////////  try
    //////////////  {
    //////////////    Boolean Result = true;
    //////////////    CXmlReader XMLReader = new CXmlReader();
    //////////////    XMLReader.SetNotifier(FNotifier);
    //////////////    XMLReader.EnableProtocol(true);
    //////////////    // Root
    //////////////    XmlDocument XMLDocument = new XmlDocument();
    //////////////    XMLDocument.InnerXml = text;
    //////////////    //
    //////////////    XmlNode XMLNodeBase = XMLDocument.SelectSingleNode(CUdpXmlHeader.XML_USERMEASUREMENTPROTOCOL);
    //////////////    String Line = String.Format("<{0}>", XMLNodeBase.LocalName);
    //////////////    Write(Line);
    //////////////    WriteIncrement();
    //////////////    //
    //////////////    foreach (XmlNode NodeChild in XMLNodeBase.ChildNodes)
    //////////////    { // debug Console.WriteLine("----> {0}", NodeChild.Name);
    //////////////      switch (NodeChild.Name)
    //////////////      {
    //////////////        case CUdpXmlHeader.XML_REQUEST:
    //////////////          Result &= AnalyseAllRequests(NodeChild);
    //////////////          break;
    //////////////        //case CUdpXmlHeader.XML_COMMAND:
    //////////////        //  //Result &= AnalyseAllCommands(NodeChild);
    //////////////        //  break;
    //////////////        //case CUdpXmlHeader.XML_RESPONSE:
    //////////////        //  //Result &= AnalyseAllResponses(NodeChild);
    //////////////        //  break;
    //////////////      }
    //////////////    }
    //////////////    WriteDecrement();
    //////////////    return true;
    //////////////  }
    //////////////  catch (Exception)
    //////////////  {
    //////////////    FNotifier.Error(CUdpXmlHeader.INIT_HEADER, 1, "Cannot analyse UserMeasurementProtocol");
    //////////////    return false;
    //////////////  }
    //////////////}





  }
}


//foreach (XmlNode NodeChild in nodebase.ChildNodes)
//{
//  String Name = NodeChild.Name;
//  switch (Name)
//  {
//  //  case CUdpXmlHeader.XML_TYPE:
//  //    //if (CUdpXmlHeader.XML_FINDALLDEVICES == nodebase.InnerText)
//  //    //{
//  //    //  return AnalyseFindAllDevices
//  //    //}
//  //    //else
//  //    //  return false;
//  //    //  // --> CUdpXmlMtcReader.cs
//  //    //  //if (CUdpXmlHeader.XML_GETPROPERTIES == nodebase.InnerText)
//  //    //  //{
//  //    //  //}
//  //    //  //else
//  //    //  //  if (CUdpXmlHeader.XML_GETMEASUREMENT == nodebase.InnerText)
//  //    //  //  {
//  //    //  //  }

//  //  case XML_IPADDRESS:

//  //  //  Result &= AnalyseIPAddress(NodeChild);
//  //  //  break;
//  //  //case XML_IPPORT:
//  //  //  Result &= AnalyseIPPort(NodeChild);
//  //  //  break;
//  //  //case XML_DEVICE:
//  //  //  Result &= AnalyseDevice(NodeChild);
//  //  //  break;
//  //  default:
//  //    Result = false;
//  //    break;
//  //}
//}

//// Request
//XmlNode XMLNodeRequest;
//XMLWriter.AddNode(1, XMLDocument, XMLNodeBase, out XMLNodeRequest, XML_REQUEST);
//// Type
//XmlNode XMLNodeType;
//XMLWriter.AddNode(2, XMLDocument, XMLNodeRequest, out XMLNodeType,
//                  XML_TYPE, XML_FINDALLDEVICES);
//// IPAddress
//XmlNode XMLNodeIPAddress;
//XMLWriter.AddNode(3, XMLDocument, XMLNodeRequest, out XMLNodeIPAddress, XML_IPADDRESS);
//XmlNode XMLNodeSource;
//IPAddress IPAL;
//CUdpBase.FindIpAddressLocal(out IPAL);
//XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeSource,
//                  XML_SOURCE, IPAL.ToString());
//XmlNode XMLNodeTarget;
//IPAddress IPAT;
//CUdpBase.FindIpAddressLocalBroadcast(out IPAT);
//XMLWriter.AddNode(4, XMLDocument, XMLNodeIPAddress, out XMLNodeTarget,
//                  XML_TARGET, IPAT.ToString());
//// IPPort
//XmlNode XMLNodeIPPort;
//XMLWriter.AddNode(3, XMLDocument, XMLNodeRequest, out XMLNodeIPPort, XML_IPPORT);
//XmlNode XMLNodeFrom;
//XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeFrom,
//                  XML_FROM, IPPORT_BROADCAST_FROM.ToString());
//XmlNode XMLNodeBack;
//XMLWriter.AddNode(4, XMLDocument, XMLNodeIPPort, out XMLNodeBack,
//                  XML_BACK, IPPORT_BROADCAST_BACK.ToString());
////
//text = XMLDocument.InnerXml;