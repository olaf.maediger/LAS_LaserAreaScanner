﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace Xml
{
  public class CXmlReader : CXmlBase 
  {

    //
    //-------------------------------------
    //	Segment - Constructor
    //-------------------------------------
    //
    public CXmlReader()
      : base()
    {
    }
    //
    //------------------------------------------
    //	Segment - Helper
    //------------------------------------------
    //
    private String TransformString(String value)
    { // filtering "\r", "\n"
      return value.Replace("\\r\\n", "\r\n");
    }
    //
    //------------------------------------------
    //	Segment - Read - LowLevel
    //------------------------------------------
    //
    public Boolean BuildString(String value, out String result)
    {
      result = "";
      if (value is String)
      {
        result = TransformString(value);
        return true;
      }
      return false;
    }

    public Boolean BuildByte(String value, out Byte result)
    {
      try
      {
        result = Byte.Parse(value);
        return true;
      }
      catch (Exception)
      {
        result = 0x00;
        return false;
      }
    }

    public Boolean BuildBoolean(String value, out Boolean result)
    {
      try
      {
        switch (value)
        {
          case "TRUE":
            result = true;
            return true;
          case "True":
            result = true;
            return true;
          case "true":
            result = true;
            return true;
          case "FALSE":
            result = false;
            return false;
          case "False":
            result = false;
            return false;
          case "false":
            result = false;
            return false;
          default:
            result = Boolean.Parse(value);
            return true;
        }
      }
      catch (Exception)
      {
        result = false;
        return false;
      }
    }

    public Boolean BuildInt32(String value, out Int32 result)
    {
      try
      {
        result = Int32.Parse(value);
        return true;
      }
      catch (Exception)
      {
        result = 0;
        return false;
      }
    }

    public Boolean BuildFloat(String value, out float result)
    {
      try
      {
        result = float.Parse(value);
        return true;
      }
      catch (Exception)
      {
        result = 0f;
        return false;
      }
    }

    public Boolean BuildDouble(String value, out Double result)
    {
      try
      {
        result = Double.Parse(value);
        return true;
      }
      catch (Exception)
      {
        result = 0.0;
        return false;
      }
    }

    public Boolean BuildGuid(String value, out Guid result)
    {
      try
      {
        result = new Guid(value);
        return true;
      }
      catch (Exception)
      {
        result = Guid.Empty;
        return false;
      }
    }



  }
}
