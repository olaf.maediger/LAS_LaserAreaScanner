﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using Xml;
using UdpXmlTransfer;
using UdpTransfer;
using UCUdpTransfer;
using NetworkHardwareDevice;
using NhdMultiTemperatureController;
using UCCommon;
using UCMultiTemperatureControllerServer;
//
namespace UdpServerMultipleTemperatureController
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMainServerMTC : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICENAME = CUdpXmlMtcHeader.XML_DEVICENAME; // SERVER!!!!
    private const String INIT_DEVICETYPE = CUdpXmlMtcHeader.XML_DEVICETYPE;
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = false;//true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private CDeviceParameter FDeviceParameter;
    private CResponse FResponseRequest;
    private CResponse FResponseCommand;
    // actual NC private CCommand FCommandMeasurement;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FDeviceParameter = new CDeviceParameter();
      FDeviceParameter.Name = INIT_DEVICENAME;
      FDeviceParameter.Type = INIT_DEVICETYPE;
      FDeviceParameter.ID = Guid.NewGuid();
      IPAddress IPA;
      CUdpBase.FindIPAddressLocal(out IPA);
      FDeviceParameter.IPA = IPA;
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      btnStartStop.Text = "Open";
    }

    private void FreeProgrammerControls()
    {
      FUCMultiTemperatureControllerServer.Stop(0);
      FUCMultiTemperatureControllerServer.Stop(1);
      FUCMultiTemperatureControllerServer.Stop(2);
      FUCMultiTemperatureControllerServer.Stop(3);
      tmrStartup.Stop();
      //
      if (FResponseRequest is CResponse)
      {
        FResponseRequest.Abort();
        FResponseRequest = null;
      }
      if (FResponseCommand is CResponse)
      {
        FResponseCommand.Abort();
        FResponseCommand = null;
      }
      //if (FCommandMeasurement is CCommand)
      //{
      //  FCommandMeasurement.Abort();
      //  FCommandMeasurement = null;
      //}
    }

    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
     if (cbxAutomate.Checked)
      {
        FStartupState = 0;
        tmrStartup.Enabled = true;
      }
      else
      {
        //
        if (FResponseRequest is CResponse)
        {
          FResponseRequest.Abort();
          FResponseRequest = null;
        }
        if (FResponseCommand is CResponse)
        {
          FResponseCommand.Abort();
          FResponseCommand = null;
        }
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - ResponseRequest
    //------------------------------------------------------------------------
    // 
    private void ResponseRequestOnExecutionStart(RTaskData taskdata)
    {
    }
    private Boolean ResponseRequestOnExecutionBusy(ref RTaskData taskdata)
    {
      return true;
    }
    private void ResponseRequestOnExecutionEnd(RTaskData taskdata)
    {
    }
    private void ResponseRequestOnExecutionAbort(RTaskData taskdata)
    {
    }
    private void ResponseRequestOnDataCommandReceived(CDataCommand datacommand)
    {
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - ResponseCommand
    //------------------------------------------------------------------------
    // 
    private void ResponseCommandOnExecutionStart(RTaskData taskdata)
    {
    }
    private Boolean ResponseCommandOnExecutionBusy(ref RTaskData taskdata)
    {
      return true;
    }
    private void ResponseCommandOnExecutionEnd(RTaskData taskdata)
    {
    }
    private void ResponseCommandOnExecutionAbort(RTaskData taskdata)
    {
    }



    //private void UCChannelOnSetDataTemperature(Int32 channelindex, RDataTemperatureServer data)
    //{
    //      FCommand = new CCommand();
    //      FCommand.SetNotifier(FNotifier);
    //      // Command Header
    //      FCommand.TxdCommand.Type = CUdpXmlMtcHeader.XML_SETDATA;
    //      IPAddress IPA;
    //      CUdpBase.FindIPAddressLocal(out IPA);
    //      FCommand.TxdCommand.IPAddressSource = IPA;
    //      FCommand.TxdCommand.IPAddressTarget = DP.IPA;
    //      FCommand.TxdCommand.IPPortFrom = CUdpXmlMtcHeader.IPPORT_COMMAND_FROM;
    //      FCommand.TxdCommand.IPPortBack = CUdpXmlMtcHeader.IPPORT_COMMAND_BACK;
    //      // Command Parameter
    //      FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_CHANNEL, channelindex.ToString());
    //      FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TEMPERATUREMEAN, data.TemperatureMean.ToString());
    //      FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TEMPERATUREDELTA, data.TemperatureDelta.ToString());
    //      FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TEMPERATUREACTUAL, data.TemperatureActual.ToString());
    //      FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TIMEPERIOD, data.TimePeriod.ToString());
    //      FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TIMEDELTA, data.TimeDelta.ToString());
    //      FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_TIMEACTUAL, data.TimeActual.ToString());
    //      String SVK = CVariationKind.VariationKindToText(data.VariationKind);
    //      FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_VARIATIONKIND, SVK);
    //      FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_STEPPRESET, data.StepPreset.ToString());
    //      FCommand.TxdCommand.ParameterList.Add(CUdpXmlMtcHeader.XML_STEPACTUAL, data.StepActual.ToString());
    //      // Execute Command
    //      FCommand.Execute("CST",
    //                       CommandOnExecutionStart, CommandOnExecutionBusy,
    //                       CommandOnExecutionEnd, CommandOnExecutionAbort);
    //    }
    //  }
    //}


    private delegate void CBResponseCommandOnDataCommandReceived(CDataCommand datacommand);
    private void ResponseCommandOnDataCommandReceived(CDataCommand datacommand)
    {
      if (this.InvokeRequired)
      {
        CBResponseCommandOnDataCommandReceived CB = 
          new CBResponseCommandOnDataCommandReceived(ResponseCommandOnDataCommandReceived);
        Invoke(CB, new object[] { datacommand });
      }
      else
      {
        String Line;
        Int32 Channel;
        RDataTemperatureServer DT;
        switch (datacommand.Type)
        {
          case CUdpXmlMtcHeader.XML_GETDATA:
            Channel = Int32.Parse(datacommand.ParameterList[0].Value);
            FUCMultiTemperatureControllerServer.GetDataTemperature(Channel, out DT);
            // ResponseCommand - Header
            FResponseCommand.TxdResponse.Type = CUdpXmlMtcHeader.XML_GETDATA;
            //IPAddress IPA;
            //CUdpBase.FindIPAddressLocal(out IPA);
            //FResponseCommand.TxdResponse.IPAddressSource = IPA;
            //FResponseCommand.TxdResponse.IPAddressTarget = IPA; // CHANGE!!!!!!!!!
            //FResponseCommand.TxdResponse.IPPortFrom = CUdpXmlMtcHeader.IPPORT_COMMAND_BACK;
            // Command Parameter
            FResponseCommand.TxdResponse.ParameterList.Clear();
            FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlMtcHeader.XML_CHANNEL, Channel.ToString());
            FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlMtcHeader.XML_TEMPERATUREMEAN, DT.TemperatureMean.ToString());
            FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlMtcHeader.XML_TEMPERATUREDELTA, DT.TemperatureDelta.ToString());
            FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlMtcHeader.XML_TEMPERATUREACTUAL, DT.TemperatureActual.ToString());
            FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlMtcHeader.XML_TIMEPERIOD, DT.TimePeriod.ToString());
            FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlMtcHeader.XML_TIMEDELTA, DT.TimeDelta.ToString());
            FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlMtcHeader.XML_TIMEACTUAL, DT.TimeActual.ToString());
            String SVK = CVariationKind.VariationKindToText(DT.VariationKind);
            FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlMtcHeader.XML_VARIATIONKIND, SVK);
            FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlMtcHeader.XML_STEPPRESET, DT.StepPreset.ToString());
            FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlMtcHeader.XML_STEPACTUAL, DT.StepActual.ToString());
            break;
          case CUdpXmlMtcHeader.XML_SETDATA:
            Channel = Int32.Parse(datacommand.ParameterList[0].Value);
            FUCMultiTemperatureControllerServer.GetDataTemperature(Channel, out DT);
            DT.TemperatureMean = Double.Parse(datacommand.ParameterList[1].Value,
                                      System.Globalization.NumberStyles.AllowDecimalPoint, 
                                      System.Globalization.NumberFormatInfo.InvariantInfo);
            DT.TemperatureDelta = Double.Parse(datacommand.ParameterList[2].Value,
                                      System.Globalization.NumberStyles.AllowDecimalPoint, 
                                      System.Globalization.NumberFormatInfo.InvariantInfo);
            DT.TemperatureActual = Double.Parse(datacommand.ParameterList[3].Value,
                                      System.Globalization.NumberStyles.AllowDecimalPoint, 
                                      System.Globalization.NumberFormatInfo.InvariantInfo);
            DT.TimePeriod = Double.Parse(datacommand.ParameterList[4].Value,
                                      System.Globalization.NumberStyles.AllowDecimalPoint, 
                                      System.Globalization.NumberFormatInfo.InvariantInfo);
            DT.TimeDelta = Double.Parse(datacommand.ParameterList[5].Value,
                                      System.Globalization.NumberStyles.AllowDecimalPoint, 
                                      System.Globalization.NumberFormatInfo.InvariantInfo);
            DT.TimeActual = Double.Parse(datacommand.ParameterList[6].Value,
                                      System.Globalization.NumberStyles.AllowDecimalPoint, 
                                      System.Globalization.NumberFormatInfo.InvariantInfo);
            String SS = datacommand.ParameterList[7].Value;
            DT.VariationKind = CVariationKind.TextToVariationKind(SS);
            DT.StepPreset = Int32.Parse(datacommand.ParameterList[8].Value,
                                      System.Globalization.NumberStyles.AllowDecimalPoint, 
                                      System.Globalization.NumberFormatInfo.InvariantInfo);
            DT.StepActual = Int32.Parse(datacommand.ParameterList[9].Value,
                                      System.Globalization.NumberStyles.AllowDecimalPoint, 
                                      System.Globalization.NumberFormatInfo.InvariantInfo);
            FUCMultiTemperatureControllerServer.SetDataTemperature(Channel, DT);
            DT.Info(FUCNotifier, ">>> SETDATA");
            break;
          case CUdpXmlMtcHeader.XML_START:
            Channel = Int32.Parse(datacommand.ParameterList[0].Value);
            Line = String.Format(">>> START Channel[{0}]", Channel);
            FUCNotifier.Write(Line);
            FUCMultiTemperatureControllerServer.Start(Channel);
            break;
          case CUdpXmlMtcHeader.XML_STOP:
            Channel = Int32.Parse(datacommand.ParameterList[0].Value);
            Line = String.Format(">>> STOP Channel[{0}]", Channel);
            FUCNotifier.Write(Line);
            FUCMultiTemperatureControllerServer.Stop(Channel);
            break;
        }
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - Command
    //------------------------------------------------------------------------
    // 
    //private void CommandOnExecutionStart(RTaskData taskdata)
    //{
    //}
    //private Boolean CommandOnExecutionBusy(ref RTaskData taskdata)
    //{
    //  return true;
    //}
    //private void CommandOnExecutionEnd(RTaskData taskdata)
    //{
    //}
    //private void CommandOnExecutionAbort(RTaskData taskdata)
    //{
    //}
    //private void CommandOnDataCommandReceived(CDataCommand datacommand)
    //{
    //}
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          if (FResponseRequest is CResponse)
          {
            FResponseRequest.Abort();
            FResponseRequest = null;
          }
          if (FResponseCommand is CResponse)
          {
            FResponseCommand.Abort();
            FResponseCommand = null;
          }
          //if (FCommand is CCommand)
          //{
          //  FCommand.Abort();
          //  FCommand = null;
          //}
          return true;
        case 1:
          FResponseRequest = new CResponse();
          FResponseRequest.SetNotifier(FUCNotifier);
          FResponseRequest.SetOnDataCommandReceived(ResponseRequestOnDataCommandReceived);
          //
          FResponseRequest.RxdCommand.Type = CUdpXmlHeader.XML_FINDALLDEVICES;
          IPAddress IPA;
          CUdpBase.FindIPAddressLocal(out IPA);
          FResponseRequest.RxdCommand.IPAddressSource = IPA;
          FResponseRequest.RxdCommand.IPAddressTarget = IPA;
          FResponseRequest.RxdCommand.IPPortFrom = CUdpXmlHeader.IPPORT_REQUEST_FROM;
          FResponseRequest.RxdCommand.IPPortBack = CUdpXmlHeader.IPPORT_REQUEST_BACK;
          // no parameter
          //
          FResponseRequest.TxdResponse.Type = CUdpXmlHeader.XML_FINDALLDEVICES;
          FResponseRequest.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_NAME, FDeviceParameter.Name);
          FResponseRequest.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_TYPE, FDeviceParameter.Type);
          FResponseRequest.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_GUID, FDeviceParameter.ID.ToString());
          FResponseRequest.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_IPADDRESS, FDeviceParameter.IPA.ToString());
          //
          FResponseRequest.Execute("ResponseRequest",
                                   ResponseRequestOnExecutionStart,
                                   ResponseRequestOnExecutionBusy,
                                   ResponseRequestOnExecutionEnd,
                                   ResponseRequestOnExecutionAbort);
          return true;
        case 2:
          FResponseCommand = new CResponse();
          FResponseCommand.SetNotifier(FUCNotifier);
          FResponseCommand.SetOnDataCommandReceived(ResponseCommandOnDataCommandReceived);
          //
          FResponseCommand.RxdCommand.Type = "";
          CUdpBase.FindIPAddressLocal(out IPA);
          FResponseCommand.RxdCommand.IPAddressSource = IPA;
          FResponseCommand.RxdCommand.IPAddressTarget = IPA;
          FResponseCommand.RxdCommand.IPPortFrom = CUdpXmlHeader.IPPORT_COMMAND_FROM;
          FResponseCommand.RxdCommand.IPPortBack = CUdpXmlHeader.IPPORT_COMMAND_BACK;
          // no parameter
          //
          //FResponseCommand.TxdResponse.Type = CUdpXmlHeader.XML_FINDALLDEVICES;
          //FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_NAME, FDeviceParameter.Name);
          //FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_TYPE, FDeviceParameter.Type);
          //FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_GUID, FDeviceParameter.ID.ToString());
          //FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_IPADDRESS, FDeviceParameter.IPA.ToString());
          //
          FResponseCommand.Execute("ResponseCommand",
                                   ResponseCommandOnExecutionStart,
                                   ResponseCommandOnExecutionBusy,
                                   ResponseCommandOnExecutionEnd,
                                   ResponseCommandOnExecutionAbort);
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          //cbxAutomate.Checked = false;
          //return false;
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }

    private void btnStartStopClose_Click(object sender, EventArgs e)
    {
      if (!(FUCMultiTemperatureControllerServer.IsOpen))
      {
        FUCMultiTemperatureControllerServer.Start(0);
        FUCMultiTemperatureControllerServer.Start(1);
        FUCMultiTemperatureControllerServer.Start(2);
        FUCMultiTemperatureControllerServer.Start(3);
        btnStartStop.Text = "Stop";
      }
      else
      {
        FUCMultiTemperatureControllerServer.Stop(0);
        FUCMultiTemperatureControllerServer.Stop(1);
        FUCMultiTemperatureControllerServer.Stop(2);
        FUCMultiTemperatureControllerServer.Stop(3);
        btnStartStop.Text = "Start";
      }
    }


  }
}
