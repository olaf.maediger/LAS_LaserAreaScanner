﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using UdpTransfer;
using UdpXmlTransfer;
using UCUdpTransfer;
using NetworkHardwareDevice;
using NhdDisplay;
using NhdKeyboard;
using NhdMultiTemperatureController;
//
namespace UdpClientObserver
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMainClientNDO : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICENAME = "NetworkDeviceObserver"; // CLIENT!!!
    private const String INIT_DEVICETYPE = "NDO";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = false;//true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private static CDeviceParameter FDeviceParameter;
    private CDeviceParameterList FReceivedDevices; // only for Request for all devices!
    private CDeviceParameterList FNetworkHardwareDevicesNDO;
    private CDeviceParameterList FNetworkHardwareDevicesDIS;
    private CDeviceParameterList FNetworkHardwareDevicesKBD;
    private CDeviceParameterList FNetworkHardwareDevicesSES;
    private CDeviceParameterList FNetworkHardwareDevicesMTC;
    private CDeviceParameterList FNetworkHardwareDevicesGPS;
    //
    //private CCommand FCommand; -> CRequest
    private CRequest FRequest;
    private CResponse FResponse;
    ////////////private CCommand FCommand;
    ////////////private CResponse FResponseCommand;
    //
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FDeviceParameter = new CDeviceParameter();
      FDeviceParameter.Name = INIT_DEVICENAME;
      FDeviceParameter.Type = INIT_DEVICETYPE;
      FDeviceParameter.ID = Guid.NewGuid();
      IPAddress IPA;
      CUdpBase.FindIPAddressLocal(out IPA);
      FDeviceParameter.IPA = IPA;
      //
      FReceivedDevices = new CDeviceParameterList("ReceivedDevices");
      //
      FNetworkHardwareDevicesDIS = new CDeviceParameterList("DIS");
      FNetworkHardwareDevicesGPS = new CDeviceParameterList("GPS");
      FNetworkHardwareDevicesKBD = new CDeviceParameterList("KBD");
      FNetworkHardwareDevicesMTC = new CDeviceParameterList("MTC");
      FNetworkHardwareDevicesNDO = new CDeviceParameterList("NDO");
      FNetworkHardwareDevicesSES = new CDeviceParameterList("SES");
      //
      FUCDisplayClient.SetNotifier(FUCNotifier);
      FUCMultiTemperatureControllerClient.SetNotifier(FUCNotifier);
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      if (FRequest is CRequest)
      {
        FRequest.Abort();
        FRequest = null;
      } 
      if (FResponse is CResponse)
      {
        FResponse.Abort();
        FResponse = null;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxAutomate.Checked)
      {
        FStartupState = 0;
        tmrStartup.Enabled = true;
      }
      else
      {
        tmrStartup.Enabled = false;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper - Device
    //------------------------------------------------------------------------
    //
    private Boolean CheckAndRegisterDevice(CDeviceParameter deviceparameter)
    {
      if (FDeviceParameter.Type == deviceparameter.Type)
      {
        FNetworkHardwareDevicesNDO.Add(deviceparameter);
        String Line = String.Format("ClientNDO[{0}, {1}, {2}, {3}] registered",
                                    deviceparameter.Name, deviceparameter.Type,
                                    deviceparameter.IPA, deviceparameter.ID);
        FUCNotifier.Write(Line);
        return true;
      }
      else // Check if device is type display: "DIS"
        if (CUdpXmlDisHeader.XML_DEVICETYPE == deviceparameter.Type)
        {
          FNetworkHardwareDevicesDIS.Add(deviceparameter);
          String Line = String.Format("ServerDIS[{0}, {1}, {2}, {3}] registered",
                                      deviceparameter.Name, deviceparameter.Type,
                                      deviceparameter.IPA, deviceparameter.ID);
        }
        else // Check if device is type keyboard: "KBD"
          if (CUdpXmlKbdHeader.XML_DEVICETYPE == deviceparameter.Type)
          {
            FNetworkHardwareDevicesKBD.Add(deviceparameter);
            String Line = String.Format("ServerKBD[{0}, {1}, {2}, {3}] registered",
                                        deviceparameter.Name, deviceparameter.Type,
                                        deviceparameter.IPA, deviceparameter.ID);
          }
          else // Check if device is type MultiTemperatureController: "MTC"
            if (CUdpXmlMtcHeader.XML_DEVICETYPE == deviceparameter.Type)
            {
              FNetworkHardwareDevicesMTC.Add(deviceparameter);
              String Line = String.Format("ServerMTC[{0}, {1}, {2}, {3}] registered",
                                          deviceparameter.Name, deviceparameter.Type,
                                          deviceparameter.IPA, deviceparameter.ID);
            }
     
      return false;
    }

    private Boolean BuildParameterFindAllDevices(CDataResponse dataresponse)
    {
      try
      {
        CDeviceParameter DeviceParameter = new CDeviceParameter();
        if (dataresponse.ParameterList is CParameterList)
        {
          if (3 < dataresponse.ParameterList.Count)
          {

            Boolean Result = true;
            foreach (CParameter Parameter in dataresponse.ParameterList)
            {
              switch (Parameter.Name)
              {
                case CUdpXmlHeader.XML_NAME:
                  DeviceParameter.Name = Parameter.Value;
                  break;
                case CUdpXmlHeader.XML_TYPE:
                  DeviceParameter.Type = Parameter.Value;
                  break;
                case CUdpXmlHeader.XML_GUID:                  
                  String SG = Parameter.Value;
                  Guid DPID = Guid.Parse(SG);
                  DeviceParameter.ID = DPID;
                  break;
                case CUdpXmlHeader.XML_IPADDRESS:
                  String SA = Parameter.Value;
                  IPAddress DPIPA = IPAddress.Parse(SA);
                  DeviceParameter.IPA = DPIPA;
                  ////////////////Result &= CUdpBase.TextToIPAddress(SA, out DPIPA);
                  ////////////////if (Result)
                  ////////////////{
                  //////////////  //DeviceParameter.IPA = DPIPA;
                  ////////////////}
                  break;
                default:
                  Result = false;
                  break;
              }
            }
            if (Result)
            {
              String Line = String.Format("DeviceParameter[{0}, {1}, {2}, {3}] detected",
                                   DeviceParameter.Name, DeviceParameter.Type,
                                   DeviceParameter.IPA, DeviceParameter.ID);
              FUCNotifier.Write(Line);
              FReceivedDevices.Add(DeviceParameter);
              return Result;
            }
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //########################################################################
    //
    //------------------------------------------------------------------------
    //  Section - Callback - Response
    //------------------------------------------------------------------------
    // 
    private void ResponseOnExecutionStart(RTaskData taskdata)
    {
    }

    private Boolean ResponseOnExecutionBusy(ref RTaskData taskdata)
    {
      return true;
    }

    private void ResponseOnExecutionEnd(RTaskData taskdata)
    {
    }

    private void ResponseOnExecutionAbort(RTaskData taskdata)
    {
    }

    private void ResponseOnDataCommandReceived(CDataCommand datacommand)
    { 
    }
    //
    //########################################################################
    //
    //------------------------------------------------------------------------
    //  Section - Callback - Request
    //------------------------------------------------------------------------
    // 
    private void RequestOnExecutionStart(RTaskData taskdata)
    {
      if (FReceivedDevices is CDeviceParameterList)
      {
        FReceivedDevices.Clear();
        FReceivedDevices = null;
        FReceivedDevices = new CDeviceParameterList("ReceivedDevices");
      }
    }
    private Boolean RequestOnExecutionBusy(ref RTaskData taskdata)
    {
      return true;
    }
    private void RequestOnExecutionEnd(RTaskData taskdata)
    {
    }
    private void RequestOnExecutionAbort(RTaskData taskdata)
    {
      if (FReceivedDevices is CDeviceParameterList)
      {
        String Line;
        Line = String.Format("{0}[{1}]:", FReceivedDevices.Name, FReceivedDevices.Count);
        FUCNotifier.Write(Line);
        Int32 DPC = FReceivedDevices.Count;
        for (Int32 DPI = 0; DPI < DPC; DPI++)
        {
          CDeviceParameter DP = FReceivedDevices[DPI];
          Line = String.Format("  {0}[{1}, {2}, {3}]",
                               DP.Name, DP.Type, DP.IPA, DP.ID);
          FUCNotifier.Write(Line);
          CheckAndRegisterDevice(DP);
        }
      }
      FNetworkHardwareDevicesNDO.Info(FUCNotifier);
      FNetworkHardwareDevicesDIS.Info(FUCNotifier);
      FUCDisplayClient.SetNetworkHardwareDevicesDIS(FNetworkHardwareDevicesDIS);
      FNetworkHardwareDevicesKBD.Info(FUCNotifier);
      FNetworkHardwareDevicesSES.Info(FUCNotifier);
      FNetworkHardwareDevicesMTC.Info(FUCNotifier);
      FUCMultiTemperatureControllerClient.SetNetworkHardwareDevicesMTC(FNetworkHardwareDevicesMTC);
      FNetworkHardwareDevicesGPS.Info(FUCNotifier);
    }

    private void RequestOnDataResponseReceived(CDataResponse dataresponse)
    {
      switch (dataresponse.Type)
      {
        case CUdpXmlHeader.XML_FINDALLDEVICES:
          Byte[] ABDC = dataresponse.IPAddressSource.GetAddressBytes();
          Byte[] ABL = FDeviceParameter.IPA.GetAddressBytes();
          if (ABDC[3] != ABL[3])
          {
            FDeviceParameter.RequestFindAllDevicesExternal = true;
            FUCNotifier.Write("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            FUCNotifier.Write("Request[FindAllDevices] comes from external Device - not local!");
            FUCNotifier.Write("Dont start Request[FindAllDevices] from this Device until External finished!");
          }
          else
          {
            // set RequestFindAllDevicesExternal to false NOT HERE!
            FUCNotifier.Write("--------------------------------------------------------------");
            FUCNotifier.Write("Request[FindAllDevices] comes from this Device - not external!");
            FUCNotifier.Write("Request[FindAllDevices] from this Device can be started!");
          }
          BuildParameterFindAllDevices(dataresponse);
          break;
      }
    }    
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          if (FRequest is CRequest)
          {
            FRequest.Abort();
            FRequest = null;
          }
          if (FResponse is CResponse)
          {
            FResponse.Abort();
            FResponse = null;
          }
          FNetworkHardwareDevicesNDO.Clear();
          FNetworkHardwareDevicesDIS.Clear();
          FUCDisplayClient.SetNetworkHardwareDevicesDIS(FNetworkHardwareDevicesDIS);
          FNetworkHardwareDevicesKBD.Clear();
          FNetworkHardwareDevicesSES.Clear();
          FNetworkHardwareDevicesMTC.Clear();
          FUCMultiTemperatureControllerClient.SetNetworkHardwareDevicesMTC(FNetworkHardwareDevicesMTC);
          FNetworkHardwareDevicesGPS.Clear();
          FNetworkHardwareDevicesNDO.Info(FUCNotifier);
          FNetworkHardwareDevicesDIS.Info(FUCNotifier);
          FNetworkHardwareDevicesKBD.Info(FUCNotifier);
          FNetworkHardwareDevicesSES.Info(FUCNotifier);
          FNetworkHardwareDevicesMTC.Info(FUCNotifier);
          FNetworkHardwareDevicesGPS.Info(FUCNotifier);
          return true;
        case 1:
          FResponse = new CResponse();
          FResponse.SetNotifier(FUCNotifier);
          FResponse.SetOnDataCommandReceived(ResponseOnDataCommandReceived);
          // ???? FResponse.SetOnDataRequestReceived(ResponseRequestOnDataRequestReceived);
          //
          FResponse.RxdCommand.Type = CUdpXmlHeader.XML_FINDALLDEVICES;
          IPAddress IPA;
          CUdpBase.FindIPAddressLocal(out IPA);
          FResponse.RxdCommand.IPAddressSource = IPA;
          FResponse.RxdCommand.IPPortFrom = CUdpXmlHeader.IPPORT_REQUEST_FROM;
          FResponse.RxdCommand.IPPortBack = CUdpXmlHeader.IPPORT_REQUEST_BACK;
          // no parameter
          //
          FResponse.TxdResponse.Type = CUdpXmlHeader.XML_FINDALLDEVICES;
          FResponse.TxdResponse.IPAddressSource = IPA;
          FResponse.TxdResponse.IPAddressTarget = IPA; // MUELL
          FResponse.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_NAME, FDeviceParameter.Name);
          FResponse.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_TYPE, FDeviceParameter.Type);
          FResponse.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_GUID, FDeviceParameter.ID.ToString());
          FResponse.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_IPADDRESS, FDeviceParameter.IPA.ToString());
          //
          FResponse.Execute("Response",
                                   ResponseOnExecutionStart,
                                   ResponseOnExecutionBusy,
                                   ResponseOnExecutionEnd,
                                   ResponseOnExecutionAbort);
          return true;
        case 2:
          if (!(FDeviceParameter.RequestFindAllDevicesExternal))
          {
            FRequest = new CRequest();
            FRequest.SetNotifier(FUCNotifier);
            // ???? FCommand.SetOnDataCommandReceived(ResponseCommandOnDataCommandReceived);
            FRequest.SetOnDataResponseReceived(RequestOnDataResponseReceived);
            //
            FRequest.TxdCommand.Type = CUdpXmlHeader.XML_FINDALLDEVICES;
            IPAddress IPABC;
            CUdpBase.FindIPAddressLocalBroadcast(out IPABC);
            FRequest.TxdCommand.IPAddressSource = FDeviceParameter.IPA;
            FRequest.TxdCommand.IPAddressTarget = IPABC;
            FRequest.TxdCommand.IPPortFrom = CUdpXmlHeader.IPPORT_REQUEST_FROM;
            FRequest.TxdCommand.IPPortBack = CUdpXmlHeader.IPPORT_REQUEST_BACK;
            //
            FRequest.Execute("Request",
                             RequestOnExecutionStart,
                             RequestOnExecutionBusy,
                             RequestOnExecutionEnd,
                             RequestOnExecutionAbort);
          }
          return true;
        case 3:
         return true;
        case 4:
         return true;
        case 5:
         return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          //cbxAutomate.Checked = false;
          //return false;
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          cbxAutomate.Checked = false;
          return false;
      }
    }


  }
}

// Check if "FindAllDevices" comes from other device -> no starting of this device for his own "FindAllDevices"!!!!
//////////switch (datacommand.Type)
//////////{
//////////  case CUdpXmlHeader.XML_FINDALLDEVICES:
//////////    Byte[] ABDC = datacommand.IPAddressSource.GetAddressBytes();
//////////    Byte[] ABL = FDeviceParameter.IPA.GetAddressBytes();
//////////    if (ABDC[3] != ABL[3])
//////////    {
//////////      FDeviceParameter.RequestFindAllDevicesExternal = true;
//////////      FUCNotifier.Write("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//////////      FUCNotifier.Write("Request[FindAllDevices] comes from external Device - not local!");
//////////      FUCNotifier.Write("Dont start Request[FindAllDevices] from this Device until External finished!");
//////////    }
//////////    else
//////////    {
//////////      // set RequestFindAllDevicesExternal to false NOT HERE!
//////////      FUCNotifier.Write("--------------------------------------------------------------");
//////////      FUCNotifier.Write("Request[FindAllDevices] comes from this Device - not external!");
//////////      FUCNotifier.Write("Request[FindAllDevices] from this Device can be started!");
//////////    }
//////////    break;
//////////}

////
////########################################################################
////
////------------------------------------------------------------------------
////  Section - Callback - Command
////------------------------------------------------------------------------
//// 
//private void CommandOnExecutionStart(RTaskData taskdata)
//{
//  if (FReceivedDevices is CDeviceParameterList)
//  {
//    FReceivedDevices.Clear();
//    FReceivedDevices = null;
//    FReceivedDevices = new CDeviceParameterList("ReceivedDevices");
//  }
//}
//private Boolean CommandOnExecutionBusy(ref RTaskData taskdata)
//{
//  return true;
//}
//private void CommandOnExecutionEnd(RTaskData taskdata)
//{
//}
//private void CommandOnExecutionAbort(RTaskData taskdata)
//{
//  if (FReceivedDevices is CDeviceParameterList)
//  {
//    String Line;
//    Line = String.Format("{0}[{1}]:", FReceivedDevices.Name, FReceivedDevices.Count);
//    FUCNotifier.Write(Line);
//    Int32 DPC = FReceivedDevices.Count;
//    for (Int32 DPI = 0; DPI < DPC; DPI++)
//    {
//      CDeviceParameter DP = FReceivedDevices[DPI];
//      Line = String.Format("  {0}[{1}, {2}, {3}]",
//                           DP.Name, DP.Type, DP.IPA, DP.ID);
//      FUCNotifier.Write(Line);
//      CheckAndRegisterDevice(DP);
//    }
//  }
//  FNetworkHardwareDevicesNDO.Info(FUCNotifier);
//  FNetworkHardwareDevicesDIS.Info(FUCNotifier);
//  FUCDisplayClient.SetNetworkHardwareDevicesDIS(FNetworkHardwareDevicesDIS);
//  FNetworkHardwareDevicesKBD.Info(FUCNotifier);
//  FNetworkHardwareDevicesSES.Info(FUCNotifier);
//  FNetworkHardwareDevicesMTC.Info(FUCNotifier);
//  FNetworkHardwareDevicesGPS.Info(FUCNotifier);
//}

//private void CommandOnDataResponseReceived(CDataResponse dataresponse)
//{
//  switch (dataresponse.Type)
//  {
//    case CUdpXmlHeader.XML_FINDALLDEVICES:
//      Byte[] ABDC = dataresponse.IPAddressSource.GetAddressBytes();
//      Byte[] ABL = FDeviceParameter.IPA.GetAddressBytes();
//      if (ABDC[3] != ABL[3])
//      {
//        FDeviceParameter.RequestFindAllDevicesExternal = true;
//        FUCNotifier.Write("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//        FUCNotifier.Write("Request[FindAllDevices] comes from external Device - not local!");
//        FUCNotifier.Write("Dont start Request[FindAllDevices] from this Device until External finished!");
//      }
//      else
//      {
//        // set RequestFindAllDevicesExternal to false NOT HERE!
//        FUCNotifier.Write("--------------------------------------------------------------");
//        FUCNotifier.Write("Request[FindAllDevices] comes from this Device - not external!");
//        FUCNotifier.Write("Request[FindAllDevices] from this Device can be started!");
//      }
//      BuildParameterFindAllDevices(dataresponse);
//      break;
//    }
//}

//switch (datacommand.Type)
//{
//  case CUdpXmlHeader.XML_FINDALLDEVICES:
//    //CDataResponse DataResponse = new CDataResponse();
//    //DataResponse.Type = CUdpXmlHeader.XML_FINDALLDEVICES;
//    //DataResponse.IPAddressSource = FDeviceParameter.IPAddress;
//    //DataResponse.IPPortFrom = datacommand.IPPortBack;
//    //DataResponse.IPAddressTarget = datacommand.IPAddressSource;
//    //CParameterList ParameterList = new CParameterList();
//    //ParameterList.Add(CUdpXmlHeader.XML_NAME, FDeviceParameter.Name);
//    //ParameterList.Add(CUdpXmlHeader.XML_TYPE, FDeviceParameter.Type);
//    //ParameterList.Add(CUdpXmlHeader.XML_GUID, FDeviceParameter.ID.ToString());
//    //ParameterList.Add(CUdpXmlHeader.XML_IPADDRESS, FDeviceParameter.IPAddress.ToString());
//    //DataResponse.ParameterList = ParameterList;
//    ////
//    //String TxdText;
//    //if (FResponse.BuildTxdResponse(DataResponse, out TxdText))
//    //{
//    //}
//    break;
//  //case CUdpXmlDisHeader.XML_SETTEXT:
//  //  Int32 LocationX = Int32.Parse(datacommand.ParameterList[0].Value);
//  //  Int32 LocationY = Int32.Parse(datacommand.ParameterList[1].Value);
//  //  String Text = datacommand.ParameterList[2].Value;
//  //  String Line = String.Format(">>> SETTEXT - LX[{0}] LY[{1}] T[{2}]",
//  //                              LocationX, LocationY, Text);
//  //  FUCNotifier.Write(Line);
//  //  FUCDisplayServer.AddText(LocationX, LocationY, Text);
//  //  break;
//  //case CUdpXmlDisHeader.XML_SETFRAME:
//  //  LocationX = Int32.Parse(datacommand.ParameterList[0].Value);
//  //  LocationY = Int32.Parse(datacommand.ParameterList[1].Value);
//  //  Int32 W = Int32.Parse(datacommand.ParameterList[2].Value);
//  //  Int32 H = Int32.Parse(datacommand.ParameterList[3].Value);
//  //  Line = String.Format(">>> SETFRAME - LX[{0}] LY[{1}] W[{2}] H[{3}]",
//  //                       LocationX, LocationY, W, H);
//  //  FUCNotifier.Write(Line);
//  //  FUCDisplayServer.AddFrame(LocationX, LocationY, W, H);
//  //  break;
//}
///////}
////////////////
////////////////------------------------------------------------------------------------
////////////////  Section - Callback - Request
////////////////------------------------------------------------------------------------
//////////////// 
//////////////private void RequestOnExecutionStart(RTaskData taskdata)
//////////////{
//////////////  if (FReceivedDevices is CDeviceParameterList)
//////////////  {
//////////////    FReceivedDevices.Clear();
//////////////    FReceivedDevices = null;
//////////////    FReceivedDevices = new CDeviceParameterList("ReceivedDevices");
//////////////  }
//////////////}
//////////////private Boolean RequestOnExecutionBusy(ref RTaskData taskdata)
//////////////{
//////////////  return true;
//////////////}
//////////////private void RequestOnExecutionEnd(RTaskData taskdata)
//////////////{
//////////////}
//////////////private void RequestOnExecutionAbort(RTaskData taskdata)
//////////////{
//////////////  if (FReceivedDevices is CDeviceParameterList)
//////////////  {
//////////////    String Line;
//////////////    Line = String.Format("{0}[{1}]:", FReceivedDevices.Name, FReceivedDevices.Count); 
//////////////    FUCNotifier.Write(Line);
//////////////    Int32 DPC = FReceivedDevices.Count;
//////////////    for (Int32 DPI = 0; DPI < DPC; DPI++)
//////////////    {
//////////////      CDeviceParameter DP = FReceivedDevices[DPI];
//////////////      Line = String.Format("  {0}[{1}, {2}, {3}]", 
//////////////                           DP.Name, DP.Type, DP.IPA, DP.ID);
//////////////      FUCNotifier.Write(Line);
//////////////      CheckAndRegisterDevice(DP);
//////////////    }
//////////////  }
//////////////  FNetworkHardwareDevicesNDO.Info(FUCNotifier);
//////////////  FNetworkHardwareDevicesDIS.Info(FUCNotifier);
//////////////  FUCDisplayClient.SetNetworkHardwareDevicesDIS(FNetworkHardwareDevicesDIS);
//////////////  FNetworkHardwareDevicesKBD.Info(FUCNotifier);
//////////////  FNetworkHardwareDevicesSES.Info(FUCNotifier);
//////////////  FNetworkHardwareDevicesMTC.Info(FUCNotifier);
//////////////  FNetworkHardwareDevicesGPS.Info(FUCNotifier);
//////////////}

//////////////private void RequestOnDataResponseReceived(CDataResponse dataresponse)
//////////////{
//////////////  switch (dataresponse.Type)
//////////////  { // Common - all Requests;
//////////////    case CUdpXmlHeader.XML_FINDALLDEVICES:
//////////////      BuildParameterFindAllDevices(dataresponse);
//////////////      break;
//////////////  }
//////////////}
