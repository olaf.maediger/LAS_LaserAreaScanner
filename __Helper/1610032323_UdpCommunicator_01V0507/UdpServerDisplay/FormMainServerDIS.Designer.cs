﻿namespace UdpServerDisplay
{
  partial class FormMainServerDIS
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlProtocol = new System.Windows.Forms.Panel();
      this.FUCSerialNumber = new UCSerialNumber.CUCSerialNumber();
      this.splProtocol = new System.Windows.Forms.Splitter();
      this.tmrStartup = new System.Windows.Forms.Timer(this.components);
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.mitSystem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.mitConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCDefault = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCResetToDefaultConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveAutomaticAtProgramEnd = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCStartup = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitElevator = new System.Windows.Forms.ToolStripMenuItem();
      this.mitStartSimulation = new System.Windows.Forms.ToolStripMenuItem();
      this.mitProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPEditParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPClearProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
      this.diagnosticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPLoadDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPSaveDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSendDiagnosticEmail = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHelp = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHContents = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHTopic = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHSearch = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterApplicationProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterDeviceProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.DialogSaveInitdata = new System.Windows.Forms.SaveFileDialog();
      this.FHelpProvider = new System.Windows.Forms.HelpProvider();
      this.DialogLoadInitdata = new System.Windows.Forms.OpenFileDialog();
      this.tbcMain = new System.Windows.Forms.TabControl();
      this.tbpDisplay = new System.Windows.Forms.TabPage();
      this.FUCDisplayServer = new UCDisplayServer.CUCDisplayServer();
      this.tbpUdp = new System.Windows.Forms.TabPage();
      this.pnlTop = new System.Windows.Forms.Panel();
      this.label3 = new System.Windows.Forms.Label();
      this.FUCIPAddressLocal = new UCUdpTransfer.CUCIpAddress();
      this.label2 = new System.Windows.Forms.Label();
      this.tbxMessageReceived = new System.Windows.Forms.TextBox();
      this.FUCIpEndPointTransmitter = new UCUdpTransfer.CUCIpEndPoint();
      this.FUCIpEndPointReceiver = new UCUdpTransfer.CUCIpEndPoint();
      this.label1 = new System.Windows.Forms.Label();
      this.tbxMessageTransmit = new System.Windows.Forms.TextBox();
      this.cbxEnableReceiver = new System.Windows.Forms.CheckBox();
      this.cbxEnableTransmitter = new System.Windows.Forms.CheckBox();
      this.cbxAutomate = new System.Windows.Forms.CheckBox();
      this.pnlProtocol.SuspendLayout();
      this.mstMain.SuspendLayout();
      this.tbcMain.SuspendLayout();
      this.tbpDisplay.SuspendLayout();
      this.tbpUdp.SuspendLayout();
      this.pnlTop.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlProtocol
      // 
      this.pnlProtocol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlProtocol.Controls.Add(this.FUCSerialNumber);
      this.pnlProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlProtocol.Location = new System.Drawing.Point(0, 437);
      this.pnlProtocol.Name = "pnlProtocol";
      this.pnlProtocol.Size = new System.Drawing.Size(819, 95);
      this.pnlProtocol.TabIndex = 133;
      // 
      // FUCSerialNumber
      // 
      this.FUCSerialNumber.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCSerialNumber.Location = new System.Drawing.Point(0, 0);
      this.FUCSerialNumber.Name = "FUCSerialNumber";
      this.FUCSerialNumber.Size = new System.Drawing.Size(817, 64);
      this.FUCSerialNumber.TabIndex = 113;
      this.FUCSerialNumber.Visible = false;
      // 
      // splProtocol
      // 
      this.splProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splProtocol.Location = new System.Drawing.Point(0, 434);
      this.splProtocol.Name = "splProtocol";
      this.splProtocol.Size = new System.Drawing.Size(819, 3);
      this.splProtocol.TabIndex = 134;
      this.splProtocol.TabStop = false;
      // 
      // tmrStartup
      // 
      this.tmrStartup.Interval = 1;
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSystem,
            this.mitConfiguration,
            this.mitElevator,
            this.mitProtocol,
            this.mitHelp});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(819, 24);
      this.mstMain.TabIndex = 135;
      this.mstMain.Text = "System";
      // 
      // mitSystem
      // 
      this.mitSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSQuit});
      this.mitSystem.Name = "mitSystem";
      this.mitSystem.Size = new System.Drawing.Size(57, 20);
      this.mitSystem.Text = "&System";
      // 
      // mitSQuit
      // 
      this.mitSQuit.Name = "mitSQuit";
      this.mitSQuit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
      this.mitSQuit.Size = new System.Drawing.Size(140, 22);
      this.mitSQuit.Text = "&Quit";
      // 
      // mitConfiguration
      // 
      this.mitConfiguration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitCDefault,
            this.mitCResetToDefaultConfiguration,
            this.mitCSaveAutomaticAtProgramEnd,
            this.mitCStartup,
            this.mitCLoadStartupConfiguration,
            this.mitCSaveStartupConfiguration,
            this.mitCShowEditStartupConfiguration,
            this.toolStripMenuItem7,
            this.mitCLoadSelectableConfiguration,
            this.mitCSaveSelectableConfiguration,
            this.mitCShowEditSelectableConfiguration});
      this.mitConfiguration.Name = "mitConfiguration";
      this.mitConfiguration.Size = new System.Drawing.Size(93, 20);
      this.mitConfiguration.Text = "&Configuration";
      // 
      // mitCDefault
      // 
      this.mitCDefault.Enabled = false;
      this.mitCDefault.Name = "mitCDefault";
      this.mitCDefault.Size = new System.Drawing.Size(300, 22);
      this.mitCDefault.Text = "- Default -";
      // 
      // mitCResetToDefaultConfiguration
      // 
      this.mitCResetToDefaultConfiguration.Name = "mitCResetToDefaultConfiguration";
      this.mitCResetToDefaultConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
      this.mitCResetToDefaultConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCResetToDefaultConfiguration.Text = "Reset to Default-Configuration";
      // 
      // mitCSaveAutomaticAtProgramEnd
      // 
      this.mitCSaveAutomaticAtProgramEnd.Name = "mitCSaveAutomaticAtProgramEnd";
      this.mitCSaveAutomaticAtProgramEnd.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveAutomaticAtProgramEnd.Text = "Save automatic at program end";
      // 
      // mitCStartup
      // 
      this.mitCStartup.Enabled = false;
      this.mitCStartup.Name = "mitCStartup";
      this.mitCStartup.Size = new System.Drawing.Size(300, 22);
      this.mitCStartup.Text = "- Startup -";
      // 
      // mitCLoadStartupConfiguration
      // 
      this.mitCLoadStartupConfiguration.Name = "mitCLoadStartupConfiguration";
      this.mitCLoadStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
      this.mitCLoadStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadStartupConfiguration.Text = "Load Startup-Configuration";
      // 
      // mitCSaveStartupConfiguration
      // 
      this.mitCSaveStartupConfiguration.Name = "mitCSaveStartupConfiguration";
      this.mitCSaveStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
      this.mitCSaveStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveStartupConfiguration.Text = "Save Startup-Configuration";
      // 
      // mitCShowEditStartupConfiguration
      // 
      this.mitCShowEditStartupConfiguration.Name = "mitCShowEditStartupConfiguration";
      this.mitCShowEditStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditStartupConfiguration.Text = "Show / Edit Startup-Configuration";
      // 
      // toolStripMenuItem7
      // 
      this.toolStripMenuItem7.Enabled = false;
      this.toolStripMenuItem7.Name = "toolStripMenuItem7";
      this.toolStripMenuItem7.Size = new System.Drawing.Size(300, 22);
      this.toolStripMenuItem7.Text = "- Named -";
      // 
      // mitCLoadSelectableConfiguration
      // 
      this.mitCLoadSelectableConfiguration.Name = "mitCLoadSelectableConfiguration";
      this.mitCLoadSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadSelectableConfiguration.Text = "Load Selectable-Configuration ";
      // 
      // mitCSaveSelectableConfiguration
      // 
      this.mitCSaveSelectableConfiguration.Name = "mitCSaveSelectableConfiguration";
      this.mitCSaveSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveSelectableConfiguration.Text = "Save Selectable-Configuration";
      // 
      // mitCShowEditSelectableConfiguration
      // 
      this.mitCShowEditSelectableConfiguration.Name = "mitCShowEditSelectableConfiguration";
      this.mitCShowEditSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditSelectableConfiguration.Text = "Show / Edit Selectable-Configuration";
      // 
      // mitElevator
      // 
      this.mitElevator.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitStartSimulation});
      this.mitElevator.Name = "mitElevator";
      this.mitElevator.Size = new System.Drawing.Size(106, 20);
      this.mitElevator.Text = "Communication";
      // 
      // mitStartSimulation
      // 
      this.mitStartSimulation.Name = "mitStartSimulation";
      this.mitStartSimulation.Size = new System.Drawing.Size(98, 22);
      this.mitStartSimulation.Text = "Start";
      // 
      // mitProtocol
      // 
      this.mitProtocol.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitPEditParameter,
            this.mitPClearProtocol,
            this.mitPCopyToClipboard,
            this.diagnosticToolStripMenuItem,
            this.mitPLoadDiagnosticFile,
            this.mitPSaveDiagnosticFile,
            this.mitSendDiagnosticEmail});
      this.mitProtocol.Name = "mitProtocol";
      this.mitProtocol.Size = new System.Drawing.Size(64, 20);
      this.mitProtocol.Text = "&Protocol";
      // 
      // mitPEditParameter
      // 
      this.mitPEditParameter.Name = "mitPEditParameter";
      this.mitPEditParameter.Size = new System.Drawing.Size(171, 22);
      this.mitPEditParameter.Text = "Edit Parameter";
      // 
      // mitPClearProtocol
      // 
      this.mitPClearProtocol.Name = "mitPClearProtocol";
      this.mitPClearProtocol.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
      this.mitPClearProtocol.Size = new System.Drawing.Size(171, 22);
      this.mitPClearProtocol.Text = "Clear";
      // 
      // mitPCopyToClipboard
      // 
      this.mitPCopyToClipboard.Name = "mitPCopyToClipboard";
      this.mitPCopyToClipboard.Size = new System.Drawing.Size(171, 22);
      this.mitPCopyToClipboard.Text = "Copy to Clipboard";
      // 
      // diagnosticToolStripMenuItem
      // 
      this.diagnosticToolStripMenuItem.Enabled = false;
      this.diagnosticToolStripMenuItem.Name = "diagnosticToolStripMenuItem";
      this.diagnosticToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
      this.diagnosticToolStripMenuItem.Text = "- Diagnostic -";
      // 
      // mitPLoadDiagnosticFile
      // 
      this.mitPLoadDiagnosticFile.Name = "mitPLoadDiagnosticFile";
      this.mitPLoadDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPLoadDiagnosticFile.Text = "Load File";
      // 
      // mitPSaveDiagnosticFile
      // 
      this.mitPSaveDiagnosticFile.Name = "mitPSaveDiagnosticFile";
      this.mitPSaveDiagnosticFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
      this.mitPSaveDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPSaveDiagnosticFile.Text = "Save File";
      // 
      // mitSendDiagnosticEmail
      // 
      this.mitSendDiagnosticEmail.Name = "mitSendDiagnosticEmail";
      this.mitSendDiagnosticEmail.Size = new System.Drawing.Size(171, 22);
      this.mitSendDiagnosticEmail.Text = "Send Email";
      // 
      // mitHelp
      // 
      this.mitHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitHContents,
            this.mitHTopic,
            this.mitHSearch,
            this.mitHEnterApplicationProductKey,
            this.mitHEnterDeviceProductKey,
            this.mitHAbout});
      this.mitHelp.Name = "mitHelp";
      this.mitHelp.Size = new System.Drawing.Size(44, 20);
      this.mitHelp.Text = "Help";
      // 
      // mitHContents
      // 
      this.mitHContents.Name = "mitHContents";
      this.mitHContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.mitHContents.Size = new System.Drawing.Size(234, 22);
      this.mitHContents.Text = "Contents";
      // 
      // mitHTopic
      // 
      this.mitHTopic.Name = "mitHTopic";
      this.mitHTopic.Size = new System.Drawing.Size(234, 22);
      this.mitHTopic.Text = "Topic";
      // 
      // mitHSearch
      // 
      this.mitHSearch.Name = "mitHSearch";
      this.mitHSearch.Size = new System.Drawing.Size(234, 22);
      this.mitHSearch.Text = "Search";
      // 
      // mitHEnterApplicationProductKey
      // 
      this.mitHEnterApplicationProductKey.Name = "mitHEnterApplicationProductKey";
      this.mitHEnterApplicationProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterApplicationProductKey.Text = "Enter Application Product-Key";
      // 
      // mitHEnterDeviceProductKey
      // 
      this.mitHEnterDeviceProductKey.Name = "mitHEnterDeviceProductKey";
      this.mitHEnterDeviceProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterDeviceProductKey.Text = "Enter Device Product-Key";
      // 
      // mitHAbout
      // 
      this.mitHAbout.Name = "mitHAbout";
      this.mitHAbout.ShortcutKeys = System.Windows.Forms.Keys.F2;
      this.mitHAbout.Size = new System.Drawing.Size(234, 22);
      this.mitHAbout.Text = "About";
      // 
      // DialogSaveInitdata
      // 
      this.DialogSaveInitdata.DefaultExt = "ini.xml";
      this.DialogSaveInitdata.FileName = "Initdata";
      this.DialogSaveInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogSaveInitdata.Title = "MDesign: Save Initdata";
      // 
      // DialogLoadInitdata
      // 
      this.DialogLoadInitdata.DefaultExt = "ini.xml";
      this.DialogLoadInitdata.FileName = "Initdata";
      this.DialogLoadInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogLoadInitdata.Title = "MDesign: Load Initdata";
      // 
      // tbcMain
      // 
      this.tbcMain.Alignment = System.Windows.Forms.TabAlignment.Bottom;
      this.tbcMain.Controls.Add(this.tbpDisplay);
      this.tbcMain.Controls.Add(this.tbpUdp);
      this.tbcMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcMain.Location = new System.Drawing.Point(0, 24);
      this.tbcMain.Name = "tbcMain";
      this.tbcMain.SelectedIndex = 0;
      this.tbcMain.Size = new System.Drawing.Size(819, 410);
      this.tbcMain.TabIndex = 136;
      // 
      // tbpDisplay
      // 
      this.tbpDisplay.Controls.Add(this.FUCDisplayServer);
      this.tbpDisplay.Location = new System.Drawing.Point(4, 4);
      this.tbpDisplay.Name = "tbpDisplay";
      this.tbpDisplay.Padding = new System.Windows.Forms.Padding(3);
      this.tbpDisplay.Size = new System.Drawing.Size(811, 384);
      this.tbpDisplay.TabIndex = 6;
      this.tbpDisplay.Text = "Display";
      this.tbpDisplay.UseVisualStyleBackColor = true;
      // 
      // FUCDisplayServer
      // 
      this.FUCDisplayServer.BackColor = System.Drawing.Color.Beige;
      this.FUCDisplayServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.FUCDisplayServer.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCDisplayServer.Location = new System.Drawing.Point(3, 3);
      this.FUCDisplayServer.Name = "FUCDisplayServer";
      this.FUCDisplayServer.Size = new System.Drawing.Size(805, 378);
      this.FUCDisplayServer.TabIndex = 0;
      // 
      // tbpUdp
      // 
      this.tbpUdp.Controls.Add(this.pnlTop);
      this.tbpUdp.Location = new System.Drawing.Point(4, 4);
      this.tbpUdp.Name = "tbpUdp";
      this.tbpUdp.Size = new System.Drawing.Size(811, 384);
      this.tbpUdp.TabIndex = 5;
      this.tbpUdp.Text = "Udp";
      // 
      // pnlTop
      // 
      this.pnlTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlTop.Controls.Add(this.label3);
      this.pnlTop.Controls.Add(this.FUCIPAddressLocal);
      this.pnlTop.Controls.Add(this.label2);
      this.pnlTop.Controls.Add(this.tbxMessageReceived);
      this.pnlTop.Controls.Add(this.FUCIpEndPointTransmitter);
      this.pnlTop.Controls.Add(this.FUCIpEndPointReceiver);
      this.pnlTop.Controls.Add(this.label1);
      this.pnlTop.Controls.Add(this.tbxMessageTransmit);
      this.pnlTop.Controls.Add(this.cbxEnableReceiver);
      this.pnlTop.Controls.Add(this.cbxEnableTransmitter);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(811, 148);
      this.pnlTop.TabIndex = 138;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(28, 24);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(84, 13);
      this.label3.TabIndex = 11;
      this.label3.Text = "IPAddress Local";
      // 
      // FUCIPAddressLocal
      // 
      this.FUCIPAddressLocal.Location = new System.Drawing.Point(122, 9);
      this.FUCIPAddressLocal.Name = "FUCIPAddressLocal";
      this.FUCIPAddressLocal.Size = new System.Drawing.Size(160, 33);
      this.FUCIPAddressLocal.TabIndex = 10;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(348, 104);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(96, 13);
      this.label2.TabIndex = 9;
      this.label2.Text = "MessageReceived";
      // 
      // tbxMessageReceived
      // 
      this.tbxMessageReceived.Location = new System.Drawing.Point(444, 101);
      this.tbxMessageReceived.Name = "tbxMessageReceived";
      this.tbxMessageReceived.ReadOnly = true;
      this.tbxMessageReceived.Size = new System.Drawing.Size(349, 20);
      this.tbxMessageReceived.TabIndex = 8;
      this.tbxMessageReceived.Text = "---";
      // 
      // FUCIpEndPointTransmitter
      // 
      this.FUCIpEndPointTransmitter.BackColor = System.Drawing.SystemColors.Info;
      this.FUCIpEndPointTransmitter.Location = new System.Drawing.Point(122, 50);
      this.FUCIpEndPointTransmitter.Name = "FUCIpEndPointTransmitter";
      this.FUCIpEndPointTransmitter.Size = new System.Drawing.Size(212, 33);
      this.FUCIpEndPointTransmitter.TabIndex = 7;
      // 
      // FUCIpEndPointReceiver
      // 
      this.FUCIpEndPointReceiver.BackColor = System.Drawing.SystemColors.Info;
      this.FUCIpEndPointReceiver.Location = new System.Drawing.Point(122, 88);
      this.FUCIpEndPointReceiver.Name = "FUCIpEndPointReceiver";
      this.FUCIpEndPointReceiver.Size = new System.Drawing.Size(212, 33);
      this.FUCIpEndPointReceiver.TabIndex = 6;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(348, 64);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(90, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "MessageTransmit";
      // 
      // tbxMessageTransmit
      // 
      this.tbxMessageTransmit.Location = new System.Drawing.Point(444, 61);
      this.tbxMessageTransmit.Name = "tbxMessageTransmit";
      this.tbxMessageTransmit.Size = new System.Drawing.Size(349, 20);
      this.tbxMessageTransmit.TabIndex = 4;
      this.tbxMessageTransmit.Text = "Server-Transmitter: Hello from Server";
      // 
      // cbxEnableReceiver
      // 
      this.cbxEnableReceiver.AutoSize = true;
      this.cbxEnableReceiver.Location = new System.Drawing.Point(8, 103);
      this.cbxEnableReceiver.Name = "cbxEnableReceiver";
      this.cbxEnableReceiver.Size = new System.Drawing.Size(105, 17);
      this.cbxEnableReceiver.TabIndex = 1;
      this.cbxEnableReceiver.Text = "Enable Receiver";
      this.cbxEnableReceiver.UseVisualStyleBackColor = true;
      // 
      // cbxEnableTransmitter
      // 
      this.cbxEnableTransmitter.AutoSize = true;
      this.cbxEnableTransmitter.Location = new System.Drawing.Point(8, 63);
      this.cbxEnableTransmitter.Name = "cbxEnableTransmitter";
      this.cbxEnableTransmitter.Size = new System.Drawing.Size(114, 17);
      this.cbxEnableTransmitter.TabIndex = 0;
      this.cbxEnableTransmitter.Text = "Enable Transmitter";
      this.cbxEnableTransmitter.UseVisualStyleBackColor = true;
      // 
      // cbxAutomate
      // 
      this.cbxAutomate.AutoSize = true;
      this.cbxAutomate.Location = new System.Drawing.Point(385, 5);
      this.cbxAutomate.Name = "cbxAutomate";
      this.cbxAutomate.Size = new System.Drawing.Size(71, 17);
      this.cbxAutomate.TabIndex = 138;
      this.cbxAutomate.Text = "Automate";
      this.cbxAutomate.UseVisualStyleBackColor = true;
      this.cbxAutomate.CheckedChanged += new System.EventHandler(this.cbxAutomate_CheckedChanged);
      // 
      // FormMainServerDIS
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(819, 532);
      this.Controls.Add(this.cbxAutomate);
      this.Controls.Add(this.tbcMain);
      this.Controls.Add(this.mstMain);
      this.Controls.Add(this.splProtocol);
      this.Controls.Add(this.pnlProtocol);
      this.Name = "FormMainServerDIS";
      this.Text = "Form1";
      this.pnlProtocol.ResumeLayout(false);
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.tbcMain.ResumeLayout(false);
      this.tbpDisplay.ResumeLayout(false);
      this.tbpUdp.ResumeLayout(false);
      this.pnlTop.ResumeLayout(false);
      this.pnlTop.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel pnlProtocol;
    private UCSerialNumber.CUCSerialNumber FUCSerialNumber;
    private System.Windows.Forms.Splitter splProtocol;
    private System.Windows.Forms.Timer tmrStartup;
    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem mitSystem;
    private System.Windows.Forms.ToolStripMenuItem mitSQuit;
    private System.Windows.Forms.ToolStripMenuItem mitConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCDefault;
    private System.Windows.Forms.ToolStripMenuItem mitCResetToDefaultConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveAutomaticAtProgramEnd;
    private System.Windows.Forms.ToolStripMenuItem mitCStartup;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitElevator;
    private System.Windows.Forms.ToolStripMenuItem mitStartSimulation;
    private System.Windows.Forms.ToolStripMenuItem mitProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPEditParameter;
    private System.Windows.Forms.ToolStripMenuItem mitPClearProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPCopyToClipboard;
    private System.Windows.Forms.ToolStripMenuItem diagnosticToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitPLoadDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitPSaveDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitSendDiagnosticEmail;
    private System.Windows.Forms.ToolStripMenuItem mitHelp;
    private System.Windows.Forms.ToolStripMenuItem mitHContents;
    private System.Windows.Forms.ToolStripMenuItem mitHTopic;
    private System.Windows.Forms.ToolStripMenuItem mitHSearch;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterApplicationProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterDeviceProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHAbout;
    private System.Windows.Forms.SaveFileDialog DialogSaveInitdata;
    private System.Windows.Forms.HelpProvider FHelpProvider;
    private System.Windows.Forms.OpenFileDialog DialogLoadInitdata;
    private System.Windows.Forms.TabControl tbcMain;
    private System.Windows.Forms.TabPage tbpUdp;
    private System.Windows.Forms.CheckBox cbxAutomate;
    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.Label label3;
    private UCUdpTransfer.CUCIpAddress FUCIPAddressLocal;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox tbxMessageReceived;
    private UCUdpTransfer.CUCIpEndPoint FUCIpEndPointTransmitter;
    private UCUdpTransfer.CUCIpEndPoint FUCIpEndPointReceiver;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox tbxMessageTransmit;
    private System.Windows.Forms.CheckBox cbxEnableReceiver;
    private System.Windows.Forms.CheckBox cbxEnableTransmitter;
    private System.Windows.Forms.TabPage tbpDisplay;
    private UCDisplayServer.CUCDisplayServer FUCDisplayServer;
  }
}

