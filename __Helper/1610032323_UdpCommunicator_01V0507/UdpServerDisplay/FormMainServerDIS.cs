﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using Xml;
using UdpXmlTransfer;
using UdpTransfer;
using UCUdpTransfer;
using NetworkHardwareDevice;
using NhdDisplay;
using UCDisplayServer;
//
namespace UdpServerDisplay
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMainServerDIS : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICENAME = CUdpXmlDisHeader.XML_DEVICENAME; // SERVER!!!!
    private const String INIT_DEVICETYPE = CUdpXmlDisHeader.XML_DEVICETYPE;
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = false;//true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private CDeviceParameter FDeviceParameter;
    private CResponse FResponseRequest;
    private CResponse FResponseCommand;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FDeviceParameter = new CDeviceParameter();
      FDeviceParameter.Name = INIT_DEVICENAME;
      FDeviceParameter.Type = INIT_DEVICETYPE;
      FDeviceParameter.ID = Guid.NewGuid();
      IPAddress IPA;
      CUdpBase.FindIPAddressLocal(out IPA);
      FDeviceParameter.IPA = IPA;
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      if (FResponseRequest is CResponse)
      {
        FResponseRequest.Abort();
        FResponseRequest = null;
      }
      if (FResponseCommand is CResponse)
      {
        FResponseCommand.Abort();
        FResponseCommand = null;
      }
      ////////////if (FCommand is CCommand)
      ////////////{
      ////////////  FCommand.Abort();
      ////////////  FCommand = null;
      ////////////}
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
     if (cbxAutomate.Checked)
      {
        FStartupState = 0;
        tmrStartup.Enabled = true;
      }
      else
      {
        tmrStartup.Enabled = false;
        //
        if (FResponseRequest is CResponse)
        {
          FResponseRequest.Abort();
          FResponseRequest = null;
        }
        if (FResponseCommand is CResponse)
        {
          FResponseCommand.Abort();
          FResponseCommand = null;
        }
        ////////////if (FCommand is CCommand)
        ////////////{
        ////////////  FCommand.Abort();
        ////////////  FCommand = null;
        ////////////}
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - ResponseRequest
    //------------------------------------------------------------------------
    // 
    private void ResponseRequestOnExecutionStart(RTaskData taskdata)
    {
    }
    private Boolean ResponseRequestOnExecutionBusy(ref RTaskData taskdata)
    {
      return true;
    }
    private void ResponseRequestOnExecutionEnd(RTaskData taskdata)
    {
    }
    private void ResponseRequestOnExecutionAbort(RTaskData taskdata)
    {
    }
    private void ResponseRequestOnDataCommandReceived(CDataCommand datacommand)
    {
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - ResponseCommand
    //------------------------------------------------------------------------
    // 
    private void ResponseCommandOnExecutionStart(RTaskData taskdata)
    {
    }
    private Boolean ResponseCommandOnExecutionBusy(ref RTaskData taskdata)
    {
      return true;
    }
    private void ResponseCommandOnExecutionEnd(RTaskData taskdata)
    {
    }
    private void ResponseCommandOnExecutionAbort(RTaskData taskdata)
    {
    }
    private void ResponseCommandOnDataCommandReceived(CDataCommand datacommand)
    {
      switch (datacommand.Type)
      {
        case CUdpXmlDisHeader.XML_CLEARDISPLAY:
          String Line = String.Format(">>> CLEARDISPLAY - ");
          FUCNotifier.Write(Line);
          FUCDisplayServer.ClearDisplay();
          break;
        case CUdpXmlDisHeader.XML_SETTEXT:
          Int32 LocationX = Int32.Parse(datacommand.ParameterList[0].Value);
          Int32 LocationY = Int32.Parse(datacommand.ParameterList[1].Value);
          String Text = datacommand.ParameterList[2].Value;
          Line = String.Format(">>> SETTEXT - LX[{0}] LY[{1}] T[{2}]",
                               LocationX, LocationY, Text);
          FUCNotifier.Write(Line);
          FUCDisplayServer.AddText(LocationX, LocationY, Text);
          break;
        case CUdpXmlDisHeader.XML_SETFRAME:
          LocationX = Int32.Parse(datacommand.ParameterList[0].Value);
          LocationY = Int32.Parse(datacommand.ParameterList[1].Value);
          Int32 W = Int32.Parse(datacommand.ParameterList[2].Value);
          Int32 H = Int32.Parse(datacommand.ParameterList[3].Value);
          Line = String.Format(">>> SETFRAME - LX[{0}] LY[{1}] W[{2}] H[{3}]",
                               LocationX, LocationY, W, H);
          FUCNotifier.Write(Line);
          FUCDisplayServer.AddFrame(LocationX, LocationY, W, H);
          break;
      }
    }
    ////
    ////------------------------------------------------------------------------
    ////  Section - Callback - Command
    ////------------------------------------------------------------------------
    //// 
    //private void CommandOnExecutionStart(RTaskData taskdata)
    //{
    //}
    //private Boolean CommandOnExecutionBusy(ref RTaskData taskdata)
    //{
    //  return true;
    //}
    //private void CommandOnExecutionEnd(RTaskData taskdata)
    //{
    //}
    //private void CommandOnExecutionAbort(RTaskData taskdata)
    //{
    //}
    //private void CommandOnCommandReceived(CDataCommand datacommand)
    //{
    //}
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          if (FResponseRequest is CResponse)
          {
            FResponseRequest.Abort();
            FResponseRequest = null;
          }
          if (FResponseCommand is CResponse)
          {
            FResponseCommand.Abort();
            FResponseCommand = null;
          }
          ////////////if (FCommand is CCommand)
          ////////////{
          ////////////  FCommand.Abort();
          ////////////  FCommand = null;
          ////////////}
          return true;
        case 1:
          FResponseRequest = new CResponse();
          FResponseRequest.SetNotifier(FUCNotifier);
          FResponseRequest.SetOnDataCommandReceived(ResponseRequestOnDataCommandReceived);
          //
          FResponseRequest.RxdCommand.Type = CUdpXmlHeader.XML_FINDALLDEVICES;
          IPAddress IPA;
          CUdpBase.FindIPAddressLocal(out IPA);
          FResponseRequest.RxdCommand.IPAddressSource = IPA;
          FResponseRequest.RxdCommand.IPAddressTarget = IPA;
          FResponseRequest.RxdCommand.IPPortFrom = CUdpXmlHeader.IPPORT_REQUEST_FROM;
          FResponseRequest.RxdCommand.IPPortBack = CUdpXmlHeader.IPPORT_REQUEST_BACK;
          // no parameter
          //
          FResponseRequest.TxdResponse.Type = CUdpXmlHeader.XML_FINDALLDEVICES;
          FResponseRequest.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_NAME, FDeviceParameter.Name);
          FResponseRequest.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_TYPE, FDeviceParameter.Type);
          FResponseRequest.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_GUID, FDeviceParameter.ID.ToString());
          FResponseRequest.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_IPADDRESS, FDeviceParameter.IPA.ToString());
          //
          FResponseRequest.Execute("ResponseRequest",
                                   ResponseRequestOnExecutionStart,
                                   ResponseRequestOnExecutionBusy,
                                   ResponseRequestOnExecutionEnd,
                                   ResponseRequestOnExecutionAbort);
          return true;
        case 2:
          FResponseCommand = new CResponse();
          FResponseCommand.SetNotifier(FUCNotifier);
          FResponseCommand.SetOnDataCommandReceived(ResponseCommandOnDataCommandReceived);
          //
          FResponseCommand.RxdCommand.Type = CUdpXmlDisHeader.XML_SETTEXT;
          CUdpBase.FindIPAddressLocal(out IPA);
          FResponseCommand.RxdCommand.IPAddressSource = IPA;
          FResponseCommand.RxdCommand.IPAddressTarget = IPA;
          FResponseCommand.RxdCommand.IPPortFrom = CUdpXmlHeader.IPPORT_COMMAND_FROM;
          FResponseCommand.RxdCommand.IPPortBack = CUdpXmlHeader.IPPORT_COMMAND_BACK;
          // no parameter
          //
          //FResponseCommand.TxdResponse.Type = CUdpXmlHeader.XML_FINDALLDEVICES;
          //FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_NAME, FDeviceParameter.Name);
          //FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_TYPE, FDeviceParameter.Type);
          //FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_GUID, FDeviceParameter.ID.ToString());
          //FResponseCommand.TxdResponse.ParameterList.Add(CUdpXmlHeader.XML_IPADDRESS, FDeviceParameter.IPA.ToString());
          //
          FResponseCommand.Execute("ResponseCommand",
                                   ResponseCommandOnExecutionStart,
                                   ResponseCommandOnExecutionBusy,
                                   ResponseCommandOnExecutionEnd,
                                   ResponseCommandOnExecutionAbort);
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          //cbxAutomate.Checked = false;
          //return false;
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }


  }
}
