﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using TextFile;
using UCNotifier;
//
//!!!!!!!!!!using XmlFile;
//!!!!!!!!!!using NLExpression;
//!!!!!!!!!!using NLStatement;
//!!!!!!!!!!using NLProgram;
//!!!!!!!!!!using NLStatementComPort;
//
namespace UCTerminal
{ //
  //-------------------------------
  //  Segment - Type
  //-------------------------------
  //
  public delegate void DOnSendData(String data, Int32 delaycharacter, Int32 delaycr, Int32 delaylf);
  //
  //##################################################
  //  Segment - Main - CUCTerminal
  //##################################################
  //
  public partial class CUCTerminal : UserControl
  { //
    //-------------------------------
    //  Segment - Constant
    //-------------------------------
    // Color RXD
    private Byte INIT_COLORRXD_A = 0xFF;
    private Byte INIT_COLORRXD_R = 0xFF;
    private Byte INIT_COLORRXD_G = 0x00;
    private Byte INIT_COLORRXD_B = 0x00;
    // Color TXD
    private Byte INIT_COLORTXD_A = 0xFF;
    private Byte INIT_COLORTXD_R = 0x00;
    private Byte INIT_COLORTXD_G = 0x00;
    private Byte INIT_COLORTXD_B = 0xFF;
    //
    private String FILENAME_COMMANDS = "TerminalStatements.txt";
    //
    private Char CARRIAGERETURN = '\r';
    private Char LINEFEED = '\n';
    //
    //-------------------------------
    //  Segment - Field
    //-------------------------------
    //
    private CNotifier FNotifier;
    private DOnSendData FOnSendData;
    private Color FTxdColor;
    private Color FRxdColor;
    //
    //private CProgram FProgram;
    //
    //-------------------------------
    //  Segment - Constructor
    //-------------------------------
    //
    public CUCTerminal()
    {
      InitializeComponent();
      FTxdColor = Color.FromArgb(INIT_COLORTXD_A, INIT_COLORTXD_R, INIT_COLORTXD_G, INIT_COLORTXD_B);
      FRxdColor = Color.FromArgb(INIT_COLORRXD_A, INIT_COLORRXD_R, INIT_COLORRXD_G, INIT_COLORRXD_B);
      cbxSendData.Items.Clear();
      LoadText(FILENAME_COMMANDS);
      //FProgram = new CProgram();
    }
    //
    //-------------------------------
    //  Segment - Property
    //-------------------------------
    //
    public void SetOnSendData(DOnSendData value)
    {
      FOnSendData = value;
    }

    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

    public Int32 DelayCharacter
    {
      get { return (Int32)nudDelayCharacter.Value; }
      set { nudDelayCharacter.Value = value; }
    }
    public Int32 DelayCR
    {
      get { return (Int32)nudDelayCR.Value; }
      set { nudDelayCR.Value = value; }
    }
    public Int32 DelayLF
    {
      get { return (Int32)nudDelayLF.Value; }
      set { nudDelayLF.Value = value; }
    }
    //
    //-------------------------------
    //  Segment - Event
    //-------------------------------
    // cbxSendLine: Enter -> Send
    private void cbxSendData_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        btnSendData_Click(this, null);
      }
    }
    //
    //-------------------------------
    //  Segment - Helper
    //-------------------------------
    //
    private void AddColoredText(Color color, String text)
    {
      FUCMultiColorText.Add(color, text);
    }

    private Boolean LoadText(String filename)
    {
      if (File.Exists(filename))
      {
        CTextFile TextFile = new CTextFile();
        //???? TextFile.SetNotifier(FNotifier);
        if (TextFile.OpenRead(filename))
        {
          String Line;
          while (!TextFile.IsEndOfFile())
          {
            if (TextFile.Read(out Line))
            {
              cbxSendData.Items.Add(Line);
            }
          }
          TextFile.Close();
        }
        return true;
      }
      return false;
    }

    private Boolean SaveText(String filename)
    {
      CTextFile TextFile = new CTextFile();
      //?????????????TextFile.SetNotifier(FNotifier);
      TextFile.OpenWrite(filename);
      foreach (String Line in cbxSendData.Items)
      {
        TextFile.Write(Line + CARRIAGERETURN + LINEFEED);
      }
      TextFile.Close();
      return true;
    }
    //
    //-------------------------------
    //  Segment - Menu
    //-------------------------------
    //
    private void btnSendData_Click(object sender, EventArgs e)
    {
      String Data = cbxSendData.Text;
      // add local controls:
      if (cbxCR.Checked)
      {
        Data += CARRIAGERETURN;
      }
      if (cbxLF.Checked)
      {
        Data += LINEFEED;
      }
      // -> Main
      if (FOnSendData is DOnSendData)
      {
        FOnSendData(Data, DelayCharacter, DelayCR, DelayLF);
      }
      // add (if new) to ComboBox
      if (!cbxSendData.Items.Contains(Data))
      {
        cbxSendData.Items.Add(Data);
      }
    }

    private void mitSaveText_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogSaveStatementText.ShowDialog())
      {
        SaveText(DialogSaveStatementText.FileName);
      }
    }

    private void mitClearAllLines_Click(object sender, EventArgs e)
    {
      cbxSendData.Items.Clear();
    }

    private void mitLoadText_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadStatementText.ShowDialog())
      {
        LoadText(DialogLoadStatementText.FileName);
      }
    }
    //
    //-------------------------------
    //  Segment - Management
    //-------------------------------
    //
    public void Clear()
    {
      FUCMultiColorText.Clear();
    }

    public void EnableText(Boolean enable)
    {
      FUCMultiColorText.EnableText(enable);
    }

    public void Add(Color color, String text)
    {
      AddColoredText(color, text);
    }

    public void AddRxData(String text)
    {
      AddColoredText(FRxdColor, text);
    }

    public void AddTxData(String text)
    {
      AddColoredText(FTxdColor, text);
    }

    public void EnableControls(Boolean enable)
    {
      this.Enabled = enable;
    }

  }
}
