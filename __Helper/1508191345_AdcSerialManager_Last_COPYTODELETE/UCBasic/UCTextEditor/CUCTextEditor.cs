﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Initdata;
using Programdata;
using UCNotifier;
//
namespace UCTextEditor
{
  public delegate void DOnDataChanged(RTextEditorData data);
  public delegate void DOnDisplayProjection(RTextEditorData data);
  public delegate void DOnHide(); 
  //
  public struct RTextEditorData
  { // Common
    public String FileName;
    public String WorkingDirectory;
    // Load
    public Boolean ShowPanelLoad;
    // Save
    public Boolean ShowPanelSave;
    // Parameter
    public Boolean ShowPanelParameter;
    public Boolean ShowLegend;
    public Boolean IsReadOnly;
    public Int32 TabulatorWidth;
    public Boolean IsChanged;
    // Font
    public Boolean ShowPanelFont;
    public Font Font;
    public Color FontColor;
    public Byte FontPositionX;  // [%]
    public Byte FontPositionY;  // [%]
    public Byte FontOpacity;    // [%]
    // Callback
    public DOnDataChanged OnDataChanged;
    public DOnDisplayProjection OnDisplayProjection;
    public DOnHide OnHide;
    //
    public RTextEditorData(Int32 init)
    { // Common
      FileName = CUCEditTextEditor.INIT_FILENAME;
      WorkingDirectory = "";
      // Load
      ShowPanelLoad = CUCEditTextEditor.INIT_SHOWPANELLOAD;
      // Save
      ShowPanelSave = CUCEditTextEditor.INIT_SHOWPANELSAVE;
      // Parameter
      ShowPanelParameter = CUCEditTextEditor.INIT_SHOWPANELPARAMETER;
      ShowLegend = CUCEditTextEditor.INIT_SHOWLEGEND;
      IsReadOnly = CUCEditTextEditor.INIT_ISREADONLY;
      TabulatorWidth = CUCEditTextEditor.INIT_TABULATORWIDTH;
      IsChanged = CUCEditTextEditor.INIT_ISCHANGED;
      // Font
      ShowPanelFont = CUCEditTextEditor.INIT_SHOWPANELFONT;
      Font = CUCEditTextEditor.INIT_FONT;
      FontColor = CUCEditTextEditor.INIT_FONTCOLOR;
      FontPositionX = CUCEditTextEditor.INIT_FONTPOSITIONX;
      FontPositionY = CUCEditTextEditor.INIT_FONTPOSITIONY;
      FontOpacity = CUCEditTextEditor.INIT_FONTOPACITY;
      // Callback
      OnDataChanged = null;
      OnDisplayProjection = null;
      OnHide = null;
    }
  };

  public partial class CUCEditTextEditor : UserControl
  { //
    //-------------------------------
    //	Segment - Constant
    //-------------------------------
    //
    public const String SECTION_LIBRARY = "UCTextEditor";
    public const String HEADER_LIBRARY = "UCTextEditor";
    //
    // Name - Common
    public const String NAME_FILENAME = "FileName";
    public const String NAME_WORKINGDIRECTORY = "WorkingDirectory";
    // Name - Load
    public const String SECTION_PANELLOAD = "PanelLoad";
    public const String NAME_SHOWPANELLOAD = "ShowPanelLoad";
    public const String NAME_LOADLISTCOUNT = "LoadListCount";
    public const String NAME_FILENAMELOAD = "FileNameLoad";
    // Name - Save
    public const String SECTION_PANELSAVE = "PanelSave";
    public const String NAME_SHOWPANELSAVE = "ShowPanelSave";
    public const String NAME_SAVELISTCOUNT = "SaveListCount";
    public const String NAME_FILENAMESAVE = "FileNameSave";
    // Name - Parameter
    public const String SECTION_PANELPARAMETER = "PanelParameter";
    public const String NAME_SHOWPANELPARAMETER = "ShowPanelParameter";
    public const String NAME_SHOWLEGEND = "ShowLegend";
    public const String NAME_ISREADONLY = "IsReadOnly";
    public const String NAME_TABULATORWIDTH = "TabulatorWidth";
    public const String NAME_ISCHANGED = "IsChanged";
    // Name - Font
    public const String SECTION_PANELFONT = "PanelFont";
    public const String NAME_SHOWPANELFONT = "ShowPanelFont";
    public const String HEADER_FONT = "";
    public const String HEADER_FONTCOLOR = "";
    public const String NAME_FONTPOSITIONX = "FontPositionX";
    public const String NAME_FONTPOSITIONY = "FontPositionY";
    public const String NAME_FONTOPACITY = "FontOpacity";
    //
    // Init - Common
    public const BorderStyle INIT_BORDERSTYLE = BorderStyle.Fixed3D;
    public const String INIT_FILENAME = "";
    // Init - Load
    public const Boolean INIT_SHOWPANELLOAD = false;
    public const Int32 INIT_LOADLISTCOUNT = 0;
    // Init - Save
    public const Boolean INIT_SHOWPANELSAVE = false;
    public const Int32 INIT_SAVELISTCOUNT = 0;
    // Init - Parameter
    public const Boolean INIT_SHOWPANELPARAMETER = false;
    public const Boolean INIT_SHOWLEGEND = true;
    public const Boolean INIT_ISREADONLY = false;
    public const Int32 INIT_TABULATORWIDTH = 2;
    public const Boolean INIT_ISCHANGED = false;
    // Init - Font
    public const Boolean INIT_SHOWPANELFONT = false;
    public static Font INIT_FONT = DefaultFont;
    public static Color INIT_FONTCOLOR = Color.Black;
    public const Byte INIT_FONTPOSITIONX = 25;
    public const Byte INIT_FONTPOSITIONY = 25;
    public const Byte INIT_FONTOPACITY = 100;
    //
    private const String HEADER_FILENAME = "File: ";
    private const String HEADER_WORKINGDIRECTORY = "Path: ";
    private const String HEADER_TEXTMEASURE = "ABCDEFabcdef";
    private const String FORMAT_TABULATOR = "Tabulator [{0}]";
    private const String FORMAT_CURSORROW = "Row: {0}";
    private const String FORMAT_CURSORCOL = "Col: {0}";
    //
    //------------------------------------------------------
    //  Segment - Error-Handling
    //------------------------------------------------------
    //
    protected enum EErrorCode
    {
      None = 0,
      UnknownErrorcode
    };

    public static readonly String[] ERRORS = 
    { 
      "None",
  	  "Unknown errorcode"
    };
    //
    //----------------------------
    //  Segment - Get/SetData
    //----------------------------
    //
    public Boolean GetData(out RTextEditorData data)
    {
      data = FData;
      return true;
    }

    public Boolean SetData(RTextEditorData data)
    { // Common
      FileName = data.FileName;
      WorkingDirectory = data.WorkingDirectory;
      // Load
      ShowPanelLoad(data.ShowPanelLoad);
      // Save
      ShowPanelSave(data.ShowPanelSave);
      // Parameter
      ShowPanelParameter(data.ShowPanelParameter);
      ShowLegend(data.ShowLegend);
      IsReadOnly = data.IsReadOnly;
      TabulatorWidth = data.TabulatorWidth;
      SetIsChanged(data.IsChanged);
      // Font
      ShowPanelFont(data.ShowPanelFont);
      SetFont(data.Font);
      SetFontColor(data.FontColor);
      SetFontPositionX(data.FontPositionX);
      SetFontPositionY(data.FontPositionY);
      SetFontOpacity(data.FontOpacity);
      // Callback
      FData.OnDataChanged = data.OnDataChanged;
      FData.OnDisplayProjection = data.OnDisplayProjection;
      FData.OnHide = data.OnHide;
      return true;
    }
    //
    //----------------------------
    //  Segment - Messaging
    //----------------------------
    //
    private String BuildHeader()
    {
      return String.Format("{0}", HEADER_LIBRARY);
    }

    protected Boolean _Error(EErrorCode code)
    {
      if (FNotifier is CNotifier)
      {
        Int32 Index = (Int32)code;
        if ((Index < 0) && (ERRORS.Length <= Index))
        {
          Index = ERRORS.Length - 1;
        }
        String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
        FNotifier.Write(BuildHeader(), Line);
      }
      return false;
    }

    protected void _Protocol(String line)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(BuildHeader(), line);
      }
    }
    //
    //----------------------------
    //  Segment - Init
    //----------------------------
    //
    private void Initialize()
    {
      FData = new RTextEditorData(0);
      // Common - extern from FData
      SetBorderStyle(INIT_BORDERSTYLE);
      ShowLegend(INIT_SHOWLEGEND);
      FileName = INIT_FILENAME;
      // Load
      ShowPanelLoad(false);
      btnLoadFromFile.Click += new EventHandler(btnLoadFromFile_Click);
      btnClearLoadList.Click += new EventHandler(btnClearLoadList_Click);
      btnHideEditorLoad.Click += new EventHandler(btnHideEditor_Click);
      btnClosePanelLoad.Click += new EventHandler(btnClosePanelLoad_Click);
      // Save
      ShowPanelSave(false);
      btnSaveToFile.Click += new EventHandler(btnSaveToFile_Click);
      btnClearSaveList.Click += new EventHandler(btnClearSaveList_Click);
      btnHideEditorSave.Click += new EventHandler(btnHideEditor_Click);
      btnClosePanelSave.Click += new EventHandler(btnClosePanelSave_Click);
      // Parameter
      ShowPanelParameter(false);
      IsReadOnly = INIT_ISREADONLY;
      ShowLegend(INIT_SHOWLEGEND);
      scbTabulator.Value = INIT_TABULATORWIDTH;
      scbTabulator.LargeChange = INIT_TABULATORWIDTH;
      scbTabulator.SmallChange = 1;
      TabulatorWidth = 1 + INIT_TABULATORWIDTH;
      TabulatorWidth = INIT_TABULATORWIDTH;
      SetIsChanged(INIT_ISCHANGED);
      cbxIsReadOnly.CheckedChanged += new EventHandler(cbxIsReadOnly_CheckedChanged);
      cbxShowLegend.CheckedChanged += new EventHandler(cbxShowLegend_CheckedChanged);
      scbTabulator.ValueChanged += new EventHandler(scbTabulator_ValueChanged);
      btnHideEditorParameter.Click += new EventHandler(btnHideEditor_Click);
      btnClosePanelParameter.Click += new EventHandler(btnClosePanelParameter_Click);
      // Font
      ShowPanelFont(false);
      // Font -> DialogFont
      // FontColor -> DialogColor
      nudFontPositionX.ValueChanged += new EventHandler(nudFontPositionX_ValueChanged);
      nudFontPositionY.ValueChanged += new EventHandler(nudFontPositionY_ValueChanged);
      nudFontOpacity.ValueChanged += new EventHandler(nudFontOpacity_ValueChanged);
      btnHideEditorFont.Click += new EventHandler(btnHideEditor_Click);
      btnClosePanelFont.Click += new EventHandler(btnClosePanelFont_Click);
    }

    public Boolean LoadProgramdata(CInitdataReader initdata,
                                String initfilename,
                                String initworkingdirectory,
                                Int32 inittabulatorwidth,
                                Boolean initshowpanelload,
                                Boolean initshowpanelsave,
                                Boolean initshowpanelparameter,
                                Boolean initshowpanelfont,
                                Boolean initshowlegend)
    {
      Int32 IValue;
      Boolean BValue;
      String SWorkingDirectory, SFileName, SValue;
      Boolean Result = true;
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      // Common
      SFileName = initfilename;
      Result &= initdata.ReadString(NAME_FILENAME, out SFileName, SFileName);
      //
      SWorkingDirectory = initworkingdirectory;
      Result &= initdata.ReadString(NAME_WORKINGDIRECTORY, out SWorkingDirectory, SWorkingDirectory);
      //
      if (0 < SWorkingDirectory.Length)
      {
        WorkingDirectory = SWorkingDirectory;
      }
      else
      {
        WorkingDirectory = initworkingdirectory;
      }
      if (0 < SFileName.Length)
      {
        FileName = SFileName;
      }
      else
      {
        FileName = initfilename;
      }
      //
      // Load
      Result &= initdata.OpenSection(SECTION_PANELLOAD);
      //
      BValue = initshowpanelload;
      Result &= initdata.ReadBoolean(NAME_SHOWPANELLOAD, out BValue, BValue);
      ShowPanelLoad(BValue);
      //
      IValue = INIT_LOADLISTCOUNT;
      Result &= initdata.ReadInt32(NAME_LOADLISTCOUNT, out IValue, IValue);
      for (Int32 LI = 0; LI < IValue; LI++)
      {
        SValue = INIT_FILENAME;
        Result &= initdata.ReadString(NAME_FILENAMELOAD + LI.ToString(), out SValue, SValue);
        if (File.Exists(SValue))
        {
          if (!cbxLoadFromFile.Items.Contains(SValue))
          {
            cbxLoadFromFile.Items.Add(SValue);
          }
        }
      }
      //
      Result &= initdata.CloseSection();
      //
      // Save
      Result &= initdata.OpenSection(SECTION_PANELSAVE);
      //
      BValue = initshowpanelsave;
      Result &= initdata.ReadBoolean(NAME_SHOWPANELSAVE, out BValue, BValue);
      ShowPanelSave(BValue);
      //
      IValue = INIT_SAVELISTCOUNT;
      initdata.ReadInt32(NAME_SAVELISTCOUNT, out IValue, IValue);
      for (Int32 LI = 0; LI < IValue; LI++)
      {
        SValue = INIT_FILENAME;
        initdata.ReadString(NAME_FILENAMESAVE + LI.ToString(), out SValue, SValue);
        if (!cbxSaveToFile.Items.Contains(SValue))
        {
          cbxSaveToFile.Items.Add(SValue);
        }
      }
      //
      Result &= initdata.CloseSection();
      //
      // Parameter
      Result &= initdata.OpenSection(SECTION_PANELPARAMETER);
      //
      BValue = initshowpanelparameter;
      Result &= initdata.ReadBoolean(NAME_SHOWPANELPARAMETER, out BValue, BValue);
      ShowPanelParameter(BValue);
      //
      BValue = initshowlegend;
      Result &= initdata.ReadBoolean(NAME_SHOWLEGEND, out BValue, BValue);
      ShowLegend(BValue);
      //
      BValue = INIT_ISREADONLY;
      Result &= initdata.ReadBoolean(NAME_ISREADONLY, out BValue, BValue);
      IsReadOnly = BValue;
      //
      IValue = inittabulatorwidth;
      Result &= initdata.ReadInt32(NAME_TABULATORWIDTH, out IValue, IValue);
      TabulatorWidth = IValue;
      //
      BValue = INIT_ISCHANGED;
      Result &= initdata.ReadBoolean(NAME_ISCHANGED, out BValue, BValue);
      SetIsChanged(BValue);
      //
      Result &= initdata.CloseSection();
      //
      // Font
      Result &= initdata.OpenSection(SECTION_PANELFONT);
      //
      BValue = initshowpanelfont;
      Result &= initdata.ReadBoolean(NAME_SHOWPANELFONT, out BValue, BValue);
      ShowPanelFont(BValue);
      //
      Font FValue = INIT_FONT;
      Result &= initdata.ReadFont(HEADER_FONT, out FValue, FValue);
      SetFont(FValue);
      //
      Color CValue = INIT_FONTCOLOR;
      Result &= initdata.ReadColor(HEADER_FONTCOLOR, out CValue, CValue);
      SetFontColor(CValue);
      //
      Byte PValue = INIT_FONTPOSITIONX;
      Result &= initdata.ReadByte(NAME_FONTPOSITIONX, out PValue, PValue);
      SetFontPositionX(PValue);
      //
      PValue = INIT_FONTPOSITIONY;
      Result &= initdata.ReadByte(NAME_FONTPOSITIONY, out PValue, PValue);
      SetFontPositionY(PValue);
      //
      PValue = INIT_FONTOPACITY;
      Result &= initdata.ReadByte(NAME_FONTOPACITY, out PValue, PValue);
      SetFontOpacity(PValue);
      //
      Result &= initdata.CloseSection();
      //
      // - Main
      Result &= initdata.CloseSection();
      //
      String FileEntry = Path.Combine(FData.WorkingDirectory, FData.FileName);
      if (File.Exists(FileEntry))
      {
        FRTBEditor.LoadFile(FileEntry, RichTextBoxStreamType.PlainText);
      }
      //
      return Result;
    }

    public Boolean LoadProgramdata(CInitdataReader initdata)
    {
      String InitWorkingDirectory = FNotifier.GetCommonDocumentsFolderProgramVersionPath();
      return LoadProgramdata(initdata,
                          INIT_FILENAME,
                          InitWorkingDirectory,
                          INIT_TABULATORWIDTH,
                          INIT_SHOWPANELLOAD,
                          INIT_SHOWPANELSAVE,
                          INIT_SHOWPANELPARAMETER,
                          INIT_SHOWPANELFONT,
                          INIT_SHOWLEGEND);
    }

    public Boolean SaveProgramdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      // Common
      Result &= initdata.WriteString(NAME_FILENAME, FileName);
      Result &= initdata.WriteString(NAME_WORKINGDIRECTORY, WorkingDirectory);
      //
      // Load
      Result &= initdata.CreateSection(SECTION_PANELLOAD);
      Result &= initdata.WriteBoolean(NAME_SHOWPANELLOAD, IsPanelLoadVisible);
      // Load - List
      Result &= initdata.WriteInt32(NAME_LOADLISTCOUNT, cbxLoadFromFile.Items.Count);
      for (Int32 LI = 0; LI < cbxLoadFromFile.Items.Count; LI++)
      {
        Result &= initdata.WriteString(NAME_FILENAMELOAD + LI.ToString(), cbxLoadFromFile.Items[LI].ToString());
      }
      Result &= initdata.CloseSection();
      //
      // Save
      Result &= initdata.CreateSection(SECTION_PANELSAVE);
      Result &= initdata.WriteBoolean(NAME_SHOWPANELSAVE, IsPanelSaveVisible);
      // Save - List
      Result &= initdata.WriteInt32(NAME_SAVELISTCOUNT, cbxSaveToFile.Items.Count);
      for (Int32 LI = 0; LI < cbxSaveToFile.Items.Count; LI++)
      {
        Result &= initdata.WriteString(NAME_FILENAMESAVE + LI.ToString(), cbxSaveToFile.Items[LI].ToString());
      }
      Result &= initdata.CloseSection();
      // Parameter
      Result &= initdata.CreateSection(SECTION_PANELPARAMETER);
      Result &= initdata.WriteBoolean(NAME_SHOWPANELPARAMETER, IsPanelParameterVisible);
      Result &= initdata.WriteBoolean(NAME_SHOWLEGEND, IsLegendVisible);
      Result &= initdata.WriteBoolean(NAME_ISREADONLY, IsReadOnly);
      Result &= initdata.WriteInt32(NAME_TABULATORWIDTH, TabulatorWidth);
      Result &= initdata.WriteBoolean(NAME_ISCHANGED, IsChanged);
      Result &= initdata.CloseSection();
      // Font
      Result &= initdata.CreateSection(SECTION_PANELFONT);
      Result &= initdata.WriteBoolean(NAME_SHOWPANELFONT, IsPanelFontVisible);
      Result &= initdata.WriteFont(HEADER_FONT, GetFont());
      Result &= initdata.WriteColor(HEADER_FONTCOLOR, GetFontColor());
      Result &= initdata.WriteByte(NAME_FONTPOSITIONX, GetFontPositionX());
      Result &= initdata.WriteByte(NAME_FONTPOSITIONY, GetFontPositionY());
      Result &= initdata.WriteByte(NAME_FONTOPACITY, GetFontOpacity());
      Result &= initdata.CloseSection();
      // - Common
      Result &= initdata.CloseSection();
      return Result;
    }
    //
    //-------------------------------
    //  Segment - Event - this
    //-------------------------------
    //
    private void CUCEditTextEditor_Load(object sender, EventArgs e)
    {
      FRTBEditor.Dock = DockStyle.Fill;
    }

    private void stsEditor_Resize(object sender, EventArgs e)
    {
      stsWorkingDirectory.Width = stsLegend.Width - stsRow.Width - stsCol.Width - stsFileName.Width;
    }

    private void CUCEditTextEditor_Resize(object sender, EventArgs e)
    {
      cbxLoadFromFile.Width = Width - cbxLoadFromFile.Left - btnClearLoadList.Width - 20;
      btnClearLoadList.Left = 10 + cbxLoadFromFile.Left + cbxLoadFromFile.Width;
      cbxSaveToFile.Width = Width - cbxSaveToFile.Left - btnClearSaveList.Width - 20;
      btnClearSaveList.Left = 10 + cbxSaveToFile.Left + cbxSaveToFile.Width;
    }
    //
    //--------------------------------------------
    //  Segment - Event - this from FRTBEditor
    //--------------------------------------------
    //
    private void FUCEditor_TextChanged(object sender, EventArgs e)
    {
      SetIsChanged(true);
    }

    private void FUCEditor_FCursorPositionChanged(object sender, EventArgs e)
    {
      stsRow.Text = String.Format(FORMAT_CURSORROW, FRTBEditor.CursorRow);
      stsCol.Text = String.Format(FORMAT_CURSORCOL, FRTBEditor.CursorCol);
    }
    //
    //--------------------------------------------
    //  Segment - Event - Panel - Common
    //--------------------------------------------
    //
    private void btnHideEditor_Click(object sender, EventArgs e)
    {
      if (FData.OnHide is DOnHide)
      {
        FData.OnHide();
      }
    }
    //
    //--------------------------------------------
    //  Segment - Event - Panel - Load
    //--------------------------------------------
    //
    private void cbxLoadFromFile_SelectedValueChanged(object sender, EventArgs e)
    {
      FileName = cbxLoadFromFile.Text;
      LoadFromFile();
    }

    private void btnClosePanelLoad_Click(object sender, EventArgs e)
    {
      ShowPanelLoad(false);
    }

    private void btnLoadFromFile_Click(object sender, EventArgs e)
    {
      LoadFromFileModal();
      ShowPanelLoad(false);
    }

    private void btnClearLoadList_Click(object sender, EventArgs e)
    {
      cbxLoadFromFile.Items.Clear();
    }
    //
    //--------------------------------------------
    //  Segment - Event - Panel - Save
    //--------------------------------------------
    //
    private void cbxSaveToFile_SelectedValueChanged(object sender, EventArgs e)
    {
      FileName = cbxSaveToFile.Text;
    }

    private void btnClosePanelSave_Click(object sender, EventArgs e)
    {
      ShowPanelSave(false);
    }

    private void btnSaveToFile_Click(object sender, EventArgs e)
    {
      SaveToFileModal();
      ShowPanelSave(false);
    }

    private void btnClearSaveList_Click(object sender, EventArgs e)
    {
      cbxSaveToFile.Items.Clear();
    }
    //
    //--------------------------------------------
    //  Segment - Event - Panel - Parameter
    //--------------------------------------------
    //
    private void btnClosePanelParameter_Click(object sender, EventArgs e)
    {
      ShowPanelParameter(false);
    }

    private void cbxShowLegend_CheckedChanged(object sender, EventArgs e)
    {
      ShowLegend(cbxShowLegend.Checked);
    }

    private void cbxIsReadOnly_CheckedChanged(object sender, EventArgs e)
    {
      IsReadOnly = cbxIsReadOnly.Checked;
    }

    private void scbTabulator_ValueChanged(object sender, EventArgs e)
    {
      TabulatorWidth = scbTabulator.Value;
    }
    //
    //--------------------------------------------
    //  Segment - Event - Panel - Font
    //--------------------------------------------
    //
    private void btnClosePanelFont_Click(object sender, EventArgs e)
    {
      ShowPanelFont(false);
    }

    private void btnSelectFont_Click(object sender, EventArgs e)
    {
      SelectFontModal();
    }

    private void btnSelectFontColor_Click(object sender, EventArgs e)
    {
      SelectFontColorModal();
    }

    private void nudFontPositionX_ValueChanged(object sender, EventArgs e)
    {
      SetFontPositionX((Byte)nudFontPositionX.Value);
    }

    private void nudFontPositionY_ValueChanged(object sender, EventArgs e)
    {
      SetFontPositionY((Byte)nudFontPositionY.Value);
    }

    private void nudFontOpacity_ValueChanged(object sender, EventArgs e)
    {
      SetFontOpacity((Byte)nudFontOpacity.Value);
    }
    //
    //-------------------------------
    //  Segment - Event - Popup-Menu
    //-------------------------------
    //
    private void mitLoadText_Click(object sender, EventArgs e)
    {
      ShowPanelLoad(true);
      ShowPanelSave(false);
      ShowPanelParameter(false);
      ShowPanelFont(false);
    }

    private void mitSaveText_Click(object sender, EventArgs e)
    {
      ShowPanelLoad(false);
      ShowPanelSave(true);
      ShowPanelParameter(false);
      ShowPanelFont(false);
      SaveToFile();
      ShowPanelSave(false);
    }

    private void mitSaveTextAs_Click(object sender, EventArgs e)
    {
      ShowPanelLoad(false);
      ShowPanelSave(true);
      ShowPanelParameter(false);
      ShowPanelFont(false);
    }

    private void mitShowParameter_Click(object sender, EventArgs e)
    {
      ShowPanelLoad(false);
      ShowPanelSave(false);
      ShowPanelParameter(true);
      ShowPanelFont(false);
    }

    private void mitSelectFont_Click(object sender, EventArgs e)
    {
      ShowPanelLoad(false);
      ShowPanelSave(false);
      ShowPanelParameter(false);
      ShowPanelFont(true);
    }

    private void mitShowLegend_Click(object sender, EventArgs e)
    {
      ShowLegend(!mitShowLegend.Checked);
    }

    private void mitDisplayProjection_Click(object sender, EventArgs e)
    {
      if (FData.OnDisplayProjection is DOnDisplayProjection)
      {
        FData.OnDisplayProjection(FData);
      }
    }


  }
}
