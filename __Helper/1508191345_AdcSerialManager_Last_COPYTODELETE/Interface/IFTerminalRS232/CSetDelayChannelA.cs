﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  public class CSetDelayChannelA : CStatement
  {
    public const String INIT_NAME = "SetDelayChannelA";
    public const String COMMAND_TEXT = "SDA";

    public CSetDelayChannelA(CStatementlist parent,
                             Int32 delay,
                             CNotifier notifier,
                             CDevice device,
                             CComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, delay),
             notifier, device, comport)
    {
      SetParent(this);
      //SetOnStatementExecuteResponse(SelfOnSerialResponseReceived);
    }

    public void SelfOnSerialResponseReceived(Guid comportid,
                                             String line)
    {
      /*base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = line.Split(' ');
      if (2 < Tokenlist.Length)
      {
        StatementlistParent.Library.RefreshDelayChannelA(Tokenlist[2]);
      }*/
    }

  }
}
