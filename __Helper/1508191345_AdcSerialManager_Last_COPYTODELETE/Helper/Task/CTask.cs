﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
namespace Task
{
  public delegate void DOnExecutionBegin(RTaskData data);
  public delegate void DOnExecutionBusy(RTaskData data);
  public delegate void DOnExecutionEnd(RTaskData data);
  public delegate void DOnExecutionAbort(RTaskData data);

  public struct RTaskData
  {
    public String Name;
    public Int32 Counter;
    public Boolean IsActive;
    public Boolean IsAborted;
    public DOnExecutionBegin OnExecutionBegin;
    public DOnExecutionBusy OnExecutionBusy;
    public DOnExecutionEnd OnExecutionEnd;
    public DOnExecutionAbort OnExecutionAbort;
  }

  public class CTask
  {
    private RTaskData FData;
    private Thread FThread;

    public CTask(String name,
                 DOnExecutionBegin onexecutionbegin,
                 DOnExecutionBusy onexecutionbusy,
                 DOnExecutionEnd onexecutionend,
                 DOnExecutionAbort onexecutionabort)
    {
      FData.Name = name;
      FData.OnExecutionBegin = onexecutionbegin;
      FData.OnExecutionBusy = onexecutionbusy;
      FData.OnExecutionEnd = onexecutionend;
      FData.OnExecutionAbort = onexecutionabort;
      FData.Counter = 0;
      FData.IsAborted = false;
      FData.IsActive = false;
      FThread = null;
      // debug String Line = String.Format("### Task[{0}].Created", FData.Name);
      // debug Console.WriteLine(Line);
    }

 

    public Boolean IsActive()
    {
      return FData.IsActive;
    }

    public Boolean IsAborted()
    {
      return FData.IsAborted;
    }

    private void OnExecute()
    {
      // debug String Line = String.Format("### Task[{0}].OnExecute() - S", FData.Name);
      // debug Console.WriteLine(Line);
      FData.IsActive = true;
      if (FData.OnExecutionBegin is DOnExecutionBegin)
      {
        FData.OnExecutionBegin(FData);
      }
      if (FData.OnExecutionBusy is DOnExecutionBusy)
      {
        FData.OnExecutionBusy(FData);
      }
      if (FData.OnExecutionEnd is DOnExecutionEnd)
      {
        FData.OnExecutionEnd(FData);
      }
      FThread = null;
      FData.IsActive = false;
      // debug Line = String.Format("### Task[{0}].OnExecute() - E", FData.Name);
      // debug Console.WriteLine(Line);
    }

    public Boolean Begin()
    {
      // debug String Line = String.Format("### Task[{0}].Start() - S", FData.Name);
      // debug Console.WriteLine(Line);
      FData.Counter += 1;
      // RO FData.Name 
      FThread = new Thread(OnExecute);
      FThread.Name = FData.Name;
      FThread.Start();
      // debug Line = String.Format("### Task[{0}].Start() - E", FData.Name);
      // debug Console.WriteLine(Line);
      return true;
    }

    public Boolean Abort()
    {
      // debug String Line = String.Format("### Task[{0}].Abort() - S", FData.Name);
      // debug Console.WriteLine(Line);
      try
      {
        if (FThread is Thread)
        {
          FThread.Abort();
          FData.IsAborted = true;
          FData.IsActive = false;
          if (FData.OnExecutionAbort is DOnExecutionAbort)
          {
            FData.OnExecutionAbort(FData);
          }
          if (ThreadState.Aborted == FThread.ThreadState)
          {
            FThread = null;
            // debug Line = String.Format("### Task[{0}].Abort() - E", FData.Name);
            // debug Console.WriteLine(Line);
            return true;
          }
        }
        FThread = null;
        // debug Line = String.Format("### Task[{0}].Abort() - Error <null>!!!", FData.Name);
        // debug Console.WriteLine(Line);
        return false;
      }
      catch (Exception)
      {
        FThread = null;
        // debug Line = String.Format("### Task[{0}].Abort() - Error<exception>!!!", FData.Name);
        // debug Console.WriteLine(Line);
        return false;
      }
    }

  }
}
