﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
//-------------------------------------------------------------------
//  Segment - Dll Import
//-------------------------------------------------------------------
using PiezoHardwareBase;
using PHAerotechQLab;
using PHPhysicInstrumentsE816;
//
namespace DemoPiezoHardware
{
  //
  //-------------------------------------------------------------------
  //  Segment - MainWindow
  //-------------------------------------------------------------------
  //
  public partial class FormMain : Form
  {
    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    private CPiezoHardwareBase FPiezoHardware;
    // private Double FPositionActual;
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public FormMain()
    {
      InitializeComponent();
      //
      btnOpenClose.Text = "Open";
      cbxPiezoHardware.Enabled = true;
      pnlControls.Enabled = false;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Event - Window
    //-------------------------------------------------------------------
    //
    private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        FPiezoHardware.Close();
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Helper
    //-------------------------------------------------------------------
    //
    private void Info(String line)
    {
      lbxInfo.Items.Add(line);
      lbxInfo.SelectedIndex = lbxInfo.Items.Count - 1;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //
    private delegate void CBPiezoHardwareOnPositionActualChanged(Double positionactual);
    private void PiezoHardwareOnPositionActualChanged(Double positionactual)
    {
      if (this.InvokeRequired)
      {
        CBPiezoHardwareOnPositionActualChanged CB =
          new CBPiezoHardwareOnPositionActualChanged(PiezoHardwareOnPositionActualChanged);
        Invoke(CB, new object[] { positionactual });
      }
      else
      {
        Info(String.Format("PositionActual[..] = {0}", positionactual));
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Event - Menu
    //-------------------------------------------------------------------
    //
    private void btnOpenClose_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        tmrRefresh.Stop();
        FPiezoHardware.Close();
        FPiezoHardware = null;
        btnOpenClose.Text = "Open";
        cbxPiezoHardware.Enabled = true;
        pnlControls.Enabled = false;
      }
      else
      {
        switch (cbxPiezoHardware.Text)
        {
          case "AerotechQLab":
            FPiezoHardware = new CPHAerotechQLab();
            FPiezoHardware.SetOnPositionActualChanged(PiezoHardwareOnPositionActualChanged);
            FPiezoHardware.SetVelocityTarget(10000);
            FPiezoHardware.Open();
            btnOpenClose.Text = "Close";
            cbxPiezoHardware.Enabled = false;
            pnlControls.Enabled = true;
            tmrRefresh.Start();
            break;
          case "PhysicInstrumentsE816":
            FPiezoHardware = new CPHPhysicInstrumentsE816();
            FPiezoHardware.SetOnPositionActualChanged(PiezoHardwareOnPositionActualChanged);
            FPiezoHardware.Open();
            btnOpenClose.Text = "Close";
            cbxPiezoHardware.Enabled = false;
            pnlControls.Enabled = true;
            tmrRefresh.Start();
            break;
        }
      }
    }

    private void btnReset_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        FPiezoHardware.ResetDevice();
      }
    }

    private void btnSetVelocityTarget_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double VelocityTarget = (Double)nudVelocityTarget.Value;
        FPiezoHardware.SetVelocityTarget(VelocityTarget);
      }
    }

    private void btnMovePositionAbsolute_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double PositionAbsolute = (Double)nudPositionAbsolute.Value;
        FPiezoHardware.MoveAbsolute(PositionAbsolute);
      }
    }

    private void btnMovePositionRelative_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double PositionRelative = (Double)nudPositionRelative.Value;
        FPiezoHardware.MoveRelative(PositionRelative);
      }
    }

    private void btnMovePositionA_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double PositionAbsolute = (Double)nudPositionA.Value;
        FPiezoHardware.MoveAbsolute(PositionAbsolute);
      }
    }

    private void btnMovePositionB_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double PositionAbsolute = (Double)nudPositionB.Value;
        FPiezoHardware.MoveAbsolute(PositionAbsolute);
      }
    }

    //private void tmrRefresh_Tick(object sender, EventArgs e)
    //{
    //  if (FPiezoHardware is CPiezoHardwareBase)
    //  {
    //    Double PA = FPiezoHardware.GetPositionActual();
    //    //if (0.000000001 < Math.Abs(PA - FPositionActual))
    //    if (PA != FPositionActual)
    //    {
    //      Info(String.Format("PositionActual[..] = {0}", PA));
    //      FPositionActual = PA;
    //    }
    //  }
    //}


  }
}
