﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
//-------------------------------------------------------------------
//  Segment - Dll Import
//-------------------------------------------------------------------
using PiezoHardwareBase;
using PHAerotechQLab;
//
namespace DemoPiezoHardware
{
  //
  //-------------------------------------------------------------------
  //  Segment - MainWindow
  //-------------------------------------------------------------------
  //
  public partial class FormMain : Form
  {
    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    private CPiezoHardwareBase FPiezoHardware;
    private Double FPositionActual;
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public FormMain()
    {
      InitializeComponent();
      //
      btnOpenClose.Text = "Open";
      cbxPiezoHardware.Enabled = true;
      pnlControls.Enabled = false;
      pnlVelocity.Enabled = false;
      gpbPositionActual.Enabled = false;
      btnResetDevice.Enabled = false;
      cbxPiezoHardware.SelectedIndex = 0;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Event - Window
    //-------------------------------------------------------------------
    //
    private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        FPiezoHardware.Close();
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Helper
    //-------------------------------------------------------------------
    //
    private void Info(String line)
    {
      lbxInfo.Items.Add(line);
      lbxInfo.SelectedIndex = lbxInfo.Items.Count - 1;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //
    private delegate void CBPiezoHardwareOnPositionActualChanged(Double positionactual);
    private void PiezoHardwareOnPositionActualChanged(Double positionactual)
    {
      if (this.InvokeRequired)
      {
        CBPiezoHardwareOnPositionActualChanged CB =
          new CBPiezoHardwareOnPositionActualChanged(PiezoHardwareOnPositionActualChanged);
        Invoke(CB, new object[] { positionactual });
      }
      else
      {
        Info(String.Format("PositionActual[..] = {0}", positionactual));
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Event - Menu
    //-------------------------------------------------------------------
    //
    private void btnOpenClose_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        tmrRefresh.Stop();
        FPiezoHardware.Close();
        FPiezoHardware = null;
        btnOpenClose.Text = "Open";
        cbxPiezoHardware.Enabled = true;
        pnlControls.Enabled = false;
        pnlVelocity.Enabled = false;
        gpbPositionActual.Enabled = false;
        btnResetDevice.Enabled = false;
      }
      else
      {
        switch (cbxPiezoHardware.Text)
        {
          case "AerotechQLab":
            FPiezoHardware = new CPHAerotechQLab();
            FPiezoHardware.SetOnPositionActualChanged(PiezoHardwareOnPositionActualChanged);
            FPiezoHardware.SetVelocityTarget(100);
            FPiezoHardware.Open();
            btnOpenClose.Text = "Close";
            cbxPiezoHardware.Enabled = false;
            pnlControls.Enabled = true;
            pnlVelocity.Enabled = true;
            gpbPositionActual.Enabled = true;
            btnResetDevice.Enabled = true;
            tmrRefresh.Start();
            break;
        }
      }
    }

    private void btnReset_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        FPiezoHardware.ResetDevice();
      }
    }

    private void btnSetVelocityTarget_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double VelocityTarget = (Double)nudVelocityTarget.Value;
        FPiezoHardware.SetVelocityTarget(VelocityTarget);
      }
    }

    private void btnMovePositionAbsolute_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double PositionAbsolute = (Double)nudPositionAbsolute.Value;
        FPiezoHardware.moveABS(PositionAbsolute);
      }
    }

    private void btnMovePositionRelativePlus_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double PositionRelative = (Double)nudPositionRelative.Value;
        FPiezoHardware.moveREL(+PositionRelative);
      }
    }

    private void btnMovePositionRelativeMinus_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double PositionRelative = (Double)nudPositionRelative.Value;
        FPiezoHardware.moveREL(-PositionRelative);
      }
    }

    private void tmrRefresh_Tick(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double PA = FPiezoHardware.GetPosition();
        if (0.000000001 < Math.Abs(PA - FPositionActual))
        {
          FPositionActual = PA;
          Info(String.Format("PositionActual[..] = {0}", PA));
          tbxPositionActual.Text = PA.ToString();
        }
      }
    }

    private void btnMovePositionMaximum_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double PositionAbsolute = 100.0;
        FPiezoHardware.moveABS(PositionAbsolute);
      }
    }

    private void btnMovePositionMinimum_Click(object sender, EventArgs e)
    {
      if (FPiezoHardware is CPiezoHardwareBase)
      {
        Double PositionAbsolute = 0.0;
        FPiezoHardware.moveABS(PositionAbsolute);
      }
    }


  }
}
