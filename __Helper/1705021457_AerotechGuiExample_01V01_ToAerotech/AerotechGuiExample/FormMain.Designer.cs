﻿namespace DemoPiezoHardware
{
  partial class FormMain
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.panel1 = new System.Windows.Forms.Panel();
      this.gpbPositionActual = new System.Windows.Forms.GroupBox();
      this.label3 = new System.Windows.Forms.Label();
      this.tbxPositionActual = new System.Windows.Forms.TextBox();
      this.pnlVelocity = new System.Windows.Forms.Panel();
      this.btnSetVelocityTarget = new System.Windows.Forms.Button();
      this.nudVelocityTarget = new System.Windows.Forms.NumericUpDown();
      this.pnlControls = new System.Windows.Forms.Panel();
      this.btnMovePositionMaximum = new System.Windows.Forms.Button();
      this.btnMovePositionRelativeMinus = new System.Windows.Forms.Button();
      this.nudPositionRelative = new System.Windows.Forms.NumericUpDown();
      this.btnMovePositionAbsolute = new System.Windows.Forms.Button();
      this.btnMovePositionMinimum = new System.Windows.Forms.Button();
      this.nudPositionAbsolute = new System.Windows.Forms.NumericUpDown();
      this.btnMovePositionRelativePlus = new System.Windows.Forms.Button();
      this.btnResetDevice = new System.Windows.Forms.Button();
      this.btnOpenClose = new System.Windows.Forms.Button();
      this.cbxPiezoHardware = new System.Windows.Forms.ComboBox();
      this.lbxInfo = new System.Windows.Forms.ListBox();
      this.tmrRefresh = new System.Windows.Forms.Timer(this.components);
      this.panel1.SuspendLayout();
      this.gpbPositionActual.SuspendLayout();
      this.pnlVelocity.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudVelocityTarget)).BeginInit();
      this.pnlControls.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionRelative)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionAbsolute)).BeginInit();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.SystemColors.Info;
      this.panel1.Controls.Add(this.gpbPositionActual);
      this.panel1.Controls.Add(this.pnlVelocity);
      this.panel1.Controls.Add(this.pnlControls);
      this.panel1.Controls.Add(this.btnResetDevice);
      this.panel1.Controls.Add(this.btnOpenClose);
      this.panel1.Controls.Add(this.cbxPiezoHardware);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(657, 203);
      this.panel1.TabIndex = 0;
      // 
      // gpbPositionActual
      // 
      this.gpbPositionActual.Controls.Add(this.label3);
      this.gpbPositionActual.Controls.Add(this.tbxPositionActual);
      this.gpbPositionActual.Location = new System.Drawing.Point(12, 40);
      this.gpbPositionActual.Name = "gpbPositionActual";
      this.gpbPositionActual.Size = new System.Drawing.Size(363, 46);
      this.gpbPositionActual.TabIndex = 31;
      this.gpbPositionActual.TabStop = false;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(6, 16);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(88, 16);
      this.label3.TabIndex = 15;
      this.label3.Text = "Position (µm):";
      // 
      // tbxPositionActual
      // 
      this.tbxPositionActual.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
      this.tbxPositionActual.Location = new System.Drawing.Point(98, 11);
      this.tbxPositionActual.Name = "tbxPositionActual";
      this.tbxPositionActual.Size = new System.Drawing.Size(259, 29);
      this.tbxPositionActual.TabIndex = 16;
      // 
      // pnlVelocity
      // 
      this.pnlVelocity.BackColor = System.Drawing.Color.MistyRose;
      this.pnlVelocity.Controls.Add(this.btnSetVelocityTarget);
      this.pnlVelocity.Controls.Add(this.nudVelocityTarget);
      this.pnlVelocity.Location = new System.Drawing.Point(405, 11);
      this.pnlVelocity.Name = "pnlVelocity";
      this.pnlVelocity.Size = new System.Drawing.Size(113, 87);
      this.pnlVelocity.TabIndex = 14;
      // 
      // btnSetVelocityTarget
      // 
      this.btnSetVelocityTarget.Location = new System.Drawing.Point(14, 28);
      this.btnSetVelocityTarget.Name = "btnSetVelocityTarget";
      this.btnSetVelocityTarget.Size = new System.Drawing.Size(86, 47);
      this.btnSetVelocityTarget.TabIndex = 2;
      this.btnSetVelocityTarget.Text = "Set Velocity Target [um/s]";
      this.btnSetVelocityTarget.UseVisualStyleBackColor = true;
      this.btnSetVelocityTarget.Click += new System.EventHandler(this.btnSetVelocityTarget_Click);
      // 
      // nudVelocityTarget
      // 
      this.nudVelocityTarget.DecimalPlaces = 3;
      this.nudVelocityTarget.Location = new System.Drawing.Point(20, 6);
      this.nudVelocityTarget.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudVelocityTarget.Name = "nudVelocityTarget";
      this.nudVelocityTarget.Size = new System.Drawing.Size(67, 20);
      this.nudVelocityTarget.TabIndex = 3;
      this.nudVelocityTarget.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudVelocityTarget.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // pnlControls
      // 
      this.pnlControls.BackColor = System.Drawing.Color.MistyRose;
      this.pnlControls.Controls.Add(this.btnMovePositionMaximum);
      this.pnlControls.Controls.Add(this.btnMovePositionRelativeMinus);
      this.pnlControls.Controls.Add(this.nudPositionRelative);
      this.pnlControls.Controls.Add(this.btnMovePositionAbsolute);
      this.pnlControls.Controls.Add(this.btnMovePositionMinimum);
      this.pnlControls.Controls.Add(this.nudPositionAbsolute);
      this.pnlControls.Controls.Add(this.btnMovePositionRelativePlus);
      this.pnlControls.Location = new System.Drawing.Point(12, 104);
      this.pnlControls.Name = "pnlControls";
      this.pnlControls.Size = new System.Drawing.Size(506, 87);
      this.pnlControls.TabIndex = 13;
      // 
      // btnMovePositionMaximum
      // 
      this.btnMovePositionMaximum.Location = new System.Drawing.Point(107, 28);
      this.btnMovePositionMaximum.Name = "btnMovePositionMaximum";
      this.btnMovePositionMaximum.Size = new System.Drawing.Size(91, 48);
      this.btnMovePositionMaximum.TabIndex = 11;
      this.btnMovePositionMaximum.Text = "Move Maximum [um]";
      this.btnMovePositionMaximum.UseVisualStyleBackColor = true;
      this.btnMovePositionMaximum.Click += new System.EventHandler(this.btnMovePositionMaximum_Click);
      // 
      // btnMovePositionRelativeMinus
      // 
      this.btnMovePositionRelativeMinus.Location = new System.Drawing.Point(316, 28);
      this.btnMovePositionRelativeMinus.Name = "btnMovePositionRelativeMinus";
      this.btnMovePositionRelativeMinus.Size = new System.Drawing.Size(86, 48);
      this.btnMovePositionRelativeMinus.TabIndex = 6;
      this.btnMovePositionRelativeMinus.Text = "Move Relative Minus [um]";
      this.btnMovePositionRelativeMinus.UseVisualStyleBackColor = true;
      this.btnMovePositionRelativeMinus.Click += new System.EventHandler(this.btnMovePositionRelativeMinus_Click);
      // 
      // nudPositionRelative
      // 
      this.nudPositionRelative.DecimalPlaces = 3;
      this.nudPositionRelative.Location = new System.Drawing.Point(375, 6);
      this.nudPositionRelative.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
      this.nudPositionRelative.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
      this.nudPositionRelative.Name = "nudPositionRelative";
      this.nudPositionRelative.Size = new System.Drawing.Size(61, 20);
      this.nudPositionRelative.TabIndex = 10;
      this.nudPositionRelative.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionRelative.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      // 
      // btnMovePositionAbsolute
      // 
      this.btnMovePositionAbsolute.Location = new System.Drawing.Point(213, 28);
      this.btnMovePositionAbsolute.Name = "btnMovePositionAbsolute";
      this.btnMovePositionAbsolute.Size = new System.Drawing.Size(86, 48);
      this.btnMovePositionAbsolute.TabIndex = 4;
      this.btnMovePositionAbsolute.Text = "Move Absolute [um]";
      this.btnMovePositionAbsolute.UseVisualStyleBackColor = true;
      this.btnMovePositionAbsolute.Click += new System.EventHandler(this.btnMovePositionAbsolute_Click);
      // 
      // btnMovePositionMinimum
      // 
      this.btnMovePositionMinimum.Location = new System.Drawing.Point(15, 28);
      this.btnMovePositionMinimum.Name = "btnMovePositionMinimum";
      this.btnMovePositionMinimum.Size = new System.Drawing.Size(88, 48);
      this.btnMovePositionMinimum.TabIndex = 5;
      this.btnMovePositionMinimum.Text = "Move Minimum [um]";
      this.btnMovePositionMinimum.UseVisualStyleBackColor = true;
      this.btnMovePositionMinimum.Click += new System.EventHandler(this.btnMovePositionMinimum_Click);
      // 
      // nudPositionAbsolute
      // 
      this.nudPositionAbsolute.DecimalPlaces = 3;
      this.nudPositionAbsolute.Location = new System.Drawing.Point(213, 6);
      this.nudPositionAbsolute.Name = "nudPositionAbsolute";
      this.nudPositionAbsolute.Size = new System.Drawing.Size(86, 20);
      this.nudPositionAbsolute.TabIndex = 8;
      this.nudPositionAbsolute.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionAbsolute.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // btnMovePositionRelativePlus
      // 
      this.btnMovePositionRelativePlus.Location = new System.Drawing.Point(405, 28);
      this.btnMovePositionRelativePlus.Name = "btnMovePositionRelativePlus";
      this.btnMovePositionRelativePlus.Size = new System.Drawing.Size(86, 48);
      this.btnMovePositionRelativePlus.TabIndex = 7;
      this.btnMovePositionRelativePlus.Text = "MoveRelative Plus [um]";
      this.btnMovePositionRelativePlus.UseVisualStyleBackColor = true;
      this.btnMovePositionRelativePlus.Click += new System.EventHandler(this.btnMovePositionRelativePlus_Click);
      // 
      // btnResetDevice
      // 
      this.btnResetDevice.Location = new System.Drawing.Point(300, 11);
      this.btnResetDevice.Name = "btnResetDevice";
      this.btnResetDevice.Size = new System.Drawing.Size(75, 23);
      this.btnResetDevice.TabIndex = 12;
      this.btnResetDevice.Text = "Reset Device";
      this.btnResetDevice.UseVisualStyleBackColor = true;
      this.btnResetDevice.Click += new System.EventHandler(this.btnReset_Click);
      // 
      // btnOpenClose
      // 
      this.btnOpenClose.Location = new System.Drawing.Point(196, 11);
      this.btnOpenClose.Name = "btnOpenClose";
      this.btnOpenClose.Size = new System.Drawing.Size(75, 23);
      this.btnOpenClose.TabIndex = 1;
      this.btnOpenClose.Text = "Open/Close";
      this.btnOpenClose.UseVisualStyleBackColor = true;
      this.btnOpenClose.Click += new System.EventHandler(this.btnOpenClose_Click);
      // 
      // cbxPiezoHardware
      // 
      this.cbxPiezoHardware.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPiezoHardware.FormattingEnabled = true;
      this.cbxPiezoHardware.Items.AddRange(new object[] {
            "AerotechQLab"});
      this.cbxPiezoHardware.Location = new System.Drawing.Point(12, 12);
      this.cbxPiezoHardware.Name = "cbxPiezoHardware";
      this.cbxPiezoHardware.Size = new System.Drawing.Size(178, 21);
      this.cbxPiezoHardware.TabIndex = 0;
      // 
      // lbxInfo
      // 
      this.lbxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbxInfo.FormattingEnabled = true;
      this.lbxInfo.Location = new System.Drawing.Point(0, 203);
      this.lbxInfo.Name = "lbxInfo";
      this.lbxInfo.Size = new System.Drawing.Size(657, 170);
      this.lbxInfo.TabIndex = 1;
      // 
      // tmrRefresh
      // 
      this.tmrRefresh.Tick += new System.EventHandler(this.tmrRefresh_Tick);
      // 
      // FormMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(657, 373);
      this.Controls.Add(this.lbxInfo);
      this.Controls.Add(this.panel1);
      this.Name = "FormMain";
      this.Text = "DemoPiezoHardware";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
      this.panel1.ResumeLayout(false);
      this.gpbPositionActual.ResumeLayout(false);
      this.gpbPositionActual.PerformLayout();
      this.pnlVelocity.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudVelocityTarget)).EndInit();
      this.pnlControls.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionRelative)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionAbsolute)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ComboBox cbxPiezoHardware;
    private System.Windows.Forms.Button btnOpenClose;
    private System.Windows.Forms.NumericUpDown nudVelocityTarget;
    private System.Windows.Forms.Button btnSetVelocityTarget;
    private System.Windows.Forms.NumericUpDown nudPositionRelative;
    private System.Windows.Forms.NumericUpDown nudPositionAbsolute;
    private System.Windows.Forms.Button btnMovePositionRelativePlus;
    private System.Windows.Forms.Button btnMovePositionRelativeMinus;
    private System.Windows.Forms.Button btnMovePositionMinimum;
    private System.Windows.Forms.Button btnMovePositionAbsolute;
    private System.Windows.Forms.ListBox lbxInfo;
    private System.Windows.Forms.Button btnResetDevice;
    private System.Windows.Forms.Panel pnlControls;
    private System.Windows.Forms.Timer tmrRefresh;
    private System.Windows.Forms.Panel pnlVelocity;
    private System.Windows.Forms.Button btnMovePositionMaximum;
    private System.Windows.Forms.GroupBox gpbPositionActual;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox tbxPositionActual;
  }
}

