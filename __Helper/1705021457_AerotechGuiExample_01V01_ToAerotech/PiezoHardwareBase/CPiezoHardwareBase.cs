﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PiezoHardwareBase
{
  //
  public delegate void DOnPositionActualChanged(Double positionactual);
  //
  //-------------------------------------------------------------------
  //  Segment - BaseClass PiezoHardware
  //-------------------------------------------------------------------
  //
  public abstract class CPiezoHardwareBase
  {
    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    protected String FError;
    protected Double FPositionTarget;
    protected Double FVelocityTarget;
    protected Double FPositionActual = 0;
    protected DOnPositionActualChanged FOnPositionActualChanged;
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public CPiezoHardwareBase()
    {
      FError = "";
      FPositionTarget = 0.0;
      FPositionActual = 0.0;
      FVelocityTarget = 1.0;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public void SetOnPositionActualChanged(DOnPositionActualChanged value)
    {
      FOnPositionActualChanged = value;
    }

    public void SetVelocityTarget(Double value)
    {
      FVelocityTarget = value;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Public Management
    //-------------------------------------------------------------------
    //
    public abstract Boolean Open();
    public abstract Boolean Close();
    public abstract Boolean IsConnected();
    //
    //-------------------------------------------------------------------
    //  Segment - Public Handler
    //-------------------------------------------------------------------
    //
    public abstract Boolean ResetDevice();
    //public abstract Boolean MoveAbsolute(Double positiontarget);
    public abstract Boolean moveABS(Double positiontarget);
    //public abstract Boolean MoveRelative(Double positiondelta);
    public abstract Boolean moveREL(Double positiondelta);
    //public abstract Double GetPositionActual();
    public abstract Double GetPosition();
    // ??? public abstract Boolean AbortMotion();
  }
}
