﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using Aerotech.Ensemble;
using Aerotech.Ensemble.Communication;
using Aerotech.Ensemble.Status;
using Aerotech.Ensemble.Exceptions;
using Aerotech.Ensemble.Parameters;
using Aerotech.Common;
using Aerotech.Ensemble.Information;
using Aerotech.Ensemble.Commands;
//
using PiezoHardwareBase;
//
namespace PHAerotechQLab
{
  //
  //-------------------------------------------------------------------
  //  Segment - CPHAerotechQLab
  //-------------------------------------------------------------------
  //
  public class CPHAerotechQLab : CPiezoHardwareBase
  {
    private const Int32 DELAY_REFRESH_POSITIONACTUAL = 100;
    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    private Controller FController = null;

    public CPHAerotechQLab()
      : base()
    {
      FVelocityTarget = 10000.0;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public override Boolean IsConnected()
    {
      // return (FController is Controller);
      return FController.IsConnected;
    }

    //public override Double GetPositionActual()
    public override Double GetPosition()
    {
      return FPositionActual;
    }   
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //
    private void OnDiagnosticArrived(object sender, 
                                            NewDiagPacketArrivedEventArgs e)
	  {
      Double PA = e.Data["X"].PositionCommand;
      if (PA != FPositionActual)
      {
        FPositionActual = PA;
        if (FOnPositionActualChanged is DOnPositionActualChanged)
        {
          FOnPositionActualChanged(FPositionActual);
        }
        //Thread.Sleep(DELAY_REFRESH_POSITIONACTUAL);
      }
      // Console.WriteLine(FPositionActual);
	  }
    //
    //-------------------------------------------------------------------
    //  Segment - Public Management
    //-------------------------------------------------------------------
    //
    public override Boolean Open()
    {
      try
      {
        // Controller.Disconnect();
        FController = Controller.Connect()[0];
        //!!!!!!!!!!FController.Reset();
        FController.ControlCenter.Diagnostics.NewDiagPacketArrived +=
          new EventHandler<NewDiagPacketArrivedEventArgs>(OnDiagnosticArrived);
        FController.Commands.Axes["X"].Motion.Enable();
        FController.Commands.Motion.Setup.Servo("X", OnOff.On);
        FController.Commands.Axes["X"].Motion.Home();
        // debug FController.Commands.Axes["X"].Motion.Setup.PosCap;
        // debug double PA = FController.Commands.Axes.Select["X"].Position["X"];
        return true;
      }
      catch (Exception e)
      {
        FController = null;
        FError = e.ToString();
        return false;
      }
    }

    public override Boolean Close()
    {
      try
      {
        FController.ControlCenter.Diagnostics.NewDiagPacketArrived -=
          new EventHandler<NewDiagPacketArrivedEventArgs>(OnDiagnosticArrived);
        Controller.Disconnect();
        FController = null;
        return true;
      }
      catch (Exception e)
      {
        FError = e.ToString();
        return false;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Public Handler
    //-------------------------------------------------------------------
    //
    public override Boolean ResetDevice()
    {
      try
      {
        FController.Reset();
        return true;
      }
      catch (Exception e)
      {
        FError = e.ToString();
        return false;
      }
    }

    //public override Boolean AbortMotion()
    //{
    //  try
    //  {
    //    return true;
    //  }
    //  catch (Exception e)
    //  {
    //    FError = e.ToString();
    //    return false;
    //  }
    //}

    //public override Boolean MoveAbsolute(Double positiontarget)
    public override Boolean moveABS(Double positiontarget)
    {
      try
      {
        if (IsConnected())
        {
          FPositionTarget = positiontarget;
          FController.Commands.Motion.MoveAbs("X", FPositionTarget, FVelocityTarget);
          FController.Commands.Motion.WaitForMotionDone(WaitOption.MoveDone, new string[] { "X" });
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        FError = e.ToString();
        return false;
      }
    }

    //public override Boolean MoveRelative(Double positiondelta)
    public override Boolean moveREL(Double positiondelta)
    {
      try
      {
        if (IsConnected())
        {
          FPositionTarget += positiondelta;
          FController.Commands.Motion.MoveAbs("X", FPositionTarget, FVelocityTarget);
          FController.Commands.Motion.WaitForMotionDone(WaitOption.MoveDone, new string[] { "X" });
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        FError = e.ToString();
        return false;
      }
    }


  }
}
