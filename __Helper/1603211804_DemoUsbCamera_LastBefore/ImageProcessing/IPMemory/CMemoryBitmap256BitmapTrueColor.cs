﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMemoryBitmap256BitmapTrueColor : CMemory
  {
    //
    //#################################################################################
    //  Copy Bitmap256 (no Palette) → Bitmap24bpp
    //#################################################################################
    //
    public static Boolean CopyBitmap256ToBitmap24bpp(CBitmap256 bitmapsource256,
                                                     Byte opacity, // 0..100%
                                                     out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        Bitmap BitmapSource = bitmapsource256.GetBitmapArgb();
        PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        Byte BOpacity = BuildOpacityByte(opacity);
        if (PixelFormat.Format32bppRgb == PixelFormatSource)
        { // conversion from 32bpp -> 32 bpp
          unsafe
          { // Source(32bpp)
            Int32 BWS = BitmapSource.Width;
            Int32 BHS = BitmapSource.Height;
            BitmapData DataS = BitmapSource.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                     ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp)
            bitmaptarget24bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                // NC *PByteT = BOpacity; // Target(32bpp)
                // NC PByteT++;
                // NC Source(24bpp)
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            BitmapSource.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap256 (no Palette) → Bitmap32bpp
    //#################################################################################
    //
    public static Boolean CopyBitmap256ToBitmap32bpp(CBitmap256 bitmapsource256,
                                                     Byte opacity, // 0..100%
                                                     out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        Bitmap BitmapSource = bitmapsource256.GetBitmapArgb();
        PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = BuildOpacityByte(opacity);
        if (PixelFormat.Format32bppRgb == PixelFormatSource)
        { // conversion from 32bpp -> 32 bpp
          unsafe
          { // Source(32bpp)
            Int32 BWS = BitmapSource.Width;
            Int32 BHS = BitmapSource.Height;
            BitmapData DataS = BitmapSource.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                     ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp)
            bitmaptarget32bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                *PByteT = BOpacity; // Target(32bpp)
                PByteT++;
                // NC Source(24bpp)
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            BitmapSource.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap256 (no Palette) → BitmapRgb
    //#################################################################################
    //
    public static Boolean CopyBitmap256ToBitmapRgb(CBitmap256 bitmapsource256,
                                                   out CBitmapRgb bitmaptargetrgb)
    {
      bitmaptargetrgb = null;
      try
      { // Force Source to 24bpp
        Bitmap BitmapSource = bitmapsource256.GetBitmapRgb(); //////!!!!!!!!!!!!!!!!!!!!!!!!!!CMemory.Copy...
        PixelFormat PixelFormatSource = BitmapSource.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (BitmapSource is Bitmap)
        { // conversion from 24bpp -> 24 bpp
          unsafe
          { // Source(24bpp) 256 forced RGB
            Int32 BWS = BitmapSource.Width;
            Int32 BHS = BitmapSource.Height;
            BitmapData DataS = BitmapSource.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                     ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp) RGB
            bitmaptargetrgb = new CBitmapRgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptargetrgb.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                               ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp) -> Target(24bpp)
                // Target - B - 8Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - A - 32Bit
                // NC only 24Bit
              }
            }
            bitmaptargetrgb.Bitmap.UnlockBits(DataT);
            BitmapSource.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap256 (no Palette) → BitmapArgb
    //#################################################################################
    //
    public static Boolean CopyBitmap256ToBitmapArgb(CBitmap256 bitmapsource256,
                                                    Byte opacity, // 0..100%
                                                    out CBitmapArgb bitmaptargetargb)
    {
      bitmaptargetargb = null;
      Byte BOpacity = BuildOpacityByte(opacity);
      try
      { // Force Source to 32bpp
        Bitmap BitmapSource = bitmapsource256.GetBitmapArgb();
        PixelFormat PixelFormatSource = BitmapSource.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        if (BitmapSource is Bitmap)
        { // conversion from 32bpp -> 32bpp
          unsafe
          { // Source(32bpp) 256 forced ARGB
            Int32 BWS = BitmapSource.Width;
            Int32 BHS = BitmapSource.Height;
            BitmapData DataS = BitmapSource.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                     ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) ARGB
            bitmaptargetargb = new CBitmapArgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptargetargb.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(32bpp) -> Target(32bpp)
                // Target - B - 8Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - A - 32Bit
                *PByteT = BOpacity;
                PByteS++;
                PByteT++;
              }
            }
            bitmaptargetargb.Bitmap.UnlockBits(DataT);
            BitmapSource.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
