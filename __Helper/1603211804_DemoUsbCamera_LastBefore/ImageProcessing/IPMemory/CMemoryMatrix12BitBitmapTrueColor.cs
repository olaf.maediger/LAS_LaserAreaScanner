﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMemoryMatrix12BitBitmapTrueColor : CMemory
  { //
    //#################################################################################
    //  Copy Matrix12Bit (no Palette) -> Bitmap24bpp (Grey)
    //#################################################################################
    //
    public static Boolean CopyMatrix12bitToBitmap24bpp(CMatrix12Bit matrixsource12bit,
                                                       out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        unsafe
        { // Source Matrix12Bit
          Int32 BWS = matrixsource12bit.ColCount;
          Int32 BHS = matrixsource12bit.RowCount;
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix10bit (2 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08Bit
              Int32 IValue = matrixsource12bit[XI, YI] >> 4;
              Byte BValue = (Byte)Math.Max(0, Math.Min(255, IValue));
              // Target (3 Byte)
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
            }
          }
          bitmaptarget24bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Matrix12Bit (no Palette) -> Bitmap32bpp (Grey)
    //#################################################################################
    //
    public static Boolean CopyMatrix12BitToBitmap32bpp(CMatrix12Bit matrixsource12bit,
                                                       Byte opacity, // 0..100%
                                                       out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = BuildOpacityByte(opacity);
        unsafe
        { // Source Matrix08Bit
          Int32 BWS = matrixsource12bit.ColCount;
          Int32 BHS = matrixsource12bit.RowCount;
          // Target(32bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix08Bit (1 Byte) + Palette256 -> Bitmap32bpp (4 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08Bit
              Byte BValue = (Byte)(matrixsource12bit[YI, XI] >> 4);
              // Target (4 Byte)
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BOpacity;
              PByteT++;
            }
          }
          bitmaptarget32bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }



  }
}
