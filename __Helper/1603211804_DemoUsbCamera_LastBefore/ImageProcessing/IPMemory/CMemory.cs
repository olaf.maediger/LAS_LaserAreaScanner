﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public abstract partial class CMemory
  {
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Common
    //---------------------------------------------------------------------------------
    //
    public static Byte BuildOpacityByte(Byte opacity)
    {
      Byte Result = (Byte)Math.Min(255f, 255f * (float)opacity / 100f);
      return (Byte)Math.Max(0f, Result);
    }

    public static Byte BuildPaletteIndexFromColor(Byte colorr, Byte colorg, Byte colorb)
    {
      return (Byte)(0.299f * (float)colorr + 0.587f * (float)colorg + 0.114f * (float)colorb);
    }


    public static UInt16 BuildGreyValue10Bit(Byte colorr, Byte colorg, Byte colorb)
    {
      return (UInt16)(1.196f * (float)colorr + 2.348f * (float)colorg + 0.456f * (float)colorb);             
    }


  }
}
