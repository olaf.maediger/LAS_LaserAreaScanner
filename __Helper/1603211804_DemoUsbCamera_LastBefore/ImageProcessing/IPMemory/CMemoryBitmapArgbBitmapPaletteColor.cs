﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public class CMemoryBitmapArgbBitmapPaletteColor : CMemory
  {
  
    
    //
    //#################################################################################
    //  Copy BitmapArgb + Palette256 → Bitmap256
    //#################################################################################
    //
    public static Boolean CopyBitmapArgbPalette256ToBitmap256(CBitmapArgb bitmapsourceargb,
                                                              CPalette256 palette256,
                                                              out CBitmap256 bitmaptarget256)
    {
      bitmaptarget256 = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsourceargb.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        bitmaptarget256 = new CBitmap256(bitmapsourceargb.Bitmap, palette256, PixelFormatTarget);
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }



    //
    //#################################################################################
    //  Copy BitmapArgb + Palette256 → BitmapRgb
    //#################################################################################
    //
    public static Boolean CopyBitmapArgbPalette256ToBitmapRgb(CBitmapArgb bitmapsourceargb,
                                                              CPalette256 palette256,
                                                              out CBitmapRgb bitmaptargetrgb)
    {
      bitmaptargetrgb = null;
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (bitmapsourceargb is CBitmapArgb)
        { // conversion from 32bpp -> 24 bpp
          unsafe
          { // Source(32bpp) ARGB
            Int32 BWS = bitmapsourceargb.Width;
            Int32 BHS = bitmapsourceargb.Height;
            BitmapData DataS = bitmapsourceargb.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                                ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) RGB
            bitmaptargetrgb = new CBitmapRgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptargetrgb.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                               ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(32bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++; // NC 
                Byte PI = BuildPaletteIndexFromColor(R, G, B);
                // Target(24bpp)
                *PByteT = palette256[PI].B;
                PByteT++;
                *PByteT = palette256[PI].G;
                PByteT++;
                *PByteT = palette256[PI].R;
              }
            }
            bitmaptargetrgb.Bitmap.UnlockBits(DataT);
            bitmapsourceargb.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }



  }
}
