﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPMemory
{
  public class CMatrix16Bit : CMatrix
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private UInt16[,] FMatrix;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrix16Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrix = new UInt16[FColCount, FRowCount];
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    private UInt16 GetValue(Int32 colindex, Int32 rowindex)
    {
      if ((0 <= colindex) && (colindex < FMatrix.GetLength(0)))
      {
        if ((0 <= rowindex) && (rowindex < FMatrix.GetLength(1)))
        {
          return FMatrix[colindex, rowindex];
        }
      }
      return 0x0000;
    }
    private void SetValue(Int32 colindex, Int32 rowindex, UInt16 value)
    {
      if ((0 <= colindex) && (colindex < FMatrix.GetLength(0)))
      {
        if ((0 <= rowindex) && (rowindex < FMatrix.GetLength(1)))
        {
          FMatrix[colindex, rowindex] = value;
        }
      }
    }
    public UInt16 this[Int32 colindex, Int32 rowindex]
    {
      get { return GetValue(colindex, rowindex); }
      set { SetValue(colindex, rowindex, value); }
    }

  }
}
