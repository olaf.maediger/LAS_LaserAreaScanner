﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public class CMemoryBitmap32bppBitmapPaletteColor : CMemory
  {
    //
    //#################################################################################
    //  Copy Bitmap32bpp + Palette256 → Bitmap24bpp
    //#################################################################################
    //
    public static Boolean CopyBitmap32bppPalette256ToBitmap24bpp(Bitmap bitmapsource32bpp,
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource32bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (PixelFormat.Format32bppArgb == PixelFormatSource)
        {
          unsafe
          { // Source(32bpp)
            Int32 BWS = bitmapsource32bpp.Width;
            Int32 BHS = bitmapsource32bpp.Height;
            BitmapData DataS = bitmapsource32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            // Bitmap32bpp (4 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // --- Source(32bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                PByteS++; // ignore A 
                Byte PI = BuildPaletteIndexFromColor(R, G, B);
                // --- Target(24bpp)
                Color PC = palette256[PI];
                // Target - B
                *PByteT = PC.B;
                PByteT++;
                // Target - G
                *PByteT = PC.G;
                PByteT++;
                // Target - R
                *PByteT = PC.R;
                PByteT++;
                // Target - A
                // NC
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            bitmapsource32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap32bpp + Palette256 → Bitmap32bpp
    //#################################################################################
    //
    public static Boolean CopyBitmap32bppPalette256ToBitmap32bpp(Bitmap bitmapsource32bpp,
                                                                 Byte opacity, // 0..100%
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource32bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = BuildOpacityByte(opacity);
        if (PixelFormat.Format32bppArgb == PixelFormatSource)
        {
          unsafe
          { // Source(32bpp)
            Int32 BWS = bitmapsource32bpp.Width;
            Int32 BHS = bitmapsource32bpp.Height;
            BitmapData DataS = bitmapsource32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            // Copy Bitmap32bpp (4 Byte) + Palette256 -> Bitmap32bpp (4 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                Byte PI = BuildPaletteIndexFromColor(R, G, B);
                // Target
                *PByteT = palette256[PI].B;
                PByteT++;
                *PByteT = palette256[PI].G;
                PByteT++;
                *PByteT = palette256[PI].R;
                PByteT++;
                *PByteT = BOpacity;
                PByteT++;
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsource32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }



  }
}
