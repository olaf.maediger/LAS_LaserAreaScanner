﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public class CMemoryBitmap24bppMatrixPaletteColor : CMemory
  {

    //
    //#################################################################################
    //  Copy Bitmap24bpp + Palette256 → MatrixByte
    //#################################################################################
    //
    public static Boolean CopyBitmap24bppPalette256ToMatrixByte(Bitmap bitmapsource24bpp,
                                                                CPalette256 palette256,
                                                                out Byte[,] matrixtargetbyte)
    {
      matrixtargetbyte = null;
      try
      {
        if (PixelFormat.Format24bppRgb == bitmapsource24bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(08bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrixtargetbyte = new Byte[BWT, BHT];
            // Copy Bitmap24bpp (3 Byte) -> Palette256 -> Matrix08Bit (1 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                // Target
                Byte PI = BuildPaletteIndexFromColor(R, G, B);
                B = palette256[PI].B;
                G = palette256[PI].G;
                R = palette256[PI].R;
                Byte BValue = BuildPaletteIndexFromColor(R, G, B);
                matrixtargetbyte[XI, YI] = BValue;
              }
            }
            bitmapsource24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }






    //
    //#################################################################################
    //  Copy Bitmap24bpp + Palette256 → MatrixUInt10
    //#################################################################################
    //
    public static Boolean CopyBitmap24bppPalette256ToMatrixUInt10(Bitmap bitmap24bpp,
                                                                  CPalette256 palette256,
                                                                  out UInt16[,] matrixuint10)
    {
      matrixuint10 = null;
      try
      {
        if (PixelFormat.Format24bppRgb == bitmap24bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap24bpp.Width;
            Int32 BHS = bitmap24bpp.Height;
            BitmapData DataS = bitmap24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(10bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrixuint10 = new UInt16[BWT, BHT];
            // Copy Bitmap24bpp (3 Byte) -> Palette256 -> Matrix10bit (2 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                // Target(16bit)
                Byte PI = BuildPaletteIndexFromColor(R, G, B);
                B = palette256[PI].B;
                G = palette256[PI].G;
                R = palette256[PI].R;
                UInt16 UValue = BuildGreyValue10Bit(R, G, B);
                matrixuint10[XI, YI] = UValue;
              }
            }
            bitmap24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
