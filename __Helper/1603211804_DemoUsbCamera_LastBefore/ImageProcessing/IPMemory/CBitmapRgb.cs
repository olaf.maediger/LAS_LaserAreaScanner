﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public class CBitmapRgb : CBitmap
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmapRgb(Int32 width, Int32 height)
      : base(width, height, PixelFormat.Format24bppRgb)
    {
    }

    public CBitmapRgb(Bitmap bitmap)
      : base(bitmap.Width, bitmap.Height, PixelFormat.Format24bppRgb)
    {
      if (PixelFormat.Format24bppRgb == bitmap.PixelFormat)
      {
        CMemoryBitmap24bppBitmapTrueColor.CopyBitmap24bppToBitmap24bpp(bitmap, out FBitmap);
      } 
      else
        if (PixelFormat.Format32bppRgb == bitmap.PixelFormat)
        {
          CMemoryBitmap32bppBitmapTrueColor.CopyBitmap32bppToBitmap24bpp(bitmap, out FBitmap);
        } 
    }

    public CBitmapRgb(Bitmap bitmap, CPalette256 palette256)
      : base(bitmap.Width, bitmap.Height, PixelFormat.Format24bppRgb)
    {
      if (PixelFormat.Format24bppRgb == bitmap.PixelFormat)
      {
        CMemoryBitmap24bppBitmapPaletteColor.CopyBitmap24bppPalette256ToBitmap24bpp(bitmap, palette256, out FBitmap);
      } 
      else
        if (PixelFormat.Format32bppRgb == bitmap.PixelFormat)
        {
          CMemoryBitmap32bppBitmapPaletteColor.CopyBitmap32bppPalette256ToBitmap24bpp(bitmap, palette256, out FBitmap);
        } 
    }
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //
    public override Boolean GetMatrix(out Byte[,] matrix,
                                      Int32 offsetx, Int32 offsety,
                                      Int32 sizex, Int32 sizey)
    {
      matrix = null;
      return false;
    }

    public override Boolean SetMatrix(Byte[,] matrix,
                                      Int32 offsetx, Int32 offsety,
                                      Int32 sizex, Int32 sizey)
    {
      return false;
    }

    public override Boolean SetBitmap(Bitmap bitmap)
    {
      try
      {
        return SetBitmap(bitmap, PixelFormat.Format32bppArgb); // Definition!!!
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean SetBitmap(Bitmap bitmap, PixelFormat pixelformat)
    {
      try
      {
        if (!(bitmap is Bitmap)) return false;
        switch (bitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            switch (pixelformat)
            {
              case PixelFormat.Format24bppRgb:
                return CMemoryBitmap24bppBitmapTrueColor.CopyBitmap24bppToBitmap24bpp(bitmap, out FBitmap);
              case PixelFormat.Format32bppArgb:
                return CMemoryBitmap24bppBitmapTrueColor.CopyBitmap24bppToBitmap32bpp(bitmap, 100, out FBitmap);
            }
            return false;
          case PixelFormat.Format32bppArgb:
            switch (pixelformat)
            {
              case PixelFormat.Format24bppRgb:
                return CMemoryBitmap32bppBitmapTrueColor.CopyBitmap32bppToBitmap24bpp(bitmap, out FBitmap);
              case PixelFormat.Format32bppArgb:
                return CMemoryBitmap32bppBitmapTrueColor.CopyBitmap32bppToBitmap32bpp(bitmap, 100, out FBitmap);
            }
            return false;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Helper
    //--------------------------------------------------------
    //
    //private Boolean CopyBitmap24bppTo24bpp(Bitmap bitmap)
    //{
    //  try
    //  {
    //    if (PixelFormat.Format24bppRgb == bitmap.PixelFormat)
    //    {
    //      unsafe
    //      { // Source - bitmap
    //        Int32 BWS = bitmap.Width;
    //        Int32 BHS = bitmap.Height;
    //        BitmapData DataS = bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
    //                                           ImageLockMode.ReadOnly, bitmap.PixelFormat);
    //        // Target - FBitmap
    //        FBitmap = new Bitmap(bitmap.Width, bitmap.Height, PixelFormat.Format24bppRgb);
    //        Int32 BWT = FBitmap.Width;
    //        Int32 BHT = FBitmap.Height;
    //        BitmapData DataT = FBitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
    //                                            ImageLockMode.WriteOnly, FBitmap.PixelFormat);
    //        //
    //        Int32 XIL = 0;
    //        Int32 XIH = BWT - 1;
    //        Int32 YIL = 0;
    //        Int32 YIH = BHT - 1;
    //        for (Int32 YI = YIL; YI <= YIH; YI++)
    //        {
    //          Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
    //          Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
    //          for (Int32 XI = XIL; XI <= XIH; XI++)
    //          { // B - 8Bit
    //            *PByteT = *PByteS;
    //            PByteT++; PByteS++;
    //            // G - 16Bit
    //            *PByteT = *PByteS;
    //            PByteT++; PByteS++;
    //            // R - 24Bit
    //            *PByteT = *PByteS;
    //            PByteT++; PByteS++;
    //            // NC A
    //          }
    //        }
    //        bitmap.UnlockBits(DataS);
    //        FBitmap.UnlockBits(DataT);
    //      }
    //      return true;
    //    }
    //    return false;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

    //private Boolean CopyBitmap32bppTo24bpp(Bitmap bitmap)
    //{
    //  try
    //  {
    //    if (PixelFormat.Format32bppArgb == bitmap.PixelFormat)
    //    {
    //      unsafe
    //      { // Source - bitmap
    //        Int32 BWS = bitmap.Width;
    //        Int32 BHS = bitmap.Height;
    //        BitmapData DataS = bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
    //                                           ImageLockMode.ReadOnly, bitmap.PixelFormat);
    //        // Target - FBitmap
    //        FBitmap = new Bitmap(bitmap.Width, bitmap.Height, PixelFormat.Format24bppRgb);
    //        Int32 BWT = FBitmap.Width;
    //        Int32 BHT = FBitmap.Height;
    //        BitmapData DataT = FBitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
    //                                            ImageLockMode.WriteOnly, FBitmap.PixelFormat);
    //        //
    //        Int32 XIL = 0;
    //        Int32 XIH = BWT - 1;
    //        Int32 YIL = 0;
    //        Int32 YIH = BHT - 1;
    //        for (Int32 YI = YIL; YI <= YIH; YI++)
    //        {
    //          Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
    //          Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
    //          for (Int32 XI = XIL; XI <= XIH; XI++)
    //          { // B - 8Bit
    //            *PByteT = *PByteS;
    //            PByteT++; PByteS++;
    //            // G - 16Bit
    //            *PByteT = *PByteS;
    //            PByteT++; PByteS++;
    //            // R - 24Bit
    //            *PByteT = *PByteS;
    //            PByteT++; PByteS++;
    //            // NC A
    //            *PByteT = 0xFF;
    //            PByteT++; // no increment source!
    //          }
    //        }
    //        bitmap.UnlockBits(DataS);
    //        FBitmap.UnlockBits(DataT);
    //      }
    //      return true;
    //    }
    //    return false;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}
    //
    //--------------------------------------------------------
    //	Segment - Public
    //--------------------------------------------------------
    //

  }
}
