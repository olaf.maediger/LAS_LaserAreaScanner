﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public abstract class CBitmap : CMemory
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //
    public static Int32 INIT_BITMAPWIDTH = 1;
    public static Int32 INIT_BITMAPHEIGHT = 1;
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    protected Byte[,] FMatrix08Bit;
    protected UInt16[,] FMatrix10Bit;
    protected Bitmap FBitmap; // Extern, SpecialType // Bitmap -> Display (24bitRgb or 32bitArgb)
    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmap(Int32 width, Int32 height, PixelFormat pixelformat)
    {
      Int32 W = Math.Max(INIT_BITMAPWIDTH, width);
			Int32 H = Math.Max(INIT_BITMAPHEIGHT, height);
      FBitmap = new System.Drawing.Bitmap(W, H, pixelformat);
    }
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //   
    public Bitmap Bitmap
    {
      get { return FBitmap; }
    }

    public PixelFormat PixelFormat
    {
      get { return FBitmap.PixelFormat; }
    }

    public Int32 Width
    {
      get { return FBitmap.Width; }
    }
    public Int32 Height
    {
      get { return FBitmap.Height; }
    }

    public abstract Boolean GetMatrix(out Byte[,] matrix,
                                      Int32 offsetx, Int32 offsety,
                                      Int32 sizex, Int32 sizey);

    public abstract Boolean SetMatrix(Byte[,] matrix,
                                      Int32 offsetx, Int32 offsety,
                                      Int32 sizex, Int32 sizey);

    public Bitmap GetBitmapRgb()
    { // 24bpp/32bpp -> 24bpp
      PixelFormat PF = FBitmap.PixelFormat;
      switch (PF)
      {
        case PixelFormat.Format24bppRgb:
          return FBitmap;
        case PixelFormat.Format32bppArgb:
          Bitmap B24;
          CMemoryBitmap32bppBitmapTrueColor.CopyBitmap32bppToBitmap24bpp(FBitmap, out B24);
          return B24;
      }
      return null;
    }

    public Bitmap GetBitmapArgb()
    { // 24bpp/32bpp -> 32bpp
      PixelFormat PF = FBitmap.PixelFormat;
      switch (PF)
      {
        case PixelFormat.Format24bppRgb:
          Bitmap B32;
          CMemoryBitmap24bppBitmapTrueColor.CopyBitmap24bppToBitmap32bpp(FBitmap, 100, out B32);
          return B32;
        case PixelFormat.Format32bppArgb:
          return FBitmap;
      }
      return null;
    }

    public abstract Boolean SetBitmap(Bitmap bitmap);
    public abstract Boolean SetBitmap(Bitmap bitmap, PixelFormat pixelformat);

    //
    //--------------------------------------------------------
    //	Segment - Public
    //--------------------------------------------------------
    //
    public Boolean Draw(Graphics graphics,
                        Int32 offsetx, Int32 offsety,
                        Int32 sizex, Int32 sizey)
    { // BitmapTarget -> graphics
      if (FBitmap is Bitmap)
      {
        graphics.DrawImage(FBitmap, offsetx, offsety, sizex, sizey);
        return true;
      }
      return false;
    }
 
  }
}
