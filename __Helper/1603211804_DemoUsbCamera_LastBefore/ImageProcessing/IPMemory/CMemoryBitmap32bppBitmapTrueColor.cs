﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMemoryBitmap32bppBitmapTrueColor : CMemory
  {
    //
    //#################################################################################
    //  Copy Bitmap32bpp (no Palette) → Bitmap24bpp
    //#################################################################################
    //
    public static Boolean CopyBitmap32bppToBitmap24bpp(Bitmap bitmapsource32bpp,
                                                       out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource32bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (PixelFormat.Format32bppArgb == PixelFormatSource)
        { // conversion from 32bpp -> 24bpp
          unsafe
          { // Source(32bpp)
            Int32 BWS = bitmapsource32bpp.Width;
            Int32 BHS = bitmapsource32bpp.Height;
            BitmapData DataS = bitmapsource32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                PByteS++; // Source(32bpp)
                // NC Target(24bpp)
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            bitmapsource32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap32bpp (no Palette) → Bitmap32bpp
    //#################################################################################
    //
    public static Boolean CopyBitmap32bppToBitmap32bpp(Bitmap bitmapsource32bpp,
                                                       Byte opacity, // 0..100%
                                                       out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource32bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = BuildOpacityByte(opacity);
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        { // conversion from 32bpp -> 32 bpp
          unsafe
          { // Source(32bpp)
            Int32 BWS = bitmapsource32bpp.Width;
            Int32 BHS = bitmapsource32bpp.Height;
            BitmapData DataS = bitmapsource32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp)
            bitmaptarget32bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                *PByteT = BOpacity; // Target(32bpp)
                PByteT++; PByteS++;
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsource32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }




  }
}
