﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMemoryMatrix10BitMatrixTrueColor : CMemory
  {


    //
    //#################################################################################
    //  Copy MatrixUInt10 (no Palette) → MatrixByte
    //#################################################################################
    //
    public static Boolean CopyMatrixUInt10ToMatrixByte(UInt16[,] matrixsourceuint10,
                                                       out Byte[,] matrixtargetbyte)
    {
      matrixtargetbyte = null;
      try
      {
        unsafe
        { // Source MatrixUInt10
          Int32 BWS = matrixsourceuint10.GetLength(0);
          Int32 BHS = matrixsourceuint10.GetLength(1);
          // Target Matrix10bit
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          matrixtargetbyte = new Byte[BWT, BHT];
          // Copy Matrix10bit (2 Byte) -> Matrix08Bit (1 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source MatrixUInt10
              Byte BValue = (Byte)(matrixsourceuint10[XI, YI] >> 2);
              // Target MatrixByte
              matrixtargetbyte[XI, YI] = BValue;
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }




  }
}
