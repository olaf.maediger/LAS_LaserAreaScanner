﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMemoryMatrixUInt10BitmapTrueColor : CMemory
  {




    //
    //#################################################################################
    //  Copy MatrixUInt10 (no Palette) -> Bitmap32bpp (Grey)
    //#################################################################################
    //
    public static Boolean CopyMatrixUInt10ToBitmap32bpp(UInt16[,] matrixsourceuint10,
                                                        Byte opacity, // 0..100%
                                                        out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppRgb;
        Byte BOpacity = CMemory.BuildOpacityByte(opacity);
        unsafe
        { // Source MatrixUInt16
          Int32 BWS = matrixsourceuint10.GetLength(0);
          Int32 BHS = matrixsourceuint10.GetLength(1);
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy MatrixUInt16 (2 Byte) >> 8 + Palette256 -> Bitmap32bpp (4 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source MatrixUInt16
              Int32 IValue = matrixsourceuint10[XI, YI] >> 2;
              Byte BValue = (Byte)Math.Max(0, Math.Min(255, IValue));
              // Target (4 Byte)
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BOpacity;
              PByteT++;
            }
          }
          bitmaptarget32bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }





  }
}
