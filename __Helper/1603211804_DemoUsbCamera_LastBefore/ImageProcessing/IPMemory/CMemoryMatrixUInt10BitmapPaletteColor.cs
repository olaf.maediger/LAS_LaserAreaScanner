﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public class CMemoryMatrixUInt10BitmapPaletteColor : CMemory
  {


    //
    //#################################################################################
    //  Copy MatrixUInt10 + Palette256 → Bitmap24bpp
    //#################################################################################
    //
    public static Boolean CopyMatrixUInt10Palette256ToBitmap24bpp(UInt16[,] matrixsourceuint10,
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        unsafe
        { // Source Matrix08Bit
          Int32 BHS = matrixsourceuint10.GetLength(1);
          Int32 BWS = matrixsourceuint10.GetLength(0);
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix10bit (2 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08Bit
              Int32 IValue = matrixsourceuint10[XI, YI] >> 2;
              Byte PI = (Byte)Math.Max(0, Math.Min(255, IValue));
              // Target (3 Byte)
              *PByteT = palette256[PI].B;
              PByteT++;
              *PByteT = palette256[PI].G;
              PByteT++;
              *PByteT = palette256[PI].R;
              PByteT++;
            }
          }
          bitmaptarget24bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy MatrixUInt10 + Palette256 → Bitmap32bpp
    //#################################################################################
    //
    public static Boolean CopyMatrix10BitPalette256ToBitmap32bpp(UInt16[,] matrixsourceuint10,
                                                                 Byte opacity, // 0..100%
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        Byte BOpacity = BuildOpacityByte(opacity);
        unsafe
        { // Source Matrix08Bit
          Int32 BHS = matrixsourceuint10.GetLength(1);
          Int32 BWS = matrixsourceuint10.GetLength(0);
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix10bit (2 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08Bit
              Int32 IValue = matrixsourceuint10[XI, YI] >> 2;
              Byte PI = (Byte)Math.Max(0, Math.Min(255, IValue));
              // Target (3 Byte)
              *PByteT = palette256[PI].B;
              PByteT++;
              *PByteT = palette256[PI].G;
              PByteT++;
              *PByteT = palette256[PI].R;
              PByteT++;
              *PByteT = BOpacity;
              PByteT++;
            }
          }
          bitmaptarget32bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }




  }
}
