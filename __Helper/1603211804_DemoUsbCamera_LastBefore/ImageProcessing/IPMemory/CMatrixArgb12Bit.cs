﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPMemory
{
  public class CMatrixArgb12Bit : CMatrix
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private UInt16[,] FMatrixA;
    private UInt16[,] FMatrixR;
    private UInt16[,] FMatrixG;
    private UInt16[,] FMatrixB;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrixArgb12Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrixA = new UInt16[colcount, rowcount];
      FMatrixR = new UInt16[colcount, rowcount];
      FMatrixG = new UInt16[colcount, rowcount];
      FMatrixB = new UInt16[colcount, rowcount];
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    //protected override Int32 GetColCount()
    //{
    //  return FMatrix.GetLength(INDEX_COL);
    //}

    //protected override Int32 GetRowCount()
    //{
    //  return FMatrix.GetLength(INDEX_ROW);
    //}

  }
}
