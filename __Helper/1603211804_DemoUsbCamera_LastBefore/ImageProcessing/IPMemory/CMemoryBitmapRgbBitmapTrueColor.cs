﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMemoryBitmapRgbBitmapTrueColor : CMemory
  {
    //
    //#################################################################################
    //  Copy BitmapRgb (no Palette) → Bitmap24bpp
    //#################################################################################
    //
    public static Boolean CopyBitmapRgbToBitmap24bpp(CBitmapRgb bitmapsourcergb,
                                                     out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (bitmapsourcergb is CBitmapRgb)
        { // conversion from 24bpp -> 24 bpp
          unsafe
          { // Source(24bpp) RGB
            Int32 BWS = bitmapsourcergb.Width;
            Int32 BHS = bitmapsourcergb.Height;
            BitmapData DataS = bitmapsourcergb.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                               ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp) RGB
            bitmaptarget24bpp = new Bitmap(BWS, BHS, PixelFormat.Format24bppRgb);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // BitmapRgb (3 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // NC A - 32Bit
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            bitmapsourcergb.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }    
    //
    //#################################################################################
    //  Copy BitmapRgb (no Palette) → Bitmap32bpp
    //#################################################################################
    //
    public static Boolean CopyBitmapRgbToBitmap32bpp(CBitmapRgb bitmapsourcergb,
                                                     Byte opacity, // 0..100%
                                                     out Bitmap bitmaptarget32bpp)
    {
      Byte BOpacity = BuildOpacityByte(opacity);
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        if (bitmapsourcergb is CBitmapRgb)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp) RGB
            Int32 BWS = bitmapsourcergb.Width;
            Int32 BHS = bitmapsourcergb.Height;
            BitmapData DataS = bitmapsourcergb.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                               ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) ARGB
            bitmaptarget32bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // BitmapRgb (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                *PByteT = BOpacity;
                PByteT++; // NC PByteS++;
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsourcergb.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }




    //
    //#################################################################################
    //  Copy BitmapRgb (no Palette) → Bitmap256 (BW)
    //#################################################################################
    //
    // no sense because of missing palette-parameter!!!
    //public static Boolean CopyBitmapRgbToBitmap256(CBitmapRgb bitmaprgb,
    //                                               out CBitmap256 bitmap256)
    //{
    //  bitmap256 = null;
    //  return false;
    //}


    //
    //#################################################################################
    //  Copy BitmapRgb (no Palette) → BitmapArgb
    //#################################################################################
    //
    public static Boolean CopyBitmapRgbToBitmapArgb(CBitmapRgb bitmapsourcergb,
                                                    Byte opacity, // 0..100%
                                                    out CBitmapArgb bitmaptargetargb)
    {
      bitmaptargetargb = null;
      Byte BOpacity = BuildOpacityByte(opacity);
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        if (bitmapsourcergb is CBitmapRgb)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp) RGB
            Int32 BWS = bitmapsourcergb.Width;
            Int32 BHS = bitmapsourcergb.Height;
            BitmapData DataS = bitmapsourcergb.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                               ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) ARGB
            bitmaptargetargb = new CBitmapArgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptargetargb.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                // Source(24bpp)
                *PByteT = BOpacity;
                PByteT++; // Target(32bpp)                
              }
            }
            bitmaptargetargb.Bitmap.UnlockBits(DataT);
            bitmapsourcergb.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
