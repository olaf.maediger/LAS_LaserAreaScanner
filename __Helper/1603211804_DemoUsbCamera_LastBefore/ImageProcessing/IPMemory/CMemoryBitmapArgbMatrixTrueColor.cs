﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMemoryBitmapArgbMatrixTrueColor : CMemory
  {
    //
    //#################################################################################
    //  Copy BitmapArgb (no Palette) → Bitmap32bpp
    //#################################################################################
    //
    public static Boolean CopyBitmapArgbToBitmap32bpp(CBitmapArgb bitmapsourceargb,
                                                      Byte opacity, // 0..100%
                                                      out Bitmap bitmaptarget32bpp)
    {
      Byte BOpacity = BuildOpacityByte(opacity);
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format32bppRgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        if (bitmapsourceargb is CBitmapArgb)
        { // conversion from 32bpp -> 32bpp
          unsafe
          { // Source(32bpp) ARGB
            Int32 BWS = bitmapsourceargb.Width;
            Int32 BHS = bitmapsourceargb.Height;
            BitmapData DataS = bitmapsourceargb.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                                ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) ARGB
            bitmaptarget32bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // BitmapArgb (4 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                *PByteT = BOpacity;
                PByteT++; PByteS++;
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsourceargb.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
  }
}
