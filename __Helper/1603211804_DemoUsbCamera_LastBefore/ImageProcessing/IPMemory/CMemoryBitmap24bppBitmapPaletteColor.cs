﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public class CMemoryBitmap24bppBitmapPaletteColor : CMemory
  {
    //
    //#################################################################################
    //  Copy Bitmap24bpp + Palette256 → Bitmap24bpp
    //#################################################################################
    //
    public static Boolean CopyBitmap24bppPalette256ToBitmap24bpp(Bitmap bitmapsource24bpp,
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource24bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        {
          unsafe
          { // Source(24bpp)
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            // Copy Bitmap24bpp (3 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = CMemory.BuildPaletteIndexFromColor(R, G, B);
                // Target(24bpp)
                *PByteT = palette256[PI].B;
                PByteT++;
                *PByteT = palette256[PI].G;
                PByteT++;
                *PByteT = palette256[PI].R;
                PByteT++;
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            bitmapsource24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap24bpp + Palette256 → Bitmap32bpp
    //#################################################################################
    //
    public static Boolean CopyBitmap24bppPalette256ToBitmap32bpp(Bitmap bitmapsource24bpp,
                                                                 Byte opacity, // 0..100%
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource24bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = BuildOpacityByte(opacity);
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        {
          unsafe
          { // Source(24bpp)
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            // Copy Bitmap24bpp (3 Byte) + Palette256 -> Bitmap32bpp (4 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = CMemory.BuildPaletteIndexFromColor(R, G, B);
                // Target
                *PByteT = palette256[PI].B;
                PByteT++;
                *PByteT = palette256[PI].G;
                PByteT++;
                *PByteT = palette256[PI].R;
                PByteT++;
                *PByteT = BOpacity;
                PByteT++; // 
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsource24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
  
  
  }
}
