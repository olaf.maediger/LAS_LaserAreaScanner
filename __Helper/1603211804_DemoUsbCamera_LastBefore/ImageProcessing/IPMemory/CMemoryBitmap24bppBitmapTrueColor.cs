﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMemoryBitmap24bppBitmapTrueColor : CMemory
  {
    //
    //#################################################################################
    //  Copy Bitmap24bpp (no Palette) → Bitmap24bpp
    //#################################################################################
    //
    public static Boolean CopyBitmap24bppToBitmap24bpp(Bitmap bitmapsource24bpp,
                                                       out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource24bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        { // conversion from 24bpp -> 24bpp
          unsafe
          { // Source(24bpp)
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp)
            bitmaptarget24bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // NC A
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            bitmapsource24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }    
    //
    //#################################################################################
    //  Copy Bitmap24bpp (no Palette) → Bitmap32bpp
    //#################################################################################
    //
    public static Boolean CopyBitmap24bppToBitmap32bpp(Bitmap bitmapsource24bpp,
                                                       Byte opacity, // 0..100%
                                                       out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource24bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = BuildOpacityByte(opacity);
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp)
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp)
            bitmaptarget32bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                *PByteT = BOpacity; // Target(32bpp)
                PByteT++;
                // NC Source(24bpp)
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsource24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }




    //
    //#################################################################################
    //  Copy Bitmap24bpp (no Palette) → BitmapRgb
    //#################################################################################
    //
    public static Boolean CopyBitmap24bppToBitmapRgb(Bitmap bitmapsource24bpp,
                                                       out CBitmapRgb bitmaptargetrgb)
    {
      bitmaptargetrgb = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource24bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        { // conversion from 24bpp -> 24bpp
          unsafe
          { // Source(24bpp)
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp)
            bitmaptargetrgb = new CBitmapRgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptargetrgb.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // NC A
              }
            }
            bitmaptargetrgb.Bitmap.UnlockBits(DataT);
            bitmapsource24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }    //



  }
}
