﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMatrix08Bit : CMatrix
  { //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private Byte[,] FMatrix;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrix08Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrix = new Byte[FColCount, FRowCount];
    }

    public CMatrix08Bit(Bitmap bitmap)
      : base(bitmap.Width, bitmap.Height)
    {
      switch (bitmap.PixelFormat)
      {
        case PixelFormat.Format24bppRgb:
          CMemoryBitmap24bppMatrixTrueColor.CopyBitmap24bppToMatrixByte(bitmap, out FMatrix);
          break;
        case PixelFormat.Format32bppArgb:
          CMemoryBitmap32bppMatrixTrueColor.CopyBitmap32bppToMatrixByte(bitmap, out FMatrix);
          break;
      }      
    }

    public CMatrix08Bit(CBitmap bitmap)
      : base(bitmap.Width, bitmap.Height)
    {
      if (bitmap is CBitmapRgb)
      {
        CMemoryBitmap24bppMatrixTrueColor.CopyBitmap24bppToMatrixByte(bitmap.Bitmap, out FMatrix);
      }
      if (bitmap is CBitmapArgb)
      {
        CMemoryBitmap32bppMatrixTrueColor.CopyBitmap32bppToMatrixByte(bitmap.Bitmap, out FMatrix);
      }
      if (bitmap is CBitmap256)
      {
        //!!!!!!!!!!!!!!!!!!!!!!!!  CMemoryBitmap256MatrixTrueColor.CopyBitmap256ToMatrixByte(bitmap, out FMatrix);
      }
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    private Byte GetValue(Int32 colindex, Int32 rowindex)
    {
      if ((0 <= colindex) && (colindex < FMatrix.GetLength(0)))
      {
        if ((0 <= rowindex) && (rowindex < FMatrix.GetLength(1)))
        {
          return FMatrix[colindex, rowindex];
        }
      }
      return 0x00;
    }
    private void SetValue(Int32 colindex, Int32 rowindex, Byte value)
    {
      if ((0 <= colindex) && (colindex < FMatrix.GetLength(0)))
      {
        if ((0 <= rowindex) && (rowindex < FMatrix.GetLength(1)))
        {
          FMatrix[colindex, rowindex] = value;
        }
      }
    }
    public Byte this[Int32 colindex, Int32 rowindex]
    {
      get { return GetValue(colindex, rowindex); }
      set { SetValue(colindex, rowindex, value); }
    }

  }
}
