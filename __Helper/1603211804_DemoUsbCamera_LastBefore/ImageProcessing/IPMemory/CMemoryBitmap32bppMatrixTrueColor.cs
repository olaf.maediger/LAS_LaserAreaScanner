﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMemoryBitmap32bppMatrixTrueColor : CMemory
  {

    //
    //#################################################################################
    //  Copy Bitmap32bpp (no Palette) → MatrixByte
    //#################################################################################
    //
    public static Boolean CopyBitmap32bppToMatrixByte(Bitmap bitmap32bpp,
                                                      out Byte[,] matrixbyte)
    {
      matrixbyte = null;
      try
      {
        if (PixelFormat.Format32bppArgb == bitmap32bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap32bpp.Width;
            Int32 BHS = bitmap32bpp.Height;
            BitmapData DataS = bitmap32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(08bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrixbyte = new Byte[BWT, BHT];
            // Copy Bitmap24bpp (3 Byte) -> Matrix08Bit (1 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                // Target
                Byte BValue = BuildPaletteIndexFromColor(R, G, B);
                matrixbyte[XI, YI] = BValue;
              }
            }
            bitmap32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap32bpp (no Palette) → MatrixUInt10
    //#################################################################################
    //
    public static Boolean CopyBitmap32bppToMatrixUInt10(Bitmap bitmap32bpp,
                                                        out UInt16[,] matrixuint10)
    {
      matrixuint10 = null;
      try
      {
        if (PixelFormat.Format32bppArgb == bitmap32bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap32bpp.Width;
            Int32 BHS = bitmap32bpp.Height;
            BitmapData DataS = bitmap32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(08bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrixuint10 = new UInt16[BWT, BHT];
            // Copy Bitmap32bpp (4 Byte) -> Matrix10bit (2 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                // Target
                UInt16 UValue = BuildGreyValue10Bit(R, G, B);
                matrixuint10[XI, YI] = UValue;
              }
            }
            bitmap32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }


    //
    //#################################################################################
    //  Copy Bitmap32bpp (no Palette) → Matrix08Bit
    //#################################################################################
    //
    public static Boolean CopyBitmap32bppToMatrix08Bit(Bitmap bitmap32bpp,
                                                       out CMatrix08Bit matrix08Bit)
    {
      matrix08Bit = null;
      try
      {
        if (PixelFormat.Format32bppArgb == bitmap32bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap32bpp.Width;
            Int32 BHS = bitmap32bpp.Height;
            BitmapData DataS = bitmap32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(08bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrix08Bit = new CMatrix08Bit(BWT, BHT);
            // Copy Bitmap24bpp (3 Byte) -> Matrix08Bit (1 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                // Target
                Byte BValue = BuildPaletteIndexFromColor(R, G, B);
                matrix08Bit[XI, YI] = BValue;
              }
            }
            bitmap32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }





  }
}
