﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public class CMemoryBitmap256BitmapPaletteColor : CMemory
  {
    //
    //#################################################################################
    //  Copy Bitmap256 + Palette256 → BitmapRgb 
    //#################################################################################
    //
    public static Boolean CopyBitmap256Palette256ToBitmapRgb(CBitmap256 bitmapsource256,
                                                             CPalette256 palette256,
                                                             out CBitmapRgb bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      { // Force Source to 24bpp
        Bitmap BitmapSource = bitmapsource256.GetBitmapRgb();
        PixelFormat PixelFormatSource = BitmapSource.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (BitmapSource is Bitmap)
        { // conversion from 24bpp -> 24 bpp
          unsafe
          { // Source(24bpp) 256 forced RGB
            Int32 BWS = BitmapSource.Width;
            Int32 BHS = BitmapSource.Height;
            BitmapData DataS = BitmapSource.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                     ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp) RGB
            bitmaptarget24bpp = new CBitmapRgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget24bpp.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                 ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = BuildPaletteIndexFromColor(R, G, B);
                // Target(32bpp)
                // Target - B - 8Bit
                *PByteT = palette256[PI].B;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = palette256[PI].G;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = palette256[PI].R;
                PByteT++;
                // Target - A - 32Bit
                // NC only 24Bit
              }
            }
            bitmaptarget24bpp.Bitmap.UnlockBits(DataT);
            BitmapSource.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap256 + Palette256 → BitmapArgb
    //#################################################################################
    //
    public static Boolean CopyBitmap256Palette256ToBitmapArgb(CBitmap256 bitmapsource256,
                                                              Byte opacity, // 0..100%
                                                              CPalette256 palette256,
                                                              out CBitmapArgb bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      Byte BOpacity = BuildOpacityByte(opacity);
      try
      { // Force Source to 24bpp
        Bitmap BitmapSource = bitmapsource256.GetBitmapRgb();
        PixelFormat PixelFormatSource = BitmapSource.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        if (BitmapSource is Bitmap)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp) 256 forced RGB
            Int32 BWS = BitmapSource.Width;
            Int32 BHS = BitmapSource.Height;
            BitmapData DataS = BitmapSource.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                     ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) ARGB
            bitmaptarget32bpp = new CBitmapArgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                 ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = BuildPaletteIndexFromColor(R, G, B);
                // Target(32bpp)
                // Target - B - 8Bit
                *PByteT = palette256[PI].B;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = palette256[PI].G;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = palette256[PI].R;
                PByteT++;
                // Target - A - 32Bit
                *PByteT = BOpacity;
                PByteT++;
              }
            }
            bitmaptarget32bpp.Bitmap.UnlockBits(DataT);
            BitmapSource.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }



  }
}
