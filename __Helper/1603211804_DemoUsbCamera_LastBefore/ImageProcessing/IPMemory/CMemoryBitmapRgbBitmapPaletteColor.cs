﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public class CMemoryBitmapRgbBitmapPaletteColor : CMemory
  {
    //
    //#################################################################################
    //  Copy BitmapRgb + Palette256 → Bitmap(24bpp)
    //#################################################################################
    //
    public static Boolean CopyBitmapRgbPalette256ToBitmap24bpp(CBitmapRgb bitmapsourcergb,
                                                               CPalette256 palette256,
                                                               out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsourcergb.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        if (bitmapsourcergb is CBitmapRgb)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp) RGB
            Int32 BWS = bitmapsourcergb.Width;
            Int32 BHS = bitmapsourcergb.Height;
            BitmapData DataS = bitmapsourcergb.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                               ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp) 
            bitmaptarget24bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = BuildPaletteIndexFromColor(R, G, B);
                // Target(32bpp)
                // Target - B - 8Bit
                *PByteT = palette256[PI].B;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = palette256[PI].G;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = palette256[PI].R;
                PByteT++;
                // NC !!! Target - A - 32Bit
                // NC *PByteT = BOpacity;
                // NC PByteT++;
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            bitmapsourcergb.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy BitmapRgb + Palette256 → Bitmap(32bpp)
    //#################################################################################
    //
    public static Boolean CopyBitmapRgbPalette256ToBitmap32bpp(CBitmapRgb bitmapsourcergb,
                                                               Byte opacity, // 0..100%
                                                               CPalette256 palette256,
                                                               out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      Byte BOpacity = BuildOpacityByte(opacity);
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        if (bitmapsourcergb is CBitmapRgb)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp) RGB
            Int32 BWS = bitmapsourcergb.Width;
            Int32 BHS = bitmapsourcergb.Height;
            BitmapData DataS = bitmapsourcergb.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                               ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) 
            bitmaptarget32bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = BuildPaletteIndexFromColor(R, G, B);
                // Target(32bpp)
                // Target - B - 8Bit
                *PByteT = palette256[PI].B;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = palette256[PI].G;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = palette256[PI].R;
                PByteT++;
                // Target - A - 32Bit
                *PByteT = BOpacity;
                PByteT++;
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsourcergb.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }


    //
    //#################################################################################
    //  Copy BitmapRgb + Palette256 → Bitmap256
    //#################################################################################
    //
    public static Boolean CopyBitmapRgbPalette256ToBitmap256(CBitmapRgb bitmapsourcergb,
                                                             CPalette256 palette256,
                                                             out CBitmap256 bitmaptarget256)
    {
      bitmaptarget256 = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsourcergb.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        bitmaptarget256 = new CBitmap256(bitmapsourcergb.Bitmap, palette256, PixelFormatTarget);
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    //
    //#################################################################################
    //  Copy BitmapRgb + Palette256 → BitmapArgb
    //#################################################################################
    //
    public static Boolean CopyBitmapRgbPalette256ToBitmapArgb(CBitmapRgb bitmapsourcergb,
                                                              Byte opacity, // 0..100%
                                                              CPalette256 palette256,
                                                              out CBitmapArgb bitmaptargetargb)
    {
      bitmaptargetargb = null;
      Byte BOpacity = BuildOpacityByte(opacity);
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        if (bitmapsourcergb is CBitmapRgb)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp) RGB
            Int32 BWS = bitmapsourcergb.Width;
            Int32 BHS = bitmapsourcergb.Height;
            BitmapData DataS = bitmapsourcergb.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                               ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) ARGB
            bitmaptargetargb = new CBitmapArgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptargetargb.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = BuildPaletteIndexFromColor(R, G, B);
                // Target(32bpp)
                // Target - B - 8Bit
                *PByteT = palette256[PI].B;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = palette256[PI].G;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = palette256[PI].R;
                PByteT++;
                // Target - A - 32Bit
                *PByteT = BOpacity;
                PByteT++;
              }
            }
            bitmaptargetargb.Bitmap.UnlockBits(DataT);
            bitmapsourcergb.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
