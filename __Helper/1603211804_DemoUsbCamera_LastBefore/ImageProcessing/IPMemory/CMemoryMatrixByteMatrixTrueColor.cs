﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPMemory
{
  public class CMemoryMatrixByteMatrixTrueColor : CMemory
  {


    //
    //#################################################################################
    //  Copy MatrixByte (no Palette) → MatrixUInt10
    //#################################################################################
    //
    public static Boolean CopyMatrixByteToMatrixUInt10(Byte[,] matrixsourcebyte,
                                                       out UInt16[,] matrixtargetuint10)
    {
      matrixtargetuint10 = null;
      try
      {
        unsafe
        { // Source MatrixByte
          Int32 BWS = matrixsourcebyte.GetLength(0);
          Int32 BHS = matrixsourcebyte.GetLength(1);
          // Target Matrix10bit
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          matrixtargetuint10 = new UInt16[BWT, BHT];
          // Copy MatrixByte (1 Byte) -> MatrixUInt10 (2 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08Bit
              UInt16 UValue = (UInt16)(matrixsourcebyte[XI, YI] << 2);
              // Target MatrixUInt10
              matrixtargetuint10[XI, YI] = UValue;
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }




  }
}
