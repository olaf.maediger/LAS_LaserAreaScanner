﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPMemory
{
  public class CMemoryMatrixByteBitmapPaletteColor : CMemory
  {


    //
    //#################################################################################
    //  Copy MatrixByte + Palette256 → Bitmap24bpp
    //#################################################################################
    //
    public static Boolean CopyMatrixBytePalette256ToBitmap24bpp(Byte[,] matrixsourcebyte,
                                                                CPalette256 palette256,
                                                                out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        unsafe
        { // Source Matrix08Bit
          Int32 BHS = matrixsourcebyte.GetLength(1);
          Int32 BWS = matrixsourcebyte.GetLength(0);
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix08Bit (1 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08Bit
              Byte PI = matrixsourcebyte[XI, YI];
              // Target (3 Byte)
              *PByteT = palette256[PI].B;
              PByteT++;
              *PByteT = palette256[PI].G;
              PByteT++;
              *PByteT = palette256[PI].R;
              PByteT++;
            }
          }
          bitmaptarget24bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy MatrixByte + Palette256 → Bitmap32bpp
    //#################################################################################
    //
    public static Boolean CopyMatrixBytePalette256ToBitmap32bpp(Byte[,] matrixsourcebyte,
                                                                Byte opacity,
                                                                CPalette256 palette256,
                                                                out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = BuildOpacityByte(opacity);
        unsafe
        { // Source Matrix08Bit
          Int32 BWS = matrixsourcebyte.GetLength(0);
          Int32 BHS = matrixsourcebyte.GetLength(1);
          // Target(32bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix08Bit (1 Byte) + Palette256 -> Bitmap32bpp (4 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08Bit
              Byte PI = matrixsourcebyte[YI, XI];
              // Target (4 Byte)
              *PByteT = palette256[PI].B;
              PByteT++;
              *PByteT = palette256[PI].G;
              PByteT++;
              *PByteT = palette256[PI].R;
              PByteT++;
              *PByteT = BOpacity;
              PByteT++;
            }
          }
          bitmaptarget32bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
