﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPMatrix
{
  public class CMatrix10Bit : CMatrix
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private UInt16[,] FMatrix;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    protected override Int32 GetColCount()
    {
      return FMatrix.GetLength(INDEX_COL);
    }

    protected override Int32 GetRowCount()
    {
      return FMatrix.GetLength(INDEX_ROW);
    }

  }
}
