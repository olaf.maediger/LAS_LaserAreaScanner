﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPMatrix
{
  //
  // Abstarct Basic Class for all Matrices of ImageProcessing
  //
  public abstract class CMatrix
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------------------
    //  
    public const Int32 INDEX_COL = 0;
    public const Int32 INDEX_ROW = 1;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  

    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    protected abstract Int32 GetColCount();
    protected abstract Int32 GetRowCount();

    public Int32 ColCount
    {
      get { return GetColCount(); }
    }

    public Int32 RowCount
    {
      get { return GetRowCount(); }
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Static Conversion
    //----------------------------------------------------------------------------
    //  

    //  
    //----------------------------------------------------------------------------
    //  Segment - Static Conversion
    //----------------------------------------------------------------------------
    //  
    public static Double[,] ConvertMatrixToDouble(CMatrix matrix)
    {
      Double [,] Result = null;
      if (matrix is CMatrix08Bit)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        Result = new Double[CC, RC];
        return Result;
      }
      return Result;
    }

  }
}
