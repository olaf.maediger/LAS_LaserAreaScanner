﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette256
{
  public class CRainbowHigh : CPalette256
  {
    //
    //------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------
    //
    protected static String NAME_PALETTE = CPalette256.PALETTEKINDS[(int)EPaletteKind.GreyScale];
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CRainbowHigh()
      : base(NAME_PALETTE, EPaletteKind.GreyScale)
    {
      BuildPaletteColors();
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------
    //
    protected override Boolean BuildPaletteColors()
    {
      FColors[0xFC] = Color.FromArgb(0xFF, 137, 6, 0);
      FColors[0xF8] = Color.FromArgb(0xFF, 137, 6, 0);
      FColors[0xF4] = Color.FromArgb(0xFF, 143, 10, 0);
      FColors[0xF0] = Color.FromArgb(0xFF, 154, 18, 2);
      FColors[0xEC] = Color.FromArgb(0xFF, 160, 22, 3);
      FColors[0xE8] = Color.FromArgb(0xFF, 165, 26, 4);
      FColors[0xE4] = Color.FromArgb(0xFF, 171, 30, 5);
      FColors[0xE0] = Color.FromArgb(0xFF, 176, 34, 5);
      FColors[0xDC] = Color.FromArgb(0xFF, 182, 38, 6);
      FColors[0xD8] = Color.FromArgb(0xFF, 187, 42, 7);
      FColors[0xD4] = Color.FromArgb(0xFF, 193, 46, 8);
      FColors[0xD0] = Color.FromArgb(0xFF, 198, 50, 9);
      FColors[0xCC] = Color.FromArgb(0xFF, 204, 54, 10);
      FColors[0xC8] = Color.FromArgb(0xFF, 209, 58, 10);
      FColors[0xC4] = Color.FromArgb(0xFF, 215, 62, 11);
      FColors[0xC0] = Color.FromArgb(0xFF, 220, 66, 12);
      FColors[0xBC] = Color.FromArgb(0xFF, 226, 70, 13);
      FColors[0xB8] = Color.FromArgb(0xFF, 231, 74, 14);
      FColors[0xB4] = Color.FromArgb(0xFF, 237, 78, 15);
      FColors[0xB0] = Color.FromArgb(0xFF, 242, 82, 16);
      FColors[0xAC] = Color.FromArgb(0xFF, 248, 86, 16);
      FColors[0xA8] = Color.FromArgb(0xFF, 254, 90, 17);
      FColors[0xA4] = Color.FromArgb(0xFF, 255, 95, 17);
      FColors[0xA0] = Color.FromArgb(0xFF, 255, 99, 16);
      FColors[0x9C] = Color.FromArgb(0xFF, 255, 104, 16);
      FColors[0x98] = Color.FromArgb(0xFF, 255, 109, 15);
      FColors[0x94] = Color.FromArgb(0xFF, 255, 113, 14);
      FColors[0x90] = Color.FromArgb(0xFF, 255, 118, 13);
      FColors[0x8C] = Color.FromArgb(0xFF, 255, 123, 13);
      FColors[0x88] = Color.FromArgb(0xFF, 255, 127, 12);
      FColors[0x84] = Color.FromArgb(0xFF, 255, 132, 11);
      FColors[0x80] = Color.FromArgb(0xFF, 255, 137, 10);
      FColors[0x7C] = Color.FromArgb(0xFF, 255, 141, 10);
      FColors[0x78] = Color.FromArgb(0xFF, 255, 146, 9);
      FColors[0x74] = Color.FromArgb(0xFF, 255, 151, 8);
      FColors[0x70] = Color.FromArgb(0xFF, 255, 156, 7);
      FColors[0x6C] = Color.FromArgb(0xFF, 255, 160, 7);
      FColors[0x68] = Color.FromArgb(0xFF, 255, 165, 6);
      FColors[0x64] = Color.FromArgb(0xFF, 255, 170, 5);
      FColors[0x60] = Color.FromArgb(0xFF, 255, 174, 4);
      FColors[0x5C] = Color.FromArgb(0xFF, 255, 179, 4);
      FColors[0x58] = Color.FromArgb(0xFF, 255, 184, 3);
      FColors[0x54] = Color.FromArgb(0xFF, 255, 188, 2);
      FColors[0x50] = Color.FromArgb(0xFF, 255, 193, 1);
      FColors[0x4C] = Color.FromArgb(0xFF, 255, 198, 1);
      FColors[0x48] = Color.FromArgb(0xFF, 255, 203, 0);
      FColors[0x44] = Color.FromArgb(0xFF, 252, 206, 0);
      FColors[0x40] = Color.FromArgb(0xFF, 243, 208, 0);
      FColors[0x3C] = Color.FromArgb(0xFF, 235, 209, 0);
      FColors[0x38] = Color.FromArgb(0xFF, 227, 211, 0);
      FColors[0x34] = Color.FromArgb(0xFF, 219, 212, 0);
      FColors[0x30] = Color.FromArgb(0xFF, 203, 215, 0);
      FColors[0x2C] = Color.FromArgb(0xFF, 195, 217, 0);
      FColors[0x28] = Color.FromArgb(0xFF, 187, 218, 0);
      FColors[0x24] = Color.FromArgb(0xFF, 179, 220, 0);
      FColors[0x20] = Color.FromArgb(0xFF, 163, 223, 0);
      FColors[0x1C] = Color.FromArgb(0xFF, 147, 226, 0);
      FColors[0x18] = Color.FromArgb(0xFF, 139, 227, 0);
      FColors[0x14] = Color.FromArgb(0xFF, 131, 229, 0);
      FColors[0x10] = Color.FromArgb(0xFF, 123, 230, 0);
      FColors[0x0C] = Color.FromArgb(0xFF, 115, 232, 0);
      FColors[0x08] = Color.FromArgb(0xFF, 107, 233, 0);
      FColors[0x04] = Color.FromArgb(0xFF, 99, 235, 0);
      FColors[0x00] = Color.FromArgb(0xFF, 91, 237, 0);
      for (Int32 CI = 0; CI <= 252; CI += 4)
      {
        FColors[1 + CI] = FColors[CI];
        FColors[2 + CI] = FColors[CI];
        FColors[3 + CI] = FColors[CI];
      }
      //
      return true;
    }

  }
}
