﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette256
{
  public class CBlueSection : CPalette256
  {
    //
    //------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------
    //
    protected static String NAME_PALETTE = CPalette256.PALETTEKINDS[(int)EPaletteKind.BlueSection];
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CBlueSection()
      : base(NAME_PALETTE, EPaletteKind.BlueSection)
    {
      BuildPaletteColors();
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------
    //
    protected override Boolean BuildPaletteColors()
    {
      return BuildPaletteColorsSection(0, 0, 255, 0, 8);
    }

  }
}
