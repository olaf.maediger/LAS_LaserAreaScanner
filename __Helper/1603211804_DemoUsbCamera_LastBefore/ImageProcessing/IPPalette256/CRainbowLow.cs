﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette256
{
  public class CRainbowLow : CPalette256
  {
    //
    //------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------
    //
    protected static String NAME_PALETTE = CPalette256.PALETTEKINDS[(int)EPaletteKind.GreyScale];
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CRainbowLow()
      : base(NAME_PALETTE, EPaletteKind.GreyScale)
    {
      BuildPaletteColors();
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------
    //
    protected override Boolean BuildPaletteColors()
    {
      FColors[0xFC] = Color.FromArgb(0xFF, 83, 238, 0);
      FColors[0xF8] = Color.FromArgb(0xFF, 75, 240, 0);
      FColors[0xF4] = Color.FromArgb(0xFF, 72, 240, 8);
      FColors[0xF0] = Color.FromArgb(0xFF, 69, 241, 16);
      FColors[0xEC] = Color.FromArgb(0xFF, 62, 242, 32);
      FColors[0xE8] = Color.FromArgb(0xFF, 58, 243, 41);
      FColors[0xE4] = Color.FromArgb(0xFF, 55, 244, 49);
      FColors[0xE0] = Color.FromArgb(0xFF, 52, 245, 57);
      FColors[0xDC] = Color.FromArgb(0xFF, 48, 245, 65);
      FColors[0xD8] = Color.FromArgb(0xFF, 45, 246, 73);
      FColors[0xD4] = Color.FromArgb(0xFF, 41, 247, 81);
      FColors[0xD0] = Color.FromArgb(0xFF, 38, 247, 90);
      FColors[0xCC] = Color.FromArgb(0xFF, 35, 248, 98);
      FColors[0xC8] = Color.FromArgb(0xFF, 31, 249, 106);
      FColors[0xC4] = Color.FromArgb(0xFF, 28, 250, 114);
      FColors[0xC0] = Color.FromArgb(0xFF, 24, 250, 122);
      FColors[0xBC] = Color.FromArgb(0xFF, 21, 251, 130);
      FColors[0xB8] = Color.FromArgb(0xFF, 17, 252, 138);
      FColors[0xB4] = Color.FromArgb(0xFF, 14, 252, 147);
      FColors[0xB0] = Color.FromArgb(0xFF, 11, 253, 155);
      FColors[0xAC] = Color.FromArgb(0xFF, 7, 254, 163);
      FColors[0xA8] = Color.FromArgb(0xFF, 4, 255, 171);
      FColors[0xA4] = Color.FromArgb(0xFF, 0, 255, 179);
      FColors[0xA0] = Color.FromArgb(0xFF, 1, 246, 181);
      FColors[0x9C] = Color.FromArgb(0xFF, 1, 234, 180);
      FColors[0x98] = Color.FromArgb(0xFF, 1, 222, 179);
      FColors[0x94] = Color.FromArgb(0xFF, 2, 210, 177);
      FColors[0x90] = Color.FromArgb(0xFF, 2, 198, 176);
      FColors[0x8C] = Color.FromArgb(0xFF, 2, 186, 175);
      FColors[0x88] = Color.FromArgb(0xFF, 3, 174, 174);
      FColors[0x84] = Color.FromArgb(0xFF, 3, 162, 173);
      FColors[0x80] = Color.FromArgb(0xFF, 3, 150, 172);
      FColors[0x7C] = Color.FromArgb(0xFF, 4, 139, 171);
      FColors[0x78] = Color.FromArgb(0xFF, 4, 127, 170);
      FColors[0x74] = Color.FromArgb(0xFF, 4, 115, 169);
      FColors[0x70] = Color.FromArgb(0xFF, 5, 103, 168);
      FColors[0x6C] = Color.FromArgb(0xFF, 5, 91, 167);
      FColors[0x68] = Color.FromArgb(0xFF, 5, 79, 166);
      FColors[0x64] = Color.FromArgb(0xFF, 6, 67, 165);
      FColors[0x60] = Color.FromArgb(0xFF, 6, 55, 163);
      FColors[0x5C] = Color.FromArgb(0xFF, 6, 43, 162);
      FColors[0x58] = Color.FromArgb(0xFF, 7, 31, 161);
      FColors[0x54] = Color.FromArgb(0xFF, 7, 19, 160);
      FColors[0x50] = Color.FromArgb(0xFF, 7, 7, 159);
      FColors[0x4C] = Color.FromArgb(0xFF, 8, 0, 157);
      FColors[0x48] = Color.FromArgb(0xFF, 8, 0, 154);
      FColors[0x44] = Color.FromArgb(0xFF, 8, 0, 151);
      FColors[0x40] = Color.FromArgb(0xFF, 8, 0, 148);
      FColors[0x3C] = Color.FromArgb(0xFF, 8, 0, 145);
      FColors[0x38] = Color.FromArgb(0xFF, 8, 0, 141);
      FColors[0x34] = Color.FromArgb(0xFF, 8, 0, 138);
      FColors[0x30] = Color.FromArgb(0xFF, 8, 0, 132);
      FColors[0x2C] = Color.FromArgb(0xFF, 8, 0, 129);
      FColors[0x28] = Color.FromArgb(0xFF, 9, 0, 126);
      FColors[0x24] = Color.FromArgb(0xFF, 9, 0, 119);
      FColors[0x20] = Color.FromArgb(0xFF, 9, 0, 116);
      FColors[0x1C] = Color.FromArgb(0xFF, 10, 0, 113);
      FColors[0x18] = Color.FromArgb(0xFF, 10, 0, 110);
      FColors[0x14] = Color.FromArgb(0xFF, 10, 0, 106);
      FColors[0x10] = Color.FromArgb(0xFF, 10, 0, 103);
      FColors[0x0C] = Color.FromArgb(0xFF, 10, 0, 100);
      FColors[0x08] = Color.FromArgb(0xFF, 10, 0, 97);
      FColors[0x04] = Color.FromArgb(0xFF, 10, 0, 94);
      FColors[0x00] = Color.FromArgb(0xFF, 10, 0, 91);
      for (Int32 CI = 0; CI <= 252; CI += 4)
      {
        FColors[1 + CI] = FColors[CI];
        FColors[2 + CI] = FColors[CI];
        FColors[3 + CI] = FColors[CI];
      }      //
      return true;
    }

  }
}
