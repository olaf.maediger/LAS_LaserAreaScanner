﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPBitmap
{
  public class CMatrixArgb08Bit : CMatrix
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private Byte[,] FMatrixA;
    private Byte[,] FMatrixR;
    private Byte[,] FMatrixG;
    private Byte[,] FMatrixB;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrixArgb08Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrixA = new Byte[FColCount, FRowCount];
      FMatrixR = new Byte[FColCount, FRowCount];
      FMatrixG = new Byte[FColCount, FRowCount];
      FMatrixB = new Byte[FColCount, FRowCount];
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    //protected override Int32 GetColCount()
    //{
    //  return FMatrix.GetLength(INDEX_COL);
    //}

    //protected override Int32 GetRowCount()
    //{
    //  return FMatrix.GetLength(INDEX_ROW);
    //}

  }
}
