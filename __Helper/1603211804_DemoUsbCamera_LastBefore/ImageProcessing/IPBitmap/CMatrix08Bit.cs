﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPBitmap
{
  public class CMatrix08Bit : CMatrix
  {

    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private Byte[,] FMatrix;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrix08Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrix = new Byte[FColCount, FRowCount];
    }

    public CMatrix08Bit(CBitmap bitmap)
      : base(bitmap.Width, bitmap.Height)
    {
      if (bitmap is CBitmapRgb)
      {
        CBitmap.CopyBitmap24bppToMatrix08bit(bitmap, out FMatrix);
      }
      if (bitmap is CBitmapArgb)
      {
        CBitmap.CopyBitmap32bppToMatrix08bit(bitmap, out FMatrix);
      }
      if (bitmap is CBitmap256)
      {
        // ?? CBitmap..CopyBitmap256ToMatrix08bit(bitmap, out FMatrix);
      }
      FMatrix = new Byte[FColCount, FRowCount];
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  


  }
}
