﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPBitmap
{
  public class CBitmap256 : CBitmap
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    protected CPalette256 FPalette256;
    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmap256(Int32 width, 
                      Int32 height,
                      EPaletteKind palettekind, 
                      PixelFormat pixelformat)
      : base(width, height, pixelformat)
    {
      FPalette256 = CPalette256.CreatePalette256(palettekind);
    }

    public CBitmap256(Bitmap bitmap,
                      EPaletteKind palettekind,
                      PixelFormat pixelformat)
      : base(bitmap.Width, bitmap.Height, pixelformat)
    {
      FPalette256 = CPalette256.CreatePalette256(palettekind);
      SetBitmap(bitmap);
    }

    public CBitmap256(Bitmap bitmap,
                      CPalette256 palette256,
                      PixelFormat pixelformat)
      : base(bitmap.Width, bitmap.Height, pixelformat)
    {
      FPalette256 = palette256;
      SetBitmap(bitmap);
    }
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //
    public override Boolean GetMatrix(out Byte[,] matrix,
                                      Int32 offsetx, Int32 offsety,
                                      Int32 sizex, Int32 sizey)
    {
      matrix = null;
      try
      {
        unsafe
        {
          Int32 BW = FBitmap.Width;
          Int32 BH = FBitmap.Height;
          Int32 XIL = 0;
          Int32 XIH = BW - 1;
          Int32 YIL = 0;
          Int32 YIH = BH - 1;
          BitmapData Data = FBitmap.LockBits(new Rectangle(0, 0, BW, BH),
                                             ImageLockMode.ReadOnly, FBitmap.PixelFormat);
          matrix = new Byte[BW, BH];
          switch (FBitmap.PixelFormat)
          {
            case PixelFormat.Format24bppRgb:
              for (Int32 YI = YIL; YI <= YIH; YI++)
              {
                Byte* PByte = (Byte*)(Data.Scan0 + offsetx + (YI + offsety) * Data.Stride);
                for (Int32 XI = XIL; XI <= XIH; XI++)
                {
                  matrix[YI, XI] = *PByte;
                  PByte++;
                }
              }                      
              FBitmap.UnlockBits(Data);
              return true;
            case PixelFormat.Format32bppArgb:
              for (Int32 YI = YIL; YI <= YIH; YI++)
              {
                Byte* PByte = (Byte*)(Data.Scan0 + offsetx + (YI + offsety) * Data.Stride);
                for (Int32 XI = XIL; XI <= XIH; XI++)
                {
                  matrix[YI, XI] = *PByte;
                  PByte++;
                }
              }
              FBitmap.UnlockBits(Data);
              return true;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean SetMatrix(Byte[,] matrix,
                                      Int32 offsetx, Int32 offsety,
                                      Int32 sizex, Int32 sizey)
    {
      try
      {
        unsafe
        {
          Int32 BW = FBitmap.Width;
          Int32 BH = FBitmap.Height;
          BitmapData Data = FBitmap.LockBits(new Rectangle(0, 0, BW, BH),
                                             ImageLockMode.WriteOnly, FBitmap.PixelFormat);
          for (Int32 YI = offsety; YI < offsety + sizey; YI++)
          {
            Byte* PByte = (Byte*)Data.Scan0 + YI * Data.Stride;
            for (Int32 XI = offsetx; XI < offsetx + sizex; XI++)
            {
              *PByte = matrix[YI, XI];
              PByte++;
            }
          }
          FBitmap.UnlockBits(Data);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    
    public override Boolean SetBitmap(Bitmap bitmap)
    {
      try
      {
        if (!(bitmap is Bitmap)) return false;
        switch (bitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            return CopyBitmap24bppPalette256ToBitmap32bpp(bitmap, FPalette256, 0xFF, out FBitmap);
          case PixelFormat.Format32bppArgb:
            return CopyBitmap32bppPalette256ToBitmap32bpp(bitmap, FPalette256, 0xFF, out FBitmap);
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Helper
    //--------------------------------------------------------


    //
    //--------------------------------------------------------
    //	Segment - Public
    //--------------------------------------------------------
    //

  }
}
