﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPBitmap
{
  public class CMatrix10Bit : CMatrix
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private UInt16[,] FMatrix;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrix10Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrix = new UInt16[FColCount, FRowCount];
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    //protected override Int32 GetColCount()
    //{
    //  return FMatrix.GetLength(INDEX_COL);
    //}

    //protected override Int32 GetRowCount()
    //{
    //  return FMatrix.GetLength(INDEX_ROW);
    //}

  }
}
