﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPBitmap
{
  public class CMatrixRgb08Bit : CMatrix
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private Byte[,] FMatrixR;
    private Byte[,] FMatrixG;
    private Byte[,] FMatrixB;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrixRgb08Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrixR = new Byte[FColCount, FRowCount];
      FMatrixG = new Byte[FColCount, FRowCount];
      FMatrixB = new Byte[FColCount, FRowCount];
    }

  }
}
