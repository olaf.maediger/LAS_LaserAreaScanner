﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPPalette256;
//
namespace IPBitmap
{
  public abstract class CBitmap
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //
    public static Int32 INIT_BITMAPWIDTH = 1;
    public static Int32 INIT_BITMAPHEIGHT = 1;
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    protected Byte[,] FMatrix08Bit;
    protected UInt16[,] FMatrix10Bit;
    protected Bitmap FBitmap; // Extern, SpecialType // Bitmap -> Display (24bitRgb or 32bitArgb)
    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmap(Int32 width, Int32 height, PixelFormat pixelformat)
    {
      Int32 W = Math.Max(INIT_BITMAPWIDTH, width);
			Int32 H = Math.Max(INIT_BITMAPHEIGHT, height);
      FBitmap = new System.Drawing.Bitmap(W, H, pixelformat);
    }
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //   
    public Bitmap Bitmap
    {
      get { return FBitmap; }
    }

    public PixelFormat PixelFormat
    {
      get { return FBitmap.PixelFormat; }
    }

    public Int32 Width
    {
      get { return FBitmap.Width; }
    }
    public Int32 Height
    {
      get { return FBitmap.Height; }
    }

    public abstract Boolean GetMatrix(out Byte[,] matrix,
                                      Int32 offsetx, Int32 offsety,
                                      Int32 sizex, Int32 sizey);

    public abstract Boolean SetMatrix(Byte[,] matrix,
                                      Int32 offsetx, Int32 offsety,
                                      Int32 sizex, Int32 sizey);

    public Bitmap GetBitmapRgb()
    { // 24bpp/32bpp -> 24bpp
      PixelFormat PF = FBitmap.PixelFormat;
      switch (PF)
      {
        case PixelFormat.Format24bppRgb:
          return FBitmap;
        case PixelFormat.Format32bppArgb:
          Bitmap B24;
          CopyBitmap32bppTo24bpp(FBitmap, out B24);
          return B24;
      }
      return null;
    }

    public Bitmap GetBitmapArgb()
    { // 24bpp/32bpp -> 32bpp
      PixelFormat PF = FBitmap.PixelFormat;
      switch (PF)
      {
        case PixelFormat.Format24bppRgb:
          Bitmap B32;
          CopyBitmap24bppTo32bpp(FBitmap, 0xFF, out B32);
          return B32;
        case PixelFormat.Format32bppArgb:
          return FBitmap;
      }
      return null;
    }
    public abstract Boolean SetBitmap(Bitmap bitmap);

    //
    //--------------------------------------------------------
    //	Segment - Public
    //--------------------------------------------------------
    //
    public Boolean Draw(Graphics graphics,
                        Int32 offsetx, Int32 offsety,
                        Int32 sizex, Int32 sizey)
    { // BitmapTarget -> graphics
      if (FBitmap is Bitmap)
      {
        graphics.DrawImage(FBitmap, offsetx, offsety, sizex, sizey);
        return true;
      }
      return false;
    }
    //
    //#################################################################################
    //  Segment - Helper - Copy Source -> Destination
    //#################################################################################
    //
    //#################################################################################
    //  Copy Bitmap (no Palette) → Bitmap
    //#################################################################################
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap24bpp (no Palette256) -> Bitmap32bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap24bppTo32bpp(Bitmap bitmapsource24bpp,
                                                 Byte opacity, // 0..100%
                                                 out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource24bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp)
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp)
            bitmaptarget32bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                *PByteT = BOpacity; // Target(32bpp)
                PByteT++;
                // NC Source(24bpp)
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsource24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap32bpp (no Palette256) -> Bitmap24bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap32bppTo24bpp(Bitmap bitmapsource32bpp,
                                                 out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource32bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (PixelFormat.Format32bppArgb == PixelFormatSource)
        { // conversion from 32bpp -> 24bpp
          unsafe
          { // Source(32bpp)
            Int32 BWS = bitmapsource32bpp.Width;
            Int32 BHS = bitmapsource32bpp.Height;
            BitmapData DataS = bitmapsource32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                PByteS++; // Source(32bpp)
                // NC Target(24bpp)
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            bitmapsource32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap24bpp (no Palette256) -> Bitmap24bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap24bppTo24bpp(Bitmap bitmapsource24bpp,                                             
                                                 out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource24bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        { // conversion from 24bpp -> 24bpp
          unsafe
          { // Source(24bpp)
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp)
            bitmaptarget24bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // NC A
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            bitmapsource24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap32bpp (no Palette256) -> Bitmap32bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap32bppTo32bpp(Bitmap bitmapsource32bpp,
                                                 Byte opacity, // 0..100%
                                                 out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource32bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        { // conversion from 32bpp -> 32 bpp
          unsafe
          { // Source(32bpp)
            Int32 BWS = bitmapsource32bpp.Width;
            Int32 BHS = bitmapsource32bpp.Height;
            BitmapData DataS = bitmapsource32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp)
            bitmaptarget32bpp = new Bitmap(BWS, BHS, PixelFormatTarget);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                *PByteT = BOpacity; // Target(32bpp)
                PByteT++;
                // NC Source(32bpp)
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsource32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap + Palette256 → Bitmap 
    //#################################################################################
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap24bpp + Palette256 -> Bitmap32bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap24bppPalette256ToBitmap32bpp(Bitmap bitmapsource24bpp,
                                                                 CPalette256 palette256,
                                                                 Byte opacity, // 0..100%
                                                                 out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource24bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        {
          unsafe
          { // Source(24bpp)
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            // Copy Bitmap24bpp (3 Byte) + Palette256 -> Bitmap32bpp (4 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                // Target
                *PByteT = palette256[PI].B;
                PByteT++;
                *PByteT = palette256[PI].G;
                PByteT++;
                *PByteT = palette256[PI].R;
                PByteT++;
                *PByteT = BOpacity;
                PByteT++; // 
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsource24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap32bpp + Palette256 -> Bitmap24bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap32bppPalette256ToBitmap24bpp(Bitmap bitmapsource32bpp,
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource32bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (PixelFormat.Format32bppArgb == PixelFormatSource)
        {
          unsafe
          { // Source(32bpp)
            Int32 BWS = bitmapsource32bpp.Width;
            Int32 BHS = bitmapsource32bpp.Height;
            BitmapData DataS = bitmapsource32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            // Bitmap32bpp (4 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // --- Source(32bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                PByteS++; // ignore A 
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                // --- Target(24bpp)
                Color PC = palette256[PI]; 
                // Target - B
                *PByteT = PC.B;
                PByteT++;
                // Target - G
                *PByteT = PC.G;
                PByteT++;
                // Target - R
                *PByteT = PC.R;
                PByteT++;
                // Target - A
                // NC
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            bitmapsource32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap24bpp + Palette256 -> Bitmap24bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap24bppPalette256ToBitmap24bpp(Bitmap bitmapsource24bpp,
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource24bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (PixelFormat.Format24bppRgb == PixelFormatSource)
        {
          unsafe
          { // Source(24bpp)
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
            BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            // Copy Bitmap24bpp (3 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                // Target(24bpp)
                *PByteT = palette256[PI].B;
                PByteT++;
                *PByteT = palette256[PI].G;
                PByteT++;
                *PByteT = palette256[PI].R;
                PByteT++;
              }
            }
            bitmaptarget24bpp.UnlockBits(DataT);
            bitmapsource24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap32bpp + Palette256 -> Bitmap32bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap32bppPalette256ToBitmap32bpp(Bitmap bitmapsource32bpp,
                                                                 CPalette256 palette256,
                                                                 Byte opacity, // 0..100%
                                                                 out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource32bpp.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
        if (PixelFormat.Format32bppArgb == PixelFormatSource)
        {
          unsafe
          { // Source(32bpp)
            Int32 BWS = bitmapsource32bpp.Width;
            Int32 BHS = bitmapsource32bpp.Height;
            BitmapData DataS = bitmapsource32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                          ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
            BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                          ImageLockMode.WriteOnly, PixelFormatTarget);
            // Copy Bitmap32bpp (4 Byte) + Palette256 -> Bitmap32bpp (4 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                // Target
                *PByteT = palette256[PI].B;
                PByteT++;
                *PByteT = palette256[PI].G;
                PByteT++;
                *PByteT = palette256[PI].R;
                PByteT++;
                *PByteT = BOpacity;
                PByteT++; 
              }
            }
            bitmaptarget32bpp.UnlockBits(DataT);
            bitmapsource32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap (no Palette) → Matrix 
    //#################################################################################
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap24bpp -> Matrix08bit
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap24bppToMatrix08bit(Bitmap bitmap24bpp, 
                                                       out Byte[,] matrix08bit)
    {
      matrix08bit = null;
      try
      {
        if (PixelFormat.Format24bppRgb == bitmap24bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap24bpp.Width;
            Int32 BHS = bitmap24bpp.Height;
            BitmapData DataS = bitmap24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(08bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrix08bit = new Byte[BWT, BHT];
            // Copy Bitmap24bpp (3 Byte) -> Matrix08bit (1 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                // Target
                Byte BValue = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                matrix08bit[XI, YI] = BValue;
              }
            }
            bitmap24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap24bpp -> Matrix10bit
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap24bppToMatrix10bit(Bitmap bitmap24bpp,
                                                       out UInt16[,] matrix10bit)
    {
      matrix10bit = null;
      try
      {
        if (PixelFormat.Format24bppRgb == bitmap24bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap24bpp.Width;
            Int32 BHS = bitmap24bpp.Height;
            BitmapData DataS = bitmap24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(08bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrix10bit = new UInt16[BWT, BHT];
            // Copy Bitmap24bpp (3 Byte) -> Matrix10bit (2 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                // Target(10bit)
                UInt16 UValue = (UInt16)(1.196f * (float)R + 2.348f * (float)G + 0.456f * (float)B);
                matrix10bit[XI, YI] = UValue;
              }
            }
            bitmap24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap32bpp -> Matrix08bit
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap32bppToMatrix08bit(Bitmap bitmap32bpp,
                                                       out Byte[,] matrix08bit)
    {
      matrix08bit = null;
      try
      {
        if (PixelFormat.Format32bppArgb == bitmap32bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap32bpp.Width;
            Int32 BHS = bitmap32bpp.Height;
            BitmapData DataS = bitmap32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(08bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrix08bit = new Byte[BWT, BHT];
            // Copy Bitmap24bpp (3 Byte) -> Matrix08bit (1 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                // Target
                Byte BValue = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                matrix08bit[XI, YI] = BValue;
              }
            }
            bitmap32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap32bpp -> Matrix10bit
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap32bppToMatrix10bit(Bitmap bitmap32bpp,
                                                       out UInt16[,] matrix10bit)
    {
      matrix10bit = null;
      try
      {
        if (PixelFormat.Format32bppArgb == bitmap32bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap32bpp.Width;
            Int32 BHS = bitmap32bpp.Height;
            BitmapData DataS = bitmap32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(08bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrix10bit = new UInt16[BWT, BHT];
            // Copy Bitmap32bpp (4 Byte) -> Matrix10bit (2 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                // Target
                UInt16 UValue = (UInt16)(1.196f * (float)R + 2.348f * (float)G + 0.456f * (float)B);
                matrix10bit[XI, YI] = UValue;
              }
            }
            bitmap32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap + Palette256 → Matrix 
    //#################################################################################
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap24bpp + Palette256 -> Matrix08bit
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap24bppPalette256ToMatrix08bit(Bitmap bitmap24bpp, 
                                                                 CPalette256 palette256, 
                                                                 out Byte[,] matrix08bit)
    {
      matrix08bit = null;
      try
      {
        if (PixelFormat.Format24bppRgb == bitmap24bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap24bpp.Width;
            Int32 BHS = bitmap24bpp.Height;
            BitmapData DataS = bitmap24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(08bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrix08bit = new Byte[BWT, BHT];
            // Copy Bitmap24bpp (3 Byte) -> Palette256 -> Matrix08bit (1 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                // Target
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                B = palette256[PI].B;
                G = palette256[PI].G;
                R = palette256[PI].R;
                Byte BValue = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                matrix08bit[XI, YI] = BValue;
              }
            }
            bitmap24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap24bpp + Palette256 -> Matrix10bit
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap24bppPalette256ToMatrix10bit(Bitmap bitmap24bpp,
                                                                 CPalette256 palette256,
                                                                 out UInt16[,] matrix10bit)
    {
      matrix10bit = null;
      try
      {
        if (PixelFormat.Format24bppRgb == bitmap24bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap24bpp.Width;
            Int32 BHS = bitmap24bpp.Height;
            BitmapData DataS = bitmap24bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(10bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrix10bit = new UInt16[BWT, BHT];
            // Copy Bitmap24bpp (3 Byte) -> Palette256 -> Matrix10bit (2 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                // Target(16bit)
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                B = palette256[PI].B;
                G = palette256[PI].G;
                R = palette256[PI].R;
                UInt16 UValue = (UInt16)(1.196f * (float)R + 2.348f * (float)G + 0.456f * (float)B);
                matrix10bit[XI, YI] = UValue; 
              }
            }
            bitmap24bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap32bpp + Palette256 -> Matrix08bit
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap32bppPalette256ToMatrix08bit(Bitmap bitmap32bpp, 
                                                                 CPalette256 palette256,
                                                                 out Byte[,] matrix08bit)
    {
      matrix08bit = null;
      try
      {
        if (PixelFormat.Format32bppArgb == bitmap32bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap32bpp.Width;
            Int32 BHS = bitmap32bpp.Height;
            BitmapData DataS = bitmap32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(08bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrix08bit = new Byte[BWT, BHT];
            // Copy Bitmap24bpp (3 Byte) -> Palette256 -> Matrix08bit (1 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                // Target(16bit)
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                B = palette256[PI].B;
                G = palette256[PI].G;
                R = palette256[PI].R;
                Byte BValue = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                matrix08bit[XI, YI] = BValue;
              }
            }
            bitmap32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Bitmap32bpp + Palette256 -> Matrix10bit
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyBitmap32bppPalette256ToMatrix10bit(Bitmap bitmap32bpp,
                                                                 CPalette256 palette256,
                                                                 out UInt16[,] matrix10bit)
    {
      matrix10bit = null;
      try
      {
        if (PixelFormat.Format32bppArgb == bitmap32bpp.PixelFormat)
        {
          PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
          unsafe
          { // Source Bitmap(24bpp)
            Int32 BWS = bitmap32bpp.Width;
            Int32 BHS = bitmap32bpp.Height;
            BitmapData DataS = bitmap32bpp.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                    ImageLockMode.ReadOnly, PixelFormatSource);
            // Target Matrix(10bit)
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            matrix10bit = new UInt16[BWT, BHT];
            // Copy Bitmap24bpp (3 Byte) -> Palette256 -> Matrix10bit (2 Byte)
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++;
                // Target(16bit)
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                B = palette256[PI].B;
                G = palette256[PI].G;
                R = palette256[PI].R;
                Byte BValue = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                matrix10bit[XI, YI] = BValue;
              }
            }
            bitmap32bpp.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Matrix (no Palette) → Bitmap
    //#################################################################################
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Matrix08bit (no Palette) -> Bitmap24bpp (Grey)
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyMatrix08bitToBitmap24bpp(Byte[,] matrixsource08bit,
                                                       out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        unsafe
        { // Source Matrix08bit
          Int32 BHS = matrixsource08bit.GetLength(1);
          Int32 BWS = matrixsource08bit.GetLength(0);
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix08bit (1 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08bit
              Byte BValue = matrixsource08bit[XI, YI];
              // Target (3 Byte)
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
            }
          }
          bitmaptarget24bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Matrix08bit (no Palette) -> Bitmap32bpp (Grey)
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyMatrix08bitToBitmap32bpp(Byte[,] matrixsource08bit,
                                                       Byte opacity, // 0..100%
                                                       out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
        unsafe
        { // Source Matrix08bit
          Int32 BHS = matrixsource08bit.GetLength(0);
          Int32 BWS = matrixsource08bit.GetLength(1);
          // Target(32bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix08bit (1 Byte) + Palette256 -> Bitmap32bpp (4 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08bit
              Byte BValue = matrixsource08bit[YI, XI];
              // Target (4 Byte)
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BOpacity;
              PByteT++;
            }
          }
          bitmaptarget32bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Matrix10bit (no Palette) -> Bitmap24bpp (Grey)
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyMatrix10bitToBitmap24bpp(UInt16[,] matrixsource10bit,
                                                       out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        unsafe
        { // Source Matrix08bit
          Int32 BWS = matrixsource10bit.GetLength(0);
          Int32 BHS = matrixsource10bit.GetLength(1);
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix10bit (2 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08bit
              Int32 IValue = matrixsource10bit[XI, YI] >> 2;
              Byte BValue = (Byte)Math.Max(0, Math.Min(255, IValue));
              // Target (3 Byte)
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
            }
          }
          bitmaptarget24bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Matrix10bit (no Palette) -> Bitmap32bpp (Grey)
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyMatrix10bitToBitmap32bpp(UInt16[,] matrixsource10bit,
                                                       Int32 opacity, // 0..100%
                                                       out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppRgb;
        Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
        unsafe
        { // Source Matrix08bit
          Int32 BHS = matrixsource10bit.GetLength(1);
          Int32 BWS = matrixsource10bit.GetLength(0);
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix10bit (2 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08bit
              Int32 IValue = matrixsource10bit[XI, YI] >> 2;
              Byte BValue = (Byte)Math.Max(0, Math.Min(255, IValue));
              // Target (3 Byte)
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BValue;
              PByteT++;
              *PByteT = BOpacity;
              PByteT++;
            }
          }
          bitmaptarget32bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Matrix + Palette256 → Bitmap
    //#################################################################################
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Matrix08bit + Palette256 -> Bitmap24bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyMatrix08bitPalette256ToBitmap24bpp(Byte[,] matrixsource08bit,
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        unsafe
        { // Source Matrix08bit
          Int32 BHS = matrixsource08bit.GetLength(1);
          Int32 BWS = matrixsource08bit.GetLength(0);
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix08bit (1 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08bit
              Byte PI = matrixsource08bit[XI, YI];
              // Target (3 Byte)
              *PByteT = palette256[PI].B;
              PByteT++;
              *PByteT = palette256[PI].G;
              PByteT++;
              *PByteT = palette256[PI].R;
              PByteT++;
            }
          }
          bitmaptarget24bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Matrix08bit + Palette256 -> Bitmap32bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyMatrix08bitPalette256ToBitmap32bpp(Byte[,] matrixsource08bit,
                                                                 CPalette256 palette256,
                                                                 Byte opacity,
                                                                 out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
        unsafe
        { // Source Matrix08bit
          Int32 BHS = matrixsource08bit.GetLength(0);
          Int32 BWS = matrixsource08bit.GetLength(1);
          // Target(32bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix08bit (1 Byte) + Palette256 -> Bitmap32bpp (4 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08bit
              Byte PI = matrixsource08bit[YI, XI];
              // Target (4 Byte)
              *PByteT = palette256[PI].B;
              PByteT++;
              *PByteT = palette256[PI].G;
              PByteT++;
              *PByteT = palette256[PI].R;
              PByteT++;
              *PByteT = BOpacity;
              PByteT++;
            }
          }
          bitmaptarget32bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Matrix10bit + Palette256 -> Bitmap24bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyMatrix10bitPalette256ToBitmap24bpp(UInt16[,] matrixsource10bit,
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        unsafe
        { // Source Matrix08bit
          Int32 BHS = matrixsource10bit.GetLength(1);
          Int32 BWS = matrixsource10bit.GetLength(0);
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget24bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget24bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix10bit (2 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08bit
              Int32 IValue = matrixsource10bit[XI, YI] >> 2;
              Byte PI = (Byte)Math.Max(0, Math.Min(255, IValue));
              // Target (3 Byte)
              *PByteT = palette256[PI].B;
              PByteT++;
              *PByteT = palette256[PI].G;
              PByteT++;
              *PByteT = palette256[PI].R;
              PByteT++;
            }
          }
          bitmaptarget24bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Matrix10bit + Palette256 -> Bitmap24bpp
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyMatrix10bitPalette256ToBitmap32bpp(UInt16[,] matrixsource10bit,
                                                                 Int32 opacity, // 0..100%
                                                                 CPalette256 palette256,
                                                                 out Bitmap bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      try
      {
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
        unsafe
        { // Source Matrix08bit
          Int32 BHS = matrixsource10bit.GetLength(1);
          Int32 BWS = matrixsource10bit.GetLength(0);
          // Target(24bpp)
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          bitmaptarget32bpp = new Bitmap(BWT, BHT, PixelFormatTarget);
          BitmapData DataT = bitmaptarget32bpp.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                        ImageLockMode.WriteOnly, PixelFormatTarget);
          // Copy Matrix10bit (2 Byte) + Palette256 -> Bitmap24bpp (3 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08bit
              Int32 IValue = matrixsource10bit[XI, YI] >> 2;
              Byte PI = (Byte)Math.Max(0, Math.Min(255, IValue));
              // Target (3 Byte)
              *PByteT = palette256[PI].B;
              PByteT++;
              *PByteT = palette256[PI].G;
              PByteT++;
              *PByteT = palette256[PI].R;
              PByteT++;
              *PByteT = BOpacity;
              PByteT++;
            }
          }
          bitmaptarget32bpp.UnlockBits(DataT);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Matrix → Matrix (Rest Copies with Cloning!)
    //#################################################################################
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Matrix08bit -> Matrix10bit
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyMatrix08bitToMatrix10bit(Byte[,] matrixsource08bit,
                                                       out UInt16[,] matrixtarget10bit)
    {
      matrixtarget10bit = null;
      try
      {
        unsafe
        { // Source Matrix08bit
          Int32 BWS = matrixsource08bit.GetLength(0);
          Int32 BHS = matrixsource08bit.GetLength(1);
          // Target Matrix10bit
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          matrixtarget10bit = new UInt16[BWT, BHT];
          // Copy Matrix08bit (1 Byte) -> Matrix10bit (2 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix08bit
              UInt16 UValue = (UInt16)(matrixsource08bit[XI, YI] << 2);
              // Target Matrix10bit
              matrixtarget10bit[XI, YI] = UValue;
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Helper - Matrix10bit -> Matrix08bit
    //---------------------------------------------------------------------------------
    //
    public static Boolean CopyMatrix10bitToMatrix08bit(UInt16[,] matrixsource10bit,
                                                       out Byte[,] matrixtarget08bit)
    {
      matrixtarget08bit = null;
      try
      {
        unsafe
        { // Source Matrix08bit
          Int32 BWS = matrixsource10bit.GetLength(0);
          Int32 BHS = matrixsource10bit.GetLength(1);
          // Target Matrix10bit
          Int32 BWT = BWS;
          Int32 BHT = BHS;
          matrixtarget08bit = new Byte[BWT, BHT];
          // Copy Matrix10bit (2 Byte) -> Matrix08bit (1 Byte)
          Int32 XIL = 0;
          Int32 XIH = BWT - 1;
          Int32 YIL = 0;
          Int32 YIH = BHT - 1;
          for (Int32 YI = YIL; YI <= YIH; YI++)
          {
            for (Int32 XI = XIL; XI <= XIH; XI++)
            { // Source Matrix10bit
              Byte BValue = (Byte)(matrixsource10bit[XI, YI] >> 2);
              // Target Matrix08bit
              matrixtarget08bit[XI, YI] = BValue;
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap256 (no Palette) → BitmapRgb / BitmapArgb
    //#################################################################################
    //
    public static Boolean CopyBitmap256ToBitmapRgb(CBitmap256 bitmapsource256,
                                                   out CBitmapRgb bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      { // Force Source to 24bpp
        Bitmap BitmapSource = bitmapsource256.GetBitmapRgb();
        PixelFormat PixelFormatSource = BitmapSource.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (BitmapSource is Bitmap)
        { // conversion from 24bpp -> 24 bpp
          unsafe
          { // Source(24bpp) 256 forced RGB
            Int32 BWS = BitmapSource.Width;
            Int32 BHS = BitmapSource.Height;
            BitmapData DataS = BitmapSource.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                     ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp) RGB
            bitmaptarget24bpp = new CBitmapRgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget24bpp.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                 ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp) -> Target(24bpp)
                // Target - B - 8Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - A - 32Bit
                // NC only 24Bit
              }
            }
            bitmaptarget24bpp.Bitmap.UnlockBits(DataT);
            BitmapSource.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    } 

    public static Boolean CopyBitmap256ToBitmapArgb(CBitmap256 bitmapsource256,
                                                    Byte opacity, // 0..100%
                                                    out CBitmapRgb bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
      try
      { // Force Source to 24bpp
        Bitmap BitmapSource = bitmapsource256.GetBitmapRgb();
        PixelFormat PixelFormatSource = BitmapSource.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (BitmapSource is Bitmap)
        { // conversion from 24bpp -> 24 bpp
          unsafe
          { // Source(24bpp) 256 forced RGB
            Int32 BWS = BitmapSource.Width;
            Int32 BHS = BitmapSource.Height;
            BitmapData DataS = BitmapSource.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                     ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp) RGB
            bitmaptarget32bpp = new CBitmapRgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                 ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp) -> Target(24bpp)
                // Target - B - 8Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = *PByteS;
                PByteS++;
                PByteT++;
                // Target - A - 32Bit
                *PByteT = BOpacity;
                PByteT++;                
              }
            }
            bitmaptarget32bpp.Bitmap.UnlockBits(DataT);
            BitmapSource.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy Bitmap256 + Palette256 → BitmapRgb / BitmapArgb
    //#################################################################################
    //
    public static Boolean CopyBitmap256Palette256ToBitmapRgb(CBitmap256 bitmapsource256,
                                                             CPalette256 palette256,
                                                             out CBitmapRgb bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      { // Force Source to 24bpp
        Bitmap BitmapSource = bitmapsource256.GetBitmapRgb();
        PixelFormat PixelFormatSource = BitmapSource.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (BitmapSource is Bitmap)
        { // conversion from 24bpp -> 24 bpp
          unsafe
          { // Source(24bpp) 256 forced RGB
            Int32 BWS = BitmapSource.Width;
            Int32 BHS = BitmapSource.Height;
            BitmapData DataS = BitmapSource.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                     ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(24bpp) RGB
            bitmaptarget24bpp = new CBitmapRgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget24bpp.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                 ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                // Target(32bpp)
                // Target - B - 8Bit
                *PByteT = palette256[PI].B;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = palette256[PI].G;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = palette256[PI].R;
                PByteT++;
                // Target - A - 32Bit
                // NC only 24Bit
              }
            }
            bitmaptarget24bpp.Bitmap.UnlockBits(DataT);
            BitmapSource.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    } 

    public static Boolean CopyBitmap256Palette256ToBitmapArgb(CBitmap256 bitmapsource256,
                                                              CPalette256 palette256,
                                                              Byte opacity, // 0..100%
                                                              out CBitmapArgb bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
      try
      { // Force Source to 24bpp
        Bitmap BitmapSource = bitmapsource256.GetBitmapRgb();
        PixelFormat PixelFormatSource = BitmapSource.PixelFormat;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        if (BitmapSource is Bitmap)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp) 256 forced RGB
            Int32 BWS = BitmapSource.Width;
            Int32 BHS = BitmapSource.Height;
            BitmapData DataS = BitmapSource.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                     ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) ARGB
            bitmaptarget32bpp = new CBitmapArgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                 ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                // Target(32bpp)
                // Target - B - 8Bit
                *PByteT = palette256[PI].B;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = palette256[PI].G;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = palette256[PI].R;
                PByteT++;
                // Target - A - 32Bit
                *PByteT = BOpacity;
                PByteT++;
              }
            }
            bitmaptarget32bpp.Bitmap.UnlockBits(DataT);
            BitmapSource.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }   
    //
    //#################################################################################
    //  Copy BitmapRgb (no Palette) → Bitmap256 / BitmapArgb
    //#################################################################################
    // no sense because of missing palette-parameter!!!
    //public static Boolean CopyBitmapRgbToBitmap256(CBitmapRgb bitmaprgb,
    //                                               out CBitmap256 bitmap256)
    //{
    //  bitmap256 = null;
    //  return false;
    //}

    public static Boolean CopyBitmapRgbToBitmapArgb(CBitmapRgb bitmapsource24bpp,
                                                    Byte opacity, // 0..100%
                                                    out CBitmapArgb bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (bitmapsource24bpp is CBitmapRgb)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp) RGB
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                                 ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) ARGB
            bitmaptarget32bpp = new CBitmapArgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                 ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                // Source(24bpp)
                *PByteT = BOpacity;
                PByteT++; // Target(32bpp)                
              }
            }
            bitmaptarget32bpp.Bitmap.UnlockBits(DataT);
            bitmapsource24bpp.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy BitmapRgb + Palette256 → Bitmap256 / BitmapArgb
    //#################################################################################
    //
    public static Boolean CopyBitmapRgbPalette256ToBitmap256(CBitmapRgb bitmapsource24bpp,
                                                             CPalette256 palette256,
                                                             out CBitmap256 bitmaptarget256)
    {
      bitmaptarget256 = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource24bpp.PixelFormat;
        bitmaptarget256 = new CBitmap256(bitmapsource24bpp.Bitmap, palette256, PixelFormatSource);
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CopyBitmapRgbPalette256ToBitmapArgb(CBitmapRgb bitmapsource24bpp,
                                                              CPalette256 palette256,
                                                              Byte opacity, // 0..100%
                                                              out CBitmapArgb bitmaptarget32bpp)
    {
      bitmaptarget32bpp = null;
      Byte BOpacity = (Byte)Math.Max(0f, Math.Max(255f, 256f * (float)opacity / 100f));
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format24bppRgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format32bppArgb;
        if (bitmapsource24bpp is CBitmapRgb)
        { // conversion from 24bpp -> 32 bpp
          unsafe
          { // Source(24bpp) RGB
            Int32 BWS = bitmapsource24bpp.Width;
            Int32 BHS = bitmapsource24bpp.Height;
            BitmapData DataS = bitmapsource24bpp.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                                 ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) ARGB
            bitmaptarget32bpp = new CBitmapArgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget32bpp.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                                 ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap24bpp (3 Byte) -> Bitmap32bpp (4 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(24bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                // Target(32bpp)
                // Target - B - 8Bit
                *PByteT = palette256[PI].B;
                PByteT++;
                // Target - G - 16Bit
                *PByteT = palette256[PI].G;
                PByteT++;
                // Target - R - 24Bit
                *PByteT = palette256[PI].R;
                PByteT++;
                // Target - A - 32Bit
                *PByteT = BOpacity;
                PByteT++; 
              }
            }
            bitmaptarget32bpp.Bitmap.UnlockBits(DataT);
            bitmapsource24bpp.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy BitmapArgb (no Palette) → Bitmap256 / BitmapRgb
    //#################################################################################
    // no sense because of missing palette-parameter!!!
    //public static Boolean CopyBitmapArgbToBitmap256(CBitmapRgb bitmapsource32bpp,
    //                                                out CBitmap256 bitmap256)
    //{
    //  bitmap256 = null;
    //  try
    //  {
    //    bitmap256 = new CBitmap256( palette256 fehlt!!!
    //    return 
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

    public static Boolean CopyBitmapArgbToBitmapRgb(CBitmapArgb bitmapsource32bpp,
                                                    out CBitmapRgb bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (bitmapsource32bpp is CBitmapArgb)
        { // conversion from 32bpp -> 24 bpp
          unsafe
          { // Source(32bpp) ARGB
            Int32 BWS = bitmapsource32bpp.Width;
            Int32 BHS = bitmapsource32bpp.Height;
            BitmapData DataS = bitmapsource32bpp.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                                ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) RGB
            bitmaptarget24bpp = new CBitmapRgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget24bpp.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                               ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                PByteS++; // Source(32bpp)
                // NC Target(24bpp)
              }
            }
            bitmaptarget24bpp.Bitmap.UnlockBits(DataT);
            bitmapsource32bpp.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################
    //  Copy BitmapArgb + Palette256 → Bitmap256 / BitmapRgb
    //#################################################################################
    //    
    public static Boolean CopyBitmapArgbPalette256ToBitmap256(CBitmapArgb bitmapsource32bpp,
                                                              CPalette256 palette256,
                                                              out CBitmap256 bitmaptarget256)
    {
      bitmaptarget256 = null;
      try
      {
        PixelFormat PixelFormatSource = bitmapsource32bpp.PixelFormat;
        bitmaptarget256 = new CBitmap256(bitmapsource32bpp.Bitmap, palette256, PixelFormatSource);
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CopyBitmapArgbPalette256ToBitmapRgb(CBitmapArgb bitmapsource32bpp,
                                                              CPalette256 palette256,
                                                              out CBitmapRgb bitmaptarget24bpp)
    {
      bitmaptarget24bpp = null;
      try
      {
        PixelFormat PixelFormatSource = PixelFormat.Format32bppArgb;
        PixelFormat PixelFormatTarget = PixelFormat.Format24bppRgb;
        if (bitmapsource32bpp is CBitmapArgb)
        { // conversion from 32bpp -> 24 bpp
          unsafe
          { // Source(32bpp) ARGB
            Int32 BWS = bitmapsource32bpp.Width;
            Int32 BHS = bitmapsource32bpp.Height;
            BitmapData DataS = bitmapsource32bpp.Bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                                                ImageLockMode.ReadOnly, PixelFormatSource);
            // Target(32bpp) RGB
            bitmaptarget24bpp = new CBitmapRgb(BWS, BHS);
            Int32 BWT = BWS;
            Int32 BHT = BHS;
            BitmapData DataT = bitmaptarget24bpp.Bitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                               ImageLockMode.WriteOnly, PixelFormatTarget);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            // Bitmap32bpp (4 Byte) -> Bitmap24bpp (3 Byte)
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // Source(32bpp)
                Byte B = *PByteS; PByteS++;
                Byte G = *PByteS; PByteS++;
                Byte R = *PByteS; PByteS++;
                Byte A = *PByteS; PByteS++; // NC 
                Byte PI = (Byte)(0.299f * (float)R + 0.587f * (float)G + 0.114f * (float)B);
                // Target(24bpp)
                *PByteT = palette256[PI].B;
                PByteT++;
                *PByteT = palette256[PI].G;
                PByteT++;
                *PByteT = palette256[PI].R;
              }            
            }
            bitmaptarget24bpp.Bitmap.UnlockBits(DataT);
            bitmapsource32bpp.Bitmap.UnlockBits(DataS);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
