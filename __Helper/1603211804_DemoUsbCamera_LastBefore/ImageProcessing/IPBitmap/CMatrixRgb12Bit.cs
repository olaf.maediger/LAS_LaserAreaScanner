﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPBitmap
{
  public class CMatrixRgb12Bit : CMatrix
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private UInt16[,] FMatrixR;
    private UInt16[,] FMatrixG;
    private UInt16[,] FMatrixB;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrixRgb12Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrixR = new UInt16[FColCount, FRowCount];
      FMatrixG = new UInt16[FColCount, FRowCount];
      FMatrixB = new UInt16[FColCount, FRowCount];
    }
  }
}
