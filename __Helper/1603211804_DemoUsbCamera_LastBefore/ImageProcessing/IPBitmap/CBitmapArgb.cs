﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
namespace IPBitmap
{
  public class CBitmapArgb : CBitmap
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //
    public const Byte INIT_OPACITY = 100;
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    private Byte FOpacity;
    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmapArgb(Int32 width, Int32 height)
      : base(width, height, PixelFormat.Format32bppArgb)
    {
      FOpacity = INIT_OPACITY;
    }
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //
    public override Boolean GetMatrix(out Byte[,] matrix,
                                      Int32 offsetx, Int32 offsety,
                                      Int32 sizex, Int32 sizey)
    {
      matrix = null;
      return false;
    }

    public override Boolean SetMatrix(Byte[,] matrix,
                                      Int32 offsetx, Int32 offsety,
                                      Int32 sizex, Int32 sizey)
    {
      return false;
    }

    public override Boolean SetBitmap(Bitmap bitmap)
    {
      try
      {
        if (!(bitmap is Bitmap)) return false;
        if (PixelFormat.Format24bppRgb == bitmap.PixelFormat)
        {
          if (CopyBitmap24bppTo32bpp(bitmap, FOpacity))
          {
            return true;
          }
        }
        if (PixelFormat.Format32bppArgb == bitmap.PixelFormat)
        {
          if (CopyBitmap32bppTo32bpp(bitmap, FOpacity))
          {
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Helper
    //--------------------------------------------------------
    //
    private Boolean CopyBitmap24bppTo32bpp(Bitmap bitmap, Byte opacity)
    {
      try
      {
        if (PixelFormat.Format24bppRgb == bitmap.PixelFormat)
        {
          Byte BOpacity = (Byte)Math.Max(0, Math.Min(255, opacity * 255f / 100f));
          unsafe
          { // Source - bitmap
            Int32 BWS = bitmap.Width;
            Int32 BHS = bitmap.Height;
            BitmapData DataS = bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                               ImageLockMode.ReadOnly, bitmap.PixelFormat);
            // Target - FBitmap
            Int32 BWT = FBitmap.Width;
            Int32 BHT = FBitmap.Height;
            FBitmap = new Bitmap(BWT, BHT, PixelFormat.Format32bppArgb);
            BitmapData DataT = FBitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                ImageLockMode.WriteOnly, FBitmap.PixelFormat);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                *PByteT = BOpacity;
                PByteT++; // no increment on source 
              }
            }
            bitmap.UnlockBits(DataS);
            FBitmap.UnlockBits(DataT);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean CopyBitmap32bppTo32bpp(Bitmap bitmap, Byte opacity)
    {
      try
      {
        if (PixelFormat.Format24bppRgb == bitmap.PixelFormat)
        {
          Byte Opacity = (Byte)Math.Max(0, Math.Min(100, opacity * 255f / 100f));
          unsafe
          { // Source - bitmap
            Int32 BWS = bitmap.Width;
            Int32 BHS = bitmap.Height;
            BitmapData DataS = bitmap.LockBits(new Rectangle(0, 0, BWS, BHS),
                                               ImageLockMode.ReadOnly, bitmap.PixelFormat);
            // Target - FBitmap
            Int32 BWT = FBitmap.Width;
            Int32 BHT = FBitmap.Height;
            FBitmap = new Bitmap(BWT, BHT, PixelFormat.Format32bppArgb);
            BitmapData DataT = FBitmap.LockBits(new Rectangle(0, 0, BWT, BHT),
                                                ImageLockMode.WriteOnly, FBitmap.PixelFormat);
            //
            Int32 XIL = 0;
            Int32 XIH = BWT - 1;
            Int32 YIL = 0;
            Int32 YIH = BHT - 1;
            for (Int32 YI = YIL; YI <= YIH; YI++)
            {
              Byte* PByteS = (Byte*)(DataS.Scan0 + YI * DataS.Stride);
              Byte* PByteT = (Byte*)(DataT.Scan0 + YI * DataT.Stride);
              for (Int32 XI = XIL; XI <= XIH; XI++)
              { // B - 8Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // G - 16Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // R - 24Bit
                *PByteT = *PByteS;
                PByteT++; PByteS++;
                // A - 32Bit
                *PByteT = Opacity;
                PByteT++; // 
              }
            }
            bitmap.UnlockBits(DataS);
            FBitmap.UnlockBits(DataT);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public
    //--------------------------------------------------------
    //

  }
}
