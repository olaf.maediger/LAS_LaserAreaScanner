﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Task;
//
namespace UCDataTable
{
  public partial class CUCDataTable : UserControl
  {
    //
    //---------------------------------------------------------------------
    //  Section - Constant
    //---------------------------------------------------------------------
    //
    //
    //---------------------------------------------------------------------
    //  Section - Field
    //---------------------------------------------------------------------
    //
    private CTask FTask;
    private Int32 FColCount;
    private Int32 FRowCount;
    private UInt16[,] FMatrix10bit;
    //
    //---------------------------------------------------------------------
    //  Section - Constructor
    //---------------------------------------------------------------------
    //
    public CUCDataTable()
    {
      InitializeComponent();
      //
      FTask = null;
    }
    //
    //---------------------------------------------------------------------
    //  Section - Property
    //---------------------------------------------------------------------
    //
    public void SetMatrix10bit(UInt16[,] matrix10bit)
    {
      if (!(FTask is CTask))
      {
        FTask = new CTask("CopyMatrix10bitToDatagrid",
                          OnTaskExecutionStart,
                          OnTaskExecutionBusy,
                          OnTaskExecutionEnd,
                          OnTaskExecutionAbort);
        FMatrix10bit = (UInt16[,])matrix10bit.Clone();
        FColCount = FMatrix10bit.GetLength(0);
        FRowCount = matrix10bit.GetLength(1);
        FTask.Start();
      }
      // if Task is still copying -> ignore this Matrix10bit!
    }
    //
    //---------------------------------------------------------------------
    //  Section - Cakkback - Task
    //---------------------------------------------------------------------
    //
    private void OnTaskExecutionStart(RTaskData taskdata)
    {
      //      
    }
    private delegate void CBOnTaskExecutionBusy(RTaskData taskdata);
    private void OnTaskExecutionBusy(RTaskData taskdata)
    {
      if (this.InvokeRequired)
      {
        CBOnTaskExecutionBusy CB = new CBOnTaskExecutionBusy(OnTaskExecutionBusy);
        //NC!!!if (FTask is CTask)
        {
          Invoke(CB, new object[] { taskdata });
        }
      }
      else
      {
        this.SuspendLayout();
        dgvTable.SuspendLayout();
        if (FColCount != dgvTable.ColumnCount)
        {
          dgvTable.ColumnCount = FColCount;
          for (Int32 CI = 0; CI < FColCount; CI++)
          {
            dgvTable.Columns[CI].Width = 40;
            dgvTable.Columns[CI].Name = String.Format("C{0}", CI);
          }
        }
        //
        if (FRowCount != dgvTable.RowCount)
        {
          dgvTable.RowCount = FRowCount;
          for (Int32 RI = 0; RI < FRowCount; RI++)
          {
            dgvTable.Rows[RI].HeaderCell.Value = String.Format("R{0}", RI);
          }
        }
        //
        for (Int32 RI = 0; RI < FRowCount; RI++)
        {
          for (Int32 CI = 0; CI < FColCount; CI++)
          {
            dgvTable.Rows[RI].Cells[CI].Value = String.Format("{0}", FMatrix10bit[CI, RI]);
          }
        }
        dgvTable.ResumeLayout(false);
        this.ResumeLayout(false);
      }
      FTask = null;
    }

    private void OnTaskExecutionEnd(RTaskData taskdata)
    {
      // NC 
    }

    private void OnTaskExecutionAbort(RTaskData taskdata)
    {
      FTask = null;
    }
    //
    //---------------------------------------------------------------------
    //  Section - Event
    //---------------------------------------------------------------------
    //

  }
}
