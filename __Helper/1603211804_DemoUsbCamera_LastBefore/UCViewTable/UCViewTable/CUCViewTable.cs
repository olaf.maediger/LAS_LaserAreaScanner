﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCDataTable;
//
namespace UCViewTable
{
  public partial class CUCViewTable : UserControl
  {
    //
    //---------------------------------------------------------------------
    //  Section - Constant
    //---------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------
    //  Section - Field
    //---------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------
    //  Section - Constructor
    //---------------------------------------------------------------------
    //
    public CUCViewTable()
    {
      InitializeComponent();
    }
    //
    //---------------------------------------------------------------------
    //  Section - Property
    //---------------------------------------------------------------------
    //
    public void SetMatrix10bit(UInt16[,] matrix10bit)
    {
      FUCDataTable.SetMatrix10bit(matrix10bit);
    }
    //
    //---------------------------------------------------------------------
    //  Section - Event
    //---------------------------------------------------------------------
    //
    //private void pbxView_SizeChanged(object sender, EventArgs e)
    //{
    //  BuildImage();
    //}

    //private void pbxView_Paint(object sender, PaintEventArgs e)
    //{
    //  if (FImage is Image)
    //  {
    //    e.Graphics.DrawImage(FImage, 0, 0);
    //  }
    //}

    private void mitColorTable_Click(object sender, EventArgs e)
    {
      tbcUser.Visible = true;
      tbcUser.SelectedTab = tbpParameter;
    }

    private void tbcUser_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (tbpHide == tbcUser.SelectedTab)
      {
        tbcUser.Visible = false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Public Management
    //---------------------------------------------------------------------
    //   
    //public void Lock()
    //{
    //  FLockCount++;
    //}
    //public void Unlock()
    //{
    //  if (0 < FLockCount)
    //  {
    //    FLockCount--;
    //  }
    //  if (0 == FLockCount)
    //  {
    //    pbxView.Refresh();
    //  }
    //}

  }
}
