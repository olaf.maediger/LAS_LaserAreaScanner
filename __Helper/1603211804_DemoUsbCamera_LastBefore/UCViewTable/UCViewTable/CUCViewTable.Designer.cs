﻿namespace UCViewTable
{
  partial class CUCViewTable
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tbcUser = new System.Windows.Forms.TabControl();
      this.tbpParameter = new System.Windows.Forms.TabPage();
      this.tbpHide = new System.Windows.Forms.TabPage();
      this.FUCDataTable = new UCDataTable.CUCDataTable();
      this.tbcUser.SuspendLayout();
      this.SuspendLayout();
      // 
      // tbcUser
      // 
      this.tbcUser.Controls.Add(this.tbpParameter);
      this.tbcUser.Controls.Add(this.tbpHide);
      this.tbcUser.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.tbcUser.Location = new System.Drawing.Point(0, 295);
      this.tbcUser.Name = "tbcUser";
      this.tbcUser.SelectedIndex = 0;
      this.tbcUser.Size = new System.Drawing.Size(508, 125);
      this.tbcUser.TabIndex = 0;
      this.tbcUser.SelectedIndexChanged += new System.EventHandler(this.tbcUser_SelectedIndexChanged);
      // 
      // tbpParameter
      // 
      this.tbpParameter.Location = new System.Drawing.Point(4, 22);
      this.tbpParameter.Name = "tbpParameter";
      this.tbpParameter.Padding = new System.Windows.Forms.Padding(3);
      this.tbpParameter.Size = new System.Drawing.Size(500, 99);
      this.tbpParameter.TabIndex = 0;
      this.tbpParameter.Text = "Parameter";
      this.tbpParameter.UseVisualStyleBackColor = true;
      // 
      // tbpHide
      // 
      this.tbpHide.Location = new System.Drawing.Point(4, 22);
      this.tbpHide.Name = "tbpHide";
      this.tbpHide.Padding = new System.Windows.Forms.Padding(3);
      this.tbpHide.Size = new System.Drawing.Size(500, 99);
      this.tbpHide.TabIndex = 1;
      this.tbpHide.Text = "Hide";
      this.tbpHide.UseVisualStyleBackColor = true;
      // 
      // FUCDataTable
      // 
      this.FUCDataTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCDataTable.Location = new System.Drawing.Point(0, 0);
      this.FUCDataTable.Name = "FUCDataTable";
      this.FUCDataTable.Size = new System.Drawing.Size(508, 295);
      this.FUCDataTable.TabIndex = 1;
      // 
      // CUCViewTable
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCDataTable);
      this.Controls.Add(this.tbcUser);
      this.Name = "CUCViewTable";
      this.Size = new System.Drawing.Size(508, 420);
      this.tbcUser.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tbcUser;
    private System.Windows.Forms.TabPage tbpParameter;
    private System.Windows.Forms.TabPage tbpHide;
    private UCDataTable.CUCDataTable FUCDataTable;
  }
}
