﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRandom;
using UCNotifier;
using GHCommon;
using UCEditor;
using UCViewImage;
using GrabberHardware;
using UCCamera;
//
namespace DemoUsbCamera
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    //private const String NAME_SHOWWINDOWFRAME = "ShowWindowFrame";
    //private const Boolean INIT_SHOWWINDOWFRAME = true;
    //
    //private const String TEXT_LEFTOPEN = "Open CameraLeft";
    //private const String TEXT_LEFTCLOSE = "Close CameraLeft";
    //
    private const String NAME_AUTOLOADIMAGE = "AutoLoadImage";
    private const Boolean INIT_AUTOLOADIMAGE = true;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    // NC private CRandomDouble FRandomDouble;
    private Int32 FLeftPreset, FTopPreset;
    private Int32 FCounterImage = 1;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      //
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      // important for correct z-order of child-forms
      this.AddOwnedForm(FUCCamera.UCViewImage.DialogReal);
      this.AddOwnedForm(FUCCamera.UCViewImage.DialogGrid);
      this.AddOwnedForm(FUCCamera.UCViewImage.DialogTitle);
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      //
      FUCCamera.SetOnErrorUCCamera(OnErrorUCCamera);
      FUCCamera.SetOnInfoUCCamera(OnInfoUCCamera);
      FUCCamera.SetOnErrorGrabberHardware(OnErrorGrabberHardware);
      FUCCamera.SetOnInfoGrabberHardware(OnInfoGrabberHardware);
      FUCCamera.SetViewImageOnDialogRealChanged(UCCameraViewImageOnDialogRealChanged);
      FUCCamera.SetViewImageOnDialogGridChanged(UCCameraViewImageOnDialogGridChanged);
      FUCCamera.SetViewImageOnDialogTitleChanged(UCCameraViewImageOnDialogTitleChanged);
      //
      FLeftPreset = Left;
      FTopPreset = Top;
      //
      // FUCCamera.SetNotifier(FUCNotifier);
      FUCCamera.Dock = DockStyle.Fill;
      // FUCCamera.SetOnCameraLeftStateChanged(UCLaserLabyrinthOnCameraLeftStateChanged);
      //
      tmrStartup.Interval = 100;
      tmrStartup.Start();
      //
      // NC FRandomDouble = new CRandomDouble();
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FUCCamera.CloseCamera();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      Boolean BValue;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      Result &= FUCCamera.LoadInitdata(initdata, "UCCamera");
      //
      BValue = INIT_AUTOLOADIMAGE;
      Result &= initdata.ReadBoolean(NAME_AUTOLOADIMAGE, out BValue, BValue);
      cbxAutoLoadImage.Checked = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      Result &= FUCCamera.SaveInitdata(initdata, "UCCamera");
      //
      initdata.WriteBoolean(NAME_AUTOLOADIMAGE, cbxAutoLoadImage.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCCamera
    //------------------------------------------------------------------------
    //    
    private void OnErrorUCCamera(String namefunction,
                                 String texterror)
    {
      String SError = String.Format("UCCamera-{0}: Error: {1}!", namefunction, texterror);
      FUCNotifier.Write(SError);
    }

    private void OnInfoUCCamera(String textinfo)
    {
      String SInfo = String.Format("UCCamera: {0}", textinfo);
      FUCNotifier.Write(SInfo);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - GrabberHardware
    //------------------------------------------------------------------------
    //    
    private void OnErrorGrabberHardware(String namelibrary,
                                        String namefunction,
                                        String texterror)
    {
      String SError = String.Format("{0}-{1}: Error: {2}!", namelibrary, namefunction, texterror);
      FUCNotifier.Write(SError);
    }

    private void OnInfoGrabberHardware(String namelibrary,
                                       String textinfo)
    {
      String SInfo = String.Format("{0}: {1}", namelibrary, textinfo);
      FUCNotifier.Write(SInfo);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCViewImage - DialogReal
    //------------------------------------------------------------------------
    //    
    private void UCCameraViewImageOnDialogRealChanged(Boolean visible, Int32 left, Int32 top)
    {
      cbxShowDialogEditReal.Checked = visible;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCViewImage - DialogGrid
    //------------------------------------------------------------------------
    //    
    private void UCCameraViewImageOnDialogGridChanged(Boolean visible, Int32 left, Int32 top)
    {
      cbxShowDialogEditGrid.Checked = visible;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCViewImage - DialogTitle
    //------------------------------------------------------------------------
    //    
    private void UCCameraViewImageOnDialogTitleChanged(Boolean visible, Int32 left, Int32 top)
    {
      cbxShowDialogEditTitle.Checked = visible;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          String FileName = "Pantoffeltierchen01.jpg";
          Bitmap BF = new Bitmap(FileName);
          Bitmap BC = new Bitmap(BF.Width, BF.Height, PixelFormat.Format24bppRgb); 
          Graphics GBC = Graphics.FromImage(BC);
          GBC.DrawImage(BF, 0, 0, BF.Width, BF.Height);// debug / 2);
          GBC.Dispose();
          FUCCamera.SetBitmapBackground(BC);
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        default:
          return false;
      }
    }
    //
    //########################################################################
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void FormMain_Move(object sender, EventArgs e)
    { // Synchronize positions of all Childs: 
      Int32 LeftActual = Left;
      Int32 TopActual = Top;
      Int32 DL = LeftActual - FLeftPreset;
      Int32 DT = TopActual - FTopPreset;
      FLeftPreset = LeftActual;
      FTopPreset = TopActual;
      //
      FUCCamera.MoveDialogs(DL, DT);
    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }

    private void cbxAutoLoadImage_CheckedChanged(object sender, EventArgs e)
    {
      tmrLoadImage.Enabled = cbxAutoLoadImage.Checked;
    }

    private void cbxShowDialogEditReal_CheckedChanged(object sender, EventArgs e)
    {
      FUCCamera.ShowDialogEditReal(cbxShowDialogEditReal.Checked);
    }

    private void cbxShowDialogEditGrid_CheckedChanged(object sender, EventArgs e)
    {
      FUCCamera.ShowDialogEditGrid(cbxShowDialogEditGrid.Checked);
    }

    private void cbxShowDialogEditTitle_CheckedChanged(object sender, EventArgs e)
    {
      FUCCamera.ShowDialogEditTitle(cbxShowDialogEditTitle.Checked);
    }

    private void tmrLoadImage_Tick(object sender, EventArgs e)
    {
      try
      {
        // ok String FileName = String.Format("Video{0:00}.tif", FCounterImage);
        String FileName = String.Format("Pantoffeltierchen{0:00}.jpg", FCounterImage);
        Bitmap BF = new Bitmap(FileName);
        // debug !!! Bitmap BC = new Bitmap(640, 200, PixelFormat.Format24bppRgb);//BF.Width, BF.Height, PixelFormat.Format24bppRgb);
        Bitmap BC = new Bitmap(BF.Width, BF.Height, PixelFormat.Format24bppRgb); // debug BF.Height / 2, PixelFormat.Format24bppRgb);
        Graphics GBC = Graphics.FromImage(BC);
        GBC.DrawImage(BF, 0, 0, BF.Width, BF.Height);// debug / 2);
        GBC.Dispose();
        FUCCamera.SetBitmapSource(BC);
        FCounterImage++;
        if (18 <= FCounterImage)
        {
          FCounterImage = 1;
        }
      }
      catch (Exception)
      {
      }
    }


  }
}

