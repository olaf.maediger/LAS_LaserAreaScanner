﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NLRandom
{
  public class CRandomInterval : CRandomBase
  {
    private const Double INTERVAL_MINIMUM = -2.0;
    private const Double INTERVAL_MAXIMUM = +2.0;

    public CRandomInterval()
      : base()
    {
    }

    Double GetValue()
    {
      return INTERVAL_MINIMUM + (INTERVAL_MAXIMUM - INTERVAL_MINIMUM) * FRandom.Next();
    }
    public Double Value
    {
      get { return GetValue(); }
    }

  }

}
