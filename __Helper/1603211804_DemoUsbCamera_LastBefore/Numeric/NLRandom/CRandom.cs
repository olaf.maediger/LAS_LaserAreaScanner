﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLRandom
{
  public class CRandomDouble : CRandomBase
  {
    public Double GetValue()
    {
      return FRandom.NextDouble();
    }
  }
}
