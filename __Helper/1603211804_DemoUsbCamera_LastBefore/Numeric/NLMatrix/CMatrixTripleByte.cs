﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using UCNotifier;
using NLScalar;
using NLVector;
using TextFile;
//
namespace NLMatrix
{
  public class CMatrixTripleByte : CMatrixBase
  { //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //
    private const Byte INIT_ZERO = 0x00;
    private const Byte INIT_ONE = 0x01;
    //
    //----------------------------------------
    //	Segment - Member
    //----------------------------------------
    //
    private Byte[,] FRValues = null;
    private Byte[,] FGValues = null;
    private Byte[,] FBValues = null;
    //---------------------------------------
    // Overloading of Index-Operator
    //---------------------------------------
    /* NC protected override Object GetValue(Int32 y, Int32 x)
    {
      if (CheckIndices(y, x))
      {
        return FRValues[y, x];
      }
      return INIT_ZERO;
    }
    protected override void SetValue(Int32 y, Int32 x, Object value)
    {
      if (CheckIndices(y, x))
      {
        FRValues[y, x] = (Byte)value;
      }
    }*/

    protected override Object GetValue(Int32 y, Int32 x)
    {
      unsafe
      {
        fixed (Byte* PValues = FRValues)
        {
          Byte BValue = *(Byte*)(PValues + y * FColCount + x);
          return (Byte)BValue;
        }
      }
    }
    protected override void SetValue(Int32 y, Int32 x, Object value)
    {
      unsafe
      {
        fixed (Byte* PValues = FRValues)
        {
          *(Byte*)(PValues + y * FColCount + x) = (Byte)value;
        }
      }
    }
    public new Byte this[Int32 y, Int32 x]
    {
      get { return (Byte)GetValue(y, x); }
      set { SetValue(y, x, value); }
    }

    public Byte GetRValue(Int32 y, Int32 x)
    {
      unsafe
      {
        fixed (Byte* PValues = FRValues)
        {
          Byte BValue = *(Byte*)(PValues + y * FColCount + x);
          return (Byte)BValue;
        }
      }
    }
    public void SetRValue(Int32 y, Int32 x, Byte value)
    {
      unsafe
      {
        fixed (Byte* PValues = FRValues)
        {
          *(Byte*)(PValues + y * FColCount + x) = value;
        }
      }
    }

    public Byte GetGValue(Int32 y, Int32 x)
    {
      unsafe
      {
        fixed (Byte* PValues = FGValues)
        {
          Byte BValue = *(Byte*)(PValues + y * FColCount + x);
          return (Byte)BValue;
        }
      }
    }
    public void SetGValue(Int32 y, Int32 x, Byte value)
    {
      unsafe
      {
        fixed (Byte* PValues = FGValues)
        {
          *(Byte*)(PValues + y * FColCount + x) = value;
        }
      }
    }

    public Byte GetBValue(Int32 y, Int32 x)
    {
      unsafe
      {
        fixed (Byte* PValues = FBValues)
        {
          Byte BValue = *(Byte*)(PValues + y * FColCount + x);
          return (Byte)BValue;
        }
      }
    }
    public void SetBValue(Int32 y, Int32 x, Byte value)
    {
      unsafe
      {
        fixed (Byte* PValues = FBValues)
        {
          *(Byte*)(PValues + y * FColCount + x) = value;
        }
      }
    }

    // Reading SolutionVector/Writing InhomogenousVector
    public IntPtr GetBytePointerR()
    {
      unsafe
      {
        fixed (Byte* PByte = FRValues)
        {
          return (IntPtr)PByte;
        }
      }
    }
    public IntPtr GetBytePointerG()
    {
      unsafe
      {
        fixed (Byte* PByte = FGValues)
        {
          return (IntPtr)PByte;
        }
      }
    }
    public IntPtr GetBytePointerB()
    {
      unsafe
      {
        fixed (Byte* PByte = FGValues)
        {
          return (IntPtr)PByte;
        }
      }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CMatrixTripleByte(CNotifier notifier, Int32 rowcount, Int32 colcount)
      : base(notifier, rowcount, colcount)
    {
      FRValues = new Byte[rowcount, colcount];
      FGValues = new Byte[rowcount, colcount];
      FBValues = new Byte[rowcount, colcount];
      FBitResolution = EBitResolution.TripleBit8;
      InitValues(INIT_ZERO);
    }

    public CMatrixTripleByte(CNotifier notifier, Int32 rowcount, Int32 colcount, Byte initvalue)
      : base(notifier, rowcount, colcount)
    {
      FRValues = new Byte[rowcount, colcount];
      FGValues = new Byte[rowcount, colcount];
      FBValues = new Byte[rowcount, colcount];
      FBitResolution = EBitResolution.TripleBit8;
      InitValues(initvalue);
    }

    public CMatrixTripleByte(CNotifier notifier, Byte[,] matrix)
      : base(notifier, matrix.GetLength(0), matrix.GetLength(1))
    {
      FRValues = new Byte[matrix.GetLength(0), matrix.GetLength(1)];
      FGValues = new Byte[matrix.GetLength(0), matrix.GetLength(1)];
      FBValues = new Byte[matrix.GetLength(0), matrix.GetLength(1)];
      FBitResolution = EBitResolution.TripleBit8;
      InitValues(INIT_ZERO);
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CMatrixTripleByte(Int32 rowcount, Int32 colcount)
      : base(rowcount, colcount)
    {
      FRValues = new Byte[rowcount, colcount];
      FGValues = new Byte[rowcount, colcount];
      FBValues = new Byte[rowcount, colcount];
      FBitResolution = EBitResolution.TripleBit8;
      InitValues(INIT_ZERO);
    }

    public CMatrixTripleByte(Int32 rowcount, Int32 colcount, Byte initvalue)
      : base(rowcount, colcount)
    {
      FRValues = new Byte[rowcount, colcount];
      FGValues = new Byte[rowcount, colcount];
      FBValues = new Byte[rowcount, colcount];
      FBitResolution = EBitResolution.TripleBit8;
      InitValues(initvalue);
    }

    public CMatrixTripleByte(Byte[,] matrix)
      : base(matrix.GetLength(0), matrix.GetLength(1))
    {
      FRValues = new Byte[matrix.GetLength(0), matrix.GetLength(1)];
      FGValues = new Byte[matrix.GetLength(0), matrix.GetLength(1)];
      FBValues = new Byte[matrix.GetLength(0), matrix.GetLength(1)];
      FBitResolution = EBitResolution.TripleBit8;
      InitValues(INIT_ZERO);
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public override Boolean DefineMatrix(Int32 rowcount, Int32 colcount)
    {
      base.DefineMatrix(rowcount, colcount);
      FRValues = new Byte[rowcount, colcount];
      FGValues = new Byte[rowcount, colcount];
      FBValues = new Byte[rowcount, colcount];
      return InitValues(INIT_ZERO);
    }

    public Boolean InitValues(Byte initvalue)
    {
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          FRValues[YI, XI] = initvalue;
          FGValues[YI, XI] = initvalue;
          FBValues[YI, XI] = initvalue;
        }
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    protected override void Write(String header)
    {
      base.Write(header);
      if (FNotifier is CNotifier)
      {
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          String Line = String.Format(" RI[{0}]:", YI);
          for (Int32 XI = XIL; XI <= XIH; XI++)
          {
            Line += String.Format(" {0}:{1:X02}", XI, this[YI, XI]);
          }
          GetNotifier().Write(Line);
        }
      }
    }

    protected override void Write(String header,
                                  String formatpositive,
                                  String formatnegative)
    {
      GetNotifier().Write(header);
      if (FNotifier is CNotifier)
      {
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          String Line = String.Format(" RI[{0}]:", YI);
          for (Int32 XI = XIL; XI <= XIH; XI++)
          {
            Double DValue = this[YI, XI];
            String SValue;

            if (0 < DValue)
            {
              SValue = String.Format(formatpositive, DValue);
            }
            else
            {
              SValue = String.Format(formatnegative, DValue);
            }
            Line += String.Format(" {0}:{1}", XI, SValue);
          }
          GetNotifier().Write(Line);
        }
      }
    }
    //
    //----------------------------------------
    //	Segment - Management
    //----------------------------------------
    //
    public override CMatrixBase Clone()
    {
      CMatrixBase Matrix = new CMatrixTripleByte(base.GetNotifier(), SizeY, SizeX);
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          ((CMatrixTripleByte)Matrix).SetRValue(YI, XI, this.FRValues[YI, XI]);
          ((CMatrixTripleByte)Matrix).SetGValue(YI, XI, this.FGValues[YI, XI]);
          ((CMatrixTripleByte)Matrix).SetBValue(YI, XI, this.FBValues[YI, XI]);
        }
      }
      return Matrix;
    }

    public Boolean SetDimension(Int32 sizey, Int32 sizex, Byte initvalue)
    {
      if (base.SetDimension(sizey, sizex))
      {
        FRValues = new Byte[sizey, sizex];
        FGValues = new Byte[sizey, sizex];
        FBValues = new Byte[sizey, sizex];
        InitValues(initvalue);
        return true;
      }
      return false;
    }

    public Boolean SetConstant(Byte value)
    {
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          this.FRValues[YI, XI] = value;
          this.FGValues[YI, XI] = value;
          this.FBValues[YI, XI] = value;
        }
      }
      return true;
    }

    public override Boolean SetUnit()
    {
      if (base.SetUnit())
      {
        SetConstant(INIT_ZERO);
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          this.FRValues[YI, YI] = INIT_ONE;
          this.FGValues[YI, YI] = INIT_ONE;
          this.FBValues[YI, YI] = INIT_ONE;
        }
        return true;
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - File
    //----------------------------------------
    //
    public override Boolean LoadFromTextFile(String fileentry)
    {
      CTextFile TextFile = new CTextFile();//GetNotifier());
      if (TextFile.OpenRead(fileentry))
      {
        Int32 RC = TextFile.RowCount;
        Int32 CC = TextFile.ColCount(0);
        DefineMatrix(RC, CC);
        //
        Boolean Endloop = false;
        while (!Endloop)
        { // Read next Value
          Int32 YI;
          Int32 XI;
          if (TextFile.ReadRowIndex(out YI))
          {
            if (TextFile.ReadColIndex(out XI))
            {
              Byte BValue;
              String SValue;
              if (TextFile.Read(out BValue, out SValue))
              {
                FRValues[YI, XI] = BValue;
              }
              else
              {
                Endloop = true;
              }
              if (TextFile.Read(out BValue, out SValue))
              {
                FGValues[YI, XI] = BValue;
              }
              else
              {
                Endloop = true;
              }
              if (TextFile.Read(out BValue, out SValue))
              {
                FBValues[YI, XI] = BValue;
              }
              else
              {
                Endloop = true;
              }
            }
            else
            {
              Endloop = true;
            }
          }
          else
          {
            Endloop = true;
          }
        }
        //
        TextFile.Close();
        return true;
      }
      TextFile.Close();
      return false;
    }

    public override Boolean SaveToTextFile(String fileentry)
    {
      RTextFileData TextFileData;
      GetTextFileData(out TextFileData);
      TextFileData.Entry = fileentry;
      Boolean Result = true;
      if ((XIH < 0) || (YIH < 0))
      {
        return false;
      }
      CTextFile TextFile = new CTextFile();//GetNotifier());
      //
      try
      {
        TextFileData = new RTextFileData();
        Result &= TextFile.GetData(ref TextFileData);
        String DelimiterValue = TextFileData.TokenDelimiters[0];
        String DelimiterLine = TextFileData.LineDelimiters[0];
        //
        Result &= TextFile.OpenWrite(fileentry);
        //
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          for (Int32 XI = XIL; XI < XIH; XI++)
          {
            TextFile.Write((Byte)FRValues[YI, XI]);
            TextFile.Write(DelimiterValue);
            TextFile.Write((Byte)FGValues[YI, XI]);
            TextFile.Write(DelimiterValue);
            TextFile.Write((Byte)FBValues[YI, XI]);
            TextFile.Write(DelimiterValue);
          }
          TextFile.Write((Byte)FRValues[YI, XIH]);
          TextFile.Write(DelimiterValue);
          TextFile.Write((Byte)FGValues[YI, XIH]);
          TextFile.Write(DelimiterValue);
          TextFile.Write((Byte)FBValues[YI, XIH]);
          TextFile.Write(DelimiterLine);
        }

        Result &= true;
      }
      catch (Exception)
      {
        Result = false;
      }
      finally
      {
        Result &= TextFile.Close();
      }
      return Result;
    }
    //
    //----------------------------------------
    //	Segment - Mathematic Primitives
    //----------------------------------------
    //
    public Boolean SetColVectorR(CVectorByte colvector, Int32 colindex)
    {
      if ((colindex < XIL) || (XIH < colindex))
      {
        return false;
      }
      if (colvector.Size != this.SizeX)
      {
        return false;
      }
      for (Int32 RI = colvector.VIL; RI <= colvector.VIH; RI++)
      {     // Row Col
        FRValues[RI, colindex] = colvector[RI];
      }
      return true;
    }
    public Boolean SetColVectorG(CVectorByte colvector, Int32 colindex)
    {
      if ((colindex < XIL) || (XIH < colindex))
      {
        return false;
      }
      if (colvector.Size != this.SizeX)
      {
        return false;
      }
      for (Int32 RI = colvector.VIL; RI <= colvector.VIH; RI++)
      {     // Row Col
        FGValues[RI, colindex] = colvector[RI];
      }
      return true;
    }
    public Boolean SetColVectorB(CVectorByte colvector, Int32 colindex)
    {
      if ((colindex < XIL) || (XIH < colindex))
      {
        return false;
      }
      if (colvector.Size != this.SizeX)
      {
        return false;
      }
      for (Int32 RI = colvector.VIL; RI <= colvector.VIH; RI++)
      {     // Row Col
        FBValues[RI, colindex] = colvector[RI];
      }
      return true;
    }

    public Boolean SetRowVectorR(CVectorByte rowvector, Int32 rowindex)
    {
      if ((rowindex < YIL) || (YIH < rowindex))
      {
        return false;
      }
      if (rowvector.Size != this.SizeY)
      {
        return false;
      }
      for (Int32 CI = rowvector.VIL; CI <= rowvector.VIH; CI++)
      {    // Row Col
        FRValues[rowindex, CI] = rowvector[CI];
      }
      return true;
    }
    public Boolean SetRowVectorG(CVectorByte rowvector, Int32 rowindex)
    {
      if ((rowindex < YIL) || (YIH < rowindex))
      {
        return false;
      }
      if (rowvector.Size != this.SizeY)
      {
        return false;
      }
      for (Int32 CI = rowvector.VIL; CI <= rowvector.VIH; CI++)
      {    // Row Col
        FGValues[rowindex, CI] = rowvector[CI];
      }
      return true;
    }
    public Boolean SetRowVectorB(CVectorByte rowvector, Int32 rowindex)
    {
      if ((rowindex < YIL) || (YIH < rowindex))
      {
        return false;
      }
      if (rowvector.Size != this.SizeY)
      {
        return false;
      }
      for (Int32 CI = rowvector.VIL; CI <= rowvector.VIH; CI++)
      {    // Row Col
        FBValues[rowindex, CI] = rowvector[CI];
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Mathematic Functions
    //----------------------------------------
    //
    public Boolean Transpose(out CMatrixTripleByte target)
    { // exchange y <-> X
      target = new CMatrixTripleByte(GetNotifier(), SizeY, SizeX);
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          target.SetRValue(XI, YI, FRValues[YI, XI]);
          target.SetGValue(XI, YI, FGValues[YI, XI]);
          target.SetBValue(XI, YI, FBValues[YI, XI]);
        }
      }
      return true;
    }

    public Boolean Multiply(CMatrixTripleByte multiplicator, out CMatrixTripleByte result)
    { // result(Matrix) = this(Matrix) * multiplicator(Matrix)
      result = null;
      // check: ColCount(this) == RowCount(multiplicator)
      if ((this.SizeX == multiplicator.SizeY) &&
          (this.SizeY == multiplicator.SizeX))
      { // Row/ColCount(result) := this.SizeY
        Int32 Size = this.SizeY;
        result = new CMatrixTripleByte(GetNotifier(), Size, Size);
        for (Int32 YI = result.YIL; YI <= result.YIH; YI++)
        {
          for (Int32 XI = result.XIL; XI <= result.XIH; XI++)
          {
            Double SumR = 0.0;
            Double SumG = 0.0;
            Double SumB = 0.0;
            for (Int32 SI = this.XIL; SI <= this.XIH; SI++)
            {
              SumR += FRValues[YI, SI] * multiplicator.GetRValue(SI, XI);
              SumG += FGValues[YI, SI] * multiplicator.GetGValue(SI, XI);
              SumB += FBValues[YI, SI] * multiplicator.GetBValue(SI, XI);
            }
            result.SetRValue(YI, XI, (Byte)SumR);
            result.SetGValue(YI, XI, (Byte)SumG);
            result.SetBValue(YI, XI, (Byte)SumB);
          }
        }
        return true;
      }
      return false;
    }

    public Boolean Multiply(CVectorTripleByte multiplicator, out CVectorTripleByte result)
    { // result(CVector) = this(CMatrix) * multiplicator(CVector)
      result = null;
      // check: ColCount(this) == Size(multiplicator)
      if (this.SizeX == multiplicator.Size)
      { // Size(result) := this.SizeY
        Int32 Size = this.SizeY;
        result = new CVectorTripleByte(GetNotifier(), Size);
        for (Int32 VI = result.VIL; VI <= result.VIH; VI++)
        {
          Double SumR = 0.0;
          Double SumG = 0.0;
          Double SumB = 0.0;
          for (Int32 SI = this.XIL; SI <= this.XIH; SI++)
          {
            SumR += FRValues[VI, SI] * multiplicator[SI];
            SumG += FGValues[VI, SI] * multiplicator[SI];
            SumB += FBValues[VI, SI] * multiplicator[SI];
          }
          result.SetRValue(VI, (Byte)SumR);
          result.SetGValue(VI, (Byte)SumG);
          result.SetBValue(VI, (Byte)SumB);
        }
        return true;
      }
      return false;
    }

    public Boolean Normalize()
    {
      // Find ByteMaximum
      Byte ByteMaximum = 0x00;
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          Byte BValue = this[YI, XI];
          if (ByteMaximum < BValue)
          {
            ByteMaximum = BValue;
          }
        }
      }
      // Normalize with ByteMaximum
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          Byte BValue = this[YI, XI];
          BValue = (Byte)(255.0f * (float)(BValue) / (float)ByteMaximum);
          this[YI, XI] = BValue;
        }
      }
      return true;
    }


    public Boolean FindMaximum(out Int32 xindex,
                               out Int32 yindex,
                               out Int32 value)
    {
      value = this[0, 0];
      xindex = 0;
      yindex = 0;
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          Byte BValue = this[YI, XI];
          if (value < BValue)
          {
            value = BValue;
            xindex = XI;
            yindex = YI;
          }
        }
      }
      return true;
    }

    public Boolean FindMinimum(out Int32 xindex,
                               out Int32 yindex,
                               out Int32 value)
    {
      value = this[0, 0];
      xindex = 0;
      yindex = 0;
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          Byte BValue = this[YI, XI];
          if (BValue < value)
          {
            value = BValue;
            xindex = XI;
            yindex = YI;
          }
        }
      }
      return true;
    }

    public Boolean SumOverAll(out Int32 result)
    {
      result = 0;
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          Byte BValue = this[YI, XI];
          result += BValue;
        }
      }
      return true;
    }
    //
    //---------------------------------------------
    //	Segment - Mathematic Functions - HighLevel
    //---------------------------------------------
    //
    public override Boolean AddConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] += (Byte)constant;
                FGValues[YI, XI] += (Byte)constant;
                FBValues[YI, XI] += (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] += (Byte)constant;
                FGValues[YI, XI] += (Byte)constant;
                FBValues[YI, XI] += (Byte)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] += (Byte)constant;
                FGValues[YI, XI] += (Byte)constant;
                FBValues[YI, XI] += (Byte)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean SubtractConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] -= (Byte)constant;
                FGValues[YI, XI] -= (Byte)constant;
                FBValues[YI, XI] -= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] -= (Byte)constant;
                FGValues[YI, XI] -= (Byte)constant;
                FBValues[YI, XI] -= (Byte)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] -= (Byte)constant;
                FGValues[YI, XI] -= (Byte)constant;
                FBValues[YI, XI] -= (Byte)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean MultiplyConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] *= (Byte)constant;
                FGValues[YI, XI] *= (Byte)constant;
                FBValues[YI, XI] *= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] *= (Byte)constant;
                FGValues[YI, XI] *= (Byte)constant;
                FBValues[YI, XI] *= (Byte)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] *= (Byte)constant;
                FGValues[YI, XI] *= (Byte)constant;
                FBValues[YI, XI] *= (Byte)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean DivideConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] /= (Byte)constant;
                FGValues[YI, XI] /= (Byte)constant;
                FBValues[YI, XI] /= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] /= (Byte)constant;
                FGValues[YI, XI] /= (Byte)constant;
                FBValues[YI, XI] /= (Byte)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FRValues[YI, XI] /= (Byte)constant;
                FGValues[YI, XI] /= (Byte)constant;
                FBValues[YI, XI] /= (Byte)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Transpose(out CMatrixBase target)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        target = new CMatrixTripleByte(GetNotifier(), SX, SY);
        for (Int32 YI = 0; YI < SY; YI++)
        {
          for (Int32 XI = 0; XI < SX; XI++)
          {
            ((CMatrixTripleByte)target).SetRValue(XI, YI, FRValues[YI, XI]);
            ((CMatrixTripleByte)target).SetGValue(XI, YI, FGValues[YI, XI]);
            ((CMatrixTripleByte)target).SetBValue(XI, YI, FBValues[YI, XI]);
          }
        }
        return true;
      }
      catch (Exception)
      {
        target = null;
        return false;
      }
    }

    public override Boolean Add(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == sourceb.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[l x m] -> Result[l x m]
          result = new CMatrixTripleByte(GetNotifier(), SY, sourceb.SizeX);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Byte BValue = (Byte)sourceb[YI, XI];
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)(FRValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)result).SetGValue(YI, XI, (Byte)(FGValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)result).SetBValue(YI, XI, (Byte)(FBValues[YI, XI] + BValue));
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Byte BValue = (Byte)sourceb[YI, XI];
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)(FRValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)result).SetGValue(YI, XI, (Byte)(FGValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)result).SetBValue(YI, XI, (Byte)(FBValues[YI, XI] + BValue));
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Byte BValue = (Byte)sourceb[YI, XI];
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)(FRValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)result).SetGValue(YI, XI, (Byte)(FGValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)result).SetBValue(YI, XI, (Byte)(FBValues[YI, XI] + BValue));
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public override Boolean Add(CMatrixBase source)
    {
      try
      {
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == source.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[l x m] -> Result[l x m]
          Type TValue = source.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Byte BValue = (Byte)source[YI, XI];
                  ((CMatrixTripleByte)this).SetRValue(YI, XI, (Byte)(FRValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)this).SetGValue(YI, XI, (Byte)(FGValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)this).SetBValue(YI, XI, (Byte)(FBValues[YI, XI] + BValue));
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Byte BValue = (Byte)source[YI, XI];
                  ((CMatrixTripleByte)this).SetRValue(YI, XI, (Byte)(FRValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)this).SetGValue(YI, XI, (Byte)(FGValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)this).SetBValue(YI, XI, (Byte)(FBValues[YI, XI] + BValue));
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Byte BValue = (Byte)source[YI, XI];
                  ((CMatrixTripleByte)this).SetRValue(YI, XI, (Byte)(FRValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)this).SetGValue(YI, XI, (Byte)(FGValues[YI, XI] + BValue));
                  ((CMatrixTripleByte)this).SetBValue(YI, XI, (Byte)(FBValues[YI, XI] + BValue));
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Subtract(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == sourceb.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[l x m] -> Result[l x m]
          result = new CMatrixByte(GetNotifier(), SY, sourceb.SizeX);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Byte BValue = (Byte)sourceb[YI, XI];
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)(FRValues[YI, XI] - BValue));
                  ((CMatrixTripleByte)result).SetGValue(YI, XI, (Byte)(FGValues[YI, XI] - BValue));
                  ((CMatrixTripleByte)result).SetBValue(YI, XI, (Byte)(FBValues[YI, XI] - BValue));
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Byte BValue = (Byte)sourceb[YI, XI];
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)(FRValues[YI, XI] - BValue));
                  ((CMatrixTripleByte)result).SetGValue(YI, XI, (Byte)(FGValues[YI, XI] - BValue));
                  ((CMatrixTripleByte)result).SetBValue(YI, XI, (Byte)(FBValues[YI, XI] - BValue));
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Byte BValue = (Byte)sourceb[YI, XI];
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)(FRValues[YI, XI] - BValue));
                  ((CMatrixTripleByte)result).SetGValue(YI, XI, (Byte)(FGValues[YI, XI] - BValue));
                  ((CMatrixTripleByte)result).SetBValue(YI, XI, (Byte)(FBValues[YI, XI] - BValue));
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public override Boolean Multiply(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == sourceb.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[m x n] -> Result[l x n]
          // ColCount(A)[this.SizeY x this.SizeX] == RowCount(B)[B.SizeY x B.SizeX] 
          // -> Result[this.SizeY x b.SizeX]
          result = new CMatrixTripleByte(GetNotifier(), SY, sourceb.SizeX);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Double SumR = 0.0;
                  Double SumG = 0.0;
                  Double SumB = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Byte BValue = (Byte)sourceb[KI, XI];
                    SumR += FRValues[YI, KI] * BValue;
                    SumG += FGValues[YI, KI] * BValue;
                    SumB += FBValues[YI, KI] * BValue;
                  }
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)SumR);
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)SumG);
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)SumB);
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Double SumR = 0.0;
                  Double SumG = 0.0;
                  Double SumB = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    UInt16 UValue = (UInt16)sourceb[KI, XI];
                    SumR += FRValues[YI, KI] * UValue;
                    SumG += FGValues[YI, KI] * UValue;
                    SumB += FBValues[YI, KI] * UValue;
                  }
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)SumR);
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)SumG);
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)SumB);
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Double SumR = 0.0;
                  Double SumG = 0.0;
                  Double SumB = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Double DValue = (Double)sourceb[KI, XI];
                    SumR += FRValues[YI, KI] * DValue;
                    SumG += FGValues[YI, KI] * DValue;
                    SumB += FBValues[YI, KI] * DValue;
                  }
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)SumR);
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)SumG);
                  ((CMatrixTripleByte)result).SetRValue(YI, XI, (Byte)SumB);
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }


  }
}
