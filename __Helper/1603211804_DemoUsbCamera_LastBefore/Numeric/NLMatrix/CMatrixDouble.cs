﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using NLScalar;
using NLVector;
using TextFile;
//
namespace NLMatrix
{
  public class CMatrixDouble : CMatrixBase
  { //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //
    private const Double INIT_ZERO = 0.0;
    private const Double INIT_ONE = 1.0;
    //
    //----------------------------------------
    //	Segment - Member
    //----------------------------------------
    //
    private Double[,] FValues = null;
    //---------------------------------------
    // Overloading of Index-Operator
    //---------------------------------------
    /* NC protected override Object GetValue(Int32 y, Int32 x)
    {
      if (CheckIndices(y, x))
      {
        return FValues[y, x];
      }
      return INIT_ZERO;
    }
    protected override void SetValue(Int32 y, Int32 x, Object value)
    {
      if (CheckIndices(y, x))
      {
        FValues[y, x] = (Double)value;
      }
    }*/

    /*/ Reading/Writing MainMatrix
    public new Double this[Int32 y, Int32 x]
    { // Pointer!!!!!! see above !!!!!!!!!!!!!!!
      get 
      { 
        return (Double)GetValue(y, x); }
      set { SetValue(y, x, value); }
    }*/
    // Pointer!!!!!! see above !!!!!!!!!!!!!!!
    // Reading SolutionVector/Writing InhomogenousVector
    /* NC protected Double this[Int32 y]
    {
      get { return GetValue(y, XIH); }
      set { SetValue(y, XIH, value); }
    }*/


    protected override Object GetValue(Int32 y, Int32 x)
    {
      unsafe
      {
        fixed (Double* PValues = FValues)
        {
          Double BValue = *(Double*)(PValues + y * FColCount + x);
          return (Double)BValue;
        }
      }
    }
    protected override void SetValue(Int32 y, Int32 x, Object value)
    {
      unsafe
      {
        fixed (Double* PValues = FValues)
        {
          *(Double*)(PValues + y * FColCount + x) = (Double)value;
        }
      }
    }
    public new Double this[Int32 y, Int32 x]
    {
      get { return (Double)GetValue(y, x); }
      set { SetValue(y, x, value); }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CMatrixDouble(CNotifier notifier, Int32 rowcount, Int32 colcount)
      : base(notifier, rowcount, colcount)
    {
      FValues = new Double[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit64;
      InitValues(INIT_ZERO);
    }
    
    public CMatrixDouble(CNotifier notifier, Int32 rowcount, Int32 colcount, Double initvalue)
      : base(notifier, rowcount, colcount)
    {
    	FValues = new Double[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit64;
      InitValues(initvalue);
		}

    public CMatrixDouble(CNotifier notifier, CMatrixDouble source)
      : base(notifier, source.SizeY, source.SizeX)
    {
      FValues = new Double[source.SizeY, source.SizeX];
      FBitResolution = EBitResolution.MonoBit64;
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          FValues[YI, XI] = source[YI, XI];
        }
      }      
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CMatrixDouble(Int32 rowcount, Int32 colcount)
      : base(rowcount, colcount)
    {
      FValues = new Double[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit64;
      InitValues(INIT_ZERO);
    }

    public CMatrixDouble(Int32 rowcount, Int32 colcount, Double initvalue)
      : base(rowcount, colcount)
    {
      FValues = new Double[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit64;
      InitValues(initvalue);
    }

    public CMatrixDouble(CMatrixDouble source)
      : base(source.SizeY, source.SizeX)
    {
      FValues = new Double[source.SizeY, source.SizeX];
      FBitResolution = EBitResolution.MonoBit64;
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          FValues[YI, XI] = source[YI, XI];
        }
      }
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public override Boolean DefineMatrix(Int32 rowcount, Int32 colcount)
    {
      base.DefineMatrix(rowcount, colcount);
      FValues = new Double[rowcount, colcount];
      return InitValues(INIT_ZERO);
    }

    public Boolean InitValues(Double initvalue)
    {
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          FValues[YI, XI] = initvalue;
        }
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    protected override void Write(String header)
    {
      base.Write(header);
      if (FNotifier is CNotifier)
      {
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          String Line = String.Format(" RI[{0}]:", YI);
          for (Int32 XI = XIL; XI <= XIH; XI++)
          {
            Line += String.Format(" {0}:{1:0.000000000}", XI, this[YI, XI]);
          }
          GetNotifier().Write(Line);
        }
      }
    }

    protected override void Write(String header,
                                  String formatpositive,
                                  String formatnegative)
    {
      GetNotifier().Write(header);
      if (FNotifier is CNotifier)
      {
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          String Line = String.Format(" RI[{0}]:", YI);
          for (Int32 XI = XIL; XI <= XIH; XI++)
          {
            Double DValue = this[YI, XI];
            String SValue;

            if (0 < DValue)
            {
              SValue = String.Format(formatpositive, DValue);
            }
            else
            {
              SValue = String.Format(formatnegative, DValue);
            }
            Line += String.Format(" {0}:{1}", XI, SValue);
          }
          GetNotifier().Write(Line);
        }
      }
    }
    //
    //----------------------------------------
    //	Segment - Helper
    //----------------------------------------
    //
    private static Boolean BuildMainDiagonalUnequalZero(ref CMatrixDouble matrixmain,
                                                        ref CMatrixDouble matrixsub)
    {
      try
      {
        Int32 SYMain = matrixmain.SizeY;
        Int32 SXMain = matrixmain.SizeX;
        Int32 SYSub = matrixsub.SizeY;
        Int32 SXSub = matrixsub.SizeX;
        if ((SXMain == SYMain) && (SYMain == SYSub) && (SXMain == SXSub))
        {
          Boolean[] MainDiagonalUnequalZero = new Boolean[SYMain];
          // NC CMatrixDouble MatrixMain = new CMatrixDouble(null, matrixmain);
          for (Int32 MDI = 0; MDI < SYMain; MDI++)
          {
            MainDiagonalUnequalZero[MDI] = false;
            if (0.0 != matrixmain[MDI, MDI])
            {
              MainDiagonalUnequalZero[MDI] = true;
            }
            else
            { // MainDiagonalElement equal Zero -> search [Rows, MDI] for unequal!
              Int32 YI = 0;
              do
              {
                if (YI != MDI) // jump over MDI-Line!
                {
                  if (0.0 != matrixmain[YI, MDI])
                  {
                    for (Int32 XI = 0; XI < SXMain; XI++)
                    {
                      matrixmain[MDI, XI] += matrixmain[YI, XI];
                      matrixsub[MDI, XI] += matrixsub[YI, XI];
                    }
                    MainDiagonalUnequalZero[MDI] = true;
                  }
                }
                YI++;
              }
              while ((YI < SYMain) && !MainDiagonalUnequalZero[MDI]);
            }
          }
          // All MDI != Zero?
          Boolean BResult = true;
          for (Int32 MDI = 0; MDI < SYMain; MDI++)
          {
            BResult &= MainDiagonalUnequalZero[MDI];
          }
          return BResult;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private static Boolean BuildMainDiagonalOne(ref CMatrixDouble matrixmain,
                                                ref CMatrixDouble matrixsub)
    {
      try
      {
        Int32 SYMain = matrixmain.SizeY;
        Int32 SXMain = matrixmain.SizeX;
        Int32 SYSub = matrixsub.SizeY;
        Int32 SXSub = matrixsub.SizeX;
        if ((SXMain == SYMain) && (SYMain == SYSub) && (SXMain == SXSub))
        {
          for (Int32 MDI = 0; MDI < SYMain; MDI++)
          {
            if (0.0 == matrixmain[MDI, MDI])
            {
              return false;
            }
            // MainDiagonalElement equal Zero -> Divide by MainDiagonaElement
            Double Divisor = matrixmain[MDI, MDI];
            for (Int32 XI = 0; XI < SXMain; XI++)
            {
              matrixmain[MDI, XI] /= Divisor;
              matrixsub[MDI, XI] /= Divisor;
            }
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }    
    //
    //----------------------------------------
    //	Segment - Management
    //----------------------------------------
    //
    public override CMatrixBase Clone()
    {
      CMatrixBase Matrix = new CMatrixDouble(base.GetNotifier(), SizeY, SizeX);
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          FValues[YI, XI] = this[YI, XI];
        }
      }
      return Matrix;
    }

    public Boolean SetDimension(Int32 sizey, Int32 sizex, Double initvalue)
    {
      if (base.SetDimension(sizey, sizex))
      {
        FValues = new Double[sizey, sizex];
        InitValues(initvalue);
        return true;
      }
      return false;
    }

    public Boolean SetConstant(Double value)
    {
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          this[YI, XI] = value;
        }
      }
      return true;
    }

    public override Boolean SetUnit()
    {
      if (base.SetUnit())
      {
        SetConstant(INIT_ZERO);
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          this[YI, YI] = INIT_ONE;
        }
        return true;
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - File
    //----------------------------------------
    //
    public override Boolean LoadFromTextFile(String fileentry)
    {
      CTextFile TextFile = new CTextFile();//GetNotifier());
      if (TextFile.OpenRead(fileentry))
      {
        DefineMatrix(TextFile.RowCount, TextFile.ColCount(0));
        //
        Boolean Endloop = false;
        while (!Endloop)
        { // Read next Value
          Int32 YI;
          Int32 XI;
          if (TextFile.ReadRowIndex(out YI))
          {
            if (TextFile.ReadColIndex(out XI))
            {
              Double DValue;
              String SValue;
              if (TextFile.Read(out DValue, out SValue))
              {
                FValues[YI, XI] = DValue;
              }
              else
              {
                Endloop = true;
              }
            }
            else
            {
              Endloop = true;
            }
          }
          else
          {
            Endloop = true;
          }
        }
        //
        TextFile.Close();
        return true;
      }
      TextFile.Close();
      return false;
    }

    public override Boolean SaveToTextFile(String fileentry)
    {
      RTextFileData TextFileData;
      GetTextFileData(out TextFileData);
      TextFileData.Entry = fileentry;
      Boolean Result = true;
      if ((XIH < 0) || (YIH < 0))
      {
        return false;
      }
      CTextFile TextFile = new CTextFile();//GetNotifier());
      //
      try
      {
        TextFileData = new RTextFileData();
        Result &= TextFile.GetData(ref TextFileData);
        String DelimiterValue = TextFileData.TokenDelimiters[0];
        String DelimiterLine = TextFileData.LineDelimiters[0];
        //
        Result &= TextFile.OpenWrite(fileentry);
        //
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          for (Int32 XI = XIL; XI < XIH; XI++)
          {
            TextFile.Write((Double)FValues[YI, XI]);
            TextFile.Write(DelimiterValue);
          }
          TextFile.Write((Double)FValues[YI, XIH]);
          TextFile.Write(DelimiterLine);
        }

        Result &= true;
      }
      catch (Exception)
      {
        Result = false;
      }
      finally
      {
        Result &= TextFile.Close();
      }
      return Result;
    }
    //
    //----------------------------------------
    //	Segment - Mathematic Primitives
    //----------------------------------------
    //
    public Boolean GetColVector(Int32 colindex, out CVectorDouble colvector)
    {
      colvector = null;
      try
      {
        Int32 IL = this.YIL;
        Int32 IH = this.YIH;
        colvector = new CVectorDouble(base.GetNotifier(), this.SizeY);
        for (Int32 RI = IL; RI <= IH; RI++)
        {    // Row Col
          colvector[RI] = FValues[RI, colindex];
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean GetColVector(Int32 colindex, Int32 sizefactor,
                                out CVectorDouble colvector)
    {
      colvector = null;
      try
      {
        Int32 IL = this.YIL;
        Int32 IH = this.YIH;
        colvector = new CVectorDouble(base.GetNotifier(), sizefactor * this.SizeY);
        for (Int32 RI = IL; RI <= IH; RI++)
        {
          for (Int32 FI = 0; FI < sizefactor; FI++)
          {
            colvector[sizefactor * RI + FI] = FValues[RI, colindex];
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean SetColVector(CVectorDouble colvector, Int32 colindex)
    {
      if ((colindex < XIL) || (XIH < colindex))
      {
        return false;
      }
      if (colvector.Size != this.SizeX)
      {
        return false;
      }
      for (Int32 RI = colvector.VIL; RI <= colvector.VIH; RI++)
      {     // Row Col
        this[RI, colindex] = colvector[RI];
      }
      return true;
    }

    public Boolean GetRowVector(Int32 rowindex, out CVectorDouble rowvector)
    {
      rowvector = null;
      try
      {
        Int32 IL = this.XIL;
        Int32 IH = this.XIH;
        rowvector = new CVectorDouble(base.GetNotifier(), this.SizeX);
        for (Int32 CI = IL; CI <= IH; CI++)
        {    // Row Col
          rowvector[CI] = FValues[rowindex, CI];
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean GetRowVector(Int32 rowindex, Int32 sizefactor,
                                out CVectorDouble rowvector)
    {
      rowvector = null;
      try
      {
        Int32 IL = this.XIL;
        Int32 IH = this.XIH;
        rowvector = new CVectorDouble(base.GetNotifier(), sizefactor * this.SizeX);
        for (Int32 CI = IL; CI <= IH; CI++)
        {    
          for (Int32 FI = 0; FI < sizefactor; FI++)
          {
            rowvector[sizefactor * CI + FI] = FValues[rowindex, CI];
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean SetRowVector(CVectorDouble rowvector, Int32 rowindex)
    {
      if ((rowindex < YIL) || (YIH < rowindex))
      {
        return false;
      }
      if (rowvector.Size != this.SizeY)
      {
        return false;
      }
      for (Int32 CI = rowvector.VIL; CI <= rowvector.VIH; CI++)
      {    // Row Col
        this[rowindex, CI] = rowvector[CI];
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Mathematic Functions
    //----------------------------------------
    //
    public Boolean Transpose(out CMatrixDouble result)
    { // exchange y <-> X
      result = new CMatrixDouble(GetNotifier(), SizeY, SizeX);
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          result[XI, YI] = this[YI, XI];
        }
      }
      return true;
    }

    public Boolean Multiply(CMatrixDouble multiplicator, out CMatrixDouble result)
    { // result(Matrix) = this(Matrix) * multiplicator(Matrix)
      result = null;
      // check: ColCount(this) == RowCount(multiplicator)
      if ((this.SizeX == multiplicator.SizeY) &&
          (this.SizeY == multiplicator.SizeX))
      { // Row/ColCount(result) := this.SizeY
        Int32 Size = this.SizeY;
        result = new CMatrixDouble(GetNotifier(), Size, Size);
        for (Int32 YI = result.YIL; YI <= result.YIH; YI++)
        {
          for (Int32 XI = result.XIL; XI <= result.XIH; XI++)
          {
            Double Sum = 0.0;
            for (Int32 SI = this.XIL; SI <= this.XIH; SI++)
            {
              Sum += this[YI, SI] * multiplicator[SI, XI];
            }
            result[YI, XI] = Sum;
          }
        }
        return true;
      }
      return false;
    }

    public Boolean Multiply(CVectorDouble multiplicator, out CVectorDouble result)
    { // result(CVector) = this(CMatrix) * multiplicator(CVector)
      result = null;
      // check: ColCount(this) == Size(multiplicator)
      if (this.SizeX == multiplicator.Size)
      { // Size(result) := this.SizeY
        Int32 Size = this.SizeY;
        result = new CVectorDouble(GetNotifier(), Size);
        for (Int32 VI = result.VIL; VI <= result.VIH; VI++)
        {
          Double Sum = 0.0;
          for (Int32 SI = this.XIL; SI <= this.XIH; SI++)
          {
            Sum += this[VI, SI] * multiplicator[SI];
          }
          result[VI] = Sum;
        }
        return true;
      }
      return false;
    }
    //
    //---------------------------------------------
    //	Segment - Mathematic Functions - HighLevel
    //---------------------------------------------
    //
    public override Boolean AddConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] += (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] += (UInt16)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] += (Double)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean SubtractConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] -= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] -= (UInt16)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] -= (Double)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean MultiplyConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] *= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] *= (UInt16)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] *= (Double)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean DivideConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] /= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] /= (UInt16)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] /= (Double)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Transpose(out CMatrixBase target)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        target = new CMatrixDouble(GetNotifier(), SX, SY);
        for (Int32 YI = 0; YI < SY; YI++)
        {
          for (Int32 XI = 0; XI < SX; XI++)
          {
            target[XI, YI] = FValues[YI, XI];
          }
        }
        return true;
      }
      catch (Exception)
      {
        target = null;
        return false;
      }
    }

    public override Boolean Add(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if ((sourceb.SizeX == SX) && (sourceb.SizeY == SY))
        {
          result = new CMatrixDouble(GetNotifier(), SY, SY);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] + (Byte)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] + (UInt16)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] + (Double)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public override Boolean Add(CMatrixBase source)
    {
      try
      {
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if ((source.SizeX == SX) && (source.SizeY == SY))
        {
          Type TValue = source.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  FValues[YI, XI] += (Byte)source[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  FValues[YI, XI] += (UInt16)source[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  FValues[YI, XI] += (Double)source[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }


    public override Boolean Subtract(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if ((sourceb.SizeX == SX) && (sourceb.SizeY == SY))
        {
          result = new CMatrixDouble(GetNotifier(), SY, SY);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] - (Byte)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] - (UInt16)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] - (Double)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public override Boolean Multiply(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == sourceb.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[m x n] -> Result[l x n]
          // ColCount(A)[this.SizeY x this.SizeX] == RowCount(B)[B.SizeY x B.SizeX] 
          // -> Result[this.SizeY x b.SizeX]
          result = new CMatrixDouble(GetNotifier(), SY, sourceb.SizeX);
          Int32 SXR = result.SizeX;
          Int32 SYR = result.SizeY;
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SYR; YI++)
              {
                for (Int32 XI = 0; XI < SXR; XI++)
                {
                  Double Sum = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Sum += FValues[YI, KI] * (Byte)sourceb[KI, XI];
                  }
                  result[YI, XI] = (Double)Sum;
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SYR; YI++)
              {
                for (Int32 XI = 0; XI < SXR; XI++)
                {
                  Double Sum = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Sum += FValues[YI, KI] * (UInt16)sourceb[KI, XI];
                  }
                  result[YI, XI] = (Double)Sum;
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SYR; YI++)
              {
                for (Int32 XI = 0; XI < SXR; XI++)
                {
                  Double Sum = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Sum += FValues[YI, KI] * (Double)sourceb[KI, XI];
                  }
                  result[YI, XI] = (Double)Sum;
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public Boolean Invert(out CMatrixDouble result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == SY)
        { // MatrixResult -> MatrixUnit
          CMatrixDouble MatrixResult = new CMatrixDouble(GetNotifier(), SY, SX, 0.0);
          for (Int32 II = 0; II < SY; II++)
          {
            MatrixResult.SetValue(II, II, 1.0);
          }
          // MatrixBuffer <- this.Copy
          CMatrixDouble MatrixThis = new CMatrixDouble(GetNotifier(), this);
          if (BuildMainDiagonalUnequalZero(ref MatrixThis, ref MatrixResult))
          { // Gauss-Jordan: MatrixBuffer -> MatrixUnit and MatrixResult -> MatrixInverted
            //!!! MainDiagonalElements must be != Zero!!! -> Presort / Preadd!!!
            if (BuildMainDiagonalOne(ref MatrixThis, ref MatrixResult))
            { // Gauss-Jordan: MatrixBuffer -> MatrixUnit and MatrixResult -> MatrixInverted
              // Multiply, Subtract
              for (Int32 MDI = 0; MDI < SY; MDI++)
              {
                Double Divisor = MatrixThis[MDI, MDI];
                if (1.0 != Divisor)
                {
                  for (Int32 XI = 0; XI < SX; XI++)
                  {
                    MatrixThis[MDI, XI] /= Divisor;
                    MatrixResult[MDI, XI] /= Divisor;
                  }
                }
                //
                for (Int32 YI = 0; YI < MDI; YI++)
                {
                  Double Factor = MatrixThis[YI, MDI];
                  for (Int32 XI = 0; XI < SX; XI++)
                  {
                    Double SummandMain = -Factor * MatrixThis[MDI, XI];
                    MatrixThis[YI, XI] += SummandMain;
                    Double SummandSub = -Factor * MatrixResult[MDI, XI];
                    MatrixResult[YI, XI] += SummandSub;
                  }
                }
                for (Int32 YI = 1 + MDI; YI < SY; YI++)
                {
                  Double Factor = MatrixThis[YI, MDI];
                  for (Int32 XI = 0; XI < SX; XI++)
                  {
                    Double SummandMain = -Factor * MatrixThis[MDI, XI];
                    MatrixThis[YI, XI] += SummandMain;
                    Double SummandSub = -Factor * MatrixResult[MDI, XI];
                    MatrixResult[YI, XI] += SummandSub;
                  }
                }
              }
            }
          }
          //
          result = MatrixResult;
          //
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }



    public static Boolean Invert(CMatrixDouble matrixsource, 
                                 out CMatrixDouble matrixresult)
    {
      try
      {
        matrixresult = null;
        Int32 SY = matrixsource.SizeY;
        Int32 SX = matrixsource.SizeX;
        if (SX == SY)
        { // MatrixResult -> MatrixUnit
          matrixresult = new CMatrixDouble(matrixsource.GetNotifier(), SY, SX, 0.0);
          for (Int32 II = 0; II < SY; II++)
          {
            matrixresult.SetValue(II, II, 1.0);
          }
          // matrixsource...
          if (BuildMainDiagonalUnequalZero(ref matrixsource, ref matrixresult))
          { // Gauss-Jordan: MatrixBuffer -> MatrixUnit and MatrixResult -> MatrixInverted
            //!!! MainDiagonalElements must be != Zero!!! -> Presort / Preadd!!!
            if (BuildMainDiagonalOne(ref matrixsource, ref matrixresult))
            { // Gauss-Jordan: MatrixBuffer -> MatrixUnit and MatrixResult -> MatrixInverted
              // Multiply, Subtract
              for (Int32 MDI = 0; MDI < SY; MDI++)
              {
                Double Divisor = matrixsource[MDI, MDI];
                if (1.0 != Divisor)
                {
                  for (Int32 XI = 0; XI < SX; XI++)
                  {
                    matrixsource[MDI, XI] /= Divisor;
                    matrixresult[MDI, XI] /= Divisor;
                  }
                }
                //
                for (Int32 YI = 0; YI < MDI; YI++)
                {
                  Double Factor = matrixsource[YI, MDI];
                  for (Int32 XI = 0; XI < SX; XI++)
                  {
                    Double SummandMain = -Factor * matrixsource[MDI, XI];
                    matrixsource[YI, XI] += SummandMain;
                    Double SummandSub = -Factor * matrixresult[MDI, XI];
                    matrixresult[YI, XI] += SummandSub;
                  }
                }
                for (Int32 YI = 1 + MDI; YI < SY; YI++)
                {
                  Double Factor = matrixsource[YI, MDI];
                  for (Int32 XI = 0; XI < SX; XI++)
                  {
                    Double SummandMain = -Factor * matrixsource[MDI, XI];
                    matrixsource[YI, XI] += SummandMain;
                    Double SummandSub = -Factor * matrixresult[MDI, XI];
                    matrixresult[YI, XI] += SummandSub;
                  }
                }
              }
            }
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        matrixresult = null;
        return false;
      }
    }

  }
}
