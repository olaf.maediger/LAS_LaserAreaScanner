﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using UCNotifier;
using NLScalar;
using NLVector;
using TextFile;
//
namespace NLMatrix
{
  public class CMatrixByte : CMatrixBase
  { //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //
    private const Byte INIT_ZERO = 0x00;
    private const Byte INIT_ONE = 0x01;
    //
    //----------------------------------------
    //	Segment - Member
    //----------------------------------------
    //
    private Byte[,] FValues = null;
    //---------------------------------------
    // Overloading of Index-Operator
    //---------------------------------------
    /* NC 
    protected override void SetValue(Int32 y, Int32 x, Object value)
    {
      if (CheckIndices(y, x))
      {
        FValues[y, x] = (Byte)value;
      }
    }*/

    // Reading/Writing MainMatrix
    /*
    public new Byte this[Int32 y, Int32 x]
    {
      get { return (Byte)GetValue(y, x); }
      set { SetValue(y, x, value); }
    }
    */

    protected override Object GetValue(Int32 y, Int32 x)
    {
      unsafe
      {
        fixed (Byte* PValues = FValues)
        {
          Byte BValue = *(Byte*)(PValues + y * FColCount + x);
          return (Byte)BValue;
        }
      }
    }
    protected override void SetValue(Int32 y, Int32 x, Object value)
    {
      unsafe
      {
        fixed (Byte* PValues = FValues)
        {
          *(Byte*)(PValues + y * FColCount + x) = (Byte)value;
        }
      }
    }
    public new Byte this[Int32 y, Int32 x]
    {
      get { return (Byte)GetValue(y, x); }
      set { SetValue(y, x, value); }
    }

/*    public new Byte this[Int32 y, Int32 x]
    {
      get 
      {
      }
      set 
      {
      }
    }
    */

    // Reading SolutionVector/Writing InhomogenousVector
    /* to slow!!! NC public Byte this[Int32 y]
    {
      get { return GetValue(y, XIH); }
      set { SetValue(y, XIH, value); }
    }*/

    public IntPtr GetBytePointer()
    {
      unsafe
      {
        fixed (Byte* PByte = FValues)
        {
          return (IntPtr)PByte;
        }
      }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CMatrixByte(CNotifier notifier, Int32 rowcount, Int32 colcount)
      : base(notifier, rowcount, colcount)
    {
      FValues = new Byte[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit8;
      InitValues(INIT_ZERO);
    }

    public CMatrixByte(CNotifier notifier, Int32 rowcount, Int32 colcount, Byte initvalue)
      : base(notifier, rowcount, colcount)
    {
      FValues = new Byte[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit8;
      InitValues(initvalue);
    }

    public CMatrixByte(CNotifier notifier, Byte[,] matrix)
      : base(notifier, matrix.GetLength(0), matrix.GetLength(1))
    {
      FValues = new Byte[matrix.GetLength(0), matrix.GetLength(1)];
      FBitResolution = EBitResolution.MonoBit8;
      InitValues(INIT_ZERO);
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CMatrixByte(Int32 rowcount, Int32 colcount)
      : base(rowcount, colcount)
    {
      FValues = new Byte[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit8;
      InitValues(INIT_ZERO);
    }

    public CMatrixByte(Int32 rowcount, Int32 colcount, Byte initvalue)
      : base(rowcount, colcount)
    {
      FValues = new Byte[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit8;
      InitValues(initvalue);
    }

    public CMatrixByte(Byte[,] matrix)
      : base(matrix.GetLength(0), matrix.GetLength(1))
    {
      FValues = new Byte[matrix.GetLength(0), matrix.GetLength(1)];
      FBitResolution = EBitResolution.MonoBit8;
      InitValues(INIT_ZERO);
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public override Boolean DefineMatrix(Int32 rowcount, Int32 colcount)
    {
      base.DefineMatrix(rowcount, colcount);
      FValues = new Byte[rowcount, colcount];
      return InitValues(INIT_ZERO);
    }

    public Boolean InitValues(Byte initvalue)
    {
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          FValues[YI, XI] = initvalue;
        }
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    protected override void Write(String header)
    {
      base.Write(header);
      if (FNotifier is CNotifier)
      {
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          String Line = String.Format(" RI[{0}]:", YI);
          for (Int32 XI = XIL; XI <= XIH; XI++)
          {
            Line += String.Format(" {0}:{1:X02}", XI, this[YI, XI]);
          }
          GetNotifier().Write(Line);
        }
      }
    }

    protected override void Write(String header,
                                  String formatpositive,
                                  String formatnegative)
    {
      if (FNotifier is CNotifier)
      {
        GetNotifier().Write(header);
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          String Line = String.Format(" RI[{0}]:", YI);
          for (Int32 XI = XIL; XI <= XIH; XI++)
          {
            Double DValue = this[YI, XI];
            String SValue;

            if (0 < DValue)
            {
              SValue = String.Format(formatpositive, DValue);
            }
            else
            {
              SValue = String.Format(formatnegative, DValue);
            }
            Line += String.Format(" {0}:{1}", XI, SValue);
          }
          GetNotifier().Write(Line);
        }
      }
    }
    //
    //----------------------------------------
    //	Segment - Management
    //----------------------------------------
    //
    public override CMatrixBase Clone()
    {
      CMatrixBase Matrix = new CMatrixByte(base.GetNotifier(), SizeY, SizeX);
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          FValues[YI, XI] = this[YI, XI];
        }
      }
      return Matrix;
    }

    public Boolean SetDimension(Int32 sizey, Int32 sizex, Byte initvalue)
    {
      if (base.SetDimension(sizey, sizex))
      {
        FValues = new Byte[sizey, sizex];
        InitValues(initvalue);
        return true;
      }
      return false;
    }

    public Boolean SetConstant(Byte value)
    {
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          this[YI, XI] = value;
        }
      }
      return true;
    }

    public override Boolean SetUnit()
    {
      if (base.SetUnit())
      {
        SetConstant(INIT_ZERO);
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          this[YI, YI] = INIT_ONE;
        }
        return true;
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - File
    //----------------------------------------
    //
    public override Boolean LoadFromTextFile(String fileentry)
    {
      CTextFile TextFile = new CTextFile();//GetNotifier());
      if (TextFile.OpenRead(fileentry))
      {
        Int32 RC = TextFile.RowCount;
        Int32 CC = TextFile.ColCount(0);
        DefineMatrix(RC, CC);
        //
        Boolean Endloop = false;
        while (!Endloop)
        { // Read next Value
          Int32 YI;
          Int32 XI;
          if (TextFile.ReadRowIndex(out YI))
          {
            if (TextFile.ReadColIndex(out XI))
            {
              Byte BValue;
              String SValue;
              if (TextFile.Read(out BValue, out SValue))
              {
                FValues[YI, XI] = BValue;
              }
              else
              {
                Endloop = true;
              }
            }
            else
            {
              Endloop = true;
            }
          }
          else
          {
            Endloop = true;
          }
        }
        //
        TextFile.Close();
        return true;
      }
      TextFile.Close();
      return false;
    }

    public override Boolean SaveToTextFile(String fileentry)
    {
      RTextFileData TextFileData;
      GetTextFileData(out TextFileData);
      TextFileData.Entry = fileentry;
      Boolean Result = true;
      if ((XIH < 0) || (YIH < 0))
      {
        return false;
      }
      CTextFile TextFile = new CTextFile();//GetNotifier());
      //
      try
      {
        TextFileData = new RTextFileData();
        Result &= TextFile.GetData(ref TextFileData);
        String DelimiterValue = TextFileData.TokenDelimiters[0];
        String DelimiterLine = TextFileData.LineDelimiters[0];
        //
        Result &= TextFile.OpenWrite(fileentry);
        //
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          for (Int32 XI = XIL; XI < XIH; XI++)
          {
            TextFile.Write((Byte)FValues[YI, XI]);
            TextFile.Write(DelimiterValue);
          }
          TextFile.Write((Byte)FValues[YI, XIH]);
          TextFile.Write(DelimiterLine);
        }

        Result &= true;
      }
      catch (Exception)
      {
        Result = false;
      }
      finally
      {
        Result &= TextFile.Close();
      }
      return Result;
    }
    //
    //----------------------------------------
    //	Segment - Mathematic Primitives
    //----------------------------------------
    //
    public Boolean SetColVector(CVectorByte colvector, Int32 colindex)
    {
      if ((colindex < XIL) || (XIH < colindex))
      {
        return false;
      }
      if (colvector.Size != this.SizeX)
      {
        return false;
      }
      for (Int32 RI = colvector.VIL; RI <= colvector.VIH; RI++)
      {     // Row Col
        this[RI, colindex] = colvector[RI];
      }
      return true;
    }

    public Boolean SetRowVector(CVectorByte rowvector, Int32 rowindex)
    {
      if ((rowindex < YIL) || (YIH < rowindex))
      {
        return false;
      }
      if (rowvector.Size != this.SizeY)
      {
        return false;
      }
      for (Int32 CI = rowvector.VIL; CI <= rowvector.VIH; CI++)
      {    // Row Col
        this[rowindex, CI] = rowvector[CI];
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Mathematic Functions
    //----------------------------------------
    //
    public Boolean Transpose(out CMatrixByte result)
    { // exchange y <-> X
      result = new CMatrixByte(GetNotifier(), SizeY, SizeX);
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          result[XI, YI] = this[YI, XI];
        }
      }
      return true;
    }

    public Boolean Multiply(CMatrixByte multiplicator, out CMatrixByte result)
    { // result(Matrix) = this(Matrix) * multiplicator(Matrix)
      result = null;
      // check: ColCount(this) == RowCount(multiplicator)
      if ((this.SizeX == multiplicator.SizeY) &&
          (this.SizeY == multiplicator.SizeX))
      { // Row/ColCount(result) := this.SizeY
        Int32 Size = this.SizeY;
        result = new CMatrixByte(GetNotifier(), Size, Size);
        for (Int32 YI = result.YIL; YI <= result.YIH; YI++)
        {
          for (Int32 XI = result.XIL; XI <= result.XIH; XI++)
          {
            Double Sum = 0.0;
            for (Int32 SI = this.XIL; SI <= this.XIH; SI++)
            {
              Sum += this[YI, SI] * multiplicator[SI, XI];
            }
            result[YI, XI] = (Byte)Sum;
          }
        }
        return true;
      }
      return false;
    }

    public Boolean Multiply(CVectorByte multiplicator, out CVectorByte result)
    { // result(CVector) = this(CMatrix) * multiplicator(CVector)
      result = null;
      // check: ColCount(this) == Size(multiplicator)
      if (this.SizeX == multiplicator.Size)
      { // Size(result) := this.SizeY
        Int32 Size = this.SizeY;
        result = new CVectorByte(GetNotifier(), Size);
        for (Int32 VI = result.VIL; VI <= result.VIH; VI++)
        {
          Double Sum = 0.0;
          for (Int32 SI = this.XIL; SI <= this.XIH; SI++)
          {
            Sum += this[VI, SI] * multiplicator[SI];
          }
          result[VI] = (Byte)Sum;
        }
        return true;
      }
      return false;
    }

    public Boolean Normalize()
    {
      // Find ByteMaximum
      Byte ByteMaximum = 0x00;
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          Byte BValue = this[YI, XI];
          if (ByteMaximum < BValue)
          {
            ByteMaximum = BValue;
          }
        }
      }
      // Normalize with ByteMaximum
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          Byte BValue = this[YI, XI];
          BValue = (Byte)(255.0f * (float)(BValue) / (float)ByteMaximum);
          this[YI, XI] = BValue;
        }
      }
      return true;
    }


    public Boolean FindMaximum(out Int32 xindex,
                               out Int32 yindex,
                               out Int32 value)
    {
      value = this[0, 0];
      xindex = 0;
      yindex = 0;
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          Byte BValue = this[YI, XI];
          if (value < BValue)
          {
            value = BValue;
            xindex = XI;
            yindex = YI;
          }
        }
      }
      return true;
    }

    public Boolean FindMinimum(out Int32 xindex,
                               out Int32 yindex,
                               out Int32 value)
    {
      value = this[0, 0];
      xindex = 0;
      yindex = 0;
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          Byte BValue = this[YI, XI];
          if (BValue < value)
          {
            value = BValue;
            xindex = XI;
            yindex = YI;
          }
        }
      }
      return true;
    }

    public Boolean SumOverAll(out Int32 result)
    {
      result = 0;
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          Byte BValue = this[YI, XI];
          result += BValue;
        }
      }
      return true;
    }
    //
    //---------------------------------------------
    //	Segment - Mathematic Functions - HighLevel
    //---------------------------------------------
    //
    public override Boolean AddConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] += (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] += (Byte)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] += (Byte)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean SubtractConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] -= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] -= (Byte)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] -= (Byte)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean MultiplyConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] *= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] *= (Byte)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] *= (Byte)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean DivideConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] /= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] /= (Byte)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] /= (Byte)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Transpose(out CMatrixBase target)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        target = new CMatrixByte(GetNotifier(), SX, SY);
        for (Int32 YI = 0; YI < SY; YI++)
        {
          for (Int32 XI = 0; XI < SX; XI++)
          {
            target[XI, YI] = FValues[YI, XI];
          }
        }
        return true;
      }
      catch (Exception)
      {
        target = null;
        return false;
      }
    }

    public override Boolean Add(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == sourceb.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[l x m] -> Result[l x m]
          result = new CMatrixByte(GetNotifier(), SY, sourceb.SizeX);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] + (Byte)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] + (Byte)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] + (Byte)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public override Boolean Add(CMatrixBase source)
    {
      try
      {
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == source.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[l x m] -> Result[l x m]
          Type TValue = source.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  FValues[YI, XI] += (Byte)source[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  FValues[YI, XI] += (Byte)source[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  FValues[YI, XI] += (Byte)source[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Subtract(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == sourceb.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[l x m] -> Result[l x m]
          result = new CMatrixByte(GetNotifier(), SY, sourceb.SizeX);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] - (Byte)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] - (Byte)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] - (Byte)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public override Boolean Multiply(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == sourceb.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[m x n] -> Result[l x n]
          // ColCount(A)[this.SizeY x this.SizeX] == RowCount(B)[B.SizeY x B.SizeX] 
          // -> Result[this.SizeY x b.SizeX]
          result = new CMatrixByte(GetNotifier(), SY, sourceb.SizeX);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Double Sum = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Sum += FValues[YI, KI] * (Byte)sourceb[KI, XI];
                  }
                  result[YI, XI] = (Byte)Sum;
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Double Sum = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Sum += FValues[YI, KI] * (Byte)sourceb[KI, XI];
                  }
                  result[YI, XI] = (Byte)Sum;
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Double Sum = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Sum += FValues[YI, KI] * (Byte)sourceb[KI, XI];
                  }
                  result[YI, XI] = (Byte)Sum;
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

  }
}
