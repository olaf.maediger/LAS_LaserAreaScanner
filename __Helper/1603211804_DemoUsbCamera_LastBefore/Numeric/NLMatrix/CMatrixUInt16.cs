﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using NLScalar;
using NLVector;
using TextFile;
//
namespace NLMatrix
{
  public class CMatrixUInt16 : CMatrixBase
  { //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //
    private const UInt16 INIT_ZERO = 0x0000;
    private const UInt16 INIT_ONE = 0x0001;
    //
    //----------------------------------------
    //	Segment - Member
    //----------------------------------------
    //
    private UInt16[,] FValues = null;
    //---------------------------------------
    // Overloading of Index-Operator
    //---------------------------------------
    /* NC protected override Object GetValue(Int32 y, Int32 x)
    {
      if (CheckIndices(y, x))
      {
        return FValues[y, x];
      }
      return INIT_ZERO;
    }
    protected override void SetValue(Int32 y, Int32 x, Object value)
    {
      if (CheckIndices(y, x))
      {
        FValues[y, x] = (UInt16)value;
      }
    }

    // Reading/Writing MainMatrix
    protected new UInt16 this[Int32 y, Int32 x]
    { // Pointer!!!!!! see above !!!!!!!!!!!!!!!
      get { return (UInt16)GetValue(y, x); }
      set { SetValue(y, x, value); }
    }*/

    // Reading SolutionVector/Writing InhomogenousVector
    /*protected UInt16 this[Int32 y]
    {
      get { return GetValue(y, XIH); }
      set { SetValue(y, XIH, value); }
    }*/

    protected override Object GetValue(Int32 y, Int32 x)
    {
      unsafe
      {
        fixed (UInt16* PValues = FValues)
        {
          UInt16 BValue = *(UInt16*)(PValues + y * FColCount + x);
          return (UInt16)BValue;
        }
      }
    }
    protected override void SetValue(Int32 y, Int32 x, Object value)
    {
      unsafe
      {
        fixed (UInt16* PValues = FValues)
        {
          UInt16 UValue = Convert.ToUInt16(value);
          *(UInt16*)(PValues + y * FColCount + x) = UValue;
        }
      }
    }   

    public IntPtr GetUInt16Pointer()
    {
      unsafe
      {
        fixed (UInt16* PUInt16 = FValues)
        {
          return (IntPtr)PUInt16;
        }
      }
    }
    public new UInt16 this[Int32 y, Int32 x]
    {
      get { return (UInt16)GetValue(y, x); }
      set { SetValue(y, x, value); }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CMatrixUInt16(CNotifier notifier, Int32 rowcount, Int32 colcount)
      : base(notifier, rowcount, colcount)
    {
      FValues = new UInt16[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit16;
      InitValues(INIT_ZERO);
    }

    public CMatrixUInt16(CNotifier notifier, Int32 rowcount, Int32 colcount, UInt16 initvalue)
      : base(notifier, rowcount, colcount)
    {
      FValues = new UInt16[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit16;
      InitValues(initvalue);
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CMatrixUInt16(Int32 rowcount, Int32 colcount)
      : base(rowcount, colcount)
    {
      FValues = new UInt16[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit16;
      InitValues(INIT_ZERO);
    }

    public CMatrixUInt16(Int32 rowcount, Int32 colcount, UInt16 initvalue)
      : base(rowcount, colcount)
    {
      FValues = new UInt16[rowcount, colcount];
      FBitResolution = EBitResolution.MonoBit16;
      InitValues(initvalue);
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public override Boolean DefineMatrix(Int32 rowcount, Int32 colcount)
    {
      base.DefineMatrix(rowcount, colcount);
      FValues = new UInt16[rowcount, colcount];
      return InitValues(INIT_ZERO);
    }

    public Boolean InitValues(UInt16 initvalue)
    {
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          FValues[YI, XI] = initvalue;
        }
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    protected override void Write(String header)
    {
      base.Write(header);
      if (FNotifier is CNotifier)
      {
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          String Line = String.Format(" RI[{0}]:", YI);
          for (Int32 XI = XIL; XI <= XIH; XI++)
          {
            Line += String.Format(" {0}:{1:0.000000000}", XI, this[YI, XI]);
          }
          GetNotifier().Write(Line);
        }
      }
    }

    protected override void Write(String header,
                                  String formatpositive,
                                  String formatnegative)
    {
      GetNotifier().Write(header);
      if (FNotifier is CNotifier)
      {
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          String Line = String.Format(" RI[{0}]:", YI);
          for (Int32 XI = XIL; XI <= XIH; XI++)
          {
            Double DValue = this[YI, XI];
            String SValue;

            if (0 < DValue)
            {
              SValue = String.Format(formatpositive, DValue);
            }
            else
            {
              SValue = String.Format(formatnegative, DValue);
            }
            Line += String.Format(" {0}:{1}", XI, SValue);
          }
          GetNotifier().Write(Line);
        }
      }
    }
    //
    //----------------------------------------
    //	Segment - Management
    //----------------------------------------
    //
    public override CMatrixBase Clone()
    {
      CMatrixBase Matrix = new CMatrixUInt16(base.GetNotifier(), SizeY, SizeX);
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          FValues[YI, XI] = this[YI, XI];
        }
      }
      return Matrix;
    }

    public Boolean SetDimension(Int32 sizey, Int32 sizex, UInt16 initvalue)
    {
      if (base.SetDimension(sizey, sizex))
      {
        FValues = new UInt16[sizey, sizex];
        InitValues(initvalue);
        return true;
      }
      return false;
    }

    public Boolean SetConstant(UInt16 value)
    {
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          this[YI, XI] = value;
        }
      }
      return true;
    }

    public override Boolean SetUnit()
    {
      if (base.SetUnit())
      {
        SetConstant(INIT_ZERO);
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          this[YI, YI] = INIT_ONE;
        }
        return true;
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - File
    //----------------------------------------
    //
    public override Boolean LoadFromTextFile(String fileentry)
    {
      CTextFile TextFile = new CTextFile();//GetNotifier());
      if (TextFile.OpenRead(fileentry))
      {
        DefineMatrix(TextFile.RowCount, TextFile.ColCount(0));
        //
        Boolean Endloop = false;
        while (!Endloop)
        { // Read next Value
          Int32 YI;
          Int32 XI;
          if (TextFile.ReadRowIndex(out YI))
          {
            if (TextFile.ReadColIndex(out XI))
            {
              UInt16 WValue;
              String SValue;
              if (TextFile.Read(out WValue, out SValue))
              {
                FValues[YI, XI] = WValue;
              }
              else
              {
                Endloop = true;
              }
            }
            else
            {
              Endloop = true;
            }
          }
          else
          {
            Endloop = true;
          }
        }
        //
        TextFile.Close();
        return true;
      }
      TextFile.Close();
      return false;
    }

    public override Boolean SaveToTextFile(String fileentry)
    {
      RTextFileData TextFileData;
      GetTextFileData(out TextFileData);
      TextFileData.Entry = fileentry;
      Boolean Result = true;
      if ((XIH < 0) || (YIH < 0))
      {
        return false;
      }
      CTextFile TextFile = new CTextFile();//GetNotifier());
      //
      try
      {
        TextFileData = new RTextFileData();
        Result &= TextFile.GetData(ref TextFileData);
        String DelimiterValue = TextFileData.TokenDelimiters[0];
        String DelimiterLine = TextFileData.LineDelimiters[0];
        //
        Result &= TextFile.OpenWrite(fileentry);
        //
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          for (Int32 XI = XIL; XI < XIH; XI++)
          {
            TextFile.Write((UInt16)FValues[YI, XI]);
            TextFile.Write(DelimiterValue);
          }
          TextFile.Write((UInt16)FValues[YI, XIH]);
          TextFile.Write(DelimiterLine);
        }

        Result &= true;
      }
      catch (Exception)
      {
        Result = false;
      }
      finally
      {
        Result &= TextFile.Close();
      }
      return Result;
    }
    //
    //----------------------------------------
    //	Segment - Mathematic Primitives
    //----------------------------------------
    //
    public Boolean SetColVector(CVectorUInt16 colvector, Int32 colindex)
    {
      if ((colindex < XIL) || (XIH < colindex))
      {
        return false;
      }
      if (colvector.Size != this.SizeX)
      {
        return false;
      }
      for (Int32 RI = colvector.VIL; RI <= colvector.VIH; RI++)
      {     // Row Col
        this[RI, colindex] = colvector[RI];
      }
      return true;
    }

    public Boolean SetRowVector(CVectorUInt16 rowvector, Int32 rowindex)
    {
      if ((rowindex < YIL) || (YIH < rowindex))
      {
        return false;
      }
      if (rowvector.Size != this.SizeY)
      {
        return false;
      }
      for (Int32 CI = rowvector.VIL; CI <= rowvector.VIH; CI++)
      {    // Row Col
        this[rowindex, CI] = rowvector[CI];
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Mathematic Functions
    //----------------------------------------
    //
    public Boolean Transpose(out CMatrixUInt16 result)
    { // exchange y <-> X
      result = new CMatrixUInt16(GetNotifier(), SizeY, SizeX);
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          result[XI, YI] = this[YI, XI];
        }
      }
      return true;
    }

    public Boolean Multiply(CMatrixUInt16 multiplicator, out CMatrixUInt16 result)
    { // result(Matrix) = this(Matrix) * multiplicator(Matrix)
      result = null;
      // check: ColCount(this) == RowCount(multiplicator)
      if ((this.SizeX == multiplicator.SizeY) &&
          (this.SizeY == multiplicator.SizeX))
      { // Row/ColCount(result) := this.SizeY
        Int32 Size = this.SizeY;
        result = new CMatrixUInt16(GetNotifier(), Size, Size);
        for (Int32 YI = result.YIL; YI <= result.YIH; YI++)
        {
          for (Int32 XI = result.XIL; XI <= result.XIH; XI++)
          {
            UInt16 Sum = INIT_ZERO;
            for (Int32 SI = this.XIL; SI <= this.XIH; SI++)
            {
              Sum += (UInt16)(this[YI, SI] * multiplicator[SI, XI]);
            }
            result[YI, XI] = Sum;
          }
        }
        return true;
      }
      return false;
    }

    public Boolean Multiply(CVectorUInt16 multiplicator, out CVectorUInt16 result)
    { // result(CVector) = this(CMatrix) * multiplicator(CVector)
      result = null;
      // check: ColCount(this) == Size(multiplicator)
      if (this.SizeX == multiplicator.Size)
      { // Size(result) := this.SizeY
        Int32 Size = this.SizeY;
        result = new CVectorUInt16(GetNotifier(), Size);
        for (Int32 VI = result.VIL; VI <= result.VIH; VI++)
        {
          UInt16 Sum = INIT_ZERO;
          for (Int32 SI = this.XIL; SI <= this.XIH; SI++)
          {
            Sum += (UInt16)(this[VI, SI] * multiplicator[SI]);
          }
          result[VI] = Sum;
        }
        return true;
      }
      return false;
    }
    //
    //---------------------------------------------
    //	Segment - Mathematic Functions - HighLevel
    //---------------------------------------------
    //
    public override Boolean AddConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] += (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] += (UInt16)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] += (UInt16)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean SubtractConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] -= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] -= (UInt16)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] -= (UInt16)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean MultiplyConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] *= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] *= (UInt16)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] *= (UInt16)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public override Boolean DivideConstant(Object constant)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        Type TValue = constant.GetType();
        switch (TValue.FullName)
        {
          case "System.Byte":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] /= (Byte)constant;
              }
            }
            return true;
          case "System.UInt16":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] /= (UInt16)constant;
              }
            }
            return true;
          case "System.Double":
            for (Int32 YI = 0; YI < SY; YI++)
            {
              for (Int32 XI = 0; XI < SX; XI++)
              {
                FValues[YI, XI] /= (UInt16)constant;
              }
            }
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Transpose(out CMatrixBase target)
    {
      try
      {
        Int32 SY = SizeY;
        Int32 SX = SizeX;
        target = new CMatrixUInt16(GetNotifier(), SX, SY);
        for (Int32 YI = 0; YI < SY; YI++)
        {
          for (Int32 XI = 0; XI < SX; XI++)
          {
            target[XI, YI] = FValues[YI, XI];
          }
        }
        return true;
      }
      catch (Exception)
      {
        target = null;
        return false;
      }
    }

    public override Boolean Add(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == sourceb.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[l x m] -> Result[l x m]
          result = new CMatrixUInt16(GetNotifier(), SY, sourceb.SizeX);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] + (Byte)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] + (UInt16)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] + (UInt16)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public override Boolean Add(CMatrixBase source)
    {
      try
      {
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == source.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[l x m] -> Result[l x m]
          Type TValue = source.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  FValues[YI, XI] += (Byte)source[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  FValues[YI, XI] += (UInt16)source[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  FValues[YI, XI] += (UInt16)source[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Subtract(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == sourceb.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[l x m] -> Result[l x m]
          result = new CMatrixUInt16(GetNotifier(), SY, sourceb.SizeX);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] - (Byte)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] - (UInt16)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  result[YI, XI] = FValues[YI, XI] - (UInt16)sourceb[YI, XI];
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public override Boolean Multiply(CMatrixBase sourceb, out CMatrixBase result)
    {
      try
      {
        result = null;
        Int32 SY = this.SizeY;
        Int32 SX = this.SizeX;
        if (SX == sourceb.SizeY)
        { // ColCount(A)[l x m] == RowCount(B)[m x n] -> Result[l x n]
          // ColCount(A)[this.SizeY x this.SizeX] == RowCount(B)[B.SizeY x B.SizeX] 
          // -> Result[this.SizeY x b.SizeX]
          result = new CMatrixUInt16(GetNotifier(), SY, sourceb.SizeX);
          Type TValue = sourceb.GetType();
          switch (TValue.FullName)
          {
            case "NLMatrix.CMatrixByte":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Double Sum = 0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Sum += FValues[YI, KI] * (Byte)sourceb[KI, XI];
                  }
                  result[YI, XI] = (UInt16)Sum;
                }
              }
              return true;
            case "NLMatrix.CMatrixUInt16":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Double Sum = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Sum += FValues[YI, KI] * (UInt16)sourceb[KI, XI];
                  }
                  result[YI, XI] = (UInt16)Sum;
                }
              }
              return true;
            case "NLMatrix.CMatrixDouble":
              for (Int32 YI = 0; YI < SY; YI++)
              {
                for (Int32 XI = 0; XI < SX; XI++)
                {
                  Double Sum = 0.0;
                  for (Int32 KI = 0; KI < SX; KI++)
                  {
                    Sum += FValues[YI, KI] * (Double)sourceb[KI, XI];
                  }
                  result[YI, XI] = (UInt16)Sum;
                }
              }
              return true;
            case "NLMatrix.CMatrixTripleByte":
              return false;
          }
        }
        return false;
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }


  }
}
