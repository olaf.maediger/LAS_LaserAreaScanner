﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using NLScalar;
using NLVector;
using TextFile;
//
namespace NLMatrix
{
  public enum EBitResolution
  {
    MonoBit2,
    MonoBit4,
    MonoBit8,   // Byte
    MonoBit10,
    MonoBit12,
    MonoBit14,
    MonoBit16,
    MonoBit18,
    MonoBit20,
    MonoBit24,  
    MonoBit64,  // Double
    TripleBit8  // TripleByte
  }

  public abstract class CMatrixBase
  { //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //

    //
    //----------------------------------------
    //	Segment - Member
    //----------------------------------------
    //
    protected CNotifier FNotifier;
    protected Int32 FColCount;
    protected Int32 FRowCount;
    private RTextFileData FTextFileData;
    protected EBitResolution FBitResolution;
    //
    //----------------------------------------
    //	Segment - Property
    //----------------------------------------
    //
    protected CNotifier GetNotifier()
    {
      return FNotifier;
    }

    public Int32 XIL
    {
      get { return 0; }
    }
    public Int32 XIH
    {
      get { return FColCount - 1; }
    }
    public Int32 SizeX
    {
      get { return FColCount; }
    }

    public Int32 YIL
    {
      get { return 0; }
    }
    public Int32 YIH
    {
      get { return FRowCount - 1; }
    }
    public Int32 SizeY
    {
      get { return FRowCount; }
    }

    // NC protected abstract Object GetValue(Int32 y, Int32 x);
    // NC protected abstract void SetValue(Int32 y, Int32 x, Object value);
    // Reading/Writing MainMatrix
    // NC public Object this[Int32 y, Int32 x]
    // NC {
      // NC get { return GetValue(y, x); }
      // NC set { SetValue(y, x, value); }
    // NC }

    protected abstract Object GetValue(Int32 y, Int32 x);
    protected abstract void SetValue(Int32 y, Int32 x, Object value);
    public Object this[Int32 y, Int32 x]
    {
      get { return GetValue(y, x); }
      set { SetValue(y, x, value); }
    }


    public EBitResolution BitResolution
    {
      get { return FBitResolution; }
      set { FBitResolution = value; }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CMatrixBase(CNotifier notifier, Int32 rowcount, Int32 colcount)
    {
      FNotifier = notifier;
      FRowCount = Math.Max(1, rowcount);
      FColCount = Math.Max(1, colcount);
    }
    
    public CMatrixBase(CNotifier notifier, Int32 rowcount, Int32 colcount, Double initvalue)
		{
      FNotifier = notifier;
      FRowCount = Math.Max(1, rowcount);
			FColCount = Math.Max(1, colcount);
		}
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CMatrixBase(Int32 rowcount, Int32 colcount)
    {
      FNotifier = null;
      FRowCount = Math.Max(1, rowcount);
      FColCount = Math.Max(1, colcount);
    }

    public CMatrixBase(Int32 rowcount, Int32 colcount, Double initvalue)
    {
      FNotifier = null;
      FRowCount = Math.Max(1, rowcount);
      FColCount = Math.Max(1, colcount);
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public virtual Boolean DefineMatrix(Int32 rowcount, Int32 colcount)
    {
      FRowCount = Math.Max(1, rowcount);
      FColCount = Math.Max(1, colcount);
      return true;
    }

    public abstract CMatrixBase Clone();
    //
    //----------------------------------------
    //	Segment - Helper
    //----------------------------------------
    //
    protected Boolean CheckIndexY(Int32 yindex)
    {
      return ((YIL <= yindex) & (yindex <= YIH));
    }

    protected Boolean CheckIndexX(Int32 xindex)
    {
      return ((XIL <= xindex) & (xindex <= XIH));
    }

    protected Boolean CheckIndices(Int32 yindex, Int32 xindex)
    {
      return (CheckIndexY(yindex) & CheckIndexX(xindex));
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    protected virtual void Write(String header)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}, {2}]:", header, FRowCount, FColCount));
      }
    }

    protected virtual void Write(String header,
                                 String formatpositive, 
                                 String formatnegative)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}, {2}]:", header, SizeY, SizeX));
      }
    }

    public virtual void Write(CNotifier notifier,
                              String header,
                              String format)
    {
      if (notifier is CNotifier)
      {
        notifier.Write(String.Format("*** {0}[RC:{1} | CC:{2}]:", header, FRowCount, FColCount));
        for (Int32 YI = this.YIL; YI <= this.YIH; YI++)
        {
          String Line = String.Format(" [{0}, {1}..{2}]:", YI, XIL, XIH);
          for (Int32 XI = this.XIL; XI <= this.XIH; XI++)
          {
            Double DValue = (Double)this[YI, XI];
            if (0.0 <= DValue)
            {
              Line += String.Format(" +" + format, DValue);
            }
            else
            {
              Line += String.Format(" " + format, DValue);
            }
          }
          notifier.Write(Line);
        }
      }
    }
    //
    //----------------------------------------
    //	Segment - Get/SetData
    //----------------------------------------
    //
    public Boolean GetTextFileData(out RTextFileData data)
    {
      data = FTextFileData;
      return true;
    }

    public Boolean SetTextFileData(RTextFileData data)
    {
      FTextFileData = data;
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Management
    //----------------------------------------
    //
    public Boolean SetDimension(Int32 sizey, Int32 sizex)
    {
      if ((0 < sizey) && (0 < sizex))
      {
        FRowCount = sizey;
        FColCount = sizex;
        return true;
      }
      return false;
    }

    public virtual Boolean SetUnit()
    {
      return (SizeY == SizeX);
    }
    //
    //----------------------------------------
    //	Segment - File
    //----------------------------------------
    //
    public abstract Boolean LoadFromTextFile(String fileentry);
    public abstract Boolean SaveToTextFile(String fileentry);
    //
    //---------------------------------------------
    //	Segment - Mathematic Functions - LowLevel
    //---------------------------------------------
    //
    public virtual Byte[,] GetByteArray()
    {
      Byte[,] Result = new Byte[SizeY, SizeX];
      Int32 SY = SizeY;
      Int32 SX = SizeX;
      for (Int32 YI = 0; YI < SY; YI++)
      {
        for (Int32 XI = 0; XI < SX; XI++)
        {
          Result[YI, XI] = (Byte)this[YI, XI];
        }
      }
      return Result;
    }
    public virtual UInt16[,] GetUInt16Array()
    {
      UInt16[,] Result = new UInt16[SizeY, SizeX];
      Int32 SY = SizeY;
      Int32 SX = SizeX;
      for (Int32 YI = 0; YI < SY; YI++)
      {
        for (Int32 XI = 0; XI < SX; XI++)
        {
          Result[YI, XI] = (UInt16)this[YI, XI];
        }
      }
      return Result;
    }
    public virtual Double[,] GetDoubleArray()
    {
      Double[,] Result = new Double[SizeY, SizeX];
      Int32 SY = SizeY;
      Int32 SX = SizeX;
      for (Int32 YI = 0; YI < SY; YI++)
      {
        for (Int32 XI = 0; XI < SX; XI++)
        {
          Result[YI, XI] = (Double)this[YI, XI];
        }
      }
      return Result;
    }

    public virtual Boolean FindMinimumMaximum(out Double minimum, out Double maximum)
    {
      minimum = 1E20;
      maximum = -1E20;
      Int32 SY = SizeY;
      Int32 SX = SizeX;
      for (Int32 YI = 0; YI < SY; YI++)
      {
        for (Int32 XI = 0; XI < SX; XI++)
        {
          Double DValue = (Double)this[YI, XI];
          if (DValue < minimum)
          {
            minimum= DValue;
          }
          else
            if (maximum < DValue)
            {
              maximum = DValue;
            }
        }
      }
      return true;
    }

    public virtual Boolean FindMinimumMaximumAbsolute(out Double minimum, out Double maximum)
    {
      minimum = +1E20;
      maximum = -1E20;
      Int32 SY = SizeY;
      Int32 SX = SizeX;
      for (Int32 YI = 0; YI < SY; YI++)
      {
        for (Int32 XI = 0; XI < SX; XI++)
        {
          Double DValue = Math.Abs((Double)this[YI, XI]);
          if (DValue < minimum)
          {
            minimum = DValue;
          }
          else
            if (maximum < DValue)
            {
              maximum = DValue;
            }
        }
      }
      return true;
    }
    //
    //---------------------------------------------
    //	Segment - Mathematic Functions - HighLevel
    //---------------------------------------------
    //
    public abstract Boolean AddConstant(Object constant);
    public abstract Boolean SubtractConstant(Object constant);
    public abstract Boolean MultiplyConstant(Object constant);
    public abstract Boolean DivideConstant(Object constant);

    public abstract Boolean Transpose(out CMatrixBase target);
    public abstract Boolean Add(CMatrixBase sourceb, out CMatrixBase result);
    public abstract Boolean Subtract(CMatrixBase sourceb, out CMatrixBase result);
    public abstract Boolean Multiply(CMatrixBase sourceb, out CMatrixBase result);

    public abstract Boolean Add(CMatrixBase source);
    
  }
}
