﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NLRealPixel
{ //
  //---------------------------------------------------------------------------
  //  Segment - Theory
  //---------------------------------------------------------------------------
  //
  // (xp - xpl) / (xph - xpl) = (xr - xrl) / (xrh - xrl)
  // (yp - yph) / (ypl - yph) = (yr - yrl) / (yrh - yrl)
  // 
  // Pixel -> Real
  //------------------
  // X : Pixel -> Real
  // xr = xrl + (xp - xpl) / (xph - xpl) * (xrh - xrl)
  // dxr = dxp / (xph - xpl) * (xrh - xrl)
  //
  // Y : Pixel -> Real
  // yr = yrl + (yp - yph) / (ypl - yph) * (yrh - yrl)
  // dyr = dyp / (yph - ypl) * (yrh - yrl)
  //
  // Real -> Pixel
  //------------------
  // X : Real -> Pixel
  // xp = xpl + (xr - xrl) / (xrh - xrl) * (xph - xpl)
  // dxp = dxr / (xrh - xrl) * (xph - xpl)
  //
  // Y : Real -> Pixel
  // yp = yph + (yr - yrl) / (yrh - yrl) * (ypl - yph)
  // dyp = dyr / (yrh - yrl) * (ypl - yph)
  //
  //
  public enum EUnitScale
  {
    None,
    MM, // default
    UM,
    NM
  }

  public delegate void DOnUnitScaleChanged(EUnitScale unitscale);
  public delegate void DOnRealExtremaChanged(Double xminimum, Double yminimum,
                                             Double xmaximum, Double ymaximum);
  public delegate void DOnRealResolutionChanged(Int32 stepcountx, Int32 stepcounty);
  //
  public class CRealPixel
  { //
    //---------------------------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------------------------
    //
    public const EUnitScale INIT_UNITSCALE = EUnitScale.MM;
    //
    public const Double INIT_XREALMINIMUM = 0.0;
    public const Double INIT_XREALMAXIMUM = +10.0;
    public const Double INIT_YREALMINIMUM = 0.0;
    public const Double INIT_YREALMAXIMUM = +10.0;
    //
    public const Int32 INIT_XPIXELMINIMUM = 0;
    public const Int32 INIT_XPIXELMAXIMUM = +100;
    public const Int32 INIT_YPIXELMINIMUM = 0;
    public const Int32 INIT_YPIXELMAXIMUM = +100;
    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    // Real
    private EUnitScale FUnitScale;
    private Double FXRL, FXRH;
    private Double FYRL, FYRH;
    // Pixel
    private Int32 FXPL, FXPH;
    private Int32 FYPL, FYPH;
    //
    private DOnUnitScaleChanged FOnUnitScaleChanged;
    private DOnRealExtremaChanged FOnRealExtremaChanged;
    private DOnRealResolutionChanged FOnRealResolutionChanged;
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CRealPixel()
    {
      Init();
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    public void SetOnUnitScaleChanged(DOnUnitScaleChanged value)
    {
      FOnUnitScaleChanged = value;
    }

    public void SetOnRealExtremaChanged(DOnRealExtremaChanged value)
    {
      FOnRealExtremaChanged = value;
    }

    public void SetOnRealResolutionChanged(DOnRealResolutionChanged value)
    {
      FOnRealResolutionChanged = value;
    }



    public EUnitScale UnitScale
    {
      get { return FUnitScale; }
      set
      {
        if (value != FUnitScale)
        {
          FUnitScale = value;
          if (FOnUnitScaleChanged is DOnUnitScaleChanged)
          {
            FOnUnitScaleChanged(FUnitScale);
          }
        }
      }
    }

    public void SetPixelExtrema(Int32 width, Int32 height)
    {
      if ((width != FXPH) || (height != FYPH))
      {
        FXPH = width;
        FYPH = height;
        BuildRealExtremaY();
        if (FOnRealExtremaChanged is DOnRealExtremaChanged)
        {
          FOnRealExtremaChanged(FXRL, FYRL, FXRH, FYRH);
        }
      }
    }

    public void SetRealExtrema(Double xrl, Double yrl,
                               Double xrh, Double yrh)
    {
      FXRL = xrl;
      FYRL = yrl;
      FXRH = xrh;
      FYRH = yrh;
      if (FOnRealExtremaChanged is DOnRealExtremaChanged)
      {
        FOnRealExtremaChanged(FXRL, FYRL, FXRH, FYRH);
      }
    }

    public void SetRealExtrema(Double xrl, Double yrl,
                               Double dxr)
    {
      FXRL = xrl;
      FYRL = yrl;
      FXRH = xrl + dxr;
      BuildRealExtremaY();
      if (FOnRealExtremaChanged is DOnRealExtremaChanged)
      {
        FOnRealExtremaChanged(FXRL, FYRL, FXRH, FYRH);
      }
    }

    public void SetRealMinimum(Double xrl, Double yrl)
    {
      FXRL = xrl;
      FYRL = yrl;
      if (FOnRealExtremaChanged is DOnRealExtremaChanged)
      {
        FOnRealExtremaChanged(FXRL, FYRL, FXRH, FYRH);
      }
    }
    public void SetRealMaximum(Double xrh, Double yrh)
    {
      FXRH = xrh;
      FYRH = yrh;
      if (FOnRealExtremaChanged is DOnRealExtremaChanged)
      {
        FOnRealExtremaChanged(FXRL, FYRL, FXRH, FYRH);
      }
    }

    public Double XRealMinimum
    {
      get { return FXRL; }
    }
    public Double XRealMaximum
    {
      get { return FXRH; }
    }

    public Double YRealMinimum
    {
      get { return FYRL; }
    }
    public Double YRealMaximum
    {
      get { return FYRH; }
    }

    public Double DXReal
    {
      get { return FXRH - FXRL; }
    }
    public Double DYReal
    {
      get { return FYRH - FYRL; }
    }


    public Int32 XPixelMinimum
    {
      get { return FXPL; }
    }
    public Int32 XPixelMaximum
    {
      get { return FXPH; }
    }

    public Int32 YPixelMinimum
    {
      get { return FYPL; }
    }
    public Int32 YPixelMaximum
    {
      get { return FYPH; }
    }

    public Int32 XPixelSize
    {
      get { return FXPH - FXPL; }
    }
    public Int32 YPixelSize
    {
      get { return FYPH - FYPL; }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------------------
    //
    public static String UnitScaleText(EUnitScale value)
    {
      switch (value)
      {
        case EUnitScale.MM:
          return "mm";
        case EUnitScale.UM:
          return "um";
        case EUnitScale.NM:
          return "nm";
        default:
          return "None";
      }
    }

    public static EUnitScale TextUnitScale(String value)
    {
      switch (value.ToLower())
      {
        case "mm":
          return EUnitScale.MM;
        case "um":
          return EUnitScale.UM;
        case "nm":
          return EUnitScale.NM;
        default: //"NONE"
          return EUnitScale.None;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Init
    //---------------------------------------------------------------------------
    //
    private void Init()
    {
      FUnitScale = INIT_UNITSCALE;
      //
      FXRL = INIT_XREALMINIMUM;
      FXRH = INIT_XREALMAXIMUM;
      FYRL = INIT_YREALMINIMUM;
      FYRH = INIT_YREALMAXIMUM;
      //
      FXPL = INIT_XPIXELMINIMUM;
      FXPH = INIT_XPIXELMAXIMUM;
      FYPL = INIT_YPIXELMINIMUM;
      FYPH = INIT_YPIXELMAXIMUM;
    }

    private void BuildRealExtremaY()
    { // RX0, RX1, RY0 -> RY1 (PX0, PY0, PX1, PY1)
      Double FYX = (Double)(FYPH - FYPL) /
                   (Double)(FXPH - FXPL);
      Double DY = FYX * (FXRH - FXRL);
      FYRH = FYRL + DY;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    // X : Real -> Pixel
    public Int32 XRealPixel(Double xr)
    {
      return Convert.ToInt32(FXPL + (xr - FXRL) * (FXPH - FXPL) / (FXRH - FXRL));
    }
    public Int32 DXRealPixel(Double dxr)
    {
      return Convert.ToInt32(dxr * (FXPH - FXPL) / (FXRH - FXRL));
    }
    // Y : Real -> Pixel
    public Int32 YRealPixel(Double yr)
    {
      return Convert.ToInt32(FYPH + (yr - FYRL) * (FYPL - FYPH) / (FYRH - FYRL));
    }
    public Int32 DYRealPixel(Double dyr)
    {
      return Convert.ToInt32(dyr * (FYPL - FYPH) / (FYRH - FYRL));
    }
    // X : Pixel -> Real
    public Double XPixelReal(Int32 xp)
    {
      return Convert.ToDouble(FXRL + (xp - FXPL) * (FXRH - FXRL) / (FXPH - FXPL));
    }
    public Double DXPixelReal(Int32 dxp)
    {
      return Convert.ToDouble(dxp * (FXRH - FXRL) / (FXPH - FXPL));
    }
    // Y : Pixel -> Real
    public Double YPixelReal(Int32 yp)
    {
      return Convert.ToDouble(FYRL + (yp - FYPH) * (FYRH - FYRL) / (FYPL - FYPH));
    }
    public Double DYPixelReal(Int32 dyp)
    {
      return Convert.ToDouble(dyp * (FYRH - FYRL) / (FYPL - FYPH));
    }

  }
}
