﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace NLGraphicElement
{
  public class CElementList : List<CElement>
  {
    //
    //----------------------------------------------------------------
    //  Segment - 
    //----------------------------------------------------------------
    //

    //
    //----------------------------------------------------------------
    //  Segment - Helper
    //----------------------------------------------------------------
    //

    //
    //----------------------------------------------------------------
    //  Segment - Public Management
    //----------------------------------------------------------------
    //
    public CElement Find(Guid id)
    {
      foreach (CElement Element in this)
      {
        if (id == Element.ID)
        {
          return Element;
        }
      }
      return null;
    }

    //public void Show()
    //{
    //  foreach (CElement Element in this)
    //  {
    //    Element.Show = true;
    //  }
    //}
    //public Boolean Show(Guid id)
    //{
    //  CElement Element = Find(id);
    //  if (Element is CElement)
    //  {
    //    Element.Show = true;
    //    return true;
    //  }
    //  return false;
    //}

    //public void Hide()
    //{
    //  foreach (CElement Element in this)
    //  {
    //    Element.Show = false;
    //  }
    //}
    //public Boolean Hide(Guid id)
    //{
    //  CElement Element = Find(id);
    //  if (Element is CElement)
    //  {
    //    Element.Show = false;
    //    return true;
    //  }
    //  return false;
    //}

    public Boolean BuildRealVector()
    {
      Boolean Result = true;
      foreach (CElement Element in this)
      {
        Result &= Element.BuildRealVector();
      }
      return Result;
    }

    public Boolean BuildPixelVector()
    {
      Boolean Result = true;
      foreach (CElement Element in this)
      {
        Result &= Element.BuildPixelVector();
      }
      return Result;
    }

    public Boolean Draw(Graphics graphics)
    {
      Boolean Result = true;
      foreach (CElement Element in this)
      {
        Result &= Element.Draw(graphics);
      }
      return Result;
    }

  }
}
