﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLGraphicElement
{ // *** Text *** 
  //
  public enum EAlignmentHorizontal 
  {
    Left,
    Center,
    Right
  };

  public enum EAlignmentVertical 
  {
    Top,
    Center,
    Bottom
  };

  public class CTextRow : CElement
  { //
    //----------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------
    private const Int32 SIZE_VECTOR = 2;
    private const Int32 INDEX_ORIGIN = 0;
    private const Int32 INDEX_SIZE = 1;
    // Font
    private const String INIT_FONTNAME = "Arial";
    private const float INIT_FONTSIZE = 10;
    private const FontStyle INIT_FONTSTYLE = FontStyle.Regular;
    private const String INIT_TEXT = "undefined";
    private const Double INIT_ORIGINX = 10.0; // [%]
    private const Double INIT_ORIGINY = 10.0; // [%]
    private const Double INIT_SIZEX = 80.0; // [%]
    private const Double INIT_SIZEY = 80.0; // [%]
    // Background
    private const Boolean INIT_SHOWBACKGROUND = true;
    private const Double INIT_SCALEFACTORX = 100.0; // [%]
    private const Double INIT_SCALEFACTORY = 100.0; // [%]
    private const float INIT_CORRECTIONTEXT_DY = 6f;  // [%]
    // Color...
    // Frame
    private const Boolean INIT_SHOWFRAME = true;
    // Color...
    private const Int32 INIT_LINEWIDTHFRAME = 1;
    //
    //----------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------
    // Common
    // Text
    protected Boolean FShowText;
    protected Font FFont;
    protected Color FColorFont;
    protected String FText;
    // Background
    protected Boolean FShowBackground;
    protected Color FColorBackground;
    protected Double FScaleFactorX, FScaleFactorY;
    // Frame
    protected Boolean FShowFrame;
    protected Color FColorFrame;
    protected Int32 FLineWidthFrame;
    //
    //----------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------
    //
    public CTextRow(CRealPixel realpixel)
      : base(realpixel)
    { // Font
      FFont = new Font(INIT_FONTNAME, INIT_FONTSIZE, INIT_FONTSTYLE);
      FColorFont = Color.Black;
      FText = INIT_TEXT;
      FRealX = new Double[SIZE_VECTOR];
      FRealY = new Double[SIZE_VECTOR];
      FRealX[INDEX_ORIGIN] = INIT_ORIGINX;
      FRealY[INDEX_ORIGIN] = INIT_ORIGINY;
      FRealX[INDEX_SIZE] = INIT_SIZEX;
      FRealY[INDEX_SIZE] = INIT_SIZEY;
      // Background
      FShowBackground = INIT_SHOWBACKGROUND;
      FColorBackground = Color.White;
      FScaleFactorX = INIT_SCALEFACTORX;
      FScaleFactorY = INIT_SCALEFACTORY;
      // Frame
      FShowFrame = INIT_SHOWFRAME;
      FColorFrame = Color.Black;
      FLineWidthFrame = INIT_LINEWIDTHFRAME;
      //
      BuildRealVector();
      BuildPixelVector();
    }

    public CTextRow(CRealPixel realpixel,
                    Double originx, Double originy,
                    Double sizex, Double sizey)
      : base(realpixel)
    { // Font
      FFont = new Font(INIT_FONTNAME, INIT_FONTSIZE, INIT_FONTSTYLE);
      FColorFont = Color.Black;
      FText = INIT_TEXT;
      FRealX = new Double[SIZE_VECTOR];
      FRealY = new Double[SIZE_VECTOR];
      FRealX[INDEX_ORIGIN] = originx;
      FRealY[INDEX_ORIGIN] = originy;
      FRealX[INDEX_SIZE] = INIT_SIZEX;
      FRealY[INDEX_SIZE] = INIT_SIZEY;
      // Background
      FShowBackground = INIT_SHOWBACKGROUND;
      FColorBackground = Color.White;
      FScaleFactorX = INIT_SCALEFACTORX;
      FScaleFactorY = INIT_SCALEFACTORY;
      // Frame
      FShowFrame = INIT_SHOWFRAME;
      FColorFrame = Color.Black;
      FLineWidthFrame = INIT_LINEWIDTHFRAME;
      //
      BuildRealVector();
      BuildPixelVector();
    }
    //
    //----------------------------------------------------------------
    //  Segment - Property - Common
    //----------------------------------------------------------------
    // 
    public void SetShowText(Boolean value)
    {
      FShowText = value; 
    }
    //
    //----------------------------------------------------------------
    //  Segment - Property - Text
    //----------------------------------------------------------------
    // 
    public void SetText(String text)
    {
      FText = text;
      BuildRealVector();
      BuildPixelVector();
    }

    public void SetOrigin(Double originx, Double originy) // [%]
    {
      FRealX[INDEX_ORIGIN] = originx;
      FRealY[INDEX_ORIGIN] = originy;
      BuildRealVector();
      BuildPixelVector();
    }

    public void SetSize(Double sizex, Double sizey) // [%]
    {
      FRealX[INDEX_SIZE] = sizex;
      FRealY[INDEX_SIZE] = sizey;
      BuildRealVector();
      BuildPixelVector();
    }

    public void SetFont(Font font)
    {
      FFont = font;
      BuildRealVector();
      BuildPixelVector();
    }
    //
    //----------------------------------------------------------------
    //  Segment - Property - Background
    //----------------------------------------------------------------
    // 
    public Boolean ShowBackground
    {
      get { return FShowBackground; }
      set 
      {
        FShowBackground = value;
        BuildRealVector();
        BuildPixelVector();
      }
    }

    public void SetColorBackground(Color color)
    {
      FColorBackground = color;
      BuildRealVector();
      BuildPixelVector();
    }

    public void SetColorText(Color color)
    {
      FColorFont = color;
      BuildRealVector();
      BuildPixelVector();
    }

    public void SetScaleFactor(Double scalefactorx, Double scalefactory) // [%]
    {
      FScaleFactorX = scalefactorx;
      FScaleFactorY = scalefactory;
      BuildRealVector();
      BuildPixelVector();
    }
    //
    //----------------------------------------------------------------
    //  Segment - Property - Frame
    //----------------------------------------------------------------
    //     
    public Boolean ShowFrame
    {
      get { return FShowFrame; }
      set 
      { 
        FShowFrame = value;
        BuildRealVector();
        BuildPixelVector();
      }
    }

    public Int32 LineWidthFrame
    {
      get { return FLineWidthFrame; }
      set { FLineWidthFrame = value; }
    }

    public void SetColorFrame(Color color)
    {
      FColorFrame = color;
      BuildRealVector();
      BuildPixelVector();
    }
    //
    //----------------------------------------------------------------
    //  Segment - Helper
    //----------------------------------------------------------------
    //
    public override Boolean BuildRealVector()
    {
      try
      {
        // nothing to do - no real coordinates!
        //Double DXR = FRealPixel.XRealMaximum - FRealPixel.XRealMinimum;        
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildPixelVector()
    {
      try
      { // Childs:
        Boolean Result = FElementList.BuildPixelVector();
        //
        FPixelX = new Int32[SIZE_VECTOR];
        FPixelY = new Int32[SIZE_VECTOR];
        FPixelX[INDEX_ORIGIN] = (Int32)(FRealPixel.XPixelMinimum + FRealX[INDEX_ORIGIN] * FRealPixel.XPixelSize / 100.0);
        FPixelY[INDEX_ORIGIN] = (Int32)(FRealPixel.YPixelMinimum + FRealY[INDEX_ORIGIN] * FRealPixel.YPixelSize / 100.0);
        FPixelX[INDEX_SIZE] = (Int32)(FRealPixel.XPixelMinimum + FRealX[INDEX_SIZE] * FRealPixel.XPixelSize / 100.0);
        FPixelY[INDEX_SIZE] = (Int32)(FRealPixel.YPixelMinimum + FRealY[INDEX_SIZE] * FRealPixel.YPixelSize / 100.0);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      Brush B;
      Rectangle RI, RSF;
      if (FShowText) // identical to FShowText!
      { // Parameter
        Point PO = new Point(FPixelX[INDEX_ORIGIN], FPixelY[INDEX_ORIGIN]);
        SizeF MS = graphics.MeasureString(FText, FFont);
        Int32 SFDX = (Int32)((FScaleFactorX - 100f) * MS.Width / 100f);
        Int32 SFDY = (Int32)((FScaleFactorY - 100f) * MS.Height / 100f);
        RSF = new Rectangle(FPixelX[INDEX_ORIGIN] - SFDX, FPixelY[INDEX_ORIGIN] - SFDY,
                            FPixelX[INDEX_SIZE] + SFDX + SFDX, FPixelY[INDEX_SIZE] + SFDY + SFDY);
        float FontSize = FFont.Size * (float)FPixelX[INDEX_SIZE] / MS.Width;
        Font FontTarget = new Font(FFont.Name, FontSize, FFont.Style);
        // Background
        if (FShowBackground)
        {
          B = new SolidBrush(FColorBackground);
          graphics.FillRectangle(B, RSF);
          B.Dispose();
        }
        // Text
        B = new SolidBrush(FColorFont);
        StringFormat SF = new StringFormat();
        SF.Alignment = StringAlignment.Center;
        SF.LineAlignment = StringAlignment.Center;
        Int32 CDY = (Int32)(INIT_CORRECTIONTEXT_DY * FPixelY[INDEX_SIZE] / 100f);
        RI = new Rectangle(FPixelX[INDEX_ORIGIN], FPixelY[INDEX_ORIGIN] + CDY,
                           FPixelX[INDEX_SIZE], FPixelY[INDEX_SIZE]);
        graphics.DrawString(FText, FontTarget, B, RI, SF);
        //
        // Frame
        if (FShowFrame)
        {
          Pen P = new Pen(FColorFrame, FLineWidthFrame);
          graphics.DrawRectangle(P, RSF);
          P.Dispose();
        }
        //
        FontTarget.Dispose();
        SF.Dispose();
        B.Dispose();
      }
      return true;
    }


  }
}

