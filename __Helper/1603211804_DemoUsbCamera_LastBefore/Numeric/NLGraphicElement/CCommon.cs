﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
//
using NLRealPixel;
//
namespace NLGraphicElement
{
  public static class CCommon
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Calculation
    //--------------------------------------------------------------------------------------------
    //
    ////public static void BuildColorTextRGB(Color backcolor)
    //{
    //  const Byte COLOR_DIFFERENCE = 0x80;
    //  lblColorText.BackColor = backcolor;
    //  Byte BCR = backcolor.R;
    //  Byte BCG = backcolor.G;
    //  Byte BCB = backcolor.B;
    //  Byte CCR = (Byte)(0xFF - BCR);
    //  Byte CCG = (Byte)(0xFF - BCG);
    //  Byte CCB = (Byte)(0xFF - BCB);
    //  Byte DCR = (Byte)Math.Abs(BCR - CCR);
    //  Byte DCG = (Byte)Math.Abs(BCG - CCG);
    //  Byte DCB = (Byte)Math.Abs(BCB - CCB);
    //  if ((DCR <= COLOR_DIFFERENCE) && (DCG < COLOR_DIFFERENCE) && (DCB < COLOR_DIFFERENCE))
    //  { // Difference to slow -> more Contrast
    //    CCR = (Byte)(0xFF - DCR);
    //    CCG = (Byte)(0x00 + DCG);
    //    CCB = (Byte)(0xFF - DCB);
    //  }
    //  lblColorText.ForeColor = Color.FromArgb(CCR, CCG, CCB);
    //  lblColorText.Text = String.Format("{0:X2} {1:X2} {2:X2}",
    //                                     backcolor.R, backcolor.G, backcolor.B);
    //}
    public static Color BuildColorComplementary(Color color)
    {
      const Byte COLOR_DIFFERENCE = 0x80;
      Byte BCR = color.R;
      Byte BCG = color.G;
      Byte BCB = color.B;
      Byte CCR = (Byte)(0xFF - BCR);
      Byte CCG = (Byte)(0xFF - BCG);
      Byte CCB = (Byte)(0xFF - BCB);
      Byte DCR = (Byte)Math.Abs(BCR - CCR);
      Byte DCG = (Byte)Math.Abs(BCG - CCG);
      Byte DCB = (Byte)Math.Abs(BCB - CCB);
      if ((DCR <= COLOR_DIFFERENCE) && (DCG < COLOR_DIFFERENCE) && (DCB < COLOR_DIFFERENCE))
      { // Difference to slow -> more Contrast
        CCR = (Byte)(0xFF - DCR);
        CCG = (Byte)(0x00 + DCG);
        CCB = (Byte)(0xFF - DCB);
      }
      return Color.FromArgb(CCR, CCG, CCB);
    }

  }
}
