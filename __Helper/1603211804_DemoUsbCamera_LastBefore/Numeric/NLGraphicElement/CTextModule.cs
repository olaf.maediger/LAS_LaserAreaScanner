﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLGraphicElement
{
  public class CTextModule : CElement
  {
     //
    //----------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------
    //
    public CTextModule(CRealPixel realpixel)
      : base(realpixel)
    {
      BuildRealVector();
      BuildPixelVector();
    }
    //
    //----------------------------------------------------------------
    //  Segment - Helper
    //----------------------------------------------------------------
    //
    public override Boolean BuildRealVector()
    {
      try
      {
        //return true;
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildPixelVector()
    {
      try
      {
        return FElementList.BuildPixelVector();
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      //if (FShow)
      //{
      //}
      return true;
    }

  }
}
