﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLGraphicElement
{
  public abstract class CElement
  { //
    //----------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------
    //
    public const Boolean INIT_SHOW = true;
    //
    //----------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------
    //
    private Guid FID;
    // reference to global transformation
    protected CRealPixel FRealPixel;
    protected Double[] FRealX, FRealY;
    protected Int32[] FPixelX, FPixelY;    
    protected CElementList FElementList;
    //
    //----------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------
    //
    public CElement(CRealPixel realpixel)
    {
      FID = Guid.NewGuid();
      FRealPixel = realpixel;
      //!!!!FShow = INIT_SHOW;
      FElementList = new CElementList();
    }
    //
    //----------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------
    //
    public Guid ID
    {
      get { return FID; }
    }
    //
    //----------------------------------------------------------
    //  Segment - Helper
    //----------------------------------------------------------
    //
    public abstract Boolean BuildRealVector();
    public abstract Boolean BuildPixelVector(); // from RealVector
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public abstract Boolean Draw(Graphics graphics);

  }
}
