﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLGraphicElement
{ // *** Grid *** 
  // Parametric Equation 
  // - two Points P0 = (x0, y0), P1 = (x1, y1)) on Diagonals
  // - countx/y: count lines vertical and horizontal
  //
  public class CGrid : CElement
  { //
    //----------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------
    //
    private const Boolean INIT_SHOWGRID = true;
    private const Boolean INIT_SHOWLINESHORIZONTAL = true;
    private const Boolean INIT_SHOWLINESVERTICAL = true;
    private const Double INIT_ORIGINX = 0.0;
    private const Double INIT_ORIGINY = 0.0;
    private const Double INIT_SIZEX = 10.0;
    private const Double INIT_SIZEY = 10.0;
    private const Int32 INIT_STEPCOUNTX = 10;
    private const Int32 INIT_STEPCOUNTY = 10;
    private const Int32 INIT_LINEWIDTHROW = 1;
    private const Int32 INIT_LINEWIDTHCOL = 1;
    //
    //----------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------
    // 
    protected Boolean FShowGrid, FShowLinesHorizontal, FShowLinesVertical;
    protected Double FOriginX, FOriginY;
    protected Double FSizeX, FSizeY;
    protected Int32 FPX0, FPY0;
    protected Int32 FPX1, FPY1;
    protected Int32 FStepCountX, FStepCountY;
    protected Int32 FLineWidthRow, FLineWidthCol;
    protected Color FColorRow, FColorCol;
    //
    //----------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------
    //
    public CGrid(CRealPixel realpixel)
      : base(realpixel)
    {
      FShowGrid = INIT_SHOWGRID;
      FShowLinesHorizontal = INIT_SHOWLINESHORIZONTAL;
      FShowLinesVertical = INIT_SHOWLINESVERTICAL;
      FOriginX = INIT_ORIGINX;
      FOriginY = INIT_ORIGINY;
      FSizeX = INIT_SIZEX;
      FSizeY= INIT_SIZEY;
      FStepCountX = INIT_STEPCOUNTX;
      FStepCountY = INIT_STEPCOUNTY;
      FLineWidthRow = INIT_LINEWIDTHROW;
      FLineWidthCol = INIT_LINEWIDTHCOL;
      FColorRow = Color.Red;
      FColorCol = Color.Green;
      BuildRealVector();
      BuildPixelVector();
    }

    public CGrid(CRealPixel realpixel,
                 Double originx, Double originy,
                 Double sizex, Double sizey,
                 Int32 stepcountx, Int32 stepcounty)
      : base(realpixel)
    {
      FShowGrid = INIT_SHOWGRID;
      FShowLinesHorizontal = INIT_SHOWLINESHORIZONTAL;
      FShowLinesVertical = INIT_SHOWLINESVERTICAL;
      FOriginX = originx;
      FOriginY = originy;
      FSizeX = sizex;
      FSizeY = sizey;
      FStepCountX = Math.Max(2, stepcountx);
      FStepCountY = Math.Max(2, stepcounty);
      FLineWidthRow = INIT_LINEWIDTHROW;
      FLineWidthCol = INIT_LINEWIDTHCOL;
      FColorRow = Color.Red;
      FColorCol = Color.Green;
      BuildRealVector();
      BuildPixelVector();
    }
    //
    //----------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------
    //
    public Boolean ShowGrid
    {
      get { return FShowGrid; }
      set 
      { 
        FShowGrid = value;
        BuildRealVector();
        BuildPixelVector();
      }
    }
    public Boolean ShowLinesHorizontal
    {
      get { return FShowLinesHorizontal; }
      set 
      { 
        FShowLinesHorizontal = value;
        BuildRealVector();
        BuildPixelVector();
      }
    }
    public Boolean ShowLinesVertical
    {
      get { return FShowLinesVertical; }
      set 
      { 
        FShowLinesVertical = value;
        BuildRealVector();
        BuildPixelVector();
      }
    }

    public void SetOrigin(Double originx, Double originy)
    {
      FOriginX = originx;
      FOriginY = originy;
      BuildRealVector();
      BuildPixelVector();
    }

    public void SetSize(Double sizex, Double sizey)
    {
      FSizeX = sizex;
      FSizeY = sizey;
      BuildRealVector();
      BuildPixelVector();
    }

    public void SetStepCount(Int32 stepcountx, Int32 stepcounty)
    {
      FStepCountX = Math.Max(2, stepcountx);
      FStepCountY = Math.Max(2, stepcounty);
      BuildRealVector();
      BuildPixelVector();
    }

    public void SetLineWidth(Int32 linewidthrow, Int32 linewidthcol)
    {
      FLineWidthRow = Math.Max(0, linewidthrow);
      FLineWidthCol = Math.Max(0, linewidthcol);
      BuildRealVector();
      BuildPixelVector();
    }

    public void SetColorRowCol(Color colorrow, Color colorcol)
    {
      FColorRow = colorrow;
      FColorCol = colorcol;
      BuildRealVector();
      BuildPixelVector();
    }
    //
    //----------------------------------------------------------------
    //  Segment - Helper
    //----------------------------------------------------------------
    //
    public override Boolean BuildRealVector()
    {
      try
      {
        FElementList.Clear();
        // Vertical Lines
        if (FShowLinesVertical)
        {
          Double DX = FSizeX / (FStepCountX - 1);
          Double Y0 = FOriginY;
          Double Y1 = FOriginY + FSizeY;
          for (Int32 CI = 0; CI < FStepCountX; CI++)
          {
            Double X = FOriginX + CI * DX;
            CSegment Segment = new CSegment(FRealPixel, X, Y0, X, Y1);
            Segment.LineColor = FColorCol;
            Segment.LineWidth = FLineWidthCol;
            FElementList.Add(Segment);
          }
        }
        // Horizontal Lines
        if (FShowLinesHorizontal)
        {
          Double DY = FSizeY / (FStepCountY - 1);
          Double X0 = FOriginX;
          Double X1 = FOriginX + FSizeX;
          for (Int32 RI = 0; RI < FStepCountY; RI++)
          {
            Double Y = FOriginY + RI * DY;
            CSegment Segment = new CSegment(FRealPixel, X0, Y, X1, Y);
            Segment.LineColor = FColorRow;
            Segment.LineWidth = FLineWidthRow;
            FElementList.Add(Segment);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildPixelVector()
    {
      try
      {
        return FElementList.BuildPixelVector();
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      if (FShowGrid)
      {
        FElementList.Draw(graphics);
      }
      return true;
    }


  }
}
