﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLGraphicElement
{ // *** Section *** (Strecke)
  // Parametric Equation 
  // - two Points P0 = (x0, y0), P1 = (x1, y1))
  //
  public class CSegment : CElement
  { //
    //----------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------
    //
    private const Double INIT_XREAL_0 = 0.0;
    private const Double INIT_YREAL_0 = 1.0;
    private const Double INIT_XREAL_1 = 1.0;
    private const Double INIT_YREAL_1 = 0.0;
    private const Int32 INIT_LINEWIDTH = 1;
    private Color INIT_LINECOLOR = Color.Yellow;
    //
    //----------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------
    // 
    protected Double FRX0, FRY0;
    protected Double FRX1, FRY1;
    protected Int32 FPX0, FPY0;
    protected Int32 FPX1, FPY1;
    protected Int32 FLineWidth;
    protected Color FLineColor;
    //
    //----------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------
    //
    public CSegment(CRealPixel realpixel)
      : base(realpixel)
    {
      FRX0 = INIT_XREAL_0;
      FRY0 = INIT_YREAL_0;
      FRX1 = INIT_XREAL_1;
      FRY1 = INIT_YREAL_1;
      BuildRealVector();
      BuildPixelVector();
      FLineWidth = INIT_LINEWIDTH;
      FLineColor = INIT_LINECOLOR;
    }

    public CSegment(CRealPixel realpixel,
                    Double rx0, Double ry0,
                    Double rx1, Double ry1)
      : base(realpixel)
    {
      FRX0 = rx0;
      FRY0 = ry0;
      FRX1 = rx1;
      FRY1 = ry1;
      BuildRealVector();
      BuildPixelVector();
      FLineWidth = INIT_LINEWIDTH;
      FLineColor = INIT_LINECOLOR;
    }
    //
    //----------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------
    //
    private void SetRX0(Double value)
    {
      FRX0 = value;
      BuildRealVector();
      BuildPixelVector();
    }
    public Double RX0
    {
      get { return FRX0; }
      set { SetRX0(value); }
    }

    private void SetRY0(Double value)
    {
      FRY0 = value;
      BuildRealVector();
      BuildPixelVector();
    }
    public Double RY0
    {
      get { return FRY0; }
      set { SetRY0(value); }
    }

    private void SetRX1(Double value)
    {
      FRX1 = value;
      BuildRealVector();
      BuildPixelVector();
    }
    public Double RX1
    {
      get { return FRX1; }
      set { SetRX1(value); }
    }

    private void SetRY1(Double value)
    {
      FRY1 = value;
      BuildRealVector();
      BuildPixelVector();
    }
    public Double RY1
    {
      get { return FRY1; }
      set { SetRY1(value); }
    }

    public Color LineColor
    {
      get { return FLineColor; }
      set { FLineColor = value; }
    }

    public Int32 LineWidth
    {
      get { return FLineWidth; }
      set { FLineWidth = value; }
    }
    //
    //----------------------------------------------------------------
    //  Segment - Helper
    //----------------------------------------------------------------
    //
    public override Boolean BuildRealVector()
    {
      return true;
    }

    public override Boolean BuildPixelVector()
    {
      try
      {
        FPX0 = FRealPixel.XRealPixel(FRX0);
        FPY0 = FRealPixel.YRealPixel(FRY0);
        FPX1 = FRealPixel.XRealPixel(FRX1);
        FPY1 = FRealPixel.YRealPixel(FRY1);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      //if (FShow)
      {
        Pen PL = new Pen(FLineColor, FLineWidth);
        graphics.DrawLine(PL, FPX0, FPY0, FPX1, FPY1);
      }
      return true;
    }


  }
}
