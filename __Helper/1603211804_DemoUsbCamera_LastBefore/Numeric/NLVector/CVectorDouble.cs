﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace NLVector
{
	public class CVectorDouble : CVectorBase
	{
		//
		//----------------------------------------
		//	Segment - Constant
		//----------------------------------------
		//
		private const Double INIT_VALUE = 0.0;
		//
		//----------------------------------------
		//	Segment - Member
		//----------------------------------------
		//
		private Double[] FValues = null;
		//
		//----------------------------------------
		//	Segment - Property
		//----------------------------------------
		//
		public Int32 VIL
		{
			get { return 0; }
		}
		public Int32 VIH
		{
			get { return Size - 1; }
		}
		public Int32 Size
		{
			get { return FValues.Length; }
		}
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorDouble(CNotifier notifier, Int32 size)
      : base(notifier)
    {
      FValues = new Double[size];
			InitValues(INIT_VALUE);
		}

    public CVectorDouble(CNotifier notifier, Int32 size, Double initvalue)
      : base(notifier)
    {
      FValues = new Double[size];
			InitValues(initvalue);
		}

    public CVectorDouble(CNotifier notifier, CVectorDouble v)
      : base(notifier)
    {
      FValues = new Double[v.Size];
			for (Int32 I = VIL; I <= VIH; I++)
			{
				FValues[I] = v[I];
			}
		}

    public CVectorDouble(CNotifier notifier, CVectorDouble sourcevector, Int32 deltacount, Int32 copyoffset)
      : base(notifier)
    {
      FValues = new Double[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      } else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorDouble(CNotifier notifier, CVectorDouble indices, CVectorDouble source)
      : base(notifier)
    {
      FValues = new Double[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[(Int32)indices[VI]];
      }
    }

    public CVectorDouble(CNotifier notifier, Double[] values)
      : base(notifier)
    {
      FValues = new Double[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CVectorDouble(Int32 size)
      : base()
    {
      FValues = new Double[size];
      InitValues(INIT_VALUE);
    }

    public CVectorDouble(Int32 size, Double initvalue)
      : base()
    {
      FValues = new Double[size];
      InitValues(initvalue);
    }

    public CVectorDouble(CVectorDouble v)
      : base()
    {
      FValues = new Double[v.Size];
      for (Int32 I = VIL; I <= VIH; I++)
      {
        FValues[I] = v[I];
      }
    }

    public CVectorDouble(CVectorDouble sourcevector, Int32 deltacount, Int32 copyoffset)
      : base()
    {
      FValues = new Double[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      }
      else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorDouble(CVectorDouble indices, CVectorDouble source)
      : base()
    {
      FValues = new Double[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[(Int32)indices[VI]];
      }
    }

    public CVectorDouble(Double[] values)
      : base()
    {
      FValues = new Double[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public bool InitValues(Double initvalue)
		{
			for (Int32 VI = VIL; VI <= VIH; VI++)
			{
				FValues[VI] = initvalue;
			}
			return true;
		}
		//
		//----------------------------------------
		//	Segment - Helper
		//----------------------------------------
		//
		protected Boolean CheckIndex(Int32 index)
		{
			return ((VIL <= index) & (index <= VIH));
		}

    public void Add(Double v)
    {
      Double[] V = new Double[1 + this.Size];
      for (Int32 VI = 0; VI < Size; VI++)
      {
        V[VI] = this[VI];
      }
      V[this.Size] = v;
      FValues = V;
    }

    public void RemoveZeroIndex()
    {
      Double[] V = new Double[this.Size - 1];
      for (Int32 VI = 0; VI < Size - 1; VI++)
      {
        V[VI] = this[VI];
      }
      FValues = V;
    }

    public Boolean PresetConstant(Double presetvalue, Int32 startindex, Int32 presetcount)
    {
      try
      {
        for (Int32 VI = startindex; VI <= startindex + presetcount; VI++)
        {
          FValues[VI] = presetvalue;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
		//
		//---------------------------------------
		//	Segment - Overloading of Index-Operator
		//---------------------------------------
		//
		protected Double GetValue(Int32 index)
		{
			if (CheckIndex(index))
			{
				return FValues[index];
			}
			return 0.0;
		}
		protected void SetValue(Int32 index, Double value)
		{
			if (CheckIndex(index))
			{
				FValues[index] = value;
			}
		}
		public Double this[Int32 index]
		{
			get { return GetValue(index); }
			set { SetValue(index, value); }
		}
    //
    //----------------------------------------
    //	Segment - Operation
    //----------------------------------------
    //
    public override Boolean Normalize()
    {
      try
      {
        Double Sum2 = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum2 += this[VI] * this[VI];
        }
        Sum2 = Math.Sqrt(Sum2);
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = (Double)(this[VI] / Sum2);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
		
    public override Boolean IsDifferent(CVectorBase vector)
    {

      if (null == vector)
      {
        return true;
      }
      if (vector.GetType() != this.GetType())
      {
        return true;
      }
      if (((CVectorDouble)vector).Size != this.Size)
      {
        return true;
      }
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        if (((CVectorDouble)vector)[VI] != this[VI])
        {
          return true;
        }
      }
      return false;
    }		

    public Boolean AddConstant(Double value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] += value;
      }
      return true;
    }

    public Boolean SubtractConstant(Double value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] -= value;
      }
      return true;
    }

    public Boolean MultiplyConstant(Double value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] *= value;
      }
      return true;
    }

    public Boolean DivideConstant(Double value)
    {
      if (0 != value)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] /= value;
        }
        return true;
      }
      return false;
    }

    public Boolean GetMeanValue(out Double value)
    {
      if (0 < this.Size)
      {
        Double Sum = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum += this[VI];
        }
        value = Sum / this.Size;
        return true;
      }
      value = 0.0;
      return false;
    }

    // 
    public Boolean SubtractGreaterEqual(Double subtrahend, Double border)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = Math.Max(border, this[VI] - subtrahend);
        }
        return true;
      }
      return false;
    }

    public Boolean ShiftToLowerIndices(Double value)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH - 1; VI++)
        {
          this[VI] = this[1 + VI];
        }
        this[VIH] = value;
        return true;
      }
      return false;
    }

    //
    //----------------------------------------
    //	Segment - Mathematical Operations
    //----------------------------------------
    //
    public Boolean FindThresholdValueLower(Double threshold,
                                           out Int32 valueindex)
    {
      valueindex = 0;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        Double Value = this[VI];
        if (Value < threshold)
        {
          valueindex = VI;
          return true;
        }
      }
      return false;
    }

    public Boolean FindMinimumMaximum(out Double minimum,
                                      out Double maximum)
    {
      minimum = +1E10;
      maximum = -1E10;
      Boolean Result = false;
      if (0 <= VIH)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Double Value = this[VI];
          if (Value < minimum)
          {
            minimum = Value;
          }
          else
            if (maximum < Value)
            {
              maximum = Value;
            }
        }
        Result = true;
      }
      return Result;
    }

    public Boolean FindMinimumMaximumPreset(ref Double minimum,
                                            ref Double maximum)
    {
      Boolean Result = true;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        Double Value = this[VI];
        if (Value < minimum)
        {
          minimum = Value;
        } else
          if (maximum < Value)
          {
            maximum = Value;
          }
      }
      return Result;
    }

    public Boolean FindCorrelatedValue(Double correlatedvalue,
                                       out Int32 valueindex)
    {
      valueindex = 0;
      if (correlatedvalue <= this[this.VIL])
      {
        return true;
      }
      //
      if (this[this.VIH] <= correlatedvalue)
      {
        valueindex = this.VIH;
        return true;
      }
      //
      for (Int32 VI = 1 + VIL; VI < VIH; VI++)
      {
        Double ValueR = this[VI];
        if (correlatedvalue < ValueR)
        {
          Double ValueL = this[VI - 1];
          Double DL = Math.Abs(correlatedvalue - ValueL);
          Double DR = Math.Abs(ValueR - correlatedvalue);
          if (DL <= DR)
          {
            valueindex = VI - 1;
            return true;
          }
          else
          {
            valueindex = VI;
            return true;
          }
        }
      }
      return false;
    }

    public Boolean FindCorrelatedThresholdLower(Double valuepreset,
                                                out Int32 indexfound,
                                                out Double valuefound)
    {
      indexfound = VIL;
      valuefound = this[indexfound];
      //
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        Double DValue = this[VI];
        if (valuepreset <= DValue)
        { // interval lower limit detected
          indexfound = VI;
          valuefound = DValue;
          return true;
        } 
      }
      return false;
    }

    public Boolean FindCorrelatedThresholdUpper(Double valuepreset,
                                                out Int32 indexfound,
                                                out Double valuefound)
    {
      indexfound = VIL;
      valuefound = this[indexfound];
      //
      for (Int32 VI = VIH; VIL <= VI; VI--)
      {
        Double DValue = this[VI];
        if (DValue <= valuepreset)
        { // interval upper limit detected
          indexfound = VI;
          valuefound = DValue;
          return true;
        } 
      }
      return false;
    }

    public Boolean CrossProduct(CVectorDouble vector)
    {
      const int X = 0;
      const int Y = 1;
      const int Z = 2;
      //
      if (3 != this.Size)
      {
        return false;
      }
      if (3 != vector.Size)
      {
        return false;
      }
      this[X] = this[Y] * vector[Z] - this[Z] * vector[Y];
      this[Y] = this[Z] * vector[X] - this[X] * vector[Z];
      this[Z] = this[X] * vector[Y] - this[Y] * vector[X];
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    public void Write(String header)
		{
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]:", header, Size));
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          String Line = String.Format(" V[{0}] = {1,7:0.000000000}",
                                      VI, this[VI]);
          FNotifier.Write(Line);
        }
      }
		}

	}
}
