﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLVector
{
  public class CMixedValue
  {
    public Object Value
    {
      get { return null; }
    }
  }

  public class CInt32Value : CMixedValue
  {
    public CInt32Value(Int32 value)
    {
      FValue = value;
    }

    private Int32 FValue;
    public new Int32 Value
    {
      get { return FValue; }
    }
  }

  public class CDoubleValue : CMixedValue
  {
    public CDoubleValue(Double value)
    {
      FValue = value;
    }

    private Double FValue;
    public new Double Value
    {
      get { return FValue; }
    }
  }

  public class CTimeValue : CMixedValue
  {
    public CTimeValue(DateTime value)
    {
      FValue = value;
    }

    private DateTime FValue;
    public new DateTime Value
    {
      get { return FValue; }
    }
    public Int32 Hour
    {
      get { return FValue.Hour; }
    }
    public Int32 Minute
    {
      get { return FValue.Minute; }
    }
    public Int32 Second
    {
      get { return FValue.Second; }
    }
  }

  public class CDateValue : CMixedValue
  {
    public CDateValue(DateTime value)
    {
      FValue = value;
    }

    private DateTime FValue;
    public new DateTime Value
    {
      get { return FValue; }
    }
    public Int32 Day
    {
      get { return FValue.Day; }
    }
    public Int32 Month
    {
      get { return FValue.Month; }
    }
    public Int32 Year
    {
      get { return FValue.Year; }
    }
  }

  public class CMixedValues : List<CMixedValue>
  {
  }
}
