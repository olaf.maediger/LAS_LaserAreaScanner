﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace NLVector
{
  public class CVectorString : CVectorBase
  { //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //
    private const String INIT_ZERO = "";
    //
    //----------------------------------------
    //	Segment - Field
    //----------------------------------------
    //
    private String[] FValues = null;
    //
    //----------------------------------------
    //	Segment - Property
    //----------------------------------------
    //
    public Int32 VIL
    {
      get { return 0; }
    }
    public Int32 VIH
    {
      get { return Size - 1; }
    }
    public Int32 Size
    {
      get { return FValues.Length; }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorString(CNotifier notifier, Int32 size)
      : base(notifier)
    {
      FValues = new String[size];
			InitValues(INIT_ZERO);
		}

    public CVectorString(CNotifier notifier, Int32 size, String initvalue)
      : base(notifier)
    {
      FValues = new String[size];
			InitValues(initvalue);
		}

    public CVectorString(CNotifier notifier, CVectorString v)
      : base(notifier)
    {
      FValues = new String[v.Size];
			for (Int32 I = VIL; I <= VIH; I++)
			{
				FValues[I] = v[I];
			}
		}

    public CVectorString(CNotifier notifier, CVectorString sourcevector, 
                         Int32 deltacount, Int32 copyoffset)
      : base(notifier)
    {
      FValues = new String[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      } else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorString(CNotifier notifier, 
                         CVectorInt32 indices, 
                         CVectorString source)
      : base(notifier)
    {
      FValues = new String[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[indices[VI]];
      }
    }

    public CVectorString(CNotifier notifier, String[] values)
      : base(notifier)
    {
      FValues = new String[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CVectorString(Int32 size)
      : base()
    {
      FValues = new String[size];
      InitValues(INIT_ZERO);
    }

    public CVectorString(Int32 size, String initvalue)
      : base()
    {
      FValues = new String[size];
      InitValues(initvalue);
    }

    public CVectorString(CVectorString v)
      : base()
    {
      FValues = new String[v.Size];
      for (Int32 I = VIL; I <= VIH; I++)
      {
        FValues[I] = v[I];
      }
    }

    public CVectorString(CVectorString sourcevector,
                         Int32 deltacount, Int32 copyoffset)
      : base()
    {
      FValues = new String[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      }
      else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorString(CVectorInt32 indices,
                         CVectorString source)
      : base()
    {
      FValues = new String[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[indices[VI]];
      }
    }

    public CVectorString(String[] values)
      : base()
    {
      FValues = new String[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public Boolean InitValues(String initvalue)
		{
			for (Int32 VI = VIL; VI <= VIH; VI++)
			{
				FValues[VI] = initvalue;
			}
			return true;
		}
		//
		//----------------------------------------
		//	Segment - Helper
		//----------------------------------------
		//
		protected Boolean CheckIndex(Int32 index)
		{
			return ((VIL <= index) & (index <= VIH));
		}

    public void Add(String v)
		{
      String[] V = new String[1 + this.Size];
			for (Int32 VI = 0; VI < Size; VI++)
			{
				V[VI] = this[VI];
			}
			V[this.Size] = v;
			FValues = V;
		}

    public Boolean PresetConstant(String presetvalue, 
                                  Int32 startindex, 
                                  Int32 presetcount)
    {
      try
      {
        for (Int32 VI = startindex; VI <= startindex + presetcount; VI++)
        {
          FValues[VI] = presetvalue;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
		//
		//---------------------------------------
		//	Segment - Overloading of Index-Operator
		//---------------------------------------
		//
    protected String GetValue(Int32 index)
		{
			if (CheckIndex(index))
			{
				return FValues[index];
			}
			return INIT_ZERO;
		}
    protected void SetValue(Int32 index, String value)
		{
			if (CheckIndex(index))
			{
				FValues[index] = value;
			}
		}
    public String this[Int32 index]
		{
			get { return GetValue(index); }
			set { SetValue(index, value); }
		}
    //
    //----------------------------------------
    //	Segment - Operation
    //----------------------------------------
    //
    public override Boolean Normalize()
    {
      try
      {
        Int32 Sum2 = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum2 += this[VI].Length * this[VI].Length;
        }
        Sum2 = (Int32)Math.Sqrt(Sum2);
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = this[VI].Substring(0, Sum2);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean AddConstant(String value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] += value;
      }
      return true;
    }

    public Boolean SubtractConstant(String value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] = this[VI].Substring(0, this[VI].Length - 1);
      }
      return true;
    }

    public Boolean MultiplyConstant(String value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] += value + value;
      }
      return true;
    }

    public Boolean DivideConstant(String value)
    {
      if (0 < value.Length)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Int32 L = Math.Max(1, value.Length);
          L = Math.Min(L, this[VI].Length - 1);
          this[VI] = this[VI].Substring(0, L);
        }
        return true;
      }
      return false;
    }

    public Boolean GetMeanValue(out Int32 value)
    {
      if (0 < this.Size)
      {
        Int32 Sum = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum += this[VI].Length;
        }
        value = (Int32)(Sum / this.Size);
        return true;
      }
      value = 0;
      return false;
    }

    // 
    public Boolean SubtractGreaterEqual(Int32 subtrahend, Int32 border)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Int32 L = (Int32)Math.Max(border, (int)this[VI].Length - (int)subtrahend);
          this[VI] = this[VI].Substring(0, L);
        }
        return true;
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - Mathematical Operations
    //----------------------------------------
    //
    public Boolean FindThresholdValueLower(Int32 threshold,
                                           out Int32 valueindex)
    {
      valueindex = 0;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        Int32  Value = this[VI].Length;
        if (Value < threshold)
        {
          valueindex = VI;
          return true;
        }
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    public void Write(String header)
		{
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]:", header, Size));
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          String Line = String.Format(" V[{0}] = {1}",
                                      VI, this[VI]);
          FNotifier.Write(Line);
        }
      }
		}

	}
}

