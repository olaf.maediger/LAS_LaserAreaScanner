﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace NLVector
{
  public class CVectorByte : CVectorBase
  {
    //
    //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //
    private const Byte INIT_ZERO = 0x00;
    //
    //----------------------------------------
    //	Segment - Member
    //----------------------------------------
    //
    private Byte[] FValues = null;
    //
    //----------------------------------------
    //	Segment - Property
    //----------------------------------------
    //
    public Int32 VIL
    {
      get { return 0; }
    }
    public Int32 VIH
    {
      get { return Size - 1; }
    }
    public Int32 Size
    {
      get { return FValues.Length; }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorByte(CNotifier notifier, Int32 size)
      : base()
    {
      FValues = new Byte[size];
			InitValues(INIT_ZERO);
		}

    public CVectorByte(CNotifier notifier, Int32 size, Byte initvalue)
      : base()
    {
      FValues = new Byte[size];
			InitValues(initvalue);
		}

    public CVectorByte(CNotifier notifier, CVectorByte v)
      : base()
    {
      FValues = new Byte[v.Size];
			for (Int32 I = VIL; I <= VIH; I++)
			{
				FValues[I] = v[I];
			}
		}

    public CVectorByte(CNotifier notifier, CVectorByte sourcevector, 
                       Int32 deltacount, Int32 copyoffset)
      : base()
    {
      FValues = new Byte[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      } else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorByte(CNotifier notifier, 
                       CVectorByte indices, 
                       CVectorByte source)
      : base()
    {
      FValues = new Byte[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[(Int32)indices[VI]];
      }
    }

    public CVectorByte(CNotifier notifier, Byte[] values)
      : base()
    {
      FValues = new Byte[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CVectorByte(Int32 size)
      : base()
    {
      FValues = new Byte[size];
      InitValues(INIT_ZERO);
    }

    public CVectorByte(Int32 size, Byte initvalue)
      : base()
    {
      FValues = new Byte[size];
      InitValues(initvalue);
    }

    public CVectorByte(CVectorByte v)
      : base()
    {
      FValues = new Byte[v.Size];
      for (Int32 I = VIL; I <= VIH; I++)
      {
        FValues[I] = v[I];
      }
    }

    public CVectorByte(CVectorByte sourcevector,
                       Int32 deltacount, Int32 copyoffset)
      : base()
    {
      FValues = new Byte[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      }
      else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorByte(CVectorByte indices,
                       CVectorByte source)
      : base()
    {
      FValues = new Byte[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[(Int32)indices[VI]];
      }
    }

    public CVectorByte(Byte[] values)
      : base()
    {
      FValues = new Byte[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public bool InitValues(Byte initvalue)
		{
			for (Int32 VI = VIL; VI <= VIH; VI++)
			{
				FValues[VI] = initvalue;
			}
			return true;
		}
		//
		//----------------------------------------
		//	Segment - Helper
		//----------------------------------------
		//
		protected Boolean CheckIndex(Int32 index)
		{
			return ((VIL <= index) & (index <= VIH));
		}

    public void Add(Byte v)
		{
      Byte[] V = new Byte[1 + this.Size];
			for (Int32 VI = 0; VI < Size; VI++)
			{
				V[VI] = this[VI];
			}
			V[this.Size] = v;
			FValues = V;
		}

    public Boolean PresetConstant(Byte presetvalue, 
                                  Int32 startindex, 
                                  Int32 presetcount)
    {
      try
      {
        for (Int32 VI = startindex; VI <= startindex + presetcount; VI++)
        {
          FValues[VI] = presetvalue;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
		//
		//---------------------------------------
		//	Segment - Overloading of Index-Operator
		//---------------------------------------
		//
    protected Byte GetValue(Int32 index)
		{
			if (CheckIndex(index))
			{
				return FValues[index];
			}
			return INIT_ZERO;
		}
    protected void SetValue(Int32 index, Byte value)
		{
			if (CheckIndex(index))
			{
				FValues[index] = value;
			}
		}
    public Byte this[Int32 index]
		{
			get { return GetValue(index); }
			set { SetValue(index, value); }
		}
    //
    //----------------------------------------
    //	Segment - Operation
    //----------------------------------------
    //
    public override Boolean Normalize()
    {
      try
      {
        Double Sum2 = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum2 += this[VI] * this[VI];
        }
        Sum2 = Math.Sqrt(Sum2);
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = (Byte)(this[VI] / Sum2);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean AddConstant(Byte value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] += value;
      }
      return true;
    }

    public Boolean SubtractConstant(Byte value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] -= value;
      }
      return true;
    }

    public Boolean MultiplyConstant(Byte value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] *= value;
      }
      return true;
    }

    public Boolean DivideConstant(Byte value)
    {
      if (0 != value)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] /= value;
        }
        return true;
      }
      return false;
    }

    public Boolean GetMeanValue(out Byte value)
    {
      if (0 < this.Size)
      {
        Double Sum = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum += this[VI];
        }
        value = (Byte)(Sum / this.Size);
        return true;
      }
      value = INIT_ZERO;
      return false;
    }

    // 
    public Boolean SubtractGreaterEqual(Byte subtrahend, Byte border)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = (Byte)Math.Max(border, (int)this[VI] - (int)subtrahend);
        }
        return true;
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - Mathematical Operations
    //----------------------------------------
    //
    public Boolean FindThresholdValueLower(Byte threshold,
                                           out Int32 valueindex)
    {
      valueindex = 0;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        Double Value = this[VI];
        if (Value < threshold)
        {
          valueindex = VI;
          return true;
        }
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    public void Write(String header)
		{
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]:", header, Size));
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          String Line = String.Format(" V[{0}] = {1:X02}",
                                      VI, this[VI]);
          FNotifier.Write(Line);
        }
      }
		}

	}
}
