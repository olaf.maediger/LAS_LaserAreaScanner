﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLVector
{
  public class CTupel2Dlist : List<CTupel2D>
  {

    //
    //----------------------------------------
    //	Segment - Property
    //----------------------------------------
    //
    public Int32 VIL
    {
      get { return 0; }
    }
    public Int32 VIH
    {
      get { return Size - 1; }
    }
    public Int32 Size
    {
      get { return this.Count; }
    }

    private CTupel2D GetIndexTupel2D(Int32 index)
    {
      if (CheckIndex(index))
      {
        return base[index];
      }
      return null;
    }
    private void SetIndexTupel2D(Int32 index, CTupel2D value)
    {
      if (CheckIndex(index))
      {
        if (value is CTupel2D)
        {
          base[index] = value; 
        }
      }
    }
    public new CTupel2D this[Int32 index]
    {
      get { return GetIndexTupel2D(index); }
      set { SetIndexTupel2D(index, value); }
    }
    //
    //----------------------------------------
    //	Segment - Constructor
    //----------------------------------------
    //


    //
    //----------------------------------------
    //	Helper
    //----------------------------------------
    //
    protected Boolean CheckIndex(Int32 index)
    {
      return ((VIL <= index) & (index <= VIH));
    }




    public Boolean Add(Double x, Double y)
    {
      CTupel2D Tupel2D = new CTupel2D(x, y);
      Add(Tupel2D);
      return true;
    }

    /*
    public Boolean ConvertToVectorDoubleXY(out CVectorDouble vectorx,
                                           out CVectorDouble vectory)
    {
      return true;
    }*/
  }
}
