﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace NLVector
{
  public class CVectorMixed : CVectorBase
  {
    //
    //
    //----------------------------------------
    //	Constant
    //----------------------------------------
    //
    private const Double INIT_VALUE = 0.0;
    //
    //----------------------------------------
    //	Member
    //----------------------------------------
    //
    private CMixedValue[] FValues = null;
    //
    //----------------------------------------
    //	Property
    //----------------------------------------
    //
    public Int32 VIL
    {
      get { return 0; }
    }
    public Int32 VIH
    {
      get { return Size - 1; }
    }
    public Int32 Size
    {
      get { return FValues.Length; }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorMixed(CNotifier notifier, Int32 size)
      : base(notifier)
    {
      FValues = new CMixedValue[size];
			// NC InitValues(INIT_VALUE);
		}

    public CVectorMixed(CNotifier notifier, CVectorDouble vectordouble)
      : base(notifier)
    {
      FValues = new CMixedValue[vectordouble.Size];
			for (Int32 VI = VIL; VI <= VIH; VI++)
			{
				FValues[VI] = new CDoubleValue(vectordouble[VI]);
			}
		}

    public CVectorMixed(CNotifier notifier, Double[] doublevalues)
      : base(notifier)
    {
      FValues = new CMixedValue[doublevalues.Length];
      for (Int32 VI = 0; VI < doublevalues.Length; VI++)
      {
        FValues[VI] = new CDoubleValue(doublevalues[VI]);
      }
    }

    public CVectorMixed(CNotifier notifier, CMixedValue[] mixedvalues)
      : base(notifier)
    {
      FValues = mixedvalues;
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CVectorMixed(Int32 size)
      : base()
    {
      FValues = new CMixedValue[size];
    }

    public CVectorMixed(CVectorDouble vectordouble)
      : base()
    {
      FValues = new CMixedValue[vectordouble.Size];
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        FValues[VI] = new CDoubleValue(vectordouble[VI]);
      }
    }

    public CVectorMixed(Double[] doublevalues)
      : base()
    {
      FValues = new CMixedValue[doublevalues.Length];
      for (Int32 VI = 0; VI < doublevalues.Length; VI++)
      {
        FValues[VI] = new CDoubleValue(doublevalues[VI]);
      }
    }

    public CVectorMixed(CMixedValue[] mixedvalues)
      : base()
    {
      FValues = mixedvalues;
    }
    //
    //----------------------------------------
    //	Initialisation
    //----------------------------------------
    //

    //
		//
		//----------------------------------------
		//	Helper
		//----------------------------------------
		//
		protected Boolean CheckIndex(Int32 index)
		{
			return ((VIL <= index) & (index <= VIH));
		}
		//
		//
		//---------------------------------------
		//	Overloading of Index-Operator
		//---------------------------------------
		//
		protected CMixedValue GetValue(Int32 index)
		{
			if (CheckIndex(index))
			{
        return FValues[index];
			}
			return null;
		}
		protected void SetValue(Int32 index, CMixedValue value)
		{
			if (CheckIndex(index))
			{
				FValues[index] = value;
			}
		}
		public CMixedValue this[Int32 index]
		{
			get { return GetValue(index); }
			set { SetValue(index, value); }
		}
    //
    //----------------------------------------
    //	Segment - Operation
    //----------------------------------------
    //
    public override Boolean Normalize()
    {
      return false;
      /*try
      {
        Double Sum2 = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum2 += this[VI] * this[VI];
        }
        Sum2 = Math.Sqrt(Sum2);
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = (Byte)(this[VI] / Sum2);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }*/
    }

    //
    //----------------------------------------
    //	Segment - Mathematical Operations
    //----------------------------------------
    //

    //
    //----------------------------------------
    //	Protocol
    //----------------------------------------
    //
    public void Write(String header)
		{
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]:", header, Size));
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          //String Line = String.Format(" V[{0}] = {1,7:0.000000000}",
          //                            VI, this[VI]);

          //...
          //...FNotifier.Write(Line);
        }
      }
		}




	}
}
