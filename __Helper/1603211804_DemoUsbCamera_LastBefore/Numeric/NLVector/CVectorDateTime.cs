﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace NLVector
{
  public class CVectorDateTime : CVectorBase
  {
		//
		//----------------------------------------
		//	Segment - Constant
		//----------------------------------------
		//
    // NC -> use DateTime.Zero direct! private const DateTime INIT_VALUE = DateTime.Zero;
		//
		//----------------------------------------
		//	Segment - Member
		//----------------------------------------
		//
		private DateTime[] FValues = null;
		//
		//----------------------------------------
		//	Segment - Property
		//----------------------------------------
		//
		public Int32 VIL
		{
			get { return 0; }
		}
		public Int32 VIH
		{
			get { return Size - 1; }
		}
		public Int32 Size
		{
			get { return FValues.Length; }
		}
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorDateTime(CNotifier notifier, Int32 size)
      : base(notifier)
    {
      FValues = new DateTime[size];
      InitValues(DateTime.MinValue);
		}

    public CVectorDateTime(CNotifier notifier, Int32 size, DateTime initvalue)
      : base(notifier)
    {
      FValues = new DateTime[size];
			InitValues(initvalue);
		}

    public CVectorDateTime(CNotifier notifier, CVectorDateTime v)
      : base(notifier)
    {
      FValues = new DateTime[v.Size];
			for (Int32 I = VIL; I <= VIH; I++)
			{
				FValues[I] = v[I];
			}
		}

    public CVectorDateTime(CNotifier notifier, CVectorDateTime sourcevector, Int32 deltacount, Int32 copyoffset)
      : base(notifier)
    {
      FValues = new DateTime[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      } else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorDateTime(CNotifier notifier, CVectorInt32 indices, CVectorDateTime source)
      : base(notifier)
    {
      FValues = new DateTime[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[(Int32)indices[VI]];
      }
    }

    public CVectorDateTime(CNotifier notifier, DateTime[] values)
      : base(notifier)
    {
      FValues = new DateTime[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CVectorDateTime(Int32 size)
      : base()
    {
      FValues = new DateTime[size];
      InitValues(DateTime.MinValue);
    }

    public CVectorDateTime(Int32 size, DateTime initvalue)
      : base()
    {
      FValues = new DateTime[size];
      InitValues(initvalue);
    }

    public CVectorDateTime(CVectorDateTime v)
      : base()
    {
      FValues = new DateTime[v.Size];
      for (Int32 I = VIL; I <= VIH; I++)
      {
        FValues[I] = v[I];
      }
    }

    public CVectorDateTime(CVectorDateTime sourcevector, Int32 deltacount, Int32 copyoffset)
      : base()
    {
      FValues = new DateTime[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      }
      else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorDateTime(CVectorInt32 indices, CVectorDateTime source)
      : base()
    {
      FValues = new DateTime[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[(Int32)indices[VI]];
      }
    }

    public CVectorDateTime(DateTime[] values)
      : base()
    {
      FValues = new DateTime[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public bool InitValues(DateTime initvalue)
		{
			for (Int32 VI = VIL; VI <= VIH; VI++)
			{
				FValues[VI] = initvalue;
			}
			return true;
		}
		//
		//----------------------------------------
		//	Segment - Helper
		//----------------------------------------
		//
		protected Boolean CheckIndex(Int32 index)
		{
			return ((VIL <= index) & (index <= VIH));
		}

    public void Add(DateTime v)
    {
      DateTime[] V = new DateTime[1 + this.Size];
      for (Int32 VI = 0; VI < Size; VI++)
      {
        V[VI] = this[VI];
      }
      V[this.Size] = v;
      FValues = V;
    }

    public void RemoveZeroIndex()
    {
      DateTime[] V = new DateTime[this.Size - 1];
      for (Int32 VI = 0; VI < Size - 1; VI++)
      {
        V[VI] = this[VI];
      }
      FValues = V;
    }

    public Boolean PresetConstant(DateTime presetvalue, Int32 startindex, Int32 presetcount)
    {
      try
      {
        for (Int32 VI = startindex; VI <= startindex + presetcount; VI++)
        {
          FValues[VI] = presetvalue;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
		//
		//---------------------------------------
		//	Segment - Overloading of Index-Operator
		//---------------------------------------
		//
		protected DateTime GetValue(Int32 index)
		{
			if (CheckIndex(index))
			{
				return FValues[index];
			}
      return DateTime.MinValue;
		}
		protected void SetValue(Int32 index, DateTime value)
		{
			if (CheckIndex(index))
			{
				FValues[index] = value;
			}
		}
		public DateTime this[Int32 index]
		{
			get { return GetValue(index); }
			set { SetValue(index, value); }
		}
    //
    //----------------------------------------
    //	Segment - Operation
    //----------------------------------------
    //
    public override Boolean Normalize()
    {
      try
      {
        Double Sum2 = 0.0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum2 += this[VI].Ticks * this[VI].Ticks;
        }
        Sum2 = Math.Sqrt(Sum2);
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = new DateTime((long)(this[VI].Ticks / Sum2));
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
		
    public override Boolean IsDifferent(CVectorBase vector)
    {

      if (null == vector)
      {
        return true;
      }
      if (vector.GetType() != this.GetType())
      {
        return true;
      }
      if (((CVectorDateTime)vector).Size != this.Size)
      {
        return true;
      }
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        if (((CVectorDateTime)vector)[VI] != this[VI])
        {
          return true;
        }
      }
      return false;
    }		

    public Boolean AddConstant(DateTime value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI].AddTicks(value.Ticks);
      }
      return true;
    }

    public Boolean SubtractConstant(DateTime value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI].AddTicks(-value.Ticks);
      }
      return true;
    }

    public Boolean MultiplyConstant(DateTime value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] = new DateTime(this[VI].Ticks * value.Ticks);
      }
      return true;
    }

    public Boolean DivideConstant(DateTime value)
    {
      if (DateTime.MinValue != value)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = new DateTime(this[VI].Ticks / value.Ticks);
        }
        return true;
      }
      return false;
    }

    public Boolean GetMeanValue(out DateTime value)
    {
      if (0 < this.Size)
      {
        long Sum = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum += this[VI].Ticks;
        }
        value = new DateTime((long)(Sum / this.Size));
        return true;
      }
      value = DateTime.MinValue;
      return false;
    }

    // 
    public Boolean SubtractGreaterEqual(DateTime subtrahend, DateTime border)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = new DateTime(Math.Max(border.Ticks, this[VI].Ticks - subtrahend.Ticks));
        }
        return true;
      }
      return false;
    }

    public Boolean ShiftToLowerIndices(DateTime value)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH - 1; VI++)
        {
          this[VI] = this[1 + VI];
        }
        this[VIH] = value;
        return true;
      }
      return false;
    }

    //
    //----------------------------------------
    //	Segment - Mathematical Operations
    //----------------------------------------
    //
    public Boolean FindThresholdValueLower(DateTime threshold,
                                           out Int32 valueindex)
    {
      valueindex = 0;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        DateTime Value = this[VI];
        if (Value < threshold)
        {
          valueindex = VI;
          return true;
        }
      }
      return false;
    }

    public Boolean FindMinimumMaximum(out DateTime minimum,
                                      out DateTime maximum)
    {
      minimum = DateTime.MaxValue;
      maximum = DateTime.MinValue;
      Boolean Result = false;
      if (0 <= VIH)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          DateTime Value = this[VI];
          if (Value < minimum)
          {
            minimum = Value;
          }
          else
            if (maximum < Value)
            {
              maximum = Value;
            }
        }
        Result = true;
      }
      return Result;
    }

    public Boolean FindMinimumMaximumPreset(ref DateTime minimum,
                                            ref DateTime maximum)
    {
      Boolean Result = true;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        DateTime Value = this[VI];
        if (Value < minimum)
        {
          minimum = Value;
        } else
          if (maximum < Value)
          {
            maximum = Value;
          }
      }
      return Result;
    }

    public Boolean FindCorrelatedValue(DateTime correlatedvalue,
                                       out Int32 valueindex)
    {
      valueindex = 0;
      if (correlatedvalue <= this[this.VIL])
      {
        return true;
      }
      //
      if (this[this.VIH] <= correlatedvalue)
      {
        valueindex = this.VIH;
        return true;
      }
      //
      for (Int32 VI = 1 + VIL; VI < VIH; VI++)
      {
        DateTime ValueR = this[VI];
        if (correlatedvalue < ValueR)
        {
          DateTime ValueL = this[VI - 1];
          DateTime DL = new DateTime(Math.Abs(correlatedvalue.Ticks - ValueL.Ticks));
          DateTime DR = new DateTime(Math.Abs(ValueR.Ticks - correlatedvalue.Ticks));
          if (DL <= DR)
          {
            valueindex = VI - 1;
            return true;
          }
          else
          {
            valueindex = VI;
            return true;
          }
        }
      }
      return false;
    }

    public Boolean FindCorrelatedThresholdLower(DateTime valuepreset,
                                                out Int32 indexfound,
                                                out DateTime valuefound)
    {
      indexfound = VIL;
      valuefound = this[indexfound];
      //
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        DateTime DValue = this[VI];
        if (valuepreset <= DValue)
        { // interval lower limit detected
          indexfound = VI;
          valuefound = DValue;
          return true;
        } 
      }
      return false;
    }

    public Boolean FindCorrelatedThresholdUpper(DateTime valuepreset,
                                                out Int32 indexfound,
                                                out DateTime valuefound)
    {
      indexfound = VIL;
      valuefound = this[indexfound];
      //
      for (Int32 VI = VIH; VIL <= VI; VI--)
      {
        DateTime DValue = this[VI];
        if (DValue <= valuepreset)
        { // interval upper limit detected
          indexfound = VI;
          valuefound = DValue;
          return true;
        } 
      }
      return false;
    }

    public Boolean CrossProduct(CVectorDateTime vector)
    {
      const int X = 0;
      const int Y = 1;
      const int Z = 2;
      //
      if (3 != this.Size)
      {
        return false;
      }
      if (3 != vector.Size)
      {
        return false;
      }
      this[X] = new DateTime(this[Y].Ticks * vector[Z].Ticks - this[Z].Ticks * vector[Y].Ticks);
      this[Y] = new DateTime(this[Z].Ticks * vector[X].Ticks - this[X].Ticks * vector[Z].Ticks);
      this[Z] = new DateTime(this[X].Ticks * vector[Y].Ticks - this[Y].Ticks * vector[X].Ticks);
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    public void Write(String header)
		{
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]:", header, Size));
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          String Line = String.Format(" V[{0}] = {1}", VI, this[VI]);
          FNotifier.Write(Line);
        }
      }
		}

	}
}
