﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace NLVector
{
  public class CVectorTimeSpan : CVectorBase
  {
		//
		//----------------------------------------
		//	Segment - Constant
		//----------------------------------------
		//
    // NC -> use TimeSpan.Zero direct! private const TimeSpan INIT_VALUE = TimeSpan.Zero;
		//
		//----------------------------------------
		//	Segment - Member
		//----------------------------------------
		//
		private TimeSpan[] FValues = null;
		//
		//----------------------------------------
		//	Segment - Property
		//----------------------------------------
		//
		public Int32 VIL
		{
			get { return 0; }
		}
		public Int32 VIH
		{
			get { return Size - 1; }
		}
		public Int32 Size
		{
			get { return FValues.Length; }
		}
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorTimeSpan(CNotifier notifier, Int32 size)
      : base(notifier)
    {
      FValues = new TimeSpan[size];
      InitValues(TimeSpan.Zero);
		}

    public CVectorTimeSpan(CNotifier notifier, Int32 size, TimeSpan initvalue)
      : base(notifier)
    {
      FValues = new TimeSpan[size];
			InitValues(initvalue);
		}

    public CVectorTimeSpan(CNotifier notifier, CVectorTimeSpan v)
      : base(notifier)
    {
      FValues = new TimeSpan[v.Size];
			for (Int32 I = VIL; I <= VIH; I++)
			{
				FValues[I] = v[I];
			}
		}

    public CVectorTimeSpan(CNotifier notifier, CVectorTimeSpan sourcevector, Int32 deltacount, Int32 copyoffset)
      : base(notifier)
    {
      FValues = new TimeSpan[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      } else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorTimeSpan(CNotifier notifier, CVectorInt32 indices, CVectorTimeSpan source)
      : base(notifier)
    {
      FValues = new TimeSpan[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[(Int32)indices[VI]];
      }
    }

    public CVectorTimeSpan(CNotifier notifier, TimeSpan[] values)
      : base(notifier)
    {
      FValues = new TimeSpan[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CVectorTimeSpan(Int32 size)
      : base()
    {
      FValues = new TimeSpan[size];
      InitValues(TimeSpan.Zero);
    }

    public CVectorTimeSpan(Int32 size, TimeSpan initvalue)
      : base()
    {
      FValues = new TimeSpan[size];
      InitValues(initvalue);
    }

    public CVectorTimeSpan(CVectorTimeSpan v)
      : base()
    {
      FValues = new TimeSpan[v.Size];
      for (Int32 I = VIL; I <= VIH; I++)
      {
        FValues[I] = v[I];
      }
    }

    public CVectorTimeSpan(CVectorTimeSpan sourcevector, Int32 deltacount, Int32 copyoffset)
      : base()
    {
      FValues = new TimeSpan[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      }
      else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorTimeSpan(CVectorInt32 indices, CVectorTimeSpan source)
      : base()
    {
      FValues = new TimeSpan[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[(Int32)indices[VI]];
      }
    }

    public CVectorTimeSpan(TimeSpan[] values)
      : base()
    {
      FValues = new TimeSpan[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public bool InitValues(TimeSpan initvalue)
		{
			for (Int32 VI = VIL; VI <= VIH; VI++)
			{
				FValues[VI] = initvalue;
			}
			return true;
		}
		//
		//----------------------------------------
		//	Segment - Helper
		//----------------------------------------
		//
		protected Boolean CheckIndex(Int32 index)
		{
			return ((VIL <= index) & (index <= VIH));
		}

    public void Add(TimeSpan v)
    {
      TimeSpan[] V = new TimeSpan[1 + this.Size];
      for (Int32 VI = 0; VI < Size; VI++)
      {
        V[VI] = this[VI];
      }
      V[this.Size] = v;
      FValues = V;
    }

    public void RemoveZeroIndex()
    {
      TimeSpan[] V = new TimeSpan[this.Size - 1];
      for (Int32 VI = 0; VI < Size - 1; VI++)
      {
        V[VI] = this[VI];
      }
      FValues = V;
    }

    public Boolean PresetConstant(TimeSpan presetvalue, Int32 startindex, Int32 presetcount)
    {
      try
      {
        for (Int32 VI = startindex; VI <= startindex + presetcount; VI++)
        {
          FValues[VI] = presetvalue;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
		//
		//---------------------------------------
		//	Segment - Overloading of Index-Operator
		//---------------------------------------
		//
		protected TimeSpan GetValue(Int32 index)
		{
			if (CheckIndex(index))
			{
				return FValues[index];
			}
      return TimeSpan.Zero;
		}
		protected void SetValue(Int32 index, TimeSpan value)
		{
			if (CheckIndex(index))
			{
				FValues[index] = value;
			}
		}
		public TimeSpan this[Int32 index]
		{
			get { return GetValue(index); }
			set { SetValue(index, value); }
		}
    //
    //----------------------------------------
    //	Segment - Operation
    //----------------------------------------
    //
    public override Boolean Normalize()
    {
      try
      {
        Double Sum2 = 0.0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum2 += this[VI].Ticks * this[VI].Ticks;
        }
        Sum2 = Math.Sqrt(Sum2);
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = new TimeSpan((long)(this[VI].Ticks / Sum2));
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
		
    public override Boolean IsDifferent(CVectorBase vector)
    {

      if (null == vector)
      {
        return true;
      }
      if (vector.GetType() != this.GetType())
      {
        return true;
      }
      if (((CVectorTimeSpan)vector).Size != this.Size)
      {
        return true;
      }
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        if (((CVectorTimeSpan)vector)[VI] != this[VI])
        {
          return true;
        }
      }
      return false;
    }		

    public Boolean AddConstant(TimeSpan value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] += value;
      }
      return true;
    }

    public Boolean SubtractConstant(TimeSpan value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] -= value;
      }
      return true;
    }

    public Boolean MultiplyConstant(TimeSpan value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] = new TimeSpan(this[VI].Ticks * value.Ticks);
      }
      return true;
    }

    public Boolean DivideConstant(TimeSpan value)
    {
      if (TimeSpan.Zero != value)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = new TimeSpan(this[VI].Ticks / value.Ticks);
        }
        return true;
      }
      return false;
    }

    public Boolean GetMeanValue(out TimeSpan value)
    {
      if (0 < this.Size)
      {
        TimeSpan Sum = TimeSpan.Zero;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum += this[VI];
        }
        value = new TimeSpan(Sum.Ticks / this.Size);
        return true;
      }
      value = TimeSpan.Zero;
      return false;
    }

    // 
    public Boolean SubtractGreaterEqual(TimeSpan subtrahend, TimeSpan border)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = new TimeSpan(Math.Max(border.Ticks, this[VI].Ticks - subtrahend.Ticks));
        }
        return true;
      }
      return false;
    }

    public Boolean ShiftToLowerIndices(TimeSpan value)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH - 1; VI++)
        {
          this[VI] = this[1 + VI];
        }
        this[VIH] = value;
        return true;
      }
      return false;
    }

    //
    //----------------------------------------
    //	Segment - Mathematical Operations
    //----------------------------------------
    //
    public Boolean FindThresholdValueLower(TimeSpan threshold,
                                           out Int32 valueindex)
    {
      valueindex = 0;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        TimeSpan Value = this[VI];
        if (Value < threshold)
        {
          valueindex = VI;
          return true;
        }
      }
      return false;
    }

    public Boolean FindMinimumMaximum(out TimeSpan minimum,
                                      out TimeSpan maximum)
    {
      minimum = TimeSpan.MaxValue;
      maximum = TimeSpan.MinValue;
      Boolean Result = false;
      if (0 <= VIH)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          TimeSpan Value = this[VI];
          if (Value < minimum)
          {
            minimum = Value;
          }
          else
            if (maximum < Value)
            {
              maximum = Value;
            }
        }
        Result = true;
      }
      return Result;
    }

    public Boolean FindMinimumMaximumPreset(ref TimeSpan minimum,
                                            ref TimeSpan maximum)
    {
      Boolean Result = true;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        TimeSpan Value = this[VI];
        if (Value < minimum)
        {
          minimum = Value;
        } else
          if (maximum < Value)
          {
            maximum = Value;
          }
      }
      return Result;
    }

    public Boolean FindCorrelatedValue(TimeSpan correlatedvalue,
                                       out Int32 valueindex)
    {
      valueindex = 0;
      if (correlatedvalue <= this[this.VIL])
      {
        return true;
      }
      //
      if (this[this.VIH] <= correlatedvalue)
      {
        valueindex = this.VIH;
        return true;
      }
      //
      for (Int32 VI = 1 + VIL; VI < VIH; VI++)
      {
        TimeSpan ValueR = this[VI];
        if (correlatedvalue < ValueR)
        {
          TimeSpan ValueL = this[VI - 1];
          TimeSpan DL = new TimeSpan(Math.Abs(correlatedvalue.Ticks - ValueL.Ticks));
          TimeSpan DR = new TimeSpan(Math.Abs(ValueR.Ticks - correlatedvalue.Ticks));
          if (DL <= DR)
          {
            valueindex = VI - 1;
            return true;
          }
          else
          {
            valueindex = VI;
            return true;
          }
        }
      }
      return false;
    }

    public Boolean FindCorrelatedThresholdLower(TimeSpan valuepreset,
                                                out Int32 indexfound,
                                                out TimeSpan valuefound)
    {
      indexfound = VIL;
      valuefound = this[indexfound];
      //
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        TimeSpan DValue = this[VI];
        if (valuepreset <= DValue)
        { // interval lower limit detected
          indexfound = VI;
          valuefound = DValue;
          return true;
        } 
      }
      return false;
    }

    public Boolean FindCorrelatedThresholdUpper(TimeSpan valuepreset,
                                                out Int32 indexfound,
                                                out TimeSpan valuefound)
    {
      indexfound = VIL;
      valuefound = this[indexfound];
      //
      for (Int32 VI = VIH; VIL <= VI; VI--)
      {
        TimeSpan DValue = this[VI];
        if (DValue <= valuepreset)
        { // interval upper limit detected
          indexfound = VI;
          valuefound = DValue;
          return true;
        } 
      }
      return false;
    }

    public Boolean CrossProduct(CVectorTimeSpan vector)
    {
      const int X = 0;
      const int Y = 1;
      const int Z = 2;
      //
      if (3 != this.Size)
      {
        return false;
      }
      if (3 != vector.Size)
      {
        return false;
      }
      this[X] = new TimeSpan(this[Y].Ticks * vector[Z].Ticks - this[Z].Ticks * vector[Y].Ticks);
      this[Y] = new TimeSpan(this[Z].Ticks * vector[X].Ticks - this[X].Ticks * vector[Z].Ticks);
      this[Z] = new TimeSpan(this[X].Ticks * vector[Y].Ticks - this[Y].Ticks * vector[X].Ticks);
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    public void Write(String header)
		{
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]:", header, Size));
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          String Line = String.Format(" V[{0}] = {1}", VI, this[VI]);
          FNotifier.Write(Line);
        }
      }
		}

	}
}
