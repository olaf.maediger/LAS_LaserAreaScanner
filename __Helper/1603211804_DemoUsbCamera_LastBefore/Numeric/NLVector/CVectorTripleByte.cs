﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace NLVector
{
  public class CVectorTripleByte : CVectorBase
  {
    //
    //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //
    private const Byte INIT_ZERO = 0x00;
    //
    //----------------------------------------
    //	Segment - Member
    //----------------------------------------
    //
    private Byte[] FRValues = null;
    private Byte[] FGValues = null;
    private Byte[] FBValues = null;
    //
    //----------------------------------------
    //	Segment - Property
    //----------------------------------------
    //
    public Int32 VIL
    {
      get { return 0; }
    }
    public Int32 VIH
    {
      get { return Size - 1; }
    }
    public Int32 Size
    {
      get { return FRValues.Length; }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorTripleByte(CNotifier notifier, Int32 size)
      : base(notifier)
    {
      FRValues = new Byte[size];
      FGValues = new Byte[size];
      FBValues = new Byte[size];
      InitValues(INIT_ZERO);
    }

    public CVectorTripleByte(CNotifier notifier, Int32 size, Byte initvalue)
      : base(notifier)
    {
      FRValues = new Byte[size];
      FGValues = new Byte[size];
      FBValues = new Byte[size];
      InitValues(initvalue);
    }

    public CVectorTripleByte(CNotifier notifier, CVectorTripleByte v)
      : base(notifier)
    {
      FRValues = new Byte[v.Size];
      FGValues = new Byte[v.Size];
      FBValues = new Byte[v.Size];
      for (Int32 I = VIL; I <= VIH; I++)
      {
        FRValues[I] = v.GetRValue(I);
        FGValues[I] = v.GetGValue(I);
        FBValues[I] = v.GetBValue(I);
      }
    }

    public CVectorTripleByte(CNotifier notifier, CVectorTripleByte sourcevector,
                             Int32 deltacount, Int32 copyoffset)
      : base(notifier)
    {
      FRValues = new Byte[sourcevector.Size + deltacount];
      FGValues = new Byte[sourcevector.Size + deltacount];
      FBValues = new Byte[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FRValues[TI++] = sourcevector.GetRValue(VI);
          FGValues[TI++] = sourcevector.GetGValue(VI);
          FBValues[TI++] = sourcevector.GetBValue(VI);
        }
      }
      else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FRValues[VI] = sourcevector.GetRValue(SI++);
          FGValues[VI] = sourcevector.GetGValue(SI++);
          FBValues[VI] = sourcevector.GetBValue(SI++);
        }
      }
    }

    public CVectorTripleByte(CNotifier notifier,
                             CVectorByte indices,
                             CVectorTripleByte source)
      : base(notifier)
    {
      FRValues = new Byte[indices.Size];
      FGValues = new Byte[indices.Size];
      FBValues = new Byte[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FRValues[VI] = source.GetRValue((Int32)indices[VI]);
        FGValues[VI] = source.GetGValue((Int32)indices[VI]);
        FBValues[VI] = source.GetBValue((Int32)indices[VI]);
      }
    }

    public CVectorTripleByte(CNotifier notifier, Byte[] values)
      : base(notifier)
    {
      FRValues = new Byte[values.Length];
      FGValues = new Byte[values.Length];
      FBValues = new Byte[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FRValues[VI] = values[VI];
        FGValues[VI] = values[VI];
        FBValues[VI] = values[VI];
      }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CVectorTripleByte(Int32 size)
      : base()
    {
      FRValues = new Byte[size];
      FGValues = new Byte[size];
      FBValues = new Byte[size];
      InitValues(INIT_ZERO);
    }

    public CVectorTripleByte(Int32 size, Byte initvalue)
      : base()
    {
      FRValues = new Byte[size];
      FGValues = new Byte[size];
      FBValues = new Byte[size];
      InitValues(initvalue);
    }

    public CVectorTripleByte(CVectorTripleByte v)
      : base()
    {
      FRValues = new Byte[v.Size];
      FGValues = new Byte[v.Size];
      FBValues = new Byte[v.Size];
      for (Int32 I = VIL; I <= VIH; I++)
      {
        FRValues[I] = v.GetRValue(I);
        FGValues[I] = v.GetGValue(I);
        FBValues[I] = v.GetBValue(I);
      }
    }

    public CVectorTripleByte(CVectorTripleByte sourcevector,
                             Int32 deltacount, Int32 copyoffset)
      : base()
    {
      FRValues = new Byte[sourcevector.Size + deltacount];
      FGValues = new Byte[sourcevector.Size + deltacount];
      FBValues = new Byte[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FRValues[TI++] = sourcevector.GetRValue(VI);
          FGValues[TI++] = sourcevector.GetGValue(VI);
          FBValues[TI++] = sourcevector.GetBValue(VI);
        }
      }
      else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FRValues[VI] = sourcevector.GetRValue(SI++);
          FGValues[VI] = sourcevector.GetGValue(SI++);
          FBValues[VI] = sourcevector.GetBValue(SI++);
        }
      }
    }

    public CVectorTripleByte(CVectorByte indices,
                             CVectorTripleByte source)
      : base()
    {
      FRValues = new Byte[indices.Size];
      FGValues = new Byte[indices.Size];
      FBValues = new Byte[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FRValues[VI] = source.GetRValue((Int32)indices[VI]);
        FGValues[VI] = source.GetGValue((Int32)indices[VI]);
        FBValues[VI] = source.GetBValue((Int32)indices[VI]);
      }
    }

    public CVectorTripleByte(Byte[] values)
      : base()
    {
      FRValues = new Byte[values.Length];
      FGValues = new Byte[values.Length];
      FBValues = new Byte[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FRValues[VI] = values[VI];
        FGValues[VI] = values[VI];
        FBValues[VI] = values[VI];
      }
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public bool InitValues(Byte initvalue)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        FRValues[VI] = initvalue;
        FGValues[VI] = initvalue;
        FBValues[VI] = initvalue;
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Helper
    //----------------------------------------
    //
    protected Boolean CheckIndex(Int32 index)
    {
      return ((VIL <= index) & (index <= VIH));
    }

    public void Add(Byte v)
    {
      Byte[] VR = new Byte[1 + this.Size];
      Byte[] VG = new Byte[1 + this.Size];
      Byte[] VB = new Byte[1 + this.Size];
      for (Int32 VI = 0; VI < Size; VI++)
      {
        VR[VI] = this.GetRValue(VI);
        VG[VI] = this.GetGValue(VI);
        VB[VI] = this.GetBValue(VI);
      }
      VR[this.Size] = v;
      VG[this.Size] = v;
      VB[this.Size] = v;
      FRValues = VR;
      FGValues = VG;
      FBValues = VB;
    }

    public Boolean PresetConstant(Byte presetvalue,
                                  Int32 startindex,
                                  Int32 presetcount)
    {
      try
      {
        for (Int32 VI = startindex; VI <= startindex + presetcount; VI++)
        {
          FRValues[VI] = presetvalue;
          FGValues[VI] = presetvalue;
          FBValues[VI] = presetvalue;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------
    //	Segment - Overloading of Index-Operator
    //---------------------------------------
    //
    protected Byte GetValue(Int32 index)
    {
      if (CheckIndex(index))
      {
        return FRValues[index];
      }
      return INIT_ZERO;
    }
    protected void SetValue(Int32 index, Byte value)
    {
      if (CheckIndex(index))
      {
        FRValues[index] = value;
        FGValues[index] = value;
        FBValues[index] = value;
      }
    }
    public Byte this[Int32 index]
    {
      get { return GetValue(index); }
      set { SetValue(index, value); }
    }

    public Byte GetRValue(Int32 index)
    {
      if (CheckIndex(index))
      {
        return FRValues[index];
      }
      return INIT_ZERO;
    }
    public void SetRValue(Int32 index, Byte value)
    {
      if (CheckIndex(index))
      {
        FRValues[index] = value;
      }
    }

    public Byte GetGValue(Int32 index)
    {
      if (CheckIndex(index))
      {
        return FGValues[index];
      }
      return INIT_ZERO;
    }
    public void SetGValue(Int32 index, Byte value)
    {
      if (CheckIndex(index))
      {
        FGValues[index] = value;
      }
    }

    public Byte GetBValue(Int32 index)
    {
      if (CheckIndex(index))
      {
        return FBValues[index];
      }
      return INIT_ZERO;
    }
    public void SetBValue(Int32 index, Byte value)
    {
      if (CheckIndex(index))
      {
        FBValues[index] = value;
      }
    }
    //
    //----------------------------------------
    //	Segment - Operation
    //----------------------------------------
    //
    public override Boolean Normalize()
    {
      /*try
      {
        Double Sum2 = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum2 += this[VI] * this[VI];
        }
        Sum2 = Math.Sqrt(Sum2);
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = (Byte)(this[VI] / Sum2);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }*/
      return false;
    }

    public Boolean AddConstant(Byte value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        FRValues[VI] += value;
        FGValues[VI] += value;
        FBValues[VI] += value;
      }
      return true;
    }

    public Boolean SubtractConstant(Byte value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        FRValues[VI] -= value;
        FGValues[VI] -= value;
        FBValues[VI] -= value;
      }
      return true;
    }

    public Boolean MultiplyConstant(Byte value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        FRValues[VI] *= value;
        FGValues[VI] *= value;
        FBValues[VI] *= value;
      }
      return true;
    }

    public Boolean DivideConstant(Byte value)
    {
      if (0 != value)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          FRValues[VI] /= value;
          FGValues[VI] /= value;
          FBValues[VI] /= value;
        }
        return true;
      }
      return false;
    }

    public Boolean GetMeanValueR(out Byte value)
    {
      if (0 < this.Size)
      {
        Double Sum = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum += FRValues[VI];
        }
        value = (Byte)(Sum / this.Size);
        return true;
      }
      value = INIT_ZERO;
      return false;
    }
    public Boolean GetMeanValueG(out Byte value)
    {
      if (0 < this.Size)
      {
        Double Sum = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum += FGValues[VI];
        }
        value = (Byte)(Sum / this.Size);
        return true;
      }
      value = INIT_ZERO;
      return false;
    }
    public Boolean GetMeanValueB(out Byte value)
    {
      if (0 < this.Size)
      {
        Double Sum = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum += FBValues[VI];
        }
        value = (Byte)(Sum / this.Size);
        return true;
      }
      value = INIT_ZERO;
      return false;
    }

    // 
    public Boolean SubtractGreaterEqualR(Byte subtrahend, Byte border)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          FRValues[VI] = (Byte)Math.Max(border, (int)FRValues[VI] - (int)subtrahend);
        }
        return true;
      }
      return false;
    }

    public Boolean SubtractGreaterEqualG(Byte subtrahend, Byte border)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          FGValues[VI] = (Byte)Math.Max(border, (int)FGValues[VI] - (int)subtrahend);
        }
        return true;
      }
      return false;
    }

    public Boolean SubtractGreaterEqualB(Byte subtrahend, Byte border)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          FBValues[VI] = (Byte)Math.Max(border, (int)FBValues[VI] - (int)subtrahend);
        }
        return true;
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - Mathematical Operations
    //----------------------------------------
    //
    public Boolean FindThresholdValueLowerR(Byte threshold,
                                            out Int32 valueindex)
    {
      valueindex = 0;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        Double Value = FRValues[VI];
        if (Value < threshold)
        {
          valueindex = VI;
          return true;
        }
      }
      return false;
    }

    public Boolean FindThresholdValueLowerG(Byte threshold,
                                            out Int32 valueindex)
    {
      valueindex = 0;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        Double Value = FGValues[VI];
        if (Value < threshold)
        {
          valueindex = VI;
          return true;
        }
      }
      return false;
    }

    public Boolean FindThresholdValueLowerB(Byte threshold,
                                            out Int32 valueindex)
    {
      valueindex = 0;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        Double Value = FBValues[VI];
        if (Value < threshold)
        {
          valueindex = VI;
          return true;
        }
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    public void Write(String header)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]:", header, Size));
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          String Line = String.Format(" V[{0}] = {1:X02}",
                                      VI, this[VI]);
          FNotifier.Write(Line);
        }
      }
    }

  }
}
