﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace NLVector
{
  public class CVectorInt32 : CVectorBase
  { //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //
    private const Int32 INIT_ZERO = 0x0000;
    //
    //----------------------------------------
    //	Segment - Field
    //----------------------------------------
    //
    private Int32[] FValues = null;
    //
    //----------------------------------------
    //	Segment - Property
    //----------------------------------------
    //
    public Int32 VIL
    {
      get { return 0; }
    }
    public Int32 VIH
    {
      get { return Size - 1; }
    }
    public Int32 Size
    {
      get { return FValues.Length; }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorInt32(CNotifier notifier, Int32 size)
      : base(notifier)
    {
      FValues = new Int32[size];
			InitValues(INIT_ZERO);
		}

    public CVectorInt32(CNotifier notifier, Int32 size, Int32 initvalue)
      : base(notifier)
    {
      FValues = new Int32[size];
			InitValues(initvalue);
		}

    public CVectorInt32(CNotifier notifier, CVectorInt32 v)
      : base(notifier)
    {
      FValues = new Int32[v.Size];
			for (Int32 I = VIL; I <= VIH; I++)
			{
				FValues[I] = v[I];
			}
		}

    public CVectorInt32(CNotifier notifier, CVectorInt32 sourcevector, 
                         Int32 deltacount, Int32 copyoffset)
      : base(notifier)
    {
      FValues = new Int32[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      } else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorInt32(CNotifier notifier, 
                         CVectorInt32 indices, 
                         CVectorInt32 source)
      : base(notifier)
    {
      FValues = new Int32[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[(Int32)indices[VI]];
      }
    }

    public CVectorInt32(CNotifier notifier, Int32[] values)
      : base(notifier)
    {
      FValues = new Int32[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CVectorInt32(Int32 size)
      : base()
    {
      FValues = new Int32[size];
      InitValues(INIT_ZERO);
    }

    public CVectorInt32(Int32 size, Int32 initvalue)
      : base()
    {
      FValues = new Int32[size];
      InitValues(initvalue);
    }

    public CVectorInt32(CVectorInt32 v)
      : base()
    {
      FValues = new Int32[v.Size];
      for (Int32 I = VIL; I <= VIH; I++)
      {
        FValues[I] = v[I];
      }
    }

    public CVectorInt32(CVectorInt32 sourcevector,
                        Int32 deltacount, Int32 copyoffset)
      : base()
    {
      FValues = new Int32[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      }
      else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }

    public CVectorInt32(CVectorInt32 indices,
                        CVectorInt32 source)
      : base()
    {
      FValues = new Int32[indices.Size];
      for (Int32 VI = indices.VIL; VI <= indices.VIH; VI++)
      {
        FValues[VI] = source[(Int32)indices[VI]];
      }
    }

    public CVectorInt32(Int32[] values)
      : base()
    {
      FValues = new Int32[values.Length];
      for (Int32 VI = 0; VI < values.Length; VI++)
      {
        FValues[VI] = values[VI];
      }
    }
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    public bool InitValues(Int32 initvalue)
		{
			for (Int32 VI = VIL; VI <= VIH; VI++)
			{
				FValues[VI] = initvalue;
			}
			return true;
		}
		//
		//----------------------------------------
		//	Segment - Helper
		//----------------------------------------
		//
		protected Boolean CheckIndex(Int32 index)
		{
			return ((VIL <= index) & (index <= VIH));
		}

    public void Add(Byte v)
		{
      Int32[] V = new Int32[1 + this.Size];
			for (Int32 VI = 0; VI < Size; VI++)
			{
				V[VI] = this[VI];
			}
			V[this.Size] = v;
			FValues = V;
		}

    public Boolean PresetConstant(Int32 presetvalue, 
                                  Int32 startindex, 
                                  Int32 presetcount)
    {
      try
      {
        for (Int32 VI = startindex; VI <= startindex + presetcount; VI++)
        {
          FValues[VI] = presetvalue;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
		//
		//---------------------------------------
		//	Segment - Overloading of Index-Operator
		//---------------------------------------
		//
    protected Int32 GetValue(Int32 index)
		{
			if (CheckIndex(index))
			{
				return FValues[index];
			}
			return INIT_ZERO;
		}
    protected void SetValue(Int32 index, Int32 value)
		{
			if (CheckIndex(index))
			{
				FValues[index] = value;
			}
		}
    public Int32 this[Int32 index]
		{
			get { return GetValue(index); }
			set { SetValue(index, value); }
		}
    //
    //----------------------------------------
    //	Segment - Operation
    //----------------------------------------
    //
    public override Boolean Normalize()
    {
      try
      {
        Double Sum2 = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum2 += this[VI] * this[VI];
        }
        Sum2 = Math.Sqrt(Sum2);
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = (Int32)(this[VI] / Sum2);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean AddConstant(Int32 value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] += value;
      }
      return true;
    }

    public Boolean SubtractConstant(Int32 value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] -= value;
      }
      return true;
    }

    public Boolean MultiplyConstant(Int32 value)
    {
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        this[VI] *= value;
      }
      return true;
    }

    public Boolean DivideConstant(Int32 value)
    {
      if (0 != value)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] /= value;
        }
        return true;
      }
      return false;
    }

    public Boolean GetMeanValue(out Int32 value)
    {
      if (0 < this.Size)
      {
        Double Sum = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum += this[VI];
        }
        value = (Byte)(Sum / this.Size);
        return true;
      }
      value = INIT_ZERO;
      return false;
    }

    // 
    public Boolean SubtractGreaterEqual(Int32 subtrahend, Int32 border)
    {
      if (0 < this.Size)
      {
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = (Byte)Math.Max(border, (int)this[VI] - (int)subtrahend);
        }
        return true;
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - Mathematical Operations
    //----------------------------------------
    //
    public Boolean FindThresholdValueLower(Int32 threshold,
                                           out Int32 valueindex)
    {
      valueindex = 0;
      for (Int32 VI = VIL; VI <= VIH; VI++)
      {
        Double Value = this[VI];
        if (Value < threshold)
        {
          valueindex = VI;
          return true;
        }
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    public void Write(String header)
		{
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]:", header, Size));
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          String Line = String.Format(" V[{0}] = {1:X04}",
                                      VI, this[VI]);
          FNotifier.Write(Line);
        }
      }
		}

	}
}

