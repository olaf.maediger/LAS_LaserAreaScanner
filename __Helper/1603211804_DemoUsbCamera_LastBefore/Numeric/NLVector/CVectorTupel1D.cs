﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace NLVector
{
  /* until now NC
  public class CVectorTupel1D
  {
    //
    //----------------------------------------
    //	Segment - Field
    //----------------------------------------
    //
    private CNotifier FNotifier;
    private CTupel1Dlist FTupel1Dlist;
    //
    //----------------------------------------
    //	Segment - Constructor
    //----------------------------------------
    //
    public CVectorTupel1D(CNotifier notifier)
    {
      FNotifier = notifier;
      FTupel1Dlist = new CTupel1Dlist();
    }
    //
    //----------------------------------------
    //	Segment - Property
    //----------------------------------------
    //
    public Int32 Count
    {
      get { return FTupel1Dlist.Count; }
    }
    //
    //----------------------------------------
    //	Segment - Public Management
    //----------------------------------------
    //
    public Boolean Add(CTupel1D tupel1d)
    {
      if (tupel1d is CTupel1D)
      {
        FTupel1Dlist.Add(tupel1d);
        return true;
      }
      return false;
    }

    public Boolean Add(Double value)
    {
      FTupel1Dlist.Add(new CTupel1D(value));
      return true;
    }

    public Boolean ConvertToVectorDouble(out CVectorDouble vector)
    {
      vector = new CVectorDouble(FNotifier, Count);
      for (Int32 VI = 0; VI < Count; VI++)
      {
        CTupel1D Tupel1D = FTupel1Dlist[VI];
        vector[VI] = Tupel1D.X;
      }
      return true;
    }
  }*/
}
