﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLVector
{
  public class CTupel2D
  {
    private Double FX;
    private Double FY;

    public CTupel2D(Double x, Double y)
    {
      FX = x;
      FY = y;
    }

    public Double X
    {
      get { return FX; }
      set { FX = value; }
    }

    public Double Y
    {
      get { return FY; }
      set { FY = value; }
    }

  }
}
