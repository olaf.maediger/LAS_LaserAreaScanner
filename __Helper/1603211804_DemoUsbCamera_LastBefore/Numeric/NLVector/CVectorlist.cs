﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLVector
{
  public class CVectorlist : List<CVectorBase>
  {

    public Boolean FilterMinimumSizeZeroPositive(Int32 minimumsize)
    {
      const Double DVALUE_ZERO = 0.0;
      Boolean Result = true;
      for (Int32 VI = this.Count - 1; 0 <= VI; VI--)
      {
        CVectorBase Vector = this[VI];
        if (Vector is CVectorDouble)
        {
          if (((CVectorDouble)Vector).Size < minimumsize)
          {
            this.Remove(Vector);
          }
          else
          {
            Int32 ValueIndex;
            if (((CVectorDouble)Vector).FindThresholdValueLower(DVALUE_ZERO - 1, out ValueIndex))
            {
              this.Remove(Vector);
            }
          }
        }
      }
      return Result;
    }



    public Boolean FindMinimumMaximum(out Double minimum,
                                      out Double maximum)
    {
      minimum = +1E10;
      maximum = -1E10;
      Boolean Result = true;
      foreach (CVectorDouble Vector in this)
      {
        Result &= Vector.FindMinimumMaximumPreset(ref minimum, ref maximum);
      }
      return Result;
    }

    public Boolean FindColumnMinimumMaximum(Int32 colindex,
                                            out DateTime minimum,
                                            out DateTime maximum)
    {
      minimum = DateTime.MaxValue;
      maximum = DateTime.MinValue;
      Boolean Result = false;
      foreach (CVectorMixed VectorMixed in this)
      {
        Result = true;
        CMixedValue MixedValue = VectorMixed[colindex];
        if (MixedValue is CTimeValue)
        {
          DateTime Value = ((CTimeValue)MixedValue).Value;
          if (Value < minimum)
          {
            minimum = Value;
          } else
            if (maximum < Value)
            {
              maximum = Value;
            }
        } else
        { // nothing
        }
      }
      return Result;
    }


    public Boolean FilterSizeMinimum(Int32 minimumsize, out CVectorlist targetlist)
    {
      targetlist = new CVectorlist();
      foreach (CVectorDouble Vector in this)
      {
        if (minimumsize <= Vector.Size)
        {
          targetlist.Add(Vector);
        }
      }
      return true;
    }

    public Boolean FilterHeaderSection(String header,
                                       Int32 headersize,
                                       out CVectorDouble target)
    {
      target = null;
      for (Int32 VI = 0; VI < this.Count; VI++)
      {
        CVectorBase VectorSource = this[VI];
        if (VectorSource is CVectorHeader)
        {
          String Header = ((CVectorHeader)VectorSource).GetHeader();
          Int32 SubIndex = Math.Min(headersize, Header.Length);
          Header = Header.Substring(0, SubIndex);
          if (header == Header)
          {
            for (Int32 TI = 1 + VI; TI < this.Count; TI++)
            {
              CVectorBase VectorTarget = this[TI];
              if (VectorTarget is CVectorDouble)
              {
                target = new CVectorDouble((CVectorDouble)VectorTarget);
              } else
              {
                return (target is CVectorDouble);
              }
            }
          }
        }
      }
      return false;
    }

    public Boolean FilterHeaderSection(String header, 
                                       Int32 headersize,
                                       out CVectorlist targetlist)
    {
      targetlist = new CVectorlist();
      for (Int32 VI = 0; VI < this.Count; VI++)
      {
        CVectorBase VectorSource = this[VI];
        if (VectorSource is CVectorHeader)
        {
          String Header = ((CVectorHeader)VectorSource).GetHeader();
          Int32 SubIndex = Math.Min(headersize, Header.Length);
          Header = Header.Substring(0, SubIndex);
          if (header == Header)
          {
            for (Int32 TI = 1 + VI; TI < this.Count; TI++)
            {
              CVectorBase VectorTarget = this[TI];
              if (VectorTarget is CVectorDouble)
              {
                targetlist.Add(VectorTarget);
              } else
                if (VectorTarget is CVectorMixed)
                {
                  targetlist.Add(VectorTarget);
                } else
                {
                  return (0 < targetlist.Count);
                }
            }
          }
        }
      }
      return false;
    }



  }
}
