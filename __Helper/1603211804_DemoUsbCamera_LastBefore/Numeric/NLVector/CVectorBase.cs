﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using NLScalar;
//
namespace NLVector
{
  public abstract class CVectorBase
  {
    //
    //----------------------------------------
    //  Segment - Field
    //----------------------------------------
    //
    protected CNotifier FNotifier;
    private CObservablelist FObservablelist;
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorBase(CNotifier notifier)
    {
      FNotifier = notifier;
      FObservablelist = new CObservablelist();
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    // 
    public CVectorBase()
    {
      FNotifier = null;
      FObservablelist = new CObservablelist();
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //

    public Boolean AddObservable(String name,
                                 Double value,
                                 String unit)
    {
      return FObservablelist.Add(name, value, unit);
    }

    public abstract Boolean Normalize();
		public virtual Boolean IsDifferent(CVectorBase vector)
    {
      return false;
    }
  }
}
