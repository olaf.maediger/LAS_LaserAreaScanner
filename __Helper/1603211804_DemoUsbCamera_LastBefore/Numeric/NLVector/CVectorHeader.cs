﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace NLVector
{
  public class CVectorHeader : CVectorBase
  {
    //
    //--------------------------------------------
    //	Segment - Field
    //--------------------------------------------
    //
    private String[] FHeaders;
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorHeader(CNotifier notifier, String header)
      : base(notifier)
    {
      FHeaders = new String[1];
      FHeaders[0] = header;
    }

    public CVectorHeader(CNotifier notifier, String[] headers)
      : base(notifier)
    {
      FHeaders = headers;
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CVectorHeader(String header)
      : base()
    {
      FHeaders = new String[1];
      FHeaders[0] = header;
    }

    public CVectorHeader(String[] headers)
      : base()
    {
      FHeaders = headers;
    }
    //
    //--------------------------------------------
    //	Segment - Property
    //--------------------------------------------
    //
    public String GetHeader()
    {
      if (FHeaders is String[])
      {
        if (0 < FHeaders.Length)
        {
          return FHeaders[0];
        }
      }
      return "";
    }
    public String GetHeader(Int32 index)
    {
      if (FHeaders is String[])
      {
        if ((index <= 0) && (index < FHeaders.Length))
        {
          return FHeaders[index];
        }
      }
      return "";
    }

    //
    //----------------------------------------
    //	Segment - Operation
    //----------------------------------------
    //
    public override Boolean Normalize()
    {
      return false;
      /*try
      {
        Double Sum2 = 0;
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          Sum2 += this[VI] * this[VI];
        }
        Sum2 = Math.Sqrt(Sum2);
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          this[VI] = (Byte)(this[VI] / Sum2);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }*/
    }

  }
}
