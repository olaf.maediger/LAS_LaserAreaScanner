﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLVector
{
  class CTupel1D
  {
    private Double FX;

    public CTupel1D(Double x)
    {
      FX = x;
    }

    public Double X
    {
      get { return FX; }
      set { FX = value; }
    }

  }
}
