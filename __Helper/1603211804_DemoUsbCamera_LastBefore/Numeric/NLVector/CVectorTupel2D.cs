﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
//
namespace NLVector
{
  public class CVectorTupel2D : CVectorBase
  {
    //
    //----------------------------------------
    //	Constant
    //----------------------------------------
    //
    private const Double INIT_VALUE = 0.0;
    //
    //----------------------------------------
    //	Member
    //----------------------------------------
    //
    private CTupel2Dlist FTupel2Dlist;
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CVectorTupel2D(CNotifier notifier)
      : base(notifier)
    {
      FTupel2Dlist = new CTupel2Dlist();
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CVectorTupel2D()
      : base()
    {
      FTupel2Dlist = new CTupel2Dlist();
    }
    //
    //----------------------------------------
    //	Segment - Property
    //----------------------------------------
    //
    public Int32 VIL
    {
      get { return FTupel2Dlist.VIL; }
    }
    public Int32 VIH
    {
      get { return FTupel2Dlist.VIH; }
    }
    public Int32 Size
    {
      get { return FTupel2Dlist.Size; }
    }

    public Int32 Count
    {
      get { return FTupel2Dlist.Count; }
    }

    public CTupel2D this[Int32 index]
    {
      get { return FTupel2Dlist[index]; }
      set { FTupel2Dlist[index] = value; }
    }
    //
    //----------------------------------------
    //	Segment - Helper
    //----------------------------------------
    //
    protected Boolean CheckIndex(Int32 index)
    {
      return ((VIL <= index) & (index <= VIH));
    }
    //
    //----------------------------------------
    //	Segment - Public Management
    //----------------------------------------
    //
    public override Boolean Normalize()
    { // !!!!!!!!!!!!!!!!!!!!!!!!!!!! fill it!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      return true;
    }

    public Boolean Clear()
    {
      FTupel2Dlist.Clear();
      return (0 == Count);
    }

    public Boolean Add(CTupel2D tupel2d)
    {
      if (tupel2d is CTupel2D)
      {
        FTupel2Dlist.Add(tupel2d);
        return true;
      }
      return false;
    }

    public Boolean Add(Double x, Double y)
    {
      FTupel2Dlist.Add(new CTupel2D(x, y));
      return true;
    }

    public Boolean ConvertToVectorDoubleXY(out CVectorDouble vectorx,
                                           out CVectorDouble vectory)
    {
      vectorx = new CVectorDouble(FNotifier, Count);
      vectory = new CVectorDouble(FNotifier, Count);
      for (Int32 VI = 0; VI < Count; VI++)
      {
        CTupel2D Tupel2D = FTupel2Dlist[VI];
        vectorx[VI] = Tupel2D.X;
        vectory[VI] = Tupel2D.Y;
      }
      return true;
    }


  }
}
