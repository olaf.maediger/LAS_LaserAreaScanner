﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLParametricCurve
{ // *** Arc *** (Kreisbogen)
  // Parametric Equation 
  // - Two Points P0 = (x0, y0), P1 = (x1, y1)) for Start/End-Angle
  //      alpha = arctan((y0 - yC) / (x0 - xC))
  //      beta = arctan((y1 - yC) / (x1 - xC))
  // - Middlepoint PC = (xC, yC) and Radius R
  // - Parameter t : t in [0,1]
  // x(t) = xC + R cos(alpha + (beta - alpha) t)
  // y(t) = yC + R sin(alpha + (beta - alpha) t)
  //
  public class CArc : CParametric
  { //
    //----------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------
    //
    private const Int32 INIT_COUNTRESOLUTION = 20;
    private const Double INIT_XREAL_C = 0.0;
    private const Double INIT_YREAL_C = 0.0;
    private const Double INIT_XREAL_0 = 0.0;
    private const Double INIT_YREAL_0 = 1.0;
    private const Double INIT_XREAL_1 = 1.0;
    private const Double INIT_YREAL_1 = 0.0;
    //
    //----------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------
    // local real coordinates of line
    private Double FXC, FYC;
    private Double FX0, FY0;
    private Double FX1, FY1;
    //
    //----------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------
    //
    public CArc(CRealPixel realpixel)
      : base(realpixel)
    {
      FXC = INIT_XREAL_C;
      FYC = INIT_YREAL_C;
      FX0 = INIT_XREAL_0;
      FY0 = INIT_YREAL_0;
      FX1 = INIT_XREAL_1;
      FY1 = INIT_YREAL_1;
      CountResolution = INIT_COUNTRESOLUTION;
      BuildRealVector();
      BuildPixelVector();
    }

    public CArc(CRealPixel realpixel,
                Double rxm, Double rym,
                Double rx0, Double ry0,
                Double rx1, Double ry1)
      : base(realpixel)
    {
      FXC = rxm;
      FYC = rym;
      FX0 = rx0;
      FY0 = ry0;
      FX1 = rx1;
      FY1 = ry1;
      CountResolution = INIT_COUNTRESOLUTION;
      BuildRealVector();
      BuildPixelVector();
    }

    public CArc(CRealPixel realpixel,
                Double rxm, Double rym,
                Double rx0, Double ry0,
                Double rx1, Double ry1,
                Int32 countresolution)
      : base(realpixel)
    {
      FXC = rxm;
      FYC = rym;
      FX0 = rx0;
      FY0 = ry0;
      FX1 = rx1;
      FY1 = ry1;
      CountResolution = countresolution;
      BuildRealVector();
      BuildPixelVector();
    }



    private Double GetRadius()
    {
      return Math.Sqrt((FX0 - FXC) * (FX0 - FXC) + (FY0 - FYC) * (FY0 - FYC));
    }
    public Double R
    {
      get { return GetRadius(); }
    }

    private Double GetAngle0()
    {
      return CMath.ArcTan2(FY0 - FYC, FX0 - FXC); 
    }
    public Double A0
    {
      get { return GetAngle0(); }
    }

    private Double GetAngle1()
    {
      return CMath.ArcTan2(FY1 - FYC, FX1 - FXC);
    }
    public Double A1
    {
      get { return GetAngle1(); }
    }

    private Double GetDX()
    {
      Double XL = Math.Min(FXC, FX0);
      XL = Math.Min(XL, FX1);
      Double XH = Math.Max(FXC, FX0);
      XL = Math.Max(XH, FX1);
      return FX1 - FX0;
    }
    public Double DX
    {
      get { return GetDX(); }
    }

    private Double GetDY()
    {
      Double YL = Math.Min(FYC, FY0);
      YL = Math.Min(YL, FY1);
      Double YH = Math.Max(FYC, FY0);
      YL = Math.Max(YH, FY1);
      return FY1 - FY0;
    }
    public Double DY
    {
      get { return GetDY(); }
    }

    protected override Double GetLength()
    {
      return R * (A1 - A0);
    }

    public override Boolean BuildRealVector()
    {
      try
      {
        Double Radius = R;
        Console.WriteLine(String.Format("A0[deg]:{0} A1[deg]:{1}", 
                          CMath.RadianDegree(A0), CMath.RadianDegree(A1)));
        Double DA;
        if (A0 < A1)
        { // Rotation positive 
          DA = (A1 - A0) / (FCountResolution - 1);
          for (Int32 Index = 0; Index < FCountResolution; Index++)
          {
            Double A = A0 + Index * DA;
            Double XR = FXC + Radius * Math.Cos(A);
            Double YR = FYC + Radius * Math.Sin(A);
            //
            FXRealVector[Index] = XR;
            FYRealVector[Index] = YR;
          }
        }
        else
        { // A1 < A0 - Rotation neagtive
          Double PID = 2.0 * Math.PI;
          DA = (PID - A0 + A1) / (FCountResolution - 1);
          Int32 IL = 0;
          for (Int32 Index = 0; Index < FCountResolution; Index++)
          {
            // NC Double T = Index * 2 * DT;
            Double A;
            A = A0 + DA * Index;
            if (PID <= A)
            {
              A = DA * (Index - IL);
            }
            else
            {
              IL += 1;
            }
            Double XR = FXC + Radius * Math.Cos(A);
            Double YR = FYC + Radius * Math.Sin(A);
            FXRealVector[Index] = XR;
            FYRealVector[Index] = YR;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      const Int32 R = 2;
      Int32 IH = FXPixelVector.Length - 1;
      Pen PL = new Pen(Color.Blue, 3);
      Pen PP = new Pen(Color.Red, 1);
      Int32 R2 = 2 * R;
      for (Int32 Index = 0; Index < IH; Index++)
      {
        Int32 XP = FXPixelVector[Index];
        graphics.DrawLine(PL, FXPixelVector[Index], FYPixelVector[Index],
                          FXPixelVector[1 + Index], FYPixelVector[1 + Index]);
        graphics.DrawEllipse(PP, FXPixelVector[Index] - R, FYPixelVector[Index] - R, R2, R2);
      }
      graphics.DrawEllipse(PP, FXPixelVector[IH] - R, FYPixelVector[IH] - R, R2, R2);
      return true;

    }
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //




  }
}
