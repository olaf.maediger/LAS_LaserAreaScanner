﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLParametricCurve
{
  public class CParametricList : List<CParametric>
  { //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private CRealPixel FRealPixel;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CParametricList(CRealPixel realpixel)
    {
      FRealPixel = realpixel;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    public CRealPixel RealPixel
    {
      get { return FRealPixel; }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public void Draw(Graphics graphics)
    {
      foreach (CParametric P in this)
      {
        P.Draw(graphics);
      }
    }

    public void BuildPixelVector()
    {
      foreach (CParametric P in this)
      {
        P.BuildPixelVector();
      }
    }

    //
    //---------------------------------------------------------------------------
    //  Segment - Public
    //---------------------------------------------------------------------------
    //
    public Boolean AddCircle(Double rxm, Double rym,
                             Double rr,
                             Int32 countresolution)
    {
      CCircle Circle = new CCircle(FRealPixel, rxm, rym, rr, countresolution);
      Add(Circle);
      return true;
    }

    public Boolean AddCircle(Double rxm, Double rym,
                             Double rx0, Double ry0,
                             Int32 countresolution)
    {
      CCircle Circle = new CCircle(FRealPixel, rxm, rym, rx0, ry0, countresolution);
      Add(Circle);
      return true;
    }

    public Boolean AddCircle(CCircle circle)
    {
      if (circle is CCircle)
      {
        Add(circle);
        return true;
      }
      return false;
    }

  }
}
