﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLParametricCurve
{
  public enum ERayTransition
  {
    PositiveEntry,
    PositiveExit,
    NegativeEntry,
    NegativeExit
  };

  public class CIntersectionPoint
  {
    private Double FXS;
    private Double FYS;
    private ERayTransition FRayTransition;

    public CIntersectionPoint(Double xs, Double ys,
                              ERayTransition raytransition)
    {
      FXS = xs;
      FYS = ys;
      FRayTransition = raytransition;
    }

    public Double XS
    {
      get { return FXS; }
    }

    public Double YS
    {
      get { return FYS; }
    }

    public ERayTransition RayTransition
    {
      get { return FRayTransition; }
    }
  };

  public class CIntersectionPoints : List<CIntersectionPoint>
  {
    public Boolean Add(Double xs, Double ys, 
                       ERayTransition raytransition)
    {
      base.Add(new CIntersectionPoint(xs, ys, raytransition));
      return true;
    }

    public Boolean Add(Double[] xs, Double[] ys, ERayTransition[] raytransitions)
    {
      if ((xs is Double[]) && (ys is Double[]) && 
          (raytransitions is ERayTransition[]))
      {
        Int32 SIH = Math.Min(xs.Length, ys.Length);
        SIH = Math.Min(SIH, raytransitions.Length);
        for (Int32 SI = 0; SI < SIH; SI++)
        {
          base.Add(new CIntersectionPoint(xs[SI], ys[SI], raytransitions[SI]));
        }
        return (0 < SIH);
      }
      return false;
    }

  }
}
