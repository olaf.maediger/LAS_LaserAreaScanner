﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLParametricCurve
{ // *** Envelope *** (Kreisevolvente)
  //
  // *** Envelope Equation Parameter Free
  // - Parameter t : t in (-oo..+oo)
  // - Middlepoint PC = (xC, yC) and Radius R
  // - x(t) = xC + R [cos(2 PI t) + 2 PI t * sin(2 PI t)]
  // - y(t) = yC + R [sin(2 PI t) - 2 PI t * cos(2 PI t)]
  // - S = 2 PI^2 R (t1^2 - t0^2)
  //
  public class CEnvelopeFree : CParametric
  {
    //
    //----------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------
    //
    private const Double INIT_T0 = 0.0;
    private const Double INIT_T1 = 1.0;
    private const Double INIT_R = 1.0;
    private const Double INIT_XC = 0.0;
    private const Double INIT_YC = 0.0;
    //
    //----------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------
    // local parameter - Point Middle
    private Double FXC, FYC;
    // local parameter - Radius
    private Double FR;
    // local parameter - Parameter Start / End
    private Double FT0, FT1; 
    //
    //----------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------
    //
    public CEnvelopeFree(CRealPixel realpixel)
      : base(realpixel)
    {
      FT0 = INIT_T0;
      FT1 = INIT_T1;
      FR = INIT_R;
      FXC = INIT_XC;
      FYC = INIT_YC;
      BuildRealVector();
      BuildPixelVector();
    }

    public Double XC
    {
      get { return FXC; }
      set { FXC = value; }
    }
    public Double YC
    {
      get { return FYC; }
      set { FYC = value; }
    }

    public Double T0
    {
      get { return FT0; }
      set { FT0 = value; }
    }
    public Double T1
    {
      get { return FT1; }
      set { FT1 = value; }
    }

    public Double R
    {
      get { return FR; }
      set { FR = value; }
    }

    protected override Double GetLength()
    { // 2 PI^2 R (t1^2 - t0^2)
      return Math.Sqrt(2 * Math.PI * Math.PI * R * (T1 * T1 - T0 * T0));
    }

    // - x(t) = xC + R [cos(2 PI t) + 2 PI t * sin(2 PI t)]
    // - y(t) = yC + R [sin(2 PI t) - 2 PI t * cos(2 PI t)]
    public override Boolean BuildRealVector()
    {
      try
      { // here: free parameter
        //Console.WriteLine(String.Format("A0:{0} A1:{1}", A0, A1));
        Double DT = (FT1 - FT0) / (FCountResolution - 0);
        for (Int32 Index = 0; Index <= FCountResolution; Index++)
        {
          Double T = FT0 + Index * DT;
          Double A = 2 * Math.PI * T;
          Double CA = Math.Cos(A);
          Double SA = Math.Sin(A);
          Double XR = FXC + R * (CA + A * SA);
          Double YR = FYC + R * (SA - A * CA);     
          //
          FXRealVector[Index] = XR;
          FYRealVector[Index] = YR;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      const Int32 R = 1;
      Int32 IH = FXPixelVector.Length - 1;
      Pen PL = new Pen(Color.FromArgb(0xFF, 0xEE, 0xCC, 0xDD), 3);
      Pen PP = new Pen(Color.FromArgb(0xFF, 0xFF, 0x55, 0x66), 1);
      Int32 R2 = 2 * R;
      for (Int32 Index = 0; Index < IH; Index++)
      {
        Int32 XP = FXPixelVector[Index];
        graphics.DrawLine(PL, FXPixelVector[Index], FYPixelVector[Index],
                          FXPixelVector[1 + Index], FYPixelVector[1 + Index]);
        graphics.DrawEllipse(PP, FXPixelVector[Index] - R, FYPixelVector[Index] - R, R2, R2);
      }
      graphics.DrawEllipse(PP, FXPixelVector[IH] - R, FYPixelVector[IH] - R, R2, R2);
      return true;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //

  }
}
