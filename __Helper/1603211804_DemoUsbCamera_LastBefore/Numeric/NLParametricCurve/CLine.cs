﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLParametricCurve
{
  // *** Line *** (Gerade)
  // Parametric Equation 
  // - two Points P0 = (x0, y0), P1 = (x1, y1)
  // - Parameter t : t in (-oo, +oo) (!!! compare with Section !!!)
  // x(t) = x0 + (x1 - x0) t
  // y(t) = y0 + (y1 - y0) t
  //
 
  public class CLine : CParametric
  {
    //
    //----------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------
    //
    private const Int32 INIT_COUNTRESOLUTION = 2;
    private const Double INIT_XREAL_0 = 1.0;
    private const Double INIT_YREAL_0 = 1.0;
    private const Double INIT_XREAL_1 = 2.0;
    private const Double INIT_YREAL_1 = 2.0;
    //
    //----------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------
    // local parameter
    private Double FX0, FX1;
    private Double FY0, FY1;
    private Double FXS0, FYS0, FXS1, FYS1;
    //
    //----------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------
    //
    public CLine(CRealPixel realpixel)
      : base(realpixel)
    {
      FX0 = INIT_XREAL_0;
      FY0 = INIT_YREAL_0;
      FX1 = INIT_XREAL_1;
      FY1 = INIT_YREAL_1;
      CountResolution = INIT_COUNTRESOLUTION;
      BuildRealVector();
      BuildPixelVector();
    }

    public CLine(CRealPixel realpixel,
                 Double x0, Double y0,
                 Double x1, Double y1)
      : base(realpixel)
    {
      FX0 = x0;
      FY0 = y0;
      FX1 = x1;
      FY1 = y1;
      CountResolution = INIT_COUNTRESOLUTION;
      BuildRealVector();
      BuildPixelVector();
    }

    public Double X0
    {
      get { return FX0; }
      set { FX0 = value; }
    }
    public Double Y0
    {
      get { return FY0; }
      set { FY0 = value; }
    }
    public Double X1
    {
      get { return FX1; }
      set { FX1 = value; }
    }
    public Double Y1
    {
      get { return FY1; }
      set { FY1 = value; }
    }

    protected override Double GetLength()
    {
      return Math.Sqrt((FX1 - FX0) * (FX1 - FX0) + (FY1 - FY0) * (FY1 - FY0));
    }

    public override Boolean BuildRealVector()
    {
      try
      {
        Double DT = 1.0 / (FCountResolution - 1);
        Double DX = FX1 - FX0;
        Double DY = FY1 - FY0;
        for (Int32 Index = 0; Index < FCountResolution; Index++)
        {
          Double T = Index * DT;
          Double XR = FX0 + DX * T;
          Double YR = FY0 + DY * T;
          //
          FXRealVector[Index] = XR;
          FYRealVector[Index] = YR;
        }
        // add two intersections of border of real world
        // this Line
        CLine Line = this;
        // Intersection S - L
        CIntersection Intersection = new CIntersection();
        Double SS, TS, XS, YS;
        //------------------------------------------
        // Left Border
        //------------------------------------------
        CSegment Section = new CSegment(FRealPixel);
        Section.X0 = FRealPixel.XRealMinimum;
        Section.Y0 = FRealPixel.YRealMinimum;
        Section.X1 = FRealPixel.XRealMinimum;
        Section.Y1 = FRealPixel.YRealMaximum;
        if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
        {
          FXS0 = XS; FYS0 = YS; 
          // Right Border
          Section.X0 = FRealPixel.XRealMaximum;
          Section.Y0 = FRealPixel.YRealMinimum;
          Section.X1 = FRealPixel.XRealMaximum;
          Section.Y1 = FRealPixel.YRealMaximum;
          if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
          {
            FXS1 = XS; FYS1 = YS;
            return true;
          } else
          { // Top Border
            Section.X0 = FRealPixel.XRealMinimum;
            Section.Y0 = FRealPixel.YRealMaximum;
            Section.X1 = FRealPixel.XRealMaximum;
            Section.Y1 = FRealPixel.YRealMaximum;
            if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
            {
              FXS1 = XS; FYS1 = YS;
              return true;
            } else
            { // Bottom Border
              Section.X0 = FRealPixel.XRealMinimum;
              Section.Y0 = FRealPixel.YRealMinimum;
              Section.X1 = FRealPixel.XRealMaximum;
              Section.Y1 = FRealPixel.YRealMinimum;
              if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
              {
                FXS1 = XS; FYS1 = YS;
                return true;
              }        
            }
          }
          //Error
          return false;
        } 
        //------------------------------------------
        // Bottom Border
        //------------------------------------------
        Section.X0 = FRealPixel.XRealMinimum;
        Section.Y0 = FRealPixel.YRealMinimum;
        Section.X1 = FRealPixel.XRealMaximum;
        Section.Y1 = FRealPixel.YRealMinimum;
        if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
        {
          FXS0 = XS; FYS0 = YS;
          // Right Border
          Section.X0 = FRealPixel.XRealMaximum;
          Section.Y0 = FRealPixel.YRealMinimum;
          Section.X1 = FRealPixel.XRealMaximum;
          Section.Y1 = FRealPixel.YRealMaximum;
          if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
          {
            FXS1 = XS; FYS1 = YS;
            return true;
          } else
          { // Top Border
            Section.X0 = FRealPixel.XRealMinimum;
            Section.Y0 = FRealPixel.YRealMaximum;
            Section.X1 = FRealPixel.XRealMaximum;
            Section.Y1 = FRealPixel.YRealMaximum;
            if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
            {
              FXS1 = XS; FYS1 = YS;
              return true;
            } else
            { // Left Border
              Section.X0 = FRealPixel.XRealMinimum;
              Section.Y0 = FRealPixel.YRealMinimum;
              Section.X1 = FRealPixel.XRealMinimum;
              Section.Y1 = FRealPixel.YRealMaximum;
              if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
              {
                FXS1 = XS; FYS1 = YS;
                return true;
              } 
            }
          }
          //Error
          return false;
        } 
        //------------------------------------------
        // Bottom Top
        //------------------------------------------
        Section.X0 = FRealPixel.XRealMinimum;
        Section.Y0 = FRealPixel.YRealMaximum;
        Section.X1 = FRealPixel.XRealMaximum;
        Section.Y1 = FRealPixel.YRealMaximum;
        if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
        {
          FXS0 = XS; FYS0 = YS;
          // Right Border
          Section.X0 = FRealPixel.XRealMaximum;
          Section.Y0 = FRealPixel.YRealMinimum;
          Section.X1 = FRealPixel.XRealMaximum;
          Section.Y1 = FRealPixel.YRealMaximum;
          if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
          {
            FXS1 = XS; FYS1 = YS; 
            return true;
          }
          else
          { // Bottom Border
            Section.X0 = FRealPixel.XRealMinimum;
            Section.Y0 = FRealPixel.YRealMinimum;
            Section.X1 = FRealPixel.XRealMaximum;
            Section.Y1 = FRealPixel.YRealMinimum;
            if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
            {
              FXS1 = XS; FYS1 = YS;
              return true;
            }
            else
            { // Left Border
              Section.X0 = FRealPixel.XRealMinimum;
              Section.Y0 = FRealPixel.YRealMinimum;
              Section.X1 = FRealPixel.XRealMinimum;
              Section.Y1 = FRealPixel.YRealMaximum;
              if (Intersection.SectionLineIterative(Section, Line, 0.001, out SS, out TS, out XS, out YS))
              {
                FXS1 = XS; FYS1 = YS;
                return true;
              }
            }
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      const Int32 LINE_WIDTH = 3;
      const Int32 R = 2;
      //
      Color ColorLineBackground = Color.FromArgb(0x22, 0xEE, 0x88, 0x33);
      Color ColorLine = Color.FromArgb(0xFF, 0xFF, 0x66, 055);
      Color ColorPoint = Color.FromArgb(0xFF, 0xAA, 0x66, 0x88);
      //
      Pen PL = new Pen(ColorLineBackground, LINE_WIDTH);
      Int32 XPL = FRealPixel.XRealPixel(FXS0);
      Int32 YPL = FRealPixel.YRealPixel(FYS0);
      Int32 XPH = FRealPixel.XRealPixel(FXS1);
      Int32 YPH = FRealPixel.YRealPixel(FYS1);
      graphics.DrawLine(PL, XPL, YPL, XPH, YPH);
      //
      Int32 IH = FXPixelVector.Length - 1;
      PL = new Pen(ColorLine, LINE_WIDTH);
      Brush BP = new SolidBrush(ColorPoint);
      Int32 R2 = 2 * R;
      //
      for (Int32 Index = 0; Index < IH; Index++)
      {
        Int32 XP = FXPixelVector[Index];
        graphics.DrawLine(PL, FXPixelVector[Index], FYPixelVector[Index],
                          FXPixelVector[1 + Index], FYPixelVector[1 + Index]);
        graphics.FillEllipse(BP, FXPixelVector[Index] - R, FYPixelVector[Index] - R, R2, R2);
      }
      graphics.FillEllipse(BP, FXPixelVector[IH] - R, FYPixelVector[IH] - R, R2, R2);
      return true;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //


  }
}
