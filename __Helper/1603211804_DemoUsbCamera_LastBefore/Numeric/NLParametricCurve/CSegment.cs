﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLParametricCurve
{ // *** Section *** (Strecke)
  // Parametric Equation 
  // - two Points P0 = (x0, y0), P1 = (x1, y1))
  // - Parameter t : t in [0,1] (!!! compare with Line !!!)
  // x(t) = x0 + (x1 - x0) t
  // y(t) = y0 + (y1 - y0) t
  //
  public class CSegment : CParametric
  { //
    //----------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------
    //
    private const Int32 INIT_COUNTRESOLUTION = 2;
    private const Double INIT_XREAL_0 = 0.0;
    private const Double INIT_YREAL_0 = 1.0;
    private const Double INIT_XREAL_1 = 1.0;
    private const Double INIT_YREAL_1 = 0.0;
    //
    //----------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------
    // local real parameter
    private Double FX0, FY0;  // Point0
    private Double FX1, FY1;  // Point1
    //
    //----------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------
    //
    public CSegment(CRealPixel realpixel)
      : base(realpixel)
    {
      FX0 = INIT_XREAL_0;
      FY0 = INIT_YREAL_0;
      FX1 = INIT_XREAL_1;
      FY1 = INIT_YREAL_1;
      CountResolution = INIT_COUNTRESOLUTION;
      BuildRealVector();
      BuildPixelVector();
    }

    public CSegment(CRealPixel realpixel,
                    Double x0, Double y0,
                    Double x1, Double y1)
      : base(realpixel)
    {
      FX0 = x0;
      FY0 = y0;
      FX1 = x1;
      FY1 = y1;
      CountResolution = INIT_COUNTRESOLUTION;
      BuildRealVector();
      BuildPixelVector();
    }

    public CSegment(CRealPixel realpixel,
                    Double x0, Double y0,
                    Double x1, Double y1,
                    Int32 countresolution)
      : base(realpixel)
    {
      FX0 = x0;
      FY0 = y0;
      FX1 = x1;
      FY1 = y1;
      CountResolution = countresolution;
      BuildRealVector();
      BuildPixelVector();
    }



    

    public Double X0
    {
      get { return FX0; }
      set { FX0 = value; }
    }
    public Double Y0
    {
      get { return FY0; }
      set { FY0 = value; }
    }
    public Double X1
    {
      get { return FX1; }
      set { FX1 = value; }
    }
    public Double Y1
    {
      get { return FY1; }
      set { FY1 = value; }
    }

    protected override Double GetLength()
    {
      return Math.Sqrt((FX1 - FX0) * (FX1 - FX0) + (FY1 - FY0) * (FY1 - FY0));
    }

    public override Boolean BuildRealVector()
    {
      try
      {
        Double DT = 1.0 / (FCountResolution - 1);
        Double DX = FX1 - FX0;
        Double DY = FY1 - FY0;
        for (Int32 Index = 0; Index < FCountResolution; Index++)
        {
          Double T = Index * DT;
          Double XR = FX0 + DX * T;
          Double YR = FY0 + DY * T;
          //
          FXRealVector[Index] = XR;
          FYRealVector[Index] = YR;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      const Int32 R = 2;
      Int32 IH = FXPixelVector.Length - 1;
      Pen PL = new Pen(Color.Blue, 3);
      Pen PP = new Pen(Color.Red, 1);
      Int32 R2 = 2 * R;
      for (Int32 Index = 0; Index < IH; Index++)
      {
        Int32 XP = FXPixelVector[Index];
        graphics.DrawLine(PL, FXPixelVector[Index], FYPixelVector[Index],
                          FXPixelVector[1 + Index], FYPixelVector[1 + Index]);
        graphics.DrawEllipse(PP, FXPixelVector[Index] - R, FYPixelVector[Index] - R, R2, R2);
      }
      graphics.DrawEllipse(PP, FXPixelVector[IH] - R, FYPixelVector[IH] - R, R2, R2);
      return true;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //








  }
}
