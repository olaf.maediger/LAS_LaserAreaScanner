﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLScalar
{
  public class CObservable
  {
    private String FName;
    public String Name
    {
      get { return FName; }
      set { FName = value; }
    }

    private Double FValue;
    public Double Value
    {
      get { return FValue; }
      set { FValue = value; }
    }

    private String FUnit;
    public String Unit
    {
      get { return FUnit; }
      set { FUnit = value; }
    }

    public CObservable(String name,
                       Double value,
                       String unit)
    {
      Name = name;
      Value = value;
      Unit = unit;
    }

  }
}
