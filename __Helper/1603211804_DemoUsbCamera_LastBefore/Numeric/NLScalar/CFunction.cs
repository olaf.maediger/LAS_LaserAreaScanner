﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NLScalar
{
  public class CFunction
  {
    public const Double INIT_EPSILON_DOUBLE = 0.000000000000001;
    public const Decimal INIT_EPSILON_DECIMAL = 0.000000000000000000000000001M;
    public const Decimal PIHALF = (Decimal)(Math.PI / 2.0);

    public static Boolean IsEqual(Double source, Double target)
    {
      Double Delta = INIT_EPSILON_DOUBLE;
      Double Difference = Math.Abs(source - target);
      if (Delta < Difference)
      {
        return true;
      }
      return false;
    }
    public static Boolean IsEqual(Double source, Double target, Double delta)
    {
      Double Delta = Math.Abs(delta);
      Double Difference = Math.Abs(source - target);
      if (Delta < Difference)
      {
        return true;
      }
      return false;
    }

    public static Boolean IsEqual(Decimal source, Decimal target)
    {
      Decimal Delta = INIT_EPSILON_DECIMAL;
      Decimal Difference = Math.Abs(source - target);
      if (Delta < Difference)
      {
        return true;
      }
      return false;
    }
    public static Boolean IsEqual(Decimal source, Decimal target, Decimal delta)
    {
      Decimal Delta = Math.Abs(delta);
      Decimal Difference = Math.Abs(source - target);
      if (Delta < Difference)
      {
        return true;
      }
      return false;
    }

    public static Double Clamp(Double source, Double borderlow, Double borderhigh)
    {
      if (source <= borderlow)
      {
        return borderlow;
      }
      if (borderhigh <= source)
      {
        return borderhigh;
      }
      return source;
    }

    public static Double RadDeg(Double rad)
    {
      return (180.0 * rad / Math.PI);
    }

    public static Double DegRad(Double deg)
    {
      return (Math.PI * deg / 180.0);
    }

  }
}
