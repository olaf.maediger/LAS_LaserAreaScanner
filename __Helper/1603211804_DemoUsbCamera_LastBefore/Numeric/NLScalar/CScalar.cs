﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;

namespace NLScalar
{		
  public class CScalar
  { //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //
    private const Double INIT_VALUE = 0.0;
    //
    //----------------------------------------
    //	Segment - Member
    //----------------------------------------
    //
    static private CNotifier FNotifier;
    private Double FValue = INIT_VALUE;
    //
    //----------------------------------------
    //	Segment - Property
    //----------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }

    public Double Value
    {
      get { return FValue; }
      set { FValue = value; }
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - With Notifier
    //--------------------------------------------
    //
    public CScalar(CNotifier notifier)
    {
      FNotifier = notifier;
      FValue = INIT_VALUE;
    }

    public CScalar(CNotifier notifier, Double initvalue)
    {
      FNotifier = notifier;
      FValue = initvalue;
    }

    public CScalar(CNotifier notifier, CScalar scalar)
    {
      FNotifier = notifier;
      FValue = scalar.Value;
    }
    //
    //--------------------------------------------
    //	Segment - Constructor - Without Notifier
    //--------------------------------------------
    //
    public CScalar()
    {
      FNotifier = null;
      FValue = INIT_VALUE;
    }

    public CScalar(Double initvalue)
    {
      FNotifier = null;
      FValue = initvalue;
    }

    public CScalar(CScalar scalar)
    {
      FNotifier = null;
      FValue = scalar.Value;
    }
    //
    //----------------------------------------
    //	Segment - Operator
    //----------------------------------------
    //
    // "+"
    public static CScalar operator +(CScalar sl, CScalar sr)
    {
      CScalar S = new CScalar(FNotifier, sl.Value + sr.Value);
      return S;
    }

    public static CScalar operator +(CScalar sl, Double vr)
    {
      return new CScalar(FNotifier, sl.Value + vr);
    }

    public static CScalar operator +(Double vl, CScalar sr)
    {
      return new CScalar(FNotifier, vl + sr.Value);
    }

    // "-"
    public static CScalar operator -(CScalar sl, CScalar sr)
    {
      return new CScalar(FNotifier, sl.Value - sr.Value);
    }

    public static CScalar operator -(CScalar sl, Double vr)
    {
      return new CScalar(FNotifier, sl.Value - vr);
    }

    public static CScalar operator -(Double vl, CScalar sr)
    {
      return new CScalar(FNotifier, vl - sr.Value);
    }

    // "+=" - automatically

    // "-=" - automatically

    //
    //----------------------------------------
    //	Protocol
    //----------------------------------------
    //
    public void Write(String header)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}:", header));
        String Line = String.Format(" V = {0,7:0.000}", Value);
        FNotifier.Write(Line);
      }
    }

  }
}
