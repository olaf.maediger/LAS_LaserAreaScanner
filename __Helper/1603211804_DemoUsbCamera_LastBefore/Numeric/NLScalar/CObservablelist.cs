﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLScalar
{
  public class CObservablelist : List<CObservable>
  {
    public Boolean Add(String name,
                           Double value,
                           String unit)
    {
      CObservable Observable = new CObservable(name, value, unit);
      if (Observable is CObservable)
      {
        base.Add(Observable);
        return true;
      }
      return false;
    }

  }
}
