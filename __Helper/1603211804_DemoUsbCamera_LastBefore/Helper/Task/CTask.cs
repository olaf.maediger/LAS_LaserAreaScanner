﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
namespace Task
{
  public delegate void DOnExecutionStart(RTaskData data);
  public delegate void DOnExecutionBusy(RTaskData data);
  public delegate void DOnExecutionEnd(RTaskData data);
  public delegate void DOnExecutionAbort(RTaskData data);

  public struct RTaskData
  {
    public String Name;
    public Int32 Counter;
    public Boolean IsActive;
    public Boolean IsAborted;
    public DOnExecutionStart OnExecutionStart;
    public DOnExecutionBusy OnExecutionBusy;
    public DOnExecutionEnd OnExecutionEnd;
    public DOnExecutionAbort OnExecutionAbort;
  }

  public class CTask
  {
    private RTaskData FData;
    private Thread FThread;

    public CTask(String name,
                 DOnExecutionStart onexecutionstart,
                 DOnExecutionBusy onexecutionbusy,
                 DOnExecutionEnd onexecutionend,
                 DOnExecutionAbort onexecutionabort)
    {
      FData.Name = name;
      FData.OnExecutionStart = onexecutionstart;
      FData.OnExecutionBusy = onexecutionbusy;
      FData.OnExecutionEnd = onexecutionend;
      FData.OnExecutionAbort = onexecutionabort;
      FData.Counter = 0;
      FData.IsAborted = false;
      FData.IsActive = false;
      FThread = null;
      // debug String Line = String.Format("### Task[{0}].Created", FData.Name);
      // debug FNotifier.Write(Line);
    }

   /* NC  public CTask(String name)
    {
      FData.Name = name;
      FData.OnExecutionStart = null;
      FData.OnExecutionBusy = null;
      FData.OnExecutionEnd = null;
      FData.OnExecutionAbort = null;
      FData.Counter = 0;
      FData.IsAborted = false;
      FData.IsActive = false;
      FThread = null;
      // debug String Line = String.Format("### Task[{0}].Created", FData.Name);
      // debug FNotifier.Write(Line);
    }

    
    public void SetOnExecutionStart(DOnExecutionStart value)
    {
      FData.OnExecutionStart = value;
    }
    public void SetOnExecutionStart(DOnExecutionBusy value)
    {
      FData.OnExecutionBusy = value;
    }
    public void SetOnExecutionStart(DOnExecutionEnd value)
    {
      FData.OnExecutionEnd = value;
    }
    public void SetOnExecutionStart(DOnExecutionAbort value)
    {
      FData.OnExecutionAbort = value;
    }*/



    public Boolean IsActive()
    {
      return FData.IsActive;
    }

    public Boolean IsAborted()
    {
      return FData.IsAborted;
    }

    private void OnExecute()
    {
      // debug String Line = String.Format("### Task[{0}].OnExecute() - S", FData.Name);
      // debug FNotifier.Write(Line);
      FData.IsActive = true;
      if (FData.OnExecutionStart is DOnExecutionStart)
      {
        FData.OnExecutionStart(FData);
      }
      if (FData.OnExecutionBusy is DOnExecutionBusy)
      {
        FData.OnExecutionBusy(FData);
      }
      if (FData.OnExecutionEnd is DOnExecutionEnd)
      {
        FData.OnExecutionEnd(FData);
      }
      FThread = null;
      FData.IsActive = false;
      // debug Line = String.Format("### Task[{0}].OnExecute() - E", FData.Name);
      // debug FNotifier.Write(Line);
    }

    public Boolean Start()
    {
      // debug String Line = String.Format("### Task[{0}].Start() - S", FData.Name);
      // debug FNotifier.Write(Line);
      FData.Counter += 1;
      // RO FData.Name 
      FThread = new Thread(OnExecute);
      FThread.Name = FData.Name;
      FThread.Start();
      // debug Line = String.Format("### Task[{0}].Start() - E", FData.Name);
      // debug FNotifier.Write(Line);
      return true;
    }

    public Boolean Abort()
    {
      // debug String Line = String.Format("### Task[{0}].Abort() - S", FData.Name);
      // debug FNotifier.Write(Line);
      try
      {
        if (FThread is Thread)
        {
          FThread.Abort();
          FData.IsAborted = true;
          FData.IsActive = false;
          if (FData.OnExecutionAbort is DOnExecutionAbort)
          {
            FData.OnExecutionAbort(FData);
          }
          if (ThreadState.Aborted == FThread.ThreadState)
          {
            FThread = null;
            // debug Line = String.Format("### Task[{0}].Abort() - E", FData.Name);
            // debug FNotifier.Write(Line);
            return true;
          }
        }
        FThread = null;
        // debug Line = String.Format("### Task[{0}].Abort() - Error <null>!!!", FData.Name);
        // debug FNotifier.Write(Line);
        return false;
      }
      catch (Exception)
      {
        FThread = null;
        // debug Line = String.Format("### Task[{0}].Abort() - Error<exception>!!!", FData.Name);
        // debug FNotifier.Write(Line);
        return false;
      }
    }

  }
}
