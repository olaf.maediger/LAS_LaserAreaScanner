﻿namespace UCViewImage
{
  partial class CUCViewImage
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlTopLeft = new System.Windows.Forms.Panel();
      this.pnlTopRight = new System.Windows.Forms.Panel();
      this.pnlTop = new System.Windows.Forms.Panel();
      this.FUCScrollTransparency = new UCControl.CUCScrollTransparency();
      this.FUCScrollRotation = new UCScrollRotation.CUCScrollRotation();
      this.panel8 = new System.Windows.Forms.Panel();
      this.panel9 = new System.Windows.Forms.Panel();
      this.panel10 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.FUCScrollZoom = new UCScrollZoom.CUCScrollZoom();
      this.FUCFrame = new UCFrame.CUCFrame();
      this.FUCView = new UCView.CUCView();
      this.pnlTop.SuspendLayout();
      this.panel8.SuspendLayout();
      this.FUCFrame.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlTopLeft
      // 
      this.pnlTopLeft.BackColor = System.Drawing.SystemColors.InactiveCaption;
      this.pnlTopLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlTopLeft.Location = new System.Drawing.Point(0, 0);
      this.pnlTopLeft.Name = "pnlTopLeft";
      this.pnlTopLeft.Size = new System.Drawing.Size(16, 34);
      this.pnlTopLeft.TabIndex = 2;
      // 
      // pnlTopRight
      // 
      this.pnlTopRight.BackColor = System.Drawing.SystemColors.InactiveCaption;
      this.pnlTopRight.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnlTopRight.Location = new System.Drawing.Point(620, 0);
      this.pnlTopRight.Name = "pnlTopRight";
      this.pnlTopRight.Size = new System.Drawing.Size(16, 34);
      this.pnlTopRight.TabIndex = 4;
      // 
      // pnlTop
      // 
      this.pnlTop.Controls.Add(this.FUCScrollTransparency);
      this.pnlTop.Controls.Add(this.FUCScrollRotation);
      this.pnlTop.Controls.Add(this.pnlTopRight);
      this.pnlTop.Controls.Add(this.pnlTopLeft);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(636, 34);
      this.pnlTop.TabIndex = 3;
      // 
      // FUCScrollTransparency
      // 
      this.FUCScrollTransparency.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCScrollTransparency.Location = new System.Drawing.Point(16, 16);
      this.FUCScrollTransparency.Name = "FUCScrollTransparency";
      this.FUCScrollTransparency.Size = new System.Drawing.Size(604, 16);
      this.FUCScrollTransparency.TabIndex = 6;
      this.FUCScrollTransparency.Transparency = 1F;
      // 
      // FUCScrollRotation
      // 
      this.FUCScrollRotation.AngleDeg = 0D;
      this.FUCScrollRotation.AngleRad = 0D;
      this.FUCScrollRotation.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCScrollRotation.Location = new System.Drawing.Point(16, 0);
      this.FUCScrollRotation.Name = "FUCScrollRotation";
      this.FUCScrollRotation.Size = new System.Drawing.Size(604, 16);
      this.FUCScrollRotation.TabIndex = 5;
      // 
      // panel8
      // 
      this.panel8.Controls.Add(this.panel9);
      this.panel8.Controls.Add(this.panel10);
      this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel8.Location = new System.Drawing.Point(0, 538);
      this.panel8.Name = "panel8";
      this.panel8.Size = new System.Drawing.Size(636, 16);
      this.panel8.TabIndex = 12;
      this.panel8.Visible = false;
      // 
      // panel9
      // 
      this.panel9.BackColor = System.Drawing.SystemColors.InactiveCaption;
      this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel9.Location = new System.Drawing.Point(620, 0);
      this.panel9.Name = "panel9";
      this.panel9.Size = new System.Drawing.Size(16, 16);
      this.panel9.TabIndex = 4;
      this.panel9.Visible = false;
      // 
      // panel10
      // 
      this.panel10.BackColor = System.Drawing.SystemColors.InactiveCaption;
      this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel10.Location = new System.Drawing.Point(0, 0);
      this.panel10.Name = "panel10";
      this.panel10.Size = new System.Drawing.Size(16, 16);
      this.panel10.TabIndex = 2;
      // 
      // panel3
      // 
      this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel3.Location = new System.Drawing.Point(620, 34);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(16, 504);
      this.panel3.TabIndex = 14;
      this.panel3.Visible = false;
      // 
      // FUCScrollZoom
      // 
      this.FUCScrollZoom.Dock = System.Windows.Forms.DockStyle.Left;
      this.FUCScrollZoom.Location = new System.Drawing.Point(0, 34);
      this.FUCScrollZoom.Name = "FUCScrollZoom";
      this.FUCScrollZoom.Size = new System.Drawing.Size(16, 504);
      this.FUCScrollZoom.TabIndex = 16;
      this.FUCScrollZoom.ZoomFactor = 1D;
      // 
      // FUCFrame
      // 
      this.FUCFrame.AutoScroll = true;
      this.FUCFrame.Controls.Add(this.FUCView);
      this.FUCFrame.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCFrame.Location = new System.Drawing.Point(16, 34);
      this.FUCFrame.Name = "FUCFrame";
      this.FUCFrame.Size = new System.Drawing.Size(604, 504);
      this.FUCFrame.TabIndex = 17;
      this.FUCFrame.Paint += new System.Windows.Forms.PaintEventHandler(this.FUCFrame_Paint);
      this.FUCFrame.Resize += new System.EventHandler(this.FUCFrame_Resize);
      // 
      // FUCView
      // 
      this.FUCView.BackColor = System.Drawing.Color.Transparent;
      this.FUCView.Location = new System.Drawing.Point(52, 57);
      this.FUCView.Name = "FUCView";
      this.FUCView.Size = new System.Drawing.Size(494, 371);
      this.FUCView.TabIndex = 20;
      this.FUCView.Paint += new System.Windows.Forms.PaintEventHandler(this.FUCView_Paint);
      this.FUCView.Resize += new System.EventHandler(this.FUCView_Resize);
      // 
      // CUCViewImage
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCFrame);
      this.Controls.Add(this.FUCScrollZoom);
      this.Controls.Add(this.panel3);
      this.Controls.Add(this.panel8);
      this.Controls.Add(this.pnlTop);
      this.Name = "CUCViewImage";
      this.Size = new System.Drawing.Size(636, 554);
      this.pnlTop.ResumeLayout(false);
      this.panel8.ResumeLayout(false);
      this.FUCFrame.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlTopLeft;
    private System.Windows.Forms.Panel pnlTopRight;
    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.Panel panel8;
    private System.Windows.Forms.Panel panel9;
    private System.Windows.Forms.Panel panel10;
    private System.Windows.Forms.Panel panel3;
    private UCScrollZoom.CUCScrollZoom FUCScrollZoom;
    private UCScrollRotation.CUCScrollRotation FUCScrollRotation;
    private UCFrame.CUCFrame FUCFrame;
    private UCView.CUCView FUCView;
    private UCControl.CUCScrollTransparency FUCScrollTransparency;
  }
}
