﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using NLRealPixel;
using NLGraphicElement;
using IPMemory;
using IPPalette256;
using Initdata;
using UCControl;
using UCEditor;
//
namespace UCViewImage
{
  public partial class CUCViewImage : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCViewImage";
    //
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private Bitmap FBitmapSource; // External Bitmap, no changes
    private Bitmap FBitmapTarget; // Internal Bitmap, temporary build from Source, Buffer to show
    private Bitmap FBitmapBackground; // Overlay Bitmap with Transparency
    private Double FZoomFactor;   // [0.01 .. 1.0 .. 10.0]
    private Double FRotationAngle; // [rad]
    private float FTransparency;  // [0..1]
    private CElementList FPreElementList;
    private CElementList FPostElementList;
    //
    private CRealPixel FRealPixel;
    private CDialogReal FDialogReal;
    //
    private CGrid FGrid;
    private CDialogGrid FDialogGrid;
    //
    private CTextRow FTitle;
    private CDialogTitle FDialogTitle;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCViewImage()
    {
      InitializeComponent();
      //
      OnZoomChanged(1.0);
      OnRotationChanged(0.0);
      OnTransparencyChanged(1f);
      //
      FUCScrollZoom.SetOnZoomChanged(OnZoomChanged);
      FUCScrollRotation.SetOnRotationChanged(OnRotationChanged);
      FUCScrollTransparency.SetOnTransparencyChanged(OnTransparencyChanged);
      // Constructor / Callback - Child
      FPreElementList = new CElementList();
      FPostElementList = new CElementList();
      //
      FRealPixel = new CRealPixel();
      FRealPixel.SetOnUnitScaleChanged(RealPixelOnUnitScaleChanged);
      FRealPixel.SetOnRealExtremaChanged(RealPixelOnRealExtremaChanged);
      FRealPixel.SetOnRealResolutionChanged(RealPixelOnRealResolutionChanged);
      //      
      FDialogReal = new CDialogReal();
      FDialogReal.SetOnUnitScaleChanged(DialogRealOnUnitScaleChanged);
      FDialogReal.SetOnMinimumChanged(DialogRealOnMinimumChanged);
      FDialogReal.SetOnMaximumChanged(DialogRealOnMaximumChanged);
      FDialogReal.Synchronize(FRealPixel);
      //
      FDialogGrid = new CDialogGrid();
      FDialogGrid.StartPosition = FormStartPosition.CenterParent;
      FDialogGrid.SetOnShowGridChanged(DialogGridOnShowGridChanged);
      FDialogGrid.SetOnShowLinesHorizontalChanged(DialogGridOnShowLinesHorizontalChanged);
      FDialogGrid.SetOnShowLinesVerticalChanged(DialogGridOnShowLinesVerticalChanged);
      FDialogGrid.SetOnOriginChanged(DialogGridOnOriginChanged);
      FDialogGrid.SetOnSizeChanged(DialogGridOnSizeChanged);
      FDialogGrid.SetOnStepCountChanged(DialogGridOnStepCountChanged);
      FDialogGrid.SetOnLineWidthRowColChanged(DialogGridOnLineWidthRowColChanged);
      FDialogGrid.SetOnColorRowColChanged(DialogGridOnColorRowColChanged);
      // Property - Child
      FRealPixel.UnitScale = EUnitScale.UM;
      FRealPixel.SetRealExtrema(0.0, 0.0, 10.0);
      //
      FGrid = new CGrid(FRealPixel,
                        FDialogGrid.OriginX, FDialogGrid.OriginY,
                        FDialogGrid.SizeX, FDialogGrid.SizeY,
                        FDialogGrid.StepCountX, FDialogGrid.StepCountY);
      FPostElementList.Add(FGrid);
      //
      FDialogTitle = new CDialogTitle();
      FDialogTitle.StartPosition = FormStartPosition.CenterParent;
      // DialogTitle - Text
      FDialogTitle.SetOnShowTextChanged(DialogTitleOnShowTextChanged);
      FDialogTitle.SetOnOriginChanged(DialogTitleOnOriginChanged);
      FDialogTitle.SetOnSizeChanged(DialogTitleOnSizeChanged);
      FDialogTitle.SetOnScaleFactorChanged(DialogTitleOnScaleFactorChanged);
      FDialogTitle.SetOnTextChanged(DialogTitleOnTextChanged);
      FDialogTitle.SetOnFontColorOpacityChanged(DialogTitleOnFontColorOpacityChanged);
      FDialogTitle.SetOnFontChanged(DialogTitleOnFontChanged);
      // DialogTitle - Background
      FDialogTitle.SetOnShowBackgroundChanged(DialogTitleOnShowBackgroundChanged);
      FDialogTitle.SetOnBackgroundColorOpacityChanged(DialogTitleOnBackgroundColorOpacityChanged);
      // DialogTitle - Frame
      FDialogTitle.SetOnShowFrameChanged(DialogTitleOnShowFrameChanged);
      FDialogTitle.SetOnLineWidthFrameChanged(DialogTitleOnLineWidthFrameChanged);
      FDialogTitle.SetOnColorOpacityFrameChanged(DialogTitleOnColorOpacityFrameChanged);
      //
      FTitle = new CTextRow(FRealPixel);
      FPostElementList.Add(FTitle);
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public Form DialogReal
    {
      get { return FDialogReal; }
    }

    public Form DialogGrid
    {
      get { return FDialogGrid; }
    }

    public Form DialogTitle
    {
      get { return FDialogTitle; }
    }

    public void SetBitmap(Bitmap bitmap)
    {
      FBitmapSource = bitmap;
      OnZoomChanged(FZoomFactor);
    }
    //
    //---------------------------------------------------------------------------------------
    // Set Bitmap (no Palette, Truecolor)
    //---------------------------------------------------------------------------------------
    //
    public void SetBitmapRgb(CBitmapRgb bitmaprgb)
    { 
      CMemoryBitmapRgbBitmapTrueColor.CopyBitmapRgbToBitmap32bpp(bitmaprgb, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetBitmapArgb(CBitmapArgb bitmapargb)
    { 
      CMemoryBitmapArgbBitmapTrueColor.CopyBitmapArgbToBitmap32bpp(bitmapargb, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetBitmap256(CBitmap256 bitmap256)
    { 
      CMemoryBitmap256BitmapTrueColor.CopyBitmap256ToBitmap32bpp(bitmap256, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetMatrix08Bit(CMatrix08Bit matrix08Bit)
    {
      CMemoryMatrix08BitBitmapTrueColor.CopyMatrix08BitToBitmap32bpp(matrix08Bit, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetMatrix10Bit(CMatrix10Bit matrix10Bit)
    {
      CMemoryMatrix10BitBitmapTrueColor.CopyMatrix10BitToBitmap32bpp(matrix10Bit, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetMatrix12Bit(CMatrix12Bit matrix12Bit)
    {
      CMemoryMatrix12BitBitmapTrueColor.CopyMatrix12BitToBitmap32bpp(matrix12Bit, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetMatrix16Bit(CMatrix16Bit matrix16Bit)
    {
      CMemoryMatrix16BitBitmapTrueColor.CopyMatrix16BitToBitmap32bpp(matrix16Bit, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }
    //
    //---------------------------------------------------------------------------------------
    // Set Bitmap with Palette256
    //---------------------------------------------------------------------------------------
    //
    public void SetBitmapRgbPalette256(CBitmapRgb bitmaprgb, CPalette256 palette256)
    {
      CMemoryBitmapRgbBitmapPaletteColor.
        CopyBitmapRgbPalette256ToBitmap32bpp(bitmaprgb, 100, palette256, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetBitmapArgbPalette256(CBitmapArgb bitmapargb, CPalette256 palette256)
    {
      CMemoryBitmapArgbBitmapPaletteColor.
        CopyBitmapArgbPalette256ToBitmap32bpp(bitmapargb, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetBitmap256Palette256(CBitmap256 bitmap256, CPalette256 palette256)
    {
      // !!!!!!!!!!!!!!!!!!!!! CMemory.CopyBitmap256Palette256ToBitmap32bpp(bitmap256, palette256, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetMatrix08BitPalette256(CMatrix08Bit matrix08bit, CPalette256 palette256)
    {
      // !!!!!!!!!!!!!!!!!!!!! CMemory.CopyMatrix08BitPalette256ToBitmap32bpp(matrix08bit, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetMatrix10BitPalette256(CMatrix10Bit matrix10bit, CPalette256 palette256)
    {
      // !!!!!!!!!!!!!!!!!!!!! CMemory.CopyMatrix10BitPalette256ToBitmap32bpp(matrix10bit, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetMatrix12BitPalette256(CMatrix12Bit matrix12bit, CPalette256 palette256)
    {
      // !!!!!!!!!!!!!!!!!!!!! CMemory.CopyMatrix12BitPalette256ToBitmap32bpp(matrix12bit, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetMatrix16BitPalette256(CMatrix16Bit matrix16bit, CPalette256 palette256)
    {
      // !!!!!!!!!!!!!!!!!!!!! CMemory.CopyMatrix16BitPalette256ToBitmap32bpp(matrix16bit, 100, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }










    public void SetMatrix08Bit(CMatrix08Bit matrix08Bit, CPalette256 palette256)
    {
      CMemory.CopyMatrix08BitPalette256ToBitmap32bpp(matrix08Bit, 100, palette256, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetBitmapSource(CMatrix08Bit matrix08Bit, CPalette256 palette256)
    {
      CMemory.CopyMatrix08BitPalette256ToBitmap32bpp(matrix08Bit, 100, palette256, out FBitmapSource);
      OnZoomChanged(FZoomFactor);
    }

    public void SetBitmapBackground(Bitmap bitmap)
    { 
      FBitmapBackground = bitmap;
      //
      //BuildTargetFromSource();
      FUCView.Refresh();
    }

    public void SetOnDialogRealChanged(DOnDialogRealChanged value)
    {
      FDialogReal.SetOnDialogRealChanged(value);
    }

    public void SetOnDialogGridChanged(DOnDialogGridChanged value)
    {
      FDialogGrid.SetOnDialogGridChanged(value);
    }

    public void SetOnDialogTitleChanged(DOnDialogTitleChanged value)
    {
      FDialogTitle.SetOnDialogTitleChanged(value);
    }

    public float BitmapTransparency
    {
      get { return FUCScrollTransparency.Transparency; }
      set { FUCScrollTransparency.Transparency = value; }
    }
    ////
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY + header);
      //
      Result &= FUCScrollZoom.LoadInitdata(initdata, "");
      Result &= FUCScrollRotation.LoadInitdata(initdata, "");
      Result &= FUCScrollTransparency.LoadInitdata(initdata, "");
      //
      Result &= FDialogReal.LoadInitdata(initdata);
      Result &= FDialogGrid.LoadInitdata(initdata);
      Result &= FDialogTitle.LoadInitdata(initdata);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY + header);
      //
      Result &= FUCScrollZoom.SaveInitdata(initdata, "");
      Result &= FUCScrollRotation.SaveInitdata(initdata, "");
      Result &= FUCScrollTransparency.SaveInitdata(initdata, "");
      //
      Result &= FDialogReal.SaveInitdata(initdata);
      Result &= FDialogGrid.SaveInitdata(initdata);
      Result &= FDialogTitle.SaveInitdata(initdata);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------------------------
    //
    private Boolean BuildTargetFromSource()
    {
      Boolean Result = false;
      if (FBitmapSource is Bitmap)
      {
        float BTW = (float)(FBitmapSource.Width * FZoomFactor);
        float BTH = (float)(FBitmapSource.Height * FZoomFactor);
        float DX = FUCView.ClientRectangle.Width / 2f;
        float DY = FUCView.ClientRectangle.Height / 2f;
        //
        if (!(FBitmapTarget is Bitmap))
        {
          FBitmapTarget = new Bitmap(FUCView.Width, FUCView.Height, PixelFormat.Format32bppArgb);
          FRealPixel.SetPixelExtrema((Int32)BTW, (Int32)BTH);
          FPostElementList.BuildRealVector();
          FPostElementList.BuildPixelVector();
        } 
        else
          if ((FBitmapTarget.Width != FUCView.Width) || (FBitmapTarget.Height != FUCView.Height))
          {
            FBitmapTarget = new Bitmap(FUCView.Width, FUCView.Height, PixelFormat.Format32bppArgb);
            FRealPixel.SetPixelExtrema((Int32)BTW, (Int32)BTH);
            FPostElementList.BuildRealVector();
            FPostElementList.BuildPixelVector();
          }
        // Allocate Graphics
        Graphics GT = Graphics.FromImage(FBitmapTarget);        // Init
        GT.ResetTransform();
        GT.TranslateTransform((float)DX, (float)DY);
        GT.RotateTransform((float)(FRotationAngle * 180.0 / Math.PI));
        DX = BTW / 2;
        DY = BTH / 2;
        GT.TranslateTransform((float)-DX, (float)-DY);
        //
        ColorMatrix CM = new ColorMatrix();
        CM.Matrix33 = BitmapTransparency;
        ImageAttributes IA = new ImageAttributes();
        IA.SetColorMatrix(CM, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
        // Background
        if (!(FBitmapBackground is Bitmap))
        {
          Brush BB = new SolidBrush(Color.White);
          GT.FillRectangle(BB, 0, 0, BTW, BTH);
          BB.Dispose();
        }
        else
        {
          GT.DrawImage(FBitmapBackground,
                       new Rectangle(0, 0, (Int32)BTW, (Int32)BTH),
                       0f, 0f, (float)FBitmapBackground.Width, (float)FBitmapBackground.Height,
                       GraphicsUnit.Pixel);
        }
        // Draw Pre-Elements
        //if (FPreElementList is CElementList)
        //{
        //  FPreElementList.Draw(GT);
        //}
        // Copy Source -> Target
        GT.DrawImage(FBitmapSource,
                     new Rectangle(0, 0, (Int32)BTW, (Int32)BTH),
                     0f, 0f, (float)FBitmapSource.Width, (float)FBitmapSource.Height,
                     GraphicsUnit.Pixel, IA);
        // Draw Post-Elements
        if (FPostElementList is CElementList)
        {
          FPostElementList.Draw(GT);
        }
        // Free Graphics
        GT.Dispose();
        //
        Result = true;
      }
      return Result;
    }

    private Boolean DrawTarget(Graphics graphics)
    {
      try
      {
        if (FBitmapTarget is Bitmap)
        {
          graphics.DrawImage(FBitmapTarget, 0, 0);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        // ErroR!!!!!! Insufficient Memory !!!!
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void FUCFrame_Paint(object sender, PaintEventArgs e)
    {
      // NC 
    }
    private void FUCFrame_Resize(object sender, EventArgs e)
    {
      FUCView_Resize(this, null);
    }

    private void FUCView_Paint(object sender, PaintEventArgs e)
    {
      DrawTarget(e.Graphics);
    }

    private void FUCView_Resize(object sender, EventArgs e)
    {
      if (FBitmapSource is Bitmap)
      {
        Double BTW = FBitmapSource.Width * FZoomFactor;
        Double BTH = FBitmapSource.Height * FZoomFactor;
        Double CA = Math.Cos(FRotationAngle);
        Double SA = Math.Sin(FRotationAngle);
        Double TPW = Math.Abs(BTW * CA) + Math.Abs(BTH * SA);
        Double TPH = Math.Abs(BTW * SA) + Math.Abs(BTH * CA);
        //if (FUCFrame.HorizontalScroll.Visible) 
        if (FUCFrame.Width <= BTW)
        {
          // MUELL FUCView.Top = 0;
          FUCView.Height = (Int32)TPH;
        }
        else
        {
          // MUELL FUCView.Top = 0;
          FUCView.Height = (Int32)TPH;
          FUCView.Top = (Int32)(FUCFrame.Height / 2 - FUCView.Height / 2);
        }
        //if (FUCFrame.VerticalScroll.Visible) 
        if (FUCFrame.Height <= BTH)
        {
          // MUELL FUCView.Left = 0;
          FUCView.Width = (Int32)TPW;
        }
        else
        {
          // MUELL FUCView.Left = 0;
          FUCView.Width = (Int32)TPW;
          FUCView.Left = (Int32)(FUCFrame.Width / 2 - FUCView.Width / 2);
        }
      }
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Callback
    //--------------------------------------------------------------------------------------------
    //
    private void OnZoomChanged(Double zoomfactor)
    { // debug Console.WriteLine(String.Format("{0}", zoomfactor));
      FZoomFactor = zoomfactor;
      FUCView_Resize(this, null);
      BuildTargetFromSource();
      FUCView.Refresh();
    }

    private void OnRotationChanged(Double rotationangle)
    { // debug Console.WriteLine(String.Format("{0}", rotationangle));
      FRotationAngle = rotationangle;
      FUCView_Resize(this, null);
      BuildTargetFromSource();
      FUCView.Refresh();
    }

    private void OnTransparencyChanged(float transparency)
    { // debug Console.WriteLine(String.Format("{0}", transparency));
      FTransparency = transparency;
      FUCView_Resize(this, null);
      BuildTargetFromSource();
      FUCView.Refresh();
    }
    //
    //---------------------------------------------------------------------------------------
    // Segment - Callback - RealPixel
    //---------------------------------------------------------------------------------------
    //
    private void RealPixelOnUnitScaleChanged(EUnitScale unitscale)
    {
      if (FDialogReal is CDialogReal)
      {
        FDialogReal.UnitScale = unitscale;
      }
      if (FDialogGrid is CDialogGrid)
      {
        FDialogGrid.UnitScale = unitscale;
      }
    }

    private void RealPixelOnRealExtremaChanged(Double xminimum, Double yminimum,
                                               Double xmaximum, Double ymaximum)
    {
    }

    private void RealPixelOnRealResolutionChanged(Int32 stepcountx, Int32 stepcounty)
    {
    }
    //
    //---------------------------------------------------------------------------------------
    // Segment - Callback - DialogReal
    //---------------------------------------------------------------------------------------
    //
    private void DialogRealOnUnitScaleChanged(EUnitScale unitscale)
    {
      FRealPixel.UnitScale = unitscale;
      if (FDialogGrid is CDialogGrid)
      {
        FDialogGrid.UnitScale = unitscale;
      }
      Refresh();
    }

    private void DialogRealOnMinimumChanged(Double xminimum, Double yminimum)
    {
      FRealPixel.SetRealMinimum(xminimum, yminimum);
      Refresh();
    }

    private void DialogRealOnMaximumChanged(Double xmaximum, Double ymaximum)
    {
      FRealPixel.SetRealMaximum(xmaximum, ymaximum);
      Refresh();
    }
    //
    //---------------------------------------------------------------------------------------
    // Segment - Callback - DialogGrid
    //---------------------------------------------------------------------------------------
    //
    private void DialogGridOnShowGridChanged(Boolean show)
    {
      FGrid.ShowGrid = show;
      Refresh();
    }
    private void DialogGridOnShowLinesHorizontalChanged(Boolean show)
    {
      FGrid.ShowLinesHorizontal = show;
      Refresh();
    }
    private void DialogGridOnShowLinesVerticalChanged(Boolean show)
    {
      FGrid.ShowLinesVertical = show;
      Refresh();
    }

    private void DialogGridOnOriginChanged(Double originx, Double originy)
    {
      if (FGrid is CGrid)
      {
        FGrid.SetOrigin(originx, originy);
        Refresh();
      }
    }

    private void DialogGridOnSizeChanged(Double sizex, Double sizey)
    {
      if (FGrid is CGrid)
      {
        FGrid.SetSize(sizex, sizey);
        Refresh();
      }
    }

    private void DialogGridOnStepCountChanged(Int32 stepcountx, Int32 stepcounty)
    {
      if (FGrid is CGrid)
      {
        FGrid.SetStepCount(stepcountx, stepcounty);
        Refresh();        
      }
    }

    private void DialogGridOnLineWidthRowColChanged(Int32 linewidthrow, Int32 linewidthcol)
    {
      if (FGrid is CGrid)
      {
        FGrid.SetLineWidth(linewidthrow, linewidthcol);
        Refresh();
      }
    }

    private void DialogGridOnColorRowColChanged(Color colorrow, Color colorcol)
    {
      if (FGrid is CGrid)
      {
        FGrid.SetColorRowCol(colorrow, colorcol);
        Refresh();
      }
    }
    //
    //---------------------------------------------------------------------------------------
    // Segment - Callback - DialogTitle
    //---------------------------------------------------------------------------------------
    //
    private void DialogTitleOnShowTextChanged(Boolean showtext)
    {
      if (FTitle is CTextRow)
      {
        FTitle.SetShowText(showtext);
        Refresh();
      }
    }

    private void DialogTitleOnOriginChanged(Double originx, Double originy)
    {
      if (FTitle is CTextRow)
      {
        FTitle.SetOrigin(originx, originy);
        Refresh();
      }
    }

    private void DialogTitleOnSizeChanged(Double sizex, Double sizey)
    {
      if (FTitle is CTextRow)
      {
        FTitle.SetSize(sizex, sizey);
        Refresh();
      }
    }

    private void DialogTitleOnTextChanged(String text)
    {
      if (FTitle is CTextRow)
      {
        FTitle.SetText(text);
        Refresh();
      }
    }

    private void DialogTitleOnFontColorOpacityChanged(Color color, Byte opacity)
    {
      Byte CA = (Byte)(opacity * 255f / 100f);
      Color CO = Color.FromArgb(CA, color.R, color.G, color.B);
      if (FTitle is CTextRow)
      {
        FTitle.SetColorText(CO);
        Refresh();
      }
    }

    private void DialogTitleOnFontChanged(Font font)
    {
      if (FTitle is CTextRow)
      {
        FTitle.SetFont(font);
        Refresh();
      }
    }
    //
    //---------------------------------------------------------------------------------------
    // Segment - Callback - DialogTitle - Background
    //---------------------------------------------------------------------------------------
    //
    private void DialogTitleOnShowBackgroundChanged(Boolean show)
    {
      if (FTitle is CTextRow)
      {
        FTitle.ShowBackground = show;
        Refresh();
      }
    }

    private void DialogTitleOnBackgroundColorOpacityChanged(Color color, Byte opacity)
    {
      Byte CA = (Byte)(opacity * 255f / 100f);
      Color CO = Color.FromArgb(CA, color.R, color.G, color.B);
      if (FTitle is CTextRow)
      {
        FTitle.SetColorBackground(CO);
        Refresh();
      }
    }

    private void DialogTitleOnScaleFactorChanged(Double scalefactorx, Double scalefactory)
    {
      if (FTitle is CTextRow)
      {
        FTitle.SetScaleFactor(scalefactorx, scalefactory);
        Refresh();
      }
    }
    //
    //---------------------------------------------------------------------------------------
    // Segment - Callback - DialogTitle - Frame
    //---------------------------------------------------------------------------------------
    //
    private void DialogTitleOnShowFrameChanged(Boolean show)
    {
      if (FTitle is CTextRow)
      {
        FTitle.ShowFrame = show;
        Refresh();
      }
    }

    private void DialogTitleOnLineWidthFrameChanged(Int32 linewidth)
    {
      if (FTitle is CTextRow)
      {
        FTitle.LineWidthFrame = linewidth;
        Refresh();
      }
    }

    private void DialogTitleOnColorOpacityFrameChanged(Color color, Byte opacity)
    {
      Byte CA = (Byte)(opacity * 255f / 100f);
      Color CO = Color.FromArgb(CA, color.R, color.G, color.B);
      if (FTitle is CTextRow)
      {
        FTitle.SetColorFrame(CO);
        Refresh();
      }
    }
    //
    //---------------------------------------------------------------------------------------
    // Segment - Public Management
    //---------------------------------------------------------------------------------------
    //
    public void ShowDialogEditReal(Boolean show)
    {
      if (show)
      {
        FDialogReal.Show();
      }
      else
      {
        FDialogReal.Hide();
      }
    }

    public void ShowDialogEditGrid(Boolean show)
    {
      if (show)
      {
        FDialogGrid.Show();
      }
      else
      {
        FDialogGrid.Hide();
      }
    }

    public void ShowDialogEditTitle(Boolean show)
    {
      if (show)
      {
        FDialogTitle.Show();
      }
      else
      {
        FDialogTitle.Hide();
      }
    }

    public void MoveDialogs(Int32 deltaleft, Int32 deltatop)
    {
      FDialogReal.Left += deltaleft;
      FDialogReal.Top += deltatop;
      //
      FDialogGrid.Left += deltaleft;
      FDialogGrid.Top += deltatop;
      //
      FDialogTitle.Left += deltaleft;
      FDialogTitle.Top += deltatop;
    }
  }
}
