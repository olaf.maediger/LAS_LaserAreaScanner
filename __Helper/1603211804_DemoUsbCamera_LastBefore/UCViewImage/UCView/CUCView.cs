﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCView
{
  public partial class CUCView : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCView()
    {
      InitializeComponent();
      DoubleBuffered = true;
      BorderStyle = System.Windows.Forms.BorderStyle.None;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    protected override void OnPaintBackground(PaintEventArgs e)
    {
      Graphics G = e.Graphics;
      Brush B = new SolidBrush(Color.FromArgb(0xFF, 0xE0, 0xE0, 0xFF));
      G.FillRectangle(B, ClientRectangle);
    }

  }
}
