﻿namespace UCEditor
{
  partial class CDialogTitle
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.FUCLineWidthLineFrame = new UCControl.CUCLineWidthLine();
      this.FUCShowFrame = new UCControl.CUCShowProperty();
      this.FUCShowBackground = new UCControl.CUCShowProperty();
      this.FUCSelectFontColorOpacity = new UCControl.CUCSelectFontColorOpacity();
      this.FUCText = new UCControl.CUCText();
      this.FUCOriginPercent = new UCControl.CUCPointPercent();
      this.FUCColorOpacityBackground = new UCControl.CUCColorOpacity();
      this.FUCColorOpacityFrame = new UCControl.CUCColorOpacity();
      this.FUCShowText = new UCControl.CUCShowProperty();
      this.FUCSizePercent = new UCControl.CUCSizePercent();
      this.FUCScaleFactor = new UCControl.CUCSizePercent();
      this.SuspendLayout();
      // 
      // FUCLineWidthLineFrame
      // 
      this.FUCLineWidthLineFrame.BackColor = System.Drawing.SystemColors.Info;
      this.FUCLineWidthLineFrame.LineWidth = 1;
      this.FUCLineWidthLineFrame.Location = new System.Drawing.Point(4, 342);
      this.FUCLineWidthLineFrame.Name = "FUCLineWidthLineFrame";
      this.FUCLineWidthLineFrame.Size = new System.Drawing.Size(214, 26);
      this.FUCLineWidthLineFrame.TabIndex = 27;
      this.FUCLineWidthLineFrame.Title = "LineWidth";
      // 
      // FUCShowFrame
      // 
      this.FUCShowFrame.BackColor = System.Drawing.SystemColors.Info;
      this.FUCShowFrame.Location = new System.Drawing.Point(4, 320);
      this.FUCShowFrame.Name = "FUCShowFrame";
      this.FUCShowFrame.ShowProperty = true;
      this.FUCShowFrame.Size = new System.Drawing.Size(214, 18);
      this.FUCShowFrame.TabIndex = 26;
      this.FUCShowFrame.Title = "Show Frame";
      // 
      // FUCShowBackground
      // 
      this.FUCShowBackground.BackColor = System.Drawing.SystemColors.Info;
      this.FUCShowBackground.Location = new System.Drawing.Point(5, 213);
      this.FUCShowBackground.Name = "FUCShowBackground";
      this.FUCShowBackground.ShowProperty = true;
      this.FUCShowBackground.Size = new System.Drawing.Size(213, 18);
      this.FUCShowBackground.TabIndex = 25;
      this.FUCShowBackground.Title = "Show Background";
      // 
      // FUCSelectFontColorOpacity
      // 
      this.FUCSelectFontColorOpacity.BackColor = System.Drawing.SystemColors.Info;
      this.FUCSelectFontColorOpacity.FontColor = System.Drawing.SystemColors.ControlText;
      this.FUCSelectFontColorOpacity.FontOpacity = ((byte)(100));
      this.FUCSelectFontColorOpacity.FontText = "Hello World!";
      this.FUCSelectFontColorOpacity.Location = new System.Drawing.Point(4, 135);
      this.FUCSelectFontColorOpacity.Name = "FUCSelectFontColorOpacity";
      this.FUCSelectFontColorOpacity.Size = new System.Drawing.Size(214, 69);
      this.FUCSelectFontColorOpacity.TabIndex = 23;
      this.FUCSelectFontColorOpacity.Title = "Font";
      // 
      // FUCText
      // 
      this.FUCText.BackColor = System.Drawing.SystemColors.Info;
      this.FUCText.Location = new System.Drawing.Point(4, 28);
      this.FUCText.Name = "FUCText";
      this.FUCText.Size = new System.Drawing.Size(214, 34);
      this.FUCText.TabIndex = 22;
      this.FUCText.Title = "Text";
      this.FUCText.Value = "---Title---";
      // 
      // FUCOriginPercent
      // 
      this.FUCOriginPercent.BackColor = System.Drawing.SystemColors.Info;
      this.FUCOriginPercent.Location = new System.Drawing.Point(4, 65);
      this.FUCOriginPercent.MaximumX = 100D;
      this.FUCOriginPercent.MaximumY = 100D;
      this.FUCOriginPercent.MinimumX = 0D;
      this.FUCOriginPercent.MinimumY = 0D;
      this.FUCOriginPercent.Name = "FUCOriginPercent";
      this.FUCOriginPercent.NameX = "PointX";
      this.FUCOriginPercent.NameY = "PointY";
      this.FUCOriginPercent.PointX = 10D;
      this.FUCOriginPercent.PointY = 10D;
      this.FUCOriginPercent.Size = new System.Drawing.Size(214, 31);
      this.FUCOriginPercent.TabIndex = 20;
      this.FUCOriginPercent.Title = "Origin";
      this.FUCOriginPercent.Unit = "%";
      this.FUCOriginPercent.WidthNameX = 42;
      this.FUCOriginPercent.WidthNameY = 42;
      // 
      // FUCColorOpacityBackground
      // 
      this.FUCColorOpacityBackground.BackColor = System.Drawing.SystemColors.Info;
      this.FUCColorOpacityBackground.Location = new System.Drawing.Point(4, 235);
      this.FUCColorOpacityBackground.Name = "FUCColorOpacityBackground";
      this.FUCColorOpacityBackground.Size = new System.Drawing.Size(214, 40);
      this.FUCColorOpacityBackground.TabIndex = 12;
      this.FUCColorOpacityBackground.Title = "Color Background";
      // 
      // FUCColorOpacityFrame
      // 
      this.FUCColorOpacityFrame.BackColor = System.Drawing.SystemColors.Info;
      this.FUCColorOpacityFrame.Location = new System.Drawing.Point(4, 372);
      this.FUCColorOpacityFrame.Name = "FUCColorOpacityFrame";
      this.FUCColorOpacityFrame.Size = new System.Drawing.Size(214, 40);
      this.FUCColorOpacityFrame.TabIndex = 11;
      this.FUCColorOpacityFrame.Title = "Color Frame";
      // 
      // FUCShowText
      // 
      this.FUCShowText.BackColor = System.Drawing.SystemColors.Info;
      this.FUCShowText.Location = new System.Drawing.Point(4, 5);
      this.FUCShowText.Name = "FUCShowText";
      this.FUCShowText.ShowProperty = true;
      this.FUCShowText.Size = new System.Drawing.Size(214, 18);
      this.FUCShowText.TabIndex = 24;
      this.FUCShowText.Title = "Show Text";
      // 
      // FUCSizePercent
      // 
      this.FUCSizePercent.BackColor = System.Drawing.SystemColors.Info;
      this.FUCSizePercent.Location = new System.Drawing.Point(4, 100);
      this.FUCSizePercent.MaximumX = 100D;
      this.FUCSizePercent.MaximumY = 100D;
      this.FUCSizePercent.MinimumX = 0D;
      this.FUCSizePercent.MinimumY = 0D;
      this.FUCSizePercent.Name = "FUCSizePercent";
      this.FUCSizePercent.NameX = "SizeX";
      this.FUCSizePercent.NameY = "SizeY";
      this.FUCSizePercent.Size = new System.Drawing.Size(214, 31);
      this.FUCSizePercent.SizeX = 80D;
      this.FUCSizePercent.SizeY = 80D;
      this.FUCSizePercent.TabIndex = 28;
      this.FUCSizePercent.Title = "Size";
      this.FUCSizePercent.Unit = "%";
      this.FUCSizePercent.WidthNameX = 42;
      this.FUCSizePercent.WidthNameY = 42;
      // 
      // FUCScaleFactor
      // 
      this.FUCScaleFactor.BackColor = System.Drawing.SystemColors.Info;
      this.FUCScaleFactor.Location = new System.Drawing.Point(4, 279);
      this.FUCScaleFactor.MaximumX = 200D;
      this.FUCScaleFactor.MaximumY = 200D;
      this.FUCScaleFactor.MinimumX = 100D;
      this.FUCScaleFactor.MinimumY = 100D;
      this.FUCScaleFactor.Name = "FUCScaleFactor";
      this.FUCScaleFactor.NameX = "ScaleX";
      this.FUCScaleFactor.NameY = "ScaleY";
      this.FUCScaleFactor.Size = new System.Drawing.Size(214, 31);
      this.FUCScaleFactor.SizeX = 100D;
      this.FUCScaleFactor.SizeY = 100D;
      this.FUCScaleFactor.TabIndex = 29;
      this.FUCScaleFactor.Title = "ScaleFactor";
      this.FUCScaleFactor.Unit = "%";
      this.FUCScaleFactor.WidthNameX = 42;
      this.FUCScaleFactor.WidthNameY = 42;
      // 
      // CDialogTitle
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(224, 415);
      this.ControlBox = false;
      this.Controls.Add(this.FUCScaleFactor);
      this.Controls.Add(this.FUCSizePercent);
      this.Controls.Add(this.FUCLineWidthLineFrame);
      this.Controls.Add(this.FUCShowFrame);
      this.Controls.Add(this.FUCShowBackground);
      this.Controls.Add(this.FUCSelectFontColorOpacity);
      this.Controls.Add(this.FUCText);
      this.Controls.Add(this.FUCOriginPercent);
      this.Controls.Add(this.FUCColorOpacityBackground);
      this.Controls.Add(this.FUCColorOpacityFrame);
      this.Controls.Add(this.FUCShowText);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "CDialogTitle";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "Edit Title Properties";
      this.ResumeLayout(false);

    }

    #endregion

    private UCControl.CUCShowProperty FUCShowText;
    private UCControl.CUCColorOpacity FUCColorOpacityFrame;
    private UCControl.CUCColorOpacity FUCColorOpacityBackground;
    private UCControl.CUCPointPercent FUCOriginPercent;
    private UCControl.CUCText FUCText;
    private UCControl.CUCSelectFontColorOpacity FUCSelectFontColorOpacity;
    private UCControl.CUCShowProperty FUCShowBackground;
    private UCControl.CUCShowProperty FUCShowFrame;
    private UCControl.CUCLineWidthLine FUCLineWidthLineFrame;
    private UCControl.CUCSizePercent FUCSizePercent;
    private UCControl.CUCSizePercent FUCScaleFactor;
  }
}