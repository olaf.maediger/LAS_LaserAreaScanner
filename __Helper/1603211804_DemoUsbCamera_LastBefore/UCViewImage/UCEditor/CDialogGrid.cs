﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using NLRealPixel;
using Initdata;
using UCControl;
//
namespace UCEditor
{
  public delegate void DOnDialogGridChanged(Boolean visible, Int32 left, Int32 top);
  //public delegate void DOnShowGridChanged(Boolean showgrid, 
  //                                        Boolean showlineshorizontal,
  //                                        Boolean showlinesvertical);
  //
  public partial class CDialogGrid : Form
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "DialogGrid";
    private const String NAME_VISIBLE = "Visible";
    private const Boolean INIT_VISIBLE = true;
    private const String NAME_LEFT = "Left";
    private const Int32 INIT_LEFT = 10;
    private const String NAME_TOP = "Top";
    private const Int32 INIT_TOP = 10;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnDialogGridChanged FOnDialogGridChanged;
    private DOnShowPropertyChanged FUCShowGridOnShowGridChanged;
    private DOnSizeChanged FOnSizeChanged;
    private DOnStepCountChanged FOnStepCountChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CDialogGrid()
    {
      InitializeComponent();
      StartPosition = FormStartPosition.Manual;
      //
      FUCSize.SetOnSizeChanged(UCSizeOnSizeChanged);
      FUCStepCount.SetOnStepCountChanged(UCStepCountOnStepCountChanged);
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public EUnitScale UnitScale
    {
      set
      {
        FUCOrigin.UnitScale = value;
        FUCSize.UnitScale = value;
        FUCStepSize.UnitScale = value;
      }
    }

    public Boolean ShowGrid
    {
      get { return FUCShowGrid.ShowProperty; }
      set { FUCShowGrid.ShowProperty = value; }
    }
    public Boolean ShowLinesHorizontal
    {
      get { return FUCShowLinesHorizontal.ShowProperty; }
      set { FUCShowLinesHorizontal.ShowProperty = value; }
    }
    public Boolean ShowLinesVertical
    {
      get { return FUCShowLinesVertical.ShowProperty; }
      set { FUCShowLinesVertical.ShowProperty = value; }
    }

    public Double OriginX
    {
      get { return FUCOrigin.X; }
      set { FUCOrigin.X = value; }
    }
    public Double OriginY
    {
      get { return FUCOrigin.Y; }
      set { FUCOrigin.Y = value; }
    }

    public Double SizeX
    {
      get { return FUCSize.SizeX; }
      set { FUCSize.SizeX = value; }
    }
    public Double SizeY
    {
      get { return FUCSize.SizeY; }
      set { FUCSize.SizeY = value; }
    }

    public Int32 StepCountX
    {
      get { return FUCStepCount.StepCountX; }
      set { FUCStepCount.StepCountX = value; }
    }
    public Int32 StepCountY
    {
      get { return FUCStepCount.StepCountY; }
      set { FUCStepCount.StepCountY = value; }
    }

    public Int32 LineWidthRow
    {
      get { return FUCLineWidthRowCol.LineWidthRow; }
      set { FUCLineWidthRowCol.LineWidthRow = value; }
    }
    public Int32 LineWidthCol
    {
      get { return FUCLineWidthRowCol.LineWidthCol; }
      set { FUCLineWidthRowCol.LineWidthCol = value; }
    }

    public Color ColorRow
    {
      get { return FUCColorOpacityRowCol.ColorRow; }
      set { FUCColorOpacityRowCol.ColorRow = value; }
    }
    public Color ColorCol
    {
      get { return FUCColorOpacityRowCol.ColorCol; }
      set { FUCColorOpacityRowCol.ColorCol = value; }
    }


    public void SetOnDialogGridChanged(DOnDialogGridChanged value)
    {
      FOnDialogGridChanged = value;
    }

    public void SetOnShowGridChanged(DOnShowPropertyChanged value)
    {
      FUCShowGridOnShowGridChanged = value;
      FUCShowGrid.SetOnShowPropertyChanged(UCShowGridOnShowGridChanged);
    }
    public void SetOnShowLinesHorizontalChanged(DOnShowPropertyChanged value)
    {
      FUCShowLinesHorizontal.SetOnShowPropertyChanged(value);
    }
    public void SetOnShowLinesVerticalChanged(DOnShowPropertyChanged value)
    {
      FUCShowLinesVertical.SetOnShowPropertyChanged(value);
    }

    public void SetOnOriginChanged(DOnOriginChanged value)
    {
      FUCOrigin.SetOnOriginChanged(value);
    }

    public void SetOnSizeChanged(DOnSizeChanged value)
    {
      FOnSizeChanged = value;
      FUCSize.SetOnSizeChanged(UCSizeOnSizeChanged);
    }

    public void SetOnStepCountChanged(DOnStepCountChanged value)
    {
      FOnStepCountChanged = value;
      FUCStepCount.SetOnStepCountChanged(UCStepCountOnStepCountChanged);
    }

    public void SetOnLineWidthRowColChanged(DOnLineWidthRowColChanged value)
    {
      FUCLineWidthRowCol.SetOnLineWidthRowColChanged(value);
    }

    public void SetOnColorRowColChanged(DOnColorRowColChanged value)
    {
      FUCColorOpacityRowCol.SetOnColorRowColChanged(value);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Int32 IValue;
      Boolean BValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      BValue = INIT_VISIBLE;
      Result &= initdata.ReadBoolean(NAME_VISIBLE, out BValue, BValue);
      Visible = BValue;
      //
      IValue = INIT_LEFT;
      Result &= initdata.ReadInt32(NAME_LEFT, out IValue, IValue);
      Left = IValue;
      //
      IValue = INIT_TOP;
      Result &= initdata.ReadInt32(NAME_TOP, out IValue, IValue);
      Top = IValue;
      //
      //if (FOnDialogGridChanged is DOnDialogGridChanged)
      //{
      //  FOnDialogGridChanged(Visible, Left, Top);
      //}
      //
      Result &= FUCShowGrid.LoadInitdata(initdata, "ShowGrid");
      Result &= FUCShowLinesHorizontal.LoadInitdata(initdata, "ShowLinesHorizontal");
      Result &= FUCShowLinesVertical.LoadInitdata(initdata, "ShowLinesVertical");
      Result &= FUCShowGrid.LoadInitdata(initdata, "ShowGrid");
      Result &= FUCOrigin.LoadInitdata(initdata);
      Result &= FUCSize.LoadInitdata(initdata);
      Result &= FUCStepCount.LoadInitdata(initdata);
      Result &= FUCLineWidthRowCol.LoadInitdata(initdata);
      Result &= FUCColorOpacityRowCol.LoadInitdata(initdata);
      //
      Result &= initdata.CloseSection();
      //
      if (FOnDialogGridChanged is DOnDialogGridChanged)
      {
        FOnDialogGridChanged(Visible, Left, Top);
      }
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteBoolean(NAME_VISIBLE, Visible);
      Result &= initdata.WriteInt32(NAME_LEFT, Left);
      Result &= initdata.WriteInt32(NAME_TOP, Top);
      //
      Result &= FUCShowGrid.SaveInitdata(initdata, "ShowGrid");
      Result &= FUCShowLinesHorizontal.SaveInitdata(initdata, "ShowLinesHorizontal");
      Result &= FUCShowLinesVertical.SaveInitdata(initdata, "ShowLinesVertical");
      Result &= FUCOrigin.SaveInitdata(initdata);
      Result &= FUCSize.SaveInitdata(initdata);
      Result &= FUCStepCount.SaveInitdata(initdata);
      Result &= FUCLineWidthRowCol.SaveInitdata(initdata);
      Result &= FUCColorOpacityRowCol.SaveInitdata(initdata);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //
    private Double CalculateStepSizeX()
    {
      return FUCSize.SizeX / FUCStepCount.StepCountX;
    }

    private Double CalculateStepSizeY()
    {
      return FUCSize.SizeY / FUCStepCount.StepCountY;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - ShowGrid
    //------------------------------------------------------------------------
    //
    private void UCShowGridOnShowGridChanged(Boolean showgrid)
    {
      FUCShowLinesHorizontal.Enabled = showgrid;
      //FUCShowLinesHorizontal.ShowProperty = showgrid;
      FUCShowLinesVertical.Enabled = showgrid;
      //FUCShowLinesVertical.ShowProperty = showgrid;
      if (FUCShowGridOnShowGridChanged is DOnShowPropertyChanged)
      {
        FUCShowGridOnShowGridChanged(showgrid);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - Size
    //------------------------------------------------------------------------
    //
    private void UCSizeOnSizeChanged(Double sizex, Double sizey)
    {
      FUCStepSize.StepSizeX = CalculateStepSizeX();
      FUCStepSize.StepSizeY = CalculateStepSizeY();
      if (FOnSizeChanged is DOnSizeChanged)
      {
        FOnSizeChanged(sizex, sizey);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - StepCount
    //------------------------------------------------------------------------
    //
    private void UCStepCountOnStepCountChanged(Int32 stepcountx, Int32 stepcounty)
    {
      FUCStepSize.StepSizeX = CalculateStepSizeX();
      FUCStepSize.StepSizeY = CalculateStepSizeY();
      if (FOnStepCountChanged is DOnStepCountChanged)
      {
        FOnStepCountChanged(stepcountx, stepcounty);
      }
    }
 
  }
}
