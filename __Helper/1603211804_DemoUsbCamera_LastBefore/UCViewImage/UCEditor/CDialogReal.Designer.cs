﻿namespace UCEditor
{
  partial class CDialogReal
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.FUCMaximum = new UCControl.CUCMaximum();
      this.FUCMinimum = new UCControl.CUCMinimum();
      this.FUCUnitScale = new UCControl.CUCUnitScale();
      this.SuspendLayout();
      // 
      // FUCMaximum
      // 
      this.FUCMaximum.BackColor = System.Drawing.SystemColors.Info;
      this.FUCMaximum.Location = new System.Drawing.Point(5, 75);
      this.FUCMaximum.Name = "FUCMaximum";
      this.FUCMaximum.Size = new System.Drawing.Size(277, 30);
      this.FUCMaximum.TabIndex = 2;
      this.FUCMaximum.XMaximum = 0D;
      this.FUCMaximum.YMaximum = 0D;
      // 
      // FUCMinimum
      // 
      this.FUCMinimum.BackColor = System.Drawing.SystemColors.Info;
      this.FUCMinimum.Location = new System.Drawing.Point(5, 39);
      this.FUCMinimum.Name = "FUCMinimum";
      this.FUCMinimum.Size = new System.Drawing.Size(277, 30);
      this.FUCMinimum.TabIndex = 1;
      this.FUCMinimum.XMinimum = 0D;
      this.FUCMinimum.YMinimum = 0D;
      // 
      // FUCUnitScale
      // 
      this.FUCUnitScale.BackColor = System.Drawing.SystemColors.Info;
      this.FUCUnitScale.Location = new System.Drawing.Point(5, 7);
      this.FUCUnitScale.Name = "FUCUnitScale";
      this.FUCUnitScale.Size = new System.Drawing.Size(194, 26);
      this.FUCUnitScale.TabIndex = 0;
      this.FUCUnitScale.UnitScale = NLRealPixel.EUnitScale.NM;
      // 
      // CDialogReal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(288, 137);
      this.ControlBox = false;
      this.Controls.Add(this.FUCMaximum);
      this.Controls.Add(this.FUCMinimum);
      this.Controls.Add(this.FUCUnitScale);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "CDialogReal";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "Edit Real Properties";
      this.ResumeLayout(false);

    }

    #endregion

    private UCControl.CUCUnitScale FUCUnitScale;
    private UCControl.CUCMinimum FUCMinimum;
    private UCControl.CUCMaximum FUCMaximum;

  }
}