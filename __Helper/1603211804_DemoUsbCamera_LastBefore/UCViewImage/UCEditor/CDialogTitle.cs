﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using NLRealPixel;
using Initdata;
using UCControl;
//
namespace UCEditor
{
  public delegate void DOnDialogTitleChanged(Boolean visible, Int32 left, Int32 top);
  //
  public partial class CDialogTitle : Form
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "DialogTitle";
    private const String NAME_VISIBLE = "Visible";
    private const Boolean INIT_VISIBLE = true;
    private const String NAME_LEFT = "Left";
    private const Int32 INIT_LEFT = 10;
    private const String NAME_TOP = "Top";
    private const Int32 INIT_TOP = 10;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnDialogTitleChanged FOnDialogTitleChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CDialogTitle()
    {
      InitializeComponent();
      //
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    //  Text
    //
    public Boolean ShowText
    {
      get { return FUCShowText.ShowProperty; }
      set { FUCShowText.ShowProperty = value; }
    }

    public Double OriginX
    {
      get { return FUCOriginPercent.PointX; }
      set { FUCOriginPercent.PointX = value; }
    }
    public Double OriginY
    {
      get { return FUCOriginPercent.PointY; }
      set { FUCOriginPercent.PointY = value; }
    }

    public void SetOnDialogTitleChanged(DOnDialogTitleChanged value)
    {
      FOnDialogTitleChanged = value;
    }

    public void SetOnShowTextChanged(DOnShowPropertyChanged value)
    {
      FUCShowText.SetOnShowPropertyChanged(value);
    }

    public void SetOnTextChanged(DOnTextChanged value)
    {
      FUCText.SetOnTextChanged(value);
    }

    public void SetOnOriginChanged(DOnPointPercentChanged value)
    {
      FUCOriginPercent.SetOnPointPercentChanged(value);
    }

    public void SetOnSizeChanged(DOnSizePercentChanged value)
    {
      FUCSizePercent.SetOnSizePercentChanged(value);
    }

    public void SetOnFontColorOpacityChanged(DOnColorOpacityChanged value)
    {
      FUCSelectFontColorOpacity.SetOnColorOpacityChanged(value);
    }

    public void SetOnFontChanged(DOnFontChanged value)
    {
      FUCSelectFontColorOpacity.SetOnFontChanged(value);
    }
    //
    //  Background
    //
    public void SetOnShowBackgroundChanged(DOnShowPropertyChanged value)
    {
      FUCShowBackground.SetOnShowPropertyChanged(value);
    }

    public void SetOnBackgroundColorOpacityChanged(DOnColorOpacityChanged value)
    {
      FUCColorOpacityBackground.SetOnColorOpacityChanged(value);
    }

    public Double ScaleFactorX
    {
      get { return FUCScaleFactor.SizeX; }
      set { FUCScaleFactor.SizeX = value; }
    }
    public Double ScaleFactorY
    {
      get { return FUCScaleFactor.SizeY; }
      set { FUCScaleFactor.SizeY = value; }
    }

    public void SetOnScaleFactorChanged(DOnSizePercentChanged value)
    {
      FUCScaleFactor.SetOnSizePercentChanged(value);
    }
    //
    //  Frame
    //
    public void SetOnShowFrameChanged(DOnShowPropertyChanged value)
    {
      FUCShowFrame.SetOnShowPropertyChanged(value);
    }

    public void SetOnLineWidthFrameChanged(DOnLineWidthChanged value)
    {
      FUCLineWidthLineFrame.SetOnLineWidthChanged(value);
    }

    public void SetOnColorOpacityFrameChanged(DOnColorOpacityChanged value)
    {
      FUCColorOpacityFrame.SetOnColorOpacityChanged(value);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Int32 IValue;
      Boolean BValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      // Dialog
      BValue = INIT_VISIBLE;
      Result &= initdata.ReadBoolean(NAME_VISIBLE, out BValue, BValue);
      Visible = BValue;
      //
      IValue = INIT_LEFT;
      Result &= initdata.ReadInt32(NAME_LEFT, out IValue, IValue);
      Left = IValue;
      //
      IValue = INIT_TOP;
      Result &= initdata.ReadInt32(NAME_TOP, out IValue, IValue);
      Top = IValue;
      //
      if (FOnDialogTitleChanged is DOnDialogTitleChanged)
      {
        FOnDialogTitleChanged(Visible, Left, Top);
      }
      // Text
      Result &= FUCShowText.LoadInitdata(initdata, "Text");
      Result &= FUCOriginPercent.LoadInitdata(initdata, "Text");
      Result &= FUCSizePercent.LoadInitdata(initdata, "Text");
      Result &= FUCText.LoadInitdata(initdata, "Text");
      Result &= FUCSelectFontColorOpacity.LoadInitdata(initdata, "Text");
      // Background
      Result &= FUCShowBackground.LoadInitdata(initdata, "Background");
      Result &= FUCColorOpacityBackground.LoadInitdata(initdata, "Background");
      Result &= FUCScaleFactor.LoadInitdata(initdata, "Background");
      // Frame
      Result &= FUCShowFrame.LoadInitdata(initdata, "Frame");
      Result &= FUCLineWidthLineFrame.LoadInitdata(initdata, "Frame");
      Result &= FUCColorOpacityFrame.LoadInitdata(initdata, "Frame");
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      // Dialog
      Result &= initdata.WriteBoolean(NAME_VISIBLE, Visible);
      Result &= initdata.WriteInt32(NAME_LEFT, Left);
      Result &= initdata.WriteInt32(NAME_TOP, Top);
      // Text
      Result &= FUCShowText.SaveInitdata(initdata, "Text");
      Result &= FUCOriginPercent.SaveInitdata(initdata, "Text");
      Result &= FUCSizePercent.SaveInitdata(initdata, "Text");
      Result &= FUCText.SaveInitdata(initdata, "Text");
      Result &= FUCSelectFontColorOpacity.SaveInitdata(initdata, "Text");
      // Background
      Result &= FUCShowBackground.SaveInitdata(initdata, "Background");
      Result &= FUCColorOpacityBackground.SaveInitdata(initdata, "Background");
      Result &= FUCScaleFactor.SaveInitdata(initdata, "Background");
      // Frame
      Result &= FUCShowFrame.SaveInitdata(initdata, "Frame");
      Result &= FUCLineWidthLineFrame.SaveInitdata(initdata, "Frame");
      Result &= FUCColorOpacityFrame.SaveInitdata(initdata, "Frame");
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //
 

    //
    //------------------------------------------------------------------------
    //  Section - Callback - 
    //------------------------------------------------------------------------
    //
 

  }
}
