﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
using UCControl;
//
namespace UCEditor
{
  public delegate void DOnDialogRealChanged(Boolean visible, Int32 left, Int32 top);
  //
  public partial class CDialogReal : Form
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "DialogReal";
    private const String NAME_VISIBLE = "Visible";
    private const Boolean INIT_VISIBLE = true;
    private const String NAME_LEFT = "Left";
    private const Int32 INIT_LEFT = 10;
    private const String NAME_TOP = "Top";
    private const Int32 INIT_TOP = 10;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnDialogRealChanged FOnDialogRealChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CDialogReal()
    {
      InitializeComponent();
      StartPosition = FormStartPosition.Manual;
     }

    public EUnitScale UnitScale
    {
      get { return FUCUnitScale.UnitScale; }
      set { FUCUnitScale.UnitScale = value; }
    }

    public Double XMinimum
    {
      get { return FUCMinimum.XMinimum; }
      set { FUCMinimum.XMinimum = value; }
    }
    public Double YMinimum
    {
      get { return FUCMinimum.YMinimum; }
      set { FUCMinimum.YMinimum = value; }
    }

    public Double XMaximum
    {
      get { return FUCMaximum.XMaximum; }
      set { FUCMaximum.XMaximum = value; }
    }
    public Double YMaximum
    {
      get { return FUCMaximum.YMaximum; }
      set { FUCMaximum.YMaximum = value; }
    }


    public void SetOnDialogRealChanged(DOnDialogRealChanged value)
    {
      FOnDialogRealChanged = value;
    }
    public void SetOnUnitScaleChanged(DOnUnitScaleChanged value)
    {
      FUCUnitScale.SetOnUnitScaleChanged(value);
    }

    public void SetOnMinimumChanged(DOnMinimumChanged value)
    {
      FUCMinimum.SetOnMinimumChanged(value);
    }

    public void SetOnMaximumChanged(DOnMaximumChanged value)
    {
      FUCMaximum.SetOnMaximumChanged(value);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Int32 IValue;
      Boolean BValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      BValue = INIT_VISIBLE;
      Result &= initdata.ReadBoolean(NAME_VISIBLE, out BValue, BValue);
      Visible = BValue;
      //
      IValue = INIT_LEFT;
      Result &= initdata.ReadInt32(NAME_LEFT, out IValue, IValue);
      Left = IValue;
      //
      IValue = INIT_TOP;
      Result &= initdata.ReadInt32(NAME_TOP, out IValue, IValue);
      Top = IValue;
      //
      if (FOnDialogRealChanged is DOnDialogRealChanged)
      {
        FOnDialogRealChanged(Visible, Left, Top);
      }
      //
      Result &= FUCUnitScale.LoadInitdata(initdata);
      Result &= FUCMinimum.LoadInitdata(initdata);
      Result &= FUCMaximum.LoadInitdata(initdata);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteBoolean(NAME_VISIBLE, Visible);
      Result &= initdata.WriteInt32(NAME_LEFT, Left);
      Result &= initdata.WriteInt32(NAME_TOP, Top);
      //
      Result &= FUCUnitScale.SaveInitdata(initdata);
      Result &= FUCMinimum.SaveInitdata(initdata);
      Result &= FUCMaximum.SaveInitdata(initdata);
      // 
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Public
    //------------------------------------------------------------------------
    //
    public void Synchronize(CRealPixel realpixel)
    {
      FUCUnitScale.Synchronize(realpixel);
      FUCMinimum.Synchronize(realpixel);
      FUCMaximum.Synchronize(realpixel);
    }

  }
}
