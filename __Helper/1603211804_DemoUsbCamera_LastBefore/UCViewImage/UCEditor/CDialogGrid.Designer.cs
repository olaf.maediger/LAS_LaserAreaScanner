﻿namespace UCEditor
{
  partial class CDialogGrid
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.FUCShowLinesVertical = new UCControl.CUCShowProperty();
      this.FUCShowLinesHorizontal = new UCControl.CUCShowProperty();
      this.FUCShowGrid = new UCControl.CUCShowProperty();
      this.FUCColorOpacityRowCol = new UCControl.CUCColorOpacityRowCol();
      this.FUCLineWidthRowCol = new UCControl.CUCLineWidthRowCol();
      this.FUCStepSize = new UCControl.CUCStepSize();
      this.FUCStepCount = new UCControl.CUCStepCount();
      this.FUCSize = new UCControl.CUCSizeReal();
      this.FUCOrigin = new UCControl.CUCOrigin();
      this.SuspendLayout();
      // 
      // FUCShowLinesVertical
      // 
      this.FUCShowLinesVertical.BackColor = System.Drawing.SystemColors.Info;
      this.FUCShowLinesVertical.Location = new System.Drawing.Point(114, 23);
      this.FUCShowLinesVertical.Name = "FUCShowLinesVertical";
      this.FUCShowLinesVertical.ShowProperty = true;
      this.FUCShowLinesVertical.Size = new System.Drawing.Size(104, 18);
      this.FUCShowLinesVertical.TabIndex = 9;
      this.FUCShowLinesVertical.Title = "Vertical";
      // 
      // FUCShowLinesHorizontal
      // 
      this.FUCShowLinesHorizontal.BackColor = System.Drawing.SystemColors.Info;
      this.FUCShowLinesHorizontal.Location = new System.Drawing.Point(7, 23);
      this.FUCShowLinesHorizontal.Name = "FUCShowLinesHorizontal";
      this.FUCShowLinesHorizontal.ShowProperty = true;
      this.FUCShowLinesHorizontal.Size = new System.Drawing.Size(104, 18);
      this.FUCShowLinesHorizontal.TabIndex = 8;
      this.FUCShowLinesHorizontal.Title = "Horizontal";
      // 
      // FUCShowGrid
      // 
      this.FUCShowGrid.BackColor = System.Drawing.SystemColors.Info;
      this.FUCShowGrid.Location = new System.Drawing.Point(7, 5);
      this.FUCShowGrid.Name = "FUCShowGrid";
      this.FUCShowGrid.ShowProperty = true;
      this.FUCShowGrid.Size = new System.Drawing.Size(211, 18);
      this.FUCShowGrid.TabIndex = 7;
      this.FUCShowGrid.Title = "Show Grid";
      // 
      // FUCColorOpacityRowCol
      // 
      this.FUCColorOpacityRowCol.BackColor = System.Drawing.SystemColors.Info;
      this.FUCColorOpacityRowCol.ColorCol = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
      this.FUCColorOpacityRowCol.ColorRow = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
      this.FUCColorOpacityRowCol.Location = new System.Drawing.Point(3, 236);
      this.FUCColorOpacityRowCol.Name = "FUCColorOpacityRowCol";
      this.FUCColorOpacityRowCol.Size = new System.Drawing.Size(214, 61);
      this.FUCColorOpacityRowCol.TabIndex = 6;
      // 
      // FUCLineWidthRowCol
      // 
      this.FUCLineWidthRowCol.BackColor = System.Drawing.SystemColors.Info;
      this.FUCLineWidthRowCol.LineWidthCol = 1;
      this.FUCLineWidthRowCol.LineWidthRow = 1;
      this.FUCLineWidthRowCol.Location = new System.Drawing.Point(3, 197);
      this.FUCLineWidthRowCol.Name = "FUCLineWidthRowCol";
      this.FUCLineWidthRowCol.Size = new System.Drawing.Size(214, 33);
      this.FUCLineWidthRowCol.TabIndex = 5;
      // 
      // FUCStepSize
      // 
      this.FUCStepSize.BackColor = System.Drawing.SystemColors.Info;
      this.FUCStepSize.Location = new System.Drawing.Point(3, 158);
      this.FUCStepSize.Name = "FUCStepSize";
      this.FUCStepSize.Size = new System.Drawing.Size(214, 33);
      this.FUCStepSize.TabIndex = 4;
      // 
      // FUCStepCount
      // 
      this.FUCStepCount.BackColor = System.Drawing.SystemColors.Info;
      this.FUCStepCount.Location = new System.Drawing.Point(3, 121);
      this.FUCStepCount.Name = "FUCStepCount";
      this.FUCStepCount.Size = new System.Drawing.Size(214, 31);
      this.FUCStepCount.StepCountX = 5;
      this.FUCStepCount.StepCountY = 5;
      this.FUCStepCount.TabIndex = 3;
      // 
      // FUCSize
      // 
      this.FUCSize.BackColor = System.Drawing.SystemColors.Info;
      this.FUCSize.Location = new System.Drawing.Point(2, 84);
      this.FUCSize.Name = "FUCSize";
      this.FUCSize.Size = new System.Drawing.Size(214, 31);
      this.FUCSize.SizeX = 10D;
      this.FUCSize.SizeY = 10D;
      this.FUCSize.TabIndex = 2;
      // 
      // FUCOrigin
      // 
      this.FUCOrigin.BackColor = System.Drawing.SystemColors.Info;
      this.FUCOrigin.Location = new System.Drawing.Point(3, 47);
      this.FUCOrigin.Name = "FUCOrigin";
      this.FUCOrigin.Size = new System.Drawing.Size(214, 31);
      this.FUCOrigin.TabIndex = 1;
      this.FUCOrigin.X = 0D;
      this.FUCOrigin.Y = 0D;
      // 
      // CDialogGrid
      // 
      this.ClientSize = new System.Drawing.Size(224, 327);
      this.ControlBox = false;
      this.Controls.Add(this.FUCShowLinesVertical);
      this.Controls.Add(this.FUCShowLinesHorizontal);
      this.Controls.Add(this.FUCShowGrid);
      this.Controls.Add(this.FUCColorOpacityRowCol);
      this.Controls.Add(this.FUCLineWidthRowCol);
      this.Controls.Add(this.FUCStepSize);
      this.Controls.Add(this.FUCStepCount);
      this.Controls.Add(this.FUCSize);
      this.Controls.Add(this.FUCOrigin);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "CDialogGrid";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.Text = "Edit Grid Properties";
      this.ResumeLayout(false);

    }

    #endregion

    private UCControl.CUCOrigin FUCOrigin;
    private UCControl.CUCSizeReal FUCSize;
    private UCControl.CUCStepCount FUCStepCount;
    private UCControl.CUCStepSize FUCStepSize;
    private UCControl.CUCLineWidthRowCol FUCLineWidthRowCol;
    private UCControl.CUCColorOpacityRowCol FUCColorOpacityRowCol;
    private UCControl.CUCShowProperty FUCShowGrid;
    private UCControl.CUCShowProperty FUCShowLinesHorizontal;
    private UCControl.CUCShowProperty FUCShowLinesVertical;

  }
}