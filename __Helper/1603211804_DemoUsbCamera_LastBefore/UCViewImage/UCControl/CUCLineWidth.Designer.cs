﻿namespace UCControl
{
  partial class CUCLineWidth
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label14 = new System.Windows.Forms.Label();
      this.nudLineWidth = new System.Windows.Forms.NumericUpDown();
      this.lblHeader = new System.Windows.Forms.Label();
      this.pnlTop = new System.Windows.Forms.Panel();
      ((System.ComponentModel.ISupportInitialize)(this.nudLineWidth)).BeginInit();
      this.SuspendLayout();
      // 
      // label14
      // 
      this.label14.Dock = System.Windows.Forms.DockStyle.Left;
      this.label14.Location = new System.Drawing.Point(98, 7);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(24, 22);
      this.label14.TabIndex = 43;
      this.label14.Text = "pxl";
      this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudLineWidth
      // 
      this.nudLineWidth.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudLineWidth.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudLineWidth.Location = new System.Drawing.Point(57, 7);
      this.nudLineWidth.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudLineWidth.Name = "nudLineWidth";
      this.nudLineWidth.Size = new System.Drawing.Size(41, 16);
      this.nudLineWidth.TabIndex = 42;
      this.nudLineWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudLineWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // lblHeader
      // 
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblHeader.Location = new System.Drawing.Point(0, 7);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(57, 22);
      this.lblHeader.TabIndex = 41;
      this.lblHeader.Text = "LineWidth";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // pnlTop
      // 
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(214, 7);
      this.pnlTop.TabIndex = 40;
      // 
      // CUCLineWidth
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.label14);
      this.Controls.Add(this.nudLineWidth);
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.pnlTop);
      this.Name = "CUCLineWidth";
      this.Size = new System.Drawing.Size(214, 29);
      ((System.ComponentModel.ISupportInitialize)(this.nudLineWidth)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.NumericUpDown nudLineWidth;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Panel pnlTop;

   
   
  }
}
