﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnPointChanged(Double pointx, Double pointy);
  //
  public partial class CUCPointReal : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String HEADER_LIBRARY = "UC{0}";
    private const String INIT_NAME = "Undefined";
    private const String HEADER_POINTX = "{0}X";
    private const Double INIT_POINTX = 0.0;
    private const String HEADER_POINTY = "{0}Y";
    private const Double INIT_POINTY = 0.0;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private String FTitle;
    //????private DOnShowPropertyChanged FOnShowTitleChanged;
    private DOnPointChanged FOnPointChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCPointReal()
    {
      InitializeComponent();
      //
      FTitle = INIT_NAME;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public String Title
    {
      get { return FTitle; }
      set { FTitle = value; }
    }

    //public void SetOnShowTitleChanged(DOnShowTitleChanged value)
    //{
    //  FOnShowTitleChanged = value;
    //}

    public void SetOnPointChanged(DOnPointChanged value)
    {
      FOnPointChanged = value;
    }

    public EUnitScale UnitScale
    {
      set
      {
        String SUnit = CRealPixel.UnitScaleText(value);
        lblUnitPointX.Text = SUnit;
        lblUnitPointY.Text = SUnit;
      }
    }

    public Double X
    {
      get { return (Double)nudX.Value; }
      set { nudX.Value = (Decimal)value; }
    }

    public Double Y
    {
      get { return (Double)nudY.Value; }
      set { nudY.Value = (Decimal)value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Double DValue;
      //
      String Header = String.Format(HEADER_LIBRARY, FTitle);
      Result &= initdata.OpenSection(Header);
      //
      String HeaderX = String.Format(HEADER_POINTX, FTitle);
      DValue = INIT_POINTX;
      Result &= initdata.ReadDouble(HeaderX, out DValue, DValue);
      X = DValue;
      //
      String HeaderY = String.Format(HEADER_POINTY, FTitle);
      DValue = INIT_POINTY;
      Result &= initdata.ReadDouble(HeaderY, out DValue, DValue);
      Y = DValue;
      //
      Result &= initdata.CloseSection();
      //
      nudX_ValueChanged(this, null);
      nudY_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      String Header = String.Format(HEADER_LIBRARY, FTitle);
      Result &= initdata.CreateSection(Header);
      //
      String HeaderX = String.Format(HEADER_POINTX, FTitle);
      Result &= initdata.WriteDouble(HEADER_POINTX, X);
      String HeaderY = String.Format(HEADER_POINTY, FTitle);
      Result &= initdata.WriteDouble(HEADER_POINTY, Y);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void nudX_ValueChanged(object sender, EventArgs e)
    {
      if (FOnPointChanged is DOnPointChanged)
      {
        FOnPointChanged(X, Y);
      }
    }

    private void nudY_ValueChanged(object sender, EventArgs e)
    {
      if (FOnPointChanged is DOnPointChanged)
      {
        FOnPointChanged(X, Y);
      }
    }

  }
}

