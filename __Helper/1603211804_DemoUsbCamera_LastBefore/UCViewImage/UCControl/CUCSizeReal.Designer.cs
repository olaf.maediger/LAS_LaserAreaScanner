﻿namespace UCControl
{
  partial class CUCSizeReal
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label18 = new System.Windows.Forms.Label();
      this.lblUnitSX = new System.Windows.Forms.Label();
      this.nudX = new System.Windows.Forms.NumericUpDown();
      this.label17 = new System.Windows.Forms.Label();
      this.lblUnitSY = new System.Windows.Forms.Label();
      this.nudY = new System.Windows.Forms.NumericUpDown();
      this.label15 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudX)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudY)).BeginInit();
      this.SuspendLayout();
      // 
      // label18
      // 
      this.label18.Dock = System.Windows.Forms.DockStyle.Top;
      this.label18.Location = new System.Drawing.Point(0, 0);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(214, 14);
      this.label18.TabIndex = 7;
      this.label18.Text = "Size";
      // 
      // lblUnitSX
      // 
      this.lblUnitSX.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitSX.Location = new System.Drawing.Point(81, 14);
      this.lblUnitSX.Name = "lblUnitSX";
      this.lblUnitSX.Size = new System.Drawing.Size(26, 17);
      this.lblUnitSX.TabIndex = 10;
      this.lblUnitSX.Text = "um";
      this.lblUnitSX.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudX
      // 
      this.nudX.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudX.DecimalPlaces = 3;
      this.nudX.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudX.Location = new System.Drawing.Point(27, 14);
      this.nudX.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudX.Name = "nudX";
      this.nudX.Size = new System.Drawing.Size(54, 16);
      this.nudX.TabIndex = 9;
      this.nudX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudX.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudX.ValueChanged += new System.EventHandler(this.nudX_ValueChanged);
      // 
      // label17
      // 
      this.label17.Dock = System.Windows.Forms.DockStyle.Left;
      this.label17.Location = new System.Drawing.Point(0, 14);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(27, 17);
      this.label17.TabIndex = 8;
      this.label17.Text = "SX";
      this.label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lblUnitSY
      // 
      this.lblUnitSY.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitSY.Location = new System.Drawing.Point(188, 14);
      this.lblUnitSY.Name = "lblUnitSY";
      this.lblUnitSY.Size = new System.Drawing.Size(26, 17);
      this.lblUnitSY.TabIndex = 13;
      this.lblUnitSY.Text = "um";
      this.lblUnitSY.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudY
      // 
      this.nudY.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudY.DecimalPlaces = 3;
      this.nudY.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudY.Location = new System.Drawing.Point(134, 14);
      this.nudY.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudY.Name = "nudY";
      this.nudY.Size = new System.Drawing.Size(54, 16);
      this.nudY.TabIndex = 12;
      this.nudY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudY.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudY.ValueChanged += new System.EventHandler(this.nudY_ValueChanged);
      // 
      // label15
      // 
      this.label15.Dock = System.Windows.Forms.DockStyle.Left;
      this.label15.Location = new System.Drawing.Point(107, 14);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(27, 17);
      this.label15.TabIndex = 11;
      this.label15.Text = "SY";
      this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // CUCSizeReal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.lblUnitSY);
      this.Controls.Add(this.nudY);
      this.Controls.Add(this.label15);
      this.Controls.Add(this.lblUnitSX);
      this.Controls.Add(this.nudX);
      this.Controls.Add(this.label17);
      this.Controls.Add(this.label18);
      this.Name = "CUCSizeReal";
      this.Size = new System.Drawing.Size(214, 31);
      ((System.ComponentModel.ISupportInitialize)(this.nudX)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudY)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Label lblUnitSX;
    private System.Windows.Forms.NumericUpDown nudX;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.Label lblUnitSY;
    private System.Windows.Forms.NumericUpDown nudY;
    private System.Windows.Forms.Label label15;
  }
}
