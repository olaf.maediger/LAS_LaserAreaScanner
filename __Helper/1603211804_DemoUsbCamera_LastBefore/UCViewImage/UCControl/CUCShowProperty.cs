﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnShowPropertyChanged(Boolean show);

  public partial class CUCShowProperty : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String HEADER_LIBRARY = "UCShowProperty";
    private const String NAME_SHOW = "Show";
    private const Boolean INIT_SHOW = true;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private DOnShowPropertyChanged FOnShowPropertyChanged;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    public CUCShowProperty()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetOnShowPropertyChanged(DOnShowPropertyChanged value)
    {
      FOnShowPropertyChanged = value;
    }

    public Boolean ShowProperty
    {
      get { return cbxShowProperty.Checked; }
      set { cbxShowProperty.Checked = value; }
    }

    public String Title
    {
      get { return cbxShowProperty.Text; }
      set { cbxShowProperty.Text = value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Boolean BValue;
      //
      Result &= initdata.OpenSection(HEADER_LIBRARY + header);
      //
      BValue = INIT_SHOW;
      Result &= initdata.ReadBoolean(NAME_SHOW, out BValue, BValue);
      ShowProperty = BValue;
      //
      Result &= initdata.CloseSection();
      //
      cbxShowProperty_CheckedChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(HEADER_LIBRARY + header);
      //
      Result &= initdata.WriteBoolean(NAME_SHOW, ShowProperty);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxShowProperty_CheckedChanged(object sender, EventArgs e)
    {
      if (FOnShowPropertyChanged is DOnShowPropertyChanged)
      {
        FOnShowPropertyChanged(cbxShowProperty.Checked);
      }
    }

  }
}
