﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using NLRealPixel;
using Initdata;
//
namespace UCControl
{
  public partial class CUCUnitScale : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCUnitScale";
    private const String NAME_UNITSCALE = "UnitScale";
    private const EUnitScale INIT_UNITSCALE = EUnitScale.UM;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private DOnUnitScaleChanged FOnUnitScaleChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCUnitScale()
    {
      InitializeComponent();
      //
      UnitScale = EUnitScale.NM;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetOnUnitScaleChanged(DOnUnitScaleChanged value)
    {
      FOnUnitScaleChanged = value;
    }

    private EUnitScale GetUnitScale()
    {
      if (rbtMM.Checked)
      {
        return EUnitScale.MM;
      }
      if (rbtUM.Checked)
      {
        return EUnitScale.UM;
      }
      if (rbtNM.Checked)
      {
        return EUnitScale.NM;
      }
      rbtMM.Checked = true;
      return EUnitScale.MM;
    }
    public void SetUnitScale(EUnitScale value)
    {
      switch (value)
      {
        case EUnitScale.MM:
          rbtMM.Checked = true;
          break;
        case EUnitScale.UM:
          rbtUM.Checked = true;
          break;
        case EUnitScale.NM:
          rbtNM.Checked = true;
          break;
      }
    }
    public EUnitScale UnitScale
    {
      get { return GetUnitScale(); }
      set { SetUnitScale(value); }
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Init
    //--------------------------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      String SValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      SValue = CRealPixel.UnitScaleText(INIT_UNITSCALE);
      Result &= initdata.ReadEnumeration(NAME_UNITSCALE, out SValue, SValue);
      UnitScale = CRealPixel.TextUnitScale(SValue);
      //
      Result &= initdata.CloseSection();
      //
      UnitScale_CheckedChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteEnumeration(NAME_UNITSCALE, CRealPixel.UnitScaleText(UnitScale));
      // 
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void UnitScale_CheckedChanged(object sender, EventArgs e)
    {
      if (FOnUnitScaleChanged is DOnUnitScaleChanged)
      {
        FOnUnitScaleChanged(GetUnitScale());
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Public
    //------------------------------------------------------------------------
    //
    public void Synchronize(CRealPixel realpixel)
    {
      UnitScale = realpixel.UnitScale;
      if (FOnUnitScaleChanged is DOnUnitScaleChanged)
      {
        FOnUnitScaleChanged(GetUnitScale());
      }
    }


  }
}
