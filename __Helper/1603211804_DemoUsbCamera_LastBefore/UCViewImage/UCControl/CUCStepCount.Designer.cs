﻿namespace UCControl
{
  partial class CUCStepCount
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label10 = new System.Windows.Forms.Label();
      this.panel2 = new System.Windows.Forms.Panel();
      this.label6 = new System.Windows.Forms.Label();
      this.nudY = new System.Windows.Forms.NumericUpDown();
      this.label7 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.nudX = new System.Windows.Forms.NumericUpDown();
      this.label9 = new System.Windows.Forms.Label();
      this.panel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudY)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudX)).BeginInit();
      this.SuspendLayout();
      // 
      // label10
      // 
      this.label10.Dock = System.Windows.Forms.DockStyle.Top;
      this.label10.Location = new System.Drawing.Point(0, 0);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(214, 14);
      this.label10.TabIndex = 4;
      this.label10.Text = "StepCount";
      // 
      // panel2
      // 
      this.panel2.BackColor = System.Drawing.SystemColors.Info;
      this.panel2.Controls.Add(this.label6);
      this.panel2.Controls.Add(this.nudY);
      this.panel2.Controls.Add(this.label7);
      this.panel2.Controls.Add(this.label8);
      this.panel2.Controls.Add(this.nudX);
      this.panel2.Controls.Add(this.label9);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(0, 14);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(214, 17);
      this.panel2.TabIndex = 5;
      // 
      // label6
      // 
      this.label6.Dock = System.Windows.Forms.DockStyle.Left;
      this.label6.Location = new System.Drawing.Point(188, 0);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(26, 17);
      this.label6.TabIndex = 6;
      this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudY
      // 
      this.nudY.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudY.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudY.Location = new System.Drawing.Point(141, 0);
      this.nudY.Maximum = new decimal(new int[] {
            101,
            0,
            0,
            0});
      this.nudY.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.nudY.Name = "nudY";
      this.nudY.Size = new System.Drawing.Size(47, 16);
      this.nudY.TabIndex = 5;
      this.nudY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudY.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
      this.nudY.ValueChanged += new System.EventHandler(this.nudY_ValueChanged);
      // 
      // label7
      // 
      this.label7.Dock = System.Windows.Forms.DockStyle.Left;
      this.label7.Location = new System.Drawing.Point(107, 0);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(34, 17);
      this.label7.TabIndex = 4;
      this.label7.Text = "NY";
      this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // label8
      // 
      this.label8.Dock = System.Windows.Forms.DockStyle.Left;
      this.label8.Location = new System.Drawing.Point(81, 0);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(26, 17);
      this.label8.TabIndex = 3;
      this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudX
      // 
      this.nudX.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudX.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudX.Location = new System.Drawing.Point(34, 0);
      this.nudX.Maximum = new decimal(new int[] {
            101,
            0,
            0,
            0});
      this.nudX.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.nudX.Name = "nudX";
      this.nudX.Size = new System.Drawing.Size(47, 16);
      this.nudX.TabIndex = 2;
      this.nudX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudX.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
      this.nudX.ValueChanged += new System.EventHandler(this.nudX_ValueChanged);
      // 
      // label9
      // 
      this.label9.Dock = System.Windows.Forms.DockStyle.Left;
      this.label9.Location = new System.Drawing.Point(0, 0);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(34, 17);
      this.label9.TabIndex = 1;
      this.label9.Text = "NX";
      this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // CUCStepCount
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.label10);
      this.Name = "CUCStepCount";
      this.Size = new System.Drawing.Size(214, 31);
      this.panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudY)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudX)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.NumericUpDown nudY;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.NumericUpDown nudX;
    private System.Windows.Forms.Label label9;
  }
}
