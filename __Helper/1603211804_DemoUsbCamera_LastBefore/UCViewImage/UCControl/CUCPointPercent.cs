﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnPointPercentChanged(Double pointx, Double pointy);
  //
  public partial class CUCPointPercent : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCPointPercent";
    private const String NAME_POINTX = "PointX";
    private const Double INIT_POINTX = 10.0;  // [%]
    private const String NAME_POINTY = "PointY";
    private const Double INIT_POINTY = 10.0;  // [%]
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnPointPercentChanged FOnPointPercentChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCPointPercent()
    {
      InitializeComponent();
      //
      PointX = INIT_POINTX;
      PointY = INIT_POINTY;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public void SetOnPointPercentChanged(DOnPointPercentChanged value)
    {
      FOnPointPercentChanged = value;
    }

    public String Title
    {
      get { return lblTitle.Text; }
      set { lblTitle.Text = value; }
    }

    public String Unit
    {
      get { return lblUnitX.Text; }
      set
      {
        lblUnitX.Text = value;
        lblUnitY.Text = value;
      }
    }

    public String NameX
    {
      get { return lblNameX.Text; }
      set { lblNameX.Text = value; }
    }

    public String NameY
    {
      get { return lblNameY.Text; }
      set { lblNameY.Text = value; }
    }

    public Int32 WidthNameX
    {
      get { return lblNameX.Width; }
      set { lblNameX.Width = value; }
    }

    public Int32 WidthNameY
    {
      get { return lblNameY.Width; }
      set { lblNameY.Width = value; }
    }

    public Double PointX
    {
      get { return (Double)nudPointX.Value; }
      set
      {
        if ((nudPointX.Minimum <= (Decimal)value) && ((Decimal)value <= nudPointX.Maximum))
        {
          nudPointX.Value = (Decimal)value;
        }
        else
          if ((Decimal)value < nudPointX.Minimum)
          {
            nudPointX.Value = nudPointX.Minimum;
          }
          else
            if (nudPointX.Maximum < (Decimal)value)
            {
              nudPointX.Value = nudPointX.Maximum;
            }
      }
    }

    public Double PointY
    {
      get { return (Double)nudPointY.Value; }
      set
      {
        if ((nudPointY.Minimum <= (Decimal)value) && ((Decimal)value <= nudPointY.Maximum))
        {
          nudPointY.Value = (Decimal)value;
        }
        else
          if ((Decimal)value < nudPointY.Minimum)
          {
            nudPointY.Value = nudPointY.Minimum;
          }
          else
            if (nudPointY.Maximum < (Decimal)value)
            {
              nudPointY.Value = nudPointY.Maximum;
            }
      }
    }

    public Double MinimumX
    {
      get { return (Double)nudPointX.Minimum; }
      set { nudPointX.Minimum = (Decimal)value; }
    }
    public Double MaximumX
    {
      get { return (Double)nudPointX.Maximum; }
      set { nudPointX.Maximum = (Decimal)value; }
    }

    public Double MinimumY
    {
      get { return (Double)nudPointY.Minimum; }
      set { nudPointY.Minimum = (Decimal)value; }
    }
    public Double MaximumY
    {
      get { return (Double)nudPointY.Maximum; }
      set { nudPointY.Maximum = (Decimal)value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Double DValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY + header);
      //
      DValue = INIT_POINTX;
      Result &= initdata.ReadDouble(NAME_POINTX, out DValue, DValue);
      PointX = DValue;
      //
      DValue = INIT_POINTY;
      Result &= initdata.ReadDouble(NAME_POINTY, out DValue, DValue);
      PointY = DValue;
      //
      Result &= initdata.CloseSection();
      //
      nudX_ValueChanged(this, null);
      nudY_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY + header);
      //
      Result &= initdata.WriteDouble(NAME_POINTX, PointX);
      Result &= initdata.WriteDouble(NAME_POINTY, PointY);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void nudX_ValueChanged(object sender, EventArgs e)
    {
      if (FOnPointPercentChanged is DOnPointPercentChanged)
      {
        FOnPointPercentChanged(PointX, PointY);
      }
    }

    private void nudY_ValueChanged(object sender, EventArgs e)
    {
      if (FOnPointPercentChanged is DOnPointPercentChanged)
      {
        FOnPointPercentChanged(PointX, PointY);
      }
    }

  }
}
