﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
//
namespace UCControl
{
  public delegate void DOnMinimumChanged(Double xminimum, Double yminimum);

  public partial class CUCMinimum : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCMinimum";
    private const String NAME_XMINIMUM = "XMinimum";
    private const Double INIT_XMINIMUM = 0.0;
    private const String NAME_YMINIMUM = "YMinimum";
    private const Double INIT_YMINIMUM = 0.0;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnMinimumChanged FOnMinimumChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCMinimum()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetOnMinimumChanged(DOnMinimumChanged value)
    {
      FOnMinimumChanged = value;
    }

    public Double XMinimum
    {
      get { return (Double)nudXMinimum.Value; }
      set { nudXMinimum.Value = (Decimal)value;}
    }

    public Double YMinimum
    {
      get { return (Double)nudYMinimum.Value; }
      set { nudYMinimum.Value = (Decimal)value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Double DValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      DValue = INIT_XMINIMUM;
      Result &= initdata.ReadDouble(NAME_XMINIMUM, out DValue, DValue);
      XMinimum = DValue;
      //
      DValue = INIT_YMINIMUM;
      Result &= initdata.ReadDouble(NAME_YMINIMUM, out DValue, DValue);
      YMinimum = DValue;
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteDouble(NAME_XMINIMUM, XMinimum);
      Result &= initdata.WriteDouble(NAME_YMINIMUM, YMinimum);
      //
      Result &= initdata.CloseSection();
      //
      XYExtrema_ValueChanged(this, null);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void XYExtrema_ValueChanged(object sender, EventArgs e)
    {
      if (FOnMinimumChanged is DOnMinimumChanged)
      {
        FOnMinimumChanged((Double)nudXMinimum.Value, (Double)nudYMinimum.Value);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Public
    //------------------------------------------------------------------------
    //
    public void Synchronize(CRealPixel realpixel)
    {
      XMinimum = realpixel.XRealMinimum;
      YMinimum = realpixel.YRealMinimum;
      if (FOnMinimumChanged is DOnMinimumChanged)
      {
        FOnMinimumChanged((Double)nudXMinimum.Value, (Double)nudYMinimum.Value);
      }
    }


  }
}
