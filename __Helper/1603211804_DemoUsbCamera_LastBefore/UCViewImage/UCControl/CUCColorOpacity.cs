﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
using NLGraphicElement;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnColorOpacityChanged(Color color, Byte opacity);
  //
  public partial class CUCColorOpacity : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String HEADER_LIBRARY = "UCColorOpacity";
    private const String NAME_COLOR = "Color";
    private Color INIT_COLOR = Color.FromArgb(0xFF, 0xFF, 0x88, 0x44);
    private const String NAME_OPACITY = "Opacity";
    private const Byte INIT_OPACITY = 100; // [%]
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnColorOpacityChanged FOnColorOpacityChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCColorOpacity()
    {
      InitializeComponent();
      //
      SetColor(INIT_COLOR);
      SetOpacity(INIT_OPACITY);
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public String Title
    {
      get { return lblTitle.Text; }
      set { lblTitle.Text = value;  }
    }

    public Color GetColor()
    {
      return lblColorText.BackColor; 
    }
    public void SetColor(Color value)
    {
      BuildColorBackForeground(value);
    }

    public Byte GetOpacity()
    {
      return (Byte)nudOpacity.Value;
    }
    public void SetOpacity(Byte value)
    {
      nudOpacity.Value = value;
    }

    public void SetOnColorOpacityChanged(DOnColorOpacityChanged value)
    {
      FOnColorOpacityChanged = value;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Color CValue;
      Byte BValue;
      //
      Result &= initdata.OpenSection(HEADER_LIBRARY + header);
      //
      CValue = INIT_COLOR;
      Result &= initdata.ReadColor(NAME_COLOR, out CValue, CValue);
      SetColor(CValue);
      //
      BValue = INIT_OPACITY;
      Result &= initdata.ReadByte(NAME_OPACITY, out BValue, BValue);
      SetOpacity(BValue);
      //
      Result &= initdata.CloseSection();
      //
      // NC btnColorRGB_Click(this, null);
      nudOpacityRow_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(HEADER_LIBRARY + header);
      //
      Result &= initdata.WriteColor(NAME_COLOR, GetColor());
      Result &= initdata.WriteByte(NAME_OPACITY, GetOpacity());
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------------------------
    //
    private void BuildColorBackForeground(Color colorbackground)
    {
      lblColorText.BackColor = colorbackground;
      Color ColorForeground = NLGraphicElement.CCommon.BuildColorComplementary(colorbackground);
      lblColorText.ForeColor = ColorForeground;
      lblColorText.Text = String.Format("{0:X2} {1:X2} {2:X2}",
                                         colorbackground.R, colorbackground.G, colorbackground.B);
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void CUCColorOpacity_SizeChanged(object sender, EventArgs e)
    {
      pnlColor.Width = this.ClientRectangle.Width / 2;
    }

    private void btnColorRGB_Click(object sender, EventArgs e)
    {
      ColorDialog CD = new ColorDialog();
      CD.Color = GetColor();
      if (DialogResult.OK == CD.ShowDialog())
      {
        SetColor(CD.Color);
        if (FOnColorOpacityChanged is DOnColorOpacityChanged)
        {
          FOnColorOpacityChanged(GetColor(), GetOpacity());
        }
      }
    }

    private void nudOpacityRow_ValueChanged(object sender, EventArgs e)
    {
      SetOpacity((Byte)nudOpacity.Value);
      if (FOnColorOpacityChanged is DOnColorOpacityChanged)
      {
        FOnColorOpacityChanged(GetColor(), GetOpacity());
      }
    }

  }
}
