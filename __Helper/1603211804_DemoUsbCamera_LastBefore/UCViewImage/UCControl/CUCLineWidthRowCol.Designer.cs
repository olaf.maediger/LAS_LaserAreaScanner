﻿namespace UCControl
{
  partial class CUCLineWidthRowCol
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel1 = new System.Windows.Forms.Panel();
      this.pnlOpacityCol = new System.Windows.Forms.Panel();
      this.nudLineWidthCol = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.pnlOpacityRow = new System.Windows.Forms.Panel();
      this.nudLineWidthRow = new System.Windows.Forms.NumericUpDown();
      this.label14 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.pnlTitle = new System.Windows.Forms.Panel();
      this.lblTitleCol = new System.Windows.Forms.Label();
      this.lblTitleRow = new System.Windows.Forms.Label();
      this.panel1.SuspendLayout();
      this.pnlOpacityCol.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudLineWidthCol)).BeginInit();
      this.pnlOpacityRow.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudLineWidthRow)).BeginInit();
      this.pnlTitle.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.pnlOpacityCol);
      this.panel1.Controls.Add(this.pnlOpacityRow);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 16);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(214, 15);
      this.panel1.TabIndex = 29;
      // 
      // pnlOpacityCol
      // 
      this.pnlOpacityCol.Controls.Add(this.nudLineWidthCol);
      this.pnlOpacityCol.Controls.Add(this.label2);
      this.pnlOpacityCol.Controls.Add(this.label1);
      this.pnlOpacityCol.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlOpacityCol.Location = new System.Drawing.Point(106, 0);
      this.pnlOpacityCol.Name = "pnlOpacityCol";
      this.pnlOpacityCol.Size = new System.Drawing.Size(108, 15);
      this.pnlOpacityCol.TabIndex = 27;
      // 
      // nudLineWidthCol
      // 
      this.nudLineWidthCol.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudLineWidthCol.Dock = System.Windows.Forms.DockStyle.Fill;
      this.nudLineWidthCol.Location = new System.Drawing.Point(41, 0);
      this.nudLineWidthCol.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudLineWidthCol.Name = "nudLineWidthCol";
      this.nudLineWidthCol.Size = new System.Drawing.Size(41, 16);
      this.nudLineWidthCol.TabIndex = 27;
      this.nudLineWidthCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudLineWidthCol.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudLineWidthCol.ValueChanged += new System.EventHandler(this.nudLineWidthCol_ValueChanged);
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Right;
      this.label2.Location = new System.Drawing.Point(82, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(26, 15);
      this.label2.TabIndex = 26;
      this.label2.Text = "pxl";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Left;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(41, 15);
      this.label1.TabIndex = 25;
      this.label1.Text = "LWC";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnlOpacityRow
      // 
      this.pnlOpacityRow.Controls.Add(this.nudLineWidthRow);
      this.pnlOpacityRow.Controls.Add(this.label14);
      this.pnlOpacityRow.Controls.Add(this.label5);
      this.pnlOpacityRow.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlOpacityRow.Location = new System.Drawing.Point(0, 0);
      this.pnlOpacityRow.Name = "pnlOpacityRow";
      this.pnlOpacityRow.Size = new System.Drawing.Size(106, 15);
      this.pnlOpacityRow.TabIndex = 26;
      // 
      // nudLineWidthRow
      // 
      this.nudLineWidthRow.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudLineWidthRow.Dock = System.Windows.Forms.DockStyle.Fill;
      this.nudLineWidthRow.Location = new System.Drawing.Point(41, 0);
      this.nudLineWidthRow.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudLineWidthRow.Name = "nudLineWidthRow";
      this.nudLineWidthRow.Size = new System.Drawing.Size(39, 16);
      this.nudLineWidthRow.TabIndex = 26;
      this.nudLineWidthRow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudLineWidthRow.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudLineWidthRow.ValueChanged += new System.EventHandler(this.nudLineWidthRow_ValueChanged);
      // 
      // label14
      // 
      this.label14.Dock = System.Windows.Forms.DockStyle.Right;
      this.label14.Location = new System.Drawing.Point(80, 0);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(26, 15);
      this.label14.TabIndex = 25;
      this.label14.Text = "pxl";
      this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Left;
      this.label5.Location = new System.Drawing.Point(0, 0);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(41, 15);
      this.label5.TabIndex = 24;
      this.label5.Text = "LWR";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnlTitle
      // 
      this.pnlTitle.Controls.Add(this.lblTitleCol);
      this.pnlTitle.Controls.Add(this.lblTitleRow);
      this.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTitle.Location = new System.Drawing.Point(0, 0);
      this.pnlTitle.Name = "pnlTitle";
      this.pnlTitle.Size = new System.Drawing.Size(214, 16);
      this.pnlTitle.TabIndex = 27;
      // 
      // lblTitleCol
      // 
      this.lblTitleCol.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblTitleCol.Location = new System.Drawing.Point(106, 0);
      this.lblTitleCol.Name = "lblTitleCol";
      this.lblTitleCol.Size = new System.Drawing.Size(108, 16);
      this.lblTitleCol.TabIndex = 18;
      this.lblTitleCol.Text = "LineWidth - Col";
      this.lblTitleCol.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lblTitleRow
      // 
      this.lblTitleRow.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblTitleRow.Location = new System.Drawing.Point(0, 0);
      this.lblTitleRow.Name = "lblTitleRow";
      this.lblTitleRow.Size = new System.Drawing.Size(106, 16);
      this.lblTitleRow.TabIndex = 17;
      this.lblTitleRow.Text = "LineWidth - Row";
      this.lblTitleRow.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // CUCLineWidthRowCol
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.pnlTitle);
      this.Name = "CUCLineWidthRowCol";
      this.Size = new System.Drawing.Size(214, 33);
      this.panel1.ResumeLayout(false);
      this.pnlOpacityCol.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudLineWidthCol)).EndInit();
      this.pnlOpacityRow.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudLineWidthRow)).EndInit();
      this.pnlTitle.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel pnlOpacityCol;
    private System.Windows.Forms.Panel pnlOpacityRow;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Panel pnlTitle;
    private System.Windows.Forms.Label lblTitleCol;
    private System.Windows.Forms.Label lblTitleRow;
    private System.Windows.Forms.NumericUpDown nudLineWidthRow;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.NumericUpDown nudLineWidthCol;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
  }
}
