﻿namespace UCControl
{
  partial class CUCPointPercent
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblTitle = new System.Windows.Forms.Label();
      this.lblNameX = new System.Windows.Forms.Label();
      this.nudPointX = new System.Windows.Forms.NumericUpDown();
      this.lblUnitX = new System.Windows.Forms.Label();
      this.lblNameY = new System.Windows.Forms.Label();
      this.nudPointY = new System.Windows.Forms.NumericUpDown();
      this.lblUnitY = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudPointX)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPointY)).BeginInit();
      this.SuspendLayout();
      // 
      // lblTitle
      // 
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(214, 14);
      this.lblTitle.TabIndex = 4;
      this.lblTitle.Text = "PointPercent";
      // 
      // lblNameX
      // 
      this.lblNameX.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblNameX.Location = new System.Drawing.Point(0, 14);
      this.lblNameX.Name = "lblNameX";
      this.lblNameX.Size = new System.Drawing.Size(40, 17);
      this.lblNameX.TabIndex = 5;
      this.lblNameX.Text = "PointX";
      this.lblNameX.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudPointX
      // 
      this.nudPointX.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudPointX.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudPointX.Location = new System.Drawing.Point(40, 14);
      this.nudPointX.Name = "nudPointX";
      this.nudPointX.Size = new System.Drawing.Size(49, 16);
      this.nudPointX.TabIndex = 6;
      this.nudPointX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPointX.ValueChanged += new System.EventHandler(this.nudX_ValueChanged);
      // 
      // lblUnitX
      // 
      this.lblUnitX.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitX.Location = new System.Drawing.Point(89, 14);
      this.lblUnitX.Name = "lblUnitX";
      this.lblUnitX.Size = new System.Drawing.Size(18, 17);
      this.lblUnitX.TabIndex = 7;
      this.lblUnitX.Text = "%";
      this.lblUnitX.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lblNameY
      // 
      this.lblNameY.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblNameY.Location = new System.Drawing.Point(107, 14);
      this.lblNameY.Name = "lblNameY";
      this.lblNameY.Size = new System.Drawing.Size(40, 17);
      this.lblNameY.TabIndex = 8;
      this.lblNameY.Text = "PointY";
      this.lblNameY.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudPointY
      // 
      this.nudPointY.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudPointY.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudPointY.Location = new System.Drawing.Point(147, 14);
      this.nudPointY.Name = "nudPointY";
      this.nudPointY.Size = new System.Drawing.Size(49, 16);
      this.nudPointY.TabIndex = 9;
      this.nudPointY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPointY.ValueChanged += new System.EventHandler(this.nudY_ValueChanged);
      // 
      // lblUnitY
      // 
      this.lblUnitY.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitY.Location = new System.Drawing.Point(196, 14);
      this.lblUnitY.Name = "lblUnitY";
      this.lblUnitY.Size = new System.Drawing.Size(18, 17);
      this.lblUnitY.TabIndex = 10;
      this.lblUnitY.Text = "%";
      this.lblUnitY.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // CUCPointPercent
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.lblUnitY);
      this.Controls.Add(this.nudPointY);
      this.Controls.Add(this.lblNameY);
      this.Controls.Add(this.lblUnitX);
      this.Controls.Add(this.nudPointX);
      this.Controls.Add(this.lblNameX);
      this.Controls.Add(this.lblTitle);
      this.Name = "CUCPointPercent";
      this.Size = new System.Drawing.Size(214, 31);
      ((System.ComponentModel.ISupportInitialize)(this.nudPointX)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPointY)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.Label lblNameX;
    private System.Windows.Forms.NumericUpDown nudPointX;
    private System.Windows.Forms.Label lblUnitX;
    private System.Windows.Forms.Label lblNameY;
    private System.Windows.Forms.NumericUpDown nudPointY;
    private System.Windows.Forms.Label lblUnitY;
  }
}
