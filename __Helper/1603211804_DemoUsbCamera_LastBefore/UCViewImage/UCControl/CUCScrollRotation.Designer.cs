﻿namespace UCScrollRotation
{
  partial class CUCScrollRotation
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.scbRotation = new System.Windows.Forms.HScrollBar();
      this.SuspendLayout();
      // 
      // scbRotation
      // 
      this.scbRotation.Dock = System.Windows.Forms.DockStyle.Top;
      this.scbRotation.Location = new System.Drawing.Point(0, 0);
      this.scbRotation.Maximum = 360;
      this.scbRotation.Name = "scbRotation";
      this.scbRotation.Size = new System.Drawing.Size(428, 17);
      this.scbRotation.TabIndex = 0;
      this.scbRotation.ValueChanged += new System.EventHandler(this.scbRotation_ValueChanged);
      // 
      // CUCScrollRotation
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.scbRotation);
      this.Name = "CUCScrollRotation";
      this.Size = new System.Drawing.Size(428, 17);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.HScrollBar scbRotation;
  }
}
