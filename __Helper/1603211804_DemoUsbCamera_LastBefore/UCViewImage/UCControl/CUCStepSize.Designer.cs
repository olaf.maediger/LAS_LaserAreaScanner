﻿namespace UCControl
{
  partial class CUCStepSize
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label13 = new System.Windows.Forms.Label();
      this.lblUnitDX = new System.Windows.Forms.Label();
      this.lblStepSizeX = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.lblUnitDY = new System.Windows.Forms.Label();
      this.lblStepSizeY = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // label13
      // 
      this.label13.Dock = System.Windows.Forms.DockStyle.Top;
      this.label13.Location = new System.Drawing.Point(0, 0);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(214, 14);
      this.label13.TabIndex = 6;
      this.label13.Text = "StepSize";
      this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblUnitDX
      // 
      this.lblUnitDX.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitDX.Location = new System.Drawing.Point(81, 14);
      this.lblUnitDX.Name = "lblUnitDX";
      this.lblUnitDX.Size = new System.Drawing.Size(26, 19);
      this.lblUnitDX.TabIndex = 10;
      this.lblUnitDX.Text = "um";
      this.lblUnitDX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblStepSizeX
      // 
      this.lblStepSizeX.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblStepSizeX.Location = new System.Drawing.Point(27, 14);
      this.lblStepSizeX.Name = "lblStepSizeX";
      this.lblStepSizeX.Size = new System.Drawing.Size(54, 19);
      this.lblStepSizeX.TabIndex = 9;
      this.lblStepSizeX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label12
      // 
      this.label12.Dock = System.Windows.Forms.DockStyle.Left;
      this.label12.Location = new System.Drawing.Point(0, 14);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(27, 19);
      this.label12.TabIndex = 8;
      this.label12.Text = "DX";
      this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblUnitDY
      // 
      this.lblUnitDY.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitDY.Location = new System.Drawing.Point(188, 14);
      this.lblUnitDY.Name = "lblUnitDY";
      this.lblUnitDY.Size = new System.Drawing.Size(26, 19);
      this.lblUnitDY.TabIndex = 13;
      this.lblUnitDY.Text = "um";
      this.lblUnitDY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblStepSizeY
      // 
      this.lblStepSizeY.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblStepSizeY.Location = new System.Drawing.Point(134, 14);
      this.lblStepSizeY.Name = "lblStepSizeY";
      this.lblStepSizeY.Size = new System.Drawing.Size(54, 19);
      this.lblStepSizeY.TabIndex = 12;
      this.lblStepSizeY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label4
      // 
      this.label4.Dock = System.Windows.Forms.DockStyle.Left;
      this.label4.Location = new System.Drawing.Point(107, 14);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(27, 19);
      this.label4.TabIndex = 11;
      this.label4.Text = "DY";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCStepSize
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.lblUnitDY);
      this.Controls.Add(this.lblStepSizeY);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.lblUnitDX);
      this.Controls.Add(this.lblStepSizeX);
      this.Controls.Add(this.label12);
      this.Controls.Add(this.label13);
      this.Name = "CUCStepSize";
      this.Size = new System.Drawing.Size(214, 33);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label lblUnitDX;
    private System.Windows.Forms.Label lblStepSizeX;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label lblUnitDY;
    private System.Windows.Forms.Label lblStepSizeY;
    private System.Windows.Forms.Label label4;
  }
}
