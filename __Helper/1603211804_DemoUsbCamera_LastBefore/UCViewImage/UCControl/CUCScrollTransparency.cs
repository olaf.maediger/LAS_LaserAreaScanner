﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
//
namespace UCControl
{ //
  public delegate void DOnTransparencyChanged(float transparency); // t in [0.0 .. 1.0] 
  //
  public partial class CUCScrollTransparency : UserControl
  { //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String HEADER_LIBRARY = "UCScrollTransparency";
    private const String NAME_TRANSPARENCY = "Transparency";
    private const float INIT_TRANSPARENCY = 1f;
    //
    private const Int32 TRANSPARENCY_MAXIMUM = 100;
    private const Int32 TRANSPARENCY_MINIMUM = 0;
    private const Int32 TRANSPARENCY_OFFSET = 7;
    //
    //-----------------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------------
    //
    private DOnTransparencyChanged FOnTransparencyChanged;
    //
    //-----------------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------------
    //
    public CUCScrollTransparency()
    {
      InitializeComponent();
      //
      scbTransparency.Maximum =  TRANSPARENCY_MAXIMUM + TRANSPARENCY_OFFSET; // [%]
      scbTransparency.Minimum = TRANSPARENCY_MINIMUM - TRANSPARENCY_OFFSET; // [%]
      scbTransparency.Value = TRANSPARENCY_MAXIMUM; // [%]
    }
    //
    //-----------------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------------
    //
    public float Transparency
    {
      get { return BuildTransparency(scbTransparency.Value); }
      set { scbTransparency.Value = BuildTransparency(value); }
    }

    public void SetOnTransparencyChanged(DOnTransparencyChanged value)
    {
      FOnTransparencyChanged = value;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      float FValue;
      //
      Result &= initdata.OpenSection(HEADER_LIBRARY + header);
      //
      FValue = INIT_TRANSPARENCY;
      Result &= initdata.ReadFloat(NAME_TRANSPARENCY, out FValue, FValue);
      Transparency = FValue;
      //
      Result &= initdata.CloseSection();
      //
      scbTransparency_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(HEADER_LIBRARY + header);
      //
      Result &= initdata.WriteFloat(NAME_TRANSPARENCY, Transparency);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------------------------
    //
    private float BuildTransparency(Int32 value)
    {
      float Result = (float)value / (float)TRANSPARENCY_MAXIMUM;
      Result = Math.Min(TRANSPARENCY_MAXIMUM, Result);
      Result = Math.Max(TRANSPARENCY_MINIMUM, Result);
      // debug Console.WriteLine(String.Format("{0} -> {1}", value, Result));
      return Result;
    }

    private Int32 BuildTransparency(float value)
    {
      Int32 Result = (Int32)(0.5f + (float)TRANSPARENCY_MAXIMUM * value);
      Result = Math.Min(scbTransparency.Maximum, Result);
      Result = Math.Max(scbTransparency.Minimum, Result);
      // debug Console.WriteLine(String.Format("{0} -> {1}", value, Result));
      return Result;
    }
    //
    //-----------------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------------
    //
    private void scbTransparency_ValueChanged(object sender, EventArgs e)
    {
      if (FOnTransparencyChanged is DOnTransparencyChanged)
      {
        float Transparency = BuildTransparency(scbTransparency.Value);
        FOnTransparencyChanged(Transparency);
      }
    }

  }
}
