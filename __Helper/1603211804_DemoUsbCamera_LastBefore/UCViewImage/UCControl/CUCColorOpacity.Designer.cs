﻿namespace UCControl
{
  partial class CUCColorOpacity
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblTitle = new System.Windows.Forms.Label();
      this.pnlColor = new System.Windows.Forms.Panel();
      this.lblColorText = new System.Windows.Forms.Label();
      this.btnColorRGB = new System.Windows.Forms.Button();
      this.pnlOpacity = new System.Windows.Forms.Panel();
      this.nudOpacity = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.pnlColor.SuspendLayout();
      this.pnlOpacity.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudOpacity)).BeginInit();
      this.SuspendLayout();
      // 
      // lblTitle
      // 
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(240, 19);
      this.lblTitle.TabIndex = 19;
      this.lblTitle.Text = "Title";
      this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pnlColor
      // 
      this.pnlColor.Controls.Add(this.lblColorText);
      this.pnlColor.Controls.Add(this.btnColorRGB);
      this.pnlColor.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlColor.Location = new System.Drawing.Point(0, 19);
      this.pnlColor.Name = "pnlColor";
      this.pnlColor.Size = new System.Drawing.Size(139, 20);
      this.pnlColor.TabIndex = 20;
      // 
      // lblColorText
      // 
      this.lblColorText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblColorText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblColorText.Location = new System.Drawing.Point(41, 0);
      this.lblColorText.Name = "lblColorText";
      this.lblColorText.Size = new System.Drawing.Size(98, 20);
      this.lblColorText.TabIndex = 30;
      this.lblColorText.Text = "AA BB CC";
      this.lblColorText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnColorRGB
      // 
      this.btnColorRGB.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnColorRGB.Location = new System.Drawing.Point(0, 0);
      this.btnColorRGB.Name = "btnColorRGB";
      this.btnColorRGB.Size = new System.Drawing.Size(41, 20);
      this.btnColorRGB.TabIndex = 29;
      this.btnColorRGB.Text = "Color";
      this.btnColorRGB.UseVisualStyleBackColor = true;
      this.btnColorRGB.Click += new System.EventHandler(this.btnColorRGB_Click);
      // 
      // pnlOpacity
      // 
      this.pnlOpacity.Controls.Add(this.nudOpacity);
      this.pnlOpacity.Controls.Add(this.label2);
      this.pnlOpacity.Controls.Add(this.label1);
      this.pnlOpacity.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlOpacity.Location = new System.Drawing.Point(139, 19);
      this.pnlOpacity.Name = "pnlOpacity";
      this.pnlOpacity.Size = new System.Drawing.Size(101, 20);
      this.pnlOpacity.TabIndex = 21;
      // 
      // nudOpacity
      // 
      this.nudOpacity.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudOpacity.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.nudOpacity.Location = new System.Drawing.Point(47, 4);
      this.nudOpacity.Name = "nudOpacity";
      this.nudOpacity.Size = new System.Drawing.Size(38, 16);
      this.nudOpacity.TabIndex = 30;
      this.nudOpacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudOpacity.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudOpacity.ValueChanged += new System.EventHandler(this.nudOpacityRow_ValueChanged);
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Right;
      this.label2.Location = new System.Drawing.Point(85, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(16, 20);
      this.label2.TabIndex = 28;
      this.label2.Text = "%";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Left;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(47, 20);
      this.label1.TabIndex = 19;
      this.label1.Text = "Opacity";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCColorOpacity
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.pnlOpacity);
      this.Controls.Add(this.pnlColor);
      this.Controls.Add(this.lblTitle);
      this.Name = "CUCColorOpacity";
      this.Size = new System.Drawing.Size(240, 39);
      this.SizeChanged += new System.EventHandler(this.CUCColorOpacity_SizeChanged);
      this.pnlColor.ResumeLayout(false);
      this.pnlOpacity.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudOpacity)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.Panel pnlColor;
    private System.Windows.Forms.Label lblColorText;
    private System.Windows.Forms.Button btnColorRGB;
    private System.Windows.Forms.Panel pnlOpacity;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudOpacity;
  }
}
