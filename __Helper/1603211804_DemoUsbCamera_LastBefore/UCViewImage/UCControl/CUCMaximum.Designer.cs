﻿namespace UCControl
{
  partial class CUCMaximum
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.pnlLeft = new System.Windows.Forms.Panel();
      this.nudXMaximum = new System.Windows.Forms.NumericUpDown();
      this.lblUnitX = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.pnlRight = new System.Windows.Forms.Panel();
      this.nudYMaximum = new System.Windows.Forms.NumericUpDown();
      this.lblUnitY = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.pnlLeft.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudXMaximum)).BeginInit();
      this.pnlRight.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudYMaximum)).BeginInit();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Top;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(277, 13);
      this.label1.TabIndex = 9;
      this.label1.Text = "Maximum";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pnlLeft
      // 
      this.pnlLeft.Controls.Add(this.nudXMaximum);
      this.pnlLeft.Controls.Add(this.lblUnitX);
      this.pnlLeft.Controls.Add(this.label2);
      this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlLeft.Location = new System.Drawing.Point(0, 13);
      this.pnlLeft.Name = "pnlLeft";
      this.pnlLeft.Size = new System.Drawing.Size(133, 17);
      this.pnlLeft.TabIndex = 10;
      // 
      // nudXMaximum
      // 
      this.nudXMaximum.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudXMaximum.Dock = System.Windows.Forms.DockStyle.Fill;
      this.nudXMaximum.Location = new System.Drawing.Point(64, 0);
      this.nudXMaximum.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
      this.nudXMaximum.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
      this.nudXMaximum.Name = "nudXMaximum";
      this.nudXMaximum.Size = new System.Drawing.Size(43, 16);
      this.nudXMaximum.TabIndex = 19;
      this.nudXMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudXMaximum.ValueChanged += new System.EventHandler(this.XYExtrema_ValueChanged);
      // 
      // lblUnitX
      // 
      this.lblUnitX.Dock = System.Windows.Forms.DockStyle.Right;
      this.lblUnitX.Location = new System.Drawing.Point(107, 0);
      this.lblUnitX.Name = "lblUnitX";
      this.lblUnitX.Size = new System.Drawing.Size(26, 17);
      this.lblUnitX.TabIndex = 18;
      this.lblUnitX.Text = "um";
      this.lblUnitX.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Left;
      this.label2.Location = new System.Drawing.Point(0, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(64, 17);
      this.label2.TabIndex = 16;
      this.label2.Text = "XMaximum";
      this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // pnlRight
      // 
      this.pnlRight.Controls.Add(this.nudYMaximum);
      this.pnlRight.Controls.Add(this.lblUnitY);
      this.pnlRight.Controls.Add(this.label5);
      this.pnlRight.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlRight.Location = new System.Drawing.Point(133, 13);
      this.pnlRight.Name = "pnlRight";
      this.pnlRight.Size = new System.Drawing.Size(144, 17);
      this.pnlRight.TabIndex = 11;
      // 
      // nudYMaximum
      // 
      this.nudYMaximum.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudYMaximum.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudYMaximum.Location = new System.Drawing.Point(64, 0);
      this.nudYMaximum.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudYMaximum.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
      this.nudYMaximum.Name = "nudYMaximum";
      this.nudYMaximum.Size = new System.Drawing.Size(54, 16);
      this.nudYMaximum.TabIndex = 21;
      this.nudYMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudYMaximum.ValueChanged += new System.EventHandler(this.XYExtrema_ValueChanged);
      // 
      // lblUnitY
      // 
      this.lblUnitY.Dock = System.Windows.Forms.DockStyle.Right;
      this.lblUnitY.Location = new System.Drawing.Point(118, 0);
      this.lblUnitY.Name = "lblUnitY";
      this.lblUnitY.Size = new System.Drawing.Size(26, 17);
      this.lblUnitY.TabIndex = 20;
      this.lblUnitY.Text = "um";
      this.lblUnitY.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Left;
      this.label5.Location = new System.Drawing.Point(0, 0);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(64, 17);
      this.label5.TabIndex = 18;
      this.label5.Text = "YMaximum";
      this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // CUCMaximum
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.pnlRight);
      this.Controls.Add(this.pnlLeft);
      this.Controls.Add(this.label1);
      this.Name = "CUCMaximum";
      this.Size = new System.Drawing.Size(277, 30);
      this.pnlLeft.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudXMaximum)).EndInit();
      this.pnlRight.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudYMaximum)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Panel pnlLeft;
    private System.Windows.Forms.NumericUpDown nudXMaximum;
    private System.Windows.Forms.Label lblUnitX;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Panel pnlRight;
    private System.Windows.Forms.NumericUpDown nudYMaximum;
    private System.Windows.Forms.Label lblUnitY;
    private System.Windows.Forms.Label label5;
  }
}
