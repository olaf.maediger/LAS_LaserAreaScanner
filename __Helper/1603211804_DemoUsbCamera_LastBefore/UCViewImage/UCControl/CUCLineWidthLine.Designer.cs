﻿namespace UCControl
{
  partial class CUCLineWidthLine
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlTop = new System.Windows.Forms.Panel();
      this.nudLineWidth = new System.Windows.Forms.NumericUpDown();
      this.lblTitle = new System.Windows.Forms.Label();
      this.label14 = new System.Windows.Forms.Label();
      this.panel2 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.panel5 = new System.Windows.Forms.Panel();
      this.panel4 = new System.Windows.Forms.Panel();
      this.lblShow = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudLineWidth)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlTop
      // 
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(214, 7);
      this.pnlTop.TabIndex = 0;
      // 
      // nudLineWidth
      // 
      this.nudLineWidth.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudLineWidth.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudLineWidth.Location = new System.Drawing.Point(57, 7);
      this.nudLineWidth.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudLineWidth.Name = "nudLineWidth";
      this.nudLineWidth.Size = new System.Drawing.Size(41, 16);
      this.nudLineWidth.TabIndex = 33;
      this.nudLineWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudLineWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudLineWidth.ValueChanged += new System.EventHandler(this.nudLineWidthLine_ValueChanged);
      // 
      // lblTitle
      // 
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblTitle.Location = new System.Drawing.Point(0, 7);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(57, 19);
      this.lblTitle.TabIndex = 32;
      this.lblTitle.Text = "LineWidth";
      this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // label14
      // 
      this.label14.Dock = System.Windows.Forms.DockStyle.Left;
      this.label14.Location = new System.Drawing.Point(98, 7);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(24, 19);
      this.label14.TabIndex = 34;
      this.label14.Text = "pxl";
      this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // panel2
      // 
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(122, 7);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(92, 5);
      this.panel2.TabIndex = 35;
      // 
      // panel3
      // 
      this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel3.Location = new System.Drawing.Point(122, 20);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(92, 6);
      this.panel3.TabIndex = 36;
      // 
      // panel5
      // 
      this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel5.Location = new System.Drawing.Point(122, 12);
      this.panel5.Name = "panel5";
      this.panel5.Size = new System.Drawing.Size(5, 8);
      this.panel5.TabIndex = 37;
      // 
      // panel4
      // 
      this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel4.Location = new System.Drawing.Point(209, 12);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(5, 8);
      this.panel4.TabIndex = 38;
      // 
      // lblShow
      // 
      this.lblShow.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblShow.Location = new System.Drawing.Point(127, 12);
      this.lblShow.Name = "lblShow";
      this.lblShow.Size = new System.Drawing.Size(82, 8);
      this.lblShow.TabIndex = 39;
      this.lblShow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lblShow.Paint += new System.Windows.Forms.PaintEventHandler(this.lblShow_Paint);
      // 
      // CUCLineWidthLine
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.lblShow);
      this.Controls.Add(this.panel4);
      this.Controls.Add(this.panel5);
      this.Controls.Add(this.panel3);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.label14);
      this.Controls.Add(this.nudLineWidth);
      this.Controls.Add(this.lblTitle);
      this.Controls.Add(this.pnlTop);
      this.Name = "CUCLineWidthLine";
      this.Size = new System.Drawing.Size(214, 26);
      this.SizeChanged += new System.EventHandler(this.CUCLineWidthShow_SizeChanged);
      ((System.ComponentModel.ISupportInitialize)(this.nudLineWidth)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.NumericUpDown nudLineWidth;
    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Panel panel5;
    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.Label lblShow;

  }
}
