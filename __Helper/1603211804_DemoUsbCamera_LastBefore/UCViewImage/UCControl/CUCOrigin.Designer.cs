﻿namespace UCControl
{
  partial class CUCOrigin
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.nudX = new System.Windows.Forms.NumericUpDown();
      this.lblUnitOriginX0 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.nudY = new System.Windows.Forms.NumericUpDown();
      this.lblUnitOriginY0 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudX)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudY)).BeginInit();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Top;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(214, 14);
      this.label1.TabIndex = 3;
      this.label1.Text = "Origin";
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Left;
      this.label2.Location = new System.Drawing.Point(0, 14);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(27, 17);
      this.label2.TabIndex = 4;
      this.label2.Text = "X0";
      this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudX
      // 
      this.nudX.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudX.DecimalPlaces = 3;
      this.nudX.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudX.Location = new System.Drawing.Point(27, 14);
      this.nudX.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
      this.nudX.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
      this.nudX.Name = "nudX";
      this.nudX.Size = new System.Drawing.Size(54, 16);
      this.nudX.TabIndex = 5;
      this.nudX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudX.ValueChanged += new System.EventHandler(this.nudX_ValueChanged);
      // 
      // lblUnitOriginX0
      // 
      this.lblUnitOriginX0.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitOriginX0.Location = new System.Drawing.Point(81, 14);
      this.lblUnitOriginX0.Name = "lblUnitOriginX0";
      this.lblUnitOriginX0.Size = new System.Drawing.Size(26, 17);
      this.lblUnitOriginX0.TabIndex = 6;
      this.lblUnitOriginX0.Text = "um";
      this.lblUnitOriginX0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Left;
      this.label5.Location = new System.Drawing.Point(107, 14);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(27, 17);
      this.label5.TabIndex = 7;
      this.label5.Text = "Y0";
      this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudY
      // 
      this.nudY.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudY.DecimalPlaces = 3;
      this.nudY.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudY.Location = new System.Drawing.Point(134, 14);
      this.nudY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudY.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
      this.nudY.Name = "nudY";
      this.nudY.Size = new System.Drawing.Size(54, 16);
      this.nudY.TabIndex = 8;
      this.nudY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudY.ValueChanged += new System.EventHandler(this.nudY_ValueChanged);
      // 
      // lblUnitOriginY0
      // 
      this.lblUnitOriginY0.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitOriginY0.Location = new System.Drawing.Point(188, 14);
      this.lblUnitOriginY0.Name = "lblUnitOriginY0";
      this.lblUnitOriginY0.Size = new System.Drawing.Size(26, 17);
      this.lblUnitOriginY0.TabIndex = 9;
      this.lblUnitOriginY0.Text = "um";
      this.lblUnitOriginY0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // CUCOrigin
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.lblUnitOriginY0);
      this.Controls.Add(this.nudY);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.lblUnitOriginX0);
      this.Controls.Add(this.nudX);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Name = "CUCOrigin";
      this.Size = new System.Drawing.Size(214, 31);
      ((System.ComponentModel.ISupportInitialize)(this.nudX)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudY)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown nudX;
    private System.Windows.Forms.Label lblUnitOriginX0;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.NumericUpDown nudY;
    private System.Windows.Forms.Label lblUnitOriginY0;
  }
}
