﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
//
namespace UCControl
{
  public delegate void DOnMaximumChanged(Double xmaximum, Double ymaximum);
  //
  public partial class CUCMaximum : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCMaximum";
    private const String NAME_XMAXIMUM = "XMaximum";
    private const Double INIT_XMAXIMUM = 10.0;
    private const String NAME_YMAXIMUM = "YMaximum";
    private const Double INIT_YMAXIMUM = 10.0;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnMaximumChanged FOnMaximumChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCMaximum()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetOnMaximumChanged(DOnMaximumChanged value)
    {
      FOnMaximumChanged = value;
    }

    public Double XMaximum
    {
      get { return (Double)nudXMaximum.Value; }
      set { nudXMaximum.Value = (Decimal)value; }
    }

    public Double YMaximum
    {
      get { return (Double)nudYMaximum.Value; }
      set { nudYMaximum.Value = (Decimal)value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Double DValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      DValue = INIT_XMAXIMUM;
      Result &= initdata.ReadDouble(NAME_XMAXIMUM, out DValue, DValue);
      XMaximum = DValue;
      //
      DValue = INIT_YMAXIMUM;
      Result &= initdata.ReadDouble(NAME_YMAXIMUM, out DValue, DValue);
      YMaximum = DValue;
      //
      Result &= initdata.CloseSection();
      //
      XYExtrema_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteDouble(NAME_XMAXIMUM, XMaximum);
      Result &= initdata.WriteDouble(NAME_YMAXIMUM, YMaximum);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void XYExtrema_ValueChanged(object sender, EventArgs e)
    {
      if (FOnMaximumChanged is DOnMaximumChanged)
      {
        FOnMaximumChanged((Double)nudXMaximum.Value, (Double)nudYMaximum.Value);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Public
    //------------------------------------------------------------------------
    //
    public void Synchronize(CRealPixel realpixel)
    {
      XMaximum = realpixel.XRealMaximum;
      YMaximum = realpixel.YRealMaximum;
      if (FOnMaximumChanged is DOnMaximumChanged)
      {
        FOnMaximumChanged((Double)nudXMaximum.Value, (Double)nudYMaximum.Value);
      }
    }



  }
}
