﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnFontColorChanged(Color color);
  //
  public partial class CUCSelectFontColorOpacity : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String HEADER_LIBRARY = "UCSelectFontColorOpacity";
    private const String NAME_VALUE = "Value";
    private const String INIT_VALUE = "<value>";
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    // ?????? private DOnFontColorChanged FOnFontColorChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCSelectFontColorOpacity()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public String Title
    {
      get { return FUCColorOpacity.Title; }
      set { FUCColorOpacity.Title = value; }
    }

    // Font - Color/Opacity
    public Color FontColor
    {
      get { return FUCColorOpacity.GetColor(); }
      set { FUCColorOpacity.SetColor(value); }
    }
    public Byte FontOpacity
    {
      get { return FUCColorOpacity.GetOpacity(); }
      set { FUCColorOpacity.SetOpacity(value); }
    }

    public void SetOnColorOpacityChanged(DOnColorOpacityChanged value)
    {
      FUCColorOpacity.SetOnColorOpacityChanged(value);
    }


    // Font - Select
    public String FontText
    {
      get { return FUCSelectFont.FontText; }
      set { FUCSelectFont.FontText = value; }
    }
    public Font GetFont()
    {
      return FUCSelectFont.GetFont();
    }
    public void SetFont(Font value)
    {
      FUCSelectFont.SetFont(value);
    }

    public void SetOnFontChanged(DOnFontChanged value)
    {
      FUCSelectFont.SetOnFontChanged(value);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.OpenSection(HEADER_LIBRARY + header);
      //
      FUCColorOpacity.LoadInitdata(initdata, "");
      FUCSelectFont.LoadInitdata(initdata, "");
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(HEADER_LIBRARY + header);
      //
      Result &= FUCColorOpacity.SaveInitdata(initdata, "");
      Result &= FUCSelectFont.SaveInitdata(initdata, "");
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //

  }
}
