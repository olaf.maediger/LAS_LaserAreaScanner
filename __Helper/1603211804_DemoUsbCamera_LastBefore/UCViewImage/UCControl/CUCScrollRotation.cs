﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
//
namespace UCScrollRotation
{ //
  public delegate void DOnRotationChanged(Double anglerotation);
  //
  public partial class CUCScrollRotation : UserControl
  { //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String HEADER_LIBRARY = "UCScrollRotation";
    private const String NAME_ROTATIONDEG = "RotationDeg";
    private const Double INIT_ROTATIONDEG = 0.0; // [deg]
    //
    //-----------------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------------
    //
    private DOnRotationChanged FOnRotationChanged;
    //
    //-----------------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------------
    //
    public CUCScrollRotation()
    {
      InitializeComponent();
      //
      scbRotation.Maximum = +180;
      scbRotation.Minimum = -180;
      scbRotation.Value = 0;
    }
    //
    //-----------------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------------
    //
    public void SetOnRotationChanged(DOnRotationChanged value)
    {
      FOnRotationChanged = value;
    }

    public Double AngleDeg
    {
      get { return (Double)scbRotation.Value; }
      set { scbRotation.Value = (Int32)value; }
    }

    public Double AngleRad
    {
      get { return DegRad(AngleDeg); }
      set { AngleDeg = RadDeg(value); }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Double DValue;
      //
      Result &= initdata.OpenSection(HEADER_LIBRARY + header);
      //
      DValue = INIT_ROTATIONDEG;
      Result &= initdata.ReadDouble(NAME_ROTATIONDEG, out DValue, DValue);
      AngleDeg = DValue;
      //
      Result &= initdata.CloseSection();
      //
      scbRotation_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(HEADER_LIBRARY + header);
      //
      Result &= initdata.WriteDouble(NAME_ROTATIONDEG, AngleDeg);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //-----------------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------------
    //
    private Double RadDeg(Double rad)
    {
      return 180.0 * rad / Math.PI;
    }
    private Double DegRad(Double deg)
    {
      return Math.PI * deg / 180.0;
    }
    //
    //-----------------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------------
    //
    private void scbRotation_ValueChanged(object sender, EventArgs e)
    {
      if (FOnRotationChanged is DOnRotationChanged)
      {
        Double RA = AngleRad;
        FOnRotationChanged(RA);
      }
    }

  }
}
