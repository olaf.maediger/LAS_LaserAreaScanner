﻿namespace UCControl
{
  partial class CUCSelectFont
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.FDialogFont = new System.Windows.Forms.FontDialog();
      this.panel1 = new System.Windows.Forms.Panel();
      this.btnSelectFont = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.panel3 = new System.Windows.Forms.Panel();
      this.panel4 = new System.Windows.Forms.Panel();
      this.panel5 = new System.Windows.Forms.Panel();
      this.lblFontText = new System.Windows.Forms.Label();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // FDialogFont
      // 
      this.FDialogFont.AllowVerticalFonts = false;
      this.FDialogFont.FontMustExist = true;
      this.FDialogFont.ShowColor = true;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.btnSelectFont);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(76, 30);
      this.panel1.TabIndex = 15;
      // 
      // btnSelectFont
      // 
      this.btnSelectFont.Location = new System.Drawing.Point(2, 3);
      this.btnSelectFont.Name = "btnSelectFont";
      this.btnSelectFont.Size = new System.Drawing.Size(71, 24);
      this.btnSelectFont.TabIndex = 15;
      this.btnSelectFont.Text = "Select Font";
      this.btnSelectFont.UseVisualStyleBackColor = true;
      this.btnSelectFont.Click += new System.EventHandler(this.btnSelectFont_Click);
      // 
      // panel2
      // 
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.Location = new System.Drawing.Point(76, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(138, 5);
      this.panel2.TabIndex = 16;
      // 
      // panel3
      // 
      this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel3.Location = new System.Drawing.Point(76, 25);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(138, 5);
      this.panel3.TabIndex = 17;
      // 
      // panel4
      // 
      this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel4.Location = new System.Drawing.Point(209, 5);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(5, 20);
      this.panel4.TabIndex = 18;
      // 
      // panel5
      // 
      this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel5.Location = new System.Drawing.Point(76, 5);
      this.panel5.Name = "panel5";
      this.panel5.Size = new System.Drawing.Size(5, 20);
      this.panel5.TabIndex = 19;
      // 
      // lblFontText
      // 
      this.lblFontText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblFontText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblFontText.Location = new System.Drawing.Point(81, 5);
      this.lblFontText.Name = "lblFontText";
      this.lblFontText.Size = new System.Drawing.Size(128, 20);
      this.lblFontText.TabIndex = 20;
      this.lblFontText.Text = "Hello World!";
      this.lblFontText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCSelectFont
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.lblFontText);
      this.Controls.Add(this.panel5);
      this.Controls.Add(this.panel4);
      this.Controls.Add(this.panel3);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.panel1);
      this.Name = "CUCSelectFont";
      this.Size = new System.Drawing.Size(214, 30);
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.FontDialog FDialogFont;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button btnSelectFont;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.Panel panel5;
    private System.Windows.Forms.Label lblFontText;
  }
}
