﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
//
namespace UCControl
{
  public delegate void DOnColorRowColChanged(Color colorrow, Color colorcol);
  //
  public partial class CUCColorOpacityRowCol : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCColorOpacityRowCol";
    private const String NAME_COLORROW = "Row";
    private const Byte INIT_COLORROW_A = 0xFF;
    private const Byte INIT_COLORROW_R = 0xFF;
    private const Byte INIT_COLORROW_G = 0x88;
    private const Byte INIT_COLORROW_B = 0x44;
    private const Byte INIT_OPACITYROW = 100;
    private const String NAME_COLORCOL = "Col";
    private const Byte INIT_COLORCOL_A = 0xFF;
    private const Byte INIT_COLORCOL_R = 0xFF;
    private const Byte INIT_COLORCOL_G = 0x88;
    private const Byte INIT_COLORCOL_B = 0x44;
    private const Byte INIT_OPACITYCOL = 100;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private DOnColorRowColChanged FOnColorRowColChanged;
    private ColorDialog FColorDialog;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    public CUCColorOpacityRowCol()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetColorDialog(ColorDialog value)
    {
      FColorDialog = value;
    }

    public void SetOnColorRowColChanged(DOnColorRowColChanged value)
    {
      FOnColorRowColChanged = value;
    }

    public Color ColorRow
    {
      get 
      {
        return Color.FromArgb((Byte)((255 * nudOpacityRow.Value) / 100),
                                 lblColorRow.BackColor.R,
                                 lblColorRow.BackColor.G,
                                 lblColorRow.BackColor.B);
      }
      set
      {
        lblColorRow.BackColor = Color.FromArgb(0xFF, value.R, value.G, value.B);
        nudOpacityRow.Value = (Byte)((100.0 * value.A) / 255 );
      }
    }

    public Color ColorCol
    {
      get
      {
        return Color.FromArgb((Byte)((255 * nudOpacityCol.Value) / 100),
                                 lblColorCol.BackColor.R,
                                 lblColorCol.BackColor.G,
                                 lblColorCol.BackColor.B);
      }
      set
      {
        lblColorCol.BackColor = value;
        nudOpacityCol.Value = (Byte)((100.0 * value.A) / 255);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Color CValue;
      Byte OValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      CValue = Color.FromArgb(INIT_COLORROW_A, INIT_COLORROW_R, 
                              INIT_COLORROW_G, INIT_COLORROW_B);
      OValue = INIT_OPACITYROW;
      Result &= initdata.ReadColorOpacity(NAME_COLORROW, out CValue, CValue,
                                          out OValue, OValue);
      lblColorRow.BackColor = CValue;
      nudOpacityRow.Value = OValue;
      //
      CValue = Color.FromArgb(INIT_COLORCOL_A, INIT_COLORCOL_R,
                              INIT_COLORCOL_G, INIT_COLORCOL_B);
      OValue = INIT_OPACITYCOL;
      Result &= initdata.ReadColorOpacity(NAME_COLORCOL, out CValue, CValue,
                                          out OValue, OValue);
      lblColorCol.BackColor = CValue;
      nudOpacityCol.Value = OValue;
      //
      Result &= initdata.CloseSection();
      //
      if (FOnColorRowColChanged is DOnColorRowColChanged)
      {
        FOnColorRowColChanged(ColorRow, ColorCol);
      }
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteColorOpacity(NAME_COLORROW,
                                           lblColorRow.BackColor,
                                           (Byte)nudOpacityRow.Value);
      Result &= initdata.WriteColorOpacity(NAME_COLORCOL,
                                           lblColorCol.BackColor,
                                           (Byte)nudOpacityCol.Value);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void pnlTitle_SizeChanged(object sender, EventArgs e)
    {
      //Int32 WH = pnlTitle.ClientRectangle.Width / 2;
      //lblTitleCol.Width = WH;
    }

    private void btnColorRow_Click(object sender, EventArgs e)
    {
      if (!(FColorDialog is ColorDialog))
      {
        FColorDialog = new ColorDialog();
      }
      Color C = lblColorRow.BackColor;
      FColorDialog.Color = Color.FromArgb(C.A, C.R, C.G, C.B);
      if (DialogResult.OK == FColorDialog.ShowDialog())
      {
        C = FColorDialog.Color;
        lblColorRow.BackColor = Color.FromArgb(C.A, C.R, C.G, C.B);
        if (FOnColorRowColChanged is DOnColorRowColChanged)
        {
          FOnColorRowColChanged(ColorRow, ColorCol);
        }
      }
    }

    private void btnColorCol_Click(object sender, EventArgs e)
    {
      if (!(FColorDialog is ColorDialog))
      {
        FColorDialog = new ColorDialog();
      }
      Color C = lblColorCol.BackColor;
      FColorDialog.Color = Color.FromArgb(0xFF, C.R, C.G, C.B);
      if (DialogResult.OK == FColorDialog.ShowDialog())
      {
        C = FColorDialog.Color;
        lblColorCol.BackColor = Color.FromArgb(0xFF, C.R, C.G, C.B);
        if (FOnColorRowColChanged is DOnColorRowColChanged)
        {
          FOnColorRowColChanged(ColorRow, ColorCol);
        }
      }
    }

    private void nudOpacityRow_ValueChanged(object sender, EventArgs e)
    {
      if (FOnColorRowColChanged is DOnColorRowColChanged)
      {
        FOnColorRowColChanged(ColorRow, ColorCol);
      }
    }

    private void nudOpacityCol_ValueChanged(object sender, EventArgs e)
    {
      if (FOnColorRowColChanged is DOnColorRowColChanged)
      {
        FOnColorRowColChanged(ColorRow, ColorCol);
      }
    }

  }
}
