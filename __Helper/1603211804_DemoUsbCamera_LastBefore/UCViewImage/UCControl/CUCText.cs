﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnTextChanged(String text);
  //
  public partial class CUCText : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String HEADER_LIBRARY = "UCText";
    //private const String NAME_TITLE = "Title";
    //private const String INIT_TITLE = "<title>";
    private const String NAME_VALUE = "Value";
    private const String INIT_VALUE = "<value>";
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnTextChanged FOnTextChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCText()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public String Title
    {
      get { return lblTitle.Text; }
      set { lblTitle.Text = value; }
    }

    public String Value
    {
      get { return tbxText.Text; }
      set { tbxText.Text = value; }
    }

    public void SetOnTextChanged(DOnTextChanged value)
    {
      FOnTextChanged = value;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      String SValue;
      //
      Result &= initdata.OpenSection(HEADER_LIBRARY + header);
      //
      //SValue = INIT_TITLE;
      //Result &= initdata.ReadString(NAME_TITLE, out SValue, SValue);
      //Title = SValue;
      ////
      SValue = INIT_VALUE;
      Result &= initdata.ReadString(NAME_VALUE, out SValue, SValue);
      Value = SValue;
      //
      Result &= initdata.CloseSection();
      //
      tbxText_TextChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(HEADER_LIBRARY + header);
      //
      //Result &= initdata.WriteString(NAME_TITLE, Title);
      Result &= initdata.WriteString(NAME_VALUE, Value);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void tbxText_TextChanged(object sender, EventArgs e)
    {
      if (FOnTextChanged is DOnTextChanged)
      {
        FOnTextChanged(Value);
      }
    }
 

  }
}
