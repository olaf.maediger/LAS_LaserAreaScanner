﻿namespace UCControl
{
  partial class CUCSizePercent
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblUnitY = new System.Windows.Forms.Label();
      this.nudSizeY = new System.Windows.Forms.NumericUpDown();
      this.lblNameY = new System.Windows.Forms.Label();
      this.lblUnitX = new System.Windows.Forms.Label();
      this.nudSizeX = new System.Windows.Forms.NumericUpDown();
      this.lblNameX = new System.Windows.Forms.Label();
      this.lblTitle = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudSizeY)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSizeX)).BeginInit();
      this.SuspendLayout();
      // 
      // lblUnitY
      // 
      this.lblUnitY.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitY.Location = new System.Drawing.Point(196, 14);
      this.lblUnitY.Name = "lblUnitY";
      this.lblUnitY.Size = new System.Drawing.Size(18, 17);
      this.lblUnitY.TabIndex = 17;
      this.lblUnitY.Text = "%";
      this.lblUnitY.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudSizeY
      // 
      this.nudSizeY.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudSizeY.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudSizeY.Location = new System.Drawing.Point(147, 14);
      this.nudSizeY.Name = "nudSizeY";
      this.nudSizeY.Size = new System.Drawing.Size(49, 16);
      this.nudSizeY.TabIndex = 16;
      this.nudSizeY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSizeY.ValueChanged += new System.EventHandler(this.nudY_ValueChanged);
      // 
      // lblNameY
      // 
      this.lblNameY.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblNameY.Location = new System.Drawing.Point(107, 14);
      this.lblNameY.Name = "lblNameY";
      this.lblNameY.Size = new System.Drawing.Size(40, 17);
      this.lblNameY.TabIndex = 15;
      this.lblNameY.Text = "SizeY";
      this.lblNameY.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lblUnitX
      // 
      this.lblUnitX.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitX.Location = new System.Drawing.Point(89, 14);
      this.lblUnitX.Name = "lblUnitX";
      this.lblUnitX.Size = new System.Drawing.Size(18, 17);
      this.lblUnitX.TabIndex = 14;
      this.lblUnitX.Text = "%";
      this.lblUnitX.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudSizeX
      // 
      this.nudSizeX.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudSizeX.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudSizeX.Location = new System.Drawing.Point(40, 14);
      this.nudSizeX.Name = "nudSizeX";
      this.nudSizeX.Size = new System.Drawing.Size(49, 16);
      this.nudSizeX.TabIndex = 13;
      this.nudSizeX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSizeX.ValueChanged += new System.EventHandler(this.nudX_ValueChanged);
      // 
      // lblNameX
      // 
      this.lblNameX.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblNameX.Location = new System.Drawing.Point(0, 14);
      this.lblNameX.Name = "lblNameX";
      this.lblNameX.Size = new System.Drawing.Size(40, 17);
      this.lblNameX.TabIndex = 12;
      this.lblNameX.Text = "SizeX";
      this.lblNameX.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lblTitle
      // 
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(214, 14);
      this.lblTitle.TabIndex = 11;
      this.lblTitle.Text = "SizePercent";
      // 
      // CUCSizePercent
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.lblUnitY);
      this.Controls.Add(this.nudSizeY);
      this.Controls.Add(this.lblNameY);
      this.Controls.Add(this.lblUnitX);
      this.Controls.Add(this.nudSizeX);
      this.Controls.Add(this.lblNameX);
      this.Controls.Add(this.lblTitle);
      this.Name = "CUCSizePercent";
      this.Size = new System.Drawing.Size(214, 31);
      ((System.ComponentModel.ISupportInitialize)(this.nudSizeY)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSizeX)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblUnitY;
    private System.Windows.Forms.NumericUpDown nudSizeY;
    private System.Windows.Forms.Label lblNameY;
    private System.Windows.Forms.Label lblUnitX;
    private System.Windows.Forms.NumericUpDown nudSizeX;
    private System.Windows.Forms.Label lblNameX;
    private System.Windows.Forms.Label lblTitle;
  }
}
