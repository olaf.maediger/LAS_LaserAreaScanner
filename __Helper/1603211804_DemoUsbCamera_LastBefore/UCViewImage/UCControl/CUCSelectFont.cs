﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnFontChanged(Font font);
  //
  public partial class CUCSelectFont : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String HEADER_LIBRARY = "UCSelectFont";
    private const String NAME_FONT = "Font";
    private const String NAME_FONTNAME = "FontName";
    private const String INIT_FONTNAME = "Arial";
    private const String NAME_FONTSIZE = "FontSize";
    private const Int32 INIT_FONTSIZE = 10;
    private const String NAME_FONTSTYLE = "FontStyle";
    private const FontStyle INIT_FONTSTYLE = FontStyle.Regular;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private Font FFont;
    private float FFactorFontSize;
    private DOnFontChanged FOnFontChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //   
    public CUCSelectFont()
    {
      InitializeComponent();
      //
      SetFont(new Font(INIT_FONTNAME, INIT_FONTSIZE, INIT_FONTSTYLE));
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public Font GetFont()
    {
      return FFont;
    }
    public void SetFont(Font font)
    {
      if (font != FFont)
      {
        FFont = font;
        lblFontText.Font = FFont;
      }
      if (FOnFontChanged is DOnFontChanged)
      {
        FOnFontChanged(FFont);
      }
    }

    public String FontText
    {
      get { return lblFontText.Text; }
      set { lblFontText.Text = value; }
    }

    public Color FontColor
    {
      get { return lblFontText.ForeColor; }
      set { lblFontText.ForeColor = value; }
    }

    public float FactorFontSize
    {
      get { return FFactorFontSize; }
      set { FFactorFontSize = value; }
    }

    public void SetOnFontChanged(DOnFontChanged value)
    {
      FOnFontChanged = value;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Font FValue;
      //
      Result &= initdata.OpenSection(HEADER_LIBRARY + header);
      //
      FValue = new Font(INIT_FONTNAME, INIT_FONTSIZE, INIT_FONTSTYLE);
      Result &= initdata.ReadFont(NAME_FONT, out FValue, FValue);
      SetFont(FValue);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(HEADER_LIBRARY + header);
      //
      Result &= initdata.WriteFont(NAME_FONT, GetFont());
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void btnSelectFont_Click(object sender, EventArgs e)
    {
      FDialogFont.Font = FFont;
      if (DialogResult.OK == FDialogFont.ShowDialog())
      {
        SetFont(FDialogFont.Font);
      }
    }

  }
}
