﻿namespace UCControl
{
  partial class CUCColorOpacityRowCol
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlTitle = new System.Windows.Forms.Panel();
      this.lblTitleCol = new System.Windows.Forms.Label();
      this.lblTitleRow = new System.Windows.Forms.Label();
      this.panel4 = new System.Windows.Forms.Panel();
      this.pnlRGBCol = new System.Windows.Forms.Panel();
      this.lblColorCol = new System.Windows.Forms.Label();
      this.btnColorCol = new System.Windows.Forms.Button();
      this.pnlRGBRow = new System.Windows.Forms.Panel();
      this.lblColorRow = new System.Windows.Forms.Label();
      this.btnColorRow = new System.Windows.Forms.Button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.pnlOpacityCol = new System.Windows.Forms.Panel();
      this.nudOpacityCol = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.pnlOpacityRow = new System.Windows.Forms.Panel();
      this.nudOpacityRow = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.pnlTitle.SuspendLayout();
      this.panel4.SuspendLayout();
      this.pnlRGBCol.SuspendLayout();
      this.pnlRGBRow.SuspendLayout();
      this.panel1.SuspendLayout();
      this.pnlOpacityCol.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudOpacityCol)).BeginInit();
      this.pnlOpacityRow.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudOpacityRow)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlTitle
      // 
      this.pnlTitle.Controls.Add(this.lblTitleCol);
      this.pnlTitle.Controls.Add(this.lblTitleRow);
      this.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTitle.Location = new System.Drawing.Point(0, 0);
      this.pnlTitle.Name = "pnlTitle";
      this.pnlTitle.Size = new System.Drawing.Size(214, 16);
      this.pnlTitle.TabIndex = 20;
      this.pnlTitle.SizeChanged += new System.EventHandler(this.pnlTitle_SizeChanged);
      // 
      // lblTitleCol
      // 
      this.lblTitleCol.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblTitleCol.Location = new System.Drawing.Point(106, 0);
      this.lblTitleCol.Name = "lblTitleCol";
      this.lblTitleCol.Size = new System.Drawing.Size(108, 16);
      this.lblTitleCol.TabIndex = 18;
      this.lblTitleCol.Text = "Color - Col";
      this.lblTitleCol.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lblTitleRow
      // 
      this.lblTitleRow.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblTitleRow.Location = new System.Drawing.Point(0, 0);
      this.lblTitleRow.Name = "lblTitleRow";
      this.lblTitleRow.Size = new System.Drawing.Size(106, 16);
      this.lblTitleRow.TabIndex = 17;
      this.lblTitleRow.Text = "Color - Row";
      this.lblTitleRow.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // panel4
      // 
      this.panel4.Controls.Add(this.pnlRGBCol);
      this.panel4.Controls.Add(this.pnlRGBRow);
      this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel4.Location = new System.Drawing.Point(0, 16);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(214, 27);
      this.panel4.TabIndex = 25;
      // 
      // pnlRGBCol
      // 
      this.pnlRGBCol.Controls.Add(this.lblColorCol);
      this.pnlRGBCol.Controls.Add(this.btnColorCol);
      this.pnlRGBCol.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlRGBCol.Location = new System.Drawing.Point(106, 0);
      this.pnlRGBCol.Name = "pnlRGBCol";
      this.pnlRGBCol.Size = new System.Drawing.Size(108, 27);
      this.pnlRGBCol.TabIndex = 28;
      // 
      // lblColorCol
      // 
      this.lblColorCol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblColorCol.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblColorCol.Location = new System.Drawing.Point(40, 0);
      this.lblColorCol.Name = "lblColorCol";
      this.lblColorCol.Size = new System.Drawing.Size(68, 27);
      this.lblColorCol.TabIndex = 29;
      this.lblColorCol.Text = "AA BB CC";
      this.lblColorCol.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnColorCol
      // 
      this.btnColorCol.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnColorCol.Location = new System.Drawing.Point(0, 0);
      this.btnColorCol.Name = "btnColorCol";
      this.btnColorCol.Size = new System.Drawing.Size(40, 27);
      this.btnColorCol.TabIndex = 28;
      this.btnColorCol.Text = "RGB";
      this.btnColorCol.UseVisualStyleBackColor = true;
      this.btnColorCol.Click += new System.EventHandler(this.btnColorCol_Click);
      // 
      // pnlRGBRow
      // 
      this.pnlRGBRow.Controls.Add(this.lblColorRow);
      this.pnlRGBRow.Controls.Add(this.btnColorRow);
      this.pnlRGBRow.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlRGBRow.Location = new System.Drawing.Point(0, 0);
      this.pnlRGBRow.Name = "pnlRGBRow";
      this.pnlRGBRow.Size = new System.Drawing.Size(106, 27);
      this.pnlRGBRow.TabIndex = 27;
      // 
      // lblColorRow
      // 
      this.lblColorRow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblColorRow.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblColorRow.Location = new System.Drawing.Point(40, 0);
      this.lblColorRow.Name = "lblColorRow";
      this.lblColorRow.Size = new System.Drawing.Size(66, 27);
      this.lblColorRow.TabIndex = 29;
      this.lblColorRow.Text = "AA BB CC";
      this.lblColorRow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnColorRow
      // 
      this.btnColorRow.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnColorRow.Location = new System.Drawing.Point(0, 0);
      this.btnColorRow.Name = "btnColorRow";
      this.btnColorRow.Size = new System.Drawing.Size(40, 27);
      this.btnColorRow.TabIndex = 28;
      this.btnColorRow.Text = "RGB";
      this.btnColorRow.UseVisualStyleBackColor = true;
      this.btnColorRow.Click += new System.EventHandler(this.btnColorRow_Click);
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.pnlOpacityCol);
      this.panel1.Controls.Add(this.pnlOpacityRow);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 43);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(214, 19);
      this.panel1.TabIndex = 26;
      // 
      // pnlOpacityCol
      // 
      this.pnlOpacityCol.Controls.Add(this.nudOpacityCol);
      this.pnlOpacityCol.Controls.Add(this.label4);
      this.pnlOpacityCol.Controls.Add(this.label3);
      this.pnlOpacityCol.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlOpacityCol.Location = new System.Drawing.Point(106, 0);
      this.pnlOpacityCol.Name = "pnlOpacityCol";
      this.pnlOpacityCol.Size = new System.Drawing.Size(108, 19);
      this.pnlOpacityCol.TabIndex = 27;
      // 
      // nudOpacityCol
      // 
      this.nudOpacityCol.Dock = System.Windows.Forms.DockStyle.Fill;
      this.nudOpacityCol.Location = new System.Drawing.Point(48, 0);
      this.nudOpacityCol.Name = "nudOpacityCol";
      this.nudOpacityCol.Size = new System.Drawing.Size(44, 20);
      this.nudOpacityCol.TabIndex = 28;
      this.nudOpacityCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudOpacityCol.ValueChanged += new System.EventHandler(this.nudOpacityCol_ValueChanged);
      // 
      // label4
      // 
      this.label4.Dock = System.Windows.Forms.DockStyle.Right;
      this.label4.Location = new System.Drawing.Point(92, 0);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(16, 19);
      this.label4.TabIndex = 26;
      this.label4.Text = "%";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label3
      // 
      this.label3.Dock = System.Windows.Forms.DockStyle.Left;
      this.label3.Location = new System.Drawing.Point(0, 0);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(48, 19);
      this.label3.TabIndex = 25;
      this.label3.Text = "Opacity";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnlOpacityRow
      // 
      this.pnlOpacityRow.Controls.Add(this.nudOpacityRow);
      this.pnlOpacityRow.Controls.Add(this.label2);
      this.pnlOpacityRow.Controls.Add(this.label1);
      this.pnlOpacityRow.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlOpacityRow.Location = new System.Drawing.Point(0, 0);
      this.pnlOpacityRow.Name = "pnlOpacityRow";
      this.pnlOpacityRow.Size = new System.Drawing.Size(106, 19);
      this.pnlOpacityRow.TabIndex = 26;
      // 
      // nudOpacityRow
      // 
      this.nudOpacityRow.Dock = System.Windows.Forms.DockStyle.Fill;
      this.nudOpacityRow.Location = new System.Drawing.Point(48, 0);
      this.nudOpacityRow.Name = "nudOpacityRow";
      this.nudOpacityRow.Size = new System.Drawing.Size(42, 20);
      this.nudOpacityRow.TabIndex = 27;
      this.nudOpacityRow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudOpacityRow.ValueChanged += new System.EventHandler(this.nudOpacityRow_ValueChanged);
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Right;
      this.label2.Location = new System.Drawing.Point(90, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(16, 19);
      this.label2.TabIndex = 25;
      this.label2.Text = "%";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Left;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(48, 19);
      this.label1.TabIndex = 24;
      this.label1.Text = "Opacity";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCColorOpacityRowCol
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.panel4);
      this.Controls.Add(this.pnlTitle);
      this.Name = "CUCColorOpacityRowCol";
      this.Size = new System.Drawing.Size(214, 61);
      this.pnlTitle.ResumeLayout(false);
      this.panel4.ResumeLayout(false);
      this.pnlRGBCol.ResumeLayout(false);
      this.pnlRGBRow.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.pnlOpacityCol.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudOpacityCol)).EndInit();
      this.pnlOpacityRow.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudOpacityRow)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlTitle;
    private System.Windows.Forms.Label lblTitleRow;
    private System.Windows.Forms.Label lblTitleCol;
    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.Panel pnlRGBCol;
    private System.Windows.Forms.Label lblColorCol;
    private System.Windows.Forms.Button btnColorCol;
    private System.Windows.Forms.Panel pnlRGBRow;
    private System.Windows.Forms.Label lblColorRow;
    private System.Windows.Forms.Button btnColorRow;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel pnlOpacityCol;
    private System.Windows.Forms.Panel pnlOpacityRow;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudOpacityRow;
    private System.Windows.Forms.NumericUpDown nudOpacityCol;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
  }
}
