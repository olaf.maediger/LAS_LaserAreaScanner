﻿namespace UCControl
{
  partial class CUCUnitScale
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.rbtMM = new System.Windows.Forms.RadioButton();
      this.rbtUM = new System.Windows.Forms.RadioButton();
      this.rbtNM = new System.Windows.Forms.RadioButton();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Left;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(65, 26);
      this.label1.TabIndex = 10;
      this.label1.Text = "UnitScale";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // rbtMM
      // 
      this.rbtMM.Dock = System.Windows.Forms.DockStyle.Left;
      this.rbtMM.Location = new System.Drawing.Point(65, 0);
      this.rbtMM.Name = "rbtMM";
      this.rbtMM.Size = new System.Drawing.Size(42, 26);
      this.rbtMM.TabIndex = 16;
      this.rbtMM.TabStop = true;
      this.rbtMM.Text = "mm";
      this.rbtMM.UseVisualStyleBackColor = true;
      this.rbtMM.CheckedChanged += new System.EventHandler(this.UnitScale_CheckedChanged);
      // 
      // rbtUM
      // 
      this.rbtUM.Dock = System.Windows.Forms.DockStyle.Left;
      this.rbtUM.Location = new System.Drawing.Point(107, 0);
      this.rbtUM.Name = "rbtUM";
      this.rbtUM.Size = new System.Drawing.Size(42, 26);
      this.rbtUM.TabIndex = 17;
      this.rbtUM.TabStop = true;
      this.rbtUM.Text = "um";
      this.rbtUM.UseVisualStyleBackColor = true;
      this.rbtUM.CheckedChanged += new System.EventHandler(this.UnitScale_CheckedChanged);
      // 
      // rbtNM
      // 
      this.rbtNM.Dock = System.Windows.Forms.DockStyle.Left;
      this.rbtNM.Location = new System.Drawing.Point(149, 0);
      this.rbtNM.Name = "rbtNM";
      this.rbtNM.Size = new System.Drawing.Size(42, 26);
      this.rbtNM.TabIndex = 18;
      this.rbtNM.TabStop = true;
      this.rbtNM.Text = "nm";
      this.rbtNM.UseVisualStyleBackColor = true;
      this.rbtNM.CheckedChanged += new System.EventHandler(this.UnitScale_CheckedChanged);
      // 
      // CUCUnitScale
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.rbtNM);
      this.Controls.Add(this.rbtUM);
      this.Controls.Add(this.rbtMM);
      this.Controls.Add(this.label1);
      this.Name = "CUCUnitScale";
      this.Size = new System.Drawing.Size(195, 26);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.RadioButton rbtMM;
    private System.Windows.Forms.RadioButton rbtUM;
    private System.Windows.Forms.RadioButton rbtNM;
  }
}
