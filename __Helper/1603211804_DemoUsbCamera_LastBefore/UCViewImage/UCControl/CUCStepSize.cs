﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using NLRealPixel;
//
namespace UCControl
{
  public partial class CUCStepSize : UserControl
  {
    public CUCStepSize()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public EUnitScale UnitScale
    {
      set
      {
        String SUnit = CRealPixel.UnitScaleText(value);
        lblUnitDX.Text = SUnit;
        lblUnitDY.Text = SUnit;
      }
    }

    public Double StepSizeX
    {
      set { lblStepSizeX.Text = String.Format("{0:0.000}", value); }
    }
    public Double StepSizeY
    {
      set { lblStepSizeY.Text = String.Format("{0:0.000}", value); }
    }
    // Load -> On...
  }
}
