﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnOriginChanged(Double originx, Double originy);
  //
  public partial class CUCOrigin : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCOrigin";
    private const String NAME_ORIGINX = "OriginX";
    private const Double INIT_ORIGINX = 0.0;
    private const String NAME_ORIGINY = "OriginY";
    private const Double INIT_ORIGINY = 0.0;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    //????private DOnShowPropertyChanged FOnShowTitleChanged;
    private DOnOriginChanged FOnOriginChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCOrigin()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    //public void SetOnShowTitleChanged(DOnShowTitleChanged value)
    //{
    //  FOnShowTitleChanged = value;
    //}

    public void SetOnOriginChanged(DOnOriginChanged value)
    {
      FOnOriginChanged = value;
    }

    public EUnitScale UnitScale
    {
      set
      {
        String SUnit = CRealPixel.UnitScaleText(value);
        lblUnitOriginX0.Text = SUnit;
        lblUnitOriginY0.Text = SUnit;
      }
    }

    public Double X
    {
      get { return (Double)nudX.Value; }
      set { nudX.Value = (Decimal)value; }
    }

    public Double Y
    {
      get { return (Double)nudY.Value; }
      set { nudY.Value = (Decimal)value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Double DValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      DValue = INIT_ORIGINX;
      Result &= initdata.ReadDouble(NAME_ORIGINX, out DValue, DValue);
      X = DValue;
      //
      DValue = INIT_ORIGINY;
      Result &= initdata.ReadDouble(NAME_ORIGINY, out DValue, DValue);
      Y = DValue;
      //
      Result &= initdata.CloseSection();
      //
      nudX_ValueChanged(this, null);
      nudY_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteDouble(NAME_ORIGINX, X);
      Result &= initdata.WriteDouble(NAME_ORIGINY, Y);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void nudX_ValueChanged(object sender, EventArgs e)
    {
      if (FOnOriginChanged is DOnOriginChanged)
      {
        FOnOriginChanged(X, Y);
      }
    }

    private void nudY_ValueChanged(object sender, EventArgs e)
    {
      if (FOnOriginChanged is DOnOriginChanged)
      {
        FOnOriginChanged(X, Y);
      }
    }

  }
}
