﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
//
namespace UCScrollZoom
{
  public delegate void DOnZoomChanged(Double zoomfactor);

  public partial class CUCScrollZoom : UserControl
  { //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String HEADER_LIBRARY = "UCScrollZoom";
    private const String NAME_ZOOMFACTOR = "ZoomFactor";
    private const Double INIT_ZOOMFACTOR = 1.0;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private DOnZoomChanged FOnZoomChanged;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    public CUCScrollZoom()
    {
      InitializeComponent();
      //
      scbZoom.Maximum = +99;
      scbZoom.Minimum = -99;
      scbZoom.Value = BuildZoomFactor(INIT_ZOOMFACTOR);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetOnZoomChanged(DOnZoomChanged value)
    {
      FOnZoomChanged = value;
    }

    public Double ZoomFactor
    {
      get { return BuildZoomFactor(scbZoom.Value); }
      set { scbZoom.Value = BuildZoomFactor(value); }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Double DValue;
      //
      Result &= initdata.OpenSection(HEADER_LIBRARY + header);
      //
      DValue = INIT_ZOOMFACTOR;
      Result &= initdata.ReadDouble(NAME_ZOOMFACTOR, out DValue, DValue);
      ZoomFactor = DValue;
      //
      Result &= initdata.CloseSection();
      //
      scbZoom_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(HEADER_LIBRARY + header);
      //
      Result &= initdata.WriteDouble(NAME_ZOOMFACTOR, ZoomFactor);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------------------------
    //
    private Double BuildZoomFactor(Int32 value)
    {
      Double ZF = INIT_ZOOMFACTOR;
      Int32 IValue = -value;
      if (0 < IValue)
      { // [1, 1, 100]
        ZF = 1.0 + IValue / 10.0;
      }
      else
        if (IValue < 0)
        { // [-1, -1, -100]
          ZF = 1.0 + IValue / 100.0;
        }
      // debug Console.WriteLine(String.Format("IF[{0}] -> ZF[{1}]", IValue, ZF));
      return ZF;
    }

    private Int32 BuildZoomFactor(Double value)
    {
      Int32 IF = 0;
      if (1 < value)
      { // [1, 1, 100]
        IF = (Int32)(-(10.0 * value - 10.0));
      }
      else
        if (value < 1)
        { // [-1, -1, -100]
          IF = (Int32)(-(100.0 * value - 100.0));
        }
      // debug Console.WriteLine(String.Format("ZF[{0}] -> IF[{1}]", value, IF));
      return IF;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void scbZoom_ValueChanged(object sender, EventArgs e)
    {
      if (FOnZoomChanged is DOnZoomChanged)
      {
        Double ZF = BuildZoomFactor(scbZoom.Value);
        FOnZoomChanged(ZF);
      }
    }

  }
}
