﻿namespace UCScrollZoom
{
  partial class CUCScrollZoom
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.scbZoom = new System.Windows.Forms.VScrollBar();
      this.SuspendLayout();
      // 
      // scbZoom
      // 
      this.scbZoom.Dock = System.Windows.Forms.DockStyle.Left;
      this.scbZoom.Location = new System.Drawing.Point(0, 0);
      this.scbZoom.Minimum = -100;
      this.scbZoom.Name = "scbZoom";
      this.scbZoom.Size = new System.Drawing.Size(16, 504);
      this.scbZoom.TabIndex = 7;
      this.scbZoom.ValueChanged += new System.EventHandler(this.scbZoom_ValueChanged);
      // 
      // CUCScrollZoom
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.scbZoom);
      this.Name = "CUCScrollZoom";
      this.Size = new System.Drawing.Size(16, 504);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.VScrollBar scbZoom;
  }
}
