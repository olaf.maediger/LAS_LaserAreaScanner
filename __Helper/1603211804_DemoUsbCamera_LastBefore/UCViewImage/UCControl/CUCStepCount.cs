﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
//
namespace UCControl
{
  public delegate void DOnStepCountChanged(Int32 stepcountx, Int32 stepcounty);
  //
  public partial class CUCStepCount : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCStepCount";
    private const String NAME_STEPCOUNTX = "StepcountX";
    private const Int32 INIT_STEPCOUNTX = 11;
    private const String NAME_STEPCOUNTY = "StepcountY";
    private const Int32 INIT_STEPCOUNTY = 11;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private DOnStepCountChanged FOnStepCountChanged;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    public CUCStepCount()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetOnStepCountChanged(DOnStepCountChanged value)
    {
      FOnStepCountChanged = value;
    }

    public Int32 StepCountX
    {
      get { return (Int32)nudX.Value; }
      set { nudX.Value = (Decimal)value; }
    }
    public Int32 StepCountY
    {
      get { return (Int32)nudY.Value; }
      set { nudY.Value = (Decimal)value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Int32 IValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      IValue = INIT_STEPCOUNTX;
      Result &= initdata.ReadInt32(NAME_STEPCOUNTX, out IValue, IValue);
      StepCountX = IValue;
      //
      IValue = INIT_STEPCOUNTY;
      Result &= initdata.ReadInt32(NAME_STEPCOUNTY, out IValue, IValue);
      StepCountY = IValue;
      //
      Result &= initdata.CloseSection();
      //
      nudX_ValueChanged(this, null);
      nudY_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteInt32(NAME_STEPCOUNTX, StepCountX);
      Result &= initdata.WriteInt32(NAME_STEPCOUNTY, StepCountY);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void nudX_ValueChanged(object sender, EventArgs e)
    {
      if (FOnStepCountChanged is DOnStepCountChanged)
      {
        FOnStepCountChanged((Int32)nudX.Value, (Int32)nudY.Value);
      }
    }

    private void nudY_ValueChanged(object sender, EventArgs e)
    {
      if (FOnStepCountChanged is DOnStepCountChanged)
      {
        FOnStepCountChanged((Int32)nudX.Value, (Int32)nudY.Value);
      }
    }

  }
}
