﻿namespace UCControl
{
  partial class CUCShowProperty
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxShowProperty = new System.Windows.Forms.CheckBox();
      this.SuspendLayout();
      // 
      // cbxShowProperty
      // 
      this.cbxShowProperty.BackColor = System.Drawing.SystemColors.Info;
      this.cbxShowProperty.Checked = true;
      this.cbxShowProperty.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbxShowProperty.Dock = System.Windows.Forms.DockStyle.Fill;
      this.cbxShowProperty.Location = new System.Drawing.Point(0, 0);
      this.cbxShowProperty.Name = "cbxShowProperty";
      this.cbxShowProperty.Size = new System.Drawing.Size(214, 18);
      this.cbxShowProperty.TabIndex = 2;
      this.cbxShowProperty.Text = "<show property>";
      this.cbxShowProperty.UseVisualStyleBackColor = false;
      this.cbxShowProperty.CheckedChanged += new System.EventHandler(this.cbxShowProperty_CheckedChanged);
      // 
      // CUCShowProperty
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.cbxShowProperty);
      this.Name = "CUCShowProperty";
      this.Size = new System.Drawing.Size(214, 18);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.CheckBox cbxShowProperty;
  }
}
