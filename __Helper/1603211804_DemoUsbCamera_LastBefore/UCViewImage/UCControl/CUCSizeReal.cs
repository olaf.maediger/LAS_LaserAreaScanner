﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnSizeChanged(Double sizex, Double sizey);
  //
  public partial class CUCSizeReal : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCSize";
    private const String NAME_SIZEX = "SizeX";
    private const Double INIT_SIZEX = 10.0;
    private const String NAME_SIZEY = "SizeY";
    private const Double INIT_SIZEY = 10.0;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnSizeChanged FOnSizeChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCSizeReal()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public void SetOnSizeChanged(DOnSizeChanged value)
    {
      FOnSizeChanged = value;
    }

    public EUnitScale UnitScale
    {
      set
      {
        String SUnit = CRealPixel.UnitScaleText(value);
        lblUnitSX.Text = SUnit;
        lblUnitSY.Text = SUnit;
      }
    }

    public Double SizeX
    {
      get { return (Double)nudX.Value; }
      set { nudX.Value = (Decimal)value; }
    }

    public Double SizeY
    {
      get { return (Double)nudY.Value; }
      set { nudY.Value = (Decimal)value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Double DValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      DValue = INIT_SIZEX;
      Result &= initdata.ReadDouble(NAME_SIZEX, out DValue, DValue);
      SizeX = DValue;
      //
      DValue = INIT_SIZEY;
      Result &= initdata.ReadDouble(NAME_SIZEY, out DValue, DValue);
      SizeY = DValue;
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteDouble(NAME_SIZEX, SizeX);
      Result &= initdata.WriteDouble(NAME_SIZEY, SizeY);
      //
      Result &= initdata.CloseSection();
      //
      nudX_ValueChanged(this, null);
      nudY_ValueChanged(this, null);
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void nudX_ValueChanged(object sender, EventArgs e)
    {
      if (FOnSizeChanged is DOnSizeChanged)
      {
        FOnSizeChanged(SizeX, SizeY);
      }
    }

    private void nudY_ValueChanged(object sender, EventArgs e)
    {
      if (FOnSizeChanged is DOnSizeChanged)
      {
        FOnSizeChanged(SizeX, SizeY);
      }
    }


  }
}
