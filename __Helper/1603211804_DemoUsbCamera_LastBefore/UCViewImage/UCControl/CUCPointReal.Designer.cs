﻿namespace UCControl
{
  partial class CUCPointReal
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblUnitPointY = new System.Windows.Forms.Label();
      this.nudY = new System.Windows.Forms.NumericUpDown();
      this.label5 = new System.Windows.Forms.Label();
      this.lblUnitPointX = new System.Windows.Forms.Label();
      this.nudX = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.lblTitle = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudY)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudX)).BeginInit();
      this.SuspendLayout();
      // 
      // lblUnitPointY
      // 
      this.lblUnitPointY.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitPointY.Location = new System.Drawing.Point(188, 14);
      this.lblUnitPointY.Name = "lblUnitPointY";
      this.lblUnitPointY.Size = new System.Drawing.Size(26, 16);
      this.lblUnitPointY.TabIndex = 16;
      this.lblUnitPointY.Text = "um";
      this.lblUnitPointY.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudY
      // 
      this.nudY.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudY.DecimalPlaces = 3;
      this.nudY.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudY.Location = new System.Drawing.Point(134, 14);
      this.nudY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudY.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
      this.nudY.Name = "nudY";
      this.nudY.Size = new System.Drawing.Size(54, 16);
      this.nudY.TabIndex = 15;
      this.nudY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Left;
      this.label5.Location = new System.Drawing.Point(107, 14);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(27, 16);
      this.label5.TabIndex = 14;
      this.label5.Text = "Y";
      this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lblUnitPointX
      // 
      this.lblUnitPointX.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblUnitPointX.Location = new System.Drawing.Point(81, 14);
      this.lblUnitPointX.Name = "lblUnitPointX";
      this.lblUnitPointX.Size = new System.Drawing.Size(26, 16);
      this.lblUnitPointX.TabIndex = 13;
      this.lblUnitPointX.Text = "um";
      this.lblUnitPointX.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // nudX
      // 
      this.nudX.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudX.DecimalPlaces = 3;
      this.nudX.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudX.Location = new System.Drawing.Point(27, 14);
      this.nudX.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
      this.nudX.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
      this.nudX.Name = "nudX";
      this.nudX.Size = new System.Drawing.Size(54, 16);
      this.nudX.TabIndex = 12;
      this.nudX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Left;
      this.label2.Location = new System.Drawing.Point(0, 14);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(27, 16);
      this.label2.TabIndex = 11;
      this.label2.Text = "X";
      this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // lblTitle
      // 
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(214, 14);
      this.lblTitle.TabIndex = 10;
      this.lblTitle.Text = "<title>";
      // 
      // CUCPointReal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.lblUnitPointY);
      this.Controls.Add(this.nudY);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.lblUnitPointX);
      this.Controls.Add(this.nudX);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.lblTitle);
      this.Name = "CUCPointReal";
      this.Size = new System.Drawing.Size(214, 30);
      ((System.ComponentModel.ISupportInitialize)(this.nudY)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudX)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblUnitPointY;
    private System.Windows.Forms.NumericUpDown nudY;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label lblUnitPointX;
    private System.Windows.Forms.NumericUpDown nudX;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lblTitle;
  }
}
