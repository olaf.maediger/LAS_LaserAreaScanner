﻿namespace UCControl
{
  partial class CUCText
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblTitle = new System.Windows.Forms.Label();
      this.tbxText = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // lblTitle
      // 
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(214, 14);
      this.lblTitle.TabIndex = 22;
      this.lblTitle.Text = "<title>";
      // 
      // tbxText
      // 
      this.tbxText.Dock = System.Windows.Forms.DockStyle.Top;
      this.tbxText.Location = new System.Drawing.Point(0, 14);
      this.tbxText.Name = "tbxText";
      this.tbxText.Size = new System.Drawing.Size(214, 20);
      this.tbxText.TabIndex = 23;
      this.tbxText.Text = "<text>";
      this.tbxText.TextChanged += new System.EventHandler(this.tbxText_TextChanged);
      // 
      // CUCText
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.tbxText);
      this.Controls.Add(this.lblTitle);
      this.Name = "CUCText";
      this.Size = new System.Drawing.Size(214, 34);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.TextBox tbxText;

  }
}
