﻿namespace UCControl
{
  partial class CUCMinimum
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.pnlLeft = new System.Windows.Forms.Panel();
      this.nudXMinimum = new System.Windows.Forms.NumericUpDown();
      this.lblUnitOriginX0 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.pnlRight = new System.Windows.Forms.Panel();
      this.nudYMinimum = new System.Windows.Forms.NumericUpDown();
      this.lblUnitOriginY0 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.pnlLeft.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudXMinimum)).BeginInit();
      this.pnlRight.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudYMinimum)).BeginInit();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Top;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(277, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "Minimum";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pnlLeft
      // 
      this.pnlLeft.Controls.Add(this.nudXMinimum);
      this.pnlLeft.Controls.Add(this.lblUnitOriginX0);
      this.pnlLeft.Controls.Add(this.label2);
      this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlLeft.Location = new System.Drawing.Point(0, 13);
      this.pnlLeft.Name = "pnlLeft";
      this.pnlLeft.Size = new System.Drawing.Size(133, 17);
      this.pnlLeft.TabIndex = 9;
      // 
      // nudXMinimum
      // 
      this.nudXMinimum.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudXMinimum.Dock = System.Windows.Forms.DockStyle.Fill;
      this.nudXMinimum.Location = new System.Drawing.Point(64, 0);
      this.nudXMinimum.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
      this.nudXMinimum.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
      this.nudXMinimum.Name = "nudXMinimum";
      this.nudXMinimum.Size = new System.Drawing.Size(43, 16);
      this.nudXMinimum.TabIndex = 19;
      this.nudXMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // lblUnitOriginX0
      // 
      this.lblUnitOriginX0.Dock = System.Windows.Forms.DockStyle.Right;
      this.lblUnitOriginX0.Location = new System.Drawing.Point(107, 0);
      this.lblUnitOriginX0.Name = "lblUnitOriginX0";
      this.lblUnitOriginX0.Size = new System.Drawing.Size(26, 17);
      this.lblUnitOriginX0.TabIndex = 18;
      this.lblUnitOriginX0.Text = "um";
      this.lblUnitOriginX0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Left;
      this.label2.Location = new System.Drawing.Point(0, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(64, 17);
      this.label2.TabIndex = 16;
      this.label2.Text = "XMinimum";
      this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // pnlRight
      // 
      this.pnlRight.Controls.Add(this.nudYMinimum);
      this.pnlRight.Controls.Add(this.lblUnitOriginY0);
      this.pnlRight.Controls.Add(this.label5);
      this.pnlRight.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlRight.Location = new System.Drawing.Point(133, 13);
      this.pnlRight.Name = "pnlRight";
      this.pnlRight.Size = new System.Drawing.Size(144, 17);
      this.pnlRight.TabIndex = 10;
      // 
      // nudYMinimum
      // 
      this.nudYMinimum.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.nudYMinimum.Dock = System.Windows.Forms.DockStyle.Left;
      this.nudYMinimum.Location = new System.Drawing.Point(64, 0);
      this.nudYMinimum.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudYMinimum.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
      this.nudYMinimum.Name = "nudYMinimum";
      this.nudYMinimum.Size = new System.Drawing.Size(54, 16);
      this.nudYMinimum.TabIndex = 21;
      this.nudYMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // lblUnitOriginY0
      // 
      this.lblUnitOriginY0.Dock = System.Windows.Forms.DockStyle.Right;
      this.lblUnitOriginY0.Location = new System.Drawing.Point(118, 0);
      this.lblUnitOriginY0.Name = "lblUnitOriginY0";
      this.lblUnitOriginY0.Size = new System.Drawing.Size(26, 17);
      this.lblUnitOriginY0.TabIndex = 20;
      this.lblUnitOriginY0.Text = "um";
      this.lblUnitOriginY0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Left;
      this.label5.Location = new System.Drawing.Point(0, 0);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(64, 17);
      this.label5.TabIndex = 18;
      this.label5.Text = "YMinimum";
      this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // CUCMinimum
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.pnlRight);
      this.Controls.Add(this.pnlLeft);
      this.Controls.Add(this.label1);
      this.Name = "CUCMinimum";
      this.Size = new System.Drawing.Size(277, 30);
      this.pnlLeft.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudXMinimum)).EndInit();
      this.pnlRight.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudYMinimum)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Panel pnlLeft;
    private System.Windows.Forms.NumericUpDown nudXMinimum;
    private System.Windows.Forms.Label lblUnitOriginX0;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Panel pnlRight;
    private System.Windows.Forms.NumericUpDown nudYMinimum;
    private System.Windows.Forms.Label lblUnitOriginY0;
    private System.Windows.Forms.Label label5;
  }
}
