﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnLineWidthRowColChanged(Int32 linewidthrow, Int32 linewidthcol);

  public partial class CUCLineWidthRowCol : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCLineWidthRowCol";
    private const String NAME_LINEWIDTHROW = "LineWidthRow";
    private const Int32 INIT_LINEWIDTHROW = 1;
    private const String NAME_LINEWIDTHCOL = "LineWidthCol";
    private const Int32 INIT_LINEWIDTHCOL = 1;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private DOnLineWidthRowColChanged FOnLineWidthRowColChanged;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    public CUCLineWidthRowCol()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetOnLineWidthRowColChanged(DOnLineWidthRowColChanged value)
    {
      FOnLineWidthRowColChanged = value;
    }

    public Int32 LineWidthRow
    {
      get { return (Int32)nudLineWidthRow.Value; }
      set { nudLineWidthRow.Value = (Decimal)value; }
    }

    public Int32 LineWidthCol
    {
      get { return (Int32)nudLineWidthCol.Value; }
      set { nudLineWidthCol.Value = (Decimal)value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Int32 IValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      IValue = INIT_LINEWIDTHROW;
      Result &= initdata.ReadInt32(NAME_LINEWIDTHROW, out IValue, IValue);
      LineWidthRow = IValue;
      //
      IValue = INIT_LINEWIDTHCOL;
      Result &= initdata.ReadInt32(NAME_LINEWIDTHCOL, out IValue, IValue);
      LineWidthCol = IValue;
      //
      Result &= initdata.CloseSection();
      //
      //
      //.......................OnColorChanged.......
      //

      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteInt32(NAME_LINEWIDTHROW, LineWidthRow);
      Result &= initdata.WriteInt32(NAME_LINEWIDTHCOL, LineWidthCol);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    private void nudLineWidthRow_ValueChanged(object sender, EventArgs e)
    {
      if (FOnLineWidthRowColChanged is DOnLineWidthRowColChanged)
      {
        FOnLineWidthRowColChanged(LineWidthRow, LineWidthCol);
      }
    }

    private void nudLineWidthCol_ValueChanged(object sender, EventArgs e)
    {
      if (FOnLineWidthRowColChanged is DOnLineWidthRowColChanged)
      {
        FOnLineWidthRowColChanged(LineWidthRow, LineWidthCol);
      }
    }




  }
}
