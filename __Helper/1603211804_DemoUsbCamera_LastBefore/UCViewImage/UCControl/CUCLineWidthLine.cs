﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
using NLGraphicElement;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  // delegate...
  //  
  public partial class CUCLineWidthLine : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String HEADER_LIBRARY = "UCLineWidthLine";
    private const String NAME_LINEWIDTH = "LineWidth";
    private const Int32 INIT_LINEWIDTH = 1; // [pxl]
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnLineWidthChanged FOnLineWidthChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCLineWidthLine()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public String Title
    {
      get { return lblTitle.Text; }
      set { lblTitle.Text = value; }
    }

    public Int32 LineWidth
    {
      get { return (Int32)nudLineWidth.Value; }
      set { nudLineWidth.Value = (Decimal)value; }
    }

    public void SetOnLineWidthChanged(DOnLineWidthChanged value)
    {
      FOnLineWidthChanged = value;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Int32 IValue;
      //
      Result &= initdata.OpenSection(HEADER_LIBRARY + header);
      //
      IValue = INIT_LINEWIDTH;
      Result &= initdata.ReadInt32(NAME_LINEWIDTH, out IValue, IValue);
      LineWidth = IValue;
      //
      Result &= initdata.CloseSection();
      //
      // NC btnColorRGB_Click(this, null);
      nudLineWidthLine_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(HEADER_LIBRARY + header);
      //
      Result &= initdata.WriteInt32(NAME_LINEWIDTH, LineWidth);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void lblShow_Paint(object sender, PaintEventArgs e)
    {
      Graphics G = e.Graphics;
      Int32 TH = lblShow.Height / 2;
      Int32 W = lblShow.Width;
      Pen P = new Pen(Color.Black, (Int32)nudLineWidth.Value);
      G.DrawLine(P, 0, TH, W, TH);
    }

    private void CUCLineWidthShow_SizeChanged(object sender, EventArgs e)
    {
      nudLineWidth.Top = 5;
    }

    private void nudLineWidthLine_ValueChanged(object sender, EventArgs e)
    {
      if (FOnLineWidthChanged is DOnLineWidthChanged)
      {
        FOnLineWidthChanged((Int32)nudLineWidth.Value);
      }
      lblShow.Invalidate();
    }

  }
}
