﻿namespace UCControl
{
  partial class CUCRowCol
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label19 = new System.Windows.Forms.Label();
      this.btnColorCol = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.btnColorRow = new System.Windows.Forms.Button();
      this.label11 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // label19
      // 
      this.label19.Dock = System.Windows.Forms.DockStyle.Top;
      this.label19.Location = new System.Drawing.Point(0, 0);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(213, 14);
      this.label19.TabIndex = 8;
      this.label19.Text = "Color";
      // 
      // btnColorCol
      // 
      this.btnColorCol.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnColorCol.Location = new System.Drawing.Point(0, 14);
      this.btnColorCol.Name = "btnColorCol";
      this.btnColorCol.Size = new System.Drawing.Size(51, 25);
      this.btnColorCol.TabIndex = 11;
      this.btnColorCol.Text = "Col";
      this.btnColorCol.UseVisualStyleBackColor = true;
      // 
      // label3
      // 
      this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.label3.Dock = System.Windows.Forms.DockStyle.Left;
      this.label3.Location = new System.Drawing.Point(51, 14);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(55, 25);
      this.label3.TabIndex = 12;
      this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // btnColorRow
      // 
      this.btnColorRow.Dock = System.Windows.Forms.DockStyle.Left;
      this.btnColorRow.Location = new System.Drawing.Point(106, 14);
      this.btnColorRow.Name = "btnColorRow";
      this.btnColorRow.Size = new System.Drawing.Size(51, 25);
      this.btnColorRow.TabIndex = 13;
      this.btnColorRow.Text = "Row";
      this.btnColorRow.UseVisualStyleBackColor = true;
      // 
      // label11
      // 
      this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.label11.Dock = System.Windows.Forms.DockStyle.Left;
      this.label11.Location = new System.Drawing.Point(157, 14);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(55, 25);
      this.label11.TabIndex = 14;
      this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // CUCColRowColor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.label11);
      this.Controls.Add(this.btnColorRow);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.btnColorCol);
      this.Controls.Add(this.label19);
      this.Name = "CUCColRowColor";
      this.Size = new System.Drawing.Size(213, 39);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.Button btnColorCol;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Button btnColorRow;
    private System.Windows.Forms.Label label11;
  }
}
