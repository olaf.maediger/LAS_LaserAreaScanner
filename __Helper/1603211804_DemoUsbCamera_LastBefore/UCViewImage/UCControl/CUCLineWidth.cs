﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCControl
{
  public delegate void DOnLineWidthChanged(Int32 linewidth);
  //
  public partial class CUCLineWidth : UserControl
  {
    public CUCLineWidth()
    {
      InitializeComponent();
    }

    private void CUCLineWidth_SizeChanged(object sender, EventArgs e)
    {
      nudLineWidth.Top = 5;
    }
  }
}
