﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using NLRealPixel;
//
namespace UCControl
{
  //
  //------------------------------------------------------------------------
  //  Section - Definition
  //------------------------------------------------------------------------
  //
  public delegate void DOnSizePercentChanged(Double sizex, Double sizey);
  //
  public partial class CUCSizePercent : UserControl
  {
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------------------------
    //
    private const String SECTION_LIBRARY = "UCSizePercent";
    private const String NAME_SIZEX = "SizeX";
    private const Double INIT_SIZEX = 80.0;  // [%]
    private const String NAME_SIZEY = "SizeY";
    private const Double INIT_SIZEY = 80.0;  // [%]
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------------------------
    //
    private DOnSizePercentChanged FOnSizePercentChanged;
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------------------------
    //
    public CUCSizePercent()
    {
      InitializeComponent();
      //
      SizeX = INIT_SIZEX;
      SizeY = INIT_SIZEY;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------------------------
    //
    public void SetOnSizePercentChanged(DOnSizePercentChanged value)
    {
      FOnSizePercentChanged = value;
    }

    public String Title
    {
      get { return lblTitle.Text; }
      set { lblTitle.Text = value; }
    }

    public String Unit
    {
      get { return lblUnitX.Text; }
      set
      {
        lblUnitX.Text = value;
        lblUnitY.Text = value;
      }
    }

    public String NameX
    {
      get { return lblNameX.Text; }
      set { lblNameX.Text = value; }
    }

    public String NameY
    {
      get { return lblNameY.Text; }
      set { lblNameY.Text = value; }
    }

    public Int32 WidthNameX
    {
      get { return lblNameX.Width; }
      set { lblNameX.Width = value; }
    }

    public Int32 WidthNameY
    {
      get { return lblNameY.Width; }
      set { lblNameY.Width = value; }
    }

    public Double SizeX
    {
      get { return (Double)nudSizeX.Value; }
      set
      {
        if ((nudSizeX.Minimum <= (Decimal)value) && ((Decimal)value <= nudSizeX.Maximum))
        {
          nudSizeX.Value = (Decimal)value;
        }
        else
          if ((Decimal)value < nudSizeX.Minimum)
          {
            nudSizeX.Value = nudSizeX.Minimum;
          }
          else
            if (nudSizeX.Maximum < (Decimal)value)
            {
              nudSizeX.Value = nudSizeX.Maximum;
            }
      }
    }

    public Double SizeY
    {
      get { return (Double)nudSizeY.Value; }
      set
      {
        if ((nudSizeY.Minimum <= (Decimal)value) && ((Decimal)value <= nudSizeY.Maximum))
        {
          nudSizeY.Value = (Decimal)value;
        }
        else
          if ((Decimal)value < nudSizeY.Minimum)
          {
            nudSizeY.Value = nudSizeY.Minimum;
          }
          else
            if (nudSizeY.Maximum < (Decimal)value)
            {
              nudSizeY.Value = nudSizeY.Maximum;
            }
      }
    }

    public Double MinimumX
    {
      get { return (Double)nudSizeX.Minimum; }
      set { nudSizeX.Minimum = (Decimal)value; }
    }
    public Double MaximumX
    {
      get { return (Double)nudSizeX.Maximum; }
      set { nudSizeX.Maximum = (Decimal)value; }
    }

    public Double MinimumY
    {
      get { return (Double)nudSizeY.Minimum; }
      set { nudSizeY.Minimum = (Decimal)value; }
    }
    public Double MaximumY
    {
      get { return (Double)nudSizeY.Maximum; }
      set { nudSizeY.Maximum = (Decimal)value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String header)
    {
      Boolean Result = true;
      Double DValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY + header);
      //
      DValue = INIT_SIZEX;
      Result &= initdata.ReadDouble(NAME_SIZEX, out DValue, DValue);
      SizeX = DValue;
      //
      DValue = INIT_SIZEY;
      Result &= initdata.ReadDouble(NAME_SIZEY, out DValue, DValue);
      SizeY = DValue;
      //
      Result &= initdata.CloseSection();
      //
      nudX_ValueChanged(this, null);
      nudY_ValueChanged(this, null);
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String header)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY + header);
      //
      Result &= initdata.WriteDouble(NAME_SIZEX, SizeX);
      Result &= initdata.WriteDouble(NAME_SIZEY, SizeY);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //--------------------------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------------------------
    //
    private void nudX_ValueChanged(object sender, EventArgs e)
    {
      if (FOnSizePercentChanged is DOnSizePercentChanged)
      {
        FOnSizePercentChanged(SizeX, SizeY);
      }
    }

    private void nudY_ValueChanged(object sender, EventArgs e)
    {
      if (FOnSizePercentChanged is DOnSizePercentChanged)
      {
        FOnSizePercentChanged(SizeX, SizeY);
      }
    }

  }
}

