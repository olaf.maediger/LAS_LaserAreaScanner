﻿namespace UCControl
{
  partial class CUCSelectFontColorOpacity
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.FDialogFont = new System.Windows.Forms.FontDialog();
      this.FUCColorOpacity = new UCControl.CUCColorOpacity();
      this.FUCSelectFont = new UCControl.CUCSelectFont();
      this.SuspendLayout();
      // 
      // FDialogFont
      // 
      this.FDialogFont.AllowVerticalFonts = false;
      this.FDialogFont.FontMustExist = true;
      this.FDialogFont.ShowColor = true;
      // 
      // FUCColorOpacity
      // 
      this.FUCColorOpacity.BackColor = System.Drawing.SystemColors.Info;
      this.FUCColorOpacity.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCColorOpacity.Location = new System.Drawing.Point(0, 0);
      this.FUCColorOpacity.Name = "FUCColorOpacity";
      this.FUCColorOpacity.Size = new System.Drawing.Size(214, 39);
      this.FUCColorOpacity.TabIndex = 1;
      this.FUCColorOpacity.Title = "Title";
      // 
      // FUCSelectFont
      // 
      this.FUCSelectFont.BackColor = System.Drawing.SystemColors.Info;
      this.FUCSelectFont.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCSelectFont.Location = new System.Drawing.Point(0, 39);
      this.FUCSelectFont.Name = "FUCSelectFont";
      this.FUCSelectFont.Size = new System.Drawing.Size(214, 30);
      this.FUCSelectFont.TabIndex = 2;
      // 
      // CUCSelectFontColor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.FUCSelectFont);
      this.Controls.Add(this.FUCColorOpacity);
      this.Name = "CUCSelectFontColor";
      this.Size = new System.Drawing.Size(214, 69);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.FontDialog FDialogFont;
    private CUCColorOpacity FUCColorOpacity;
    private CUCSelectFont FUCSelectFont;
  }
}
