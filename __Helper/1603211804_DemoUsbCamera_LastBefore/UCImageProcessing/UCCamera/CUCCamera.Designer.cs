﻿namespace UCCamera
{
  partial class CUCCamera
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlControls = new System.Windows.Forms.Panel();
      this.btnStartStopAcquisition = new System.Windows.Forms.Button();
      this.btnOpenCloseCamera = new System.Windows.Forms.Button();
      this.btnRefreshCollectionCameras = new System.Windows.Forms.Button();
      this.cbxTrueColor = new System.Windows.Forms.CheckBox();
      this.cbxPaletteKind = new System.Windows.Forms.ComboBox();
      this.lblPaletteKind = new System.Windows.Forms.Label();
      this.lblFrameRate = new System.Windows.Forms.Label();
      this.lblFrameRateHeader = new System.Windows.Forms.Label();
      this.cbxSelectResolution = new System.Windows.Forms.ComboBox();
      this.cbxSelectCamera = new System.Windows.Forms.ComboBox();
      this.btnInfinite = new System.Windows.Forms.Button();
      this.lblFrameCount = new System.Windows.Forms.Label();
      this.lblFrameCountTitle = new System.Windows.Forms.Label();
      this.lblFramePreset = new System.Windows.Forms.Label();
      this.nudFramePreset = new System.Windows.Forms.NumericUpDown();
      this.btnShowCameraProperties = new System.Windows.Forms.Button();
      this.pnlImage = new System.Windows.Forms.Panel();
      this.tmrSearchUsbCamera = new System.Windows.Forms.Timer(this.components);
      this.tbcCamera = new System.Windows.Forms.TabControl();
      this.tbpImage = new System.Windows.Forms.TabPage();
      this.tbpData = new System.Windows.Forms.TabPage();
      this.FUCViewTable = new UCViewTable.CUCViewTable();
      this.FUCViewImage = new UCViewImage.CUCViewImage();
      this.pnlControls.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudFramePreset)).BeginInit();
      this.pnlImage.SuspendLayout();
      this.tbcCamera.SuspendLayout();
      this.tbpImage.SuspendLayout();
      this.tbpData.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlControls
      // 
      this.pnlControls.Controls.Add(this.btnStartStopAcquisition);
      this.pnlControls.Controls.Add(this.btnOpenCloseCamera);
      this.pnlControls.Controls.Add(this.btnRefreshCollectionCameras);
      this.pnlControls.Controls.Add(this.cbxTrueColor);
      this.pnlControls.Controls.Add(this.cbxPaletteKind);
      this.pnlControls.Controls.Add(this.lblPaletteKind);
      this.pnlControls.Controls.Add(this.lblFrameRate);
      this.pnlControls.Controls.Add(this.lblFrameRateHeader);
      this.pnlControls.Controls.Add(this.cbxSelectResolution);
      this.pnlControls.Controls.Add(this.cbxSelectCamera);
      this.pnlControls.Controls.Add(this.btnInfinite);
      this.pnlControls.Controls.Add(this.lblFrameCount);
      this.pnlControls.Controls.Add(this.lblFrameCountTitle);
      this.pnlControls.Controls.Add(this.lblFramePreset);
      this.pnlControls.Controls.Add(this.nudFramePreset);
      this.pnlControls.Controls.Add(this.btnShowCameraProperties);
      this.pnlControls.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlControls.Location = new System.Drawing.Point(0, 637);
      this.pnlControls.Name = "pnlControls";
      this.pnlControls.Size = new System.Drawing.Size(735, 88);
      this.pnlControls.TabIndex = 82;
      // 
      // btnStartStopAcquisition
      // 
      this.btnStartStopAcquisition.Location = new System.Drawing.Point(160, 34);
      this.btnStartStopAcquisition.Name = "btnStartStopAcquisition";
      this.btnStartStopAcquisition.Size = new System.Drawing.Size(146, 23);
      this.btnStartStopAcquisition.TabIndex = 24;
      this.btnStartStopAcquisition.Text = "<start/stop acquisition>";
      this.btnStartStopAcquisition.UseVisualStyleBackColor = true;
      this.btnStartStopAcquisition.Click += new System.EventHandler(this.btnStartStopAcquisition_Click);
      // 
      // btnOpenCloseCamera
      // 
      this.btnOpenCloseCamera.Location = new System.Drawing.Point(8, 34);
      this.btnOpenCloseCamera.Name = "btnOpenCloseCamera";
      this.btnOpenCloseCamera.Size = new System.Drawing.Size(146, 23);
      this.btnOpenCloseCamera.TabIndex = 23;
      this.btnOpenCloseCamera.Text = "<open/close camera>";
      this.btnOpenCloseCamera.UseVisualStyleBackColor = true;
      this.btnOpenCloseCamera.Click += new System.EventHandler(this.btnOpenCloseCamera_Click);
      // 
      // btnRefreshCollectionCameras
      // 
      this.btnRefreshCollectionCameras.Location = new System.Drawing.Point(8, 6);
      this.btnRefreshCollectionCameras.Name = "btnRefreshCollectionCameras";
      this.btnRefreshCollectionCameras.Size = new System.Drawing.Size(146, 23);
      this.btnRefreshCollectionCameras.TabIndex = 18;
      this.btnRefreshCollectionCameras.Text = "Refresh Collection Cameras";
      this.btnRefreshCollectionCameras.UseVisualStyleBackColor = true;
      this.btnRefreshCollectionCameras.Click += new System.EventHandler(this.btnRefreshCollectionCameras_Click);
      // 
      // cbxTrueColor
      // 
      this.cbxTrueColor.Enabled = false;
      this.cbxTrueColor.Location = new System.Drawing.Point(11, 63);
      this.cbxTrueColor.Name = "cbxTrueColor";
      this.cbxTrueColor.Size = new System.Drawing.Size(83, 24);
      this.cbxTrueColor.TabIndex = 17;
      this.cbxTrueColor.Text = "TrueColor";
      this.cbxTrueColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxTrueColor.UseVisualStyleBackColor = true;
      this.cbxTrueColor.CheckedChanged += new System.EventHandler(this.cbxTrueColor_CheckedChanged);
      // 
      // cbxPaletteKind
      // 
      this.cbxPaletteKind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPaletteKind.Enabled = false;
      this.cbxPaletteKind.FormattingEnabled = true;
      this.cbxPaletteKind.Location = new System.Drawing.Point(161, 63);
      this.cbxPaletteKind.Name = "cbxPaletteKind";
      this.cbxPaletteKind.Size = new System.Drawing.Size(145, 21);
      this.cbxPaletteKind.TabIndex = 16;
      this.cbxPaletteKind.SelectedValueChanged += new System.EventHandler(this.cbxPaletteKind_SelectedValueChanged);
      // 
      // lblPaletteKind
      // 
      this.lblPaletteKind.AutoSize = true;
      this.lblPaletteKind.Enabled = false;
      this.lblPaletteKind.Location = new System.Drawing.Point(97, 67);
      this.lblPaletteKind.Name = "lblPaletteKind";
      this.lblPaletteKind.Size = new System.Drawing.Size(61, 13);
      this.lblPaletteKind.TabIndex = 15;
      this.lblPaletteKind.Text = "PaletteKind";
      // 
      // lblFrameRate
      // 
      this.lblFrameRate.BackColor = System.Drawing.SystemColors.Info;
      this.lblFrameRate.Location = new System.Drawing.Point(354, 92);
      this.lblFrameRate.Name = "lblFrameRate";
      this.lblFrameRate.Size = new System.Drawing.Size(45, 19);
      this.lblFrameRate.TabIndex = 14;
      this.lblFrameRate.Text = "0";
      this.lblFrameRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lblFrameRate.Visible = false;
      // 
      // lblFrameRateHeader
      // 
      this.lblFrameRateHeader.AutoSize = true;
      this.lblFrameRateHeader.Location = new System.Drawing.Point(301, 94);
      this.lblFrameRateHeader.Name = "lblFrameRateHeader";
      this.lblFrameRateHeader.Size = new System.Drawing.Size(52, 13);
      this.lblFrameRateHeader.TabIndex = 13;
      this.lblFrameRateHeader.Text = "Rate [Hz]";
      this.lblFrameRateHeader.Visible = false;
      // 
      // cbxSelectResolution
      // 
      this.cbxSelectResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxSelectResolution.FormattingEnabled = true;
      this.cbxSelectResolution.Location = new System.Drawing.Point(359, 8);
      this.cbxSelectResolution.Name = "cbxSelectResolution";
      this.cbxSelectResolution.Size = new System.Drawing.Size(119, 21);
      this.cbxSelectResolution.TabIndex = 11;
      // 
      // cbxSelectCamera
      // 
      this.cbxSelectCamera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxSelectCamera.FormattingEnabled = true;
      this.cbxSelectCamera.Location = new System.Drawing.Point(160, 8);
      this.cbxSelectCamera.Name = "cbxSelectCamera";
      this.cbxSelectCamera.Size = new System.Drawing.Size(193, 21);
      this.cbxSelectCamera.TabIndex = 9;
      this.cbxSelectCamera.SelectedIndexChanged += new System.EventHandler(this.cbxSelectCamera_SelectedIndexChanged);
      // 
      // btnInfinite
      // 
      this.btnInfinite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.btnInfinite.Location = new System.Drawing.Point(151, 90);
      this.btnInfinite.Name = "btnInfinite";
      this.btnInfinite.Size = new System.Drawing.Size(49, 23);
      this.btnInfinite.TabIndex = 4;
      this.btnInfinite.Text = "Infinite";
      this.btnInfinite.UseVisualStyleBackColor = true;
      this.btnInfinite.Visible = false;
      this.btnInfinite.Click += new System.EventHandler(this.btnInfinite_Click);
      // 
      // lblFrameCount
      // 
      this.lblFrameCount.BackColor = System.Drawing.SystemColors.Info;
      this.lblFrameCount.Location = new System.Drawing.Point(238, 92);
      this.lblFrameCount.Name = "lblFrameCount";
      this.lblFrameCount.Size = new System.Drawing.Size(59, 19);
      this.lblFrameCount.TabIndex = 3;
      this.lblFrameCount.Text = "0";
      this.lblFrameCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lblFrameCount.Visible = false;
      // 
      // lblFrameCountTitle
      // 
      this.lblFrameCountTitle.AutoSize = true;
      this.lblFrameCountTitle.Location = new System.Drawing.Point(204, 95);
      this.lblFrameCountTitle.Name = "lblFrameCountTitle";
      this.lblFrameCountTitle.Size = new System.Drawing.Size(35, 13);
      this.lblFrameCountTitle.TabIndex = 3;
      this.lblFrameCountTitle.Text = "Count";
      this.lblFrameCountTitle.Visible = false;
      // 
      // lblFramePreset
      // 
      this.lblFramePreset.AutoSize = true;
      this.lblFramePreset.Location = new System.Drawing.Point(9, 94);
      this.lblFramePreset.Name = "lblFramePreset";
      this.lblFramePreset.Size = new System.Drawing.Size(66, 13);
      this.lblFramePreset.TabIndex = 3;
      this.lblFramePreset.Text = "FramePreset";
      this.lblFramePreset.Visible = false;
      // 
      // nudFramePreset
      // 
      this.nudFramePreset.Location = new System.Drawing.Point(78, 91);
      this.nudFramePreset.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudFramePreset.Name = "nudFramePreset";
      this.nudFramePreset.Size = new System.Drawing.Size(68, 20);
      this.nudFramePreset.TabIndex = 2;
      this.nudFramePreset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudFramePreset.Visible = false;
      // 
      // btnShowCameraProperties
      // 
      this.btnShowCameraProperties.Location = new System.Drawing.Point(311, 34);
      this.btnShowCameraProperties.Name = "btnShowCameraProperties";
      this.btnShowCameraProperties.Size = new System.Drawing.Size(121, 23);
      this.btnShowCameraProperties.TabIndex = 1;
      this.btnShowCameraProperties.Text = "Camera Properties";
      this.btnShowCameraProperties.UseVisualStyleBackColor = true;
      this.btnShowCameraProperties.Click += new System.EventHandler(this.btnShowCameraProperties_Click);
      // 
      // pnlImage
      // 
      this.pnlImage.Controls.Add(this.tbcCamera);
      this.pnlImage.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlImage.Location = new System.Drawing.Point(0, 0);
      this.pnlImage.Name = "pnlImage";
      this.pnlImage.Size = new System.Drawing.Size(735, 637);
      this.pnlImage.TabIndex = 83;
      // 
      // tbcCamera
      // 
      this.tbcCamera.Alignment = System.Windows.Forms.TabAlignment.Bottom;
      this.tbcCamera.Controls.Add(this.tbpImage);
      this.tbcCamera.Controls.Add(this.tbpData);
      this.tbcCamera.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcCamera.Location = new System.Drawing.Point(0, 0);
      this.tbcCamera.Name = "tbcCamera";
      this.tbcCamera.SelectedIndex = 0;
      this.tbcCamera.Size = new System.Drawing.Size(735, 637);
      this.tbcCamera.TabIndex = 3;
      // 
      // tbpImage
      // 
      this.tbpImage.Controls.Add(this.FUCViewImage);
      this.tbpImage.Location = new System.Drawing.Point(4, 4);
      this.tbpImage.Name = "tbpImage";
      this.tbpImage.Padding = new System.Windows.Forms.Padding(3);
      this.tbpImage.Size = new System.Drawing.Size(727, 611);
      this.tbpImage.TabIndex = 0;
      this.tbpImage.Text = "Image";
      this.tbpImage.UseVisualStyleBackColor = true;
      // 
      // tbpData
      // 
      this.tbpData.Controls.Add(this.FUCViewTable);
      this.tbpData.Location = new System.Drawing.Point(4, 4);
      this.tbpData.Name = "tbpData";
      this.tbpData.Padding = new System.Windows.Forms.Padding(3);
      this.tbpData.Size = new System.Drawing.Size(727, 611);
      this.tbpData.TabIndex = 1;
      this.tbpData.Text = "Data";
      this.tbpData.UseVisualStyleBackColor = true;
      // 
      // FUCViewTable
      // 
      this.FUCViewTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCViewTable.Location = new System.Drawing.Point(3, 3);
      this.FUCViewTable.Name = "FUCViewTable";
      this.FUCViewTable.Size = new System.Drawing.Size(721, 605);
      this.FUCViewTable.TabIndex = 0;
      // 
      // FUCViewImage
      // 
      this.FUCViewImage.BitmapTransparency = 1F;
      this.FUCViewImage.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCViewImage.Location = new System.Drawing.Point(3, 3);
      this.FUCViewImage.Name = "FUCViewImage";
      this.FUCViewImage.Size = new System.Drawing.Size(721, 605);
      this.FUCViewImage.TabIndex = 3;
      // 
      // CUCCamera
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlImage);
      this.Controls.Add(this.pnlControls);
      this.Name = "CUCCamera";
      this.Size = new System.Drawing.Size(735, 725);
      this.pnlControls.ResumeLayout(false);
      this.pnlControls.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudFramePreset)).EndInit();
      this.pnlImage.ResumeLayout(false);
      this.tbcCamera.ResumeLayout(false);
      this.tbpImage.ResumeLayout(false);
      this.tbpData.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlControls;
    private System.Windows.Forms.ComboBox cbxPaletteKind;
    private System.Windows.Forms.Label lblPaletteKind;
    private System.Windows.Forms.Label lblFrameRate;
    private System.Windows.Forms.Label lblFrameRateHeader;
    private System.Windows.Forms.ComboBox cbxSelectResolution;
    private System.Windows.Forms.ComboBox cbxSelectCamera;
    private System.Windows.Forms.Button btnInfinite;
    private System.Windows.Forms.Label lblFrameCount;
    private System.Windows.Forms.Label lblFrameCountTitle;
    private System.Windows.Forms.Label lblFramePreset;
    private System.Windows.Forms.NumericUpDown nudFramePreset;
    private System.Windows.Forms.Button btnShowCameraProperties;
    private System.Windows.Forms.Panel pnlImage;
    private System.Windows.Forms.Timer tmrSearchUsbCamera;
    private System.Windows.Forms.CheckBox cbxTrueColor;
    private System.Windows.Forms.Button btnRefreshCollectionCameras;
    private System.Windows.Forms.Button btnStartStopAcquisition;
    private System.Windows.Forms.Button btnOpenCloseCamera;
    private System.Windows.Forms.TabControl tbcCamera;
    private System.Windows.Forms.TabPage tbpImage;
    private UCViewImage.CUCViewImage FUCViewImage;
    private System.Windows.Forms.TabPage tbpData;
    private UCViewTable.CUCViewTable FUCViewTable;
  }
}
