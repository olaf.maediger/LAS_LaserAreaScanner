﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
//
using NLMatrix;
using IPPalette256;
using IPMemory;
using Initdata;
//
using GrabberHardware;
using GHCommon; // no placement for using GrabberHardware here!!!!
using UCEditor;
using UCViewImage;
//
//
namespace UCCamera
{
  public delegate void DOnErrorUCCamera(String namefunction,
                                        String texterror);
  public delegate void DOnInfoUCCamera(String textinfo);

  public partial class CUCCamera : UserControl
  { //
    //---------------------------------------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------------------------------------
    //
    private const String NAME_LIBRARY = "UCCamera";
    //
    private const String NAME_CAMERANAME = "CameraName";
    private const String INIT_CAMERANAME = "QuickCam for Notebooks Deluxe";
    //
    private const String NAME_CAMERARESOLUTION = "CameraResolution";
    private const String INIT_CAMERARESOLUTION = "640 * 480 [30 Hz]";
    private const String FORMAT_RESOLUTION = "{0} * {1} {2}Hz";
    //
    private const String NAME_FRAMEPRESET = "FramePreset";
    private const Int32 INIT_FRAMEPRESET = 0;
    //
    private const String NAME_PALETTEKIND = "PaletteKind";
    private const EPaletteKind INIT_PALETTEKIND = EPaletteKind.RGBSection;
    //
    private const String NAME_TRUECOLORMODE = "TrueColorMode";
    private const Boolean INIT_TRUECOLORMODE = true;
    //
    private const String NAME_CAMERAISOPEN = "CameraIsOpen";
    private const Boolean INIT_CAMERAISOPEN = false;
    //
    private const String NAME_CAMERAISBUSY = "CameraIsBusy";
    private const Boolean INIT_CAMERAISBUSY = false;
    //
    private const String TEXT_OPENCAMERA = "Open Camera";
    private const String TEXT_CLOSECAMERA = "Close Camera";
    //
    private const String TEXT_STARTACQUISITION = "Start Acquisition";
    private const String TEXT_STOPACQUISITION = "Stop Acquisition";
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------------------
    //
    private DOnErrorUCCamera FOnErrorUCCamera;
    private DOnInfoUCCamera FOnInfoUCCamera;
    private DOnErrorGrabberHardware FOnErrorGrabberHardware;
    private DOnInfoGrabberHardware FOnInfoGrabberHardware;
    private CGrabberLibrary FGrabberLibrary;
    private DOnCameraStateChanged FOnCameraStateChanged;
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------------------
    //
    public CUCCamera()
    {
      InitializeComponent();
      //
      btnOpenCloseCamera.Text = TEXT_OPENCAMERA;
      btnOpenCloseCamera.Enabled = false;
      btnStartStopAcquisition.Text = TEXT_STARTACQUISITION;
      btnStartStopAcquisition.Enabled = false;
      cbxTrueColor.Enabled = false;
      lblPaletteKind.Enabled = false;
      cbxPaletteKind.Enabled = false;
      cbxPaletteKind.Items.AddRange(CPalette256.PALETTEKINDS);
      cbxPaletteKind.SelectedIndex = 0;
      btnShowCameraProperties.Enabled = false;
    }
    //
    //--------------------------------------------
    //	Section - Property - Common
    //--------------------------------------------
    //
    public void SetOnErrorUCCamera(DOnErrorUCCamera value)
    {
      FOnErrorUCCamera = value;
    }
    public void SetOnInfoUCCamera(DOnInfoUCCamera value)
    {
      FOnInfoUCCamera = value;
    }
    //
    //--------------------------------------------
    //	Section - Property - GrabberHardware
    //--------------------------------------------
    //
    public void SetOnErrorGrabberHardware(DOnErrorGrabberHardware value)
    {
      FOnErrorGrabberHardware = value;
    }
    public void SetOnInfoGrabberHardware(DOnInfoGrabberHardware value)
    {
      FOnInfoGrabberHardware = value;
    }
    //
    //--------------------------------------------
    //	Section - Property - Camera
    //--------------------------------------------
    //
    public void SetOnCameraStateChanged(DOnCameraStateChanged value)
    {
      FOnCameraStateChanged = value;
    }

    public Boolean IsCameraOpen()
    {
      return (FGrabberLibrary is CGrabberLibrary);
    }

    public Boolean IsCameraBusy()
    {
      if (FGrabberLibrary is CGrabberLibrary)
      {
        return FGrabberLibrary.IsCameraBusy();
      }
      return false;
    }

    public void SetTrueColorMode(Boolean value)
    {
      cbxTrueColor.CheckedChanged -= cbxTrueColor_CheckedChanged;
      cbxTrueColor.Checked = value;
      cbxTrueColor.CheckedChanged += cbxTrueColor_CheckedChanged;
      //
      if (FGrabberLibrary is CGrabberLibrary)
      {
        FGrabberLibrary.SetTrueColorMode(value);
      }
      //
      if (value)
      {
        lblPaletteKind.Enabled = false;
        cbxPaletteKind.Enabled = false;
      }
      else
      {
        lblPaletteKind.Enabled = true;
        cbxPaletteKind.Enabled = true;
      }
    }

    public Boolean IsTrueColorMode
    {
      get { return cbxTrueColor.Checked; }
    }
    //
    //--------------------------------------------
    //	Section - Property - UCViewImage
    //--------------------------------------------
    //
    public void SetBitmapSource(Bitmap bitmap)
    {
      FUCViewImage.SetBitmap(bitmap);
    }

    public void SetBitmapBackground(Bitmap bitmap)
    {
      FUCViewImage.SetBitmapBackground(bitmap);
    }

    public CUCViewImage UCViewImage
    {
      get { return FUCViewImage; }
    }

    public void SetViewImageOnDialogRealChanged(DOnDialogRealChanged value)
    {
      FUCViewImage.SetOnDialogRealChanged(value);
    }

    public void SetViewImageOnDialogGridChanged(DOnDialogGridChanged value)
    {
      FUCViewImage.SetOnDialogGridChanged(value);
    }

    public void SetViewImageOnDialogTitleChanged(DOnDialogTitleChanged value)
    {
      FUCViewImage.SetOnDialogTitleChanged(value);
    }
    //
    //----------------------------
    //  Section - Initdata
    //----------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata, String section)
    {
      Boolean Result = true;
      Boolean BValue;
      // Initialisation of actual Camera/Resolutions
      RefreshCollectionCameras();
      RefreshCollectionResolutionFramerates();
      // 
      Result &= initdata.OpenSection(section);
      //
      // Camera
      //
      String SValue = INIT_CAMERANAME;
      Result &= initdata.ReadString(NAME_CAMERANAME, out SValue, SValue);
      String CameraName = SValue;
      SelectCameraName(CameraName);
      //
      SValue = INIT_CAMERARESOLUTION;
      Result &= initdata.ReadString(NAME_CAMERARESOLUTION, out SValue, SValue);
      String CameraResolution = SValue;
      SelectCameraResolution(CameraResolution);
      //
      Int32 IValue = INIT_FRAMEPRESET;
      Result &= initdata.ReadInt32(NAME_FRAMEPRESET, out IValue, IValue);
      nudFramePreset.Value = IValue;
      //
      // Palette
      //
      SValue = CPalette256.PaletteKindToText(INIT_PALETTEKIND);
      Result &= initdata.ReadEnumeration(NAME_PALETTEKIND, out SValue, SValue);
      SelectPaletteKindName(SValue);
      //
      BValue = INIT_TRUECOLORMODE;
      Result &= initdata.ReadBoolean(NAME_TRUECOLORMODE, out BValue, BValue);
      Boolean TCM = BValue;
      //
      // Acquisition
      //
      BValue = INIT_CAMERAISOPEN;
      Result &= initdata.ReadBoolean(NAME_CAMERAISOPEN, out BValue, BValue);
      Boolean CIO = BValue;
      //
      BValue = INIT_CAMERAISBUSY;
      Result &= initdata.ReadBoolean(NAME_CAMERAISBUSY, out BValue, BValue);
      Boolean CIB = BValue;
      //
      Result &= FUCViewImage.LoadInitdata(initdata, "");
      //
      Result &= initdata.CloseSection();
      //
      // Active Camera from Initfile
      if (CIO)
      {
        OpenCamera();
        SetTrueColorMode(TCM);
        if (CIB)
        {
          StartAcquisition();
        }
        else
        {
          StopAcquisition();
        }
      }
      else
      {
        SetTrueColorMode(TCM);
        CloseCamera(); // includes StopAcquisition()
      }
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata, String section)
    {
      Boolean Result = true;
      // 
      Result &= initdata.CreateSection(section);
      // Camera
      Result &= initdata.WriteString(NAME_CAMERANAME, cbxSelectCamera.Text);
      Result &= initdata.WriteString(NAME_CAMERARESOLUTION, cbxSelectResolution.Text);
      Result &= initdata.WriteInt32(NAME_FRAMEPRESET, (Int32)nudFramePreset.Value);
      // Palette
      Result &= initdata.WriteBoolean(NAME_TRUECOLORMODE, IsTrueColorMode);
      Result &= initdata.WriteEnumeration(NAME_PALETTEKIND, cbxPaletteKind.Text);
      // Acquisition
      Result &= initdata.WriteBoolean(NAME_CAMERAISOPEN, IsCameraOpen());
      Result &= initdata.WriteBoolean(NAME_CAMERAISBUSY, IsCameraBusy());
      //
      Result &= FUCViewImage.SaveInitdata(initdata, "");
      //
      Result &= initdata.CloseSection();
      // 
      return Result;
    }
    //
    //------------------------------------------------
    //	Section - Public Helper for Moving / Showing
    //------------------------------------------------
    //
    public void MoveDialogs(Int32 deltaleft, Int32 deltatop)
    {
      FUCViewImage.MoveDialogs(deltaleft, deltatop);
    }

    public void ShowDialogEditReal(Boolean show)
    {
      FUCViewImage.ShowDialogEditReal(show);
    }

    public void ShowDialogEditGrid(Boolean show)
    {
      FUCViewImage.ShowDialogEditGrid(show);
    }

    public void ShowDialogEditTitle(Boolean show)
    {
      FUCViewImage.ShowDialogEditTitle(show);
    }
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Helper - Common
    //---------------------------------------------------------------------------------------
    // 
    private void Error(String namefunction, String texterror)
    {
      if (FOnErrorUCCamera is DOnErrorUCCamera)
      {
        FOnErrorUCCamera(namefunction, texterror);
      }
    }

    private void Info(String textinfo)
    {
      if (FOnInfoUCCamera is DOnInfoUCCamera)
      {
        FOnInfoUCCamera(textinfo);
      }
    }
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Helper - Common
    //---------------------------------------------------------------------------------------
    // 
    public Boolean RefreshCollectionCameras()
    {
      cbxSelectCamera.Items.Clear();
      List<ECameraGroup> CameraGroups;
      List<String> CameraIdentifiers;
      List<String> CameraNames;
      if (CGHCommon.GetCollectionCameras(out CameraGroups, out CameraIdentifiers, out CameraNames))
      {
        foreach (String CameraName in CameraNames)
        {
          cbxSelectCamera.Items.Add(CameraName);
        }
        if (0 < cbxSelectCamera.Items.Count)
        {
          if (cbxSelectCamera.Text.Length < 1)
          {
            cbxSelectCamera.SelectedIndex = 0;
            btnOpenCloseCamera.Enabled = true;
          }
          return true;
        }
      }
      return false;
    }

    public Boolean RefreshCollectionResolutionFramerates()
    {
      cbxSelectResolution.Items.Clear();
      List<Int32> CameraFrameWidths;
      List<Int32> CameraFrameHeights;
      List<Int32> CameraFrameRates;
      Int32 IndexCamera = cbxSelectCamera.SelectedIndex;
      if (0 <= IndexCamera)
      {
        if (CGHCommon.GetCollectionResolutionFramerates(IndexCamera,
                                                        out CameraFrameWidths,
                                                        out CameraFrameHeights,
                                                        out CameraFrameRates))
        {
          Int32 CC = CameraFrameWidths.Count;
          for (Int32 CI = 0; CI < CC; CI++)
          {
            String Line = String.Format(FORMAT_RESOLUTION,
                                        CameraFrameWidths[CI], 
                                        CameraFrameHeights[CI], 
                                        CameraFrameRates[CI]);
            cbxSelectResolution.Items.Add(Line);
          }
        }
        if (0 < cbxSelectResolution.Items.Count)
        {
          if (cbxSelectResolution.Text.Length < 1)
          {
            cbxSelectResolution.SelectedIndex = 0;
          }
          return true;
        }
      }
      return false;
    }

    private Boolean SelectCameraName(String cameraname)
    {
      Int32 CameraIndex = -1;
      foreach (String CameraName in cbxSelectCamera.Items)
      {
        CameraIndex++;
        if (cameraname == CameraName)
        {
          cbxSelectCamera.SelectedIndex = CameraIndex;
          return true;
        }
      }
      return false;
    }

    private Boolean SelectCameraResolution(String cameraresolution)
    {
      Int32 CameraIndex = -1;
      foreach (String CameraResolution in cbxSelectResolution.Items)
      {
        CameraIndex++;
        if (cameraresolution == CameraResolution)
        {
          cbxSelectResolution.SelectedIndex = CameraIndex;
          return true;
        }
      }
      return false;
    }

    private Boolean SelectPaletteGrabberLibrary()
    {
      if (FGrabberLibrary is CGrabberLibrary)
      {
        EPaletteKind PK = CPalette256.TextToPaletteKind(cbxPaletteKind.Text);
        return FGrabberLibrary.SetPaletteKind(PK);
      }
      return false;
    }

    private Boolean SelectPaletteKindName(String palettekindname)
    {
      Int32 PaletteIndex = -1;
      foreach (String PaletteKindName in cbxPaletteKind.Items)
      {
        PaletteIndex++;
        if (palettekindname == PaletteKindName)
        {
          cbxPaletteKind.SelectedIndex = PaletteIndex;
          SelectPaletteGrabberLibrary();
          return true;
        }
      }
      return false;
    }
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Callback - GrabberHardware
    //---------------------------------------------------------------------------------------
    // 
    private void OnErrorGrabberHardware(String namelibrary,
                                        String namefunction,
                                        String texterror)
    {
      if (FOnErrorGrabberHardware is DOnErrorGrabberHardware)
      {
        FOnErrorGrabberHardware(namelibrary, namefunction, texterror);
      }
    }

    private void OnInfoGrabberHardware(String namelibrary,
                                       String textinfo)
    {
      if (FOnInfoGrabberHardware is DOnInfoGrabberHardware)
      {
        FOnInfoGrabberHardware(namelibrary, textinfo);
      }
    }
    //
    //--------------------------------------------
    //	Section - Callback - Camera Aquisition
    //--------------------------------------------
    //
    private void UCCameraOnCameraStateChanged(Boolean iscameraopen, Boolean iscamerabusy)
    { // Camera Open?!
      if (iscameraopen)
      {
        btnRefreshCollectionCameras.Enabled = false;
        cbxSelectCamera.Enabled = false;
        cbxSelectResolution.Enabled = false;
        btnOpenCloseCamera.Text = TEXT_CLOSECAMERA;
        btnStartStopAcquisition.Enabled = true;
        cbxTrueColor.Enabled = true;
        lblPaletteKind.Enabled = !cbxTrueColor.Checked;
        cbxPaletteKind.Enabled = !cbxTrueColor.Checked;
        btnShowCameraProperties.Enabled = true;
      }
      else
      {
        btnRefreshCollectionCameras.Enabled = true;
        cbxSelectCamera.Enabled = true;
        cbxSelectResolution.Enabled = true;
        btnOpenCloseCamera.Text = TEXT_OPENCAMERA;
        btnStartStopAcquisition.Enabled = false;
        cbxTrueColor.Enabled = false;
        lblPaletteKind.Enabled = false;
        cbxPaletteKind.Enabled = false;
        btnShowCameraProperties.Enabled = false;
      }
      // Camera Busy?!
      if (iscamerabusy)
      {
        btnStartStopAcquisition.Text = TEXT_STOPACQUISITION;
      }
      else
      {
        btnStartStopAcquisition.Text = TEXT_STARTACQUISITION;
      }
      //
      if (FOnCameraStateChanged is DOnCameraStateChanged)
      {
        FOnCameraStateChanged(iscameraopen, iscamerabusy);
      }
    }
    //
    //--------------------------------------------
    //	Section - Callback - TrueColor
    //--------------------------------------------
    //
    private delegate void CBOnNewFrameBitmapRgb(CBitmapRgb bitmaprgb);
    private void OnNewFrameBitmapRgb(CBitmapRgb bitmaprgb)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameBitmapRgb CB = new CBOnNewFrameBitmapRgb(OnNewFrameBitmapRgb);
          Invoke(CB, new object[] { bitmaprgb });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewImage.SetBitmapRgb(bitmaprgb);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameBitmapRgb", "SetBitmapSource");
      }
    }

    private delegate void CBOnNewFrameBitmapArgb(CBitmapArgb bitmapargb);
    private void OnNewFrameBitmapArgb(CBitmapArgb bitmapargb)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameBitmapArgb CB = new CBOnNewFrameBitmapArgb(OnNewFrameBitmapArgb);
          Invoke(CB, new object[] { bitmapargb });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewImage.SetBitmapArgb(bitmapargb);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameBitmapArgb", "SetBitmapSource");
      }
    }

    private delegate void CBOnNewFrameBitmap256(CBitmap256 bitmap256);
    private void OnNewFrameBitmap256(CBitmap256 bitmap256)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameBitmap256 CB = new CBOnNewFrameBitmap256(OnNewFrameBitmap256);
          Invoke(CB, new object[] { bitmap256 });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewImage.SetBitmap256(bitmap256);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameBitmap256", "SetBitmap256");
      }
    }

    private delegate void CBOnNewFrameMatrix08Bit(CMatrix08Bit matrix08Bit);
    private void OnNewFrameMatrix08Bit(CMatrix08Bit matrix08Bit)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameMatrix08Bit CB = new CBOnNewFrameMatrix08Bit(OnNewFrameMatrix08Bit);
          Invoke(CB, new object[] { matrix08Bit });
        }
        else
        {
          EPaletteKind PaletteKind = FGrabberLibrary.GetPaletteKind();
          CPalette256 Palette256 = CPalette256.CreatePalette256(PaletteKind);
          // !!!!!!!!!!!!!!!!!!!!! FUCViewImage.SetMatrix08Bit(matrix08Bit, Palette256);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameMatrix08Bit", "SetMatrix08Bit");
      }
    }

    private delegate void CBOnNewFrameMatrix10Bit(CMatrix10Bit matrix10bit);
    private void OnNewFrameMatrix10Bit(CMatrix10Bit matrix10bit)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameMatrix10Bit CB = new CBOnNewFrameMatrix10Bit(OnNewFrameMatrix10Bit);
          Invoke(CB, new object[] { matrix10bit });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrix10bit(matrix10bit);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameMatrix10Bit", "SetMatrix10Bit");
      }
    }

    private delegate void CBOnNewFrameMatrix12Bit(CMatrix12Bit matrix12bit);
    private void OnNewFrameMatrix12Bit(CMatrix12Bit matrix12bit)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameMatrix12Bit CB = new CBOnNewFrameMatrix12Bit(OnNewFrameMatrix12Bit);
          Invoke(CB, new object[] { matrix12bit });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrix12bit(matrix12bit);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameMatrix12Bit", "SetMatrix12Bit");
      }
    }

    private delegate void CBOnNewFrameMatrix16Bit(CMatrix16Bit matrix16bit);
    private void OnNewFrameMatrix16Bit(CMatrix16Bit matrix16bit)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameMatrix16Bit CB = new CBOnNewFrameMatrix16Bit(OnNewFrameMatrix16Bit);
          Invoke(CB, new object[] { matrix16bit });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrix16bit(matrix16bit);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameMatrix16Bit", "SetMatrix16Bit");
      }
    }

    private delegate void CBOnNewFrameMatrixByte(Byte[,] matrixbyte);
    private void OnNewFrameMatrixByte(Byte[,] matrixbyte)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameMatrixByte CB = new CBOnNewFrameMatrixByte(OnNewFrameMatrixByte);
          Invoke(CB, new object[] { matrixbyte });
        }
        else
        {
          EPaletteKind PaletteKind = FGrabberLibrary.GetPaletteKind();
          CPalette256 Palette256 = CPalette256.CreatePalette256(PaletteKind);
          // !!!!!!!!!!!!!!!!!!!!! FUCViewImage.SetMatrixByte(matrixbyte, Palette256);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameMatrixByte", "SetMatrixByte");
      }
    }

    private delegate void CBOnNewFrameMatrixUInt10(UInt16[,] matrixuint10);
    private void OnNewFrameMatrixUInt10(UInt16[,] matrixuint10)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameMatrixUInt10 CB = new CBOnNewFrameMatrixUInt10(OnNewFrameMatrixUInt10);
          Invoke(CB, new object[] { matrixuint10 });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrixUInt10(matrixuint10);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameMatrix10Bit", "SetMatrixUInt10");
      }
    }

    private delegate void CBOnNewFrameMatrixUInt12(UInt16[,] matrixuint12);
    private void OnNewFrameMatrixUInt12(UInt16[,] matrixuint12)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameMatrixUInt12 CB = new CBOnNewFrameMatrixUInt12(OnNewFrameMatrixUInt12);
          Invoke(CB, new object[] { matrixuint12 });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrixUInt12(matrixuint12);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameMatrix12Bit", "SetMatrixUInt12");
      }
    }

    private delegate void CBOnNewFrameMatrixUInt16(UInt16[,] matrixuint16);
    private void OnNewFrameMatrixUInt16(UInt16[,] matrixuint16)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFrameMatrixUInt16 CB = new CBOnNewFrameMatrixUInt16(OnNewFrameMatrixUInt16);
          Invoke(CB, new object[] { matrixuint16 });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrixUInt16(matrixuint16);
        }
      }
      catch (Exception)
      {
        Error("OnNewFrameMatrix16Bit", "SetMatrixUInt16");
      }
    }
    //
    //--------------------------------------------
    //	Section - Callback - PaletteColor
    //--------------------------------------------
    //
    private delegate void CBOnNewFramePalette256BitmapRgb(CBitmapRgb bitmaprgb);
    private void OnNewFramePalette256BitmapRgb(CBitmapRgb bitmaprgb)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256BitmapRgb CB = new CBOnNewFramePalette256BitmapRgb(OnNewFramePalette256BitmapRgb);
          Invoke(CB, new object[] { bitmaprgb });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewImage.SetBitmapRgb(bitmaprgb);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256BitmapRgb", "SetBitmapSource");
      }
    }

    private delegate void CBOnNewFramePalette256BitmapArgb(CBitmapArgb bitmapargb);
    private void OnNewFramePalette256BitmapArgb(CBitmapArgb bitmapargb)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256BitmapArgb CB = new CBOnNewFramePalette256BitmapArgb(OnNewFramePalette256BitmapArgb);
          Invoke(CB, new object[] { bitmapargb });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewImage.SetBitmapArgb(bitmapargb);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256BitmapArgb", "SetBitmapSource");
      }
    }

    private delegate void CBOnNewFramePalette256Bitmap256(CBitmap256 bitmap256);
    private void OnNewFramePalette256Bitmap256(CBitmap256 bitmap256)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256Bitmap256 CB = new CBOnNewFramePalette256Bitmap256(OnNewFramePalette256Bitmap256);
          Invoke(CB, new object[] { bitmap256 });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewImage.SetBitmap256(bitmap256);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256Bitmap256", "SetBitmap256");
      }
    }

    private delegate void CBOnNewFramePalette256Matrix08Bit(CMatrix08Bit matrix08Bit);
    private void OnNewFramePalette256Matrix08Bit(CMatrix08Bit matrix08Bit)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256Matrix08Bit CB = new CBOnNewFramePalette256Matrix08Bit(OnNewFramePalette256Matrix08Bit);
          Invoke(CB, new object[] { matrix08Bit });
        }
        else
        {
          //EPaletteKind PaletteKind = FGrabberLibrary.GetPaletteKind();
          //CPalette256 Palette256 = CPalette256.CreatePalette256(PaletteKind);
          // !!!!!!!!!!!!!!!!!!!!! FUCViewImage.SetMatrix08Bit(matrix08Bit, Palette256);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256Matrix08Bit", "SetMatrix08Bit");
      }
    }

    private delegate void CBOnNewFramePalette256Matrix10Bit(CMatrix10Bit matrix10bit);
    private void OnNewFramePalette256Matrix10Bit(CMatrix10Bit matrix10bit)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256Matrix10Bit CB = new CBOnNewFramePalette256Matrix10Bit(OnNewFramePalette256Matrix10Bit);
          Invoke(CB, new object[] { matrix10bit });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrix10bit(matrix10bit);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256Matrix10Bit", "SetMatrix10Bit");
      }
    }

    private delegate void CBOnNewFramePalette256Matrix12Bit(CMatrix12Bit matrix12bit);
    private void OnNewFramePalette256Matrix12Bit(CMatrix12Bit matrix12bit)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256Matrix12Bit CB = new CBOnNewFramePalette256Matrix12Bit(OnNewFramePalette256Matrix12Bit);
          Invoke(CB, new object[] { matrix12bit });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrix12bit(matrix12bit);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256Matrix12Bit", "SetMatrix12Bit");
      }
    }

    private delegate void CBOnNewFramePalette256Matrix16Bit(CMatrix16Bit matrix16bit);
    private void OnNewFramePalette256Matrix16Bit(CMatrix16Bit matrix16bit)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256Matrix16Bit CB = new CBOnNewFramePalette256Matrix16Bit(OnNewFramePalette256Matrix16Bit);
          Invoke(CB, new object[] { matrix16bit });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrix16bit(matrix16bit);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256Matrix16Bit", "SetMatrix16Bit");
      }
    }

    private delegate void CBOnNewFramePalette256MatrixByte(Byte[,] matrixbyte);
    private void OnNewFramePalette256MatrixByte(Byte[,] matrixbyte)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256MatrixByte CB = new CBOnNewFramePalette256MatrixByte(OnNewFramePalette256MatrixByte);
          Invoke(CB, new object[] { matrixbyte });
        }
        else
        {
          //EPaletteKind PaletteKind = FGrabberLibrary.GetPaletteKind();
          //CPalette256 Palette256 = CPalette256.CreatePalette256(PaletteKind);
          // !!!!!!!!!!!!!!!!!!!!! FUCViewImage.SetMatrixByte(matrixbyte, Palette256);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256MatrixByte", "SetMatrixByte");
      }
    }

    private delegate void CBOnNewFramePalette256MatrixUInt10(UInt16[,] matrixuint10);
    private void OnNewFramePalette256MatrixUInt10(UInt16[,] matrixuint10)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256MatrixUInt10 CB = new CBOnNewFramePalette256MatrixUInt10(OnNewFramePalette256MatrixUInt10);
          Invoke(CB, new object[] { matrixuint10 });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrixUInt10(matrixuint10);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256Matrix10Bit", "SetMatrixUInt10");
      }
    }

    private delegate void CBOnNewFramePalette256MatrixUInt12(UInt16[,] matrixuint12);
    private void OnNewFramePalette256MatrixUInt12(UInt16[,] matrixuint12)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256MatrixUInt12 CB = new CBOnNewFramePalette256MatrixUInt12(OnNewFramePalette256MatrixUInt12);
          Invoke(CB, new object[] { matrixuint12 });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrixUInt12(matrixuint12);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256Matrix12Bit", "SetMatrixUInt12");
      }
    }

    private delegate void CBOnNewFramePalette256MatrixUInt16(UInt16[,] matrixuint16);
    private void OnNewFramePalette256MatrixUInt16(UInt16[,] matrixuint16)
    {
      try
      {
        if (this.InvokeRequired)
        {
          CBOnNewFramePalette256MatrixUInt16 CB = new CBOnNewFramePalette256MatrixUInt16(OnNewFramePalette256MatrixUInt16);
          Invoke(CB, new object[] { matrixuint16 });
        }
        else
        {
          // !!!!!!!!!!!!!!!!!!!!! FUCViewTable.SetMatrixUInt16(matrixuint16);
        }
      }
      catch (Exception)
      {
        Error("OnNewFramePalette256Matrix16Bit", "SetMatrixUInt16");
      }
    }



    /*??????!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * 
     * 
        private delegate void CBOnNewFrameBitmap(Bitmap bitmap);
        private void OnNewFrameBitmap(Bitmap bitmap)
        {
          try
          {
            if (this.InvokeRequired)
            {
              CBOnNewFrameBitmap CB = new CBOnNewFrameBitmap(OnNewFrameBitmap);
              Invoke(CB, new object[] { bitmap });
            }
            else
            {
              FUCViewImage.SetBitmapSource(bitmap);
            }
          }
          catch (Exception)
          {
            Error("OnNewFrameBitmapRgb", "SetBitmapSource");
          }
        }
     
     * 
     * 
     * * 
        private delegate void CBOnNewFramePalette256Matrix10Bit(CPalette256 palette256, UInt16[,] matrix10bit);
        private void OnNewFramePalette256Matrix10Bit(CPalette256 palette256, UInt16[,] matrix10bit)
        {
          try
          {
            if (this.InvokeRequired)
            {
              CBOnNewFramePalette256Matrix10Bit CB = 
                new CBOnNewFramePalette256Matrix10Bit(OnNewFramePalette256Matrix10Bit);
              Invoke(CB, new object[] { palette256, matrix10bit });
            }
            else
            {
              //EPaletteKind PaletteKind = FGrabberLibrary.GetPaletteKind();
              //CPalette256 Palette256 = CPalette256.CreatePalette256(PaletteKind);
              //Bitmap BitmapSource;
              //// ok debug 
              //if (CBitmap.CopyMatrix10bitPalette256ToBitmap24bpp(matrix10bit, Palette256, out BitmapSource))
              //// ok debug BW if (CBitmap.CopyMatrix10bitToBitmap24bpp(matrix10bit, out BitmapSource))
              //{
              //  FUCViewImage.SetBitmapSource(BitmapSource);
              //}
              // End-Exception????? FUCViewTable.SetMatrix10bit(matrix10bit);
            }
          }
          catch (Exception)
          {
            Error("OnNewFrameMatrix10Bit", "SetMatrixSource");
          }
        }
     */
    //############################################
    //
    //--------------------------------------------
    //	Section - Event - Paintbox
    //--------------------------------------------
    //
    private void pbxImage_Resize(object sender, EventArgs e)
    {
      Refresh();
    }
    //
    //--------------------------------------------
    //	Section - Control - Event
    //--------------------------------------------
    //
    private void cbxSelectCamera_SelectedIndexChanged(object sender, EventArgs e)
    {
      RefreshCollectionResolutionFramerates();
    }

    private void cbxPaletteKind_SelectedValueChanged(object sender, EventArgs e)
    {
      SelectPaletteGrabberLibrary();
    }
    //
    //--------------------------------------------
    //	Section - Menu 
    //--------------------------------------------
    //
    private void btnInfinite_Click(object sender, EventArgs e)
    {
      // later!! nudFramePreset.Value = 0;
    }

    private void btnShowCameraProperties_Click(object sender, EventArgs e)
    {
      if (FGrabberLibrary is CGrabberLibrary)
      {
        if (FGrabberLibrary.IsCameraOpen())
        {
          FGrabberLibrary.ShowCameraProperties(this.Handle);
        }
      }
    }

    private void cbxTrueColor_CheckedChanged(object sender, EventArgs e)
    {
      SetTrueColorMode(cbxTrueColor.Checked);
    }
    //
    //--------------------------------------------
    //	Section - Management - Camera
    //--------------------------------------------
    //
    public void ShowImage(Boolean show)
    {
      this.Visible = show;
      //pbxImage.Visible = show;
    }

    public void ShowControls(Boolean show)
    {
      pnlControls.Visible = show;
    }

    private Boolean OpenCamera()
    {
      if (CGHCommon.OpenCamera(cbxSelectCamera.Text,
                               IsTrueColorMode,
                               OnErrorGrabberHardware,
                               OnInfoGrabberHardware,
                               UCCameraOnCameraStateChanged,
                               out FGrabberLibrary))
      {
        SelectPaletteGrabberLibrary();
        Info("OpenCamera - true");
        return true;
      }
      Info("OpenCamera - false");
      return false;
    }

    public Boolean StartAcquisition()
    {
      if (FGrabberLibrary is CGrabberLibrary)
      { // TrueColor
        FGrabberLibrary.SetOnNewFrameBitmapRgb(OnNewFrameBitmapRgb);
        FGrabberLibrary.SetOnNewFrameBitmapArgb(OnNewFrameBitmapArgb);
        FGrabberLibrary.SetOnNewFrameBitmap256(OnNewFrameBitmap256);
        FGrabberLibrary.SetOnNewFrameMatrix08Bit(OnNewFrameMatrix08Bit);
        FGrabberLibrary.SetOnNewFrameMatrix10Bit(OnNewFrameMatrix10Bit);
        FGrabberLibrary.SetOnNewFrameMatrix12Bit(OnNewFrameMatrix12Bit);
        FGrabberLibrary.SetOnNewFrameMatrix16Bit(OnNewFrameMatrix16Bit);
        //FGrabberLibrary.SetOnNewFrameMatrixByte(OnNewFrameMatrixByte);
        //FGrabberLibrary.SetOnNewFrameMatrixUInt10(OnNewFrameMatrixUInt10);
        //FGrabberLibrary.SetOnNewFrameMatrixUInt12(OnNewFrameMatrixUInt12);
        //FGrabberLibrary.SetOnNewFrameMatrixUInt16(OnNewFrameMatrixUInt16);
        // PaletteColor
        // !!!!!!!!!!!!!!!!!!!!! FGrabberLibrary.SetOnNewFramePalette256BitmapRgb(OnNewFramePalette256BitmapRgb);
        // !!!!!!!!!!!!!!!!!!!!! FGrabberLibrary.SetOnNewFramePalette256BitmapArgb(OnNewFramePalette256BitmapArgb);
        // !!!!!!!!!!!!!!!!!!!!! FGrabberLibrary.SetOnNewFramePalette256Bitmap256(OnNewFramePalette256Bitmap256);
        // !!!!!!!!!!!!!!!!!!!!! FGrabberLibrary.SetOnNewFramePalette256Matrix08Bit(OnNewFramePalette256Matrix08Bit);
        // !!!!!!!!!!!!!!!!!!!!! FGrabberLibrary.SetOnNewFramePalette256Matrix10Bit(OnNewFramePalette256Matrix10Bit);
        // !!!!!!!!!!!!!!!!!!!!! FGrabberLibrary.SetOnNewFramePalette256Matrix12Bit(OnNewFramePalette256Matrix12Bit);
        // !!!!!!!!!!!!!!!!!!!!! FGrabberLibrary.SetOnNewFramePalette256Matrix16Bit(OnNewFramePalette256Matrix16Bit);
        //FGrabberLibrary.SetOnNewFramePalette256MatrixByte(OnNewFramePalette256MatrixByte);
        //FGrabberLibrary.SetOnNewFramePalette256MatrixUInt10(OnNewFramePalette256MatrixUInt10);
        //FGrabberLibrary.SetOnNewFramePalette256MatrixUInt12(OnNewFramePalette256MatrixUInt12);
        //FGrabberLibrary.SetOnNewFramePalette256MatrixUInt16(OnNewFramePalette256MatrixUInt16);
        //
        if (FGrabberLibrary.StartAcquisition())
        {
          // NC !!! OnCameraStateChanged(IsCameraOpen(), true);
          Info("StartAcquisition - true");
          return true;
        }
      }
      Info("StartAcquisition - false");
      return false;
    }

    public Boolean StopAcquisition()
    {
      if (FGrabberLibrary is CGrabberLibrary)
      {
        Boolean Result = FGrabberLibrary.StopAcquisition();
        // TrueColor
        FGrabberLibrary.SetOnNewFrameBitmapRgb(null);
        FGrabberLibrary.SetOnNewFrameBitmapArgb(null);
        FGrabberLibrary.SetOnNewFrameBitmap256(null);
        FGrabberLibrary.SetOnNewFrameMatrix08Bit(null);
        FGrabberLibrary.SetOnNewFrameMatrix10Bit(null);
        FGrabberLibrary.SetOnNewFrameMatrix12Bit(null);
        FGrabberLibrary.SetOnNewFrameMatrix16Bit(null);
        //FGrabberLibrary.SetOnNewFrameMatrixByte(null);
        //FGrabberLibrary.SetOnNewFrameMatrixUInt10(null);
        //FGrabberLibrary.SetOnNewFrameMatrixUInt12(null);
        //FGrabberLibrary.SetOnNewFrameMatrixUInt16(null);
        // PaletteColor
        FGrabberLibrary.SetOnNewFramePalette256BitmapRgb(null);
        FGrabberLibrary.SetOnNewFramePalette256BitmapArgb(null);
        FGrabberLibrary.SetOnNewFramePalette256Bitmap256(null);
        FGrabberLibrary.SetOnNewFramePalette256Matrix08Bit(null);
        FGrabberLibrary.SetOnNewFramePalette256Matrix10Bit(null);
        FGrabberLibrary.SetOnNewFramePalette256Matrix12Bit(null);
        FGrabberLibrary.SetOnNewFramePalette256Matrix16Bit(null);
        //FGrabberLibrary.SetOnNewFramePalette256MatrixByte(null);
        //FGrabberLibrary.SetOnNewFramePalette256MatrixUInt10(null);
        //FGrabberLibrary.SetOnNewFramePalette256MatrixUInt12(null);
        //FGrabberLibrary.SetOnNewFramePalette256MatrixUInt16(null);
        //
        Info("StopAcquisition - true");
        return Result;
      }
      Info("StopAcquisition - false");
      return false;
    }

    public Boolean CloseCamera()
    {
      if (FGrabberLibrary is CGrabberLibrary)
      {
        if (FGrabberLibrary.Close())
        {
          FGrabberLibrary = null;
          Info("CloseCamera - true");
          return true;
        }
      }
      Info("CloseCamera - false");
      return false;
    }
    //
    //--------------------------------------------
    //	Section - Menu
    //--------------------------------------------
    //
    private void btnRefreshCollectionCameras_Click(object sender, EventArgs e)
    {
      RefreshCollectionCameras();
    }

    private void SetExposure(Int32 value)
    { // NO EFFECT !!!!
      //if (FGrabberLibrary is CGrabberLibrary)
      //{
      //  FGrabberLibrary.SetExposure(10);
      //}
      Error("SetExposure", "not supported");
    }

    private void btnOpenCloseCamera_Click(object sender, EventArgs e)
    {
      if (TEXT_OPENCAMERA == btnOpenCloseCamera.Text)
      {
        OpenCamera();
      }
      else
        if (TEXT_CLOSECAMERA == btnOpenCloseCamera.Text)
        {
          CloseCamera();
        }
    }

    private void btnStartStopAcquisition_Click(object sender, EventArgs e)
    {
      if (TEXT_STARTACQUISITION == btnStartStopAcquisition.Text)
      {
        StartAcquisition();
      }
      else
        if (TEXT_STOPACQUISITION == btnStartStopAcquisition.Text)
        {
          StopAcquisition();
        }
    }

  }
}
