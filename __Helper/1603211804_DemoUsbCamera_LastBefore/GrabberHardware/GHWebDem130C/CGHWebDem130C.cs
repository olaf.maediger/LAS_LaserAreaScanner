﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using AForge.Video;
using AForge.Video.DirectShow;
//
using IPMemory;
using GrabberHardware;
//
namespace GHWebDem130C
{
  public class CGHWebDem130C : CGrabberHardware
  {
    //
    //--------------------------------------------
    //	Section - Constant
    //--------------------------------------------
    //    
    //public const String CAMERA_NAME = "D130C-i";
    //public const String ENTRY_NAME = "Web " + CAMERA_NAME;
    //public const String LIBRARY_NAME = "WebDem130C";
    //
    //--------------------------------------------
    //	Section - Field
    //--------------------------------------------
    //    
    //!!!!!!!!!!!!####private VideoCaptureDevice FVideoCaptureDevice;
    private Boolean FIsCameraBusy;
    //
    //--------------------------------------------
    //	Section - Constructor
    //--------------------------------------------
    //
    public CGHWebDem130C()
      : base()
    {
      Int32 CI = (int)CDefinition.ECameraIndex.Dem130Ci;
      FGrabberData.LibraryName = CDefinition.LIBRARY_NAMES[CI];
      FGrabberData.EntryName = CDefinition.ENTRY_NAMES[CI];
      FGrabberData.CameraName = CDefinition.CAMERA_NAMES[CI];
      FIsCameraBusy = false;
    }
    //
    //--------------------------------------------
    //	Section - Property
    //--------------------------------------------
    //
    public override Boolean IsCameraOpen()
    {
      return false;// (FVideoCaptureDevice is VideoCaptureDevice);
    }

    public override Boolean IsCameraBusy()
    {
      return FIsCameraBusy;
    }

    //
    //--------------------------------------------
    //	Section - Handler
    //--------------------------------------------
    //
    public override Boolean Open(DOnErrorGrabberHardware onerrorgrabberhardware,
                                 DOnInfoGrabberHardware oninfograbberhardware)
    {
      FGrabberData.OnErrorGrabberHardware = onerrorgrabberhardware;
      FGrabberData.OnInfoGrabberHardware = oninfograbberhardware;
      //
      return false;
    }

    public override Boolean Close()
    {
      return false;
    }

    public override Boolean StartAcquisition()//RAcquisitionData acquisitiondata)
    {
      return false;
    }

    public override Boolean StopAcquisition()
    {
      return false;
    }

    public override Boolean SetExposure(Int32 value)
    {
      return false;
    }

    public override Boolean ShowCameraProperties(IntPtr hparent)
    {
      return false;
    }



  }
}
