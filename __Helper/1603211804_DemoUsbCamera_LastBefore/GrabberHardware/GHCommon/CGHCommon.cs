﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
//
using AForge.Video;
using AForge.Video.DirectShow;
//
using GrabberHardware;
//
// WebCamera
using GHWebDem130C;
using GHWebDntDigitalMicroscope;
using GHWebPhilipsSpc900Nc;
using GHWebUsb20PcCamera;
// VisioSens
using GHVisioSensVfuV024;
// PointGrey
using GHPointGreyFlea3;
//
namespace GHCommon
{
  //
  //-------------------------------------------------------------------------------
  // Segment - Definition
  //-------------------------------------------------------------------------------
  //
  public enum ECameraGroup
  {
    WebCamera = 0,
    VisioSens = 1,
    PointGrey = 2
  };
  //
  // ??? public delegate void DOnCameraStateChanged(Boolean isopen, Boolean isbusy);
  //
  //-------------------------------------------------------------------------------
  // Segment - CGrabberHardware - Common (static) Methods for ALL Grabber
  //-------------------------------------------------------------------------------
  //
  public class CGHCommon
  { //
    //---------------------------------------------------------------------------------------
    //  Segment - Field - WebCameras
    //---------------------------------------------------------------------------------------
    //
    // -> GHWebCam...
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Field - VisioSens ...
    //---------------------------------------------------------------------------------------
    //
    // -> GHVisioSens
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Refresh Management - WebCameras
    //---------------------------------------------------------------------------------------
    //
    private static Boolean GetWebCamLibraryNames(out List<String> librarynames)
    {
      librarynames = null;
      try
      {
        String WD = Directory.GetCurrentDirectory();
        DirectoryInfo DI = new DirectoryInfo(WD);
        FileInfo[] FIS = DI.GetFiles("GHWeb*.dll", SearchOption.TopDirectoryOnly);
        if (FIS is FileInfo[])
        {
          librarynames = new List<String>();
          for (Int32 II = 0; II < FIS.Length; II++)
          {
            FileInfo FI = FIS[II];
            librarynames.Add(FI.Name);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private static Boolean GetCollectionWebCameras(out List<String> cameraidentifier,
                                                   out List<String> cameranames)
    {
      cameraidentifier = new List<String>();
      cameranames = new List<String>();
      try
      {
        FilterInfoCollection FIC = new FilterInfoCollection(FilterCategory.VideoInputDevice);
        if (FIC is FilterInfoCollection)
        {
          Int32 CountCameras = 0;
          for (Int32 II = 0; II < FIC.Count; II++)
          {
            FilterInfo FI = FIC[II];
            String CameraName = FI.Name;
            if (0 < CameraName.Length)
            {
              cameraidentifier.Add(FI.MonikerString);
              cameranames.Add("Web " + FI.Name);
              CountCameras++;
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private static Boolean GetWebCameraResolutionFramerates(String cameraidentifier,
                                                            out List<Int32> cameraresolutionwidths,
                                                            out List<Int32> cameraresolutionheights,
                                                            out List<Int32> cameraframerates)
    {
      cameraresolutionwidths = new List<Int32>();
      cameraresolutionheights = new List<Int32>();
      cameraframerates = new List<Int32>();
      try
      {
        VideoCaptureDevice VCD = new VideoCaptureDevice(cameraidentifier);
        Int32 VCL = VCD.VideoCapabilities.Length;
        if (0 < VCL)
        {
          for (Int32 VCI = 0; VCI < VCL; VCI++)
          {
            Int32 VRWidth = VCD.VideoCapabilities[VCI].FrameSize.Width;
            cameraresolutionwidths.Add(VRWidth);
            Int32 VRHeight = VCD.VideoCapabilities[VCI].FrameSize.Height;
            cameraresolutionheights.Add(VRHeight);
            Int32 VRFrameRate = VCD.VideoCapabilities[VCI].AverageFrameRate;
            cameraframerates.Add(VRFrameRate);
          }
          //
          //Int32 CRF = cameraresolutionwidths.Count;
          //Result = new String[CRF];
          //for (Int32 RFI = 0; RFI < CRF; RFI++)
          //{
          //  String Line = String.Format("{0} * {1} [{2} Hz]",
          //                              cameraresolutionwidths[RFI],
          //                              cameraresolutionheights[RFI],
          //                              cameraframerates[RFI]);
          //  Result[RFI] = Line;
          //}
        }
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Refresh Management - Visiosens
    //---------------------------------------------------------------------------------------
    //
    private static Boolean GetVisioSensLibraryNames(out List<String> librarynames)
    {
      librarynames = null;
      try
      {
        String WD = Directory.GetCurrentDirectory();
        DirectoryInfo DI = new DirectoryInfo(WD);
        FileInfo[] FIS = DI.GetFiles("GHVisioSens*.dll", SearchOption.TopDirectoryOnly);
        if (FIS is FileInfo[])
        {
          librarynames = new List<String>();
          for (Int32 II = 0; II < FIS.Length; II++)
          {
            FileInfo FI = FIS[II];
            librarynames.Add(FI.Name);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private static Boolean GetCollectionVisioSensVfuV024s(out List<String> cameraidentifier,
                                                         out List<String> cameranames)
    {
      cameraidentifier = new List<String>();
      cameranames = new List<String>();
      try
      {
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private static Boolean GetVisioSensResolutionFramerates(String cameraidentifier,
                                                            out List<Int32> cameraresolutionwidths,
                                                            out List<Int32> cameraresolutionheights,
                                                            out List<Int32> cameraframerates)
    {
      cameraresolutionwidths = new List<Int32>();
      cameraresolutionheights = new List<Int32>();
      cameraframerates = new List<Int32>();
      try
      {
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Refresh Management - PointGrey
    //---------------------------------------------------------------------------------------
    //
    private static Boolean GetPointGreyLibraryNames(out List<String> librarynames)
    {
      librarynames = null;
      try
      {
        String WD = Directory.GetCurrentDirectory();
        DirectoryInfo DI = new DirectoryInfo(WD);
        FileInfo[] FIS = DI.GetFiles("GHPointGrey*.dll", SearchOption.TopDirectoryOnly);
        if (FIS is FileInfo[])
        {
          librarynames = new List<String>();
          for (Int32 II = 0; II < FIS.Length; II++)
          {
            FileInfo FI = FIS[II];
            librarynames.Add(FI.Name);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private static Boolean GetCollectionPointGreyCameras(out List<String> cameraidentifiers,
                                                         out List<String> cameranames)
    {
      cameraidentifiers = null;
      cameranames = null;
      try
      {
        String WD = Directory.GetCurrentDirectory();
        DirectoryInfo DI = new DirectoryInfo(WD);
        FileInfo[] FIS = DI.GetFiles("GHPointGrey*.dll", SearchOption.TopDirectoryOnly);
        if (FIS is FileInfo[])
        {
          cameraidentifiers = new List<String>();
          cameranames = new List<String>();
          for (Int32 II = 0; II < FIS.Length; II++)
          {
            FileInfo FI = FIS[II];
            //
            String[] SeparatorsMain = new String[2];
            SeparatorsMain[0] = "GH";
            SeparatorsMain[1] = ".dll";
            String[] TokensMain = FI.Name.Split(SeparatorsMain, StringSplitOptions.RemoveEmptyEntries);
            //
            String[] SeparatorsSub = new String[1];
            SeparatorsSub[0] = "PointGrey";
            String[] TokensSub = TokensMain[0].Split(SeparatorsSub, StringSplitOptions.RemoveEmptyEntries);
            //
            cameraidentifiers.Add(TokensSub[0]);
            cameranames.Add(SeparatorsSub[0] + " " + TokensSub[0]);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private static Boolean GetPointGreyResolutionFramerates(String cameraidentifier,
                                                            out List<Int32> cameraresolutionwidths,
                                                            out List<Int32> cameraresolutionheights,
                                                            out List<Int32> cameraframerates)
    {
      cameraresolutionwidths = new List<Int32>();
      cameraresolutionheights = new List<Int32>();
      cameraframerates = new List<Int32>();
      try
      {
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Global Management - Eltec
    //---------------------------------------------------------------------------------------
    //


    //
    //---------------------------------------------------------------------------------------
    //  Segment - Global Management - Lumenera
    //---------------------------------------------------------------------------------------
    //
    public static String[] GetLibraryNames()
    {
      try
      {
        String WD = Directory.GetCurrentDirectory();
        DirectoryInfo DI = new DirectoryInfo(WD);
        FileInfo[] FIS = DI.GetFiles("GH*.dll", SearchOption.TopDirectoryOnly);
        if (FIS is FileInfo[])
        {
          String[] LibraryNames = new String[FIS.Length];
          for (Int32 II = 0; II < FIS.Length; II++)
          {
            FileInfo FI = FIS[II];
            LibraryNames[II] = FI.Name;
          }
          return LibraryNames;
        }
        return null;
      }
      catch (Exception)
      {
        return null;
      }
    }

    public static Boolean GetCollectionCameras(out List<ECameraGroup> cameragroup,
                                               out List<String> cameraidentifiers,
                                               out List<String> cameranames)
    {
      cameragroup = new List<ECameraGroup>();
      cameraidentifiers = new List<String>();
      cameranames = new List<String>();
      List<String> CameraIdentifiers;
      List<String> CameraNames;
      // WebCamera
      if (GetCollectionWebCameras(out CameraIdentifiers, out CameraNames))
      {
        Int32 CC = Math.Min(CameraIdentifiers.Count, CameraNames.Count);
        for (Int32 CI = 0; CI < CC; CI++)
        {
          cameragroup.Add(ECameraGroup.WebCamera);
          cameraidentifiers.Add(CameraIdentifiers[CI]);
          cameranames.Add(CameraNames[CI]);
        }
      }
      // VisioSens
      if (GetCollectionVisioSensVfuV024s(out CameraIdentifiers, out CameraNames))
      {
        Int32 CC = Math.Min(CameraIdentifiers.Count, CameraNames.Count);
        for (Int32 CI = 0; CI < CC; CI++)
        {
          cameragroup.Add(ECameraGroup.VisioSens);
          cameraidentifiers.Add(CameraIdentifiers[CI]);
          cameranames.Add(CameraNames[CI]);
        }
      }
      // PointGrey
      if (GetCollectionPointGreyCameras(out CameraIdentifiers, out CameraNames))
      {
        Int32 CC = Math.Min(CameraIdentifiers.Count, CameraNames.Count);
        for (Int32 CI = 0; CI < CC; CI++)
        {
          cameragroup.Add(ECameraGroup.PointGrey);
          cameraidentifiers.Add(CameraIdentifiers[CI]);
          cameranames.Add(CameraNames[CI]);
        }
      }
      // 
      return (0 < cameraidentifiers.Count);
    }

    public static Boolean GetCollectionResolutionFramerates(Int32 indexcamera,
                                                            out List<Int32> framewidths,
                                                            out List<Int32> frameheights,
                                                            out List<Int32> framerates)
    {
      framewidths = null;
      frameheights = null;
      framerates = null;
      //
      List<ECameraGroup> CameraGroups = new List<ECameraGroup>(); // global
      List<String> CameraIdentifiers = new List<String>(); // global
      List<String> CameraNames = new List<String>(); // global
      List<String> CIS; // local
      List<String> CNS; // local
      //  WebCamera
      if (GetCollectionWebCameras(out CIS, out CNS))
      {
        Int32 CC = Math.Min(CIS.Count, CNS.Count);
        for (Int32 CI = 0; CI < CC; CI++)
        {
          CameraGroups.Add(ECameraGroup.WebCamera);
          CameraIdentifiers.Add(CIS[CI]);
          CameraNames.Add(CNS[CI]);
        }
      }
      //  VisioSens
      if (GetCollectionVisioSensVfuV024s(out CIS, out CNS))
      {
        Int32 CC = Math.Min(CIS.Count, CNS.Count);
        for (Int32 CI = 0; CI < CC; CI++)
        {
          CameraGroups.Add(ECameraGroup.WebCamera);
          CameraIdentifiers.Add(CIS[CI]);
          CameraNames.Add(CNS[CI]);
        }
      }
      //  PointGrey
      if (GetCollectionPointGreyCameras(out CIS, out CNS))
      {
        Int32 CC = Math.Min(CIS.Count, CNS.Count);
        for (Int32 CI = 0; CI < CC; CI++)
        {
          CameraGroups.Add(ECameraGroup.WebCamera);
          CameraIdentifiers.Add(CIS[CI]);
          CameraNames.Add(CNS[CI]);
        }
      }
      //--------------------------------------------------------------------
      if ((0 <= indexcamera) && (indexcamera < CameraIdentifiers.Count))
      {
        ECameraGroup CameraGroup = CameraGroups[indexcamera];
        String CameraIdentifier = CameraIdentifiers[indexcamera];
        String CameraName = CameraNames[indexcamera];
        if ((ECameraGroup.WebCamera == CameraGroup) &&
             GetWebCameraResolutionFramerates(CameraIdentifier, out framewidths,
                                              out frameheights, out framerates))
        {
          return true;
        }
        if ((ECameraGroup.VisioSens == CameraGroup) &&
             GetVisioSensResolutionFramerates(CameraIdentifier, out framewidths,
                                              out frameheights, out framerates))
        {
          return true;
        }
        if ((ECameraGroup.PointGrey == CameraGroup) &&
             GetPointGreyResolutionFramerates(CameraIdentifier, out framewidths,
                                              out frameheights, out framerates))
        {
          return true;
        }
      }
      return false;
    }
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Global Management - All - Open Camera
    //---------------------------------------------------------------------------------------
    //
    //  <DeviceName>MotorController PIC843</DeviceName>
    //  <LibraryName>MHPIC843.dll</LibraryName>
    //  <ClassName>MHPIC843.CMHPIC843</ClassName>
    private static Boolean OpenCameraLibrary(String cameraname,
                                             DOnErrorGrabberHardware onerrorgrabberhardware,
                                             DOnInfoGrabberHardware oninfograbberhardware,
                                             out CGrabberLibrary grabberlibrary)
    {
      grabberlibrary = null;
      String[] LibraryNames = CGHCommon.GetLibraryNames();
      if (0 < LibraryNames.Length)
      {
        String LibraryName = "GH" + cameraname + ".dll";
        String ClassName = "GH" + cameraname + ".CGH" + cameraname;
        grabberlibrary = new CGrabberLibrary(LibraryName, ClassName);
        if (grabberlibrary is CGrabberLibrary)
        {
          return grabberlibrary.Open(onerrorgrabberhardware, oninfograbberhardware);
        }
      }
      return false;
    }

    public  static Boolean OpenCamera(String selectcamera,
                                      Boolean istruecolormode,
                                      DOnErrorGrabberHardware onerrorgrabberhardware,
                                      DOnInfoGrabberHardware oninfograbberhardware,
                                      DOnCameraStateChanged oncamerastatechanged,
                                      out CGrabberLibrary grabberlibrary)
    {
      grabberlibrary = null;
      // Web Cameras
      if (CDefinition.ENTRY_NAMES[(int)CDefinition.ECameraIndex.Dem130Ci] == selectcamera)
      {
        if (OpenCameraLibrary(CDefinition.LIBRARY_NAMES[(int)CDefinition.ECameraIndex.Dem130Ci],
                              onerrorgrabberhardware,
                              oninfograbberhardware,
                              out grabberlibrary))
        {
          grabberlibrary.SetTrueColorMode(istruecolormode);
          grabberlibrary.SetOnCameraStateChanged(oncamerastatechanged);
          return true;
        }
      }
      if (CDefinition.ENTRY_NAMES[(int)CDefinition.ECameraIndex.Usb2p0PcCamera] == selectcamera)
      {
        if (OpenCameraLibrary(CDefinition.LIBRARY_NAMES[(int)CDefinition.ECameraIndex.Usb2p0PcCamera],
                              onerrorgrabberhardware,
                              oninfograbberhardware,
                              out grabberlibrary))
        {
          grabberlibrary.SetTrueColorMode(istruecolormode);
          grabberlibrary.SetOnCameraStateChanged(oncamerastatechanged);
          return true;
        }
      }
      if (CDefinition.ENTRY_NAMES[(int)CDefinition.ECameraIndex.PhilipsSpc900Nc] == selectcamera)
      {
        if (OpenCameraLibrary(CDefinition.LIBRARY_NAMES[(int)CDefinition.ECameraIndex.PhilipsSpc900Nc],
                              onerrorgrabberhardware,
                              oninfograbberhardware,
                              out grabberlibrary))
        {
          grabberlibrary.SetTrueColorMode(istruecolormode);
          grabberlibrary.SetOnCameraStateChanged(oncamerastatechanged);
          return true;
        }
      }
      if (CDefinition.ENTRY_NAMES[(int)CDefinition.ECameraIndex.DntDigitalMicroscope] == selectcamera)
      {
        if (OpenCameraLibrary(CDefinition.LIBRARY_NAMES[(int)CDefinition.ECameraIndex.DntDigitalMicroscope],
                              onerrorgrabberhardware,
                              oninfograbberhardware,
                              out grabberlibrary))
        {
          grabberlibrary.SetTrueColorMode(istruecolormode);
          grabberlibrary.SetOnCameraStateChanged(oncamerastatechanged);
          return true;
        }
      }
      // VisioSens Cameras
      if (CDefinition.ENTRY_NAMES[(int)CDefinition.ECameraIndex.VfuV024] == selectcamera)
      {
        if (OpenCameraLibrary(CDefinition.LIBRARY_NAMES[(int)CDefinition.ECameraIndex.VfuV024],
                              onerrorgrabberhardware,
                              oninfograbberhardware,
                              out grabberlibrary))
        {
          grabberlibrary.SetTrueColorMode(istruecolormode);
          grabberlibrary.SetOnCameraStateChanged(oncamerastatechanged);
          return true;
        }
      }
      // PointGrey Cameras
      if (CDefinition.ENTRY_NAMES[(int)CDefinition.ECameraIndex.Flea3] == selectcamera)
      {
        if (OpenCameraLibrary(CDefinition.LIBRARY_NAMES[(int)CDefinition.ECameraIndex.Flea3],
                              onerrorgrabberhardware,
                              oninfograbberhardware,
                              out grabberlibrary))
        {
          grabberlibrary.SetTrueColorMode(istruecolormode);
          grabberlibrary.SetOnCameraStateChanged(oncamerastatechanged);
          return true;
        }
      }
      return false;
    }



  }
}
