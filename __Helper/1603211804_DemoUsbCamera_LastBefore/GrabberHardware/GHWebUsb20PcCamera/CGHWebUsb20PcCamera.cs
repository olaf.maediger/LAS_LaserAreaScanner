﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using AForge.Video;
using AForge.Video.DirectShow;
//
using IPPalette256;
using IPMemory;
using GrabberHardware;
//
namespace GHWebUsb20PcCamera
{
  public class CGHWebUsb20PcCamera : CGrabberHardware
  {
    //
    //--------------------------------------------
    //	Section - Constant
    //--------------------------------------------
    //    

    //
    //---------------------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------------------
    //
    private VideoCaptureDevice FVideoCaptureDevice;
    private String FCameraMoniker;
    private Int32 FIndexResolutionFrameRate;
    private List<Int32>[] FCameraResolutionWidths;
    private List<Int32>[] FCameraResolutionHeights;
    private List<Int32>[] FCameraFrameRates;
    private Boolean FIsCameraBusy;
    //
    //--------------------------------------------
    //	Section - Constructor
    //--------------------------------------------
    //
    public CGHWebUsb20PcCamera()
      : base()
    {
      Int32 CI = (int)CDefinition.ECameraIndex.Usb2p0PcCamera;
      FGrabberData.LibraryName = CDefinition.LIBRARY_NAMES[CI];
      FGrabberData.EntryName = CDefinition.ENTRY_NAMES[CI];
      FGrabberData.CameraName = CDefinition.CAMERA_NAMES[CI];
      FVideoCaptureDevice = null;
      FCameraMoniker = "";
      FCameraResolutionWidths = null;
      FCameraResolutionHeights = null;
      FCameraFrameRates = null;
      FIndexResolutionFrameRate = -1;
      FIsCameraBusy = false;
    }
    //
    //--------------------------------------------
    //	Section - Property
    //--------------------------------------------
    //
    public override Boolean IsCameraOpen()
    {
      return (FVideoCaptureDevice is VideoCaptureDevice);
    }

    public override Boolean IsCameraBusy()
    {
      return FIsCameraBusy;
    }
    //
    //--------------------------------------------
    //	Section - Callback
    //--------------------------------------------
    //
    private void OnNewFrame(object sender,
                            NewFrameEventArgs arguments)
    {
      try
      { // Driver provides only Bitmap(24bpp) as Source:
        Bitmap BS = (Bitmap)arguments.Frame.Clone();
        Int32 BW = BS.Width;
        Int32 BH = BS.Height;
        //if (FGrabberData.TrueColorMode)
        //{ // Bitmap(24bpp) -> BitmapRgb(24bpp)
        //  if (FGrabberData.OnNewFrameBitmapRgb is DOnNewFrameBitmapRgb)
        //  { 
        //    if (FGrabberData.OnNewFrameBitmapRgb is DOnNewFrameBitmapRgb)
        //    {
        //      FGrabberData.OnNewFrameBitmapRgb(BS);
        //    }
        //  }
        //  // Bitmap(24bpp) -> BitmapArgb(32bpp)
        //  if (FGrabberData.OnNewFrameBitmapArgb is DOnNewFrameBitmapArgb)
        //  { 
        //    Bitmap BT32;
        //    switch (BS.PixelFormat)
        //    { // 24bpp -> 32bpp
        //      case PixelFormat.Format24bppRgb:                
        //        CBitmap.CopyBitmap24bppTo32bpp(BS, 0xFF, out BT32);
        //        if (FGrabberData.OnNewFrameBitmapArgb is DOnNewFrameBitmapArgb)
        //        {
        //          FGrabberData.OnNewFrameBitmapArgb(BT32);
        //        }
        //        break;
        //      // driver doesnt suppert this mode!!!
        //      //case PixelFormat.Format32bppArgb:
        //    }
        //  }
        //  // Bitmap(24bpp) -> Matrix08Bit
        //  if (FGrabberData.OnNewFrameMatrix08Bit is DOnNewFrameMatrix08Bit)
        //  {
        //    Byte[,] Matrix08Bit;
        //    if (CBitmap.CopyBitmap24bppToMatrix08Bit(BS, out Matrix08Bit))
        //    {
        //      if (FGrabberData.OnNewFrameMatrix08Bit is DOnNewFrameMatrix08Bit)
        //      {
        //        FGrabberData.OnNewFrameMatrix08Bit(Matrix08Bit);
        //      }
        //    }
        //  }
        //  // Bitmap(24bpp) + Palette256 -> Matrix08Bit
        //  if (FGrabberData.OnNewFramePalette256Matrix08Bit is DOnNewFramePalette256Matrix08Bit)
        //  {
        //    CPalette256 Palette256 = CPalette256.CreatePalette256(FGrabberData.PaletteKind);
        //    Byte[,] Matrix08Bit;
        //    if (CBitmap.CopyBitmap24bppPalette256ToMatrix08Bit(BS, Palette256, out Matrix08Bit))
        //    {
        //      if (FGrabberData.OnNewFrameMatrix08Bit is DOnNewFrameMatrix08Bit)
        //      {
        //        FGrabberData.OnNewFrameMatrix08Bit(Matrix08Bit);
        //      }
        //    }
        //  }
        //  // Bitmap(24bpp) -> Matrix10bit
        //  if (FGrabberData.OnNewFrameMatrix10Bit is DOnNewFrameMatrix10Bit)
        //  {
        //    UInt16[,] Matrix10bit;
        //    if (CBitmap.CopyBitmap24bppToMatrix10bit(BS, out Matrix10bit))
        //    {
        //      if (FGrabberData.OnNewFrameMatrix10Bit is DOnNewFrameMatrix10Bit)
        //      {
        //        FGrabberData.OnNewFrameMatrix10Bit(Matrix10bit);
        //      }
        //    }
        //  }
        //  // Bitmap(24bpp) + Palette256 -> Matrix10bit
        //  if (FGrabberData.OnNewFramePalette256Matrix10Bit is DOnNewFramePalette256Matrix10Bit)
        //  {
        //    CPalette256 Palette256 = CPalette256.CreatePalette256(FGrabberData.PaletteKind);
        //    UInt16[,] Matrix10bit;
        //    if (CBitmap.CopyBitmap24bppPalette256ToMatrix10bit(BS, Palette256, out Matrix10bit))
        //    {
        //      if (FGrabberData.OnNewFrameMatrix10Bit is DOnNewFrameMatrix10Bit)
        //      {
        //        FGrabberData.OnNewFrameMatrix10Bit(Matrix10bit);
        //      }
        //    }
        //  }
        //}
        //else // PaletteColorMode
        //{ // Bitmap(24bpp) -> Bitmap256(24bpp, Palette256)
        //  if (FGrabberData.OnNewFrameBitmapRgb is DOnNewFrameBitmapRgb)
        //  { 
        //    CBitmap256 Bitmap256 = new CBitmap256(BS, FGrabberData.PaletteKind, PixelFormat.Format24bppRgb);
        //    if (FGrabberData.OnNewFrameBitmapRgb is DOnNewFrameBitmapRgb)
        //    {
        //      FGrabberData.OnNewFrameBitmapRgb(Bitmap256.GetBitmapRgb());
        //    }
        //  }
        //  // Bitmap(24bpp) -> Bitmap256(32bpp, Palette256)
        //  if (FGrabberData.OnNewFrameBitmapArgb is DOnNewFrameBitmapArgb)
        //  {
        //    CBitmap256 Bitmap256 = new CBitmap256(BS, FGrabberData.PaletteKind, PixelFormat.Format32bppArgb);
        //    if (FGrabberData.OnNewFrameBitmapArgb is DOnNewFrameBitmapArgb)
        //    {
        //      FGrabberData.OnNewFrameBitmapArgb(Bitmap256.GetBitmapArgb());
        //    }
        //  }
        //}

      }
      catch (Exception)
      {
        Error(FGrabberData.LibraryName, "OnNewFrame", "CopyBitmap");
      }
    }
    //
    //--------------------------------------------
    //	Section - Management - Camera
    //--------------------------------------------
    //
    public override Boolean Open(DOnErrorGrabberHardware onerrorgrabberhardware,
                                 DOnInfoGrabberHardware oninfograbberhardware)
    {
      FGrabberData.OnErrorGrabberHardware = onerrorgrabberhardware;
      FGrabberData.OnInfoGrabberHardware = oninfograbberhardware;
      //
      if (IsCameraOpen())
      {
        Info(FGrabberData.LibraryName, "Open - false");
        return false;
      }
      // Refresh installed Usb-Cameras with Resolutions
      FilterInfoCollection FIC = new FilterInfoCollection(FilterCategory.VideoInputDevice);
      if (FIC is FilterInfoCollection)
      {
        List<String> CameraIdentifiers = new List<String>();
        List<String> CameraNames = new List<String>();
        for (Int32 II = 0; II < FIC.Count; II++)
        {
          FilterInfo FI = FIC[II];
          if (0 < FI.Name.Length)
          {
            CameraIdentifiers.Add(FI.MonikerString);
            CameraNames.Add(FI.Name);
          }
        }
        Int32 CountCameras = CameraNames.Count;
        if (0 < CountCameras)
        {
          List<Int32>[] CameraResolutionWidths = new List<Int32>[CountCameras];
          List<Int32>[] CameraResolutionHeights = new List<Int32>[CountCameras];
          List<Int32>[] CameraFrameRates = new List<Int32>[CountCameras];
          for (Int32 II = 0; II < CountCameras; II++)
          {
            VideoCaptureDevice VCD = new VideoCaptureDevice(CameraIdentifiers[II]);
            CameraResolutionWidths[II] = new List<Int32>();
            CameraResolutionHeights[II] = new List<Int32>();
            CameraFrameRates[II] = new List<Int32>();
            Int32 VCL = VCD.VideoCapabilities.Length;
            if (0 < VCL)
            {
              Int32 VRMaximumIndex = 0;
              Int32 VRWidthMaximum = 0;
              Int32 VRHeightMaximum = 0;
              for (Int32 VCI = 0; VCI < VCL; VCI++)
              {
                Int32 VRWidth = VCD.VideoCapabilities[VCI].FrameSize.Width;
                CameraResolutionWidths[II].Add(VRWidth);
                Int32 VRHeight = VCD.VideoCapabilities[VCI].FrameSize.Height;
                CameraResolutionHeights[II].Add(VRHeight);
                Int32 VRFrameRate = VCD.VideoCapabilities[VCI].AverageFrameRate;
                CameraFrameRates[II].Add(VRFrameRate);
                if ((VRWidthMaximum < VRWidth) && (VRHeightMaximum < VRHeight))
                {
                  VRWidthMaximum = VRWidth;
                  VRHeightMaximum = VRHeight;
                  VRMaximumIndex = VCI;
                }
              }
              // Check if this desired Camera is in refreshed list:
              for (Int32 CI = 0; CI < CountCameras; CI++)
              {
                if (FGrabberData.CameraName == CameraNames[CI])
                {
                  FCameraMoniker = CameraIdentifiers[CI];
                  FVideoCaptureDevice = new VideoCaptureDevice(FCameraMoniker);
                  if (FVideoCaptureDevice is VideoCaptureDevice)
                  {
                    FCameraResolutionWidths = CameraResolutionWidths;
                    FCameraResolutionHeights = CameraResolutionHeights;
                    FCameraFrameRates = CameraFrameRates;
                    // default highest Resolution / FrameRate:
                    FIndexResolutionFrameRate = VRMaximumIndex;
                    Info(FGrabberData.LibraryName, "Open - true");
                    if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
                    {
                      FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
                    }
                    return true;
                  }
                }
              }
            }
          }
        }
      }
      Info(FGrabberData.LibraryName, "Open - false");
      if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
      {
        FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
      }
      return false;
    }

    public override Boolean Close()
    {
      if (IsCameraOpen())
      {
        StopAcquisition();
        FIsCameraBusy = false;
        FVideoCaptureDevice = null;
        FCameraMoniker = "";
        FCameraResolutionWidths = null;
        FCameraResolutionHeights = null;
        FCameraFrameRates = null;
        FIndexResolutionFrameRate = -1;
        Info(FGrabberData.LibraryName, "Close - true");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return true;
      }
      Info(FGrabberData.LibraryName, "Close - false");
      if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
      {
        FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
      }
      return false;
    }

    public override Boolean StartAcquisition()//RAcquisitionData acquisitiondata)
    {
      try
      {
        if (IsCameraOpen())
        {
          if (!IsCameraBusy())
          {
            // debug FGrabberData.PaletteKind = EPaletteKind.RedSectionBright;
            FVideoCaptureDevice.VideoResolution = FVideoCaptureDevice.VideoCapabilities[0];
            // RO FVideoCaptureDevice.VideoResolution.FrameRate;
//!!!!!!!!!!!!####            FBitmap = new CBitmapRgb(FVideoCaptureDevice.VideoResolution.FrameSize.Width,
            //!!!!!!!!!!!!####FVideoCaptureDevice.VideoResolution.FrameSize.Height);
            FIsCameraBusy = true;
            FVideoCaptureDevice.NewFrame += new AForge.Video.NewFrameEventHandler(OnNewFrame);
            FVideoCaptureDevice.Start();
            Info(FGrabberData.LibraryName, "StartAcquisition - true");
            if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
            {
              FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
            }
            return true;
          }
        }
        Info(FGrabberData.LibraryName, "StartAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
      catch (Exception)
      {
        Info(FGrabberData.LibraryName, "StartAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
    }

    public override Boolean StopAcquisition()
    {
      try
      {
        if (IsCameraOpen())
        {
          if (IsCameraBusy())
          {
            FVideoCaptureDevice.SignalToStop();
            FIsCameraBusy = false;
            FVideoCaptureDevice.NewFrame -= new AForge.Video.NewFrameEventHandler(OnNewFrame);
            Info(FGrabberData.LibraryName, "StopAcquisition - true");
            if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
            {
              FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
            }
            return true;
          }
        }
        Info(FGrabberData.LibraryName, "StopAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
      catch (Exception)
      {
        Info(FGrabberData.LibraryName, "StopAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
    }


    public override Boolean SetExposure(Int32 value)
    {
      if (IsCameraOpen())
      {
        FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Focus, value, CameraControlFlags.Manual);
        FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Exposure, value, CameraControlFlags.Manual);
        FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Zoom, value, CameraControlFlags.Manual);
        FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Iris, value, CameraControlFlags.Manual);
        FVideoCaptureDevice.SetCameraProperty(CameraControlProperty.Roll, value, CameraControlFlags.Manual);
        //
        //FVideoCaptureDevice.SetVideoProperty(VideoProcAmpProperty.Brightness,
        //    brightnessValue,
        //    VideoProcAmpFlags.Manual);
        return true;
      }
      return false;
    }

    public override Boolean ShowCameraProperties(IntPtr hparent)
    {
      if (FVideoCaptureDevice is VideoCaptureDevice)
      {
        FVideoCaptureDevice.DisplayPropertyPage(hparent);
        return true;
      }
      return false;
    }


  }
}
