﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Drawing;
//
using FlyCapture2Managed;
using FlyCapture2Managed.Gui;
//
using Task;
using IPPalette256;
using IPMemory;
using GrabberHardware;
//
namespace GHPointGreyFlea3
{
  public class CGHPointGreyFlea3 : CGrabberHardware
  {
    //
    //--------------------------------------------
    //	Section - Constant
    //--------------------------------------------
    //    
    //
    //---------------------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------------------
    //
    private FlyCapture2Managed.Gui.CameraControlDialog FDialogCameraProperties;
    private ManagedCameraBase FCamera = null;
    private ManagedImage FImageRawData;
    private ManagedImage FImageBitmap;
    private CTask FTask;
    //
    //--------------------------------------------
    //	Section - Constructor
    //--------------------------------------------
    //
    public CGHPointGreyFlea3()
      : base()
    {
      Int32 CI = (int)CDefinition.ECameraIndex.Flea3;
      FGrabberData.LibraryName = CDefinition.LIBRARY_NAMES[CI];
      FGrabberData.EntryName = CDefinition.ENTRY_NAMES[CI];
      FGrabberData.CameraName = CDefinition.CAMERA_NAMES[CI];
      FImageRawData = new ManagedImage();
      FImageBitmap = new ManagedImage();
      FDialogCameraProperties = new CameraControlDialog();
    }
    //
    //--------------------------------------------
    //	Section - Property
    //--------------------------------------------
    //
    public override Boolean IsCameraOpen()
    {
      return (FCamera is ManagedCamera);
    }

    public override Boolean IsCameraBusy()
    {
      return (FTask is CTask);
    }

    public override void SetOnCameraStateChanged(DOnCameraStateChanged value)
    {
      base.SetOnCameraStateChanged(value);
    }
    //
    //--------------------------------------------
    //	Section - Callback - Acquisition
    //--------------------------------------------
    //
    private void OnNewFrame()
    {
      try
      {
        Bitmap BitmapSource24bpp = (Bitmap)FImageBitmap.bitmap.Clone();
        Int32 BW = BitmapSource24bpp.Width;
        Int32 BH = BitmapSource24bpp.Height;
        //
        if (FGrabberData.TrueColorMode)
        { // Bitmap24bpp -> BitmapRgb
          //if (FGrabberData.OnNewFrameBitmapRgb is DOnNewFrameBitmapRgb)
          //{
          //  CBitmapRgb BS = new CBitmapRgb(BitmapSource24bpp);
          //  FGrabberData.OnNewFrameBitmapRgb(BS);
          //}
          // Bitmap24bpp -> BitmapArgb
          //if (FGrabberData.OnNewFrameBitmapArgb is DOnNewFrameBitmapArgb)
          //{
          //  CBitmapRgb BS = new CBitmapArgb(BitmapSource24bpp, 100);
          //  FGrabberData.OnNewFrameBitmapRgb(BS);
          //}
        }
      }
      catch (Exception)
      {
      }
    }
    //
    //--------------------------------------------
    //	Section - Callback - Task
    //--------------------------------------------
    //
    private void GrabOnExecutionStart(RTaskData taskdata)
    {
    }
    private void GrabOnExecutionBusy(RTaskData taskdata)
    {
      while (!taskdata.IsAborted)
      {
        FCamera.RetrieveBuffer(FImageRawData);
        lock (this)
        {
          FImageRawData.Convert(PixelFormat.PixelFormatBgr, FImageBitmap);
          // 24bpp FImageRawData.Convert(PixelFormat.PixelFormatRgb, FImageBitmap);
          // 32bpp FImageRawData.Convert(PixelFormat.PixelFormatRgbu, FImageBitmap);
          OnNewFrame();
        }
      }
    }
    private void GrabOnExecutionEnd(RTaskData taskdata)
    { // never...
    }
    private void GrabOnExecutionAbort(RTaskData taskdata)
    {
    }
    //
    //--------------------------------------------
    //	Section - Management - Camera
    //--------------------------------------------
    //
    public override Boolean Open(DOnErrorGrabberHardware onerrorgrabberhardware,
                                 DOnInfoGrabberHardware oninfograbberhardware)
    {
      FGrabberData.OnErrorGrabberHardware = onerrorgrabberhardware;
      FGrabberData.OnInfoGrabberHardware = oninfograbberhardware;
      //
      if (IsCameraOpen())
      {
        Console.WriteLine("GHPointGreyFlea3: Open - false");
        return false;
      }
      try
      {
        ManagedBusManager busMgr = new ManagedBusManager();
        ManagedPGRGuid MPGRID = busMgr.GetCameraFromIndex(0);
        FCamera = new ManagedCamera();
        FCamera.Connect(MPGRID);
        FDialogCameraProperties.Connect(FCamera);
        CameraInfo camInfo = FCamera.GetCameraInfo();
        Console.WriteLine("GHPointGreyFlea3: Open - true");
        return true;
      }
      catch (Exception)
      {
        Console.WriteLine("Error: Failed to open GHPointGreyFlea3!");
        return false;
      }
    }

      
      //try
      //{
      //FImageBitmap = new ManagedImage();
      //FDialogCameraProperties = new CameraControlDialog();

    //    ManagedBusManager busMgr = new ManagedBusManager();
    //    ManagedPGRGuid MPGRID = busMgr.GetCameraFromIndex(0);
    //    FCamera = new ManagedCamera();
    //    FCamera.Connect(MPGRID);
    //    FDialogCameraProperties.Connect(FCamera);
    //    CameraInfo camInfo = FCamera.GetCameraInfo();
    //    Console.WriteLine("GHPointGreyFlea3: Open - true");
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    Console.WriteLine("Error: Failed to open GHPointGreyFlea3!");
    //    return false;
    //  }
    //}
    //// Refresh installed Usb-Cameras with Resolutions
    ////List<Int32>[] CameraResolutionWidths = new List<Int32>[CountCameras];
    ////List<Int32>[] CameraResolutionHeights = new List<Int32>[CountCameras];
    ////List<Int32>[] CameraFrameRates = new List<Int32>[CountCameras];
    ////VideoCaptureDevice VCD = new VideoCaptureDevice(CameraMonikers[II]);
    ////CameraResolutionWidths[II] = new List<Int32>();
    ////CameraResolutionHeights[II] = new List<Int32>();
    ////CameraFrameRates[II] = new List<Int32>();
    ////for (Int32 VCI = 0; VCI < VCL; VCI++)
    ////{
    ////  Int32 VRWidth = VCD.VideoCapabilities[VCI].FrameSize.Width;
    ////  CameraResolutionWidths[II].Add(VRWidth);
    ////  Int32 VRHeight = VCD.VideoCapabilities[VCI].FrameSize.Height;
    ////  CameraResolutionHeights[II].Add(VRHeight);
    ////  Int32 VRFrameRate = VCD.VideoCapabilities[VCI].AverageFrameRate;
    ////  CameraFrameRates[II].Add(VRFrameRate);
    ////}
    //// Check if this desired Camera is in refreshed list:
    ////Console.WriteLine("GHWebUsb20PcCamera: Open - true");
    ////if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
    ////{
    ////  FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
    ////}
    ////return true;
    ////  Console.WriteLine("GHWebUsb20PcCamera: Open - false");
    ////  if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
    ////  {
    ////    FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
      //}

    public override Boolean Close()
    {
      if (!IsCameraOpen())
      {
        Console.WriteLine("GHPointGreyFlea3: Close - false");
        return false;
      }
      try
      {
        if (IsCameraBusy())
        {
          StopAcquisition();
        }
        FCamera.Disconnect();
        FCamera = null;
        Console.WriteLine("GHPointGreyFlea3: Close - true");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return true;
      }
      catch (Exception)
      {
        Console.WriteLine("Error: Failed to close GHPointGreyFlea3!");
        return false;
      }
    }
    //    StopAcquisition();
    //    FIsCameraBusy = false;
    //    //!!!!FCameraMoniker = "";
    //    //!!!!FCameraResolutionWidths = null;
    //    //!!!!FCameraResolutionHeights = null;
    //    //!!!!FCameraFrameRates = null;
    //    //!!!!FIndexResolutionFrameRate = -1;
    //    Console.WriteLine("GHWebUsb20PcCamera: Close - true");
    //    if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
    //    {
    //      FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
    //    }
    //    return true;
    //  }
    //  Console.WriteLine("GHWebUsb20PcCamera: Close - false");
    //  if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
    //  {
    //    FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
    //  }
    //  return false;
    //}

    public override Boolean StartAcquisition()
    {
      try
      {
        if (IsCameraOpen())
        {
          if (!IsCameraBusy())
          {
            // Idle -> Busy
            FCamera.StartCapture();
            //
            FTask = new CTask("Grab",
                              GrabOnExecutionStart, GrabOnExecutionBusy,
                              GrabOnExecutionEnd, GrabOnExecutionAbort);
            FTask.Start();
            Console.WriteLine("GHPointGreyFlea3: StartAcquisition - true");
            if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
            {
              FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
            }
            return true;
          }
        }
        Console.WriteLine("GHPointGreyFlea3: StartAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
      catch (Exception)
      {
        Console.WriteLine("GHPointGreyFlea3: StartAcquisition - false");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
    }

    public override Boolean StopAcquisition()
    {
      try
      {
        if (IsCameraOpen())
        {
          if (IsCameraBusy())
          { // Busy -> Idle
            FTask.Abort();
            FTask = null;
            FCamera.StopCapture();
            Console.WriteLine("GHWebUsb20PcCamera: StopAcquisition - true");
            if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
            {
              FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception)
      {
        Debug.WriteLine("Failed to stop camera!");
        if (FGrabberData.OnCameraStateChanged is DOnCameraStateChanged)
        {
          FGrabberData.OnCameraStateChanged(IsCameraOpen(), IsCameraBusy());
        }
        return false;
      }
    }


    public override Boolean SetExposure(Int32 value)
    {
      //if (IsCameraOpen())
      //{
      //  return true;
      //}
      return false;
    }

    public override Boolean ShowCameraProperties(IntPtr hparent)
    {
      return false;
    }




  }
}
