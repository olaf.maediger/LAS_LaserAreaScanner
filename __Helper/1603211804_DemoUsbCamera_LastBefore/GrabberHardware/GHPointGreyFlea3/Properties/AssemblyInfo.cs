﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("GHPointGreyFlea3")]
[assembly: AssemblyDescription("1601111220")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("LLG")]
[assembly: AssemblyProduct("GHPointGreyFlea3")]
[assembly: AssemblyCopyright("Copyright © LLG 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6f154b68-72a9-4fec-a11b-44698fe6aa56")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.2.1.*")]
[assembly: AssemblyFileVersion("1.2.1")]
