﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using NLRealPixel;
using NLParametricCurve;
using NLViewParametric;
using UCViewParametric;
//
namespace UCViewEnvelope 
{
  public partial class CUCViewEnvelope : CUCViewParametric
  {
   //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCViewEnvelope()
    {
      InitializeComponent();
      //
      base.ForceDock();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private new void ForceRedraw()
    {
      base.ForceRedraw();
    }

    public void ExampleEnvelopeNormSection()
    {
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      ViewParametric.RealPixel.XRealMinimum = -10;
      ViewParametric.RealPixel.YRealMinimum = -10;
      ViewParametric.RealPixel.XRealMaximum = +10;
      ViewParametric.RealPixel.YRealMaximum = +10;
      //
      ViewParametric.ParametricList.Clear();
      //
      //----------------------------------------------
      // Example Intersection EnvelopeNorm - Section
      //----------------------------------------------
      //
      CEnvelopeNorm Envelope = new CEnvelopeNorm(ViewParametric.RealPixel);
      Envelope.CountResolution = 17;
      Envelope.X0 = 1; Envelope.Y0 = 0;
      Envelope.X1 = -1; Envelope.Y1 = 0;
      Envelope.BuildRealVector();
      ViewParametric.ParametricList.Add(Envelope);
      //
      CSection Section = new CSection(ViewParametric.RealPixel);
      Section.CountResolution = 15;
      Section.X0 = -9; Section.Y0 = -2;
      Section.X1 = 9; Section.Y1 = 7;
      Section.BuildRealVector();
      ViewParametric.ParametricList.Add(Section);
      // 
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.EnvelopeNormSectionIterative(Envelope, Section,
                                                    0.001,
                                                    out SS, out TS,
                                                    out XS, out YS))
      {
        CCircle CircleSection = new CCircle(ViewParametric.RealPixel);
        CircleSection.CountResolution = 12;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.2;
        CircleSection.BuildRealVector();
        ViewParametric.ParametricList.Add(CircleSection);
      }
    }

    public void ExampleEnvelopeFreeSection()
    {
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      ViewParametric.RealPixel.XRealMinimum = -10;
      ViewParametric.RealPixel.YRealMinimum = -10;
      ViewParametric.RealPixel.XRealMaximum = +10;
      ViewParametric.RealPixel.YRealMaximum = +10;
      //
      ViewParametric.ParametricList.Clear();
      //
      //----------------------------------------------
      // Example Intersection EnvelopeFree - Section
      //----------------------------------------------
      //
      CCircle CircleBase = new CCircle(ViewParametric.RealPixel);
      CircleBase.CountResolution = 12;
      CircleBase.XC = 0; CircleBase.YC = 0;
      CircleBase.R = 1;
      CircleBase.BuildRealVector();
      ViewParametric.ParametricList.Add(CircleBase);
      //
      CEnvelopeFree Envelope = new CEnvelopeFree(ViewParametric.RealPixel);
      Envelope.CountResolution = 17;
      Envelope.R = 1;
      Envelope.XC = 0; Envelope.YC = 0;
      Envelope.BuildRealVector();
      ViewParametric.ParametricList.Add(Envelope);
      //
      CSection Section = new CSection(ViewParametric.RealPixel);
      Section.CountResolution = 15;
      Section.X0 = -9; Section.Y0 = -2;
      Section.X1 = 9; Section.Y1 = 7;
      Section.BuildRealVector();
      ViewParametric.ParametricList.Add(Section);
      // 
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.EnvelopeFreeSectionIterative(Envelope, Section,
                                                    0.001, 20,
                                                    out SS, out TS,
                                                    out XS, out YS))
      {
        CCircle CircleSection = new CCircle(ViewParametric.RealPixel);
        CircleSection.CountResolution = 12;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.2;
        CircleSection.BuildRealVector();
        ViewParametric.ParametricList.Add(CircleSection);
        Console.WriteLine(String.Format("XS={0} YS={1}", XS, YS));
      }
    }

    public void ExampleEnvelopeFreeCircle()
    {
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      ViewParametric.RealPixel.XRealMinimum = -3;
      ViewParametric.RealPixel.YRealMinimum = -3;
      ViewParametric.RealPixel.XRealMaximum = +3;
      ViewParametric.RealPixel.YRealMaximum = +3;
      //
      ViewParametric.ParametricList.Clear();
      //
      //----------------------------------------------
      // Example Intersection EnvelopeFree - Section
      //----------------------------------------------
      //
      CCircle CircleBase = new CCircle(ViewParametric.RealPixel);
      CircleBase.CountResolution = 32;
      CircleBase.XC = 0; CircleBase.YC = 0;
      CircleBase.R = 1;
      CircleBase.BuildRealVector();
      ViewParametric.ParametricList.Add(CircleBase);
      //
      CEnvelopeFree Envelope = new CEnvelopeFree(ViewParametric.RealPixel);
      Envelope.CountResolution = 64;
      Envelope.R = 1;
      Envelope.XC = 0; Envelope.YC = 0;
      Envelope.BuildRealVector();
      ViewParametric.ParametricList.Add(Envelope);
      //
      CCircle Circle = new CCircle(ViewParametric.RealPixel);
      Circle.CountResolution = 64;
      Circle.XC = 0.00; Circle.YC = 0;
      Circle.R = 1.8; 
      Circle.BuildRealVector();
      ViewParametric.ParametricList.Add(Circle);
      // 
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.EnvelopeFreeCircleIterative(Envelope, 0.001, 0.7, 10,                                                  
                                                   Circle, 0.025, 0.7, 10,                                                   
                                                   0.001, 100,
                                                   out SS, out TS,
                                                   out XS, out YS))
      {
        CCircle CircleSection = new CCircle(ViewParametric.RealPixel);
        CircleSection.CountResolution = 12;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.2;
        CircleSection.BuildRealVector();
        ViewParametric.ParametricList.Add(CircleSection);
        Console.WriteLine(String.Format("XS={0} YS={1}", XS, YS));
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Menu
    //--------------------------------------------------------------------------
    //
    private void btnExampleEnvelopeNormSection_Click(object sender, EventArgs e)
    {
      ExampleEnvelopeFreeSection();
      ForceRedraw();
    }

    private void btnExampleEnvelopeFreeCircle_Click(object sender, EventArgs e)
    {
      ExampleEnvelopeFreeCircle();
      ForceRedraw();
    }



  }
}
