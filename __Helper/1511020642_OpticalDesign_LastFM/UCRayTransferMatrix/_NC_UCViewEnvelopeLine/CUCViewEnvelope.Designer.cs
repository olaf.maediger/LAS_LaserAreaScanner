﻿namespace UCViewEnvelope
{
  partial class CUCViewEnvelope
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlTop = new System.Windows.Forms.Panel();
      this.btnExampleSectionSection = new System.Windows.Forms.Button();
      this.btnExampleEnvelopeFreeCircle = new System.Windows.Forms.Button();
      this.btnExampleEnvelopeNormSection = new System.Windows.Forms.Button();
      this.pnlTop.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlTop
      // 
      this.pnlTop.Controls.Add(this.btnExampleSectionSection);
      this.pnlTop.Controls.Add(this.btnExampleEnvelopeFreeCircle);
      this.pnlTop.Controls.Add(this.btnExampleEnvelopeNormSection);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Margin = new System.Windows.Forms.Padding(7);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(1409, 67);
      this.pnlTop.TabIndex = 16;
      // 
      // btnExampleSectionSection
      // 
      this.btnExampleSectionSection.Location = new System.Drawing.Point(628, 7);
      this.btnExampleSectionSection.Margin = new System.Windows.Forms.Padding(7);
      this.btnExampleSectionSection.Name = "btnExampleSectionSection";
      this.btnExampleSectionSection.Size = new System.Drawing.Size(296, 51);
      this.btnExampleSectionSection.TabIndex = 2;
      this.btnExampleSectionSection.Text = "....";
      this.btnExampleSectionSection.UseVisualStyleBackColor = true;
      // 
      // btnExampleEnvelopeFreeCircle
      // 
      this.btnExampleEnvelopeFreeCircle.Location = new System.Drawing.Point(317, 7);
      this.btnExampleEnvelopeFreeCircle.Margin = new System.Windows.Forms.Padding(7);
      this.btnExampleEnvelopeFreeCircle.Name = "btnExampleEnvelopeFreeCircle";
      this.btnExampleEnvelopeFreeCircle.Size = new System.Drawing.Size(296, 51);
      this.btnExampleEnvelopeFreeCircle.TabIndex = 1;
      this.btnExampleEnvelopeFreeCircle.Text = "EnvelopeFree-Circle";
      this.btnExampleEnvelopeFreeCircle.UseVisualStyleBackColor = true;
      this.btnExampleEnvelopeFreeCircle.Click += new System.EventHandler(this.btnExampleEnvelopeFreeCircle_Click);
      // 
      // btnExampleEnvelopeNormSection
      // 
      this.btnExampleEnvelopeNormSection.Location = new System.Drawing.Point(7, 7);
      this.btnExampleEnvelopeNormSection.Margin = new System.Windows.Forms.Padding(7);
      this.btnExampleEnvelopeNormSection.Name = "btnExampleEnvelopeNormSection";
      this.btnExampleEnvelopeNormSection.Size = new System.Drawing.Size(296, 51);
      this.btnExampleEnvelopeNormSection.TabIndex = 0;
      this.btnExampleEnvelopeNormSection.Text = "EnvelopeNorm-Section";
      this.btnExampleEnvelopeNormSection.UseVisualStyleBackColor = true;
      this.btnExampleEnvelopeNormSection.Click += new System.EventHandler(this.btnExampleEnvelopeNormSection_Click);
      // 
      // CUCViewEnvelope
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlTop);
      this.Margin = new System.Windows.Forms.Padding(16);
      this.Name = "CUCViewEnvelope";
      this.Size = new System.Drawing.Size(1409, 1258);
      this.Controls.SetChildIndex(this.pnlTop, 0);
      this.pnlTop.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.Button btnExampleSectionSection;
    private System.Windows.Forms.Button btnExampleEnvelopeFreeCircle;
    private System.Windows.Forms.Button btnExampleEnvelopeNormSection;
  }
}
