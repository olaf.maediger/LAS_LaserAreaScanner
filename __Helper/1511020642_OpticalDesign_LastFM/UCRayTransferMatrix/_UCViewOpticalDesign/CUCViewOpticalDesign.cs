﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using NLRealPixel;
using NLRayTransferMatrix;
//
namespace UCViewOpticalDesign
{
  public partial class CUCViewOpticalDesign : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private CRealPixel FRealPixel;
    private CComponentList FComponentList;
    private CRayList FRayList;

    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCViewOpticalDesign()
    {
      InitializeComponent();
      //
      FRealPixel = new CRealPixel();
      FComponentList = new CComponentList();
      FRayList = new CRayList();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void ForceDock()
    {
      //pbxView.Dock = DockStyle.Fill;
    }

    //public CViewParametric ViewParametric
    //{
    //  get { return FViewParametric; }
    //}
    //public CRealPixel RealPixel
    //{
    //  get { return FViewParametric.RealPixel; }
    //}
    //public CParametricList ParametricList
    //{
    //  get { return FViewParametric.ParametricList; }
    //}
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private void ThisRedraw(Graphics graphics)
    {
      if (FRealPixel is CRealPixel)
      {
        Brush B = new SolidBrush(Color.FromArgb(0xFF, 0xEE, 0xEE, 0xAA));
        //graphics.FillRectangle(B, pbxView.ClientRectangle);
        //FRealPixel.Redraw(graphics);
      }
    }

    private Boolean CalculateComponentMatrix()
    {
      try
      {
        if ( 0 < FComponentList.Count)
        {
          CMatrix22 Result = FComponentList[0].Matrix22;
          for (Int32 CI = 0; CI < FComponentList.Count; CI++)
          {
            CComponent Component = FComponentList[CI];
            Result.Multiply(Component);
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //
    private void pbxView_Paint(object sender, PaintEventArgs e)
    {
      ThisRedraw(e.Graphics);
    }

    private void pbxView_Resize(object sender, EventArgs e)
    {
      if (FRealPixel is CRealPixel)
      {
        //FRealPixel.SetPixelExtrema(pbxView.Width, pbxView.Height);
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public
    //--------------------------------------------------------------------------
    //
    //public void ForceRedraw()
    //{
    //  pbxView_Resize(this, null);
    //  Graphics G = pbxView.CreateGraphics();
    //  ThisRedraw(G);
    //}

    public Boolean AddTranslation(Double distance, Double refractionindex)
    {
      try
      {
        CTranslation Translation = new CTranslation(distance, refractionindex);
        FComponentList.Add(Translation);
        CalculateComponentMatrix();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean AddMirrorPlane()
    {
      try
      {
        CMirrorPlane MirrorPlane = new CMirrorPlane();
        FComponentList.Add(MirrorPlane);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    

  }
}
