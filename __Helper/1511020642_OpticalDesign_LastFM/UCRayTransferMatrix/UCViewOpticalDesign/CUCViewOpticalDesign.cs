﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using NLRealPixel;
using NLParametricCurve;
using NLRayTransferMatrix;
//
namespace UCViewOpticalDesign
{  
  public partial class CUCViewOpticalDesign : UserControl
  { //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private CRealPixel FRealPixel;
    private CComponentList FComponentList;
    private CRayPool FRayPool;
    private CParametricList FParametricList;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCViewOpticalDesign()
    {
      InitializeComponent();
      //
      FRealPixel = new CRealPixel();
      FComponentList = new CComponentList();
      FRayPool = new CRayPool();
      FParametricList = new CParametricList(FRealPixel);
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public CRealPixel RealPixel
    {
      get { return FRealPixel; }
    }
    public CComponentList ComponentList
    {
      get { return FComponentList; }
    }
    public CRayPool RayPool
    {
      get { return FRayPool; }
    }
    public CParametricList GraphicList
    {
      get { return FParametricList; }
    }      
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private void ThisRedraw(Graphics graphics)
    {
      if (FRealPixel is CRealPixel)
      {
        Brush B = new SolidBrush(Color.FromArgb(0xFF, 0xF0, 0xF0, 0xFF));
        graphics.FillRectangle(B, this.ClientRectangle);
        FComponentList.Draw(graphics);
        FRayPool.Draw(graphics, FRealPixel);
        FParametricList.Draw(graphics);

      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //
    private void ThisPaint(object sender, PaintEventArgs e)
    {
      ThisRedraw(e.Graphics);
    }

    private void ThisResize(object sender, EventArgs e)
    {
      if (FRealPixel is CRealPixel)
      {
        FRealPixel.SetPixelExtrema(ClientRectangle.Width, ClientRectangle.Height);
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public
    //--------------------------------------------------------------------------
    //
    public void ForceRedraw()
    {
      ThisResize(this, null);
      Graphics G = this.CreateGraphics();
      ThisRedraw(G);
    }

    public CLine AddLine(Double rx0, Double ry0,
                         Double rx1, Double ry1)
    {
      CLine Line = new CLine(FRealPixel, rx0, ry0, rx1, ry1);
      FParametricList.Add(Line);
      return Line;
    }

    public CSegment AddSegment(Double rx0, Double ry0,
                               Double rx1, Double ry1)
    {
      CSegment Segment = new CSegment(FRealPixel, rx0, ry0, rx1, ry1);
      FParametricList.Add(Segment);
      return Segment;
    }

    public CSegment AddSegment(Double rx0, Double ry0,
                               Double rx1, Double ry1,
                               Int32 countresolution)
    {
      CSegment Segment = new CSegment(FRealPixel, rx0, ry0, rx1, ry1, countresolution);
      FParametricList.Add(Segment);
      return Segment;
    }

    public Boolean AddCircle(Double rxm, Double rym,
                             Double rr,
                             Int32 countresolution)
    {
      CCircle Circle = new CCircle(FRealPixel, rxm, rym, rr, countresolution);
      FParametricList.Add(Circle);
      return true;
    }

    public Boolean AddCircle(Double rxm, Double rym,
                             Double rx0, Double ry0,
                             Int32 countresolution)
    {
      CCircle Circle = new CCircle(FRealPixel, rxm, rym, rx0, ry0, countresolution);
      FParametricList.Add(Circle);
      return true;
    }

    public CArc AddArc(Double rxm, Double rym,
                       Double rx0, Double ry0,
                       Double rx1, Double ry1)
    {
      CArc Arc = new CArc(FRealPixel, rxm, rym, rx0, ry0, rx1, ry1);
      FParametricList.Add(Arc);
      return Arc;
    }

    public CArc AddArc(Double rxm, Double rym,
                       Double rx0, Double ry0,
                       Double rx1, Double ry1,
                       Int32 countresolution)
    {
      CArc Arc = new CArc(FRealPixel, rxm, rym, rx0, ry0, rx1, ry1, countresolution);
      FParametricList.Add(Arc);
      return Arc;
    }

    public Boolean AddTranslation(Double positionx, Double width,
                                  Double[] fractionindices)
    {
      try
      {
        CTranslation Translation = new CTranslation(FRealPixel, positionx, width, fractionindices);
        FComponentList.Add(Translation);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean AddPlateParallel(Double positionx, Double positiony,
                                    Double width, Double height, 
                                    Double[] fractionindices)
    {
      try
      {
        CPlateParallel PlateParallel = new CPlateParallel(FRealPixel, positionx, positiony,
                                                          width, height, fractionindices);
        FComponentList.Add(PlateParallel);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean AddMirrorPlane(Double positionx, Double positiony,
                                  Double width, Double height, 
                                  Double[] fractionindices)
    {
      try
      {
        CMirrorPlane MirrorPlane = new CMirrorPlane(FRealPixel, positionx, positiony,
                                                    width, height, fractionindices);
        FComponentList.Add(MirrorPlane);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean AddRay(Double positionx, Double positiony, Double angle) // [mm], [deg]
    {
      try
      {
        return FRayPool.AddRay(FRealPixel, positionx, positiony, angle);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean CalculateRays()
    {
      return FRayPool.CalculateRays(FComponentList);
    }


  }
}
