﻿namespace UCViewLine
{
  partial class CUCViewLine
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlTop = new System.Windows.Forms.Panel();
      this.btnExampleSectionSection = new System.Windows.Forms.Button();
      this.btnExampleSectionLine = new System.Windows.Forms.Button();
      this.btnExampleLineLine = new System.Windows.Forms.Button();
      this.pnlTop.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlTop
      // 
      this.pnlTop.Controls.Add(this.btnExampleSectionSection);
      this.pnlTop.Controls.Add(this.btnExampleSectionLine);
      this.pnlTop.Controls.Add(this.btnExampleLineLine);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(406, 30);
      this.pnlTop.TabIndex = 16;
      // 
      // btnExampleSectionSection
      // 
      this.btnExampleSectionSection.Location = new System.Drawing.Point(269, 3);
      this.btnExampleSectionSection.Name = "btnExampleSectionSection";
      this.btnExampleSectionSection.Size = new System.Drawing.Size(127, 23);
      this.btnExampleSectionSection.TabIndex = 2;
      this.btnExampleSectionSection.Text = "ExampleSectionSection";
      this.btnExampleSectionSection.UseVisualStyleBackColor = true;
      this.btnExampleSectionSection.Click += new System.EventHandler(this.btnExampleSectionSection_Click);
      // 
      // btnExampleSectionLine
      // 
      this.btnExampleSectionLine.Location = new System.Drawing.Point(136, 3);
      this.btnExampleSectionLine.Name = "btnExampleSectionLine";
      this.btnExampleSectionLine.Size = new System.Drawing.Size(127, 23);
      this.btnExampleSectionLine.TabIndex = 1;
      this.btnExampleSectionLine.Text = "ExampleSectionLine";
      this.btnExampleSectionLine.UseVisualStyleBackColor = true;
      this.btnExampleSectionLine.Click += new System.EventHandler(this.btnExampleSectionLine_Click);
      // 
      // btnExampleLineLine
      // 
      this.btnExampleLineLine.Location = new System.Drawing.Point(3, 3);
      this.btnExampleLineLine.Name = "btnExampleLineLine";
      this.btnExampleLineLine.Size = new System.Drawing.Size(127, 23);
      this.btnExampleLineLine.TabIndex = 0;
      this.btnExampleLineLine.Text = "ExampleLineLine";
      this.btnExampleLineLine.UseVisualStyleBackColor = true;
      this.btnExampleLineLine.Click += new System.EventHandler(this.btnExampleLineLine_Click);
      // 
      // CUCViewLine
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlTop);
      this.Name = "CUCViewLine";
      this.Size = new System.Drawing.Size(604, 564);
      this.Controls.SetChildIndex(this.pnlTop, 1); // not zero!!!
      this.pnlTop.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    #endregion

    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.Button btnExampleSectionSection;
    private System.Windows.Forms.Button btnExampleSectionLine;
    private System.Windows.Forms.Button btnExampleLineLine;
  }
}
