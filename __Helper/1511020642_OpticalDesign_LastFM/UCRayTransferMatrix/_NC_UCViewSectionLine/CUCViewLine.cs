﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using NLRealPixel;
using NLParametricCurve;
using NLViewParametric;
using UCViewParametric;
//
namespace UCViewLine
{
  public partial class CUCViewLine : CUCViewParametric
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCViewLine()
    {
      InitializeComponent();
      //
      base.ForceDock();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    private new void ForceRedraw()
    {
      base.ForceRedraw();
    }

    public void ExampleSectionSection()
    {
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      RealPixel.XRealMinimum = 0;
      RealPixel.YRealMinimum = 0;
      RealPixel.XRealMaximum = +10;
      RealPixel.YRealMaximum = +10;
      //
      ParametricList.Clear();
      //
      //----------------------------------------------
      // Example Intersection Section - Section
      //----------------------------------------------
      //
      CSection Section0 = new CSection(RealPixel);
      Section0.CountResolution = 7;
      Section0.X0 = 1; Section0.Y0 = 1;
      Section0.X1 = 9; Section0.Y1 = 9;
      Section0.BuildRealVector();
      ParametricList.Add(Section0);
      //
      CSection Section1 = new CSection(RealPixel);
      Section1.CountResolution = 5;
      Section1.X0 = 1; Section1.Y0 = 9;
      Section1.X1 = 9; Section1.Y1 = 1;
      Section1.BuildRealVector();
      ParametricList.Add(Section1);
      // 
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.SectionSectionIterative(Section0, Section1,
                                               0.001,
                                               out SS, out TS,
                                               out XS, out YS))
      {
        CCircle CircleSection = new CCircle(RealPixel);
        CircleSection.CountResolution = 12;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.2;
        CircleSection.BuildRealVector();
        ParametricList.Add(CircleSection);
      }
    }

    public void ExampleSectionLine()
    {
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      ViewParametric.RealPixel.XRealMinimum = 0;
      ViewParametric.RealPixel.YRealMinimum = 0;
      ViewParametric.RealPixel.XRealMaximum = +10;
      ViewParametric.RealPixel.YRealMaximum = +10;
      //
      ViewParametric.ParametricList.Clear();
      //
      //----------------------------------------------
      // Example Intersection Section - Line
      //----------------------------------------------
      //
      CSection Section = new CSection(ViewParametric.RealPixel);
      Section.CountResolution = 17;
      Section.X0 = 1; Section.Y0 = 5;
      Section.X1 = 9; Section.Y1 = 5;
      Section.BuildRealVector();
      ViewParametric.ParametricList.Add(Section);
      //
      CLine Line = new CLine(ViewParametric.RealPixel);
      Line.CountResolution = 15;
      Line.X0 = 1; Line.Y0 = 2;
      Line.X1 = 9; Line.Y1 = 7;
      Line.BuildRealVector();
      ViewParametric.ParametricList.Add(Line);
      // 
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.SectionLineIterative(Section, Line,
                                            0.001,
                                            out SS, out TS,
                                            out XS, out YS))
      {
        CCircle CircleSection = new CCircle(ViewParametric.RealPixel);
        CircleSection.CountResolution = 12;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.2;
        CircleSection.BuildRealVector();
        ViewParametric.ParametricList.Add(CircleSection);
      }
    }

    public void ExampleLineLine()
    {
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      ViewParametric.RealPixel.XRealMinimum = 0;
      ViewParametric.RealPixel.YRealMinimum = 0;
      ViewParametric.RealPixel.XRealMaximum = +10;
      ViewParametric.RealPixel.YRealMaximum = +10;
      //
      ViewParametric.ParametricList.Clear();
      //
      //----------------------------------------------
      // Example Intersection Line - Line
      //----------------------------------------------
      //
      CLine Line0 = new CLine(ViewParametric.RealPixel);
      Line0.CountResolution = 17;
      Line0.X0 = 1; Line0.Y0 = 9;
      Line0.X1 = 9; Line0.Y1 = 5;
      Line0.BuildRealVector();
      ViewParametric.ParametricList.Add(Line0);
      //
      CLine Line1 = new CLine(ViewParametric.RealPixel);
      Line1.CountResolution = 15;
      Line1.X0 = 1; Line1.Y0 = 2;
      Line1.X1 = 9; Line1.Y1 = 7;
      Line1.BuildRealVector();
      ViewParametric.ParametricList.Add(Line1);
      // 
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.LineLineIterative(Line0, Line1,
                                         0.001,
                                         out SS, out TS,
                                         out XS, out YS))
      {
        CCircle CircleSection = new CCircle(ViewParametric.RealPixel);
        CircleSection.CountResolution = 12;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.2;
        CircleSection.BuildRealVector();
        ViewParametric.ParametricList.Add(CircleSection);
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Menu
    //--------------------------------------------------------------------------
    //
    private void btnExampleLineLine_Click(object sender, EventArgs e)
    {
      ExampleLineLine();
      ForceRedraw();
    }

    private void btnExampleSectionLine_Click(object sender, EventArgs e)
    {
      ExampleSectionLine();
      ForceRedraw();
    }

    private void btnExampleSectionSection_Click(object sender, EventArgs e)
    {
      ExampleSectionSection();
      ForceRedraw();
    }




  }
}
