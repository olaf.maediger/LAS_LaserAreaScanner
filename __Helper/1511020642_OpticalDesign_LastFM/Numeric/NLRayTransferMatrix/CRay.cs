﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
using NLParametricCurve;
//
namespace NLRayTransferMatrix
{
  public enum ERayDirection 
  {
    Undefined = 0,
    Positive = 1,
    Negative = 2
  };

  public class CRay : CVector2
  {
    private CParametricList FParametricList;
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CRay(CRealPixel realpixel,
                Double z, Double x, Double a) // [mm], [deg]
      : base()
    {
      FValues[INDEX_POSITIONX] = z; // [mm]
      FValues[INDEX_POSITIONY] = x; // [mm]
      FValues[INDEX_ANGLE] = a; // [deg]
      BuildUnitVector();
      FParametricList = new CParametricList(realpixel);
    }

    public CRay(CRealPixel realpixel,
                CRay ray)
      : base()
    {
      FValues[INDEX_POSITIONX] = ray[INDEX_POSITIONX]; // [mm]
      FValues[INDEX_POSITIONY] = ray[INDEX_POSITIONY]; // [mm]
      FValues[INDEX_ANGLE] = ray[INDEX_ANGLE];
      BuildUnitVector();
      FParametricList = new CParametricList(realpixel);
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    private void SetZ0(Double value)
    {
      FValues[INDEX_POSITIONX] = value;
      BuildUnitVector();
    }
    public Double Z0
    {
      get { return FValues[INDEX_POSITIONX]; }
      set { SetZ0(value); }
    }
    public Double Z1
    {
      get { return FValues[INDEX_POSITIONX] + FValues[INDEX_DZ]; }
    }

    private void SetX0(Double value)
    {
      FValues[INDEX_POSITIONY] = value;
      BuildUnitVector();
    }
    public Double X0
    {
      get { return FValues[INDEX_POSITIONY]; }
      set { SetX0(value); }
    }
    public Double X1
    {
      get { return FValues[INDEX_POSITIONY] + FValues[INDEX_DX]; }
    }

    public Double A
    {
      get { return FValues[INDEX_ANGLE]; }
    }

    private ERayDirection GetDirection()
    {
      if (FValues[INDEX_DZ] < 0)
      {
        return ERayDirection.Negative;
      }
      if (0 < FValues[INDEX_DZ])
      {
        return ERayDirection.Positive;
      }
      return ERayDirection.Undefined;
    }
    public ERayDirection Direction
    {
      get { return GetDirection(); }
    }

    public CRealPixel RealPixel
    {
      get { return FParametricList.RealPixel; }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------------------
    //
    //public Boolean MultiplyEntry(CComponent component)
    //{
    //  c
    //}
    //public Boolean MultiplyExit(CComponent component)
    //{
    //}

    public Boolean Draw(Graphics graphics,
                        CRealPixel realpixel)
    {
      Double Z0 = FValues[INDEX_POSITIONX];
      Double X0 = FValues[INDEX_POSITIONY];
      Double DZ = FValues[INDEX_DZ];
      Double DX = FValues[INDEX_DX];
      Double Z1 = Z0 + DZ;
      Double X1 = X0 + DX;
      Int32 PZ0 = realpixel.XRealPixel(Z0);
      Int32 PX0 = realpixel.YRealPixel(X0);
      Int32 PZ1 = realpixel.XRealPixel(Z1);
      Int32 PX1 = realpixel.YRealPixel(X1);
      Pen P = new Pen(Color.Red, 2);
      graphics.DrawLine(P, PZ0, PX0, PZ1, PX1);
      //
      Double FF = 0.25;
      Double ZC = Z1 - FF * DZ;
      Double XC = X1 - FF * DX;
      // debug Int32 PZC = realpixel.XRealPixel(ZC);
      // debug Int32 PXC = realpixel.YRealPixel(XC);
      Double AR = CMath.DegreeRadian(FValues[INDEX_ANGLE] + 90);
      Double DR = FF * FF * Math.Sqrt(DZ * DZ + DX * DX);
      Double CDR = DR * Math.Cos(AR);
      Double SDR = DR * Math.Sin(AR);
      Double ZL = ZC + CDR;
      Double XL = XC + SDR;
      Int32 PZL = realpixel.XRealPixel(ZL);
      Int32 PXL = realpixel.YRealPixel(XL);
      Double ZR = ZC - CDR;
      Double XR = XC - SDR;
      Int32 PZR = realpixel.XRealPixel(ZR);
      Int32 PXR = realpixel.YRealPixel(XR);
      Point[] PS = new Point[3];
      PS[0].X = PZ1; PS[0].Y = PX1;
      PS[1].X = PZL; PS[1].Y = PXL;
      PS[2].X = PZR; PS[2].Y = PXR;
      Brush B = new SolidBrush(Color.Red);
      graphics.FillPolygon(B, PS);
      //
      FParametricList.Draw(graphics);
      //
      return true;
    }

    

    public Boolean BuildPositionX(CComponent component)
    {
      Double Z0 = component.PositionX; 
      Double DZ = component.Width;
      FValues[INDEX_ANGLE] = CMath.NormalizeAngleDegree(FValues[INDEX_ANGLE]);
      Double AngleDegree = FValues[INDEX_ANGLE];
      if ((0 < AngleDegree) && (AngleDegree < 90))
      {
        FValues[INDEX_POSITIONX] = Z0 + DZ;
      }
      else
        if ((90 < AngleDegree) && (AngleDegree < 180))
        {
          FValues[INDEX_POSITIONX] = Z0 - DZ;
        }
        else
          if ((180 < AngleDegree) && (AngleDegree < 270))
          {
            FValues[INDEX_POSITIONX] = Z0 - DZ;
          }
          else
            if ((270 < AngleDegree) && (AngleDegree < 360))
            {
              FValues[INDEX_POSITIONX] = Z0 + DZ;
            }
            else
              if (0 == AngleDegree)
              {
                FValues[INDEX_POSITIONX] = Z0 + DZ;
              }
              else
                if (90 == AngleDegree)
                {
                  FValues[INDEX_POSITIONX] = Z0;
                }
                else
                  if (180 == AngleDegree)
                  {
                    FValues[INDEX_POSITIONX] = Z0 - DZ;
                  }
                  else
                    if (270 == AngleDegree)
                    {
                      FValues[INDEX_POSITIONX] = Z0;
                    }
                    else
                      if (360 == AngleDegree)
                      {
                        FValues[INDEX_POSITIONX] = Z0 + DZ;
                      }
                      else
                      {
                        return false;
                      }
      return true;
    }

    //public override Boolean BuildRayList(CRayList raylist)
    //{
    //  Int32 RC = raylist.Count;
    //  if (0 < RC)
    //  { // RayIn: Entry of this Component
    //    CRay RayIn = new CRay(FRealPixel, raylist[RC - 1]);
    //    RayIn.Multiply(this); // Fraction on Entry
    //    RayIn.PositionX = this.PositionX;
    //    RayIn.BuildUnitVector();
    //    raylist.Add(RayIn);
    //    // RayTarget: Out from this Component
    //    CRay RayOut = new CRay(FRealPixel, RayIn);
    //    CTranslation T = new CTranslation(FRealPixel, this.PositionX, this.Width, FFractionIndices);
    //    RayOut.Multiply(T);
    //    RayOut.PositionX = this.PositionX + this.Width;
    //    RayOut.BuildUnitVector();
    //    raylist.Add(RayOut);
    //    return true;
    //  }
    //  return false;
    //}


    public Boolean BuildRayTarget(CComponentList componentlist,
                                  out CRay raytarget)
    {
      raytarget = null;
      try
      {
        // Find nearest Intersection
        Double R2Minimum = 1E9;
        CCircle CircleMinimum = null;
        //
        foreach (CComponent Component in componentlist)
        {
          CIntersectionPoints IntersectionPoints;
          if (Component.CalculateIntersection(Direction, this, out IntersectionPoints))
          {
            // debug Double RS = 0.2;
            foreach (CIntersectionPoint IntersectionPoint in IntersectionPoints)
            { // Draw each Intersection
              // debug FParametricList.AddCircle(IntersectionPoint.XS, IntersectionPoint.YS, RS, 9);
              // proof each Intersection if nearest
              Double DX2 = this.Z0 - IntersectionPoint.XS;
              DX2 *= DX2;
              Double DY2 = this.X0 - IntersectionPoint.YS;
              DY2 *= DY2;
              Double R2 = DX2 + DY2;
              if (R2 < R2Minimum)
              {
                R2Minimum = R2;
                raytarget = new CRay(this.RealPixel, IntersectionPoint.XS, IntersectionPoint.YS, this.A);
                switch (IntersectionPoint.RayTransition)
                {
                  case ERayTransition.PositiveEntry:
                    raytarget.Multiply(Component.MatrixEntry);
                    break;
                  case ERayTransition.PositiveExit:
                    raytarget.Multiply(Component.MatrixExit);
                    break;
                  case ERayTransition.NegativeEntry:
                    raytarget.Multiply(Component.MatrixEntry);
                    break;
                  case ERayTransition.NegativeExit:
                    raytarget.Multiply(Component.MatrixExit);
                    break;
                }
                CircleMinimum = new CCircle(this.RealPixel,
                                            raytarget.Z0, raytarget.X0,
                                            0.5, 12);
              }
            }
          }
        }
        // Draw nearest Intersection
        if (CircleMinimum is CCircle)
        {
          // debug FParametricList.AddCircle(CircleMinimum);
        }
        return (raytarget is CRay);
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
