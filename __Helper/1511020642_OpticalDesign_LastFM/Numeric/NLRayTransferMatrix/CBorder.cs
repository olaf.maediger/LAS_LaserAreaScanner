﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLRealPixel;
using NLParametricCurve;
//
namespace NLRayTransferMatrix
{
  public abstract class CBorder : CParametric
  {
    public CBorder(CRealPixel realpixel)
      : base(realpixel)
    {
    }
  }
}
