﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
using NLParametricCurve;
//
namespace NLRayTransferMatrix
{
  public class CLenseSmall : CComponent
  {

    public CLenseSmall(CRealPixel realpixel,
                       Double positionx, Double positiony,
                       Double width, Double height,
                       Double[] fractionindices)
      : base(realpixel, positionx, positiony, width, height, fractionindices)
    {
    }

    //protected override Double GetLength()
    //{
    //  return 1.0;
    //}

    //
    //---------------------------------------------------------------------------
    //  Segment - Public
    //---------------------------------------------------------------------------
    //
    //public override Boolean BuildRealVector()
    //{
    //  try
    //  {
    //    //Double DT = 1.0 / CountResolution;
    //    //for (Int32 Index = 0; Index <= CountResolution; Index++)
    //    //{
    //    //  Double T = Index * DT;
    //    //  Double A = 2 * Math.PI * T;
    //    //  //Double XR = FXC + FR * Math.Cos(A);
    //    //  //Double YR = FYC + FR * Math.Sin(A);
    //    //  ////
    //    //  //FXRealVector[Index] = XR;
    //    //  //FYRealVector[Index] = YR;
    //    //}
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

    //public override Boolean Draw(Graphics graphics)
    //{

    //  return true;
    //}

    //public override Boolean BuildRayList(CRayList raylist)
    //{
    //  return false;
    //}

    public override Boolean CalculateIntersection(ERayDirection raydirection,
                                                  CRay ray,
                                                  out CIntersectionPoints intersectionpoints)
    {
      intersectionpoints = new CIntersectionPoints();
      return (0 < intersectionpoints.Count);
    }


  }
}
