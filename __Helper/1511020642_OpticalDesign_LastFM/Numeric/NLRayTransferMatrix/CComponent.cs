﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
using NLParametricCurve;
//
namespace NLRayTransferMatrix
{
  public abstract class CComponent : CBorderList
  {
    //
    //---------------------------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    //
    protected Double[] FFractionIndices;
    protected Double FPositionX;
    protected Double FPositionY;
    protected Double FWidth;
    protected Double FHeight;
    protected CMatrix22 FMatrixEntry;
    protected CMatrix22 FMatrixExit;
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CComponent(CRealPixel realpixel,
                      Double positionx, Double positiony,
                      Double width, Double height,
                      Double[] fractionindices) // 3: pre, inner, post
      : base(realpixel)
    {
      FPositionX = positionx;
      FPositionY = positiony;
      FWidth = width;
      FHeight = height;
      Int32 FIH = fractionindices.Length;
      FFractionIndices = new Double[FIH];
      for (Int32 FI = 0; FI < FIH; FI++)
      {
        FFractionIndices[FI] = fractionindices[FI];
      }
      FMatrixEntry = new CMatrix22();
      FMatrixExit = new CMatrix22();
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    private CMatrix22 GetMatrixEntry()
    {
      return FMatrixEntry;
    }
    public CMatrix22 MatrixEntry
    {
      get { return GetMatrixEntry(); }
    }

    private CMatrix22 GetMatrixExit()
    {
      return FMatrixExit;
    }
    public CMatrix22 MatrixExit
    {
      get { return GetMatrixExit(); }
    }

    public Double PositionX
    {
      get { return FPositionX; }
    }
    public Double PositionY
    {
      get { return FPositionY; }
    }
    public Double Width
    {
      get { return FWidth; }
    }
    public Double Height
    {
      get { return FHeight; }
    }
    public Double[] FractionIndices
    {
      get { return FFractionIndices; }
    }

    protected virtual Double GetZ0()
    {
      return FPositionX;
    }
    protected virtual void SetZ0(Double value)
    {
      FPositionX = value;
    }
    public Double Z0
    {
      get { return GetZ0(); }
      set { SetZ0(value); }
    }

    protected virtual Double GetZ1()
    {
      return FPositionX + FWidth;
    }    
    public Double Z1
    {
      get { return GetZ1(); }
    }

    protected virtual Double GetX0()
    {
      return FPositionY;
    }
    protected virtual void SetX0(Double value)
    {
      FPositionY = value;
    }
    public Double X0
    {
      get { return GetX0(); }
      set { SetX0(value); }
    }

    protected virtual Double GetX1()
    {
      return FPositionY + FHeight;
    }
    public Double X1
    {
      get { return GetX1(); }
    }

    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //   
    // ??? public abstract Boolean BuildRayList(CRayList raylist);
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //
    public abstract Boolean CalculateIntersection(ERayDirection raydirection,
                                                  CRay ray,
                                                  out CIntersectionPoints intersectionpoints);

  }
}
