﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
using NLParametricCurve;
//
namespace NLRayTransferMatrix
{
  public class CRayList : List<CRay>
  {
    private CRay FRayBase; // Base for all followers(source/target) in list

    public CRayList(CRay raybase)
      : base()
    {
      FRayBase = raybase;
      Add(FRayBase);
    }



    public CRay RayBase
    {
      get
      {
        return FRayBase; 
      }
    }

    public Boolean ClearRays()
    {
      base.Clear();
      Add(FRayBase);
      return (1 == this.Count);
    }

    public Boolean AddRay(CRealPixel realpixel,
                          Double positionx, Double positiony, Double angle) // [mm], [deg]
    {
      try
      {
        CRay Ray = new CRay(realpixel, positionx, positiony, angle);
        this.Add(Ray);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean AddRay(CRay ray)
    {
      try
      {
        this.Add(ray);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean CalculateRays(CComponentList componentlist)
    {
      try
      {
        ClearRays();
        CRay RaySource = RayBase;
        CRay RayTarget = null;
        while (RaySource is CRay)
        {
          if (RaySource.BuildRayTarget(componentlist, out RayTarget))
          {
            AddRay(RayTarget);
          }
          RaySource = RayTarget;
        }
        return (1 < this.Count);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Draw(Graphics graphics,
                        CRealPixel realpixel)
    {
      Boolean Result = true;
      for (Int32 CI = 0; CI < this.Count; CI++)
      {
        CRay Ray = this[CI];
        Result &= Ray.Draw(graphics, realpixel);
      }
      return Result;
    }

  }
}
