﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
using NLParametricCurve;
//
namespace NLRayTransferMatrix
{
  public class CPlateParallel : CComponent
  {
    //
    //---------------------------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    //
 
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CPlateParallel(CRealPixel realpixel,
                          Double positionx, Double positiony, 
                          Double width, Double height,
                          Double[] fractionindices)
      : base(realpixel, positionx, positiony, width, height, fractionindices)
    {
      FMatrixEntry[0, 0] = 1;
      FMatrixEntry[0, 1] = 0;
      FMatrixEntry[1, 0] = 0;
      FMatrixEntry[1, 1] = FFractionIndices[0] / FFractionIndices[1];
      //
      FMatrixExit[0, 0] = 1;
      FMatrixExit[0, 1] = 0;
      FMatrixExit[1, 0] = 0;
      FMatrixExit[1, 1] = FFractionIndices[1] / FFractionIndices[2];
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    protected override double GetX0()
    {
      return FPositionY;
    }

    protected override double GetX1()
    {
      return FPositionY + FHeight;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------------------
    //
    //protected override Double GetLength()
    //{
    //  return 2;// *Math.PI * FR;
    //}

    //public override Boolean BuildRealVector()
    //{
    //  try
    //  {
    //    Double DT = 1.0 / FCountResolution;
    //    for (Int32 Index = 0; Index <= FCountResolution; Index++)
    //    {
    //      Double T = Index * DT;
    //      Double A = 2 * Math.PI * T;
    //      //Double XR = FXC + FR * Math.Cos(A);
    //      //Double YR = FYC + FR * Math.Sin(A);
    //      ////
    //      //FXRealVector[Index] = XR;
    //      //FYRealVector[Index] = YR;
    //    }
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}
    //
    //---------------------------------------------------------------------------
    //  Segment - Public
    //---------------------------------------------------------------------------
    //
    //public override Boolean Draw(Graphics graphics)
    //{
    //  Int32 PX = FRealPixel.XRealPixel(FPositionX);
    //  Int32 PY = FRealPixel.YRealPixel(FPositionY);
    //  Int32 PDX = FRealPixel.DXRealPixel(FWidth);
    //  Int32 PDY = Math.Abs(FRealPixel.DYRealPixel(FHeight));
    //  PY -= PDY;
    //  Byte R = (Byte)(255 - 255.0 * (FFractionIndices[1] - 1.0));
    //  Brush B = new SolidBrush(Color.FromArgb(0x8F, R, 0xF0, 0xF0)); 
    //  graphics.FillRectangle(B, PX, PY, PDX, PDY);
    //  Pen P = new Pen(Color.FromArgb(0xA0, 0xFF, 0x66, 0x88), 1);
    //  graphics.DrawRectangle(P, PX, PY, PDX, PDY);
    //  //
    //  P.Dispose();
    //  B.Dispose();
    //  //
    //  return true;
    //}

    //public override Boolean BuildRayList(CRayList raylist)
    //{
    //  Int32 RC = raylist.Count;
    //  if (0 < RC)
    //  { // RayIn: Entry of this Component
    //    CRay RayIn = new CRay(FRealPixel, raylist[RC - 1]);
    //    RayIn.Multiply(this); // Fraction on Entry
    //    RayIn.PositionX = this.PositionX;
    //    RayIn.BuildUnitVector();
    //    raylist.Add(RayIn);
    //    // RayTarget: Out from this Component
    //    CRay RayOut = new CRay(FRealPixel, RayIn);
    //    CTranslation T = new CTranslation(FRealPixel, this.PositionX, this.Width, FFractionIndices);
    //    RayOut.Multiply(T);
    //    RayOut.PositionX = this.PositionX + this.Width;
    //    RayOut.BuildUnitVector();
    //    raylist.Add(RayOut);
    //    return true;
    //  }
    //  return false;
    //}

    public override Boolean CalculateIntersection(ERayDirection raydirection,
                                                  CRay ray,
                                                  out CIntersectionPoints intersectionpoints)
    {
      Double[] XS, YS;
      Double Z0Ray = ray.Z0;
      Double Z0Left = Z0;
      Double Z0Right = Z1;
      intersectionpoints = new CIntersectionPoints();
      if (ERayDirection.Positive == ray.Direction)
      {
        if (Z0Ray < Z0Left)
        { 
          if (CMath.IntersectionLineSegment(Z0Ray, ray.X0, ray.Z1, ray.X1,
                                            Z0Left, X0, Z0Left, X1,
                                            out XS, out YS))
          { // Intersection - Entry: Outer -> Inner
            Int32 RTIH = Math.Max(XS.Length, YS.Length);
            ERayTransition[] RTS = new ERayTransition[RTIH];
            for (Int32 RTI = 0; RTI < RTIH; RTI++)
            {
              RTS[RTI] = ERayTransition.PositiveEntry;
            }
            intersectionpoints.Add(XS, YS, RTS);
          }
        }
        if (Z0Ray < Z0Right)
        {
          if (CMath.IntersectionLineSegment(Z0Ray, ray.X0, ray.Z1, ray.X1,
                                            Z0Right, X0, Z0Right, X1,
                                            out XS, out YS))
          { // Intersection - Exit: Inner -> Outer
            Int32 RTIH = Math.Max(XS.Length, YS.Length);
            ERayTransition[] RTS = new ERayTransition[RTIH];
            for (Int32 RTI = 0; RTI < RTIH; RTI++)
            {
              RTS[RTI] = ERayTransition.PositiveExit;
            }
            intersectionpoints.Add(XS, YS, RTS);
          }
        }
      }
      if (ERayDirection.Negative == ray.Direction)
      {
        if (Z0Left < Z0Ray)
        { // Intersection - Exit: Outer <- Inner
          if (CMath.IntersectionLineSegment(Z0Ray, ray.X0, ray.Z1, ray.X1,
                                            Z0Left, X0, Z0Left, X1,
                                            out XS, out YS))
          {
            Int32 RTIH = Math.Max(XS.Length, YS.Length);
            ERayTransition[] RTS = new ERayTransition[RTIH];
            for (Int32 RTI = 0; RTI < RTIH; RTI++)
            {
              RTS[RTI] = ERayTransition.NegativeExit;
            }
            intersectionpoints.Add(XS, YS, RTS);
          }
        }
        if (Z0Right < Z0Ray)
        { // Intersection - Entry: Inner <- Outer
          if (CMath.IntersectionLineSegment(Z0Ray, ray.X0, ray.Z1, ray.X1,
                                            Z0Right, X0, Z0Right, X1,
                                            out XS, out YS))
          {
            Int32 RTIH = Math.Max(XS.Length, YS.Length);
            ERayTransition[] RTS = new ERayTransition[RTIH];
            for (Int32 RTI = 0; RTI < RTIH; RTI++)
            {
              RTS[RTI] = ERayTransition.NegativeEntry;
            }
            intersectionpoints.Add(XS, YS, RTS);
          }
        }
      }
      return (0 < intersectionpoints.Count);
    }


  }
}
