﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLRayTransferMatrix
{
  public class CComponentList : List<CComponent>
  {

    //
    //---------------------------------------------------------------------------
    //  Segment - Public
    //---------------------------------------------------------------------------
    //
    public Boolean Draw(Graphics graphics)
    {
      Boolean Result = true;
      foreach (CComponent Component in this)
      {
        //Result &= Component.Draw(graphics);
      }
      return Result;
    }

  }
}
