﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLRayTransferMatrix
{
  public class CMatrix22
  {
    //
    //---------------------------------------------------------------------------
    //  Segment - Constant 
    //---------------------------------------------------------------------------
    //
    public const Int32 DIMENSION_ROW = 2;
    public const Int32 DIMENSION_COL = 2;
    public const Double INIT_VALUE = 0;

    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    //
    private Double[,] FValues;
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CMatrix22()
    {
      FValues = new Double[DIMENSION_ROW, DIMENSION_COL];
      InitValue(INIT_VALUE);
    }

    public CMatrix22(CMatrix22 matrix)
    {
      FValues = new Double[DIMENSION_ROW, DIMENSION_COL];
      Copy(matrix);
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    private Double GetThis(Int32 row, Int32 col)
    {
      try
      {
        return FValues[row, col];
      }
      catch (Exception)
      {
        return INIT_VALUE;
      }
    }
    private void SetThis(Int32 row, Int32 col, Double value)
    {
      try
      {
        FValues[row, col] = value;
      }
      catch (Exception)
      {
        return;
      }
    }
    public Double this[Int32 row, Int32 col]
    {
      get { return GetThis(row, col); }
      set { SetThis(row, col, value); }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public Boolean InitValue(Double value)
    {
      try
      {
        for (Int32 RI = 0; RI < DIMENSION_ROW; RI++)
        {
          for (Int32 CI = 0; CI < DIMENSION_COL; CI++)
          {
            FValues[RI, CI] = value;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Copy(Double[,] values)
    {
      try
      {
        for (Int32 RI = 0; RI < DIMENSION_ROW; RI++)
        {
          for (Int32 CI = 0; CI < DIMENSION_COL; CI++)
          {
            FValues[RI, CI] = values[RI, CI];
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public Boolean Copy(CMatrix22 matrix)
    {
      try
      {
        for (Int32 RI = 0; RI < DIMENSION_ROW; RI++)
        {
          for (Int32 CI = 0; CI < DIMENSION_COL; CI++)
          {
            FValues[RI, CI] = matrix[RI, CI];
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Multiply(CMatrix22 matrix)
    {
      try
      {
        Double[,] Result = new Double[DIMENSION_ROW, DIMENSION_COL];
        for (Int32 RI = 0; RI < DIMENSION_ROW; RI++)
        {
          for (Int32 CI = 0; CI < DIMENSION_COL; CI++)
          {
            Result[RI, CI] = FValues[RI, 0] * matrix[0, CI] +
                             FValues[RI, 1] * matrix[1, CI];
          }
        }
        return Copy(Result);
      }
      catch (Exception)
      {
        return false;
      }
    }

    //public Boolean Multiply(CComponent component)
    //{
    //  try
    //  {
    //    Double[,] Result = new Double[DIMENSION_ROW, DIMENSION_COL];
    //    for (Int32 RI = 0; RI < DIMENSION_ROW; RI++)
    //    {
    //      for (Int32 CI = 0; CI < DIMENSION_COL; CI++)
    //      {
    //        Result[RI, CI] = FValues[RI, 0] * component.Matrix22[0, CI] +
    //                         FValues[RI, 1] * component.Matrix22[1, CI];
    //      }
    //    }
    //    return Copy(Result);
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

  }
}
