﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLParametricCurve;
//
namespace NLRayTransferMatrix
{
  public class CVector2
  { //
    //---------------------------------------------------------------------------
    //  Segment - Constant 
    //---------------------------------------------------------------------------
    //
    public const Int32 DIMENSION_VECTOR2 = 5; 
    public const Int32 INDEX_POSITIONX = 0;
    public const Int32 INDEX_POSITIONY = 1;
    public const Int32 INDEX_ANGLE = 2;
    public const Int32 INDEX_DZ = 3;
    public const Int32 INDEX_DX = 4;
    //
    public const Double INIT_VALUE = 0;
    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    //
    protected Double[] FValues; // [mm] , [deg]
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CVector2()
    {
      FValues = new Double[DIMENSION_VECTOR2];
      InitValue(INIT_VALUE);
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    private Double GetThis(Int32 row)
    {
      try
      {
        return FValues[row];
      }
      catch (Exception)
      {
        return INIT_VALUE;
      }
    }
    private void SetThis(Int32 row, Double value)
    {
      try
      {
        FValues[row] = value;
      }
      catch (Exception)
      {
      }
    }
    public Double this[Int32 row]
    {
      get { return GetThis(row); }
      set { SetThis(row, value); }
    }

    public Double PositionX
    {
      get { return FValues[INDEX_POSITIONX]; }
      set { FValues[INDEX_POSITIONX] = value; }
    }

    //
    //---------------------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------------------
    //
    public Boolean BuildUnitVector(Double angledegree) // [deg]
    {
      try
      {
        Double AD = CMath.NormalizeAngleDegree(angledegree);
        Double AR = CMath.DegreeRadian(AD);
        Double SAR = Math.Abs(Math.Sin(AR));
        Double CAR = Math.Abs(Math.Cos(AR));
        if ((0 < AD) && (AD < 90))
        {
          FValues[INDEX_DZ] = CAR;
          FValues[INDEX_DX] = SAR;
        }
        else
          if ((90 < AD) && (AD < 180))
          {
            FValues[INDEX_DZ] = -CAR;
            FValues[INDEX_DX] = SAR;
          }
          else
            if ((180 < AD) && (AD < 270))
            {
              FValues[INDEX_DZ] = -CAR;
              FValues[INDEX_DX] = -SAR;
            }
            else
              if ((270 < AD) && (AD < 360))
              {
                FValues[INDEX_DZ] = CAR;
                FValues[INDEX_DX] = -SAR;
              }
              else
                if (0 == AD)
                {
                  FValues[INDEX_DZ] = 1.0;
                  FValues[INDEX_DX] = 0.0;
                }
                else
                  if (90 == AD)
                  {
                    FValues[INDEX_DZ] = 0.0;
                    FValues[INDEX_DX] = 1.0;
                  }
                  else
                    if (180 == AD)
                    {
                      FValues[INDEX_DZ] = -1.0;
                      FValues[INDEX_DX] = 0.0;
                    }
                    else
                      if (270 == AD)
                      {
                        FValues[INDEX_DZ] = 0.0;
                        FValues[INDEX_DX] = -1.0;
                      }
                      else
                        if (360 == AD)
                        {
                          FValues[INDEX_DZ] = 1.0;
                          FValues[INDEX_DX] = 0.0;
                        }
                        else
                        {
                          return false;
                        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean BuildUnitVector()
    {
      Double AD = FValues[INDEX_ANGLE]; // [deg]
      return BuildUnitVector(AD);
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public Boolean InitValue(Double value)
    {
      try
      {
        for (Int32 RI = 0; RI < DIMENSION_VECTOR2; RI++)
        {
          FValues[RI] = value;
        }
        BuildUnitVector();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Copy(Double[] values)
    {
      try
      {
        for (Int32 RI = 0; RI < DIMENSION_VECTOR2; RI++)
        {
          FValues[RI] = values[RI];
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public Boolean Copy(CVector2 vector)
    {
      try
      {
        for (Int32 RI = 0; RI < DIMENSION_VECTOR2; RI++)
        {
          FValues[RI] = vector[RI];
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Multiply(CMatrix22 matrix)
    {
      try
      {
        CVector2 Result = new CVector2();
        Double TA = Math.Tan(CMath.DegreeRadian(FValues[INDEX_ANGLE]));
        // Row 0
        Double RX = matrix[0, 0] * FValues[INDEX_POSITIONY];
        RX += matrix[0, 1] * TA;
        // Row 1
        Double RA = matrix[1, 0] * FValues[INDEX_POSITIONY];
        RA += matrix[1, 1] * TA;
        RA = CMath.RadianDegree(Math.Atan(RA));
        //
        Result[INDEX_POSITIONX] = this.FValues[INDEX_POSITIONX];
        Result[INDEX_POSITIONY] = RX;
        Result[INDEX_ANGLE] = RA;
        Result.BuildUnitVector();
        //
        return Copy(Result);
      }
      catch (Exception)
      {
        return false;
      }
    }

    //public Boolean Multiply(CComponent component)
    //{
    //  try
    //  {
    //    CVector2 Result = new CVector2();
    //    Double TA = Math.Tan(CMath.DegreeRadian(FValues[INDEX_ANGLE]));
    //    // Row 0
    //    Double RX = component.Matrix[0, 0] * FValues[INDEX_POSITIONY];
    //    RX += component[0, 1] * TA;
    //    // Row 1
    //    Double RA = component[1, 0] * FValues[INDEX_POSITIONY];
    //    RA += component[1, 1] * TA;
    //    RA = CMath.RadianDegree(Math.Atan(RA));
    //    //
    //    Result[INDEX_POSITIONX] = this.FValues[INDEX_POSITIONX];
    //    Result[INDEX_POSITIONY] = RX;
    //    Result[INDEX_ANGLE] = RA;
    //    Result.BuildUnitVector();
    //    //
    //    return Copy(Result);
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

    //public Boolean MultiplyEntry(CComponent component)
    //{
    //  try
    //  {
    //    CVector2 Result = new CVector2();
    //    Double TA = Math.Tan(CMath.DegreeRadian(FValues[INDEX_ANGLE]));
    //    // Row 0
    //    Double RX = component.MatrixEntry(0, 0) * FValues[INDEX_POSITIONY];
    //    RX += component.MatrixEntry(0, 1) * TA;
    //    // Row 1
    //    Double RA = component.MatrixEntry(1, 0) * FValues[INDEX_POSITIONY];
    //    RA += component.MatrixEntry(1, 1) * TA;
    //    RA = CMath.RadianDegree(Math.Atan(RA));
    //    //
    //    Result[INDEX_POSITIONX] = this.FValues[INDEX_POSITIONX];
    //    Result[INDEX_POSITIONY] = RX;
    //    Result[INDEX_ANGLE] = RA;
    //    Result.BuildUnitVector();
    //    //
    //    return Copy(Result);
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

    //public Boolean MultiplyExit(CComponent component)
    //{
    //  try
    //  {
    //    CVector2 Result = new CVector2();
    //    Double TA = Math.Tan(CMath.DegreeRadian(FValues[INDEX_ANGLE]));
    //    // Row 0
    //    Double RX = component.MatrixExit(0, 0) * FValues[INDEX_POSITIONY];
    //    RX += component.MatrixExit[0, 1] * TA;
    //    // Row 1
    //    Double RA = component.MatrixExit[1, 0] * FValues[INDEX_POSITIONY];
    //    RA += component.MatrixExit[1, 1] * TA;
    //    RA = CMath.RadianDegree(Math.Atan(RA));
    //    //
    //    Result[INDEX_POSITIONX] = this.FValues[INDEX_POSITIONX];
    //    Result[INDEX_POSITIONY] = RX;
    //    Result[INDEX_ANGLE] = RA;
    //    Result.BuildUnitVector();
    //    //
    //    return Copy(Result);
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

  }
}
