﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLRayTransferMatrix
{
  public class CRayPool : List<CRayList>
  {
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CRayPool()
    {
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------
    //  Segment - Public
    //---------------------------------------------------------------------------
    //
    public Boolean AddRay(CRealPixel realpixel,
                          Double positionx, Double positiony, Double angle) // [mm], [deg]
    {
      try
      {
        CRay RayBase = new CRay(realpixel, positionx, positiony, angle);
        CRayList RayList = new CRayList(RayBase);
        this.Add(RayList);
        return (1 == RayList.Count);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean CalculateRays(CComponentList componentlist)
    {
      Boolean Result = true;
      foreach (CRayList RayList in this)
      {
        Result &= RayList.CalculateRays(componentlist);
      }
      return Result;
    }

    public Boolean Draw(Graphics graphics,
                        CRealPixel realpixel)
    {
      Boolean Result = true;
      for (Int32 CI = 0; CI < this.Count; CI++)
      {
        CRayList RayList = this[CI];
        Result &= RayList.Draw(graphics, realpixel);
      }
      return Result;
    }



  }
}
