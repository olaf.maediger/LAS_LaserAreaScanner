﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
using NLParametricCurve;
//
namespace NLRayTransferMatrix
{
  public class CPrismaticSquare : CComponent
  {
    public CPrismaticSquare(CRealPixel realpixel,
                            Double positionx, Double positiony,
                            Double width, Double height,
                            Double[] fractionindices)
      : base(realpixel, positionx, positiony, width, height, fractionindices)
    {
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------------------
    //
    //protected override Double GetLength()
    //{
    //  return 2;// *Math.PI * FR;
    //}

    //public override Boolean BuildRealVector()
    //{
    //  try
    //  {
    //    Double DT = 1.0 / FCountResolution;
    //    for (Int32 Index = 0; Index <= FCountResolution; Index++)
    //    {
    //      Double T = Index * DT;
    //      Double A = 2 * Math.PI * T;
    //      //Double XR = FXC + FR * Math.Cos(A);
    //      //Double YR = FYC + FR * Math.Sin(A);
    //      ////
    //      //FXRealVector[Index] = XR;
    //      //FYRealVector[Index] = YR;
    //    }
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}
    //
    //---------------------------------------------------------------------------
    //  Segment - Public
    //---------------------------------------------------------------------------
    //
    //public override Boolean Draw(Graphics graphics)
    //{

    //  return true;
    //}

    //public override Boolean BuildRayList(CRayList raylist)
    //{
    //  return false;
    //}

    public override Boolean CalculateIntersection(ERayDirection raydirection,
                                                  CRay ray,
                                                  out CIntersectionPoints intersectionpoints)
    {
      intersectionpoints = new CIntersectionPoints();
      return false;
    }


  }
}
