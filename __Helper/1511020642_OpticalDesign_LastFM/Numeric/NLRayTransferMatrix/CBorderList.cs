﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLRealPixel;
using NLParametricCurve;
//
namespace NLRayTransferMatrix
{
  public class CBorderList : List<CBorder>
  {
    //
    //---------------------------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    // reference to global transformation
    protected CRealPixel FRealPixel;
    //
    
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CBorderList(CRealPixel realpixel)
    {
      FRealPixel = realpixel;
    }

  }
}
