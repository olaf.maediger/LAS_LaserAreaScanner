﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
using NLParametricCurve;
//
namespace NLRayTransferMatrix
{
  public class CMirrorPlane : CComponent
  {
    //
    //---------------------------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CMirrorPlane(CRealPixel realpixel,
                        Double positionx, Double positiony, 
                        Double width, Double height,
                        Double[] fractionindices)
      : base(realpixel, positionx, positiony, width, height, fractionindices)
    {
      FMatrixEntry[0, 0] = 1;
      FMatrixEntry[0, 1] = 0;
      FMatrixEntry[1, 0] = 0;
      FMatrixEntry[1, 1] = -1;
      //
      FMatrixExit[0, 0] = 1;
      FMatrixExit[0, 1] = 0;
      FMatrixExit[1, 0] = 0;
      FMatrixExit[1, 1] = 1;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    //protected override Double GetLength()
    //{
    //  return Math.Sqrt(1.0);
    //}

    //
    //---------------------------------------------------------------------------
    //  Segment - Public
    //---------------------------------------------------------------------------
    //
    //public override Boolean BuildRealVector()
    //{
    //  try
    //  {
    //    Double DT = 1.0 / FCountResolution;
    //    for (Int32 Index = 0; Index <= FCountResolution; Index++)
    //    {
    //      Double T = Index * DT;
    //      Double A = 2 * Math.PI * T;
    //      //Double XR = FXC + FR * Math.Cos(A);
    //      //Double YR = FYC + FR * Math.Sin(A);
    //      ////
    //      //FXRealVector[Index] = XR;
    //      //FYRealVector[Index] = YR;
    //    }
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}



    //public override Boolean Draw(Graphics graphics)
    //{
    //  Int32 PX = FRealPixel.XRealPixel(FPositionX);
    //  Int32 PY = FRealPixel.YRealPixel(FPositionY);
    //  Int32 PDX = FRealPixel.DXRealPixel(FWidth);
    //  Int32 PDY = Math.Abs(FRealPixel.DYRealPixel(FHeight));
    //  PY -= PDY;
    //  Brush B = new SolidBrush(Color.FromArgb(0x80, 0xEE, 0xBB, 0xEE));
    //  graphics.FillRectangle(B, PX, PY, PDX, PDY);
    //  Pen P = new Pen(Color.FromArgb(0xA0, 0xFF, 0x66, 0x88), 1);
    //  graphics.DrawRectangle(P, PX, PY, PDX, PDY);
    //  //
    //  P.Dispose();
    //  B.Dispose();
    //  //
    //  return true;
    //}

    //public override Boolean BuildRayList(CRayList raylist)
    //{
    //  return false;
    //}

    public override Boolean CalculateIntersection(ERayDirection raydirection,
                                                  CRay ray,
                                                  out CIntersectionPoints intersectionpoints)
    {
      intersectionpoints = new CIntersectionPoints();
      return (0 < intersectionpoints.Count);
    }


  }
}