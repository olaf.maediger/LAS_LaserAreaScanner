﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NLRealPixel
{ //
  //---------------------------------------------------------------------------
  //  Segment - Theory
  //---------------------------------------------------------------------------
  //
  // (xp - xpl) / (xph - xpl) = (xr - xrl) / (xrh - xrl)
  // (yp - yph) / (ypl - yph) = (yr - yrl) / (yrh - yrl)
  // 
  // Pixel -> Real
  //------------------
  // X : Pixel -> Real
  // xr = xrl + (xp - xpl) / (xph - xpl) * (xrh - xrl)
  // dxr = dxp / (xph - xpl) * (xrh - xrl)
  //
  // Y : Pixel -> Real
  // yr = yrl + (yp - yph) / (ypl - yph) * (yrh - yrl)
  // dyr = dyp / (yph - ypl) * (yrh - yrl)
  //
  // Real -> Pixel
  //------------------
  // X : Real -> Pixel
  // xp = xpl + (xr - xrl) / (xrh - xrl) * (xph - xpl)
  // dxp = dxr / (xrh - xrl) * (xph - xpl)
  //
  // Y : Real -> Pixel
  // yp = yph + (yr - yrl) / (yrh - yrl) * (ypl - yph)
  // dyp = dyr / (yrh - yrl) * (ypl - yph)
  //
  //
  public class CRealPixel
  { //
    //---------------------------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------------------------
    //
    public const Double INIT_XREALMINIMUM = -10.0;
    public const Double INIT_XREALMAXIMUM = +10.0;
    public const Double INIT_YREALMINIMUM = -10.0;
    public const Double INIT_YREALMAXIMUM = +10.0;
    //
    public const Int32 INIT_XPIXELMINIMUM = 0;
    public const Int32 INIT_XPIXELMAXIMUM = +100;
    public const Int32 INIT_YPIXELMINIMUM = 0;
    public const Int32 INIT_YPIXELMAXIMUM = +100;
    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    // Real
    private Double FXRL, FXRH;
    private Double FYRL, FYRH;
    // Pixel
    private Int32 FXPL, FXPH;
    private Int32 FYPL, FYPH;
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CRealPixel()
    {
      Init();
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //
    public Double XRealMinimum
    {
      get { return FXRL; }
      set { FXRL = value; }
    }
    public Double XRealMaximum
    {
      get { return FXRH; }
      set { FXRH = value; }
    }

    public Double YRealMinimum
    {
      get { return FYRL; }
      set { FYRL = value; }
    }
    public Double YRealMaximum
    {
      get { return FYRH; }
      set { FYRH = value; }
    }

    public Double DXReal
    {
      get { return FXRH - FXRL; }
    }
    public Double DYReal
    {
      get { return FYRH - FYRL; }
    }

    public void SetPixelExtrema(Int32 width, Int32 height)
    {
      FXPH = width;
      FYPH = height;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------------------
    //
    private void Init()
    {
      FXRL = INIT_XREALMINIMUM;
      FXRH = INIT_XREALMAXIMUM;
      FYRL = INIT_YREALMINIMUM;
      FYRH = INIT_YREALMAXIMUM;
      //
      FXPL = INIT_XPIXELMINIMUM;
      FXPH = INIT_XPIXELMAXIMUM;
      FYPL = INIT_YPIXELMINIMUM;
      FYPH = INIT_YPIXELMAXIMUM;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    // X : Real -> Pixel
    public Int32 XRealPixel(Double xr)
    {
      return Convert.ToInt32(FXPL + (xr - FXRL) * (FXPH - FXPL) / (FXRH - FXRL));
    }
    public Int32 DXRealPixel(Double dxr)
    {
      return Convert.ToInt32(dxr * (FXPH - FXPL) / (FXRH - FXRL));
    }
    // Y : Real -> Pixel
    public Int32 YRealPixel(Double yr)
    {
      return Convert.ToInt32(FYPH + (yr - FYRL) * (FYPL - FYPH) / (FYRH - FYRL));
    }
    public Int32 DYRealPixel(Double dyr)
    {
      return Convert.ToInt32(dyr * (FYPL - FYPH) / (FYRH - FYRL));
    }
    // X : Pixel -> Real
    public Double XPixelReal(Int32 xp)
    {
      return Convert.ToDouble(FXRL + (xp - FXPL) * (FXRH - FXRL) / (FXPH - FXPL));
    }
    public Double DXPixelReal(Int32 dxp)
    {
      return Convert.ToDouble(dxp * (FXRH - FXRL) / (FXPH - FXPL));
    }
    // Y : Pixel -> Real
    public Double YPixelReal(Int32 yp)
    {
      return Convert.ToDouble(FYRL + (yp - FYPH) * (FYRH - FYRL) / (FYPL - FYPH));
    }
    public Double DYPixelReal(Int32 dyp)
    {
      return Convert.ToDouble(dyp * (FYRH - FYRL) / (FYPL - FYPH));
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //
  }
}
