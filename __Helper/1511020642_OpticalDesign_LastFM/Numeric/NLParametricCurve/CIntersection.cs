﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLParametricCurve
{
  public class CIntersection
  {
    //
    //-----------------------------------------------------------------------------
    //  Section - 
    //-----------------------------------------------------------------------------
    //
  
    //
    //-----------------------------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------------------------
    //
    public Boolean SolveLinearEquationSystem(Double a00, Double a01, Double b0,
                                          Double a10, Double a11, Double b1,
                                          out Double xm, out Double ym)
    {
      Double D = a00 * a11 - a01 * a10;
      Double DX = b0 * a11 - a01 * b1;
      Double DY = a00 * b1 - b0 * a10;
      if (0 != D)
      {
        xm = DX / D;
        ym = DY / D;
        return true;
      }
      xm = 0; ym = 0;
      return false;
    }
    //
    //-----------------------------------------------------------------------------
    //  Section - Explicit
    //-----------------------------------------------------------------------------
    // 
    //
    //-----------------------------------------------------------------------------
    //  Section - Explicit - Section - Section
    //-----------------------------------------------------------------------------
    //
    public Boolean SectionSectionExplicit(CSegment section0, CSegment section1,
                                          out Double ss, out Double ts,
                                          out Double xs, out Double ys)
    {
      ss = 0.0;
      ts = 0.0;
      xs = 0.0;
      ys = 0.0;
      try
      {
        Double X1 = section0.X0;
        Double Y1 = section0.Y0;
        Double X2 = section0.X1;
        Double Y2 = section0.Y1;
        Double X3 = section1.X0;
        Double Y3 = section1.Y0;
        Double X4 = section1.X1;
        Double Y4 = section1.Y1;
        Double DS = (X3 - X1) * (Y3 - Y4) - (X3 - X4) * (Y3 - Y1);
        Double DT = (X2 - X1) * (Y3 - Y1) - (X3 - X1) * (Y2 - Y1);
        Double D1 = (X2 - X1) * (Y3 - Y4);
        Double D2 = (X3 - X4) * (Y2 - Y1);
        if (D1 == D2)
        {
          return false;
        }
        Double DD = D1 - D2;
        Double SS = DS / DD;
        Double TS = DT / DD;
        xs = X1 + (X2 - X1) * SS;
        ys = Y1 + (Y2 - Y1) * SS;
        return ((0 <= SS) && (SS <= 1) && (0 <= TS) && (TS <=1));
      }
      catch (Exception)
      {
        return false;
      }
    }

    //
    //-----------------------------------------------------------------------------
    //  Section - Iterative
    //-----------------------------------------------------------------------------
    //
    //
    //-----------------------------------------------------------------------------
    //  Section - Iterative - Section - Section
    //-----------------------------------------------------------------------------
    //
    // Intersection: Section-Section
    //
    // s in [0..1], t in [0..1]
    // f(s,t) = x0 + (x1 - x0)s - x2 - (x3 - x2)t = 0
    // g(s,t) = y0 + (y1 - y0)s - y2 - (y3 - y2)t = 0
    //
    // df(s,t) / ds = +(x1 - x0)
    // df(s,t) / dt = -(x3 - x2)
    // dg(s,t) / ds = +(y1 - y0)
    // dg(s,t) / dt = -(y3 - y2)
    //
    // J(sn, tn) (dsn, dtn) = -f(sn, tn)
    //
    // (+(x1 - x0)   -(x3 - x2)) (dsn) = -(x0 + (x1 - x0)sn - x2 - (x3 - x2)tn)
    // (+(y1 - y0)   -(y3 - y2)) (dtn) = -(y0 + (y1 - y0)sn - y2 - (y3 - y2)tn)
    // 
    // sm = sn + dsn
    // tm = tn + dtn
    //
    public Boolean SectionSectionIterative(CSegment section0, CSegment section1,
                                           Double epsilon,
                                           out Double ss, out Double ts,
                                           out Double xs, out Double ys)
    {
      ss = 0.0; ts = 0.0;
      xs = 0.0; ys = 0.0;
      Double SN = 1, SM = 0;
      Double TN = 1, TM = 0;
      Double X0 = section0.X0; Double Y0 = section0.Y0;
      Double X1 = section0.X1; Double Y1 = section0.Y1;
      Double X2 = section1.X0; Double Y2 = section1.Y0;
      Double X3 = section1.X1; Double Y3 = section1.Y1;

      for (Int32 RI = 0; RI < 10; RI++)
      {
        Double J00 = +(X1 - X0);
        Double J01 = -(X3 - X2);
        Double J10 = +(Y1 - Y0);
        Double J11 = -(Y3 - Y2);
        Double IT0 = -(X0 + (X1 - X0) * SN - X2 - (X3 - X2) * TN);
        Double IT1 = -(Y0 + (Y1 - Y0) * SN - Y2 - (Y3 - Y2) * TN);
        //
        Double D = J00 * J11 - J01 * J10;
        if (0 == D)
        {
          return false;
        }
        Double DX = IT0 * J11 - J01 * IT1;
        Double DY = J00 * IT1 - IT0 * J10;
        Double DSN = DX / D;
        Double DTN = DY / D;
        SM = SN + DSN;
        TM = TN + DTN;
        if ((Math.Abs(DSN) < epsilon) && (Math.Abs(DTN) < epsilon))
        {
          if ((0 <= SM) && (SM <= 1) && (0 <= TM) && (TM <= 1))
          {
            ss = SM;
            ts = TM;
            xs = X0 + (X1 - X0) * SM;
            ys = Y0 + (Y1 - Y0) * SM;
            return true;
          }
          else
          {
            return false;
          }
        }
        SN = SM;
        TN = TM;
      }
      return false;
    }
    //
    //-----------------------------------------------------------------------------
    //  Section - Iterative - Line - Line
    //-----------------------------------------------------------------------------
    //
    //
    // Intersection: Line-Line
    //
    // s in (-oo..+oo), t in (-oo..+oo)
    // f(s,t) = x0 + (x1 - x0)s - x2 - (x3 - x2)t = 0
    // g(s,t) = y0 + (y1 - y0)s - y2 - (y3 - y2)t = 0
    //
    // df(s,t) / ds = +(x1 - x0)
    // df(s,t) / dt = -(x3 - x2)
    // dg(s,t) / ds = +(y1 - y0)
    // dg(s,t) / dt = -(y3 - y2)
    //
    // J(sn, tn) (dsn, dtn) = -f(sn, tn)
    //
    // (+(x1 - x0)   -(x3 - x2)) (dsn) = -(x0 + (x1 - x0)sn - x2 - (x3 - x2)tn)
    // (+(y1 - y0)   -(y3 - y2)) (dtn) = -(y0 + (y1 - y0)sn - y2 - (y3 - y2)tn)
    // 
    // sm = sn + dsn
    // tm = tn + dtn
    //
    public Boolean LineLineIterative(CLine line0, CLine line1,
                                     Double epsilon,
                                     out Double ss, out Double ts,
                                     out Double xs, out Double ys)
    {
      ss = 0.0; ts = 0.0;
      xs = 0.0; ys = 0.0;
      Double SN = 1, SM = 0;
      Double TN = 1, TM = 0;
      Double X0 = line0.X0; Double Y0 = line0.Y0;
      Double X1 = line0.X1; Double Y1 = line0.Y1;
      Double X2 = line1.X0; Double Y2 = line1.Y0;
      Double X3 = line1.X1; Double Y3 = line1.Y1;

      for (Int32 RI = 0; RI < 10; RI++)
      {
        Double J00 = +(X1 - X0);
        Double J01 = -(X3 - X2);
        Double J10 = +(Y1 - Y0);
        Double J11 = -(Y3 - Y2);
        Double IT0 = -(X0 + (X1 - X0) * SN - X2 - (X3 - X2) * TN);
        Double IT1 = -(Y0 + (Y1 - Y0) * SN - Y2 - (Y3 - Y2) * TN);
        //
        Double D = J00 * J11 - J01 * J10;
        if (0 == D)
        {
          return false;
        }
        Double DX = IT0 * J11 - J01 * IT1;
        Double DY = J00 * IT1 - IT0 * J10;
        Double DSN = DX / D;
        Double DTN = DY / D;
        SM = SN + DSN;
        TM = TN + DTN;
        if ((Math.Abs(DSN) < 0.001) && (Math.Abs(DTN) < 0.001))
        {
          // Lines have no normed parameter! -> NC if ((0 <= SM) && (SM <= 1) && (0 <= TM) && (TM <= 1))
          ss = SM;
          ts = TM;
          xs = X0 + (X1 - X0) * SM;
          ys = Y0 + (Y1 - Y0) * SM;
          return true;
        }
        SN = SM;
        TN = TM;
      }
      return false;
    }
    //
    //-----------------------------------------------------------------------------
    //  Section - Iterative - Section - Line
    //-----------------------------------------------------------------------------
    //
    // Intersection: Section-Line
    //
    // s in [0..1], t in (-oo..+oo)
    // f(s,t) = x0 + (x1 - x0)s - x2 - (x3 - x2)t = 0
    // g(s,t) = y0 + (y1 - y0)s - y2 - (y3 - y2)t = 0
    //
    // df(s,t) / ds = +(x1 - x0)
    // df(s,t) / dt = -(x3 - x2)
    // dg(s,t) / ds = +(y1 - y0)
    // dg(s,t) / dt = -(y3 - y2)
    //
    // J(sn, tn) (dsn, dtn) = -f(sn, tn)
    //
    // (+(x1 - x0)   -(x3 - x2)) (dsn) = -(x0 + (x1 - x0)sn - x2 - (x3 - x2)tn)
    // (+(y1 - y0)   -(y3 - y2)) (dtn) = -(y0 + (y1 - y0)sn - y2 - (y3 - y2)tn)
    // 
    // sm = sn + dsn
    // tm = tn + dtn
    //
    public Boolean SectionLineIterative(CSegment section, CLine line,
                                        Double epsilon,
                                        out Double ss, out Double ts,
                                        out Double xs, out Double ys)
    {
      ss = 0.0; ts = 0.0;
      xs = 0.0; ys = 0.0;
      Double SN = 1, SM = 0;
      Double TN = 1, TM = 0;
      Double X0 = section.X0; Double Y0 = section.Y0;
      Double X1 = section.X1; Double Y1 = section.Y1;
      Double X2 = line.X0; Double Y2 = line.Y0;
      Double X3 = line.X1; Double Y3 = line.Y1;

      for (Int32 RI = 0; RI < 10; RI++)
      {
        Double J00 = +(X1 - X0);
        Double J01 = -(X3 - X2);
        Double J10 = +(Y1 - Y0);
        Double J11 = -(Y3 - Y2);
        Double IT0 = -(X0 + (X1 - X0) * SN - X2 - (X3 - X2) * TN);
        Double IT1 = -(Y0 + (Y1 - Y0) * SN - Y2 - (Y3 - Y2) * TN);
        //
        Double D = J00 * J11 - J01 * J10;
        if (0 == D)
        {
          return false;
        }
        Double DX = IT0 * J11 - J01 * IT1;
        Double DY = J00 * IT1 - IT0 * J10;
        Double DSN = DX / D;
        Double DTN = DY / D;
        SM = SN + DSN;
        TM = TN + DTN;
        if ((Math.Abs(DSN) < 0.001) && (Math.Abs(DTN) < 0.001))
        { // Section normed - Line free
          if ((0 <= SM) && (SM <= 1))
          {
            ss = SM;
            ts = TM;
            xs = X0 + (X1 - X0) * SM;
            ys = Y0 + (Y1 - Y0) * SM;
            return true;
          }
          else
          {
            return false;
          }
        }
        SN = SM;
        TN = TM;
      }
      return false;
    }
    //
    //-----------------------------------------------------------------------------
    //  Section - Iterative - EnvelopeNorm - Section
    //-----------------------------------------------------------------------------
    //
    //
    // Intersection: EnvelopeNorm-Section
    //
    // s in [0..1], t in [0..1]
    // f(s,t) = x3 + R[cos(a + (b - a)t) + [a + (b - a)t] sin(a + (b - a)t)] - x1 - (x2 - x1)s = 0
    // g(s,t) = y3 + R[sin(a + (b - a)t) - [a + (b - a)t] cos(a + (b - a)t)] - y1 - (y2 - y1)s = 0
    //
    // df(s,t) / dt = +R (a + (b - a)t) (b - a) cos(a + (b - a)t)
    // df(s,t) / ds = -(x2 - x1)
    // dg(s,t) / dt = +R (a + (b - a)t) (b - a) sin(a + (b - a)t)
    // dg(s,t) / ds = -(y2 - y1)
    //
    // J(sn, tn) (dsn, dtn) = -f(sn, tn)
    //
    // (-(x2 - x1)   +R (a + (b - a)tn) (b - a) cos(a + (b - a)tn)) (dsn) 
    // (-(y2 - y1)   +R (a + (b - a)tn) (b - a) sin(a + (b - a)tn)) (dtn) 
    //
    //        = -(x3 + R[cos(a + (b - a)tn + [a + (b - a)tn] sin(a + (b - a)tn)] - x1 - (x2 - x1)sn)
    //        = -(y3 + R[sin(a + (b - a)tn) - [a + (b - a)t] cos(a + (b - a)tn)] - y1 - (y2 - y1)sn)
    // 
    // sm = sn + dsn
    // tm = tn + dtn
    //
    public Boolean EnvelopeNormSectionIterative(CEnvelopeNorm envelope, CSegment section,
                                                Double epsilon,
                                                out Double ss, out Double ts,
                                                out Double xs, out Double ys)
    {
      ss = 0.0; ts = 0.0;
      xs = 0.0; ys = 0.0;
      Double SN = 1, SM = 0;
      Double TN = 1, TM = 0;
      Double X0 = envelope.X0; Double Y0 = envelope.Y0;
      Double X1 = envelope.X1; Double Y1 = envelope.Y1;
      Double X2 = section.X0; Double Y2 = section.Y0;
      Double X3 = section.X1; Double Y3 = section.Y1;

      for (Int32 RI = 0; RI < 10; RI++)
      {
        Double J00 = +(X1 - X0);
        Double J01 = -(X3 - X2);
        Double J10 = +(Y1 - Y0);
        Double J11 = -(Y3 - Y2);
        Double IT0 = -(X0 + (X1 - X0) * SN - X2 - (X3 - X2) * TN);
        Double IT1 = -(Y0 + (Y1 - Y0) * SN - Y2 - (Y3 - Y2) * TN);
        //
        Double D = J00 * J11 - J01 * J10;
        if (0 == D)
        {
          return false;
        }
        Double DX = IT0 * J11 - J01 * IT1;
        Double DY = J00 * IT1 - IT0 * J10;
        Double DSN = DX / D;
        Double DTN = DY / D;
        SM = SN + DSN;
        TM = TN + DTN;
        if ((Math.Abs(DSN) < 0.001) && (Math.Abs(DTN) < 0.001))
        {
          // Lines have no normed parameter! -> NC if ((0 <= SM) && (SM <= 1) && (0 <= TM) && (TM <= 1))
          ss = SM;
          ts = TM;
          xs = X0 + (X1 - X0) * SM;
          ys = Y0 + (Y1 - Y0) * SM;
          return true;
        }
        SN = SM;
        TN = TM;
      }
      return false;
    }

    //
    // ...ok
    //-----------------------------------------------------------------------------
    //  Section - Iterative - EnvelopeFree - Section
    //-----------------------------------------------------------------------------
    //
    // Intersection: EnvelopeFree-Section
    //
    // t in [0..+oo), s in [0..1]
    // f(t, s) = xC + R[cos(2 PI t) + [2 PI t] sin(2 PI t)] - x0 - (x1 - x0)s = 0
    // g(t, s) = yC + R[sin(2 PI t) - [2 PI t] cos(2 PI t)] - y0 - (y1 - y0)s = 0
    //
    // df(t, s) / dt = +4 PI^2 R t cos(2 PI t)
    // df(t, s) / ds = -(x1 - x0)
    // dg(t, s) / dt = +4 PI^2 R t sin(2 PI t)
    // dg(t, s) / ds = -(y1 - y0)
    //
    // J(tn, sn) (dtn, dsn) = -f(tn, sn) // !!! -f(tn, sn) !!!
    //
    // (+4 PI^2 R tn cos(2 PI tn)   -(x2 - x1)) (dtn) 
    // (+4 PI^2 R tn sin(2 PI tn)   -(y2 - y1)) (dsn) 
    //        = -(xC + R[cos(2 PI tn) + 2 PI tn sin(2 PI tn)] - x0 - (x1 - x0)sn)
    //        = -(yC + R[sin(2 PI tn) - 2 PI tn cos(2 PI tn)] - y0 - (y1 - y0)sn)
    // 
    // -> tm = tn + dtn 
    // -> sm = sn + dsn
    //
    public Boolean EnvelopeFreeSectionIterative(CEnvelopeFree envelope, CSegment section,
                                                Double epsilon, Int32 countiteration,
                                                out Double ss, out Double ts,
                                                out Double xs, out Double ys)
    {
      ts = 0.0; ss = 0.0;
      xs = 0.0; ys = 0.0;
      Double TN = 0.5, TM = TN;
      Double SN = 1, SM = SN;
      Double XC = envelope.XC; Double YC = envelope.YC;
      Double R = envelope.R;
      Double X0 = section.X0; Double Y0 = section.Y0;
      Double X1 = section.X1; Double Y1 = section.Y1;
      Double F4P2R = +4 * Math.PI * Math.PI * R;
      for (Int32 RI = 0; RI < countiteration; RI++)
      {
        Double A = 2 * Math.PI * TN;
        Double J00 = F4P2R * TN * Math.Cos(A);
        Double J01 = +(X1 - X0);
        Double J10 = F4P2R * TN * Math.Sin(A);
        Double J11 = +(Y1 - Y0);
        Double IT0 = -(XC + R * Math.Cos(A) + A * R * Math.Sin(A) - X0 - (X1 - X0) * SN);
        Double IT1 = -(YC + R * Math.Sin(A) - A * R * Math.Cos(A) - Y0 - (Y1 - Y0) * SN);
        //
        Double D = J00 * J11 - J01 * J10;
        if (0 == D)
        {
          return false;
        }
        Double DT = IT0 * J11 - J01 * IT1;
        Double DS = J00 * IT1 - IT0 * J10;
        Double DTN = DT / D;
        Double DSN = DS / D;
        TM = TN + DTN;
        SM = SN + DSN;
        Console.WriteLine(String.Format("Tn={0} Sn={1} DT={2} DS={3}", TM, SM, DTN, DSN));
        if ((Math.Abs(DTN) < 0.001))//&& (Math.Abs(DSN) < 0.001))
        {
          //!!!if ((0 <= SM) && (SM <= 1)) // EnvelopeFree!!! && (0 <= TM) && (TM <= 1))          
          //ts = TM; ss = 0.7;// SM;
          //xs = X0 + (X1 - X0) * ss;
          //ys = Y0 + (Y1 - Y0) * ss;
          ts = TM; ss = 0.7;// SM;
          xs = XC + R * Math.Cos(2 * Math.PI * ts) + 2 * Math.PI * ts * R * Math.Sin(2 * Math.PI * ts);
          ys = YC + R * Math.Sin(2 * Math.PI * ts) - 2 * Math.PI * ts * R * Math.Cos(2 * Math.PI * ts);
          return true;
        }
        TN = TM; SN = SM;
      }
      return false;
    }
    //
    // ...testing
    //-----------------------------------------------------------------------------
    //  Circle - Iterative - EnvelopeFree - Circle
    //-----------------------------------------------------------------------------
    //
    // Intersection: EnvelopeFree-Circle
    //
    // t in [0..+oo), s in [0..1]
    // f(t, s) = x0 + R0[cos(2 PI t) + [2 PI t] sin(2 PI t)] - x1 - R1 cos(2 PI s) = 0
    // g(t, s) = y0 + R0[sin(2 PI t) - [2 PI t] cos(2 PI t)] - y1 - R1 sin(2 PI s) = 0
    //
    // df(t, s) / dt = +4 PI^2 R0 t cos(2 PI t)
    // df(t, s) / ds = +2 PI R1 sin(2 PI s)
    // dg(t, s) / dt = +4 PI^2 R0 t sin(2 PI t)
    // dg(t, s) / ds = -2 PI R1 cos(2 PI s)
    //
    // J(tn, sn) (dtn, dsn) = -f(tn, sn) // !!! -f(tn, sn) !!!
    //
    // (+4 PI^2 R tn cos(2 PI tn)   +2 PI R1 sin(2 PI s)) (dtn) 
    // (+4 PI^2 R tn sin(2 PI tn)   -2 PI R1 cos(2 PI s)) (dsn) 
    //        = -(x0 + R0[cos(2 PI t) + [2 PI t] sin(2 PI t)] - x1 - R1 cos(2 PI s))
    //        = -(y0 + R0[sin(2 PI t) - [2 PI t] cos(2 PI t)] - y1 - R1 sin(2 PI s))
    // 
    // -> tm = tn + dtn 
    // -> sm = sn + dsn
    //
    public Boolean EnvelopeFreeCircleIterative(CEnvelopeFree envelope, CCircle circle,
                                               Double epsilon, Int32 countiteration,
                                               ref Double ts, ref Double ss,
                                               out Double xs, out Double ys)
    {
      // NC ts = 0.0; ss = 0.0;
      xs = 0.0; ys = 0.0;
      //Double TN = 0.25, TM = TN; // R1 = 2.8
      //Double SN = 0.75, SM = SN;
      //Double TN = 0.3, TM = TN;
      //Double SN = 0.2, SM = SN;
      Double TN = ts, TM = TN;
      Double SN = ss, SM = SN;
      Double X0 = envelope.XC; Double Y0 = envelope.YC;
      Double R0 = envelope.R;
      Double X1 = circle.XC; Double Y1 = circle.YC;
      Double R1 = circle.R;
      //
      for (Int32 RI = 0; RI < countiteration; RI++)
      {
        Double A = 2 * Math.PI * TN;
        Double B = 2 * Math.PI * SN;
        Double J00 = 4 * Math.PI * Math.PI * R0 * TN * Math.Cos(A);   // +4 PI^2 R0 t cos(2 PI t)
        Double J01 = 2 * Math.PI * R1 * Math.Sin(B);                  // +2 PI R1 sin(2 PI s)
        Double J10 = 4 * Math.PI * Math.PI * R0 * TN * Math.Sin(A);   // +4 PI^2 R0 t sin(2 PI t)
        Double J11 = 2 * Math.PI * R1 * Math.Cos(B);                  // -2 PI R1 cos(2 PI s)
        Double IT0 = -(X0 + R0 * Math.Cos(A) + R0 * A * Math.Sin(A) - X1 - R1 * Math.Cos(B));
        Double IT1 = -(Y0 + R0 * Math.Sin(A) - R0 * A * Math.Cos(A) - Y1 - R1 * Math.Sin(B));
        //
        Double D = J00 * J11 - J01 * J10;
        if (0 == D)
        {
          return false;
        }
        Double DT = IT0 * J11 - J01 * IT1;
        Double DS = J00 * IT1 - IT0 * J10;
        Double DTN = DT / D;
        Double DSN = DS / D;
        TM = TN + DTN;
        SM = SN + DSN;
        Console.WriteLine(String.Format("Tn={0} Sn={1} DT={2} DS={3}", TM, SM, DTN, DSN));
        if ((Math.Abs(DTN) < 0.001) && (Math.Abs(DSN) < 0.001))
        {           
          ts = TM; ss = SM;
          xs = X1 + R1 * Math.Cos(2 * Math.PI * ss);
          ys = Y1 + R1 * Math.Sin(2 * Math.PI * ss);
          // debug xs = XC + R * Math.Cos(2 * Math.PI * ts) + 2 * Math.PI * ts * R * Math.Sin(2 * Math.PI * ts);
          // debug ys = YC + R * Math.Sin(2 * Math.PI * ts) - 2 * Math.PI * ts * R * Math.Cos(2 * Math.PI * ts);
          return true;
        }
        TN = TM; SN = SM;
      }
      return false;
    }

    private Boolean BuildEnvelopeVector(CEnvelopeFree envelope,
                                        Double tlow, Double thigh, Int32 tcount,
                                        out Double[] vectorx, out Double[] vectory)
    {
      vectorx = null;
      vectory = null;
      try
      {
        Int32 TCount = Math.Max(2, tcount - 1);
        vectorx = new Double[TCount]; 
        vectory = new Double[TCount];
        Double DT = (thigh - tlow) / TCount;
        Double XC = envelope.XC; Double YC = envelope.YC;
        Double R = envelope.R;
        for (Int32 TI = 0; TI < TCount; TI++)
        {
          Double T = tlow + TI * DT;
          Double A = 2 * Math.PI * T;
          vectorx[TI] = XC + R * Math.Cos(A) + R * A * Math.Sin(A);
          vectory[TI] = YC + R * Math.Sin(A) - R * A * Math.Cos(A);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean BuildCircleVector(CCircle circle,
                                      Double tlow, Double thigh, Int32 tcount,
                                      out Double[] vectorx, out Double[] vectory)
    {
      vectorx = null;
      vectory = null;
      try
      {
        Int32 TCount = Math.Max(2, tcount - 1);
        vectorx = new Double[TCount];
        vectory = new Double[TCount];
        Double DT = (thigh - tlow) / TCount;
        Double XC = circle.XC; Double YC = circle.YC;
        Double R = circle.R;
        for (Int32 TI = 0; TI < TCount; TI++)
        {
          Double T = tlow + TI * DT;
          Double A = 2 * Math.PI * T;
          vectorx[TI] = XC + R * Math.Cos(A);
          vectory[TI] = YC + R * Math.Sin(A);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean FindDistanceMinimum(Double[] vectorx0, Double[] vectory0,
                                        Double[] vectorx1, Double[] vectory1,
                                        out Int32 index0, out Int32 index1)
    {
      index0 = 0; index1 = 0;
      //
      try
      {
        Int32 VI0Count = Math.Min(vectorx0.Length, vectory0.Length);
        Int32 VI1Count = Math.Min(vectorx1.Length, vectory1.Length);
        //
        Double DX2 = vectorx1[0] - vectorx0[0];
        Double D2Minimum = DX2 * DX2;
        Double DY2 = vectory1[0] - vectory0[0];
        D2Minimum += DY2 * DY2;
        //
        for (Int32 VI1 = 0; VI1 < VI1Count; VI1++)
        {
          for (Int32 VI0 = 0; VI0 < VI0Count; VI0++)
          {
            DX2 = vectorx1[VI1] - vectorx0[VI0];
            Double D2 = DX2 * DX2;
            DY2 = vectory1[VI1] - vectory0[VI0];
            D2 += DY2 * DY2;
            if (D2 < D2Minimum)
            {
              D2Minimum = D2;
              index0 = VI0;
              index1 = VI1;
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean FindEnvelopeFreeCircleStartPoint(CEnvelopeFree envelope,
                                                     Double tlow, Double thigh, Int32 tcount,
                                                     CCircle circle,
                                                     Double slow, Double shigh, Int32 scount,
                                                     out Double tstart, out Double sstart)
    {
      tstart = 0.0; sstart = 0.0;
      try
      {
        Double[] VectorEnvelopeX, VectorEnvelopeY;
        Double[] VectorCircleX, VectorCircleY;
        if (BuildEnvelopeVector(envelope, tlow, thigh, tcount,
                                out VectorEnvelopeX, out VectorEnvelopeY))
        {
          if (BuildCircleVector(circle, slow, shigh, scount,
                                out VectorCircleX, out VectorCircleY))
          {
            Int32 IndexEnvelope, IndexCircle;
            if (FindDistanceMinimum(VectorEnvelopeX, VectorEnvelopeY,
                                    VectorCircleX, VectorCircleY,
                                    out IndexEnvelope, out IndexCircle))
            {
              Double DT = (thigh - tlow) / VectorEnvelopeX.Length;
              tstart = IndexEnvelope * DT;
              Double DS = (shigh - slow) / VectorCircleX.Length;
              sstart = IndexCircle * DS;
              return true;
            }
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean EnvelopeFreeCircleIterative(CEnvelopeFree envelope, 
                                               Double tlow, Double thigh, Int32 tcount,
                                               CCircle circle,
                                               Double slow, Double shigh, Int32 scount,
                                               Double epsilon, Int32 countiteration,
                                               out Double ss, out Double ts,
                                               out Double xs, out Double ys)
    {
      ts = 0.0; ss = 0.0;
      xs = 0.0; ys = 0.0;
      //
      Double TStart, SStart;
      if (FindEnvelopeFreeCircleStartPoint(envelope, tlow, thigh, tcount,
                                           circle, slow, shigh, scount,
                                           out TStart, out SStart))
      {
        ts = TStart; ss = SStart;
        if (EnvelopeFreeCircleIterative(envelope, circle,
                                        epsilon, countiteration,
                                        ref ts, ref ss,
                                        out xs, out ys))
        {
          return true;
        }
      }
      return false;
    }



















  }
}
