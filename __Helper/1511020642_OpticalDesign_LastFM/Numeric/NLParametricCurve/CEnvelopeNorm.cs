﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLParametricCurve
{ // *** Envelope *** (Kreisevolvente)
  //
  // *** Envelope Equation Parameter Normed
  // - Two Points P0 = (x0, y0), P1 = (x1, y1)) for Start/End-Angle
  //      alpha = arctan((y0 - yC) / (x0 - xC))
  //      beta = arctan((y1 - yC) / (x1 - xC))
  // - Middlepoint PC = (xC, yC) and Radius R
  // - Parameter t : t in [0, 1]
  // - x(t) = xC + R cos(alpha + (beta - alpha) t) + [alpha + (beta - alpha)] * t * R * sin(alpha + (beta - alpha) t)
  // - y(t) = yC + R sin(alpha + (beta - alpha) t) - [alpha + (beta - alpha)] * t * R * cos(alpha + (beta - alpha) t)
  // - S = (beta^2 - alpha^2) R / 2
  //
  public class CEnvelopeNorm : CParametric
  {
    //
    //----------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------
    //
    private const Double INIT_XREAL_0 = 1.0;
    private const Double INIT_YREAL_0 = 0.0;
    private const Double INIT_XREAL_1 = -1.0;
    private const Double INIT_YREAL_1 = 0.0;
    private const Double INIT_XREAL_C = 0.0;
    private const Double INIT_YREAL_C = 0.0;
    //
    //----------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------
    // local parameter - Point Middle
    private Double FXC, FYC;
    // local parameter - Point Start / End
    private Double FX0, FY0;
    private Double FX1, FY1;
    //
    //----------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------
    //
    public CEnvelopeNorm(CRealPixel realpixel)
      : base(realpixel)
    {
      FX0 = INIT_XREAL_0;
      FY0 = INIT_YREAL_0;
      FX1 = INIT_XREAL_1;
      FY1 = INIT_YREAL_1;
      FXC = INIT_XREAL_C;
      FYC = INIT_YREAL_C;
      BuildRealVector();
      BuildPixelVector();
    }

    public Double XC
    {
      get { return FXC; }
      set { FXC = value; }
    }
    public Double YC
    {
      get { return FYC; }
      set { FYC = value; }
    }

    public Double X0
    {
      get { return FX0; }
      set { FX0 = value; }
    }
    public Double Y0
    {
      get { return FY0; }
      set { FY0 = value; }
    }

    public Double X1
    {
      get { return FX1; }
      set { FX1 = value; }
    }
    public Double Y1
    {
      get { return FY1; }
      set { FY1 = value; }
    }

    private Double GetRadius()
    {
      return Math.Sqrt((FX0 - FXC) * (FX0 - FXC) + (FY0 - FYC) * (FY0 - FYC));
    }
    public Double R
    {
      get { return GetRadius(); }
    }

    public Double A
    {
      get { return CMath.ArcTan2(FY0 - FYC, FX0 - FXC); }
    }
    public Double B
    {
      get { return CMath.ArcTan2(FY1 - FYC, FX1 - FXC); }
    }


    protected override Double GetLength()
    {
      return Math.Sqrt(22);
    }

    private Double PointAngle(Double xr, Double yr)
    {
      return CMath.ArcTan2(yr - FYC, xr - FXC);
    }
    
    public override Boolean BuildRealVector()
    {
      try
      { // here: normed parameter
        Double A0 = PointAngle(FX0, FY0);
        Double A1 = PointAngle(FX1, FY1);
        Console.WriteLine(String.Format("A0:{0} A1:{1}", A0, A1));
        Double DT = 1.0 / (FCountResolution - 0);
        for (Int32 Index = 0; Index <= FCountResolution; Index++)
        {
          Double T = Index * DT;
          Double A = A0 + (A1 - A0) * T;
          Double CA = Math.Cos(A);
          Double SA = Math.Sin(A);
          Double XR = FXC + R * (CA + A * SA);
          Double YR = FYC + R * (SA - A * CA);     
          //
          FXRealVector[Index] = XR;
          FYRealVector[Index] = YR;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      const Int32 R = 1;
      Int32 IH = FXPixelVector.Length - 1;
      Pen PL = new Pen(Color.FromArgb(0xFF, 0xEE, 0xCC, 0xDD), 3);
      Pen PP = new Pen(Color.FromArgb(0xFF, 0xFF, 0x55, 0x66), 1);
      Int32 R2 = 2 * R;
      for (Int32 Index = 0; Index < IH; Index++)
      {
        Int32 XP = FXPixelVector[Index];
        graphics.DrawLine(PL, FXPixelVector[Index], FYPixelVector[Index],
                          FXPixelVector[1 + Index], FYPixelVector[1 + Index]);
        graphics.DrawEllipse(PP, FXPixelVector[Index] - R, FYPixelVector[Index] - R, R2, R2);
      }
      graphics.DrawEllipse(PP, FXPixelVector[IH] - R, FYPixelVector[IH] - R, R2, R2);
      return true;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //

  }
}
