﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLParametricCurve
{
  public static class CMath
  {
    //
    //#################################################################################################
    //
    public static Double DegreeRadian(Double degree)
    {
      return Math.PI * degree / 180.0;
    }

    public static Double RadianDegree(Double radian)
    {
      return 180.0 * radian / Math.PI;
    }

    public static Double NormalizeAngleDegree(Double angledegree) // [deg] -> [deg]
    {
      Double AD = angledegree;
      AD %= 360.0;
      if (AD < 0)
      {
        AD += 180;
      }
      return AD;
    }

    public static Double ArcTan2(Double dy, Double dx)
    { // Wikipedia https://de.wikipedia.org/wiki/Arkustangens_und_Arkuskotangens#atan2
      if (0 < dx)
      {
        if (0 <= dy)
        {
          return Math.Atan2(dy, dx);
        } 
        else
          if (dy < 0)
          {
            return 2 * Math.PI + Math.Atan2(dy, dx);
          }
      } 
      if (dx < 0)
      {
        if (0 <= dy)
        {
          return Math.Atan2(dy, dx); 
        }
        return Math.Atan2(dy, dx) + 2 * Math.PI;
      }
      // 0 == dx
      if (dy < 0)
      {
        return 3 * Math.PI / 2.0;
      } 
      else
        if (0 < dy)
        {
          return Math.PI / 2.0;
        }
      // 0 == dx and 0 == dy
      return 0;
    }
    //
    //#################################################################################################
    //
    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Conversion - Line - SlopeIntercept -> DualPoint
    //                                y = m x + b    -> (y - y0) / (y1 - y0) = (x - x0) / (x1 - x0) 
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean LineSlopeInterceptToDualPoint(Double m, Double b,
                                                        out Double x0, out Double y0,
                                                        out Double x1, out Double y1)
    {
      x0 = 0;
      y0 = b;
      x1 = 1;
      y1 = m + b;
      return true;
    }
    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Conversion - Line - DualPoint    -> SlopeIntercept
    // (y - y0) / (y1 - y0) = (x - x0) / (x1 - x0) -> y = m x + b
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean LineDualPointToSlopeIntercept(Double x0, Double y0,
                                                        Double x1, Double y1,
                                                        out Double m, out Double b)
    {
      m = 1E10;
      b = 1E10;
      if (x0 != x1)
      {
        m = (y1 - y0) / (x1 - x0);
        b = y0 - m * x0;
        return true;
      }
      return false;
    }
    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Conversion - Line - Coefficient   -> DualPoint
    //                                a x + b y = c -> (y - y0) / (y1 - y0) = (x - x0) / (x1 - x0) 
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean LineCoefficientToDualPoint(Double a, Double b, Double c,
                                                     out Double x0, out Double y0,
                                                     out Double x1, out Double y1)
    {
      if (0 != b)
      {
        x0 = 0;
        y0 = c / b;
        x1 = 1;
        y1 = (c - a) / b;
        return true;
      }
      x0 = 0; y0 = -1E10;
      x1 = 0; y1 = +1E10;
      return false;
    }
    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Conversion - Line - DualPoint     -> Coefficient
    //  (y - y0) / (y1 - y0) = (x - x0) / (x1 - x0) -> a x + b y = c
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean LineDualPointToCoefficient(Double x0, Double y0,
                                                     Double x1, Double y1,
                                                     out Double a, out Double b, out Double c)
    {
      a = -(y1 - y0);
      b = +(x1 - x0);
      c = (x1 - x0) * y0 - (y1 - y0) * x0;
      return true;
    }
    //
    //#################################################################################################
    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Calculate - Line - Point 
    //  yl = y0 + (xl - x0) * (y1 - y0) / (x1 - x0)
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean LineCalculateY(Double x0, Double y0,
                                         Double x1, Double y1,
                                         Double x, out Double y)
    {
      y = 1E10;
      if (x1 != x0)
      {
        y = y0 + (x - x0) * (y1 - y0) / (x1 - x0);
        return true;
      }
      return false;
    }
    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Conversion - Circle - DualPoint     -> Coefficient
    //  (x - xc)^2 + (y - yc)^2 = (x0 - xc)^2 + (y0 - yc)^2  -> (x - xc)^2 + (y0 - yc)^2 = r^2
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean CircleDualPointToRadius(Double xc, Double yc,
                                                  Double x0, Double y0,
                                                  out Double r)
    {
      Double DX = x0 - xc;
      Double DY = y0 - yc;
      r = DX * DX;
      r += DY * DY;
      r = Math.Sqrt(r);
      return true;
    }
    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Conversion - Arc - TriplePoint     -> Radius, Angle0, Angle1
    //  
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean ArcTriplePointToRadiusAngle(Double xc, Double yc,
                                                      Double x0, Double y0,
                                                      Double x1, Double y1,
                                                      out Double r, out Double a0, out Double a1)
    {
      Double DX = x0 - xc;
      Double DY = y0 - yc;
      r = DX * DX;
      r += DY * DY;
      r = Math.Sqrt(r);
      //
      a0 = CMath.ArcTan2(DY, DX);
      //
      DX = x1 - xc;
      DY = y1 - yc;
      a1 = CMath.ArcTan2(DY, DX);
      return true;
    }

    //
    //#################################################################################################
    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Intersection - Line - Line
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean IntersectionLineLine(Double a1, Double b1, Double c1, // L1: a1 x + b1 y = c1
                                               Double a2, Double b2, Double c2, // L2: a2 x + b2 y = c2
                                               out Double[] xs, out Double[] ys)
    {
      xs = null;
      ys = null;
      try
      {
        Double Dx = c1 * b2 - b1 * c2;
        Double Dy = a1 * c2 - c1 * a2;
        Double D = a1 * b2 - b1 * a2;
        if (0 == D)
        { // no intersection!
          return false;
        }
        // single intersection
        xs = new Double[1];
        ys = new Double[1];
        xs[0] = Dx / D;
        ys[0] = Dy / D;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public static Boolean IntersectionLineSegment(Double linex0, Double liney0, // Line P0
                                                  Double linex1, Double liney1, // Line P1
                                                  Double segmentx0, Double segmenty0,
                                                  Double segmentx1, Double segmenty1,
                                                  out Double[] xs, out Double[] ys)                                                  
    {
      xs = null;
      ys = null;
      try
      {
        Double LA, LB, LC;
        LineDualPointToCoefficient(linex0, liney0, linex1, liney1, out LA, out LB, out LC);
        Double SA, SB, SC;
        LineDualPointToCoefficient(segmentx0, segmenty0, segmentx1, segmenty1, out SA, out SB, out SC);
        Double DX = LC * SB - LB * SC;
        Double DY = LA * SC - LC * SA;
        Double D = LA * SB - LB * SA;
        if (0 == D)
        { // no intersection!
          return false;
        }
        // single intersection
        xs = new Double[1];
        ys = new Double[1];
        xs[0] = DX / D;
        ys[0] = DY / D;
        // Intersection element of Segment?
        Double TX, TY;
        if ((segmentx1 != segmentx0) && (segmenty1 != segmenty0))
        {
          TX = (xs[0] - segmentx0) / (segmentx1 - segmentx0);
          TY = (ys[0] - segmenty0) / (segmenty1 - segmenty0);
          if (TX == TY) 
          {
            if ((0 <= TX) && (TX <= 1))
            {
              return true;
            }
          }
         }
          else
          if (segmentx1 != segmentx0)
          {
            TX = (xs[0] - segmentx0) / (segmentx1 - segmentx0);
            if ((0 <= TX) && (TX <= 1))
            {
              return true;
            }
          }
          else
            if (segmenty1 != segmenty0)
            {
              TY = (ys[0] - segmenty0) / (segmenty1 - segmenty0);
              if ((0 <= TY) && (TY <= 1))
              {
                return true;
              }
            }
            else
            { // Error
            }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Intersection - Line - Circle
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean IntersectionLineCircle(Double a, Double b, Double c, // L: a x + b y = c
                                                 Double xc, Double yc, Double r, // C: (x - xc)^2 + (y - yc)^2 = r^2
                                                 out Double[] xs, out Double[] ys)
    {
      xs = null;
      ys = null;
      try
      {
        Double d = c - a * xc - b * yc;
        Double D = (a * a + b * b) * r * r - d * d;
        if (D < 0)
        { // no intersection!
          return false;
        }
        if (0 == D)
        { // single intersection
          xs = new Double[1];
          ys = new Double[1];
          xs[0] = xc + a * d / (a * a + b * b);
          ys[0] = yc + a * d / (a * a + b * b);
          return true;
        }
        // dual intersection
        xs = new Double[2];
        ys = new Double[2];
        D = Math.Sqrt(D);
        xs[0] = xc + (a * d + b * D) / (a * a + b * b);
        ys[0] = yc + (b * d - a * D) / (a * a + b * b);
        xs[1] = xc + (a * d - b * D) / (a * a + b * b);
        ys[1] = yc + (b * d + a * D) / (a * a + b * b);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean IntersectionLineCircle(Double x0, Double y0, Double x1, Double y1, // L: P0, P1
                                                 Double xc, Double yc, Double r, // C: (x - xc)^2 + (y - yc)^2 = r^2
                                                 out Double[] xs, out Double[] ys)
    {
      xs = null;
      ys = null;
      try
      {
        Double A, B, C;
        Boolean Result = LineDualPointToCoefficient(x0, y0, x1, y1, out A, out B, out C);
        Result &= IntersectionLineCircle(A, B, C,
                                         xc, yc, r,
                                         out xs, out ys);
        return Result;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean IntersectionLineCircle(Double x0line, Double y0line,  // L: P0, P1
                                                 Double x1line, Double y1line,  // L: P0, P1
                                                 Double xccircle, Double yccircle,  // C: (x - xc)^2 + (y - yc)^2 = r^2
                                                 Double x0circle, Double y0circle,
                                                 // only Arc!!!Double x1circle, Double y1circle, 
                                                 out Double[] xs, out Double[] ys)
    {
      xs = null;
      ys = null;
      try
      {
        Double ALine, BLine, CLine;
        Boolean Result = LineDualPointToCoefficient(x0line, y0line, x1line, y1line, out ALine, out BLine, out CLine);
        Double XCCircle = xccircle;
        Double YCCircle = yccircle;
        Double RCircle;
        Result &= CircleDualPointToRadius(xccircle, yccircle,
                                          x0circle, y0circle,
                                          out RCircle);                                                 
        Result &= IntersectionLineCircle(ALine, BLine, CLine,
                                         XCCircle, YCCircle, RCircle,
                                         out xs, out ys);
        return Result;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Intersection - Line - Arc
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean IntersectionLineArc(Double a, Double b, Double c, // L: a x + b y = c
                                              Double xc, Double yc, Double r, // A: (x - xc)^2 + (y - yc)^2 = r^2
                                              Double a0arc, Double a1arc,     // A: A0 -> A1 
                                              out Double[] xs, out Double[] ys)
    {
      xs = null;
      ys = null;
      try
      {
        Double[] XS, YS;
        // Find all Intersections [0, 1, 2]
        Boolean Result = IntersectionLineCircle(a, b, c, xc, yc, r, out XS, out YS);
        if (Result)
        { // Test: Are Intersections [1, 2] on Arc [A0 .. A1]?
          Double AS0, AS1, DX, DY;
          if (1 < XS.Length)
          { // Two Intersections on Circle -> Test [0, 1, 2] Intersections on Arc
            DX = XS[0] - xc;
            DY = YS[0] - yc;
            AS0 = CMath.ArcTan2(DY, DX);
            DX = XS[1] - xc;
            DY = YS[1] - yc;
            AS1 = CMath.ArcTan2(DY, DX);
            // check if a1 < a0
            if (a0arc <= a1arc)
            {
              if ((a0arc <= AS0) && (AS0 <= a1arc) &&
                  (a0arc <= AS1) && (AS1 <= a1arc))
              { // Two Intersections on Arc
                xs = new Double[2];
                xs[0] = XS[0];
                xs[1] = XS[1];
                ys = new Double[2];
                ys[0] = YS[0];
                ys[1] = YS[1];
                return true;
              }
              if ((a0arc <= AS0) && (AS0 <= a1arc))
              { // One Intersection on Arc (PS0)
                xs = new Double[1];
                xs[0] = XS[0];
                ys = new Double[1];
                ys[0] = YS[0];
                return true;
              }
              if ((a0arc <= AS1) && (AS1 <= a1arc))
              { // One Intersection on Arc (PS1)
                xs = new Double[1];
                xs[0] = XS[1];
                ys = new Double[1];
                ys[0] = YS[1];
                return true;
              }
            }
            else
            { // a0arc > a1arc -> jump over 2Pi
              Double PID = 2.0 * Math.PI;
              Boolean HS0 = (a0arc <= AS0) && (AS0 <= PID);
              Boolean LS0 = (0 <= AS0) && (AS0 <= a1arc);
              Boolean S0 = HS0 || LS0;
              Boolean HS1 = (a0arc <= AS1) && (AS1 <= PID);
              Boolean LS1 = (0 <= AS1) && (AS1 <= a1arc);
              Boolean S1 = HS1 || LS1;
              if (S0 && S1)
              { // Two Intersections on Arc
                xs = new Double[2];
                xs[0] = XS[0];
                xs[1] = XS[1];
                ys = new Double[2];
                ys[0] = YS[0];
                ys[1] = YS[1];
                return true;
              }
              if (S0)
              { // One Intersection on Arc (PS0)
                xs = new Double[1];
                xs[0] = XS[0];
                ys = new Double[1];
                ys[0] = YS[0];
                return true;
              }
              if (S1)
              { // One Intersection on Arc (PS1)
                xs = new Double[1];
                xs[0] = XS[1];
                ys = new Double[1];
                ys[0] = YS[1];
                return true;
              }
            }
          }
          if (0 < XS.Length)
          { // One Intersection on Circle -> Test [0, 1] Intersections on Arc
            DX = XS[0] - xc;
            DY = YS[0] - yc;
            AS0 = CMath.ArcTan2(DY, DX);
            if (a0arc <= a1arc)
            { // AS0 in [a0arc .. a1arc]?
              if ((a0arc <= AS0) && (AS0 <= a1arc))
              { // One Intersection on Arc
                xs = new Double[1];
                xs[0] = XS[0];
                ys = new Double[1];
                ys[0] = YS[0];
                return true;
              }
            }
            else
            { // a0arc > a1arc -> jump over 2Pi
              Double PID = 2.0 * Math.PI;
              Boolean HS0 = (a0arc <= AS0) && (AS0 <= PID);
              Boolean LS0 = (0 <= AS0) && (AS0 <= a1arc);
              Boolean S0 = HS0 || LS0;
              if (S0)
              { // One Intersection on Arc
                xs = new Double[1];
                xs[0] = XS[0];
                ys = new Double[1];
                ys[0] = YS[0];
                return true;
              }
            }
          }
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean IntersectionLineArc(Double x0line, Double y0line,  // L: P0, P1
                                              Double x1line, Double y1line,  // L: P0, P1
                                              Double xcarc, Double ycarc,  // C: (x - xc)^2 + (y - yc)^2 = r^2
                                              Double x0arc, Double y0arc,
                                              Double x1arc, Double y1arc, 
                                              out Double[] xs, out Double[] ys)
    {
      xs = null;
      ys = null;
      try
      {
        Double ALine, BLine, CLine;
        Boolean Result = LineDualPointToCoefficient(x0line, y0line, x1line, y1line, out ALine, out BLine, out CLine);
        Double RArc, A0Arc, A1Arc;
        Result &= ArcTriplePointToRadiusAngle(xcarc, ycarc,
                                              x0arc, y0arc,
                                              x1arc, y1arc,
                                              out RArc, out A0Arc, out A1Arc);
        Result &= IntersectionLineArc(ALine, BLine, CLine,
                                      xcarc, ycarc, 
                                      RArc, A0Arc, A1Arc,
                                      out xs, out ys);
        return Result;
      }
      catch (Exception)
      {
        return false;
      }
    }

    //
    //-------------------------------------------------------------------------------------------------
    //  Section - Intersection - Circle - Circle
    //-------------------------------------------------------------------------------------------------
    //
    public static Boolean IntersectionCircleCircle(Double m1, Double n1, Double r1, // C1: (x - m1)^2 + (y - n1)^2 = r1^2
                                                   Double m2, Double n2, Double r2, // C2: (x - m2)^2 + (y - n2)^2 = r2^2
                                                   out Double[] xs, out Double[] ys)
    {
      xs = null;
      ys = null;
      try
      {
        Double a = 2 * (m2 - m1);
        Double b = 2 * (n2 - n1);
        Double c = r1 * r1  - r2 * r2 - m1 * m1 - n1 * n1 +m2 * m2 + n2 * n2;
        Double d = c - a * m1 - b * n1;
        Double D = r1 * r1 *(a * a + b * b) - d;
        if (D < 0)
        { // no intersection!
          return false;
        }
        if (0 == D)
        { // single intersection
          xs = new Double[1];
          ys = new Double[1];
          xs[0] = m1 + a * d / (a * a + b * b);
          ys[0] = n1 + a * d / (a * a + b * b);
        }
        // dual intersection
        xs = new Double[2];
        ys = new Double[2];
        D = Math.Sqrt(D);
        xs[0] = m1 + (a * d + b * D) / (a * a + b * b);
        ys[0] = n1 + (a * d + a * D) / (a * a + b * b);
        xs[1] = m1 + (a * d - b * D) / (a * a + b * b);
        ys[1] = n1 + (a * d - a * D) / (a * a + b * b);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
