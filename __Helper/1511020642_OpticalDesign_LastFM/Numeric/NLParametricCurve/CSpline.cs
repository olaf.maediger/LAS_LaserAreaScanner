﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLParametricCurve
{
  public class CSpline : CParametric
  {

    public CSpline(CRealPixel realpixel)
      : base(realpixel)
    {
    }

    protected override Double GetLength()
    {
      return Math.Sqrt(1.0);
    }

    public override Boolean BuildRealVector()
    {
      try
      {
        /* later ...Double DX = FX1 - FX0;
        Double DY = FY1 - FY0;
        Double DT = GetLength() / FCountResolution;
        Int32 Index = 0;
        Double T = 0.0;
        while (T <= 1.0)
        {
          Double XR = FX0 + DX * T;
          Double YR = FY0 + DY * T;
          //
          FXRealVector[Index] = XR;
          FYRealVector[Index] = YR;
          //
          T += DT;
        }*/
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      return false;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //




  }
}
