﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLParametricCurve
{
  public abstract class CParametric
  {
    private const Int32 INIT_COUNTRESOLUTION = 10; 
    private const Int32 INIT_COUNTRESOLUTION_MINIMUM = 2; 


    // reference to global transformation
    protected CRealPixel FRealPixel;
    protected Int32 FCountResolution;
    // local real coordinates of line
    protected Double[] FXRealVector, FYRealVector;
    // local pixel coordinates of line
    protected Int32[] FXPixelVector, FYPixelVector;

    public CParametric(CRealPixel realpixel)
    {
      FRealPixel = realpixel;
      CountResolution = INIT_COUNTRESOLUTION;
    }

    protected void SetCountResolution(Int32 value)
    {
      FCountResolution = Math.Max(2, value);
      FXRealVector = new Double[FCountResolution];
      FYRealVector = new Double[FCountResolution];
      FXPixelVector = new Int32[FCountResolution];
      FYPixelVector = new Int32[FCountResolution];
    }
    public Int32 CountResolution
    {
      get { return FCountResolution; }
      set { SetCountResolution(value); }
    }

    protected abstract Double GetLength();
    public Double Length
    {
      get { return GetLength(); }    
    }

    public abstract Boolean BuildRealVector();
    public virtual Boolean BuildPixelVector()
    {
      try
      {
        Int32 RVL = FXRealVector.Length;
        for (Int32 Index = 0; Index < RVL; Index++)
        {
          FXPixelVector[Index] = FRealPixel.XRealPixel(FXRealVector[Index]);
          FYPixelVector[Index] = FRealPixel.YRealPixel(FYRealVector[Index]);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public abstract Boolean Draw(Graphics graphics);

    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //

  }
}
