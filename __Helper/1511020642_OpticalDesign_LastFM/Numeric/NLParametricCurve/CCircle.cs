﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLParametricCurve
{ // *** Circle *** (Kreis)
  // Parametric Equation 
  // - One (Middle)Point PC = (xC, yC) and Radius R
  // - Parameter t : t in [0,1]
  // x(t) = xC + R cos(2 PI t)
  // y(t) = yC + R sin(2 PI t)
  //
  public class CCircle : CParametric
  { //
    //----------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------
    //
    private const Int32 INIT_COUNTRESOLUTION = 10;
    private const Double INIT_RADIUS = 1.0;
    private const Double INIT_CENTERX = 0.0;
    private const Double INIT_CENTERY = 0.0;
    //
    //----------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------
    //
    protected Double FR;
    protected Double FXC, FYC;
    //
    //----------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------
    //
    public CCircle(CRealPixel realpixel)
      : base(realpixel)
    {
      FR = INIT_RADIUS;
      FXC = INIT_CENTERX;
      FYC = INIT_CENTERY;
      CountResolution = INIT_COUNTRESOLUTION;
      BuildRealVector();
      BuildPixelVector();
    }

    public CCircle(CRealPixel realpixel,
                   Double xc, Double yc, Double r,
                   Int32 countresolution)
      : base(realpixel)
    {
      FR = r;
      FXC = xc;
      FYC = yc;
      CountResolution = countresolution;
      BuildRealVector();
      BuildPixelVector();
    }

    public CCircle(CRealPixel realpixel,
                   Double xc, Double yc, 
                   Double x0, Double y0,
                   Int32 countresolution)
      : base(realpixel)
    {
      FXC = xc;
      FYC = yc;
      CMath.CircleDualPointToRadius(FXC, FYC, x0, y0, out FR);
      CountResolution = countresolution;
      BuildRealVector();
      BuildPixelVector();
    }



    protected Double GetRadius()
    {
      return FR;
    }
    protected void SetRadius(Double radius)
    {
      if (0 < radius)
      {
        FR = radius;
      }
    }
    public Double R
    {
      get { return GetRadius(); }
      set { SetRadius(value); }
    }

    public Double XC
    {
      get { return FXC; }
      set { FXC = value; }
    }
    public Double YC
    {
      get { return FYC; }
      set { FYC = value; }
    }

    protected override Double GetLength()
    {
      return 2 * Math.PI * FR;
    }

    public override Boolean BuildRealVector()
    {
      try
      {
        Double DT = 1.0 / (FCountResolution - 1);
        for (Int32 Index = 0; Index < FCountResolution; Index++)
        {
          Double T = Index * DT;
          Double A = 2 * Math.PI * T;
          Double XR = FXC + FR * Math.Cos(A);
          Double YR = FYC + FR * Math.Sin(A);
          //
          FXRealVector[Index] = XR;
          FYRealVector[Index] = YR;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public override Boolean Draw(Graphics graphics)
    {
      const Int32 R = 1;
      Int32 IH = FXPixelVector.Length - 1;
      Pen PL = new Pen(Color.FromArgb(0xFF, 0xCC, 0x88, 0xAA), 3);
      Pen PP = new Pen(Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF), 1);
      Int32 R2 = 2 * R;
      for (Int32 Index = 0; Index < IH; Index++)
      {
        Int32 XP = FXPixelVector[Index];
        graphics.DrawLine(PL, FXPixelVector[Index], FYPixelVector[Index],
                          FXPixelVector[1 + Index], FYPixelVector[1 + Index]);
        graphics.DrawEllipse(PP, FXPixelVector[Index] - R, FYPixelVector[Index] - R, R2, R2);
      }
      graphics.DrawEllipse(PP, FXPixelVector[IH] - R, FYPixelVector[IH] - R, R2, R2);
      return true;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //



  }

}
