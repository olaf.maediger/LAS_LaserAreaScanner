﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
using NLParametricCurve;
//
namespace NLViewParametric
{
  
  public class CViewParametric
  { //
    //---------------------------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------
    //
    protected CRealPixel FRealPixel;
    protected CParametricList FParametricList;
    //
    //---------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------
    //
    public CViewParametric()
    {
      Init();
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------
    //


    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public CRealPixel RealPixel
    {
      get { return FRealPixel; }
    }

    public CParametricList ParametricList
    {
      get { return FParametricList; }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //


    //
    //---------------------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------------------
    //
    private void Init()
    {
      FRealPixel = new CRealPixel();
      FRealPixel.XRealMinimum = -10;
      FRealPixel.YRealMinimum = -10;
      FRealPixel.XRealMaximum = +10;
      FRealPixel.YRealMaximum = +10;
      //
      FParametricList = new CParametricList();
      //
      /*
      FRealPixel = new CRealPixel();
      FRealPixel.XRealMinimum = -10;
      FRealPixel.YRealMinimum = -10;
      FRealPixel.XRealMaximum = 20;
      FRealPixel.YRealMaximum = +20;
       */
      //
      /*/ debug:
      CSection Section0 = new CSection(FRealPixel);
      Section0.X0 = -100; Section0.Y0 = -100;
      Section0.X1 = 100; Section0.Y1 = 100;
      Section0.BuildRealVector();
      FParametricList.Add(Section0);
      //
      CSection Section1 = new CSection(FRealPixel);
      Section1.X0 = 0; Section1.Y0 = 0;
      Section1.X1 = 90; Section1.Y1 = 0;
      Section1.BuildRealVector();
      FParametricList.Add(Section1);
       */
      /* ?????
      CCircle CircleBase = new CCircle(FRealPixel);
      CircleBase.CountResolution = 64;
      CircleBase.XC = 0; CircleBase.YC = 0;
      CircleBase.R = 1;
      CircleBase.BuildRealVector();
      FParametricList.Add(CircleBase);
      //
      CCircle CircleHead = new CCircle(FRealPixel);
      CircleHead.CountResolution = 64;
      CircleHead.XC = 0; CircleHead.YC = 0;
      CircleHead.R = 2;
      CircleHead.BuildRealVector();
      FParametricList.Add(CircleHead);
      //
      CEnvelope Envelope = new CEnvelope(FRealPixel);
      Envelope.CountResolution = 64;
      Envelope.X0 = 1; Envelope.Y0 = 0;
      Envelope.X1 = -1; Envelope.Y1 = 1;
      Envelope.XC = 0; Envelope.YC = 0;
      Envelope.BuildRealVector();
      FParametricList.Add(Envelope);
      //
      CIntersection Intersection = new CIntersection();
      Intersection.CircleEnvelope(CircleHead, Envelope);
       */

      /* OK explizit Section-Section
      CSection Section0 = new CSection(FRealPixel);
      Section0.CountResolution = 7;
      Section0.X0 = 0; Section0.Y0 = -10;
      Section0.X1 = 10; Section0.Y1 = 10;
      Section0.BuildRealVector();
      FParametricList.Add(Section0);
      //
      CSection Section1 = new CSection(FRealPixel);
      Section1.CountResolution = 5;
      Section1.X0 = 0; Section1.Y0 = 10;
      Section1.X1 = 10; Section1.Y1 = 0;
      Section1.BuildRealVector();
      FParametricList.Add(Section1);
      //
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.SectionSection(Section0, Section1, out SS, out TS, out XS, out YS))
      {
        CCircle CircleSection = new CCircle(FRealPixel);
        CircleSection.CountResolution = 4;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.4;
        CircleSection.BuildRealVector();
        FParametricList.Add(CircleSection);
      }
       */

/* ???
      // implizit Section-Section
      CSection Section0 = new CSection(FRealPixel);
      Section0.CountResolution = 7;
      Section0.X0 = 0; Section0.Y0 = 0;
      Section0.X1 = 10; Section0.Y1 = 10;
      Section0.BuildRealVector();
      FParametricList.Add(Section0);
      //
      CSection Section1 = new CSection(FRealPixel);
      Section1.CountResolution = 5;
      Section1.X0 = 0; Section1.Y0 = 10;
      Section1.X1 = 10; Section1.Y1 = 5;
      Section1.BuildRealVector();
      FParametricList.Add(Section1);
      //
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.SectionSectionIterative(Section0, Section1, out SS, out TS, out XS, out YS))
      {
        Console.WriteLine(String.Format("XS={0} YS={1}", XS, YS));
        CCircle CircleSection = new CCircle(FRealPixel);
        CircleSection.CountResolution = 4;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.4;
        CircleSection.BuildRealVector();
        FParametricList.Add(CircleSection);
      }
      */

      // OK SolveX3CosX();
      // OK SolveVector();
      // OK SolveSectionSection();

      /* OK!!!!!!!!!!!!!!!!!!!!!!
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      FRealPixel = new CRealPixel();
      FRealPixel.XRealMinimum = 0;
      FRealPixel.YRealMinimum = 0;
      FRealPixel.XRealMaximum = +10;
      FRealPixel.YRealMaximum = +10;
      //
      FParametricList = new CParametricList();
      //
      //----------------------------------------------
      // Example Intersection Section - Section
      //----------------------------------------------
      //
      CSection Section0 = new CSection(FRealPixel);
      Section0.CountResolution = 7;
      Section0.X0 = 1; Section0.Y0 = 1;
      Section0.X1 = 9; Section0.Y1 = 9;
      Section0.BuildRealVector();
      FParametricList.Add(Section0);
      //
      CSection Section1 = new CSection(FRealPixel);
      Section1.CountResolution = 5;
      Section1.X0 = 1; Section1.Y0 = 9;
      Section1.X1 = 9; Section1.Y1 = 1;
      Section1.BuildRealVector();
      FParametricList.Add(Section1);
      // 
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.SectionSectionIterative(Section0, Section1, 
                                               0.001,
                                               out SS, out TS,
                                               out XS, out YS))
      {
        CCircle CircleSection = new CCircle(FRealPixel);
        CircleSection.CountResolution = 8;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.4;
        CircleSection.BuildRealVector();
        FParametricList.Add(CircleSection);
      }*/
    }



    // OK!!!
    private Boolean SolveX3CosX()
    {
      // x^3 = cos(x) -> f(x) = x^3 - cos(x) = 0
      // df / dx = 3x^2 + sin(x)
      // J(xn) dxn = -f(xn)
      // -> (3xn^2 + sin(xn)) dxn = -xn^3 + cos(xn)
      // dxn = (-xn^3 + cos(xn)) / (3xn^2 + sin(xn))
      // xn+1 = xn + dxn
      //
      Double XN = 1, XNp1 = 0;
      for (Int32 RI = 0; RI < 10; RI++)
      {
        Double D = 3 * XN * XN + Math.Sin(XN);
        if (0 == D)
        {
          return false;
        }
        Double DXN = (-XN * XN * XN + Math.Cos(XN)) / D;
        XNp1 = XN + DXN;
        if (Math.Abs(DXN) < 0.001)
        {
          return true;
        }
        XN = XNp1;
      }
      return false;
    }

    // OK!!!
    private Boolean SolveVector()
    {
      // x^3 = cos(x) -> f(x,y) = x^3 - cos(x) = 0
      // y^2 = q -> g(x,y) = y^2 -a = 0
      //
      // df(x,y) / dx = 3x^2 + sin(x)
      // dg(x,y) / dy = 2y
      //
      // J(xn, yn) (dxn, dyn) = -f(xn, yn)
      //
      // ( 3xn^2 + sin(xn)  0  ) (dxn) = -(xn^3 - cos(xn))
      // (       0          2y ) (dyn) = -(y^2 - a)
      // 
      // xn+1 = xn + dxn
      //
      Double XN = 1, XM = 0;
      Double YN = 1, YM = 0;
      Double Q = 2;
      for (Int32 RI = 0; RI < 10; RI++)
      {
        Double J00 = 3 * XN * XN + Math.Sin(XN);
        Double J01 = 0;
        Double J10 = 0;
        Double J11 = 2 * YN;
        Double IT0 = -(XN * XN * XN - Math.Cos(XN));
        Double IT1 = -(YN * YN - Q);
        //
        Double D = J00 * J11 - J01 * J10;
        if (0 == D)
        {
          return false;
        }
        Double DX = IT0 * J11 - J01 * IT1;
        Double DY = J00 * IT1 - IT0 * J10;
        Double DXN = DX / D;
        Double DYN = DY / D;
        XM = XN + DXN;
        YM = YN + DYN;
        if ((Math.Abs(DXN) < 0.001) && (Math.Abs(DYN) < 0.001))
        {
          return true;
        }
        XN = XM;
        YN = YM;
      }
      return false;
    }

    //!!! OK !!!
    private Boolean SolveSectionSection()
    {
      // f(s,t) = x0 + (x1 - x0)s - x2 - (x3 - x2)t = 0
      // g(s,t) = y0 + (y1 - y0)s - y2 - (y3 - y2)t = 0
      //
      // df(s,t) / ds = +(x1 - x0)
      // df(s,t) / dt = -(x3 - x2)
      // dg(s,t) / ds = +(y1 - y0)
      // dg(s,t) / dt = -(y3 - y2)
      //
      // J(sn, tn) (dsn, dtn) = -f(sn, tn)
      //
      // (+(x1 - x0)   -(x3 - x2)) (dsn) = -(x0 + (x1 - x0)sn - x2 - (x3 - x2)tn)
      // (+(y1 - y0)   -(y3 - y2)) (dtn) = -(y0 + (y1 - y0)sn - y2 - (y3 - y2)tn)
      // 
      // sm = sn + dsn
      // tm = tn + dtn
      //
      Double SN = 1, SM = 0;
      Double TN = 1, TM = 0;
      Double X0 = 0; Double Y0 = 0;
      Double X1 = 10; Double Y1 = 10;
      Double X2 = 0; Double Y2 = 10;
      Double X3 = 10; Double Y3 = 0;

      // 
      CSection Section0 = new CSection(FRealPixel);
      Section0.CountResolution = 7;
      Section0.X0 = X0; Section0.Y0 = Y0;
      Section0.X1 = X1; Section0.Y1 = Y1;
      Section0.BuildRealVector();
      FParametricList.Add(Section0);
      //
      CSection Section1 = new CSection(FRealPixel);
      Section1.CountResolution = 5;
      Section1.X0 = X2; Section1.Y0 = Y2;
      Section1.X1 = X3; Section1.Y1 = Y3;
      Section1.BuildRealVector();
      FParametricList.Add(Section1);


      for (Int32 RI = 0; RI < 10; RI++)
      {
        Double J00 = +(X1 - X0);
        Double J01 = -(X3 - X2);
        Double J10 = +(Y1 - Y0);
        Double J11 = -(Y3 - Y2);
        Double IT0 = -(X0 + (X1 - X0) * SN - X2 - (X3 - X2) * TN);
        Double IT1 = -(Y0 + (Y1 - Y0) * SN - Y2 - (Y3 - Y2) * TN);
        //
        Double D = J00 * J11 - J01 * J10;
        if (0 == D)
        {
          return false;
        }
        Double DX = IT0 * J11 - J01 * IT1;
        Double DY = J00 * IT1 - IT0 * J10;
        Double DSN = DX / D;
        Double DTN = DY / D;
        SM = SN + DSN;
        TM = TN + DTN;
        if ((Math.Abs(DSN) < 0.001) && (Math.Abs(DTN) < 0.001))
        {
          Double XS = X0 + (X1 - X0) * SN;
          Double YS = Y0 + (Y1 - Y0) * SN;
          CCircle CircleSection = new CCircle(FRealPixel);
          CircleSection.CountResolution = 8;
          CircleSection.XC = XS; CircleSection.YC = YS;
          CircleSection.R = 0.4;
          CircleSection.BuildRealVector();
          FParametricList.Add(CircleSection);
          return true;
        }
        SN = SM;
        TN = TM;
      }
      return false;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Management 
    //---------------------------------------------------------------------------
    //
    public void Redraw(Graphics graphics)
    {
      FParametricList.Redraw(graphics);
    }

    public void SetPixelExtrema(Int32 width, Int32 height)
    {
      FRealPixel.SetPixelExtrema(width, height);
      FParametricList.BuildPixelVector();
    }

    /*
    public void ExampleSectionSection()
    {
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      FRealPixel.XRealMinimum = 0;
      FRealPixel.YRealMinimum = 0;
      FRealPixel.XRealMaximum = +10;
      FRealPixel.YRealMaximum = +10;
      //
      FParametricList.Clear();
      //
      //----------------------------------------------
      // Example Intersection Section - Section
      //----------------------------------------------
      //
      CSection Section0 = new CSection(FRealPixel);
      Section0.CountResolution = 7;
      Section0.X0 = 1; Section0.Y0 = 1;
      Section0.X1 = 9; Section0.Y1 = 9;
      Section0.BuildRealVector();
      FParametricList.Add(Section0);
      //
      CSection Section1 = new CSection(FRealPixel);
      Section1.CountResolution = 5;
      Section1.X0 = 1; Section1.Y0 = 9;
      Section1.X1 = 9; Section1.Y1 = 1;
      Section1.BuildRealVector();
      FParametricList.Add(Section1);
      // 
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.SectionSectionIterative(Section0, Section1,
                                               0.001,
                                               out SS, out TS,
                                               out XS, out YS))
      {
        CCircle CircleSection = new CCircle(FRealPixel);
        CircleSection.CountResolution = 8;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.4;
        CircleSection.BuildRealVector();
        FParametricList.Add(CircleSection);
      }
    }

    public void ExampleLineLine()
    {
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      FRealPixel.XRealMinimum = 0;
      FRealPixel.YRealMinimum = 0;
      FRealPixel.XRealMaximum = +10;
      FRealPixel.YRealMaximum = +10;
      //
      FParametricList.Clear();
      //
      //----------------------------------------------
      // Example Intersection Line - Line
      //----------------------------------------------
      //
      CLine Line0 = new CLine(FRealPixel);
      Line0.CountResolution = 7;
      Line0.X0 = 1; Line0.Y0 = 1;
      Line0.X1 = 9; Line0.Y1 = 9;
      Line0.BuildRealVector();
      FParametricList.Add(Line0);
      //
      CLine Line1 = new CLine(FRealPixel);
      Line1.CountResolution = 5;
      Line1.X0 = 1; Line1.Y0 = 9;
      Line1.X1 = 9; Line1.Y1 = 1;
      Line1.BuildRealVector();
      FParametricList.Add(Line1);
      // 
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.LineLineIterative(Line0, Line1,
                                         0.001,
                                         out SS, out TS,
                                         out XS, out YS))
      {
        CCircle CircleSection = new CCircle(FRealPixel);
        CircleSection.CountResolution = 8;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.4;
        CircleSection.BuildRealVector();
        FParametricList.Add(CircleSection);
      }
    }

    public void ExampleSectionLine()
    {
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      FRealPixel.XRealMinimum = 0;
      FRealPixel.YRealMinimum = 0;
      FRealPixel.XRealMaximum = +10;
      FRealPixel.YRealMaximum = +10;
      //
      FParametricList.Clear();
      //
      //----------------------------------------------
      // Example Intersection Section - Line
      //----------------------------------------------
      //
      CLine Section = new CLine(FRealPixel);
      Section.CountResolution = 7;
      Section.X0 = 1; Section.Y0 = 1;
      Section.X1 = 9; Section.Y1 = 9;
      Section.BuildRealVector();
      FParametricList.Add(Section);
      //
      CLine Line = new CLine(FRealPixel);
      Line.CountResolution = 5;
      Line.X0 = 1; Line.Y0 = 9;
      Line.X1 = 9; Line.Y1 = 1;
      Line.BuildRealVector();
      FParametricList.Add(Line);
      // 
      CIntersection Intersection = new CIntersection();
      Double SS, TS, XS, YS;
      if (Intersection.LineLineIterative(Section, Line,
                                         0.001,
                                         out SS, out TS,
                                         out XS, out YS))
      {
        CCircle CircleSection = new CCircle(FRealPixel);
        CircleSection.CountResolution = 8;
        CircleSection.XC = XS; CircleSection.YC = YS;
        CircleSection.R = 0.4;
        CircleSection.BuildRealVector();
        FParametricList.Add(CircleSection);
      }
    }
*/    
    
    //
    //---------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------
    //

  }
}
