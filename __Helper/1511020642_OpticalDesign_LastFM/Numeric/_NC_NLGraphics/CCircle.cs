﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLGraphics
{
  public class CCircle : CGraphicBase
  {
    private Double FRXM, FRYM, FRR;


    public CCircle(Double rxm, Double rym,
                   Double rr)
    {
      FRXM = rxm;
      FRYM = rym;
      FRR = rr;
    }



    public override Boolean Draw(Graphics graphics, CRealPixel realpixel)
    {
      Int32 PX0 = realpixel.XRealPixel(FRXM - FRR);
      Int32 PY0 = realpixel.YRealPixel(FRYM - FRR);
      Int32 PDX = realpixel.DXRealPixel(2 * FRR);
      Int32 PDY = realpixel.DYRealPixel(2 * FRR);
      Pen P = new Pen(Color.Blue, 3);
      graphics.DrawEllipse(P, PX0, PY0, PDX, PDY);
      return true;
    }

  }
}
