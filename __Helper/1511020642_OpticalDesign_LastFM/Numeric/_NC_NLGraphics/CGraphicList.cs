﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLGraphics
{
  public class CGraphicList : List<CGraphicBase>
  {

    public Boolean Draw(Graphics graphics, CRealPixel realpixel)
    {
      Boolean Result = true;
      foreach (CGraphicBase Base in this)
      {
        Result &= Base.Draw(graphics, realpixel);
      }
      return Result;
    }


  }
}
