﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
//
namespace NLGraphics
{
  public abstract class CGraphicBase
  {

    public abstract Boolean Draw(Graphics graphics, CRealPixel realpixel);

  }
}
