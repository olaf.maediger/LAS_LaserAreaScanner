﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using NLRealPixel;
using NLGraphics;
//
namespace NLGraphics
{
  public class CLine : CGraphicBase
  {
    private Double FRX0, FRY0, FRX1, FRY1;
    //

    public CLine(Double rx0, Double ry0,
                 Double rx1, Double ry1)
    {
      FRX0 = rx0;
      FRY0 = ry0;
      FRX1 = rx1;
      FRY1 = ry1;
    }


    public override Boolean Draw(Graphics graphics, CRealPixel realpixel)
    {
      Int32 PX0 = realpixel.XRealPixel(FRX0);
      Int32 PY0 = realpixel.YRealPixel(FRY0);
      Int32 PX1 = realpixel.XRealPixel(FRX1);
      Int32 PY1 = realpixel.YRealPixel(FRY1);
      Pen PS = new Pen(Color.FromArgb(0xF0, 0x80, 0x80, 0xFF), 3);
      graphics.DrawLine(PS, PX0, PY0, PX1, PY1);
      //
      Double YRL, YRH;
      if (CMath.LineCalculateY(FRX0, FRY0, FRX1, FRY1, realpixel.XRealMinimum, out YRL))
      {
        if (CMath.LineCalculateY(FRX0, FRY0, FRX1, FRY1, realpixel.XRealMaximum, out YRH))
        {
        }

      }
      else
      {
      }
      PY0 = realpixel.YRealPixel(FRY0);
      PX1 = realpixel.XRealPixel(FRX1);
      PY1 = realpixel.YRealPixel(FRY1);
      Pen PL = new Pen(Color.FromArgb(0x80, 0x80, 0x80, 0xFF), 1);
      graphics.DrawLine(PL, PX0, PY0, PX1, PY1);
      return true;
    }

  }
}
