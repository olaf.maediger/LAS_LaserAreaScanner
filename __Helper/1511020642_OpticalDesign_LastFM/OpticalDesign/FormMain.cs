﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
using NLParametricCurve;
using NLRayTransferMatrix;
using UCViewOpticalDesign;
//
namespace OpticalDesign
{
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;
    //private CUCViewLine FUCViewLine;
    //private CUCViewEnvelope FUCViewEnvelope;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);      
      tbcMain.Controls.Remove(tbpSerialNumber);
      //
      tmrStartup.Interval = 100;
      tmrStartup.Start();
      //
      FRandom = new Random();
      //
      FUCViewOpticalDesign.Dock = DockStyle.Fill;
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      // ... Int32 IValue;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      return Result;
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Event - 
    //---------------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUC...
    //------------------------------------------------------------------------
    // 

    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //


    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          btnActionA_Click(this, null);
          return true;
        case 11:
          return true;
        case 21:
          return true;
        case 31:
          return true;
        case 41:
         
          return true;
        case 51:
          return true;
        case 61:
          
          return true;
        case 71:
          return true;
        case 81:
          //Close();
          return true;
        case 91:
          return true;
        case 101:
          return true;
        default:
          return false;
      }
    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }




    ////
    //Double X00 = 0; Double Y00 = 0;
    //Double X01 = 1; Double Y01 = 1;
    //Double A0, B0, C0;
    //FUCViewOpticalDesign.AddLine(X00, Y00, X01, Y01);
    //CMath.LineDualPointToCoefficient(X00, Y00, X01, Y01, out A0, out B0, out C0);
    ////
    //Double X10 = 0; Double Y10 = 1;
    //Double X11 = 1; Double Y11 = 0;
    //Double A1, B1, C1;
    //FUCViewOpticalDesign.AddLine(X10, Y10, X11, Y11);
    //CMath.LineDualPointToCoefficient(X10, Y10, X11, Y11, out A1, out B1, out C1);
    ////
    //Double[] XS;
    //Double[] YS;
    //if (CMath.IntersectionLineLine(A0, B0, C0,
    //                               A1, B1, C1,
    //                               out XS, out YS))
    //{
    //  Double RS = 0.1;
    //  FUCViewOpticalDesign.AddCircle(XS[0], YS[0], RS);
    //}
    ////
    //Double R = 1;
    //Double XM = 2;
    //Double YM = -1;
    //FUCViewOpticalDesign.AddCircle(XM, YM, R);
    //if (CMath.IntersectionLineCircle(A1, B1, C1, //A0, B0, C0,
    //                                 XM, YM, R,
    //                                 out XS, out YS))
    //{
    //  if (2 == XS.Length)
    //  {
    //    Double RS = 0.1;
    //    FUCViewOpticalDesign.AddCircle(XS[0], YS[0], RS);
    //    FUCViewOpticalDesign.AddCircle(XS[1], YS[1], RS);
    //  }
    //  else
    //    if (1 == XS.Length)
    //    {
    //      Double RS = 0.1;
    //      FUCViewOpticalDesign.AddCircle(XS[0], YS[0], RS);
    //    }
    //}


    //
    //----------------------------------------------
    // Example Intersection EnvelopeNorm - Section
    //----------------------------------------------
    //
    //Double PZ = 0; // [mm]
    //Double W = 1.0; // [mm]
    //FUCViewOpticalDesign.AddTranslation(PZ, W);
    ////
    //PZ = 1; // [mm]
    //W = 2.0; // [mm]
    //FUCViewOpticalDesign.AddTranslation(PZ, W);
    ////
    //PZ = 3; // [mm]
    //Double PX = -4; // [mm]
    //W = 1.0; // [mm]
    //Double H = 8.0; // [mm]
    //Double FI = 2.0; // [1]
    //FUCViewOpticalDesign.AddPlateParallel(PZ, PX, W, H, FI);
    ////
    //PZ = 4; // [mm]
    //W = 1.0; // [mm]
    //FUCViewOpticalDesign.AddTranslation(PZ, W);



    // PZ = PZ + W; // [mm]
    // W = 2.4; // [mm]
    // FI = 3.0; // [1]
    // //FUCViewOpticalDesign.AddTranslation(PZ, W, FI);
    // //FUCViewOpticalDesign.AddPlateParallel(PZ, PX, W, H, FI);
    // //
    // PZ = PZ + W; // [mm]
    // W = 2.8; // [mm]
    // FI = 6.0; // [1]
    // //FUCViewOpticalDesign.AddTranslation(PZ, W, FI);
    // //FUCViewOpticalDesign.AddPlateParallel(PZ, PX, W, H, FI);
    // //
    // PZ = PZ + W;
    // PX = -4; // [mm]
    // W = 0.2; // [mm]
    // H = 8.0; // [mm]
    // FI = 1.0; // [1]
    ////FUCViewOpticalDesign.AddMirrorPlane(PZ, PX, W, H, FI);
    // //
    // PZ = 5.0; // [mm]
    // W = 4.0; // [mm]
    // FI = 1.0; // [1]
    // //FUCViewOpticalDesign.AddTranslation(PZ, W, FI);
    //
    //PZ = 0.0;  // [mm]
    //PX = 0.0;  // [mm]
    //Double A = 30.0;  // [deg]
    //!!!!!!!FUCViewOpticalDesign.AddRay(PZ, PX, A);
    //
    //FUCViewOpticalDesign.CalculateRays();







    //Double PZ = 0; // [mm]
    //Double W = 1.0; // [mm]
    //FUCViewOpticalDesign.AddTranslation(PZ, W);
 














    //
    // OK 
    //

    //private void btnLineSegment_Click(object sender, EventArgs e)
    //{ //
    //  //----------------------------------------------
    //  // World-Coordinates
    //  //----------------------------------------------
    //  //
    //  FUCViewOpticalDesign.RealPixel.XRealMinimum = 0;
    //  FUCViewOpticalDesign.RealPixel.XRealMaximum = +10;
    //  FUCViewOpticalDesign.RealPixel.YRealMinimum = -5;
    //  FUCViewOpticalDesign.RealPixel.YRealMaximum = +5;
    //  //
    //  FUCViewOpticalDesign.ComponentList.Clear();
    //  FUCViewOpticalDesign.RayPool.Clear();
    //  //
    //  Double X00 = 1; Double Y00 = 1;
    //  Double X01 = 2; Double Y01 = 2;
    //  CLine Line = FUCViewOpticalDesign.AddLine(X00, Y00, X01, Y01);
    //  //
    //  Double X10 = 3; Double Y10 = -4;
    //  Double X11 = 3; Double Y11 = +4;
    //  CSegment Segment = FUCViewOpticalDesign.AddSegment(X10, Y10, X11, Y11);
    //  //
    //  Double[] XS;
    //  Double[] YS;
    //  if (CMath.IntersectionLineSegment(X00, Y00, X01, Y01,
    //                                    X10, Y10, X11, Y11,
    //                                    out XS, out YS))
    //  {
    //    Double RS = 0.06;
    //    FUCViewOpticalDesign.AddCircle(XS[0], YS[0], RS, 6);
    //  }
    //  //
    //  FUCViewOpticalDesign.ForceRedraw();
    //}

    // OK!!!
    //private void btnLineArc_Click(object sender, EventArgs e)
    //{ //
    //  //----------------------------------------------
    //  // World-Coordinates
    //  //----------------------------------------------
    //  //
    //  FUCViewOpticalDesign.RealPixel.XRealMinimum = 0;
    //  FUCViewOpticalDesign.RealPixel.XRealMaximum = +20;
    //  FUCViewOpticalDesign.RealPixel.YRealMinimum = -10;
    //  FUCViewOpticalDesign.RealPixel.YRealMaximum = +10;
    //  //
    //  FUCViewOpticalDesign.ComponentList.Clear();
    //  FUCViewOpticalDesign.RayPool.Clear();
    //  //
    //  Double X0L = 1; Double Y0L = 0;
    //  Double X1L = 3; Double Y1L = 0.7;
    //  CLine Line = FUCViewOpticalDesign.AddLine(X0L, Y0L, X1L, Y1L);
    //  //
    //  Double XCA = 9; Double YCA = 0.0;
    //  Double X0A = 4; Double Y0A = 3;
    //  Double X1A = 4; Double Y1A = -3;
    //  FUCViewOpticalDesign.AddCircle(XCA, YCA, X0A, Y0A, 100);
    //  FUCViewOpticalDesign.AddArc(XCA, YCA, X0A, Y0A, X1A, Y1A, 20);
    //  //
    //  Double[] XS;
    //  Double[] YS;
    //  if (CMath.IntersectionLineCircle(X0L, Y0L, X1L, Y1L,
    //                                   XCA, YCA, X0A, Y0A,
    //                                   out XS, out YS))
    //  {
    //    Double RS = 0.2;
    //    if (0 < XS.Length)
    //    {
    //      FUCViewOpticalDesign.AddCircle(XS[0], YS[0], RS, 9);
    //    }
    //    if (1 < XS.Length)
    //    {
    //      FUCViewOpticalDesign.AddCircle(XS[1], YS[1], RS, 9);
    //    }
    //  }
    //  //
    //  Double XCB = 1; Double YCB = 0.0;
    //  Double X0B = 6; Double Y0B = -3;
    //  Double X1B = 6; Double Y1B = 3;
    //  FUCViewOpticalDesign.AddArc(XCB, YCB, X0B, Y0B, X1B, Y1B, 20);
    //  //
    //  FUCViewOpticalDesign.ForceRedraw();
    //}
    ////
    //// Intersection Line Arc OK
    ////
    //private void btnLineArc_Click(object sender, EventArgs e)
    //{ //
    //  //----------------------------------------------
    //  // World-Coordinates
    //  //----------------------------------------------
    //  //
    //  FUCViewOpticalDesign.RealPixel.XRealMinimum = 0;
    //  FUCViewOpticalDesign.RealPixel.XRealMaximum = +20;
    //  FUCViewOpticalDesign.RealPixel.YRealMinimum = -10;
    //  FUCViewOpticalDesign.RealPixel.YRealMaximum = +10;
    //  //
    //  FUCViewOpticalDesign.ComponentList.Clear();
    //  FUCViewOpticalDesign.RayPool.Clear();
    //  //
    //  Double X0L = 1; Double Y0L = 0;
    //  Double X1L = 3; Double Y1L = 0.7;
    //  CLine Line = FUCViewOpticalDesign.AddLine(X0L, Y0L, X1L, Y1L);
    //  //
    //  Double XCA = 9; Double YCA = 0.0;
    //  Double X0A = 4; Double Y0A = 3;
    //  Double X1A = 4; Double Y1A = -3;
    //  //FUCViewOpticalDesign.AddCircle(XCA, YCA, X0A, Y0A, 100);
    //  FUCViewOpticalDesign.AddArc(XCA, YCA, X0A, Y0A, X1A, Y1A, 20);

    //  Double[] XS;
    //  Double[] YS;
    //  //
    //  if (CMath.IntersectionLineArc(X0L, Y0L, X1L, Y1L,
    //                                XCA, YCA, X0A, Y0A, X1A, Y1A,
    //                                out XS, out YS))
    //  {
    //    Double RS = 0.2;
    //    if (0 < XS.Length)
    //    {
    //      FUCViewOpticalDesign.AddCircle(XS[0], YS[0], RS, 9);
    //    }
    //    if (1 < XS.Length)
    //    {
    //      FUCViewOpticalDesign.AddCircle(XS[1], YS[1], RS, 9);
    //    }
    //  }
    //  //
    //  Double XCB = 1; Double YCB = 0.0;
    //  Double X0B = 6; Double Y0B = -3;
    //  Double X1B = 6; Double Y1B = 3;
    //  FUCViewOpticalDesign.AddArc(XCB, YCB, X0B, Y0B, X1B, Y1B, 20);
    //  //
    //  if (CMath.IntersectionLineArc(X0L, Y0L, X1L, Y1L,
    //                                XCB, YCB, X0B, Y0B, X1B, Y1B,
    //                                out XS, out YS))
    //  {
    //    Double RS = 0.2;
    //    if (0 < XS.Length)
    //    {
    //      FUCViewOpticalDesign.AddCircle(XS[0], YS[0], RS, 9);
    //    }
    //    if (1 < XS.Length)
    //    {
    //      FUCViewOpticalDesign.AddCircle(XS[1], YS[1], RS, 9);
    //    }
    //  }
    //  //
    //  FUCViewOpticalDesign.ForceRedraw();
    //}

    ////
    //// Intersection Line Arc OK
    ////
    //private void btnLineArc_Click(object sender, EventArgs e)
    //{ //
    //  //----------------------------------------------
    //  // World-Coordinates
    //  //----------------------------------------------
    //  //
    //  FUCViewOpticalDesign.RealPixel.XRealMinimum = 0;
    //  FUCViewOpticalDesign.RealPixel.XRealMaximum = +20;
    //  FUCViewOpticalDesign.RealPixel.YRealMinimum = -10;
    //  FUCViewOpticalDesign.RealPixel.YRealMaximum = +10;
    //  //
    //  FUCViewOpticalDesign.ComponentList.Clear();
    //  FUCViewOpticalDesign.RayPool.Clear();
    //  //
    //  Double X0L = 1; Double Y0L = 0;
    //  Double X1L = 3; Double Y1L = 0.7;
    //  CLine Line = FUCViewOpticalDesign.AddLine(X0L, Y0L, X1L, Y1L);
    //  //
    //  Double X0A = 4; Double Y0A = 3;
    //  Double X1A = 4; Double Y1A = -3;
    //  FUCViewOpticalDesign.AddSegment(X0A, Y0A, X1A, Y1A, 11);

    //  Double[] XS;
    //  Double[] YS;
    //  //
    //  if (CMath.IntersectionLineSegment(X0L, Y0L, X1L, Y1L,
    //                                    X0A, Y0A, X1A, Y1A,
    //                                    out XS, out YS))
    //  {
    //    Double RS = 0.2;
    //    if (0 < XS.Length)
    //    {
    //      FUCViewOpticalDesign.AddCircle(XS[0], YS[0], RS, 9);
    //    }
    //  }
    //  ////
    //  Double X0B = 6; Double Y0B = -1;
    //  Double X1B = 6; Double Y1B = 1;
    //  FUCViewOpticalDesign.AddSegment(X0B, Y0B, X1B, Y1B, 5);
    //  ////
    //  if (CMath.IntersectionLineSegment(X0L, Y0L, X1L, Y1L,
    //                                    X0B, Y0B, X1B, Y1B,
    //                                    out XS, out YS))
    //  {
    //    Double RS = 0.2;
    //    if (0 < XS.Length)
    //    {
    //      FUCViewOpticalDesign.AddCircle(XS[0], YS[0], RS, 9);
    //    }
    //  }
    //  //
    //  FUCViewOpticalDesign.ForceRedraw();
    //}

     //private void btnLineArc_Click(object sender, EventArgs e)
    //{ //
    //  //----------------------------------------------
    //  // World-Coordinates
    //  //----------------------------------------------
    //  //
    //  FUCViewOpticalDesign.RealPixel.XRealMinimum = 0;
    //  FUCViewOpticalDesign.RealPixel.XRealMaximum = +20;
    //  FUCViewOpticalDesign.RealPixel.YRealMinimum = -10;
    //  FUCViewOpticalDesign.RealPixel.YRealMaximum = +10;
    //  //
    //  FUCViewOpticalDesign.ComponentList.Clear();
    //  FUCViewOpticalDesign.RayPool.Clear();
    //  //
    //  Double PZ = 0; // [mm]
    //  Double W = 1.0; // [mm]
    //  FUCViewOpticalDesign.AddTranslation(PZ, W);
    //  //
    //  FUCViewOpticalDesign.ForceRedraw();
    //}
    //
    //--------------------------------------------------------------------------
    //  Segment - 
    //--------------------------------------------------------------------------
    //
    //
    //--------------------------------------------------------------------------
    //  Segment - Menu
    //--------------------------------------------------------------------------
    //
    private void btnActionA_Click(object sender, EventArgs e)
    {
      //
      //----------------------------------------------
      // World-Coordinates
      //----------------------------------------------
      //
      FUCViewOpticalDesign.RealPixel.XRealMinimum = -1;
      FUCViewOpticalDesign.RealPixel.XRealMaximum = +21;
      FUCViewOpticalDesign.RealPixel.YRealMinimum = -11;
      FUCViewOpticalDesign.RealPixel.YRealMaximum = +11;
      //
      FUCViewOpticalDesign.ComponentList.Clear();
      FUCViewOpticalDesign.RayPool.Clear();
      //
      //Double X0L = 1; Double Y0L = 0;
      //Double X1L = 3; Double Y1L = 0.7;
      //      CLine Line = FUCViewOpticalDesign.AddLine(X0L, Y0L, X1L, Y1L);
      //
      //Double X0A = 4; Double Y0A = 3;
      //Double X1A = 4; Double Y1A = -3;
      //FUCViewOpticalDesign.AddSegment(X0A, Y0A, X1A, Y1A, 11);

      //Double[] XS;
      //Double[] YS;
      //
      //if (CMath.IntersectionLineSegment(X0L, Y0L, X1L, Y1L,
      //                                  X0A, Y0A, X1A, Y1A,
      //                                  out XS, out YS))
      //{
      //  Double RS = 0.2;
      //  if (0 < XS.Length)
      //  {
      //    FUCViewOpticalDesign.AddCircle(XS[0], YS[0], RS, 9);
      //  }
      //}
      //
      Double[] FIS = new Double[3];
      FIS[0] = 1.00;
      FIS[1] = 1.00;
      FIS[2] = 1.00;
      //
      Double PZ = 0.0; // [mm]
      Double W = 2.0; // [mm]
      //FUCViewOpticalDesign.AddTranslation(PZ, W, FIS);
      //
      PZ = 2.0;//2.0; // [mm]
      Double PX = -8; // [mm]
      W = 2.0; // [mm]
      Double H = 16.0; // [mm]
      FIS[0] = 1.00;
      FIS[1] = 1.55;
      FIS[2] = 1.00;
      FUCViewOpticalDesign.AddPlateParallel(PZ, PX, W, H, FIS);
      //
      PZ = 6.0; // [mm]
      W = 2.0; // [mm]
      FIS[0] = 1.00;
      FIS[1] = 1.55;
      FIS[2] = 1.00;
      FUCViewOpticalDesign.AddPlateParallel(PZ, PX, W, H, FIS);
      //
      PZ = 0.0;  // [mm]
      PX = 0.0;  // [mm]
      Int32 CA = 6;
      for (Int32 AI = 0; AI < CA; AI++)
      {
        Double A = 0 + AI * (90.0 / (CA - 1));  // [deg]
        FUCViewOpticalDesign.AddRay(PZ, PX, A);
      }
      // ERROR !!!!!!!!!!!!!!!!!!
      //for (Int32 AI = 0; AI < CA; AI++)
      //{
      //  Double A = 270 + AI * 10;  // [deg]
      //  FUCViewOpticalDesign.AddRay(PZ, PX, A);
      //}
      //
      //PZ = 18.0;  // [mm]
      //PX = 0.0;  // [mm]
      ////A = 151.0;  // [deg]
      //A = 152.0;  // [deg]
      //FUCViewOpticalDesign.AddRay(PZ, PX, A);
      //
      FUCViewOpticalDesign.CalculateRays();
      //
      FUCViewOpticalDesign.ForceRedraw();
    }




  }
}
