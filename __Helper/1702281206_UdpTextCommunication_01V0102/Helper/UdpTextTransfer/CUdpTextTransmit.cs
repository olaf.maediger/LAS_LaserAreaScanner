﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
//
using Task;
//
namespace UdpTextTransfer
{
  //
  //-------------------------------------------------------------------
  //  Segment - Type
  //-------------------------------------------------------------------
  //
  public class CUdpTextTransmit : CUdpTextBase
  {
    //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public CUdpTextTransmit()
      : base()
    {
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public void SetOnDatagramTransmitted(DOnDatagramTransmitted value)
    {
      FOnDatagramTransmitted = value;
    }

    public Boolean PresetDatagram(Byte[] datagram)
    {
      try
      {
        if (null == FTxDatagram)
        {
          FTxDatagram = datagram;
          if (FTask is CTask)
          {
            if (FTask.IsFinished())
            {
              FTask = null;
            }
          }
          if (!(FTask is CTask))
          {
            FTask = new CTask("UdpTextTransmit",
                              TaskOnExecutionStart,
                              TaskOnExecutionBusy,
                              TaskOnExecutionEnd,
                              TaskOnExecutionAbort);
            FTask.Start();
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //
    private void TaskOnExecutionStart(RTaskData data)
    {
    }

    private Boolean TaskOnExecutionBusy(ref RTaskData data)
    {
      try
      {
        Int32 L = FTxDatagram.Length;
        if (0 < FTxDatagram.Length)
        {
          IPEndPoint IPETarget = new IPEndPoint(FIpEndPoint.Address, FIpEndPoint.Port);
          if (FOnDatagramTransmitted is DOnDatagramTransmitted)
          {
            FOnDatagramTransmitted(IPETarget, FTxDatagram, L);
          }
          FUdpClient.Send(FTxDatagram, L);
          Thread.Sleep(100);
          FTxDatagram = null;
        }
        return false;
      }
      catch (Exception)
      {
//        FNotifier.Error("UdpBinaryTransmitter", 1, "Invalid UdpDatagram");
        return false;
      }
    }

    private void TaskOnExecutionEnd(RTaskData data)
    {
    }

    private void TaskOnExecutionAbort(RTaskData data)
    {
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Method
    //-------------------------------------------------------------------
    //
    public override Boolean Open(IPAddress ipaddresstarget, UInt16 ipporttarget)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        FTxDatagram = null;
        FTask = null;
        //
        FIpEndPoint = new IPEndPoint(ipaddresstarget, ipporttarget);
        FUdpClient = new UdpClient();
        FUdpClient.Connect(FIpEndPoint);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Open(String sipendpointtarget)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        //
        CUdpTextBase.TextToIPEndPoint(sipendpointtarget, out FIpEndPoint);
        FUdpClient = new UdpClient();
        // open UdpConnection
        FUdpClient.Connect(FIpEndPoint);
        //
        FTask = null;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Close()
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        if (FUdpClient is UdpClient)
        { // close UdpConnection
          FUdpClient.Close();
          FUdpClient = null;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}

