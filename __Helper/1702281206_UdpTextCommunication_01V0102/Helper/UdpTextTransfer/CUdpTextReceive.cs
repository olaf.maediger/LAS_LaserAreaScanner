﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Net;
using System.Net.Sockets;
//
using Task;
//
namespace UdpTextTransfer
{
  public class CUdpTextReceive : CUdpTextBase
  { //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public CUdpTextReceive()
      : base()
    {
      FTask = null;
      FOnDatagramReceived = null;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public void SetOnDatagramReceived(DOnDatagramReceived value)
    {
      FOnDatagramReceived = value;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //
    private void TaskOnExecutionStart(RTaskData data)
    {
    }
    private Boolean TaskOnExecutionBusy(ref RTaskData data)
    {
      try
      {
        IPEndPoint IPESource = FIpEndPoint;
        FRxDatagram = FUdpClient.Receive(ref IPESource);// OK !!! 
        if (FRxDatagram is Byte[])
        {
          //String Text = System.Text.Encoding.UTF8.GetString(UdpDatagram, 0, UdpDatagram.Length);
          //String Line = String.Format("Rxd[{0}]|{1}|",
          //              IPEndPointToText(IPESource), // IPEndpoint from sender!
          //              Text);
          //FNotifier.Write(Line);
          if (FOnDatagramReceived is DOnDatagramReceived)
          {
            FOnDatagramReceived(IPESource, FRxDatagram, FRxDatagram.Length);
          }
        }
        return true;
      }
      catch (Exception)
      {
        //FNotifier.Write(e.Message);
        //FNotifier.Write("UdpClient: Receive-Thread aborted");
        return false;
      }
    }
    private void TaskOnExecutionEnd(RTaskData data)
    {
    }
    private void TaskOnExecutionAbort(RTaskData data)
    {
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Method
    //-------------------------------------------------------------------
    //
    public override Boolean Open(IPAddress ipaddresstarget, UInt16 ipporttarget)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        FIpEndPoint = new IPEndPoint(ipaddresstarget, ipporttarget);
        // !!! OK FIpEndPoint = new IPEndPoint(IPAddress.Any, ipport);
        FUdpClient = new UdpClient(FIpEndPoint);
        //
        FTask = new CTask("UdpTextReceive",
                          TaskOnExecutionStart,
                          TaskOnExecutionBusy,
                          TaskOnExecutionEnd,
                          TaskOnExecutionAbort);
        FTask.Start();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Open(String sipendpointtarget)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        CUdpTextBase.TextToIPEndPoint(sipendpointtarget, out FIpEndPoint);
        FUdpClient = new UdpClient(FIpEndPoint);
        //
        FTask = new CTask("UdpTextReceive",
                          TaskOnExecutionStart,
                          TaskOnExecutionBusy,
                          TaskOnExecutionEnd,
                          TaskOnExecutionAbort);
        FTask.Start();
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    
    public override Boolean Close()
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        if (FUdpClient is UdpClient)
        {
          FUdpClient.Close();
          FUdpClient = null;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
