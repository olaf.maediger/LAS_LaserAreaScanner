﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
//
using Task;
//
namespace UdpTextTransfer
{
  //
  //-------------------------------------------------------------------
  //  Segment - Type - Callback
  //-------------------------------------------------------------------
  // 
  public delegate void DOnDatagramTransmitted(IPEndPoint ipendpointtarget,
                                              Byte[] datagram, Int32 size);
  //
  public delegate void DOnDatagramReceived(IPEndPoint ipendpointsource,
                                           Byte[] datagram, Int32 size);  
  //
  //-------------------------------------------------------------------
  //  Segment - Type - UdpText
  //-------------------------------------------------------------------
  // 
  public abstract class CUdpTextBase
  { //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //
    public const UInt16 UDP_SERVER_PORT_TRANSMIT = 12040;
    public const UInt16 UDP_SERVER_PORT_RECEIVE  = 12050;
    public const UInt16 UDP_CLIENT_PORT_TRANSMIT = 12050;
    public const UInt16 UDP_CLIENT_PORT_RECEIVE  = 12040;
    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    protected CTask FTask;
    protected DOnDatagramTransmitted FOnDatagramTransmitted;
    protected Byte[] FTxDatagram;
    protected DOnDatagramReceived FOnDatagramReceived;
    protected Byte[] FRxDatagram;
    protected UdpClient FUdpClient;
    protected IPEndPoint FIpEndPoint;
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    protected CUdpTextBase()
      : base()
    {
      FTask = null;
      FOnDatagramTransmitted = null;
      FTxDatagram = null;
      FOnDatagramReceived = null;
      FRxDatagram = null;
      FUdpClient = null;
      FIpEndPoint = null;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Helper - Static
    //-------------------------------------------------------------------
    //
    public static Boolean TextToIPAddress(String sipaddress, out IPAddress ipaddress)
    {
      ipaddress = null;
      try
      {
        if (IPAddress.TryParse(sipaddress, out ipaddress))
        {
          return (ipaddress is IPAddress);
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public static String IPAddressToText(IPEndPoint ipendpoint)
    {
      Byte[] AB = ipendpoint.Address.GetAddressBytes();
      return String.Format("{0}.{1}.{2}.{3}", AB[0], AB[1], AB[2], AB[3]);
    }


    public static Boolean TextToIPPort(String sipport, out UInt16 ipport)
    {
      ipport = 0;
      try
      {
        if (UInt16.TryParse(sipport, out ipport))
        {
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public static String IPPortToText(UInt16 ipport)
    {
      return String.Format("{0}", ipport);
    }


    public static Boolean TextToIPEndPoint(String sipendpoint, out IPEndPoint ipendpoint)
    {
      ipendpoint = null;
      try
      {
        String[] SEP = sipendpoint.Split(':');
        if (2 == SEP.Length)
        {
          IPAddress IPA;
          if (IPAddress.TryParse(SEP[0], out IPA))
          {
            Int32 IPP;
            if (Int32.TryParse(SEP[1], out IPP))
            {
              ipendpoint = new IPEndPoint(IPA, IPP);
              return (ipendpoint is IPEndPoint);
            }
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public static String IPEndPointToText(IPEndPoint ipendpoint)
    {
      Byte[] AB = ipendpoint.Address.GetAddressBytes();
      return String.Format("{0}.{1}.{2}.{3}:{4}",
                            AB[0], AB[1], AB[2], AB[3], ipendpoint.Port);
    }

    public static Boolean FindIPAddressLocal(out IPAddress ipalocal)
    {
      ipalocal = null;
      try
      {
        String HN = Dns.GetHostName();
        IPHostEntry IPHE = Dns.GetHostEntry(HN);// ok Dns.GetHostByName(HN);
        IPAddress[] IPAA = IPHE.AddressList;
        if (IPAA is IPAddress[])
        {
          if (0 < IPAA.Length)
          {
            Char[] Delimiters = new Char[1];
            Delimiters[0] = '.';
            foreach (IPAddress IPA in IPAA)
            {
              try
              {
                String SIPAddress = IPA.ToString();
                String[] Tokens = SIPAddress.Split(Delimiters,
                                                   StringSplitOptions.RemoveEmptyEntries);
                if (4 == Tokens.Length)
                {
                  Byte[] BA = new Byte[4];
                  BA[3] = byte.Parse(Tokens[3]);
                  BA[2] = byte.Parse(Tokens[2]);
                  BA[1] = byte.Parse(Tokens[1]);
                  BA[0] = byte.Parse(Tokens[0]);
                  ipalocal = new IPAddress(BA);
                  // return true;
                }
              }
              catch (Exception)// e)
              {
              }
            }
          }
        }
        return (ipalocal is IPAddress);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public static Boolean FindIPAddressBroadcast(out IPAddress ipalocal)
    {
      ipalocal = null;
      try
      {
        String HN = Dns.GetHostName();
        IPHostEntry IPHE = Dns.GetHostEntry(HN);// ok Dns.GetHostByName(HN);
        IPAddress[] IPAA = IPHE.AddressList;
        if (IPAA is IPAddress[])
        {
          if (0 < IPAA.Length)
          {
            Char[] Delimiters = new Char[1];
            Delimiters[0] = '.';
            foreach (IPAddress IPA in IPAA)
            {
              try
              {
                String SIPAddress = IPA.ToString();
                String[] Tokens = SIPAddress.Split(Delimiters,
                                                   StringSplitOptions.RemoveEmptyEntries);
                if (4 == Tokens.Length)
                {
                  Byte[] BA = new Byte[4];
                  BA[3] = 0xFF; // byte.Parse(Tokens[3]);
                  BA[2] = byte.Parse(Tokens[2]);
                  BA[1] = byte.Parse(Tokens[1]);
                  BA[0] = byte.Parse(Tokens[0]);
                  ipalocal = new IPAddress(BA);
                  return true;
                }
              }
              catch (Exception)// e)
              {
              }
            }
          }
        }
        return (ipalocal is IPAddress);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Management
    //-------------------------------------------------------------------
    //
    public abstract Boolean Open(IPAddress ipaddresstarget, UInt16 ipporttarget);
    public abstract Boolean Open(String sipendpointtarget);
    public abstract Boolean Close();

  }
}

