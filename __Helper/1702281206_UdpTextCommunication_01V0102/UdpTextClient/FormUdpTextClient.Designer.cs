﻿namespace UdpTextClient
{
  partial class FormUdpTextClient
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlTop = new System.Windows.Forms.Panel();
      this.tbxTxData = new System.Windows.Forms.TextBox();
      this.cbxSendPeriodic = new System.Windows.Forms.CheckBox();
      this.btnSend = new System.Windows.Forms.Button();
      this.lbxInfo = new System.Windows.Forms.ListBox();
      this.tmrSend = new System.Windows.Forms.Timer(this.components);
      this.pnlTop.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlTop
      // 
      this.pnlTop.Controls.Add(this.tbxTxData);
      this.pnlTop.Controls.Add(this.cbxSendPeriodic);
      this.pnlTop.Controls.Add(this.btnSend);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(384, 48);
      this.pnlTop.TabIndex = 1;
      // 
      // tbxTxData
      // 
      this.tbxTxData.Dock = System.Windows.Forms.DockStyle.Top;
      this.tbxTxData.Location = new System.Drawing.Point(0, 0);
      this.tbxTxData.Name = "tbxTxData";
      this.tbxTxData.Size = new System.Drawing.Size(384, 20);
      this.tbxTxData.TabIndex = 2;
      this.tbxTxData.Text = "Hello from Client!";
      // 
      // cbxSendPeriodic
      // 
      this.cbxSendPeriodic.AutoSize = true;
      this.cbxSendPeriodic.Location = new System.Drawing.Point(84, 27);
      this.cbxSendPeriodic.Name = "cbxSendPeriodic";
      this.cbxSendPeriodic.Size = new System.Drawing.Size(92, 17);
      this.cbxSendPeriodic.TabIndex = 1;
      this.cbxSendPeriodic.Text = "Send Periodic";
      this.cbxSendPeriodic.UseVisualStyleBackColor = true;
      this.cbxSendPeriodic.Click += new System.EventHandler(this.cbxSendPeriodic_CheckedChanged);
      // 
      // btnSend
      // 
      this.btnSend.Location = new System.Drawing.Point(3, 22);
      this.btnSend.Name = "btnSend";
      this.btnSend.Size = new System.Drawing.Size(75, 23);
      this.btnSend.TabIndex = 0;
      this.btnSend.Text = "Send";
      this.btnSend.UseVisualStyleBackColor = true;
      this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
      // 
      // lbxInfo
      // 
      this.lbxInfo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbxInfo.FormattingEnabled = true;
      this.lbxInfo.Location = new System.Drawing.Point(0, 48);
      this.lbxInfo.Name = "lbxInfo";
      this.lbxInfo.Size = new System.Drawing.Size(384, 313);
      this.lbxInfo.TabIndex = 2;
      // 
      // tmrSend
      // 
      this.tmrSend.Interval = 1000;
      this.tmrSend.Tick += new System.EventHandler(this.tmrSend_Tick);
      // 
      // FormUdpTextClient
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(384, 361);
      this.Controls.Add(this.lbxInfo);
      this.Controls.Add(this.pnlTop);
      this.Name = "FormUdpTextClient";
      this.Text = "UdpTextClient";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormUdpTextClient_FormClosing);
      this.pnlTop.ResumeLayout(false);
      this.pnlTop.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.TextBox tbxTxData;
    private System.Windows.Forms.CheckBox cbxSendPeriodic;
    private System.Windows.Forms.Button btnSend;
    private System.Windows.Forms.ListBox lbxInfo;
    private System.Windows.Forms.Timer tmrSend;
  }
}

