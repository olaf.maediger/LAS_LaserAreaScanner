﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using System.Net;
using Task;
using UdpTextTransfer;
//
namespace UdpTextClient
{
  public partial class FormUdpTextClient : Form
  {
    private CUdpTextTransmit FUdpTextTransmit;
    private CUdpTextReceive FUdpTextReceive; 
    //
    public FormUdpTextClient()
    {
      InitializeComponent();
      StartPosition = FormStartPosition.Manual;
      Left = 20;
      Top = 20;
      Width = 640;
      Height = 480;
      //
      FUdpTextTransmit = new CUdpTextTransmit();
      FUdpTextReceive = new CUdpTextReceive();
      //
      IPAddress IPATarget;
      CUdpTextBase.FindIPAddressBroadcast(out IPATarget);
      FUdpTextTransmit.Open(IPATarget, CUdpTextBase.UDP_CLIENT_PORT_TRANSMIT);
      FUdpTextTransmit.SetOnDatagramTransmitted(UdpTextTransmitOnDatagramTransmitted);
      //
      IPAddress IPALocal;
      CUdpTextBase.FindIPAddressLocal(out IPALocal);
      if (FUdpTextReceive.Open(IPALocal, CUdpTextBase.UDP_CLIENT_PORT_RECEIVE))
      {
        FUdpTextReceive.SetOnDatagramReceived(UdpTextReceiveOnDatagramReceived);
      }
    }



    private delegate void CBUdpTextTransmitOnDatagramTransmitted(IPEndPoint ipendpoint,
                                                                 Byte[] datagram,
                                                                 Int32 size);
    private void UdpTextTransmitOnDatagramTransmitted(IPEndPoint ipendpoint,
                                                      Byte[] datagram,
                                                      Int32 size)
    {
      if (this.InvokeRequired)
      {
        CBUdpTextTransmitOnDatagramTransmitted CB =
          new CBUdpTextTransmitOnDatagramTransmitted(UdpTextTransmitOnDatagramTransmitted);
        Invoke(CB, new object[] { ipendpoint, datagram, size });
      }
      else
      {
        String Line = Encoding.ASCII.GetString(datagram, 0, size);
        lbxInfo.Items.Add(Line);
      }
    }

    private delegate void CBUdpTextTransmitOnDatagramReceived(IPEndPoint ipendpoint,
                                                              Byte[] datagram,
                                                              Int32 size);
    private void UdpTextReceiveOnDatagramReceived(IPEndPoint ipendpoint,
                                                  Byte[] datagram,
                                                  Int32 size)
    {
      if (this.InvokeRequired)
      {
        CBUdpTextTransmitOnDatagramReceived CB =
          new CBUdpTextTransmitOnDatagramReceived(UdpTextReceiveOnDatagramReceived);
        Invoke(CB, new object[] { ipendpoint, datagram, size });
      }
      else
      {
        String Line = Encoding.ASCII.GetString(datagram, 0, size);
        lbxInfo.Items.Add(Line);
      }
    }



    private void btnSend_Click(object sender, EventArgs e)
    {
      String TxText = tbxTxData.Text;
      Byte[] TxByteVector = Encoding.ASCII.GetBytes(TxText);
      FUdpTextTransmit.PresetDatagram(TxByteVector);
    }

    private void cbxSendPeriodic_CheckedChanged(object sender, EventArgs e)
    {
      tmrSend.Interval = 1000;
      tmrSend.Enabled = cbxSendPeriodic.Checked;
    }

    private void tmrSend_Tick(object sender, EventArgs e)
    {
      String TxText = tbxTxData.Text;
      Byte[] TxByteVector = Encoding.ASCII.GetBytes(TxText);
      FUdpTextTransmit.PresetDatagram(TxByteVector);
    }

    private void FormUdpTextClient_FormClosing(object sender, FormClosingEventArgs e)
    {
      tmrSend.Enabled = false;
      FUdpTextTransmit.Close();
      FUdpTextReceive.Close();
    }

  }
}
