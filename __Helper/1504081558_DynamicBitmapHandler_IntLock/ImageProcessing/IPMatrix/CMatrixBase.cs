﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPMatrix
{
  public abstract class CMatrixBase
  { //
    //-----------------------------------------------------------------------------
    //  Segment - Constant
    //-----------------------------------------------------------------------------
    //
    protected const Int32 INIT_ROWCOUNT = 1;
    protected const Int32 INIT_COLCOUNT = 1;
    //
    protected const Int32 SHIFT_256_256 = 0;
    protected const Int32 SHIFT_256_1K = 2;
    protected const Int32 SHIFT_256_4K = 4;
    //
    protected const Int32 SHIFT_1K_256 = 2;
    protected const Int32 SHIFT_1K_4K = 2;
    //
    protected const Int32 SHIFT_4K_256 = 4;
    protected const Int32 SHIFT_4K_1K = 2;
    //
    protected const Int32 SIZE_QUAD = 4;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------------
    //

    //
    //-----------------------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------------------
    //
    public CMatrixBase()
    {
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------------------
    //
    protected abstract Int32 GetRowCount();
    protected abstract Int32 GetColCount();

    public Int32 RowCount
    {
      get { return GetRowCount(); }
    }
    public Int32 ColCount
    {
      get { return GetColCount(); }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function
    //-----------------------------------------------------------------------------
    //
    public abstract Boolean BuildConstant(Int32 rowcount, Int32 colcount, Int32 value);
    public abstract Boolean BuildLinear(Int32 rowcount, Int32 colcount);
    public abstract Boolean BuildInverse(Int32 rowcount, Int32 colcount);
    public abstract Boolean BuildRandom(Int32 rowcount, Int32 colcount);
    //
    public abstract Byte[] CloneDataVector256();
    public abstract Byte[] CloneDataVectorQuad256();
    //
    public abstract UInt16[] CloneDataVector1K();
    public abstract UInt16[] CloneDataVectorQuad1K();
    //
    public abstract UInt16[] CloneDataVector4K();
    public abstract UInt16[] CloneDataVectorQuad4K();
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone256
    //-----------------------------------------------------------------------------
    //
    protected Byte[] CloneDataVector256(CMatrixBase matrix)
    {
      Int32 RC = matrix.GetRowCount();
      Int32 CC = matrix.GetColCount();
      Byte[] Result = null;
      Int32 BI = 0;
      if (matrix is CMatrix256)
      {
        Result = new Byte[RC * CC * sizeof(Byte)];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Result[BI] = (Byte)(((CMatrix256)matrix)[RI, CI] >> SHIFT_256_256);
            BI++;
          }
        }
      } else
        if (matrix is CMatrix1K)
        {
          Result = new Byte[RC * CC * sizeof(UInt16)];
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Result[BI] = (Byte)(((CMatrix1K)matrix)[RI, CI] >> SHIFT_1K_256);
              BI++;
            }
          }
        }
          else
          if (matrix is CMatrix4K)
          {
            Result = new Byte[RC * CC * sizeof(UInt16)];
            for (Int32 RI = 0; RI < RC; RI++)
            {
              for (Int32 CI = 0; CI < CC; CI++)
              {
                Result[BI] = (Byte)(((CMatrix4K)matrix)[RI, CI] >> SHIFT_4K_256);
                BI++;
              }
            }
          }
      return Result;
    }

    public Byte[] CloneDataVectorQuad256(CMatrixBase matrix)
    {
      Int32 RC = matrix.GetRowCount();
      Int32 CC = matrix.GetColCount();
      Byte[] Result = new Byte[SIZE_QUAD * RC * CC];
      Int32 BI = 0;
      if (matrix is CMatrix256)
      {
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte BValue = (Byte)(((CMatrix256)matrix)[RI, CI] >> SHIFT_256_256);
            Result[BI] = BValue;
            BI++;
            Result[BI] = BValue;
            BI++;
            Result[BI] = BValue;
            BI++;
            Result[BI] = 0xFF;
            BI++;
          }
        }
      }
      else
        if (matrix is CMatrix1K)
        {
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Byte BValue = (Byte)(((CMatrix1K)matrix)[RI, CI] >> SHIFT_1K_256);
              Result[BI] = BValue;
              BI++;
              Result[BI] = BValue;
              BI++;
              Result[BI] = BValue;
              BI++;
              Result[BI] = 0xFF;
              BI++;
            }
          }
        }
        else
          if (matrix is CMatrix4K)
          {
            for (Int32 RI = 0; RI < RC; RI++)
            {
              for (Int32 CI = 0; CI < CC; CI++)
              {
                Byte BValue = (Byte)(((CMatrix4K)matrix)[RI, CI] >> SHIFT_4K_256);
                Result[BI] = BValue;
                BI++;
                Result[BI] = BValue;
                BI++;
                Result[BI] = BValue;
                BI++;
                Result[BI] = 0xFF;
                BI++;
              }
            }
          }
      return Result;
    }


  }
}
