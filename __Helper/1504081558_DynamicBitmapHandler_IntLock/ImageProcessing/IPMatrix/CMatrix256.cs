﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPMatrix
{
  public class CMatrix256 : CMatrixBase
  {
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constant
    //-----------------------------------------------------------------------------
    //
    private const Byte INIT_VALUE_LOW = 0x00;
    private const Byte INIT_VALUE_HIGH = 0xFF;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------------
    //
    protected Byte[,] FMatrix;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------------------
    //
    public CMatrix256()
    {
      FMatrix = new Byte[INIT_ROWCOUNT, INIT_COLCOUNT];
    }

    public CMatrix256(Int32 sizey, Int32 sizex)
    {
      Int32 SY = Math.Max(INIT_ROWCOUNT, sizey);
      Int32 SX = Math.Max(INIT_COLCOUNT, sizex);
      FMatrix = new Byte[SY, SX];
    }

    public CMatrix256(Int32 sizey, Int32 sizex, Byte init)
    {
      Int32 RC = Math.Max(INIT_ROWCOUNT, sizey);
      Int32 CC = Math.Max(INIT_COLCOUNT, sizex);
      FMatrix = new Byte[RC, CC];
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          FMatrix[RI, CI] = (Byte)init;
        }
      }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------------------
    //
    protected override Int32 GetRowCount()
    {
      return FMatrix.GetLength(0);
    }
    protected override Int32 GetColCount()
    {
      return FMatrix.GetLength(1);
    }

    protected Byte GetValue(Int32 row, Int32 col)
    {
      return FMatrix[row, col];
    }
    protected void SetValue(Int32 row, Int32 col, Byte value)
    {
      FMatrix[row, col] = value;
    }
    public Byte this[Int32 row, Int32 col]
    {
      get { return GetValue(row, col); }
      set { SetValue(row, col, value); }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone256
    //-----------------------------------------------------------------------------
    //
    public override Byte[] CloneDataVector256()
    {
      return base.CloneDataVector256(this);
    }

    public override Byte[] CloneDataVectorQuad256()
    {
      return base.CloneDataVectorQuad256(this);
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone1K
    //-----------------------------------------------------------------------------
    //
    public override UInt16[] CloneDataVector1K()
    {
      Int32 RC = FMatrix.GetLength(0);
      Int32 CC = FMatrix.GetLength(1);
      UInt16[] Result = new UInt16[RC * CC];
      Int32 BI = 0;
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          Result[BI] = (UInt16)(FMatrix[RI, CI] << SHIFT_256_1K);
          BI++;
        }
      }
      return Result;
    }

    public override UInt16[] CloneDataVectorQuad1K()
    {
      Int32 RC = FMatrix.GetLength(0);
      Int32 CC = FMatrix.GetLength(1);
      UInt16[] Result = new UInt16[4 * RC * CC];
      Int32 BI = 0;
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          UInt16 UValue = (UInt16)(FMatrix[RI, CI] << SHIFT_256_1K);
          Result[BI] = UValue;
          BI++;
          Result[BI] = UValue;
          BI++;
          Result[BI] = UValue;
          BI++;
          Result[BI] = 0x3FF;
          BI++;
        }
      }
      return Result;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone4K
    //-----------------------------------------------------------------------------
    //
    public override UInt16[] CloneDataVector4K()
    {
      Int32 RC = FMatrix.GetLength(0);
      Int32 CC = FMatrix.GetLength(1);
      UInt16[] Result = new UInt16[RC * CC];
      Int32 BI = 0;
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          Result[BI] = (UInt16)(FMatrix[RI, CI] << SHIFT_256_4K);
          BI++;
        }
      }
      return Result;
    }

    public override UInt16[] CloneDataVectorQuad4K()
    {
      Int32 RC = FMatrix.GetLength(0);
      Int32 CC = FMatrix.GetLength(1);
      UInt16[] Result = new UInt16[4 * RC * CC];
      Int32 BI = 0;
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          UInt16 UValue = (UInt16)(FMatrix[RI, CI] << SHIFT_256_4K);
          Result[BI] = UValue;
          BI++;
          Result[BI] = UValue;
          BI++;
          Result[BI] = UValue;
          BI++;
          Result[BI] = 0x3FF;
          BI++;
        }
      }
      return Result;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Build
    //-----------------------------------------------------------------------------
    //
    public override Boolean BuildConstant(Int32 rowcount, Int32 colcount, Int32 value)
    {
      try
      {
        Int32 RC = rowcount;
        Int32 CC = colcount;
        FMatrix = new Byte[RC, CC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FMatrix[RI, CI] = (Byte)value;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildLinear(Int32 rowcount, Int32 colcount)
    {
      try
      {
        Int32 RC = rowcount;
        Int32 CC = colcount;
        FMatrix = new Byte[RC, CC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FMatrix[RI, CI] = (Byte)((CI * INIT_VALUE_HIGH) / (CC - 1));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildInverse(Int32 rowcount, Int32 colcount)
    {
      try
      {
        Int32 RC = rowcount;
        Int32 CC = colcount;
        FMatrix = new Byte[RC, CC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FMatrix[RI, CI] = (Byte)(INIT_VALUE_HIGH - CI * INIT_VALUE_HIGH / (CC - 1));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildRandom(Int32 rowcount, Int32 colcount)
    {
      try
      {
        Random R = new Random((Int32)DateTime.Now.Ticks);
        Int32 RC = rowcount;
        Int32 CC = colcount;
        FMatrix = new Byte[RC, CC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FMatrix[RI, CI] = (Byte)R.Next(INIT_VALUE_LOW, 1 + INIT_VALUE_HIGH);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
