﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMatrix;
//
namespace IPBitmap
{
  public partial class CBitmap1KPalette : CBitmapPaletteBase
  {
    protected Boolean FreeBitmapColored()
    {
      return false;
    }

    protected Boolean BuildBitmapColored()
    {
      return false;
    }

    protected Boolean BuildBitmapColored(Int32 width, Int32 height, Byte[] data)
    {
      return false;
    }

    protected Boolean BuildBitmapColored(Int32 width, Int32 height, UInt16[] data)
    {
      return false;
    }

    protected Boolean BuildBitmapColored(Int32 width, Int32 height, Int16[] data)
    {
      return false;
    }

    protected Boolean BuildBitmapColored(Int32 width, Int32 height, UInt32[] data)
    {
      return false;
    }

    protected Boolean BuildBitmapColored(Int32 width, Int32 height, Int32[] data)
    {
      return false;
    }

    protected Boolean BuildBitmapColored(Int32 width, Int32 height, Double[] data)
    {
      return false;
    }

    protected Boolean BuildBitmapColored(CMatrixBase matrix)
    {
      return false;
    }
  }
}
