﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
using IPPalette;
//
namespace IPBitmap
{
  public partial class CBitmap1KPalette : CBitmapPaletteBase
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //
    protected const Int32 INIT_BITSPERPIXEL = 64;
    protected const PixelFormat INIT_PIXELFORMAT = PixelFormat.Format64bppArgb;
    //
    public const Int32 INTENSITY_MINIMUM = 0;
    public const Int32 INTENSITY_MAXIMUM = 1023;
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //      

    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //
    protected override Int32 GetBitsPerPixel()
    {
      return INIT_BITSPERPIXEL;
    }

    protected override PixelFormat GetPixelFormat()
    {
      return INIT_PIXELFORMAT;
    }
    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmap1KPalette(Bitmap bitmapgreyscale,
                            EPalette1K palette)
    {
      FBitmapGreyscale = (Bitmap)bitmapgreyscale.Clone();
      FPalette = CIPPalette1K.CreatePalette(palette);
      BuildBitmapColoredFromGreyscale();
    }

    public CBitmap1KPalette(Bitmap bitmapgreyscale,
                            CPalette1KBase palette)
    {
      FBitmapGreyscale = (Bitmap)bitmapgreyscale.Clone();
      FPalette = palette;
      BuildBitmapColoredFromGreyscale();
    }
    //
    //--------------------------------------------------------
    //	Segment - Property - Common
    //--------------------------------------------------------
    //
    public override Int32 GetWidth()
    {
      return FBitmapGreyscale.Width;
    }
    public override Int32 GetHeight()
    {
      return FBitmapGreyscale.Height;
    }
    //
    //--------------------------------------------------------
    //	Segment - Property - Palette
    //--------------------------------------------------------
    //
    private CPalette1KBase GetPalette1K()
    {
      return (CPalette1KBase)FPalette;
    }
    private void SetPalette1K(CPalette1KBase value)
    {
      FPalette = value;
      BuildBitmapColored();
      if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
      {
        FOnBitmapColoredChanged();
      }
    }
    public CPalette1KBase Palette1K
    {
      get { return GetPalette1K(); }
      set { SetPalette1K(value); }
    }

    public void SetPalette1K(EPalette1K value)
    {
      FPalette = CIPPalette1K.CreatePalette(value);
      BuildBitmapColored();
      if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
      {
        FOnBitmapColoredChanged();
      }
    }

    public UInt16[,] MatrixQuadGreyscale
    {
      get { return CreateMatrixQuadFromBitmapGreyscale(FBitmapGreyscale); }
      set { CreateBitmapGreyscaleFromMatrix(value); }
    }

    public UInt16[,] MatrixColored
    {
      get { return CreateMatrixFromBitmapColored(FBitmapColored); }
      set { CreateBitmapColoredFromMatrix(value, Color.White); }    // !!!!!!!!!!!!!!!!!!!!! ändern!!!!!!!!!          
    }
    //
    //--------------------------------------------------------
    //	Segment - Helper
    //--------------------------------------------------------
    //

    //
    //#################################################################################################
    //  Section - Bitmap1KPalette - Common - Order BGRA
    //#################################################################################################
    //
    // BitmapColored + Palette -> BitmapGreyscale
    static private Bitmap BuildBitmapGreyscaleFromColored(Bitmap bitmapcolored, CPalette1KBase palette)
    {
      try
      {
        Int32 BitmapWidth = bitmapcolored.Width;
        Int32 BitmapHeight = bitmapcolored.Height;
        Bitmap BitmapGreyscale = new Bitmap(BitmapWidth, BitmapHeight, INIT_PIXELFORMAT_1K);
        // Source - Colored
        Rectangle RSource = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDSource = bitmapcolored.LockBits(RSource, ImageLockMode.ReadOnly, bitmapcolored.PixelFormat);
        IntPtr PointerSource = BDSource.Scan0;
        Int32 StrideSource = BDSource.Stride;
        // Target - Greyscale
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapGreyscale.LockBits(RTarget, ImageLockMode.WriteOnly, BitmapGreyscale.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        // 
        unsafe
        {
          Byte BL, BH;
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PSource = (Byte*)(PointerSource + RI * StrideSource);
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            UInt16 ColorIndex = 0;
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Source
              // Blue-Value
              BL = *(Byte*)PSource; PSource++;
              BH = *(Byte*)PSource; PSource++;
              ColorIndex = (UInt16)(((UInt16)BH << 8) | ((UInt16)BL));
              Byte PartBlue = (Byte)(ColorIndex >> CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              // Green-Value
              BL = *(Byte*)PSource; PSource++;
              BH = *(Byte*)PSource; PSource++;
              ColorIndex = (UInt16)(((UInt16)BH << 8) | ((UInt16)BL));
              Byte PartGreen = (Byte)(ColorIndex >> CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              // Red-Value
              BL = *(Byte*)PSource; PSource++;
              BH = *(Byte*)PSource; PSource++;
              ColorIndex = (UInt16)(((UInt16)BH << 8) | ((UInt16)BL));
              Byte PartRed = (Byte)(ColorIndex >> CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              // Alpha-Value
              BL = *(Byte*)PSource; PSource++;
              BH = *(Byte*)PSource; PSource++;
              UInt16 PartAlpha = (UInt16)(((UInt16)BH << 8) | ((UInt16)BL));
              // Target
              ColorIndex = palette.FindPaletteIndex((Byte)(PartRed + 2),
                                                    (Byte)(PartGreen + 2),
                                                    (Byte)(PartBlue + 2));
              ColorIndex = (UInt16)(ColorIndex << CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              BL = (Byte)((0x00FF & ColorIndex) >> 0);
              BH = (Byte)((0xFF00 & ColorIndex) >> 8);
              // Blue-Value
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Green-Value
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Red-Value
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Alpha-Value
              BL = (Byte)((0x00FF & PartAlpha) >> 0);
              BH = (Byte)((0xFF00 & PartAlpha) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
            }
          }
        }
        bitmapcolored.UnlockBits(BDSource);
        BitmapGreyscale.UnlockBits(BDTarget);
        return BitmapGreyscale;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    //
    /*/ BitmapGreyscale + Palette -> BitmapColored
    static public override Bitmap BuildBitmapColoredFromGreyscale()//Bitmap bitmapgreyscale, CPalette1KBase palette)
    {
      try
      {
/ *#if FASTLIBRARY
        / *Int32 BitmapWidth = bitmapgreyscale.Width;
        Int32 BitmapHeight = bitmapgreyscale.Height;
        Bitmap BitmapColored = new Bitmap(BitmapWidth, BitmapHeight, INIT_PIXELFORMAT_1K);
        // Source - Greyscale
        Rectangle RSource = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDSource = bitmapgreyscale.LockBits(RSource, ImageLockMode.ReadOnly, bitmapgreyscale.PixelFormat);
        IntPtr PointerSource = BDSource.Scan0;
        Int32 StrideSource = BDSource.Stride;
        // Target - Colored
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapColored.LockBits(RTarget, ImageLockMode.WriteOnly, BitmapColored.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        // 
        unsafe
        {
          Byte BL, BH;
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PSource = (Byte*)(PointerSource + RI * StrideSource);
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            UInt16 ColorIndex = 0;
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Source
              // Blue-Value
              BL = *(Byte*)PSource; PSource++;
              BH = *(Byte*)PSource; PSource++;
              ColorIndex = (UInt16)(((UInt16)BH << 8) | ((UInt16)BL));
              ColorIndex = (UInt16)(ColorIndex >> CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              // Green-Value (NC)
              PSource++;
              PSource++;
              // Red-Value (NC)
              PSource++;
              PSource++;
              // Alpha-Value
              BL = *(Byte*)PSource; PSource++;
              BH = *(Byte*)PSource; PSource++;
              UInt16 PartAlpha = (UInt16)(((UInt16)BH << 8) | ((UInt16)BL));
              // Target
              Byte PartBlue = palette.GetPartsBlue(ColorIndex);
              Byte PartGreen = palette.GetPartsGreen(ColorIndex);
              Byte PartRed = palette.GetPartsRed(ColorIndex);
              // Blue-Value
              UInt16 ColorValue = (UInt16)(PartBlue << CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              BL = (Byte)((0x00FF & ColorValue) >> 0);
              BH = (Byte)((0xFF00 & ColorValue) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Green-Value
              ColorValue = (UInt16)(PartGreen << CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              BL = (Byte)((0x00FF & ColorValue) >> 0);
              BH = (Byte)((0xFF00 & ColorValue) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Red-Value
              ColorValue = (UInt16)(PartRed << CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              BL = (Byte)((0x00FF & ColorValue) >> 0);
              BH = (Byte)((0xFF00 & ColorValue) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Alpha-Value
              BL = (Byte)((0x00FF & PartAlpha) >> 0);
              BH = (Byte)((0xFF00 & PartAlpha) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
            }
          }
        }
        bitmapgreyscale.UnlockBits(BDSource);
        BitmapColored.UnlockBits(BDTarget);
         
#else
#endif* /
        return null;// BitmapColored;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }*/
    //
    // BitmapColored + Palette -> BitmapGreyscale
    protected Boolean BuildBitmapGreyscale()
    {
      try
      {
        Int32 BW = FBitmapColored.Width;
        Int32 BH = FBitmapColored.Height;
        FBitmapGreyscale = new Bitmap(BW, BH, INIT_PIXELFORMAT_1K);
        // Source - Colored
        Rectangle RSource = new Rectangle(0, 0, BW, BH);
        BitmapData BDSource = FBitmapColored.LockBits(RSource, ImageLockMode.ReadOnly, INIT_PIXELFORMAT_1K);
        IntPtr PointerSource = BDSource.Scan0;
        Int32 StrideSource = BDSource.Stride;
        // Target - Greyscale
        Rectangle RTarget = new Rectangle(0, 0, BW, BH);
        BitmapData BDTarget = FBitmapGreyscale.LockBits(RTarget, ImageLockMode.WriteOnly, INIT_PIXELFORMAT_1K);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        // 
        unsafe
        {
          for (Int32 RI = 0; RI < BH; RI++)
          {
            UInt16* PSource = (UInt16*)(PointerSource + RI * StrideSource);
            UInt16* PTarget = (UInt16*)(PointerTarget + RI * StrideTarget);
            for (Int32 CI = 0; CI < BW; CI++)
            { // Source
              // Blue-Value
              Byte ColorBlue = (Byte)((*(UInt16*)PSource) >> CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              PSource++;
              // Green-Value
              Byte ColorGreen = (Byte)((*(UInt16*)PSource) >> CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              PSource++;
              // Red-Value
              Byte ColorRed = (Byte)((*(UInt16*)PSource) >> CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              PSource++;
              // Alpha-Value
              UInt16 ColorAlpha = *(UInt16*)PSource;
              PSource++;
              // Target
              //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!UInt16 ColorIndex = FPalette.FindColorIndex(ColorRed, ColorGreen, ColorBlue);
              // Blue-Value
              //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*PTarget = (UInt16)(ColorIndex << CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              PTarget++;
              // Green-Value
              //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*PTarget = (UInt16)(ColorIndex << CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              PTarget++;
              // Red-Value
              //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*PTarget = (UInt16)(ColorIndex << CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              PTarget++;
              // Alpha-Value
              *PTarget = ColorAlpha;
              PTarget++;
            }
          }
        }
        FBitmapColored.UnlockBits(BDSource);
        FBitmapGreyscale.UnlockBits(BDTarget);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    /*!!!!!!!!!!!!!!!!!!! -> / BitmapGreyscale + Palette -> BitmapColored
    protected Boolean BuildBitmapColored()
    {
      try
      {
        Int32 BW = FBitmapGreyscale.Width;
        Int32 BH = FBitmapGreyscale.Height;
        FBitmapColored = new Bitmap(BW, BH, INIT_PIXELFORMAT_1K);
        // Source - Greyscale
        Rectangle RSource = new Rectangle(0, 0, BW, BH);
        BitmapData BDSource = FBitmapGreyscale.LockBits(RSource, ImageLockMode.ReadOnly, INIT_PIXELFORMAT_1K);
        IntPtr PointerSource = BDSource.Scan0;
        Int32 StrideSource = BDSource.Stride;
        // Target - Colored
        Rectangle RTarget = new Rectangle(0, 0, BW, BH);
        BitmapData BDTarget = FBitmapColored.LockBits(RTarget, ImageLockMode.WriteOnly, INIT_PIXELFORMAT_1K);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        // 
        unsafe
        {
          for (Int32 RI = 0; RI < BH; RI++)
          {
            UInt16* PSource = (UInt16*)(PointerSource + RI * StrideSource);
            UInt16* PTarget = (UInt16*)(PointerTarget + RI * StrideTarget);
            for (Int32 CI = 0; CI < BW; CI++)
            { // Source (ColorIndex only from one Channel(Blue))
              UInt16 ColorIndex = (UInt16)((*(UInt16*)PSource) >> CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              UInt16 ColorAlpha = (UInt16)(*(UInt16*)(PSource + 3));
              PSource += 4;
              // Target
              // Blue-Value
              //!!!!!!!!!!!!!!!!!!!!!!*PTarget = (UInt16)(FPalette.GetPartsBlue(ColorIndex) << CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              PTarget++;
              // Green-Value
              //!!!!!!!!!!!!!!!!!!!!!!*PTarget = (UInt16)(FPalette.GetPartsGreen(ColorIndex) << CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              PTarget++;
              // Red-Value
              //!!!!!!!!!!!!!!!!!!!!!!*PTarget = (UInt16)(FPalette.GetPartsRed(ColorIndex) << CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              PTarget++;
              // Alpha-Value
              *PTarget = ColorAlpha;// Intransparent: (UInt16)(1023 << CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              PTarget++;
            }
          }
        }        
        FBitmapGreyscale.UnlockBits(BDSource);
        FBitmapColored.UnlockBits(BDTarget);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }      
    }*/

    protected Boolean BuildPalette1K(EPalette1K palette)
    {
      try
      {
        //!!!!!!!!!!!!!!!!!!!!!!FPalette1K = CIPPalette1K.CreatePalette(palette);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - BmpFile
    //--------------------------------------------------------
    //
    public Boolean SaveGreyscaleToBmpFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        Boolean Result = Bitmap1KTransfer.SaveBitmap1K(fileentry, FBitmapGreyscale, true, ImageFormat.Bmp);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToBmpFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        Boolean Result = Bitmap1KTransfer.SaveBitmap1K(fileentry, FBitmapColored, true, ImageFormat.Bmp);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadGreyscaleFromBmpFile(String fileentry)//,
                                            //EPalette1K palette)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapGreyscale = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapGreyscale is Bitmap)
        {
          if (INIT_PIXELFORMAT_1K == FBitmapGreyscale.PixelFormat)
          {
            // ??? BuildPalette1K(palette);
            BuildBitmapColored();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromBmpFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapColored = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapColored is Bitmap)
        {
          if (INIT_PIXELFORMAT_1K == FBitmapColored.PixelFormat)
          {
            // ??? BuildPalette1K(palette);
            BuildBitmapGreyscale();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - TifFile
    //--------------------------------------------------------
    //
    public Boolean SaveGreyscaleToTifFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        Boolean Result = Bitmap1KTransfer.SaveBitmap1K(fileentry, FBitmapGreyscale, true, ImageFormat.Tiff);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToTifFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        Boolean Result = Bitmap1KTransfer.SaveBitmap1K(fileentry, FBitmapColored, true, ImageFormat.Tiff);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadGreyscaleFromTifFile(String fileentry)//,
                                            //EPalette1K palette)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapGreyscale = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapGreyscale is Bitmap)
        {
          if (INIT_PIXELFORMAT_1K == FBitmapGreyscale.PixelFormat)
          {
            // ??? BuildPalette1K(palette);
            BuildBitmapColored();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromTifFile(String fileentry)//,
                                          //EPalette1K palette)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapColored = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapColored is Bitmap)
        {
          if (INIT_PIXELFORMAT_1K == FBitmapColored.PixelFormat)
          {
            // ??? BuildPalette1K(palette);
            BuildBitmapGreyscale();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - PngFile
    //--------------------------------------------------------
    //
    public Boolean SaveGreyscaleToPngFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        Boolean Result = Bitmap1KTransfer.SaveBitmap1K(fileentry, FBitmapGreyscale, true, ImageFormat.Png);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToPngFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        Boolean Result = Bitmap1KTransfer.SaveBitmap1K(fileentry, FBitmapColored, true, ImageFormat.Png);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadGreyscaleFromPngFile(String fileentry)//,
                                            //EPalette1K palette)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapGreyscale = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapGreyscale is Bitmap)
        {
          if (INIT_PIXELFORMAT_1K == FBitmapGreyscale.PixelFormat)
          {
            // ??? BuildPalette1K(palette);
            BuildBitmapColored();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromPngFile(String fileentry)//,
                                          //EPalette1K palette)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapColored = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapColored is Bitmap)
        {
          if (INIT_PIXELFORMAT_1K == FBitmapGreyscale.PixelFormat)
          {
            // ??? BuildPalette1K(palette);
            BuildBitmapGreyscale();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - TextFile
    //--------------------------------------------------------
    //
    public Boolean SaveGreyscaleToTextFile(String fileentry)
    { // 1412180928 OK
      try
      { 
        CTextFile TextFile = new CTextFile();
        Boolean Result = TextFile.OpenWrite(fileentry);
        // Bitmap
        UInt16[,] MatrixBitmap1K = CreateMatrixQuadFromBitmapGreyscale(FBitmapGreyscale);
        Result &= TextFile.Write(MatrixBitmap1K);
        // Palette
        /*!!!!!!!!!!!!!!!!!!!!!!!!!String NamePalette = FPalette.GetName();
        TextFile.Write(NamePalette);
        TextFile.WriteTokenDelimiter();
        Byte[,] MatrixPalette1K = CreateMatrixFromPalette(FPalette);
        Result &= TextFile.Write(MatrixPalette1K);*/
        Result &= TextFile.Close();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToTextFile(String fileentry)
    {
      try
      {
        CTextFile TextFile = new CTextFile();
        Boolean Result = TextFile.OpenWrite(fileentry);
        UInt16[,] Matrix = CreateMatrixFromBitmapColored(FBitmapColored);
        Result &= TextFile.Write(Matrix);
        // ??? Byte[,] Palette = CIPBitmap1K.GetMatrixFromPalette(FPalette1K);
        // ??? Result &= TextFile.Write(Palette);
        Result &= TextFile.Close();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    
    public Boolean LoadGreyscaleFromTextFile(String fileentry)
    { // 1412180928 ------
      try
      {
        CTextFile TextFile = new CTextFile();
        Boolean Result = TextFile.OpenRead(fileentry);
        // Bitmap
        UInt16[,] MatrixBitmap1K = null;
        Result &= TextFile.Read(out MatrixBitmap1K);
        FBitmapGreyscale = CreateBitmapGreyscaleFromMatrixQuads(MatrixBitmap1K);
        // Palette
        String NamePalette1K = "?";
        TextFile.Read(out NamePalette1K);
        if (CIPPalette1K.Exists(NamePalette1K))
        {
          FPalette = CIPPalette1K.CreatePalette(NamePalette1K);
        }
        else
        {
          Byte[,] MatrixPalette1K;
          Result &= TextFile.Read(out MatrixPalette1K);
          FPalette = CIPPalette1K.CreatePalette1KFromMatrix(MatrixPalette1K);
        }
        //
        Result &= TextFile.Close();
        if (FBitmapGreyscale is Bitmap)
        {
          if (INIT_PIXELFORMAT_1K == FBitmapGreyscale.PixelFormat)
          {
            // ??? BuildPalette1K(palette);
            BuildBitmapColored();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
          }
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromTextFile(String fileentry)
                                           //EPalette1K palette)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapColored = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapColored is Bitmap)
        {
          if (INIT_PIXELFORMAT_1K == FBitmapColored.PixelFormat)
          {
            // ??? BuildPalette1K(palette);
            BuildBitmapGreyscale();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - TaggedBnaryFile
    //--------------------------------------------------------
    //
    public Boolean SaveGreyscaleToTaggedBinaryFile(String fileentry)
    {
      try
      {
        CTaggedBinaryFile TaggedBinaryFile = new CTaggedBinaryFile();
        Boolean Result = TaggedBinaryFile.OpenWrite(fileentry);
        UInt16[,] Matrix = CreateMatrixQuadFromBitmapGreyscale(FBitmapGreyscale);
        Result &= TaggedBinaryFile.Write(Matrix);
        // ??? CPalette1KBase Palette1KGreyscale = CIPPalette1K.CreatePalette(EPalette1K.Greyscale);
        // ??? Byte[,] Palette = CIPBitmap1K.GetMatrixFromPalette(Palette1KGreyscale);
        // ??? Result &= TaggedBinaryFile.Write(Palette);
        Result &= TaggedBinaryFile.CloseWrite();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToTaggedBinaryFile(String fileentry)
    {
      try
      {
        CTaggedBinaryFile TaggedBinaryFile = new CTaggedBinaryFile();
        Boolean Result = TaggedBinaryFile.OpenWrite(fileentry);
        UInt16[,] Matrix = CreateMatrixFromBitmapColored(FBitmapColored);
        Result &= TaggedBinaryFile.Write(Matrix);
        // ??? Byte[,] Palette = CIPBitmap1K.GetMatrixFromPalette(FPalette1K);
        // ??? Result &= TaggedBinaryFile.Write(Palette);
        Result &= TaggedBinaryFile.CloseWrite();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    // TaggedBinaryFile : uses Palette from File:
    public Boolean LoadGreyscaleFromTaggedBinaryFile(String fileentry)
    {
      try
      {
        CTaggedBinaryFile TaggedBinaryFile = new CTaggedBinaryFile();
        Boolean Result = TaggedBinaryFile.OpenWrite(fileentry);
        UInt16[,] MatrixBitmap = null;
        // ??? Byte[,] MatrixPalette = null;
        Result &= TaggedBinaryFile.Read(out MatrixBitmap);
        // ??? Result &= TaggedBinaryFile.Read(out MatrixPalette);
        Result &= TaggedBinaryFile.CloseRead();
        FBitmapGreyscale = CreateBitmapGreyscaleFromMatrixQuads(MatrixBitmap);
        if (FBitmapGreyscale is Bitmap)
        {
          if (INIT_PIXELFORMAT_1K == FBitmapGreyscale.PixelFormat)
          {
            // ??? FPalette1K = CIPPalette1K.CreatePalette1KFromMatrix(MatrixPalette);
            BuildBitmapColored();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromTaggedBinaryFile(String fileentry)//,
    // ??? EPalette1K palette)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapColored = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapColored is Bitmap)
        {
          if (INIT_PIXELFORMAT_1K == FBitmapColored.PixelFormat)
          {
            // ???  BuildPalette1K(palette);
            BuildBitmapGreyscale();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - ExcelFile
    //--------------------------------------------------------
    //
    public Boolean SaveGreyscaleToExcelFile(String fileentry)
    {
      try
      {
        String FileName = Path.GetFileNameWithoutExtension(fileentry);
        String FileExtension = Path.GetExtension(fileentry);
        String FileEntryMatrix = FileName + "Matrix" + FileExtension;
        // ??? String FileEntryPalette = FileName + "Palette" + FileExtension;
        CExcelFile ExcelFile = new CExcelFile();
        UInt16[,] Matrix = CreateMatrixQuadFromBitmapGreyscale(FBitmapGreyscale);
        Boolean Result = ExcelFile.WriteMatrixUInt16(FileEntryMatrix, "Matrix", Matrix);
        // ??? CPalette1KBase Palette1KGreyscale = CIPPalette1K.CreatePalette(EPalette1K.Greyscale);
        // ??? Byte[,] Palette = CIPBitmap1K.GetMatrixFromPalette(Palette1KGreyscale);
        // ??? Result &= ExcelFile.WriteMatrixByte(FileEntryPalette, "Palette", Palette);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToExcelFile(String fileentry)
    {
      try
      {
        String FileName = Path.GetFileNameWithoutExtension(fileentry);
        String FileExtension = Path.GetExtension(fileentry);
        String FileEntryMatrix = FileName + "Matrix" + FileExtension;
        // ??? String FileEntryPalette = FileName + "Palette" + FileExtension;
        CExcelFile ExcelFile = new CExcelFile();
        UInt16[,] Matrix = CreateMatrixFromBitmapColored(FBitmapColored);
        Boolean Result = ExcelFile.WriteMatrixUInt16(FileEntryMatrix, "Matrix", Matrix);
        // ??? Byte[,] Palette = CIPBitmap1K.GetMatrixFromPalette(FPalette1K);
        // ??? Result &= ExcelFile.WriteMatrixByte(FileEntryPalette, "Palette", Palette);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadGreyscaleFromExcelFile(String fileentry)// ??? , EPalette1K palette)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromExcelFile(String fileentry)// ??? , EPalette1K palette)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - ExcelSheet
    //--------------------------------------------------------
    //
    public Boolean ExportGreyscaleToExcelSheet(String sheetname)
    {
      try
      {
        String SheetMatrix = sheetname + "Matrix";
        // ??? String SheetPalette = sheetname + "Palette";
        CExcelSheet ExcelSheet = new CExcelSheet();
        UInt16[,] Matrix = CreateMatrixQuadFromBitmapGreyscale(FBitmapGreyscale);
        Boolean Result = ExcelSheet.ExportMatrixUInt16(SheetMatrix, Matrix);
        // ??? CPalette1KBase Palette1KGreyscale = CIPPalette1K.CreatePalette(EPalette1K.Greyscale);
        // ??? Byte[,] Palette = CIPBitmap1K.GetMatrixFromPalette(Palette1KGreyscale);
        // ??? Result &= ExcelSheet.ExportMatrixByte(SheetPalette, Palette);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean ExportColoredToExcelSheet(String sheetname)
    {
      try
      {
        String SheetMatrix = sheetname + "Matrix";
        // ??? String SheetPalette = sheetname + "Palette";
        CExcelSheet ExcelSheet = new CExcelSheet();
        UInt16[,] Matrix = CreateMatrixFromBitmapColored(FBitmapColored);
        Boolean Result = ExcelSheet.ExportMatrixUInt16(SheetMatrix, Matrix);
        // ??? Byte[,] Palette = CIPBitmap1K.GetMatrixFromPalette(FPalette1K);
        // ??? Result &= ExcelSheet.ExportMatrixByte(SheetPalette, Palette);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean ImportGreyscaleFromExcelSheet(String sheetname)// ??? , EPalette1K palette)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean ImportColoredFromExcelSheet(String sheetname)// ??? , EPalette1K palette)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //
    //#################################################################################################
    //  Section - Static Bitmap1KPalette - BitmapGreyscale (Palette) - Order BGRA
    //#################################################################################################
    //
    public static Byte[,] CreateMatrixFromPalette(CPalette1KBase palette)
    {
      const Int32 PALETTEWIDTH = 3;
      const Int32 INDEXBLUE = 2;
      const Int32 INDEXGREEN = 1;
      const Int32 INDEXRED = 0;
      try
      {
        Int32 PaletteHeight = CIPPalette1K.COLORRANGE_1K;
        Byte[,] Result = new Byte[PaletteHeight, PALETTEWIDTH];
        unsafe
        {
          for (Int32 RI = 0; RI < PaletteHeight; RI++)
          { // Blue-Value
            Result[RI, INDEXBLUE] = palette.GetPartsBlue((UInt16)RI);
            // Green-Value
            Result[RI, INDEXGREEN] = palette.GetPartsGreen((UInt16)RI);
            // Red-Value
            Result[RI, INDEXRED] = palette.GetPartsRed((UInt16)RI);
          }
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    
    public static Bitmap CreateBitmapGreyscaleFromMatrix(UInt16[,] matrix)
    { // 1408121851
      try
      {
        Int32 BitmapHeight = matrix.GetLength(0);
        Int32 BitmapWidth = matrix.GetLength(1);
        Bitmap BitmapGreyscale = new Bitmap(BitmapWidth, BitmapHeight, INIT_PIXELFORMAT_1K);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapGreyscale.LockBits(RTarget, ImageLockMode.WriteOnly, BitmapGreyscale.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        {
          Byte BL, BH;
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Target
              UInt16 WValue = (UInt16)(matrix[RI, CI] << 3); // Matrix-Correction
              BL = (Byte)((0x00FF & WValue) >> 0);
              BH = (Byte)((0xFF00 & WValue) >> 8);
              // Blue-Value
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Green-Value
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Red-Value
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Alpha-Value
              BL = 0xFF;//!!!!!!!!!!!!!!!!! (Byte)((0x00FF & 0x1FFF) >> 0);
              BH = 0x1F;//!!!!!!!!!!!!!!!!!(Byte)((0xFF00 & 0x1FFF) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
            }
          }
        }
        BitmapGreyscale.UnlockBits(BDTarget);
        return BitmapGreyscale;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    
    public static Bitmap CreateBitmapGreyscaleFromMatrixQuads(UInt16[,] matrix)
    { // 1412161417 - OK
      const Int32 SIZE_QUAD = 4;
      const Int32 SHIFT_PALETTE = 3;
      //
      try
      {
        Int32 BitmapHeight = matrix.GetLength(0);
        Int32 BitmapWidth = matrix.GetLength(1) / SIZE_QUAD;
        Bitmap BitmapGreyscale = new Bitmap(BitmapWidth, BitmapHeight, INIT_PIXELFORMAT_1K);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapGreyscale.LockBits(RTarget, ImageLockMode.WriteOnly, 
                                                       BitmapGreyscale.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        {
          Byte AL, AH, RL, RH, GL, GH, BL, BH;
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            Int32 MCI = 0;
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Source: Matrix ARGB 
              // Matrix A
              UInt16 WValue = (UInt16)(matrix[RI, MCI] << SHIFT_PALETTE);
              MCI++;
              AL = (Byte)((0x00FF & WValue) >> 0);
              AH = (Byte)((0xFF00 & WValue) >> 8);
              // Matrix R
              WValue = (UInt16)(matrix[RI, MCI] << SHIFT_PALETTE);
              MCI++;
              RL = (Byte)((0x00FF & WValue) >> 0);
              RH = (Byte)((0xFF00 & WValue) >> 8);
              // Matrix G
              WValue = (UInt16)(matrix[RI, MCI] << SHIFT_PALETTE);
              MCI++;
              GL = (Byte)((0x00FF & WValue) >> 0);
              GH = (Byte)((0xFF00 & WValue) >> 8);
              // Matrix B
              WValue = (UInt16)(matrix[RI, MCI] << SHIFT_PALETTE);
              MCI++;
              BL = (Byte)((0x00FF & WValue) >> 0);
              BH = (Byte)((0xFF00 & WValue) >> 8);
              //
              // Target: Bitmap 64bpp BGRA
              // Bitmap B
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Bitmap G
              *PTarget = GL; PTarget++;
              *PTarget = GH; PTarget++;
              // Bitmap R
              *PTarget = RL; PTarget++;
              *PTarget = RH; PTarget++;
              // Bitmap A
              *PTarget = AL; PTarget++;
              *PTarget = AH; PTarget++;
            }
          }
        }
        BitmapGreyscale.UnlockBits(BDTarget);
        return BitmapGreyscale;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }


    public static UInt16[,] CreateMatrixQuadFromBitmapGreyscale(Bitmap bitmap)
    {
      const Int32 SIZE_QUAD = 4;
      try
      {
        Int32 BitmapHeight = bitmap.Height;
        Int32 BitmapWidth = bitmap.Width;
        Rectangle RSource = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDSource = bitmap.LockBits(RSource, ImageLockMode.ReadOnly, bitmap.PixelFormat);
        IntPtr PointerSource = BDSource.Scan0;
        Int32 StrideSource = BDSource.Stride;
        UInt16[,] Result = new UInt16[BitmapHeight, SIZE_QUAD * BitmapWidth]; // 4: BGRA
        unsafe
        {
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PSource = (Byte*)(PointerSource + RI * StrideSource);
            Int32 CII = 0;
            Byte BL, BH, GL, GH, RL, RH, AL, AH;
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Source Bitmap 64bpp BGRA
              // Blue-Value
              BL = *(Byte*)PSource;
              PSource++;
              BH = *(Byte*)PSource;
              PSource++;
              // Green-Value
              GL = *(Byte*)PSource;
              PSource++;
              GH = *(Byte*)PSource;
              PSource++;
              // Red-Value
              RL = *(Byte*)PSource;
              PSource++;
              RH = *(Byte*)PSource;
              PSource++;
              // Alpha-Value
              AL = *(Byte*)PSource;
              PSource++;
              AH = *(Byte*)PSource;
              PSource++;
              //
              // Target Matrix UInt16[,] ARGB
              // A
              Result[RI, CII] = (UInt16)((AH << 8 | AL) >> 3);
              CII++;
              // R
              Result[RI, CII] = (UInt16)((RH << 8 | RL) >> 3);
              CII++;
              // G
              Result[RI, CII] = (UInt16)((GH << 8 | GL) >> 3);
              CII++;
              // B
              Result[RI, CII] = (UInt16)((BH << 8 | BL) >> 3);
              CII++;
            }
          }
        }
        bitmap.UnlockBits(BDSource);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    //
    //
    //#################################################################################################
    //  Section - Bitmap1KPalette - BitmapColored - Order BGRA
    //#################################################################################################
    //
    public static UInt16[,] CreateMatrixFromBitmapColored(Bitmap bitmap)
    {
      return CreateMatrixQuadFromBitmapGreyscale(bitmap);
    }

    static public Bitmap CreateBitmapColoredFromMatrix(UInt16[,] matrix, Color color)
    {
      try
      {
        Int32 BitmapHeight = matrix.GetLength(0);
        Int32 BitmapWidth = matrix.GetLength(1);
        Bitmap BitmapTarget = new Bitmap(BitmapWidth, BitmapHeight, INIT_PIXELFORMAT_1K);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapTarget.LockBits(RTarget, ImageLockMode.WriteOnly, 
                                                    BitmapTarget.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        {
          Byte BL, BH;
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Target
              UInt16 WValue = (UInt16)(matrix[RI, CI]);// << 3);
              UInt16 WRed = (UInt16)((WValue * color.R) / 255);
              UInt16 WGreen = (UInt16)((WValue * color.G) / 255);
              UInt16 WBlue = (UInt16)((WValue * color.B) / 255);
              // Blue-Value
              BL = (Byte)((0x00FF & WBlue) >> 0);
              BH = (Byte)((0xFF00 & WBlue) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Green-Value
              BL = (Byte)((0x00FF & WGreen) >> 0);
              BH = (Byte)((0xFF00 & WGreen) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Red-Value
              BL = (Byte)((0x00FF & WRed) >> 0);
              BH = (Byte)((0xFF00 & WRed) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Alpha-Value
              BL = 0xFF;//!!!!!!!!!!!!!!!!! (Byte)((0x00FF & 0x1FFF) >> 0);
              BH = 0x1F;//!!!!!!!!!!!!!!!!!(Byte)((0xFF00 & 0x1FFF) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
            }
          }
        }
        BitmapTarget.UnlockBits(BDTarget);
        return BitmapTarget;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public Method
    //--------------------------------------------------------
    //
    public Boolean SetMatrixQuadGreyscale(Int32 colleft,
                                          Int32 rowtop,
                                          UInt16[,] matrix)
    { // 1412101451 - Test!!!
      try
      {
        const Int32 SIZE_QUAD = 4;
        //
        UInt16[,] Matrix = MatrixQuadGreyscale;
        Int32 BitmapWidth = FBitmapGreyscale.Width;
        Int32 BitmapHeight = FBitmapGreyscale.Height;
        Int32 MatrixHeight = matrix.GetLength(0);
        Int32 MatrixWidth = matrix.GetLength(1);
        Int32 RowMaximum = Math.Min(rowtop + MatrixHeight, BitmapHeight);
        Int32 ColMaximum = Math.Min(colleft + MatrixWidth, SIZE_QUAD * BitmapWidth);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = FBitmapGreyscale.LockBits(RTarget, ImageLockMode.WriteOnly, 
                                                        FBitmapGreyscale.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        {
          Byte BL, BH;
          Int32 MRI, MCI;
          MRI = 0;
          for (Int32 RI = rowtop; RI < RowMaximum; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            PTarget += 4 * colleft;
            MCI = 0;
            for (Int32 CI = colleft; CI < ColMaximum; CI++)
            { // Target
              UInt16 WValue = matrix[MRI, MCI];
              MCI++;
              BL = (Byte)((0x00FF & WValue) >> 0);
              BH = (Byte)((0xFF00 & WValue) >> 8);
              // Blue-Value
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Green-Value
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Red-Value
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Alpha-Value
              BL = 0xFF;
              BH = 0x1F;
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
            }
            MRI++;
          }
        }
        FBitmapGreyscale.UnlockBits(BDTarget);
        if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
        {
          FOnBitmapGreyscaleChanged();
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetMatrixQuadColored(Int32 colleft,
                                        Int32 rowtop,
                                        UInt16[,] matrix)
    {
      try
      {
        UInt16[,] Matrix = MatrixQuadGreyscale;
        Int32 BitmapWidth = FBitmapColored.Width;
        Int32 BitmapHeight = FBitmapColored.Height;
        Int32 MatrixHeight = matrix.GetLength(0);
        Int32 MatrixWidth = matrix.GetLength(1);// not here!! / 4; 
        Int32 RowMaximum = Math.Min(rowtop + MatrixHeight, BitmapHeight);
        Int32 ColMaximum = Math.Min(colleft + MatrixWidth, BitmapWidth);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = FBitmapColored.LockBits(RTarget, ImageLockMode.WriteOnly,
                                                      FBitmapColored.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        {
          //!!!!!!!!!!!!!!!!!Byte BL, BH;
          Int32 MRI, MCI;
          MRI = 0;
          for (Int32 RI = rowtop; RI < RowMaximum; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            PTarget += 4 * colleft;
            MCI = 0;
            for (Int32 CI = colleft; CI < ColMaximum; CI++)
            { // Target
              UInt16 ColorIndex = (UInt16)(matrix[MRI, MCI] >> CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              MCI++;
              /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/ Blue-Value
              UInt16 CP = (UInt16)(FPalette.GetPartsBlue(ColorIndex) << CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              BL = (Byte)((0x00FF & CP) >> 0);
              BH = (Byte)((0xFF00 & CP) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Green-Value
              CP = (UInt16)(FPalette.GetPartsGreen(ColorIndex) << CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              BL = (Byte)((0x00FF & CP) >> 0);
              BH = (Byte)((0xFF00 & CP) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Red-Value
              CP = (UInt16)(FPalette.GetPartsRed(ColorIndex) << CIPPalette1K.COLORRANGE_SHIFTPIXEL);
              BL = (Byte)((0x00FF & CP) >> 0);
              BH = (Byte)((0xFF00 & CP) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Alpha-Value
              BL = 0xFF;
              BH = 0x1F;
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
               */
            }
            MRI++;
          }
        }
        FBitmapColored.UnlockBits(BDTarget);
        if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
        {
          FOnBitmapColoredChanged();
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }






    public Boolean GetMatrixQuadGreyscale(Int32 colleft, Int32 rowtop,
                                          Int32 colwidth, Int32 rowheight,
                                          out UInt16[,] matrix)
    { // 1412101431 - Test!!!
      try
      {
        const Int32 SIZE_QUAD = 4;
        //
        Int32 BitmapWidth = FBitmapGreyscale.Width;
        Int32 BitmapHeight = FBitmapGreyscale.Height;
        // Matrix Quad ARGB
        Int32 MatrixHeight = Math.Min(rowheight, BitmapHeight - rowtop);
        Int32 MatrixWidth = SIZE_QUAD * Math.Min(colwidth, BitmapWidth - colleft);
        //
        matrix = new UInt16[MatrixHeight, MatrixWidth]; 
        Rectangle RSource = new Rectangle(colleft, rowtop, MatrixWidth, MatrixHeight);
        BitmapData BDSource = FBitmapGreyscale.LockBits(RSource, ImageLockMode.ReadOnly,
                                                        FBitmapGreyscale.PixelFormat);
        IntPtr PointerTarget = BDSource.Scan0;
        Int32 StrideTarget = BDSource.Stride;
        unsafe
        {
          Byte AL, AH, RL, RH, GL, GH, BL, BH;
          Int16 MRI = 0;
          for (Int32 RI = rowtop; RI < rowtop + MatrixHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            PTarget += SIZE_QUAD * colleft;
            Int16 MCI = 0;
            Int32 CL = SIZE_QUAD * colleft;
            Int32 CH = CL + MatrixWidth;
            for (Int32 CI = CL; CI < CH; CI += SIZE_QUAD)
            { // Source Bitmap 64bpp BGRA
              // B
              BL = *PTarget; PTarget++;
              BH = *PTarget; PTarget++;
              // G
              GL = *PTarget; PTarget++;
              GH = *PTarget; PTarget++;
              // R
              RL = *PTarget; PTarget++;
              RH = *PTarget; PTarget++;
              // A
              AL = *PTarget; PTarget++;
              AH = *PTarget; PTarget++;
              //
              // -> Target Matrix Quad ARGB
              matrix[MRI, MCI] = (UInt16)(AH << 8 + AL);
              MCI++;
              matrix[MRI, MCI] = (UInt16)(RH << 8 + RL);
              MCI++;
              matrix[MRI, MCI] = (UInt16)(GH << 8 + GL);
              MCI++;
              matrix[MRI, MCI] = (UInt16)(BH << 8 + BL);
              MCI++;
            }
            MRI++;
          }
        }
        FBitmapGreyscale.UnlockBits(BDSource);
        return true;
      }
      catch (Exception e)
      {
        matrix = null;
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean GetMatrixQuadColored(Int32 colleft, Int32 rowtop,
                                        Int32 colwidth, Int32 rowheight,
                                        out UInt16[,] matrix)
    { // 1412101431 - Test!!!
      try
      {
        const Int32 SIZE_QUAD = 4;
        //
        Int32 BitmapWidth = FBitmapColored.Width;
        Int32 BitmapHeight = FBitmapColored.Height;
        Int32 MatrixHeight = Math.Min(rowheight, BitmapHeight - rowtop);
        Int32 MatrixWidth = SIZE_QUAD * Math.Min(colwidth, BitmapWidth - colleft);
        //
        matrix = new UInt16[MatrixHeight, MatrixWidth]; 
        Rectangle RSource = new Rectangle(colleft, rowtop, MatrixWidth, MatrixHeight);
        BitmapData BDSource = FBitmapColored.LockBits(RSource, ImageLockMode.WriteOnly,
                                                      FBitmapColored.PixelFormat);
        IntPtr PointerTarget = BDSource.Scan0;
        Int32 StrideTarget = BDSource.Stride;
        unsafe
        {
          Byte AL, AH, RL, RH, GL, GH, BL, BH;
          Int16 MRI = 0;
          for (Int32 RI = rowtop; RI < rowtop + MatrixHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            PTarget += SIZE_QUAD * colleft;
            Int16 MCI = 0;
            Int32 CL = SIZE_QUAD * colleft;
            Int32 CH = CL + MatrixWidth;
            for (Int32 CI = CL; CI < CH; CI += SIZE_QUAD)
            { // Source Bitmap 64bpp BGRA
              // B
              BL = *PTarget; PTarget++;
              BH = *PTarget; PTarget++;
              // G
              GL = *PTarget; PTarget++;
              GH = *PTarget; PTarget++;
              // R
              RL = *PTarget; PTarget++;
              RH = *PTarget; PTarget++;
              // A
              AL = *PTarget; PTarget++;
              AH = *PTarget; PTarget++;
              //
              // -> Target Matrix Quad ARGB
              matrix[MRI, MCI] = (UInt16)(AH << 8 + AL);
              MCI++;
              matrix[MRI, MCI] = (UInt16)(RH << 8 + RL);
              MCI++;
              matrix[MRI, MCI] = (UInt16)(GH << 8 + GL);
              MCI++;
              matrix[MRI, MCI] = (UInt16)(BH << 8 + BL);
              MCI++;
            }
            MRI++;
          }
        }
        FBitmapGreyscale.UnlockBits(BDSource);
        return true;
      }
      catch (Exception e)
      {
        matrix = null;
        Console.WriteLine(e.Message);
        return false;
      }
    }






    public Boolean GetMatrixIndexGreyscale(Int32 colleft, Int32 rowtop,
                                           Int32 colwidth, Int32 rowheight,
                                           out UInt16[,] matrix)
    { // 1412111114 - Test!!!
      try
      {
        const Int32 SIZE_INDEX = 1;
        const Int32 SIZE_QUAD = 4;
        //
        Int32 BitmapWidth = FBitmapGreyscale.Width;
        Int32 BitmapHeight = FBitmapGreyscale.Height;
        Int32 MatrixHeight = Math.Min(rowheight, BitmapHeight - rowtop);
        Int32 MatrixWidth = SIZE_INDEX * Math.Min(colwidth, BitmapWidth - colleft);
        //
        matrix = new UInt16[MatrixHeight, MatrixWidth];     
        Rectangle RSource = new Rectangle(colleft, rowtop, MatrixWidth, MatrixHeight);
        BitmapData BDSource = FBitmapGreyscale.LockBits(RSource, ImageLockMode.WriteOnly,
                                                        FBitmapGreyscale.PixelFormat);
        IntPtr PointerTarget = BDSource.Scan0;
        Int32 StrideTarget = BDSource.Stride;
        unsafe
        {
          Byte BL, BH;
          Int16 MRI = 0;
          for (Int32 RI = rowtop; RI < rowtop + MatrixHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            PTarget += SIZE_QUAD * colleft;
            Int16 MCI = 0;
            Int32 CL = SIZE_QUAD * colleft;
            for (Int32 CI = CL; CI < colleft + MatrixWidth; CI++)
            { // Index -> Palette -> Color
              BL = *PTarget; PTarget++;
              BH = *PTarget; PTarget++;
              matrix[MRI, MCI] = (UInt16)(BH << 8 + BL << 0);
              MCI++;
            }
            MRI++;
          }
        }
        FBitmapGreyscale.UnlockBits(BDSource);
        return true;
      }
      catch (Exception e)
      {
        matrix = null;
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean GetMatrixIndexColored(Int32 colleft, Int32 rowtop,
                                         Int32 colwidth, Int32 rowheight,
                                         out UInt16[,] matrix)
    { // 1412101427 - test!!!
      try
      {
        Int32 BitmapWidth = FBitmapColored.Width;
        Int32 BitmapHeight = FBitmapColored.Height;
        Int32 MatrixHeight = Math.Min(rowheight, BitmapHeight - rowtop);
        Int32 MatrixWidth = Math.Min(colwidth, BitmapWidth - colleft);
        matrix = new UInt16[MatrixHeight, MatrixWidth]; // S(B) + S(G) + S(R) +S(A)        
        Rectangle RSource = new Rectangle(colleft, rowtop, MatrixWidth, MatrixHeight);
        BitmapData BDSource = FBitmapColored.LockBits(RSource, ImageLockMode.WriteOnly,
                                                      FBitmapColored.PixelFormat);
        IntPtr PointerTarget = BDSource.Scan0;
        Int32 StrideTarget = BDSource.Stride;
        unsafe
        {
          Byte BL, BH;
          Int16 MRI = 0;
          for (Int32 RI = rowtop; RI < rowtop + MatrixHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            PTarget += 4 * colleft;
            Int16 MCI = 0;
            for (Int32 CI = colleft; CI < colleft + MatrixWidth; CI++)
            { // Index -> Palette -> Color
              BL = *PTarget; PTarget++;
              BH = *PTarget; PTarget++;
              matrix[MRI, MCI] = (UInt16)(BH << 8 + BL << 0);
              MCI++;
            }
            MRI++;
          }
        }
        FBitmapColored.UnlockBits(BDSource);
        return true;
      }
      catch (Exception e)
      {
        matrix = null;
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public Method
    //--------------------------------------------------------
    //


  }
}
