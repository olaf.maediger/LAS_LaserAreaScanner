﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using IPMatrix;
using IPPalette;
//
namespace IPBitmap
{
  public abstract class CBitmapPaletteBase : CBitmapBase
  {

    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    protected Bitmap FBitmapGreyscale;  // Dominant
    protected Byte[] FDataGreyscale;
    protected GCHandle FHandleDataGreyscale;
    protected DOnBitmapGreyscaleChanged FOnBitmapGreyscaleChanged;
    //
    protected Bitmap FBitmapColored;    // Dependent from Greyscale
    protected Byte[] FDataColored;
    protected GCHandle FHandleDataColored;
    protected DOnBitmapColoredChanged FOnBitmapColoredChanged;
    // 
    protected CPaletteBase FPalette;
    //
    //--------------------------------------------------------
    //	Segment - Property - Common
    //--------------------------------------------------------
    //
    public override Int32 GetWidth()
    {
      return FBitmapGreyscale.Width;
    }
    public override Int32 GetHeight()
    {
      return FBitmapGreyscale.Height;
    }
    //
    //--------------------------------------------------------
    //	Segment - Property - Bitmap Greyscale
    //--------------------------------------------------------
    //   
    public Bitmap BitmapGreyscale
    {
      get { return FBitmapGreyscale; }
    }
    public Byte[] DataGreyscale
    {
      get { return FDataGreyscale; }
    }
    protected IntPtr PointerDataGreyscale
    {
      get { return FHandleDataGreyscale.AddrOfPinnedObject(); }
    }

    /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public Byte[,] MatrixGreyscale
    {
      get { return CreateMatrixFromBitmapGreyscale(FBitmapGreyscale); }
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!set { CreateBitmapGreyscaleFromMatrix(value); }
    }*/

    public void SetOnBitmapGreyscaleChanged(DOnBitmapGreyscaleChanged value)
    {
      FOnBitmapGreyscaleChanged = value;
      if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
      {
        FOnBitmapGreyscaleChanged();
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Property - Bitmap Colored
    //--------------------------------------------------------
    //
    public Bitmap BitmapColored
    {
      get { return FBitmapColored; }
    }
    public Byte[] DataColored
    {
      get { return FDataColored; }
    }
    protected IntPtr PointerDataColored
    {
      get { return FHandleDataColored.AddrOfPinnedObject(); }
    }
    /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    public Byte[,] MatrixColored
    {
      get { return CreateMatrixFromBitmapColored(FBitmapColored); }
      //???????????????!!!!!!!!!!!!!!!      set { CreateBitmapColoredFromMatrix(value, Color.White); }
    }*/

    public void SetOnBitmapColoredChanged(DOnBitmapColoredChanged value)
    {
      FOnBitmapColoredChanged = value;
      if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
      {
        FOnBitmapColoredChanged();
      }
    }


  }
}
