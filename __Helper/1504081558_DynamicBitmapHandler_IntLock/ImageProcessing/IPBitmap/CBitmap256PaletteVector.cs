﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPVector;
//
namespace IPBitmap
{
  public partial class CBitmap256Palette : CBitmapPaletteBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Vector Row - Quad
    //--------------------------------------------------------------------------
    //
    private Int32 CalculateDestinationIndex(Int32 row, Int32 offset)
    {
      return row * FBitmapGreyscale.Width * SIZE_BYTEQUAD + offset * SIZE_BYTEQUAD;
    }
    private Int32 CalculateByteCount(Int32 destinationindex, Int32 vectorsize)
    {
      return Math.Min(vectorsize,
                      FBitmapGreyscale.Height * FBitmapGreyscale.Width * SIZE_BYTEQUAD - destinationindex);
    }

    public Boolean SetVectorQuadRow(Int32 row, Int32 offset, Byte[] vector, Int32 shiftright)
    {
      try
      {
        Int32 DI = CalculateDestinationIndex(row, offset);
        Int32 BC = CalculateByteCount(DI, vector.GetLength(0));
        for (Int32 BI = 0; BI < BC; BI++)
        {
          Byte BValue = (Byte)(((Int32)vector[BI]) >> shiftright);
          FDataGreyscale[DI] = BValue;
          DI++;
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorQuadRow(Int32 row, Int32 offset, UInt16[] vector, Int32 shiftright)
    {
      try
      {
        Int32 DI = CalculateDestinationIndex(row, offset);
        Int32 BC = CalculateByteCount(DI, vector.GetLength(0));
        for (Int32 BI = 0; BI < BC; BI++)
        {
          Byte BValue = (Byte)(((Int32)vector[BI]) >> shiftright);
          FDataGreyscale[DI] = BValue;
          DI++;
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorQuadRow(Int32 row, Int32 offset, Int16[] vector, Int32 shiftright)
    {
      try
      {
        Int32 DI = CalculateDestinationIndex(row, offset);
        Int32 BC = CalculateByteCount(DI, vector.GetLength(0));
        for (Int32 BI = 0; BI < BC; BI++)
        {
          Byte BValue = (Byte)(((Int32)vector[BI]) >> shiftright);
          FDataGreyscale[DI] = BValue;
          DI++;
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorQuadRow(Int32 row, Int32 offset, UInt32[] vector, Int32 shiftright)
    {
      try
      {
        Int32 DI = CalculateDestinationIndex(row, offset);
        Int32 BC = CalculateByteCount(DI, vector.GetLength(0));
        for (Int32 BI = 0; BI < BC; BI++)
        {
          Byte BValue = (Byte)(((Int32)vector[BI]) >> shiftright);
          FDataGreyscale[DI] = BValue;
          DI++;
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorQuadRow(Int32 row, Int32 offset, Int32[] vector, Int32 shiftright)
    {
      try
      {
        Int32 DI = CalculateDestinationIndex(row, offset);
        Int32 BC = CalculateByteCount(DI, vector.GetLength(0));
        for (Int32 BI = 0; BI < BC; BI++)
        {
          Byte BValue = (Byte)(((Int32)vector[BI]) >> shiftright);
          FDataGreyscale[DI] = BValue;
          DI++;
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorQuadRow(Int32 row, Int32 offset, Double[] vector, Int32 shiftright)
    {
      try
      {
        Int32 DI = CalculateDestinationIndex(row, offset);
        Int32 BC = CalculateByteCount(DI, vector.GetLength(0));
        for (Int32 BI = 0; BI < BC; BI++)
        {
          Byte BValue = (Byte)(((Int32)vector[BI]) >> shiftright);
          FDataGreyscale[DI] = BValue;
          DI++;
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorQuadRow(Int32 row, Int32 offset, CVector256 vector, Int32 shiftright)
    {
      try
      {
        Int32 DI = CalculateDestinationIndex(row, offset);
        Int32 BC = CalculateByteCount(DI, vector.BaseCount);
        for (Int32 BI = 0; BI < BC; BI++)
        {
          Byte BValue = (Byte)(((Int32)vector[BI]) >> shiftright);
          FDataGreyscale[DI] = BValue;
          DI++;
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorQuadRow(Int32 row, Int32 offset, CVector1K vector, Int32 shiftright)
    {
      try
      {
        Int32 DI = CalculateDestinationIndex(row, offset);
        Int32 BC = CalculateByteCount(DI, vector.BaseCount);
        for (Int32 BI = 0; BI < BC; BI++)
        {
          Byte BValue = (Byte)(((Int32)vector[BI]) >> shiftright);
          FDataGreyscale[DI] = BValue;
          DI++;
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorQuadRow(Int32 row, Int32 offset, CVector4K vector, Int32 shiftright)
    {
      try
      {
        Int32 DI = CalculateDestinationIndex(row, offset);
        Int32 BC = CalculateByteCount(DI, vector.BaseCount);
        for (Int32 BI = 0; BI < BC; BI++)
        {
          Byte BValue = (Byte)(((Int32)vector[BI]) >> shiftright);
          FDataGreyscale[DI] = BValue;
          DI++;
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Vector Row - Index -> Quad
    //--------------------------------------------------------------------------
    //
    public Boolean SetVectorIndexRow(Int32 row, Int32 offset, Byte[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadRow(row, offset, VectorQuad, shiftright);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexRow(Int32 row, Int32 offset, UInt16[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadRow(row, offset, VectorQuad, shiftright);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexRow(Int32 row, Int32 offset, Int16[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadRow(row, offset, VectorQuad, shiftright);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexRow(Int32 row, Int32 offset, UInt32[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadRow(row, offset, VectorQuad, shiftright);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexRow(Int32 row, Int32 offset, Int32[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadRow(row, offset, VectorQuad, shiftright);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexRow(Int32 row, Int32 offset, Double[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadRow(row, offset, VectorQuad, shiftright);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexRow(Int32 row, Int32 offset, CVector256 vector)
    {
      try
      {
        Byte[] VectorQuad = vector.CloneDataVectorQuad256(vector);
        SetVectorQuadRow(row, offset, VectorQuad, CVectorBase.SHIFT_256_256);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SetVectorIndexRow(Int32 row, Int32 offset, CVector1K vector)
    {
      try
      {
        Byte[] VectorQuad = vector.CloneDataVectorQuad256(vector);
        SetVectorQuadRow(row, offset, VectorQuad, CVectorBase.SHIFT_1K_256);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SetVectorIndexRow(Int32 row, Int32 offset, CVector4K vector)
    {
      try
      {
        Byte[] VectorQuad = vector.CloneDataVectorQuad256(vector);
        SetVectorQuadRow(row, offset, VectorQuad, CVectorBase.SHIFT_4K_256);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Vector Col - Quad
    //--------------------------------------------------------------------------
    //
    public Boolean SetVectorQuadCol(Int32 col, Byte[] vector, Int32 shiftright)
    {
      try
      {
        Int32 BW = SIZE_BYTEQUAD * FBitmapGreyscale.Width;
        Int32 BC = vector.GetLength(0);
        Int32 DI = SIZE_BYTEQUAD * col;
        Int32 QuadCount = 1;
        for (Int32 BI = 0; BI < BC; BI++) // all vector elements
        {
          Byte BValue = (Byte)(((Int32)vector[BI]) >> shiftright);
          FDataGreyscale[DI] = BValue;
          if (QuadCount < SIZE_BYTEQUAD)
          {
            DI++;
            QuadCount++;
          }
          else
          {
            DI += 1 + BW - SIZE_BYTEQUAD;
            QuadCount = 1;
          }
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Vector Col - Index -> Quad
    //--------------------------------------------------------------------------
    //
    public Boolean SetVectorIndexCol(Int32 col, Byte[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadCol(col, VectorQuad, shiftright);
        BuildBitmapColoredFromGreyscale();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexCol(Int32 col, UInt16[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadCol(col, VectorQuad, shiftright);
        BuildBitmapColoredFromGreyscale();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexCol(Int32 col, Int16[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadCol(col, VectorQuad, shiftright);
        BuildBitmapColoredFromGreyscale();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexCol(Int32 col, UInt32[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadCol(col, VectorQuad, shiftright);
        BuildBitmapColoredFromGreyscale();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexCol(Int32 col, Int32[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadCol(col, VectorQuad, shiftright);
        BuildBitmapColoredFromGreyscale();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexCol(Int32 col, float[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadCol(col, VectorQuad, shiftright);
        BuildBitmapColoredFromGreyscale();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexCol(Int32 col, Double[] vector, Int32 shiftright)
    {
      try
      {
        Byte[] VectorQuad = CVectorBase.CloneDataVectorQuad256(vector);
        SetVectorQuadCol(col, VectorQuad, shiftright);
        BuildBitmapColoredFromGreyscale();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexCol(Int32 col, CVector256 vector)
    {
      try
      {
        Byte[] VectorQuad = vector.CloneDataVectorQuad256(vector);
        SetVectorQuadCol(col, VectorQuad, CVectorBase.SHIFT_256_256);
        BuildBitmapColoredFromGreyscale();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexCol(Int32 col, CVector1K vector)
    {
      try
      {
        Byte[] VectorQuad = vector.CloneDataVectorQuad256(vector);
        SetVectorQuadCol(col, VectorQuad, CVectorBase.SHIFT_1K_256);
        BuildBitmapColoredFromGreyscale();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetVectorIndexCol(Int32 col, CVector4K vector)
    {
      try
      {
        Byte[] VectorQuad = vector.CloneDataVectorQuad256(vector);
        SetVectorQuadCol(col, VectorQuad, CVectorBase.SHIFT_4K_256);
        BuildBitmapColoredFromGreyscale();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------------
    //	Segment - Function - SetMatrix256 
    //--------------------------------------------------------------------------------
    //




































  }
}
