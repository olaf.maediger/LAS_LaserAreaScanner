﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
//
using IPBitmap;
//
namespace IPBitmap
{
  //-----------------------------------------------------------
  //  DIB
  //-----------------------------------------------------------
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct RGBQUAD
  {
    public Byte rgbBlue;
    public Byte rgbGreen;
    public Byte rgbRed;
    public Byte rgbAlpha;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 2)]
  public struct BITMAPINFOHEADER
  {
    public UInt32 biSize;
    public UInt32 biWidth;
    public UInt32 biHeight;
    public UInt16 biPlanes;
    public UInt16 biBitCount;
    public UInt32 biCompression;
    public UInt32 biSizeImage;
    public UInt32 biXPelsPerMeter;
    public UInt32 biYPelsPerMeter;
    public UInt32 biClrUsed;
    public UInt32 biClrImportant;
  };

  [StructLayout(LayoutKind.Sequential, Pack = 2)]
  public struct BITMAPINFO
  {
    public BITMAPINFOHEADER bmiHeader;
    public RGBQUAD[] bmiColors;
  };
  //-----------------------------------------------------------
  // BMF
  //-----------------------------------------------------------
  [StructLayout(LayoutKind.Sequential, Pack = 2)]
  public struct BITMAPFILEHEADER
  {
    public UInt16 bfType;
    public UInt32 bfSize;
    public UInt16 bfReserved1;
    public UInt16 bfReserved2;
    public UInt32 bfOffsetBits;
  }; // Size = 14

  public class CBitmap64bppBmpFile
  {
    //
    //--------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------
    //
    static private UInt16 BuildUInt16(Char characterhigh, Char characterlow)
    {
      UInt16 Result = 0x0000;
      Result |= (UInt16)(0x00FF & (characterhigh << 0));
      Result |= (UInt16)(0xFF00 & (characterlow << 8));
      return Result;
    }

    static private Byte[] BuildBytes(UInt16 value)
    {
      Byte[] Result = new Byte[2];
      Result[0] = (Byte)((0x00FF & value) >> 0);
      Result[1] = (Byte)((0xFF00 & value) >> 8);
      return Result;
    }
    static private Byte[] BuildBytes(UInt32 value)
    {
      Byte[] Result = new Byte[4];
      Result[0] = (Byte)((0x000000FF & value) >> 0);
      Result[1] = (Byte)((0x0000FF00 & value) >> 8);
      Result[2] = (Byte)((0x00FF0000 & value) >> 16);
      Result[3] = (Byte)((0xFF000000 & value) >> 24);
      return Result;
    }

    static private Char[] BuildCharacters(Byte[] buffer)
    {
      Char[] Result = new Char[buffer.Length];
      for (Int32 BI = 0; BI < buffer.Length; BI++)
      {
        Result[BI] = (Char)buffer[BI];
      }
      return Result;
    }

    static private UInt16 BuildUInt16(Byte[] buffer)
    {
      UInt16 Result = (UInt16)((buffer[1] << 8) | (buffer[0] << 0));
      return Result;
    }

    static private UInt32 BuildUInt32(Byte[] buffer)
    {
      UInt32 Result = (UInt32)((buffer[3] << 24) | (buffer[2] << 16) | (buffer[1] << 8) | (buffer[0] << 0));
      return Result;
    }


    //
    //--------------------------------------------------------------
    //  Segment - 
    //--------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------
    //  Segment - Save
    //--------------------------------------------------------------
    //
    static public Boolean SaveBitmapToBmpFile(String filename, Bitmap bitmap)
    {
      try
      {
        Int32 BW = bitmap.Width;
        Int32 BH = bitmap.Height;
        //
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = bitmap.LockBits(R, ImageLockMode.ReadOnly, bitmap.PixelFormat);
        //
        BITMAPFILEHEADER BFH = new BITMAPFILEHEADER();
        BITMAPINFOHEADER BIH = new BITMAPINFOHEADER();
        //
        BFH.bfType = BuildUInt16('B', 'M');
        BFH.bfSize = (UInt32)(Marshal.SizeOf(BFH) + Marshal.SizeOf(BIH) + BD.Stride * BH);
        BFH.bfReserved1 = 0x0000;
        BFH.bfReserved2 = 0x0000;
        BFH.bfOffsetBits = (UInt32)(Marshal.SizeOf(BFH) + Marshal.SizeOf(BIH));
        //
        BIH.biSize = (UInt32)Marshal.SizeOf(BIH);
        BIH.biWidth = (UInt32)BW;
        BIH.biHeight = (UInt32)BH;
        BIH.biPlanes = 0x0001;
        BIH.biBitCount = 0x0040; // 64bpp TrueColor
        BIH.biCompression = 0x00000000;
        BIH.biSizeImage = 0x00000000;
        BIH.biXPelsPerMeter = 0x00000000;
        BIH.biYPelsPerMeter = 0x00000000;
        BIH.biClrUsed = 0x00000000;
        BIH.biClrImportant = 0x00000000;
        //
        FileStream FS = new FileStream(filename, FileMode.Create, FileAccess.Write);
        // BFH
        Byte[] BV = BuildBytes(BFH.bfType);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BFH.bfSize);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BFH.bfReserved1);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BFH.bfReserved2);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BFH.bfOffsetBits);
        FS.Write(BV, 0, BV.Length);
        // BIH
        BV = BuildBytes(BIH.biSize);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BIH.biWidth);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BIH.biHeight);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BIH.biPlanes);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BIH.biBitCount);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BIH.biCompression);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BIH.biSizeImage);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BIH.biXPelsPerMeter);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BIH.biYPelsPerMeter);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BIH.biClrUsed);
        FS.Write(BV, 0, BV.Length);
        BV = BuildBytes(BIH.biClrImportant);
        FS.Write(BV, 0, BV.Length);
        //
        unsafe
        {
          for (Int32 RI = 0; RI < BH; RI++)
          {
            UInt16* PColor = (UInt16*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = 0; CI < BW; CI++)
            { // Blue
              UInt16 WValue = (UInt16)(*(UInt16*)PColor);
              BV = BuildBytes(WValue);
              FS.Write(BV, 0, BV.Length);
              PColor++;
              // Green
              WValue = (UInt16)(*(UInt16*)PColor);
              BV = BuildBytes(WValue);
              FS.Write(BV, 0, BV.Length);
              PColor++;
              // Red
              WValue = (UInt16)(*(UInt16*)PColor);
              BV = BuildBytes(WValue);
              FS.Write(BV, 0, BV.Length);
              PColor++;
              // Alpha
              WValue = (UInt16)(*(UInt16*)PColor);
              BV = BuildBytes(WValue);
              FS.Write(BV, 0, BV.Length);
              PColor++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        FS.Close();
        //
        return true;
        //
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------------
    //  Segment - Load
    //--------------------------------------------------------------
    //
    static public Bitmap LoadBitmapFromBmpFile(String filename)
    {
      Bitmap Result = null;
      try
      {
        //
        FileStream FS = new FileStream(filename, FileMode.Open, FileAccess.Read);
        //
        Byte[] BV = new Byte[2];
        FS.Read(BV, 0, BV.Length);
        Char[] Characters = BuildCharacters(BV);
        //
        BITMAPFILEHEADER BFH = new BITMAPFILEHEADER();
        //
        BV = new Byte[4];
        FS.Read(BV, 0, BV.Length);
        BFH.bfSize = BuildUInt32(BV);
        //        
        BV = new Byte[2];
        FS.Read(BV, 0, BV.Length);
        BFH.bfReserved1 = BuildUInt16(BV);
        //        
        FS.Read(BV, 0, BV.Length);
        BFH.bfReserved1 = BuildUInt16(BV);
        //
        BV = new Byte[4];
        FS.Read(BV, 0, BV.Length);
        BFH.bfOffsetBits = BuildUInt32(BV);
        //
        BITMAPINFOHEADER BIH = new BITMAPINFOHEADER();
        //
        BV = new Byte[4];
        FS.Read(BV, 0, BV.Length);
        BIH.biSize = BuildUInt32(BV);
        //
        FS.Read(BV, 0, BV.Length);
        BIH.biWidth = BuildUInt32(BV);
        //
        FS.Read(BV, 0, BV.Length);
        BIH.biHeight = BuildUInt32(BV);
        //
        BV = new Byte[2];
        FS.Read(BV, 0, BV.Length);
        BIH.biPlanes = BuildUInt16(BV);
        //
        FS.Read(BV, 0, BV.Length);
        BIH.biBitCount = BuildUInt16(BV);
        //
        BV = new Byte[4];
        FS.Read(BV, 0, BV.Length);
        BIH.biCompression = BuildUInt32(BV);
        //
        BV = new Byte[4];
        FS.Read(BV, 0, BV.Length);
        BIH.biSizeImage = BuildUInt32(BV);
        //
        BV = new Byte[4];
        FS.Read(BV, 0, BV.Length);
        BIH.biXPelsPerMeter = BuildUInt32(BV);
        //
        BV = new Byte[4];
        FS.Read(BV, 0, BV.Length);
        BIH.biYPelsPerMeter = BuildUInt32(BV);
        //
        BV = new Byte[4];
        FS.Read(BV, 0, BV.Length);
        BIH.biClrUsed = BuildUInt32(BV);
        //
        BV = new Byte[4];
        FS.Read(BV, 0, BV.Length);
        BIH.biClrImportant = BuildUInt32(BV);
        //
        // Pixel...
        PixelFormat PF = CBitmapBase.INIT_PIXELFORMAT_1K;
        switch (BIH.biBitCount)
        {
          case 0x0020:
            PF = CBitmapBase.INIT_PIXELFORMAT_256;
            break;
          case 0x0040:
            PF = CBitmapBase.INIT_PIXELFORMAT_1K;
            break;
        }
        Result = new Bitmap((Int32)BIH.biWidth, (Int32)BIH.biHeight, PF);
        Rectangle R = new Rectangle(0, 0, (Int32)BIH.biWidth, (Int32)BIH.biHeight);
        BitmapData BD = Result.LockBits(R, ImageLockMode.WriteOnly, Result.PixelFormat);
        unsafe
        {
          for (Int32 RI = 0; RI < BIH.biHeight; RI++)
          {
            UInt16* PColor = (UInt16*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = 0; CI < BIH.biWidth; CI++)
            { // Blue
              BV = new Byte[2];
              FS.Read(BV, 0, BV.Length);
              UInt16 ColorBlue = BuildUInt16(BV);
              (*(UInt16*)PColor) = ColorBlue;
              PColor++;
              // Green
              BV = new Byte[2];
              FS.Read(BV, 0, BV.Length);
              UInt16 ColorGreen = BuildUInt16(BV);
              (*(UInt16*)PColor) = ColorGreen;
              PColor++;
              // Red
              BV = new Byte[2];
              FS.Read(BV, 0, BV.Length);
              UInt16 ColorRed = BuildUInt16(BV);
              (*(UInt16*)PColor) = ColorRed;
              PColor++;
              // Alpha
              BV = new Byte[2];
              FS.Read(BV, 0, BV.Length);
              UInt16 ColorAlpha = BuildUInt16(BV);
              (*(UInt16*)PColor) = ColorAlpha;
              PColor++;
            }
          }
        }
        Result.UnlockBits(BD);
        FS.Close();
        //
        return Result;
        //
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }


  }
}
