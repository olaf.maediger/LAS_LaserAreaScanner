﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
using IPPalette;
//
namespace IPBitmap
{
  public class CBitmap1KTruecolor : CBitmapTruecolorBase
  {
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //
    protected const Int32 INIT_BITSPERPIXEL = 64;
    protected const PixelFormat INIT_PIXELFORMAT = PixelFormat.Format64bppArgb;
    //
    public const Int32 INTENSITY_MINIMUM = 0;
    public const Int32 INTENSITY_MAXIMUM = 1023;
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmap1KTruecolor(Bitmap bitmapsource)
    {
      FBitmapTruecolor = (Bitmap)bitmapsource.Clone();
    }

    public CBitmap1KTruecolor(Int32 width, Int32 height)
    {
      Int32 BW = Math.Max(1, width);
      Int32 BH = Math.Max(1, height);
      FBitmapTruecolor = new Bitmap(BW, BH, INIT_PIXELFORMAT);
    }
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //
    public Bitmap GetBitmapTruecolor()
    {
      return FBitmapTruecolor;
    }

    public void SetOnBitmapTruecolorChanged(DOnBitmapTruecolorChanged value)
    {
      FOnBitmapTruecolorChanged = value;
      if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
      {
        FOnBitmapTruecolorChanged();
      }
    }

    public Bitmap BitmapTruecolor
    {
      get { return FBitmapTruecolor; }
    }

    public UInt16[,] MatrixTruecolor
    {
      get { return CreateMatrixFromBitmapTruecolor(FBitmapTruecolor); }
      set { CreateBitmapTruecolorFromMatrix(value); }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - BmpFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToBmpFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        Boolean Result = Bitmap1KTransfer.SaveBitmap1K(fileentry, FBitmapTruecolor, true, ImageFormat.Bmp);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromBmpFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapTruecolor = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapTruecolor is Bitmap)
        {
          if (INIT_PIXELFORMAT == FBitmapTruecolor.PixelFormat)
          {
            if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
            {
              FOnBitmapTruecolorChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - TifFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToTifFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        Boolean Result = Bitmap1KTransfer.SaveBitmap1K(fileentry, FBitmapTruecolor, true, ImageFormat.Tiff);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromTifFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapTruecolor = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapTruecolor is Bitmap)
        {
          if (INIT_PIXELFORMAT == FBitmapTruecolor.PixelFormat)
          {
            if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
            {
              FOnBitmapTruecolorChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - PngFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToPngFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        Boolean Result = Bitmap1KTransfer.SaveBitmap1K(fileentry, FBitmapTruecolor, true, ImageFormat.Png);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromPngFile(String fileentry)
    {
      try
      {
        CBitmap1KTransfer Bitmap1KTransfer = new CBitmap1KTransfer();
        FBitmapTruecolor = Bitmap1KTransfer.LoadBitmap1K(fileentry);
        if (FBitmapTruecolor is Bitmap)
        {
          if (INIT_PIXELFORMAT == FBitmapTruecolor.PixelFormat)
          {
            if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
            {
              FOnBitmapTruecolorChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - TextFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToTextFile(String fileentry)
    {
      try
      {
        UInt16[,] Matrix = MatrixTruecolor;
        CTextFile TextFile = new CTextFile();
        Boolean Result = TextFile.OpenWrite(fileentry);
        Result &= TextFile.Write(Matrix);
        Result &= TextFile.Close();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromTextFile(String fileentry)
    {
      try
      {
        CTextFile TextFile = new CTextFile();
        Boolean Result = TextFile.OpenRead(fileentry);
        UInt16[,] Matrix;
        Result &= TextFile.Read(out Matrix);
        Result &= TextFile.Close();
        FBitmapTruecolor = CreateBitmapTruecolorFromMatrix(Matrix);
        if (FBitmapTruecolor is Bitmap)
        {
          if (INIT_PIXELFORMAT == FBitmapTruecolor.PixelFormat)
          {
            if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
            {
              FOnBitmapTruecolorChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - TaggedBinaryFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToTaggedBinaryFile(String fileentry)
    {
      try
      {
        CTaggedBinaryFile TaggedBinaryFile = new CTaggedBinaryFile();
        UInt16[,] Matrix = MatrixTruecolor;
        Boolean Result = TaggedBinaryFile.OpenWrite(fileentry);
        Result &= TaggedBinaryFile.Write(Matrix);
        // ??? Byte[,] Palette = CIPBitmap1K.GetMatrixFromPalette(FPalette1K);
        // ??? Result &= TaggedBinaryFile.Write(Palette);
        Result &= TaggedBinaryFile.CloseWrite();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromTaggedBinaryFile(String fileentry)
    {
      try
      {
        CTaggedBinaryFile TaggedBinaryFile = new CTaggedBinaryFile();
        Boolean Result = TaggedBinaryFile.OpenRead(fileentry);
        UInt16[,] Matrix;
        Result &= TaggedBinaryFile.Read(out Matrix);
        Result &= TaggedBinaryFile.CloseRead();
        FBitmapTruecolor = CreateBitmapTruecolorFromMatrix(Matrix);
        if (FBitmapTruecolor is Bitmap)
        {
          if (INIT_PIXELFORMAT == FBitmapTruecolor.PixelFormat)
          {
            if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
            {
              FOnBitmapTruecolorChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - ExcelFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToExcelFile(String fileentry)
    {
      try
      {
        String FileName = Path.GetFileNameWithoutExtension(fileentry);
        String FileExtension = Path.GetExtension(fileentry);
        String FileEntryMatrix = FileName + "Matrix" + FileExtension;
        // ??? String FileEntryPalette = FileName + "Palette" + FileExtension;
        CExcelFile ExcelFile = new CExcelFile();
        UInt16[,] Matrix = MatrixTruecolor;
        Boolean Result = ExcelFile.WriteMatrixUInt16(FileEntryMatrix, "Matrix", Matrix);
        // ??? Byte[,] Palette = CIPBitmap1K.GetMatrixFromPalette(FPalette1K);
        // ??? Result &= ExcelFile.WriteMatrixByte(FileEntryPalette, "Palette", Palette);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromExcelFile(String fileentry)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - ExcelSheet
    //--------------------------------------------------------
    //
    public Boolean ExportTruecolorToExcelSheet(String sheetname)
    {
      try
      {
        String SheetMatrix = sheetname + "Matrix";
        // ??? String SheetPalette = sheetname + "Palette";
        CExcelSheet ExcelSheet = new CExcelSheet();
        UInt16[,] Matrix = MatrixTruecolor;
        Boolean Result = ExcelSheet.ExportMatrixUInt16(SheetMatrix, Matrix);
        // ??? Byte[,] Palette = CIPBitmap1K.GetMatrixFromPalette(FPalette1K);
        // ??? Result &= ExcelSheet.ExportMatrixByte(SheetPalette, Palette);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean ImportTruecolorFromExcelSheet(String sheetname)// ??? , EPalette1K palette)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Static Method
    //--------------------------------------------------------
    //
    static public UInt16[,] CreateMatrixFromBitmapTruecolor(Bitmap bitmap)
    {
      const Int32 SIZE_COLOR = 4;
      try
      {
        Int32 BitmapWidth = bitmap.Width;
        Int32 BitmapHeight = bitmap.Height;
        Rectangle RSource = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDSource = bitmap.LockBits(RSource, ImageLockMode.ReadOnly, bitmap.PixelFormat);
        IntPtr PointerSource = BDSource.Scan0;
        Int32 StrideSource = BDSource.Stride;
        UInt16[,] Result = new UInt16[BitmapHeight, SIZE_COLOR * BitmapWidth]; // 4: BGRA
        unsafe
        {
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PSource = (Byte*)(PointerSource + RI * StrideSource);
            Int32 CII = 0;
            Byte BBL, BBH, BRL, BRH, BGL, BGH, BAL, BAH;
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Source BitmapTruecolor BGRA
              // Blue-Value
              BBL = *(Byte*)PSource;
              PSource++;
              BBH = *(Byte*)PSource;
              PSource++;
              // Green-Value
              BGL = *(Byte*)PSource;
              PSource++;
              BGH = *(Byte*)PSource;
              PSource++;
              // Red-Value
              BRL = *(Byte*)PSource;
              PSource++;
              BRH = *(Byte*)PSource;
              PSource++;
              // Alpha-Value
              BAL = *(Byte*)PSource;
              PSource++;
              BAH = *(Byte*)PSource;
              PSource++;
              // Target Matrix ARGB
              Result[RI, CII] = (UInt16)((BAH << 8 + BAL) >> 3);
              CII++;
              Result[RI, CII] = (UInt16)((BRH << 8 + BRL) >> 3);
              CII++;
              Result[RI, CII] = (UInt16)((BGH << 8 + BGL) >> 3);
              CII++;
              Result[RI, CII] = (UInt16)((BBH << 8 + BBL) >> 3);
              CII++;
            }
          }
        }
        bitmap.UnlockBits(BDSource);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }

    static public Bitmap CreateBitmapTruecolorFromMatrix(UInt16[,] matrix)
    {
      try
      {
        Int32 BitmapHeight = matrix.GetLength(0);
        Int32 BitmapWidth = matrix.GetLength(1) / 4; // ( S(R) + S(G) + S(B) +S(A)
        Bitmap BitmapTarget = new Bitmap(BitmapWidth, BitmapHeight, INIT_PIXELFORMAT);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapTarget.LockBits(RTarget, ImageLockMode.WriteOnly, BitmapTarget.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        {
          Byte BL, BH;
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            Int16 MCI = 0;
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Target              
              UInt16 WBlue = (UInt16)matrix[RI, MCI];
              MCI++;
              UInt16 WGreen = (UInt16)matrix[RI, MCI];
              MCI++;
              UInt16 WRed = (UInt16)matrix[RI, MCI];
              MCI++;
              UInt16 WAlpha = (UInt16)matrix[RI, MCI];
              MCI++;
              // Blue-Value
              BL = (Byte)((0x00FF & WBlue) >> 0);
              BH = (Byte)((0xFF00 & WBlue) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Green-Value
              BL = (Byte)((0x00FF & WGreen) >> 0);
              BH = (Byte)((0xFF00 & WGreen) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Red-Value
              BL = (Byte)((0x00FF & WRed) >> 0);
              BH = (Byte)((0xFF00 & WRed) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Alpha-Value
              BL = (Byte)((0x00FF & 0x1FFF) >> 0);
              BH = (Byte)((0xFF00 & 0x1FFF) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
            }
          }
        }
        BitmapTarget.UnlockBits(BDTarget);
        return BitmapTarget;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public Method
    //--------------------------------------------------------
    //
    public Boolean SetMatrixTruecolor(Int32 rowtop,
                                      Int32 colleft,
                                      UInt16[,] matrix)
    {
      try
      {
        UInt16[,] Matrix = MatrixTruecolor;
        Int32 BitmapWidth = FBitmapTruecolor.Width;
        Int32 BitmapHeight = FBitmapTruecolor.Height;
        Int32 MatrixHeight = matrix.GetLength(0);
        Int32 MatrixWidth = matrix.GetLength(1) / 4; // ( S(R) + S(G) + S(B) +S(A)
        Int32 RowMaximum = Math.Min(rowtop + MatrixHeight, BitmapHeight);
        Int32 ColMaximum = Math.Min(colleft + MatrixWidth, BitmapWidth);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapTruecolor.LockBits(RTarget, ImageLockMode.WriteOnly, 
                                                       BitmapTruecolor.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        {
          Byte BL, BH;
          Int16 MRI = 0;
          for (Int32 RI = rowtop; RI < RowMaximum; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            PTarget += 4 * colleft;
            Int16 MCI = 0;
            for (Int32 CI = colleft; CI < ColMaximum; CI++)
            { // Target              
              UInt16 WBlue = (UInt16)matrix[MRI, MCI];
              MCI++;
              UInt16 WGreen = (UInt16)matrix[MRI, MCI];
              MCI++;
              UInt16 WRed = (UInt16)matrix[MRI, MCI];
              MCI++;
              UInt16 WAlpha = (UInt16)matrix[MRI, MCI];
              MCI++;             
              // Blue-Value
              BL = (Byte)((0x00FF & WBlue) >> 0);
              BH = (Byte)((0xFF00 & WBlue) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Green-Value
              BL = (Byte)((0x00FF & WGreen) >> 0);
              BH = (Byte)((0xFF00 & WGreen) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Red-Value
              BL = (Byte)((0x00FF & WRed) >> 0);
              BH = (Byte)((0xFF00 & WRed) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
              // Alpha-Value
              BL = (Byte)((0x00FF & 0x1FFF) >> 0);
              BH = (Byte)((0xFF00 & 0x1FFF) >> 8);
              *PTarget = BL; PTarget++;
              *PTarget = BH; PTarget++;
            }
            MRI++;
          }
        }
        BitmapTruecolor.UnlockBits(BDTarget);
        if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
        {
          FOnBitmapTruecolorChanged();
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean GetMatrixTruecolor(Int32 colleft, Int32 rowtop,
                                      Int32 colwidth, Int32 rowheight,
                                      out UInt16[,] matrix)
    {
      try
      {
        Int32 BitmapWidth = FBitmapTruecolor.Width;
        Int32 BitmapHeight = FBitmapTruecolor.Height;
        Int32 MatrixHeight = Math.Min(rowheight, BitmapHeight - rowtop);
        Int32 MatrixWidth = Math.Min(colwidth, BitmapWidth - colleft);
        matrix = new UInt16[MatrixHeight, 4 * MatrixWidth]; // S(B) + S(G) + S(R) +S(A)        
        Rectangle RSource = new Rectangle(colleft, rowtop, MatrixWidth, MatrixHeight);
        BitmapData BDSource = BitmapTruecolor.LockBits(RSource, ImageLockMode.WriteOnly, 
                                                       BitmapTruecolor.PixelFormat);
        IntPtr PointerTarget = BDSource.Scan0;
        Int32 StrideTarget = BDSource.Stride;
        unsafe
        {
          Byte BL, BH;
          Int16 MRI = 0;
          for (Int32 RI = rowtop; RI < rowtop + MatrixHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            PTarget += 4 * colleft;
            Int16 MCI = 0;
            for (Int32 CI = colleft; CI < colleft + MatrixWidth; CI++)
            { // Blue      
              BL = *PTarget; PTarget++;
              BH = *PTarget; PTarget++;
              matrix[MRI, MCI] = (UInt16)(BH << 8 + BL << 0);
              MCI++;
              // Green      
              BL = *PTarget; PTarget++;
              BH = *PTarget; PTarget++;
              matrix[MRI, MCI] = (UInt16)(BH << 8 + BL << 0);
              MCI++;
              // Red
              BL = *PTarget; PTarget++;
              BH = *PTarget; PTarget++;
              matrix[MRI, MCI] = (UInt16)(BH << 8 + BL << 0);
              MCI++;
              // Alpha
              BL = *PTarget; PTarget++;
              BH = *PTarget; PTarget++;
              matrix[MRI, MCI] = (UInt16)(BH << 8 + BL << 0);
              MCI++;
            }
            MRI++;
          }
        }
        BitmapTruecolor.UnlockBits(BDSource);
        return true;
      }
      catch (Exception e)
      {
        matrix = null;
        Console.WriteLine(e.Message);
        return false;
      }
    }


  }
}
