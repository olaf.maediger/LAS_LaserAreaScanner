﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
//
using IPMatrix;
using IPPalette;
//
namespace IPBitmap
{
  public partial class CBitmap4KPalette : CBitmapPaletteBase
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //
    protected const Int32 INIT_BITSPERPIXEL = 64;
    protected const PixelFormat INIT_PIXELFORMAT = PixelFormat.Format64bppArgb;
    //
    private const Int32 INIT_ROWCOUNT = 240;
    private const Int32 INIT_COLCOUNT = 320;
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    /*/
    public CBitmap4KPalette(Bitmap bitmapgreyscale,
                            EPalette4K palette)
    {
      Int32 BW = Math.Max(1, bitmapgreyscale.Width);
      Int32 BH = Math.Max(1, bitmapgreyscale.Height);
      FBitmapGreyscale = (Bitmap)bitmapgreyscale.Clone();
      FPalette = CIPPalette256.CreatePalette(palette);
      FBitmapColored = BuildBitmapColoredFromGreyscale(FBitmapGreyscale, (CPalette256Base)FPalette);
    }

    public CBitmap4KPalette(Bitmap bitmapgreyscale,
                            CPalette256Base palette)
    {
      Int32 BW = Math.Max(1, bitmapgreyscale.Width);
      Int32 BH = Math.Max(1, bitmapgreyscale.Height);
      FBitmapGreyscale = (Bitmap)bitmapgreyscale.Clone();
      FPalette = palette;
      FBitmapColored = BuildBitmapColoredFromGreyscale(FBitmapGreyscale, (CPalette256Base)FPalette);
    }

    public CBitmap4KPalette(CMatrixBase matrix,
                            CPalette256Base palette)
    {
      SetMatrix(matrix);
      FPalette = palette;
      FBitmapColored = BuildBitmapColoredFromGreyscale(FBitmapGreyscale, (CPalette256Base)FPalette);
    }*/
    //
    //--------------------------------------------------------
    //	Segment - Property - Common
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //
    protected override Int32 GetBitsPerPixel()
    {
      return INIT_BITSPERPIXEL;
    }

    protected override PixelFormat GetPixelFormat()
    {
      return INIT_PIXELFORMAT;
    }


  
  
  }
}
