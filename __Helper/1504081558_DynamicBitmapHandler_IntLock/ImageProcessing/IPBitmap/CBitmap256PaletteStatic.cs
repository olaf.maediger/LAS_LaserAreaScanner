﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using IPMatrix;
using IPPalette;
//
namespace IPBitmap
{
  //
  //#################################################################################################
  //  Segment - Static (no internal Access)
  //#################################################################################################
  //
  public partial class CBitmap256Palette : CBitmapPaletteBase
  {
    //
    //#################################################################################################
    //  Section - Static - BitmapGreyscale - Matrix
    //#################################################################################################
    //
    public static Bitmap CreateBitmapGreyscaleFromMatrix(Byte[,] matrix)
    { // 1412081521
      try
      {
        Int32 BitmapHeight = matrix.GetLength(0);
        Int32 BitmapWidth = matrix.GetLength(1);
        Bitmap BitmapGreyscale = new Bitmap(BitmapWidth, BitmapHeight, INIT_PIXELFORMAT_256);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapGreyscale.LockBits(RTarget, ImageLockMode.WriteOnly, INIT_PIXELFORMAT_256);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        {
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Target
              Byte BValue = matrix[RI, CI];
              // Blue-Value
              *PTarget = BValue;
              PTarget++;
              // Green-Value
              *PTarget = BValue;
              PTarget++;
              // Red-Value
              *PTarget = BValue;
              PTarget++;
              // Alpha-Value
              *PTarget = 0xFF;
              PTarget++;
            }
          }
        }
        BitmapGreyscale.UnlockBits(BDTarget);
        return BitmapGreyscale;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }

    public static Byte[,] CreateMatrixFromBitmapGreyscale(Bitmap bitmap)
    { // 1412081521
      const Int32 SIZE_COLOR = 4;
      try
      {
        Int32 BitmapHeight = bitmap.Height;
        Int32 BitmapWidth = bitmap.Width;
        Rectangle RSource = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDSource = bitmap.LockBits(RSource, ImageLockMode.ReadOnly, INIT_PIXELFORMAT_256);
        IntPtr PointerSource = BDSource.Scan0;
        Int32 StrideSource = BDSource.Stride;
        Byte[,] Result = new Byte[BitmapHeight, SIZE_COLOR * BitmapWidth]; // 4: BGRA
        unsafe
        {
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PSource = (Byte*)(PointerSource + RI * StrideSource);
            Int32 CII = 0;
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Source -> Target
              // Blue-Value
              Result[RI, CII] = *(Byte*)PSource;
              PSource++;
              CII++;
              // Green-Value
              Result[RI, CII] = *(Byte*)PSource;
              PSource++;
              CII++;
              // Red-Value
              Result[RI, CII] = *(Byte*)PSource;
              PSource++;
              CII++;
              // Alpha-Value
              Result[RI, CII] = *(Byte*)PSource;
              PSource++;
              CII++;
            }
          }
        }
        bitmap.UnlockBits(BDSource);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    //
    //#################################################################################################
    //  Section - Static - BitmapColored - Matrix
    //#################################################################################################
    //
    public static Byte[,] CreateMatrixFromBitmapColored(Bitmap bitmap)
    { // 1412081521
      return CreateMatrixFromBitmapGreyscale(bitmap);
    }

    static public Bitmap CreateBitmapFromMatrixColored(Byte[,] matrix, Color color)
    { // 1412081521
      try
      {
        Int32 BitmapHeight = matrix.GetLength(0);
        Int32 BitmapWidth = matrix.GetLength(1);
        Bitmap BitmapTarget = new Bitmap(BitmapWidth, BitmapHeight, INIT_PIXELFORMAT_256);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapTarget.LockBits(RTarget, ImageLockMode.WriteOnly,
                                                    INIT_PIXELFORMAT_256);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        {
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Target
              Byte MC = matrix[RI, CI];
              Byte BA = (Byte)(MC * color.A / 255);
              Byte BR = (Byte)(MC * color.R / 255);
              Byte BG = (Byte)(MC * color.G / 255);
              Byte BB = (Byte)(MC * color.B / 255);
              // Blue-Value
              *PTarget = BB;
              PTarget++;
              // Green-Value
              *PTarget = BG;
              PTarget++;
              // Red-Value
              *PTarget = BR;
              PTarget++;
              // Alpha-Value
              *PTarget = BA;
              PTarget++;
            }
          }
        }
        BitmapTarget.UnlockBits(BDTarget);
        return BitmapTarget;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }




  }
}
