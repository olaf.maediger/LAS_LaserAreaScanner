﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
using IPMatrix;
using IPPalette;
//
namespace IPBitmap
{
  public partial class CBitmap256Palette : CBitmapPaletteBase
  { //
    //--------------------------------------------------------------------------------
    //	Segment - Function - Get/SetMatrix(Byte/U/Int16/32/float/Double -> BitmapGreyscale
    //--------------------------------------------------------------------------------
    //
    public Boolean SetMatrix(Byte[,] matrix, Int32 shiftright)
    {
      try
      {
        return BuildBitmapGreyscale(matrix, shiftright);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean GetMatrix(out Byte[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      {
        return BuildMatrixGreyscale(out matrix, shiftleft);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetMatrix(UInt16[,] matrix, Int32 shiftright)
    {
      try
      {
        return BuildBitmapGreyscale(matrix, shiftright);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean GetMatrix(out UInt16[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      {
        return BuildMatrixGreyscale(out matrix, shiftleft);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetMatrix(Int16[,] matrix, Int32 shiftright)
    {
      try
      {
        return BuildBitmapGreyscale(matrix, shiftright);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean GetMatrix(out Int16[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      {
        return BuildMatrixGreyscale(out matrix, shiftleft);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetMatrix(UInt32[,] matrix, Int32 shiftright)
    {
      try
      {
        return BuildBitmapGreyscale(matrix, shiftright);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean GetMatrix(out UInt32[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      {
        return BuildMatrixGreyscale(out matrix, shiftleft);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetMatrix(Int32[,] matrix, Int32 shiftright)
    {
      try
      {
        return BuildBitmapGreyscale(matrix, shiftright);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean GetMatrix(out Int32[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      {
        return BuildMatrixGreyscale(out matrix, shiftleft);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetMatrix(float[,] matrix, Int32 shiftright)
    {
      try
      {
        return BuildBitmapGreyscale(matrix, shiftright);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean GetMatrix(out float[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      {
        return BuildMatrixGreyscale(out matrix, shiftleft);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean SetMatrix(Double[,] matrix, Int32 shiftright)
    {
      try
      {
        return BuildBitmapGreyscale(matrix, shiftright);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean GetMatrix(out Double[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      {
        return BuildMatrixGreyscale(out matrix, shiftleft);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------------
    //	Segment - Function - Get/SetMatrix(CMatrix256) -> BitmapGreyscale
    //--------------------------------------------------------------------------------
    //
    public Boolean SetMatrix(CMatrix256 matrix)
    {
      try
      {
        BuildBitmapGreyscale(matrix);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean GetMatrix(out CMatrix256 matrix)
    {
      matrix = null;
      try
      {
        return BuildMatrixGreyscale(out matrix, VALUESHIFT_256);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------------
    //	Segment - Function - Get/SetMatrix(CMatrix1K) -> BitmapGreyscale
    //--------------------------------------------------------------------------------
    //
    public Boolean SetMatrix(CMatrix1K matrix)
    {
      try
      {
        return BuildBitmapGreyscale(matrix);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean GetMatrix(out CMatrix1K matrix)
    {
      matrix = null;
      try
      {
        return BuildMatrixGreyscale(out matrix, VALUESHIFT_1K);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------------
    //	Segment - Function - Get/SetMatrix(CMatrix4K) -> BitmapGreyscale
    //--------------------------------------------------------------------------------
    //
    public Boolean SetMatrix(CMatrix4K matrix)
    {
      try
      {
        return BuildBitmapGreyscale(matrix);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean GetMatrix(out CMatrix4K matrix)
    {
      matrix = null;
      try
      {
        return BuildMatrixGreyscale(out matrix, VALUESHIFT_4K);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }



  }
}
