﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
//
using IPMatrix;
using IPPalette;
//
namespace IPBitmap
{
  public partial class CBitmap256Palette : CBitmapPaletteBase
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //
    protected const Int32 INIT_BITSPERPIXEL = 32;
    protected const PixelFormat INIT_PIXELFORMAT = PixelFormat.Format32bppArgb;
    //
    private const Int32 INIT_ROWCOUNT = 240;
    private const Int32 INIT_COLCOUNT = 320;
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmap256Palette(Bitmap bitmapgreyscale,
                             EPalette256 palette)
    {
      Int32 BW = Math.Max(1, bitmapgreyscale.Width);
      Int32 BH = Math.Max(1, bitmapgreyscale.Height);
      FBitmapGreyscale = (Bitmap)new Bitmap(bitmapgreyscale);
      FPalette = CIPPalette256.CreatePalette(palette);
      BuildBitmapColoredFromGreyscale();
    }

    public CBitmap256Palette(Bitmap bitmapgreyscale,
                             CPalette256Base palette)
    {
      Int32 BW = Math.Max(1, bitmapgreyscale.Width);
      Int32 BH = Math.Max(1, bitmapgreyscale.Height);
      FBitmapGreyscale = (Bitmap)bitmapgreyscale.Clone();
      FPalette = palette;
      BuildBitmapColoredFromGreyscale();
    }

    public CBitmap256Palette(CMatrix256 matrix,
                             CPalette256Base palette)
    {
      SetMatrix(matrix);
      FPalette = palette;
      BuildBitmapColoredFromGreyscale();
    }

    public CBitmap256Palette(CMatrix1K matrix,
                             CPalette256Base palette)
    {
      SetMatrix(matrix);
      FPalette = palette;
      BuildBitmapColoredFromGreyscale();
    }
    public CBitmap256Palette(CMatrix4K matrix,
                             CPalette256Base palette)
    {
      SetMatrix(matrix);
      FPalette = palette;
      BuildBitmapColoredFromGreyscale();
    }


  }
}
