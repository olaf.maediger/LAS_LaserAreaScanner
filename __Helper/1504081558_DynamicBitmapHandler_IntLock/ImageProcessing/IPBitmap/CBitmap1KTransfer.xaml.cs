﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
//
using IPPalette;
//
namespace IPBitmap
{
  /// <summary>
  /// Interaction logic for UserControl1.xaml
  /// </summary>
  public partial class CBitmap1KTransfer
  {
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    private System.Windows.Controls.Image FImage;

    public CBitmap1KTransfer()
    {
      FImage = new System.Windows.Controls.Image();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //
    private System.Drawing.Bitmap BuildBitmapFromSource(BitmapSource bitmapsource)
    {
      try
      {
        System.Drawing.Bitmap Result = null;
        using (MemoryStream MS = new MemoryStream())
        { // from System.Media.BitmapImage to System.Drawing.Bitmap 
          BitmapEncoder BE = new BmpBitmapEncoder();
          BE.Frames.Add(BitmapFrame.Create(bitmapsource));
          BE.Save(MS);
          Result = new System.Drawing.Bitmap(MS);
          MS.Close();
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    /* but correct????
    private Bitmap TransformBitmapRgba64ToArgb64(Bitmap bitmap)
    {
      Bitmap Result = null;
      try
      {
        if (bitmap is Bitmap)
        { // Source
          Int32 RW = bitmap.Width;
          Int32 RH = bitmap.Height;
          System.Drawing.Rectangle RSource = new System.Drawing.Rectangle(0, 0, RW, RH);
          BitmapData BDSource = bitmap.LockBits(RSource, ImageLockMode.ReadOnly, bitmap.PixelFormat);
          // Target
          Result = new Bitmap(RW, RH, bitmap.PixelFormat);
          System.Drawing.Rectangle RTarget = new System.Drawing.Rectangle(0, 0, RW, RH);
          BitmapData BDTarget = Result.LockBits(RTarget, ImageLockMode.WriteOnly, Result.PixelFormat);
          unsafe
          {
            for (Int32 RI = 0; RI < RH; RI++)
            {
              Byte* PChannelSource = (Byte*)(BDSource.Scan0 + RI * BDSource.Stride);
              Byte* PChannelTarget = (Byte*)(BDTarget.Scan0 + RI * BDTarget.Stride);
              for (Int32 CI = 0; CI < RW; CI++)
              { // Blue(L/H)
                Byte BL = *(Byte*)PChannelSource;
                *(Byte*)PChannelTarget = *(Byte*)(PChannelSource + 4);
                PChannelSource++;
                PChannelTarget++;
                Byte BH = *(Byte*)PChannelSource;
                *(Byte*)PChannelTarget = *(Byte*)(PChannelSource + 4);
                PChannelSource++;
                PChannelTarget++;
                // Green(L/H)
                *(Byte*)PChannelTarget = *(Byte*)PChannelSource;
                PChannelSource++;
                PChannelTarget++;
                *(Byte*)PChannelTarget = *(Byte*)PChannelSource;
                PChannelSource++;
                PChannelTarget++;
                // Red(L/H)
                *(Byte*)PChannelTarget = BL;
                PChannelSource++;
                PChannelTarget++;
                *(Byte*)PChannelTarget = BH;
                PChannelSource++;
                PChannelTarget++;
                // Alpha(L/H)
                *(Byte*)PChannelTarget = *(Byte*)PChannelSource;
                PChannelSource++;
                PChannelTarget++;
                *(Byte*)PChannelTarget = *(Byte*)PChannelSource;
                PChannelSource++;
                PChannelTarget++;
              }
            }
          }
          bitmap.UnlockBits(BDSource);
          Result.UnlockBits(BDTarget);
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    */

    private Bitmap TransformBitmapRgba64ToArgb64(Bitmap bitmap)
    {
      Bitmap Result = null;
      try
      {
        if (bitmap is Bitmap)
        { // Source
          Int32 RW = bitmap.Width;
          Int32 RH = bitmap.Height;
          System.Drawing.Rectangle RSource = new System.Drawing.Rectangle(0, 0, RW, RH);
          BitmapData BDSource = bitmap.LockBits(RSource, ImageLockMode.ReadOnly, bitmap.PixelFormat);
          // Target
          Result = new Bitmap(RW, RH, bitmap.PixelFormat);
          System.Drawing.Rectangle RTarget = new System.Drawing.Rectangle(0, 0, RW, RH);
          BitmapData BDTarget = Result.LockBits(RTarget, ImageLockMode.WriteOnly, Result.PixelFormat);
          unsafe
          {
            for (Int32 RI = 0; RI < RH; RI++)
            {
              Byte* PChannelSource = (Byte*)(BDSource.Scan0 + RI * BDSource.Stride);
              Byte* PChannelTarget = (Byte*)(BDTarget.Scan0 + RI * BDTarget.Stride);
              for (Int32 CI = 0; CI < RW; CI++)
              { // Blue(L/H)
                //Byte BL = *(Byte*)PChannelSource;
                *(Byte*)PChannelTarget = *(Byte*)(PChannelSource);// + 4);
                PChannelSource++;
                PChannelTarget++;
                //Byte BH = *(Byte*)PChannelSource;
                *(Byte*)PChannelTarget = *(Byte*)(PChannelSource);// + 4);
                PChannelSource++;
                PChannelTarget++;
                // Green(L/H)
                *(Byte*)PChannelTarget = *(Byte*)PChannelSource;
                PChannelSource++;
                PChannelTarget++;
                *(Byte*)PChannelTarget = *(Byte*)PChannelSource;
                PChannelSource++;
                PChannelTarget++;
                // Red(L/H)
                *(Byte*)PChannelTarget = *(Byte*)PChannelSource;//BL;
                PChannelSource++;
                PChannelTarget++;
                *(Byte*)PChannelTarget = *(Byte*)PChannelSource;//BH;
                PChannelSource++;
                PChannelTarget++;
                // Alpha(L/H)
                *(Byte*)PChannelTarget = *(Byte*)PChannelSource;
                PChannelSource++;
                PChannelTarget++;
                *(Byte*)PChannelTarget = *(Byte*)PChannelSource;
                PChannelSource++;
                PChannelTarget++;
              }
            }
          }
          bitmap.UnlockBits(BDSource);
          Result.UnlockBits(BDTarget);
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
      
    // RGBA <- BGRA
    private Byte[] TransformPixelBufferFromArgb64ToRgba64(Byte[] pixelbuffer)
    { // A -> R | R -> G | G -> B | B -> A
      Byte[] Result = null;
      try
      {
        Int32 L = pixelbuffer.Length;
        Result = new Byte[L];
        Int32 State = 0;
        for (Int32 BI = 0; BI < L; BI++)
        {
          switch (State)
          {
            case 0: // RL
              Result[BI] = pixelbuffer[BI + 4];
              State = 1;
              break;
            case 1: // RH
              Result[BI] = pixelbuffer[BI + 4];
              State = 2;
              break;
            case 2: // GL
              Result[BI] = pixelbuffer[BI + 0];
              State = 3;
              break;
            case 3: // GH
              Result[BI] = pixelbuffer[BI + 0];
              State = 4;
              break;
            case 4: // BL
              Result[BI] = pixelbuffer[BI - 4];
              State = 5;
              break;
            case 5: // BH
              Result[BI] = pixelbuffer[BI - 4];
              State = 6;
              break;
            case 6: // AL
              Result[BI] = pixelbuffer[BI - 0];
              State = 7;
              break;
            case 7: // AH
              Result[BI] = pixelbuffer[BI - 0];
              State = 0;
              break;
          }
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }

    private Byte[] GetPixelBufferGreyscale(Bitmap bitmap)
    {
      try
      {
        Int32 BW = bitmap.Width;
        Int32 BH = bitmap.Height;
        int Stride = 8 * BW + (BW % 4);
        Byte[] Result = new Byte[BH * Stride];
        System.Drawing.Rectangle R = new System.Drawing.Rectangle(0, 0, BW, BH);
        BitmapData BDGreyscale = bitmap.LockBits(R, ImageLockMode.ReadOnly, bitmap.PixelFormat);
        unsafe
        {
          Int32 BI = 0;
          for (Int32 RI = 0; RI < BH; RI++)
          {
            UInt16* PChannel = (UInt16*)(BDGreyscale.Scan0 + RI * BDGreyscale.Stride);
            for (Int32 CI = 0; CI < BW; CI++)
            { // Blue
              UInt16 WBlue = (UInt16)(*(UInt16*)PChannel << CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              Result[BI] = (Byte)((0x00FF & WBlue) >> 0);
              BI++;
              Result[BI] = (Byte)((0xFF00 & WBlue) >> 8);
              BI++;
              PChannel++;
              // Green
              UInt16 WGreen = (UInt16)(*(UInt16*)PChannel << CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              Result[BI] = (Byte)((0x00FF & WGreen) >> 0);
              BI++;
              Result[BI] = (Byte)((0xFF00 & WGreen) >> 8);
              BI++;
              PChannel++;
              // Red
              UInt16 WRed = (UInt16)(*(UInt16*)PChannel << CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              Result[BI] = (Byte)((0x00FF & WRed) >> 0);
              BI++;
              Result[BI] = (Byte)((0xFF00 & WRed) >> 8);
              BI++;
              PChannel++;
              // Alpha
              UInt16 WAlpha = (UInt16)(*(UInt16*)PChannel << CIPPalette1K.COLORRANGE_SHIFTPALETTE);
              Result[BI] = (Byte)((0x00FF & WAlpha) >> 0);
              BI++;
              Result[BI] = (Byte)((0xFF00 & WAlpha) >> 8);
              BI++;
              PChannel++;
            }
          }
        }
        bitmap.UnlockBits(BDGreyscale);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }

    private Byte[] GetPixelBufferColored(Bitmap bitmap)
    {
      try
      {
        Int32 BW = bitmap.Width;
        Int32 BH = bitmap.Height;
        int Stride = 8 * BW + (BW % 4);
        Byte[] Result = new Byte[BH * Stride];
        System.Drawing.Rectangle R = new System.Drawing.Rectangle(0, 0, BW, BH);
        BitmapData BDColored = bitmap.LockBits(R, ImageLockMode.ReadOnly, bitmap.PixelFormat);
        unsafe
        {
          Int32 BI = 0;
          for (Int32 RI = 0; RI < BH; RI++)
          {
            Byte BufferLow = 0;
            Byte BufferHigh = 0;
            Byte* PValues = (Byte*)(BDColored.Scan0 + RI * BDColored.Stride);
            for (Int32 CI = 0; CI < BW; CI++)
            { // Blue
              BufferLow = *(Byte*)PValues; PValues++;
              BufferHigh = *(Byte*)PValues; PValues++;
              Result[BI] = BufferLow; BI++;
              Result[BI] = BufferHigh; BI++;
              // Green
              BufferLow = *(Byte*)PValues; PValues++;
              BufferHigh = *(Byte*)PValues; PValues++;
              Result[BI] = BufferLow; BI++;
              Result[BI] = BufferHigh; BI++;
              // Red
              BufferLow = *(Byte*)PValues; PValues++;
              BufferHigh = *(Byte*)PValues; PValues++;
              Result[BI] = BufferLow; BI++;
              Result[BI] = BufferHigh; BI++;
              // Alpha
              BufferLow = *(Byte*)PValues; PValues++;
              BufferHigh = *(Byte*)PValues; PValues++;
              Result[BI] = BufferLow; BI++;
              Result[BI] = BufferHigh; BI++;
            }
          }
        }
        bitmap.UnlockBits(BDColored);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }

    //
    //------------------------------------------------------------------------
    //  Section - Public Handler
    //------------------------------------------------------------------------
    //
    public System.Drawing.Bitmap LoadBitmap1K(String fileentry)
    {
      Bitmap Result = null;
      try
      {
        Stream ISS = new FileStream(fileentry, FileMode.Open, FileAccess.Read, FileShare.Read);
        String FileExtension = System.IO.Path.GetExtension(fileentry);
        if (".bmp" == FileExtension)
        {
          /* Problem with 16/13-Bit-Identity: BmpBitmapDecoder BD = new BmpBitmapDecoder(ISS,
                                                     BitmapCreateOptions.PreservePixelFormat,
                                                     BitmapCacheOption.Default);
          BitmapSource BS = BD.Frames[0];
          Result = BuildBitmapFromSource(BS);*/
          // Selfmade Load/Save-Functions (no Retransformation required!):
          Result = CBitmap64bppBmpFile.LoadBitmapFromBmpFile(fileentry);
        }
        else
          if (".tif" == FileExtension)
          {
            TiffBitmapDecoder BD = new TiffBitmapDecoder(ISS,
                                                         BitmapCreateOptions.PreservePixelFormat,
                                                         BitmapCacheOption.Default);
            BitmapSource BS = BD.Frames[0];
            Result = BuildBitmapFromSource(BS);
            // Red -> Blue! Error! 
            return TransformBitmapRgba64ToArgb64(Result);
            // Bitmap too small!! Error!!! return Result;
          }
          else
            if (".png" == FileExtension)
            {
              PngBitmapDecoder BD = new PngBitmapDecoder(ISS,
                                                         BitmapCreateOptions.PreservePixelFormat,
                                                         BitmapCacheOption.Default);
              BitmapSource BS = BD.Frames[0];
              Result = BuildBitmapFromSource(BS);
              return TransformBitmapRgba64ToArgb64(Result);
            }
            else
            {
            }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }   

    public Boolean SaveBitmap1K(String fileentry,
                                Bitmap bitmap,
                                Boolean greyscale,
                                ImageFormat imageformat)
    {
      try
      {
        Int32 BW = bitmap.Width;
        Int32 BH = bitmap.Height;
        int Stride = 8 * BW + (BW % 4);
        //
        String FileName = System.IO.Path.GetFileNameWithoutExtension(fileentry);
        //  Bmp
        if (ImageFormat.Bmp == imageformat)
        {
          FileName += ".bmp";
          /* Problem with 16/13-Bit-Identity:
          FileStream FS = new FileStream(FileName, FileMode.Create);
          BmpBitmapEncoder E = new BmpBitmapEncoder();
          E.Frames.Add(BitmapFrame.Create(BS));
          E.Save(FS);
          FS.Close();*/
          // Selfmade Load/Save-Functions (no Retransformation required!):
          CBitmap64bppBmpFile.SaveBitmapToBmpFile(FileName, bitmap);
          return true;
        }
        // all other standard Formats:
        Byte[] PixelBuffer;
        if (greyscale)
        {
          PixelBuffer = GetPixelBufferGreyscale(bitmap);
        }
        else
        {
          PixelBuffer = GetPixelBufferColored(bitmap);
        }
        PixelBuffer = TransformPixelBufferFromArgb64ToRgba64(PixelBuffer);
        BitmapSource BS = BitmapSource.Create(BW, BH, 300, 300, 
                                              PixelFormats.Rgba64, null,
                                              PixelBuffer, Stride);
        FImage.Source = BS;
        //
        //  Tif
        if (ImageFormat.Tiff == imageformat)
        {
          FileName += ".tif";
          using (FileStream FS = new FileStream(FileName, FileMode.Create))
          {
            TiffBitmapEncoder E = new TiffBitmapEncoder();
            E.Compression = 0;
            E.Frames.Add(BitmapFrame.Create(BS));
            E.Save(FS);
            FS.Close();
          }
          return true;
        }
        //  Png
        if (ImageFormat.Png == imageformat)
        {
          FileName += ".png";
          using (FileStream FS = new FileStream(FileName, FileMode.Create))
          {
            PngBitmapEncoder E = new PngBitmapEncoder();
            E.Interlace = PngInterlaceOption.Off;
            E.Frames.Add(BitmapFrame.Create(BS));
            E.Save(FS);
            FS.Close();
          }
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }


  }
}
