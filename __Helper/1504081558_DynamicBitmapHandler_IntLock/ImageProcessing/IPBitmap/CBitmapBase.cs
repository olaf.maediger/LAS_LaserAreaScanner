﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
//
using IPMatrix;
//
namespace IPBitmap
{
  public delegate void DOnBitmapGreyscaleChanged();
  public delegate void DOnBitmapColoredChanged();
  public delegate void DOnBitmapTruecolorChanged();

  public abstract class CBitmapBase
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //
    public const PixelFormat INIT_PIXELFORMAT_256 = PixelFormat.Format32bppArgb;
    public const PixelFormat INIT_PIXELFORMAT_1K = PixelFormat.Format64bppArgb;
    public const PixelFormat INIT_PIXELFORMAT_4K = PixelFormat.Format64bppArgb;
    //
    public const Int32 VALUESHIFT_256 = 0;
    public const Int32 VALUESHIFT_1K = 2;
    public const Int32 VALUESHIFT_4K = 4;
    //
    public const Int32 SIZE_BYTEQUAD = 4;
    public const Int32 BYTEOFFSET_BLUE = 0;
    public const Int32 BYTEOFFSET_GREEN = 1;
    public const Int32 BYTEOFFSET_RED = 2;
    public const Int32 BYTEOFFSET_ALPHA = 3;
    //
    public const Int32 SIZE_WORDQUAD = 8;
    public const Int32 WORDOFFSET_BLUE = 0;
    public const Int32 WORDOFFSET_GREEN = 1;
    public const Int32 WORDOFFSET_RED = 2;
    public const Int32 WORDOFFSET_ALPHA = 3;
    //
    public const Byte INIT_ALPHA = 0xFF;
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    private Int32 FLockCount = 0;
    //
    //--------------------------------------------------------
    //	Segment - Property - Common
    //--------------------------------------------------------
    //
    public Int32 Width
    {
      get { return GetWidth(); }
    }

    public Int32 Height
    {
      get { return GetHeight(); }
    }


    public abstract Int32 GetWidth();
    public abstract Int32 GetHeight();

    public Int32 BitsPerPixel
    {
      get { return GetBitsPerPixel(); }
    }

    public Int32 Stride(Int32 width)
    {
      return (width * BitsPerPixel + 7) / 8;
    }

    
    //
    //--------------------------------------------------------
    //	Segment - Helper - Common
    //--------------------------------------------------------
    //
    protected abstract Int32 GetBitsPerPixel();
    protected abstract PixelFormat GetPixelFormat();
    //
    protected virtual Boolean BuildBitmapColoredFromGreyscale()
    { // later abstract!!!
      return false;
    }
    // !!! later protected abstract Boolean FreeBitmapColored();
    // !!! later protected abstract Boolean BuildBitmapColored();
    // !!! later protected abstract Boolean BuildBitmapColored(Int32 width, Int32 height, Byte[] data);
    // !!! later protected abstract Boolean BuildBitmapColored(Int32 width, Int32 height, UInt16[] data);
    // !!! later protected abstract Boolean BuildBitmapColored(Int32 width, Int32 height, Int16[] data);
    // !!! later protected abstract Boolean BuildBitmapColored(Int32 width, Int32 height, UInt32[] data);
    // !!! later protected abstract Boolean BuildBitmapColored(Int32 width, Int32 height, Int32[] data);
    // !!! later protected abstract Boolean BuildBitmapColored(Int32 width, Int32 height, Double[] data);
    // !!! later protected abstract Boolean BuildBitmapColored(CMatrixBase matrix);
    //
    public void Lock()
    {
      FLockCount++;
    }
    public void Unlock()
    {
      if (0 < FLockCount)
      {
        FLockCount--;
        if (0 == FLockCount)
        {
          BuildBitmapColoredFromGreyscale();
        }
      }
    }

  }
}
