﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using IPMatrix;
using IPPalette;
//
namespace IPBitmap
{
  //
  //--------------------------------------------------------
  //	Segment - Helper - Common
  //--------------------------------------------------------
  //
  public partial class CBitmap256Palette : CBitmapPaletteBase
  { //
    //#################################################################################################
    //  Segment - Palette
    //################################################################################################
    //	Segment - Builder - Palette256
    //################################################################################################
    //
    public Boolean BuildPalette256(EPalette256 palette)
    {
      try
      {
        FPalette = CIPPalette256.CreatePalette(palette);
        return (FPalette is CPalette256Base);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //#################################################################################################
    //  Segment - Builder - Matrix (<- Palette)
    //#################################################################################################
    //
    public static Byte[,] BuildMatrixFromPalette(CPalette256Base palette)
    { // 1412081521
      const Int32 PALETTEWIDTH = 3;
      const Int32 INDEXALPHA = 0;
      const Int32 INDEXRED = 1;
      const Int32 INDEXGREEN = 2;
      const Int32 INDEXBLUE = 3;
      try
      {
        Int32 PaletteHeight = CIPPalette256.COLORRANGE_256;
        Byte[,] Result = new Byte[PaletteHeight, PALETTEWIDTH];
        unsafe
        {
          for (Int32 RI = 0; RI < PaletteHeight; RI++)
          { // Alpha
            Result[RI, INDEXALPHA] = palette.GetPartsAlpha((Byte)RI);
            // Red
            Result[RI, INDEXRED] = palette.GetPartsRed((Byte)RI);
            // Green
            Result[RI, INDEXGREEN] = palette.GetPartsGreen((Byte)RI);
            // Blue
            Result[RI, INDEXBLUE] = palette.GetPartsBlue((Byte)RI);
          }
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    //
    //
    //#################################################################################################
    //  Segment - Greyscale <-> Colored
    //#################################################################################################
    //  Segment - Builder - DataColored <-> DataGreyscale
    //#################################################################################################
    //    
    protected Boolean FreeBitmapGreyscale()
    {
      try
      {
        if (FHandleDataGreyscale.IsAllocated)
        {
          FHandleDataGreyscale.Free();
        }
        if (FBitmapGreyscale is Bitmap)
        {
          FBitmapGreyscale.Dispose();
          FBitmapGreyscale = null;
        }
        FDataGreyscale = null;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }    
    protected Byte[] BuildDataGreyscaleFromColored()
    {
      try
      {
        Lock();
        //  Free
        Int32 BW = FBitmapColored.Width;
        Int32 BH = FBitmapColored.Height;
        Int32 DataSize = BH * Stride(BW);
        Byte[] Result = new Byte[DataSize];
        for (Int32 BI = BYTEOFFSET_BLUE; BI < BYTEOFFSET_BLUE + DataSize; BI += SIZE_BYTEQUAD)
        { // Source
          Byte ColorBlue = FDataColored[BYTEOFFSET_BLUE + BI];
          Byte ColorGreen = FDataColored[BYTEOFFSET_GREEN + BI];
          Byte ColorRed = FDataColored[BYTEOFFSET_RED + BI];
          Byte ColorAlpha = FDataColored[BYTEOFFSET_ALPHA + BI];
          Byte ColorIndex = (Byte)FPalette.FindPaletteIndex(ColorRed, ColorGreen, ColorBlue);
          // Target
          Result[BI + BYTEOFFSET_BLUE] = FPalette.GetPartsBlue(ColorIndex);
          Result[BI + BYTEOFFSET_GREEN] = FPalette.GetPartsGreen(ColorIndex);
          Result[BI + BYTEOFFSET_RED] = FPalette.GetPartsRed(ColorIndex);
          Result[BI + BYTEOFFSET_ALPHA] = FDataGreyscale[BI + BYTEOFFSET_ALPHA];
        }
        Unlock();
        return Result;
      }
      catch (Exception)
      {
        return null;
      }
    }
    protected Boolean BuildBitmapGreyscaleFromColored()
    {
      try
      {
        Lock();
        //  Free
        if (FHandleDataGreyscale.IsAllocated)
        {
          FHandleDataGreyscale.Free();
        }
        if (FBitmapGreyscale is Bitmap)
        {
          FBitmapGreyscale.Dispose();
          FBitmapGreyscale = null;
        }
        //  Transform
        FDataGreyscale = BuildDataGreyscaleFromColored();
        // Build
        FHandleDataGreyscale = GCHandle.Alloc(DataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(FBitmapColored.Width, FBitmapColored.Height,
                                      Stride(FBitmapColored.Width), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //
    //#################################################################################################
    //  Segment - Greyscale
    //#################################################################################################
    //  Segment - Builder - Bitmap Greyscale <- Vector
    //#################################################################################################
    //    
    protected Boolean BuildBitmapGreyscale(Int32 width, Int32 height, Byte[] data, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 Size = height * width * sizeof(Byte);
        Byte[] DataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < height; RI++)
        {
          for (Int32 CI = 0; CI < width; CI++)
          {
            DataGreyscale[DI] = (Byte)(data[DI] >> shiftright);
            DI++;
          }
        }
        // Build
        FHandleDataGreyscale = GCHandle.Alloc(DataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(width, height, Stride(width), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(Int32 width, Int32 height, UInt16[] data, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 Size = height * width * sizeof(Byte);
        Byte[] DataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < height; RI++)
        {
          for (Int32 CI = 0; CI < width; CI++)
          {
            DataGreyscale[DI] = (Byte)(data[DI] >> shiftright);
            DI++;
          }
        }
        // Build
        FHandleDataGreyscale = GCHandle.Alloc(DataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(width, height, Stride(width), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(Int32 width, Int32 height, Int16[] data, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 Size = height * width * sizeof(Byte);
        Byte[] DataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < height; RI++)
        {
          for (Int32 CI = 0; CI < width; CI++)
          {
            DataGreyscale[DI] = (Byte)(data[DI] >> shiftright);
            DI++;
          }
        }
        // Build
        FHandleDataGreyscale = GCHandle.Alloc(DataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(width, height, Stride(width), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(Int32 width, Int32 height, UInt32[] data, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 Size = height * width * sizeof(Byte);
        Byte[] DataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < height; RI++)
        {
          for (Int32 CI = 0; CI < width; CI++)
          {
            DataGreyscale[DI] = (Byte)(data[DI] >> shiftright);
            DI++;
          }
        }
        // Build
        FHandleDataGreyscale = GCHandle.Alloc(DataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(width, height, Stride(width), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(Int32 width, Int32 height, Int32[] data, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 Size = height * width * sizeof(Byte);
        Byte[] DataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < height; RI++)
        {
          for (Int32 CI = 0; CI < width; CI++)
          {
            DataGreyscale[DI] = (Byte)(data[DI] >> shiftright);
            DI++;
          }
        }
        // Build
        FHandleDataGreyscale = GCHandle.Alloc(DataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(width, height, Stride(width), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(Int32 width, Int32 height, float[] data, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 Size = height * width * sizeof(Byte);
        Byte[] DataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < height; RI++)
        {
          for (Int32 CI = 0; CI < width; CI++)
          {
            DataGreyscale[DI] = (Byte)((Int32)data[DI] >> shiftright);
            DI++;
          }
        }
        // Build
        FHandleDataGreyscale = GCHandle.Alloc(DataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(width, height, Stride(width), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(Int32 width, Int32 height, Double[] data, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 Size = height * width * sizeof(Byte);
        Byte[] DataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < height; RI++)
        {
          for (Int32 CI = 0; CI < width; CI++)
          {
            DataGreyscale[DI] = (Byte)((Int32)data[DI] >> shiftright);
            DI++;
          }
        }
        // Build
        FHandleDataGreyscale = GCHandle.Alloc(DataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(width, height, Stride(width), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    protected Boolean BuildVectorGreyscale(out Byte[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        vector = new Byte[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (Byte)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorGreyscale(out UInt16[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        vector = new UInt16[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (UInt16)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorGreyscale(out Int16[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        vector = new Int16[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (Int16)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorGreyscale(out UInt32[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        vector = new UInt32[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (UInt32)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorGreyscale(out Int32[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        vector = new Int32[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (Int32)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorGreyscale(out float[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        vector = new float[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (float)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorGreyscale(out Double[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        vector = new Double[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (Double)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################################
    //  Segment - Builder - Bitmap Greyscale <- Matrix
    //#################################################################################################
    //    
    protected Boolean BuildBitmapGreyscale(Byte[,] matrix, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Int32 Size = RC * CC * sizeof(Byte);
        Byte[] DataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            DataGreyscale[DI] = (Byte)(matrix[RI, CI] >> shiftright);
            DI++;
          }
        }
        // Build
        FHandleDataGreyscale = GCHandle.Alloc(DataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(CC, RC, Stride(CC), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(UInt16[,] matrix, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Int32 Size = RC * CC * sizeof(Byte);
        FDataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FDataGreyscale[DI] = (Byte)(matrix[RI, CI] >> shiftright);
            DI++;
          }
        }
        //  Build
        FHandleDataGreyscale = GCHandle.Alloc(FDataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(CC, RC, Stride(CC), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(Int16[,] matrix, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Int32 Size = RC * CC * sizeof(Byte);
        FDataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FDataGreyscale[DI] = (Byte)(matrix[RI, CI] >> shiftright);
            DI++;
          }
        }
        //  Build
        FHandleDataGreyscale = GCHandle.Alloc(FDataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(CC, RC, Stride(CC), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(UInt32[,] matrix, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Int32 Size = RC * CC * sizeof(Byte);
        FDataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FDataGreyscale[DI] = (Byte)(matrix[RI, CI] >> shiftright);
            DI++;
          }
        }
        //  Build
        FHandleDataGreyscale = GCHandle.Alloc(FDataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(CC, RC, Stride(CC), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(Int32[,] matrix, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Int32 Size = RC * CC * sizeof(Byte);
        FDataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FDataGreyscale[DI] = (Byte)(matrix[RI, CI] >> shiftright);
            DI++;
          }
        }
        //  Build
        FHandleDataGreyscale = GCHandle.Alloc(FDataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(CC, RC, Stride(CC), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(float[,] matrix, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Int32 Size = RC * CC * sizeof(Byte);
        FDataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FDataGreyscale[DI] = (Byte)((Int32)matrix[RI, CI] >> shiftright);
            DI++;
          }
        }
        //  Build
        FHandleDataGreyscale = GCHandle.Alloc(FDataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(CC, RC, Stride(CC), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(Double[,] matrix, Int32 shiftright)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        Int32 Size = RC * CC * sizeof(Byte);
        FDataGreyscale = new Byte[Size];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FDataGreyscale[DI] = (Byte)((Int32)matrix[RI, CI] >> shiftright);
            DI++;
          }
        }
        //  Build
        FHandleDataGreyscale = GCHandle.Alloc(FDataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(CC, RC, Stride(CC), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscale(CMatrixBase matrix)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform (Byte/Uint16[,] -> Argb[,]
        FDataGreyscale = matrix.CloneDataVectorQuad256();//shiftright);
        FHandleDataGreyscale = GCHandle.Alloc(FDataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(matrix.ColCount, matrix.RowCount,
                                      Stride(matrix.ColCount), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return (FBitmapGreyscale is Bitmap);
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapGreyscaleArgb(CMatrixBase matrixargb)
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapGreyscale();
        //  Transform (from Byte/UInt16Argb[,])
        FDataGreyscale = matrixargb.CloneDataVector256();//shiftright);
        FHandleDataGreyscale = GCHandle.Alloc(FDataGreyscale, GCHandleType.Pinned);
        FBitmapGreyscale = new Bitmap(matrixargb.ColCount / SIZE_BYTEQUAD, matrixargb.RowCount,
                                      Stride(matrixargb.ColCount), GetPixelFormat(), PointerDataGreyscale);
        Unlock();
        return (FBitmapGreyscale is Bitmap);
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    protected Boolean BuildMatrixGreyscale(out Byte[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        matrix = new Byte[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (Byte)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixGreyscale(out UInt16[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        matrix = new UInt16[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (UInt16)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixGreyscale(out Int16[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        matrix = new Int16[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (Int16)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixGreyscale(out UInt32[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        matrix = new UInt32[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (UInt32)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixGreyscale(out Int32[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        matrix = new Int32[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (Int32)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixGreyscale(out float[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        matrix = new float[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (float)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixGreyscale(out Double[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        matrix = new Double[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (Double)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixGreyscale(out CMatrix256 matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        matrix = new CMatrix256(RC, CC);
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (Byte)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixGreyscale(out CMatrix1K matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        matrix = new CMatrix1K(RC, CC);
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (UInt16)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixGreyscale(out CMatrix4K matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapGreyscale.Width;
        Int32 RC = FBitmapGreyscale.Height;
        matrix = new CMatrix4K(RC, CC);
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (UInt16)((Int32)DataGreyscale[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //
    //#################################################################################################
    //  Segment - Colored
    //#################################################################################################
    //  Segment - Builder - Bitmap Colored
    //#################################################################################################
    //    
    protected Boolean FreeBitmapColored()
    {
      try
      {
        if (FHandleDataColored.IsAllocated)
        {
          FHandleDataColored.Free();
        }
        if (FBitmapColored is Bitmap)
        {
          FBitmapColored.Dispose();
          FBitmapColored = null;
        }
        FDataColored = null;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Byte[] BuildDataColoredFromGreyscale()
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapColored();
        // Build / Transform
        Int32 BW = FBitmapGreyscale.Width;
        Int32 BH = FBitmapGreyscale.Height;
        Int32 DataSize = BH * Stride(BW);
        Byte[] Result = new Byte[DataSize];
        for (Int32 BI = BYTEOFFSET_BLUE; BI < BYTEOFFSET_BLUE + DataSize; BI += SIZE_BYTEQUAD)
        {
          Byte ColorIndex = FDataGreyscale[BI + BYTEOFFSET_BLUE];
          Result[BI + BYTEOFFSET_BLUE] = FPalette.GetPartsBlue(ColorIndex);
          Result[BI + BYTEOFFSET_GREEN] = FPalette.GetPartsGreen(ColorIndex);
          Result[BI + BYTEOFFSET_RED] = FPalette.GetPartsRed(ColorIndex);
          Result[BI + BYTEOFFSET_ALPHA] = FDataGreyscale[BI + BYTEOFFSET_ALPHA];
        }
        Unlock();
        return Result;
      }
      catch (Exception)
      {
        return null;
      }
    }
    protected override Boolean BuildBitmapColoredFromGreyscale()
    {
      try
      {
        Lock();
        //  Free
        FreeBitmapColored();
        // Transform
        FDataColored = BuildDataColoredFromGreyscale();
        FHandleDataColored = GCHandle.Alloc(DataColored, GCHandleType.Pinned);
        FBitmapColored = new Bitmap(FBitmapGreyscale.Width, FBitmapGreyscale.Height,
                                    Stride(FBitmapGreyscale.Width), GetPixelFormat(), PointerDataColored);
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################################
    //  Segment - Builder - Bitmap Colored <- Vector
    //#################################################################################################
    /* NOT SYNCHRONIZED TO BitmapGreyscale !!! -> no sense!!!!/    
    protected Boolean BuildBitmapColored(Int32 width, Int32 height, Byte[] vector, Int32 shiftright)
    {
      try
      { // Free
        FreeBitmapColored();
        // Build / Transform
        FDataColored = vector;
        FHandleDataColored = GCHandle.Alloc(DataColored, GCHandleType.Pinned);
        FBitmapColored = new Bitmap(FBitmapGreyscale.Width, FBitmapGreyscale.Height,
                                    Stride(FBitmapGreyscale.Width), GetPixelFormat(), PointerDataColored);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapColored(Int32 width, Int32 height, UInt16[] vector, Int32 shiftright)
    {
      try
      { // Free
        FreeBitmapColored();
        // Build / Transform
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapColored(Int32 width, Int32 height, Int16[] vector, Int32 shiftright)
    {
      try
      { // Free
        FreeBitmapColored();
        // Build / Transform
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapColored(Int32 width, Int32 height, UInt32[] vector, Int32 shiftright)
    {
      try
      { // Free
        FreeBitmapColored();
        // Build / Transform
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapColored(Int32 width, Int32 height, Int32[] vector, Int32 shiftright)
    { // Free
      FreeBitmapColored();
      // Build / Transform
      try
      {
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapColored(Int32 width, Int32 height, float[] vector, Int32 shiftright)
    {
      try
      { // Free
        FreeBitmapColored();
        // Build / Transform
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildBitmapColored(Int32 width, Int32 height, Double[] vector, Int32 shiftright)
    {
      try
      { // Free
        FreeBitmapColored();
        // Build / Transform
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }*/
    //
    protected Boolean BuildVectorColored(out Byte[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        vector = new Byte[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (Byte)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorColored(out UInt16[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        vector = new UInt16[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (UInt16)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorColored(out Int16[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        vector = new Int16[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (Int16)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorColored(out UInt32[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        vector = new UInt32[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (UInt32)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorColored(out Int32[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        vector = new Int32[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (Int32)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorColored(out float[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        vector = new float[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (float)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildVectorColored(out Double[] vector, Int32 shiftleft)
    {
      vector = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        vector = new Double[RC * CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            vector[DI] = (Double)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#################################################################################################
    //  Segment - Builder - Bitmap Colored <- Matrix
    //#################################################################################################
    /* NOT SYNCHRONIZED TO BitmapGreyscale !!! -> no sense!!!!/
    protected Boolean BuildBitmapColored(CMatrixBase matrix)
    {
      try
      { // Free
        FreeBitmapColored();
        // Build / Transform
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }*/
    //
    protected Boolean BuildMatrixColored(out Byte[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        matrix = new Byte[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (Byte)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixColored(out UInt16[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        matrix = new UInt16[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (UInt16)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixColored(out Int16[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        matrix = new Int16[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (Int16)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixColored(out UInt32[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        matrix = new UInt32[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (UInt32)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixColored(out Int32[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        matrix = new Int32[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (Int32)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixColored(out float[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        matrix = new float[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (float)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixColored(out Double[,] matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        matrix = new Double[RC, CC];
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (Double)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixColored(out CMatrix256 matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        matrix = new CMatrix256(RC, CC);
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (Byte)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixColored(out CMatrix1K matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        matrix = new CMatrix1K(RC, CC);
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (UInt16)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    protected Boolean BuildMatrixColored(out CMatrix4K matrix, Int32 shiftleft)
    {
      matrix = null;
      try
      { //  Transform
        Int32 CC = FBitmapColored.Width;
        Int32 RC = FBitmapColored.Height;
        matrix = new CMatrix4K(RC, CC);
        Int32 DI = 0;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrix[RI, CI] = (UInt16)((Int32)DataColored[DI] << shiftleft);
            DI++;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //
    //#################################################################################################
    //  Segment - High Level
    //#################################################################################################
    //  Segment - Builder - High Level Fill Pattern
    //#################################################################################################
    //
    public Boolean BuildConstant(Int32 rowcount, Int32 colcount, Int32 value)
    {
      Boolean Result = false;
      try
      {
        Lock();
        CMatrix256 Matrix = new CMatrix256();
        Result = Matrix.BuildConstant(rowcount, colcount, value);
        SetMatrix(Matrix);
        Unlock();
        return Result;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public Boolean BuildLinear(Int32 rowcount, Int32 colcount, Int32 value)
    {
      try
      {
        Lock();
        Int32 RC = rowcount;
        Int32 CC = colcount;
        Byte[,] Matrix = new Byte[RC, CC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Matrix[RI, CI] = (Byte)((CI * (CIPPalette256.COLORRANGE_GDI - 1)) / (CC - 1));
          }
        }
        Unlock();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
