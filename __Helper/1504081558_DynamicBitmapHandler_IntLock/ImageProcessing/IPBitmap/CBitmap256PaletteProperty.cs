﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using IPMatrix;
using IPPalette;
//
namespace IPBitmap
{
  //
  //--------------------------------------------------------
  //	Segment - Helper - Common
  //--------------------------------------------------------
  //
  public partial class CBitmap256Palette : CBitmapPaletteBase
  {
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //
    protected override Int32 GetBitsPerPixel()
    {
      return INIT_BITSPERPIXEL;
    }

    protected override PixelFormat GetPixelFormat()
    {
      return INIT_PIXELFORMAT;
    }
    //
    //--------------------------------------------------------
    //	Segment - Property - Palette256
    //--------------------------------------------------------
    //
    private CPalette256Base GetPalette256()
    {
      return (CPalette256Base)FPalette;
    }
    public void SetPalette256(EPalette256 value)
    {
      FPalette = CIPPalette256.CreatePalette(value);
      BuildBitmapColoredFromGreyscale();
      if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
      {
        FOnBitmapColoredChanged();
      }
    }
    public void SetPalette256(CPalette256Base palette256)
    {
      FPalette = palette256;
      BuildBitmapColoredFromGreyscale();
      if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
      {
        FOnBitmapColoredChanged();
      }
    }
    public CPalette256Base Palette256
    {
      get { return GetPalette256(); }
      set { SetPalette256(value); }
    }
  }
}
