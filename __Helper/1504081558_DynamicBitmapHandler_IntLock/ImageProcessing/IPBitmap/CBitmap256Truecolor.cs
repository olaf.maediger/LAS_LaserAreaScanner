﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
using IPPalette;
//
namespace IPBitmap
{
  public class CBitmap256Truecolor : CBitmapTruecolorBase
  { 
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmap256Truecolor(Bitmap bitmapsource)
    {
      FBitmapTruecolor = (Bitmap)bitmapsource.Clone();
    }
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //
    public Bitmap GetBitmapTruecolor()
    {
      return FBitmapTruecolor;
    }

    public void SetOnBitmapTruecolorChanged(DOnBitmapTruecolorChanged value)
    {
      /*!!!!!!!!!!!!!!!!!!!!!!!
      FOnBitmapTruecolorChanged = value;
      if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
      {
        FOnBitmapTruecolorChanged();
      }*/
    }


    public Bitmap BitmapTruecolor
    {
      get { return FBitmapTruecolor; }
    }

    public Byte[,] MatrixTruecolor
    {
      get { return CreateMatrixFromBitmapTruecolor(FBitmapTruecolor); }
      set { CreateBitmapTruecolorFromMatrix(value); }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - BmpFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToBmpFile(String fileentry)
    {
      try
      {
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Boolean Result = Bitmap256Transfer.SaveBitmap256(fileentry, FBitmapTruecolor, true, ImageFormat.Bmp);
        return false;//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromBmpFile(String fileentry)
    {
      try
      {
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FBitmapTruecolor = Bitmap256Transfer.LoadBitmap256(fileentry);
        /*if (FBitmapTruecolor is Bitmap)
        {
          FWidth = FBitmapTruecolor.Width;
          FHeight = FBitmapTruecolor.Height;
          if (INIT_PIXELFORMAT_1K == FBitmapTruecolor.PixelFormat)
          {
            if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
            {
              FOnBitmapTruecolorChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - TifFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToTifFile(String fileentry)
    {
      try
      {
        /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        Boolean Result = Bitmap256Transfer.SaveBitmap256(fileentry, FBitmapTruecolor, true, ImageFormat.Tiff);
        return Result;*/

        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromTifFile(String fileentry)
    {
      try
      {
        /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        FBitmapTruecolor = Bitmap256Transfer.LoadBitmap256(fileentry);
        if (FBitmapTruecolor is Bitmap)
        {
          FWidth = FBitmapTruecolor.Width;
          FHeight = FBitmapTruecolor.Height;
          if (INIT_PIXELFORMAT_1K == FBitmapTruecolor.PixelFormat)
          {
            if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
            {
              FOnBitmapTruecolorChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - PngFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToPngFile(String fileentry)
    {
      try
      {
        /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        Boolean Result = Bitmap256Transfer.SaveBitmap256(fileentry, FBitmapTruecolor, true, ImageFormat.Png);
        return Result;*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromPngFile(String fileentry)
    {
      try
      {
        /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        FBitmapTruecolor = Bitmap256Transfer.LoadBitmap256(fileentry);
        if (FBitmapTruecolor is Bitmap)
        {
          FWidth = FBitmapTruecolor.Width;
          FHeight = FBitmapTruecolor.Height;
          if (INIT_PIXELFORMAT_1K == FBitmapTruecolor.PixelFormat)
          {
            if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
            {
              FOnBitmapTruecolorChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - TextFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToTextFile(String fileentry)
    {
      try
      {
        Byte[,] Matrix = MatrixTruecolor;
        CTextFile TextFile = new CTextFile();
        Boolean Result = TextFile.OpenWrite(fileentry);
        Result &= TextFile.Write(Matrix);
        Result &= TextFile.Close();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromTextFile(String fileentry)
    {
      try
      {
        CTextFile TextFile = new CTextFile();
        Boolean Result = TextFile.OpenRead(fileentry);
        Byte[,] Matrix;
        Result &= TextFile.Read(out Matrix);
        Result &= TextFile.Close();
        FBitmapTruecolor = CreateBitmapTruecolorFromMatrix(Matrix);
        if (FBitmapTruecolor is Bitmap)
        {
          // NC FWidth = FBitmapTruecolor.Width;
          // NC FHeight = FBitmapTruecolor.Height;
          if (PixelFormat.Format32bppArgb == FBitmapTruecolor.PixelFormat)
          {
            /*!!!!!!!!!!!!!!if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
            {
              FOnBitmapTruecolorChanged();
            }*/
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - TaggedBinaryFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToTaggedBinaryFile(String fileentry)
    {
      try
      {
        CTaggedBinaryFile TaggedBinaryFile = new CTaggedBinaryFile();
        Byte[,] Matrix = MatrixTruecolor;
        Boolean Result = TaggedBinaryFile.OpenWrite(fileentry);
        Result &= TaggedBinaryFile.Write(Matrix);
        // No Palette!!!
        Result &= TaggedBinaryFile.CloseWrite();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromTaggedBinaryFile(String fileentry)
    {
      try
      {
        CTaggedBinaryFile TaggedBinaryFile = new CTaggedBinaryFile();
        Boolean Result = TaggedBinaryFile.OpenRead(fileentry);
        Byte[,] Matrix;
        Result &= TaggedBinaryFile.Read(out Matrix);
        Result &= TaggedBinaryFile.CloseRead();
        // No Palette!!!
        FBitmapTruecolor = CreateBitmapTruecolorFromMatrix(Matrix);
        if (FBitmapTruecolor is Bitmap)
        {
          // NC FWidth = FBitmapTruecolor.Width;
          // NC FHeight = FBitmapTruecolor.Height;
          if (PixelFormat.Format32bppArgb == FBitmapTruecolor.PixelFormat)
          {
            if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
            {
              FOnBitmapTruecolorChanged();
            }
            return true;
          }
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - ExcelFile
    //--------------------------------------------------------
    //
    public Boolean SaveTruecolorToExcelFile(String fileentry)
    {
      try
      {
        String FileName = Path.GetFileNameWithoutExtension(fileentry);
        String FileExtension = Path.GetExtension(fileentry);
        String FileEntryMatrix = FileName + "Matrix" + FileExtension;
        CExcelFile ExcelFile = new CExcelFile();
        Byte[,] Matrix = MatrixTruecolor;
        Boolean Result = ExcelFile.WriteMatrixByte(FileEntryMatrix, "Matrix", Matrix);
        // No Palette
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadTruecolorFromExcelFile(String fileentry)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public - ExcelSheet
    //--------------------------------------------------------
    //
    public Boolean ExportTruecolorToExcelSheet(String sheetname)
    {
      try
      {
        String SheetMatrix = sheetname + "Matrix";
        // ??? String SheetPalette = sheetname + "Palette";
        CExcelSheet ExcelSheet = new CExcelSheet();
        Byte[,] Matrix = MatrixTruecolor;
        Boolean Result = ExcelSheet.ExportMatrixByte(SheetMatrix, Matrix);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean ImportTruecolorFromExcelSheet(String sheetname)// ??? , EPalette256 palette)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Static Method
    //--------------------------------------------------------
    //
    static public Byte[,] CreateMatrixFromBitmapTruecolor(Bitmap bitmap)
    {
      try
      {
        Int32 BitmapWidth = bitmap.Width;
        Int32 BitmapHeight = bitmap.Height;
        Rectangle RSource = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDSource = bitmap.LockBits(RSource, ImageLockMode.ReadOnly, bitmap.PixelFormat);
        IntPtr PointerSource = BDSource.Scan0;
        Int32 StrideSource = BDSource.Stride;
        Byte[,] Result = new Byte[BitmapHeight, 4 * BitmapWidth]; // 4: BGRA
        unsafe
        { // Bitmap256(BGRA) -> MatrixByte(ARGB)
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PSource = (Byte*)(PointerSource + RI * StrideSource);
            Int32 CII = 0;
            Byte BB, BR, BG, BA;
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Source
              // Blue-Value
              BB = *(Byte*)PSource;
              PSource++;
              // Green-Value
              BG = *(Byte*)PSource;
              PSource++;
              // Red-Value
              BR = *(Byte*)PSource;
              PSource++;
              // Alpha-Value
              BA = *(Byte*)PSource;
              PSource++;
              // -> Target
              Result[RI, CII] = BA;
              CII++;
              Result[RI, CII] = BR;
              CII++;
              Result[RI, CII] = BG;
              CII++;
              Result[RI, CII] = BB;
              CII++;
            }
          }
        }
        bitmap.UnlockBits(BDSource);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }

    static public Bitmap CreateBitmapTruecolorFromMatrix(Byte[,] matrix)
    {
      try
      {
        Int32 BitmapHeight = matrix.GetLength(0);
        Int32 BitmapWidth = matrix.GetLength(1) / 4; // S(R) + S(G) + S(B) + S(A)
        Bitmap BitmapTarget = new Bitmap(BitmapWidth, BitmapHeight, PixelFormat.Format32bppArgb);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapTarget.LockBits(RTarget, ImageLockMode.WriteOnly, BitmapTarget.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        { // MatrixByte(ARGB) -> Bitmap256(BGRA)
          for (Int32 RI = 0; RI < BitmapHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            Int16 MCI = 0;
            for (Int32 CI = 0; CI < BitmapWidth; CI++)
            { // Source MatrixByte(ARGB)        
              Byte BA = matrix[RI, MCI];
              MCI++;
              Byte BR = matrix[RI, MCI];
              MCI++;
              Byte BG = matrix[RI, MCI];
              MCI++;
              Byte BB = matrix[RI, MCI];
              MCI++;
              // Target Bitmap256(BGRA)
              *PTarget = BB; 
              PTarget++;
              *PTarget = BG; 
              PTarget++;
              *PTarget = BR; 
              PTarget++;
              *PTarget = BA; 
              PTarget++;
            }
          }
        }
        BitmapTarget.UnlockBits(BDTarget);
        return BitmapTarget;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public Method
    //--------------------------------------------------------
    //
    public Boolean BuildFromMatrix(Int32 colleft,
                                   Int32 rowtop,
                                   Byte[,] matrix)
    { // 1408121640
      const Int32 SIZE_COLOR = 4;
      try
      {
        Byte[,] Matrix = MatrixTruecolor;
        Int32 BitmapWidth = FBitmapTruecolor.Width;
        Int32 BitmapHeight = FBitmapTruecolor.Height;
        Int32 MatrixHeight = matrix.GetLength(0);
        Int32 MatrixWidth = matrix.GetLength(1) / SIZE_COLOR; // ( S(R) + S(G) + S(B) +S(A)
        Int32 RowMaximum = Math.Min(rowtop + MatrixHeight, BitmapHeight);
        Int32 ColMaximum = Math.Min(colleft + MatrixWidth, BitmapWidth);
        Rectangle RTarget = new Rectangle(0, 0, BitmapWidth, BitmapHeight);
        BitmapData BDTarget = BitmapTruecolor.LockBits(RTarget, ImageLockMode.WriteOnly, 
                                                       BitmapTruecolor.PixelFormat);
        IntPtr PointerTarget = BDTarget.Scan0;
        Int32 StrideTarget = BDTarget.Stride;
        unsafe
        {
          Int16 MRI = 0;
          for (Int32 RI = rowtop; RI < RowMaximum; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            PTarget += SIZE_COLOR * colleft;
            Int16 MCI = 0;
            for (Int32 CI = colleft; CI < ColMaximum; CI++)
            { // Source Matrix
              Byte BA = matrix[MRI, MCI];
              MCI++;
              Byte BR = matrix[MRI, MCI];
              MCI++;
              Byte BG = matrix[MRI, MCI];
              MCI++;
              Byte BB = matrix[MRI, MCI];
              MCI++;
              // Target BitmapTruecolor BGRA
              // Blue-Value
              *PTarget = BB;
              PTarget++;
              // Green-Value
              *PTarget = BG;
              PTarget++;
              // Red-Value
              *PTarget = BR;
              PTarget++;
              // Alpha-Value
              *PTarget = BA;
              PTarget++;
            }
            MRI++;
          }
        }
        BitmapTruecolor.UnlockBits(BDTarget);
        if (FOnBitmapTruecolorChanged is DOnBitmapTruecolorChanged)
        {
          FOnBitmapTruecolorChanged();
        }
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean ExtractMatrix(Int32 colleft, Int32 rowtop, 
                                 Int32 colwidth, Int32 rowheight,
                                 out Byte[,] matrix)
    { // 1408121640
      try
      {
        Int32 BitmapWidth = FBitmapTruecolor.Width;
        Int32 BitmapHeight = FBitmapTruecolor.Height;
        Int32 MatrixHeight = Math.Min(rowheight, BitmapHeight - rowtop);
        Int32 MatrixWidth = Math.Min(colwidth, BitmapWidth - colleft);
        matrix = new Byte[MatrixHeight, 4 * MatrixWidth]; // S(B) + S(G) + S(R) +S(A)        
        Rectangle RSource = new Rectangle(colleft, rowtop, MatrixWidth, MatrixHeight);
        BitmapData BDSource = BitmapTruecolor.LockBits(RSource, ImageLockMode.WriteOnly, 
                                                       BitmapTruecolor.PixelFormat);
        IntPtr PointerTarget = BDSource.Scan0;
        Int32 StrideTarget = BDSource.Stride;
        unsafe
        {
          Int16 MRI = 0;
          for (Int32 RI = rowtop; RI < rowtop + MatrixHeight; RI++)
          {
            Byte* PTarget = (Byte*)(PointerTarget + RI * StrideTarget);
            PTarget += 4 * colleft;
            Int16 MCI = 0;
            for (Int32 CI = colleft; CI < colleft + MatrixWidth; CI++)
            { // Blue      
              matrix[MRI, MCI] = *PTarget;
              PTarget++;
              MCI++;
              // Green      
              matrix[MRI, MCI] = *PTarget;
              PTarget++;
              MCI++;
              // Red
              matrix[MRI, MCI] = *PTarget;
              PTarget++;
              MCI++;
              // Alpha
              matrix[MRI, MCI] = *PTarget;
              PTarget++;
              MCI++;
            }
            MRI++;
          }
        }
        BitmapTruecolor.UnlockBits(BDSource);
        return true;
      }
      catch (Exception e)
      {
        matrix = null;
        Console.WriteLine(e.Message);
        return false;
      }
    }


  }
}
