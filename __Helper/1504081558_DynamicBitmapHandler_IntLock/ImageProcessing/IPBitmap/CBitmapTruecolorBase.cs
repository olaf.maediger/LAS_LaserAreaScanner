﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
//
using IPMatrix;
using IPPalette;
//
namespace IPBitmap
{
  public class CBitmapTruecolorBase
  {
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    protected Bitmap FBitmapTruecolor;
    protected DOnBitmapTruecolorChanged FOnBitmapTruecolorChanged;
  }
}
