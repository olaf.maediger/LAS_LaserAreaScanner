﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
using IPMatrix;
using IPPalette;
//
namespace IPBitmap
{
  public partial class CBitmap256Palette : CBitmapPaletteBase
  {
    //
    //################################################################################################
    //	Segment - FileTransfer - Bmp
    //################################################################################################
    //
    public Boolean SaveGreyscaleToBmpFile(String fileentry)
    {
      try
      {
        /*CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        Boolean Result = Bitmap256Transfer.SaveBitmap256(fileentry, FBitmapGreyscale, true, ImageFormat.Bmp);
        return Result;*/
        // not here ImageCodecInfo[] ICIS = ImageCodecInfo.GetImageEncoders();
        // not here ImageCodecInfo ICI = ICIS[1];
        // not here System.Drawing.Imaging.Encoder E = System.Drawing.Imaging.Encoder.Quality;
        // not here EncoderParameters EP = new EncoderParameters(1);
        FBitmapGreyscale.Save(fileentry, ImageFormat.Bmp);//, ICI, EP);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToBmpFile(String fileentry)
    {
      try
      {/*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        Boolean Result = Bitmap256Transfer.SaveBitmap256(fileentry, FBitmapColored, true, ImageFormat.Bmp);
        return Result;*/
        FBitmapColored.Save(fileentry, ImageFormat.Bmp);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadGreyscaleFromBmpFile(String fileentry)//,
    //EPalette256 palette)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        FBitmapGreyscale = Bitmap256Transfer.LoadBitmap256(fileentry);
        if (FBitmapGreyscale is Bitmap)
        {
          FWidth = FBitmapGreyscale.Width;
          FHeight = FBitmapGreyscale.Height;
          if (INIT_PIXELFORMAT_256 == FBitmapGreyscale.PixelFormat)
          {
            // ??? BuildPalette256(palette);
            BuildBitmapColored();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromBmpFile(String fileentry)//,
    // ??? EPalette256 palette)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        FBitmapColored = Bitmap256Transfer.LoadBitmap256(fileentry);
        if (FBitmapColored is Bitmap)
        {
          FWidth = FBitmapColored.Width;
          FHeight = FBitmapColored.Height;
          if (INIT_PIXELFORMAT_256 == FBitmapColored.PixelFormat)
          {
            // ??? BuildPalette256(palette);
            BuildBitmapGreyscale();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //################################################################################################
    //	Segment - FileTransfer - Tif
    //################################################################################################
    //
    public Boolean SaveGreyscaleToTifFile(String fileentry)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        Boolean Result = Bitmap256Transfer.SaveBitmap256(fileentry, FBitmapGreyscale, true, ImageFormat.Tiff);
        return Result;*/
        FBitmapGreyscale.Save(fileentry, ImageFormat.Tiff);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToTifFile(String fileentry)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        Boolean Result = Bitmap256Transfer.SaveBitmap256(fileentry, FBitmapColored, true, ImageFormat.Tiff);
        return Result;*/
        FBitmapGreyscale.Save(fileentry, ImageFormat.Tiff);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadGreyscaleFromTifFile(String fileentry)//,
    //EPalette256 palette)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        FBitmapGreyscale = Bitmap256Transfer.LoadBitmap256(fileentry);
        if (FBitmapGreyscale is Bitmap)
        {
          FWidth = FBitmapGreyscale.Width;
          FHeight = FBitmapGreyscale.Height;
          if (INIT_PIXELFORMAT_256 == FBitmapGreyscale.PixelFormat)
          {
            // ??? BuildPalette256(palette);
            BuildBitmapColored();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromTifFile(String fileentry)//,
    //EPalette256 palette)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        FBitmapColored = Bitmap256Transfer.LoadBitmap256(fileentry);
        if (FBitmapColored is Bitmap)
        {
          FWidth = FBitmapColored.Width;
          FHeight = FBitmapColored.Height;
          if (INIT_PIXELFORMAT_256 == FBitmapColored.PixelFormat)
          {
            // ??? BuildPalette256(palette);
            BuildBitmapGreyscale();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //################################################################################################
    //	Segment - FileTransfer - Png
    //################################################################################################
    //
    public Boolean SaveGreyscaleToPngFile(String fileentry)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        Boolean Result = Bitmap256Transfer.SaveBitmap256(fileentry, FBitmapGreyscale, true, ImageFormat.Png);
        return Result;*/
        FBitmapGreyscale.Save(fileentry, ImageFormat.Png);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToPngFile(String fileentry)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        Boolean Result = Bitmap256Transfer.SaveBitmap256(fileentry, FBitmapColored, true, ImageFormat.Png);
        return Result;*/
        FBitmapColored.Save(fileentry, ImageFormat.Png);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadGreyscaleFromPngFile(String fileentry)//,
    //EPalette256 palette)
    {
      try
      {
        /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        FBitmapGreyscale = Bitmap256Transfer.LoadBitmap256(fileentry);
        if (FBitmapGreyscale is Bitmap)
        {
          FWidth = FBitmapGreyscale.Width;
          FHeight = FBitmapGreyscale.Height;
          if (INIT_PIXELFORMAT_256 == FBitmapGreyscale.PixelFormat)
          {
            // ??? BuildPalette256(palette);
            BuildBitmapColored();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromPngFile(String fileentry)//,
    //EPalette256 palette)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        FBitmapColored = Bitmap256Transfer.LoadBitmap256(fileentry);
        if (FBitmapColored is Bitmap)
        {
          FWidth = FBitmapColored.Width;
          FHeight = FBitmapColored.Height;
          if (INIT_PIXELFORMAT_256 == FBitmapGreyscale.PixelFormat)
          {
            // ??? BuildPalette256(palette);
            BuildBitmapGreyscale();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //################################################################################################
    //	Segment - FileTransfer - Txt
    //################################################################################################
    //
    public Boolean SaveGreyscaleToTextFile(String fileentry)
    {
      try
      {
        CTextFile TextFile = new CTextFile();
        Boolean Result = TextFile.OpenWrite(fileentry);
        Byte[,] Matrix = CreateMatrixFromBitmapGreyscale(FBitmapGreyscale);
        Result &= TextFile.Write(Matrix);
        // ??? CPalette256Base Palette256Greyscale = CIPPalette256.CreatePalette(EPalette256.Greyscale);
        // ??? Byte[,] Palette = CIPBitmap256.GetMatrixFromPalette(Palette256Greyscale);
        // ??? Result &= TextFile.Write(Palette);
        Result &= TextFile.Close();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToTextFile(String fileentry)
    { // 1412081521
      try
      {
        CTextFile TextFile = new CTextFile();
        Boolean Result = TextFile.OpenWrite(fileentry);
        Byte[,] Matrix = CreateMatrixFromBitmapColored(FBitmapColored);
        Result &= TextFile.Write(Matrix);
        // ??? Byte[,] Palette = CIPBitmap256.GetMatrixFromPalette(FPalette256);
        // ??? Result &= TextFile.Write(Palette);
        Result &= TextFile.Close();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadGreyscaleFromTextFile(String fileentry)//,
    // ??? EPalette256 palette)
    { // 1412081521
      try
      {
        CTextFile TextFile = new CTextFile();
        Boolean Result = TextFile.OpenRead(fileentry);
        Byte[,] MatrixBitmap = null;
        Result &= TextFile.Read(out MatrixBitmap);
        FBitmapGreyscale = CreateBitmapGreyscaleFromMatrix(MatrixBitmap);
        // ??? Byte[,] MatrixPalette = null;
        // ??? Result &= TextFile.Read(out MatrixPalette);
        // ??? FPalette256 = CIPPalette256.CreatePalette256FromMatrix(MatrixPalette);
        Result &= TextFile.Close();
        if (FBitmapGreyscale is Bitmap)
        {
          // NC FWidth = FBitmapGreyscale.Width;
          // NC FHeight = FBitmapGreyscale.Height;
          if (INIT_PIXELFORMAT_256 == FBitmapGreyscale.PixelFormat)
          {
            // ??? BuildPalette256(palette);
            //!!!!!!!!!!!!!!!!!!!!BuildBitmapColored(FPalette);
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
          }
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromTextFile(String fileentry)
    //EPalette256 palette)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        FBitmapColored = Bitmap256Transfer.LoadBitmap256(fileentry);
        if (FBitmapColored is Bitmap)
        {
          FWidth = FBitmapColored.Width;
          FHeight = FBitmapColored.Height;
          if (INIT_PIXELFORMAT_256 == FBitmapColored.PixelFormat)
          {
            // ??? BuildPalette256(palette);
            BuildBitmapGreyscale();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //################################################################################################
    //	Segment - FileTransfer - Tbf (TaggedBinaryFile)
    //################################################################################################
    //
    public Boolean SaveGreyscaleToTaggedBinaryFile(String fileentry)
    {
      try
      {
        CTaggedBinaryFile TaggedBinaryFile = new CTaggedBinaryFile();
        Boolean Result = TaggedBinaryFile.OpenWrite(fileentry);
        Byte[,] Matrix = CreateMatrixFromBitmapGreyscale(FBitmapGreyscale);
        Result &= TaggedBinaryFile.Write(Matrix);
        // ??? CPalette256Base Palette256Greyscale = CIPPalette256.CreatePalette(EPalette256.Greyscale);
        // ??? Byte[,] Palette = CIPBitmap256.GetMatrixFromPalette(Palette256Greyscale);
        // ??? Result &= TaggedBinaryFile.Write(Palette);
        Result &= TaggedBinaryFile.CloseWrite();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToTaggedBinaryFile(String fileentry)
    {
      try
      {
        CTaggedBinaryFile TaggedBinaryFile = new CTaggedBinaryFile();
        Boolean Result = TaggedBinaryFile.OpenWrite(fileentry);
        Byte[,] Matrix = CreateMatrixFromBitmapColored(FBitmapColored);
        Result &= TaggedBinaryFile.Write(Matrix);
        // ??? Byte[,] Palette = CIPBitmap256.GetMatrixFromPalette(FPalette256);
        // ??? Result &= TaggedBinaryFile.Write(Palette);
        Result &= TaggedBinaryFile.CloseWrite();
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    // TaggedBinaryFile : uses Palette from File:
    public Boolean LoadGreyscaleFromTaggedBinaryFile(String fileentry)
    {
      try
      {
        CTaggedBinaryFile TaggedBinaryFile = new CTaggedBinaryFile();
        Boolean Result = TaggedBinaryFile.OpenWrite(fileentry);
        Byte[,] MatrixBitmap = null;
        // ??? Byte[,] MatrixPalette = null;
        Result &= TaggedBinaryFile.Read(out MatrixBitmap);
        // ??? Result &= TaggedBinaryFile.Read(out MatrixPalette);
        Result &= TaggedBinaryFile.CloseRead();
        FBitmapGreyscale = CreateBitmapGreyscaleFromMatrix(MatrixBitmap);
        if (FBitmapGreyscale is Bitmap)
        {
          // NC FWidth = FBitmapGreyscale.Width;
          // NC FHeight = FBitmapGreyscale.Height;
          if (INIT_PIXELFORMAT_256 == FBitmapGreyscale.PixelFormat)
          {
            // ??? FPalette256 = CIPPalette256.CreatePalette256FromMatrix(MatrixPalette);
            //!!!!!!!!!!!!!!!!!!!!BuildBitmapColored(FPalette256);
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromTaggedBinaryFile(String fileentry)//,
    // ??? EPalette256 palette)
    {
      try
      { /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        CBitmap256Transfer Bitmap256Transfer = new CBitmap256Transfer();
        FBitmapColored = Bitmap256Transfer.LoadBitmap256(fileentry);
        if (FBitmapColored is Bitmap)
        {
          FWidth = FBitmapColored.Width;
          FHeight = FBitmapColored.Height;
          if (INIT_PIXELFORMAT_256 == FBitmapColored.PixelFormat)
          {
            // ???  BuildPalette256(palette);
            BuildBitmapGreyscale();
            if (FOnBitmapGreyscaleChanged is DOnBitmapGreyscaleChanged)
            {
              FOnBitmapGreyscaleChanged();
            }
            if (FOnBitmapColoredChanged is DOnBitmapColoredChanged)
            {
              FOnBitmapColoredChanged();
            }
            return true;
          }
        }*/
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //################################################################################################
    //	Segment - FileTransfer - Xls (Excel - File)
    //################################################################################################
    //
    public Boolean SaveGreyscaleToExcelFile(String fileentry)
    { // 1412081521
      try
      {
        String FileName = Path.GetFileNameWithoutExtension(fileentry);
        String FileExtension = Path.GetExtension(fileentry);
        String FileEntryMatrix = FileName + "Matrix" + FileExtension;
        // ??? String FileEntryPalette = FileName + "Palette" + FileExtension;
        CExcelFile ExcelFile = new CExcelFile();
        Byte[,] Matrix = CreateMatrixFromBitmapGreyscale(FBitmapGreyscale);
        Boolean Result = ExcelFile.WriteMatrixByte(FileEntryMatrix, "Matrix", Matrix);
        // ??? CPalette256Base Palette256Greyscale = CIPPalette256.CreatePalette(EPalette256.Greyscale);
        // ??? Byte[,] Palette = CIPBitmap256.GetMatrixFromPalette(Palette256Greyscale);
        // ??? Result &= ExcelFile.WriteMatrixByte(FileEntryPalette, "Palette", Palette);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean SaveColoredToExcelFile(String fileentry)
    { // 1412081521
      try
      {
        String FileName = Path.GetFileNameWithoutExtension(fileentry);
        String FileExtension = Path.GetExtension(fileentry);
        String FileEntryMatrix = FileName + "Matrix" + FileExtension;
        // ??? String FileEntryPalette = FileName + "Palette" + FileExtension;
        CExcelFile ExcelFile = new CExcelFile();
        Byte[,] Matrix = CreateMatrixFromBitmapColored(FBitmapColored);
        Boolean Result = ExcelFile.WriteMatrixByte(FileEntryMatrix, "Matrix", Matrix);
        // ??? Byte[,] Palette = CIPBitmap256.GetMatrixFromPalette(FPalette256);
        // ??? Result &= ExcelFile.WriteMatrixByte(FileEntryPalette, "Palette", Palette);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean LoadGreyscaleFromExcelFile(String fileentry)// ??? , EPalette256 palette)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean LoadColoredFromExcelFile(String fileentry)// ??? , EPalette256 palette)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    //
    //################################################################################################
    //	Segment - FileTransfer - Xls (Excel - Sheet)
    //################################################################################################
    //
    public Boolean ExportGreyscaleToExcelSheet(String sheetname)
    { // 1412081521
      try
      {
        String SheetMatrix = sheetname + "Matrix";
        // ??? String SheetPalette = sheetname + "Palette";
        CExcelSheet ExcelSheet = new CExcelSheet();
        Byte[,] Matrix = CreateMatrixFromBitmapGreyscale(FBitmapGreyscale);
        Boolean Result = ExcelSheet.ExportMatrixByte(SheetMatrix, Matrix);
        // ??? CPalette256Base Palette256Greyscale = CIPPalette256.CreatePalette(EPalette256.Greyscale);
        // ??? Byte[,] Palette = CIPBitmap256.GetMatrixFromPalette(Palette256Greyscale);
        // ??? Result &= ExcelSheet.ExportMatrixByte(SheetPalette, Palette);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean ExportColoredToExcelSheet(String sheetname)
    { // 1412081521
      try
      {
        String SheetMatrix = sheetname + "Matrix";
        // ??? String SheetPalette = sheetname + "Palette";
        CExcelSheet ExcelSheet = new CExcelSheet();
        Byte[,] Matrix = CreateMatrixFromBitmapColored(FBitmapColored);
        Boolean Result = ExcelSheet.ExportMatrixByte(SheetMatrix, Matrix);
        // ??? Byte[,] Palette = CIPBitmap256.GetMatrixFromPalette(FPalette256);
        // ??? Result &= ExcelSheet.ExportMatrixByte(SheetPalette, Palette);
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean ImportGreyscaleFromExcelSheet(String sheetname)// ??? , EPalette256 palette)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean ImportColoredFromExcelSheet(String sheetname)// ??? , EPalette256 palette)
    {
      try
      {
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

  }
}
