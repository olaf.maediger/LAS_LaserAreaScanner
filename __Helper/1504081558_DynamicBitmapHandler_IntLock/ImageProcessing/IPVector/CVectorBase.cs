﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPVector
{
  public abstract class CVectorBase
  { //
    //-----------------------------------------------------------------------------
    //  Segment - Constant
    //-----------------------------------------------------------------------------
    //
    protected const Int32 INIT_BASECOUNT = 1;
    //
    public const Int32 SHIFT_256_256 = 0;
    public const Int32 SHIFT_256_1K = 2;
    public const Int32 SHIFT_256_4K = 4;
    //
    public const Int32 SHIFT_1K_256 = 2;
    public const Int32 SHIFT_1K_4K = 2;
    //
    public const Int32 SHIFT_4K_256 = 4;
    public const Int32 SHIFT_4K_1K = 2;
    //
    public const Int32 SIZE_QUAD = 4;
    //
    public const Byte INIT_ALPHA_256 = 0xFF;
    public const UInt16 INIT_ALPHA_1K = 0x03FF;
    public const UInt16 INIT_ALPHA_4K = 0x0FFF;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------------
    //

    //
    //-----------------------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------------------
    //
    public CVectorBase()
    {
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------------------
    //
    protected abstract Int32 GetBaseCount();

    public Int32 BaseCount
    {
      get { return GetBaseCount(); }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function
    //-----------------------------------------------------------------------------
    //
    public abstract Boolean BuildConstant(Int32 basecount, Int32 value);
    public abstract Boolean BuildLinear(Int32 basecount);
    public abstract Boolean BuildInverse(Int32 basecount);
    public abstract Boolean BuildRandom(Int32 basecount);
    //
    //???public abstract Byte[] CloneDataVector256();
    //???public abstract Byte[] CloneDataVectorQuad256();
    //
    //???public abstract UInt16[] CloneDataVector1K();
    //???public abstract UInt16[] CloneDataVectorQuad1K();
    //
    //???public abstract UInt16[] CloneDataVector4K();
    //???public abstract UInt16[] CloneDataVectorQuad4K();
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone256 Index -> Quad 
    //-----------------------------------------------------------------------------
    //
    public static Byte[] CloneDataVectorQuad256(Byte[] vector)
    {
      Int32 BC = vector.Length;
      Byte[] Result = new Byte[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        Byte BValue = (Byte)(vector[BI] >> SHIFT_256_256);
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = INIT_ALPHA_256;
        VI++;
      }
      return Result;
    }
    
    public static Byte[] CloneDataVectorQuad256(UInt16[] vector)
    {
      Int32 BC = vector.GetLength(0);
      Byte[] Result = new Byte[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        Byte BValue = (Byte)(vector[BI] >> SHIFT_256_256);
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = INIT_ALPHA_256;
        VI++;
      }
      return Result;
    }

    public static Byte[] CloneDataVectorQuad256(Int16[] vector)
    {
      Int32 BC = vector.GetLength(0);
      Byte[] Result = new Byte[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        Byte BValue = (Byte)(vector[BI] >> SHIFT_256_256);
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = INIT_ALPHA_256;
        VI++;
      }
      return Result;
    }

    public static Byte[] CloneDataVectorQuad256(UInt32[] vector)
    {
      Int32 BC = vector.GetLength(0);
      Byte[] Result = new Byte[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        Byte BValue = (Byte)(vector[BI] >> SHIFT_256_256);
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = INIT_ALPHA_256;
        VI++;
      }
      return Result;
    }

    public static Byte[] CloneDataVectorQuad256(Int32[] vector)
    {
      Int32 BC = vector.GetLength(0);
      Byte[] Result = new Byte[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        Byte BValue = (Byte)(vector[BI] >> SHIFT_256_256);
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = INIT_ALPHA_256;
        VI++;
      }
      return Result;
    }

    public static Byte[] CloneDataVectorQuad256(float[] vector)
    {
      Int32 BC = vector.GetLength(0);
      Byte[] Result = new Byte[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        Byte BValue = (Byte)((Int32)vector[BI] >> SHIFT_256_256);
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = INIT_ALPHA_256;
        VI++;
      }
      return Result;
    }

    public static Byte[] CloneDataVectorQuad256(Double[] vector)
    {
      Int32 BC = vector.GetLength(0);
      Byte[] Result = new Byte[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        Byte BValue = (Byte)((Int32)vector[BI] >> SHIFT_256_256);
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = INIT_ALPHA_256;
        VI++;
      }
      return Result;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone1K Index -> Quad 
    //-----------------------------------------------------------------------------
    //
    public static UInt16[] CloneDataVectorQuad1K(UInt16[] vector)
    {
      Int32 BC = vector.Length;
      UInt16[] Result = new UInt16[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        UInt16 UValue = (UInt16)(vector[BI] >> SHIFT_1K_256);
        Result[VI] = UValue;
        VI++;
        Result[VI] = UValue;
        VI++;
        Result[VI] = UValue;
        VI++;
        Result[VI] = INIT_ALPHA_1K;
        VI++;
      }
      return Result;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone4K Index -> Quad 
    //-----------------------------------------------------------------------------
    //
    public static UInt16[] CloneDataVectorQuad4K(UInt16[] vector)
    {
      Int32 BC = vector.Length;
      UInt16[] Result = new UInt16[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        UInt16 UValue = (UInt16)(vector[BI] >> SHIFT_1K_256);
        Result[VI] = UValue;
        VI++;
        Result[VI] = UValue;
        VI++;
        Result[VI] = UValue;
        VI++;
        Result[VI] = INIT_ALPHA_4K;
        VI++;
      }
      return Result;
    }


    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone256 - 256/1K/4K
    //-----------------------------------------------------------------------------
    //
    /*???protected Byte[] CloneDataVector256(CVectorBase vector)
    {
      Int32 BC = vector.GetBaseCount();
      Byte[] Result = null;
      Int32 VI = 0;
      if (vector is CVector256)
      {
        Result = new Byte[BC * sizeof(Byte)];
        for (Int32 BI = 0; BI < BC; BI++)
        {
          Result[VI] = (Byte)(((CVector256)vector)[BI] >> SHIFT_256_256);
          VI++;
        }
      }
      else
        if (vector is CVector1K)
        {
          Result = new Byte[BC * sizeof(UInt16)];
          for (Int32 BI = 0; BI < BC; BI++)
          {
            Result[VI] = (Byte)(((CVector1K)vector)[BI] >> SHIFT_1K_256);
            VI++;
          }
        }
        else
          if (vector is CVector4K)
          {
            Result = new Byte[BC * sizeof(UInt16)];
            for (Int32 BI = 0; BI < BC; BI++)
            {
              Result[VI] = (Byte)(((CVector4K)vector)[BI] >> SHIFT_4K_256);
              VI++;
            }
          }
      return Result;
    }*/

    public Byte[] CloneDataVectorQuad256(CVector256 vector)
    {
      Int32 BC = vector.GetBaseCount();
      Byte[] Result = new Byte[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        Byte BValue = (Byte)(vector[BI] >> SHIFT_256_256);
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = INIT_ALPHA_256;
        VI++;
      }
      return Result;
    }

    public Byte[] CloneDataVectorQuad256(CVector1K vector)
    {
      Int32 BC = vector.GetBaseCount();
      Byte[] Result = new Byte[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        Byte BValue = (Byte)(((CVector1K)vector)[BI] >> SHIFT_1K_256);
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = INIT_ALPHA_256;
        VI++;
      }
      return Result;
    }
    
    public Byte[] CloneDataVectorQuad256(CVector4K vector)
    {
      Int32 BC = vector.GetBaseCount();
      Byte[] Result = new Byte[SIZE_QUAD * BC];
      Int32 VI = 0;
      for (Int32 BI = 0; BI < BC; BI++)
      {
        Byte BValue = (Byte)(((CVector4K)vector)[BI] >> SHIFT_4K_256);
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = BValue;
        VI++;
        Result[VI] = INIT_ALPHA_256;
        VI++;
      }
      return Result;
    }



  }
}
