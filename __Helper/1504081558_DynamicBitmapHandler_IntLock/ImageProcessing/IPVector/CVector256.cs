﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPVector
{
  public class CVector256 : CVectorBase
  {
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constant
    //-----------------------------------------------------------------------------
    //
    private const Byte INIT_VALUE_LOW = 0x00;
    private const Byte INIT_VALUE_HIGH = 0xFF;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------------
    //
    protected Byte[] FVector;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------------------
    //
    public CVector256()
    {
      FVector = new Byte[INIT_BASECOUNT];
    }

    public CVector256(Int32 size)
    {
      Int32 Size = Math.Max(INIT_BASECOUNT, size);
      FVector = new Byte[Size];
    }

    public CVector256(Int32 size, Byte init)
    {
      Int32 Size = Math.Max(INIT_BASECOUNT, size);
      FVector = new Byte[Size];
      for (Int32 BI = 0; BI < Size; BI++)
      {
        FVector[BI] = (Byte)init;
      }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------------------
    //
    protected override Int32 GetBaseCount()
    {
      return FVector.GetLength(0);
    }

    protected Byte GetValue(Int32 index)
    {
      return FVector[index];
    }
    protected void SetValue(Int32 index, Byte value)
    {
      FVector[index] = value;
    }
    public Byte this[Int32 index]
    {
      get { return GetValue(index); }
      set { SetValue(index, value); }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone256
    //-----------------------------------------------------------------------------
    //

    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone1K
    //-----------------------------------------------------------------------------
    //

    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone4K
    //-----------------------------------------------------------------------------
    //

    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Build
    //-----------------------------------------------------------------------------
    //
    public override Boolean BuildConstant(Int32 basecount, Int32 value)
    {
      try
      {
        Int32 BC = basecount;
        FVector = new Byte[BC];
        for (Int32 BI = 0; BI < BC; BI++)
        {
          FVector[BI] = (Byte)value;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildLinear(Int32 basecount)
    {
      try
      {
        Int32 BC = basecount;
        FVector = new Byte[BC];
        for (Int32 BI = 0; BI < BC; BI++)
        {
          FVector[BI] = (Byte)((BI * INIT_VALUE_HIGH) / (BC - 1));
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildInverse(Int32 basecount)
    {
      try
      {
        Int32 BC = basecount;
        FVector = new Byte[BC];
        for (Int32 BI = 0; BI < BC; BI++)
        {
          FVector[BI] = (Byte)(INIT_VALUE_HIGH - BI * INIT_VALUE_HIGH / (BC - 1));
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildRandom(Int32 basecount)
    {
      try
      {
        Random R = new Random((Int32)DateTime.Now.Ticks);
        Int32 BC = basecount;
        FVector = new Byte[BC];
        for (Int32 BI = 0; BI < BC; BI++)
        {
          FVector[BI] = (Byte)R.Next(INIT_VALUE_LOW, 1 + INIT_VALUE_HIGH);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
