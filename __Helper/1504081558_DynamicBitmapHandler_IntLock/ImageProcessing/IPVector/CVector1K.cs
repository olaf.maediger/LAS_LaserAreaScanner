﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPVector
{
  public class CVector1K : CVectorBase
  {
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constant
    //-----------------------------------------------------------------------------
    //
    private const UInt16 INIT_VALUE_LOW = 0x0000;
    private const UInt16 INIT_VALUE_HIGH = 0x03FF;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------------
    //
    protected UInt16[] FVector;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------------------
    //
    public CVector1K()
    {
      FVector = new UInt16[INIT_BASECOUNT];
    }

    public CVector1K(Int32 size)
    {
      Int32 Size = Math.Max(INIT_BASECOUNT, size);
      FVector = new UInt16[Size];
    }

    public CVector1K(Int32 size, Byte init)
    {
      Int32 Size = Math.Max(INIT_BASECOUNT, size);
      FVector = new UInt16[Size];
      for (Int32 BI = 0; BI < Size; BI++)
      {
        FVector[BI] = (Byte)init;
      }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------------------
    //
    protected override Int32 GetBaseCount()
    {
      return FVector.GetLength(0);
    }

    protected UInt16 GetValue(Int32 index)
    {
      return FVector[index];
    }
    protected void SetValue(Int32 index, UInt16 value)
    {
      FVector[index] = value;
    }
    public UInt16 this[Int32 index]
    {
      get { return GetValue(index); }
      set { SetValue(index, value); }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone256
    //-----------------------------------------------------------------------------
    //
    //

    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone1K
    //-----------------------------------------------------------------------------
    //

    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Clone4K
    //-----------------------------------------------------------------------------
    //

    //
    //-----------------------------------------------------------------------------
    //  Segment - Function - Build
    //-----------------------------------------------------------------------------
    //
    public override Boolean BuildConstant(Int32 basecount, Int32 value)
    {
      try
      {
        Int32 BC = basecount;
        FVector = new UInt16[BC];
        for (Int32 BI = 0; BI < BC; BI++)
        {
          FVector[BI] = (UInt16)value;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildLinear(Int32 basecount)
    {
      try
      {
        Int32 BC = basecount;
        FVector = new UInt16[BC];
        for (Int32 BI = 0; BI < BC; BI++)
        {
          FVector[BI] = (UInt16)((BI * INIT_VALUE_HIGH) / (BC - 1));
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildInverse(Int32 basecount)
    {
      try
      {
        Int32 BC = basecount;
        FVector = new UInt16[BC];
        for (Int32 BI = 0; BI < BC; BI++)
        {
          FVector[BI] = (UInt16)(INIT_VALUE_HIGH - BI * INIT_VALUE_HIGH / (BC - 1));
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean BuildRandom(Int32 basecount)
    {
      try
      {
        Random R = new Random((Int32)DateTime.Now.Ticks);
        Int32 BC = basecount;
        FVector = new UInt16[BC];
        for (Int32 BI = 0; BI < BC; BI++)
        {
          FVector[BI] = (UInt16)R.Next(INIT_VALUE_LOW, 1 + INIT_VALUE_HIGH);
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
