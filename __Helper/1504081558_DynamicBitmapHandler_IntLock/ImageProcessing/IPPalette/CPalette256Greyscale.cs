﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette
{
  public class CPalette256Greyscale : CPalette256Base
  {
    //
    //-------------------------------------------------------
    //  Section - Constructor
    //-------------------------------------------------------
    //
    public CPalette256Greyscale()
      : base()
    {
      Initialize();
    }
    //
    //-------------------------------------------------------
    //  Section - Build
    //-------------------------------------------------------
    //
    override protected void Initialize()
    {
      for (Int32 CI = 0; CI < CIPPalette256.COLORRANGE_256; CI++)
      {
        UInt16 WValue = (UInt16)(CI * CIPPalette256.COLORRANGE_8BIT / CIPPalette256.COLORRANGE_256);
        FPartsRed[CI] = (Byte)(1.00f * WValue);
        FPartsGreen[CI] = (Byte)(1.00f * WValue);
        FPartsBlue[CI] = (Byte)(1.00f * WValue);
      }
    }
    //
    //--------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------
    //
    public override EPalette256 GetPalette()
    {
      return EPalette256.Greyscale;
    }

  }
}
