﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette
{
  public class CPalette256UserDefined : CPalette256Base
  {
    public CPalette256UserDefined()
      : base(2)
    {
      Initialize();
    }

    override protected void Initialize()
    {
      EColorMode ColorMode = EColorMode.Red;
      Int32 FillIndex = 0;
      Int32 FillBorder = CIPPalette256.COLORRANGE_8BIT / FCountSection;
      Byte BValue = CIPPalette256.COLORRANGE_8BIT - 1;
      Byte Subtrahend = (Byte)(CIPPalette256.COLORRANGE_8BIT / FCountSection / 4);
      for (Int32 CI = CIPPalette256.COLORRANGE_256 - 1; 0 <= CI; CI--)
      {
        switch (ColorMode)
        {
          case EColorMode.Red:
            FPartsRed[CI] = BValue;
            FPartsGreen[CI] = (Byte)0x00;
            FPartsBlue[CI] = (Byte)0x00;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Green;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
          case EColorMode.Green:
            FPartsRed[CI] = (Byte)0x00;
            FPartsGreen[CI] = BValue;
            FPartsBlue[CI] = (Byte)0x00;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Blue;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
          case EColorMode.Blue:
            FPartsRed[CI] = (Byte)0x00;
            FPartsGreen[CI] = (Byte)0x00;
            FPartsBlue[CI] = BValue;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Red;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
        }
      }
    }

    public Boolean SetColorEntry(UInt16 index, Color color)
    {
      if ((0 <= index) && (index < 1023))
      {
        base.FPartsRed[index] = color.R;
        base.FPartsGreen[index] = color.G;
        base.FPartsBlue[index] = color.B;
        return true;
      }
      return false;
    }
    //
    //-------------------------------------------------------
    //  Section - Property
    //-------------------------------------------------------
    //
    public override EPalette256 GetPalette()
    {
      return EPalette256.UserDefined;
    }

  }
}

