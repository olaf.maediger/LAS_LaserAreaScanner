﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette
{
  public enum EColorMode
  {
    Red,
    Green,
    Blue
  }

  public abstract class CPaletteBase
  { //
    //--------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------
    //
    static public Byte INIT_ALPHA = 0xFF;
    //
    //--------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------
    //
    protected Byte[] FPartsAlpha;   // R(8b)
    protected Byte[] FPartsRed;   // R(8b)
    protected Byte[] FPartsGreen; // G(8b)
    protected Byte[] FPartsBlue;  // B(8b)
    //
    //--------------------------------------------------------------
    //  Segment - Properties
    //--------------------------------------------------------------
    //
    public abstract Byte GetPartsAlpha(UInt16 index);
    public abstract Byte GetPartsRed(UInt16 index);
    public abstract Byte GetPartsGreen(UInt16 index);
    public abstract Byte GetPartsBlue(UInt16 index);
    public abstract Color GetColor(UInt16 index);

    public abstract UInt16 FindPaletteIndex(Byte red, Byte green, Byte blue);
    public abstract UInt16 FindPaletteIndex(Byte alpha, Byte red, Byte green, Byte blue);

  }
}
