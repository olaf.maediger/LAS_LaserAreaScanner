﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPPalette
{
  public abstract class CPalette256RgbLight : CPalette256Base
  {
    public CPalette256RgbLight(Int32 countsection)
      : base(countsection)
    {
      Initialize();
    }

    override protected void Initialize()
    {
      EColorMode ColorMode = EColorMode.Red;
      Int32 FillIndex = 0;
      Int32 FillBorder = CIPPalette256.COLORRANGE_8BIT / FCountSection;
      Byte BValue = CIPPalette256.COLORRANGE_8BIT - 1;
      Byte Subtrahend = (Byte)(CIPPalette256.COLORRANGE_8BIT / FCountSection / 8);
      for (Int32 CI = CIPPalette256.COLORRANGE_256 - 1; 0 <= CI; CI--)
      {
        switch (ColorMode)
        {
          case EColorMode.Red:
            FPartsRed[CI] = BValue;
            FPartsGreen[CI] = (Byte)0x00;
            FPartsBlue[CI] = (Byte)0x00;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Green;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
          case EColorMode.Green:
            FPartsRed[CI] = (Byte)0x00;
            FPartsGreen[CI] = BValue;
            FPartsBlue[CI] = (Byte)0x00;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Blue;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
          case EColorMode.Blue:
            FPartsRed[CI] = (Byte)0x00;
            FPartsGreen[CI] = (Byte)0x00;
            FPartsBlue[CI] = BValue;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Red;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
        }
      }
    }
  }


  public class CPalette256Rgb8Light : CPalette256RgbLight
  {
    //
    //--------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------
    //
    public CPalette256Rgb8Light()
      : base(8)
    {
    }
    //
    //--------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------
    //
    public override EPalette256 GetPalette()
    {
      return EPalette256.RGB8Light;
    }
  }

  public class CPalette256Rgb32Light : CPalette256RgbLight
  {
    //
    //--------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------
    //
    public CPalette256Rgb32Light()
      : base(32)
    {
    }
    //
    //--------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------
    //
    public override EPalette256 GetPalette()
    {
      return EPalette256.RGB32Light;
    }
  }

}

