﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette
{
  public class CPalette256Random : CPalette256Base
  {
    //
    //-------------------------------------------------------
    //  Section - Field
    //-------------------------------------------------------
    //

    //
    //-------------------------------------------------------
    //  Section - Constructor
    //-------------------------------------------------------
    //
    public CPalette256Random()
      : base()
    {
      Initialize();
    }
    //
    //-------------------------------------------------------
    //  Section - Build
    //-------------------------------------------------------
    //
    override protected void Initialize()
    {
      Random R = new Random();
      for (Int32 CI = 0; CI < CIPPalette256.COLORRANGE_256; CI++)
      {
        Int32 RR = R.Next(0, CIPPalette256.COLORRANGE_256);
        UInt16 WR = (UInt16)(RR * CIPPalette256.COLORRANGE_8BIT / CIPPalette256.COLORRANGE_256);
        Int32 RG = R.Next(0, CIPPalette256.COLORRANGE_256);
        UInt16 WG = (UInt16)(RG * CIPPalette256.COLORRANGE_8BIT / CIPPalette256.COLORRANGE_256);
        Int32 RB = R.Next(0, CIPPalette256.COLORRANGE_256);
        UInt16 WB = (UInt16)(RB * CIPPalette256.COLORRANGE_8BIT / CIPPalette256.COLORRANGE_256);
        FPartsRed[CI] = (Byte)(WR);
        FPartsGreen[CI] = (Byte)(WG);
        FPartsBlue[CI] = (Byte)(WB);
      }
    }
    //
    //--------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------
    //
    public override EPalette256 GetPalette()
    {
      return EPalette256.Random;
    }

  }
}
