﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette
{
  public class CPalette1KRandom : CPalette1KBase
  {
    //
    //-------------------------------------------------------
    //  Section - Field
    //-------------------------------------------------------
    //

    //
    //-------------------------------------------------------
    //  Section - Constructor
    //-------------------------------------------------------
    //
    public CPalette1KRandom(Int32 countsection)
      : base(countsection)
    {
      Initialize();
    }

    public CPalette1KRandom()
      : base(1)
    {
      Initialize();
    }
    //
    //-------------------------------------------------------
    //  Section - Property
    //-------------------------------------------------------
    //
    public override EPalette1K GetPalette()
    {
      return EPalette1K.Random;
    }
    //
    //-------------------------------------------------------
    //  Section - Build
    //-------------------------------------------------------
    //
    override protected void Initialize()
    {
      Random R = new Random();
      for (Int32 CI = 0; CI < CIPPalette1K.COLORRANGE_1K; CI++)
      {
        Int32 RR = R.Next(0, CIPPalette1K.COLORRANGE_1K);
        UInt16 WR = (UInt16)(RR * CIPPalette1K.COLORRANGE_8BIT / CIPPalette1K.COLORRANGE_1K);
        Int32 RG = R.Next(0, CIPPalette1K.COLORRANGE_1K);
        UInt16 WG = (UInt16)(RG * CIPPalette1K.COLORRANGE_8BIT / CIPPalette1K.COLORRANGE_1K);
        Int32 RB = R.Next(0, CIPPalette1K.COLORRANGE_1K);
        UInt16 WB = (UInt16)(RB * CIPPalette1K.COLORRANGE_8BIT / CIPPalette1K.COLORRANGE_1K);
        FPartsRed[CI] = (Byte)(WR);
        FPartsGreen[CI] = (Byte)(WG);
        FPartsBlue[CI] = (Byte)(WB);
      }
    }

  }
}
