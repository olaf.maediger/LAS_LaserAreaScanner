﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette
{

  public enum EPalette256
  {
    Greyscale,
    Rainbow,
    Spectrum,
    RGB8Bright,
    RGB8Light,
    RGB8,
    RGB32Bright,
    RGB32Light,
    RGB32,
    Random,
    UserDefined
  }

  public class CIPPalette256
  {
    static public String[] PALETTES =
    {
      "Greyscale",
      "Rainbow",
      "Spectrum",
      "RGB8Bright",
      "RGB8Light",
      "RGB8",
      "RGB32Bright",
      "RGB32Light",
      "RGB32",
      "Random",
      "UserDefined"
    };

    public const Int32 COLORRANGE_256 = 256;
    public const Int32 COLORRANGE_GDI = 256;
    public const Int32 COLORRANGE_8BIT = 256;
    // NC public const Int32 COLORRANGE_SHIFTPIXEL = 5;
    // NC public const Int32 COLORRANGE_SHIFTPALETTE = 3;
    //
    public const Int32 COUNTSECTION_MINIMUM = 2;
    public const Int32 COUNTSECTION_MAXIMUM = CIPPalette256.COLORRANGE_256 / 2;

    static public CPalette256Base CreatePalette(EPalette256 palette256)
    { // 1412171347
      switch (palette256)
      {
        case EPalette256.Rainbow:
          //return new CPalette256Rainbow(1);
          return new CPalette256Rainbow();
        case EPalette256.Spectrum:
          //return new CPalette256Spectrum(8);
          return new CPalette256Spectrum();
        case EPalette256.RGB8Bright:
          //return new CPalette256RgbBright(8);
          return new CPalette256Rgb8Bright();
        case EPalette256.RGB8Light:
          //return new CPalette256RgbLight(8);
          return new CPalette256Rgb8Light();
        case EPalette256.RGB8:
          //return new CPalette256Rgb(8);
          return new CPalette256Rgb8();
        case EPalette256.RGB32Bright:
          //return new CPalette256RgbBright(32);
          return new CPalette256Rgb32Bright();
        case EPalette256.RGB32Light:
          //return new CPalette256RgbLight(32);
          return new CPalette256Rgb32Light();
        case EPalette256.RGB32:
          //return new CPalette256Rgb(32);
          return new CPalette256Rgb32();
        case EPalette256.Random:
          return new CPalette256Random();
        case EPalette256.UserDefined:
          return new CPalette256UserDefined();
        default:
          return new CPalette256Greyscale();
      }
    }
    /* NC ????NC ????NC ????NC ????NC ????NC ????NC ????NC ????NC ????
    static public UInt16 GetSectionCount(EPalette256 palette)
    { // 1412171347
      switch (palette)
      {
        case EPalette256.Rainbow:
          return 1;
        case EPalette256.Spectrum:
          return 8;
        case EPalette256.RGB8Bright:
          return 8;
        case EPalette256.RGB8Light:
          return 8;
        case EPalette256.RGB8:
          return 8;
        case EPalette256.RGB32Bright:
          return 32;
        case EPalette256.RGB32Light:
          return 32;
        case EPalette256.RGB32:
          return 32;
        case EPalette256.Random:
          return 32;
        case EPalette256.UserDefined:
          return 1;
        default:
          return 8;
      }
    }NC ????NC ????NC ????NC ????NC ????NC ????NC ????*/

    static public EPalette256 TextToPalette(String text)
    { // 1412171347
      if (PALETTES[(int)EPalette256.Rainbow] == text)
      {
        return EPalette256.Rainbow;
      }
      if (PALETTES[(int)EPalette256.Spectrum] == text)
      {
        return EPalette256.Spectrum;
      }
      if (PALETTES[(int)EPalette256.RGB8Bright] == text)
      {
        return EPalette256.RGB8Bright;
      }
      if (PALETTES[(int)EPalette256.RGB8Light] == text)
      {
        return EPalette256.RGB8Light;
      }
      if (PALETTES[(int)EPalette256.RGB8] == text)
      {
        return EPalette256.RGB8;
      }
      if (PALETTES[(int)EPalette256.RGB32Bright] == text)
      {
        return EPalette256.RGB32Bright;
      }
      if (PALETTES[(int)EPalette256.RGB32Light] == text)
      {
        return EPalette256.RGB32Light;
      }
      if (PALETTES[(int)EPalette256.RGB32] == text)
      {
        return EPalette256.RGB32;
      }
      if (PALETTES[(int)EPalette256.Random] == text)
      {
        return EPalette256.Random;
      }
      if (PALETTES[(int)EPalette256.UserDefined] == text)
      {
        return EPalette256.UserDefined;
      }
      return EPalette256.Greyscale;
    }

    static public String PaletteToText(EPalette256 palette)
    { // 1412171347
      switch(palette)
      {
        case EPalette256.Greyscale:
          return PALETTES[(int)EPalette256.Greyscale];
        case EPalette256.Rainbow:
          return PALETTES[(int)EPalette256.Rainbow];
        case EPalette256.Spectrum:
          return PALETTES[(int)EPalette256.Spectrum];
        case EPalette256.RGB8Bright:
          return PALETTES[(int)EPalette256.RGB8Bright];
        case EPalette256.RGB8Light:
          return PALETTES[(int)EPalette256.RGB8Light];
        case EPalette256.RGB8:
          return PALETTES[(int)EPalette256.RGB8];
        case EPalette256.RGB32Bright:
          return PALETTES[(int)EPalette256.RGB32Bright];
        case EPalette256.RGB32Light:
          return PALETTES[(int)EPalette256.RGB32Light];
        case EPalette256.RGB32:
          return PALETTES[(int)EPalette256.RGB32];
        case EPalette256.Random:
          return PALETTES[(int)EPalette256.Random];
        case EPalette256.UserDefined:
          return PALETTES[(int)EPalette256.UserDefined];
      }
      return "";
    }

    public static CPalette256Base CreatePalette256FromMatrix(Byte[,] palette)
    {
      const Int32 INDEXBLUE = 2;
      const Int32 INDEXGREEN = 1;
      const Int32 INDEXRED = 0;
      try
      {
        Int32 PaletteHeight = CIPPalette256.COLORRANGE_256;
        CPalette256UserDefined Result = new CPalette256UserDefined();
        unsafe
        {
          for (Int32 RI = 0; RI < PaletteHeight; RI++)
          {
            Color C = Color.FromArgb(palette[RI, INDEXRED], palette[RI, INDEXGREEN], palette[RI, INDEXBLUE]);
            Result.SetColorEntry((UInt16)RI, C);
          }
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }


  }
}
