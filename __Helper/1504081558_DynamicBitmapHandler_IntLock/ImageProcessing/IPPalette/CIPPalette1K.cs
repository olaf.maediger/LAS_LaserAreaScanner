﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette
{

  public enum EPalette1K
  {
    Greyscale,
    Rainbow,
    Spectrum,
    RGB8Bright,
    RGB8Light,
    RGB8,
    RGB32Bright,
    RGB32Light,
    RGB32,
    Random,
    UserDefined
  }

  public class CIPPalette1K
  {
    static public String[] PALETTES =
    {
      "Greyscale",
      "Rainbow",
      "Spectrum",
      "RGB8Bright",
      "RGB8Light",
      "RGB8",
      "RGB32Bright",
      "RGB32Light",
      "RGB32",
      "Random", 
      "UserDefined"
    };

    public const Int32 COLORRANGE_1K = 1024;
    public const Int32 COLORRANGE_GDI = 8192;
    public const Int32 COLORRANGE_8BIT = 256;
    public const Int32 COLORRANGE_SHIFTPIXEL = 5;
    public const Int32 COLORRANGE_SHIFTPALETTE = 3;
    //
    public const Int32 COUNTSECTION_MINIMUM = 2;
    public const Int32 COUNTSECTION_MAXIMUM = CIPPalette1K.COLORRANGE_1K / 2;


    static public EPalette1K TextToPalette(String text)
    { // 1412171224
      if (PALETTES[(int)EPalette1K.Rainbow] == text)
      {
        return EPalette1K.Rainbow;
      }
      if (PALETTES[(int)EPalette1K.Spectrum] == text)
      {
        return EPalette1K.Spectrum;
      }
      if (PALETTES[(int)EPalette1K.RGB32Bright] == text)
      {
        return EPalette1K.RGB8Bright;
      }
      if (PALETTES[(int)EPalette1K.RGB32Light] == text)
      {
        return EPalette1K.RGB8Light;
      }
      if (PALETTES[(int)EPalette1K.RGB8] == text)
      {
        return EPalette1K.RGB8;
      }
      if (PALETTES[(int)EPalette1K.RGB32Bright] == text)
      {
        return EPalette1K.RGB32Bright;
      }
      if (PALETTES[(int)EPalette1K.RGB32Light] == text)
      {
        return EPalette1K.RGB32Light;
      }
      if (PALETTES[(int)EPalette1K.RGB32] == text)
      {
        return EPalette1K.RGB32;
      }
      if (PALETTES[(int)EPalette1K.Random] == text)
      {
        return EPalette1K.Random;
      }
      if (PALETTES[(int)EPalette1K.UserDefined] == text)
      {
        return EPalette1K.Random;
      }
      return EPalette1K.Greyscale;
    }

    static public String PaletteToText(EPalette1K palette)
    { // 1412171224
      switch (palette)
      {
        case EPalette1K.Greyscale:
          return PALETTES[(int)EPalette1K.Greyscale];
        case EPalette1K.Rainbow:
          return PALETTES[(int)EPalette1K.Rainbow];
        case EPalette1K.Spectrum:
          return PALETTES[(int)EPalette1K.Spectrum];
        case EPalette1K.RGB8Bright:
          return PALETTES[(int)EPalette1K.RGB8Bright];
        case EPalette1K.RGB8Light:
          return PALETTES[(int)EPalette1K.RGB8Light];
        case EPalette1K.RGB8:
          return PALETTES[(int)EPalette1K.RGB8];
        case EPalette1K.RGB32Bright:
          return PALETTES[(int)EPalette1K.RGB32Bright];
        case EPalette1K.RGB32Light:
          return PALETTES[(int)EPalette1K.RGB32Light];
        case EPalette1K.RGB32:
          return PALETTES[(int)EPalette1K.RGB32];
        case EPalette1K.Random:
          return PALETTES[(int)EPalette1K.Random];
        case EPalette1K.UserDefined:
          return PALETTES[(int)EPalette1K.UserDefined];
      }
      return "";
    }

    static public Boolean Exists(String namepalette)
    {
      EPalette1K[] Palettes = (EPalette1K[])Enum.GetValues(typeof(EPalette1K));
      foreach (EPalette1K Palette in Palettes)
      {
        if (namepalette == PALETTES[(int)Palette])
        {
          return true;
        }
      }
      return false;
    }

    static public CPalette1KBase CreatePalette(EPalette1K palette1k)
    { // 1412161518
      switch (palette1k)
      {
        case EPalette1K.Rainbow:
          return new CPalette1KRainbow(1);
        case EPalette1K.Spectrum:
          return new CPalette1KSpectrum(8);
        case EPalette1K.RGB8Bright:
          //return new CPalette1KRgbBright(8);
          return new CPalette1KRgb8Bright();
        case EPalette1K.RGB8Light:
          //return new CPalette1KRgbLight(8);
          return new CPalette1KRgb8Light();
        case EPalette1K.RGB8:
          //return new CPalette1KRgb(8);
          return new CPalette1KRgb8();
        case EPalette1K.RGB32Bright:
          //return new CPalette1KRgbBright(32);
          return new CPalette1KRgb32Bright();
        case EPalette1K.RGB32Light:
          //return new CPalette1KRgbLight(32);
          return new CPalette1KRgb32Light();
        case EPalette1K.RGB32:
          //return new CPalette1KRgb(32);
          return new CPalette1KRgb32();
        case EPalette1K.Random:
          //return new CPalette1KRandom(32);
          return new CPalette1KRandom();
        case EPalette1K.UserDefined:
          return new CPalette1KUserDefined();
        default:
          return new CPalette1KGreyscale();
      }
    }

    static public CPalette1KBase CreatePalette(String palette1k)
    { // 1412161518
      EPalette1K Palette1K = TextToPalette(palette1k);
      return CreatePalette(Palette1K);
    }

    static public UInt16 GetSectionCount(EPalette1K palette)
    {
      switch (palette)
      {
        case EPalette1K.Rainbow:
          return 1;
        case EPalette1K.Spectrum:
          return 8;
        case EPalette1K.RGB8Bright:
          return 8;
        case EPalette1K.RGB8Light:
          return 8;
        case EPalette1K.RGB8:
          return 8;
        case EPalette1K.RGB32Bright:
          return 32;
        case EPalette1K.RGB32Light:
          return 32;
        case EPalette1K.RGB32:
          return 32;
        case EPalette1K.Random:
          return 32;
        case EPalette1K.UserDefined:
          return 1;
        default:
          return 8;
      }
    }


    public static CPalette1KBase CreatePalette1KFromMatrix(Byte[,] palette)
    {
      const Int32 INDEXBLUE = 2;
      const Int32 INDEXGREEN = 1;
      const Int32 INDEXRED = 0;
      try
      {
        Int32 PaletteHeight = CIPPalette1K.COLORRANGE_1K;
        CPalette1KUserDefined Result = new CPalette1KUserDefined();
        unsafe
        {
          for (Int32 RI = 0; RI < PaletteHeight; RI++)
          {
            Color C = Color.FromArgb(palette[RI, INDEXRED], palette[RI, INDEXGREEN], palette[RI, INDEXBLUE]);
            Result.SetColorEntry((UInt16)RI, C);
          }
        }
        return Result;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }


  }
}
