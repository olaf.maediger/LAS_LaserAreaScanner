﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette
{
  public class CPalette1KUserDefined : CPalette1KBase
  {
    public CPalette1KUserDefined()
      : base()
    {
      Initialize();
    }

    override protected void Initialize()
    {
      EColorMode ColorMode = EColorMode.Red;
      Int32 FillIndex = 0;
      Int32 FillBorder = CIPPalette1K.COLORRANGE_8BIT / FCountSection;
      Byte BValue = CIPPalette1K.COLORRANGE_8BIT - 1;
      Byte Subtrahend = (Byte)(CIPPalette1K.COLORRANGE_8BIT / FCountSection / 4);
      for (Int32 CI = CIPPalette1K.COLORRANGE_1K - 1; 0 <= CI; CI--)
      {
        switch (ColorMode)
        {
          case EColorMode.Red:
            FPartsRed[CI] = BValue;
            FPartsGreen[CI] = (Byte)0x00;
            FPartsBlue[CI] = (Byte)0x00;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Green;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
          case EColorMode.Green:
            FPartsRed[CI] = (Byte)0x00;
            FPartsGreen[CI] = BValue;
            FPartsBlue[CI] = (Byte)0x00;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Blue;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
          case EColorMode.Blue:
            FPartsRed[CI] = (Byte)0x00;
            FPartsGreen[CI] = (Byte)0x00;
            FPartsBlue[CI] = BValue;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Red;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
        }
      }
    }

    public Boolean SetColorEntry(UInt16 index, Color color)
    {
      if ((0 <= index) && (index < 1023))
      {
        base.FPartsRed[index] = color.R;
        base.FPartsGreen[index] = color.G;
        base.FPartsBlue[index] = color.B;
        return true;
      }
      return false;
    }

    //
    //--------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------
    //
    public override EPalette1K GetPalette()
    {
      return EPalette1K.UserDefined;
    }

  }
}

