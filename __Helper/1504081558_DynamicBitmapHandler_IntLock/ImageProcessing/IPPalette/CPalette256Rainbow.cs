﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPImagePrimitives;
//
namespace IPPalette
{
  public class CPalette256Rainbow : CPalette256Base
  {
    private const Int32 BORDER_LOW = 4;
    private const Int32 BORDER_HIGH = 4;
    //
    //-------------------------------------------------------
    //  Section - Constructor
    //-------------------------------------------------------
    //
    public CPalette256Rainbow()
      : base()
    {
      Initialize();
    }
    //
    //-------------------------------------------------------
    //  Section - Build
    //-------------------------------------------------------
    //
    override protected void Initialize()
    {
      /*
      const Double SL = 0.5;
      const Double L = 0.5;
      for (Int32 CI = 0; CI < CIPPalette.COLORRANGE_256; CI++)
      {
        Color PE;
        Double H = (Double)CI / (Double)CIPPalette.COLORRANGE_256;
        CColorConversion.HSL2RGB(H, SL, L, out PE);
        FPartsRed[CI] = PE.R;
        FPartsGreen[CI] = PE.G;
        FPartsBlue[CI] = PE.B;
      }*/

      for (Int32 CI = 0; CI < BORDER_LOW; CI++)
      {
        FPartsRed[CI] = 0x00;
        FPartsGreen[CI] = 0x00;
        FPartsBlue[CI] = 0x00;
      }
      for (Int32 CI = BORDER_LOW; CI < CIPPalette256.COLORRANGE_256 - BORDER_HIGH; CI++)
      {
        Double QuadValue = 4 * (Double)CI / (Double)CIPPalette256.COLORRANGE_256;
        FPartsRed[CI] = (Byte)(255.0 * Math.Max(Math.Min(Math.Min(QuadValue - 1.5, -QuadValue + 4.5), 1), 0));
        FPartsGreen[CI] = (Byte)(255.0 * Math.Max(Math.Min(Math.Min(QuadValue - 0.5, -QuadValue + 3.5), 1), 0));
        FPartsBlue[CI] = (Byte)(255.0 * Math.Max(Math.Min(Math.Min(QuadValue + 0.5, -QuadValue + 2.5), 1), 0));
      }
      for (Int32 CI = CIPPalette256.COLORRANGE_256 - BORDER_HIGH; CI < CIPPalette256.COLORRANGE_256; CI++)
      {
        FPartsRed[CI] = 0xFF;
        FPartsGreen[CI] = 0xFF;
        FPartsBlue[CI] = 0xFF;
      }
    }
    //
    //-------------------------------------------------------
    //  Section - Property
    //-------------------------------------------------------
    //
    public override EPalette256 GetPalette()
    {
      return EPalette256.Rainbow;
    }

  }
}




