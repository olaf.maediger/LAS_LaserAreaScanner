﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IPPalette
{
  public abstract class CPalette1KRgb : CPalette1KBase
  {
    public CPalette1KRgb(Int32 countsection)
      : base(countsection)
    {
      Initialize();
    }

    override protected void Initialize()
    {
      EColorMode ColorMode = EColorMode.Red;
      Int32 FillIndex = 0;
      Int32 FillBorder = CIPPalette1K.COLORRANGE_8BIT / FCountSection;
      Byte BValue = CIPPalette1K.COLORRANGE_8BIT - 1;
      Byte Subtrahend = (Byte)(CIPPalette1K.COLORRANGE_8BIT / FCountSection / 4);
      for (Int32 CI = CIPPalette1K.COLORRANGE_1K - 1; 0 <= CI; CI--)
      {
        switch (ColorMode)
        {
          case EColorMode.Red:
            FPartsRed[CI] = BValue;
            FPartsGreen[CI] = (Byte)0x00;
            FPartsBlue[CI] = (Byte)0x00;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Green;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
          case EColorMode.Green:
            FPartsRed[CI] = (Byte)0x00;
            FPartsGreen[CI] = BValue;
            FPartsBlue[CI] = (Byte)0x00;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Blue;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
          case EColorMode.Blue:
            FPartsRed[CI] = (Byte)0x00;
            FPartsGreen[CI] = (Byte)0x00;
            FPartsBlue[CI] = BValue;
            if (FillBorder == FillIndex)
            {
              ColorMode = EColorMode.Red;
              BValue -= Subtrahend;
              FillIndex = 0;
            }
            else
            {
              FillIndex++;
            }
            break;
        }
      }
    }
  }

  public class CPalette1KRgb8 : CPalette1KRgb
  {
    //
    //--------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------
    //
    public CPalette1KRgb8()
      : base(8)
    {
    }
    //
    //--------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------
    //
    public override EPalette1K GetPalette()
    {
      return EPalette1K.RGB8;
    }
  }

  public class CPalette1KRgb32 : CPalette1KRgb
  {
    //
    //--------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------
    //
    public CPalette1KRgb32()
      : base(32)
    {
    }
    //
    //--------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------
    //
    public override EPalette1K GetPalette()
    {
      return EPalette1K.RGB32;
    }
  }

}
