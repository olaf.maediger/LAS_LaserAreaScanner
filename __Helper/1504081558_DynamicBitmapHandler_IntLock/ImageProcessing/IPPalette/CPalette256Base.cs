﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
//
namespace IPPalette
{
  public abstract class CPalette256Base : CPaletteBase
  {
    static public Color COLOR_ZERO = Color.FromArgb(0);
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    // 1412081521
    protected Int32 FCountSection = CIPPalette256.COUNTSECTION_MINIMUM;
    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CPalette256Base(Int32 countsection)
    { // 1412081521
      FPartsAlpha = new Byte[CIPPalette256.COLORRANGE_256];
      for (Int32 AI = 0; AI < CIPPalette256.COLORRANGE_256; AI++)
      {
        FPartsAlpha[AI] = INIT_ALPHA;
      }
      FPartsRed = new Byte[CIPPalette256.COLORRANGE_256];
      FPartsGreen = new Byte[CIPPalette256.COLORRANGE_256];
      FPartsBlue = new Byte[CIPPalette256.COLORRANGE_256];
      FCountSection = Math.Max(CIPPalette256.COUNTSECTION_MINIMUM, countsection);
      FCountSection = Math.Min(CIPPalette256.COUNTSECTION_MAXIMUM, FCountSection);
    }

    public CPalette256Base()
    { // 1412171446
      FPartsAlpha = new Byte[CIPPalette256.COLORRANGE_256];
      for (Int32 AI = 0; AI < CIPPalette256.COLORRANGE_256; AI++)
      {
        FPartsAlpha[AI] = INIT_ALPHA;
      }
      FPartsRed = new Byte[CIPPalette256.COLORRANGE_256];
      FPartsGreen = new Byte[CIPPalette256.COLORRANGE_256];
      FPartsBlue = new Byte[CIPPalette256.COLORRANGE_256];
      FCountSection = Math.Max(CIPPalette256.COUNTSECTION_MINIMUM, 1);
      FCountSection = Math.Min(CIPPalette256.COUNTSECTION_MAXIMUM, FCountSection);
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //  1412171404
    public abstract EPalette256 GetPalette();

    public String GetName()
    {
      return CIPPalette256.PaletteToText(GetPalette());
    }    
    
    // 1502161653
    public override Byte GetPartsAlpha(UInt16 index)
    {
      return this.FPartsAlpha[index];
    }
    public override Byte GetPartsRed(UInt16 index)
    {
      return this.FPartsRed[index];
    }
    public override Byte GetPartsGreen(UInt16 index)
    {
      return this.FPartsGreen[index];
    }
    public override Byte GetPartsBlue(UInt16 index)
    {
      return this.FPartsBlue[index];
    }
    public override Color GetColor(UInt16 index)
    {
      Byte A = this.FPartsAlpha[index];
      Byte R = this.FPartsRed[index];
      Byte G = this.FPartsGreen[index];
      Byte B = this.FPartsBlue[index];
      return Color.FromArgb(A, R, G, B);
    }


    //  Helper
    protected abstract void Initialize();

    public override UInt16 FindPaletteIndex(Byte red, Byte green, Byte blue)
    { // 1504081435
      for (Int32 CI = 0; CI < CIPPalette256.COLORRANGE_256; CI++)
      {
        if (red == FPartsRed[CI])
        {
          if (green == FPartsGreen[CI])
          {
            if (blue == FPartsBlue[CI])
            {
              return (Byte)CI;
            }
          }
        }
      }
      return 0;
    }

    public override UInt16 FindPaletteIndex(Byte alpha, Byte red, Byte green, Byte blue)
    { // 1504081435
      for (Int32 CI = 0; CI < CIPPalette256.COLORRANGE_256; CI++)
      {
        if (alpha == FPartsAlpha[CI])
        {
          if (red == FPartsRed[CI])
          {
            if (green == FPartsGreen[CI])
            {
              if (blue == FPartsBlue[CI])
              {
                return (Byte)CI;
              }
            }
          }
        }
      }
      return 0;
    }


  }
}

