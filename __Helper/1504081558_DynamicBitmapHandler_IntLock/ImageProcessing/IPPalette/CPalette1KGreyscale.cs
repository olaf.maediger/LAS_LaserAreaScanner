﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette
{
  public class CPalette1KGreyscale : CPalette1KBase
  {
    //
    //-------------------------------------------------------
    //  Section - Constructor
    //-------------------------------------------------------
    //
    public CPalette1KGreyscale()
      : base()
    {
      Initialize();
    }
    //
    //-------------------------------------------------------
    //  Section - Build
    //-------------------------------------------------------
    //
    override protected void Initialize()
    {
      for (Int32 CI = 0; CI < CIPPalette1K.COLORRANGE_1K; CI++)
      {
        UInt16 WValue = (UInt16)(CI * CIPPalette1K.COLORRANGE_8BIT / CIPPalette1K.COLORRANGE_1K);
        FPartsRed[CI] = (Byte)(1.00f * WValue);
        FPartsGreen[CI] = (Byte)(1.00f * WValue);
        FPartsBlue[CI] = (Byte)(1.00f * WValue);
      }
    }
    //
    //-------------------------------------------------------
    //  Section - Property
    //-------------------------------------------------------
    //
    public override EPalette1K GetPalette()
    {
      return EPalette1K.Greyscale;
    }


  }
}
