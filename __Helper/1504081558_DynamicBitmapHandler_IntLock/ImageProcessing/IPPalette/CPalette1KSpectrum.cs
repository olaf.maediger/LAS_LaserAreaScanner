﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPalette
{
  public class CPalette1KSpectrum : CPalette1KBase
  {
    //
    //-------------------------------------------------------
    //  Section - Constructor
    //-------------------------------------------------------
    //
    public CPalette1KSpectrum(Int32 countsection)
      : base(countsection)
    {
      Initialize();
    }
    //
    //-------------------------------------------------------
    //  Section - Build
    //-------------------------------------------------------
    //
    override protected void Initialize()
    {
      //for (Int32 CI = 0; CI < CIPPalette.COLORRANGE_1K; CI++)
      //UInt16 WValue = (UInt16)(CI * CIPPalette.COLORRANGE_8BIT / CIPPalette.COLORRANGE_1K);
      // Red
      FPartsRed[0xFF] = 137;
      FPartsRed[0xFE] = 137;
      FPartsRed[0xFD] = 137;
      FPartsRed[0xFC] = 137;
      FPartsRed[0xFB] = 143;
      FPartsRed[0xFA] = 143;
      FPartsRed[0xF9] = 154;
      FPartsRed[0xF8] = 154;
      FPartsRed[0xF7] = 160;
      FPartsRed[0xF6] = 160;
      FPartsRed[0xF5] = 165;
      FPartsRed[0xF4] = 165;
      FPartsRed[0xF3] = 171;
      FPartsRed[0xF2] = 171;
      FPartsRed[0xF1] = 176;
      FPartsRed[0xF0] = 176;
      //                   
      FPartsRed[0xEF] = 182;
      FPartsRed[0xEE] = 182;
      FPartsRed[0xED] = 187;
      FPartsRed[0xEC] = 187;
      FPartsRed[0xEB] = 193;
      FPartsRed[0xEA] = 193;
      FPartsRed[0xE9] = 198;
      FPartsRed[0xE8] = 198;
      FPartsRed[0xE7] = 204;
      FPartsRed[0xE6] = 204;
      FPartsRed[0xE5] = 209;
      FPartsRed[0xE4] = 209;
      FPartsRed[0xE3] = 215;
      FPartsRed[0xE2] = 215;
      FPartsRed[0xE1] = 220;
      FPartsRed[0xE0] = 220;
      //                   
      FPartsRed[0xDF] = 226;
      FPartsRed[0xDE] = 226;
      FPartsRed[0xDD] = 231;
      FPartsRed[0xDC] = 231;
      FPartsRed[0xDB] = 237;
      FPartsRed[0xDA] = 237;
      FPartsRed[0xD9] = 242;
      FPartsRed[0xD8] = 242;
      FPartsRed[0xD7] = 248;
      FPartsRed[0xD6] = 248;
      FPartsRed[0xD5] = 254;
      FPartsRed[0xD4] = 254;
      FPartsRed[0xD3] = 255;
      FPartsRed[0xD2] = 255;
      FPartsRed[0xD1] = 255;
      FPartsRed[0xD0] = 255;
      //                   
      FPartsRed[0xCF] = 255;
      FPartsRed[0xCE] = 255;
      FPartsRed[0xCD] = 255;
      FPartsRed[0xCC] = 255;
      FPartsRed[0xCB] = 255;
      FPartsRed[0xCA] = 255;
      FPartsRed[0xC9] = 255;
      FPartsRed[0xC8] = 255;
      FPartsRed[0xC7] = 255;
      FPartsRed[0xC6] = 255;
      FPartsRed[0xC5] = 255;
      FPartsRed[0xC4] = 255;
      FPartsRed[0xC3] = 255;
      FPartsRed[0xC2] = 255;
      FPartsRed[0xC1] = 255;
      FPartsRed[0xC0] = 255;
      //                   
      FPartsRed[0xBF] = 255;
      FPartsRed[0xBE] = 255;
      FPartsRed[0xBD] = 255;
      FPartsRed[0xBC] = 255;
      FPartsRed[0xBB] = 255;
      FPartsRed[0xBA] = 255;
      FPartsRed[0xB9] = 255;
      FPartsRed[0xB8] = 255;
      FPartsRed[0xB7] = 255;
      FPartsRed[0xB6] = 255;
      FPartsRed[0xB5] = 255;
      FPartsRed[0xB4] = 255;
      FPartsRed[0xB3] = 255;
      FPartsRed[0xB2] = 255;
      FPartsRed[0xB1] = 255;
      FPartsRed[0xB0] = 255;
      //                   
      FPartsRed[0xAF] = 255;
      FPartsRed[0xAE] = 255;
      FPartsRed[0xAD] = 255;
      FPartsRed[0xAC] = 255;
      FPartsRed[0xAB] = 255;
      FPartsRed[0xAA] = 255;
      FPartsRed[0xA9] = 255;
      FPartsRed[0xA8] = 255;
      FPartsRed[0xA7] = 255;
      FPartsRed[0xA6] = 255;
      FPartsRed[0xA5] = 255;
      FPartsRed[0xA4] = 255;
      FPartsRed[0xA3] = 252;
      FPartsRed[0xA2] = 252;
      FPartsRed[0xA1] = 243;
      FPartsRed[0xA0] = 243;
      //                   
      FPartsRed[0x9F] = 235;
      FPartsRed[0x9E] = 235;
      FPartsRed[0x9D] = 227;
      FPartsRed[0x9C] = 227;
      FPartsRed[0x9B] = 219;
      FPartsRed[0x9A] = 219;
      FPartsRed[0x99] = 203;
      FPartsRed[0x98] = 203;
      FPartsRed[0x97] = 195;
      FPartsRed[0x96] = 195;
      FPartsRed[0x95] = 187;
      FPartsRed[0x94] = 187;
      FPartsRed[0x93] = 179;
      FPartsRed[0x92] = 179;
      FPartsRed[0x91] = 163;
      FPartsRed[0x90] = 163;
      //                   
      FPartsRed[0x8F] = 152;
      FPartsRed[0x8E] = 147;
      FPartsRed[0x8D] = 139;
      FPartsRed[0x8C] = 135;
      FPartsRed[0x8B] = 131;
      FPartsRed[0x8A] = 128;
      FPartsRed[0x89] = 123;
      FPartsRed[0x88] = 118;
      FPartsRed[0x87] = 115;
      FPartsRed[0x86] = 112;
      FPartsRed[0x85] = 107;
      FPartsRed[0x84] = 103;
      FPartsRed[0x83] = 99;
      FPartsRed[0x82] = 96;
      FPartsRed[0x81] = 91;
      FPartsRed[0x80] = 98;
      //                  
      FPartsRed[0x7F] = 83;
      FPartsRed[0x7E] = 80;
      FPartsRed[0x7D] = 75;
      FPartsRed[0x7C] = 73;
      FPartsRed[0x7B] = 72;
      FPartsRed[0x7A] = 71;
      FPartsRed[0x79] = 69;
      FPartsRed[0x78] = 65;
      FPartsRed[0x77] = 62;
      FPartsRed[0x76] = 60;
      FPartsRed[0x75] = 58;
      FPartsRed[0x74] = 57;
      FPartsRed[0x73] = 56;
      FPartsRed[0x72] = 55;
      FPartsRed[0x71] = 53;
      FPartsRed[0x70] = 52;
      //                  
      FPartsRed[0x6F] = 49;
      FPartsRed[0x6E] = 48;
      FPartsRed[0x6D] = 47;
      FPartsRed[0x6C] = 45;
      FPartsRed[0x6B] = 43;
      FPartsRed[0x6A] = 41;
      FPartsRed[0x69] = 39;
      FPartsRed[0x68] = 38;
      FPartsRed[0x67] = 37;
      FPartsRed[0x66] = 35;
      FPartsRed[0x65] = 33;
      FPartsRed[0x64] = 31;
      FPartsRed[0x63] = 28;
      FPartsRed[0x62] = 26;
      FPartsRed[0x61] = 24;
      FPartsRed[0x60] = 23;
      //                  
      FPartsRed[0x5F] = 21;
      FPartsRed[0x5E] = 20;
      FPartsRed[0x5D] = 19;
      FPartsRed[0x5C] = 17;
      FPartsRed[0x5B] = 16;
      FPartsRed[0x5A] = 14;
      FPartsRed[0x59] = 12;
      FPartsRed[0x58] = 10;
      FPartsRed[0x57] = 8;
      FPartsRed[0x56] = 6;
      FPartsRed[0x55] = 4;
      FPartsRed[0x54] = 3;
      FPartsRed[0x53] = 1;
      FPartsRed[0x52] = 0;
      FPartsRed[0x51] = 1;
      FPartsRed[0x50] = 1;
      //                  
      FPartsRed[0x4F] = 1;
      FPartsRed[0x4E] = 1;
      FPartsRed[0x4D] = 1;
      FPartsRed[0x4C] = 1;
      FPartsRed[0x4B] = 2;
      FPartsRed[0x4A] = 2;
      FPartsRed[0x49] = 2;
      FPartsRed[0x48] = 2;
      FPartsRed[0x47] = 2;
      FPartsRed[0x46] = 2;
      FPartsRed[0x45] = 3;
      FPartsRed[0x44] = 3;
      FPartsRed[0x43] = 3;
      FPartsRed[0x42] = 3;
      FPartsRed[0x41] = 3;
      FPartsRed[0x40] = 3;
      //                  
      FPartsRed[0x3F] = 4;
      FPartsRed[0x3E] = 4;
      FPartsRed[0x3D] = 4;
      FPartsRed[0x3C] = 4;
      FPartsRed[0x3B] = 4;
      FPartsRed[0x3A] = 4;
      FPartsRed[0x39] = 5;
      FPartsRed[0x38] = 5;
      FPartsRed[0x37] = 5;
      FPartsRed[0x36] = 5;
      FPartsRed[0x35] = 5;
      FPartsRed[0x34] = 5;
      FPartsRed[0x33] = 6;
      FPartsRed[0x32] = 6;
      FPartsRed[0x31] = 6;
      FPartsRed[0x30] = 6;
      //                 
      FPartsRed[0x2F] = 6;
      FPartsRed[0x2E] = 6;
      FPartsRed[0x2D] = 7;
      FPartsRed[0x2C] = 7;
      FPartsRed[0x2B] = 7;
      FPartsRed[0x2A] = 7;
      FPartsRed[0x29] = 7;
      FPartsRed[0x28] = 7;
      FPartsRed[0x27] = 8;
      FPartsRed[0x26] = 8;
      FPartsRed[0x25] = 8;
      FPartsRed[0x24] = 8;
      FPartsRed[0x23] = 8;
      FPartsRed[0x22] = 8;
      FPartsRed[0x21] = 8;
      FPartsRed[0x20] = 8;
      //                 
      FPartsRed[0x1F] = 8;
      FPartsRed[0x1E] = 8;
      FPartsRed[0x1D] = 8;
      FPartsRed[0x1C] = 8;
      FPartsRed[0x1B] = 8;
      FPartsRed[0x1A] = 8;
      FPartsRed[0x19] = 8;
      FPartsRed[0x18] = 8;
      FPartsRed[0x17] = 8;
      FPartsRed[0x16] = 8;
      FPartsRed[0x15] = 9;
      FPartsRed[0x14] = 9;
      FPartsRed[0x13] = 9;
      FPartsRed[0x12] = 9;
      FPartsRed[0x11] = 9;
      FPartsRed[0x10] = 9;
      //                 
      FPartsRed[0x0F] = 10;
      FPartsRed[0x0E] = 10;
      FPartsRed[0x0D] = 10;
      FPartsRed[0x0C] = 10;
      FPartsRed[0x0B] = 10;
      FPartsRed[0x0A] = 10;
      FPartsRed[0x09] = 10;
      FPartsRed[0x08] = 10;
      FPartsRed[0x07] = 10;
      FPartsRed[0x06] = 10;
      FPartsRed[0x05] = 10;
      FPartsRed[0x04] = 10;
      FPartsRed[0x03] = 10;
      FPartsRed[0x02] = 10;
      FPartsRed[0x01] = 10;
      FPartsRed[0x00] = 10;
      //


      // Green
      FPartsGreen[0xFF] = 0; 
      FPartsGreen[0xFE] = 2; 
      FPartsGreen[0xFD] = 4; 
      FPartsGreen[0xFC] = 6; 
      FPartsGreen[0xFB] = 12; 
      FPartsGreen[0xFA] = 14; 
      FPartsGreen[0xF9] = 16; 
      FPartsGreen[0xF8] = 18; 
      FPartsGreen[0xF7] = 22; 
      FPartsGreen[0xF6] = 24; 
      FPartsGreen[0xF5] = 26; 
      FPartsGreen[0xF4] = 28; 
      FPartsGreen[0xF3] = 30; 
      FPartsGreen[0xF2] = 32; 
      FPartsGreen[0xF1] = 34; 
      FPartsGreen[0xF0] = 36; 
      //                
      FPartsGreen[0xEF] = 38; 
      FPartsGreen[0xEE] = 40; 
      FPartsGreen[0xED] = 42; 
      FPartsGreen[0xEC] = 44; 
      FPartsGreen[0xEB] = 46; 
      FPartsGreen[0xEA] = 48; 
      FPartsGreen[0xE9] = 50; 
      FPartsGreen[0xE8] = 52; 
      FPartsGreen[0xE7] = 54; 
      FPartsGreen[0xE6] = 56; 
      FPartsGreen[0xE5] = 58; 
      FPartsGreen[0xE4] = 60; 
      FPartsGreen[0xE3] = 62; 
      FPartsGreen[0xE2] = 64; 
      FPartsGreen[0xE1] = 66; 
      FPartsGreen[0xE0] = 68; 
      //                
      FPartsGreen[0xDF] = 70; 
      FPartsGreen[0xDE] = 72; 
      FPartsGreen[0xDD] = 74; 
      FPartsGreen[0xDC] = 76; 
      FPartsGreen[0xDB] = 78; 
      FPartsGreen[0xDA] = 80; 
      FPartsGreen[0xD9] = 82; 
      FPartsGreen[0xD8] = 84; 
      FPartsGreen[0xD7] = 86; 
      FPartsGreen[0xD6] = 88; 
      FPartsGreen[0xD5] = 90; 
      FPartsGreen[0xD4] = 92; 
      FPartsGreen[0xD3] = 94; 
      FPartsGreen[0xD2] = 96; 
      FPartsGreen[0xD1] = 98; 
      FPartsGreen[0xD0] = 99; 
      //                
      FPartsGreen[0xCF] = 102; 
      FPartsGreen[0xCE] = 104; 
      FPartsGreen[0xCD] = 107; 
      FPartsGreen[0xCC] = 109; 
      FPartsGreen[0xCB] = 111; 
      FPartsGreen[0xCA] = 113; 
      FPartsGreen[0xC9] = 115; 
      FPartsGreen[0xC8] = 118; 
      FPartsGreen[0xC7] = 121; 
      FPartsGreen[0xC6] = 123; 
      FPartsGreen[0xC5] = 125; 
      FPartsGreen[0xC4] = 127; 
      FPartsGreen[0xC3] = 131; 
      FPartsGreen[0xC2] = 133; 
      FPartsGreen[0xC1] = 135; 
      FPartsGreen[0xC0] = 137; 
      //                
      FPartsGreen[0xBF] = 140; 
      FPartsGreen[0xBE] = 142; 
      FPartsGreen[0xBD] = 144; 
      FPartsGreen[0xBC] = 146; 
      FPartsGreen[0xBB] = 148; 
      FPartsGreen[0xBA] = 151; 
      FPartsGreen[0xB9] = 153; 
      FPartsGreen[0xB8] = 156; 
      FPartsGreen[0xB7] = 159; 
      FPartsGreen[0xB6] = 162; 
      FPartsGreen[0xB5] = 165; 
      FPartsGreen[0xB4] = 168; 
      FPartsGreen[0xB3] = 170; 
      FPartsGreen[0xB2] = 172; 
      FPartsGreen[0xB1] = 174; 
      FPartsGreen[0xB0] = 176; 
      //                
      FPartsGreen[0xAF] = 178; 
      FPartsGreen[0xAE] = 179; 
      FPartsGreen[0xAD] = 182; 
      FPartsGreen[0xAC] = 184; 
      FPartsGreen[0xAB] = 188; 
      FPartsGreen[0xAA] = 188; 
      FPartsGreen[0xA9] = 191; 
      FPartsGreen[0xA8] = 193; 
      FPartsGreen[0xA7] = 196; 
      FPartsGreen[0xA6] = 198; 
      FPartsGreen[0xA5] = 201; 
      FPartsGreen[0xA4] = 203; 
      FPartsGreen[0xA3] = 205; 
      FPartsGreen[0xA2] = 207; 
      FPartsGreen[0xA1] = 208; 
      FPartsGreen[0xA0] = 209;
      //                
      FPartsGreen[0x9F] = 209; 
      FPartsGreen[0x9E] = 210; 
      FPartsGreen[0x9D] = 211; 
      FPartsGreen[0x9C] = 211; 
      FPartsGreen[0x9B] = 212; 
      FPartsGreen[0x9A] = 213; 
      FPartsGreen[0x99] = 215; 
      FPartsGreen[0x98] = 216; 
      FPartsGreen[0x97] = 217; 
      FPartsGreen[0x96] = 217; 
      FPartsGreen[0x95] = 218; 
      FPartsGreen[0x94] = 219; 
      FPartsGreen[0x93] = 220; 
      FPartsGreen[0x92] = 221; 
      FPartsGreen[0x91] = 222; 
      FPartsGreen[0x90] = 223; 
      //                
      FPartsGreen[0x8F] = 224; 
      FPartsGreen[0x8E] = 225; 
      FPartsGreen[0x8D] = 226; 
      FPartsGreen[0x8C] = 227; 
      FPartsGreen[0x8B] = 228; 
      FPartsGreen[0x8A] = 229; 
      FPartsGreen[0x89] = 230; 
      FPartsGreen[0x88] = 231; 
      FPartsGreen[0x87] = 232; 
      FPartsGreen[0x86] = 232; 
      FPartsGreen[0x85] = 233; 
      FPartsGreen[0x84] = 233; 
      FPartsGreen[0x83] = 234; 
      FPartsGreen[0x82] = 235; 
      FPartsGreen[0x81] = 236; 
      FPartsGreen[0x80] = 237; 
      //                
      FPartsGreen[0x7F] = 238; 
      FPartsGreen[0x7E] = 239; 
      FPartsGreen[0x7D] = 240; 
      FPartsGreen[0x7C] = 240; 
      FPartsGreen[0x7B] = 240; 
      FPartsGreen[0x7A] = 240; 
      FPartsGreen[0x79] = 241; 
      FPartsGreen[0x78] = 241; 
      FPartsGreen[0x77] = 242; 
      FPartsGreen[0x76] = 242; 
      FPartsGreen[0x75] = 243; 
      FPartsGreen[0x74] = 243; 
      FPartsGreen[0x73] = 244; 
      FPartsGreen[0x72] = 244; 
      FPartsGreen[0x71] = 245; 
      FPartsGreen[0x70] = 245; 
      //                
      FPartsGreen[0x6F] = 245; 
      FPartsGreen[0x6E] = 245; 
      FPartsGreen[0x6D] = 246; 
      FPartsGreen[0x6C] = 246; 
      FPartsGreen[0x6B] = 247; 
      FPartsGreen[0x6A] = 247; 
      FPartsGreen[0x69] = 247; 
      FPartsGreen[0x68] = 247; 
      FPartsGreen[0x67] = 248; 
      FPartsGreen[0x66] = 248; 
      FPartsGreen[0x65] = 249; 
      FPartsGreen[0x64] = 249; 
      FPartsGreen[0x63] = 250; 
      FPartsGreen[0x62] = 250; 
      FPartsGreen[0x61] = 250; 
      FPartsGreen[0x60] = 250; 
      //                
      FPartsGreen[0x5F] = 251; 
      FPartsGreen[0x5E] = 251; 
      FPartsGreen[0x5D] = 252; 
      FPartsGreen[0x5C] = 252; 
      FPartsGreen[0x5B] = 252; 
      FPartsGreen[0x5A] = 252; 
      FPartsGreen[0x59] = 253; 
      FPartsGreen[0x58] = 253; 
      FPartsGreen[0x57] = 254; 
      FPartsGreen[0x56] = 254; 
      FPartsGreen[0x55] = 255; 
      FPartsGreen[0x54] = 255; 
      FPartsGreen[0x53] = 255; 
      FPartsGreen[0x52] = 255; 
      FPartsGreen[0x51] = 249; 
      FPartsGreen[0x50] = 246; 
      //                
      FPartsGreen[0x4F] = 234; 
      FPartsGreen[0x4E] = 230; 
      FPartsGreen[0x4D] = 222; 
      FPartsGreen[0x4C] = 220; 
      FPartsGreen[0x4B] = 210; 
      FPartsGreen[0x4A] = 204; 
      FPartsGreen[0x49] = 198; 
      FPartsGreen[0x48] = 194; 
      FPartsGreen[0x47] = 186; 
      FPartsGreen[0x46] = 180; 
      FPartsGreen[0x45] = 174; 
      FPartsGreen[0x44] = 170; 
      FPartsGreen[0x43] = 162; 
      FPartsGreen[0x42] = 156; 
      FPartsGreen[0x41] = 150; 
      FPartsGreen[0x40] = 145; 
      //                
      FPartsGreen[0x3F] = 139; 
      FPartsGreen[0x3E] = 130; 
      FPartsGreen[0x3D] = 127; 
      FPartsGreen[0x3C] = 120; 
      FPartsGreen[0x3B] = 115; 
      FPartsGreen[0x3A] = 110; 
      FPartsGreen[0x39] = 103; 
      FPartsGreen[0x38] = 99; 
      FPartsGreen[0x37] = 91; 
      FPartsGreen[0x36] = 85; 
      FPartsGreen[0x35] = 79; 
      FPartsGreen[0x34] = 73; 
      FPartsGreen[0x33] = 67; 
      FPartsGreen[0x32] = 60; 
      FPartsGreen[0x31] = 55; 
      FPartsGreen[0x30] = 50; 
      //                
      FPartsGreen[0x2F] = 47; 
      FPartsGreen[0x2E] = 43; 
      FPartsGreen[0x2D] = 39; 
      FPartsGreen[0x2C] = 31; 
      FPartsGreen[0x2B] = 25; 
      FPartsGreen[0x2A] = 19; 
      FPartsGreen[0x29] = 12; 
      FPartsGreen[0x28] = 7; 
      FPartsGreen[0x27] = 6; 
      FPartsGreen[0x26] = 5; 
      FPartsGreen[0x25] = 4; 
      FPartsGreen[0x24] = 3; 
      FPartsGreen[0x23] = 2; 
      FPartsGreen[0x22] = 1; 
      FPartsGreen[0x21] = 0; 
      FPartsGreen[0x20] = 0; 
      //                
      FPartsGreen[0x1F] = 0; 
      FPartsGreen[0x1E] = 0; 
      FPartsGreen[0x1D] = 0; 
      FPartsGreen[0x1C] = 0; 
      FPartsGreen[0x1B] = 0; 
      FPartsGreen[0x1A] = 0; 
      FPartsGreen[0x19] = 0; 
      FPartsGreen[0x18] = 0; 
      FPartsGreen[0x17] = 0; 
      FPartsGreen[0x16] = 0; 
      FPartsGreen[0x15] = 0; 
      FPartsGreen[0x14] = 0; 
      FPartsGreen[0x13] = 0; 
      FPartsGreen[0x12] = 0; 
      FPartsGreen[0x11] = 0; 
      FPartsGreen[0x10] = 0; 
      //                
      FPartsGreen[0x0F] = 0; 
      FPartsGreen[0x0E] = 0; 
      FPartsGreen[0x0D] = 0; 
      FPartsGreen[0x0C] = 0; 
      FPartsGreen[0x0B] = 0; 
      FPartsGreen[0x0A] = 0; 
      FPartsGreen[0x09] = 0; 
      FPartsGreen[0x08] = 0; 
      FPartsGreen[0x07] = 0; 
      FPartsGreen[0x06] = 0; 
      FPartsGreen[0x05] = 0; 
      FPartsGreen[0x04] = 0; 
      FPartsGreen[0x03] = 0; 
      FPartsGreen[0x02] = 0; 
      FPartsGreen[0x01] = 0; 
      FPartsGreen[0x00] = 0; 
      //


      // Blue
      FPartsBlue[0xFF] = 0; 
      FPartsBlue[0xFE] = 0; 
      FPartsBlue[0xFD] = 0; 
      FPartsBlue[0xFC] = 0; 
      FPartsBlue[0xFB] = 0; 
      FPartsBlue[0xFA] = 0; 
      FPartsBlue[0xF9] = 2; 
      FPartsBlue[0xF8] = 2; 
      FPartsBlue[0xF7] = 3; 
      FPartsBlue[0xF6] = 3; 
      FPartsBlue[0xF5] = 4; 
      FPartsBlue[0xF4] = 4; 
      FPartsBlue[0xF3] = 5; 
      FPartsBlue[0xF2] = 5; 
      FPartsBlue[0xF1] = 5; 
      FPartsBlue[0xF0] = 5; 
      //                
      FPartsBlue[0xEF] = 6; 
      FPartsBlue[0xEE] = 6; 
      FPartsBlue[0xED] = 7; 
      FPartsBlue[0xEC] = 7; 
      FPartsBlue[0xEB] = 8; 
      FPartsBlue[0xEA] = 8; 
      FPartsBlue[0xE9] = 9; 
      FPartsBlue[0xE8] = 9; 
      FPartsBlue[0xE7] = 10; 
      FPartsBlue[0xE6] = 10; 
      FPartsBlue[0xE5] = 10; 
      FPartsBlue[0xE4] = 10; 
      FPartsBlue[0xE3] = 11; 
      FPartsBlue[0xE2] = 11; 
      FPartsBlue[0xE1] = 12; 
      FPartsBlue[0xE0] = 12; 
      //                
      FPartsBlue[0xDF] = 13; 
      FPartsBlue[0xDE] = 13; 
      FPartsBlue[0xDD] = 14; 
      FPartsBlue[0xDC] = 14; 
      FPartsBlue[0xDB] = 15; 
      FPartsBlue[0xDA] = 15; 
      FPartsBlue[0xD9] = 16; 
      FPartsBlue[0xD8] = 16; 
      FPartsBlue[0xD7] = 16; 
      FPartsBlue[0xD6] = 16; 
      FPartsBlue[0xD5] = 17; 
      FPartsBlue[0xD4] = 17; 
      FPartsBlue[0xD3] = 17; 
      FPartsBlue[0xD2] = 17; 
      FPartsBlue[0xD1] = 16; 
      FPartsBlue[0xD0] = 16; 
      //                
      FPartsBlue[0xCF] = 16; 
      FPartsBlue[0xCE] = 16; 
      FPartsBlue[0xCD] = 15; 
      FPartsBlue[0xCC] = 15; 
      FPartsBlue[0xCB] = 14; 
      FPartsBlue[0xCA] = 14; 
      FPartsBlue[0xC9] = 13; 
      FPartsBlue[0xC8] = 13; 
      FPartsBlue[0xC7] = 13; 
      FPartsBlue[0xC6] = 13; 
      FPartsBlue[0xC5] = 12; 
      FPartsBlue[0xC4] = 12; 
      FPartsBlue[0xC3] = 11; 
      FPartsBlue[0xC2] = 11; 
      FPartsBlue[0xC1] = 10; 
      FPartsBlue[0xC0] = 10; 
      //                
      FPartsBlue[0xBF] = 10; 
      FPartsBlue[0xBE] = 10; 
      FPartsBlue[0xBD] = 9; 
      FPartsBlue[0xBC] = 9; 
      FPartsBlue[0xBB] = 8; 
      FPartsBlue[0xBA] = 8; 
      FPartsBlue[0xB9] = 7; 
      FPartsBlue[0xB8] = 7; 
      FPartsBlue[0xB7] = 7; 
      FPartsBlue[0xB6] = 7; 
      FPartsBlue[0xB5] = 6; 
      FPartsBlue[0xB4] = 6; 
      FPartsBlue[0xB3] = 5; 
      FPartsBlue[0xB2] = 5; 
      FPartsBlue[0xB1] = 4; 
      FPartsBlue[0xB0] = 4; 
      //                
      FPartsBlue[0xAF] = 4; 
      FPartsBlue[0xAE] = 4; 
      FPartsBlue[0xAD] = 3; 
      FPartsBlue[0xAC] = 3; 
      FPartsBlue[0xAB] = 2; 
      FPartsBlue[0xAA] = 2; 
      FPartsBlue[0xA9] = 1; 
      FPartsBlue[0xA8] = 1; 
      FPartsBlue[0xA7] = 1; 
      FPartsBlue[0xA6] = 1; 
      FPartsBlue[0xA5] = 0; 
      FPartsBlue[0xA4] = 0; 
      FPartsBlue[0xA3] = 0; 
      FPartsBlue[0xA2] = 0; 
      FPartsBlue[0xA1] = 0; 
      FPartsBlue[0xA0] = 0; 
      //                
      FPartsBlue[0x9F] = 0; 
      FPartsBlue[0x9E] = 0; 
      FPartsBlue[0x9D] = 0; 
      FPartsBlue[0x9C] = 0; 
      FPartsBlue[0x9B] = 0; 
      FPartsBlue[0x9A] = 0; 
      FPartsBlue[0x99] = 0; 
      FPartsBlue[0x98] = 0; 
      FPartsBlue[0x97] = 0; 
      FPartsBlue[0x96] = 0; 
      FPartsBlue[0x95] = 0; 
      FPartsBlue[0x94] = 0; 
      FPartsBlue[0x93] = 0; 
      FPartsBlue[0x92] = 0; 
      FPartsBlue[0x91] = 0; 
      FPartsBlue[0x90] = 0; 
      //                
      FPartsBlue[0x8F] = 0; 
      FPartsBlue[0x8E] = 0; 
      FPartsBlue[0x8D] = 0; 
      FPartsBlue[0x8C] = 0; 
      FPartsBlue[0x8B] = 0; 
      FPartsBlue[0x8A] = 0; 
      FPartsBlue[0x89] = 0; 
      FPartsBlue[0x88] = 0; 
      FPartsBlue[0x87] = 0; 
      FPartsBlue[0x86] = 0; 
      FPartsBlue[0x85] = 0; 
      FPartsBlue[0x84] = 0; 
      FPartsBlue[0x83] = 0; 
      FPartsBlue[0x82] = 0; 
      FPartsBlue[0x81] = 0; 
      FPartsBlue[0x80] = 0; 
      //                
      FPartsBlue[0x7F] = 0; 
      FPartsBlue[0x7E] = 0; 
      FPartsBlue[0x7D] = 0; 
      FPartsBlue[0x7C] = 0; 
      FPartsBlue[0x7B] = 8; 
      FPartsBlue[0x7A] = 8; 
      FPartsBlue[0x79] = 16; 
      FPartsBlue[0x78] = 16; 
      FPartsBlue[0x77] = 32; 
      FPartsBlue[0x76] = 32; 
      FPartsBlue[0x75] = 41; 
      FPartsBlue[0x74] = 41; 
      FPartsBlue[0x73] = 49; 
      FPartsBlue[0x72] = 49; 
      FPartsBlue[0x71] = 57; 
      FPartsBlue[0x70] = 57; 
      //                
      FPartsBlue[0x6F] = 65; 
      FPartsBlue[0x6E] = 65; 
      FPartsBlue[0x6D] = 73; 
      FPartsBlue[0x6C] = 73; 
      FPartsBlue[0x6B] = 81; 
      FPartsBlue[0x6A] = 81; 
      FPartsBlue[0x69] = 90; 
      FPartsBlue[0x68] = 90; 
      FPartsBlue[0x67] = 98; 
      FPartsBlue[0x66] = 98; 
      FPartsBlue[0x65] = 106; 
      FPartsBlue[0x64] = 106; 
      FPartsBlue[0x63] = 114; 
      FPartsBlue[0x62] = 114; 
      FPartsBlue[0x61] = 122; 
      FPartsBlue[0x60] = 122; 
      //                
      FPartsBlue[0x5F] = 130; 
      FPartsBlue[0x5E] = 130; 
      FPartsBlue[0x5D] = 138; 
      FPartsBlue[0x5C] = 138; 
      FPartsBlue[0x5B] = 147; 
      FPartsBlue[0x5A] = 147; 
      FPartsBlue[0x59] = 155; 
      FPartsBlue[0x58] = 155; 
      FPartsBlue[0x57] = 163; 
      FPartsBlue[0x56] = 163; 
      FPartsBlue[0x55] = 171; 
      FPartsBlue[0x54] = 171; 
      FPartsBlue[0x53] = 179; 
      FPartsBlue[0x52] = 179; 
      FPartsBlue[0x51] = 181; 
      FPartsBlue[0x50] = 181; 
      //                
      FPartsBlue[0x4F] = 180; 
      FPartsBlue[0x4E] = 180; 
      FPartsBlue[0x4D] = 179; 
      FPartsBlue[0x4C] = 179; 
      FPartsBlue[0x4B] = 177; 
      FPartsBlue[0x4A] = 177; 
      FPartsBlue[0x49] = 176; 
      FPartsBlue[0x48] = 176; 
      FPartsBlue[0x47] = 175; 
      FPartsBlue[0x46] = 175; 
      FPartsBlue[0x45] = 174; 
      FPartsBlue[0x44] = 174; 
      FPartsBlue[0x43] = 173; 
      FPartsBlue[0x42] = 173; 
      FPartsBlue[0x41] = 172; 
      FPartsBlue[0x40] = 172; 
      //                
      FPartsBlue[0x3F] = 171; 
      FPartsBlue[0x3E] = 171; 
      FPartsBlue[0x3D] = 170; 
      FPartsBlue[0x3C] = 170; 
      FPartsBlue[0x3B] = 169; 
      FPartsBlue[0x3A] = 169; 
      FPartsBlue[0x39] = 168; 
      FPartsBlue[0x38] = 168; 
      FPartsBlue[0x37] = 167; 
      FPartsBlue[0x36] = 167; 
      FPartsBlue[0x35] = 166; 
      FPartsBlue[0x34] = 166; 
      FPartsBlue[0x33] = 165; 
      FPartsBlue[0x32] = 165; 
      FPartsBlue[0x31] = 163; 
      FPartsBlue[0x30] = 163; 
      //                
      FPartsBlue[0x2F] = 162; 
      FPartsBlue[0x2E] = 162; 
      FPartsBlue[0x2D] = 161; 
      FPartsBlue[0x2C] = 161; 
      FPartsBlue[0x2B] = 160; 
      FPartsBlue[0x2A] = 160; 
      FPartsBlue[0x29] = 159; 
      FPartsBlue[0x28] = 159; 
      FPartsBlue[0x27] = 157; 
      FPartsBlue[0x26] = 155; 
      FPartsBlue[0x25] = 153; 
      FPartsBlue[0x24] = 151; 
      FPartsBlue[0x23] = 149; 
      FPartsBlue[0x22] = 158; 
      FPartsBlue[0x21] = 150; 
      FPartsBlue[0x20] = 146; 
      //                
      FPartsBlue[0x1F] = 145; 
      FPartsBlue[0x1E] = 143; 
      FPartsBlue[0x1D] = 141; 
      FPartsBlue[0x1C] = 139; 
      FPartsBlue[0x1B] = 136; 
      FPartsBlue[0x1A] = 134; 
      FPartsBlue[0x19] = 132; 
      FPartsBlue[0x18] = 130; 
      FPartsBlue[0x17] = 128; 
      FPartsBlue[0x16] = 126; 
      FPartsBlue[0x15] = 124; 
      FPartsBlue[0x14] = 122; 
      FPartsBlue[0x13] = 120; 
      FPartsBlue[0x12] = 118; 
      FPartsBlue[0x11] = 116; 
      FPartsBlue[0x10] = 114; 
      //                
      FPartsBlue[0x0F] = 113; 
      FPartsBlue[0x0E] = 112; 
      FPartsBlue[0x0D] = 111; 
      FPartsBlue[0x0C] = 110; 
      FPartsBlue[0x0B] = 108; 
      FPartsBlue[0x0A] = 106; 
      FPartsBlue[0x09] = 104; 
      FPartsBlue[0x08] = 102; 
      FPartsBlue[0x07] = 100; 
      FPartsBlue[0x06] = 97; 
      FPartsBlue[0x05] = 96; 
      FPartsBlue[0x04] = 95; 
      FPartsBlue[0x03] = 94; 
      FPartsBlue[0x02] = 93; 
      FPartsBlue[0x01] = 92; 
      FPartsBlue[0x00] = 91; 
      //


      Int32 TI = CIPPalette1K.COLORRANGE_1K - 1;
      for (Int32 CI = CIPPalette1K.COLORRANGE_8BIT - 1; 0 <= CI; CI--)
      {
        FPartsRed[TI] = FPartsRed[CI];
        FPartsGreen[TI] = FPartsGreen[CI];
        FPartsBlue[TI] = FPartsBlue[CI];
        TI--;
        FPartsRed[TI] = FPartsRed[CI];
        FPartsGreen[TI] = FPartsGreen[CI];
        FPartsBlue[TI] = FPartsBlue[CI];
        TI--;
        FPartsRed[TI] = FPartsRed[CI];
        FPartsGreen[TI] = FPartsGreen[CI];
        FPartsBlue[TI] = FPartsBlue[CI];
        TI--;
        FPartsRed[TI] = FPartsRed[CI];
        FPartsGreen[TI] = FPartsGreen[CI];
        FPartsBlue[TI] = FPartsBlue[CI];
        TI--;
      }
    }
    //
    //--------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------
    //
    public override EPalette1K GetPalette()
    {
      return EPalette1K.Spectrum;
    }

  }
}




