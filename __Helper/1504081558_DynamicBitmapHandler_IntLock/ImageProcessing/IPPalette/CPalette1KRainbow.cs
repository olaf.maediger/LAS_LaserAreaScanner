﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPImagePrimitives;
//
namespace IPPalette
{
  public class CPalette1KRainbow : CPalette1KBase
  {
    private const Int32 BORDER_LOW = 4;
    private const Int32 BORDER_HIGH = 4;
    //
    //-------------------------------------------------------
    //  Section - Constructor
    //-------------------------------------------------------
    //
    public CPalette1KRainbow(Int32 countsection)
      : base(countsection)
    {
      Initialize();
    }
    //
    //-------------------------------------------------------
    //  Section - Property
    //-------------------------------------------------------
    //
    public override EPalette1K GetPalette()
    {
      return EPalette1K.Rainbow;
    }
    //
    //-------------------------------------------------------
    //  Section - Build
    //-------------------------------------------------------
    //
    override protected void Initialize()
    {
      //???const Double SL = 0.5;
      //???const Double L = 0.5;
      //
      /* MUELL
      for (Int32 CI = 0; CI < CIPPalette.COLORRANGE_1K; CI++)
      {
        ColorRGB RGB;
        Double H = (Double)CI / (Double)CIPPalette.COLORRANGE_1K;
        CColorConversion.HSL2RGB(H, SL, L, out RGB);
        FPartsRed[CI] = RGB.R;
        FPartsGreen[CI] = RGB.G;
        FPartsBlue[CI] = RGB.B;
      }*/

      /*???
      const Double C = 255.0;

      Int32 numOfSteps = 1024;
      for (Int32 step = 0; step < numOfSteps; step++)
      {
        Double r = 0.0;
        Double g = 0.0;
        Double b = 0.0;
        Double h = (Double)step / numOfSteps;
        Int32 i = (Int32)(h * 6);
        Double f = h * 6.0 - i;
        Double q = 1 - f;

        switch (i % 6)
        {
          case 0:
            r = 1;
            g = f;
            b = 0;
            break;
          case 1:
            r = q;
            g = 1;
            b = 0;
            break;
          case 2:
            r = 0;
            g = 1;
            b = f;
            break;
          case 3:
            r = 0;
            g = q;
            b = 1;
            break;
          case 4:
            r = f;
            g = 0;
            b = 1;
            break;
          case 5:
            r = 1;
            g = 0;
            b = q;
            break;
        }
        FPartsRed[step] = ((Byte)(r * C));
        FPartsGreen[step] = ((Byte)(g * C));
        FPartsBlue[step] = ((Byte)(b * C));              
      }*/

      for (Int32 CI = 0; CI < BORDER_LOW; CI++)
      {
        FPartsRed[CI] = 0x00;
        FPartsGreen[CI] = 0x00;
        FPartsBlue[CI] = 0x00;
      }
      for (Int32 CI = BORDER_LOW; CI < CIPPalette1K.COLORRANGE_1K - BORDER_HIGH; CI++)
      {
        Double QuadValue = 4 * (Double)CI / (Double)CIPPalette1K.COLORRANGE_1K;
        FPartsRed[CI] = (Byte)(255.0 * Math.Max(Math.Min(Math.Min(QuadValue - 1.5, -QuadValue + 4.5), 1), 0));
        FPartsGreen[CI] = (Byte)(255.0 * Math.Max(Math.Min(Math.Min(QuadValue - 0.5, -QuadValue + 3.5), 1), 0));
        FPartsBlue[CI] = (Byte)(255.0 * Math.Max(Math.Min(Math.Min(QuadValue + 0.5, -QuadValue + 2.5), 1), 0));
      }
      for (Int32 CI = CIPPalette1K.COLORRANGE_1K - BORDER_HIGH; CI < CIPPalette1K.COLORRANGE_1K; CI++)
      {
        FPartsRed[CI] = 0xFF;
        FPartsGreen[CI] = 0xFF;
        FPartsBlue[CI] = 0xFF;
      }



    }



  }
}
