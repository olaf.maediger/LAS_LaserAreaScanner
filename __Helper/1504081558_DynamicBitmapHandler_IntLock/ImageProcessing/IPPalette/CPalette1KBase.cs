﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
//
namespace IPPalette
{
  public abstract class CPalette1KBase : CPaletteBase
  {
    static public Color COLOR_ZERO = Color.FromArgb(0);
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    //
    protected Int32 FCountSection = CIPPalette1K.COUNTSECTION_MINIMUM; 
    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CPalette1KBase(Int32 countsection)
    {
      FPartsAlpha = new Byte[CIPPalette1K.COLORRANGE_1K];
      for (Int32 AI = 0; AI < CIPPalette1K.COLORRANGE_1K; AI++)
      {
        FPartsAlpha[AI] = INIT_ALPHA;
      }
      FPartsRed = new Byte[CIPPalette1K.COLORRANGE_1K];
      FPartsGreen = new Byte[CIPPalette1K.COLORRANGE_1K];
      FPartsBlue = new Byte[CIPPalette1K.COLORRANGE_1K];
      FCountSection = Math.Max(CIPPalette1K.COUNTSECTION_MINIMUM, countsection);
      FCountSection = Math.Min(CIPPalette1K.COUNTSECTION_MAXIMUM, FCountSection);
    }

    public CPalette1KBase()
    {
      FPartsAlpha = new Byte[CIPPalette1K.COLORRANGE_1K];
      for (Int32 AI = 0; AI < CIPPalette1K.COLORRANGE_1K; AI++)
      {
        FPartsAlpha[AI] = INIT_ALPHA;
      }
      FPartsRed = new Byte[CIPPalette1K.COLORRANGE_1K];
      FPartsGreen = new Byte[CIPPalette1K.COLORRANGE_1K];
      FPartsBlue = new Byte[CIPPalette1K.COLORRANGE_1K];
      FCountSection = Math.Max(CIPPalette1K.COUNTSECTION_MINIMUM, 1);
      FCountSection = Math.Min(CIPPalette1K.COUNTSECTION_MAXIMUM, FCountSection);
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //
    public abstract EPalette1K GetPalette();

    public String GetName()
    {
      return CIPPalette1K.PaletteToText(GetPalette());
    }

    public override Byte GetPartsAlpha(UInt16 index)
    {
      if ((0 <= index) && (index <= CIPPalette1K.COLORRANGE_1K))
      {
        return this.FPartsAlpha[index];
      }
      return 0;
    }
    public override Byte GetPartsRed(UInt16 index)
    {
      if ((0 <= index) && (index <= CIPPalette1K.COLORRANGE_1K))
      {
        return this.FPartsRed[index];
      }
      return 0;
    }
    public override Byte GetPartsGreen(UInt16 index)
    {
      if ((0 <= index) && (index <= CIPPalette1K.COLORRANGE_1K))
      {
        return this.FPartsGreen[index];
      }
      return 0;
    }
    public override Byte GetPartsBlue(UInt16 index)
    {
      if ((0 <= index) && (index <= CIPPalette1K.COLORRANGE_1K))
      {
        return this.FPartsBlue[index];
      }
      return 0;
    }
    public override Color GetColor(UInt16 index)
    {
      if ((0 <= index) && (index <= CIPPalette1K.COLORRANGE_1K))
      {
        Byte A = this.FPartsAlpha[index];
        Byte R = this.FPartsRed[index];
        Byte G = this.FPartsGreen[index];
        Byte B = this.FPartsBlue[index];
        return Color.FromArgb(A, R, G, B);
      }
      return Color.Black;
    }

 
    //  Helper
    protected abstract void Initialize();

    public override UInt16 FindPaletteIndex(Byte red, Byte green, Byte blue)
    {
      for (UInt16 CI = 0; CI < CIPPalette1K.COLORRANGE_1K; CI++)
      {
        if (red == FPartsRed[CI])
        {
          if (green == FPartsGreen[CI])
          {
            if (blue == FPartsBlue[CI])
            {
              return CI;
            }
          }
        }
      }
      return 0;
    }

    public override UInt16 FindPaletteIndex(Byte alpha, Byte red, Byte green, Byte blue)
    {
      for (UInt16 CI = 0; CI < CIPPalette1K.COLORRANGE_1K; CI++)
      {
        if (alpha == FPartsAlpha[CI])
        {
          if (red == FPartsRed[CI])
          {
            if (green == FPartsGreen[CI])
            {
              if (blue == FPartsBlue[CI])
              {
                return CI;
              }
            }
          }
        }
      }
      return 0;
    }

  }
}
