﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
//
namespace TaggedBinaryFile
{
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct STaggUInt16Vector
  {
    public Byte Tagg;
    public UInt32 Size;
    public UInt32 ValueCount;
    public UInt16[] Values;
    //
    public STaggUInt16Vector(Int32 init)
    {
      Tagg = CTaggUInt16Vector.TAGG;
      Size = CTaggUInt16Vector.SIZE;
      ValueCount = CTaggUInt16Vector.INIT_VALUECOUNT;
      Values = new UInt16[ValueCount];
      Values[0] = CTaggUInt16Vector.INIT_VALUE;
    }
  }


  public class CTaggUInt16Vector : CTagg
  {
    public const Byte TAGG = 0x55;
    public const UInt32 SIZE = 0x00000002; // Size of one Value [Byte]
    public const UInt32 INIT_VALUECOUNT = 0x00000001;
    public const UInt16 INIT_VALUE = 0x0000;

    public STaggUInt16Vector FField;

    public CTaggUInt16Vector()
    {
      FField = new STaggUInt16Vector(0);
    }

    public CTaggUInt16Vector(UInt16[] values)
    {
      FField = new STaggUInt16Vector(0);
      UInt32 VectorSize = (UInt32)values.GetLength(0);
      FField.ValueCount = VectorSize;
      FField.Values = new UInt16[VectorSize];
      FField.Size = (UInt32)(SIZE * VectorSize);
      for (Int32 VI = 0; VI < VectorSize; VI++)
      {
        FField.Values[VI] = values[VI];
      }
    }


    public UInt32 ValueCount
    {
      get { return FField.ValueCount; }
    }
    public UInt16[] Values
    {
      get { return FField.Values; }
    }



    public override Boolean Write(BinaryWriter binarywriter)
    {
      binarywriter.Write((Byte)FField.Tagg);
      binarywriter.Write((UInt32)FField.Size);
      binarywriter.Write((UInt32)FField.ValueCount);
      for (Int32 VI = 0; VI < FField.ValueCount; VI++)
      {
        binarywriter.Write((UInt16)FField.Values[VI]);
      }
      return true;
    }

    public override Boolean Read(BinaryReader binaryreader)
    {
      FField.Tagg = binaryreader.ReadByte();
      if (TAGG == FField.Tagg)
      {
        FField.Size = binaryreader.ReadUInt32();
        FField.ValueCount = binaryreader.ReadUInt32();
        FField.Values = new UInt16[FField.ValueCount];
        for (Int32 VI = 0; VI < FField.ValueCount; VI++)
        {
          FField.Values[VI] = binaryreader.ReadUInt16();
        }
        return true;
      }
      return false;
    }

  }
}


