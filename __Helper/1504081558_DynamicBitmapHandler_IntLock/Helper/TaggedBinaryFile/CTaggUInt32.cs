﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
//
namespace TaggedBinaryFile
{
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct STaggUInt32
  {
    public Byte Tagg;
    public Byte Size;
    public UInt32 Value;
    //
    public STaggUInt32(Int32 init)
    {
      Tagg = CTaggUInt32.TAGG;
      Size = CTaggUInt32.SIZE;
      Value = CTaggUInt32.INIT_VALUE;
    }
  }


  public class CTaggUInt32 : CTagg
  {
    public const Byte TAGG = 0x37;
    public const Byte SIZE = 0x04;
    public const UInt16 INIT_VALUE = 0x00000000;

    public STaggUInt32 FField;

    public CTaggUInt32()
    {
      FField = new STaggUInt32(0);
    }

    public CTaggUInt32(UInt32 value)
    {
      FField = new STaggUInt32(0);
      FField.Value = value;
    }


    public UInt32 Value
    {
      get { return FField.Value; }
    }



    public override Boolean Write(BinaryWriter binarywriter)
    {
      binarywriter.Write((Byte)FField.Tagg);
      binarywriter.Write((Byte)FField.Size);
      binarywriter.Write((UInt32)FField.Value);
      return true;
    }

    public override Boolean Read(BinaryReader binaryreader)
    {
      FField.Tagg = binaryreader.ReadByte();
      if (TAGG == FField.Tagg)
      {
        FField.Size = binaryreader.ReadByte();
        FField.Value = binaryreader.ReadUInt32();
        return true;
      }
      return false;
    }

  }
}
