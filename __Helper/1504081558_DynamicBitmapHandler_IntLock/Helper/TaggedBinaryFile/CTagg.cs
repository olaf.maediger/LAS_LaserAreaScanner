﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
//
namespace TaggedBinaryFile
{ // BaseClass
  public abstract class CTagg
  {

    public abstract Boolean Write(BinaryWriter binarywriter);
    public abstract Boolean Read(BinaryReader binaryreader);
  }
}
