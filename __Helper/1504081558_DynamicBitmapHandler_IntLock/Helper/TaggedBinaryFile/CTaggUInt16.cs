﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
//
namespace TaggedBinaryFile
{
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct STaggUInt16
  {
    public Byte Tagg;
    public Byte Size;
    public UInt16 Value;
    //
    public STaggUInt16(Int32 init)
    {
      Tagg = CTaggUInt16.TAGG;
      Size = CTaggUInt16.SIZE;
      Value = CTaggUInt16.INIT_VALUE;
    }
  }


  public class CTaggUInt16 : CTagg
  {
    public const Byte TAGG = 0x32;
    public const Byte SIZE = 0x02;
    public const UInt16 INIT_VALUE = 0x0000;

    public STaggUInt16 FField;

    public CTaggUInt16()
    {
      FField = new STaggUInt16(0);
    }

    public CTaggUInt16(UInt16 value)
    {
      FField = new STaggUInt16(0);
      FField.Value = value;
    }


    public UInt16 Value
    {
      get { return FField.Value; }
    }


    public override Boolean Write(BinaryWriter binarywriter)
    {
      binarywriter.Write((Byte)FField.Tagg);
      binarywriter.Write((Byte)FField.Size);
      binarywriter.Write((UInt16)FField.Value);
      return true;
    }

    public override Boolean Read(BinaryReader binaryreader)
    {
      FField.Tagg = binaryreader.ReadByte();
      if (TAGG == FField.Tagg)
      {
        FField.Size = binaryreader.ReadByte();
        FField.Value = binaryreader.ReadUInt16();
        return true;
      }
      return false;
    }

  }
}
