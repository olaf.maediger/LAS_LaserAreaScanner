﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
//
namespace TaggedBinaryFile
{
  [StructLayout(LayoutKind.Sequential, Pack = 1)]
  public struct STaggByteVector
  {
    public Byte Tagg;
    public UInt32 Size;
    public UInt32 ValueCount;
    public Byte[] Values;
    //
    public STaggByteVector(Int32 init)
    {
      Tagg = CTaggByteVector.TAGG;
      Size = CTaggByteVector.SIZE;
      ValueCount = CTaggByteVector.INIT_VALUECOUNT;
      Values = new Byte[ValueCount];
      Values[0] = CTaggByteVector.INIT_VALUE;
    }
  }


  public class CTaggByteVector : CTagg
  {
    public const Byte TAGG = 0x51;
    public const UInt32 SIZE = 0x00000001; // Size of one Value [Byte]
    public const UInt32 INIT_VALUECOUNT = 0x00000001;
    public const Byte INIT_VALUE = 0x0000;

    public STaggByteVector FField;

    public CTaggByteVector()
    {
      FField = new STaggByteVector(0);
    }

    public CTaggByteVector(Byte[] values)
    {
      FField = new STaggByteVector(0);
      UInt32 VectorSize = (UInt32)values.GetLength(0);
      FField.ValueCount = VectorSize;
      FField.Values = new Byte[VectorSize];
      FField.Size = (UInt32)(SIZE * VectorSize);
      for (Int32 VI = 0; VI < VectorSize; VI++)
      {
        FField.Values[VI] = values[VI];
      }
    }


    public UInt32 ValueCount
    {
      get { return FField.ValueCount; }
    }
    public Byte[] Values
    {
      get { return FField.Values; }
    }



    public override Boolean Write(BinaryWriter binarywriter)
    {
      binarywriter.Write((Byte)FField.Tagg);
      binarywriter.Write((UInt32)FField.Size);
      binarywriter.Write((UInt32)FField.ValueCount);
      for (Int32 VI = 0; VI < FField.ValueCount; VI++)
      {
        binarywriter.Write((Byte)FField.Values[VI]);
      }
      return true;
    }

    public override Boolean Read(BinaryReader binaryreader)
    {
      FField.Tagg = binaryreader.ReadByte();
      if (TAGG == FField.Tagg)
      {
        FField.Size = binaryreader.ReadUInt32();
        FField.ValueCount = binaryreader.ReadUInt32();
        FField.Values = new Byte[FField.ValueCount];
        for (Int32 VI = 0; VI < FField.ValueCount; VI++)
        {
          FField.Values[VI] = binaryreader.ReadByte();
        }
        return true;
      }
      return false;
    }

  }
}


