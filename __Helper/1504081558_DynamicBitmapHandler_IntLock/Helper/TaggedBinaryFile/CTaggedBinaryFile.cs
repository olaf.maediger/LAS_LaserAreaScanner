﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
//
namespace TaggedBinaryFile
{
  public class CTaggedBinaryFile
  {
    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //

    private BinaryReader FBinaryReader;
    private BinaryWriter FBinaryWriter;

    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public CTaggedBinaryFile()
    {
      FBinaryReader = null;
      FBinaryWriter = null;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Write
    //-------------------------------------------------------------------
    //
    public Boolean OpenWrite(String fileentry)
    {
      try
      {
        FileStream FS = new FileStream(fileentry, FileMode.Create, FileAccess.Write);
        FBinaryWriter = new BinaryWriter(FS);
        return (FBinaryWriter is BinaryWriter);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    
    public Boolean CloseWrite()
    {
      try
      {
        FBinaryWriter.Close();
        FBinaryWriter = null;
        return (null == FBinaryWriter);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Write(UInt16 value)
    {
      try
      {
        CTaggUInt16 Tagg = new CTaggUInt16(value);
        return Tagg.Write(FBinaryWriter);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Write(UInt32 value)
    {
      try
      {
        CTaggUInt32 Tagg = new CTaggUInt32(value);
        return Tagg.Write(FBinaryWriter);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Write(Double value)
    {
      try
      {
        CTaggDouble Tagg = new CTaggDouble(value);
        return Tagg.Write(FBinaryWriter);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Write(String value)
    {
      try
      {
        CTaggString Tagg = new CTaggString(value);
        return Tagg.Write(FBinaryWriter);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Write(UInt16[] value)
    {
      try
      {
        CTaggUInt16Vector Tagg = new CTaggUInt16Vector(value);
        return Tagg.Write(FBinaryWriter);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Write(Byte[,] value)
    {
      try
      {
        CTaggByteMatrix Tagg = new CTaggByteMatrix(value);
        return Tagg.Write(FBinaryWriter);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Write(UInt16[,] value)
    {
      try
      {
        CTaggUInt16Matrix Tagg = new CTaggUInt16Matrix(value);
        return Tagg.Write(FBinaryWriter);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Reader
    //-------------------------------------------------------------------
    //
    public Boolean OpenRead(String fileentry)
    {
      try
      {
        FileStream FS = new FileStream(fileentry, FileMode.Open, FileAccess.Read);
        FBinaryReader = new BinaryReader(FS);
        return (FBinaryReader is BinaryReader);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    
    public Boolean CloseRead()
    {
      try
      {
        FBinaryReader.Close();
        FBinaryReader = null;
        return (null == FBinaryReader);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Read(out UInt16 value)
    {
      value = CTaggUInt16.INIT_VALUE;
      try
      {
        CTaggUInt16 Tagg = new CTaggUInt16();
        if (Tagg.Read(FBinaryReader))
        {
          value = Tagg.Value;
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Read(out UInt32 value)
    {
      value = CTaggUInt32.INIT_VALUE;
      try
      {
        CTaggUInt32 Tagg = new CTaggUInt32();
        if (Tagg.Read(FBinaryReader))
        {
          value = Tagg.Value;
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Read(out Double value)
    {
      value = CTaggDouble.INIT_VALUE;
      try
      {
        CTaggDouble Tagg = new CTaggDouble();
        if (Tagg.Read(FBinaryReader))
        {
          value = Tagg.Value;
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Read(out String value)
    {
      value = "";
      try
      {
        CTaggString Tagg = new CTaggString();
        if (Tagg.Read(FBinaryReader))
        {
          value = Tagg.Text;
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Read(out UInt16[] values)
    {
      values = null;
      try
      {
        CTaggUInt16Vector Tagg = new CTaggUInt16Vector();
        if (Tagg.Read(FBinaryReader))
        {
          values = Tagg.Values;
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Read(out UInt16[,] values)
    {
      values = null;
      try
      {
        CTaggUInt16Matrix Tagg = new CTaggUInt16Matrix();
        if (Tagg.Read(FBinaryReader))
        {
          values = Tagg.Values;
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean Read(out Byte[,] values)
    {
      values = null;
      try
      {
        CTaggByteMatrix Tagg = new CTaggByteMatrix();
        if (Tagg.Read(FBinaryReader))
        {
          values = Tagg.Values;
          return true;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }




  }
}
