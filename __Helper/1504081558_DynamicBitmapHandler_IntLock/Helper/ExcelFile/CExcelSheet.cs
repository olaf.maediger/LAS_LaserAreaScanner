﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
//
namespace ExcelFile
{
  public class CExcelSheet
  {
    //
    //-----------------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------------
    //
    public CExcelSheet()
    {
    }
    //
    //-----------------------------------------------------------------------
    //  Segment - Public
    //-----------------------------------------------------------------------
    //
    public Boolean ExportMatrixByte(String sheetname, 
                                    Byte[,] matrix)
    {
      try
      {
        Excel.Application Application;
        Excel.Workbook Workbook;
        Excel.Worksheet Worksheet;
        //
        Application = new Excel.Application();
        Application.Visible = true;
        Application.ScreenUpdating = true;
        Workbook = Application.Workbooks.Add();
        Worksheet = Workbook.Sheets.Add() as Excel.Worksheet;
        Worksheet.Name = sheetname;
        //
       Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Worksheet.Cells[1 + RI, 1 + CI] = matrix[RI, CI].ToString(); 
          }
        }
        //
        Worksheet = null;
        Workbook = null;
        Application = null;
        //
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }
    public Boolean ExportMatrixUInt16(String sheetname,
                                      UInt16[,] matrix)
    {
      try
      {
        Excel.Application Application;
        Excel.Workbook Workbook;
        Excel.Worksheet Worksheet;
        //
        Application = new Excel.Application();
        Application.Visible = true;
        Application.ScreenUpdating = true;
        Workbook = Application.Workbooks.Add();
        Worksheet = Workbook.Sheets.Add() as Excel.Worksheet;
        Worksheet.Name = sheetname;
        //
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Worksheet.Cells[1 + RI, 1 + CI] = matrix[RI, CI].ToString();
          }
        }
        //
        Worksheet = null;
        Workbook = null;
        Application = null;
        //
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean ImportMatrixUInt16(String sheetname,
                                      Boolean importselection,
                                      out UInt16[,] matrix)
    {
      matrix = null;
      try
      {
        Excel.Application EA;
        //Excel.Workbook WB;
        Excel.Worksheet WS;
        Excel.Range RS;
        //
        EA = (Excel.Application)Marshal.GetActiveObject("Excel.Application");
        WS = (Excel.Worksheet)EA.ActiveSheet;
        if (importselection)
        {
          RS = (Excel.Range)EA.Selection;
        }
        else
        {
          RS = (Excel.Range)WS.UsedRange;
        }
        // ;
        //
        Int32 RC = RS.Cells.Rows.Count;
        Int32 CC = RS.Cells.Columns.Count;
        //
        matrix = new UInt16[RC, CC];
        if ((1 == RC) && (1 == CC))
        {
          String SValue = RS.Cells.get_Value().ToString();
          UInt16 UValue = UInt16.Parse(SValue);
          matrix[0, 0] = UValue;
        }
        else
        {
          System.Array MatrixValues = RS.Cells.get_Value();
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              String SValue = MatrixValues.GetValue(1 + RI, 1 + CI).ToString();
              UInt16 UValue = UInt16.Parse(SValue);
              matrix[RI, CI] = UValue;
            }
          }
        }
        //
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

  }
}
