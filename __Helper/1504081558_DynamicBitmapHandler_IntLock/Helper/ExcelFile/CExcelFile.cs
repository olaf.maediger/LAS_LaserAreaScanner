﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
//
namespace ExcelFile
{
  public class CExcelFile
  {
    //
    //-----------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------
    //
    private XmlTextWriter FXmlTextWriter;
    //
    //-----------------------------------------------------------------------
    //  Segment - Helper
    //-----------------------------------------------------------------------
    //
    private Boolean WriteTable(Byte[,] matrix)
    {
      FXmlTextWriter.WriteStartElement("Table");
      //
      Int32 RC = matrix.GetLength(0);
      Int32 CC = matrix.GetLength(1);
      FXmlTextWriter.WriteAttributeString("ss", "ExpandedColumnCount", null, CC.ToString());
      FXmlTextWriter.WriteAttributeString("ss", "ExpandedRowCount", null, RC.ToString());
      FXmlTextWriter.WriteAttributeString("x", "FullColumns", null, "1");
      FXmlTextWriter.WriteAttributeString("x", "FullRows", null, "1");
      FXmlTextWriter.WriteAttributeString("ss", "DefaultColumnWidth", null, "60");
      //
      for (Int32 RI = 0; RI < RC; RI++)
      {
        FXmlTextWriter.WriteStartElement("Row");
        for (Int32 CI = 0; CI < CC; CI++)
        {
          FXmlTextWriter.WriteStartElement("Cell");
          FXmlTextWriter.WriteStartElement("Data");
          FXmlTextWriter.WriteAttributeString("ss", "Type", null, "Number");
          FXmlTextWriter.WriteValue((Int32)matrix[RI, CI]);
          FXmlTextWriter.WriteEndElement(); // </Data>
          FXmlTextWriter.WriteEndElement(); // </Cell>
        }
        FXmlTextWriter.WriteEndElement(); // </Row>
      }
      //
      FXmlTextWriter.WriteEndElement(); // </Table>
      //
      return true;
    }

    private Boolean WriteTable(UInt16[,] matrix)
    {
      FXmlTextWriter.WriteStartElement("Table");
      //
      Int32 RC = matrix.GetLength(0);
      Int32 CC = matrix.GetLength(1);
      FXmlTextWriter.WriteAttributeString("ss", "ExpandedColumnCount", null, CC.ToString());
      FXmlTextWriter.WriteAttributeString("ss", "ExpandedRowCount", null, RC.ToString());
      FXmlTextWriter.WriteAttributeString("x", "FullColumns", null, "1");
      FXmlTextWriter.WriteAttributeString("x", "FullRows", null, "1");
      FXmlTextWriter.WriteAttributeString("ss", "DefaultColumnWidth", null, "60");
      //
      for (Int32 RI = 0; RI < RC; RI++)
      {
        FXmlTextWriter.WriteStartElement("Row");
        for (Int32 CI = 0; CI < CC; CI++)
        {
          FXmlTextWriter.WriteStartElement("Cell");
          FXmlTextWriter.WriteStartElement("Data");
          FXmlTextWriter.WriteAttributeString("ss", "Type", null, "Number");
          FXmlTextWriter.WriteValue((Int32)matrix[RI, CI]);
          FXmlTextWriter.WriteEndElement(); // </Data>
          FXmlTextWriter.WriteEndElement(); // </Cell>
        }
        FXmlTextWriter.WriteEndElement(); // </Row>
      }
      //
      FXmlTextWriter.WriteEndElement(); // </Table>
      //
      return true;
    }



    private Boolean WriteWorksheet(String nameworksheet, Byte[,] matrix)
    {
      FXmlTextWriter.WriteStartElement("Worksheet");
      FXmlTextWriter.WriteAttributeString("ss", "Name", null, "DATA");//nameworksheet);
      //
      WriteTable(matrix);
      //
      FXmlTextWriter.WriteStartElement("WorksheetOptions", "urn:schemas-microsoft-com:office:excel");
      //
      FXmlTextWriter.WriteStartElement("PageSetup");
      //
      FXmlTextWriter.WriteStartElement("Header");
      FXmlTextWriter.WriteAttributeString("x", "Margin", null, "0.4921259845");
      FXmlTextWriter.WriteEndElement(); //</Header>
      //
      FXmlTextWriter.WriteStartElement("Footer");
      FXmlTextWriter.WriteAttributeString("x", "Margin", null, "0.4921259845");
      FXmlTextWriter.WriteEndElement(); //</Footer>
      //
      FXmlTextWriter.WriteStartElement("PageMargins");
      FXmlTextWriter.WriteAttributeString("x", "Bottom", null, "0.984251969");
      FXmlTextWriter.WriteAttributeString("x", "Left", null, "0.78740157499999996");
      FXmlTextWriter.WriteAttributeString("x", "Right", null, "0.78740157499999996");
      FXmlTextWriter.WriteAttributeString("x", "Top", null, "0.984251969");
      FXmlTextWriter.WriteEndElement(); //</PageMargins>
      //
      FXmlTextWriter.WriteEndElement(); //</PageSetup>
      FXmlTextWriter.WriteElementString("Selected", null);
      //
      FXmlTextWriter.WriteStartElement("Panes");
      //
      FXmlTextWriter.WriteStartElement("Pane");
      FXmlTextWriter.WriteElementString("Number", "1");
      FXmlTextWriter.WriteElementString("ActiveRow", "1");
      FXmlTextWriter.WriteElementString("ActiveCol", "1");
      FXmlTextWriter.WriteEndElement(); //</Pane>
      //
      FXmlTextWriter.WriteEndElement(); //</Panes>
      FXmlTextWriter.WriteElementString("ProtectObjects", "False");
      FXmlTextWriter.WriteElementString("ProtectScenarios", "False");
      FXmlTextWriter.WriteEndElement(); //</WorksheetOptions>
      //
      FXmlTextWriter.WriteEndElement(); //</Worksheet>
      //
      return true;
    }

    private Boolean WriteWorksheet(String nameworksheet, UInt16[,] matrix)
    {
      FXmlTextWriter.WriteStartElement("Worksheet");
      FXmlTextWriter.WriteAttributeString("ss", "Name", null, "DATA");//nameworksheet);
      //
      WriteTable(matrix);
      //
      FXmlTextWriter.WriteStartElement("WorksheetOptions", "urn:schemas-microsoft-com:office:excel");
      //
      FXmlTextWriter.WriteStartElement("PageSetup");
      //
      FXmlTextWriter.WriteStartElement("Header");
      FXmlTextWriter.WriteAttributeString("x", "Margin", null, "0.4921259845");
      FXmlTextWriter.WriteEndElement(); //</Header>
      //
      FXmlTextWriter.WriteStartElement("Footer");
      FXmlTextWriter.WriteAttributeString("x", "Margin", null, "0.4921259845");
      FXmlTextWriter.WriteEndElement(); //</Footer>
      //
      FXmlTextWriter.WriteStartElement("PageMargins");
      FXmlTextWriter.WriteAttributeString("x", "Bottom", null, "0.984251969");
      FXmlTextWriter.WriteAttributeString("x", "Left", null, "0.78740157499999996");
      FXmlTextWriter.WriteAttributeString("x", "Right", null, "0.78740157499999996");
      FXmlTextWriter.WriteAttributeString("x", "Top", null, "0.984251969");
      FXmlTextWriter.WriteEndElement(); //</PageMargins>
      //
      FXmlTextWriter.WriteEndElement(); //</PageSetup>
      FXmlTextWriter.WriteElementString("Selected", null);
      //
      FXmlTextWriter.WriteStartElement("Panes");
      //
      FXmlTextWriter.WriteStartElement("Pane");
      FXmlTextWriter.WriteElementString("Number", "1");
      FXmlTextWriter.WriteElementString("ActiveRow", "1");
      FXmlTextWriter.WriteElementString("ActiveCol", "1");
      FXmlTextWriter.WriteEndElement(); //</Pane>
      //
      FXmlTextWriter.WriteEndElement(); //</Panes>
      FXmlTextWriter.WriteElementString("ProtectObjects", "False");
      FXmlTextWriter.WriteElementString("ProtectScenarios", "False");
      FXmlTextWriter.WriteEndElement(); //</WorksheetOptions>
      //
      FXmlTextWriter.WriteEndElement(); //</Worksheet>
      //
      return true;
    }



    private Boolean WriteWorkbook(String nameworksheet, Byte[,] matrix)
    {
      FXmlTextWriter.WriteStartElement("Workbook", "urn:schemas-microsoft-com:office:spreadsheet");
      //
      FXmlTextWriter.WriteAttributeString("xmlns", "o", null, "urn:schemas-microsoft-com:office:office");
      FXmlTextWriter.WriteAttributeString("xmlns", "x", null, "urn:schemas-microsoft-com:office:excel");
      FXmlTextWriter.WriteAttributeString("xmlns", "ss", null, "urn:schemas-microsoft-com:office:spreadsheet");
      FXmlTextWriter.WriteAttributeString("xmlns", "html", null, "http://www.w3.org/TR/REC-html40");
      // 
      FXmlTextWriter.WriteStartElement("DocumentProperties", "urn:schemas-microsoft-com:office:office");
      FXmlTextWriter.WriteElementString("Author", Environment.UserName);
      FXmlTextWriter.WriteElementString("LastAuthor", Environment.UserName);
      FXmlTextWriter.WriteElementString("Created", DateTime.Now.ToString("u") + "Z");
      FXmlTextWriter.WriteElementString("Company", "Unknown");
      FXmlTextWriter.WriteElementString("Version", "11.8122");
      FXmlTextWriter.WriteEndElement(); //</DocumentProperties>
      //
      FXmlTextWriter.WriteStartElement("ExcelWorkbook", "urn:schemas-microsoft-com:office:excel");
      FXmlTextWriter.WriteElementString("WindowHeight", "13170");
      FXmlTextWriter.WriteElementString("WindowWidth", "17580");
      FXmlTextWriter.WriteElementString("WindowTopX", "120");
      FXmlTextWriter.WriteElementString("WindowTopY", "60");
      FXmlTextWriter.WriteElementString("ProtectStructure", "False");
      FXmlTextWriter.WriteElementString("ProtectWindows", "False");
      FXmlTextWriter.WriteEndElement(); //</ExcelWorkbook>
      //
      FXmlTextWriter.WriteStartElement("Styles");
      //
      FXmlTextWriter.WriteStartElement("Style");
      //
      FXmlTextWriter.WriteAttributeString("ss", "ID", null, "Default");
      FXmlTextWriter.WriteAttributeString("ss", "Name", null, "Normal");
      //
      FXmlTextWriter.WriteStartElement("Alignment");
      FXmlTextWriter.WriteAttributeString("ss", "Vertical", null, "Bottom");
      FXmlTextWriter.WriteEndElement(); //</Alignment>
      //
      FXmlTextWriter.WriteElementString("Borders", null);
      FXmlTextWriter.WriteElementString("Font", null);
      FXmlTextWriter.WriteElementString("Interior", null);
      FXmlTextWriter.WriteElementString("NumberFormat", null);
      FXmlTextWriter.WriteElementString("Protection", null);
      FXmlTextWriter.WriteEndElement(); //</Style>
      // 
      FXmlTextWriter.WriteEndElement(); //</Styles>
      //
      WriteWorksheet(nameworksheet, matrix); // <Worksheet />
      //
      FXmlTextWriter.WriteEndElement(); //</Workbook>
      return true;
    }

    private Boolean WriteWorkbook(String nameworksheet, UInt16[,] matrix)
    {
      FXmlTextWriter.WriteStartElement("Workbook", "urn:schemas-microsoft-com:office:spreadsheet");
      //
      FXmlTextWriter.WriteAttributeString("xmlns", "o", null, "urn:schemas-microsoft-com:office:office");
      FXmlTextWriter.WriteAttributeString("xmlns", "x", null, "urn:schemas-microsoft-com:office:excel");
      FXmlTextWriter.WriteAttributeString("xmlns", "ss", null, "urn:schemas-microsoft-com:office:spreadsheet");
      FXmlTextWriter.WriteAttributeString("xmlns", "html", null, "http://www.w3.org/TR/REC-html40");
      // 
      FXmlTextWriter.WriteStartElement("DocumentProperties", "urn:schemas-microsoft-com:office:office");
      FXmlTextWriter.WriteElementString("Author", Environment.UserName);
      FXmlTextWriter.WriteElementString("LastAuthor", Environment.UserName);
      FXmlTextWriter.WriteElementString("Created", DateTime.Now.ToString("u") + "Z");
      FXmlTextWriter.WriteElementString("Company", "Unknown");
      FXmlTextWriter.WriteElementString("Version", "11.8122");
      FXmlTextWriter.WriteEndElement(); //</DocumentProperties>
      //
      FXmlTextWriter.WriteStartElement("ExcelWorkbook", "urn:schemas-microsoft-com:office:excel");
      FXmlTextWriter.WriteElementString("WindowHeight", "13170");
      FXmlTextWriter.WriteElementString("WindowWidth", "17580");
      FXmlTextWriter.WriteElementString("WindowTopX", "120");
      FXmlTextWriter.WriteElementString("WindowTopY", "60");
      FXmlTextWriter.WriteElementString("ProtectStructure", "False");
      FXmlTextWriter.WriteElementString("ProtectWindows", "False");
      FXmlTextWriter.WriteEndElement(); //</ExcelWorkbook>
      //
      FXmlTextWriter.WriteStartElement("Styles");
      //
      FXmlTextWriter.WriteStartElement("Style");
      //
      FXmlTextWriter.WriteAttributeString("ss", "ID", null, "Default");
      FXmlTextWriter.WriteAttributeString("ss", "Name", null, "Normal");
      //
      FXmlTextWriter.WriteStartElement("Alignment");
      FXmlTextWriter.WriteAttributeString("ss", "Vertical", null, "Bottom");
      FXmlTextWriter.WriteEndElement(); //</Alignment>
      //
      FXmlTextWriter.WriteElementString("Borders", null);
      FXmlTextWriter.WriteElementString("Font", null);
      FXmlTextWriter.WriteElementString("Interior", null);
      FXmlTextWriter.WriteElementString("NumberFormat", null);
      FXmlTextWriter.WriteElementString("Protection", null);
      FXmlTextWriter.WriteEndElement(); //</Style>
      // 
      FXmlTextWriter.WriteEndElement(); //</Styles>
      //
      WriteWorksheet(nameworksheet, matrix); // <Worksheet />
      //
      FXmlTextWriter.WriteEndElement(); //</Workbook>
      return true;
    }
    //
    //-----------------------------------------------------------------------
    //  Segment - Public
    //-----------------------------------------------------------------------
    //
    public Boolean WriteMatrixByte(String fileentry, 
                                   String nameworksheet, 
                                   Byte[,] matrix)
    {
      try
      {
        FXmlTextWriter = new XmlTextWriter(fileentry, Encoding.UTF8);
        //
        FXmlTextWriter.Formatting = Formatting.Indented;
        FXmlTextWriter.WriteStartDocument();
        FXmlTextWriter.WriteProcessingInstruction("mso-application", "progid=\"Excel.Sheet\"");
        //
        WriteWorkbook(nameworksheet, matrix);
        //
        FXmlTextWriter.Flush();
        FXmlTextWriter.Close();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean WriteMatrixUInt16(String fileentry,
                                     String nameworksheet,
                                     UInt16[,] matrix)
    {
      try
      {
        FXmlTextWriter = new XmlTextWriter(fileentry, Encoding.UTF8);
        //
        FXmlTextWriter.Formatting = Formatting.Indented;
        FXmlTextWriter.WriteStartDocument();
        FXmlTextWriter.WriteProcessingInstruction("mso-application", "progid=\"Excel.Sheet\"");
        //
        WriteWorkbook(nameworksheet, matrix);
        //
        FXmlTextWriter.Flush();
        FXmlTextWriter.Close();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean ReadMatrixUInt16(String fileentry, 
                                    out UInt16[,] matrix)
    {
      try
      {
        matrix = null;
        var connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileentry + ";Extended Properties=\"Excel 12.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text\""; ;
        using (var conn = new OleDbConnection(connectionString))
        {
          conn.Open();

          var sheets = conn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
          using (var cmd = conn.CreateCommand())
          {
            cmd.CommandText = "SELECT * FROM [" + sheets.Rows[0]["TABLE_NAME"].ToString() + "] ";

            var adapter = new OleDbDataAdapter(cmd);
            var ds = new DataSet();
            adapter.Fill(ds);

            // debug ok Console.WriteLine(ds.GetXml());
            DataTable DT =  ds.Tables[0];
            Int32 RC = DT.Rows.Count;
            Int32 CC = DT.Columns.Count;
            matrix = new UInt16[RC, CC];
            for (Int32 RI = 0; RI < RC; RI++)
            {
              DataRow DR = DT.Rows[RI];
              for (Int32 CI = 0; CI < CC; CI++)
              {
                Double DC  = (Double)DR.ItemArray[CI];
                matrix[RI, CI] = (UInt16)DC;
              }
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        matrix = null;
        return false;
      }
    }


  }
}
