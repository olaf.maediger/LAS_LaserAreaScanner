﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
//
using IPPalette;
using IPBitmap;
//
namespace UCBitmap1KTruecolor
{
  public partial class CUCBitmap1KTruecolor : UserControl
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    private String SECTION_LIBRARY = "CUCBitmap1KTruecolor";
    //
    private const Int32 INIT_ROWCOUNT = 240;  // 3; // debug
    private const Int32 INIT_COLCOUNT = 320;  // 4; // debug
    //
    //private String NAME_HEIGHTCOLORED = "HeightColored";
    //private Int32 INIT_HEIGHTCOLORED = 340;
    //
    //-----------------------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private Random FRandom;
    private CBitmap1KTruecolor FBitmap1KTruecolor;
    //
    //-----------------------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------------------
    //
    public CUCBitmap1KTruecolor()
    {
      InitializeComponent();
      FRandom = new Random(1234);
      btnCreateBitmap1KRed_Click(this, null);
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      //Int32 IValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      //IValue = INIT_HEIGHTCOLORED;
      //Result &= initdata.ReadInt32(NAME_HEIGHTCOLORED, out IValue, IValue);
      //pbxImageTruecolor.Height = IValue;
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      //Result &= initdata.WriteInt32(NAME_HEIGHTCOLORED, pbxImageTruecolor.Height);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------------------
    //
    private void BuildBitmap1KLinear(Color color)
    {
      Int32 RC = INIT_ROWCOUNT;
      Int32 CC = INIT_COLCOUNT;
      UInt16[,] Matrix = new UInt16[RC, CC];
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          Matrix[RI, CI] = (UInt16)((CI * (CIPPalette1K.COLORRANGE_GDI - 1)) / (CC - 1));
        }
      }
      Bitmap BitmapColored = CBitmap1KPalette.CreateBitmapColoredFromMatrix(Matrix, color);
      FBitmap1KTruecolor = new CBitmap1KTruecolor(BitmapColored);
      FBitmap1KTruecolor.SetOnBitmapTruecolorChanged(Bitmap1KTruecolorOnBitmapTruecolorChanged);
      pbxImageTruecolor.Invalidate();
    }

    private void BuildBitmap1KInverse(Color color)
    {
      Int32 RC = INIT_ROWCOUNT;
      Int32 CC = INIT_COLCOUNT;
      UInt16[,] Matrix = new UInt16[RC, CC];
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          Matrix[RI, CI] = (UInt16)((CIPPalette1K.COLORRANGE_GDI - 1) -
                                     CI * (CIPPalette1K.COLORRANGE_GDI - 1) / (CC - 1));
        }
      }
      Bitmap BitmapColored = CBitmap1KPalette.CreateBitmapColoredFromMatrix(Matrix, color);
      FBitmap1KTruecolor = new CBitmap1KTruecolor(BitmapColored);
      FBitmap1KTruecolor.SetOnBitmapTruecolorChanged(Bitmap1KTruecolorOnBitmapTruecolorChanged);
      pbxImageTruecolor.Invalidate();
    }

    private void BuildBitmap1KRandom(Color color)
    {
      Int32 RC = INIT_ROWCOUNT;
      Int32 CC = INIT_COLCOUNT;
      UInt16[,] Matrix = new UInt16[RC, CC];
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          Byte RColorEntry = (Byte)FRandom.Next(0, 256);
          Matrix[RI, CI] = (UInt16)(RColorEntry * (CIPPalette1K.COLORRANGE_GDI - 1) / 255);
        }
      }
      Bitmap BitmapColored = CBitmap1KPalette.CreateBitmapColoredFromMatrix(Matrix, color);
      FBitmap1KTruecolor = new CBitmap1KTruecolor(BitmapColored);
      FBitmap1KTruecolor.SetOnBitmapTruecolorChanged(Bitmap1KTruecolorOnBitmapTruecolorChanged);
      pbxImageTruecolor.Invalidate();
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------------------
    //
    private void pbxImage_Paint(object sender, PaintEventArgs e)
    {
      if (FBitmap1KTruecolor is CBitmap1KTruecolor)
      {
        if (cbxStretchBitmap.Checked)
        {
          e.Graphics.DrawImage(FBitmap1KTruecolor.BitmapTruecolor, pbxImageTruecolor.ClientRectangle);
        }
        else
        {
          e.Graphics.DrawImage(FBitmap1KTruecolor.BitmapTruecolor, 0, 0);
        }
      }
    }
    //
    //--------------------------------------------------
    //  Segment - Callback
    //--------------------------------------------------
    //
    private void Bitmap1KTruecolorOnBitmapTruecolorChanged()
    {
      pbxImageTruecolor.Invalidate();
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Menu - Create
    //-----------------------------------------------------------------------
    //
    private void btnCreateBitmap1KRed_Click(object sender, EventArgs e)
    {
      if (cbxCreateInverse.Checked)
      {
        BuildBitmap1KInverse(Color.Red);
      }
      else
      {
        BuildBitmap1KLinear(Color.Red);
      }
    }

    private void btnCreateBitmap1KGreen_Click(object sender, EventArgs e)
    {
      if (cbxCreateInverse.Checked)
      {
        BuildBitmap1KInverse(Color.Green);
      }
      else
      {
        BuildBitmap1KLinear(Color.Green);
      }
    }

    private void btnCreateBitmap1KBlue_Click(object sender, EventArgs e)
    {
      if (cbxCreateInverse.Checked)
      {
        BuildBitmap1KInverse(Color.Blue);
      }
      else
      {
        BuildBitmap1KLinear(Color.Blue);
      }
    }

    private void btnCreateRandomBitmap1K_Click(object sender, EventArgs e)
    {
      BuildBitmap1KRandom(Color.Magenta);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu - Save - Truecolor
    //------------------------------------------------------------------------
    //
    private void btnSaveBitmap1KTruecolor_Click(object sender, EventArgs e)
    {
      Boolean Result = false;
      String Target = "File";
      if (FBitmap1KTruecolor is CBitmap1KTruecolor)
      {
        if (rbtFormatBmpFile.Checked)
        {
          Target = rbtFormatBmpFile.Text;
          Result = FBitmap1KTruecolor.SaveTruecolorToBmpFile("Bitmap1KTruecolor.bmp");
        }
        else
          if (rbtFormatTifFile.Checked)
          {
            Target = rbtFormatTifFile.Text;
            Result = FBitmap1KTruecolor.SaveTruecolorToTifFile("Bitmap1KTruecolor.tif");
          }
          else
            if (rbtFormatPngFile.Checked)
            {
              Target = rbtFormatPngFile.Text;
              Result = FBitmap1KTruecolor.SaveTruecolorToPngFile("Bitmap1KTruecolor.png");
            }
            else
              if (rbtFormatTextFile.Checked)
              {
                Target = rbtFormatTextFile.Text;
                Result = FBitmap1KTruecolor.SaveTruecolorToTextFile("Bitmap1KTruecolor.txt");
              }
              else
                if (rbtFormatTaggedBinaryFile.Checked)
                {
                  Target = rbtFormatTaggedBinaryFile.Text;
                  Result = FBitmap1KTruecolor.SaveTruecolorToTaggedBinaryFile("Bitmap1KTruecolor.tbf");
                }
                else
                  if (rbtFormatExcelFile.Checked)
                  {
                    Target = rbtFormatExcelFile.Text;
                    Result = FBitmap1KTruecolor.SaveTruecolorToExcelFile("Bitmap1KTruecolor.xls");
                  }
                  else
                    if (rbtFormatExcelSheet.Checked)
                    {
                      Target = rbtFormatExcelSheet.Text;
                      Result = FBitmap1KTruecolor.ExportTruecolorToExcelSheet("Bitmap1KTruecolor");
                    }
      }
      if (!Result)
      {
        FNotifier.Error("Main", 15, "Invalid saving Bitmap1KTruecolor to " + Target);
      }
      else
      {
        FNotifier.Write("Main: Correct saving of Bitmap1KTruecolor to " + Target);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu - Load - Truecolor
    //------------------------------------------------------------------------
    //
    private void btnLoadBitmap1KTruecolor_Click(object sender, EventArgs e)
    {
      Boolean Result = false;
      String Target = "File";
      if (FBitmap1KTruecolor is CBitmap1KTruecolor)
      {
        if (rbtFormatBmpFile.Checked)
        {
          Target = rbtFormatBmpFile.Text;
          Result = FBitmap1KTruecolor.LoadTruecolorFromBmpFile("Bitmap1KTruecolor.bmp");
        }
        else
          if (rbtFormatTifFile.Checked)
          {
            Target = rbtFormatTifFile.Text;
            Result = FBitmap1KTruecolor.LoadTruecolorFromTifFile("Bitmap1KTruecolor.tif");
          }
          else
            if (rbtFormatPngFile.Checked)
            {
              Target = rbtFormatPngFile.Text;
              Result = FBitmap1KTruecolor.LoadTruecolorFromPngFile("Bitmap1KTruecolor.png");
            }
            else
              if (rbtFormatTextFile.Checked)
              {
                Target = rbtFormatTextFile.Text;
                Result = FBitmap1KTruecolor.LoadTruecolorFromTextFile("Bitmap1KTruecolor.txt");
              }
              else
                if (rbtFormatTaggedBinaryFile.Checked)
                {
                  Target = rbtFormatTaggedBinaryFile.Text;
                  Result = FBitmap1KTruecolor.LoadTruecolorFromTaggedBinaryFile("Bitmap1KTruecolor.tbf");
                }
                else
                  if (rbtFormatExcelFile.Checked)
                  {
                    Target = rbtFormatExcelFile.Text;
                    Result = FBitmap1KTruecolor.LoadTruecolorFromExcelFile("Bitmap1KTruecolor.xls");
                  }
                  else
                    if (rbtFormatExcelSheet.Checked)
                    {
                      Target = rbtFormatExcelSheet.Text;
                      Result = FBitmap1KTruecolor.ImportTruecolorFromExcelSheet("Bitmap1KTruecolor");
                    }
      }
      if (!Result)
      {
        FNotifier.Error("Main", 14, "Invalid loading Bitmap1KTruecolor from " + Target);
      }
      else
      {
        FNotifier.Write("Main: Correct loading of Bitmap1KTruecolor from " + Target);
      }
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Event - Control
    //-----------------------------------------------------------------------
    //
    private void btnCreateBitmapWidthHeight_Click(object sender, EventArgs e)
    {
      FBitmap1KTruecolor = new CBitmap1KTruecolor((UInt16)nudBitmapWidth.Value, 
                                                  (UInt16)nudBitmapHeight.Value);
    }

    private void btnSelectColor_Click(object sender, EventArgs e)
    {
      DialogColor.Color = btnSelectColor.BackColor;
      if (DialogResult.OK == DialogColor.ShowDialog())
      {
        btnSelectColor.BackColor = DialogColor.Color;
      }
    }

    private void btnSetMatrixTruecolor_Click(object sender, EventArgs e)
    {
      Int32 RowTop = (Int32)nudMatrixTop.Value;
      Int32 RowHeight = (Int32)nudMatrixHeight.Value;
      Int32 ColLeft = (Int32)nudMatrixLeft.Value;
      Int32 ColWidth = (Int32)nudMatrixWidth.Value; // multiply with 4: BGRA
      // NC UInt16[,] MatrixTruecolor = new UInt16[RowCount, ColCount * 4]; -> instead of new: GetMatrix!
      UInt16[,] MatrixTruecolor;
      if (FBitmap1KTruecolor.GetMatrixTruecolor(ColLeft, RowTop, ColWidth, RowHeight, out MatrixTruecolor))
      {
        Color ColorFrame = btnSelectColor.BackColor;
        for (Int32 RI = 0; RI < RowHeight; RI++)
        {
          Int32 MCI = 0;
          for (Int32 CI = 0; CI < ColWidth; CI++)
          { // Blue
            MatrixTruecolor[RI, MCI] = (UInt16)(ColorFrame.B * 32);
            MCI++;
            // Green
            MatrixTruecolor[RI, MCI] = (UInt16)(ColorFrame.G * 32);
            MCI++;
            // Red
            MatrixTruecolor[RI, MCI] = (UInt16)(ColorFrame.R * 32);
            MCI++;
            // Alpha
            MatrixTruecolor[RI, MCI] = 0x1FFF;
            MCI++;
          }
        }
        FBitmap1KTruecolor.SetMatrixTruecolor((Int32)nudMatrixTop.Value,
                                              (Int32)nudMatrixLeft.Value,
                                              MatrixTruecolor);
      }
    }



  }
}
