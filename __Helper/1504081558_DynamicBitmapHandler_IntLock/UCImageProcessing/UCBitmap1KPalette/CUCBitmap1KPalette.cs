﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
//
using IPPalette;
using IPBitmap;
//
namespace UCBitmap1KPalette
{
  public partial class CUCBitmap1KPalette : UserControl
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    private String SECTION_LIBRARY = "CUCBitmap1KPalette";
    //
    private const Int32 INIT_ROWCOUNT = 240;//480;//3;////24;//!!!240;
    private const Int32 INIT_COLCOUNT = 320;//640;//4;//32;//!!!320;
    //
    private String NAME_HEIGHTCOLORED = "HeightColored";
    private Int32 INIT_HEIGHTCOLORED = 340;
    //
    private String NAME_PALETTE = "Palette";
    private EPalette1K INIT_PALETTE = EPalette1K.RGB32;
    //
    private String NAME_STRETCHBITMAP = "StretchBitmap";
    private Boolean INIT_STRETCHBITMAP = false;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private CNotifier FNotifier;
    private Random FRandom;
    private CBitmap1KPalette FBitmap1KPalette;
    //
    //--------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------
    //
    public CUCBitmap1KPalette()
    {
      InitializeComponent();
      FRandom = new Random(3210);
      // Building List of Palettes
      cbxPalette.Items.Clear();
      EPalette1K[] Palettes = (EPalette1K[])Enum.GetValues(typeof(EPalette1K));
      foreach (EPalette1K Palette in Palettes)
      {
        String NamePalette = CIPPalette1K.PaletteToText(Palette);
        cbxPalette.Items.Add(NamePalette);
      }
      nudBitmapWidth.Value = INIT_COLCOUNT;
      nudBitmapHeight.Value = INIT_ROWCOUNT;
      cbxStretchBitmap.Checked = INIT_STRETCHBITMAP;
      btnCreateLinearBitmap1K_Click(this, null);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    private EPalette1K GetPalette()
    {
      return CIPPalette1K.TextToPalette(cbxPalette.Text);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    { // 1412091806
      Boolean Result = true;
      Int32 IValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      IValue = INIT_HEIGHTCOLORED;
      Result &= initdata.ReadInt32(NAME_HEIGHTCOLORED, out IValue, IValue);
      pbxImageColored.Height = IValue;
      //
      String SValue = CIPPalette1K.PaletteToText(INIT_PALETTE);
      Result &= initdata.ReadEnumeration(NAME_PALETTE, out SValue, SValue);
      cbxPalette.SelectedIndex = (Int32)CIPPalette1K.TextToPalette(SValue);
      //
      Boolean BValue = INIT_STRETCHBITMAP;
      Result &= initdata.ReadBoolean(NAME_STRETCHBITMAP, out BValue, BValue);
      cbxStretchBitmap.Checked = BValue;
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    { // 1412091806
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteInt32(NAME_HEIGHTCOLORED, pbxImageColored.Height);
      String SValue = CIPPalette1K.PaletteToText((EPalette1K)cbxPalette.SelectedIndex);
      Result &= initdata.WriteEnumeration(NAME_PALETTE, SValue);
      Result &= initdata.WriteBoolean(NAME_STRETCHBITMAP, cbxStretchBitmap.Checked);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------------------
    //
    private void BuildBitmap1KLinear()
    { // 1412091733
      //!!!!!!!!!!!!!!!!!!!const Int32 CL = 0;
      Int32 CH = (Int32)nudBitmapWidth.Value - 1;
      //
      Int32 RC = (Int32)nudBitmapHeight.Value;
      Int32 CC = 1 + CH;
      UInt16[,] Matrix = new UInt16[RC, CC];
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        { /*!!!!//!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          Matrix[RI, CI] = (UInt16)(0.5 + CBitmap1KBase.INTENSITY_MINIMUM +
                                    (CBitmap1KBase.INTENSITY_MAXIMUM - CBitmap1KBase.INTENSITY_MINIMUM) * 
                                    (CI - CL) / (CH - CL));
           */
        }
      }
      Bitmap BitmapGreyscale = CBitmap1KPalette.CreateBitmapGreyscaleFromMatrix(Matrix);
      FBitmap1KPalette = new CBitmap1KPalette(BitmapGreyscale, GetPalette());// EPalette1K.RGB8);
      FBitmap1KPalette.SetOnBitmapGreyscaleChanged(OnBitmapGreyscaleChanged);
      FBitmap1KPalette.SetOnBitmapColoredChanged(OnBitmapColoredChanged);
      pbxImageGreyscale.Invalidate();
      pbxImageColored.Invalidate();
    }

    private void BuildBitmap1KInverse()
    { // 1412091733
      //!!!!!!!!!!!!!!!!!!!!const Int32 CL = 0;
      Int32 CH = (Int32)nudBitmapWidth.Value - 1;
      //
      Int32 RC = (Int32)nudBitmapHeight.Value;
      Int32 CC = 1 + CH;
      UInt16[,] Matrix = new UInt16[RC, CC];
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          Matrix[RI, CI] = (UInt16)(0.5 + CBitmap1KBase.INTENSITY_MINIMUM +
                                    (CBitmap1KBase.INTENSITY_MAXIMUM - CBitmap1KBase.INTENSITY_MINIMUM) *
                                    (CI - CL) / (CH - CL));
          Matrix[RI, CI] = (UInt16)(CBitmap1KBase.INTENSITY_MAXIMUM - Matrix[RI, CI]);
           */
        }
      }
      Bitmap BitmapGreyscale = CBitmap1KPalette.CreateBitmapGreyscaleFromMatrix(Matrix);
      FBitmap1KPalette = new CBitmap1KPalette(BitmapGreyscale, GetPalette());
      FBitmap1KPalette.SetOnBitmapGreyscaleChanged(OnBitmapGreyscaleChanged);
      FBitmap1KPalette.SetOnBitmapColoredChanged(OnBitmapColoredChanged);
      pbxImageGreyscale.Invalidate();
      pbxImageColored.Invalidate();
    }

    private void BuildBitmap1KRandom()
    { // 1412091748
      Int32 RC = (Int32)nudBitmapHeight.Value;
      Int32 CC = (Int32)nudBitmapWidth.Value;
      UInt16[,] Matrix = new UInt16[RC, CC];
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          /*//!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          Matrix[RI, CI] = (UInt16)FRandom.Next(CBitmap1KBase.INTENSITY_MINIMUM, 
                                                1 + CBitmap1KBase.INTENSITY_MAXIMUM);          
           */
        }
      }
      Bitmap BitmapGreyscale = CBitmap1KPalette.CreateBitmapGreyscaleFromMatrix(Matrix);
      FBitmap1KPalette = new CBitmap1KPalette(BitmapGreyscale, GetPalette());
      FBitmap1KPalette.SetOnBitmapGreyscaleChanged(OnBitmapGreyscaleChanged);
      FBitmap1KPalette.SetOnBitmapColoredChanged(OnBitmapColoredChanged);
      pbxImageGreyscale.Invalidate();
      pbxImageColored.Invalidate();
    }
    //
    //--------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------
    //
    private void cbxStretchBitmap_CheckedChanged(object sender, EventArgs e)
    { // 1412091806
      pbxImageGreyscale.Invalidate();
      pbxImageColored.Invalidate();
    }

    private void pbxImageGreyscale_Paint(object sender, PaintEventArgs e)
    { // 1412091806
      if (FBitmap1KPalette is CBitmap1KPalette)
      {
        if (cbxStretchBitmap.Checked)
        {
          e.Graphics.DrawImage(FBitmap1KPalette.BitmapGreyscale, pbxImageGreyscale.ClientRectangle);
        }
        else
        {
          e.Graphics.DrawImage(FBitmap1KPalette.BitmapGreyscale, 0, 0);
        }
      }
    }

    private void pbxImageColored_Paint(object sender, PaintEventArgs e)
    { // 1412091806
      if (FBitmap1KPalette is CBitmap1KPalette)
      {
        if (cbxStretchBitmap.Checked)
        {
          e.Graphics.DrawImage(FBitmap1KPalette.BitmapColored, pbxImageGreyscale.ClientRectangle);
        }
        else
        {
          e.Graphics.DrawImage(FBitmap1KPalette.BitmapColored, 0, 0);
        }
      }
    }

    private void cbxPalette_SelectedIndexChanged(object sender, EventArgs e)
    { // 1412091806
      if (FBitmap1KPalette is CBitmap1KPalette)
      {
        ((CBitmap1KPalette)FBitmap1KPalette).SetPalette1K(GetPalette());
      }
    }

    private void splView_SplitterMoved(object sender, SplitterEventArgs e)
    { // 1412091806
      pbxImageGreyscale.Invalidate();
      pbxImageColored.Invalidate();
    }
    //
    //--------------------------------------------------
    //  Segment - Callback
    //--------------------------------------------------
    //
    private void OnBitmapGreyscaleChanged()
    {
      pbxImageGreyscale.Invalidate();
    }

    private void OnBitmapColoredChanged()
    {
      pbxImageColored.Invalidate();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu - Create
    //------------------------------------------------------------------------
    //
    //
    //-----------------------------------------------------------------------
    //  Section - Menu - Create
    //-----------------------------------------------------------------------
    //
    private void btnCreateLinearBitmap1K_Click(object sender, EventArgs e)
    { // 1412091806
      if (cbxCreateInverse.Checked)
      {
        BuildBitmap1KInverse();
      }
      else
      {
        BuildBitmap1KLinear();
      }
    }

    private void btnCreateRandomBitmap1K_Click(object sender, EventArgs e)
    { // 1412091806
      BuildBitmap1KRandom();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu - Save - Greyscale
    //------------------------------------------------------------------------
    //
    private void btnSaveBitmap1KPaletteGreyscale_Click(object sender, EventArgs e)
    {
      Boolean Result = false;
      String Target = "File";
      if (FBitmap1KPalette is CBitmap1KPalette)
      {
        if (rbtFormatBmpFile.Checked)
        {
          Target = rbtFormatBmpFile.Text;
          Result = FBitmap1KPalette.SaveGreyscaleToBmpFile("Bitmap1KPaletteGreyscale.bmp");
        }
        else
          if (rbtFormatTifFile.Checked)
          {
            Target = rbtFormatTifFile.Text;
            Result = FBitmap1KPalette.SaveGreyscaleToTifFile("Bitmap1KPaletteGreyscale.tif");
          }
          else
            if (rbtFormatPngFile.Checked)
            {
              Target = rbtFormatPngFile.Text;
              Result = FBitmap1KPalette.SaveGreyscaleToPngFile("Bitmap1KPaletteGreyscale.png");
            }
            else
              if (rbtFormatTextFile.Checked)
              { // 1412091749
                Target = rbtFormatTextFile.Text;
                Result = FBitmap1KPalette.SaveGreyscaleToTextFile("Bitmap1KPaletteGreyscale.txt");
              }
              else
                if (rbtFormatTaggedBinaryFile.Checked)
                {
                  Target = rbtFormatTaggedBinaryFile.Text;
                  Result = FBitmap1KPalette.SaveGreyscaleToTaggedBinaryFile("Bitmap1KPaletteGreyscale.tbf");
                }
                else
                  if (rbtFormatExcelFile.Checked)
                  {
                    Target = rbtFormatExcelFile.Text;
                    Result = FBitmap1KPalette.SaveGreyscaleToExcelFile("Bitmap1KPaletteGreyscale.xls");
                  }
                  else
                    if (rbtFormatExcelSheet.Checked)
                    {
                      Target = rbtFormatExcelSheet.Text;
                      Result = FBitmap1KPalette.ExportGreyscaleToExcelSheet("Bitmap1KPaletteGreyscale");
                    }
      }
      if (!Result)
      {
        FNotifier.Error("Main", 13, "Invalid saving Bitmap1KPaletteGreyscale to " + Target);
      }
      else
      {
        FNotifier.Write("Main: Correct saving of Bitmap1KPaletteGreyscale to " + Target);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu - Save - Colored
    //------------------------------------------------------------------------
    //
    private void btnSaveBitmap1KPaletteColored_Click(object sender, EventArgs e)
    {
      Boolean Result = false;
      String Target = "File";
      if (FBitmap1KPalette is CBitmap1KPalette)
      {
        if (rbtFormatBmpFile.Checked)
        {
          Target = rbtFormatBmpFile.Text;
          Result = FBitmap1KPalette.SaveColoredToBmpFile("Bitmap1KPaletteColored.bmp");
        }
        else
          if (rbtFormatTifFile.Checked)
          {
            Target = rbtFormatTifFile.Text;
            Result = FBitmap1KPalette.SaveColoredToTifFile("Bitmap1KPaletteColored.tif");
          }
          else
            if (rbtFormatPngFile.Checked)
            {
              Target = rbtFormatPngFile.Text;
              Result = FBitmap1KPalette.SaveColoredToPngFile("Bitmap1KPaletteColored.png");
            }
            else
              if (rbtFormatTextFile.Checked)
              { // 1412091749
                Target = rbtFormatTextFile.Text;
                Result = FBitmap1KPalette.SaveColoredToTextFile("Bitmap1KPaletteColored.txt");
              }
              else
                if (rbtFormatTaggedBinaryFile.Checked)
                {
                  Target = rbtFormatTaggedBinaryFile.Text;
                  Result = FBitmap1KPalette.SaveColoredToTaggedBinaryFile("Bitmap1KPaletteColored.tbf");
                }
                else
                  if (rbtFormatExcelFile.Checked)
                  {
                    Target = rbtFormatExcelFile.Text;
                    Result = FBitmap1KPalette.SaveColoredToExcelFile("Bitmap1KPaletteColored.xls");
                  }
                  else
                    if (rbtFormatExcelSheet.Checked)
                    {
                      Target = rbtFormatExcelSheet.Text;
                      Result = FBitmap1KPalette.ExportColoredToExcelSheet("Bitmap1KPaletteColored");
                    }
      }
      if (!Result)
      {
        FNotifier.Error("Main", 15, "Invalid saving Bitmap1KPaletteColored to " + Target);
      }
      else
      {
        FNotifier.Write("Main: Correct saving of Bitmap1KPaletteColored to " + Target);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu - Load - Greyscale
    //------------------------------------------------------------------------
    // 1412161402 - Test!
    private void btnLoadBitmap1KPaletteGreyscale_Click(object sender, EventArgs e)
    {
      Boolean Result = false;
      String Target = "File";
      if (FBitmap1KPalette is CBitmap1KPalette)
      {
        if (rbtFormatBmpFile.Checked)
        {
          Target = rbtFormatBmpFile.Text;
          Result = FBitmap1KPalette.LoadGreyscaleFromBmpFile("Bitmap1KPaletteGreyscale.bmp");
        }
        else
          if (rbtFormatTifFile.Checked)
          {
            Target = rbtFormatTifFile.Text;
            Result = FBitmap1KPalette.LoadGreyscaleFromTifFile("Bitmap1KPaletteGreyscale.tif");
          }
          else
            if (rbtFormatPngFile.Checked)
            {
              Target = rbtFormatPngFile.Text;
              Result = FBitmap1KPalette.LoadGreyscaleFromPngFile("Bitmap1KPaletteGreyscale.png");
            }
            else
              if (rbtFormatTextFile.Checked)
              { // 1412161402 - OK
                Target = rbtFormatTextFile.Text;
                Result = FBitmap1KPalette.LoadGreyscaleFromTextFile("Bitmap1KPaletteGreyscale.txt");
              }
              else
                if (rbtFormatTaggedBinaryFile.Checked)
                {
                  Target = rbtFormatTaggedBinaryFile.Text;
                  Result = FBitmap1KPalette.LoadGreyscaleFromTaggedBinaryFile("Bitmap1KPaletteGreyscale.tbf");
                }
                else
                  if (rbtFormatExcelFile.Checked)
                  {
                    Target = rbtFormatExcelFile.Text;
                    Result = FBitmap1KPalette.LoadGreyscaleFromExcelFile("Bitmap1KPaletteGreyscale.xls");
                  }
                  else
                    if (rbtFormatExcelSheet.Checked)
                    {
                      Target = rbtFormatExcelSheet.Text;
                      Result = FBitmap1KPalette.ImportGreyscaleFromExcelSheet("Bitmap1KPaletteGreyscale");
                    }
      }
      if (!Result)
      {
        FNotifier.Error("Main", 14, "Invalid loading Bitmap1KPaletteGreyscale from " + Target);
      }
      else
      {
        FNotifier.Write("Main: Correct loading of Bitmap1KPaletteGreyscale from " + Target);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu - Load - Colored
    //------------------------------------------------------------------------
    //
    private void btnLoadBitmap1KPaletteColored_Click(object sender, EventArgs e)
    {
      Boolean Result = false;
      String Target = "File";
      if (FBitmap1KPalette is CBitmap1KPalette)
      {
        if (rbtFormatBmpFile.Checked)
        {
          Target = rbtFormatBmpFile.Text;
          Result = FBitmap1KPalette.LoadColoredFromBmpFile("Bitmap1KPaletteColored.bmp");
        }
        else
          if (rbtFormatTifFile.Checked)
          {
            Target = rbtFormatTifFile.Text;
            Result = FBitmap1KPalette.LoadColoredFromTifFile("Bitmap1KPaletteColored.tif");
          }
          else
            if (rbtFormatPngFile.Checked)
            {
              Target = rbtFormatPngFile.Text;
              Result = FBitmap1KPalette.LoadColoredFromPngFile("Bitmap1KPaletteColored.png");
            }
            else
              if (rbtFormatTextFile.Checked)
              {
                Target = rbtFormatTextFile.Text;
                Result = FBitmap1KPalette.LoadColoredFromTextFile("Bitmap1KPaletteColored.txt");
              }
              else
                if (rbtFormatTaggedBinaryFile.Checked)
                {
                  Target = rbtFormatTaggedBinaryFile.Text;
                  Result = FBitmap1KPalette.LoadColoredFromTaggedBinaryFile("Bitmap1KPaletteColored.tbf");
                }
                else
                  if (rbtFormatExcelFile.Checked)
                  {
                    Target = rbtFormatExcelFile.Text;
                    Result = FBitmap1KPalette.LoadColoredFromExcelFile("Bitmap1KPaletteColored.xls");
                  }
                  else
                    if (rbtFormatExcelSheet.Checked)
                    {
                      Target = rbtFormatExcelSheet.Text;
                      Result = FBitmap1KPalette.ImportColoredFromExcelSheet("Bitmap1KPaletteColored");
                    }
      }
      if (!Result)
      {
        FNotifier.Error("Main", 14, "Invalid loading Bitmap1KPaletteColored from " + Target);
      }
      else
      {
        FNotifier.Write("Main: Correct loading of Bitmap1KPaletteColored from " + Target);
      }
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Matrix - Frame
    //-----------------------------------------------------------------------
    //
    private void btnSelectColor_Click(object sender, EventArgs e)
    { // 1412101403 - test!!!
      DialogColor.Color = btnSelectColor.BackColor;
      if (DialogResult.OK == DialogColor.ShowDialog())
      {
        btnSelectColor.BackColor = Color.FromArgb(DialogColor.Color.R, 
                                                  DialogColor.Color.G, 
                                                  DialogColor.Color.B);
      }
    }

    private void btnSetMatrixQuadGreyscale_Click(object sender, EventArgs e)
    { // 1412101403 - test!!!
      const Int32 SIZE_QUAD = 4;
      const Int32 OFFSET_QUAD = 2;
      Int32 RowTop = (Int32)nudMatrixTop.Value;
      Int32 RowHeight = (Int32)nudMatrixHeight.Value;
      Int32 ColLeft = (Int32)nudMatrixLeft.Value;
      Int32 ColWidth = (Int32)nudMatrixWidth.Value; 
      UInt16[,] MatrixQuadGreyscale;
      if (FBitmap1KPalette.GetMatrixQuadGreyscale(ColLeft, RowTop, ColWidth, RowHeight, 
                                                  out MatrixQuadGreyscale))
      { // 0x00..0xFF -> 0x0000..0x03FF -> OFFSET_QUAD + SIZE_QUAD * Grey
        UInt16 A = (UInt16)(OFFSET_QUAD + SIZE_QUAD * btnSelectColor.BackColor.A);
        UInt16 R = (UInt16)(OFFSET_QUAD + SIZE_QUAD * btnSelectColor.BackColor.R);
        UInt16 G = (UInt16)(OFFSET_QUAD + SIZE_QUAD * btnSelectColor.BackColor.G);
        UInt16 B = (UInt16)(OFFSET_QUAD + SIZE_QUAD * btnSelectColor.BackColor.B);        
        // Grey = Average(R, G, B)
        UInt16 GreyFrame = (UInt16)(0.33 * (R + G + B));
        for (Int32 RI = 0; RI < RowHeight; RI++)
        {
          for (Int32 CI = 0; CI < SIZE_QUAD * ColWidth; CI += SIZE_QUAD)
          { // Matrix Quad AGGG
            MatrixQuadGreyscale[RI, CI] = GreyFrame;
          }
        }
        FBitmap1KPalette.SetMatrixQuadGreyscale((Int32)nudMatrixLeft.Value,
                                                (Int32)nudMatrixTop.Value,
                                                MatrixQuadGreyscale);
      }
    }

    private void btnSetMatrixQuadColored_Click(object sender, EventArgs e)
    { // 1412101403 - test!!!
      const Int32 SIZE_QUAD = 4;
      const Int32 OFFSET_QUAD = 2;
      Int32 RowTop = (Int32)nudMatrixTop.Value;
      Int32 RowHeight = (Int32)nudMatrixHeight.Value;
      Int32 ColLeft = (Int32)nudMatrixLeft.Value;
      Int32 ColWidth = (Int32)nudMatrixWidth.Value; 
      UInt16[,] MatrixQuadColored; // [Rowheight, SIZE_QUAD * ColWidth]
      if (FBitmap1KPalette.GetMatrixQuadColored(ColLeft, RowTop, ColWidth, RowHeight, 
                                                out MatrixQuadColored))
      { // 0x00..0xFF -> 0x0000..0x03FF -> 2 + 4 * Color
        UInt16 A = (UInt16)(OFFSET_QUAD + SIZE_QUAD * btnSelectColor.BackColor.A);
        UInt16 R = (UInt16)(OFFSET_QUAD + SIZE_QUAD * btnSelectColor.BackColor.R);
        UInt16 G = (UInt16)(OFFSET_QUAD + SIZE_QUAD * btnSelectColor.BackColor.G);
        UInt16 B = (UInt16)(OFFSET_QUAD + SIZE_QUAD * btnSelectColor.BackColor.B);
        for (Int32 RI = 0; RI < RowHeight; RI++)
        {
          for (Int32 CI = 0; CI < SIZE_QUAD * ColWidth; CI += SIZE_QUAD)
          { // Matrix Quad ARGB
            MatrixQuadColored[RI, 0 + CI] = A;
            MatrixQuadColored[RI, 1 + CI] = R;
            MatrixQuadColored[RI, 2 + CI] = G;
            MatrixQuadColored[RI, 3 + CI] = B;
          }
        }
        FBitmap1KPalette.SetMatrixQuadColored((Int32)nudMatrixLeft.Value,
                                              (Int32)nudMatrixTop.Value,
                                              MatrixQuadColored);
      }
    }


    private void btnSetMatrixIndexGreyscale_Click(object sender, EventArgs e)
    {
      Int32 RowTop = (Int32)nudMatrixTop.Value;
      Int32 RowHeight = (Int32)nudMatrixHeight.Value;
      Int32 ColLeft = (Int32)nudMatrixLeft.Value;
      Int32 ColWidth = (Int32)nudMatrixWidth.Value; 
      UInt16[,] MatrixIndexGreyscale;
      if (FBitmap1KPalette.GetMatrixIndexGreyscale(ColLeft, RowTop, ColWidth, RowHeight, 
                                                   out MatrixIndexGreyscale))
      {
      }
    }

    private void btnSetMatrixIndexColored_Click(object sender, EventArgs e)
    {
      Int32 RowTop = (Int32)nudMatrixTop.Value;
      Int32 RowHeight = (Int32)nudMatrixHeight.Value;
      Int32 ColLeft = (Int32)nudMatrixLeft.Value;
      Int32 ColWidth = (Int32)nudMatrixWidth.Value; 
      UInt16[,] MatrixIndexColored;
      if (FBitmap1KPalette.GetMatrixIndexColored(ColLeft, RowTop, ColWidth, RowHeight, 
                                                 out MatrixIndexColored))
      {
      }
    }




  }
}
