﻿namespace UCBitmap1KPalette
{
  partial class CUCBitmap1KPalette
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlTop = new System.Windows.Forms.Panel();
      this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
      this.label10 = new System.Windows.Forms.Label();
      this.btnSetMatrixIndexGreyscale = new System.Windows.Forms.Button();
      this.btnSetMatrixIndexColored = new System.Windows.Forms.Button();
      this.nudBitmapHeight = new System.Windows.Forms.NumericUpDown();
      this.nudBitmapWidth = new System.Windows.Forms.NumericUpDown();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.cbxStretchBitmap = new System.Windows.Forms.CheckBox();
      this.nudMatrixHeight = new System.Windows.Forms.NumericUpDown();
      this.nudMatrixWidth = new System.Windows.Forms.NumericUpDown();
      this.nudMatrixTop = new System.Windows.Forms.NumericUpDown();
      this.nudMatrixLeft = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.btnSelectColor = new System.Windows.Forms.Button();
      this.btnSetMatrixQuadGreyscale = new System.Windows.Forms.Button();
      this.btnSetMatrixQuadColored = new System.Windows.Forms.Button();
      this.cbxCreateInverse = new System.Windows.Forms.CheckBox();
      this.btnCreateRandomBitmap1K = new System.Windows.Forms.Button();
      this.btnCreateBitmap1KLinear = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.cbxPalette = new System.Windows.Forms.ComboBox();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.rbtFormatExcelFile = new System.Windows.Forms.RadioButton();
      this.rbtFormatTextFile = new System.Windows.Forms.RadioButton();
      this.rbtFormatExcelSheet = new System.Windows.Forms.RadioButton();
      this.rbtFormatTaggedBinaryFile = new System.Windows.Forms.RadioButton();
      this.rbtFormatTifFile = new System.Windows.Forms.RadioButton();
      this.rbtFormatPngFile = new System.Windows.Forms.RadioButton();
      this.rbtFormatBmpFile = new System.Windows.Forms.RadioButton();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.btnSaveBitmap1KPaletteGreyscale = new System.Windows.Forms.Button();
      this.btnLoadBitmap1KPaletteGreyscale = new System.Windows.Forms.Button();
      this.btnLoadBitmap1KPaletteColored = new System.Windows.Forms.Button();
      this.btnSaveBitmap1KPaletteColored = new System.Windows.Forms.Button();
      this.pbxImageColored = new System.Windows.Forms.PictureBox();
      this.splView = new System.Windows.Forms.Splitter();
      this.pbxImageGreyscale = new System.Windows.Forms.PictureBox();
      this.DialogColor = new System.Windows.Forms.ColorDialog();
      this.pnlTop.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBitmapHeight)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBitmapWidth)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixHeight)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixWidth)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixTop)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixLeft)).BeginInit();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageColored)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageGreyscale)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlTop
      // 
      this.pnlTop.Controls.Add(this.numericUpDown1);
      this.pnlTop.Controls.Add(this.label10);
      this.pnlTop.Controls.Add(this.btnSetMatrixIndexGreyscale);
      this.pnlTop.Controls.Add(this.btnSetMatrixIndexColored);
      this.pnlTop.Controls.Add(this.nudBitmapHeight);
      this.pnlTop.Controls.Add(this.nudBitmapWidth);
      this.pnlTop.Controls.Add(this.label8);
      this.pnlTop.Controls.Add(this.label9);
      this.pnlTop.Controls.Add(this.cbxStretchBitmap);
      this.pnlTop.Controls.Add(this.nudMatrixHeight);
      this.pnlTop.Controls.Add(this.nudMatrixWidth);
      this.pnlTop.Controls.Add(this.nudMatrixTop);
      this.pnlTop.Controls.Add(this.nudMatrixLeft);
      this.pnlTop.Controls.Add(this.label4);
      this.pnlTop.Controls.Add(this.label5);
      this.pnlTop.Controls.Add(this.label6);
      this.pnlTop.Controls.Add(this.label7);
      this.pnlTop.Controls.Add(this.btnSelectColor);
      this.pnlTop.Controls.Add(this.btnSetMatrixQuadGreyscale);
      this.pnlTop.Controls.Add(this.btnSetMatrixQuadColored);
      this.pnlTop.Controls.Add(this.cbxCreateInverse);
      this.pnlTop.Controls.Add(this.btnCreateRandomBitmap1K);
      this.pnlTop.Controls.Add(this.btnCreateBitmap1KLinear);
      this.pnlTop.Controls.Add(this.label3);
      this.pnlTop.Controls.Add(this.cbxPalette);
      this.pnlTop.Controls.Add(this.groupBox1);
      this.pnlTop.Controls.Add(this.label2);
      this.pnlTop.Controls.Add(this.label1);
      this.pnlTop.Controls.Add(this.btnSaveBitmap1KPaletteGreyscale);
      this.pnlTop.Controls.Add(this.btnLoadBitmap1KPaletteGreyscale);
      this.pnlTop.Controls.Add(this.btnLoadBitmap1KPaletteColored);
      this.pnlTop.Controls.Add(this.btnSaveBitmap1KPaletteColored);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(599, 154);
      this.pnlTop.TabIndex = 0;
      // 
      // numericUpDown1
      // 
      this.numericUpDown1.Location = new System.Drawing.Point(104, 123);
      this.numericUpDown1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericUpDown1.Name = "numericUpDown1";
      this.numericUpDown1.Size = new System.Drawing.Size(46, 20);
      this.numericUpDown1.TabIndex = 86;
      this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown1.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(72, 126);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(33, 13);
      this.label10.TabIndex = 87;
      this.label10.Text = "Index";
      // 
      // btnSetMatrixIndexGreyscale
      // 
      this.btnSetMatrixIndexGreyscale.Location = new System.Drawing.Point(155, 121);
      this.btnSetMatrixIndexGreyscale.Name = "btnSetMatrixIndexGreyscale";
      this.btnSetMatrixIndexGreyscale.Size = new System.Drawing.Size(92, 23);
      this.btnSetMatrixIndexGreyscale.TabIndex = 85;
      this.btnSetMatrixIndexGreyscale.Text = "Set Matrix Index";
      this.btnSetMatrixIndexGreyscale.UseVisualStyleBackColor = true;
      this.btnSetMatrixIndexGreyscale.Click += new System.EventHandler(this.btnSetMatrixIndexGreyscale_Click);
      // 
      // btnSetMatrixIndexColored
      // 
      this.btnSetMatrixIndexColored.Location = new System.Drawing.Point(251, 121);
      this.btnSetMatrixIndexColored.Name = "btnSetMatrixIndexColored";
      this.btnSetMatrixIndexColored.Size = new System.Drawing.Size(91, 23);
      this.btnSetMatrixIndexColored.TabIndex = 84;
      this.btnSetMatrixIndexColored.Text = "Set Matrix Index";
      this.btnSetMatrixIndexColored.UseVisualStyleBackColor = true;
      this.btnSetMatrixIndexColored.Click += new System.EventHandler(this.btnSetMatrixIndexColored_Click);
      // 
      // nudBitmapHeight
      // 
      this.nudBitmapHeight.Location = new System.Drawing.Point(84, 5);
      this.nudBitmapHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudBitmapHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudBitmapHeight.Name = "nudBitmapHeight";
      this.nudBitmapHeight.Size = new System.Drawing.Size(46, 20);
      this.nudBitmapHeight.TabIndex = 82;
      this.nudBitmapHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudBitmapHeight.Value = new decimal(new int[] {
            240,
            0,
            0,
            0});
      // 
      // nudBitmapWidth
      // 
      this.nudBitmapWidth.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.nudBitmapWidth.Location = new System.Drawing.Point(22, 5);
      this.nudBitmapWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudBitmapWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudBitmapWidth.Name = "nudBitmapWidth";
      this.nudBitmapWidth.Size = new System.Drawing.Size(46, 20);
      this.nudBitmapWidth.TabIndex = 80;
      this.nudBitmapWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudBitmapWidth.Value = new decimal(new int[] {
            320,
            0,
            0,
            0});
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(70, 8);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(15, 13);
      this.label8.TabIndex = 83;
      this.label8.Text = "H";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(6, 8);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(18, 13);
      this.label9.TabIndex = 81;
      this.label9.Text = "W";
      // 
      // cbxStretchBitmap
      // 
      this.cbxStretchBitmap.AutoSize = true;
      this.cbxStretchBitmap.Location = new System.Drawing.Point(84, 27);
      this.cbxStretchBitmap.Name = "cbxStretchBitmap";
      this.cbxStretchBitmap.Size = new System.Drawing.Size(60, 17);
      this.cbxStretchBitmap.TabIndex = 72;
      this.cbxStretchBitmap.Text = "Stretch";
      this.cbxStretchBitmap.UseVisualStyleBackColor = true;
      this.cbxStretchBitmap.CheckedChanged += new System.EventHandler(this.cbxStretchBitmap_CheckedChanged);
      // 
      // nudMatrixHeight
      // 
      this.nudMatrixHeight.Location = new System.Drawing.Point(422, 124);
      this.nudMatrixHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudMatrixHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMatrixHeight.Name = "nudMatrixHeight";
      this.nudMatrixHeight.Size = new System.Drawing.Size(46, 20);
      this.nudMatrixHeight.TabIndex = 70;
      this.nudMatrixHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMatrixHeight.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
      // 
      // nudMatrixWidth
      // 
      this.nudMatrixWidth.Location = new System.Drawing.Point(422, 99);
      this.nudMatrixWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudMatrixWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMatrixWidth.Name = "nudMatrixWidth";
      this.nudMatrixWidth.Size = new System.Drawing.Size(46, 20);
      this.nudMatrixWidth.TabIndex = 68;
      this.nudMatrixWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMatrixWidth.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
      // 
      // nudMatrixTop
      // 
      this.nudMatrixTop.Location = new System.Drawing.Point(361, 124);
      this.nudMatrixTop.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudMatrixTop.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMatrixTop.Name = "nudMatrixTop";
      this.nudMatrixTop.Size = new System.Drawing.Size(46, 20);
      this.nudMatrixTop.TabIndex = 66;
      this.nudMatrixTop.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMatrixTop.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
      // 
      // nudMatrixLeft
      // 
      this.nudMatrixLeft.Location = new System.Drawing.Point(360, 98);
      this.nudMatrixLeft.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudMatrixLeft.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMatrixLeft.Name = "nudMatrixLeft";
      this.nudMatrixLeft.Size = new System.Drawing.Size(46, 20);
      this.nudMatrixLeft.TabIndex = 64;
      this.nudMatrixLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMatrixLeft.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(408, 127);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(15, 13);
      this.label4.TabIndex = 71;
      this.label4.Text = "H";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(406, 102);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(18, 13);
      this.label5.TabIndex = 69;
      this.label5.Text = "W";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(348, 127);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(14, 13);
      this.label6.TabIndex = 67;
      this.label6.Text = "T";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(347, 101);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(13, 13);
      this.label7.TabIndex = 65;
      this.label7.Text = "L";
      // 
      // btnSelectColor
      // 
      this.btnSelectColor.BackColor = System.Drawing.Color.White;
      this.btnSelectColor.Location = new System.Drawing.Point(75, 94);
      this.btnSelectColor.Name = "btnSelectColor";
      this.btnSelectColor.Size = new System.Drawing.Size(75, 25);
      this.btnSelectColor.TabIndex = 63;
      this.btnSelectColor.Text = "Color";
      this.btnSelectColor.UseVisualStyleBackColor = false;
      this.btnSelectColor.Click += new System.EventHandler(this.btnSelectColor_Click);
      // 
      // btnSetMatrixQuadGreyscale
      // 
      this.btnSetMatrixQuadGreyscale.Location = new System.Drawing.Point(155, 94);
      this.btnSetMatrixQuadGreyscale.Name = "btnSetMatrixQuadGreyscale";
      this.btnSetMatrixQuadGreyscale.Size = new System.Drawing.Size(92, 23);
      this.btnSetMatrixQuadGreyscale.TabIndex = 61;
      this.btnSetMatrixQuadGreyscale.Text = "Set Matrix Quad";
      this.btnSetMatrixQuadGreyscale.UseVisualStyleBackColor = true;
      this.btnSetMatrixQuadGreyscale.Click += new System.EventHandler(this.btnSetMatrixQuadGreyscale_Click);
      // 
      // btnSetMatrixQuadColored
      // 
      this.btnSetMatrixQuadColored.Location = new System.Drawing.Point(251, 94);
      this.btnSetMatrixQuadColored.Name = "btnSetMatrixQuadColored";
      this.btnSetMatrixQuadColored.Size = new System.Drawing.Size(91, 23);
      this.btnSetMatrixQuadColored.TabIndex = 60;
      this.btnSetMatrixQuadColored.Text = "Set Matrix Quad";
      this.btnSetMatrixQuadColored.UseVisualStyleBackColor = true;
      this.btnSetMatrixQuadColored.Click += new System.EventHandler(this.btnSetMatrixQuadColored_Click);
      // 
      // cbxCreateInverse
      // 
      this.cbxCreateInverse.AutoSize = true;
      this.cbxCreateInverse.Location = new System.Drawing.Point(22, 27);
      this.cbxCreateInverse.Name = "cbxCreateInverse";
      this.cbxCreateInverse.Size = new System.Drawing.Size(61, 17);
      this.cbxCreateInverse.TabIndex = 58;
      this.cbxCreateInverse.Text = "Inverse";
      this.cbxCreateInverse.UseVisualStyleBackColor = true;
      // 
      // btnCreateRandomBitmap1K
      // 
      this.btnCreateRandomBitmap1K.Location = new System.Drawing.Point(5, 67);
      this.btnCreateRandomBitmap1K.Name = "btnCreateRandomBitmap1K";
      this.btnCreateRandomBitmap1K.Size = new System.Drawing.Size(145, 23);
      this.btnCreateRandomBitmap1K.TabIndex = 56;
      this.btnCreateRandomBitmap1K.Text = "Create Bitmap1K Random";
      this.btnCreateRandomBitmap1K.UseVisualStyleBackColor = true;
      this.btnCreateRandomBitmap1K.Click += new System.EventHandler(this.btnCreateRandomBitmap1K_Click);
      // 
      // btnCreateBitmap1KLinear
      // 
      this.btnCreateBitmap1KLinear.Location = new System.Drawing.Point(5, 44);
      this.btnCreateBitmap1KLinear.Name = "btnCreateBitmap1KLinear";
      this.btnCreateBitmap1KLinear.Size = new System.Drawing.Size(145, 23);
      this.btnCreateBitmap1KLinear.TabIndex = 54;
      this.btnCreateBitmap1KLinear.Text = "Create Bitmap1K Linear";
      this.btnCreateBitmap1KLinear.UseVisualStyleBackColor = true;
      this.btnCreateBitmap1KLinear.Click += new System.EventHandler(this.btnCreateLinearBitmap1K_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(153, 7);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(40, 13);
      this.label3.TabIndex = 53;
      this.label3.Text = "Palette";
      // 
      // cbxPalette
      // 
      this.cbxPalette.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPalette.FormattingEnabled = true;
      this.cbxPalette.Items.AddRange(new object[] {
            "Greyscale",
            "Rainbow",
            "Spectrum",
            "RGB8Bright",
            "RGB8Light",
            "RGB8",
            "RGB32Bright",
            "RGB32Light",
            "RGB32",
            "Random",
            "UserDefined"});
      this.cbxPalette.Location = new System.Drawing.Point(194, 4);
      this.cbxPalette.Name = "cbxPalette";
      this.cbxPalette.Size = new System.Drawing.Size(147, 21);
      this.cbxPalette.TabIndex = 43;
      this.cbxPalette.SelectedIndexChanged += new System.EventHandler(this.cbxPalette_SelectedIndexChanged);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.rbtFormatExcelFile);
      this.groupBox1.Controls.Add(this.rbtFormatTextFile);
      this.groupBox1.Controls.Add(this.rbtFormatExcelSheet);
      this.groupBox1.Controls.Add(this.rbtFormatTaggedBinaryFile);
      this.groupBox1.Controls.Add(this.rbtFormatTifFile);
      this.groupBox1.Controls.Add(this.rbtFormatPngFile);
      this.groupBox1.Controls.Add(this.rbtFormatBmpFile);
      this.groupBox1.Location = new System.Drawing.Point(348, 10);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(204, 80);
      this.groupBox1.TabIndex = 51;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = " Format ";
      // 
      // rbtFormatExcelFile
      // 
      this.rbtFormatExcelFile.AutoSize = true;
      this.rbtFormatExcelFile.Location = new System.Drawing.Point(13, 56);
      this.rbtFormatExcelFile.Name = "rbtFormatExcelFile";
      this.rbtFormatExcelFile.Size = new System.Drawing.Size(67, 17);
      this.rbtFormatExcelFile.TabIndex = 6;
      this.rbtFormatExcelFile.Text = "ExcelFile";
      this.rbtFormatExcelFile.UseVisualStyleBackColor = true;
      // 
      // rbtFormatTextFile
      // 
      this.rbtFormatTextFile.AutoSize = true;
      this.rbtFormatTextFile.Checked = true;
      this.rbtFormatTextFile.Location = new System.Drawing.Point(13, 36);
      this.rbtFormatTextFile.Name = "rbtFormatTextFile";
      this.rbtFormatTextFile.Size = new System.Drawing.Size(62, 17);
      this.rbtFormatTextFile.TabIndex = 5;
      this.rbtFormatTextFile.TabStop = true;
      this.rbtFormatTextFile.Text = "TextFile";
      this.rbtFormatTextFile.UseVisualStyleBackColor = true;
      // 
      // rbtFormatExcelSheet
      // 
      this.rbtFormatExcelSheet.AutoSize = true;
      this.rbtFormatExcelSheet.Enabled = false;
      this.rbtFormatExcelSheet.Location = new System.Drawing.Point(81, 56);
      this.rbtFormatExcelSheet.Name = "rbtFormatExcelSheet";
      this.rbtFormatExcelSheet.Size = new System.Drawing.Size(79, 17);
      this.rbtFormatExcelSheet.TabIndex = 4;
      this.rbtFormatExcelSheet.Text = "ExcelSheet";
      this.rbtFormatExcelSheet.UseVisualStyleBackColor = true;
      // 
      // rbtFormatTaggedBinaryFile
      // 
      this.rbtFormatTaggedBinaryFile.AutoSize = true;
      this.rbtFormatTaggedBinaryFile.Location = new System.Drawing.Point(81, 36);
      this.rbtFormatTaggedBinaryFile.Name = "rbtFormatTaggedBinaryFile";
      this.rbtFormatTaggedBinaryFile.Size = new System.Drawing.Size(107, 17);
      this.rbtFormatTaggedBinaryFile.TabIndex = 3;
      this.rbtFormatTaggedBinaryFile.Text = "TaggedBinaryFile";
      this.rbtFormatTaggedBinaryFile.UseVisualStyleBackColor = true;
      // 
      // rbtFormatTifFile
      // 
      this.rbtFormatTifFile.AutoSize = true;
      this.rbtFormatTifFile.Location = new System.Drawing.Point(81, 16);
      this.rbtFormatTifFile.Name = "rbtFormatTifFile";
      this.rbtFormatTifFile.Size = new System.Drawing.Size(53, 17);
      this.rbtFormatTifFile.TabIndex = 2;
      this.rbtFormatTifFile.Text = "TifFile";
      this.rbtFormatTifFile.UseVisualStyleBackColor = true;
      // 
      // rbtFormatPngFile
      // 
      this.rbtFormatPngFile.AutoSize = true;
      this.rbtFormatPngFile.Location = new System.Drawing.Point(140, 16);
      this.rbtFormatPngFile.Name = "rbtFormatPngFile";
      this.rbtFormatPngFile.Size = new System.Drawing.Size(60, 17);
      this.rbtFormatPngFile.TabIndex = 1;
      this.rbtFormatPngFile.Text = "PngFile";
      this.rbtFormatPngFile.UseVisualStyleBackColor = true;
      // 
      // rbtFormatBmpFile
      // 
      this.rbtFormatBmpFile.AutoSize = true;
      this.rbtFormatBmpFile.Location = new System.Drawing.Point(13, 16);
      this.rbtFormatBmpFile.Name = "rbtFormatBmpFile";
      this.rbtFormatBmpFile.Size = new System.Drawing.Size(62, 17);
      this.rbtFormatBmpFile.TabIndex = 0;
      this.rbtFormatBmpFile.Text = "BmpFile";
      this.rbtFormatBmpFile.UseVisualStyleBackColor = true;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(254, 31);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(88, 13);
      this.label2.TabIndex = 50;
      this.label2.Text = "Bitmap1KColored";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(153, 31);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(99, 13);
      this.label1.TabIndex = 49;
      this.label1.Text = "Bitmap1KGreyscale";
      // 
      // btnSaveBitmap1KPaletteGreyscale
      // 
      this.btnSaveBitmap1KPaletteGreyscale.Location = new System.Drawing.Point(155, 45);
      this.btnSaveBitmap1KPaletteGreyscale.Name = "btnSaveBitmap1KPaletteGreyscale";
      this.btnSaveBitmap1KPaletteGreyscale.Size = new System.Drawing.Size(92, 23);
      this.btnSaveBitmap1KPaletteGreyscale.TabIndex = 48;
      this.btnSaveBitmap1KPaletteGreyscale.Text = "Save";
      this.btnSaveBitmap1KPaletteGreyscale.UseVisualStyleBackColor = true;
      this.btnSaveBitmap1KPaletteGreyscale.Click += new System.EventHandler(this.btnSaveBitmap1KPaletteGreyscale_Click);
      // 
      // btnLoadBitmap1KPaletteGreyscale
      // 
      this.btnLoadBitmap1KPaletteGreyscale.Location = new System.Drawing.Point(155, 68);
      this.btnLoadBitmap1KPaletteGreyscale.Name = "btnLoadBitmap1KPaletteGreyscale";
      this.btnLoadBitmap1KPaletteGreyscale.Size = new System.Drawing.Size(92, 23);
      this.btnLoadBitmap1KPaletteGreyscale.TabIndex = 47;
      this.btnLoadBitmap1KPaletteGreyscale.Text = "Load";
      this.btnLoadBitmap1KPaletteGreyscale.UseVisualStyleBackColor = true;
      this.btnLoadBitmap1KPaletteGreyscale.Click += new System.EventHandler(this.btnLoadBitmap1KPaletteGreyscale_Click);
      // 
      // btnLoadBitmap1KPaletteColored
      // 
      this.btnLoadBitmap1KPaletteColored.Location = new System.Drawing.Point(251, 68);
      this.btnLoadBitmap1KPaletteColored.Name = "btnLoadBitmap1KPaletteColored";
      this.btnLoadBitmap1KPaletteColored.Size = new System.Drawing.Size(92, 23);
      this.btnLoadBitmap1KPaletteColored.TabIndex = 45;
      this.btnLoadBitmap1KPaletteColored.Text = "Load Bitmap1KPaletteColored";
      this.btnLoadBitmap1KPaletteColored.UseVisualStyleBackColor = true;
      this.btnLoadBitmap1KPaletteColored.Click += new System.EventHandler(this.btnLoadBitmap1KPaletteColored_Click);
      // 
      // btnSaveBitmap1KPaletteColored
      // 
      this.btnSaveBitmap1KPaletteColored.Location = new System.Drawing.Point(251, 45);
      this.btnSaveBitmap1KPaletteColored.Name = "btnSaveBitmap1KPaletteColored";
      this.btnSaveBitmap1KPaletteColored.Size = new System.Drawing.Size(92, 23);
      this.btnSaveBitmap1KPaletteColored.TabIndex = 44;
      this.btnSaveBitmap1KPaletteColored.Text = "Save";
      this.btnSaveBitmap1KPaletteColored.UseVisualStyleBackColor = true;
      this.btnSaveBitmap1KPaletteColored.Click += new System.EventHandler(this.btnSaveBitmap1KPaletteColored_Click);
      // 
      // pbxImageColored
      // 
      this.pbxImageColored.BackColor = System.Drawing.SystemColors.Info;
      this.pbxImageColored.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxImageColored.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pbxImageColored.Location = new System.Drawing.Point(0, 337);
      this.pbxImageColored.Name = "pbxImageColored";
      this.pbxImageColored.Size = new System.Drawing.Size(599, 295);
      this.pbxImageColored.TabIndex = 7;
      this.pbxImageColored.TabStop = false;
      this.pbxImageColored.Paint += new System.Windows.Forms.PaintEventHandler(this.pbxImageColored_Paint);
      // 
      // splView
      // 
      this.splView.BackColor = System.Drawing.SystemColors.Control;
      this.splView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.splView.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splView.Location = new System.Drawing.Point(0, 334);
      this.splView.Name = "splView";
      this.splView.Size = new System.Drawing.Size(599, 3);
      this.splView.TabIndex = 8;
      this.splView.TabStop = false;
      this.splView.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splView_SplitterMoved);
      // 
      // pbxImageGreyscale
      // 
      this.pbxImageGreyscale.BackColor = System.Drawing.SystemColors.Info;
      this.pbxImageGreyscale.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxImageGreyscale.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxImageGreyscale.Location = new System.Drawing.Point(0, 154);
      this.pbxImageGreyscale.Name = "pbxImageGreyscale";
      this.pbxImageGreyscale.Size = new System.Drawing.Size(599, 180);
      this.pbxImageGreyscale.TabIndex = 9;
      this.pbxImageGreyscale.TabStop = false;
      this.pbxImageGreyscale.Paint += new System.Windows.Forms.PaintEventHandler(this.pbxImageGreyscale_Paint);
      // 
      // CUCBitmap1KPalette
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pbxImageGreyscale);
      this.Controls.Add(this.splView);
      this.Controls.Add(this.pbxImageColored);
      this.Controls.Add(this.pnlTop);
      this.Name = "CUCBitmap1KPalette";
      this.Size = new System.Drawing.Size(599, 632);
      this.pnlTop.ResumeLayout(false);
      this.pnlTop.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBitmapHeight)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBitmapWidth)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixHeight)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixWidth)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixTop)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixLeft)).EndInit();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageColored)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageGreyscale)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.ComboBox cbxPalette;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.RadioButton rbtFormatExcelFile;
    private System.Windows.Forms.RadioButton rbtFormatTextFile;
    private System.Windows.Forms.RadioButton rbtFormatExcelSheet;
    private System.Windows.Forms.RadioButton rbtFormatTaggedBinaryFile;
    private System.Windows.Forms.RadioButton rbtFormatTifFile;
    private System.Windows.Forms.RadioButton rbtFormatPngFile;
    private System.Windows.Forms.RadioButton rbtFormatBmpFile;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btnSaveBitmap1KPaletteGreyscale;
    private System.Windows.Forms.Button btnLoadBitmap1KPaletteGreyscale;
    private System.Windows.Forms.Button btnLoadBitmap1KPaletteColored;
    private System.Windows.Forms.Button btnSaveBitmap1KPaletteColored;
    private System.Windows.Forms.PictureBox pbxImageColored;
    private System.Windows.Forms.Splitter splView;
    private System.Windows.Forms.PictureBox pbxImageGreyscale;
    private System.Windows.Forms.CheckBox cbxCreateInverse;
    private System.Windows.Forms.Button btnCreateRandomBitmap1K;
    private System.Windows.Forms.Button btnCreateBitmap1KLinear;
    private System.Windows.Forms.Button btnSetMatrixQuadGreyscale;
    private System.Windows.Forms.Button btnSetMatrixQuadColored;
    private System.Windows.Forms.NumericUpDown nudMatrixHeight;
    private System.Windows.Forms.NumericUpDown nudMatrixWidth;
    private System.Windows.Forms.NumericUpDown nudMatrixTop;
    private System.Windows.Forms.NumericUpDown nudMatrixLeft;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Button btnSelectColor;
    private System.Windows.Forms.ColorDialog DialogColor;
    private System.Windows.Forms.CheckBox cbxStretchBitmap;
    private System.Windows.Forms.NumericUpDown nudBitmapHeight;
    private System.Windows.Forms.NumericUpDown nudBitmapWidth;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Button btnSetMatrixIndexGreyscale;
    private System.Windows.Forms.Button btnSetMatrixIndexColored;
    private System.Windows.Forms.NumericUpDown numericUpDown1;
    private System.Windows.Forms.Label label10;

  }
}
