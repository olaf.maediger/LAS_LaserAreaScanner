﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
//
using IPPalette;
using IPBitmap;
//
namespace CUCBitmap256Truecolor
{
  public partial class CUCBitmap256Truecolor : UserControl
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    private String SECTION_LIBRARY = "CUCBitmap256Truecolor";
    //
    private const Int32 INIT_ROWCOUNT = 240;
    private const Int32 INIT_COLCOUNT = 320;
    //
    //private String NAME_HEIGHTCOLORED = "HeightColored";
    //private Int32 INIT_HEIGHTCOLORED = 340;
    //
    //-----------------------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private Random FRandom;
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!private CBitmapTruecolorBase FBitmap256Truecolor;
    //
    //-----------------------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------------------
    //
    public CUCBitmap256Truecolor()
    {
      InitializeComponent();
      FRandom = new Random(1234);
      btnCreateBitmap256Red_Click(this, null);
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      //Int32 IValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      //IValue = INIT_HEIGHTCOLORED;
      //Result &= initdata.ReadInt32(NAME_HEIGHTCOLORED, out IValue, IValue);
      //pbxImageTruecolor.Height = IValue;
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      //Result &= initdata.WriteInt32(NAME_HEIGHTCOLORED, pbxImageTruecolor.Height);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------------------
    //
    private void BuildBitmap256Linear(Color color)
    {
      Int32 RC = INIT_ROWCOUNT;
      Int32 CC = INIT_COLCOUNT;
      Byte[,] Matrix = new Byte[RC, CC];
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          Matrix[RI, CI] = (Byte)((CI * (CIPPalette256.COLORRANGE_GDI - 1)) / (CC - 1));
        }
      }
      Bitmap BitmapColored = CBitmap256Palette.CreateBitmapFromMatrixColored(Matrix, color);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!FBitmap256Truecolor = new CBitmap256TruecolorBase(BitmapColored);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!FBitmap256Truecolor.SetOnBitmapTruecolorChanged(Bitmap256TruecolorOnBitmapTruecolorChanged);
      pbxImageTruecolor.Invalidate();
    }

    private void BuildBitmap256Inverse(Color color)
    {
      Int32 RC = INIT_ROWCOUNT;
      Int32 CC = INIT_COLCOUNT;
      Byte[,] Matrix = new Byte[RC, CC];
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          Matrix[RI, CI] = (Byte)((CIPPalette256.COLORRANGE_GDI - 1) - 
                                   CI * (CIPPalette256.COLORRANGE_GDI - 1) / (CC - 1));
        }
      }
      Bitmap BitmapColored = CBitmap256Palette.CreateBitmapFromMatrixColored(Matrix, color);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!FBitmap256Truecolor = new CBitmap256TruecolorBase(BitmapColored);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!FBitmap256Truecolor.SetOnBitmapTruecolorChanged(Bitmap256TruecolorOnBitmapTruecolorChanged);
      pbxImageTruecolor.Invalidate();
    }

    private void BuildBitmap256Random(Color color)
    {
      Int32 RC = INIT_ROWCOUNT;
      Int32 CC = INIT_COLCOUNT;
      Byte[,] Matrix = new Byte[RC, CC];
      for (Int32 RI = 0; RI < RC; RI++)
      {
        for (Int32 CI = 0; CI < CC; CI++)
        {
          Byte RColorEntry = (Byte)FRandom.Next(0, 256);
          Matrix[RI, CI] = (Byte)(RColorEntry * (CIPPalette256.COLORRANGE_GDI - 1) / 255);
        }
      }
      Bitmap BitmapColored = CBitmap256Palette.CreateBitmapFromMatrixColored(Matrix, color);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!FBitmap256Truecolor = new CBitmap256TruecolorBase(BitmapColored);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!FBitmap256Truecolor.SetOnBitmapTruecolorChanged(Bitmap256TruecolorOnBitmapTruecolorChanged);
      pbxImageTruecolor.Invalidate();
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------------------
    //
    private void pbxImage_Paint(object sender, PaintEventArgs e)
    {
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!if (FBitmap256Truecolor is CBitmapTruecolorBase)
      {
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!e.Graphics.DrawImage(FBitmap256Truecolor.BitmapTruecolor, 0, 0);
        // debug e.Graphics.DrawLine(new Pen(Color.Red, 5), 10, 10, 100, 100);
      }
    }
    //
    //--------------------------------------------------
    //  Segment - Callback
    //--------------------------------------------------
    //
    private void Bitmap256TruecolorOnBitmapTruecolorChanged()
    {
      pbxImageTruecolor.Invalidate();
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Menu - Create
    //-----------------------------------------------------------------------
    //
    private void btnCreateBitmap256Red_Click(object sender, EventArgs e)
    {
      if (cbxCreateInverse.Checked)
      {
        BuildBitmap256Inverse(Color.Red);
      }
      else
      {
        BuildBitmap256Linear(Color.Red);
      }
    }

    private void btnCreateBitmap256Green_Click(object sender, EventArgs e)
    {
      if (cbxCreateInverse.Checked)
      {
        BuildBitmap256Inverse(Color.Green);
      }
      else
      {
        BuildBitmap256Linear(Color.Green);
      }
    }

    private void btnCreateBitmap256Blue_Click(object sender, EventArgs e)
    {
      if (cbxCreateInverse.Checked)
      {
        BuildBitmap256Inverse(Color.Blue);
      }
      else
      {
        BuildBitmap256Linear(Color.Blue);
      }
    }

    private void btnCreateRandomBitmap256_Click(object sender, EventArgs e)
    {
      BuildBitmap256Random(Color.Magenta);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu - Save - Truecolor
    //------------------------------------------------------------------------
    //
    private void btnSaveBitmap256Truecolor_Click(object sender, EventArgs e)
    {
      Boolean Result = false;
      String Target = "File";
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!if (FBitmap256Truecolor is CBitmap256TruecolorBase)
      {
        if (rbtFormatBmpFile.Checked)
        {
          Target = rbtFormatBmpFile.Text;
          //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.SaveTruecolorToBmpFile("Bitmap256Truecolor.bmp");
        }
        else
          if (rbtFormatTifFile.Checked)
          {
            Target = rbtFormatTifFile.Text;
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.SaveTruecolorToTifFile("Bitmap256Truecolor.tif");
          }
          else
            if (rbtFormatPngFile.Checked)
            {
              Target = rbtFormatPngFile.Text;
              //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.SaveTruecolorToPngFile("Bitmap256Truecolor.png");
            }
            else
              if (rbtFormatTextFile.Checked)
              {
                Target = rbtFormatTextFile.Text;
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.SaveTruecolorToTextFile("Bitmap256Truecolor.txt");
              }
              else
                if (rbtFormatTaggedBinaryFile.Checked)
                {
                  Target = rbtFormatTaggedBinaryFile.Text;
                  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.SaveTruecolorToTaggedBinaryFile("Bitmap256Truecolor.tbf");
                }
                else
                  if (rbtFormatExcelFile.Checked)
                  {
                    Target = rbtFormatExcelFile.Text;
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.SaveTruecolorToExcelFile("Bitmap256Truecolor.xls");
                  }
                  else
                    if (rbtFormatExcelSheet.Checked)
                    {
                      Target = rbtFormatExcelSheet.Text;
                      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.ExportTruecolorToExcelSheet("Bitmap256Truecolor");
                    }
      }
      if (!Result)
      {
        FNotifier.Error("Main", 15, "Invalid saving Bitmap256Truecolor to " + Target);
      }
      else
      {
        FNotifier.Write("Main: Correct saving of Bitmap256Truecolor to " + Target);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Menu - Load - Truecolor
    //------------------------------------------------------------------------
    //
    private void btnLoadBitmap256Truecolor_Click(object sender, EventArgs e)
    {
      Boolean Result = false;
      String Target = "File";
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!if (FBitmap256Truecolor is CBitmap256TruecolorBase)
      {
        if (rbtFormatBmpFile.Checked)
        {
          Target = rbtFormatBmpFile.Text;
          //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.LoadTruecolorFromBmpFile("Bitmap256Truecolor.bmp");
        }
        else
          if (rbtFormatTifFile.Checked)
          {
            Target = rbtFormatTifFile.Text;
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.LoadTruecolorFromTifFile("Bitmap256Truecolor.tif");
          }
          else
            if (rbtFormatPngFile.Checked)
            {
              Target = rbtFormatPngFile.Text;
              //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.LoadTruecolorFromPngFile("Bitmap256Truecolor.png");
            }
            else
              if (rbtFormatTextFile.Checked)
              {
                Target = rbtFormatTextFile.Text;
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.LoadTruecolorFromTextFile("Bitmap256Truecolor.txt");
              }
              else
                if (rbtFormatTaggedBinaryFile.Checked)
                {
                  Target = rbtFormatTaggedBinaryFile.Text;
                  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.LoadTruecolorFromTaggedBinaryFile("Bitmap256Truecolor.tbf");
                }
                else
                  if (rbtFormatExcelFile.Checked)
                  {
                    Target = rbtFormatExcelFile.Text;
                    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.LoadTruecolorFromExcelFile("Bitmap256Truecolor.xls");
                  }
                  else
                    if (rbtFormatExcelSheet.Checked)
                    {
                      Target = rbtFormatExcelSheet.Text;
                      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result = FBitmap256Truecolor.ImportTruecolorFromExcelSheet("Bitmap256Truecolor");
                    }
      }
      if (!Result)
      {
        FNotifier.Error("Main", 14, "Invalid loading Bitmap256Truecolor from " + Target);
      }
      else
      {
        FNotifier.Write("Main: Correct loading of Bitmap256Truecolor from " + Target);
      }
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Matrix - Frame
    //-----------------------------------------------------------------------
    //
    private void btnSelectColor_Click(object sender, EventArgs e)
    {
      DialogColor.Color = btnSelectColor.BackColor;
      if (DialogResult.OK == DialogColor.ShowDialog())
      {
        btnSelectColor.BackColor = DialogColor.Color;
      }
    }

    private void btnSetMatrixTruecolor_Click(object sender, EventArgs e)
    {
      Int32 ColLeft = (Int32)nudMatrixLeft.Value;
      Int32 RowTop = (Int32)nudMatrixTop.Value;
      Int32 ColWidth = (Int32)nudMatrixWidth.Value;
      Int32 RowHeight = (Int32)nudMatrixHeight.Value;
      Byte[,] MatrixTruecolor = new Byte[RowHeight, ColWidth];
      /*??????????!!!!!!!!!!!if (FBitmap256Truecolor.ConvertTruecolorToMatrix(ColLeft, RowTop, ColWidth, RowHeight, out MatrixTruecolor))
      {
        Color ColorFrame = btnSelectColor.BackColor;
        for (Int32 RI = 0; RI < RowHeight; RI++)
        {
          Int32 MCI = 0;
          for (Int32 CI = 0; CI < ColWidth; CI++)
          { // Blue
            MatrixTruecolor[RI, MCI] = (Byte)(ColorFrame.B);
            MCI++;
            // Green
            MatrixTruecolor[RI, MCI] = (Byte)(ColorFrame.G);
            MCI++;
            // Red
            MatrixTruecolor[RI, MCI] = (Byte)(ColorFrame.R);
            MCI++;
            // Alpha
            MatrixTruecolor[RI, MCI] = 0xFF;
            MCI++;
          }
        }
        FBitmap256Truecolor.ConvertMatrixToTruecolor((Int32)nudMatrixLeft.Value,
                                                     (Int32)nudMatrixTop.Value,
                                                     MatrixTruecolor);
      }*/
    }


  }
}
