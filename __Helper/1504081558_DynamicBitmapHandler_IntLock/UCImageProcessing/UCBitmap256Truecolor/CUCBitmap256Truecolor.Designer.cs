﻿namespace CUCBitmap256Truecolor
{
  partial class CUCBitmap256Truecolor
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.nudMatrixHeight = new System.Windows.Forms.NumericUpDown();
      this.pbxImageTruecolor = new System.Windows.Forms.PictureBox();
      this.nudMatrixWidth = new System.Windows.Forms.NumericUpDown();
      this.rbtFormatTifFile = new System.Windows.Forms.RadioButton();
      this.nudMatrixTop = new System.Windows.Forms.NumericUpDown();
      this.nudMatrixLeft = new System.Windows.Forms.NumericUpDown();
      this.rbtFormatTaggedBinaryFile = new System.Windows.Forms.RadioButton();
      this.label4 = new System.Windows.Forms.Label();
      this.rbtFormatPngFile = new System.Windows.Forms.RadioButton();
      this.label5 = new System.Windows.Forms.Label();
      this.rbtFormatBmpFile = new System.Windows.Forms.RadioButton();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.btnSelectColor = new System.Windows.Forms.Button();
      this.rbtFormatExcelSheet = new System.Windows.Forms.RadioButton();
      this.btnSetMatrixTruecolor = new System.Windows.Forms.Button();
      this.cbxCreateInverse = new System.Windows.Forms.CheckBox();
      this.btnCreateBitmap1KBlue = new System.Windows.Forms.Button();
      this.rbtFormatTextFile = new System.Windows.Forms.RadioButton();
      this.label1 = new System.Windows.Forms.Label();
      this.rbtFormatExcelFile = new System.Windows.Forms.RadioButton();
      this.btnSaveBitmap1K = new System.Windows.Forms.Button();
      this.btnLoadBitmap1K = new System.Windows.Forms.Button();
      this.btnCreateBitmap1KRed = new System.Windows.Forms.Button();
      this.btnCreateRandomBitmap1K = new System.Windows.Forms.Button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.btnCreateBitmap1KGreen = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.DialogColor = new System.Windows.Forms.ColorDialog();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixHeight)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageTruecolor)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixWidth)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixTop)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixLeft)).BeginInit();
      this.panel1.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // nudMatrixHeight
      // 
      this.nudMatrixHeight.Location = new System.Drawing.Point(400, 107);
      this.nudMatrixHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudMatrixHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMatrixHeight.Name = "nudMatrixHeight";
      this.nudMatrixHeight.Size = new System.Drawing.Size(46, 20);
      this.nudMatrixHeight.TabIndex = 60;
      this.nudMatrixHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMatrixHeight.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
      // 
      // pbxImageTruecolor
      // 
      this.pbxImageTruecolor.BackColor = System.Drawing.SystemColors.Info;
      this.pbxImageTruecolor.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxImageTruecolor.Location = new System.Drawing.Point(0, 135);
      this.pbxImageTruecolor.Name = "pbxImageTruecolor";
      this.pbxImageTruecolor.Size = new System.Drawing.Size(628, 489);
      this.pbxImageTruecolor.TabIndex = 3;
      this.pbxImageTruecolor.TabStop = false;
      this.pbxImageTruecolor.Paint += new System.Windows.Forms.PaintEventHandler(this.pbxImage_Paint);
      // 
      // nudMatrixWidth
      // 
      this.nudMatrixWidth.Location = new System.Drawing.Point(338, 107);
      this.nudMatrixWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudMatrixWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMatrixWidth.Name = "nudMatrixWidth";
      this.nudMatrixWidth.Size = new System.Drawing.Size(46, 20);
      this.nudMatrixWidth.TabIndex = 58;
      this.nudMatrixWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMatrixWidth.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
      // 
      // rbtFormatTifFile
      // 
      this.rbtFormatTifFile.AutoSize = true;
      this.rbtFormatTifFile.Location = new System.Drawing.Point(80, 15);
      this.rbtFormatTifFile.Name = "rbtFormatTifFile";
      this.rbtFormatTifFile.Size = new System.Drawing.Size(53, 17);
      this.rbtFormatTifFile.TabIndex = 2;
      this.rbtFormatTifFile.Text = "TifFile";
      this.rbtFormatTifFile.UseVisualStyleBackColor = true;
      // 
      // nudMatrixTop
      // 
      this.nudMatrixTop.Location = new System.Drawing.Point(273, 107);
      this.nudMatrixTop.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudMatrixTop.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMatrixTop.Name = "nudMatrixTop";
      this.nudMatrixTop.Size = new System.Drawing.Size(46, 20);
      this.nudMatrixTop.TabIndex = 56;
      this.nudMatrixTop.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMatrixTop.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
      // 
      // nudMatrixLeft
      // 
      this.nudMatrixLeft.Location = new System.Drawing.Point(211, 107);
      this.nudMatrixLeft.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudMatrixLeft.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudMatrixLeft.Name = "nudMatrixLeft";
      this.nudMatrixLeft.Size = new System.Drawing.Size(46, 20);
      this.nudMatrixLeft.TabIndex = 54;
      this.nudMatrixLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMatrixLeft.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
      // 
      // rbtFormatTaggedBinaryFile
      // 
      this.rbtFormatTaggedBinaryFile.AutoSize = true;
      this.rbtFormatTaggedBinaryFile.Location = new System.Drawing.Point(80, 35);
      this.rbtFormatTaggedBinaryFile.Name = "rbtFormatTaggedBinaryFile";
      this.rbtFormatTaggedBinaryFile.Size = new System.Drawing.Size(107, 17);
      this.rbtFormatTaggedBinaryFile.TabIndex = 3;
      this.rbtFormatTaggedBinaryFile.Text = "TaggedBinaryFile";
      this.rbtFormatTaggedBinaryFile.UseVisualStyleBackColor = true;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(386, 110);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(15, 13);
      this.label4.TabIndex = 61;
      this.label4.Text = "H";
      // 
      // rbtFormatPngFile
      // 
      this.rbtFormatPngFile.AutoSize = true;
      this.rbtFormatPngFile.Location = new System.Drawing.Point(139, 15);
      this.rbtFormatPngFile.Name = "rbtFormatPngFile";
      this.rbtFormatPngFile.Size = new System.Drawing.Size(60, 17);
      this.rbtFormatPngFile.TabIndex = 1;
      this.rbtFormatPngFile.Text = "PngFile";
      this.rbtFormatPngFile.UseVisualStyleBackColor = true;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(322, 110);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(18, 13);
      this.label5.TabIndex = 59;
      this.label5.Text = "W";
      // 
      // rbtFormatBmpFile
      // 
      this.rbtFormatBmpFile.AutoSize = true;
      this.rbtFormatBmpFile.Checked = true;
      this.rbtFormatBmpFile.Location = new System.Drawing.Point(12, 15);
      this.rbtFormatBmpFile.Name = "rbtFormatBmpFile";
      this.rbtFormatBmpFile.Size = new System.Drawing.Size(62, 17);
      this.rbtFormatBmpFile.TabIndex = 0;
      this.rbtFormatBmpFile.TabStop = true;
      this.rbtFormatBmpFile.Text = "BmpFile";
      this.rbtFormatBmpFile.UseVisualStyleBackColor = true;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(260, 110);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(14, 13);
      this.label3.TabIndex = 57;
      this.label3.Text = "T";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(198, 110);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(13, 13);
      this.label2.TabIndex = 55;
      this.label2.Text = "L";
      // 
      // btnSelectColor
      // 
      this.btnSelectColor.BackColor = System.Drawing.Color.Yellow;
      this.btnSelectColor.Location = new System.Drawing.Point(155, 104);
      this.btnSelectColor.Name = "btnSelectColor";
      this.btnSelectColor.Size = new System.Drawing.Size(40, 23);
      this.btnSelectColor.TabIndex = 53;
      this.btnSelectColor.Text = "Color";
      this.btnSelectColor.UseVisualStyleBackColor = false;
      this.btnSelectColor.Click += new System.EventHandler(this.btnSelectColor_Click);
      // 
      // rbtFormatExcelSheet
      // 
      this.rbtFormatExcelSheet.AutoSize = true;
      this.rbtFormatExcelSheet.Enabled = false;
      this.rbtFormatExcelSheet.Location = new System.Drawing.Point(80, 55);
      this.rbtFormatExcelSheet.Name = "rbtFormatExcelSheet";
      this.rbtFormatExcelSheet.Size = new System.Drawing.Size(79, 17);
      this.rbtFormatExcelSheet.TabIndex = 4;
      this.rbtFormatExcelSheet.Text = "ExcelSheet";
      this.rbtFormatExcelSheet.UseVisualStyleBackColor = true;
      // 
      // btnSetMatrixTruecolor
      // 
      this.btnSetMatrixTruecolor.Location = new System.Drawing.Point(4, 104);
      this.btnSetMatrixTruecolor.Name = "btnSetMatrixTruecolor";
      this.btnSetMatrixTruecolor.Size = new System.Drawing.Size(145, 23);
      this.btnSetMatrixTruecolor.TabIndex = 52;
      this.btnSetMatrixTruecolor.Text = "Set Matrix Truecolor";
      this.btnSetMatrixTruecolor.UseVisualStyleBackColor = true;
      this.btnSetMatrixTruecolor.Click += new System.EventHandler(this.btnSetMatrixTruecolor_Click);
      // 
      // cbxCreateInverse
      // 
      this.cbxCreateInverse.AutoSize = true;
      this.cbxCreateInverse.Location = new System.Drawing.Point(169, 10);
      this.cbxCreateInverse.Name = "cbxCreateInverse";
      this.cbxCreateInverse.Size = new System.Drawing.Size(95, 17);
      this.cbxCreateInverse.TabIndex = 51;
      this.cbxCreateInverse.Text = "Create Inverse";
      this.cbxCreateInverse.UseVisualStyleBackColor = true;
      // 
      // btnCreateBitmap1KBlue
      // 
      this.btnCreateBitmap1KBlue.Location = new System.Drawing.Point(4, 52);
      this.btnCreateBitmap1KBlue.Name = "btnCreateBitmap1KBlue";
      this.btnCreateBitmap1KBlue.Size = new System.Drawing.Size(145, 23);
      this.btnCreateBitmap1KBlue.TabIndex = 50;
      this.btnCreateBitmap1KBlue.Text = "Create Bitmap1K Blue";
      this.btnCreateBitmap1KBlue.UseVisualStyleBackColor = true;
      this.btnCreateBitmap1KBlue.Click += new System.EventHandler(this.btnCreateBitmap256Blue_Click);
      // 
      // rbtFormatTextFile
      // 
      this.rbtFormatTextFile.AutoSize = true;
      this.rbtFormatTextFile.Location = new System.Drawing.Point(12, 35);
      this.rbtFormatTextFile.Name = "rbtFormatTextFile";
      this.rbtFormatTextFile.Size = new System.Drawing.Size(62, 17);
      this.rbtFormatTextFile.TabIndex = 5;
      this.rbtFormatTextFile.Text = "TextFile";
      this.rbtFormatTextFile.UseVisualStyleBackColor = true;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(167, 37);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(57, 13);
      this.label1.TabIndex = 46;
      this.label1.Text = "Bitmap256";
      // 
      // rbtFormatExcelFile
      // 
      this.rbtFormatExcelFile.AutoSize = true;
      this.rbtFormatExcelFile.Location = new System.Drawing.Point(12, 55);
      this.rbtFormatExcelFile.Name = "rbtFormatExcelFile";
      this.rbtFormatExcelFile.Size = new System.Drawing.Size(67, 17);
      this.rbtFormatExcelFile.TabIndex = 6;
      this.rbtFormatExcelFile.Text = "ExcelFile";
      this.rbtFormatExcelFile.UseVisualStyleBackColor = true;
      // 
      // btnSaveBitmap1K
      // 
      this.btnSaveBitmap1K.Location = new System.Drawing.Point(169, 51);
      this.btnSaveBitmap1K.Name = "btnSaveBitmap1K";
      this.btnSaveBitmap1K.Size = new System.Drawing.Size(92, 23);
      this.btnSaveBitmap1K.TabIndex = 45;
      this.btnSaveBitmap1K.Text = "Save";
      this.btnSaveBitmap1K.UseVisualStyleBackColor = true;
      this.btnSaveBitmap1K.Click += new System.EventHandler(this.btnSaveBitmap256Truecolor_Click);
      // 
      // btnLoadBitmap1K
      // 
      this.btnLoadBitmap1K.Location = new System.Drawing.Point(169, 74);
      this.btnLoadBitmap1K.Name = "btnLoadBitmap1K";
      this.btnLoadBitmap1K.Size = new System.Drawing.Size(92, 23);
      this.btnLoadBitmap1K.TabIndex = 44;
      this.btnLoadBitmap1K.Text = "Load";
      this.btnLoadBitmap1K.UseVisualStyleBackColor = true;
      this.btnLoadBitmap1K.Click += new System.EventHandler(this.btnLoadBitmap256Truecolor_Click);
      // 
      // btnCreateBitmap1KRed
      // 
      this.btnCreateBitmap1KRed.Location = new System.Drawing.Point(4, 6);
      this.btnCreateBitmap1KRed.Name = "btnCreateBitmap1KRed";
      this.btnCreateBitmap1KRed.Size = new System.Drawing.Size(145, 23);
      this.btnCreateBitmap1KRed.TabIndex = 43;
      this.btnCreateBitmap1KRed.Text = "Create Bitmap1K Red";
      this.btnCreateBitmap1KRed.UseVisualStyleBackColor = true;
      this.btnCreateBitmap1KRed.Click += new System.EventHandler(this.btnCreateBitmap256Red_Click);
      // 
      // btnCreateRandomBitmap1K
      // 
      this.btnCreateRandomBitmap1K.Location = new System.Drawing.Point(4, 75);
      this.btnCreateRandomBitmap1K.Name = "btnCreateRandomBitmap1K";
      this.btnCreateRandomBitmap1K.Size = new System.Drawing.Size(145, 23);
      this.btnCreateRandomBitmap1K.TabIndex = 49;
      this.btnCreateRandomBitmap1K.Text = "Create Random Bitmap1K";
      this.btnCreateRandomBitmap1K.UseVisualStyleBackColor = true;
      this.btnCreateRandomBitmap1K.Click += new System.EventHandler(this.btnCreateRandomBitmap256_Click);
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.nudMatrixHeight);
      this.panel1.Controls.Add(this.nudMatrixWidth);
      this.panel1.Controls.Add(this.nudMatrixTop);
      this.panel1.Controls.Add(this.nudMatrixLeft);
      this.panel1.Controls.Add(this.label4);
      this.panel1.Controls.Add(this.label5);
      this.panel1.Controls.Add(this.label3);
      this.panel1.Controls.Add(this.label2);
      this.panel1.Controls.Add(this.btnSelectColor);
      this.panel1.Controls.Add(this.btnSetMatrixTruecolor);
      this.panel1.Controls.Add(this.cbxCreateInverse);
      this.panel1.Controls.Add(this.btnCreateBitmap1KBlue);
      this.panel1.Controls.Add(this.btnCreateRandomBitmap1K);
      this.panel1.Controls.Add(this.btnCreateBitmap1KGreen);
      this.panel1.Controls.Add(this.groupBox1);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Controls.Add(this.btnSaveBitmap1K);
      this.panel1.Controls.Add(this.btnLoadBitmap1K);
      this.panel1.Controls.Add(this.btnCreateBitmap1KRed);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(628, 135);
      this.panel1.TabIndex = 2;
      // 
      // btnCreateBitmap1KGreen
      // 
      this.btnCreateBitmap1KGreen.Location = new System.Drawing.Point(4, 29);
      this.btnCreateBitmap1KGreen.Name = "btnCreateBitmap1KGreen";
      this.btnCreateBitmap1KGreen.Size = new System.Drawing.Size(145, 23);
      this.btnCreateBitmap1KGreen.TabIndex = 48;
      this.btnCreateBitmap1KGreen.Text = "Create Bitmap1K Green";
      this.btnCreateBitmap1KGreen.UseVisualStyleBackColor = true;
      this.btnCreateBitmap1KGreen.Click += new System.EventHandler(this.btnCreateBitmap256Green_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.rbtFormatExcelFile);
      this.groupBox1.Controls.Add(this.rbtFormatTextFile);
      this.groupBox1.Controls.Add(this.rbtFormatExcelSheet);
      this.groupBox1.Controls.Add(this.rbtFormatTaggedBinaryFile);
      this.groupBox1.Controls.Add(this.rbtFormatTifFile);
      this.groupBox1.Controls.Add(this.rbtFormatPngFile);
      this.groupBox1.Controls.Add(this.rbtFormatBmpFile);
      this.groupBox1.Location = new System.Drawing.Point(284, 0);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(204, 80);
      this.groupBox1.TabIndex = 47;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = " Format ";
      // 
      // CUCBitmap256Truecolor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pbxImageTruecolor);
      this.Controls.Add(this.panel1);
      this.Name = "CUCBitmap256Truecolor";
      this.Size = new System.Drawing.Size(628, 624);
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixHeight)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageTruecolor)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixWidth)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixTop)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMatrixLeft)).EndInit();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.NumericUpDown nudMatrixHeight;
    private System.Windows.Forms.PictureBox pbxImageTruecolor;
    private System.Windows.Forms.NumericUpDown nudMatrixWidth;
    private System.Windows.Forms.RadioButton rbtFormatTifFile;
    private System.Windows.Forms.NumericUpDown nudMatrixTop;
    private System.Windows.Forms.NumericUpDown nudMatrixLeft;
    private System.Windows.Forms.RadioButton rbtFormatTaggedBinaryFile;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.RadioButton rbtFormatPngFile;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.RadioButton rbtFormatBmpFile;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button btnSelectColor;
    private System.Windows.Forms.RadioButton rbtFormatExcelSheet;
    private System.Windows.Forms.Button btnSetMatrixTruecolor;
    private System.Windows.Forms.CheckBox cbxCreateInverse;
    private System.Windows.Forms.Button btnCreateBitmap1KBlue;
    private System.Windows.Forms.RadioButton rbtFormatTextFile;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.RadioButton rbtFormatExcelFile;
    private System.Windows.Forms.Button btnSaveBitmap1K;
    private System.Windows.Forms.Button btnLoadBitmap1K;
    private System.Windows.Forms.Button btnCreateBitmap1KRed;
    private System.Windows.Forms.Button btnCreateRandomBitmap1K;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button btnCreateBitmap1KGreen;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.ColorDialog DialogColor;
  }
}
