﻿namespace UCBitmap256Palette
{
  partial class CUCBitmap256Palette
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.rbtFormatBmpFile = new System.Windows.Forms.RadioButton();
      this.rbtFormatPngFile = new System.Windows.Forms.RadioButton();
      this.rbtFormatTifFile = new System.Windows.Forms.RadioButton();
      this.pbxImageColored = new System.Windows.Forms.PictureBox();
      this.btnSelectColor = new System.Windows.Forms.Button();
      this.btnSetMatrixGreyscale = new System.Windows.Forms.Button();
      this.rbtFormatTaggedBinaryFile = new System.Windows.Forms.RadioButton();
      this.label3 = new System.Windows.Forms.Label();
      this.cbxPalette = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.rbtFormatExcelFile = new System.Windows.Forms.RadioButton();
      this.rbtFormatTextFile = new System.Windows.Forms.RadioButton();
      this.rbtFormatExcelSheet = new System.Windows.Forms.RadioButton();
      this.pnlTop = new System.Windows.Forms.Panel();
      this.groupBox6 = new System.Windows.Forms.GroupBox();
      this.nudTargetColValue = new System.Windows.Forms.NumericUpDown();
      this.nudTargetRowValue = new System.Windows.Forms.NumericUpDown();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.nudTargetCol = new System.Windows.Forms.NumericUpDown();
      this.nudTargetRow = new System.Windows.Forms.NumericUpDown();
      this.label10 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.groupBox5 = new System.Windows.Forms.GroupBox();
      this.rbtVectorRange4K = new System.Windows.Forms.RadioButton();
      this.rbtVectorRange1K = new System.Windows.Forms.RadioButton();
      this.rbtVectorRange256 = new System.Windows.Forms.RadioButton();
      this.btnSetVectorColGreyscale = new System.Windows.Forms.Button();
      this.btnSetVectorRowGreyscale = new System.Windows.Forms.Button();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.cbxRectangleFull = new System.Windows.Forms.CheckBox();
      this.nudTargetHeight = new System.Windows.Forms.NumericUpDown();
      this.nudTargetWidth = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.nudTargetTop = new System.Windows.Forms.NumericUpDown();
      this.nudTargetLeft = new System.Windows.Forms.NumericUpDown();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.nudFillConstant = new System.Windows.Forms.NumericUpDown();
      this.rbtFillTypeInverse = new System.Windows.Forms.RadioButton();
      this.rbtFillTypeRandom = new System.Windows.Forms.RadioButton();
      this.rbtFillTypeLinear = new System.Windows.Forms.RadioButton();
      this.rbtFillTypeConstant = new System.Windows.Forms.RadioButton();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.rbtMatrixRange4K = new System.Windows.Forms.RadioButton();
      this.rbtMatrixRange1K = new System.Windows.Forms.RadioButton();
      this.rbtMatrixRange256 = new System.Windows.Forms.RadioButton();
      this.label1 = new System.Windows.Forms.Label();
      this.btnSaveBitmap256PaletteGreyscale = new System.Windows.Forms.Button();
      this.btnLoadBitmap256PaletteGreyscale = new System.Windows.Forms.Button();
      this.btnLoadBitmap256PaletteColored = new System.Windows.Forms.Button();
      this.btnSaveBitmap256PaletteColored = new System.Windows.Forms.Button();
      this.DialogColor = new System.Windows.Forms.ColorDialog();
      this.splView = new System.Windows.Forms.Splitter();
      this.pbxImageGreyscale = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageColored)).BeginInit();
      this.groupBox1.SuspendLayout();
      this.pnlTop.SuspendLayout();
      this.groupBox6.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetColValue)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetRowValue)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetCol)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetRow)).BeginInit();
      this.groupBox5.SuspendLayout();
      this.groupBox4.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetHeight)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetWidth)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetTop)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetLeft)).BeginInit();
      this.groupBox3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudFillConstant)).BeginInit();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageGreyscale)).BeginInit();
      this.SuspendLayout();
      // 
      // rbtFormatBmpFile
      // 
      this.rbtFormatBmpFile.AutoSize = true;
      this.rbtFormatBmpFile.Checked = true;
      this.rbtFormatBmpFile.Location = new System.Drawing.Point(13, 16);
      this.rbtFormatBmpFile.Name = "rbtFormatBmpFile";
      this.rbtFormatBmpFile.Size = new System.Drawing.Size(62, 17);
      this.rbtFormatBmpFile.TabIndex = 0;
      this.rbtFormatBmpFile.TabStop = true;
      this.rbtFormatBmpFile.Text = "BmpFile";
      this.rbtFormatBmpFile.UseVisualStyleBackColor = true;
      // 
      // rbtFormatPngFile
      // 
      this.rbtFormatPngFile.AutoSize = true;
      this.rbtFormatPngFile.Location = new System.Drawing.Point(140, 16);
      this.rbtFormatPngFile.Name = "rbtFormatPngFile";
      this.rbtFormatPngFile.Size = new System.Drawing.Size(60, 17);
      this.rbtFormatPngFile.TabIndex = 1;
      this.rbtFormatPngFile.Text = "PngFile";
      this.rbtFormatPngFile.UseVisualStyleBackColor = true;
      // 
      // rbtFormatTifFile
      // 
      this.rbtFormatTifFile.AutoSize = true;
      this.rbtFormatTifFile.Location = new System.Drawing.Point(81, 16);
      this.rbtFormatTifFile.Name = "rbtFormatTifFile";
      this.rbtFormatTifFile.Size = new System.Drawing.Size(53, 17);
      this.rbtFormatTifFile.TabIndex = 2;
      this.rbtFormatTifFile.Text = "TifFile";
      this.rbtFormatTifFile.UseVisualStyleBackColor = true;
      // 
      // pbxImageColored
      // 
      this.pbxImageColored.BackColor = System.Drawing.SystemColors.Info;
      this.pbxImageColored.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxImageColored.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pbxImageColored.Location = new System.Drawing.Point(0, 430);
      this.pbxImageColored.Name = "pbxImageColored";
      this.pbxImageColored.Size = new System.Drawing.Size(813, 230);
      this.pbxImageColored.TabIndex = 11;
      this.pbxImageColored.TabStop = false;
      this.pbxImageColored.Paint += new System.Windows.Forms.PaintEventHandler(this.pbxImageColored_Paint);
      // 
      // btnSelectColor
      // 
      this.btnSelectColor.BackColor = System.Drawing.Color.White;
      this.btnSelectColor.Location = new System.Drawing.Point(439, 109);
      this.btnSelectColor.Name = "btnSelectColor";
      this.btnSelectColor.Size = new System.Drawing.Size(40, 23);
      this.btnSelectColor.TabIndex = 63;
      this.btnSelectColor.Text = "Color";
      this.btnSelectColor.UseVisualStyleBackColor = false;
      // 
      // btnSetMatrixGreyscale
      // 
      this.btnSetMatrixGreyscale.Location = new System.Drawing.Point(388, 6);
      this.btnSetMatrixGreyscale.Name = "btnSetMatrixGreyscale";
      this.btnSetMatrixGreyscale.Size = new System.Drawing.Size(91, 27);
      this.btnSetMatrixGreyscale.TabIndex = 61;
      this.btnSetMatrixGreyscale.Text = "Set Matrix";
      this.btnSetMatrixGreyscale.UseVisualStyleBackColor = true;
      this.btnSetMatrixGreyscale.Click += new System.EventHandler(this.btnSetMatrixGreyscale_Click);
      // 
      // rbtFormatTaggedBinaryFile
      // 
      this.rbtFormatTaggedBinaryFile.AutoSize = true;
      this.rbtFormatTaggedBinaryFile.Location = new System.Drawing.Point(81, 36);
      this.rbtFormatTaggedBinaryFile.Name = "rbtFormatTaggedBinaryFile";
      this.rbtFormatTaggedBinaryFile.Size = new System.Drawing.Size(107, 17);
      this.rbtFormatTaggedBinaryFile.TabIndex = 3;
      this.rbtFormatTaggedBinaryFile.Text = "TaggedBinaryFile";
      this.rbtFormatTaggedBinaryFile.UseVisualStyleBackColor = true;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(9, 97);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(40, 13);
      this.label3.TabIndex = 53;
      this.label3.Text = "Palette";
      // 
      // cbxPalette
      // 
      this.cbxPalette.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPalette.FormattingEnabled = true;
      this.cbxPalette.Items.AddRange(new object[] {
            "Greyscale",
            "Rainbow",
            "Spectrum",
            "RGB8Bright",
            "RGB8Light",
            "RGB8",
            "RGB32Bright",
            "RGB32Light",
            "RGB32",
            "Random"});
      this.cbxPalette.Location = new System.Drawing.Point(50, 93);
      this.cbxPalette.Name = "cbxPalette";
      this.cbxPalette.Size = new System.Drawing.Size(147, 21);
      this.cbxPalette.TabIndex = 43;
      this.cbxPalette.SelectedIndexChanged += new System.EventHandler(this.cbxPalette_SelectedIndexChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(109, 122);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(93, 13);
      this.label2.TabIndex = 50;
      this.label2.Text = "Bitmap256Colored";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.rbtFormatExcelFile);
      this.groupBox1.Controls.Add(this.rbtFormatTextFile);
      this.groupBox1.Controls.Add(this.rbtFormatExcelSheet);
      this.groupBox1.Controls.Add(this.rbtFormatTaggedBinaryFile);
      this.groupBox1.Controls.Add(this.rbtFormatTifFile);
      this.groupBox1.Controls.Add(this.rbtFormatPngFile);
      this.groupBox1.Controls.Add(this.rbtFormatBmpFile);
      this.groupBox1.Location = new System.Drawing.Point(203, 99);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(204, 80);
      this.groupBox1.TabIndex = 51;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = " Format ";
      // 
      // rbtFormatExcelFile
      // 
      this.rbtFormatExcelFile.AutoSize = true;
      this.rbtFormatExcelFile.Location = new System.Drawing.Point(13, 56);
      this.rbtFormatExcelFile.Name = "rbtFormatExcelFile";
      this.rbtFormatExcelFile.Size = new System.Drawing.Size(67, 17);
      this.rbtFormatExcelFile.TabIndex = 6;
      this.rbtFormatExcelFile.Text = "ExcelFile";
      this.rbtFormatExcelFile.UseVisualStyleBackColor = true;
      // 
      // rbtFormatTextFile
      // 
      this.rbtFormatTextFile.AutoSize = true;
      this.rbtFormatTextFile.Location = new System.Drawing.Point(13, 36);
      this.rbtFormatTextFile.Name = "rbtFormatTextFile";
      this.rbtFormatTextFile.Size = new System.Drawing.Size(62, 17);
      this.rbtFormatTextFile.TabIndex = 5;
      this.rbtFormatTextFile.Text = "TextFile";
      this.rbtFormatTextFile.UseVisualStyleBackColor = true;
      // 
      // rbtFormatExcelSheet
      // 
      this.rbtFormatExcelSheet.AutoSize = true;
      this.rbtFormatExcelSheet.Enabled = false;
      this.rbtFormatExcelSheet.Location = new System.Drawing.Point(81, 56);
      this.rbtFormatExcelSheet.Name = "rbtFormatExcelSheet";
      this.rbtFormatExcelSheet.Size = new System.Drawing.Size(79, 17);
      this.rbtFormatExcelSheet.TabIndex = 4;
      this.rbtFormatExcelSheet.Text = "ExcelSheet";
      this.rbtFormatExcelSheet.UseVisualStyleBackColor = true;
      // 
      // pnlTop
      // 
      this.pnlTop.Controls.Add(this.groupBox6);
      this.pnlTop.Controls.Add(this.groupBox5);
      this.pnlTop.Controls.Add(this.btnSetVectorColGreyscale);
      this.pnlTop.Controls.Add(this.btnSetVectorRowGreyscale);
      this.pnlTop.Controls.Add(this.groupBox4);
      this.pnlTop.Controls.Add(this.groupBox3);
      this.pnlTop.Controls.Add(this.groupBox2);
      this.pnlTop.Controls.Add(this.btnSelectColor);
      this.pnlTop.Controls.Add(this.btnSetMatrixGreyscale);
      this.pnlTop.Controls.Add(this.label3);
      this.pnlTop.Controls.Add(this.cbxPalette);
      this.pnlTop.Controls.Add(this.groupBox1);
      this.pnlTop.Controls.Add(this.label2);
      this.pnlTop.Controls.Add(this.label1);
      this.pnlTop.Controls.Add(this.btnSaveBitmap256PaletteGreyscale);
      this.pnlTop.Controls.Add(this.btnLoadBitmap256PaletteGreyscale);
      this.pnlTop.Controls.Add(this.btnLoadBitmap256PaletteColored);
      this.pnlTop.Controls.Add(this.btnSaveBitmap256PaletteColored);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(813, 189);
      this.pnlTop.TabIndex = 10;
      // 
      // groupBox6
      // 
      this.groupBox6.Controls.Add(this.nudTargetColValue);
      this.groupBox6.Controls.Add(this.nudTargetRowValue);
      this.groupBox6.Controls.Add(this.label8);
      this.groupBox6.Controls.Add(this.label9);
      this.groupBox6.Controls.Add(this.nudTargetCol);
      this.groupBox6.Controls.Add(this.nudTargetRow);
      this.groupBox6.Controls.Add(this.label10);
      this.groupBox6.Controls.Add(this.label11);
      this.groupBox6.Location = new System.Drawing.Point(578, 7);
      this.groupBox6.Name = "groupBox6";
      this.groupBox6.Size = new System.Drawing.Size(194, 74);
      this.groupBox6.TabIndex = 78;
      this.groupBox6.TabStop = false;
      this.groupBox6.Text = " Position/Value in Matrix ";
      // 
      // nudTargetColValue
      // 
      this.nudTargetColValue.Location = new System.Drawing.Point(126, 43);
      this.nudTargetColValue.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudTargetColValue.Name = "nudTargetColValue";
      this.nudTargetColValue.Size = new System.Drawing.Size(46, 20);
      this.nudTargetColValue.TabIndex = 74;
      this.nudTargetColValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTargetColValue.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      // 
      // nudTargetRowValue
      // 
      this.nudTargetRowValue.Location = new System.Drawing.Point(43, 43);
      this.nudTargetRowValue.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudTargetRowValue.Name = "nudTargetRowValue";
      this.nudTargetRowValue.Size = new System.Drawing.Size(46, 20);
      this.nudTargetRowValue.TabIndex = 72;
      this.nudTargetRowValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(92, 46);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(34, 13);
      this.label8.TabIndex = 75;
      this.label8.Text = "Value";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(8, 46);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(34, 13);
      this.label9.TabIndex = 73;
      this.label9.Text = "Value";
      // 
      // nudTargetCol
      // 
      this.nudTargetCol.Location = new System.Drawing.Point(126, 17);
      this.nudTargetCol.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudTargetCol.Name = "nudTargetCol";
      this.nudTargetCol.Size = new System.Drawing.Size(46, 20);
      this.nudTargetCol.TabIndex = 70;
      this.nudTargetCol.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTargetCol.Value = new decimal(new int[] {
            160,
            0,
            0,
            0});
      // 
      // nudTargetRow
      // 
      this.nudTargetRow.Location = new System.Drawing.Point(43, 17);
      this.nudTargetRow.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudTargetRow.Name = "nudTargetRow";
      this.nudTargetRow.Size = new System.Drawing.Size(46, 20);
      this.nudTargetRow.TabIndex = 68;
      this.nudTargetRow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(92, 20);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(22, 13);
      this.label10.TabIndex = 71;
      this.label10.Text = "Col";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(8, 20);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(29, 13);
      this.label11.TabIndex = 69;
      this.label11.Text = "Row";
      // 
      // groupBox5
      // 
      this.groupBox5.Controls.Add(this.rbtVectorRange4K);
      this.groupBox5.Controls.Add(this.rbtVectorRange1K);
      this.groupBox5.Controls.Add(this.rbtVectorRange256);
      this.groupBox5.Location = new System.Drawing.Point(485, 7);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new System.Drawing.Size(87, 81);
      this.groupBox5.TabIndex = 77;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = " VectorRange ";
      // 
      // rbtVectorRange4K
      // 
      this.rbtVectorRange4K.AutoSize = true;
      this.rbtVectorRange4K.Location = new System.Drawing.Point(13, 58);
      this.rbtVectorRange4K.Name = "rbtVectorRange4K";
      this.rbtVectorRange4K.Size = new System.Drawing.Size(38, 17);
      this.rbtVectorRange4K.TabIndex = 4;
      this.rbtVectorRange4K.Text = "4K";
      this.rbtVectorRange4K.UseVisualStyleBackColor = true;
      // 
      // rbtVectorRange1K
      // 
      this.rbtVectorRange1K.AutoSize = true;
      this.rbtVectorRange1K.Location = new System.Drawing.Point(13, 38);
      this.rbtVectorRange1K.Name = "rbtVectorRange1K";
      this.rbtVectorRange1K.Size = new System.Drawing.Size(38, 17);
      this.rbtVectorRange1K.TabIndex = 3;
      this.rbtVectorRange1K.Text = "1K";
      this.rbtVectorRange1K.UseVisualStyleBackColor = true;
      // 
      // rbtVectorRange256
      // 
      this.rbtVectorRange256.AutoSize = true;
      this.rbtVectorRange256.Checked = true;
      this.rbtVectorRange256.Location = new System.Drawing.Point(13, 15);
      this.rbtVectorRange256.Name = "rbtVectorRange256";
      this.rbtVectorRange256.Size = new System.Drawing.Size(43, 17);
      this.rbtVectorRange256.TabIndex = 0;
      this.rbtVectorRange256.TabStop = true;
      this.rbtVectorRange256.Text = "256";
      this.rbtVectorRange256.UseVisualStyleBackColor = true;
      // 
      // btnSetVectorColGreyscale
      // 
      this.btnSetVectorColGreyscale.Location = new System.Drawing.Point(388, 58);
      this.btnSetVectorColGreyscale.Name = "btnSetVectorColGreyscale";
      this.btnSetVectorColGreyscale.Size = new System.Drawing.Size(91, 27);
      this.btnSetVectorColGreyscale.TabIndex = 76;
      this.btnSetVectorColGreyscale.Text = "Set VectorCol";
      this.btnSetVectorColGreyscale.UseVisualStyleBackColor = true;
      this.btnSetVectorColGreyscale.Click += new System.EventHandler(this.btnSetVectorColGreyscale_Click);
      // 
      // btnSetVectorRowGreyscale
      // 
      this.btnSetVectorRowGreyscale.Location = new System.Drawing.Point(388, 32);
      this.btnSetVectorRowGreyscale.Name = "btnSetVectorRowGreyscale";
      this.btnSetVectorRowGreyscale.Size = new System.Drawing.Size(91, 27);
      this.btnSetVectorRowGreyscale.TabIndex = 75;
      this.btnSetVectorRowGreyscale.Text = "Set VectorRow";
      this.btnSetVectorRowGreyscale.UseVisualStyleBackColor = true;
      this.btnSetVectorRowGreyscale.Click += new System.EventHandler(this.btnSetVectorRowGreyscale_Click);
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.cbxRectangleFull);
      this.groupBox4.Controls.Add(this.nudTargetHeight);
      this.groupBox4.Controls.Add(this.nudTargetWidth);
      this.groupBox4.Controls.Add(this.label4);
      this.groupBox4.Controls.Add(this.label5);
      this.groupBox4.Controls.Add(this.nudTargetTop);
      this.groupBox4.Controls.Add(this.nudTargetLeft);
      this.groupBox4.Controls.Add(this.label6);
      this.groupBox4.Controls.Add(this.label7);
      this.groupBox4.Location = new System.Drawing.Point(243, 7);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(139, 78);
      this.groupBox4.TabIndex = 74;
      this.groupBox4.TabStop = false;
      // 
      // cbxRectangleFull
      // 
      this.cbxRectangleFull.Location = new System.Drawing.Point(9, -1);
      this.cbxRectangleFull.Name = "cbxRectangleFull";
      this.cbxRectangleFull.Size = new System.Drawing.Size(126, 22);
      this.cbxRectangleFull.TabIndex = 76;
      this.cbxRectangleFull.Text = "Bitmap256Rectangle";
      this.cbxRectangleFull.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxRectangleFull.UseVisualStyleBackColor = true;
      this.cbxRectangleFull.CheckedChanged += new System.EventHandler(this.cbxRectangleFull_CheckedChanged);
      // 
      // nudTargetHeight
      // 
      this.nudTargetHeight.Location = new System.Drawing.Point(83, 50);
      this.nudTargetHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudTargetHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudTargetHeight.Name = "nudTargetHeight";
      this.nudTargetHeight.Size = new System.Drawing.Size(46, 20);
      this.nudTargetHeight.TabIndex = 74;
      this.nudTargetHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTargetHeight.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
      // 
      // nudTargetWidth
      // 
      this.nudTargetWidth.Location = new System.Drawing.Point(21, 50);
      this.nudTargetWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudTargetWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudTargetWidth.Name = "nudTargetWidth";
      this.nudTargetWidth.Size = new System.Drawing.Size(46, 20);
      this.nudTargetWidth.TabIndex = 72;
      this.nudTargetWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTargetWidth.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(69, 53);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(15, 13);
      this.label4.TabIndex = 75;
      this.label4.Text = "H";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(5, 53);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(18, 13);
      this.label5.TabIndex = 73;
      this.label5.Text = "W";
      // 
      // nudTargetTop
      // 
      this.nudTargetTop.Location = new System.Drawing.Point(83, 25);
      this.nudTargetTop.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudTargetTop.Name = "nudTargetTop";
      this.nudTargetTop.Size = new System.Drawing.Size(46, 20);
      this.nudTargetTop.TabIndex = 70;
      this.nudTargetTop.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTargetTop.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
      // 
      // nudTargetLeft
      // 
      this.nudTargetLeft.Location = new System.Drawing.Point(21, 25);
      this.nudTargetLeft.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudTargetLeft.Name = "nudTargetLeft";
      this.nudTargetLeft.Size = new System.Drawing.Size(46, 20);
      this.nudTargetLeft.TabIndex = 68;
      this.nudTargetLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTargetLeft.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(70, 28);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(14, 13);
      this.label6.TabIndex = 71;
      this.label6.Text = "T";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(8, 28);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(13, 13);
      this.label7.TabIndex = 69;
      this.label7.Text = "L";
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.nudFillConstant);
      this.groupBox3.Controls.Add(this.rbtFillTypeInverse);
      this.groupBox3.Controls.Add(this.rbtFillTypeRandom);
      this.groupBox3.Controls.Add(this.rbtFillTypeLinear);
      this.groupBox3.Controls.Add(this.rbtFillTypeConstant);
      this.groupBox3.Location = new System.Drawing.Point(99, 7);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(139, 78);
      this.groupBox3.TabIndex = 73;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = " Fill Type ";
      // 
      // nudFillConstant
      // 
      this.nudFillConstant.Hexadecimal = true;
      this.nudFillConstant.Location = new System.Drawing.Point(72, 18);
      this.nudFillConstant.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudFillConstant.Name = "nudFillConstant";
      this.nudFillConstant.Size = new System.Drawing.Size(46, 20);
      this.nudFillConstant.TabIndex = 69;
      this.nudFillConstant.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudFillConstant.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
      // 
      // rbtFillTypeInverse
      // 
      this.rbtFillTypeInverse.Location = new System.Drawing.Point(77, 38);
      this.rbtFillTypeInverse.Name = "rbtFillTypeInverse";
      this.rbtFillTypeInverse.Size = new System.Drawing.Size(61, 17);
      this.rbtFillTypeInverse.TabIndex = 3;
      this.rbtFillTypeInverse.Text = "Inverse";
      this.rbtFillTypeInverse.UseVisualStyleBackColor = true;
      // 
      // rbtFillTypeRandom
      // 
      this.rbtFillTypeRandom.AutoSize = true;
      this.rbtFillTypeRandom.Location = new System.Drawing.Point(6, 57);
      this.rbtFillTypeRandom.Name = "rbtFillTypeRandom";
      this.rbtFillTypeRandom.Size = new System.Drawing.Size(65, 17);
      this.rbtFillTypeRandom.TabIndex = 2;
      this.rbtFillTypeRandom.Text = "Random";
      this.rbtFillTypeRandom.UseVisualStyleBackColor = true;
      // 
      // rbtFillTypeLinear
      // 
      this.rbtFillTypeLinear.AutoSize = true;
      this.rbtFillTypeLinear.Checked = true;
      this.rbtFillTypeLinear.Location = new System.Drawing.Point(6, 38);
      this.rbtFillTypeLinear.Name = "rbtFillTypeLinear";
      this.rbtFillTypeLinear.Size = new System.Drawing.Size(54, 17);
      this.rbtFillTypeLinear.TabIndex = 1;
      this.rbtFillTypeLinear.TabStop = true;
      this.rbtFillTypeLinear.Text = "Linear";
      this.rbtFillTypeLinear.UseVisualStyleBackColor = true;
      // 
      // rbtFillTypeConstant
      // 
      this.rbtFillTypeConstant.AutoSize = true;
      this.rbtFillTypeConstant.Location = new System.Drawing.Point(6, 19);
      this.rbtFillTypeConstant.Name = "rbtFillTypeConstant";
      this.rbtFillTypeConstant.Size = new System.Drawing.Size(67, 17);
      this.rbtFillTypeConstant.TabIndex = 0;
      this.rbtFillTypeConstant.Text = "Constant";
      this.rbtFillTypeConstant.UseVisualStyleBackColor = true;
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.rbtMatrixRange4K);
      this.groupBox2.Controls.Add(this.rbtMatrixRange1K);
      this.groupBox2.Controls.Add(this.rbtMatrixRange256);
      this.groupBox2.Location = new System.Drawing.Point(7, 4);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(87, 81);
      this.groupBox2.TabIndex = 72;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = " MatrixRange ";
      // 
      // rbtMatrixRange4K
      // 
      this.rbtMatrixRange4K.AutoSize = true;
      this.rbtMatrixRange4K.Location = new System.Drawing.Point(13, 58);
      this.rbtMatrixRange4K.Name = "rbtMatrixRange4K";
      this.rbtMatrixRange4K.Size = new System.Drawing.Size(38, 17);
      this.rbtMatrixRange4K.TabIndex = 4;
      this.rbtMatrixRange4K.Text = "4K";
      this.rbtMatrixRange4K.UseVisualStyleBackColor = true;
      // 
      // rbtMatrixRange1K
      // 
      this.rbtMatrixRange1K.AutoSize = true;
      this.rbtMatrixRange1K.Location = new System.Drawing.Point(13, 38);
      this.rbtMatrixRange1K.Name = "rbtMatrixRange1K";
      this.rbtMatrixRange1K.Size = new System.Drawing.Size(38, 17);
      this.rbtMatrixRange1K.TabIndex = 3;
      this.rbtMatrixRange1K.Text = "1K";
      this.rbtMatrixRange1K.UseVisualStyleBackColor = true;
      // 
      // rbtMatrixRange256
      // 
      this.rbtMatrixRange256.AutoSize = true;
      this.rbtMatrixRange256.Checked = true;
      this.rbtMatrixRange256.Location = new System.Drawing.Point(13, 19);
      this.rbtMatrixRange256.Name = "rbtMatrixRange256";
      this.rbtMatrixRange256.Size = new System.Drawing.Size(43, 17);
      this.rbtMatrixRange256.TabIndex = 0;
      this.rbtMatrixRange256.TabStop = true;
      this.rbtMatrixRange256.Text = "256";
      this.rbtMatrixRange256.UseVisualStyleBackColor = true;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(8, 122);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(104, 13);
      this.label1.TabIndex = 49;
      this.label1.Text = "Bitmap256Greyscale";
      // 
      // btnSaveBitmap256PaletteGreyscale
      // 
      this.btnSaveBitmap256PaletteGreyscale.Location = new System.Drawing.Point(10, 136);
      this.btnSaveBitmap256PaletteGreyscale.Name = "btnSaveBitmap256PaletteGreyscale";
      this.btnSaveBitmap256PaletteGreyscale.Size = new System.Drawing.Size(92, 23);
      this.btnSaveBitmap256PaletteGreyscale.TabIndex = 48;
      this.btnSaveBitmap256PaletteGreyscale.Text = "Save";
      this.btnSaveBitmap256PaletteGreyscale.UseVisualStyleBackColor = true;
      this.btnSaveBitmap256PaletteGreyscale.Click += new System.EventHandler(this.btnSaveBitmap256PaletteGreyscale_Click);
      // 
      // btnLoadBitmap256PaletteGreyscale
      // 
      this.btnLoadBitmap256PaletteGreyscale.Location = new System.Drawing.Point(10, 159);
      this.btnLoadBitmap256PaletteGreyscale.Name = "btnLoadBitmap256PaletteGreyscale";
      this.btnLoadBitmap256PaletteGreyscale.Size = new System.Drawing.Size(92, 23);
      this.btnLoadBitmap256PaletteGreyscale.TabIndex = 47;
      this.btnLoadBitmap256PaletteGreyscale.Text = "Load";
      this.btnLoadBitmap256PaletteGreyscale.UseVisualStyleBackColor = true;
      // 
      // btnLoadBitmap256PaletteColored
      // 
      this.btnLoadBitmap256PaletteColored.Location = new System.Drawing.Point(106, 159);
      this.btnLoadBitmap256PaletteColored.Name = "btnLoadBitmap256PaletteColored";
      this.btnLoadBitmap256PaletteColored.Size = new System.Drawing.Size(92, 23);
      this.btnLoadBitmap256PaletteColored.TabIndex = 45;
      this.btnLoadBitmap256PaletteColored.Text = "Load Bitmap256PaletteColored";
      this.btnLoadBitmap256PaletteColored.UseVisualStyleBackColor = true;
      // 
      // btnSaveBitmap256PaletteColored
      // 
      this.btnSaveBitmap256PaletteColored.Location = new System.Drawing.Point(106, 136);
      this.btnSaveBitmap256PaletteColored.Name = "btnSaveBitmap256PaletteColored";
      this.btnSaveBitmap256PaletteColored.Size = new System.Drawing.Size(92, 23);
      this.btnSaveBitmap256PaletteColored.TabIndex = 44;
      this.btnSaveBitmap256PaletteColored.Text = "Save";
      this.btnSaveBitmap256PaletteColored.UseVisualStyleBackColor = true;
      this.btnSaveBitmap256PaletteColored.Click += new System.EventHandler(this.btnSaveBitmap256PaletteColored_Click);
      // 
      // splView
      // 
      this.splView.BackColor = System.Drawing.SystemColors.Control;
      this.splView.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splView.Location = new System.Drawing.Point(0, 427);
      this.splView.Name = "splView";
      this.splView.Size = new System.Drawing.Size(813, 3);
      this.splView.TabIndex = 14;
      this.splView.TabStop = false;
      // 
      // pbxImageGreyscale
      // 
      this.pbxImageGreyscale.BackColor = System.Drawing.SystemColors.Info;
      this.pbxImageGreyscale.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxImageGreyscale.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxImageGreyscale.Location = new System.Drawing.Point(0, 189);
      this.pbxImageGreyscale.Name = "pbxImageGreyscale";
      this.pbxImageGreyscale.Size = new System.Drawing.Size(813, 238);
      this.pbxImageGreyscale.TabIndex = 15;
      this.pbxImageGreyscale.TabStop = false;
      this.pbxImageGreyscale.Paint += new System.Windows.Forms.PaintEventHandler(this.pbxImageGreyscale_Paint);
      // 
      // CUCBitmap256Palette
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pbxImageGreyscale);
      this.Controls.Add(this.splView);
      this.Controls.Add(this.pbxImageColored);
      this.Controls.Add(this.pnlTop);
      this.Name = "CUCBitmap256Palette";
      this.Size = new System.Drawing.Size(813, 660);
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageColored)).EndInit();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.pnlTop.ResumeLayout(false);
      this.pnlTop.PerformLayout();
      this.groupBox6.ResumeLayout(false);
      this.groupBox6.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetColValue)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetRowValue)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetCol)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetRow)).EndInit();
      this.groupBox5.ResumeLayout(false);
      this.groupBox5.PerformLayout();
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetHeight)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetWidth)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetTop)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTargetLeft)).EndInit();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudFillConstant)).EndInit();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImageGreyscale)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.RadioButton rbtFormatBmpFile;
    private System.Windows.Forms.RadioButton rbtFormatPngFile;
    private System.Windows.Forms.RadioButton rbtFormatTifFile;
    private System.Windows.Forms.PictureBox pbxImageColored;
    private System.Windows.Forms.Button btnSelectColor;
    private System.Windows.Forms.Button btnSetMatrixGreyscale;
    private System.Windows.Forms.RadioButton rbtFormatTaggedBinaryFile;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.ComboBox cbxPalette;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.RadioButton rbtFormatExcelFile;
    private System.Windows.Forms.RadioButton rbtFormatTextFile;
    private System.Windows.Forms.RadioButton rbtFormatExcelSheet;
    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btnSaveBitmap256PaletteGreyscale;
    private System.Windows.Forms.Button btnLoadBitmap256PaletteGreyscale;
    private System.Windows.Forms.Button btnLoadBitmap256PaletteColored;
    private System.Windows.Forms.Button btnSaveBitmap256PaletteColored;
    private System.Windows.Forms.ColorDialog DialogColor;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.NumericUpDown nudTargetHeight;
    private System.Windows.Forms.NumericUpDown nudTargetWidth;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.NumericUpDown nudTargetTop;
    private System.Windows.Forms.NumericUpDown nudTargetLeft;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.RadioButton rbtFillTypeRandom;
    private System.Windows.Forms.RadioButton rbtFillTypeLinear;
    private System.Windows.Forms.RadioButton rbtFillTypeConstant;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.RadioButton rbtMatrixRange4K;
    private System.Windows.Forms.RadioButton rbtMatrixRange1K;
    private System.Windows.Forms.RadioButton rbtMatrixRange256;
    private System.Windows.Forms.CheckBox cbxRectangleFull;
    private System.Windows.Forms.Splitter splView;
    private System.Windows.Forms.PictureBox pbxImageGreyscale;
    private System.Windows.Forms.RadioButton rbtFillTypeInverse;
    private System.Windows.Forms.NumericUpDown nudFillConstant;
    private System.Windows.Forms.Button btnSetVectorColGreyscale;
    private System.Windows.Forms.Button btnSetVectorRowGreyscale;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.RadioButton rbtVectorRange4K;
    private System.Windows.Forms.RadioButton rbtVectorRange1K;
    private System.Windows.Forms.RadioButton rbtVectorRange256;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.NumericUpDown nudTargetCol;
    private System.Windows.Forms.NumericUpDown nudTargetRow;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.NumericUpDown nudTargetColValue;
    private System.Windows.Forms.NumericUpDown nudTargetRowValue;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
  }
}
