﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
//
using IPVector;
using IPMatrix;
using IPPalette;
using IPBitmap;
//
namespace UCBitmap256Palette
{
  public partial class CUCBitmap256Palette : UserControl
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    private String SECTION_LIBRARY = "CUCBitmap256Palette";
    //
    private const Int32 INIT_ROWCOUNT = 240;
    private const Int32 INIT_COLCOUNT = 320;
    //
    private const Int32 INIT_TARGET_LEFT = 0;
    private const Int32 INIT_TARGET_TOP = 0;
    private const Int32 INIT_TARGET_WIDTH = INIT_COLCOUNT;
    private const Int32 INIT_TARGET_HEIGHT = INIT_ROWCOUNT;
    //
    private String NAME_HEIGHTCOLORED = "HeightColored";
    private Int32 INIT_HEIGHTCOLORED = 340;
    //
    private String NAME_PALETTE = "Palette";
    private EPalette256 INIT_PALETTE = EPalette256.RGB32;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private CNotifier FNotifier;
    private Random FRandom;
    private CBitmap256Palette FBitmap256Palette;
    //
    //--------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------
    //
    public CUCBitmap256Palette()
    {
      InitializeComponent();
      FRandom = new Random(3210);
      //btnCreateLinearBitmap256_Click(this, null);
      nudTargetLeft.Value = INIT_TARGET_LEFT;
      nudTargetTop.Value = INIT_TARGET_TOP;
      nudTargetWidth.Value = INIT_TARGET_WIDTH;
      nudTargetHeight.Value = INIT_TARGET_HEIGHT;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    private EPalette256 GetPalette256()
    {
      return CIPPalette256.TextToPalette(cbxPalette.Text);
    }

    private CPalette256Base BuildPalette256()
    {
      EPalette256 P256 = CIPPalette256.TextToPalette(cbxPalette.Text);
      return CIPPalette256.CreatePalette(P256);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Int32 IValue;
      //
      Result &= initdata.OpenSection(SECTION_LIBRARY);
      //
      IValue = INIT_HEIGHTCOLORED;
      Result &= initdata.ReadInt32(NAME_HEIGHTCOLORED, out IValue, IValue);
      pbxImageColored.Height = IValue;
      //
      String SValue = CIPPalette256.PaletteToText(INIT_PALETTE);
      Result &= initdata.ReadEnumeration(NAME_PALETTE, out SValue, SValue);
      cbxPalette.SelectedIndex = (Int32)CIPPalette256.TextToPalette(SValue);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= initdata.WriteInt32(NAME_HEIGHTCOLORED, pbxImageColored.Height);
      String SValue = CIPPalette256.PaletteToText((EPalette256)cbxPalette.SelectedIndex);
      Result &= initdata.WriteEnumeration(NAME_PALETTE, SValue);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------------------
    //
    private void BuildBitmap256()
    {
      Int32 RowCount = (Int32)nudTargetHeight.Value;
      Int32 ColCount = (Int32)nudTargetWidth.Value;
      CMatrix256 Matrix256 = new CMatrix256(RowCount, ColCount);
      if (rbtFillTypeConstant.Checked)
      {
        Byte Value = (Byte)nudFillConstant.Value;
        Matrix256.BuildConstant(RowCount, ColCount, Value);
      }
      else
        if (rbtFillTypeLinear.Checked)
        {
          Matrix256.BuildLinear(RowCount, ColCount);
        }
        else
          if (rbtFillTypeInverse.Checked)
          {
            Matrix256.BuildInverse(RowCount, ColCount);
          }
          else
            if (rbtFillTypeRandom.Checked)
            {
              Matrix256.BuildRandom(RowCount, ColCount);
            }
      FBitmap256Palette = new CBitmap256Palette(Matrix256, BuildPalette256());
    }

    private void BuildBitmap1K()
    {
      Int32 RowCount = (Int32)nudTargetHeight.Value;
      Int32 ColCount = (Int32)nudTargetWidth.Value;
      CMatrix1K Matrix1K = new CMatrix1K(RowCount, ColCount);
      if (rbtFillTypeConstant.Checked)
      {
        Byte Value = (Byte)nudFillConstant.Value;
        Matrix1K.BuildConstant(RowCount, ColCount, Value);
      }
      else
        if (rbtFillTypeLinear.Checked)
        {
          Matrix1K.BuildLinear(RowCount, ColCount);
        }
        else
          if (rbtFillTypeInverse.Checked)
          {
            Matrix1K.BuildInverse(RowCount, ColCount);
          }
          else
            if (rbtFillTypeRandom.Checked)
            {
              Matrix1K.BuildRandom(RowCount, ColCount);
            }
      FBitmap256Palette = new CBitmap256Palette(Matrix1K, BuildPalette256());
    }

    private void BuildBitmap4K()
    {
      Int32 RowCount = (Int32)nudTargetHeight.Value;
      Int32 ColCount = (Int32)nudTargetWidth.Value;
      CMatrix4K Matrix4K = new CMatrix4K(RowCount, ColCount);
      if (rbtFillTypeConstant.Checked)
      {
        Byte Value = (Byte)nudFillConstant.Value;
        Matrix4K.BuildConstant(RowCount, ColCount, Value);
      }
      else
        if (rbtFillTypeLinear.Checked)
        {
          Matrix4K.BuildLinear(RowCount, ColCount);
        }
        else
          if (rbtFillTypeInverse.Checked)
          {
            Matrix4K.BuildInverse(RowCount, ColCount);
          }
          else
            if (rbtFillTypeRandom.Checked)
            {
              Matrix4K.BuildRandom(RowCount, ColCount);
            }
      FBitmap256Palette = new CBitmap256Palette(Matrix4K, BuildPalette256());
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------------------
    //
    private void pbxImageGreyscale_Paint(object sender, PaintEventArgs e)
    {
      if (FBitmap256Palette is CBitmap256Palette)
      {
        if (FBitmap256Palette.BitmapGreyscale is Bitmap)
        {
          Graphics G = e.Graphics;
          G.DrawImage(FBitmap256Palette.BitmapGreyscale, 0, 0);
        }
      }
    }
    private void pbxImageColored_Paint(object sender, PaintEventArgs e)
    {
      if (FBitmap256Palette is CBitmap256Palette)
      {
        if (FBitmap256Palette.BitmapColored is Bitmap)
        {
          Graphics G = e.Graphics;
          G.DrawImage(FBitmap256Palette.BitmapColored, 0, 0);
        }
      }

    }

    private void cbxRectangleFull_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxRectangleFull.Checked)
      {
        nudTargetLeft.Enabled = false;
        nudTargetTop.Enabled = false;
        nudTargetWidth.Enabled = false;
        nudTargetHeight.Enabled = false;
        nudTargetLeft.Value = INIT_TARGET_LEFT;
        nudTargetTop.Value = INIT_TARGET_TOP;
        nudTargetWidth.Value = INIT_TARGET_WIDTH;
        nudTargetHeight.Value = INIT_TARGET_HEIGHT;
      }
      else
      {
        nudTargetLeft.Enabled = true;
        nudTargetTop.Enabled = true;
        nudTargetWidth.Enabled = true;
        nudTargetHeight.Enabled = true;
      }
    }

    private void cbxPalette_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FBitmap256Palette is CBitmap256Palette)
      {
        CPalette256Base Palette = BuildPalette256();
        FBitmap256Palette.SetPalette256(Palette);
        pbxImageGreyscale.Invalidate();
        pbxImageColored.Invalidate();
      }
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Menu - Matrix
    //-----------------------------------------------------------------------
    //
    private void btnSetMatrixGreyscale_Click(object sender, EventArgs e)
    {
      if (rbtMatrixRange256.Checked)
      {
        BuildBitmap256();
      }
      else
        if (rbtMatrixRange1K.Checked)
        {
          BuildBitmap1K();
        }
        else
          if (rbtMatrixRange4K.Checked)
          {
            BuildBitmap4K();
          }
      pbxImageGreyscale.Invalidate();
      pbxImageColored.Invalidate();
    }

    private void btnSaveBitmap256PaletteGreyscale_Click(object sender, EventArgs e)
    {
      if (rbtFormatBmpFile.Checked)
      {
        FBitmap256Palette.SaveGreyscaleToBmpFile("Bitmap256PaletteGreyscale.bmp");
      }
      else
        if (rbtFormatTifFile.Checked)
        {
          FBitmap256Palette.SaveGreyscaleToTifFile("Bitmap256PaletteGreyscale.tif");
        }
        else
          if (rbtFormatPngFile.Checked)
          {
            FBitmap256Palette.SaveGreyscaleToPngFile("Bitmap256PaletteGreyscale.png");
          } else
              if (rbtFormatTextFile.Checked)
              {
                FBitmap256Palette.SaveGreyscaleToTextFile("Bitmap256PaletteGreyscale.txt");
              } else
                  if (rbtFormatTaggedBinaryFile.Checked)
                  {
                    FBitmap256Palette.SaveGreyscaleToTaggedBinaryFile("Bitmap256PaletteGreyscale.tbf");
                  }
                  else
                    if (rbtFormatExcelFile.Checked)
                    {
                      FBitmap256Palette.SaveGreyscaleToExcelFile("Bitmap256PaletteGreyscale.xls");
                    }
    }

    private void btnSaveBitmap256PaletteColored_Click(object sender, EventArgs e)
    {
      if (rbtFormatBmpFile.Checked)
      {
        FBitmap256Palette.SaveColoredToBmpFile("Bitmap256PaletteColored.bmp");
      }
      else
        if (rbtFormatTifFile.Checked)
        {
          FBitmap256Palette.SaveColoredToBmpFile("Bitmap256PaletteColored.tif");
        } 
        else
          if (rbtFormatPngFile.Checked)
          {
            FBitmap256Palette.SaveColoredToPngFile("Bitmap256PaletteColored.png");
          } else
              if (rbtFormatTextFile.Checked)
              {
                FBitmap256Palette.SaveColoredToTextFile("Bitmap256PaletteColored.txt");
              } else
                  if (rbtFormatTaggedBinaryFile.Checked)
                  {
                    FBitmap256Palette.SaveColoredToTaggedBinaryFile("Bitmap256PaletteColored.tbf");
                  }
                  else
                    if (rbtFormatExcelFile.Checked)
                    {
                      FBitmap256Palette.SaveColoredToExcelFile("Bitmap256PaletteColored.xls");
                    }
    }
    //
    //-----------------------------------------------------------------------
    //  Section - Menu - Vector
    //-----------------------------------------------------------------------
    //  
    private void btnSetVectorRowGreyscale_Click(object sender, EventArgs e)
    {
      if (FBitmap256Palette is CBitmap256Palette)
      {
        Int32 Row = (Int32)nudTargetRow.Value;
        Int32 Offset = (Int32)nudTargetCol.Value;
        Int32 Size = (Int32)nudTargetWidth.Value;
        Int32 Value = (Int32)nudTargetRowValue.Value;
        FBitmap256Palette.Lock();
        if (rbtVectorRange256.Checked)
        {
          CVector256 Vector = new CVector256();
          Vector.BuildConstant(Size, Value);
          FBitmap256Palette.SetVectorIndexRow(Row, Offset, Vector);
        }
        else
          if (rbtVectorRange1K.Checked)
          {
            CVector1K Vector = new CVector1K();
            Vector.BuildConstant(Size, Value);
            FBitmap256Palette.SetVectorIndexRow(Row, Offset, Vector);
          }
          else
            if (rbtVectorRange4K.Checked)
            {
              CVector4K Vector = new CVector4K();
              Vector.BuildConstant(Size, Value);
              FBitmap256Palette.SetVectorIndexRow(Row, Offset, Vector);
            }
        FBitmap256Palette.Unlock();
        pbxImageGreyscale.Invalidate();
        pbxImageColored.Invalidate();
      }
    }

    private void btnSetVectorColGreyscale_Click(object sender, EventArgs e)
    {
      if (FBitmap256Palette is CBitmap256Palette)
      {
        Int32 Col = (Int32)nudTargetCol.Value;
        Int32 Size = (Int32)nudTargetHeight.Value;
        Int32 Value = (Int32)nudTargetColValue.Value;
        if (rbtVectorRange256.Checked)
        {
          CVector256 Vector = new CVector256();
          Vector.BuildConstant(Size, Value);
          FBitmap256Palette.SetVectorIndexCol(Col, Vector);
        }
        else
          if (rbtVectorRange1K.Checked)
          {
            CVector1K Vector = new CVector1K();
            Vector.BuildConstant(Size, Value);
            FBitmap256Palette.SetVectorIndexCol(Col, Vector);
          }
          else
            if (rbtVectorRange4K.Checked)
            {
              CVector4K Vector = new CVector4K();
              Vector.BuildConstant(Size, Value);
              FBitmap256Palette.SetVectorIndexCol(Col, Vector);
            }
        pbxImageGreyscale.Invalidate();
        pbxImageColored.Invalidate();
      }
    }


  }
}
