﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using Initdata;
using UCNotifier;
//
using TextFile;
using TaggedBinaryFile;
using ExcelFile;
//
using IPPalette;
using IPBitmap;
using UCBitmap256Palette;
//
namespace DynamicBitmapHandler
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;

    private CUCBitmap256Palette FUCBitmap256Palette;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      tbcMain.Controls.Remove(tbpSerialNumberProductKey);
      //
      FUCBitmap256Palette = new CUCBitmap256Palette();
      FUCBitmap256Palette.SetNotifier(FUCNotifier);
      tbpBitmap256Palette.Controls.Add(FUCBitmap256Palette);
      FUCBitmap256Palette.Dock = DockStyle.Fill;
      //
      tmrStartup.Interval = 1;
      //
      tmrStartup.Start();

      FRandom = new Random();
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      // ... Int32 IValue;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      Result &= FUCBitmap256Palette.LoadInitdata(initdata);
      // !!!!!!!!!Result &= FUCBitmap1KPalette.LoadInitdata(initdata);
      // !!!!!!!!!Result &= FUCBitmap1KTruecolor.LoadInitdata(initdata);
      // !!!!!!!!!Result &= FUCBitmap256Truecolor.LoadInitdata(initdata);
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      Result &= FUCBitmap256Palette.SaveInitdata(initdata);
      // !!!!!!!!!Result &= FUCBitmap1KPalette.SaveInitdata(initdata);
      // !!!!!!!!!Result &= FUCBitmap1KTruecolor.SaveInitdata(initdata);
      // !!!!!!!!!Result &= FUCBitmap256Truecolor.SaveInitdata(initdata);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUC...
    //------------------------------------------------------------------------
    // 
    // NC private void UCPeakDetectionOnProcessStateChanged(EProcessState statepreset, 
    // NC                                                   EProcessState stateactual)
    // NC {
    // NC }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        default:
          return false;
      }
    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }  
    //
    //--------------------------------------------------
    //  Segment - Helper - 
    //--------------------------------------------------
    //

    //
    //--------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------
    //

    //
    //--------------------------------------------------
    //  Segment - Callback
    //--------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    //--------------------------------------------
    //############################################
    //--------------------------------------------
    //  Menu-Function - ...
    //--------------------------------------------
    //############################################
    //--------------------------------------------
    // 
    //
    //--------------------------------------------------------------------------
    //  Segment - 
    //--------------------------------------------------------------------------
    //


  }
}

