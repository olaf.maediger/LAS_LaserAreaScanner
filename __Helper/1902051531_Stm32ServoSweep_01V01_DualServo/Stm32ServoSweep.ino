//
#include <Servo.h>
//
Servo ServoX;
Servo ServoY;
//
void setup() 
{
  pinMode(PC13, OUTPUT);
  for (int I = 1; I < 10; I++)
  {
    digitalWrite(PC13, LOW);
    delay(100);
    digitalWrite(PC13, HIGH);
    delay(100);
  }
  ServoX.attach(PB8);
  ServoY.attach(PB9);
}
//
void loop()
{
  digitalWrite(PC13, LOW);
  for (int Position = 30; Position <= 150; Position += 1)
  {
    ServoX.write(Position);
    ServoY.write(Position);
    delay(10);
  }
  digitalWrite(PC13, HIGH);
  for (int Position = 150; 30 <= Position; Position -= 1)
  {
    ServoX.write(Position);
    ServoY.write(Position);
    delay(10);
  }
}

