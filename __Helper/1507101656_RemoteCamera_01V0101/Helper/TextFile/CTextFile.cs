﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace TextFile
{

	//
	//
	//--------------------------------------------------
	//	Types
	//--------------------------------------------------
	//
	public struct RTextFileData
	{
		public String Entry;
		public String Drive;
		public String Path;
		public String Name;
		public String Extension;
		public String FormatString;
		public String FormatCharacter;
		public String FormatByte;
		public String FormatInt16;
		public String FormatUInt16;
		public String FormatInt32;
		public String FormatUInt32;
		public String FormatDecimal;
		public String FormatSingle;
		public String FormatDouble;

    public String[] TokenDelimiters;
    public String[] LineDelimiters;
	};


	public class CTextFile : CTextFileBase
	{
		//
		//
		//--------------------------------------------------
		//	Constructor
		//--------------------------------------------------
		//
    public CTextFile()
      : base()
    {
    }

		~CTextFile()
		{
			Close();
		}
		//
		//--------------------------------------------------
		//	Debug / Protocol / Warning / Error
		//--------------------------------------------------
		//


		//
		//--------------------------------------------------
		//	File-Management
		//--------------------------------------------------
		//
		public new Boolean OpenRead(String fileentry)
		{
			try
			{
				return base.OpenRead(fileentry);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean OpenWrite(String fileentry)
		{
			try
			{
				return base.OpenWrite(fileentry);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean Close()
		{
			try
			{
				return base.Close();
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}



		public new Boolean GetData(ref RTextFileData data)
		{
			try
			{
				return base.GetData(ref data);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean SetData(RTextFileData data)
		{
			try
			{
				return base.SetData(data);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean IsTokenAvailable()
		{
			try
			{
				return base.IsTokenAvailable();
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean ReadRowIndex(out Int32 rowindex)
		{
			try
			{
				return base.ReadRowIndex(out rowindex);
			}
			catch (Exception)
			{
				rowindex = 0;
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean ReadColIndex(out Int32 colindex)
		{
			try
			{
				return base.ReadColIndex(out colindex);
			}
			catch (Exception)
			{
				colindex = 0;
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}
    //
    //--------------------------------------------------
    //	Segment - LowLevel - Write-Methods
    //--------------------------------------------------
    //
    public new Boolean WriteTokenDelimiter()
    {
      try
      {
        return base.WriteTokenDelimiter();
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
      }
    }

    public new Boolean WriteLineDelimiter()
    {
      try
      {
        return base.WriteLineDelimiter();
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
      }
    }

    public new Boolean Write(String value)
    {
      try
      {
        return base.Write(value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
      }
    }
    
    public new Boolean Write(Char value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean Write(Byte value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean Write(Int16 value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean Write(UInt16 value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean Write(Int32 value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean Write(UInt32 value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean Write(Single value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean Write(Double value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

		public new Boolean Write(Decimal value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
			}
		}

    public new Boolean Write(DateTime value)
    {
      try
      {
        return base.Write(value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
      }
    }

    public new Boolean Write(TimeSpan value)
    {
      try
      {
        return base.Write(value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
      }
    }
    //
    //--------------------------------------------------
    //	Segment - LowLevel - Read-Methods
    //--------------------------------------------------
    //
    public new Boolean Read(out String value)
    {
      try
      {
        return base.Read(out value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        value = INIT_STRING;
        return false;
      }
    }

    public new Boolean Read(out Byte bytevalue,
                            out String textvalue)
    {
      try
      {
        return base.Read(out bytevalue, out textvalue);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        bytevalue = INIT_BYTE;
        textvalue = INIT_STRING;
        return false;
      }
    }

    public new Boolean Read(out UInt16 uint16value)
    {
      try
      {
        return base.Read(out uint16value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        uint16value = INIT_UINT16;
        return false;
      }
    }

    public new Boolean Read(out UInt16 uint16value,
                            out String textvalue)
    {
      try
      {
        return base.Read(out uint16value, out textvalue);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        uint16value = INIT_UINT16;
        textvalue = INIT_STRING;
        return false;
      }
    }

    public new Boolean Read(out float value)
    {
      try
      {
        return base.Read(out value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        value = INIT_SINGLE;
        return false;
      }
    }

    public new Boolean Read(out Int32 value)
    {
      try
      {
        return base.Read(out value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        value = INIT_INT32;
        return false;
      }
    }

    public new Boolean Read(out UInt32 value)
    {
      try
      {
        return base.Read(out value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        value = INIT_UINT32;
        return false;
      }
    }

    public new Boolean Read(out Double value)
    {
      try
      {
        return base.Read(out value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        value = INIT_DOUBLE;
        return false;
      }
    }
    
    public new Boolean Read(out Double doublevalue, 
                            out String textvalue)
		{
			try
			{
				return base.Read(out doublevalue, out textvalue);
			}
			catch (Exception)
			{
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        doublevalue = INIT_DOUBLE;
        textvalue = INIT_STRING;
        return false;
			}
		}

    public new Boolean Read(out Decimal value)
    {
      try
      {
        return base.Read(out value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        value = INIT_DECIMAL;
        return false;
      }
    }

    public new Boolean Read(out TimeSpan value)
    {
      try
      {
        return base.Read(out value);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        value = INIT_TIMESPAN;
        return false;
      }
    }

    public new Boolean Read(out Double[] doublevalues, 
                            out String[] textvalues)
    {
      try
      {
        return base.Read(out doublevalues, out textvalues);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        doublevalues = null;
        textvalues = null;
        return false;
      }
    }
    //
    //--------------------------------------------------
    //	Segment - HighLevel - Write-Methods
    //--------------------------------------------------
    //
    public new Boolean Write(Byte[,] matrix)
    {
      try
      {
        return base.Write(matrix);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
      }
    }

    public new Boolean Write(UInt16[,] matrix)
    {
      try
      {
        return base.Write(matrix);
      }
      catch (Exception)
      {
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
      }
    }
    //
    //--------------------------------------------------
    //	Segment - HighLevel - Read-Methods
    //--------------------------------------------------
    //
    public new Boolean Read(out Byte[,] matrix)
    {
      try
      {
        return base.Read(out matrix);
      }
      catch (Exception)
      {
        matrix = null;
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
      }
    }

    public new Boolean Read(out UInt16[,] matrix)
    {
      try
      {
        return base.Read(out matrix);
      }
      catch (Exception)
      {
        matrix = null;
        // Int32 EI = (int)EErrorCode.InvalidAccess;
        Console.WriteLine("ERROR");
        return false;
      }
    }



	}
}
