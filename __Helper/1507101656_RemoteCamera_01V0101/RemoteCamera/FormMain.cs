﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
//
using EDSDKLib;
//
using Initdata;
using UCNotifier;
//
namespace RemoteCamera
{
  public partial class FormCncController : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;
    //
    static SDKHandler FSDKHandler;
    static Boolean WaitForEvent;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);      
      tbcMain.Controls.Remove(tbpSerialNumber);
      //
      FSDKHandler = new SDKHandler();
      //
      tmrStartup.Interval = 1000;
      //
      tmrStartup.Start();

      FRandom = new Random();
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      // ??? 
      CloseCamera();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      // ... Int32 IValue;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      return Result;
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Event - SDKHandler
    //---------------------------------------------------------------------------------
    //
    static uint handler_SDKObjectEvent(uint inEvent, IntPtr inRef, IntPtr inContext)
    {
      if ((inEvent == EDSDK.ObjectEvent_DirItemRequestTransfer) ||
          (inEvent == EDSDK.ObjectEvent_DirItemCreated))
      {
        WaitForEvent = false;
      }
      return EDSDK.EDS_ERR_OK;
    }

    static void handler_CameraAdded()
    {
      List<Camera> Cameras = FSDKHandler.GetCameraList();
      if (0 < Cameras.Count)
      {
        FSDKHandler.OpenSession(Cameras[0]);
      }
      Console.WriteLine("Opened session with camera: " + Cameras[0].Info.szDeviceDescription);
      WaitForEvent = false;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUC...
    //------------------------------------------------------------------------
    // 

    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //
    static void CallEvent()
    {
      WaitForEvent = true;
      while (WaitForEvent)
      {
        EDSDK.EdsGetEvent();
        Thread.Sleep(200);
      }
    }

    private Boolean OpenCamera()
    {
      try
      {
        FSDKHandler.SDKObjectEvent += handler_SDKObjectEvent;
        List<Camera> cameras = FSDKHandler.GetCameraList();
        if (cameras.Count > 0)
        {
          FSDKHandler.OpenSession(cameras[0]);
          Console.WriteLine("Opened session with camera: " + cameras[0].Info.szDeviceDescription);
          return true;
        }
        Console.WriteLine("No camera found. Please plug in camera");
        FSDKHandler.CameraAdded += handler_CameraAdded;
        CallEvent();
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean CloseCamera()
    {
      try
      {
        FSDKHandler.CloseSession();
        FSDKHandler.Dispose();
        Console.WriteLine("Good bye! (press any key to close)");
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean TakePhoto()
    {
      try
      {
        FSDKHandler.ImageSaveDirectory = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), "RemotePhoto");
        FSDKHandler.SetSetting(EDSDK.PropID_SaveTo, (uint)EDSDK.EdsSaveTo.Host);

        Console.WriteLine("Taking photo with current settings...");
        FSDKHandler.TakePhoto();

        CallEvent();
        Console.WriteLine("Photo taken and saved");
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        default:
          return false;
      }
    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCTerminal
    //------------------------------------------------------------------------
    //

    
    //
    //--------------------------------------------------------------------------
    //  Segment - Menu
    //--------------------------------------------------------------------------
    //
    private void btnOpen_Click(object sender, EventArgs e)
    {
      if ("Open" == btnOpen.Text)
      {
        if (OpenCamera())
        {
          btnOpen.Enabled = false;
          btnOpen.Text = "Close on exit...";
          btnTakePhoto.Enabled = true;
        }
      }/* ??? not here!!! 
      else
      {
        if (CloseCamera())
        {
          btnOpen.Text = "Open";
          btnTakePhoto.Enabled = false;
        }
      }*/
    }

    private void btnTakePhoto_Click(object sender, EventArgs e)
    {
      TakePhoto();
    }


  }
}

