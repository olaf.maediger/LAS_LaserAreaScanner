﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StepperMotorSimulator
{
  public delegate Double DOnCalculateParameter(Double parameter);

  public class CStepCalculation
  {
    private Double[] FResolution;
    private DOnCalculateParameter[] FOnCalculateParameter;

    public CStepCalculation(Int32 axiscount)
    {
      Int32 AC = Math.Max(1, axiscount);
      AC = Math.Min(4, AC);
      Initialize(AC);
    }

    public void SetOnCalculateParameter(Int32 axisindex, DOnCalculateParameter value)
    {
      FOnCalculateParameter[axisindex] = value;
    }

    private Boolean Initialize(Int32 axiscount)
    {
      try
      {
        FResolution = new Double[axiscount];
        FOnCalculateParameter = new DOnCalculateParameter[axiscount];
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public void ExecuteAxis(Int32 axisindex)
    {
      Double R = FResolution[axisindex];

      //if (R)
    }

    public Boolean Execute()
    {
      try
      {

        return true;
      }
      catch (Exception)
      {
        return false;
      }

    }

  }
}
