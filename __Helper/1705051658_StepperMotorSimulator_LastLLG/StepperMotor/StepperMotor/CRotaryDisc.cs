﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Observable;
//
namespace StepperMotor
{
  public class CRotaryDisc : CMotion
  {

    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //		
    public CRotaryDisc(String name,
                       String unitlocation,
                       String unitvelocity,
                       String unitacceleration)
      : base(name, unitlocation, unitvelocity, unitacceleration)
    {
    }


  }
}
