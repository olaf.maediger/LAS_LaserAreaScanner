﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Task;
//
namespace StepperMotor
{
  public class CStepperController
  {

    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //		
    private CTask FTask;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //		
    public CStepperController(String name)
    {
      FTask = new CTask("StepperController" + name,
                        ThisOnExecutionStart,
                        ThisOnExecutionBusy,
                        ThisOnExecutionEnd,
                        ThisOnExecutionAbort);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback
    //------------------------------------------------------------------------
    //		
    private void ThisOnExecutionStart(RTaskData data)
    {
    }
    private Boolean ThisOnExecutionBusy(ref RTaskData data)
    {
      return true;
    }
    private void ThisOnExecutionEnd(RTaskData data)
    {
    }
    private void ThisOnExecutionAbort(RTaskData data)
    {
    }


  }
}
