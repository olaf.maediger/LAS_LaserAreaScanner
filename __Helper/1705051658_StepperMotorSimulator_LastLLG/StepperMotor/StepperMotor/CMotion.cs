﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Observable;
using Task;
//
namespace StepperMotor
{ // Base
  public class CMotion
  {
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //		
    protected String FName;
    protected CLocationInt32 FLocationStep;
    protected CLocationDouble FLocationReal;
    protected CVelocityDouble FVelocity;
    protected CAccelerationDouble FAcceleration;
    //
    protected CTask FTask;
    //
    protected CPulse FPulse;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //		
    public CMotion(String name,
                   String unitlocationstep,
                   String unitlocationreal,
                   String unitvelocity,
                   String unitacceleration)
    {
      FName = name;
      FLocationStep = new CLocationInt32(unitlocationstep);
      FLocationReal = new CLocationDouble(unitlocationreal);
      FVelocity = new CVelocityDouble(unitvelocity);
      FAcceleration = new CAccelerationDouble(unitacceleration);
      FTask = new CTask("Motion" + name,
                        ThisOnExecutionStart,
                        ThisOnExecutionBusy,
                        ThisOnExecutionEnd,
                        ThisOnExecutionAbort);      
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------------------------
    //  Section - Callback
    //------------------------------------------------------------------------
    //		
    private void ThisOnExecutionStart(RTaskData data)
    {
    }
    private Boolean ThisOnExecutionBusy(ref RTaskData data)
    {
      if (!FTask.IsActive())
      {
        return false;
      }
      // Action...
      if (FPulse.ExecuteRisingEdge())
      {
        FLocationStep.Actual++;
        // calculate FLocationReal... all real variables!
      }
      //
      return true;
    }
    private void ThisOnExecutionEnd(RTaskData data)
    {
    }
    private void ThisOnExecutionAbort(RTaskData data)
    {
    }

  }
}
