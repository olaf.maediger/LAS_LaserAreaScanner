﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Observable;
//
namespace StepperMotor
{
  public class CLinearAxis : CMotion
  {
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //		
    public CLinearAxis(String name,
                       String unitlocation,
                       String unitvelocity,
                       String unitacceleration)
      : base(name, unitlocation, unitvelocity, unitacceleration)
    {
    }
  }
}
