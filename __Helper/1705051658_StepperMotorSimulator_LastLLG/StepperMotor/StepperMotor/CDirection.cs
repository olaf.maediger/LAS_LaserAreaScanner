﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StepperMotor
{
  //
  //------------------------------------------------------------------------
  //  Section - Type
  //------------------------------------------------------------------------
  //		
  public enum EDirection
  {
    Negative = 0,
    Positive = 1
  }
  //
  public class CDirection
  {
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //		
    private EDirection FDirection;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //		
    public EDirection State
    {
      get { return FDirection; }
      set { FDirection = value; }
    }

  }
}
