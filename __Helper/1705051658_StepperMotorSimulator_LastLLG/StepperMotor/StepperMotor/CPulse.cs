﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StepperMotor
{
  //
  //------------------------------------------------------------------------
  //  Section - Type
  //------------------------------------------------------------------------
  //		
  public enum EPulseState
  {
    Low = 0,
    High = 1
  }
  //
  public class CPulse
  {
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //		
    private String FName;
    private EPulseState FOld;
    private EPulseState FNew;
    private Int32 FCounterRisingEdge;
    private Int32 FCounterFallingEdge;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //		
    public CPulse(String name, EPulseState statepreset)
    {
      FOld = statepreset;
      FNew = statepreset;
      FCounterRisingEdge = 0;
      FCounterFallingEdge = 0;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Management
    //------------------------------------------------------------------------
    //		
    public void RefreshPulse(EPulseState state)
    {
      if (state == FNew)
      {
        return;
      }
      if ((EPulseState.Low == state) && (EPulseState.High == FNew))
      {
        FCounterFallingEdge++;
        FOld = FNew;
        FNew = state;
      }
      else
        if ((EPulseState.High == state) && (EPulseState.Low == FNew))
        {
          FCounterRisingEdge++;
          FOld = FNew;
          FNew = state;
        }
    }

    public Boolean ExecuteRisingEdge()
    {
      if (0 < FCounterRisingEdge)
      {
        FCounterRisingEdge--;
        return true;
      }
      return false;
    }

    public Boolean ExecuteFallingEdge()
    {
      if (0 < FCounterFallingEdge)
      {
        FCounterFallingEdge--;
        return true;
      }
      return false;
    }


  }
}
