//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "Led.h"
//
CLed::CLed(int pin)
{
  FPin = pin;
  FState = slUndefined;
}

Boolean CLed::Open()
{
  pinMode(FPin, OUTPUT);
  digitalWrite(FPin, LOW);
  FState = slOff;
  return true;
}

Boolean CLed::Close()
{
  pinMode(FPin, INPUT);
  FState = slUndefined;
  return true;
}

Boolean CLed::On()
{
  digitalWrite(FPin, HIGH);
  FState = slOn;
  return true;
}

Boolean CLed::Off()
{
  digitalWrite(FPin, LOW);
  FState = slOff;
  return true;
}

Boolean CLed::Toggle()
{
  if (slOff == FState)
  {
    digitalWrite(FPin, HIGH);
    FState = slOn;
    return true;
  } 
  if (slOn == FState)
  {
    digitalWrite(FPin, LOW);
    FState = slOff;
  } 
  return false;
}

