﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using NLScalar;
using NLVector;
using TextFile;
//
namespace NLMatrix
{
	public class CMatrix : CMatrixBase
  {
		//
		//---------------------------------------
		//	Segment - Constructor
		//---------------------------------------
		//
    public CMatrix(CNotifier notifier, Int32 rowcount, Int32 colcount)
      : base(notifier, rowcount, colcount)
    {
    }
    
    public CMatrix(CNotifier notifier, Int32 rowcount, Int32 colcount, Double initvalue)
      : base(notifier, rowcount, colcount, initvalue)
    {
    }
    //
		//----------------------------------------
		//	Segment - Initialisation
		//----------------------------------------
		//
		public new Boolean DefineMatrix(Int32 rowcount, Int32 colcount)
		{
      try
      {
        return base.DefineMatrix(rowcount, colcount);
      }
      catch (Exception)
      {
        return false;
      }
		}


		public new Boolean InitValues(Double initvalue)
		{
      try
      {
        return base.InitValues(initvalue);
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    public new void Write(String header)
    {
      try
      {
        base.Write(header);
      }
      catch (Exception)
      {
      }
    }

    public new void Write(String header,
                          String formatpositive, 
                          String formatnegative)
    {
      try
      {
        base.Write(header, formatpositive, formatnegative);
      }
      catch (Exception)
      {
      }
    }
		//
		//----------------------------------------
		//	Segment - Get/SetData
		//----------------------------------------
		//
    public new Boolean GetTextFileData(ref RTextFileData data)
    {
      try
      {
        return base.GetTextFileData(ref data);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public new Boolean SetTextFileData(RTextFileData data)
		{
      try
      {
        return base.SetTextFileData(data);
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //----------------------------------------
    //	Segment - Management
    //----------------------------------------
    //
    public new Boolean SetDimension(Int32 sizey, Int32 sizex, Double initvalue)
    {
      try
      {
        return base.SetDimension(sizey, sizex, initvalue);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public new Boolean SetConstant(Double value)
    {
      try
      {
        return base.SetConstant(value);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public new Boolean SetUnit()
    {
      try
      {
        return base.SetUnit();
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //----------------------------------------
    //	Segment - File
    //----------------------------------------
    //
    public new Boolean LoadFromTextFile(String fileentry)
    {
      try
      {
        return base.LoadFromTextFile(fileentry);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public new Boolean SaveToTextFile(String fileentry)
    {
      try
      {
        return base.SaveToTextFile(fileentry);
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //----------------------------------------
    //	Segment - Mathematic Functions
    //----------------------------------------
    //      
    public new Boolean Transpose(out CMatrix result)
    {
      try
      {
        return base.Transpose(out result);
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public new Boolean Multiply(CMatrix multiplicator, out CMatrix result)
    {
      try
      {
        return base.Multiply(multiplicator, out result);
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

    public new Boolean Multiply(CVector multiplicator, out CVector result)
    {
      try
      {
        return base.Multiply(multiplicator, out result);
      }
      catch (Exception)
      {
        result = null;
        return false;
      }
    }

  }
}
