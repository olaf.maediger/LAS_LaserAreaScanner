﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using NLScalar;
using NLVector;
using TextFile;
//
namespace NLMatrix
{
  public class CMatrixBase
  { //
    //----------------------------------------
    //	Segment - Constant
    //----------------------------------------
    //
    private const Double INIT_VALUE = 0.0;
    //
    //----------------------------------------
    //	Segment - Member
    //----------------------------------------
    //
    private CNotifier FNotifier;
    private Int32 FColCount;
    private Int32 FRowCount;
    private Double[,] FValues = null;
    private RTextFileData FTextFileData;
    //
    //----------------------------------------
    //	Segment - Property
    //----------------------------------------
    //
    public Int32 XIL
    {
      get { return 0; }
    }
    public Int32 XIH
    {
      get { return FColCount - 1; }
    }
    public Int32 SizeX
    {
      get { return FColCount; }
    }

    public Int32 YIL
    {
      get { return 0; }
    }
    public Int32 YIH
    {
      get { return FRowCount - 1; }
    }
    public Int32 SizeY
    {
      get { return FRowCount; }
    }
    //---------------------------------------
    // Overloading of Index-Operator
    //---------------------------------------
    private Double GetValue(Int32 y, Int32 x)
    {
      if ((YIL <= y) & (y <= YIH))
      {
        if ((XIL <= x) & (x <= XIH))
        {

          return FValues[y, x];
        }
      }
      return 0.0;
    }
    private void SetValue(Int32 y, Int32 x, Double value)
    {
      if ((YIL <= y) & (y <= YIH))
      {
        if ((XIL <= x) & (x <= XIH))
        {
          FValues[y, x] = value;
        }
      }
    }

    // Reading/Writing MainMatrix
    public Double this[Int32 y, Int32 x]
    {
      get { return GetValue(y, x); }
      set { SetValue(y, x, value); }
    }

    // Reading SolutionVector/Writing InhomogenousVector
    public Double this[Int32 y]
    {
      get { return GetValue(y, XIH); }
      set { SetValue(y, XIH, value); }
    }
		//
		//---------------------------------------
		//	Segment - Constructor
		//---------------------------------------
		//
    public CMatrixBase(CNotifier notifier, Int32 rowcount, Int32 colcount)
    {
      FNotifier = notifier;
      FRowCount = Math.Max(1, rowcount);
      FColCount = Math.Max(1, colcount);
      FValues = new Double[rowcount, colcount];
      InitValues(INIT_VALUE);
    }
    
    public CMatrixBase(CNotifier notifier, Int32 rowcount, Int32 colcount, Double initvalue)
		{
      FNotifier = notifier;
      FRowCount = Math.Max(1, rowcount);
			FColCount = Math.Max(1, colcount);
			FValues = new Double[rowcount, colcount];
			InitValues(initvalue);
		}
    //
    //----------------------------------------
    //	Segment - Initialisation
    //----------------------------------------
    //
    protected Boolean DefineMatrix(Int32 rowcount, Int32 colcount)
    {
      FRowCount = Math.Max(1, rowcount);
      FColCount = Math.Max(1, colcount);
      FValues = new Double[rowcount, colcount];
      return InitValues(INIT_VALUE);
    }


    protected Boolean InitValues(Double initvalue)
    {
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          FValues[YI, XI] = initvalue;
        }
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Helper
    //----------------------------------------
    //
    private Boolean CheckIndexY(Int32 yindex)
    {
      return ((YIL <= yindex) & (yindex <= YIH));
    }

    private Boolean CheckIndexX(Int32 xindex)
    {
      return ((XIL <= xindex) & (xindex <= XIH));
    }

    private Boolean CheckIndices(Int32 yindex, Int32 xindex)
    {
      return (CheckIndexY(yindex) & CheckIndexX(xindex));
    }

    private Boolean SetColVector(CVector colvector, Int32 colindex)
    {
      if ((colindex < XIL) || (XIH < colindex))
      {
        return false;
      }
      if (colvector.Size != this.SizeX)
      {
        return false;
      }
      for (Int32 RI = colvector.VIL; RI <= colvector.VIH; RI++)
      {     // Row Col
        this[RI, colindex] = colvector[RI];
      }
      return true;
    }

    private Boolean SetRowVector(CVector rowvector, Int32 rowindex)
    {
      if ((rowindex < YIL) || (YIH < rowindex))
      {
        return false;
      }
      if (rowvector.Size != this.SizeY)
      {
        return false;
      }
      for (Int32 CI = rowvector.VIL; CI <= rowvector.VIH; CI++)
      {    // Row Col
        this[rowindex, CI] = rowvector[CI];
      }
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Protocol
    //----------------------------------------
    //
    protected void Write(String header)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}, {2}]:", header, FRowCount, FColCount));
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          String Line = String.Format(" RI[{0}]:", YI);
          for (Int32 XI = XIL; XI <= XIH; XI++)
          {
            Line += String.Format(" {0}:{1:0.000000000}", XI, this[YI, XI]);
          }
          FNotifier.Write(Line);
        }
      }
    }

    protected void Write(String header,
                         String formatpositive, 
                         String formatnegative)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}, {2}]:", header, SizeY, SizeX));
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          String Line = String.Format(" RI[{0}]:", YI);
          for (Int32 XI = XIL; XI <= XIH; XI++)
          {
            Double DValue = this[YI, XI];
            String SValue;

            if (0 < DValue)
            {
              SValue = String.Format(formatpositive, DValue);
            }
            else
            {
              SValue = String.Format(formatnegative, DValue);
            }
            Line += String.Format(" {0}:{1}", XI, SValue);
          }
          FNotifier.Write(Line);
        }
      }
    }
    //
    //----------------------------------------
    //	Segment - Get/SetData
    //----------------------------------------
    //
    protected Boolean GetTextFileData(ref RTextFileData data)
    {
      data = FTextFileData;
      return true;
    }

    protected Boolean SetTextFileData(RTextFileData data)
    {
      FTextFileData = data;
      return true;
    }
    //
    //----------------------------------------
    //	Segment - Management
    //----------------------------------------
    //
    protected Boolean SetDimension(Int32 sizey, Int32 sizex, Double initvalue)
    {
      if ((0 < sizey) && (0 < sizex))
      {
        FRowCount = sizey;
        FColCount = sizex;
        FValues = new Double[sizey, sizex];
        InitValues(initvalue);
        return true;
      }
      return false;
    }

    protected Boolean SetConstant(Double value)
    {
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          this[YI, XI] = value;
        }
      }
      return true;
    }

    protected Boolean SetUnit()
    {
      if (SizeY == SizeX)
      {
        SetConstant(0.0);
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          this[YI, YI] = 1.0;
        }
        return true;
      }
      return false;
    }
    //
    //----------------------------------------
    //	Segment - File
    //----------------------------------------
    //
    protected Boolean LoadFromTextFile(String fileentry)
    {
      CTextFile TextFile = new CTextFile(FNotifier);
      if (TextFile.OpenRead(fileentry))
      {
        DefineMatrix(TextFile.RowCount, TextFile.ColCount(0));
        //
        Boolean Endloop = false;
        while (!Endloop)
        { // Read next Value
          Int32 YI;
          Int32 XI;
          if (TextFile.ReadRowIndex(out YI))
          {
            if (TextFile.ReadColIndex(out XI))
            {
              Double DValue;
              if (TextFile.Read(out DValue))
              {
                FValues[YI, XI] = DValue;
              }
              else
              {
                Endloop = true;
              }
            }
            else
            {
              Endloop = true;
            }
          }
          else
          {
            Endloop = true;
          }
        }
        //
        TextFile.Close();
        return true;
      }
      TextFile.Close();
      return false;
    }

    protected Boolean SaveToTextFile(String fileentry)
    {
      FTextFileData.Entry = fileentry;
      Boolean Result = true;
      if ((XIH < 0) || (YIH < 0))
      {
        return false;
      }
      CTextFile TextFile = new CTextFile(FNotifier);
      //
      try
      {
        RTextFileData TextFileData = new RTextFileData();
        Result &= TextFile.GetData(ref TextFileData);
        String DelimiterValue = TextFileData.WriteDelimiters[0];
        String DelimiterLine = TextFileData.WriteDelimiters[1];
        //
        Result &= TextFile.OpenWrite(fileentry);
        //
        for (Int32 YI = YIL; YI <= YIH; YI++)
        {
          for (Int32 XI = XIL; XI < XIH; XI++)
          {
            TextFile.Write((Double)FValues[YI, XI]);
            TextFile.Write(DelimiterValue);
          }
          TextFile.Write((Double)FValues[YI, XIH]);
          TextFile.Write(DelimiterLine);
        }

        Result &= true;
      }
      catch (Exception)
      {
        Result = false;
      }
      finally
      {
        Result &= TextFile.Close();
      }
      return Result;
    }
    //
    //----------------------------------------
    //	Segment - Mathematic Functions
    //----------------------------------------
    //
    protected Boolean Transpose(out CMatrix result)
    { // exchange y <-> X
      result = new CMatrix(FNotifier, this.SizeX, this.SizeY);
      for (Int32 YI = YIL; YI <= YIH; YI++)
      {
        for (Int32 XI = XIL; XI <= XIH; XI++)
        {
          result[XI, YI] = this[YI, XI];
        }
      }
      return true;
    }

    protected Boolean Multiply(CMatrix multiplicator, out CMatrix result)
    { // result(Matrix) = this(Matrix) * multiplicator(Matrix)
      result = null;
      // check: ColCount(this) == RowCount(multiplicator)
      if ((this.SizeX == multiplicator.SizeY) &&
          (this.SizeY == multiplicator.SizeX))
      { // Row/ColCount(result) := this.SizeY
        Int32 Size = this.SizeY;
        result = new CMatrix(FNotifier, Size, Size);
        for (Int32 YI = result.YIL; YI <= result.YIH; YI++)
        {
          for (Int32 XI = result.XIL; XI <= result.XIH; XI++)
          {
            Double Sum = 0.0;
            for (Int32 SI = this.XIL; SI <= this.XIH; SI++)
            {
              Sum += this[YI, SI] * multiplicator[SI, XI];
            }
            result[YI, XI] = Sum;
          }
        }
        return true;
      }
      return false;
    }


    protected Boolean Multiply(CVector multiplicator, out CVector result)
    { // result(CVector) = this(CMatrix) * multiplicator(CVector)
      result = null;
      // check: ColCount(this) == Size(multiplicator)
      if (this.SizeX == multiplicator.Size)
      { // Size(result) := this.SizeY
        Int32 Size = this.SizeY;
        result = new CVector(FNotifier, Size);
        for (Int32 VI = result.VIL; VI <= result.VIH; VI++)
        {
          Double Sum = 0.0;
          for (Int32 SI = this.XIL; SI <= this.XIH; SI++)
          {
            Sum += this[VI, SI] * multiplicator[SI];
          }
          result[VI] = Sum;
        }
        return true;
      }
      return false;
    }


  }
}
