﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;

namespace NLVector
{
	public class CVector
	{
		//
		//
		//----------------------------------------
		//	Constant
		//----------------------------------------
		//
		private const Double INIT_VALUE = 0.0;
		//
		//----------------------------------------
		//	Member
		//----------------------------------------
		//
    private CNotifier FNotifier;
		private Double[] FValues = null;
		//
		//----------------------------------------
		//	Property
		//----------------------------------------
		//
		public Int32 VIL
		{
			get { return 0; }
		}
		public Int32 VIH
		{
			get { return Size - 1; }
		}
		public Int32 Size
		{
			get { return FValues.Length; }
		}
		//
		//----------------------------------------
		//	Constructor
		//----------------------------------------
		//
    public CVector(CNotifier notifier, Int32 size)
		{
      FNotifier = notifier;
      FValues = new Double[size];
			InitValues(INIT_VALUE);
		}

    public CVector(CNotifier notifier, Int32 size, Double initvalue)
		{
      FNotifier = notifier;
      FValues = new Double[size];
			InitValues(initvalue);
		}

    public CVector(CNotifier notifier, CVector v)
		{
      FNotifier = notifier;
      FValues = new Double[v.Size];
			for (Int32 I = VIL; I <= VIH; I++)
			{
				FValues[I] = v[I];
			}
		}

    public CVector(CNotifier notifier, CVector sourcevector, Int32 deltacount, Int32 copyoffset)
    {
      FNotifier = notifier;
      FValues = new Double[sourcevector.Size + deltacount];
      if (0 <= deltacount)
      {
        Int32 TI = copyoffset;
        for (Int32 VI = sourcevector.VIL; VI <= sourcevector.VIH; VI++)
        {
          FValues[TI++] = sourcevector[VI];
        }
      } else
      {
        Int32 SI = copyoffset;
        for (Int32 VI = this.VIL; VI <= this.VIH; VI++)
        {
          FValues[VI] = sourcevector[SI++];
        }
      }
    }
    //
		//
		//----------------------------------------
		//	Initialisation
		//----------------------------------------
		//
		public bool InitValues(Double initvalue)
		{
			for (Int32 VI = VIL; VI <= VIH; VI++)
			{
				FValues[VI] = initvalue;
			}
			return true;
		}
		//
		//
		//----------------------------------------
		//	Helper
		//----------------------------------------
		//
		protected Boolean CheckIndex(Int32 index)
		{
			return ((VIL <= index) & (index <= VIH));
		}

		public void Add(Double v)
		{
			Double[] V = new Double[1 + this.Size];
			for (Int32 VI = 0; VI < Size; VI++)
			{
				V[VI] = this[VI];
			}
			V[this.Size] = v;
			FValues = V;
		}

    public Boolean PresetConstant(Double presetvalue, Int32 startindex, Int32 presetcount)
    {
      try
      {
        for (Int32 VI = startindex; VI <= startindex + presetcount; VI++)
        {
          FValues[VI] = presetvalue;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
		//
		//
		//---------------------------------------
		//	Overloading of Index-Operator
		//---------------------------------------
		//
		protected Double GetValue(Int32 index)
		{
			if (CheckIndex(index))
			{
				return FValues[index];
			}
			return 0.0;
		}
		protected void SetValue(Int32 index, Double value)
		{
			if (CheckIndex(index))
			{
				FValues[index] = value;
			}
		}
		public Double this[Int32 index]
		{
			get { return GetValue(index); }
			set { SetValue(index, value); }
		}
		//
		//
		//----------------------------------------
		//	Protocol
		//----------------------------------------
		//
		public void Write(String header)
		{
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(String.Format("{0}[{1}]:", header, Size));
        for (Int32 VI = VIL; VI <= VIH; VI++)
        {
          String Line = String.Format(" V[{0}] = {1,7:0.000000000}",
                                      VI, this[VI]);
          FNotifier.Write(Line);
        }
      }
		}





	}
}
