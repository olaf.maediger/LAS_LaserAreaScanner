﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Initdata;

namespace UCNotifier
{

	public enum EHeaderStyle
	{
		None = 0,
		Millis = 1,
		Time = 2,
		Date = 3,
		DateTime = 4,
		TimeMillis = 5,
		DateTimeMillis = 6
	};

	public delegate void DOnRefresh();

	public struct RNotifierData
	{
		public EHeaderStyle HeaderStyle;
		public Boolean EnableProtocol;
		public Int32 ShrinkLevel;
		public Int32 ColumnTabulator;
		public String LogbookPath;
		public String LogbookFile;
		public String ApplicationName;
		public String VersionValue;
		public String CompanyName;
		public String UserName;
		public Boolean SaveOnApplicationEnd;
		public Int32 SaveOnLineCount;
		public Boolean InsertLinenumbers;
		public Boolean ClearAfterSave;
		public Int32 DirectoryFileLimit;
		public String SourceAddress;
		public String SmtpServer;
		public String UserID;
		public String Password;
		public Boolean EnableSsl;
		public Boolean SendOnError;
		public String TargetAddress;
		public String Subject;
		public String Attachment;
		public String Body;
		public DOnRefresh OnRefresh;
	};

	public class CNotifier : List<String>
	{	//
		//-------------------------------------
		//	Section - Constant
		//-------------------------------------
		//
		public const String HEADER_LIBRARY = "UCN";
		//
		private EHeaderStyle INIT_HEADERSTYLE = EHeaderStyle.DateTimeMillis;
		private Boolean INIT_ENABLEPROTOCOL = true;
		private Int32 INIT_SHRINKLEVEL = 0;
		private Int32 INIT_COLUMNTABULATOR = 0;
		private String INIT_LOGBOOKPATH = ".\\";
		private String INIT_LOGBOOKFILE = "Logbook.txt";
		private String INIT_APPLICATIONNAME = "";
		private String INIT_VERSIONVALUE = "";
		private String INIT_COMPANYNAME = "";
		private String INIT_USERNAME = "";
		private Boolean INIT_SAVEONAPPLICATIONEND = true;
		private Int32 INIT_SAVEONLINECOUNT = 500;
		private Boolean INIT_INSERTLINENUMBERS = true;
		private Boolean INIT_CLEARAFTERSAVE = true;
		private Int32 INIT_DIRECTORYFILELIMIT = 10;
		private String INIT_SOURCEADDRESS = "olaf.maediger@web.de";
		private String INIT_SMTPSERVER = "smtp.web.de";
		private String INIT_USERID = "olaf.maediger";
		private String INIT_PASSWORD = "superpio1";
		private Boolean INIT_ENABLESSL = true;
		private Boolean INIT_SENDONERROR = true;
		private String INIT_TARGETADDRESS = "olaf.maediger@llg-ev.de";
		private String INIT_SUBJECT = "Diagnostic";
		private String INIT_ATTACHMENT = "Diagnostic.txt";
		private String INIT_BODY = "This is an automatic generated diagnostic mail.";
		//
		private String NAME_HEADERSTYLE = "HeaderStyle";
		private String NAME_ENABLEPROTOCOL = "EnableProtocol";
		private String NAME_SHRINKLEVEL = "ShrinkLevel";
		private String NAME_COLUMNTABULATOR = "ColumnTabulator";
		private String NAME_LOGBOOKPATH = "LogbookPath";
		private String NAME_LOGBOOKFILE = "LogbookFile";
		private String NAME_APPLICATIONNAME = "ApplicationName";
		private String NAME_VERSIONVALUE = "VersionValue";
		private String NAME_COMPANYNAME = "CompanyName";
		private String NAME_USERNAME = "UserName";
		private String NAME_SAVEONAPPLICATIONEND = "SaveOnApplicationEnd";
		private String NAME_SAVEONLINECOUNT = "SaveOnLineCount";
		private String NAME_INSERTLINENUMBERS = "InsertLinenumbers";
		private String NAME_CLEARAFTERSAVE = "ClearAfterSave";
		private String NAME_DIRECTORYFILELIMIT = "DirectoryFileLimit";
		private String NAME_SOURCEADDRESS = "SourceAddress";
		private String NAME_SMTPSERVER = "SmtpServer";
		private String NAME_USERID = "UserID";
		private String NAME_PASSWORD = "Password";
		private String NAME_ENABLESSL = "EnableSsl";
		private String NAME_SENDONERROR = "SendOnError";
		private String NAME_TARGETADDRESS = "TargetAddress";
		private String NAME_SUBJECT = "Subject";
		private String NAME_ATTACHMENT = "Attachment";
		private String NAME_BODY = "Body";
		//
		//-------------------------------------
		//	Section - Member
		//-------------------------------------
		//
		RNotifierData FData;
		private Stopwatch FStopwatch;
		//
		//-------------------------------------
		//	Section - Constructor
		//-------------------------------------
		//
		public CNotifier()
		{
			FData.HeaderStyle = INIT_HEADERSTYLE;
			FData.EnableProtocol = INIT_ENABLEPROTOCOL;
			FData.ShrinkLevel = INIT_SHRINKLEVEL;
			FData.ColumnTabulator = INIT_COLUMNTABULATOR;
			FData.LogbookPath = INIT_LOGBOOKPATH;
			FData.LogbookFile = INIT_LOGBOOKFILE;
			FData.ApplicationName = INIT_APPLICATIONNAME;
			FData.VersionValue = INIT_VERSIONVALUE;
			FData.CompanyName = INIT_COMPANYNAME;
			FData.UserName = INIT_USERNAME;
			FData.SaveOnApplicationEnd = INIT_SAVEONAPPLICATIONEND;
			FData.SaveOnLineCount = INIT_SAVEONLINECOUNT;
			FData.InsertLinenumbers = INIT_INSERTLINENUMBERS;
			FData.ClearAfterSave = INIT_CLEARAFTERSAVE;
			FData.DirectoryFileLimit = INIT_DIRECTORYFILELIMIT;
			FData.SourceAddress = INIT_SOURCEADDRESS;
			FData.SmtpServer = INIT_SMTPSERVER;
			FData.UserID = INIT_USERID;
			FData.Password = INIT_PASSWORD;
			FData.EnableSsl = INIT_ENABLESSL;
			FData.SendOnError = INIT_SENDONERROR;
			FData.TargetAddress = INIT_TARGETADDRESS;
			FData.Subject = INIT_SUBJECT;
			FData.Attachment = INIT_ATTACHMENT;
			FData.Body = INIT_BODY;
			FData.OnRefresh = null;
			FStopwatch = new Stopwatch();
			FStopwatch.Start();
		}
		//
		//-------------------------------------
		//	Section - Initialisation
		//-------------------------------------
		//
		public Boolean LoadInitdata(CInitdata initdata)
		{
			String S;
			Int32 I;
			Boolean B;
			// 
			S = INIT_HEADERSTYLE.ToString();
			initdata.ReadEnumeration(NAME_HEADERSTYLE, out S, S);
			if (EHeaderStyle.None.ToString() == S)
			{
				FData.HeaderStyle = EHeaderStyle.None;
			} else
				if (EHeaderStyle.Millis.ToString() == S)
				{
					FData.HeaderStyle = EHeaderStyle.Millis;
				} else
					if (EHeaderStyle.Time.ToString() == S)
					{
						FData.HeaderStyle = EHeaderStyle.Time;
					} else
						if (EHeaderStyle.Date.ToString() == S)
						{
							FData.HeaderStyle = EHeaderStyle.Date;
						} else
							if (EHeaderStyle.TimeMillis.ToString() == S)
							{
								FData.HeaderStyle = EHeaderStyle.TimeMillis;
							} else
								if (EHeaderStyle.DateTime.ToString() == S)
								{
									FData.HeaderStyle = EHeaderStyle.DateTime;
								} else
								{
									FData.HeaderStyle = EHeaderStyle.DateTimeMillis;
								}
			//
			initdata.ReadBoolean(NAME_ENABLEPROTOCOL, out B, INIT_ENABLEPROTOCOL);
			FData.EnableProtocol = B;
			//
			initdata.ReadInteger(NAME_SHRINKLEVEL, out I, INIT_SHRINKLEVEL);
			FData.ShrinkLevel = I;
			//
			initdata.ReadInteger(NAME_COLUMNTABULATOR, out I, INIT_COLUMNTABULATOR);
			FData.ColumnTabulator = I;
			//
			initdata.ReadString(NAME_LOGBOOKPATH, out S, INIT_LOGBOOKPATH);
			// RO FData.ProtocolPath = S;
			//
			initdata.ReadString(NAME_LOGBOOKFILE, out S, INIT_LOGBOOKFILE);
			// RO FData.ProtocolFile = S;
			//
			initdata.ReadString(NAME_APPLICATIONNAME, out S, INIT_APPLICATIONNAME);
			// RO FData.ApplicationName = S;
			//
			initdata.ReadString(NAME_VERSIONVALUE, out S, INIT_VERSIONVALUE);
			// RO FData.VersionValue = S;
			//
			initdata.ReadString(NAME_COMPANYNAME, out S, INIT_COMPANYNAME);
			if (0 < S.Length)
			{
				FData.CompanyName = S;
			}
			//
			initdata.ReadString(NAME_USERNAME, out S, INIT_USERNAME);
			if (0 < S.Length)
			{
				FData.UserName = S;
			}
			//
			initdata.ReadBoolean(NAME_SAVEONAPPLICATIONEND, out B, INIT_SAVEONAPPLICATIONEND);
			FData.SaveOnApplicationEnd = B;
			//
			initdata.ReadInteger(NAME_SAVEONLINECOUNT, out I, INIT_SAVEONLINECOUNT);
			FData.SaveOnLineCount = I;
			//
			initdata.ReadBoolean(NAME_INSERTLINENUMBERS, out B, INIT_INSERTLINENUMBERS);
			FData.InsertLinenumbers = B;
			//
			initdata.ReadBoolean(NAME_CLEARAFTERSAVE, out B, INIT_CLEARAFTERSAVE);
			FData.ClearAfterSave = B;
			//
			initdata.ReadInteger(NAME_DIRECTORYFILELIMIT, out I, INIT_DIRECTORYFILELIMIT);
			FData.DirectoryFileLimit = I;
			//
			initdata.ReadString(NAME_SOURCEADDRESS, out S, INIT_SOURCEADDRESS);
			FData.SourceAddress = S;
			//
			initdata.ReadString(NAME_SMTPSERVER, out S, INIT_SMTPSERVER);
			FData.SmtpServer = S;
			//
			initdata.ReadString(NAME_USERID, out S, INIT_USERID);
			FData.UserID = S;
			//
			initdata.ReadString(NAME_PASSWORD, out S, INIT_PASSWORD);
			FData.Password = S;
			//
			initdata.ReadBoolean(NAME_ENABLESSL, out B, INIT_ENABLESSL);
			FData.EnableSsl = B;
			//
			initdata.ReadBoolean(NAME_SENDONERROR, out B, INIT_SENDONERROR);
			FData.SendOnError = B;
			//
			initdata.ReadString(NAME_TARGETADDRESS, out S, INIT_TARGETADDRESS);
			FData.TargetAddress = S;
			//
			initdata.ReadString(NAME_SUBJECT, out S, INIT_SUBJECT);
			FData.Subject = S;
			//
			initdata.ReadString(NAME_ATTACHMENT, out S, INIT_ATTACHMENT);
			FData.Attachment = S;
			//
			initdata.ReadString(NAME_BODY, out S, INIT_BODY);
			FData.Body = S;
			//
			return true;
		}

		public Boolean SaveInitdata(CInitdata initdata)
		{
			initdata.WriteEnumeration(NAME_HEADERSTYLE, FData.HeaderStyle.ToString());
			//
			initdata.WriteBoolean(NAME_ENABLEPROTOCOL, FData.EnableProtocol);
			//
			initdata.WriteInteger(NAME_SHRINKLEVEL, FData.ShrinkLevel);
			//
			initdata.WriteInteger(NAME_COLUMNTABULATOR, FData.ColumnTabulator);
			//
			initdata.WriteString(NAME_LOGBOOKPATH, FData.LogbookPath);
			//
			initdata.WriteString(NAME_LOGBOOKFILE, FData.LogbookFile);
			//
			initdata.WriteString(NAME_APPLICATIONNAME, FData.ApplicationName);
			//
			initdata.WriteString(NAME_VERSIONVALUE, FData.VersionValue);
			//
			initdata.WriteString(NAME_COMPANYNAME, FData.CompanyName);
			//
			initdata.WriteString(NAME_USERNAME, FData.UserName);
			//
			initdata.WriteBoolean(NAME_SAVEONAPPLICATIONEND, FData.SaveOnApplicationEnd);
			//
			initdata.WriteInteger(NAME_SAVEONLINECOUNT, FData.SaveOnLineCount);
			//
			initdata.WriteBoolean(NAME_INSERTLINENUMBERS, FData.InsertLinenumbers);
			//
			initdata.WriteBoolean(NAME_CLEARAFTERSAVE, FData.ClearAfterSave);
			//
			initdata.WriteInteger(NAME_DIRECTORYFILELIMIT, FData.DirectoryFileLimit);
			//			
			initdata.WriteString(NAME_SOURCEADDRESS, FData.SourceAddress);
			//
			initdata.WriteString(NAME_SMTPSERVER, FData.SmtpServer);
			//
			initdata.WriteString(NAME_USERID, FData.UserID);
			//
			initdata.WriteString(NAME_PASSWORD, FData.Password);
			//
			initdata.WriteBoolean(NAME_ENABLESSL, FData.EnableSsl);
			//
			initdata.WriteBoolean(NAME_SENDONERROR, FData.SendOnError);
			//
			initdata.WriteString(NAME_TARGETADDRESS, FData.TargetAddress);
			//
			initdata.WriteString(NAME_SUBJECT, FData.Subject);
			//
			initdata.WriteString(NAME_ATTACHMENT, FData.Attachment);
			//
			initdata.WriteString(NAME_BODY, FData.Body);
			//
			return true;
		}
		//
		//
		//-------------------------------------
		//	Section - Property
		//-------------------------------------
		//
		public Boolean GetData(ref RNotifierData data)
		{
			data.HeaderStyle = FData.HeaderStyle;
			data.EnableProtocol = FData.EnableProtocol;
			data.ShrinkLevel = FData.ShrinkLevel;
			data.ColumnTabulator = FData.ColumnTabulator;
			data.LogbookPath = FData.LogbookPath;									// RO
			data.LogbookFile = FData.LogbookFile;									// RO
			data.ApplicationName = FData.ApplicationName;
			data.VersionValue = FData.VersionValue;
			data.CompanyName = FData.CompanyName;
			data.UserName = FData.UserName;
			data.SaveOnApplicationEnd = FData.SaveOnApplicationEnd;
			data.SaveOnLineCount = FData.SaveOnLineCount;
			data.InsertLinenumbers = FData.InsertLinenumbers;
			data.ClearAfterSave = FData.ClearAfterSave;							// RO true
			data.DirectoryFileLimit = FData.DirectoryFileLimit;
			data.SourceAddress = FData.SourceAddress;
			data.SmtpServer = FData.SmtpServer;
			data.UserID = FData.UserID;
			data.Password = FData.Password;
			data.EnableSsl = FData.EnableSsl;
			data.SendOnError = FData.SendOnError;
			data.TargetAddress = FData.TargetAddress;
			data.Subject = FData.Subject;
			data.Attachment = FData.Attachment;
			data.Body = FData.Body;
			data.OnRefresh = FData.OnRefresh;
			return true;
		}

		public Boolean SetData(RNotifierData data)
		{
			FData.HeaderStyle = data.HeaderStyle;
			FData.EnableProtocol = data.EnableProtocol;
			FData.ShrinkLevel = data.ShrinkLevel;
			FData.ColumnTabulator = data.ColumnTabulator;
			// RO FData.LogbookPath = data.LogbookPath;
			// RO FData.LogbookFile = data.LogbookFile;
			FData.ApplicationName = data.ApplicationName;
			FData.VersionValue = data.VersionValue;
			FData.CompanyName = data.CompanyName;
			FData.UserName = data.UserName;
			FData.SaveOnApplicationEnd = data.SaveOnApplicationEnd;
			FData.SaveOnLineCount = data.SaveOnLineCount;
			FData.InsertLinenumbers = data.InsertLinenumbers;
			// FData.ClearAfterSave = data.ClearAfterSave;
			FData.DirectoryFileLimit = data.DirectoryFileLimit;
			FData.SourceAddress = data.SourceAddress;
			FData.SmtpServer = data.SmtpServer;
			FData.UserID = data.UserID;
			FData.Password = data.Password;
			FData.EnableSsl = data.EnableSsl;
			FData.SendOnError = data.SendOnError;
			FData.TargetAddress = data.TargetAddress;
			FData.Subject = data.Subject;
			FData.Attachment = data.Attachment;
			FData.Body = data.Body;
			FData.OnRefresh = data.OnRefresh;
			// ???
			if (FData.OnRefresh is DOnRefresh)
			{
				FData.OnRefresh();
			}
			return true;
		}
		//
		//-------------------------------------
		//	Section - ProtocolFolder
		//-------------------------------------
		//
		[DllImport("shfolder.dll", CharSet = CharSet.Auto)]

		private static extern int SHGetFolderPath(IntPtr hwndOwner, 
																							int nFolder, 
																							IntPtr hToken, 
																							int dwFlags, 
																							StringBuilder lpszPath);
		private const int MAX_PATH = 260;
		private const int CSIDL_COMMON_DESKTOPDIRECTORY = 0x0019;

		public string GetAllUsersDesktopFolderPath()
		{
			StringBuilder Buffer = new StringBuilder(MAX_PATH);
			SHGetFolderPath(IntPtr.Zero, CSIDL_COMMON_DESKTOPDIRECTORY, IntPtr.Zero, 0, Buffer);
			return Buffer.ToString();
		}
    //
    //-------------------------------------
    //	Section - MyDocumentsFolder
    //-------------------------------------
    //
		public String GetMyDocumentsFolderPath()
		{
			String MD = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			return MD;
		}
		//
		//-------------------------------------
		//	Section - Stopwatch
		//-------------------------------------
		//
		public void Init(DOnRefresh onrefresh)
		{
			FData.OnRefresh = onrefresh;
		}

		public void StartTime()
		{
			FStopwatch.Start();
		}

		public void StopTime()
		{
			FStopwatch.Stop();
		}

		public void ResetTime()
		{
			FStopwatch.Reset();
		}
		//
		//-------------------------------------
		//	Section - Helper 
		//-------------------------------------
		//
		private new void Add(String line)
		{
      try
      {
        if (FData.SaveOnLineCount <= this.Count)
        {
          SaveLogbookEntries();
          if (FData.ClearAfterSave)
          {
            this.Clear();
          }
        }
        base.Add(line);
        if (FData.OnRefresh is DOnRefresh)
        {
          FData.OnRefresh();
        }
      }
      catch (Exception)
      {
      }
		}

		public void EnableProtocol(Boolean enable)
		{
			FData.EnableProtocol = enable;
			String Line = "[Notice]: ";
			if (FData.EnableProtocol)
			{
				Line += "Protocol enabled.";
			} else
			{
				Line += "Protocol disabled.";
			}
			Add(Line);
		}

		public void ShrinkLevel(Boolean shrink)
		{
			String Line = "[Notice]: ";
			if (shrink)
			{
				FData.ShrinkLevel++;
			} else
			{
				FData.ShrinkLevel = Math.Max(0, FData.ShrinkLevel - 1);
			}
			Line += String.Format("Column Shrink Level = {0}", FData.ShrinkLevel);
			Add(Line);
		}

		public void ShrinkZero()
		{
			String Line = "[Notice]: ";
			FData.ShrinkLevel = 0;
			Line += String.Format("Column Shrink Level = {0}", FData.ShrinkLevel);
			Add(Line);
		}

		public void SetHeaderStyle(EHeaderStyle presetmode)
		{
			String Line = "[Notice]: ";
			FData.HeaderStyle = presetmode;
			Line += String.Format("Header Style = {0}", FData.HeaderStyle.ToString());
			Add(Line);
		}

		public void Space(Boolean space)
		{
			if (space)
			{
				FData.ColumnTabulator++;
			} else
			{
				FData.ColumnTabulator = Math.Max(0, FData.ColumnTabulator - 1);
			}
		}
		public void SpaceZero()
		{
			FData.ColumnTabulator = 0;
		}

		private String FormatHeader(String header)
		{
			String Spaces = "";
			for (Int32 CI = 0; CI < FData.ColumnTabulator; CI++)
			{
				Spaces += "  ";
			}
			switch (FData.HeaderStyle)
			{
				case EHeaderStyle.Millis:
					return String.Format("{0:000}[{1}]> {2}",
															 (UInt16)(FStopwatch.ElapsedMilliseconds % 1000),
															 header, Spaces);
				case EHeaderStyle.Time:
					return String.Format("{0}[{1}]> {2}", DateTime.Now.TimeOfDay, header, Spaces);
				case EHeaderStyle.Date:
					String Date = DateTime.Now.Date.ToString().Substring(0, 10);
					return String.Format("{0}[{1}]> {2}", Date, header, Spaces);
				case EHeaderStyle.DateTime:
					return String.Format("{0}[{1}]> {2}", DateTime.Now, header, Spaces);
				case EHeaderStyle.TimeMillis:
					return String.Format("{0}.{1:000}[{2}]> {3}",
															 DateTime.Now.TimeOfDay,
															 (UInt16)(FStopwatch.ElapsedMilliseconds % 1000),
															 header, Spaces);					
				case EHeaderStyle.DateTimeMillis:
					return String.Format("{0}.{1:000}[{2}]> {3}",
															 DateTime.Now,
															 (UInt16)(FStopwatch.ElapsedMilliseconds % 1000),
															 header, Spaces);					
			}
			// Default:
			return Spaces;
		}


		private String BuildPresetLine(String line)
		{
			return FormatHeader("") + line;
		}

		private String BuildPresetLine(String header, String line)
		{
			return FormatHeader(header) + line;
		}


		public void Write(String line)
		{
			if (FData.EnableProtocol && (0 == FData.ShrinkLevel))
			{
				Add(BuildPresetLine(line));
				if (FData.OnRefresh is DOnRefresh)
				{
					FData.OnRefresh();
				}
			}
		}

		public void Write(String header,
											String line)
		{
			if (FData.EnableProtocol && (0 == FData.ShrinkLevel))
			{
				Add(BuildPresetLine(header, line));
			}
		}

		public void Write(String[] lines)
		{
			if (FData.EnableProtocol && (0 == FData.ShrinkLevel))
			{
				foreach (String Line in lines)
				{
					Add(BuildPresetLine(Line));
				}
				if (FData.OnRefresh is DOnRefresh)
				{
					FData.OnRefresh();
				}
			}
		}

		public void Write(String header,
											String[] lines)
		{
			if (FData.EnableProtocol && (0 == FData.ShrinkLevel))
			{
				foreach (String Line in lines)
				{
					Add(BuildPresetLine(header, Line));
				}
				if (FData.OnRefresh is DOnRefresh)
				{
					FData.OnRefresh();
				}
			}
		}

		private String BuidDateTimeDiagnosticEntry(String fileentry)
		{
			DateTime DateTime = DateTime.Now;
			Int32 Year = DateTime.Year % 100;
			String Entry = Year.ToString("00");
			Entry += DateTime.Month.ToString("00");
			Entry += DateTime.Day.ToString("00");
			Entry += DateTime.Hour.ToString("00");
			Entry += DateTime.Minute.ToString("00");
			Entry += DateTime.Second.ToString("00");
			Entry += FData.ApplicationName;
			Entry += FData.VersionValue;
			// Integrate preset filename from fileentry
			if (0 < fileentry.Length)
			{
				Entry += Path.GetFileName(fileentry);
			} else
			{
				Entry += INIT_ATTACHMENT;
			}
			Entry = Path.Combine(FData.LogbookPath, Entry);
			return Entry;
		}
    //
    //
    //-------------------------------------
		//	Section - Methods - Error
    //-------------------------------------
    //
		public void Error(String header,
											Int32 errorcode,
											String errortext)
		{
			String Text = String.Format("{0} Error[{1}]: {2}", header, errorcode, errortext);
			Add(BuildPresetLine("", Text));
			if (FData.OnRefresh is DOnRefresh)
			{
				FData.OnRefresh();
			}
		}
		//
    //
    //-------------------------------------
    //	Methods - Logbook
    //-------------------------------------
    //
		public Boolean SetProtocolParameter(String targetpath,
																				String applicationname,
																				String versionvalue,
																				String companyname,
																				String username)
		{
			FData.LogbookPath = GetAllUsersDesktopFolderPath();
			FData.LogbookPath = Path.Combine(FData.LogbookPath, targetpath);
			if (!Directory.Exists(FData.LogbookPath))
			{
				Directory.CreateDirectory(FData.LogbookPath);
			}
			FData.ApplicationName = applicationname;
			FData.VersionValue = versionvalue;
			FData.CompanyName = companyname;
			FData.UserName = username;
			return true;
		}


		public String BuildDateTimeFileEntry(String filenameextension)
		{
			DateTime DateTime = DateTime.Now;
			Int32 Year = DateTime.Year % 100;
			String Filename = Year.ToString("00");
			Filename += DateTime.Month.ToString("00");
			Filename += DateTime.Day.ToString("00");
			Filename += DateTime.Hour.ToString("00");
			Filename += DateTime.Minute.ToString("00");
			Filename += DateTime.Second.ToString("00");
			UInt16 M = (UInt16)(FStopwatch.ElapsedMilliseconds % 1000);
			Filename += String.Format("{0:000}", M);
			Filename += filenameextension;
			Filename = Path.Combine(FData.LogbookPath, Filename);
			return Filename;
		}

		private String BuildDateTimeFileEntryLogbookFile()
		{
			return BuildDateTimeFileEntry(FData.LogbookFile);
		}

		public void ReduceLogbookFiles()
		{
			DirectoryInfo DI = new DirectoryInfo(FData.LogbookPath);
			FileInfo[] FileInfos = DI.GetFiles("*Logbook.txt");
			if (0 < FileInfos.Length)
			{
				DateTime[] CreationTimes = new DateTime[FileInfos.Length];
				for (Int32 FI = 0; FI < FileInfos.Length; FI++)
				{
					CreationTimes[FI] = FileInfos[FI].CreationTime;
				}
				Array.Sort(CreationTimes, FileInfos);
				Array.Reverse(FileInfos);
				/*/ Debug
				for (Int32 FI = 0; FI < FileInfos.Length; FI++)
				{
					Console.WriteLine("{0} : {1}", FI, FileInfos[FI]);
				}*/
				// Deleting all files older than the (DirectoryFileLimit-1)-Element:
				for (Int32 FI = FData.DirectoryFileLimit; FI < FileInfos.Length; FI++)
				{
					Write(HEADER_LIBRARY, String.Format("Delete {0} : {1}", FI, FileInfos[FI]));
					String FileName = Path.Combine(FData.LogbookPath, FileInfos[FI].ToString());
					File.Delete(FileName);					
				}
			}
		}

		private Boolean CreateDiagnosticFile(ref String diagnosticentry)
		{
			DirectoryInfo DI = new DirectoryInfo(FData.LogbookPath);
			FileInfo[] FileInfos = DI.GetFiles("*Logbook.txt");
			if (0 < FileInfos.Length)
			{
				diagnosticentry = BuidDateTimeDiagnosticEntry(diagnosticentry);
				StreamWriter SW = new StreamWriter(diagnosticentry, false, Encoding.UTF8);

				// Copying all Protocol-Files to one Diagnostic-File:
				for (Int32 FI = 0; FI < FileInfos.Length; FI++)
				{
					String LogbookFile = Path.Combine(FData.LogbookPath, FileInfos[FI].ToString());
					StreamReader SR = new StreamReader(LogbookFile, Encoding.UTF8, false);
					String ReadBuffer = SR.ReadToEnd();
					SR.Close();
					//
					Write(HEADER_LIBRARY, String.Format("Adding {0} to {1}", FileInfos[FI], INIT_ATTACHMENT));
					//
					SW.Write(ReadBuffer);
				}
				SW.Close();
				return true;
			}
			return false;
		}

		public Boolean LoadLogbookEntries(String filename)
		{
			try
			{
				StreamReader SR = new StreamReader(filename, Encoding.UTF8, false);
				String Buffer = SR.ReadToEnd();
				SR.Close();
				//
				Char[] Delimiters = new Char[] { '\t', '\r', '\n' };
				//((Char)0x09, (Char)0x20, (Char)0x0D, (Char)0x0A);
				String[] Lines = Buffer.Split(Delimiters, StringSplitOptions.RemoveEmptyEntries);
				this.Clear();
				foreach (String Line in Lines)
				{
					Add(Line);
				}
				if (FData.OnRefresh is DOnRefresh)
				{
					FData.OnRefresh();
				}
				//
				return true;
			}
			catch (Exception)
			{
				//Error(
				return false;
			}
		}

		private void SaveHeader(StreamWriter streamwriter)
		{
			DateTime DateTime = DateTime.Now;
			//Int32 Year = DateTime.Year % 100;
			Int32 Year = DateTime.Year % 100;
			//
			const String LINE_COMMENT = "//------------------------------------------------";
			//
			streamwriter.WriteLine(LINE_COMMENT);
			//
			String Line = String.Format("// Application: {0}", FData.ApplicationName);
			streamwriter.WriteLine(Line);
			//
			Line = String.Format("// Version....: {0}", FData.VersionValue);
			streamwriter.WriteLine(Line);
			//
			Line = DateTime.Day.ToString("00");
			Line += "." + DateTime.Month.ToString("00");
			Line += "." + DateTime.Year.ToString("0000");
			Line = String.Format("// Date.......: {0}", Line);
			streamwriter.WriteLine(Line);
			//
			Line = DateTime.Hour.ToString("00");
			Line += ":" + DateTime.Minute.ToString("00");
			Line += ":" + DateTime.Second.ToString("00");
			Line = String.Format("// Time.......: {0}", Line);
			streamwriter.WriteLine(Line);
			//
			Line = String.Format("// Company....: {0}", FData.CompanyName);
			streamwriter.WriteLine(Line);
			//
			Line = String.Format("// User.......: {0}", FData.UserName);
			streamwriter.WriteLine(Line);
			//
			streamwriter.WriteLine(LINE_COMMENT);
		}

		// Save actual Logbook-Entries (Listbox!)
		public Boolean SaveLogbookEntries(String filename)
		{
			try
			{	// Analyse Path and File
				StreamWriter SW = new StreamWriter(filename, false, Encoding.UTF8);
				SaveHeader(SW);
				for (Int32 LI = 0; LI < Count; LI++)
				{
					String Line = this[LI];
					if (FData.InsertLinenumbers)
					{
						Line = String.Format("[{0:0000}] {1}", 1 + LI, Line);
					}
					SW.WriteLine(Line);
				}
				SW.Close();
				//
				if (FData.ClearAfterSave)
				{
					this.Clear();
					if (FData.OnRefresh is DOnRefresh)
					{
						FData.OnRefresh();
					}
				}
				//
				ReduceLogbookFiles();
				//
				return true;
			}
			catch (Exception)
			{
				//Error(
				return false;
			}
		}

		public Boolean SaveLogbookEntries()
		{
			return SaveLogbookEntries(BuildDateTimeFileEntryLogbookFile());
		}

		// Sum of all Protocol-Files -> Diagnostic-File
		public Boolean SaveDiagnosticFile(String fileentry)
		{
			try
			{	// Analyse Path and File
				Write(HEADER_LIBRARY, "Creating Diagnostic File...");
				SaveLogbookEntries();
				String FilePath = Path.GetDirectoryName(fileentry);
				String FileName = Path.GetFileName(fileentry);
				String DiagnosticEntry = FileName;
				if (CreateDiagnosticFile(ref DiagnosticEntry))
				{
					FileName = Path.GetFileName(DiagnosticEntry);
					String FileEntry = Path.Combine(FilePath, FileName);
					File.Copy(DiagnosticEntry, FileEntry);
					return true;
				}
				return false;
			}
			catch (Exception)
			{
				//Error(
				return false;
			}
		}

		// Load Diagnostic-File
		public Boolean LoadDiagnosticFile(String filename)
		{
			try
			{	
				return LoadLogbookEntries(filename);
			}
			catch (Exception)
			{
				//Error(
				return false;
			}
		}

		public Boolean SendDiagnosticEmail()
		{
			try
			{
				Write(HEADER_LIBRARY, "Try to send a Diagnostic Email...");
				SaveLogbookEntries();
				String DiagnosticEntry = INIT_ATTACHMENT;
				if (CreateDiagnosticFile(ref DiagnosticEntry))
				{					
					MailMessage Email = new MailMessage();
					SmtpClient SmtpServer = new SmtpClient(FData.SmtpServer);
					//
					Email.From = new MailAddress(FData.SourceAddress);
					Email.To.Add(FData.TargetAddress);
					Email.Subject = FData.Subject;
					Email.Attachments.Add(new Attachment(DiagnosticEntry));
					Email.Body = FData.Body;
					SmtpServer.Credentials = new System.Net.NetworkCredential(FData.UserID,
																																		FData.Password);
					SmtpServer.EnableSsl = FData.EnableSsl;
					//
					SmtpServer.Send(Email);
					MessageBox.Show("Email correctly sended");
					Write(HEADER_LIBRARY, "Email correctly sended");
					return true;
				}
				return false;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
				return false;
			}
		}


	}
}
