﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;


namespace UCNotifier
{
	public partial class CUCNotifierBase : UserControl
	{

    public const String SECTION_NOTIFIER = "Notifier";
    public const String NAME_HEIGHT = "Height";
    public const Int32 INIT_HEIGHT = 146;
		//
		private static readonly String[] ERRORS = 
    { 
      "None",
      "Access failed",
      "Port failed",
      "Open failed",
      "Close failed",
      "Read failed",
      "Write failed",
  	  "Invalid"
    };
		//
		//--------------------------------------
		//	Section - Type
		//--------------------------------------
		//
		protected enum EErrorCode
		{
			None = 0,
			AccessFailed,
			PortFailed,
			OpenFailed,
			CloseFailed,
			ReadFailed,
			WriteFailed,
			Invalid
		};
		//
		//-----------------------------------
		//  Section - Member
		//-----------------------------------
		//
		private CNotifier FNotifier;
		private OpenFileDialog FDialogLoadDiagnostic;
		private SaveFileDialog FDialogSaveDiagnostic;
		//
		//-----------------------------------
		//  Section - Constructor
		//-----------------------------------
		//
		public CUCNotifierBase()
		{
			InitializeComponent();
			FNotifier = new CNotifier();
			FNotifier.Init(SelfOnRefresh);
			// 
			lbxNotifier.Dock = DockStyle.Fill;
			//
			FDialogLoadDiagnostic = new OpenFileDialog();
			FDialogSaveDiagnostic = new SaveFileDialog();
			FDialogLoadDiagnostic.FileName = "Diagnostic";
			FDialogSaveDiagnostic.FileName = "Diagnostic";
			FDialogLoadDiagnostic.Filter = "Diagnostic files (*.txt)|*.txt|All files (*.*)|*.*";
			FDialogSaveDiagnostic.Filter = "Diagnostic files (*.txt)|*.txt|All files (*.*)|*.*";
			FDialogLoadDiagnostic.Title = "Load Diagnostic";
			FDialogSaveDiagnostic.Title = "Save Diagnostic";
		}
		//
		//-----------------------------------
		//  Section - Properties
		//-----------------------------------
		//
		protected CNotifier Notifier
		{
			get
			{
				return FNotifier;
			}
			set
			{
				FNotifier = value;
			}
		}
		// typecast: CUCNotifier -> CNotifier !!!
		public static implicit operator CNotifier(CUCNotifierBase ucnotifierbase)
		{
			return ucnotifierbase.Notifier;
		}
		//
		//--------------------------------------------------
		//	Messages
		//--------------------------------------------------
		//
		protected void _Error(EErrorCode code)
		{
			if (FNotifier is CNotifier)
			{
				Int32 Index = (Int32)code;
				if ((Index < 0) && (ERRORS.Length <= Index))
				{
					Index = ERRORS.Length - 1;
				}
				String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
				FNotifier.Write(CUCNotifier.HEADER_LIBRARY, Line);
			}
		}

		protected void _Error(Int32 code,
													String text)
		{
			if (FNotifier is CNotifier)
			{
				FNotifier.Error(CUCNotifier.HEADER_LIBRARY, code, text);
			}
		}

		protected void _Protocol(String line)
		{
			if (FNotifier is CNotifier)
			{
				FNotifier.Write(CUCNotifier.HEADER_LIBRARY, line);
			}
		}
		//
		//
		//-------------------------------------
		//	Section - Callbacks
		//-------------------------------------
		//
		private void SelfOnRefresh()
		{
			RefreshAddControl();
		}

		//
		//-----------------------------------
		//  Section - Helper
		//-----------------------------------
		//
		// Auto-Saving when closing the Application
		public void SaveOnClosing()
		{
			FNotifier.SaveLogbookEntries();
		}
		//
		//-----------------------------------
		//  Section - Event
		//-----------------------------------
		//

		//
		//
		//-------------------------------------
		//	Helper 
		//-------------------------------------
		//
		private delegate void CBRefreshClearControl();
		private void RefreshClearControl()
		{
			if (this.InvokeRequired)
			{
				CBRefreshClearControl CB = new CBRefreshClearControl(RefreshClearControl);
				Invoke(CB, new object[] { });
			} else
			{
				lbxNotifier.Items.Clear();
				for (Int32 LI = 0; LI < FNotifier.Count; LI++)
				{
					lbxNotifier.Items.Add(FNotifier[LI]);
				}
				lbxNotifier.SelectedIndex = lbxNotifier.Items.Count - 1;
			}
		}

		private delegate void CBRefreshAddControl();
		private void RefreshAddControl()
		{
			if (this.InvokeRequired)
			{
				CBRefreshAddControl CB = new CBRefreshAddControl(RefreshAddControl);
				Invoke(CB, new object[] { });
			} else
			{
        try
        {
          if (lbxNotifier.Items.Count < FNotifier.Count)
          {
            for (Int32 LI = lbxNotifier.Items.Count; LI < FNotifier.Count; LI++)
            {
              String Line = FNotifier[LI];
              if (Line is String)
              {
                lbxNotifier.Items.Add(Line);
              }
            }
            lbxNotifier.SelectedIndex = lbxNotifier.Items.Count - 1;
          }
          else
            if (FNotifier.Count < lbxNotifier.Items.Count)
            {
              for (Int32 LI = lbxNotifier.Items.Count - 1; FNotifier.Count <= LI; LI--)
              {
                lbxNotifier.Items.RemoveAt(LI);
              }
            }
        }
        catch (Exception)
        {
        }
			}
		}
		//
		//-------------------------------------
		//	Segment - Path-Management
		//-------------------------------------
		//   
		protected String GetAllUsersDesktopFolderPath()
		{
			return FNotifier.GetAllUsersDesktopFolderPath();
		}

		protected String GetMyDocumentsFolderPath()
		{
			return FNotifier.GetMyDocumentsFolderPath();
		}

		public String BuildDateTimeFileEntry(String filenameextension)
		{
			return FNotifier.BuildDateTimeFileEntry(filenameextension);
		}
		//
		//-------------------------------------
		//	Methods - Initialisation
		//-------------------------------------
		//
		protected Boolean LoadInitdata(CInitdata initdata)
		{
			//initdata.SelectSection(CInitdata.NodeBase, SECTION_LIBRARY);
			//
      Int32 IValue;
      initdata.ReadInteger(NAME_HEIGHT, out IValue, INIT_HEIGHT);
      Height = IValue;
      if (Height != IValue)
      { // docked -> send message to parent
        Parent.Height = IValue;
        // parent-Window not ok here: ParentForm.Height = IValue;
      }
      return FNotifier.LoadInitdata(initdata);
		}

		protected Boolean SaveInitdata(CInitdata initdata)
		{
			//initdata.SelectCreateSection(CInitdata.NodeBase, SECTION_LIBRARY);
			//
      initdata.WriteInteger(NAME_HEIGHT, Height);
      return FNotifier.SaveInitdata(initdata);
		}
		//
		//
		//-------------------------------------
		//	Methods - Common
		//-------------------------------------
		//
		private delegate void CBClear();
		protected void Clear()
		{
			if (this.InvokeRequired)
			{
				CBClear CB = new CBClear(Clear);
				Invoke(CB, new object[] { });
			} else
			{
				lbxNotifier.Items.Clear();
				FNotifier.Clear();
			}
		}

		private delegate void CBCopyToClipboard();
		public void CopyToClipboard()
		{
			if (this.InvokeRequired)
			{
				CBCopyToClipboard CB = new CBCopyToClipboard(CopyToClipboard);
				Invoke(CB, new object[] { });
			} else
			{
				StringBuilder Buffer = new StringBuilder();
				for (Int32 LI = 0; LI < lbxNotifier.Items.Count; LI++)
				{
					String Line = (String)FNotifier[LI];
					Buffer.Append(Line + "\r\n");
				}
				Clipboard.SetText(Buffer.ToString());
			}
		}

		//
		//
		//-------------------------------------
		//	Methods - Write
		//-------------------------------------
		//
		private delegate void CBWrite(String line);
		public void Write(String line)
		{
			if (this.InvokeRequired)
			{
				CBWrite CB = new CBWrite(Write);
				Invoke(CB, new object[] { line });
			} else
			{
				FNotifier.Write(line);
				RefreshAddControl();
			}
		}

		private delegate void CBWrite2(String header,
																	 String line);
		public void Write(String header,
											String line)
		{
			if (this.InvokeRequired)
			{
				CBWrite2 CB = new CBWrite2(Write);
				Invoke(CB, new object[] { header, line });
			} else
			{
				FNotifier.Write(header, line);
				RefreshAddControl();
			}
		}

		private delegate void CBWrite3(String[] lines);
		public void Write(String[] lines)
		{
			if (this.InvokeRequired)
			{
				CBWrite3 CB = new CBWrite3(Write);
				Invoke(CB, new object[] { lines });
			} else
			{
				FNotifier.Write(lines);
				RefreshAddControl();
			}
		}

		private delegate void CBWrite4(String header,
																	 String[] lines);
		public void Write(String header,
											String[] lines)
		{
			if (this.InvokeRequired)
			{
				CBWrite4 CB = new CBWrite4(Write);
				Invoke(CB, new object[] { header, lines });
			} else
			{
				FNotifier.Write(header, lines);
				RefreshAddControl();
			}
		}
		//
		//
		//-------------------------------------
		//	Methods - Error
		//-------------------------------------
		//
		private delegate void CBError(String header,
																	Int32 code,
																	String line);
		public void Error(String header,
											Int32 code,
											String line)
		{
			if (this.InvokeRequired)
			{
				CBError CB = new CBError(Error);
				Invoke(CB, new object[] { header, code, line });
			} else
			{ // kann nicht geblockt werden!!!
				String Line = String.Format("Error[{0}]: {1}", code, line);
				FNotifier.Write(Line);
				FNotifier.Add(Line);
				RefreshAddControl();
			}
		}
		//
		//
		//-------------------------------------
		//	Methods - Protocol
		//-------------------------------------
		//
		private delegate void CBProtocol(String header,
																		 String line);
		public void Protocol(String header,
												 String line)
		{
			if (this.InvokeRequired)
			{
				CBProtocol CB = new CBProtocol(Protocol);
				Invoke(CB, new object[] { header, line });
			} else
			{
				Write(header, line);
			}
		}

		//
		//-----------------------------------
		//	Section - Management
		//-----------------------------------
		//
		protected Boolean Add(Color forecolor, Color backcolor, String line)
		{
			lbxNotifier.Items.Add(line);
			return true;
		}

		protected Boolean Add(String line)
		{
			lbxNotifier.Items.Add(line);
			return true;
		}


		//
		//-----------------------------------
		//  Section - Menu
		//-----------------------------------
		//
		private void mitCopyToClipboard_Click(object sender, EventArgs e)
		{
			StringBuilder Buffer = new StringBuilder();
			for (Int32 LI = 0; LI < lbxNotifier.Items.Count; LI++)
			{
				String Line = lbxNotifier.Items[LI].ToString();
				Buffer.Append(Line + "\r\n");
			}
			if (0 < Buffer.Length)
			{
				Clipboard.SetText(Buffer.ToString());
			}
		}

		private void mitDeleteAll_Click(object sender, EventArgs e)
		{
			Clear();
		}
		//
		//
		//-------------------------------------
		//	Methods - Logbook
		//-------------------------------------
		//
		protected Boolean SetProtocolParameter(String targetpath,
																					 String applicationname,
																					 String versionvalue,
																					 String companyname,
																					 String username)
		{
			FNotifier.SetProtocolParameter(targetpath, 
																		 applicationname, versionvalue,
																		 companyname, username);
			RNotifierData NotifierData = new RNotifierData();
			FNotifier.GetData(ref NotifierData);
			// Load : Default directory -> Common ApplcationData / All Users / <name> / <version>
			FDialogLoadDiagnostic.InitialDirectory = NotifierData.LogbookPath;
			// Save : Default directory -> Eigene Dateien
			FDialogSaveDiagnostic.InitialDirectory = FNotifier.GetMyDocumentsFolderPath();
			return true;
		}

		protected Boolean EditParameterModal()
		{
			RNotifierData NotifierData = new RNotifierData();
			FNotifier.GetData(ref NotifierData);
			CDialogEditParameter DialogEditParameter = new CDialogEditParameter();
			DialogEditParameter.SetData(NotifierData);
			if (DialogResult.OK == DialogEditParameter.ShowDialog())
			{
				DialogEditParameter.GetData(ref NotifierData);
				return FNotifier.SetData(NotifierData);
			}
			return false;
		}

		protected Boolean LoadDiagnosticFileModal()
		{
			if (DialogResult.OK == FDialogLoadDiagnostic.ShowDialog())
			{
				return FNotifier.LoadDiagnosticFile(FDialogLoadDiagnostic.FileName);
			}
			return false;
		}

		protected Boolean SaveDiagnosticFileModal()
		{
			if (DialogResult.OK == FDialogSaveDiagnostic.ShowDialog())
			{
				return FNotifier.SaveDiagnosticFile(FDialogSaveDiagnostic.FileName);
			}
			return false;
		}

		protected Boolean SendDiagnosticEmail()
		{
			return FNotifier.SendDiagnosticEmail();
		}
    
	}
}
