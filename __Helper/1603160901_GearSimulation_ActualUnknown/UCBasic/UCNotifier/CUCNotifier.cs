﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;

namespace UCNotifier
{
	public delegate void DWrite(String line);

	public partial class CUCNotifier : CUCNotifierBase
	{
		//
		//-------------------------------------
		//	Constant
		//-------------------------------------
		//
		public const String HEADER_LIBRARY = "Notifier";
		public const String SECTION_LIBRARY = "Notifier";
		//
		//-------------------------------------
		//	Constructor
		//-------------------------------------
		//
		public CUCNotifier()
		{
			InitializeComponent();
		}
		//
		//-------------------------------------
		//	Methods - Initialisation
		//-------------------------------------
		//
		public new Boolean LoadInitdata(CInitdata initdata)
		{
			try
			{
				return base.LoadInitdata(initdata);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean SaveInitdata(CInitdata initdata)
		{
			try
			{
				return base.SaveInitdata(initdata);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}
		//
		//
		//-------------------------------------
		//	Methods - Common
		//-------------------------------------
		//
		public new void Clear()
		{
			try
			{
				base.Clear();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
			}
		}

		public new void CopyToClipboard()
		{
			try
			{
				base.CopyToClipboard();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
			}
		}
		//
		//-------------------------------------
		//	Segment - Path-Management
		//-------------------------------------
		//   
		public new String GetAllUsersDesktopFolderPath()
		{
			try
			{
				return base.GetAllUsersDesktopFolderPath();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return "";
			}
		}

		public new String GetMyDocumentsFolderPath()
		{
			try
			{
				return base.GetMyDocumentsFolderPath();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return "";
			}
		}

		public new String BuildDateTimeFileEntry(String filenameextension)
		{
			try
			{
				return base.BuildDateTimeFileEntry(filenameextension);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return "";
			}
		}
		//
		//
		//-------------------------------------
		//	Methods - Logbook
		//-------------------------------------
		//
		public new Boolean SetProtocolParameter(String targetpath,
																						String application,
																						String version,
																						String company,
																						String user)
		{
			try
			{
				return base.SetProtocolParameter(targetpath, application, version, company, user);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean EditParameterModal()
		{
			try
			{
				return base.EditParameterModal();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean LoadDiagnosticFileModal()
		{
			try
			{
				return base.LoadDiagnosticFileModal();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean SaveDiagnosticFileModal()
		{
			try
			{
				return base.SaveDiagnosticFileModal();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean SendDiagnosticEmail()
		{
			try
			{
				return base.SendDiagnosticEmail();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

	}
}
