﻿namespace UCNotifier
{
	partial class CDialogEditParameter
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.pnlDialog = new System.Windows.Forms.Panel();
      this.btnConfirm = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.groupBox5 = new System.Windows.Forms.GroupBox();
      this.cbxSendOnError = new System.Windows.Forms.CheckBox();
      this.rtbBody = new System.Windows.Forms.RichTextBox();
      this.tbxAttachment = new System.Windows.Forms.TextBox();
      this.tbxSubject = new System.Windows.Forms.TextBox();
      this.label16 = new System.Windows.Forms.Label();
      this.label17 = new System.Windows.Forms.Label();
      this.label20 = new System.Windows.Forms.Label();
      this.tbxTargetAddress = new System.Windows.Forms.TextBox();
      this.label23 = new System.Windows.Forms.Label();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.cbxEnableSsl = new System.Windows.Forms.CheckBox();
      this.tbxPassword = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.tbxUserID = new System.Windows.Forms.TextBox();
      this.lblUserID = new System.Windows.Forms.Label();
      this.tbxSourceAddress = new System.Windows.Forms.TextBox();
      this.label12 = new System.Windows.Forms.Label();
      this.tbxSmtpServer = new System.Windows.Forms.TextBox();
      this.label13 = new System.Windows.Forms.Label();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.tbxUserName = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.tbxCompanyName = new System.Windows.Forms.TextBox();
      this.label8 = new System.Windows.Forms.Label();
      this.tbxVersionValue = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.tbxApplicationName = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.nudColumnTabulator = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.nudShrinkLevel = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.nudDirectoryFileLimit = new System.Windows.Forms.NumericUpDown();
      this.label9 = new System.Windows.Forms.Label();
      this.cbxClearAfterSave = new System.Windows.Forms.CheckBox();
      this.cbxInsertLinenumbers = new System.Windows.Forms.CheckBox();
      this.cbxSaveOnApplicationEnd = new System.Windows.Forms.CheckBox();
      this.nudSaveOnLineCount = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.cbxEnableProtocol = new System.Windows.Forms.CheckBox();
      this.label1 = new System.Windows.Forms.Label();
      this.cbxHeaderStyle = new System.Windows.Forms.ComboBox();
      this.pnlDialog.SuspendLayout();
      this.panel1.SuspendLayout();
      this.groupBox5.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumnTabulator)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudShrinkLevel)).BeginInit();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDirectoryFileLimit)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSaveOnLineCount)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlDialog
      // 
      this.pnlDialog.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.pnlDialog.Controls.Add(this.btnConfirm);
      this.pnlDialog.Controls.Add(this.btnCancel);
      this.pnlDialog.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlDialog.Location = new System.Drawing.Point(0, 477);
      this.pnlDialog.Name = "pnlDialog";
      this.pnlDialog.Size = new System.Drawing.Size(620, 42);
      this.pnlDialog.TabIndex = 0;
      // 
      // btnConfirm
      // 
      this.btnConfirm.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnConfirm.Location = new System.Drawing.Point(320, 8);
      this.btnConfirm.Name = "btnConfirm";
      this.btnConfirm.Size = new System.Drawing.Size(75, 23);
      this.btnConfirm.TabIndex = 0;
      this.btnConfirm.Text = "Confirm";
      this.btnConfirm.UseVisualStyleBackColor = true;
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(222, 8);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 0;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // panel1
      // 
      this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.panel1.Controls.Add(this.groupBox5);
      this.panel1.Controls.Add(this.groupBox4);
      this.panel1.Controls.Add(this.groupBox3);
      this.panel1.Controls.Add(this.groupBox2);
      this.panel1.Controls.Add(this.groupBox1);
      this.panel1.Controls.Add(this.cbxEnableProtocol);
      this.panel1.Controls.Add(this.label1);
      this.panel1.Controls.Add(this.cbxHeaderStyle);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(620, 477);
      this.panel1.TabIndex = 1;
      // 
      // groupBox5
      // 
      this.groupBox5.Controls.Add(this.cbxSendOnError);
      this.groupBox5.Controls.Add(this.rtbBody);
      this.groupBox5.Controls.Add(this.tbxAttachment);
      this.groupBox5.Controls.Add(this.tbxSubject);
      this.groupBox5.Controls.Add(this.label16);
      this.groupBox5.Controls.Add(this.label17);
      this.groupBox5.Controls.Add(this.label20);
      this.groupBox5.Controls.Add(this.tbxTargetAddress);
      this.groupBox5.Controls.Add(this.label23);
      this.groupBox5.Location = new System.Drawing.Point(12, 282);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new System.Drawing.Size(594, 184);
      this.groupBox5.TabIndex = 9;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = " Email - Target ";
      // 
      // cbxSendOnError
      // 
      this.cbxSendOnError.AutoSize = true;
      this.cbxSendOnError.Checked = true;
      this.cbxSendOnError.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbxSendOnError.Location = new System.Drawing.Point(312, 23);
      this.cbxSendOnError.Name = "cbxSendOnError";
      this.cbxSendOnError.Size = new System.Drawing.Size(154, 17);
      this.cbxSendOnError.TabIndex = 14;
      this.cbxSendOnError.Text = "Automatically send on Error";
      this.cbxSendOnError.UseVisualStyleBackColor = true;
      // 
      // rtbBody
      // 
      this.rtbBody.Location = new System.Drawing.Point(79, 77);
      this.rtbBody.Name = "rtbBody";
      this.rtbBody.Size = new System.Drawing.Size(498, 96);
      this.rtbBody.TabIndex = 15;
      this.rtbBody.Text = "This is an automatic generated service-document.";
      // 
      // tbxAttachment
      // 
      this.tbxAttachment.Location = new System.Drawing.Point(379, 48);
      this.tbxAttachment.Name = "tbxAttachment";
      this.tbxAttachment.Size = new System.Drawing.Size(198, 20);
      this.tbxAttachment.TabIndex = 14;
      this.tbxAttachment.Text = "Diagnostic.txt";
      this.tbxAttachment.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tbxSubject
      // 
      this.tbxSubject.Location = new System.Drawing.Point(79, 48);
      this.tbxSubject.Name = "tbxSubject";
      this.tbxSubject.Size = new System.Drawing.Size(198, 20);
      this.tbxSubject.TabIndex = 14;
      this.tbxSubject.Text = "Diagnostic";
      this.tbxSubject.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Location = new System.Drawing.Point(11, 94);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(31, 13);
      this.label16.TabIndex = 12;
      this.label16.Text = "Body";
      // 
      // label17
      // 
      this.label17.AutoSize = true;
      this.label17.Location = new System.Drawing.Point(311, 51);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(61, 13);
      this.label17.TabIndex = 12;
      this.label17.Text = "Attachment";
      // 
      // label20
      // 
      this.label20.AutoSize = true;
      this.label20.Location = new System.Drawing.Point(11, 51);
      this.label20.Name = "label20";
      this.label20.Size = new System.Drawing.Size(43, 13);
      this.label20.TabIndex = 11;
      this.label20.Text = "Subject";
      // 
      // tbxTargetAddress
      // 
      this.tbxTargetAddress.Location = new System.Drawing.Point(79, 21);
      this.tbxTargetAddress.Name = "tbxTargetAddress";
      this.tbxTargetAddress.Size = new System.Drawing.Size(199, 20);
      this.tbxTargetAddress.TabIndex = 5;
      this.tbxTargetAddress.Text = "olaf.maediger@llg-ev.de";
      this.tbxTargetAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label23
      // 
      this.label23.AutoSize = true;
      this.label23.Location = new System.Drawing.Point(11, 24);
      this.label23.Name = "label23";
      this.label23.Size = new System.Drawing.Size(45, 13);
      this.label23.TabIndex = 6;
      this.label23.Text = "Address";
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.cbxEnableSsl);
      this.groupBox4.Controls.Add(this.tbxPassword);
      this.groupBox4.Controls.Add(this.label10);
      this.groupBox4.Controls.Add(this.tbxUserID);
      this.groupBox4.Controls.Add(this.lblUserID);
      this.groupBox4.Controls.Add(this.tbxSourceAddress);
      this.groupBox4.Controls.Add(this.label12);
      this.groupBox4.Controls.Add(this.tbxSmtpServer);
      this.groupBox4.Controls.Add(this.label13);
      this.groupBox4.Location = new System.Drawing.Point(12, 178);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(594, 99);
      this.groupBox4.TabIndex = 8;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = " Email - Source ";
      // 
      // cbxEnableSsl
      // 
      this.cbxEnableSsl.AutoSize = true;
      this.cbxEnableSsl.Checked = true;
      this.cbxEnableSsl.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbxEnableSsl.Location = new System.Drawing.Point(80, 72);
      this.cbxEnableSsl.Name = "cbxEnableSsl";
      this.cbxEnableSsl.Size = new System.Drawing.Size(85, 17);
      this.cbxEnableSsl.TabIndex = 13;
      this.cbxEnableSsl.Text = "Enable Ssl   ";
      this.cbxEnableSsl.UseVisualStyleBackColor = true;
      // 
      // tbxPassword
      // 
      this.tbxPassword.Location = new System.Drawing.Point(379, 45);
      this.tbxPassword.Name = "tbxPassword";
      this.tbxPassword.PasswordChar = '#';
      this.tbxPassword.Size = new System.Drawing.Size(199, 20);
      this.tbxPassword.TabIndex = 9;
      this.tbxPassword.Text = "superpio1";
      this.tbxPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(311, 48);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(53, 13);
      this.label10.TabIndex = 10;
      this.label10.Text = "Password";
      // 
      // tbxUserID
      // 
      this.tbxUserID.Location = new System.Drawing.Point(79, 45);
      this.tbxUserID.Name = "tbxUserID";
      this.tbxUserID.Size = new System.Drawing.Size(198, 20);
      this.tbxUserID.TabIndex = 7;
      this.tbxUserID.Text = "olaf.maediger";
      this.tbxUserID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // lblUserID
      // 
      this.lblUserID.AutoSize = true;
      this.lblUserID.Location = new System.Drawing.Point(10, 48);
      this.lblUserID.Name = "lblUserID";
      this.lblUserID.Size = new System.Drawing.Size(43, 13);
      this.lblUserID.TabIndex = 8;
      this.lblUserID.Text = "User ID";
      // 
      // tbxSourceAddress
      // 
      this.tbxSourceAddress.Location = new System.Drawing.Point(79, 19);
      this.tbxSourceAddress.Name = "tbxSourceAddress";
      this.tbxSourceAddress.Size = new System.Drawing.Size(198, 20);
      this.tbxSourceAddress.TabIndex = 5;
      this.tbxSourceAddress.Text = "olaf.maediger@web.de";
      this.tbxSourceAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(10, 22);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(45, 13);
      this.label12.TabIndex = 6;
      this.label12.Text = "Address";
      // 
      // tbxSmtpServer
      // 
      this.tbxSmtpServer.Location = new System.Drawing.Point(379, 19);
      this.tbxSmtpServer.Name = "tbxSmtpServer";
      this.tbxSmtpServer.Size = new System.Drawing.Size(199, 20);
      this.tbxSmtpServer.TabIndex = 0;
      this.tbxSmtpServer.Text = "smtp.web.de";
      this.tbxSmtpServer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(309, 22);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(65, 13);
      this.label13.TabIndex = 4;
      this.label13.Text = "Smtp Server";
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.tbxUserName);
      this.groupBox3.Controls.Add(this.label7);
      this.groupBox3.Controls.Add(this.tbxCompanyName);
      this.groupBox3.Controls.Add(this.label8);
      this.groupBox3.Controls.Add(this.tbxVersionValue);
      this.groupBox3.Controls.Add(this.label6);
      this.groupBox3.Controls.Add(this.tbxApplicationName);
      this.groupBox3.Controls.Add(this.label5);
      this.groupBox3.Location = new System.Drawing.Point(12, 38);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(294, 135);
      this.groupBox3.TabIndex = 7;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = " Header ";
      // 
      // tbxUserName
      // 
      this.tbxUserName.Location = new System.Drawing.Point(79, 104);
      this.tbxUserName.Name = "tbxUserName";
      this.tbxUserName.Size = new System.Drawing.Size(199, 20);
      this.tbxUserName.TabIndex = 9;
      this.tbxUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(21, 107);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(29, 13);
      this.label7.TabIndex = 10;
      this.label7.Text = "User";
      // 
      // tbxCompanyName
      // 
      this.tbxCompanyName.Location = new System.Drawing.Point(79, 76);
      this.tbxCompanyName.Name = "tbxCompanyName";
      this.tbxCompanyName.Size = new System.Drawing.Size(199, 20);
      this.tbxCompanyName.TabIndex = 7;
      this.tbxCompanyName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(11, 79);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(51, 13);
      this.label8.TabIndex = 8;
      this.label8.Text = "Company";
      // 
      // tbxVersionValue
      // 
      this.tbxVersionValue.Location = new System.Drawing.Point(79, 47);
      this.tbxVersionValue.Name = "tbxVersionValue";
      this.tbxVersionValue.ReadOnly = true;
      this.tbxVersionValue.Size = new System.Drawing.Size(199, 20);
      this.tbxVersionValue.TabIndex = 5;
      this.tbxVersionValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(16, 50);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(42, 13);
      this.label6.TabIndex = 6;
      this.label6.Text = "Version";
      // 
      // tbxApplicationName
      // 
      this.tbxApplicationName.Location = new System.Drawing.Point(79, 19);
      this.tbxApplicationName.Name = "tbxApplicationName";
      this.tbxApplicationName.ReadOnly = true;
      this.tbxApplicationName.Size = new System.Drawing.Size(199, 20);
      this.tbxApplicationName.TabIndex = 0;
      this.tbxApplicationName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(10, 22);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(59, 13);
      this.label5.TabIndex = 4;
      this.label5.Text = "Application";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.nudColumnTabulator);
      this.groupBox2.Controls.Add(this.label3);
      this.groupBox2.Controls.Add(this.nudShrinkLevel);
      this.groupBox2.Controls.Add(this.label2);
      this.groupBox2.Location = new System.Drawing.Point(312, 10);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(294, 51);
      this.groupBox2.TabIndex = 6;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = " Column ";
      // 
      // nudColumnTabulator
      // 
      this.nudColumnTabulator.Location = new System.Drawing.Point(221, 22);
      this.nudColumnTabulator.Name = "nudColumnTabulator";
      this.nudColumnTabulator.Size = new System.Drawing.Size(57, 20);
      this.nudColumnTabulator.TabIndex = 7;
      this.nudColumnTabulator.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(164, 25);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(52, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Tabulator";
      // 
      // nudShrinkLevel
      // 
      this.nudShrinkLevel.Location = new System.Drawing.Point(84, 21);
      this.nudShrinkLevel.Name = "nudShrinkLevel";
      this.nudShrinkLevel.Size = new System.Drawing.Size(57, 20);
      this.nudShrinkLevel.TabIndex = 5;
      this.nudShrinkLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(11, 24);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(66, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Shrink Level";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.nudDirectoryFileLimit);
      this.groupBox1.Controls.Add(this.label9);
      this.groupBox1.Controls.Add(this.cbxClearAfterSave);
      this.groupBox1.Controls.Add(this.cbxInsertLinenumbers);
      this.groupBox1.Controls.Add(this.cbxSaveOnApplicationEnd);
      this.groupBox1.Controls.Add(this.nudSaveOnLineCount);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Location = new System.Drawing.Point(312, 67);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(294, 106);
      this.groupBox1.TabIndex = 5;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = " Save ";
      // 
      // nudDirectoryFileLimit
      // 
      this.nudDirectoryFileLimit.Location = new System.Drawing.Point(107, 78);
      this.nudDirectoryFileLimit.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudDirectoryFileLimit.Name = "nudDirectoryFileLimit";
      this.nudDirectoryFileLimit.Size = new System.Drawing.Size(55, 20);
      this.nudDirectoryFileLimit.TabIndex = 9;
      this.nudDirectoryFileLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDirectoryFileLimit.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(9, 81);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(92, 13);
      this.label9.TabIndex = 8;
      this.label9.Text = "Directory File Limit";
      // 
      // cbxClearAfterSave
      // 
      this.cbxClearAfterSave.AutoSize = true;
      this.cbxClearAfterSave.Checked = true;
      this.cbxClearAfterSave.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbxClearAfterSave.Enabled = false;
      this.cbxClearAfterSave.Location = new System.Drawing.Point(178, 51);
      this.cbxClearAfterSave.Name = "cbxClearAfterSave";
      this.cbxClearAfterSave.Size = new System.Drawing.Size(102, 17);
      this.cbxClearAfterSave.TabIndex = 7;
      this.cbxClearAfterSave.Text = "Clear after Save";
      this.cbxClearAfterSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxClearAfterSave.UseVisualStyleBackColor = true;
      // 
      // cbxInsertLinenumbers
      // 
      this.cbxInsertLinenumbers.AutoSize = true;
      this.cbxInsertLinenumbers.Location = new System.Drawing.Point(14, 51);
      this.cbxInsertLinenumbers.Name = "cbxInsertLinenumbers";
      this.cbxInsertLinenumbers.Size = new System.Drawing.Size(115, 17);
      this.cbxInsertLinenumbers.TabIndex = 7;
      this.cbxInsertLinenumbers.Text = "Insert Linenumbers";
      this.cbxInsertLinenumbers.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxInsertLinenumbers.UseVisualStyleBackColor = true;
      // 
      // cbxSaveOnApplicationEnd
      // 
      this.cbxSaveOnApplicationEnd.AutoSize = true;
      this.cbxSaveOnApplicationEnd.Location = new System.Drawing.Point(178, 24);
      this.cbxSaveOnApplicationEnd.Name = "cbxSaveOnApplicationEnd";
      this.cbxSaveOnApplicationEnd.Size = new System.Drawing.Size(106, 17);
      this.cbxSaveOnApplicationEnd.TabIndex = 7;
      this.cbxSaveOnApplicationEnd.Text = "Application End  ";
      this.cbxSaveOnApplicationEnd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxSaveOnApplicationEnd.UseVisualStyleBackColor = true;
      // 
      // nudSaveOnLineCount
      // 
      this.nudSaveOnLineCount.Location = new System.Drawing.Point(73, 22);
      this.nudSaveOnLineCount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudSaveOnLineCount.Name = "nudSaveOnLineCount";
      this.nudSaveOnLineCount.Size = new System.Drawing.Size(77, 20);
      this.nudSaveOnLineCount.TabIndex = 6;
      this.nudSaveOnLineCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSaveOnLineCount.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(9, 24);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(58, 13);
      this.label4.TabIndex = 5;
      this.label4.Text = "Line Count";
      // 
      // cbxEnableProtocol
      // 
      this.cbxEnableProtocol.AutoSize = true;
      this.cbxEnableProtocol.Location = new System.Drawing.Point(16, 12);
      this.cbxEnableProtocol.Name = "cbxEnableProtocol";
      this.cbxEnableProtocol.Size = new System.Drawing.Size(104, 17);
      this.cbxEnableProtocol.TabIndex = 2;
      this.cbxEnableProtocol.Text = "Enable Protocol ";
      this.cbxEnableProtocol.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxEnableProtocol.UseVisualStyleBackColor = true;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(128, 14);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(68, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Header Style";
      // 
      // cbxHeaderStyle
      // 
      this.cbxHeaderStyle.FormattingEnabled = true;
      this.cbxHeaderStyle.Location = new System.Drawing.Point(198, 11);
      this.cbxHeaderStyle.Name = "cbxHeaderStyle";
      this.cbxHeaderStyle.Size = new System.Drawing.Size(108, 21);
      this.cbxHeaderStyle.TabIndex = 0;
      // 
      // CDialogEditParameter
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(620, 519);
      this.ControlBox = false;
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.pnlDialog);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.Name = "CDialogEditParameter";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Protocol - Edit Parameter";
      this.pnlDialog.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.groupBox5.ResumeLayout(false);
      this.groupBox5.PerformLayout();
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumnTabulator)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudShrinkLevel)).EndInit();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDirectoryFileLimit)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudSaveOnLineCount)).EndInit();
      this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlDialog;
		private System.Windows.Forms.Button btnConfirm;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbxHeaderStyle;
		private System.Windows.Forms.CheckBox cbxEnableProtocol;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox cbxSaveOnApplicationEnd;
		private System.Windows.Forms.NumericUpDown nudSaveOnLineCount;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.NumericUpDown nudColumnTabulator;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown nudShrinkLevel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox cbxClearAfterSave;
		private System.Windows.Forms.CheckBox cbxInsertLinenumbers;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TextBox tbxVersionValue;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox tbxApplicationName;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox tbxUserName;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox tbxCompanyName;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown nudDirectoryFileLimit;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.TextBox tbxPassword;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox tbxUserID;
		private System.Windows.Forms.Label lblUserID;
		private System.Windows.Forms.TextBox tbxSourceAddress;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox tbxSmtpServer;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.CheckBox cbxEnableSsl;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.RichTextBox rtbBody;
		private System.Windows.Forms.TextBox tbxAttachment;
		private System.Windows.Forms.TextBox tbxSubject;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox tbxTargetAddress;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.CheckBox cbxSendOnError;
	}
}