﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCNotifier
{
	public partial class CDialogEditParameter : Form
	{
		//
		//
		//-------------------------------------
		//	Section - Member
		//-------------------------------------
		//
		//
		//
		//-------------------------------------
		//	Section - Constructor
		//-------------------------------------
		//
		public CDialogEditParameter()
		{
			InitializeComponent();
			InitControls();
		}
		//
		//
		//-------------------------------------
		//	Section - Property
		//-------------------------------------
		//
		private EHeaderStyle GetHeaderStyle()
		{
			for (EHeaderStyle VI = EHeaderStyle.None; VI <= EHeaderStyle.DateTimeMillis; VI++)
			{
				if (VI.ToString() == cbxHeaderStyle.Text)
				{
					return VI;
				}
			}
			cbxHeaderStyle.SelectedIndex = (Int32)EHeaderStyle.None;
			return EHeaderStyle.None;
		}
		private void SetHeaderStyle(EHeaderStyle value)
		{
			if ((EHeaderStyle.None <= value) && (value <= EHeaderStyle.DateTimeMillis))
			{
				cbxHeaderStyle.SelectedIndex = (Int32)value;
			} else
			{
				cbxHeaderStyle.SelectedIndex = (Int32)EHeaderStyle.None;
			}
		}

		private Boolean GetEnableProtocol()
		{
			return cbxEnableProtocol.Checked;
		}
		private void SetEnableProtocol(Boolean value)
		{
			cbxEnableProtocol.Checked = value;
		}

		private Int32 GetShrinkLevel()
		{
			return (Int32)nudShrinkLevel.Value;
		}
		private void SetShrinkLevel(Int32 value)
		{
			nudShrinkLevel.Value = value;
		}

		private Int32 GetColumnTabulator()
		{
			return (Int32)nudColumnTabulator.Value;
		}
		private void SetColumnTabulator(Int32 value)
		{
			nudColumnTabulator.Value = value;
		}

		// RO ProtocolPath
		// RO ProtocolFile;

		private String GetApplicationName()
		{
			return tbxApplicationName.Text;
		}
		private void SetApplicationName(String value)
		{
			tbxApplicationName.Text = value;
		}

		private String GetVersionValue()
		{
			return tbxVersionValue.Text;
		}
		private void SetVersionValue(String value)
		{
			tbxVersionValue.Text = value;
		}

		private String GetCompanyName()
		{
			return tbxCompanyName.Text;
		}
		private void SetCompanyName(String value)
		{
			tbxCompanyName.Text = value;
		}

		private String GetUserName()
		{
			return tbxUserName.Text;
		}
		private void SetUserName(String value)
		{
			tbxUserName.Text = value;
		}

		private Boolean GetSaveOnApplicationEnd()
		{
			return cbxSaveOnApplicationEnd.Checked;
		}
		private void SetSaveOnApplicationEnd(Boolean value)
		{
			cbxSaveOnApplicationEnd.Checked = value;
		}

		private Int32 GetSaveOnLineCount()
		{
			return (Int32)nudSaveOnLineCount.Value;
		}
		private void SetSaveOnLineCount(Int32 value)
		{
			nudSaveOnLineCount.Value = value;
		}

		private Boolean GetInsertLinenumbers()
		{
			return cbxInsertLinenumbers.Checked;
		}
		private void SetInsertLinenumbers(Boolean value)
		{
			cbxInsertLinenumbers.Checked = value;
		}

		private Boolean GetClearAfterSave()
		{
			return cbxClearAfterSave.Checked;
		}
		private void SetClearAfterSave(Boolean value)
		{
			cbxClearAfterSave.Checked = value;
		}

		private Int32 GetDirectoryFileLimit()
		{
			return (Int32)nudDirectoryFileLimit.Value;
		}
		private void SetDirectoryFileLimit(Int32 value)
		{
			nudDirectoryFileLimit.Value = value;
		}

		private String GetSourceAddress()
		{
			return tbxSourceAddress.Text;
		}
		private void SetSourceAddress(String value)
		{
			tbxSourceAddress.Text = value;
		}

		private String GetSmtpServer()
		{
			return tbxSmtpServer.Text;
		}
		private void SetSmtpServer(String value)
		{
			tbxSmtpServer.Text = value;
		}

		private String GetUserID()
		{
			return tbxUserID.Text;
		}
		private void SetUserID(String value)
		{
			tbxUserID.Text = value;
		}

		private String GetPassword()
		{
			return tbxPassword.Text;
		}
		private void SetPassword(String value)
		{
			tbxPassword.Text = value;
		}

		private Boolean GetEnableSsl()
		{
			return cbxEnableSsl.Checked;
		}
		private void SetEnableSsl(Boolean value)
		{
			cbxEnableSsl.Checked = value;
		}

		private Boolean GetSendOnError()
		{
			return cbxSendOnError.Checked;
		}
		private void SetSendOnError(Boolean value)
		{
			cbxSendOnError.Checked = value;
		}

		private String GetTargetAddress()
		{
			return tbxTargetAddress.Text;
		}
		private void SetTargetAddress(String value)
		{
			tbxTargetAddress.Text = value;
		}

		private String GetSubject()
		{
			return tbxSubject.Text;
		}
		private void SetSubject(String value)
		{
			tbxSubject.Text = value;
		}

		private String GetAttachment()
		{
			return tbxAttachment.Text;
		}
		private void SetAttachment(String value)
		{
			tbxAttachment.Text = value;
		}

		private String GetBody()
		{
			return rtbBody.Text;
		}
		private void SetBody(String value)
		{
			rtbBody.Text = value;
		}
		//
		//
		//-------------------------------------
		//	Section - Helper
		//-------------------------------------
		//
		private void InitControls()
		{
			cbxHeaderStyle.Items.Clear();
			for (EHeaderStyle VI = EHeaderStyle.None; VI <= EHeaderStyle.DateTimeMillis; VI++)
			{
				cbxHeaderStyle.Items.Add(VI.ToString());
			}
		}
		//
		//
		//-------------------------------------
		//	Section - Get/SetData
		//-------------------------------------
		//
		public Boolean GetData(ref RNotifierData data)
		{
			data.HeaderStyle = GetHeaderStyle();
			data.EnableProtocol = GetEnableProtocol();
			data.ShrinkLevel = GetShrinkLevel();
			data.ColumnTabulator = GetColumnTabulator();
			data.ApplicationName = GetApplicationName();
			data.VersionValue = GetVersionValue();
			data.CompanyName = GetCompanyName();
			data.UserName = GetUserName();
			data.SaveOnApplicationEnd = GetSaveOnApplicationEnd();
			data.SaveOnLineCount = GetSaveOnLineCount();
			data.InsertLinenumbers = GetInsertLinenumbers();
			data.ClearAfterSave = GetClearAfterSave();
			data.DirectoryFileLimit = GetDirectoryFileLimit();
			data.SourceAddress = GetSourceAddress();
			data.SmtpServer = GetSmtpServer();
			data.UserID = GetUserID();
			data.Password = GetPassword();
			data.EnableSsl = GetEnableSsl();
			data.SendOnError = GetSendOnError();
			data.TargetAddress = GetTargetAddress();
			data.Subject = GetSubject();
			data.Attachment = GetAttachment();
			data.Body = GetBody();
			return true;
		}

		public Boolean SetData(RNotifierData data)
		{
			SetHeaderStyle(data.HeaderStyle);
			SetEnableProtocol(data.EnableProtocol);
			SetShrinkLevel(data.ShrinkLevel);
			SetApplicationName(data.ApplicationName);
			SetVersionValue(data.VersionValue);
			SetCompanyName(data.CompanyName);
			SetUserName(data.UserName);
			SetColumnTabulator(data.ColumnTabulator);
			// ProtocolPath
			// ProtocolFile
			SetSaveOnApplicationEnd(data.SaveOnApplicationEnd);
			SetSaveOnLineCount(data.SaveOnLineCount);
			SetInsertLinenumbers(data.InsertLinenumbers);
			SetClearAfterSave(data.ClearAfterSave);
			SetDirectoryFileLimit(data.DirectoryFileLimit);
			SetSourceAddress(data.SourceAddress);
			SetSmtpServer(data.SmtpServer);
			SetUserID(data.UserID);
			SetPassword(data.Password);
			SetEnableSsl(data.EnableSsl);
			SetSendOnError(data.SendOnError);
			SetTargetAddress(data.TargetAddress);
			SetSubject(data.Subject);
			SetAttachment(data.Attachment);
			SetBody(data.Body);
			return true;
		}
		//
		//
		//-------------------------------------
		//	Section - Event
		//-------------------------------------
		//

	}
}
