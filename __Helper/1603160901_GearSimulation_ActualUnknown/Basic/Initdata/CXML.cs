using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Drawing;

namespace Initdata
{
  class CXML
  {
    private XmlDocument FXMLDocument = null;
    private String FFileEntry = "demo.xml";
    private XmlNode FNodeBase = null;
		private XmlNode FSectionSelected = null;

		private XmlNode FSectionFixed = null;

    public CXML()
    {
      FXMLDocument =  new XmlDocument();
    }

    public Boolean OpenWrite(String fileentry, String nodebase)
    {
      FFileEntry = fileentry;
      FNodeBase = FXMLDocument.CreateElement(nodebase);
      FXMLDocument.AppendChild(FNodeBase);
      //FSectionSelected = FNodeBase;
      return true;
    }

    public Boolean OpenRead(String fileentry, out String nodebase)
    {
      FFileEntry = fileentry;
      if (File.Exists(FFileEntry))
      {
        FXMLDocument.Load(FFileEntry);
        FNodeBase = FXMLDocument.FirstChild;
        nodebase = FNodeBase.Name;
        return true;
      }
      nodebase = CInitdata.NodeBase.ToString();
      //FSectionSelected = FNodeBase;
      return false;
    }

    public Boolean CloseWrite()
    {
      FXMLDocument.Save(FFileEntry);
      return true;
    }
    public Boolean CloseRead()
    {
      return true;
    }
    //
    //
    //----------------------------------------
    //
    //  Hilfsmethoden
    //
    //----------------------------------------
    //
    private Boolean SelectHeaderWrite(String header,                                      
                                      ref XmlNode node)
    {
      if (FSectionSelected is XmlNode)
      {
        node = FXMLDocument.SelectSingleNode(header);
        if (!(node is XmlNode))
        {
          node = FXMLDocument.CreateElement(header);
          FSectionSelected.AppendChild(node);
        }
        return (node is XmlNode);
      }
      return false;
    }
    private Boolean SelectHeaderRead(String header,
                                     ref XmlNode node)
    {
      if (FSectionSelected is XmlNode)
      {
        node = FSectionSelected.SelectSingleNode(header);
        return (node is XmlNode);
      }
      return false;
    }
    //
    //
    //----------------------------------------
    //
    //  Access-Methoden
    //
    //----------------------------------------
    //
    public XmlNode FindNodeBase()
    {
      return FXMLDocument.SelectSingleNode(CInitdata.NodeBase);
    }

    public XmlNode FindNode(XmlNode nodebase,
                            String nodename)
    {
			if (nodebase is XmlNode)
			{
				if (nodename == nodebase.Name)
				{
					return nodebase;
				}
				foreach (XmlNode NodeChild in nodebase)
				{
					if (nodename == NodeChild.Name)
					{
						return NodeChild;
					}
					if (NodeChild.HasChildNodes)
					{
						XmlNode NodeSub = FindNode(NodeChild, nodename);
						if (NodeSub is XmlNode)
						{
							return NodeSub;
						}
					}
				}
			}
      return null;
    }

		/* NC// Select without attribute -> NO Creation
		public Boolean SelectSection(String section)
		{
			XmlNode NodeBase = FindNodeBase();
			XmlNode NodeSection = FindNode(NodeBase, section);
			if (!(NodeSection is XmlNode))
			{ // Node doesnt exist -> do not create!
				return false;
			}
			FSectionSelected = NodeSection;
			return true;
		}*/

		// Select without attribute -> NO Creation
		public Boolean SelectSection(String parent, String section)
		{
			XmlNode NodeBase = FindNodeBase();
			XmlNode NodeParent = FindNode(NodeBase, parent);
			XmlNode NodeSection = FindNode(NodeParent, section);
			if (!(NodeSection is XmlNode))
			{ // Node doesnt exist -> do not create!
				return false;
			}
			FSectionSelected = NodeSection;
			return true;
		}

		// Select without attribute -> Creation if necessary
    public Boolean SelectCreateSection(String parent, String section)
    {
      Boolean Result = true;
      XmlNode NodeBase = FindNodeBase();
      XmlNode NodeParent = FindNode(NodeBase, parent);
      XmlNode NodeSection = FindNode(NodeParent, section);
      if (!(NodeSection is XmlNode))
      { // Node doesnt exist -> create
        NodeSection = FXMLDocument.CreateElement(section);
        NodeParent.AppendChild(NodeSection);
        Result = false;
      }
      FSectionSelected = NodeSection;
      return Result;
    }

		// Select with attribute -> NO creation
		public Boolean FixSection(String parent, String section, String attribute, Int32 index)
		{
			XmlNode NodeBase = FindNodeBase();
			XmlNode NodeParent = FindNode(NodeBase, parent);
			String SectionAttribute = section + "[@" + attribute + "=" + index.ToString() + "]";
			XmlNode NodeSection = NodeParent.SelectSingleNode(SectionAttribute);
			if (!(NodeSection is XmlNode))
			{ // Node doesnt exist -> do not create!
				return false;
			}
			FSectionFixed = NodeSection;
			return true;
		}

		// Select with attribute -> NO creation
		public Boolean SelectSection(String parent, String section, String attribute, Int32 index)
		{
			XmlNode NodeBase = FindNodeBase();
			XmlNode NodeParent = FindNode(NodeBase, parent);
			String SectionAttribute = section + "[@" + attribute + "=" + index.ToString() + "]";
			XmlNode NodeSection = NodeParent.SelectSingleNode(SectionAttribute);
			if (!(NodeSection is XmlNode))
			{ // Node doesnt exist -> do not create!
				return false;
			}
			FSectionSelected = NodeSection;
			return true;
		}

		// Select with attribute -> NO creation
		public Boolean SelectFixedSection(String section)
		{
			XmlNode NodeSection = FindNode(FSectionFixed, section);
			if (!(NodeSection is XmlNode))
			{ // Node doesnt exist -> do not create!
				return false;
			}
			FSectionSelected = NodeSection;
			return true;
		}

		// Select with attribute -> Creation if necessary
		public Boolean SelectCreateSection(String parent, String section, String attribute, String value)
		{
			Boolean Result = true;
			XmlNode NodeBase = FindNodeBase();
			XmlNode NodeParent = FindNode(NodeBase, parent);
			String SectionAttribute = section + "[@" + attribute + "=" + value + "]";
			XmlNode NodeSection = NodeParent.SelectSingleNode(SectionAttribute);
			if (!(NodeSection is XmlNode))
			{ // Node doesnt exist -> create
				NodeSection = FXMLDocument.CreateElement(section);
				XmlAttribute NodeAttribute = FXMLDocument.CreateAttribute(attribute);
				NodeAttribute.Value = value;
				NodeSection.Attributes.Append(NodeAttribute);
				NodeParent.AppendChild(NodeSection);
				Result = false;
			}
			FSectionSelected = NodeSection;
			return Result;
		}
		//
    //----------------------------------------
    //  Access-Methoden - Integer
    //----------------------------------------
    //  
    public Boolean WriteInteger(String header,
                                Int32 value)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderWrite(header, ref HeaderNode))
      {
        HeaderNode.InnerText = value.ToString();
        return true;
      }
      return false;
    }
    public Boolean ReadInteger(String header,
                               out Int32 value,
                               Int32 preset)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderRead(header, ref HeaderNode))
      {
        value = Int32.Parse(HeaderNode.InnerText);
        return true;
      }
      value = preset;
      return false;
    }

		public Boolean ReadInt32(String parent,
														 String header,
														 out Int32 value,
														 Int32 preset)
		{
			if (SelectSection(parent, header))
			{
				if (Int32.TryParse(FSectionSelected.InnerText, out value))
				{
					return true;
				}
			}
			value = preset;
			return false;
		}

		//
		//----------------------------------------
		//  Access-Methoden - Boolean
		//----------------------------------------
		//  
		public Boolean WriteBoolean(String header,
																Boolean value)
		{
			XmlNode HeaderNode = null;
			if (SelectHeaderWrite(header, ref HeaderNode))
			{
				HeaderNode.InnerText = value.ToString();
				return true;
			}
			return false;
		}
		public Boolean ReadBoolean(String header,
															 out Boolean value,
															 Boolean preset)
		{
			XmlNode HeaderNode = null;
			if (SelectHeaderRead(header, ref HeaderNode))
			{
				value = Boolean.Parse(HeaderNode.InnerText);
				return true;
			}
			value = preset;
			return false;
		}

		public Boolean WriteBoolean(String header,
																Int32 value)
		{
			XmlNode HeaderNode = null;
			if (SelectHeaderWrite(header, ref HeaderNode))
			{
				HeaderNode.InnerText = (0 != value).ToString();
				return true;
			}
			return false;
		}
		public Boolean ReadBoolean(String header,
															 out Int32 value,
															 Int32 preset)
		{
			XmlNode HeaderNode = null;
			if (SelectHeaderRead(header, ref HeaderNode))
			{
				if (Boolean.Parse(HeaderNode.InnerText))
				{
					value = 1;
				} else
				{
					value = 0;
				}
				return true;
			}
			value = preset;
			return false;
		}
		//
    //----------------------------------------
    //  Access-Methoden - String
    //----------------------------------------
    //  
    public Boolean WriteString(String header,
                               String value)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderWrite(header, ref HeaderNode))
      {
        HeaderNode.InnerText = value;
        return true;
      }
      return false;
    }
    public Boolean ReadString(String header,
                              out String value,
                              String preset)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderRead(header, ref HeaderNode))
      {
        value = HeaderNode.InnerText;
        return true;
      }
      value = preset;
      return false;
    }
    //
    //----------------------------------------
    //  Access-Methoden - Double
    //----------------------------------------
    //  
    public Boolean WriteDouble(String header,
                               Double value)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderWrite(header, ref HeaderNode))
      {
        HeaderNode.InnerText = value.ToString();
        return true;
      }
      return false;
    }
    public Boolean ReadDouble(String header,
                              out Double value,
                              Double preset)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderRead(header, ref HeaderNode))
      {
				if (Double.TryParse(HeaderNode.InnerText, out value))
				{
		      return true;
				}
      }
      value = preset;
      return false;
    }
		//
		//----------------------------------------
		//  Access-Methoden - Enumeration
		//----------------------------------------
		//  
		public Boolean WriteEnumeration(String header,
																		String value)
		{
			XmlNode HeaderNode = null;
			if (SelectHeaderWrite(header, ref HeaderNode))
			{
				HeaderNode.InnerText = value;
				return true;
			}
			return false;
		}
		public Boolean ReadEnumeration(String header,
																	 out String value,
																	 String preset)
		{
			XmlNode HeaderNode = null;
			if (SelectHeaderRead(header, ref HeaderNode))
			{
				value = HeaderNode.InnerText;
				return true;
			}
			value = preset;
			return false;
		}
		//
		//----------------------------------------
		//  Access-Methoden - Guid
		//----------------------------------------
		//  
		public Boolean WriteGuid(String header,
														 Guid value)
		{
			XmlNode HeaderNode = null;
			if (SelectHeaderWrite(header, ref HeaderNode))
			{
				HeaderNode.InnerText = value.ToString();
				return true;
			}
			return false;
		}
		public Boolean ReadGuid(String header,
														out Guid value,
														Guid preset)
		{
			XmlNode HeaderNode = null;
			if (SelectHeaderRead(header, ref HeaderNode))
			{
				value = new Guid(HeaderNode.InnerText);
				return true;
			}
			value = preset;
			return false;
		}
		//
		//----------------------------------------
		//  Access-Methoden - DateTime
		//----------------------------------------
		//  
		public Boolean WriteDateTime(String header,
															   DateTime value)
		{
			XmlNode HeaderNode = null;
			if (SelectHeaderWrite(header, ref HeaderNode))
			{
				HeaderNode.InnerText = value.ToString();
				return true;
			}
			return false;
		}
		public Boolean ReadDateTime(String header,
																out DateTime value,
																DateTime preset)
		{
			XmlNode HeaderNode = null;
			if (SelectHeaderRead(header, ref HeaderNode))
			{
				value = DateTime.Parse(HeaderNode.InnerText);
				return true;
			}
			value = preset;
			return false;
		}
		//
		//----------------------------------------
		//  Access-Methoden - Hexadecimal
		//----------------------------------------
		//  
    public Boolean WriteByte(String header,
                             Byte value)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderWrite(header, ref HeaderNode))
      {
        HeaderNode.InnerText = String.Format("{0:X2}", value);
        return true;
      }
      return false;
    }
    public Boolean WriteHexadecimal(String header,
                                    Byte value)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderWrite(header, ref HeaderNode))
      {
        HeaderNode.InnerText = String.Format("{0:X2}", value);
        return true;
      }
      return false;
    }
    public Boolean WriteHexadecimal(String header,
                                    UInt16 value)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderWrite(header, ref HeaderNode))
      {
        HeaderNode.InnerText = String.Format("{0:X4}", value);
        return true;
      }
      return false;
    }
    public Boolean WriteHexadecimal(String header,
                                    UInt32 value)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderWrite(header, ref HeaderNode))
      {
        HeaderNode.InnerText = String.Format("{0:X8}", value);
        return true;
      }
      return false;
    }


    public Boolean ReadByte(String header,
                            out Byte value,
                            Byte preset)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderRead(header, ref HeaderNode))
      {
        value = (Byte)CInitdata.TextHexadecimal(HeaderNode.InnerText);
        return true;
      }
      value = preset;
      return false;
    }
    
    public Boolean ReadHexadecimal(String header,
                                   out Byte value,
                                   Byte preset)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderRead(header, ref HeaderNode))
      {
        value = (Byte)CInitdata.TextHexadecimal(HeaderNode.InnerText);
        return true;
      }
      value = preset;
      return false;
    }
    public Boolean ReadHexadecimal(String header,
                                   out UInt16 value,
                                   UInt16 preset)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderRead(header, ref HeaderNode))
      {
        value = (UInt16)CInitdata.TextHexadecimal(HeaderNode.InnerText);
        return true;
      }
      value = preset;
      return false;
    }
    public Boolean ReadHexadecimal(String header,
                                   out UInt32 value,
                                   UInt32 preset)
    {
      XmlNode HeaderNode = null;
      if (SelectHeaderRead(header, ref HeaderNode))
      {
        value = CInitdata.TextHexadecimal(HeaderNode.InnerText);
        return true;
      }
      value = preset;
      return false;
    }
		//
		//---------------------------------------
		//  Exportierte Methoden - Color
		//---------------------------------------
		//      
		public Boolean WriteColor(String header,
														  Color value)
		{
			XmlNode FSectionSecure = FSectionSelected;
			XmlNode HeaderNode = null;
			if (SelectHeaderWrite(header, ref HeaderNode))
			{
				FSectionSelected = HeaderNode;
				if (WriteInteger("Alpha", value.A))
				{
					if (WriteInteger("Red", value.R))
					{
						if (WriteInteger("Green", value.G))
						{
							if (WriteInteger("Blue", value.B))
							{
								FSectionSelected = FSectionSecure;
								return true;
							}
						}
					}
				}
			}
			FSectionSelected = FSectionSecure;
			return false;
		}

		public Boolean ReadColor(String header,
														 out Color value,
														 Color preset)
		{
			XmlNode SectionSecure = FSectionSelected;
			XmlNode HeaderNode = null;
			if (SelectHeaderRead(header, ref HeaderNode))
			{
				FSectionSelected = HeaderNode;
				Int32 Alpha = 255;
				ReadInteger("Alpha", out Alpha, Alpha);
				Int32 Red = 255;
				ReadInteger("Red", out Red, Red);
				Int32 Green = 255;
				ReadInteger("Green", out Green, Green);
				Int32 Blue = 255;
				ReadInteger("Blue", out Blue, Blue);
				value = Color.FromArgb(Alpha, Red, Green, Blue);
				FSectionSelected = SectionSecure;
				return true;
			}
			value = preset;
			FSectionSelected = SectionSecure;
			return false;
		}

		//
		//---------------------------------------
		//  Exportierte Methoden - Pen
		//---------------------------------------
		//      
		public Boolean WritePen(String header,
														Pen value)
		{
			XmlNode SectionSecure = FSectionSelected;
			XmlNode HeaderNode = null;
			if (SelectHeaderWrite(header, ref HeaderNode))
			{
				FSectionSelected = HeaderNode;
				if (WriteColor("Color", value.Color))
				{
					if (WriteDouble("Width", value.Width))
					{
						FSectionSelected = SectionSecure;
						return true;
					}
				}
			}
			FSectionSelected = SectionSecure;
			return false;
		}

		public Boolean ReadPen(String header,
													 out Pen value,
													 Pen preset)
		{
			XmlNode SectionSecure = FSectionSelected;
			XmlNode HeaderNode = null;
			if (SelectHeaderRead(header, ref HeaderNode))
			{
				FSectionSelected = HeaderNode;
				Color C = preset.Color;
				if (ReadColor("Color", out C, C))
				{
					Double W = preset.Width;
					if (ReadDouble("Width", out W, W))
					{
						value = new Pen(C, (float)W);
						FSectionSelected = SectionSecure;
						return true;
					}
				}
			}
			value = preset;
			FSectionSelected = SectionSecure;
			return false;
		}
		//
		//---------------------------------------
		//  Exportierte Methoden - Brush
		//---------------------------------------
		//      
		public Boolean WriteBrush(String header,
															SolidBrush value)
		{
			XmlNode FSectionSecure = FSectionSelected;
			XmlNode HeaderNode = null;
			if (SelectHeaderWrite(header, ref HeaderNode))
			{
				FSectionSelected = HeaderNode;
				if (WriteColor("Color", value.Color))
				{
					FSectionSelected = FSectionSecure;
					return true;
				}
			}
			FSectionSelected = FSectionSecure;
			return false;
		}

		public Boolean ReadBrush(String header,
														 out SolidBrush value,
														 SolidBrush preset)
		{
			XmlNode SectionSecure = FSectionSelected;
			XmlNode HeaderNode = null;
			if (SelectHeaderRead(header, ref HeaderNode))
			{
				FSectionSelected = HeaderNode;
				Color C = preset.Color;
				if (ReadColor("Color", out C, C))
				{
					value = new SolidBrush(C);
					FSectionSelected = SectionSecure;
					return true;
				}
			}
			value = preset;
			FSectionSelected = SectionSecure;
			return false;
		}

  }
}
