using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Drawing;
using System.IO;

namespace Initdata
{
  public class CInitdata
  {
    // 
    //-------------------------------
    //  Section - Constant
    //-------------------------------
    //
    public const String Initfile = "Initdata.xml";
    public const String NodeBase = "Configuration";
		//
    public const String NAME_MAINWINDOW = "MainWindow";
		public const String NAME_VISIBLE = "Visible";
		public const String NAME_ENABLE = "Enable";
		public const String NAME_TITLE = "Title";
		public const String NAME_LEFT = "Left";
		public const String NAME_TOP = "Top";
    public const String NAME_WIDTH = "Width";
		public const String NAME_HEIGHT = "Height";
    public const String NAME_ABOUTDIALOG = "AboutDialog";
		public const String NAME_SEPARATOR = "Separator";
		// 
    //-------------------------------
    //  Section - Member
    //-------------------------------
    //
    private CXML FXML = null;
		private DialogAbout FDialogAbout = null;
		// 
    //-------------------------------
    //  Section - Constructor
	  //-------------------------------
    //
    public CInitdata()
    {
      FXML = new CXML();
			FDialogAbout = new DialogAbout();
		}
    // 
    //-------------------------------
		//  Section - Exported Methods
    //-------------------------------
		//
		//---------------------------------------
		//  Exported Methods - Helper
		//---------------------------------------
		//
    public static void Init()
    {
      CDefines.Init();
    }



		public static Boolean BuildInitdataDirectory(String subdirectorytarget,
																								 String filenamepreset,
																								 String fileextensionpreset,
																								 out String initdatadirectory,
																								 out String initdatafilename)
		{
			initdatadirectory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
			initdatadirectory = Path.Combine(initdatadirectory, subdirectorytarget);
			initdatafilename = filenamepreset + fileextensionpreset;
			try
			{
				// Initdata
				// Test if "All User\...company\Project" is present
				if (!Directory.Exists(initdatadirectory))
				{ // not -> create
					Directory.CreateDirectory(initdatadirectory);
				}
				// Test All User\...company\Project\project.ini.xml
				String InitdataEntry = Path.Combine(initdatadirectory, initdatafilename);
				if (!File.Exists(InitdataEntry))
				{
					// Try to copy default project.ini.xml from local _Exe
					if (File.Exists(initdatafilename))
					{ // Copy default project.ini.xml -> All User\...company\Project\project.ini.xml
						File.Copy(initdatafilename, InitdataEntry);
						return true;
					}
					return false;
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}
		//
		//---------------------------------------
		//  Exported Methods - Open / Close
		//---------------------------------------
		//
		public Boolean OpenWrite(String fileentry, String nodebase)
    {
      return FXML.OpenWrite(fileentry, nodebase);
    }
    public Boolean CloseWrite()
    {
      return FXML.CloseWrite();
    }

    public Boolean OpenRead(String fileentry, out String nodebase)
    {
      return FXML.OpenRead(fileentry, out nodebase);
    }

    public Boolean CloseRead()
    {
      return FXML.CloseRead();
    }
    //
    //---------------------------------------
		//  Exported Methods - Section
    //---------------------------------------
    //
		public Boolean SelectSection(String parent, String section)
		{
			return FXML.SelectSection(parent, section);
		}

		public Boolean SelectCreateSection(String parent, String section)
		{
			return FXML.SelectCreateSection(parent, section);
		}

		public Boolean FixSection(String parent, String section, String attribute, Int32 index)
		{
			return FXML.FixSection(parent, section, attribute, index);
		}

		public Boolean SelectSection(String parent, String section, String attribute, Int32 index)
		{
			return FXML.SelectSection(parent, section, attribute, index);
		}

		public Boolean SelectCreateSection(String parent, String section, String attribute, String value)
		{
			return FXML.SelectCreateSection(parent, section, attribute, value);
		}
		//
    //---------------------------------------
    //  Exportierte Methoden - Integer
    //---------------------------------------
    //      
    public Boolean WriteInteger(String header, 
                                Int32 value)
    {
      return FXML.WriteInteger(header, value);
    }
    public Boolean ReadInteger(String header,
                               out Int32 value,
                               Int32 preset)
    {
      return FXML.ReadInteger(header, out value, preset);
    }
		//
		//---------------------------------------
		//  Exportierte Methoden - Boolean
		//---------------------------------------
		//      
		public Boolean WriteBoolean(String header,
																Boolean value)
		{
			return FXML.WriteBoolean(header, value);
		}
		public Boolean ReadBoolean(String header,
															 out Boolean value,
															 Boolean preset)
		{
			return FXML.ReadBoolean(header, out value, preset);
		}

		public Boolean WriteBoolean(String header,
																Int32 value)
		{
			return FXML.WriteBoolean(header, value);
		}
		public Boolean ReadBoolean(String header,
															 out Int32 value,
															 Int32 preset)
		{
			return FXML.ReadBoolean(header, out value, preset);
		}
		//
    //---------------------------------------
    //  Exportierte Methoden - String
    //---------------------------------------
    //      
    public Boolean WriteString(String header, 
                               String value)
    {
      return FXML.WriteString(header, value);
    }
    public Boolean ReadString(String header, 
                              out String value,
                              String preset)
    {
      return FXML.ReadString(header, out value, preset);
    }
    //
    //---------------------------------------
    //  Exportierte Methoden - Double
    //---------------------------------------
    //      
    public Boolean WriteDouble(String header,
                               Double value)
    {
      return FXML.WriteDouble(header, value);
    }
		public Boolean ReadDouble(String header,
															out Double value,
                              Double preset)
    {
      return FXML.ReadDouble(header, out value, preset);
    }
		//
		//---------------------------------------
		//  Exportierte Methoden - Enumeration
		//---------------------------------------
		//      
		public Boolean WriteEnumeration(String header,
																		String value)
		{
			return FXML.WriteEnumeration(header, value);
		}
		public Boolean ReadEnumeration(String header,
																	 out String value,
																	 String preset)
		{
			return FXML.ReadEnumeration(header, out value, preset);
		}
		//
		//---------------------------------------
		//  Exportierte Methoden - Guid
		//---------------------------------------
		//      
		public Boolean WriteGuid(String header,
														 Guid value)
		{
			return FXML.WriteGuid(header, value);
		}
		public Boolean ReadGuid(String header,
														out Guid value,
														Guid preset)
		{
			return FXML.ReadGuid(header, out value, preset);
		}
		//
    //---------------------------------------
    //  Exportierte Methoden - DateTime
    //---------------------------------------
    //      
		public Boolean WriteDateTime(String header,
																 DateTime value)
		{
			return FXML.WriteDateTime(header, value);
		}
		public Boolean ReadDateTime(String header,
																out DateTime value,
																DateTime preset)
		{
			return FXML.ReadDateTime(header, out value, preset);
		}
    //
    //---------------------------------------
    //  Exportierte Methoden - Hexadecimal
    //---------------------------------------
    //      
    public Boolean WriteByte(String header,
                             Byte value)
    {
      return FXML.WriteByte(header, value);
    }
    public Boolean WriteHexadecimal(String header,
                                    Byte value)
    {
      return FXML.WriteHexadecimal(header, value);
    }
    public Boolean WriteHexadecimal(String header,
                                    UInt16 value)
    {
      return FXML.WriteHexadecimal(header, value);
    }
    public Boolean WriteHexadecimal(String header,
                                    UInt32 value)
    {
      return FXML.WriteHexadecimal(header, value);
    }

    public Boolean ReadByte(String header,
                            out Byte value,
                            Byte preset)
    {
      return FXML.ReadByte(header, out value, preset);
    }
    public Boolean ReadHexadecimal(String header,
                                   out Byte value,
                                   Byte preset)
    {
      return FXML.ReadHexadecimal(header, out value, preset);
    }
    public Boolean ReadHexadecimal(String header,
                                   out UInt16 value,
                                   UInt16 preset)
    {
      return FXML.ReadHexadecimal(header, out value, preset);
    }
    public Boolean ReadHexadecimal(String header,
                                   out UInt32 value,
                                   UInt32 preset)
    {
      return FXML.ReadHexadecimal(header, out value, preset);
    }
		//
		//---------------------------------------
		//  Exportierte Methoden - Color
		//---------------------------------------
		//      
		public Boolean WriteColor(String header,
															Color value)
		{
			return FXML.WriteColor(header, value);
		}

		public Boolean ReadColor(String header,
														 out Color value,
														 Color preset)
		{
			return FXML.ReadColor(header, out value, preset);
		}
		//
		//---------------------------------------
		//  Exportierte Methoden - Pen
		//---------------------------------------
		//      
		public Boolean WritePen(String header,
														Pen value)
		{
			return FXML.WritePen(header, value);
		}

		public Boolean ReadPen(String header,
													 out Pen value,
													 Pen preset)
		{
			return FXML.ReadPen(header, out value, preset);
		}
		//
		//---------------------------------------
		//  Exportierte Methoden - Brush
		//---------------------------------------
		//      
		public Boolean WriteBrush(String header,
															SolidBrush value)
		{
			return FXML.WriteBrush(header, value);
		}

		public Boolean ReadBrush(String header,
														 out SolidBrush value,
														 SolidBrush preset)
		{
			return FXML.ReadBrush(header, out value, preset);
		}
		// 
    //
    //-------------------------------
    //
    //  Conversions
    //
    //-------------------------------
    //
		static public String HexadecimalText(UInt32 value,
																				 Byte size)
		{
			String F = "{0:X" + size.ToString() + "}";
			return String.Format(F, value);
		}

		static public UInt32 TextHexadecimal(String value)
		{
			return Convert.ToUInt32(value, 16);
		}
		// 
		//
		//-------------------------------
		//
		//  Dialog About
		//
		//-------------------------------
		//
		public void SetApplicationData(String application,
																					String version,
																					String date)
		{
			if (FDialogAbout is DialogAbout)
			{
				FDialogAbout.SetApplicationData(application,
																			  version,
																				date);
			}
		}

		public void SetDescriptionData(String description,
																					String point1,
																					String point2,
																					String point3)
		{
			if (FDialogAbout is DialogAbout)
			{
				FDialogAbout.SetDescriptionData(description,
																			  point1,
																				point2,
																				point3);
			}
		}

		public void SetCompanySourceData(String organisation,
																		 String street,
																		 String city)
		{
			if (FDialogAbout is DialogAbout)
			{
				FDialogAbout.SetCompanySourceData(organisation,
																				  street,
																				  city);
			}
		}

		public void SetCompanyTargetData(String organisationtop,
																		 String organisationbottom,	
																		 String street,
																		 String city)
		{
			if (FDialogAbout is DialogAbout)
			{
				FDialogAbout.SetCompanyTargetData(organisationtop,
																					organisationbottom,
																				  street,
																				  city);
			}
		}

		public void SetIcon(Icon icon)
		{
			if (FDialogAbout is DialogAbout)
			{
				FDialogAbout.SetIcon(icon);
			}
		}

		public DialogResult ShowModalDialogAbout()
		{
			return FDialogAbout.ShowDialog();
		}

	}
}
