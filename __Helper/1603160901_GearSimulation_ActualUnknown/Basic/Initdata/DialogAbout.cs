using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Initdata
{
	public partial class DialogAbout : Form
	{
		public DialogAbout()
		{
			InitializeComponent();
		}

		public void SetApplicationData(String newapplication,
																	 String newversion,
																	 String newdate)
		{
			lblApplication.Text = newapplication;
			lblVersion.Text = newversion;
			lblDate.Text = newdate;
		}

		public void SetDescriptionData(String newdescription,
																	 String newpoint1,
																	 String newpoint2,
																	 String newpoint3)
		{
			lblDescription.Text = newdescription;
			lblPoint1.Text = newpoint1;
			lblPoint2.Text = newpoint2;
			lblPoint3.Text = newpoint3;
		}

		public void SetCompanySourceData(String organisation,
																		 String street,
																		 String city)
		{
			lblCompanySource.Text = organisation;
			lblStreetSource.Text = street;
			lblCitySource.Text = city;
		}

		public void SetCompanyTargetData(String companytop,
																		 String companybottom,
																		 String street,
																		 String city)
		{
			lblCompanyTargetTop.Text = companytop;
			lblCompanyTargetBottom.Text = companybottom;
			lblStreetTarget.Text = street;
			lblCityTarget.Text = city;
		}

		public void SetIcon(Icon icon)
		{
			pbxIcon.Image = icon.ToBitmap();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}