namespace Initdata
{
	partial class DialogAbout
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnClose = new System.Windows.Forms.Button();
			this.lpnTitle = new System.Windows.Forms.TableLayoutPanel();
			this.lblDate = new System.Windows.Forms.Label();
			this.lblApplication = new System.Windows.Forms.Label();
			this.lblVersion = new System.Windows.Forms.Label();
			this.lpnDescription = new System.Windows.Forms.TableLayoutPanel();
			this.lblPoint2 = new System.Windows.Forms.Label();
			this.lblPoint3 = new System.Windows.Forms.Label();
			this.lblPoint1 = new System.Windows.Forms.Label();
			this.lblDescription = new System.Windows.Forms.Label();
			this.lpnSource = new System.Windows.Forms.TableLayoutPanel();
			this.lblStreetSource = new System.Windows.Forms.Label();
			this.lblCitySource = new System.Windows.Forms.Label();
			this.lblCompanySource = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.pbxIcon = new System.Windows.Forms.PictureBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.lblCompanyTargetTop = new System.Windows.Forms.Label();
			this.lblCompanyTargetBottom = new System.Windows.Forms.Label();
			this.lblStreetTarget = new System.Windows.Forms.Label();
			this.lblCityTarget = new System.Windows.Forms.Label();
			this.lpnTitle.SuspendLayout();
			this.lpnDescription.SuspendLayout();
			this.lpnSource.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pbxIcon)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(36, 473);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(344, 28);
			this.btnClose.TabIndex = 23;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// lpnTitle
			// 
			this.lpnTitle.ColumnCount = 1;
			this.lpnTitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.lpnTitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.lpnTitle.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.lpnTitle.Controls.Add(this.lblDate, 0, 2);
			this.lpnTitle.Controls.Add(this.lblApplication, 0, 0);
			this.lpnTitle.Controls.Add(this.lblVersion, 0, 1);
			this.lpnTitle.Location = new System.Drawing.Point(132, 29);
			this.lpnTitle.Name = "lpnTitle";
			this.lpnTitle.RowCount = 3;
			this.lpnTitle.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.lpnTitle.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.lpnTitle.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.lpnTitle.Size = new System.Drawing.Size(261, 76);
			this.lpnTitle.TabIndex = 24;
			// 
			// lblDate
			// 
			this.lblDate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDate.Location = new System.Drawing.Point(3, 57);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(255, 19);
			this.lblDate.TabIndex = 15;
			this.lblDate.Text = "<date>";
			this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblApplication
			// 
			this.lblApplication.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblApplication.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblApplication.Location = new System.Drawing.Point(3, 0);
			this.lblApplication.Name = "lblApplication";
			this.lblApplication.Size = new System.Drawing.Size(255, 38);
			this.lblApplication.TabIndex = 13;
			this.lblApplication.Text = "<application>";
			this.lblApplication.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblVersion
			// 
			this.lblVersion.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblVersion.Location = new System.Drawing.Point(3, 38);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(255, 19);
			this.lblVersion.TabIndex = 14;
			this.lblVersion.Text = "<version>";
			this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lpnDescription
			// 
			this.lpnDescription.ColumnCount = 1;
			this.lpnDescription.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.lpnDescription.Controls.Add(this.lblPoint2, 0, 2);
			this.lpnDescription.Controls.Add(this.lblPoint3, 0, 2);
			this.lpnDescription.Controls.Add(this.lblPoint1, 0, 1);
			this.lpnDescription.Controls.Add(this.lblDescription, 0, 0);
			this.lpnDescription.Location = new System.Drawing.Point(30, 153);
			this.lpnDescription.Name = "lpnDescription";
			this.lpnDescription.RowCount = 3;
			this.lpnDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.lpnDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.lpnDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.lpnDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.lpnDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.lpnDescription.Size = new System.Drawing.Size(363, 77);
			this.lpnDescription.TabIndex = 25;
			// 
			// lblPoint2
			// 
			this.lblPoint2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblPoint2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPoint2.Location = new System.Drawing.Point(3, 38);
			this.lblPoint2.Name = "lblPoint2";
			this.lblPoint2.Size = new System.Drawing.Size(357, 19);
			this.lblPoint2.TabIndex = 18;
			this.lblPoint2.Text = "<point2>";
			this.lblPoint2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblPoint3
			// 
			this.lblPoint3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblPoint3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPoint3.Location = new System.Drawing.Point(3, 57);
			this.lblPoint3.Name = "lblPoint3";
			this.lblPoint3.Size = new System.Drawing.Size(357, 20);
			this.lblPoint3.TabIndex = 17;
			this.lblPoint3.Text = "<point3>";
			this.lblPoint3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblPoint1
			// 
			this.lblPoint1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblPoint1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPoint1.Location = new System.Drawing.Point(3, 19);
			this.lblPoint1.Name = "lblPoint1";
			this.lblPoint1.Size = new System.Drawing.Size(357, 19);
			this.lblPoint1.TabIndex = 15;
			this.lblPoint1.Text = "<point1>";
			this.lblPoint1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDescription
			// 
			this.lblDescription.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDescription.Location = new System.Drawing.Point(3, 0);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(357, 19);
			this.lblDescription.TabIndex = 14;
			this.lblDescription.Text = "<description>";
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lpnSource
			// 
			this.lpnSource.ColumnCount = 1;
			this.lpnSource.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.lpnSource.Controls.Add(this.lblStreetSource, 0, 1);
			this.lpnSource.Controls.Add(this.lblCitySource, 0, 1);
			this.lpnSource.Controls.Add(this.lblCompanySource, 0, 0);
			this.lpnSource.Location = new System.Drawing.Point(88, 400);
			this.lpnSource.Name = "lpnSource";
			this.lpnSource.RowCount = 2;
			this.lpnSource.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.lpnSource.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.lpnSource.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
			this.lpnSource.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.lpnSource.Size = new System.Drawing.Size(305, 56);
			this.lpnSource.TabIndex = 26;
			// 
			// lblStreetSource
			// 
			this.lblStreetSource.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblStreetSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblStreetSource.Location = new System.Drawing.Point(3, 18);
			this.lblStreetSource.Name = "lblStreetSource";
			this.lblStreetSource.Size = new System.Drawing.Size(299, 18);
			this.lblStreetSource.TabIndex = 18;
			this.lblStreetSource.Text = "<streetsource>";
			this.lblStreetSource.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCitySource
			// 
			this.lblCitySource.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCitySource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCitySource.Location = new System.Drawing.Point(3, 36);
			this.lblCitySource.Name = "lblCitySource";
			this.lblCitySource.Size = new System.Drawing.Size(299, 20);
			this.lblCitySource.TabIndex = 17;
			this.lblCitySource.Text = "<citysource>";
			this.lblCitySource.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCompanySource
			// 
			this.lblCompanySource.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCompanySource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCompanySource.Location = new System.Drawing.Point(3, 0);
			this.lblCompanySource.Name = "lblCompanySource";
			this.lblCompanySource.Size = new System.Drawing.Size(299, 18);
			this.lblCompanySource.TabIndex = 15;
			this.lblCompanySource.Text = "<companysource>";
			this.lblCompanySource.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label15
			// 
			this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label15.Location = new System.Drawing.Point(35, 372);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(271, 25);
			this.label15.TabIndex = 27;
			this.label15.Text = "All rights reserved by:";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pbxIcon
			// 
			this.pbxIcon.Location = new System.Drawing.Point(30, 24);
			this.pbxIcon.Name = "pbxIcon";
			this.pbxIcon.Size = new System.Drawing.Size(96, 96);
			this.pbxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pbxIcon.TabIndex = 28;
			this.pbxIcon.TabStop = false;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(34, 247);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(271, 25);
			this.label1.TabIndex = 30;
			this.label1.Text = "Licensed for:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.lblCityTarget, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.lblStreetTarget, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.lblCompanyTargetBottom, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.lblCompanyTargetTop, 0, 0);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(88, 275);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 4;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(305, 83);
			this.tableLayoutPanel1.TabIndex = 31;
			// 
			// lblCompanyTargetTop
			// 
			this.lblCompanyTargetTop.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCompanyTargetTop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCompanyTargetTop.Location = new System.Drawing.Point(3, 0);
			this.lblCompanyTargetTop.Name = "lblCompanyTargetTop";
			this.lblCompanyTargetTop.Size = new System.Drawing.Size(299, 20);
			this.lblCompanyTargetTop.TabIndex = 16;
			this.lblCompanyTargetTop.Text = "<companytargettop>";
			this.lblCompanyTargetTop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCompanyTargetBottom
			// 
			this.lblCompanyTargetBottom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCompanyTargetBottom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCompanyTargetBottom.Location = new System.Drawing.Point(3, 20);
			this.lblCompanyTargetBottom.Name = "lblCompanyTargetBottom";
			this.lblCompanyTargetBottom.Size = new System.Drawing.Size(299, 20);
			this.lblCompanyTargetBottom.TabIndex = 17;
			this.lblCompanyTargetBottom.Text = "<companytargetbottom>";
			this.lblCompanyTargetBottom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblStreetTarget
			// 
			this.lblStreetTarget.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblStreetTarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblStreetTarget.Location = new System.Drawing.Point(3, 40);
			this.lblStreetTarget.Name = "lblStreetTarget";
			this.lblStreetTarget.Size = new System.Drawing.Size(299, 20);
			this.lblStreetTarget.TabIndex = 18;
			this.lblStreetTarget.Text = "<streettarget>";
			this.lblStreetTarget.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblCityTarget
			// 
			this.lblCityTarget.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lblCityTarget.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCityTarget.Location = new System.Drawing.Point(3, 60);
			this.lblCityTarget.Name = "lblCityTarget";
			this.lblCityTarget.Size = new System.Drawing.Size(299, 23);
			this.lblCityTarget.TabIndex = 19;
			this.lblCityTarget.Text = "<citytarget>";
			this.lblCityTarget.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// DialogAbout
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(420, 511);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pbxIcon);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.lpnSource);
			this.Controls.Add(this.lpnDescription);
			this.Controls.Add(this.lpnTitle);
			this.Controls.Add(this.btnClose);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DialogAbout";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "About";
			this.lpnTitle.ResumeLayout(false);
			this.lpnDescription.ResumeLayout(false);
			this.lpnSource.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pbxIcon)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.TableLayoutPanel lpnTitle;
		private System.Windows.Forms.Label lblDate;
		private System.Windows.Forms.Label lblApplication;
		private System.Windows.Forms.Label lblVersion;
		private System.Windows.Forms.TableLayoutPanel lpnDescription;
		private System.Windows.Forms.Label lblPoint2;
		private System.Windows.Forms.Label lblPoint3;
		private System.Windows.Forms.Label lblPoint1;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.TableLayoutPanel lpnSource;
		private System.Windows.Forms.Label lblStreetSource;
		private System.Windows.Forms.Label lblCitySource;
		private System.Windows.Forms.Label lblCompanySource;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.PictureBox pbxIcon;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label lblCityTarget;
		private System.Windows.Forms.Label lblStreetTarget;
		private System.Windows.Forms.Label lblCompanyTargetBottom;
		private System.Windows.Forms.Label lblCompanyTargetTop;
	}
}