﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
//
using Initdata;
using UCNotifier;
//
namespace GearSimulation
{
  //
  //------------------------------------------------------
  //  Section - Error
  //------------------------------------------------------
  //	
  public enum EErrorCode
  {
    ecNone = 0,
    ecInvalid
  };
  //
  public partial class FormMain : Form
  {
    //
    //------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------
    //		
    private const String HEADER_LIBRARY = "Main";
    //
    public const String TITLE = "GearSimulation";
    public const String APPLICATION = TITLE;
    public const String VERSION = "01V0101";
    public const String DATE = "16. August 2015";
    public const String USER = "OM";
    //
    private const String MAIN_HELPFILE = TITLE + ".chm";
    //
    public const String DESCRIPTION = TITLE;
    public const String POINT1 = " - Parametric Gear Construction";
    public const String POINT2 = " - Physical Gear Rotation";
    public const String POINT3 = " - ";
    //
    public const String COMPANYTARGETTOP = "Mädiger";
    public const String COMPANYTARGETBOTTOM = "Programming & Development";
    public const String STREETTARGET = "Bundesstrasse 37";
    public const String CITYTARGET = "D - 37191 Katlenburg-Lindau";
    //
    public const String COMPANYSOURCE = "Mädiger - Programming & Development";
    public const String STREETSOURCE = "Bundesstrasse 37";
    public const String CITYSOURCE = "D - 37191 Katlenburg-Lindau";
    //
    private const String SUBDIRECTORY_TARGET = "MWare\\" + TITLE + VERSION;
    //
    public const String INIT_FILENAME = TITLE;
    public const String INIT_FILEEXTENSION = ".ini.xml";
    public const String INIT_TITLE = TITLE;
    public const Int32 INIT_LEFT = 10;
    public const Int32 INIT_TOP = 10;
    public const Int32 INIT_WIDTH = 740;
    public const Int32 INIT_HEIGHT = 980;
    //
    public static readonly String[] ERRORS = 
    { 
      "None",
  	  "Invalid"
    };
    //
    //------------------------------------------------------
    //  Section - Member
    //------------------------------------------------------
    //		
    private String[] FArguments;
    private String FInitdataFilename = INIT_FILENAME + INIT_FILEEXTENSION;
    private String FInitdataDirectory;
    private CUCNotifier FUCNotifier;
    // direct Instances !!!
    private Timer FCloser;
    //
    //------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------
    //		
    public FormMain()
    {	// DecimalPoint 
      CInitdata.Init();
      // 
      InitializeComponent();
      StartPosition = FormStartPosition.Manual;
      //
      // Instantiation UCNotifier
      FUCNotifier = new CUCNotifier();
      pnlProtocol.Controls.Add(FUCNotifier);
      FUCNotifier.Dock = DockStyle.Fill;
      FUCNotifier.SetProtocolParameter(SUBDIRECTORY_TARGET,
                                       APPLICATION, VERSION,
                                       COMPANYSOURCE, USER);
      _Protocol("Creating all Controls");
      //
      // Instantiating UCGaussJordan
     // pnlClient.Controls.Add(FUCGaussNewton);
      //
      //----------------------------
      // Instantiation Help
      //----------------------------
      FHelpProvider.HelpNamespace = Path.Combine(Directory.GetCurrentDirectory(), MAIN_HELPFILE);
      //
      InitControls();
      if (!CInitdata.BuildInitdataDirectory(SUBDIRECTORY_TARGET,
                                            INIT_FILENAME,
                                            INIT_FILEEXTENSION,
                                            out FInitdataDirectory,
                                            out FInitdataFilename))
      { // alternative build and save project.ini.xml with INIT_-values
        this.Text = INIT_TITLE;
        this.Left = INIT_LEFT;
        this.Top = INIT_TOP;
        this.Width = INIT_WIDTH;
        this.Height = INIT_HEIGHT;
        FUCNotifier.Height = CUCNotifier.INIT_HEIGHT;
        SaveInitdata(FInitdataDirectory, FInitdataFilename);
      }
      _Error(EErrorCode.ecNone);
      _Protocol("Construction succesful");
    }
    //
    //------------------------------------------------------
    //  Section - Startup-Arguments
    //------------------------------------------------------
    //
    public void SetArguments(String[] arguments)
    {
      FArguments = arguments;
    }
    //
    //------------------------------------------------------
    //  Section - Form-Events
    //------------------------------------------------------
    //
    private void FormMain_Load(object sender, EventArgs e)
    {
      LoadInitdata(FInitdataDirectory, FInitdataFilename);
    }

    private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (null == FCloser)
      { // donot close
        FUCNotifier.SaveOnClosing();
        FCloser = new Timer();
        FCloser.Interval = 500;
        FCloser.Tick += SelfOnClose;
        /*if (FUCGaussNewton.IsComPortOpen())
        {
          FCloser.Start();
          FUCGaussNewton.PassivateHardware();
          e.Cancel = true;
        }*/
      }
      else
      { // close
        e.Cancel = false;
      }
    }

    void SelfOnClose(object sender, EventArgs e)
    {
      /*if (FUCGaussNewton.CommandsFinished())
      {
        if (FUCGaussNewton.IsComPortOpen())
        {
          FUCGaussNewton.CloseComPort();
        }
        FCloser.Stop();
        FCloser.Tick -= SelfOnClose;
        Close();
      }*/
    }
    //
    //----------------------------
    //  Section - Callbacks / Delegates
    //----------------------------

    //
    //------------------------------------------------------
    //  Section - Control-Events
    //------------------------------------------------------
    // 

    //
    //------------------------------------------------------
    //  Section - Callbacks / Delegates
    //------------------------------------------------------
    //
    private String BuildHeader()
    {
      return String.Format("{0}", HEADER_LIBRARY);
    }

    public void _Error(EErrorCode code)
    {
      if (FUCNotifier is CUCNotifier)
      {
        Int32 Index = (Int32)code;
        if ((Index < 0) && (ERRORS.Length <= Index))
        {
          Index = ERRORS.Length - 1;
        }
        String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
        FUCNotifier.Write(BuildHeader(), Line);
      }
    }

    public void _Protocol(String line)
    {
      if (FUCNotifier is CUCNotifier)
      {
        FUCNotifier.Write(BuildHeader(), line);
      }
    }
    //
    //----------------------------
    //  Section - Init
    //----------------------------
    //
    private void InitControls()
    {
      _Protocol("Initialisation of Controls");
      Text = TITLE + "  Version " + VERSION + " | " + COMPANYSOURCE;
      //
      //  Event - System
      mitSQuit.Click += new EventHandler(mitSQuit_Click);
      //
      //  Event - Configuration
      mitCDLoadInitdata.Click += new EventHandler(mitCDefaultLoad_Click);
      mitCDSaveInitdata.Click += new EventHandler(mitCDefaultSave_Click);
      mitCNLoadInitdata.Click += new EventHandler(mitCNamedLoad_Click);
      mitCNSaveInitdata.Click += new EventHandler(mitCNamedSave_Click);
      //
      //  Event - 
      mitPEditParameter.Click += new EventHandler(mitPEditParameter_Click);
      mitPClearProtocol.Click += new EventHandler(mitPClearProtocol_Click);
      mitPCopyToClipboard.Click += new EventHandler(mitPCopyToClipboard_Click);
      mitPLoadDiagnosticFile.Click += new EventHandler(mitPLoadDiagnosticFile_Click);
      mitPSaveDiagnosticFile.Click += new EventHandler(mitPSaveDiagnosticFile_Click);
      mitSendDiagnosticEmail.Click += new EventHandler(mitSendDiagnosticEmail_Click);
      //
      //  Event - Help
      mitHTopic.Click += new EventHandler(mitHTopic_Click);
      mitHContents.Click += new EventHandler(mitHContents_Click);
      mitHSearch.Click += new EventHandler(mitHSearch_Click);
      mitHAbout.Click += new EventHandler(mitHAbout_Click);   
    }

    private Boolean LoadInitdata(String directory, String filename)
    { // Open      
      _Protocol(String.Format("Directory..: '{0}'", directory));
      _Protocol(String.Format("FileName...: '{0}'", filename));
      String FileEntry = Path.Combine(directory, filename);
      //
      CInitdata ID = new CInitdata();
      String NodeBase;
      ID.OpenRead(FileEntry, out NodeBase);
      Int32 I;
      //String S;
      // MainWindow
      ID.SelectSection(CInitdata.NodeBase, CInitdata.NAME_MAINWINDOW);
      ID.ReadInteger(CInitdata.NAME_LEFT, out I, INIT_LEFT);
      this.Left = I;
      ID.ReadInteger(CInitdata.NAME_TOP, out I, INIT_TOP);
      this.Top = I;
      ID.ReadInteger(CInitdata.NAME_WIDTH, out I, INIT_WIDTH);
      this.Width = I;
      ID.ReadInteger(CInitdata.NAME_HEIGHT, out I, INIT_HEIGHT);
      this.Height = I;
      //
      // Notifier
      ID.SelectCreateSection(CInitdata.NodeBase, CUCNotifier.SECTION_NOTIFIER);
      FUCNotifier.LoadInitdata(ID);
      //
      // Hardware
      //!!!ID.SelectCreateSection(CInitdata.NodeBase, CUCGaussNewton.SECTION_LIBRARY);
      //!!!FUCGaussNewton.LoadInitdata(ID);
      //
      // Close
      ID.CloseRead();
      ID = null;
      // 
      return true;
    }

    private Boolean SaveInitdata(String directory, String filename)
    { // Open
      _Protocol(String.Format("Directory..: '{0}'", directory));
      _Protocol(String.Format("FileName...: '{0}'", filename));
      String FileEntry = Path.Combine(directory, filename);
      //
      CInitdata ID = new CInitdata();
      ID.OpenWrite(FileEntry, CInitdata.NodeBase);
      // MainWindow
      ID.SelectCreateSection(CInitdata.NodeBase, CInitdata.NAME_MAINWINDOW);
      ID.WriteString(CInitdata.NAME_TITLE, this.Text);
      ID.WriteInteger(CInitdata.NAME_LEFT, this.Left);
      ID.WriteInteger(CInitdata.NAME_TOP, this.Top);
      ID.WriteInteger(CInitdata.NAME_WIDTH, this.Width);
      ID.WriteInteger(CInitdata.NAME_HEIGHT, this.Height);
      //
      // Notifier
      ID.SelectCreateSection(CInitdata.NodeBase, CUCNotifier.SECTION_LIBRARY);
      FUCNotifier.SaveInitdata(ID);
      //
      // Hardware
      //!!!ID.SelectCreateSection(CInitdata.NodeBase, CUCGaussNewton.SECTION_LIBRARY);
      //!!!FUCGaussNewton.SaveInitdata(ID);
      //
      // Close
      ID.CloseWrite();
      ID = null;
      return true;
    }
    //
    //----------------------------------------
    //  private Helper
    //----------------------------------------
    //
    private void AnalyseArguments()
    { // Commandline-Arguments: Username/Password -> overwrite Initdata
      /*CArguments Arguments = new CArguments(FArguments);
      if (null != Arguments[ARGUMENT_USERNAME])
      {
        FLoginData.Username = Arguments[ARGUMENT_USERNAME];
      }
      if (null != Arguments[ARGUMENT_PASSWORD])
      {
        FLoginData.Password = Arguments[ARGUMENT_PASSWORD];
      }*/
    }
    //
    //----------------------------------------
    //  Menu-Event-Methods : Help
    //----------------------------------------
    //
    private void mitHTopic_Click(object sender, EventArgs e)
    {
      Help.ShowHelp(this, MAIN_HELPFILE, HelpNavigator.Topic, "./GaussNewton/BPCIndex.html");
    }

    private void mitHContents_Click(object sender, EventArgs e)
    {
      Help.ShowHelp(this, MAIN_HELPFILE, HelpNavigator.TableOfContents);
    }

    private void mitHSearch_Click(object sender, EventArgs e)
    {
      Help.ShowHelp(this, MAIN_HELPFILE, HelpNavigator.KeywordIndex);
    }

    private void mitHAbout_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = ":";
      _Protocol(Line);
      _Protocol("Call About-Dialog");
      CInitdata ID = new CInitdata();
      ID.SetApplicationData(APPLICATION, VERSION, DATE);
      ID.SetDescriptionData(DESCRIPTION, POINT1, POINT2, POINT3);
      ID.SetCompanySourceData(COMPANYSOURCE, STREETSOURCE, CITYSOURCE);
      ID.SetCompanyTargetData(COMPANYTARGETTOP, COMPANYTARGETBOTTOM, STREETTARGET, CITYTARGET);
      ID.SetIcon(this.Icon);
      ID.ShowModalDialogAbout();
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }
    //
    //----------------------------------------
    //  Menu-Event-Methods : System
    //----------------------------------------
    //
    private void mitSQuit_Click(object sender, EventArgs e)
    {
      _Protocol("Exit Application");
      Close();
    }
    //
    //----------------------------------------
    //  Menu-Event-Methods: Protocol
    //----------------------------------------
    //
    private void mitPEditParameter_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Edit Protocol-Parameter:";
      _Protocol(Line);
      Result &= FUCNotifier.EditParameterModal();
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitPClearProtocol_Click(object sender, EventArgs e)
    {
      String Line = "Clear Protocol.";
      _Protocol(Line);
      FUCNotifier.Clear();
    }

    private void mitPCopyToClipboard_Click(object sender, EventArgs e)
    {
      String Line = "Copy Protocol to Clipboard.";
      _Protocol(Line);
      FUCNotifier.CopyToClipboard();
    }

    private void mitPLoadDiagnosticFile_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Load Diagnostic-File with Dialog:";
      _Protocol(Line);
      Result &= FUCNotifier.LoadDiagnosticFileModal();
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitPSaveDiagnosticFile_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Save Diagnostic-File with Dialog:";
      _Protocol(Line);
      Result &= FUCNotifier.SaveDiagnosticFileModal();
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitSendDiagnosticEmail_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Send Diagnostic-Email:";
      _Protocol(Line);
      Result &= FUCNotifier.SendDiagnosticEmail();
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }
    //
    //----------------------------------------
    //  Menu-Event-Methods : Configuration
    //----------------------------------------
    //
    private void mitCDefaultSave_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Save Initdata:";
      _Protocol(Line);
      Result &= SaveInitdata(FInitdataDirectory, FInitdataFilename);
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitCDefaultLoad_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Load Initdata:";
      _Protocol(Line);
      Result &= LoadInitdata(FInitdataDirectory, FInitdataFilename);
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitCNamedSave_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Save Initdata with Dialog:";
      _Protocol(Line);
      DialogSaveInitdata.InitialDirectory = FInitdataDirectory;
      DialogSaveInitdata.FileName = FInitdataFilename;
      Result &= (DialogResult.OK == DialogSaveInitdata.ShowDialog());
      if (Result)
      {
        String Directory = Path.GetDirectoryName(DialogSaveInitdata.FileName);
        String FileName = Path.GetFileName(DialogSaveInitdata.FileName);
        Result &= SaveInitdata(Directory, FileName);
      }
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }

    private void mitCNamedLoad_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Load Initdata with Dialog:";
      _Protocol(Line);
      DialogLoadInitdata.InitialDirectory = FInitdataDirectory;
      DialogLoadInitdata.FileName = FInitdataFilename;
      Result &= (DialogResult.OK == DialogLoadInitdata.ShowDialog());
      if (Result)
      {
        String Directory = Path.GetDirectoryName(DialogLoadInitdata.FileName);
        String FileName = Path.GetFileName(DialogLoadInitdata.FileName);
        Result &= LoadInitdata(Directory, FileName);
      }
      Line = String.Format("-> {0}", Result);
      _Protocol(Line);
    }


  }
}
