﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using Initdata;
using UCNotifier;

namespace TextFile
{ //
	//------------------------------------------------------
	//  Section - Error
	//------------------------------------------------------
	//	
	public enum EErrorCode
	{
		None = 0,
		InvalidAccess = 1,
		Invalid
	};
	//
	//--------------------------------------------------
	//	Types
	//--------------------------------------------------
	//
	//
	//******************************************************
	//  MainSection - CUCMeasurementCollector
	//******************************************************
	//	
	public class CTextFileBase
	{ //
		//-----------------------------------------------
		//  Section - Constant Error
		//-----------------------------------------------
		//
		public static readonly String[] ERRORS = 
    { 
      "None",
			"Invalid Access",
  	  "Invalid"
    };
		//	
		//--------------------------------------------------
		//	Section - Constant
		//--------------------------------------------------
		//
		public static String HEADER_LIBRARY = "TextFile";
		public static String SECTION_LIBRARY = HEADER_LIBRARY;
		//
		protected const String INIT_DRIVESEPARATOR = ":";
		protected const String INIT_NAMESEPARATOR = ".";
		protected const String INIT_DRIVE = "C";
		protected const String INIT_PATH = INIT_NAMESEPARATOR;
		protected const String INIT_NAME = "filename";
		protected const String INIT_EXTENSION = "txt";
		protected const String INIT_ENTRY = INIT_NAME + INIT_NAMESEPARATOR + INIT_EXTENSION;
		protected const String INIT_FORMATSTRING = "{0}";
		protected const String INIT_FORMATCHARACTER = "{0}";
		protected const String INIT_FORMATBYTE = "{0:X}";
		protected const String INIT_FORMATINT16 = "{0}";
		protected const String INIT_FORMATUINT16 = "{0}";
		protected const String INIT_FORMATINT32 = "{0}";
		protected const String INIT_FORMATUINT32 = "{0}";
		protected const String INIT_FORMATDECIMAL = "{0}";
		protected const String INIT_FORMATSINGLE = "{0}";
		protected const String INIT_FORMATDOUBLE = "{0:0.000000000}";
		protected readonly String[] INIT_LINEDELIMITERS = new String[] { "\r", "\n", "\r\n" };
		protected readonly String[] INIT_READDELIMITERS = new String[] { "\t", " ", ",", ";" };
		protected readonly String[] INIT_WRITEDELIMITERS = new String[] { "\t", "\r\n" };
		//
		protected const String INIT_STRING = "";
		protected const Char INIT_CHAR = (Char)0x00;
		protected const Byte INIT_BYTE = 0x00;
		protected const Int16 INIT_INT16 = 0;
		protected const UInt16 INIT_UINT16 = 0;
		protected const Int32 INIT_INT32 = 0;
		protected const UInt32 INIT_UINT32 = 0;
		protected const Decimal INIT_DECIMAL = 0;
		protected const Single INIT_SINGLE = 0;
		protected const Double INIT_DOUBLE = 0;
		//
		//-----------------------------------------------
		//  Section - Variable
		//-----------------------------------------------
		//
		private CNotifier FNotifier;
		private RTextFileData FData;
		private StreamWriter FStreamWriter;
		private StreamReader FStreamReader;
		private String[][] FTokenArray;	// Row, Col
		private Int32 FRowIndex;
		private Int32 FColIndex;
		//
		//--------------------------------------------------
		//	Section - Constructor
		//--------------------------------------------------
		//
		public CTextFileBase()
		{ // DecimalSeparator
      CInitdata.Init();
      FNotifier = null;
			FData = new RTextFileData();
			InitData();
		}

    public CTextFileBase(CNotifier notifier)
    { // DecimalSeparator
      CInitdata.Init();
      FNotifier = notifier;
      FData = new RTextFileData();
      InitData();
    }

		protected void InitData()
		{
			FData.Entry = INIT_ENTRY;
			FData.Drive = INIT_DRIVE;
			FData.Path = INIT_PATH;
			FData.Name = INIT_NAME;
			FData.Extension = INIT_EXTENSION;
			FData.FormatString = INIT_FORMATSTRING;
			FData.FormatCharacter = INIT_FORMATCHARACTER;
			FData.FormatByte = INIT_FORMATBYTE;
			FData.FormatInt16 = INIT_FORMATINT16;
			FData.FormatUInt16 = INIT_FORMATUINT16;
			FData.FormatInt32 = INIT_FORMATINT32;
			FData.FormatUInt32 = INIT_FORMATUINT32;
			FData.FormatDecimal = INIT_FORMATDECIMAL;
			FData.FormatSingle = INIT_FORMATSINGLE;
			FData.FormatDouble = INIT_FORMATDOUBLE;
			FData.LineDelimiters = INIT_LINEDELIMITERS;
			FData.ReadDelimiters = INIT_READDELIMITERS;
			FData.WriteDelimiters = INIT_WRITEDELIMITERS;
		}
		//
		//--------------------------------------------------
		//	Section - Property
		//--------------------------------------------------
		//
		public void SetNotifier(CNotifier notifier)
		{
			FNotifier = notifier;
		}

		public Int32 RowCount
		{
			get { return FTokenArray.Length; }
		}

		public Int32 ColCount(Int32 rowindex)
		{
			return FTokenArray[rowindex].Length; 
		}

		public Int32 RowIndex
		{
			get { return FRowIndex; }
		}

		public Int32 ColIndex
		{
			get { return FColIndex; }
		}
		//
		//------------------------------------------------------
		//  Section - Protocol
		//------------------------------------------------------
		//
		private String BuildHeader()
		{
			return String.Format("{0}", HEADER_LIBRARY);
		}

		protected void _Error(EErrorCode code)
		{
			if (FNotifier is CNotifier)
			{
				Int32 Index = (Int32)code;
				if ((Index < 0) && (ERRORS.Length <= Index))
				{
					Index = ERRORS.Length - 1;
				}
				String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
				FNotifier.Write(BuildHeader(), Line);
			}
		}

		protected void _Protocol(String line)
		{
			if (FNotifier is CNotifier)
			{
				FNotifier.Write(BuildHeader(), line);
			}
		}
		//
		//--------------------------------------------------
		//	Section - Get/SetData
		//--------------------------------------------------
		//
		protected Boolean GetData(ref RTextFileData data)
		{
			data = FData;
			return true;
		}

		protected Boolean SetData(RTextFileData data)
		{
			FData = data;
			return true;
		}
		//
		//--------------------------------------------------
		//	Section - File-Management
		//--------------------------------------------------
		//
		protected Boolean OpenRead(String fileentry)
		{
			if (!(FStreamReader is StreamReader))
			{
				FStreamReader = new StreamReader(fileentry, Encoding.ASCII, false);
				String ReadBuffer = FStreamReader.ReadToEnd();
				String[] RowLines = ReadBuffer.Split(FData.LineDelimiters, StringSplitOptions.RemoveEmptyEntries);
				FTokenArray = new String[RowLines.Length][];
				for (Int32 RI = 0; RI < RowLines.Length; RI++)
				{
					String RowLine = RowLines[RI];
					String[] Tokens = RowLine.Split(FData.ReadDelimiters, StringSplitOptions.RemoveEmptyEntries);
					FTokenArray[RI] = new String[Tokens.Length];
					for (Int32 CI = 0; CI < Tokens.Length; CI++)
					{
						FTokenArray[RI][CI] = Tokens[CI];
					}
				}
				FRowIndex = 0;
				FColIndex = 0;
				return (0 < RowCount);
			}
			return false;
		}

    /* old version?!
        protected Boolean _OpenRead(String filename)
        {
          if (!(FStreamReader is StreamReader))
          {
            FStreamReader = new StreamReader(filename, Encoding.ASCII, false);
            String ReadBuffer = FStreamReader.ReadToEnd();
            Char[] Delimiters = new Char[] { '\t', '\r', '\n' };
            FTokenList = ReadBuffer.Split(Delimiters, StringSplitOptions.RemoveEmptyEntries); //((Char)0x09, (Char)0x20, (Char)0x0D, (Char)0x0A);
            //FTokenList = Regex.Split(ReadBuffer, "\t\r\n ");
            FTokenIndex = 0;
            return (0 < FTokenList.Length);
          }
          return false;
        }
     */

    public Boolean OpenWrite(String fileentry)
		{
			if (!(FStreamWriter is StreamWriter))
			{
				FStreamWriter = new StreamWriter(fileentry, false, Encoding.ASCII);
				return true;
			}
			return false;
		}

		public Boolean Close()
		{
			if (FStreamWriter is StreamWriter)
			{
				FStreamWriter.Close();
				FStreamWriter = null;
			}
			if (FStreamReader is StreamReader)
			{
				FStreamReader.Close();
				FStreamReader = null;
			}
			return true;
		}

		private Int32 RIH()
		{
			return FTokenArray.Length - 1;
		}

		private Int32 CIH(Int32 rowindex)
		{
			return FTokenArray[rowindex].Length - 1;
		}

		protected Boolean IsTokenAvailable()
		{	// Endposition reached?
			if ((FRowIndex < 0) || (FColIndex < 0))
			{ // no more Token available!
				return false;
			}
			//
			return ((FRowIndex <= RIH()) && (FColIndex <= CIH(RIH())));
		}

		protected Boolean ReadRowIndex(out Int32 rowindex)
		{
			if (IsTokenAvailable())
			{
				rowindex = FRowIndex;
				return true;
			}
			rowindex = 0;
			return false;
		}

		protected Boolean ReadColIndex(out Int32 colindex)
		{
			if (IsTokenAvailable())
			{
				colindex = FColIndex;
				return true;
			}
			colindex = 0;
			return false;
		}

		private void IncrementTokenIndices()
		{
			if (FColIndex < CIH(FRowIndex))
			{
				FColIndex++;
				return;
			}
			if (FRowIndex < RIH())
			{
				FColIndex = 0;
				FRowIndex++;
				return;
			}
			FColIndex = -1;
			FRowIndex = -1;
		}

		private Boolean ReadToken(out String token)
		{	// Read actual Token
			if (IsTokenAvailable())
			{
				token = FTokenArray[FRowIndex][FColIndex];
				IncrementTokenIndices();
				return true;
			}
			token = "";
			return false;
		}
		//
		//--------------------------------------------------
		//	Section - Write-Methods
		//--------------------------------------------------
		//
		protected Boolean Write(String value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatString, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Char value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatCharacter, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Byte value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatByte, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Int16 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatInt16, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(UInt16 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatUInt16, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Int32 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatInt32, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(UInt32 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatUInt32, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Decimal value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatDecimal, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Single value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatSingle, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Double value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatDouble, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}
		//
		//--------------------------------------------------
		//	Section - Read-Methods
		//--------------------------------------------------
		//
		/*protected Boolean Unread(Int32 row)
		{
			if (0 < FTokenIndex)
			{
				FTokenIndex--;
				return true;
			}
			return false;
		}*/

		protected Boolean Read(out String value)
		{
			value = INIT_STRING;
			return ReadToken(out value);
		}

		protected Boolean Read(out Char value)
		{
			value = INIT_CHAR;
			String Token;
			if (ReadToken(out Token))
			{
				return Char.TryParse(Token, out value);
			}
			return false;
		}

		protected Boolean Read(out Byte value)
		{
			value = INIT_BYTE;
			String Token;
			if (ReadToken(out Token))
			{
				return Byte.TryParse(Token, out value);
			}
			return false;
		}

		public Boolean Read(out Int16 value)
		{
			value = INIT_INT16;
			String Token;
			if (ReadToken(out Token))
			{
				return Int16.TryParse(Token, out value);
			}
			return false;
		}

		protected Boolean Read(out UInt16 value)
		{
			value = INIT_UINT16;
			String Token;
			if (ReadToken(out Token))
			{
				return UInt16.TryParse(Token, out value);
			}
			return false;
		}

		protected Boolean Read(out Int32 value)
		{
			value = INIT_INT32;
			String Token;
			if (ReadToken(out Token))
			{
				return Int32.TryParse(Token, out value);
			}
			return false;
		}

		protected Boolean Read(out UInt32 value)
		{
			value = INIT_UINT32;
			String Token;
			if (ReadToken(out Token))
			{
				return UInt32.TryParse(Token, out value);
			}
			return false;
		}

		protected Boolean Read(out Decimal value)
		{
			value = INIT_DECIMAL;
			String Token;
			if (ReadToken(out Token))
			{
				return Decimal.TryParse(Token, out value);
			}
			return false;
		}

		protected Boolean Read(out Single value)
		{
			value = INIT_SINGLE;
			String Token;
			if (ReadToken(out Token))
			{
				return Single.TryParse(Token, out value);
			}
			return false;
		}

		protected Boolean Read(out Double value)
		{
			value = INIT_DOUBLE;
			String Token;
			if (ReadToken(out Token))
			{
				return Double.TryParse(Token, out value);
			}
			return false;
		}
		//
		//--------------------------------------------------
		//	Section - 
		//--------------------------------------------------
		//

	}
}
