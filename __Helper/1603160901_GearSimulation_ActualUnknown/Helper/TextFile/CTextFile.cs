﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;

namespace TextFile
{

	//
	//
	//--------------------------------------------------
	//	Types
	//--------------------------------------------------
	//
	public struct RTextFileData
	{
		public String Entry;
		public String Drive;
		public String Path;
		public String Name;
		public String Extension;
		public String FormatString;
		public String FormatCharacter;
		public String FormatByte;
		public String FormatInt16;
		public String FormatUInt16;
		public String FormatInt32;
		public String FormatUInt32;
		public String FormatDecimal;
		public String FormatSingle;
		public String FormatDouble;
		public String[] LineDelimiters;
		public String[] ReadDelimiters;
		public String[] WriteDelimiters;
	};


	public class CTextFile : CTextFileBase
	{
		//
		//
		//--------------------------------------------------
		//	Constructor
		//--------------------------------------------------
		//
    public CTextFile()
      : base()
    {
    }

    public CTextFile(CNotifier notifier)
      : base(notifier)
    {
    }

		~CTextFile()
		{
			Close();
		}
		//
		//--------------------------------------------------
		//	Debug / Protocol / Warning / Error
		//--------------------------------------------------
		//


		//
		//--------------------------------------------------
		//	File-Management
		//--------------------------------------------------
		//
		public new Boolean OpenRead(String fileentry)
		{
			try
			{
				return base.OpenRead(fileentry);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean OpenWrite(String fileentry)
		{
			try
			{
				return base.OpenWrite(fileentry);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean Close()
		{
			try
			{
				return base.Close();
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}



		public new Boolean GetData(ref RTextFileData data)
		{
			try
			{
				return base.GetData(ref data);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean SetData(RTextFileData data)
		{
			try
			{
				return base.SetData(data);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean IsTokenAvailable()
		{
			try
			{
				return base.IsTokenAvailable();
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean ReadRowIndex(out Int32 rowindex)
		{
			try
			{
				return base.ReadRowIndex(out rowindex);
			}
			catch (Exception)
			{
				rowindex = 0;
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean ReadColIndex(out Int32 colindex)
		{
			try
			{
				return base.ReadColIndex(out colindex);
			}
			catch (Exception)
			{
				colindex = 0;
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}
		//
		//--------------------------------------------------
		//	Write-Methods
		//--------------------------------------------------
		//
		public new Boolean Write(String value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean Write(Char value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean Write(Byte value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean Write(Int16 value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean Write(UInt16 value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean Write(Int32 value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean Write(UInt32 value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean Write(Decimal value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean Write(Single value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}

		public new Boolean Write(Double value)
		{
			try
			{
				return base.Write(value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				return false;
			}
		}
		//
		//--------------------------------------------------
		//	Read-Methods
		//--------------------------------------------------
		//
		public new Boolean Read(out Double value)
		{
			try
			{
				return base.Read(out value);
			}
			catch (Exception)
			{
				_Error(EErrorCode.InvalidAccess);
				value = INIT_DOUBLE;
				return false;
			}
		}

	}
}
