﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using UCNotifier;

namespace TextFile
{
	public class CTextFileBase
	{
		//
		//
		//--------------------------------------------------
		//	Constant
		//--------------------------------------------------
		//
		protected const String INIT_DRIVESEPARATOR = ":";
		protected const String INIT_NAMESEPARATOR = ".";
		protected const String INIT_DRIVE = "C";
		protected const String INIT_PATH = INIT_NAMESEPARATOR;
		protected const String INIT_NAME = "filename";
		protected const String INIT_EXTENSION = "txt";
		protected const String INIT_ENTRY = INIT_NAME + INIT_NAMESEPARATOR + INIT_EXTENSION;
		protected const String INIT_FORMATSTRING = "{0}";
		protected const String INIT_FORMATCHARACTER = "{0}";
		protected const String INIT_FORMATBYTE = "{0:X}";
		protected const String INIT_FORMATINT16 = "{0}";
		protected const String INIT_FORMATUINT16 = "{0}";
		protected const String INIT_FORMATINT32 = "{0}";
		protected const String INIT_FORMATUINT32 = "{0}";
		protected const String INIT_FORMATDECIMAL = "{0}";
		protected const String INIT_FORMATSINGLE = "{0}";
		protected const String INIT_FORMATDOUBLE = "{0:0.000}";

		protected const String INIT_STRING = "";
		protected const Char INIT_CHAR = (Char)0x00;
		protected const Byte INIT_BYTE = 0x00;
		protected const Int16 INIT_INT16 = 0;
		protected const UInt16 INIT_UINT16 = 0;
		protected const Int32 INIT_INT32 = 0;
		protected const UInt32 INIT_UINT32 = 0;
		protected const Decimal INIT_DECIMAL = 0;
		protected const Single INIT_SINGLE = 0;
		protected const Double INIT_DOUBLE = 0;

		//
		//
		//--------------------------------------------------
		//	Member
		//--------------------------------------------------
		//
		private RTextFileData FData;
		private CMail FMail;
		// private StringReadr FStringReadr;
		private StreamWriter FStreamWriter;
		private StreamReader FStreamReader;
		//private StringReader FStringReader;
		private String[] FTokenList;
		private Int32 FTokenIndex;
		//
		//
		//--------------------------------------------------
		//	Constructor
		//--------------------------------------------------
		//
		public CTextFileBase(CNotifier notifier)
		{
			Defines.Init();

			FMail = new CMail(notifier);
			// NC Mail._Protocol("Creating Library");
			FData = new RTextFileData();
			InitData();
			FData.Entry = INIT_ENTRY;
		}

		~CTextFileBase()
		{
			// NC Mail._Protocol("Force closing File");
			// NC Mail._Protocol("Destroying Library");
		}

		public void InitData()
		{
			FData.Entry = INIT_ENTRY;
			FData.Drive = INIT_DRIVE;
			FData.Path = INIT_PATH;
			FData.Name = INIT_NAME;
			FData.Extension = INIT_EXTENSION;
			FData.FormatString = INIT_FORMATSTRING;
			FData.FormatCharacter = INIT_FORMATCHARACTER;
			FData.FormatByte = INIT_FORMATBYTE;
			FData.FormatInt16 = INIT_FORMATINT16;
			FData.FormatUInt16 = INIT_FORMATUINT16;
			FData.FormatInt32 = INIT_FORMATINT32;
			FData.FormatUInt32 = INIT_FORMATUINT32;
			FData.FormatDecimal = INIT_FORMATDECIMAL;
			FData.FormatSingle = INIT_FORMATSINGLE;
			FData.FormatDouble = INIT_FORMATDOUBLE;
		}
		//
		//
		//--------------------------------------------------
		//	Property
		//--------------------------------------------------
		//
		protected CMail Mail
		{
			get { return FMail; }
		}
		//
		//
		//--------------------------------------------------
		//	File-Management
		//--------------------------------------------------
		//
		protected Boolean _OpenRead(String filename)
		{
			if (!(FStreamReader is StreamReader))
			{
				FStreamReader = new StreamReader(filename, Encoding.ASCII, false);
				String ReadBuffer = FStreamReader.ReadToEnd();
				Char[] Delimiters = new Char[] { '\t', '\r', '\n' };
				FTokenList = ReadBuffer.Split(Delimiters, StringSplitOptions.RemoveEmptyEntries); //((Char)0x09, (Char)0x20, (Char)0x0D, (Char)0x0A);
				//FTokenList = Regex.Split(ReadBuffer, "\t\r\n ");
				FTokenIndex = 0;
				return (0 < FTokenList.Length);
			}
			return false;
		}

		protected Boolean _OpenWrite(String filename)
		{
			if (!(FStreamWriter is StreamWriter))
			{
				FStreamWriter = new StreamWriter(filename, false, Encoding.ASCII);
				return true;
			}
			return false;
		}

		protected Boolean _Close()
		{
			if (FStreamWriter is StreamWriter)
			{
				FStreamWriter.Close();
				FStreamWriter = null;
			}
			if (FStreamReader is StreamReader)
			{
				FStreamReader.Close();
				FStreamReader = null;
			}
			return true;
		}

		protected Boolean _GetData(ref RTextFileData data)
		{
			data = FData;
			return true;
		}

		protected Boolean _SetData(RTextFileData data)
		{
			FData = data;
			return true;
		}

		//
		//
		//--------------------------------------------------
		//	Write-Methods
		//--------------------------------------------------
		//
		/* !!! protected Boolean _Write(object value)
		{
			if (FStreamWriter is StreamWriter)
			{
				FStreamWriter.Write(value);
				return true;
			}
			return false;
		}*/

		protected Boolean _Write(String value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatString, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean _Write(Char value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatCharacter, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean _Write(Byte value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatByte, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean _Write(Int16 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatInt16, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}
		
		protected Boolean _Write(UInt16 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatUInt16, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}
		
		protected Boolean _Write(Int32 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatInt32, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}
	
		protected Boolean _Write(UInt32 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatUInt32, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean _Write(Decimal value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatDecimal, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean _Write(Single value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatSingle, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean _Write(Double value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatDouble, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}
		//
		//
		//--------------------------------------------------
		//	Read-Methods
		//--------------------------------------------------
		//
		protected Boolean _Read(out String value)
		{
			if (FTokenIndex < FTokenList.Length)
			{
				String Token = FTokenList[FTokenIndex++];
				value = Token;
				return true;
			}
			value = INIT_STRING;
			return false;
		}

		protected Boolean _Read(out Char value)
		{
			if (FTokenIndex < FTokenList.Length)
			{
				String Token = FTokenList[FTokenIndex++];
				return Char.TryParse(Token, out value);
			}
			value = INIT_CHAR;
			return false;
		}

		protected Boolean _Read(out Byte value)
		{
			if (FTokenIndex < FTokenList.Length)
			{
				String Token = FTokenList[FTokenIndex++];
				return Byte.TryParse(Token, out value);
			}
			value = INIT_BYTE;
			return false;
		}

		protected Boolean _Read(out Int16 value)
		{
			if (FTokenIndex < FTokenList.Length)
			{
				String Token = FTokenList[FTokenIndex++];
				return Int16.TryParse(Token, out value);
			}
			value = INIT_INT16;
			return false;
		}

		protected Boolean _Read(out UInt16 value)
		{
			if (FTokenIndex < FTokenList.Length)
			{
				String Token = FTokenList[FTokenIndex++];
				return UInt16.TryParse(Token, out value);
			}
			value = INIT_UINT16;
			return false;
		}

		protected Boolean _Read(out Int32 value)
		{
			if (FTokenIndex < FTokenList.Length)
			{
				String Token = FTokenList[FTokenIndex++];
				return Int32.TryParse(Token, out value);
			}
			value = INIT_INT32;
			return false;
		}

		protected Boolean _Read(out UInt32 value)
		{
			if (FTokenIndex < FTokenList.Length)
			{
				String Token = FTokenList[FTokenIndex++];
				return UInt32.TryParse(Token, out value);
			}
			value = INIT_UINT32;
			return false;
		}

		protected Boolean _Read(out Decimal value)
		{
			if (FTokenIndex < FTokenList.Length)
			{
				String Token = FTokenList[FTokenIndex++];
				return Decimal.TryParse(Token, out value);
			}
			value = INIT_DECIMAL;
			return false;
		}

		protected Boolean _Read(out Single value)
		{
			if (FTokenIndex < FTokenList.Length)
			{
				String Token = FTokenList[FTokenIndex++];
				return Single.TryParse(Token, out value);
			}
			value = INIT_SINGLE;
			return false;
		}

		protected Boolean _Read(out Double value)
		{
			if (FTokenIndex < FTokenList.Length)
			{
				String Token = FTokenList[FTokenIndex++];
				return Double.TryParse(Token, out value);
			}
			value = INIT_DOUBLE;
			return false;
		}


		//
		//
		//--------------------------------------------------
		//
		//--------------------------------------------------
		//

	}
}
