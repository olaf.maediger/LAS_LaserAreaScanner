using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Threading;

namespace TextFile
{
	static class Defines
	{
    public const String INIT_DECIMALSEPARATOR = ".";
    //
    static public void Init()
    { // Fix DecimalSeparator to '.':      
      String CN = Thread.CurrentThread.CurrentCulture.Name;
      CultureInfo CC = new CultureInfo(CN, false);
      CC.NumberFormat.NumberDecimalSeparator = INIT_DECIMALSEPARATOR;
      Thread.CurrentThread.CurrentCulture = CC;
    }

		static public String CreateDateTimeFileName(String title)
		{
			DateTime DateTime = DateTime.Now;
			Int32 Year = DateTime.Year % 100;
			String Filename = Year.ToString("00");
			Filename += DateTime.Month.ToString("00");
			Filename += DateTime.Day.ToString("00");
			Filename += DateTime.Hour.ToString("00");
			Filename += DateTime.Minute.ToString("00");
			Filename += DateTime.Second.ToString("00");
			Filename += DateTime.Millisecond.ToString();
			Filename += title;
			return Filename;
		}

		static public String CreateDateTimeFileName()
		{
			return CreateDateTimeFileName("TextFile");
		}
	}
}
