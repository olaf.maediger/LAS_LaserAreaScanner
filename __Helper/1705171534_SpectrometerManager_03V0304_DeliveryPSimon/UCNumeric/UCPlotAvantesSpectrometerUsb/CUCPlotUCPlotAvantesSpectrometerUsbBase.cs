﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using NLVector;
using UCNotifier;
using UCPlot2D;
//
namespace UCPlotSpectrometerManager
{
  public class CUCPlotSpectrometerManagerBase : CUCPlot2D
  {
		private const Double INIT_REAL_XMINIMUM = 160.0;
		private const Double INIT_REAL_XMAXIMUM = 480.0;
		private const Double INIT_REAL_XMAINDELTA = 40.0;
    private const Double INIT_REAL_XSUBDELTA = 10.0;
    //
    private const Double INIT_REAL_YMINIMUM = -8000;//-512.0 * 16.0;
    private const Double INIT_REAL_YMAXIMUM = +8000;//+512.0 * 16.0;//64.0;//16.0;
    private const Double INIT_REAL_YMAINDELTA = 2000.0;//10000.0;
    private const Double INIT_REAL_YSUBDELTA = 1000.0;//5000.0;
    //
		private const Double INIT_REAL_VMINIMUM = 0.0;
		private const Double INIT_REAL_VMAXIMUM = 0.6;
		private const Double INIT_REAL_VMAINDELTA = 0.1;
		private const Double INIT_REAL_VSUBDELTA = 0.02;
		private const Double INIT_REAL_WMINIMUM = -1.2;
		private const Double INIT_REAL_WMAXIMUM = 1.2;
		private const Double INIT_REAL_WMAINDELTA = 0.2;
		private const Double INIT_REAL_WSUBDELTA = 0.1;
		// Title
    private const String INIT_TITLE_TEXT = "Spectrometer Measurement";
		private const Byte INIT_TITLE_ALPHA = 255;
		private const Byte INIT_TITLE_RED = 50;//244;
		private const Byte INIT_TITLE_GREEN = 0;//244;
		private const Byte INIT_TITLE_BLUE = 255;
		// MainX
		private const Byte INIT_MAINX_ALPHA = 255;
    private const Byte INIT_MAINX_RED = 44; // 198;
		private const Byte INIT_MAINX_GREEN = 44; // 168;
		private const Byte INIT_MAINX_BLUE = 255; //68;
		// MainY
		private const Byte INIT_MAINY_ALPHA = 255;
		private const Byte INIT_MAINY_RED = 44;
		private const Byte INIT_MAINY_GREEN = 44;
		private const Byte INIT_MAINY_BLUE = 240;
		// MainV
		private const Byte INIT_MAINV_ALPHA = 255;
		private const Byte INIT_MAINV_RED = 44;
		private const Byte INIT_MAINV_GREEN = 44;
		private const Byte INIT_MAINV_BLUE = 240;
		// MainW
		private const Byte INIT_MAINW_ALPHA = 255;
    private const Byte INIT_MAINW_RED = 44; // 198;
    private const Byte INIT_MAINW_GREEN = 155; // 68;
    private const Byte INIT_MAINW_BLUE = 44; // 164;
		//--------------------------------------------------------
		// Preset Point
		private const Byte INIT_POINT_PRESET_PEN_ALPHA = 255;
		private const Byte INIT_POINT_PRESET_PEN_RED = 255;
		private const Byte INIT_POINT_PRESET_PEN_GREEN = 120;
		private const Byte INIT_POINT_PRESET_PEN_BLUE = 255;
		private const Byte INIT_POINT_PRESET_BRUSH_ALPHA = 255;
		private const Byte INIT_POINT_PRESET_BRUSH_RED = 255;
		private const Byte INIT_POINT_PRESET_BRUSH_GREEN = 255;
		private const Byte INIT_POINT_PRESET_BRUSH_BLUE = 10;
		// Preset Error
		private const Byte INIT_ERROR_PRESET_PEN_ALPHA = 229;
		private const Byte INIT_ERROR_PRESET_PEN_RED = 29;
		private const Byte INIT_ERROR_PRESET_PEN_GREEN = 172;
		private const Byte INIT_ERROR_PRESET_PEN_BLUE = 178;
		private const Byte INIT_ERROR_PRESET_BRUSH_ALPHA = 33;
		private const Byte INIT_ERROR_PRESET_BRUSH_RED = 129;
		private const Byte INIT_ERROR_PRESET_BRUSH_GREEN = 172;
		private const Byte INIT_ERROR_PRESET_BRUSH_BLUE = 178;
		// Preset Line
		private const Byte INIT_LINE_PRESET_PEN_ALPHA = 125;
		private const Byte INIT_LINE_PRESET_PEN_RED = 50;
		private const Byte INIT_LINE_PRESET_PEN_GREEN = 150;
		private const Byte INIT_LINE_PRESET_PEN_BLUE = 90;
		// Preset Description
		private const Byte INIT_DESCRIPTION_PRESET_PEN_ALPHA = 188;
		private const Byte INIT_DESCRIPTION_PRESET_PEN_RED = 66;
		private const Byte INIT_DESCRIPTION_PRESET_PEN_GREEN = 222;
		private const Byte INIT_DESCRIPTION_PRESET_PEN_BLUE = 66;
		private const Byte INIT_DESCRIPTION_PRESET_FONT_ALPHA = 188;
		private const Byte INIT_DESCRIPTION_PRESET_FONT_RED = 66;
		private const Byte INIT_DESCRIPTION_PRESET_FONT_GREEN = 222;
		private const Byte INIT_DESCRIPTION_PRESET_FONT_BLUE = 66;
		//--------------------------------------------------------
		// Fit Point
		private const Byte INIT_POINT_FIT_PEN_ALPHA = 0;
		private const Byte INIT_POINT_FIT_PEN_RED = 255;
		private const Byte INIT_POINT_FIT_PEN_GREEN = 220;
		private const Byte INIT_POINT_FIT_PEN_BLUE = 255;
		private const Byte INIT_POINT_FIT_BRUSH_ALPHA = 255;
		private const Byte INIT_POINT_FIT_BRUSH_RED = 233;
		private const Byte INIT_POINT_FIT_BRUSH_GREEN = 140;
		private const Byte INIT_POINT_FIT_BRUSH_BLUE = 110;
		// Theoretical Line
		private const Byte INIT_LINE_FIT_PEN_ALPHA = 228;
		private const Byte INIT_LINE_FIT_PEN_RED = 250;
		private const Byte INIT_LINE_FIT_PEN_GREEN = 120;
		private const Byte INIT_LINE_FIT_PEN_BLUE = 250;
		// Fit
		private const Byte INIT_DESCRIPTION_FIT_PEN_ALPHA = 222;
		private const Byte INIT_DESCRIPTION_FIT_PEN_RED = 222;
		private const Byte INIT_DESCRIPTION_FIT_PEN_GREEN = 111;
		private const Byte INIT_DESCRIPTION_FIT_PEN_BLUE = 111;
		private const Byte INIT_DESCRIPTION_FIT_FONT_ALPHA = 222;
		private const Byte INIT_DESCRIPTION_FIT_FONT_RED = 222;
		private const Byte INIT_DESCRIPTION_FIT_FONT_GREEN = 111;
		private const Byte INIT_DESCRIPTION_FIT_FONT_BLUE = 111;
		// Legend
		private const Byte INIT_LEGEND_PEN_ALPHA = 159;
		private const Byte INIT_LEGEND_PEN_RED = 250;
		private const Byte INIT_LEGEND_PEN_GREEN = 250;
		private const Byte INIT_LEGEND_PEN_BLUE = 190;
		private const Byte INIT_LEGEND_BRUSH_ALPHA = 159;
		private const Byte INIT_LEGEND_BRUSH_RED = 230;
		private const Byte INIT_LEGEND_BRUSH_GREEN = 230;
		private const Byte INIT_LEGEND_BRUSH_BLUE = 190;
		private const Byte INIT_LEGEND_TITLE_FONT_ALPHA = 255;
		private const Byte INIT_LEGEND_TITLE_FONT_RED = 255;
		private const Byte INIT_LEGEND_TITLE_FONT_GREEN = 0;
		private const Byte INIT_LEGEND_TITLE_FONT_BLUE = 190;
		// CommentA
		private const Byte INIT_COMMENTA_PEN_ALPHA = 79;
		private const Byte INIT_COMMENTA_PEN_RED = 90;
		private const Byte INIT_COMMENTA_PEN_GREEN = 90;
		private const Byte INIT_COMMENTA_PEN_BLUE = 150;
		private const Byte INIT_COMMENTA_BRUSH_ALPHA = 39;
		private const Byte INIT_COMMENTA_BRUSH_RED = 210;
		private const Byte INIT_COMMENTA_BRUSH_GREEN = 170;
		private const Byte INIT_COMMENTA_BRUSH_BLUE = 190;
		private const Byte INIT_COMMENTA_LABEL_ALPHA = 160;
		private const Byte INIT_COMMENTA_LABEL_RED = 255;
		private const Byte INIT_COMMENTA_LABEL_GREEN = 250;
		private const Byte INIT_COMMENTA_LABEL_BLUE = 120;
		// CommentB
		private const Byte INIT_COMMENTB_PEN_ALPHA = 79;
		private const Byte INIT_COMMENTB_PEN_RED = 90;
		private const Byte INIT_COMMENTB_PEN_GREEN = 90;
		private const Byte INIT_COMMENTB_PEN_BLUE = 150;
		private const Byte INIT_COMMENTB_BRUSH_ALPHA = 39;
		private const Byte INIT_COMMENTB_BRUSH_RED = 210;
		private const Byte INIT_COMMENTB_BRUSH_GREEN = 170;
		private const Byte INIT_COMMENTB_BRUSH_BLUE = 190;
		private const Byte INIT_COMMENTB_LABEL_ALPHA = 160;
		private const Byte INIT_COMMENTB_LABEL_RED = 255;
		private const Byte INIT_COMMENTB_LABEL_GREEN = 250;
		private const Byte INIT_COMMENTB_LABEL_BLUE = 120;
		//
    CNotifier FNotifier;
    CVectorDouble FPresetX = null;
    CVectorDouble FPresetY = null;
    CVectorDouble FFitX = null;
    CVectorDouble FFitY = null;
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FPresetX = new CVectorDouble(FNotifier, 0);
      FPresetY = new CVectorDouble(FNotifier, 0);
      FFitX = new CVectorDouble(FNotifier, 0);
      FFitY = new CVectorDouble(FNotifier, 0);
      Init();
      //
      RLabelToolData LabelToolData = new RLabelToolData();
      GetLabelToolData(out LabelToolData);
      LabelToolData.Title.Text = INIT_TITLE_TEXT;
      SetLabelToolData(LabelToolData);
      //
      RSizeToolData SizeToolData = new RSizeToolData();
      GetSizeToolData(out SizeToolData);
      SizeToolData.RealX.Minimum = INIT_REAL_XMINIMUM;
      SizeToolData.RealX.Maximum = INIT_REAL_XMAXIMUM;
      SizeToolData.RealY.Minimum = INIT_REAL_YMINIMUM;
      SizeToolData.RealY.Maximum = INIT_REAL_YMAXIMUM;
      SizeToolData.RealV.Minimum = INIT_REAL_VMINIMUM;
      SizeToolData.RealV.Maximum = INIT_REAL_VMAXIMUM;
      SizeToolData.RealW.Minimum = INIT_REAL_WMINIMUM;
      SizeToolData.RealW.Maximum = INIT_REAL_WMAXIMUM;
      SetSizeToolData(SizeToolData);

    }

    public Boolean SetValues(Double[] vectorwavelength,
                             Double[] vectorintensity,
                             Double factor)
    {
      ClearAllVectors();
      if ((vectorwavelength is Double[]) && (vectorintensity is Double[]))
      {
        Int32 VectorSize = Math.Min(vectorwavelength.Length, vectorintensity.Length);
        VectorSize = Math.Min(2048, VectorSize); // 2048 - Maximum ChannelCount Spectrometer
        FPresetX = new CVectorDouble(FNotifier, VectorSize, vectorwavelength);
        FPresetY = new CVectorDouble(FNotifier, VectorSize, vectorintensity);
        FPresetY.MultiplyConstant(factor);
        Init();
        return true;
      }
      return false;
    }

    public void SetTitle(String title)
    {
      RLabelToolData LabelToolData = new RLabelToolData();
      GetLabelToolData(out LabelToolData);
      LabelToolData.Title.Text = title;
      SetLabelToolData(LabelToolData);
    }

    public void SetRealExtrema(Double xminimum, Double xmaximum,
                               Double yminimum, Double ymaximum)
    {
      RSizeToolData SizeToolData = new RSizeToolData();
      GetSizeToolData(out SizeToolData);
      SizeToolData.RealX.Minimum = xminimum;
      SizeToolData.RealX.Maximum = xmaximum;
      SizeToolData.RealY.Minimum = yminimum;
      SizeToolData.RealY.Maximum = ymaximum;
      SetSizeToolData(SizeToolData);
    }

    public void AddFitValue(Double x, Double y)
    {
			FFitX.Add(x);
			FFitY.Add(y);
    }


    public CUCPlotSpectrometerManagerBase()
			: base()
		{
			Init();
		}

		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// CPlotSpectrum
			// 
      this.Name = "CUCPlotSpectrometerManager";
			this.Size = new System.Drawing.Size(600, 588);
			this.ResumeLayout(false);
		}

		//
		private void Init()
		{
			//
			//  Lock -> Zeichnen einfrieren
			//
			Lock();
			//
			// *******************
			// *** MapToolData ***
			// *******************
			RMapToolData MapToolData = new RMapToolData();
			GetMapToolData(out MapToolData);
			MapToolData.AxisX = EMapMode.Linear;
      MapToolData.AxisY = EMapMode.Linear; // EMapMode.LinearReverse;
      MapToolData.AxisV = EMapMode.Linear;
      MapToolData.AxisW = EMapMode.Linear;
			SetMapToolData(MapToolData);
			//
			// *********************
			// *** FrameToolData ***
			// *********************
			RFrameToolData FrameToolData = new RFrameToolData();
			GetFrameToolData(out FrameToolData);
			FrameToolData.Brush.Color = Color.FromArgb(199, 222, 222);
			FrameToolData.Brush.Style = EBrushStyle.Solid;
			FrameToolData.Pen.Color = Color.FromArgb(199, 233, 233);
			FrameToolData.Pen.Width = 5;
			// NC FrameToolData.Pen.Mode = EPenMode.pmCopy;
			FrameToolData.Pen.Style = EPenStyle.Solid;
			SetFrameToolData(FrameToolData);
			//
			// *********************
			// *** GraphToolData ***
			// *********************
			RGraphToolData GraphToolData = new RGraphToolData();
			GetGraphToolData(out GraphToolData);
			GraphToolData.Brush.Color = Color.FromArgb(199, 233, 233);
			GraphToolData.Brush.Style = EBrushStyle.Solid;
			GraphToolData.Pen.Color = Color.FromArgb(22, 22, 255);
			GraphToolData.Pen.Width = 1;
			GraphToolData.Pen.Style = EPenStyle.Solid;
			SetGraphToolData(GraphToolData);
			//
			// ********************
			// *** SizeToolData ***
			// ********************
      RSizeToolData SizeToolData = new RSizeToolData();
      GetSizeToolData(out SizeToolData);
      SizeToolData.RealX.Minimum = INIT_REAL_XMINIMUM;
      SizeToolData.RealX.Maximum = INIT_REAL_XMAXIMUM;
      SizeToolData.RealY.Minimum = INIT_REAL_YMINIMUM;
      SizeToolData.RealY.Maximum = INIT_REAL_YMAXIMUM;
      SizeToolData.RealV.Minimum = INIT_REAL_VMINIMUM;
      SizeToolData.RealV.Maximum = INIT_REAL_VMAXIMUM;
      SizeToolData.RealW.Minimum = INIT_REAL_WMINIMUM;
      SizeToolData.RealW.Maximum = INIT_REAL_WMAXIMUM;
      SetSizeToolData(SizeToolData);
      //
			//**********************
			// *** LabelToolData ***
			//**********************
			RLabelToolData LabelToolData = new RLabelToolData();
			GetLabelToolData(out LabelToolData);
			// Label - Title
			LabelToolData.Title.Visible = true;
      // NC LabelToolData.Title.Text = INIT_TITLE_TEXT;
			LabelToolData.Title.Position.OffsetX = 0;
			LabelToolData.Title.Position.OffsetY = 0;
			LabelToolData.Title.Font.Name = "Arial";
			LabelToolData.Title.Font.Size = 18;
			LabelToolData.Title.Font.Style = EFontStyle.Standard;
			LabelToolData.Title.Font.Color = Color.FromArgb(INIT_TITLE_ALPHA,
																											INIT_TITLE_RED,
																											INIT_TITLE_GREEN,
																											INIT_TITLE_BLUE);
			// Label - AxisX
			LabelToolData.AxisX.Visible = true;
      LabelToolData.AxisX.Text = "Lambda";
			LabelToolData.AxisX.Position.DX = 0;
			LabelToolData.AxisX.Position.DY = 0;
			LabelToolData.AxisX.Font.Name = "Arial";
			LabelToolData.AxisX.Font.Size = 10;
			LabelToolData.AxisX.Font.Style = EFontStyle.Standard;
			LabelToolData.AxisX.Font.Color = Color.FromArgb(INIT_MAINX_ALPHA, 
																											INIT_MAINX_RED,
																											INIT_MAINX_GREEN,
																											INIT_MAINX_BLUE);
			// Label - AxisY
			LabelToolData.AxisY.Visible = true;
      LabelToolData.AxisY.Text = "Intensity";
			LabelToolData.AxisY.Position.DX = 0;
			LabelToolData.AxisY.Position.DY = 0;
			LabelToolData.AxisY.Font.Name = "Arial";
			LabelToolData.AxisY.Font.Size = 10;
			LabelToolData.AxisY.Font.Style = EFontStyle.Standard;
			LabelToolData.AxisY.Font.Color = Color.FromArgb(INIT_MAINY_ALPHA,
																											INIT_MAINY_RED,
																											INIT_MAINY_GREEN,
																											INIT_MAINY_BLUE);
			// Label - AxisV								 								
			LabelToolData.AxisV.Visible = false;
			LabelToolData.AxisV.Text = "";
			LabelToolData.AxisV.Position.DX = 0;
			LabelToolData.AxisV.Position.DY = 0;
			LabelToolData.AxisV.Font.Name = "Arial";
			LabelToolData.AxisV.Font.Size = 10;
			LabelToolData.AxisV.Font.Style = EFontStyle.Standard;
			LabelToolData.AxisV.Font.Color = Color.FromArgb(INIT_MAINV_ALPHA,
																											INIT_MAINV_RED,
																											INIT_MAINV_GREEN,
																											INIT_MAINV_BLUE);
			// Label - AxisW
			LabelToolData.AxisW.Visible = false;
      LabelToolData.AxisW.Text = "";
			LabelToolData.AxisW.Position.DX = 0;
			LabelToolData.AxisW.Position.DY = 0;
			LabelToolData.AxisW.Font.Name = "Arial";
			LabelToolData.AxisW.Font.Size = 10;
			LabelToolData.AxisW.Font.Style = EFontStyle.Standard;
			LabelToolData.AxisW.Font.Color = Color.FromArgb(INIT_MAINW_ALPHA,
																											INIT_MAINW_RED,
																											INIT_MAINW_GREEN,
																											INIT_MAINW_BLUE);
			// Label - UnitX
			LabelToolData.UnitX.Visible = true;
			LabelToolData.UnitX.Text = "[nm]";
			LabelToolData.UnitX.Position.DX = 0;
			LabelToolData.UnitX.Position.DY = 0;
			LabelToolData.UnitX.Font.Name = "Arial";
			LabelToolData.UnitX.Font.Size = 8;
			LabelToolData.UnitX.Font.Style = EFontStyle.Standard;
			LabelToolData.UnitX.Font.Color = Color.FromArgb(INIT_MAINX_ALPHA,
																											INIT_MAINX_RED,
																											INIT_MAINX_GREEN,
																											INIT_MAINX_BLUE);
			// Label - UnitY
			LabelToolData.UnitY.Visible = true;
			LabelToolData.UnitY.Text = "[]";
			LabelToolData.UnitY.Position.DX = 0;
			LabelToolData.UnitY.Position.DY = 0;
			LabelToolData.UnitY.Font.Name = "Arial";
			LabelToolData.UnitY.Font.Size = 8;
			LabelToolData.UnitY.Font.Style = EFontStyle.Standard;
			LabelToolData.UnitY.Font.Color = Color.FromArgb(INIT_MAINY_ALPHA,
																											INIT_MAINY_RED,	
																											INIT_MAINY_GREEN,
																											INIT_MAINY_BLUE);
			// Label - UnitV
			LabelToolData.UnitV.Visible = false;
			LabelToolData.UnitV.Text = "[ ]";
			LabelToolData.UnitV.Position.DX = 0;
			LabelToolData.UnitV.Position.DY = 0;
			LabelToolData.UnitV.Font.Name = "Arial";
			LabelToolData.UnitV.Font.Size = 8;
			LabelToolData.UnitV.Font.Style = EFontStyle.Standard;
			LabelToolData.UnitV.Font.Color = Color.FromArgb(INIT_MAINV_ALPHA,
																											INIT_MAINV_RED,
																											INIT_MAINV_GREEN,
																											INIT_MAINV_BLUE);
			// Label - UnitW
			LabelToolData.UnitW.Visible = false;
			LabelToolData.UnitW.Text = "[ ]";
			LabelToolData.UnitW.Position.DX = 0;
			LabelToolData.UnitW.Position.DY = 0;
			LabelToolData.UnitW.Font.Name = "Arial";
			LabelToolData.UnitW.Font.Size = 8;
			LabelToolData.UnitW.Font.Style = EFontStyle.Standard;
			LabelToolData.UnitW.Font.Color = Color.FromArgb(INIT_MAINW_ALPHA,
																											INIT_MAINW_RED,
																											INIT_MAINW_GREEN,
																											INIT_MAINW_BLUE);
			// Label - Transfer
			SetLabelToolData(LabelToolData);
			//
			// *********************
			// *** ScaleToolData ***
			// *********************
			RScaleToolData ScaleToolData = new RScaleToolData();
			GetScaleToolData(out ScaleToolData);
			// MainX
			ScaleToolData.MainX.Visible = true;
			ScaleToolData.MainX.Font.Name = "Arial";
			ScaleToolData.MainX.Font.Size = 8;
			ScaleToolData.MainX.Font.Color = Color.FromArgb(INIT_MAINX_ALPHA,
																											INIT_MAINX_RED,
																											INIT_MAINX_GREEN,
																											INIT_MAINX_BLUE);
			ScaleToolData.MainX.Format = "{0:0.#}";
			ScaleToolData.MainX.Position.OffsetY = 0;
			// MainY
			ScaleToolData.MainY.Visible = true;
			ScaleToolData.MainY.Font.Name = "Arial";
			ScaleToolData.MainY.Font.Size = 8;
			ScaleToolData.MainY.Font.Color = Color.FromArgb(INIT_MAINY_ALPHA,
																											INIT_MAINY_RED,
																											INIT_MAINY_GREEN,
																											INIT_MAINY_BLUE);
			ScaleToolData.MainY.Format = "{0:0.#}";
			ScaleToolData.MainY.Position.OffsetX = 0;
			// MainV
			ScaleToolData.MainV.Visible = false;
			ScaleToolData.MainV.Font.Name = "Arial";
			ScaleToolData.MainV.Font.Size = 8;
			ScaleToolData.MainV.Font.Color = Color.FromArgb(INIT_MAINV_ALPHA,
																											INIT_MAINV_RED,
																											INIT_MAINV_GREEN,
																											INIT_MAINV_BLUE);
			ScaleToolData.MainV.Format = "{0:0.0}";
			ScaleToolData.MainV.Position.OffsetY = 0;
			// MainW
			ScaleToolData.MainW.Visible = false;
			ScaleToolData.MainW.Font.Name = "Arial";
			ScaleToolData.MainW.Font.Size = 8;
			ScaleToolData.MainW.Font.Color = Color.FromArgb(INIT_MAINW_ALPHA,
																											INIT_MAINW_RED,
																											INIT_MAINW_GREEN,
																											INIT_MAINW_BLUE);
			ScaleToolData.MainW.Format = "{0:0.0}";
			ScaleToolData.MainW.Position.OffsetX = -4;
			// Scale - Transfer
			SetScaleToolData(ScaleToolData);
			//
			// ********************
			// *** GridToolData ***
			// ********************
			RGridToolData GridToolData = new RGridToolData();
			GetGridToolData(out GridToolData);
			// MainX
			GridToolData.MainX.Visible = true;
			GridToolData.MainX.Minimum = INIT_REAL_XMINIMUM;
			GridToolData.MainX.Maximum = INIT_REAL_XMAXIMUM;
			GridToolData.MainX.Delta = INIT_REAL_XMAINDELTA;
			GridToolData.MainX.Pen.Color = Color.FromArgb(INIT_MAINX_ALPHA,
																										INIT_MAINX_RED,
																										INIT_MAINX_GREEN,
																										INIT_MAINX_BLUE);
			GridToolData.MainX.Pen.Style = EPenStyle.Solid;
			// MainY
			GridToolData.MainY.Visible = true;
			GridToolData.MainY.Minimum = INIT_REAL_YMINIMUM;
			GridToolData.MainY.Maximum = INIT_REAL_YMAXIMUM;
			GridToolData.MainY.Delta = INIT_REAL_YMAINDELTA;
			GridToolData.MainY.Pen.Color = Color.FromArgb(INIT_MAINY_ALPHA,
																										INIT_MAINY_RED,
																										INIT_MAINY_GREEN,
																										INIT_MAINY_BLUE);
			GridToolData.MainY.Pen.Style = EPenStyle.Solid;
			// MainV
			GridToolData.MainV.Visible = false;
			GridToolData.MainV.Minimum = INIT_REAL_VMINIMUM;
			GridToolData.MainV.Maximum = INIT_REAL_VMAXIMUM;
			GridToolData.MainV.Delta = INIT_REAL_VMAINDELTA;
			GridToolData.MainV.Pen.Color = Color.FromArgb(INIT_MAINV_ALPHA,
																										INIT_MAINV_RED,
																										INIT_MAINV_GREEN,
																										INIT_MAINV_BLUE);
			GridToolData.MainV.Pen.Style = EPenStyle.Solid;
			// MainW
			GridToolData.MainW.Visible = false;
			GridToolData.MainW.Minimum = INIT_REAL_WMINIMUM;
			GridToolData.MainW.Maximum = INIT_REAL_WMAXIMUM;
			GridToolData.MainW.Delta = INIT_REAL_WMAINDELTA;
			GridToolData.MainW.Pen.Color = Color.FromArgb(INIT_MAINW_ALPHA,
																										INIT_MAINW_RED,
																										INIT_MAINW_GREEN,
																										INIT_MAINW_BLUE);
			GridToolData.MainW.Pen.Style = EPenStyle.Solid;
			// SubX
			GridToolData.SubX.Visible = true;
			GridToolData.SubX.Minimum = 0.0;
			GridToolData.SubX.Delta = INIT_REAL_XSUBDELTA;
			GridToolData.SubX.Pen.Color = Color.FromArgb(INIT_MAINX_ALPHA,
																									 INIT_MAINX_RED,	
																									 INIT_MAINX_GREEN,
																									 INIT_MAINX_BLUE);
			GridToolData.SubX.Pen.Style = EPenStyle.Dot;
			// SubY
			GridToolData.SubY.Visible = true;
			GridToolData.SubY.Minimum = 0.0;
			GridToolData.SubY.Delta = INIT_REAL_YSUBDELTA;
			GridToolData.SubY.Pen.Color = Color.FromArgb(INIT_MAINY_ALPHA,
																									 INIT_MAINY_RED,
																									 INIT_MAINY_GREEN,
																									 INIT_MAINY_BLUE);
			GridToolData.SubY.Pen.Style = EPenStyle.Dot;
			// SubV
			GridToolData.SubV.Visible = false;
			GridToolData.SubV.Minimum = 0.0;
			GridToolData.SubV.Delta = INIT_REAL_VSUBDELTA;
			GridToolData.SubV.Pen.Color = Color.FromArgb(INIT_MAINV_ALPHA,
																									 INIT_MAINV_RED,
																									 INIT_MAINV_GREEN,
																									 INIT_MAINV_BLUE);
			GridToolData.SubV.Pen.Style = EPenStyle.Dot;
			// SubW
			GridToolData.SubW.Visible = false;
			GridToolData.SubW.Minimum = 0.0;
			GridToolData.SubW.Maximum = 1.0;
			GridToolData.SubW.Delta = INIT_REAL_WSUBDELTA;
			GridToolData.SubW.Pen.Color = Color.FromArgb(INIT_MAINW_ALPHA,
																									 INIT_MAINW_RED,	
																									 INIT_MAINW_GREEN,
																									 INIT_MAINW_BLUE);
			GridToolData.SubW.Pen.Style = EPenStyle.Dot;
			// Grid - Transfer
			SetGridToolData(GridToolData);
			//
			// ********************
			// *** TickToolData ***
			// ********************
			RTickToolData TickToolData = new RTickToolData();
			GetTickToolData(out TickToolData);
			// MainX
			TickToolData.MainX.Visible = true;
			TickToolData.MainX.Pen.Color = Color.FromArgb(INIT_MAINX_ALPHA,
																										INIT_MAINX_RED,
																										INIT_MAINX_GREEN,
																										INIT_MAINX_BLUE);
			TickToolData.MainX.Pen.Width = 1;
			TickToolData.MainX.Pen.Style = EPenStyle.Solid;
			TickToolData.MainX.Size = 4;
			// SubX
			TickToolData.SubX.Visible = true;
			TickToolData.SubX.Pen.Color = Color.FromArgb(INIT_MAINX_ALPHA, 
																									 INIT_MAINX_RED,
																									 INIT_MAINX_GREEN,
																									 INIT_MAINX_BLUE);
			TickToolData.SubX.Pen.Width = 1;
			TickToolData.SubX.Pen.Style = EPenStyle.Solid;
			TickToolData.SubX.Size = 2;
			// MainY
			TickToolData.MainY.Visible = true;
			TickToolData.MainY.Pen.Color = Color.FromArgb(INIT_MAINY_ALPHA,
																										INIT_MAINY_RED,	
																										INIT_MAINY_GREEN,
																										INIT_MAINY_BLUE);
			TickToolData.MainY.Pen.Width = 1;
			TickToolData.MainY.Pen.Style = EPenStyle.Solid;
			TickToolData.MainY.Size = 4;
			// SubY
			TickToolData.SubY.Visible = true;
			TickToolData.SubY.Pen.Color = Color.FromArgb(INIT_MAINY_ALPHA,
																									 INIT_MAINY_RED,
																									 INIT_MAINY_GREEN,
																									 INIT_MAINY_BLUE);
			TickToolData.SubY.Pen.Width = 1;
			TickToolData.SubY.Pen.Style = EPenStyle.Solid;
			TickToolData.SubY.Size = 2;
			// MainV
			TickToolData.MainV.Visible = false;
			TickToolData.MainV.Pen.Color = Color.FromArgb(INIT_MAINV_RED,
																										INIT_MAINV_GREEN,
																										INIT_MAINV_BLUE);
			TickToolData.MainV.Pen.Width = 1;
			TickToolData.MainV.Pen.Style = EPenStyle.Solid;
			TickToolData.MainV.Size = 4;
			// SubV
			TickToolData.SubV.Visible = false;
			TickToolData.SubV.Pen.Color = Color.FromArgb(INIT_MAINV_ALPHA,
																									 INIT_MAINV_RED,	
																									 INIT_MAINV_GREEN,
																									 INIT_MAINV_BLUE);
			TickToolData.SubV.Pen.Width = 1;
			TickToolData.SubV.Pen.Style = EPenStyle.Solid;
			TickToolData.SubV.Size = 2;
			// MainW
			TickToolData.MainW.Visible = false;
			TickToolData.MainW.Pen.Color = Color.FromArgb(INIT_MAINW_ALPHA,
																										INIT_MAINW_RED,
																										INIT_MAINW_GREEN,
																										INIT_MAINW_BLUE);
			TickToolData.MainW.Pen.Width = 1;
			TickToolData.MainW.Pen.Style = EPenStyle.Solid;
			TickToolData.MainW.Size = 4;
			// SubW
			TickToolData.SubW.Visible = false;
			TickToolData.SubW.Pen.Color = Color.FromArgb(INIT_MAINW_ALPHA,
																									 INIT_MAINW_RED,
																									 INIT_MAINW_GREEN,
																									 INIT_MAINW_BLUE);
			TickToolData.SubW.Pen.Width = 1;
			TickToolData.SubW.Pen.Style = EPenStyle.Solid;
			TickToolData.SubW.Size = 2;
			// Tick - Transfer
			SetTickToolData(TickToolData);
			//
			// *********************
			// *** PointToolData ***
			// *********************
			RPointToolData PointToolData = new RPointToolData();
			GetPointToolData(out PointToolData);
			PointToolData.Pen.Style = EPenStyle.Solid;
			PointToolData.Pen.Width = 1;
			SetPointToolData(PointToolData);
			//
			// ********************
			// *** LineToolData ***
			// ********************
			RLineToolData LineToolData = new RLineToolData();
			GetLineToolData(out LineToolData);
			LineToolData.Pen.Color = Color.FromArgb(255, 128, 128);
			LineToolData.Pen.Style = EPenStyle.Solid;
			LineToolData.Pen.Width = 2;
			SetLineToolData(LineToolData);
			//
			// ****************************************
			// *** Fit Y(X) - Erzeugung der Wertepaare 
			// ****************************************
			//
			Guid FitID = CreateAddVector("Fit");
			// VectorData - Definition der Vector-Beschriftung
			RVectorData VectorData = new RVectorData();
			// Description - Vector X
			GetVectorData(FitID, out VectorData);
			VectorData.DescriptionData.Visible = true;
			VectorData.DescriptionData.ConnectionIndex = 1;
			VectorData.DescriptionData.Text = "Position";
			VectorData.DescriptionData.Position.OffsetX = 20;
			VectorData.DescriptionData.Position.OffsetY = -20;
			VectorData.DescriptionData.Font.Name = "Arial";
			VectorData.DescriptionData.Font.Size = 9;
			VectorData.DescriptionData.Font.Style = EFontStyle.Italic;
			VectorData.DescriptionData.Font.Color = Color.FromArgb(INIT_DESCRIPTION_FIT_PEN_ALPHA,
																														 INIT_DESCRIPTION_FIT_PEN_RED,
																														 INIT_DESCRIPTION_FIT_PEN_GREEN,
																														 INIT_DESCRIPTION_FIT_PEN_BLUE);
			VectorData.DescriptionData.Pen.Width = 1;
			VectorData.DescriptionData.Pen.Style = EPenStyle.Solid;
			VectorData.DescriptionData.Pen.Color = Color.FromArgb(INIT_DESCRIPTION_FIT_PEN_ALPHA,
																														INIT_DESCRIPTION_FIT_PEN_RED,
																														INIT_DESCRIPTION_FIT_PEN_GREEN,
																														INIT_DESCRIPTION_FIT_PEN_BLUE);
			SetVectorData(FitID, VectorData);
			// Description - Fit 
			GetVectorData(FitID, out VectorData);
			VectorData.DescriptionData.Visible = true;
			VectorData.DescriptionData.ConnectionIndex = 47;
			VectorData.DescriptionData.Text = "Velocity";
			VectorData.DescriptionData.Position.OffsetX = 20;
			VectorData.DescriptionData.Position.OffsetY = -20;
			VectorData.DescriptionData.Font.Name = "Arial";
			VectorData.DescriptionData.Font.Size = 10;
			VectorData.DescriptionData.Font.Style = EFontStyle.Italic;
			VectorData.DescriptionData.Font.Color = Color.FromArgb(INIT_DESCRIPTION_FIT_FONT_ALPHA,
																														 INIT_DESCRIPTION_FIT_FONT_RED,
																														 INIT_DESCRIPTION_FIT_FONT_GREEN,
																														 INIT_DESCRIPTION_FIT_FONT_BLUE);
			VectorData.DescriptionData.Pen.Width = 1;
			VectorData.DescriptionData.Pen.Style = EPenStyle.Solid;
			VectorData.DescriptionData.Pen.Color = Color.FromArgb(INIT_DESCRIPTION_FIT_PEN_ALPHA,
																														INIT_DESCRIPTION_FIT_PEN_RED,
																														INIT_DESCRIPTION_FIT_PEN_GREEN,
																														INIT_DESCRIPTION_FIT_PEN_BLUE);
			SetVectorData(FitID, VectorData);
			if ((null != FFitX) && (null != FFitY))
      {	// Definition der Wertepaare
				for (Int32 PI = 0; PI < FFitX.Size; PI++)
        {	//String Line = String.Format("{0:000} {1:0.000} {2:0.000} {3:0.000}", PI, X, YT, YM);
          //Protocol(Line);
          RPointData PointData = new RPointData();
          PointData.PointFrame.Visible = true;
					PointData.X.Value = FFitX[PI];
					PointData.Y.Value = FFitY[PI];
					PointData.PointFrame.Pen.Color = Color.FromArgb(INIT_POINT_FIT_PEN_ALPHA,
																													INIT_POINT_FIT_PEN_RED,
																													INIT_POINT_FIT_PEN_GREEN,
																													INIT_POINT_FIT_PEN_BLUE);
          PointData.PointFrame.Pen.Width = 1;
          PointData.PointFrame.Pen.Style = EPenStyle.Solid;
					PointData.PointFrame.Brush.Color = Color.FromArgb(INIT_POINT_FIT_BRUSH_ALPHA,
																														INIT_POINT_FIT_BRUSH_RED,
																														INIT_POINT_FIT_BRUSH_GREEN,
																														INIT_POINT_FIT_BRUSH_BLUE);
          PointData.PointFrame.Brush.Style = EBrushStyle.Solid;
          //PointData.PointFrame.Shape = EPointShape.None;
          //PointData.PointFrame.Shape = EPointShape.Point;
          PointData.PointFrame.Shape = EPointShape.Circle;
          //PointData.PointFrame.Shape = EPointShape.Cross;
          //PointData.PointFrame.Shape = EPointShape.Square;
          //PointData.PointFrame.Shape = EPointShape.TriangleUp;
          //PointData.PointFrame.Shape = EPointShape.TriangleDown;
          //PointData.PointFrame.Shape = EPointShape.Rhomb;
          // *** Measurement Point - Error
          PointData.ErrorFrame.Visible = false;
          /*// Error - X
          PointData.X.ErrorMinus = 0.01 + 0.005 * Math.Abs(X) / 100;
          PointData.X.ErrorPlus = 0.01 + 0.005 * Math.Abs(X) / 100;
          // Error - Y
          PointData.Y.ErrorMinus = 0.15 + 0.2 * Math.Abs(YM) / 100;
          PointData.Y.ErrorPlus = 0.15 + 0.2 * Math.Abs(YM) / 100;
          PointData.ErrorFrame.Pen.Color = Color.FromArgb(INIT_ERROR_PRESET_PEN_ALPHA,
                                                          INIT_ERROR_PRESET_PEN_RED,
                                                          INIT_ERROR_PRESET_PEN_GREEN,
                                                          INIT_ERROR_PRESET_PEN_BLUE);
          PointData.ErrorFrame.Pen.Width = 1;
          PointData.ErrorFrame.Pen.Style = EPenStyle.Solid;
          PointData.ErrorFrame.Brush.Color = Color.FromArgb(INIT_ERROR_PRESET_BRUSH_ALPHA,
                                                            INIT_ERROR_PRESET_BRUSH_RED,
                                                            INIT_ERROR_PRESET_BRUSH_GREEN,
                                                            INIT_ERROR_PRESET_BRUSH_BLUE);
          PointData.ErrorFrame.Brush.Style = EBrushStyle.Solid;
          PointData.ErrorFrame.Shape = EErrorShape.None;
          //PointData.ErrorFrame.Shape = EErrorShape.Cross;
          PointData.ErrorFrame.Shape = EErrorShape.Bracket;
          //PointData.ErrorFrame.Shape = EErrorShape.Square;
          //PointData.ErrorFrame.Shape = EErrorShape.Rhomb;
          //PointData.ErrorFrame.Shape = EErrorShape.Ellipse;
           */
          //
          // Line
          PointData.LineFrame.Visible = true;
          PointData.LineFrame.Pen.Color = Color.FromArgb(INIT_LINE_PRESET_PEN_ALPHA,
                                                         INIT_LINE_PRESET_PEN_RED,
                                                         INIT_LINE_PRESET_PEN_GREEN,
                                                         INIT_LINE_PRESET_PEN_BLUE);
          PointData.LineFrame.Pen.Width = 3;
          PointData.LineFrame.Pen.Style = EPenStyle.Solid;
          PointData.LineFrame.Shape = ELineShape.Linear;
          //
          //	-> Registration of new Point
          Guid PointMID = CreateAddPoint(FitID, PointData);
        }
        //
				// ****************************************
				// *** Preset Y(X) - Erzeugung der Wertepaare 
				// ****************************************
				//
				Guid PresetID = CreateAddVector("Preset");
				// Description - Vector X
				GetVectorData(PresetID, out VectorData);
				VectorData.DescriptionData.Visible = true;
				VectorData.DescriptionData.ConnectionIndex = 1;
				VectorData.DescriptionData.Text = "Preset";
				VectorData.DescriptionData.Position.OffsetX = 20;
				VectorData.DescriptionData.Position.OffsetY = -20;
				VectorData.DescriptionData.Font.Name = "Arial";
				VectorData.DescriptionData.Font.Size = 9;
				VectorData.DescriptionData.Font.Style = EFontStyle.Italic;
				VectorData.DescriptionData.Font.Color = Color.FromArgb(INIT_DESCRIPTION_PRESET_PEN_ALPHA,
																															 INIT_DESCRIPTION_PRESET_PEN_RED,
																															 INIT_DESCRIPTION_PRESET_PEN_GREEN,
																															 INIT_DESCRIPTION_PRESET_PEN_BLUE);
				VectorData.DescriptionData.Pen.Width = 1;
				VectorData.DescriptionData.Pen.Style = EPenStyle.Solid;
				VectorData.DescriptionData.Pen.Color = Color.FromArgb(INIT_DESCRIPTION_PRESET_PEN_ALPHA,
																															INIT_DESCRIPTION_PRESET_PEN_RED,
																															INIT_DESCRIPTION_PRESET_PEN_GREEN,
																															INIT_DESCRIPTION_PRESET_PEN_BLUE);
				SetVectorData(PresetID, VectorData);
				// Description - Vector V
				GetVectorData(PresetID, out VectorData);
				VectorData.DescriptionData.Visible = true;
				VectorData.DescriptionData.ConnectionIndex = 47;
				VectorData.DescriptionData.Text = "Velocity";
				VectorData.DescriptionData.Position.OffsetX = 20;
				VectorData.DescriptionData.Position.OffsetY = -20;
				VectorData.DescriptionData.Font.Name = "Arial";
				VectorData.DescriptionData.Font.Size = 10;
				VectorData.DescriptionData.Font.Style = EFontStyle.Italic;
				VectorData.DescriptionData.Font.Color = Color.FromArgb(INIT_DESCRIPTION_PRESET_FONT_ALPHA,
																															 INIT_DESCRIPTION_PRESET_FONT_RED,
																															 INIT_DESCRIPTION_PRESET_FONT_GREEN,
																															 INIT_DESCRIPTION_PRESET_FONT_BLUE);
				VectorData.DescriptionData.Pen.Width = 1;
				VectorData.DescriptionData.Pen.Style = EPenStyle.Solid;
				VectorData.DescriptionData.Pen.Color = Color.FromArgb(INIT_DESCRIPTION_PRESET_PEN_ALPHA,
																															INIT_DESCRIPTION_PRESET_PEN_RED,
																															INIT_DESCRIPTION_PRESET_PEN_GREEN,
																															INIT_DESCRIPTION_PRESET_PEN_BLUE);
				SetVectorData(PresetID, VectorData);
				//
				for (Int32 PI = 0; PI < FPresetX.Size; PI++)
				{	//String Line = String.Format("{0:000} {1:0.000} {2:0.000} {3:0.000}", PI, X, YT, YM);
					//Protocol(Line);
					RPointData PointData = new RPointData();
					// *****************************
					// *** Preset Y(X)
					// *****************************
					PointData.PointFrame.Visible = true;
					PointData.X.Value = FPresetX[PI];
					PointData.Y.Value = FPresetY[PI];
					PointData.PointFrame.Pen.Color = Color.FromArgb(INIT_POINT_PRESET_PEN_ALPHA,
																													INIT_POINT_PRESET_PEN_RED,
																													INIT_POINT_PRESET_PEN_GREEN,
																													INIT_POINT_PRESET_PEN_BLUE);
					PointData.PointFrame.Pen.Width = 1;
					PointData.PointFrame.Pen.Style = EPenStyle.Solid;
					PointData.PointFrame.Brush.Color = Color.FromArgb(INIT_POINT_PRESET_BRUSH_ALPHA,
																														INIT_POINT_PRESET_BRUSH_RED,
																														INIT_POINT_PRESET_BRUSH_GREEN,
																														INIT_POINT_PRESET_BRUSH_BLUE);
					PointData.PointFrame.Brush.Style = EBrushStyle.Solid;
					//PointData.PointFrame.Shape = EPointShape.None;
					//PointData.PointFrame.Shape = EPointShape.Point;
					PointData.PointFrame.Shape = EPointShape.Circle;
					//PointData.PointFrame.Shape = EPointShape.Cross;
					//PointData.PointFrame.Shape = EPointShape.Square;
					//PointData.PointFrame.Shape = EPointShape.TriangleUp;
					//PointData.PointFrame.Shape = EPointShape.TriangleDown;
					//PointData.PointFrame.Shape = EPointShape.Rhomb;
					// *** Measurement Point - Error
					PointData.ErrorFrame.Visible = false;
					/*// Error - X
					PointData.X.ErrorMinus = 0.01 + 0.005 * Math.Abs(X) / 100;
					PointData.X.ErrorPlus = 0.01 + 0.005 * Math.Abs(X) / 100;
					// Error - Y
					PointData.Y.ErrorMinus = 0.15 + 0.2 * Math.Abs(YM) / 100;
					PointData.Y.ErrorPlus = 0.15 + 0.2 * Math.Abs(YM) / 100;
					PointData.ErrorFrame.Pen.Color = Color.FromArgb(INIT_ERROR_PRESET_PEN_ALPHA,
																													INIT_ERROR_PRESET_PEN_RED,
																													INIT_ERROR_PRESET_PEN_GREEN,
																													INIT_ERROR_PRESET_PEN_BLUE);
					PointData.ErrorFrame.Pen.Width = 1;
					PointData.ErrorFrame.Pen.Style = EPenStyle.Solid;
					PointData.ErrorFrame.Brush.Color = Color.FromArgb(INIT_ERROR_PRESET_BRUSH_ALPHA,
																														INIT_ERROR_PRESET_BRUSH_RED,
																														INIT_ERROR_PRESET_BRUSH_GREEN,
																														INIT_ERROR_PRESET_BRUSH_BLUE);
					PointData.ErrorFrame.Brush.Style = EBrushStyle.Solid;
					PointData.ErrorFrame.Shape = EErrorShape.None;
					//PointData.ErrorFrame.Shape = EErrorShape.Cross;
					PointData.ErrorFrame.Shape = EErrorShape.Bracket;
					//PointData.ErrorFrame.Shape = EErrorShape.Square;
					//PointData.ErrorFrame.Shape = EErrorShape.Rhomb;
					//PointData.ErrorFrame.Shape = EErrorShape.Ellipse;
					 */
					//
					// Line
					PointData.LineFrame.Visible = (PI < FPresetX.Size - 1);
					PointData.LineFrame.Pen.Color = Color.FromArgb(INIT_LINE_PRESET_PEN_ALPHA,
																												 INIT_LINE_PRESET_PEN_RED,
																												 INIT_LINE_PRESET_PEN_GREEN,
																												 INIT_LINE_PRESET_PEN_BLUE);
					PointData.LineFrame.Pen.Width = 1;
					PointData.LineFrame.Pen.Style = EPenStyle.Solid;
					PointData.LineFrame.Shape = ELineShape.Linear;
					//
					//	-> Registration of new Point
					Guid PointMID = CreateAddPoint(PresetID, PointData);
				}
      }
      //
			// !!! erst nach der Definition der Wertepaare !!!
			// **********************
			// *** LegendToolData ***
			// **********************
			/*RLegendToolData LegendToolData = new RLegendToolData();
			GetLegendToolData(out LegendToolData);
			// Frame
			LegendToolData.Pen.Color = Color.FromArgb(INIT_LEGEND_PEN_ALPHA,
																								INIT_LEGEND_PEN_RED,
																								INIT_LEGEND_PEN_GREEN,
																								INIT_LEGEND_PEN_BLUE);
			LegendToolData.Pen.Style = EPenStyle.Solid;
			LegendToolData.Pen.Width = 2;
			// Background
			LegendToolData.Brush.Color = Color.FromArgb(INIT_LEGEND_BRUSH_ALPHA,
																									INIT_LEGEND_BRUSH_RED,
																									INIT_LEGEND_BRUSH_GREEN,
																									INIT_LEGEND_BRUSH_BLUE);
			LegendToolData.Brush.Style = EBrushStyle.Solid;
			// Frame
			LegendToolData.PercentX = 22; // OK
			LegendToolData.PercentY = 83; // OK
			LegendToolData.Position.OffsetX = 50; // OK
			LegendToolData.Position.OffsetY = 0; // OK
			// Title
			LegendToolData.Title.Visible = false;//true;
			LegendToolData.Title.Text = "Legend of Datalines";
			LegendToolData.Title.Position.OffsetX = 8; // OK bei Font-Size 10 hier 8 eingeben!!!
			LegendToolData.Title.Position.OffsetY = 0; // OK
			LegendToolData.Title.Font.Name = "Arial";
			LegendToolData.Title.Font.Size = 10;
			LegendToolData.Title.Font.Style = EFontStyle.Standard;
			LegendToolData.Title.Font.Color = Color.FromArgb(INIT_LEGEND_TITLE_FONT_ALPHA,
																											 INIT_LEGEND_TITLE_FONT_RED,
																											 INIT_LEGEND_TITLE_FONT_GREEN,
																											 INIT_LEGEND_TITLE_FONT_BLUE);			
			SetLegendToolData(LegendToolData);
			// Alle Vektoren automatisch in Legende integrieren...
			IntegrateAllVectorsToLegend();
       */
			// ***********************
			// *** CommentToolData ***
			// ***********************
			//
			/*RCommentData CommentData = new RCommentData();
			//
			// *** 1st Comment
			//
			// Comment - Placement
			CommentData.PercentX = 68; 
			CommentData.PercentY = 14; 
			// Comment - Frame
			CommentData.Pen.Color = Color.FromArgb(INIT_COMMENTA_PEN_ALPHA,
																						 INIT_COMMENTA_PEN_RED,
																						 INIT_COMMENTA_PEN_GREEN,
																						 INIT_COMMENTA_PEN_BLUE);
			CommentData.Pen.Style = EPenStyle.Dot;
			CommentData.Pen.Width = 1;
			// Comment - Background
			CommentData.Brush.Color = Color.FromArgb(INIT_COMMENTA_BRUSH_ALPHA,
																							 INIT_COMMENTA_BRUSH_RED,
																							 INIT_COMMENTA_BRUSH_GREEN,
																							 INIT_COMMENTA_BRUSH_BLUE);
			CommentData.Brush.Style = EBrushStyle.Solid;
			// Comment - Label 
			CommentData.Label.Visible = true;
			CommentData.Label.Text = "All the data based on\r\n" +
															 "experimental results";
			CommentData.Label.Font.Name = "Arial";
			CommentData.Label.Font.Size = 9;
			CommentData.Label.Font.Style = EFontStyle.Standard;
			CommentData.Label.Font.Color = Color.FromArgb(INIT_COMMENTA_LABEL_ALPHA,
																										INIT_COMMENTA_LABEL_RED,	
																										INIT_COMMENTA_LABEL_GREEN,
																										INIT_COMMENTA_LABEL_BLUE);
			Guid CommentAID = CreateAddComment(CommentData);
			//
			//	2nd Comment
			//
			// Comment - Placement
			CommentData.PercentX = 68;
			CommentData.PercentY = 20;
			// Comment - Frame
			CommentData.Pen.Color = Color.FromArgb(INIT_COMMENTB_PEN_ALPHA,
																						 INIT_COMMENTB_PEN_RED,
																						 INIT_COMMENTB_PEN_GREEN,
																						 INIT_COMMENTB_PEN_BLUE);
			CommentData.Pen.Style = EPenStyle.Dash;
			CommentData.Pen.Width = 1;
			// Comment - Background
			CommentData.Brush.Color = Color.FromArgb(INIT_COMMENTB_BRUSH_ALPHA,
																							 INIT_COMMENTB_BRUSH_RED,
																							 INIT_COMMENTB_BRUSH_GREEN,
																							 INIT_COMMENTB_BRUSH_BLUE);
			CommentData.Brush.Style = EBrushStyle.Solid;
			// Comment - Label 
			CommentData.Label.Visible = true;
			CommentData.Label.Position.OffsetX = 3;
			//CommentData.Label.Position.OffsetY = 10;
			CommentData.Label.Text = "(C) - All rights reserved";
			CommentData.Label.Font.Name = "Arial";
			CommentData.Label.Font.Size = 7;
			CommentData.Label.Font.Style = EFontStyle.Standard;
			CommentData.Label.Font.Color = Color.FromArgb(INIT_COMMENTB_LABEL_ALPHA,
																										INIT_COMMENTB_LABEL_RED,
																										INIT_COMMENTB_LABEL_GREEN,
																										INIT_COMMENTB_LABEL_BLUE);
			Guid CommentBID = CreateAddComment(CommentData);
      */
			//    
			//     *** AutoplotToolData ***
			//    
			/*RAutoplotToolData AutoplotToolData = new RAutoplotToolData();
			GetAutoplotToolData(FPlot2D.ID, out AutoplotToolData);
			AutoplotToolData.XScale = 0.1;
			AutoplotToolData.YScale = 0.1;
			AutoplotToolData.Start = 0;		// Alle Werte in Autoplot einbeziehen!
			AutoplotToolData.Range = 0;		// Alle Werte in Autoplot einbeziehen!
			AutoplotToolData.XActive = true;
			AutoplotToolData.YActive = true;
			AutoplotToolData.XGridMainFactor = 5;
			AutoplotToolData.XGridSubFactor = 2;
			AutoplotToolData.YGridMainFactor = 5;
			AutoplotToolData.YGridSubFactor = 2;
			SetAutoplotToolData(FPlot2D.ID, ref AutoplotToolData);
			 */
			//
			Unlock();

		}

	}


}

















