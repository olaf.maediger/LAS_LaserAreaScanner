﻿namespace UCPlotSpectrometerManager
{
  partial class CUCPlotSpectrometerManager
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlRight = new System.Windows.Forms.Panel();
      this.SuspendLayout();
      // 
      // pnlRight
      // 
      this.pnlRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))));
      this.pnlRight.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnlRight.Location = new System.Drawing.Point(578, 0);
      this.pnlRight.Name = "pnlRight";
      this.pnlRight.Size = new System.Drawing.Size(3, 516);
      this.pnlRight.TabIndex = 1;
      // 
      // CUCPlotSpectrometerManager
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlRight);
      this.Name = "CUCPlotSpectrometerManager";
      this.Size = new System.Drawing.Size(581, 516);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlRight;
  }
}
