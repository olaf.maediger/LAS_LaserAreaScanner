﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using SpectrometerBase;
using SHAvantes;
using SHOceanOptics;
//
namespace SpectrometerManager
{
  public delegate void DOnMeasurementBegin(String namespectrometer);
  public delegate void DOnMeasurementEnd(String namespectrometer);
  public delegate void DOnSpectrumMeasured(Double[] wavelengths, Double[] spectrum);

  public class CThreadSpectrometer
  {
    // old public const String INIT_HARDWARE_AVANTES = "AvantesAvaSpec2048x14";
    public const String INIT_HARDWARE_AVANTES = "AvantesAvaSpec-2048";
    public const String INIT_HARDWARE_OCEANOPTICS = "OceanOpticsUsb2000PlusUv";
    public const Double INIT_FACTORSCALE = 1.0;
    //
    private String FNameSpectrometer;
    private CSpectrometerBase FSpectrometer;    
    private Thread FThread;
    private DOnMeasurementBegin FOnMeasurementBegin;
    private DOnMeasurementEnd FOnMeasurementEnd;
    private DOnSpectrumMeasured FOnSpectrumMeasured;
    private Boolean FMeasurementActive;
    private Double FFactorScale;
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------------
    //
    public CThreadSpectrometer(String namespectrometer)
    {
      FNameSpectrometer = namespectrometer;
      FMeasurementActive = false;
      FFactorScale = INIT_FACTORSCALE;
    }
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------------
    //
    public void SetOnMeasurementBegin(DOnMeasurementBegin value)
    {
      FOnMeasurementBegin = value;
    }
    public void SetOnMeasurementEnd(DOnMeasurementEnd value)
    {
      FOnMeasurementEnd = value;
    }
    public void SetOnSpectrumMeasured(DOnSpectrumMeasured value)
    {
      FOnSpectrumMeasured = value;
    }

    public String NameSpectrometer
    {
      get { return FNameSpectrometer; }
    }

    public Int32 GetIntensityMaximum()
    {
      return FSpectrometer.GetIntensityMaximum();
    }

    public void SetIntegrationTime(Int32 value)
    {
      FSpectrometer.SetIntegrationTime(value);
    }

    public void SetInternalTrigger()
    {
      FSpectrometer.SetInternalTrigger();
    }

    public void SetExternalTrigger()
    {
      FSpectrometer.SetExternalTrigger();
    }

    public Double FactorScale
    {
      get { return FFactorScale; }
    }
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Public Management - Spectrometer
    //------------------------------------------------------------------------------------------
    //
    public Boolean OpenSpectrometer()
    {
      if (INIT_HARDWARE_AVANTES == FNameSpectrometer)
      {
        FFactorScale = 100.0;
        if (!(FSpectrometer is CAvantes))
        {
          FSpectrometer = new CAvantes();
        }
      }
      else
        if (INIT_HARDWARE_OCEANOPTICS == FNameSpectrometer)
        {
          FFactorScale = 20.0;
          if (!(FSpectrometer is COceanOptics))
          {
            FSpectrometer = new COceanOptics();
          }
        }
      if (FSpectrometer is CSpectrometerBase)
      {
        Boolean BResult = FSpectrometer.InitiateSpectrometer();
        return BResult;
      }
      return false;
    }

    public Boolean CloseSpectrometer()
    {
      FMeasurementActive = false;
      if (FSpectrometer is CSpectrometerBase)
      {
        return FSpectrometer.CloseSpectrometer();
      }
      return false;
    }


    public Boolean StartMeasurement()
    {
     //
      if (FSpectrometer is CSpectrometerBase)
      {
        FThread = new Thread(OnMeasurementActive);
        FThread.Start();
        return true;
      }
      return false;
    }

    public Boolean StopMeasurement()
    {
      try
      {
        FMeasurementActive = false;
//        FThread = null;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Callback - Thread
    //------------------------------------------------------------------------------------------
    //
    private void OnMeasurementActive()
    {
#if NOTIFY
      Console.WriteLine("ThreadSpectrometer: OnMeasurementActive - Start");
#endif
      if (FOnMeasurementBegin is DOnMeasurementBegin)
      {
        FOnMeasurementBegin(FNameSpectrometer);
      }
#if NOTIFY
      Console.WriteLine("ThreadSpectrometer: OnMeasurementActive - Active");
#endif
      FMeasurementActive = true;
      while (FMeasurementActive)
      {
#if NOTIFY
        Console.WriteLine("ThreadSpectrometer: OnMeasurementActive - GetSpectrum");
#endif
        Double[] Spectrum = FSpectrometer.GetSpectrum();
        if (Spectrum is Double[])
        {
          if (FOnSpectrumMeasured is DOnSpectrumMeasured)
          {
            FOnSpectrumMeasured(FSpectrometer.Wavelengths, Spectrum);
          }
        }
      }
#if NOTIFY
      Console.WriteLine("ThreadSpectrometer: OnMeasurementActive - End");
#endif
      if (FOnMeasurementEnd is DOnMeasurementEnd)
      {
        FOnMeasurementEnd(FNameSpectrometer);
      }
    }



  }
}
