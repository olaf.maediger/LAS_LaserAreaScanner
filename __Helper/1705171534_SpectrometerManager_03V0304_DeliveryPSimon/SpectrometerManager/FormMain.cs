﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
//
using Initdata;
using UCNotifier;
using NLVector;
//
using SpectrometerBase;
using SHAvantes;
using SHOceanOptics;
//
namespace SpectrometerManager
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //  NAME - Global
    private const String NAME_AUTOMATE_VISIBLE = "AutomateVisible";
    private const String NAME_AUTOMATE_ACTIVE = "AutomateActive";
    //  NAME - Spectrometer
    private const String NAME_SPECTROMETERHARDWARE = "SpectrometerHardware";
    private const String NAME_INTEGRATIONTIME_MS = "IntegrationTime_ms";
    private const String NAME_INTERVALMEASUREMENT_MS = "IntervalMeasurement_ms";
    //  INIT - Global
    private const Boolean INIT_AUTOMATE_VISIBLE = false;
    private const Boolean INIT_AUTOMATE_ACTIVE = false;
    //  INIT - Spectrometer
    private const Int32 INIT_INTEGRATIONTIME_MS = 10;
    private const Int32 INIT_INTERVALMEASUREMENT_MS = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;
    //
    private CThreadSpectrometer FThreadSpectrometer; 
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      // FREEWARE:      tbcMain.Controls.Remove(tbpSerialNumberProductKey);
      //
      FUCPlotSpectrometerManager.SetNotifier(FUCNotifier);
      //
      tmrStartup.Interval = 1000;
      tmrStartup.Stop();
      FRandom = new Random();
      //
      cbxAutomate.Visible = false;
      EnableControlsMeasurement(false);
      //
      FUCNotifier.Write("*** Create Spectrometer");
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      if (FThreadSpectrometer is CThreadSpectrometer)
      {
        FThreadSpectrometer.StopMeasurement();
        FThreadSpectrometer = null;
      }
    }


    private Int32 GetControlIntegrationTime()
    {
      if (rbtIntegrationTime1p0.Checked)
      {
        return 1;
      }
      else
        if (rbtIntegrationTime10p0.Checked)
        {
          return 10;
        }
        else
          if (rbtIntegrationTime100p0.Checked)
          {
            return 100;
          }
          else
            if (rbtIntegrationTime200p0.Checked)
            {
              return 1000;
            }
      rbtIntegrationTime1p0.Checked = true;
      return 10;
    }

    private void SetControlIntegrationTime(Int32 value)
    {
      if (1 == value)
      {
        if (rbtIntegrationTime1p0.Enabled)
        {
          rbtIntegrationTime10p0.Checked = true;
          rbtIntegrationTime1p0.Checked = true;
        }
        else
        {
          rbtIntegrationTime100p0.Checked = true;
          rbtIntegrationTime10p0.Checked = true;
        }
      }
      else
        if (10 == value)
        {
          rbtIntegrationTime100p0.Checked = true;
          rbtIntegrationTime10p0.Checked = true;
        }
        else
          if (100 == value)
          {
            rbtIntegrationTime10p0.Checked = true;
            rbtIntegrationTime100p0.Checked = true;
          }
          else
            if (1000 == value)
            {
              rbtIntegrationTime100p0.Checked = true;
              rbtIntegrationTime200p0.Checked = true;
            }
            else
            {
              rbtIntegrationTime100p0.Checked = true;
              rbtIntegrationTime10p0.Checked = true;
            }
    }

    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      Boolean BValue;
      Int32 IValue;
      String SValue;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //      
      // Spectrometer
      SValue = CThreadSpectrometer.INIT_HARDWARE_OCEANOPTICS; // CThreadSpectrometer.INIT_HARDWARE_AVANTES
      Result &= initdata.ReadString(NAME_SPECTROMETERHARDWARE, out SValue, SValue);
      // 
      FThreadSpectrometer = null;
      //
      IValue = INIT_INTEGRATIONTIME_MS;
      Result &= initdata.ReadInt32(NAME_INTEGRATIONTIME_MS, out IValue, IValue);
      SetControlIntegrationTime(IValue);
      //
      BValue = INIT_AUTOMATE_VISIBLE;
      Result &= initdata.ReadBoolean(NAME_AUTOMATE_VISIBLE, out BValue, BValue);
      cbxAutomate.Visible = BValue;
      //
      BValue = INIT_AUTOMATE_ACTIVE;
      Result &= initdata.ReadBoolean(NAME_AUTOMATE_ACTIVE, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      // Spectrometer      
      if (rbtSpectrometerAvantes.Checked)
      {
        Result &= initdata.WriteString(NAME_SPECTROMETERHARDWARE,
                                       CThreadSpectrometer.INIT_HARDWARE_AVANTES);
      }
      else
      {
        Result &= initdata.WriteString(NAME_SPECTROMETERHARDWARE,
                                       CThreadSpectrometer.INIT_HARDWARE_OCEANOPTICS);
        rbtSpectrometerOceanOptics.Checked = true;
      }
      //
      Int32 IT = GetControlIntegrationTime();
      Result &= initdata.WriteInt32(NAME_INTEGRATIONTIME_MS, IT);
      //
      Result &= initdata.WriteBoolean(NAME_AUTOMATE_VISIBLE, cbxAutomate.Visible);
      Result &= initdata.WriteBoolean(NAME_AUTOMATE_ACTIVE, cbxAutomate.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - Spectrometer
    //------------------------------------------------------------------------
    // 
    private delegate void CBOnMeasurementBegin(String namespectrometer);
    public void OnMeasurementBegin(String namespectrometer)
    {
      if (this.InvokeRequired)
      {
        CBOnMeasurementBegin CB = new CBOnMeasurementBegin(OnMeasurementBegin);
        Invoke(CB, new object[] { namespectrometer });
      }
      else
      {
        FUCNotifier.Write("Spectrometer.OnMeasurementBegin");
        Int32 IM = FThreadSpectrometer.GetIntensityMaximum();
        if (CThreadSpectrometer.INIT_HARDWARE_AVANTES == namespectrometer)
        {
          rbtSpectrometerAvantes.Checked = true;
          rbtIntegrationTime1p0.Enabled = false;
          //FUCPlotSpectrometerManager.SetTitle("Spectrometer " + CThreadSpectrometer.INIT_HARDWARE_AVANTES);
          FUCPlotSpectrometerManager.SetTitle("Spectrometer " + FThreadSpectrometer.NameSpectrometer);
          FUCPlotSpectrometerManager.SetRealExtrema(200, 500, 0, IM);
        }
        else
        {
          rbtSpectrometerOceanOptics.Checked = true;
          rbtIntegrationTime1p0.Enabled = true;
          //FUCPlotSpectrometerManager.SetTitle("Spectrometer " + CThreadSpectrometer.INIT_HARDWARE_OCEANOPTICS);
          FUCPlotSpectrometerManager.SetTitle("Spectrometer " + FThreadSpectrometer.NameSpectrometer);
          FUCPlotSpectrometerManager.SetRealExtrema(600, 1000, 0, IM);
        }
      }
    }    

    private delegate void CBOnMeasurementEnd(String namespectrometer);
    public void OnMeasurementEnd(String namespectrometer)
    {
      if (this.InvokeRequired)
      {
        CBOnMeasurementEnd CB = new CBOnMeasurementEnd(OnMeasurementEnd);
        Invoke(CB, new object[] { namespectrometer });
      }
      else
      { // nothing
        FUCNotifier.Write("Spectrometer.OnMeasurementEnd");
      }
    }

    private delegate void CBOnSpectrumMeasured(Double[] wavelengths, Double[] spectrum);
    public void OnSpectrumMeasured(Double[] wavelengths, Double[] spectrum)
    {
      if (this.InvokeRequired)
      {
        CBOnSpectrumMeasured CB = new CBOnSpectrumMeasured(OnSpectrumMeasured);
        Invoke(CB, new object[] { wavelengths, spectrum });
      }
      else
      { // debug FUCNotifier.Write("Spectrometer.OnSpectrumMeasured");
        FUCPlotSpectrometerManager.SetValues(wavelengths, spectrum, FThreadSpectrometer.FactorScale);
      }
    }
    //
    //--------------------------------------------------
    //  Segment - Helper - 
    //--------------------------------------------------
    //
    private void EnableControlsMeasurement(Boolean enable)
    {
      rbtSpectrometerOceanOptics.Enabled = !enable;
      rbtSpectrometerAvantes.Enabled = !enable;
      //
      gbxIntegrationTimeMs.Enabled = enable;
      gbxMeasurement.Enabled = enable;
    }
    //
    //--------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }

    //
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    //--------------------------------------------
    //############################################
    //--------------------------------------------
    //  Menu-Function - ...
    //--------------------------------------------
    //############################################
    //--------------------------------------------
    // 

    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      FUCNotifier.Write(String.Format("StartupState: {0}", FStartupState));
      switch (FStartupState)
      {
        case 0:
          return false;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        default:
          return false;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event - Management
    //------------------------------------------------------------------------
    //
    private void cbxOpenSpectrometer_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxOpenSpectrometer.Checked)
      { // -> Open Spectrometer
        Boolean BResult = false;
        if (rbtSpectrometerAvantes.Checked)
        {
          if (!(FThreadSpectrometer is CThreadSpectrometer))
          {
            FThreadSpectrometer = new CThreadSpectrometer(CThreadSpectrometer.INIT_HARDWARE_AVANTES);
          } 
            else
            if (CThreadSpectrometer.INIT_HARDWARE_AVANTES != FThreadSpectrometer.NameSpectrometer)
            {
              FThreadSpectrometer = new CThreadSpectrometer(CThreadSpectrometer.INIT_HARDWARE_AVANTES);
            }
          BResult = (CThreadSpectrometer.INIT_HARDWARE_AVANTES == FThreadSpectrometer.NameSpectrometer);
        }
        else // -> Close Spectrometer
          if (rbtSpectrometerOceanOptics.Checked)
          {
            if (!(FThreadSpectrometer is CThreadSpectrometer))
            {
              FThreadSpectrometer = new CThreadSpectrometer(CThreadSpectrometer.INIT_HARDWARE_OCEANOPTICS);
            } 
            if (CThreadSpectrometer.INIT_HARDWARE_OCEANOPTICS != FThreadSpectrometer.NameSpectrometer)
            {
              FThreadSpectrometer = new CThreadSpectrometer(CThreadSpectrometer.INIT_HARDWARE_OCEANOPTICS);
            }
            BResult = (CThreadSpectrometer.INIT_HARDWARE_OCEANOPTICS == FThreadSpectrometer.NameSpectrometer);
          }
        if (BResult)
        {
          FThreadSpectrometer.SetOnMeasurementBegin(OnMeasurementBegin);
          FThreadSpectrometer.SetOnMeasurementEnd(OnMeasurementEnd);
          FThreadSpectrometer.SetOnSpectrumMeasured(OnSpectrumMeasured);
          FThreadSpectrometer.OpenSpectrometer();
          FThreadSpectrometer.SetIntegrationTime(GetControlIntegrationTime());
          //
          EnableControlsMeasurement(true);
          cbxOpenSpectrometer.Text = "Close Spectrometer";
          FUCNotifier.Write("*** Open Spectrometer");
        }
        else
        {
          FUCNotifier.Write("Error: Open Spectrometer failed!");
        }
      }
      else
      {
        FThreadSpectrometer.CloseSpectrometer();
        cbxStartMeasurement.Checked = false;
        EnableControlsMeasurement(false);
        cbxOpenSpectrometer.Text = "Open Spectrometer";
        FUCNotifier.Write("*** Close Spectrometer");
      }
    }

    private void rbtIntegrationTime1p0_CheckedChanged(object sender, EventArgs e)
    {
      if (rbtIntegrationTime1p0.Checked)
      {
        if (FThreadSpectrometer is CThreadSpectrometer)
        {
          FThreadSpectrometer.SetIntegrationTime(1);
        }
      }
    }

    private void rbtIntegrationTime10p0_CheckedChanged(object sender, EventArgs e)
    {
      if (rbtIntegrationTime10p0.Checked)
      {
        if (FThreadSpectrometer is CThreadSpectrometer)
        {
          FThreadSpectrometer.SetIntegrationTime(10);
        }
      }
    }

    private void rbtIntegrationTime100p0_CheckedChanged(object sender, EventArgs e)
    {
      if (rbtIntegrationTime100p0.Checked)
      {
        if (FThreadSpectrometer is CThreadSpectrometer)
        {
          FThreadSpectrometer.SetIntegrationTime(100);
        }
      }
    }

    private void rbtIntegrationTime200p0_CheckedChanged(object sender, EventArgs e)
    {
      if (rbtIntegrationTime200p0.Checked)
      {
        if (FThreadSpectrometer is CThreadSpectrometer)
        {
          FThreadSpectrometer.SetIntegrationTime(200);
        }
      }
    }

    private void cbxStartMeasurement_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxStartMeasurement.Checked)
      {
        cbxStartMeasurement.Text = "Stop Measurement";
        FUCNotifier.Write("*** Start Measurement");
        FThreadSpectrometer.StartMeasurement();
      }
      else
      {
        FThreadSpectrometer.StopMeasurement();
        FUCNotifier.Write("*** Stop Measurement");
        cbxStartMeasurement.Text = "Start Measurement";
      }
    }



  }
}

