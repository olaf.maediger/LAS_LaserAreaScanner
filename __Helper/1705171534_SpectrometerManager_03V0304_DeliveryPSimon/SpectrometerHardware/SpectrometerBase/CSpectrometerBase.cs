﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpectrometerBase
{
  public enum ETriggerType
  {
    Internal = 0,
    External = 1,
    Synchronous = 2
  }

  public abstract class CSpectrometerBase
  {
    //
    //-----------------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------------
    //
    protected Int32 FNumberOfSpectrometers;
    protected String FSpectrometerName;
    protected Int32 FIntegrationTime;
    protected Int32 FPixelNumber;
    protected Double[] FWavelengths = null;
    protected ETriggerType FTriggerType;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------------------
    //
    public Int32 NumberOfSpectrometers
    {
      get { return FNumberOfSpectrometers; }
      set { FNumberOfSpectrometers = value; }
    }

    public String SpectrometerName
    {
      get { return FSpectrometerName; }
      set { FSpectrometerName = value; }
    }

    public ETriggerType TriggerType
    {
      get { return FTriggerType; }
      set { FTriggerType = value; }
    }

    public abstract void SetIntegrationTime(Int32 integrationtime);
    public Int32 IntegrationTime
    {
      get { return FIntegrationTime; }
    }

    public Int32 PixelNumber
    {
      get { return FPixelNumber; }
      set { FPixelNumber = value; }
    }

    public abstract void SetInternalTrigger();
    public abstract void SetExternalTrigger();
    public abstract void SetSynchronousTrigger();

    public Double[] Wavelengths
    {
      get { return FWavelengths; }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Public Management
    //-----------------------------------------------------------------------------
    // Management
    public abstract Boolean InitiateSpectrometer();
    public abstract Boolean CloseSpectrometer();
    // Measurement
    public abstract Double[] GetSpectrum();
    public abstract Int32 GetIntensityMaximum();
  }
}
