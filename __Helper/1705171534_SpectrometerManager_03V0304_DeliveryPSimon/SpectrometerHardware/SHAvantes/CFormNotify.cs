﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace SHAvantes
{
  public delegate void DOnMeasurementReady();

  public partial class CFormNotify : Form
  {
    private DOnMeasurementReady FOnMeasurementReady;


    public CFormNotify()
    {
      InitializeComponent();
    }

    public void SetOnMeasurementReady(DOnMeasurementReady value)
    {
      FOnMeasurementReady = value;
    }

    protected override void WndProc(ref Message message)
    {
      const int WM_USER = 0x8000;
      const int WM_MEASUREMENT_READY = WM_USER + 1;
      //
      if (WM_MEASUREMENT_READY == message.Msg)
      {
#if NOTIFY
        Console.WriteLine("FormNotifyWndProc: Measurement Ready!");
#endif
        if (CAvantesInterface.ERR_SUCCESS <= (int)message.WParam)
        {
          if (FOnMeasurementReady is DOnMeasurementReady)
          {
            FOnMeasurementReady();
          }
        }
        else
        {
#if NOTIFY
          Console.WriteLine("Error: FormNotifyWndProc Measurement failed!");
#endif
        }
      }
      else
      {
        base.WndProc(ref message);
      }
    }



  }
}
