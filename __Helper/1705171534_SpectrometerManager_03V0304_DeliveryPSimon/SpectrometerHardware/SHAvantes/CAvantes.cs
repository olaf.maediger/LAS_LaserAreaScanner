﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
//
using SpectrometerBase;
//
namespace SHAvantes
{
  public class CAvantes : CSpectrometerBase
  { //
    //------------------------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------------------------
    //
    private const String INIT_SPECTROMETERNAME = "Avantes AvaSpec-2048";
    //
    private const ETriggerType INIT_TRIGGERTYPE = ETriggerType.Internal;
    //
    private const Int32 INIT_PIXELNUMBER = 2048;
    //
    private const Int32 INIT_PIXELRESOLUTION = 14; // bit
    private const Int32 INIT_INTENSITYRANGE = 1024 * 16; // 2^(10bit + 4bit)
    private const Int32 INIT_INTENSITY_MAXIMUM = INIT_INTENSITYRANGE - 1;
    //
    private const Int32 INIT_INTEGRATIONTIME = 10; // [ms]
    //
    private const short INIT_SCAN_SINGLE = +1;
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Type
    //------------------------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------------------------
    //
    // Spectrometer-Hardware
    private Int32 FPort = -1;
    private IntPtr FHandleDevice;
    private Boolean FIsOpened = false;
    private ManualResetEvent FEventMeasurement;
    private Thread FThreadMeasurement;
    private Boolean FResultMeasurement;
    private CFormNotify FFormNotify;
    private IntPtr FHandleFormNotify;
    private CAvantesInterface.MeasConfigType FMCT;
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------------
    //
    public CAvantes()
      : base()
    {
      FSpectrometerName = INIT_SPECTROMETERNAME;
      FTriggerType = INIT_TRIGGERTYPE;
      SetIntegrationTime(INIT_INTEGRATIONTIME);
      FPixelNumber = INIT_PIXELNUMBER;
      FIsOpened = false;
      FEventMeasurement = new ManualResetEvent(false);
      FThreadMeasurement = null;
      FFormNotify = new CFormNotify();
      FFormNotify.Hide();
      FFormNotify.SetOnMeasurementReady(FormNotifyOnMeasurementReady);
      FHandleFormNotify = FFormNotify.Handle;
      //
      FMCT = new CAvantesInterface.MeasConfigType();
      FMCT.m_StartPixel = 0;
      FMCT.m_StopPixel = INIT_PIXELNUMBER - 1;
      FMCT.m_IntegrationTime = FIntegrationTime; // ms
      FMCT.m_IntegrationDelay = 0;
      //
      // MODE
      // SW_TRIGGER_MODE = 0;
      // HW_TRIGGER_MODE = 1;
      // SS_TRIGGER_MODE = 2;
      // SOURCE
      // SYNCH_TRIGGER_SOURCE = 0;
      // EXT_TRIGGER_SOURCE = 1;
      // SOURCETYPE
      // EDGE_TRIGGER_TYPE = 0;
      // LEVEL_TRIGGER_TYPE = 1;
      //
      FMCT.m_Trigger.m_Mode = CAvantesInterface.SW_TRIGGER_MODE;
      FMCT.m_Trigger.m_Source = CAvantesInterface.SYNCH_TRIGGER_SOURCE;
      FMCT.m_Trigger.m_SourceType = CAvantesInterface.EDGE_TRIGGER_TYPE;
      //
      FMCT.m_NrAverages = 1;
      FMCT.m_SaturationDetection = 1;
      FMCT.m_CorDynDark.m_Enable = (byte)1;
      FMCT.m_CorDynDark.m_ForgetPercentage = 100;
      FMCT.m_Smoothing.m_SmoothPix = 0;
      FMCT.m_Smoothing.m_SmoothModel = 0;
      FMCT.m_Control.m_StrobeControl = 0;
      FMCT.m_Control.m_LaserDelay = 0;
      FMCT.m_Control.m_LaserWidth = 0;
      FMCT.m_Control.m_LaserWaveLength = 0.0f;
      FMCT.m_Control.m_StoreToRam = 0;
    }
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Callback - FormNotify 
    //------------------------------------------------------------------------------------------
    //
    private void FormNotifyOnMeasurementReady()
    { // only here!!!
      FEventMeasurement.Set(); // -> Trigger GetSpectrum 
    }
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Execution - Thread
    //------------------------------------------------------------------------------------------
    //
    private void OnMeasurementTrigger()
    { 
      try
      { // s.b. PrepareMeasurement();
        Int32 IResult = (Int32)CAvantesInterface.AVS_Measure(FHandleDevice, FHandleFormNotify, INIT_SCAN_SINGLE);
        FResultMeasurement = (CAvantesInterface.ERR_SUCCESS != IResult);
      }
      catch (Exception)
      {
      }
    }
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------------
    //
    public override Int32 GetIntensityMaximum()
    {
      return (Int32)CAvantesInterface.MAX_PIXEL_VALUE;
    }

    public override void SetIntegrationTime(Int32 value)
    {
      try
      {
        FIntegrationTime = value;
      }
      catch (Exception)
      {
        return;
      }
    }

    public override void SetInternalTrigger()
    {
      try
      {
        TriggerType = ETriggerType.Internal;
        return;
      }
      catch (Exception)
      {
        return;
      }
    }

    public override void SetExternalTrigger()
    {
      try
      {
        TriggerType = ETriggerType.External;
        return;
      }
      catch (Exception)
      {
        return;
      }
    }

    public override void SetSynchronousTrigger()
    {
      try
      {
        TriggerType = ETriggerType.Synchronous;
        return;
      }
      catch (Exception)
      {
        return;
      }
    }

    public override Double[] GetSpectrum()
    {
      try
      {
        if (FIsOpened)
        {
          StartMeasurement();
          if (FEventMeasurement.WaitOne(IntegrationTime + 1000))
          {
            CAvantesInterface.PixelArrayType PSpectrum = new CAvantesInterface.PixelArrayType();
            UInt32 TimeMeasurement = 0;
            if (CAvantesInterface.ERR_SUCCESS == (int)CAvantesInterface.AVS_GetScopeData(FHandleDevice,
                                                                     ref TimeMeasurement,
                                                                     ref PSpectrum))
            {
              Double[] Spectrum = new Double[PixelNumber];
              Int32 VS = PixelNumber;
              for (Int32 VI = 0; VI < VS; VI++)
              {
                Spectrum[VI] = PSpectrum.Value[VI];
              }
              return Spectrum;
            }
          }
        }
        return null;
      }
      catch (Exception)
      {
        return null;
      }
    }
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Private Helper
    //------------------------------------------------------------------------------------------
    //  
    private Double[] GetWavelengths()
    {
      try
      { 
        CAvantesInterface.PixelArrayType PixelArray = new CAvantesInterface.PixelArrayType();
        if (CAvantesInterface.ERR_SUCCESS == (int)CAvantesInterface.AVS_GetLambda(FHandleDevice, ref PixelArray))
        {
          return PixelArray.Value;
        }
        return null;
      }
      catch (Exception)
      {
        return null;
      }
    }

    private Boolean PrepareMeasurement()
    {
      FMCT.m_NrAverages = 1;
      FMCT.m_SaturationDetection = 1;
      FMCT.m_CorDynDark.m_Enable = (byte)1;
      FMCT.m_CorDynDark.m_ForgetPercentage = 100;
      FMCT.m_IntegrationTime = FIntegrationTime; // ms
      //
      // MODE
      // SW_TRIGGER_MODE = 0;
      // HW_TRIGGER_MODE = 1;
      // SS_TRIGGER_MODE = 2;
      // SOURCE
      // SYNCH_TRIGGER_SOURCE = 0;
      // EXT_TRIGGER_SOURCE = 1;
      // SOURCETYPE
      // EDGE_TRIGGER_TYPE = 0;
      // LEVEL_TRIGGER_TYPE = 1;
      //
      FMCT.m_Trigger.m_SourceType = CAvantesInterface.EDGE_TRIGGER_TYPE;
      switch (TriggerType)
      {
        case ETriggerType.Internal: // ???
          FMCT.m_Trigger.m_Mode = CAvantesInterface.SW_TRIGGER_MODE;
          FMCT.m_Trigger.m_Source = CAvantesInterface.SYNCH_TRIGGER_SOURCE;
          break;
        case ETriggerType.External: // ???
          FMCT.m_Trigger.m_Mode = CAvantesInterface.HW_TRIGGER_MODE;
          FMCT.m_Trigger.m_Source = CAvantesInterface.EXT_TRIGGER_SOURCE;
          break;
        case ETriggerType.Synchronous: // ???
          FMCT.m_Trigger.m_Mode = CAvantesInterface.HW_TRIGGER_MODE;
          FMCT.m_Trigger.m_Source = CAvantesInterface.SYNCH_TRIGGER_SOURCE;
          break;        
      }
      //
      Int32 IResult = (int)CAvantesInterface.AVS_PrepareMeasure((IntPtr)FHandleDevice, ref FMCT);
      return (CAvantesInterface.ERR_SUCCESS != IResult);
    }

    private Boolean StartMeasurement()
    {
      try
      { 
        FEventMeasurement.Reset();
        //
        PrepareMeasurement();
        //
        FThreadMeasurement = new Thread(OnMeasurementTrigger);
        FThreadMeasurement.Start();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private Boolean StopMeasurement()
    {
      try
      { 
        Int32 IResult = (Int32)CAvantesInterface.AVS_StopMeasure(FHandleDevice);
        // and only here (Abort!)
        // NC FEventMeasurement.Set();
        //
        if (CAvantesInterface.ERR_SUCCESS != IResult)
        {
          return false;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------------------------
    //
    public override Boolean InitiateSpectrometer()
    {
      try
      { 
        FPort = CAvantesInterface.AVS_Init((short)0);
        if (0 < FPort)
        { //
          CAvantesInterface.AVS_Register(FFormNotify.Handle);
          //
          FNumberOfSpectrometers = CAvantesInterface.AVS_GetNrOfDevices();
          //
          CAvantesInterface.AvsIdentityType[] DeviceIDs = new CAvantesInterface.AvsIdentityType[FNumberOfSpectrometers];
          UInt32 RequiredSize = ((uint)FNumberOfSpectrometers) * (uint)Marshal.SizeOf(typeof(CAvantesInterface.AvsIdentityType));
          //
          FNumberOfSpectrometers = CAvantesInterface.AVS_GetList(RequiredSize, ref RequiredSize, DeviceIDs);
          //
          if (0 < FNumberOfSpectrometers)
          {
            for (Int32 DI = 0; DI < 1; DI++)//FNumberOfSpectrometers; DI++)
            {
              // MUELL!!!! FSpectrometerName = DeviceIDs[DI].m_UserFriendlyName;
              switch (DeviceIDs[DI].m_Status)
              {
                case CAvantesInterface.DEVICE_STATUS.UNKNOWN:
                  break;
                case CAvantesInterface.DEVICE_STATUS.USB_AVAILABLE:
                  CAvantesInterface.AvsIdentityType AitActive = new CAvantesInterface.AvsIdentityType();
                  AitActive.m_SerialNumber = DeviceIDs[DI].m_SerialNumber;
                  FHandleDevice = CAvantesInterface.AVS_Activate(ref AitActive);
                  if (CAvantesInterface.INVALID_AVS_HANDLE_VALUE != (Int64)FHandleDevice)
                  { // !!! only here and only one time !!!
                    FWavelengths = GetWavelengths();
                    FIsOpened = true;
                    return true;
                  }
                  return false;
                case CAvantesInterface.DEVICE_STATUS.USB_IN_USE_BY_APPLICATION:
                  break;
                case CAvantesInterface.DEVICE_STATUS.USB_IN_USE_BY_OTHER:
                  break;
                default:
                  break;
              }
            }
          }          
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean CloseSpectrometer()
    {
      try
      { 
        FIsOpened = false;
        StopMeasurement();
        CAvantesInterface.AVS_Done();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}
