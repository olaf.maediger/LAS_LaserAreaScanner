﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using SpectrometerBase;
//
namespace SHOceanOptics
{
  public class COceanOptics : CSpectrometerBase
  {
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------------------------------------
    //
    private const String INIT_SPECTROMETERNAME = "OceanOptics Usb2000PlusUv";
    //
    private const ETriggerType INIT_TRIGGERTYPE = ETriggerType.Internal;
    //
    public const Int32 INIT_INTEGRATIONTIME = 10; // [ms]
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------------------------------------
    //
    private OmniDriver.CCoWrapper FWrapper;
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------------------------
    //
    public COceanOptics()
    {
      FSpectrometerName = INIT_SPECTROMETERNAME;
      FTriggerType = INIT_TRIGGERTYPE;
      FIntegrationTime = INIT_INTEGRATIONTIME;
      FWrapper = new OmniDriver.CCoWrapper();
    }
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------------------------
    //
    public override Int32 GetIntensityMaximum()
    {
      return FWrapper.getMaximumIntensity(0);
    }

    public override void SetIntegrationTime(Int32 value)
    {
      FIntegrationTime = value;
      FWrapper.setIntegrationTime(0, IntegrationTime);
    }

    public override void SetInternalTrigger()
    {
      try
      {
        TriggerType = ETriggerType.Internal;
        FWrapper.setExternalTriggerMode(0, 0);
        return;
      }
      catch (Exception)
      {
        return;
      }
    }

    public override void SetExternalTrigger()
    {
      try
      {
        TriggerType = ETriggerType.External;
        FWrapper.setExternalTriggerMode(0, 3);
        return;
      }
      catch (Exception)
      {
        return;
      }
    }

    public override void SetSynchronousTrigger()
    {
      try
      {
        TriggerType = ETriggerType.Synchronous;
        FWrapper.setExternalTriggerMode(0, 2);
        return;
      }
      catch (Exception)
      {
        return;
      }
    }

    public override Double[] GetSpectrum()
    {
      return ((double[])FWrapper.getSpectrum(0));
    }
    //
    //------------------------------------------------------------------------------------------
    //  Segment - Public Management
    //------------------------------------------------------------------------------------------
    //
    public override Boolean InitiateSpectrometer()
    {
      try
      {
        NumberOfSpectrometers = FWrapper.openAllSpectrometers();
        if (NumberOfSpectrometers > 0)
        {
            SpectrometerName = FWrapper.getName(0);
            SetIntegrationTime(IntegrationTime);
            PixelNumber = (Int32)FWrapper.getNumberOfPixels(0);
            FWavelengths = ((double[])FWrapper.getWavelengths(0));
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean CloseSpectrometer()
    {
      try
      {
        FWrapper.closeAllSpectrometers();
	  		return true;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }
}

