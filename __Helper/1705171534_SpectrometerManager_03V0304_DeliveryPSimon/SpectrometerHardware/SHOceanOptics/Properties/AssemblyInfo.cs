﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SHOceanOptics")]
[assembly: AssemblyDescription("Hardware-Dll for OceanOptics Spectrometer")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("LLG")]
[assembly: AssemblyProduct("SHOceanOptics")]
[assembly: AssemblyCopyright("Copyright © LLG 2017")]
[assembly: AssemblyTrademark("LLG")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0a1441b8-1a53-4e56-9e2d-12ec919b59a4")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.3.4.*")]
[assembly: AssemblyFileVersion("3.3.4")]
