#
#-----------------------------------------------------
# Library - Dac MCP4725
#-----------------------------------------------------
#
from machine import I2C
from machine import Pin
#
#-----------------------------------------------------
# Library - DAC - MCP4725
#-----------------------------------------------------
class CMCP4725():
    def __init__(self, deviceid, i2c, address = 99): # 0x63        
        self.DeviceID = deviceid
        self.I2C = i2c
        self.Address = address
        self.Buffer = bytearray(2)
        print('CMCP4725[' + str(self.DeviceID) + ', ' + str(self.Address) + '].__init__()')
    #        
    def WriteValue(self, value):
        Value = 0x0FFF & value
        Voltage = float(Value * 3300 / 4095)
        self.Buffer[0] = 255 & (Value >> 8)
        self.Buffer[1] = 255 & (Value >> 0)
        print('CMCP4725[' + str(self.DeviceID) + '].WriteValue(' + str(Voltage) + 'mV, ' + str(Value) + ')')
        return 2 == self.I2C.writeto(self.Address, self.Buffer)
    #        
    def WriteVoltagemV(self, voltage):
        Value = 0x0FFF & int(0.5 + 4095 * voltage / 3300.0)
        self.Buffer[0] = 255 & (Value >> 8)
        self.Buffer[1] = 255 & (Value >> 0)
        print('CMCP4725[' + str(self.DeviceID) + '].WriteVoltagemV(' + str(voltage) + 'mV, ' + str(Value) + ')')
        return 2 == self.I2C.writeto(self.Address, self.Buffer)
#
#-----------------------------------------------------
# Library Check
#-----------------------------------------------------
if '__main__' == __name__:
    #
    PortI2C = I2C(0, scl = Pin(22), sda = Pin(21), freq = 400000) # HW-I2C
    I2CAddressesDetected = PortI2C.scan()
    #
    DAC = CMCP4725('DACtest', PortI2C, I2CAddressesDetected[0])
    #
    print(DAC.WriteVoltagemV(0.25 * 3300))
    print(DAC.WriteVoltagemV(0.50 * 3300))
    print(DAC.WriteVoltagemV(0.75 * 3300))
    print(DAC.WriteVoltagemV(1.00 * 3300))
    #
    print(DAC.WriteValue(1023))
    print(DAC.WriteValue(2047))
    print(DAC.WriteValue(3071))
    print(DAC.WriteValue(4095))
#    
    
    
    
    
    
    
    
    
#
#
# I2C_ADDRESSx62 = 16 * 6 + 2
# I2C_ADDRESSx63 = 16 * 6 + 3
# 
# POWER_DOWN_MODE = {'Off':0, '1k':1, '100k':2, '500k':3}
#         
# class CMCP4725():
#     def __init__(self, i2c, address = I2C_ADDRESSx62):
#         self.i2c = i2c
#         self.address = address        
#     #
#     def Write(self, value):
#         Buffer = bytearray(2)
#         Buffer[0] = 255 & value
#         Buffer[1] = 255 & (value >> 8)
#         print(Buffer[0])
#         print(Buffer[1])
#         #self.i2c.writeto(62, '123')
#         self.i2c.writeto(self.address, Buffer, 2)
#     #
#     def read(self):
#         buf = bytearray(5)
#         if self.i2c.readfrom_into(self.address,buf) == 5:
#             eeprom_write_busy = (buf[0] & 0x80) == 0
#             power_down = self._powerDownKey((buf[0] >> 1) & 0x03)
#             value = ((buf[1] << 8) | (buf[2])) >> 4
#             eeprom_power_down = self._powerDownKey((buf[3] >> 5) & 0x03)
#             eeprom_value = ((buf[3] & 0x0f) << 8) | buf[4] 
#             return (eeprom_write_busy, power_down, value, eeprom_power_down, eeprom_value)
#         return None
#     #
#     def config(self, power_down = 'Off', value = 0, eeprom = False):
#         buf = bytearray()
#         conf = 0x40 | (POWER_DOWN_MODE[power_down] << 1)
#         if eeprom:
#             #store the powerdown and output value in eeprom
#             conf = conf | 0x60
#         buf.append(conf)
#         #check value range
#         if value < 0:
#             value=0
#         value = value & 0xFFF
#         buf.append(value >> 4)
#         buf.append((value & 0x0F) << 4)
#         return self.i2c.writeto(self.address, buf) == 3
#     #
#     def _powerDownKey(self, value):
#         for key, item in POWER_DOWN_MODE.items():
#             if item == value:
#                 return key
#     #
# 
# #
# #return self.i2c.writeto(self.address, self._writeBuffer) == 2
# #
# 
#                 