#
#-----------------------------------------------------
# Library - LaserScanner
#-----------------------------------------------------
#
import time
from machine import Pin
#
#-----------------------------------------------------
# Library - LaserScanner
#-----------------------------------------------------
class CLaserTrigger():
    #                 [string, pin]
    def __init__(self, deviceid, pintrigger): 
        print('CLaserTrigger[' + str(deviceid) + ', ' + str(pintrigger) + '].__init__()')
        self.DeviceID = deviceid
        self.PinTrigger = pintrigger
        return
    #              [s, s, 1]
    def Pulse(self, period, width, count): 
        print('CLaserTrigger[' + str(self.DeviceID) + '].Pulse(' + str(period) + ', ' + str(width) + ', ' + str(count) + ')')
        for PI in range(0, count):
            self.PinTrigger.on()
            time.sleep(width)
            self.PinTrigger.off()
            time.sleep(abs(period - width))        
        return    
#
#-----------------------------------------------------
# Library Check
#-----------------------------------------------------
if '__main__' == __name__:
    #
    LT = CLaserTrigger('LaserTrigger', Pin(2, Pin.OUT))
    LT.Pulse(0.050, 0.010, 30)    