#
import time
#
class CLaserTrigger():
    #
    def __init__(self, pintrigger):
        print('CLaserTrigger.__init__()')
        self.PinTrigger = pintrigger
        return
    #
    def Pulse(self, period, width, count):
        print('CLaserTrigger.Pulse(' + str(period) + ', ' + str(width) + ', ' + str(count) + ')')
        for PI in range(0, count):
            self.PinTrigger.on()
            time.sleep(0.10)
            self.PinTrigger.off()
            time.sleep(0.10)        
        return 