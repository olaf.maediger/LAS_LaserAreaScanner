
PP = 0.101 # [s]
PC = 10    # [1]
DM = 1.010 # [s]
PX0 = 1800
PY0 = 1800
#
for RI in range(0, 5, 1):
    for CI in range(0, 5, 1):
        Scanner.MovePositionPulse(PX0 + CI * 100, PY0 + RI * 100, PP, PC, DM) # [1, 1, s, s, s]
