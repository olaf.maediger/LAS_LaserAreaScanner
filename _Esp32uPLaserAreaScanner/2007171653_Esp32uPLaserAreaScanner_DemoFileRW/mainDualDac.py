#
import time
from machine import Pin
from machine import I2C
#
from MCP4725 import CMCP4725
from LaserTrigger import CLaserTrigger
from LaserScanner import CLaserScanner
#
print("*** Esp32uPLaserAreaScanner")
print("Start:");
#
#PortI2C = I2C(-1, scl = Pin(22), sda = Pin(21), freq = 400000) # SW-I2C
PortI2C = I2C(0, scl = Pin(22), sda = Pin(21), freq = 400000) # HW-I2C
I2CAddressesDetected = PortI2C.scan()
if (len(I2CAddressesDetected) < 2):
    print('Error: Dual DAC-I2C-Devices not found!')
else:
    for AI in range(0, len(I2CAddressesDetected), 1):
        print('Detect I2CAddress[' + str(I2CAddressesDetected[AI]) + ']')
    #
    I2CAddressX = I2CAddressesDetected[0]
    I2CAddressY = I2CAddressesDetected[1]
    #
    PositionX = CMCP4725('PositionX', PortI2C, I2CAddressX)
    PositionY = CMCP4725('PositionY', PortI2C, I2CAddressY)
    Trigger = CLaserTrigger('Trigger', Pin(2, Pin.OUT))
    Scanner = CLaserScanner('Scanner', PositionX, PositionY, Trigger)
#     #
#     # 1
#     PP = 0.001 # [s]
#     PC = 1     # [1]
#     DM = 0.010 # [s]
#     for SI in range(0, 1000, 1):
#         Scanner.MovePositionPulse(2000, 2000, PP, PC, DM) # [1, 1, s, s, s]
#    execfile('steptable.py')
#    
print("End.");
