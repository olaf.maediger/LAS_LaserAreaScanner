#
PP = 0.001      # [s] PulsePeriod
PC = 1          # [1] PulseCount
DM = 0.001      # [s] DelayMotion
#
ColCount = 6    # [1] 
PX0 = 1800      # [bit] PositionX
DX = 100        # [bit] DeltaX
#
RowCount = 6    # [1] 
PY0 = 1800      # [bit] PositionY
DY = 100        # [bit] DeltaY
#
for RI in range(0, RowCount):
    for CI in range(0, ColCount):
        Scanner.MovePositionPulse(PX0 + CI * DX, PY0 + RI * DY, PP, PC, DM) # [1, 1, s, s, s]
