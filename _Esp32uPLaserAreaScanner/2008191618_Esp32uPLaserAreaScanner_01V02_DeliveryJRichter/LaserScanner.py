#
#-----------------------------------------------------
# Library - LaserScanner
#-----------------------------------------------------
#
import time
from machine import I2C
from machine import Pin
#
from LaserTrigger import CLaserTrigger
from MCP4725 import CMCP4725
#
#-----------------------------------------------------
# Library - LaserScanner
#-----------------------------------------------------
class CLaserScanner():
    #
    def __init__(self, deviceid, positionx, positiony, lasertrigger):
        # debug print('CLaserScanner[' + str(deviceid) + '].__init__()')
        self.DeviceID = deviceid
        self.PositionX = positionx
        self.PositionY = positiony
        self.LaserTrigger = lasertrigger
    #
    def MovePositionPulse(self, positionx, positiony, pulseperiod, pulsecount, delaymotion):
        #print('CLaserScanner[' + str(self.DeviceID) + '].MovePositionPulse(' + str(positionx) + ', ' + \
        #      str(positiony) + ', ' + str(pulseperiod) + ', ' + str(pulsecount) + ', ' + str(delaymotion) + ')')
        self.PositionX.WriteValue(positionx)
        self.PositionY.WriteValue(positiony)
        if (0 < delaymotion):
            time.sleep(delaymotion)
        self.LaserTrigger.Pulse(pulseperiod, pulseperiod / 2, pulsecount)
    #
    def ExecuteSteptable(self, filename):
        print('\r\n*** ExecuteSteptable[' + filename + ']:')
        file = open(filename, "r")
        #
        EOF = False
        while not(EOF):
            Line = file.readline().strip()
            if 0 < len(Line):
                #print('<' + Line + '>')
                Tokens = Line.split()
                #print(Tokens)
                if 5 <= len(Tokens):
                    PX = int(Tokens[0])
                    PY = int(Tokens[1])
                    PP = float(Tokens[2])
                    PC = int(Tokens[3])
                    DM = float(Tokens[4])
                    #print(str(PX) + ' ' +str(PY))
                self.MovePositionPulse(PX, PY, PP, PC, DM) 
            else:
                EOF = True
        #
        file.close()
        
        
        
#
#-----------------------------------------------------
# Library Check
#-----------------------------------------------------
if '__main__' == __name__:
    #
    PortI2C = I2C(0, scl = Pin(22), sda = Pin(21), freq = 400000) # HW-I2C
    I2CAddressesDetected = PortI2C.scan()
    #
    PositionX = CMCP4725('PositionX', PortI2C, I2CAddressesDetected[0])
    PositionY = CMCP4725('PositionY', PortI2C, I2CAddressesDetected[1])
    Trigger = CLaserTrigger('Trigger', Pin(2, Pin.OUT))
    #
    PositionX.WriteValue(2048)
    PositionY.WriteValue(2048)
    Trigger.Pulse(0.050, 0.010, 30)
    #
    Scanner = CLaserScanner('Scanner', PositionX, PositionY, Trigger)
    #Scanner.MovePositionPulse(2000, 2000, 0.050, 30, 1.0) # [1, 1, s, s, s]
    #
    execfile('Steptable25Quick.py')
    #execfile('Steptable1000Quick.py')
    
#    
    