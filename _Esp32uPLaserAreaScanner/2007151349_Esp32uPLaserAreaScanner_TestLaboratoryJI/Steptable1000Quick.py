#
PP = 0.001 # [s] PulsePeriod
PC =  1    # [1] PulseCount
DM = 0.001 # [s] DelayMotion
#
ColCount = 101  # [1] 
PX0 = 1800      # [bit] PositionX
DX = 4          # [bit] DeltaX
#
RowCount = 11   # [1] 
PY0 = 1800      # [bit] PositionY
DY = 40         # [bit] DeltaY
#
for RI in range(0, RowCount):
    for CI in range(0, ColCount):
        Scanner.MovePositionPulse(PX0 + CI * DX, PY0 + RI * DY, PP, PC, DM) # [1, 1, s, s, s]
