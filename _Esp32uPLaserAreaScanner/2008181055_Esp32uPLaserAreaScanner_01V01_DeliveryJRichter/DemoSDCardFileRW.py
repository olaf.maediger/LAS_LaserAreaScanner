import os
#
print(os.listdir() )
#
#----------------------------------------
# Write File
#----------------------------------------
print('\r\n*** Write File:')
file = open("demo.txt", "w")
#
Line = "First Line\n"
file.write(Line)
Line = "Second Line"
file.write(Line)
#
file.close()
print(os.listdir() )
#
#----------------------------------------
# Read File static
#----------------------------------------
print('\r\n*** Read File static:')
file = open ("demo.txt", "r")
#
print('<' + file.readline().strip() + '>')
print('<' + file.readline().strip() + '>')
print('<' + file.readline().strip() + '>')
#
file.close()
#
#----------------------------------------
# Read File dynamic
#----------------------------------------
print('\r\n*** Read File dynamic:')
file = open ("demo.txt", "r")
#
EOF = False
while not(EOF):
    Line = file.readline().strip()
    if 0 < len(Line):
        print('<' + Line + '>')
    else:
        EOF = True
#
file.close()

 
