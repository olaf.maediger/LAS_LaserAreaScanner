#
import time
from machine import Pin
from machine import I2C
#
from LaserTrigger import CLaserTrigger
from MCP4725 import CMCP4725
#
#
print("*** Esp32LaserAreaScanner")
print("Start:");
#
#PinLedSystem = Pin(2, Pin.OUT)
#PLS = Pin(2)
#
Trigger = CLaserTrigger('Trigger', Pin(2, Pin.OUT))
Trigger.Pulse(0.050, 0.010, 30)
#
#PortI2C = I2C(-1, scl = Pin(22), sda = Pin(21), freq = 400000) # SW-I2C
PortI2C = I2C(0, scl = Pin(22), sda = Pin(21), freq = 400000) # HW-I2C
I2CAddressesDetected = PortI2C.scan()
if (len(I2CAddressesDetected) < 2):
    print('Error: Dual DAC-I2C-Devices not found!')
else:
    for AI in range(0, len(I2CAddressesDetected), 1):
        print('Detect I2CAddress[' + str(I2CAddressesDetected[AI]) + ']')
    #
    I2CAddressX = I2CAddressesDetected[0]
    DACX = CMCP4725('DACX', PortI2C, I2CAddressX)
    DACX.WriteVoltagemV(3300 / 2) 
    I2CAddressY = I2CAddressesDetected[1]
    DACY = CMCP4725('DACY', PortI2C, I2CAddressY)
    DACY.WriteVoltagemV(3300 / 1) 
#    
print("End.");
