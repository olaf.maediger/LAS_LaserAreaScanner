import os
import uos
import machine
from machine import Pin

#uos.mount(machine.SDCard(), "/sd")

sdc = machine.SDCard(slot=2, sck=Pin(18), miso=Pin(19), mosi=Pin(23), cs=Pin(5))
#???os.unmount(sdc)

os.mount(sdc, "/sd")
fw = open("/sd/demo.txt", "rw")
fw.close()
os.unmount()


