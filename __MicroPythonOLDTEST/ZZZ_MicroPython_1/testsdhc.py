#
import os
import uos
import machine
from machine import Pin
#
print('*********************')
print('***  testsdhc.py  ***')
print('*-------------------*')
print('* Version: 01V02    *')
print('* Date:    200721   *')
print('* Time:    1515     *')
print('* Author: OMDevelop *')
print('*********************')
print('testsdhc.py : start')
#
sdc = machine.SDCard(slot=2, sck=Pin(18), miso=Pin(19), mosi=Pin(23), cs=Pin(5))
os.mount(sdc, '/sd')
#
fw = open('/sd/test33.py', 'w')
#
fw.write('*********************\r\n')
fw.write('***  testsdhc.py  ***\r\n')
fw.write('*-------------------*\r\n')
fw.write('* Version: 01V02    *\r\n')
fw.write('* Date:    200721   *\r\n')
fw.write('* Time:    1515     *\r\n')
fw.write('* Author: OMDevelop *\r\n')
fw.write('*********************\r\n')
#
for LI in range(0, 100, 1):
    fw.write('Line[' + str(LI) + ']\r\n')
fw.close()
#
print('testsdhc.py : end')




