
#include "MCP4725.h"

CMCP4725 dac;

void setup(void) 
{
  Serial.begin(115200);
  Serial.println("------------------------------------");

  // For Adafruit MCP4725A1 the address is 0x62 (default) or 0x63 (ADDR pin tied to VCC)
  // For MCP4725A0 the address is 0x60 or 0x61
  // For MCP4725A2 the address is 0x64 or 0x65
  dac.begin(0x62);
    
  //Serial.println("Generating a triangle wave...");
}

void loop(void) 
{
//    uint32_t counter;
//    Serial.println("Up...");
//    for (counter = 0; counter < 4095; counter++)
//    {
//      dac.setVoltage(counter, false);
//      delay(5);
//    }
//    delay(5000);
//    Serial.println("Down...");
//    for (counter = 4095; counter > 0; counter--)
//    {
//      dac.setVoltage(counter, false);
//      delay(5);
//    }
//    delay(5000);
    dac.setVoltage(0, false);
    delay(5000);
    dac.setVoltage(410, false);
    delay(5000);
    dac.setVoltage(819, false);
    delay(5000);
    dac.setVoltage(1638, false);
    delay(5000);
    dac.setVoltage(2457, false);
    delay(5000);
    dac.setVoltage(3276, false);
    delay(5000);
    dac.setVoltage(4095, false);
    delay(5000);
}
