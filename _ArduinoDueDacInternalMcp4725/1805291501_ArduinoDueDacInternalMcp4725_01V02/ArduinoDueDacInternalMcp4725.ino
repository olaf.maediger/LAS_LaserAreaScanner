#include "Defines.h"
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Serial.h"
#include "Led.h"
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  #include "DacInternal.h"
#endif
#if (defined(DAC_MCP4725_X) || defined(DAC_MCP4725_Y)) 
  #include "DacMcp4725.h"
#endif
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
CError LASError;
CProcess LASProcess;
CCommand LASCommand;
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED);
CLed LedLaser(PIN_LEDLASER, LEDLASER_INVERTED);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Dac
//---------------------------------------------------
//
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  CDacInternal DacInternal_XY(true, true);
#endif
#ifdef DAC_MCP4725_X
CDacMcp4725 DacMcp4725_X(CDacMcp4725::MCP4725_I2CADDRESS_DEFAULT);
#endif
#ifdef DAC_MCP4725_Y
CDacMcp4725 DacMcp4725_Y(CDacMcp4725::MCP4725_I2CADDRESS_HIGH);
#endif
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort und CommandPort!
// NC - CSerial SerialDebug(Serial);
// normal CSerial SerialCommand(Serial1);
CSerial SerialCommand(Serial);
// NC - CSerial SerialPrinter(Serial2);
// NC - Serial3
//
//
//---------------------------------------------------
// Segment - Forward-Declaration
//---------------------------------------------------
//

//
//---------------------------------------------------
// Segment - Setup
//---------------------------------------------------
//
void setup() 
{ //-------------------------------------------
  // Device - LedSystem
  //-------------------------------------------
  LedSystem.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.Off();
    delay(40);
    LedSystem.On();
    delay(40);
  }
  LedSystem.Off();
  //-------------------------------------------
  // Device - Dac
  //-------------------------------------------
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  DacInternal_XY.Open();
#endif  
#ifdef DAC_MCP4725_X
  DacMcp4725_X.Open();
#endif  
#ifdef DAC_MCP4725_Y
  DacMcp4725_Y.Open();
#endif  
  //-------------------------------------------
  // Device - Serial
  //-------------------------------------------
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_OFF);
  //-------------------------------------------
  // Device - Command
  //-------------------------------------------
  LASCommand.WriteProgramHeader(SerialCommand);
  LASCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //-------------------------------------------
  // Device - Process 
  //-------------------------------------------
  LASProcess.Open();
  LASProcess.SetState(spWelcome);
}
//
//---------------------------------------------------
// Segment - Loop
//---------------------------------------------------
//
void loop() 
{ 
  LASError.Handle(SerialCommand);    
  LASCommand.Handle(SerialCommand);
  LASProcess.Handle(SerialCommand);  
}

/*
    DacMcp4725_X.SetValue(0);
    delay(5000);
    DacMcp4725_X.SetValue(410);
    delay(5000);
    DacMcp4725_X.SetValue(819);
    delay(5000);
    DacMcp4725_X.SetValue(1638);
    delay(5000);
    DacMcp4725_X.SetValue(2457);
    delay(5000);
    DacMcp4725_X.SetValue(3276);
    delay(5000);
    DacMcp4725_X.SetValue(4095);
    delay(5000);
*/

