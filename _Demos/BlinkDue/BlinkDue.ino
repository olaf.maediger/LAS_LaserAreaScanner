void setup() 
{
  pinMode(13, OUTPUT); 
  Serial.begin(115200);
  Serial1.begin(115200);
  Serial2.begin(115200);
}

void loop() 
{
  digitalWrite(13, HIGH);
  Serial.print(" H0");
  Serial1.print(" H1");
  Serial2.print(" H2");
  delay(1000);
  digitalWrite(13, LOW);
  Serial.print(" L0");
  Serial1.print(" L1");
  Serial2.print(" L2");
  delay(1000);
}
