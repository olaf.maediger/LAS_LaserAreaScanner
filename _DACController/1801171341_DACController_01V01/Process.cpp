#include "Process.h"
//
CProcess::CProcess()
{
  FProcessPeriod = INIT_PROCESSPERIOD;
  FPeriodCountPreset = INIT_PERIODCOUNTPRESET;
  FPeriodCountActual = INIT_PERIODCOUNTACTUAL;
  FDacRampLow = INIT_DACRAMPLOW;
  FDacRampHigh = INIT_DACRAMPHIGH;
  FDacRampActual = INIT_DACRAMPACTUAL;
}

