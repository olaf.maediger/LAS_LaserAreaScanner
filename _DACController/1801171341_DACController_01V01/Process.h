#ifndef Process_h
#define Process_h
//
#include "Defines.h"
//
class CProcess
{
  private:
  UInt32 FProcessPeriod;       // [us]
  UInt32 FPeriodCountPreset;   // [1] 
  UInt32 FPeriodCountActual;   // [1] 
  UInt32 FDacRampLow;
  UInt32 FDacRampHigh;
  UInt32 FDacRampActual;
  //
  public:
  CProcess();
  //
  //  System
  //
  inline UInt32 GetProcessPeriodus()
  {
    return FProcessPeriod;
  }
  inline void SetProcessPeriodus(UInt32 value)
  {
    FProcessPeriod = value;
  }
  inline UInt32 GetProcessPeriodms()
  {
    return FProcessPeriod / 1000;
  }
  inline void SetProcessPeriodms(UInt32 value)
  {
    FProcessPeriod = 1000 * value;
  }

  inline UInt32 GetPeriodCountPreset()
  {
    return FPeriodCountPreset;
  }
  inline void SetPeriodCountPreset(UInt32 value)
  {
    FPeriodCountPreset = value;
    FPeriodCountActual = 0;
  }
  inline UInt32 GetPeriodCountActual()
  {
    return FPeriodCountActual;
  }
  inline void SetPeriodCountActual(UInt32 value)
  {
    FPeriodCountActual = value;
  }
  inline void IncrementPeriodCountActual()
  {
    FPeriodCountActual++;
  }
  inline Boolean IsPeriodCountReached()
  {
    return (FPeriodCountPreset <= FPeriodCountActual);
  }

  inline UInt32 GetDacRampLow()
  {
    return FDacRampLow;
  }
  inline void SetDacRampLow(UInt32 value)
  {
    FDacRampLow = value;
  }
  inline UInt32 GetDacRampHigh()
  {
    return FDacRampHigh;
  }
  inline void SetDacRampHigh(UInt32 value)
  {
    FDacRampHigh = value;
  }
  inline UInt32 GetDacRampActual()
  {
    return FDacRampActual;
  }
  inline void SetDacRampActual(UInt32 value)
  {
    FDacRampActual = value;
  }
  inline void IncrementDacRampActual()
  {
    if (FDacRampActual < FDacRampHigh)
    {
      FDacRampActual++;
    }
    else
    {
      FDacRampActual = FDacRampLow;
    }
  } 
  
};
//
#endif // Process_h
