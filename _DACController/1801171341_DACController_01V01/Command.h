#ifndef Command_h
#define Command_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
#include "Automation.h"
//
// Command-Parameter
#define SIZE_RXDPARAMETER   8
#define COUNT_RXDPARAMETERS 4
//
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
#define HELP_SOFTWAREVERSION  " Software-Version DAC01V01"
//
const int COUNT_ANSWERLINE = 1;
//
const int COUNT_HEADERLINES = 10;
//
#define TITEL_LINE            "------------------------------------------"
#define TITEL_PROJECT         "- Project:   DAC-Controller              -"
#define TITEL_VERSION         "- Version:   01V01                       -"
#ifdef PROCESSOR_ARDUINOUNO
#define TITEL_HARDWARE        "- Hardware:  ArduinoUnoR3                -"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoUnoR3"
#endif
#ifdef PROCESSOR_ARDUINOMEGA 
#define TITEL_HARDWARE        "- Hardware:  ArduinoMega328              -"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoMega328"
#endif
#ifdef PROCESSOR_ARDUINODUE
#define TITEL_HARDWARE        "- Hardware:  ArduinoDue3                 -"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoDue3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define TITEL_HARDWARE        "- Hardware:  Stm32F103C8                 -"
#define HELP_HARDWAREVERSION  " Hardware-Version Stm32F103C8"
#endif
#ifdef PROCESSOR_TEENSY36 
#define TITEL_HARDWARE        "- Hardware:  Teensy36                    -"
#define HELP_HARDWAREVERSION  " Hardware-Version Teensy36"
#endif
#define TITEL_DATE            "- Date:      180116                      -"
#define TITEL_TIME            "- Time:      1637                        -"
#define TITEL_AUTHOR          "- Author:    OM                          -"
#define TITEL_PORT            "- Port:      SerialCommand (Serial1-USB) -"
#define TITEL_PARAMETER       "- Parameter: 115200, N, 8, 1             -"
//
const int COUNT_HELPLINES = 29;
//
#define HELP_COMMON           " Help (Common):"
//
#define HELP_H                " H   : This Help"
#define HELP_GPH              " GPH <fdl> : Get Program Header"
#define HELP_GSV              " GSV <fdl> : Get Software-Version"
#define HELP_GHV              " GHV <fdl> : Get Hardware-Version"
//
#define HELP_LEDLASER         " Help (LedSystem):"
#define HELP_GLL              " GLS : Get State LedSystem"
#define HELP_LLH              " LSH : Switch LedSystem On"
#define HELP_LLL              " LSL : Switch LedSystem Off"
#define HELP_PLL              " PLS <p> <n> : Pulse LedSystem with <p>eriod{ms} <n>times"
//
#define HELP_DAC              " Help (DAConverter):"
#define HELP_SDV              " SDV <c0> <c1> : Set DAC-Values Channel0/1"
#define HELP_SDR              " SDR <vl> <vh> <p> <c>: DAC-Ramp <vl>..<vh> <p>eriod{ms} <c>ount"
//
class CCommand
{
  private:
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Character FRxdCommandLine[SIZE_RXDBUFFER];
  PCharacter FPRxdCommand;
  PCharacter FPRxdParameters[COUNT_RXDPARAMETERS];
  Int16 FRxdParameterCount = 0;
  Int16 FRxdBufferIndex = 0;

  public:
  CCommand();
  ~CCommand();
  //
  PCharacter GetPRxdParameters(UInt8 index)
  {
    return FPRxdParameters[index];
  }
  PCharacter GetTxdBuffer()
  {
    return FTxdBuffer;
  }
  PCharacter GetPRxdCommand()
  {
    return FPRxdCommand;
  }
  //
  void ZeroRxdBuffer();
  void ZeroTxdBuffer();
  void ZeroRxdCommandLine();
  //
  //  Segment - Execution - Basic Output
  void WriteProgramHeader(CSerial &serial);
  void WriteSoftwareVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
  void WriteHelp(CSerial &serial);
  //
  //  Segment - Execution - Help
  void ExecuteGetHelp(CSerial &serial);
  void ExecuteGetProgramHeader(CSerial &serial);
  void ExecuteGetSoftwareVersion(CSerial &serial);
  void ExecuteGetHardwareVersion(CSerial &serial);
  //
  //  Segment - Execution - LedSystem
  void ExecuteGetLedSystem(CSerial &serial);
  void ExecuteSwitchLedSystemOn(CSerial &serial);
  void ExecuteSwitchLedSystemOff(CSerial &serial);
  void ExecutePulseLedSystem(CSerial &serial);
  //
  //  Segment - Execution - LedSystem
  void ExecuteSetDAConverterValues(CSerial &serial);
  void ExecuteSetDAConverterRamp(CSerial &serial);
  //  
  void Init();
  Boolean DetectRxdLine(CSerial &serial);
  Boolean Analyse(CSerial &serial);
  void CallbackStateAutomation(CSerial &serial, EStateAutomation stateautomation);
  Boolean Execute(CSerial &serial);
  Boolean Handle(CSerial &serial);
};
//
#endif // Command_h
