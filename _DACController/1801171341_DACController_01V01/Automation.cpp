#include "Automation.h"
#include "Led.h"
#include "Dac.h"
#include "Process.h"
#include "Command.h"
//
extern CLed LedSystem;
extern CDac DigitalAnalogConverter;
extern CSerial SerialCommand;
extern CProcess DACProcess;
extern CCommand DACCommand;
extern CDac DAConverter;
//
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  //FTimeStart = millis();
  FTimeStamp = millis();
  FTimeMarker = FTimeStamp;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  if (state != FState)
  { // Message to Manager(PC):
    FState = state;
    DACCommand.CallbackStateAutomation(SerialCommand, FState);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CAutomation::Open()
{
  FState = saIdle;
  return true;
}

Boolean CAutomation::Close()
{
  FState = saUndefined;
  return true;
}
//
//----------------------------------------------------------
//  Segment - Automation - Idle
//----------------------------------------------------------
//
void CAutomation::HandleIdle(CSerial &serial)
{
  // do nothing
}
//
//----------------------------------------------------------
//  Segment - Automation - LedSystem
//----------------------------------------------------------
//
void CAutomation::HandleLedSystemPulseBegin(CSerial &serial)
{
  DACProcess.SetPeriodCountActual(0);
  SetTimeStamp();  
  LedSystem.On(); 
  SetState(saLedSystemPulseBusy);
}
void CAutomation::HandleLedSystemPulseBusy(CSerial &serial)
{  
  SetTimeMarker();  
  if (LedSystem.GetPulsePeriodHalf() <= GetTimePeriod())
  {
    SetTimeStamp();
    if (slOn == LedSystem.GetState())
    {
      LedSystem.Off();
      DACProcess.IncrementPeriodCountActual();
      if (DACProcess.IsPeriodCountReached())
      {
        SetState(saLedSystemPulseEnd);
      }
    }
    else
      if (slOff == LedSystem.GetState())
      {
        LedSystem.On();
      }
  }
}

void CAutomation::HandleLedSystemPulseEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - Dac
//----------------------------------------------------------
//
void CAutomation::HandleDacRampBegin(CSerial &serial)
{
  SetTimeStamp();
  DACProcess.SetDacRampActual(0);//DACProcess.GetDacRampLow());
  DAConverter.SetValues(DACProcess.GetDacRampActual(), DACProcess.GetDacRampActual()); 
  SetState(saDacRampBusy);
}

void CAutomation::HandleDacRampBusy(CSerial &serial)
{  
  SetTimeMarker();  
  if (DACProcess.GetProcessPeriodms() <= GetTimePeriod())
  {
    SetTimeStamp();
    LedSystem.Toggle();
    if (DACProcess.IsPeriodCountReached())
    {
      SetState(saDacRampEnd);
    }
    else
    {
      DACProcess.IncrementDacRampActual();
      DAConverter.SetValues(DACProcess.GetDacRampActual(), DACProcess.GetDacRampActual());
    }
  }
}

void CAutomation::HandleDacRampEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - Collector
//----------------------------------------------------------
//
void CAutomation::Handle(CSerial &serial)
{
  switch (FState)
  { // Idle
    case saIdle:
      HandleIdle(serial);
      break;
    // LedSystem
    case saLedSystemPulseBegin:
      HandleLedSystemPulseBegin(serial);
      break;
    case saLedSystemPulseBusy:
      HandleLedSystemPulseBusy(serial);
      break;
    case saLedSystemPulseEnd:
      HandleLedSystemPulseEnd(serial);
      break;
    // Dac
    case saDacRampBegin:
      HandleDacRampBegin(serial);
      break;
    case saDacRampBusy:
      HandleDacRampBusy(serial);
      break;
    case saDacRampEnd:
      HandleDacRampEnd(serial);
      break;
  }  
}

