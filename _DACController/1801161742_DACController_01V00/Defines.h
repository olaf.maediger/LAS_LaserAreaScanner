//
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define UInt8 byte
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Float float
#define Double double
//
#define true 1
#define false 0
//
//----------------------------------
// ARDUINOUNO :
// #define PROCESSOR_ARDUINOUNO
//
//
//----------------------------------
// ARDUINOMEGA :
// #define PROCESSOR_ARDUINOMEGA
//
//----------------------------------
// ARDUINODUE :
#define PROCESSOR_ARDUINODUE
//
//----------------------------------
// STM32F103C8 : no DACs!
// #define PROCESSOR_STM32F103C8
//
//----------------------------------
// TEENSY32 :
// #define PROCESSOR_TEENSY32
//
//----------------------------------
// TEENSY36 :
// #define PROCESSOR_TEENSY36
//
//##########################################
//  PROCESSOR_ARDUINOUNO
//##########################################
#if defined(PROCESSOR_ARDUINOUNO)
#endif // PROCESSOR_ARDUINOUNO
//
//##########################################
//  PROCESSOR_ARDUINOMEGA
//##########################################
#if defined(PROCESSOR_ARDUINOMEGA)
//#define PIN_ADC0  54
//#define PIN_ADC1  55
//#define PIN_ADC2  56
//#define PIN_ADC3  57
//#define PIN_ADC4  58
//#define PIN_ADC5  59
//#define PIN_ADC6  60
//#define PIN_ADC7  61
//const int PIN_ADC8  
//const int PIN_ADC9  
//const int PIN_ADC10 
//const int PIN_ADC11 
//
#define PIN_DAC0  66
#define PIN_DAC1  67
//
#define PIN_PWM2  2
#define PIN_PWM3  3
#define PIN_PWM4  4 
#define PIN_PWM5  5
#define PIN_PWM6  6 
#define PIN_PWM7  7
#define PIN_PWM8  8
#define PIN_PWM9  9
#define PIN_PWM10 10 
#define PIN_PWM11 11
#define PIN_PWM12 12
// System Led 
const int PIN_LEDSYSTEM   = 13;
// LedLine
const int PIN_LEDERROR    = 54;
//                          55
const int PIN_LEDBUSY =     56;
//                          57
const int PIN_LEDMOTIONX  = 58;
const int PIN_LEDMOTIONY  = 59;
//                          60
const int PIN_LEDTRIGGER  = 61;
#endif // PROCESSOR_ARDUINOMEGA
//
//##########################################
//  PROCESSOR_ARDUINODUE
//##########################################
#if defined(PROCESSOR_ARDUINODUE)
//#define PIN_ADC0  54
//#define PIN_ADC1  55
//#define PIN_ADC2  56
//#define PIN_ADC3  57
//#define PIN_ADC4  58
//#define PIN_ADC5  59
//#define PIN_ADC6  60
//#define PIN_ADC7  61
//const int PIN_ADC8  
//const int PIN_ADC9  
//const int PIN_ADC10 
//const int PIN_ADC11 
//
#define PIN_DAC0  66
#define PIN_DAC1  67
//
//#define PIN_PWM2  2
//#define PIN_PWM3  3
//#define PIN_PWM4  4 
//#define PIN_PWM5  5
//#define PIN_PWM6  6 
//#define PIN_PWM7  7
//#define PIN_PWM8  8
//#define PIN_PWM9  9
//#define PIN_PWM10 10 
//#define PIN_PWM11 11
//#define PIN_PWM12 12
// System Led 
const int PIN_LEDSYSTEM   = 13;
// LedLine
const int PIN_LEDERROR    = 54;
//                          55
const int PIN_LEDBUSY =     56;
//                          57
const int PIN_LEDMOTIONX  = 58;
const int PIN_LEDMOTIONY  = 59;
//                          60
const int PIN_LEDLASER    = 50;
#endif // PROCESSOR_ARDUINODUE
//
//##########################################
//  PROCESSOR_STM32F103C8
//##########################################
#if defined(PROCESSOR_STM32F103C8)
#define PIN_ADC8  PA0
#define PIN_ADC9  PA1
#define PIN_ADC10 PA2
#define PIN_ADC11 PA3
//
#define PIN_PWM1  PA8
#define PIN_PWM2  PA9
#define PIN_PWM3  PA10
#define PIN_PWM4  PA11
#define PIN_PWM5  PB13
#define PIN_PWM6  PB14
#define PIN_PWM7  PB15
#define PIN_PWM8  PA15
#define PIN_PWM9  PB3
#define PIN_PWM10 PB10
#define PIN_PWM11 PB11
#define PIN_PWM12 PA6
#define PIN_PWM13 PA7
//
// Single Led System (fixed)
const int PIN_LEDSYSTEM   = PC13;
//
const int PIN_LEDERROR    = PC14;
//                          
const int PIN_LEDBUSY     = PC15;
//                          
const int PIN_LEDMOTIONX  = PB10;
const int PIN_LEDMOTIONY  = PB11;
//                          
const int PIN_LEDTRIGGER  = PB1;
//
#endif // PROCESSOR_STM32F103C8
//
//--------------------------------
//  Section - Constant - Error
//--------------------------------
//
// Init-Values - Global
#define INIT_ERRORCODE ecNone
//
//--------------------------------
//  Section - Constant - Process
//--------------------------------
// Init-Values - PulsePosition
const UInt32 INIT_PULSEPERIOD = 1000000; // [us]
const UInt32 INIT_PULSECOUNTPRESET = 0;
const UInt32 INIT_PULSECOUNTACTUAL = 0;
//const UInt32 INIT_POSITIONPULSECOUNTPRESET = 0;
//const UInt32 INIT_POSITIONPULSECOUNTACTUAL = 0;
//// Matrix
//const UInt32 INIT_MATRIXCOUNTREPETITIONS = 1;
//// Stabilization of Position after Moving [us]
//const UInt32 INIT_RANGEDELAYMOTION = 500;
//// Laser-Pulse-Period [us] 1ms -> 1KHz
//const UInt32 INIT_RANGEDELAYPULSE = 1000;
//// Init-Values - Position
//// Default 4 Positions in X
//const UInt32 INIT_XPOSITIONACTUAL = 2048;
//const UInt32 INIT_XPOSITIONDELTA = 1023;
//const UInt32 INIT_XPOSITIONMINIMUM = 0;
//const UInt32 INIT_XPOSITIONMAXIMUM = 4095;
//// Default 4 Positions in Y (total 16 Positions)
//const UInt32 INIT_YPOSITIONACTUAL = 2048;
//const UInt32 INIT_YPOSITIONDELTA = 1023;
//const UInt32 INIT_YPOSITIONMINIMUM = 0;
//const UInt32 INIT_YPOSITIONMAXIMUM = 4095;
//
#endif // Defines_h





