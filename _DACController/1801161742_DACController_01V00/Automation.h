#ifndef Automation_h
#define Automation_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
//
enum EStateAutomation
{ // Base
  saUndefined = -1,
  saIdle = 0,
  // LedSystem
  saLedSystemPulseBegin = 1,
  saLedSystemPulseBusy = 2,
  saLedSystemPulseEnd = 3
};
//
class CAutomation
{ // Field:
  private:
  EStateAutomation FState;
  UInt32 FTimeStamp;
  UInt32 FTimeMarker;
  //
  public:
  // Constructor
  CAutomation();
  // Property
  EStateAutomation GetState();
  void SetState(EStateAutomation state);
  // First: TimeStamp!!!
  inline void SetTimeStamp()
  {
    FTimeStamp = millis();
    FTimeMarker = FTimeStamp;
  }
  // Later: TimeMarker!!!
  inline void SetTimeMarker()
  {
    FTimeMarker = millis();
  }
  inline UInt32 GetTimePeriod()
  {
    return FTimeMarker - FTimeStamp;
  }
  // Management
  Boolean Open();
  Boolean Close();
  // Idle
  void HandleIdle(CSerial &serial);
  // LedLaser
  void HandleLedSystemPulseBegin(CSerial &serial);
  void HandleLedSystemPulseBusy(CSerial &serial);
  void HandleLedSystemPulseEnd(CSerial &serial);
  // Collector
  void Handle(CSerial &serial);
};
//
#endif // Automation_h
