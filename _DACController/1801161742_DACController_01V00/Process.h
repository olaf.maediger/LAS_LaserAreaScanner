#ifndef Process_h
#define Process_h
//
#include "Defines.h"
//
class CProcess
{
  private:
  UInt32 FPulsePeriod;       // [us]
  UInt32 FPulseCountPreset;   // [1]  Number of LaserPulses at one Position Preset
  UInt32 FPulseCountActual;   // [1]  Number of LaserPulses at one Position Actual
  //
  public:
  CProcess();
  //
  //  LedSystem
  //
  inline UInt32 GetPulsePeriod()
  {
    return FPulsePeriod;
  }
  inline void SetPulsePeriod(UInt32 value)
  {
    FPulsePeriod = value;
  }

  inline UInt32 GetPulseCountPreset()
  {
    return FPulseCountPreset;
  }
  inline void SetPulseCountPreset(UInt32 value)
  {
    FPulseCountPreset = value;
  }
  inline UInt32 GetPulseCountActual()
  {
    return FPulseCountActual;
  }
  inline void SetPulseCountActual(UInt32 value)
  {
    FPulseCountActual = value;
  }
  inline void IncrementPulseCountActual()
  {
    FPulseCountActual++;
  }
  inline Boolean IsPulseCountReached()
  {
    return (FPulseCountPreset <= FPulseCountActual);
  }
};
//
#endif // Process_h
