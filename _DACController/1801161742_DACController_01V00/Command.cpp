//
#include "Command.h"
#include "Led.h"
#include "Dac.h"
#include "Error.h"
#include "Process.h"
//
//-------------------------------------------
//  External Global Variables 
//-------------------------------------------
//  
extern CLed LedSystem;
extern CLed LedLaser;
extern CDac DigitalAnalogConverter;
extern CSerial SerialCommand;
extern CError DACError;
extern CProcess DACProcess;
extern CCommand DACCommand;
extern CAutomation DACAutomation;
//
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand()
{
  Init();
}

CCommand::~CCommand()
{
  Init();
}
//
//#########################################################
//  Segment - Helper
//#########################################################
//
void CCommand::ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}

void CCommand::ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}

void CCommand::ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdCommandLine[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Management
//#########################################################
//
void CCommand::Init()
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
}

Boolean CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CR:
        FRxdBuffer[FRxdBufferIndex] = ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FRxdCommandLine, FRxdBuffer);
        // debug serial.WriteText("<CR>true");
        // debug delay(1000);
        return true;
      case LF: // ignore
        // debug serial.WriteText("<LF>");
        // debug delay(1000);
        break;
      default: 
        // debug serial.WriteText("<");
        // debug serial.WriteCharacter(C);
        // debug serial.WriteText(">");
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean CCommand::Analyse(CSerial &serial)
{
  if (DetectRxdLine(serial))
  {
    char *PTerminal = " \t\r\n";
    char *PRxdCommandLine = FRxdCommandLine;
    FPRxdCommand = strtok(FRxdCommandLine, PTerminal);
    if (FPRxdCommand)
    {
      FRxdParameterCount = 0;
      char *PRxdParameter;
      while (PRxdParameter = strtok(0, PTerminal))
      {
        FPRxdParameters[FRxdParameterCount] = PRxdParameter;
        FRxdParameterCount++;
        if (COUNT_RXDPARAMETERS < FRxdParameterCount)
        {
          DACError.SetCode(ecToManyParameters);
          ZeroRxdBuffer();
          // debug serialtarget.WriteLine("error[ecToManyParameters]");
          serial.WriteNewLine();
          serial.WritePrompt();
          return false;
        }
      }  
//      debug sprintf(TxdBuffer, "\n\r# RxdCommand<%s>", PRxdCommand);
//      debug serial.WriteText(TxdBuffer);
//      if (0 < RxdParameterCount)
//      {
//        int II;
//        for (II = 0; II < RxdParameterCount; II++)
//        {
//          sprintf(TxdBuffer, " RxdParameter[%i]<%s>", II, PRxdParameters[II]);
//          serial.WriteText(TxdBuffer);
//        }
//      }
      //
      ZeroRxdBuffer();
      serial.WriteNewLine();
      return true;
    }
    ZeroRxdBuffer();
    serial.WriteNewLine();
    serial.WritePrompt();
  }
  return false;
}
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_VERSION);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void CCommand::WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_SOFTWAREVERSION);
  serial.WriteNewLine();
}

void CCommand::WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_HARDWAREVERSION);
  serial.WriteNewLine();
}

void CCommand::WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.WriteLine(HELP_H);
  serial.WriteAnswer(); serial.WriteLine(HELP_GPH);
  serial.WriteAnswer(); serial.WriteLine(HELP_GSV);
  serial.WriteAnswer(); serial.WriteLine(HELP_GHV);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LEDLASER);
  serial.WriteAnswer(); serial.WriteLine(HELP_GLL);
  serial.WriteAnswer(); serial.WriteLine(HELP_LLH);
  serial.WriteAnswer(); serial.WriteLine(HELP_LLL);
  serial.WriteAnswer(); serial.WriteLine(HELP_PLL);
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
void CCommand::ExecuteGetHelp(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(GetTxdBuffer(), ": %s %i", 
          GetPRxdCommand(), COUNT_HELPLINES);
  serial.WriteLine(GetTxdBuffer());         
  WriteHelp(serial);
  serial.WritePrompt();
}

void CCommand::ExecuteGetProgramHeader(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(GetTxdBuffer(), ": %s %i", 
          GetPRxdCommand(), COUNT_HEADERLINES);
  serial.WriteLine(GetTxdBuffer());   
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void CCommand::ExecuteGetSoftwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(GetTxdBuffer(), ": %s %i", 
          GetPRxdCommand(), COUNT_ANSWERLINE);
  serial.WriteLine(GetTxdBuffer());   
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  sprintf(GetTxdBuffer(), ": %s %i", 
          GetPRxdCommand(), COUNT_ANSWERLINE);
  serial.WriteLine(GetTxdBuffer());   
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LedSystem
//#########################################################
//
void CCommand::ExecuteGetLedSystem(CSerial &serial)
{ // Analyse parameters:
  // Execute:
  int State = LedSystem.GetState();
  // Response:
  sprintf(GetTxdBuffer(), ": %s %i", 
          GetPRxdCommand(), State);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteSwitchLedSystemOn(CSerial &serial)
{  // Analyse parameters:
  // Execute:
  LedSystem.On();
  // Response:
  sprintf(GetTxdBuffer(), ": %s", GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteSwitchLedSystemOff(CSerial &serial)
{ // Analyse parameters:
  // Execute:
  LedSystem.Off();
  // Response:
  sprintf(GetTxdBuffer(), ": %s", GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecutePulseLedSystem(CSerial &serial)
{  // Analyse parameters:
  UInt32 PulsePeriod = atol(GetPRxdParameters(0));
  UInt32 PulseCount = atol(GetPRxdParameters(1));
  // Execute:
  DACProcess.SetPulsePeriod(PulsePeriod);
  DACProcess.SetPulseCountActual(0);
  DACProcess.SetPulseCountPreset(PulseCount);
  // Switch ON Automation
  DACAutomation.SetState(saLedSystemPulseBegin);
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu %lu", 
          GetPRxdCommand(), PulsePeriod, PulseCount);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
void CCommand::CallbackStateAutomation(CSerial &serial, EStateAutomation stateautomation)
{
  switch (stateautomation)
  { // Base
    case saUndefined:
      break; 
    case saIdle:
      break; 
    // LedSystem
    case saLedSystemPulseBegin:
      break; 
    case saLedSystemPulseBusy:
      break; 
    case saLedSystemPulseEnd:
      break; 
    default:
      break;
  }  
  sprintf(DACCommand.GetTxdBuffer(), ": STA %i", stateautomation);
  serial.WriteLine(DACCommand.GetTxdBuffer());
  serial.WritePrompt();
}

Boolean CCommand::Execute(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
// debug serialtarget.WriteLine(TxdBuffer);
  if (!strcmp("H", GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp("GPH", GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp("GSV", GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp("GHV", GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  // ----------------------------------
  // LedLaser
  // ---------------------------------- 
  if (!strcmp("GLS", GetPRxdCommand()))
  {
    ExecuteGetLedSystem(serial);
    return true;
  } else 
  if (!strcmp("LSH", GetPRxdCommand()))
  {
    ExecuteSwitchLedSystemOn(serial);
    return true;
  } else   
  if (!strcmp("LSL", GetPRxdCommand()))
  {
    ExecuteSwitchLedSystemOff(serial);
    return true;
  } else   
  if (!strcmp("PLS", GetPRxdCommand()))
  {
    ExecutePulseLedSystem(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    DACError.SetCode(ecInvalidCommand);
  }
  return false;  
}

Boolean CCommand::Handle(CSerial &serial)
{
  if (Analyse(serial))
  {
    return Execute(serial);
  }  
  return false;
}



