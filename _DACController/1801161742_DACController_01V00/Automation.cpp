#include "Automation.h"
#include "Led.h"
#include "Dac.h"
#include "Process.h"
#include "Command.h"
//
extern CLed LedSystem;
extern CDac DigitalAnalogConverter;
extern CSerial SerialCommand;
extern CProcess DACProcess;
extern CCommand DACCommand;
//
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  //FTimeStart = millis();
  FTimeStamp = millis();
  FTimeMarker = FTimeStamp;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  if (state != FState)
  { // Message to Manager(PC):
    FState = state;
    DACCommand.CallbackStateAutomation(SerialCommand, FState);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CAutomation::Open()
{
  FState = saIdle;
  return true;
}

Boolean CAutomation::Close()
{
  FState = saUndefined;
  return true;
}
//
//----------------------------------------------------------
//  Segment - Automation - Idle
//----------------------------------------------------------
//
void CAutomation::HandleIdle(CSerial &serial)
{
  // do nothing
}
//
//----------------------------------------------------------
//  Segment - Automation - LedSystem
//----------------------------------------------------------
//
void CAutomation::HandleLedSystemPulseBegin(CSerial &serial)
{
  DACProcess.SetPulseCountActual(0);
//  LedSystem.SetPulsePeriodCount(DACProcess.GetLedSystemPulsePeriod(), 
//                                DACProcess.GetLedSystemPulseCountPreset());
  SetTimeStamp();  
  LedSystem.On(); 
  SetState(saLedSystemPulseBusy);
}
void CAutomation::HandleLedSystemPulseBusy(CSerial &serial)
{  
  SetTimeMarker();  
  if (LedSystem.GetPulsePeriodHalf() <= GetTimePeriod())
  {
    SetTimeStamp();
    if (slOn == LedSystem.GetState())
    {
      LedSystem.Off();
      DACProcess.IncrementPulseCountActual();
      if (DACProcess.IsPulseCountReached())
      {
        SetState(saLedSystemPulseEnd);
      }
    }
    else
      if (slOff == LedSystem.GetState())
      {
        LedSystem.On();
      }
  }
}
void CAutomation::HandleLedSystemPulseEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - Collector
//----------------------------------------------------------
//
void CAutomation::Handle(CSerial &serial)
{
  switch (FState)
  { // Idle
    case saIdle:
      HandleIdle(serial);
      break;
    // LedSyste
    case saLedSystemPulseBegin:
      HandleLedSystemPulseBegin(serial);
      break;
    case saLedSystemPulseBusy:
      HandleLedSystemPulseBusy(serial);
      break;
    case saLedSystemPulseEnd:
      HandleLedSystemPulseEnd(serial);
      break;
  }  
}

