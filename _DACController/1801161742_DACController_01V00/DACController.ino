#include "Defines.h"
#include "Led.h"
#include "Serial.h"
#include "Dac.h"
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Automation.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
CError DACError;
CProcess DACProcess;
CCommand DACCommand;
CAutomation DACAutomation;
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Dac
//---------------------------------------------------
//
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
CDac DigitalAnalogConverter(true, true);
#endif
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort!
CSerial SerialDebug(Serial1);
CSerial SerialCommand(Serial);
CSerial SerialPrinter(Serial2);
// Serial3 - NC
//
void setup() 
{ // Led 
  LedSystem.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.Off();
    delay(40);
    LedSystem.On();
    delay(40);
  }
  LedSystem.Off();
  //
  LedSystem.Open();
  LedSystem.Off();
  //
  SerialDebug.Open(115200, RXDECHO_ON);
  // LATER!!! 
  SerialCommand.Open(115200, RXDECHO_OFF);
  // debug:  SerialCommand.Open(115200, RXDECHO_ON);
  //
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.Open();
#endif  
  //
  DACCommand.WriteProgramHeader(SerialCommand);
  DACCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt();  
  DACAutomation.Open();
}

void loop() 
{ 
  DACError.Handle(SerialCommand);    
  DACCommand.Handle(SerialCommand);
  DACAutomation.Handle(SerialCommand);  
}

