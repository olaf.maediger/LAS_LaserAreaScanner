//
//--------------------------------
//  Library Max31865
//--------------------------------
//
#ifndef Max31865_h
#define Max31865_h
//
#include "Arduino.h"
#include "DriverMax31865.h"
#include "Defines.h"
//
const Double RTEMPERATUREZERO = 100.0;
const Double RREFERENCE       = 430.0;
//
//--------------------------------
//  Section - CMax31865
//--------------------------------
//
class CMax31865
{
  private:
  int FPinSpiCS;
  CDriverMAX31865* FPMax31865;
  //
  public:
  CMax31865(Byte pinspics);
  //
  Boolean Open(EWireCount wirecount);
  Boolean Close();
  //
  UInt16 ReadRTD();
  Double ReadRatio();
  Double ReadResistanceOhm();
  Double ReadTemperatureCelsius();
  Byte ReadFault();
};
//
#endif // Max31865
