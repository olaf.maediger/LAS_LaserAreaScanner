#ifndef Command_h
#define Command_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
#include "Process.h"
//
#define ARGUMENT_PROJECT    "MTC - MultiTemperatureController"
#define ARGUMENT_SOFTWARE   "01V05"
#define ARGUMENT_DATE       "180103"
#define ARGUMENT_TIME       "1025"
#define ARGUMENT_AUTHOR     "OM"
#define ARGUMENT_PORT       "SerialCommand (Serial-USB)"
#define ARGUMENT_PARAMETER  "115200, N, 8, 1"
//
#ifdef PROCESSOR_ARDUINOUNO
#define ARGUMENT_HARDWARE "ArduinoUnoR3"
#endif
#ifdef PROCESSOR_ARDUINOMEGA 
#define ARGUMENT_HARDWARE "ArduinoMega328"
#endif
#ifdef PROCESSOR_ARDUINODUE
#define ARGUMENT_HARDWARE "ArduinoDue3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
//
#define SHORT_H     "H"
#define SHORT_GPH   "GPH"
#define SHORT_GSV   "GSV"
#define SHORT_GHV   "GHV"
#define SHORT_SPC   "SPC"
#define SHORT_SPP   "SPP"
#define SHORT_SPE   "SPE"
//
#define SHORT_GLS   "GLS"
#define SHORT_LSH   "LSH"
#define SHORT_LSL   "LSL"
#define SHORT_BLS   "BLS"
//
#define SHORT_GTC   "GTC"
#define SHORT_GTI   "GTI"
#define SHORT_GAT   "GAT"
//
#define SHORT_RTC   "RTC"
#define SHORT_RTI   "RTI"
#define SHORT_RAT   "RAT"
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HEADER = 12;
//
#define TITLE_LINE            "-----------------------------------------------"
#define MASK_PROJECT          "- Project:   %-32s -"
#define MASK_SOFTWARE         "- Version:   %-32s -"
#define MASK_HARDWARE         "- Hardware:  %-32s -"
#define MASK_DATE             "- Date:      %-32s -"
#define MASK_TIME             "- Time:      %-32s -"
#define MASK_AUTHOR           "- Author:    %-32s -"
#define MASK_PORT             "- Port:      %-32s -"
#define MASK_PARAMETER        "- Parameter: %-32s -"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HELP = 21;
//
#define HELP_COMMON               " Help (Common):"
#define MASK_H                    " %-3s : This Help"
#define MASK_GPH                  " %-3s : Get Program Header"
#define MASK_GSV                  " %-3s : Get Software-Version"
#define MASK_GHV                  " %-3s : Get Hardware-Version"
#define MASK_SPC                  " %-3s <pc> : Set Process Count"
#define MASK_SPP                  " %-3s <pp> : Set Process Period [ms]"
#define MASK_SPE                  " %-3s : Stop Process Execution"
//
#define HELP_LEDSYSTEM            " Help (LedSystem):"
#define MASK_GLS                  " %-3s : Get State LedSystem"
#define MASK_LSH                  " %-3s : Switch LedSystem On"
#define MASK_LSL                  " %-3s : Switch LedSystem Off"
#define MASK_BLS                  " %-3s <p> <n> : Blink LedSystem with <p>eriod{ms} <n>times"
//
#define HELP_MEASUREMENTSINGLE    " Help (MeasurementSingle):"
#define MASK_GTC                  " %-3s <c> : Get Temperature Channel [0..7]"
#define MASK_GTI                  " %-3s <cl> <ch> : Get Temperature Interval"
#define MASK_GAT                  " %-3s : Get All Temperatures"
//
#define HELP_MEASUREMENTPERIODIC  " Help (MeasurementPeriodic):"
#define MASK_RTC                  " %-3s <c> : Repeat Temperature Channel [0..7]"
#define MASK_RTI                  " %-3s <cl> <ch> : Repeat Temperature Interval"
#define MASK_RAT                  " %-3s : Repeat All Temperatures"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_SOFTWAREVERSION = 1;
//
#define MASK_SOFTWAREVERSION      " Software-Version MTC%s"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HARDWAREVERSION = 1;
//
#define MASK_HARDWAREVERSION      " Hardware-Version %s"
//
//-----------------------------------------------------------------------------------------
//
// Command-Parameter
#define COUNT_RXDPARAMETERS 4
#define SIZE_RXDPARAMETER   8
//
//-----------------------------------------------------------------------------------------
//
class CCommand
{
  private:
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Character FRxdCommandLine[SIZE_RXDBUFFER];
  PCharacter FPRxdCommand;
  PCharacter FPRxdParameters[COUNT_RXDPARAMETERS];
  Int16 FRxdParameterCount = 0;
  Int16 FRxdBufferIndex = 0;

  public:
  CCommand();
  ~CCommand();
  //
  PCharacter GetPRxdParameters(UInt8 index)
  {
    return FPRxdParameters[index];
  }
  PCharacter GetTxdBuffer()
  {
    return FTxdBuffer;
  }
  PCharacter GetPRxdCommand()
  {
    return FPRxdCommand;
  }
  //
  void ZeroRxdBuffer();
  void ZeroTxdBuffer();
  void ZeroRxdCommandLine();
  //
  //  Segment - Execution - Basic Output
  void WriteProgramHeader(CSerial &serial);
  void WriteSoftwareVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
  void WriteHelp(CSerial &serial);
  //
  //  Segment - Execution - Common
  void ExecuteGetHelp(CSerial &serial);
  void ExecuteGetProgramHeader(CSerial &serial);
  void ExecuteGetSoftwareVersion(CSerial &serial);
  void ExecuteGetHardwareVersion(CSerial &serial);
  void ExecuteSetProcessCount(CSerial &serial);
  void ExecuteSetProcessPeriod(CSerial &serial);
  void ExecuteStopProcessExecution(CSerial &serial);
  //
  //  Segment - Execution - LedSystem
  void ExecuteGetLedSystem(CSerial &serial);
  void ExecuteLedSystemOn(CSerial &serial);
  void ExecuteLedSystemOff(CSerial &serial);
  void ExecuteBlinkLedSystem(CSerial &serial);
  //
  //  Segment - Execution - Measurement - Single
  void ExecuteGetTemperatureChannel(CSerial &serial);
  void ExecuteGetTemperatureInterval(CSerial &serial);
  void ExecuteGetAllTemperatures(CSerial &serial);
  //
  //  Segment - Execution - Measurement - Periodic
  void ExecuteSetCountMeasurement(CSerial &serial);
  void ExecuteSetPeriodMeasurement(CSerial &serial);
  void ExecuteRepeatMeasurementStop(CSerial &serial);
  void ExecuteRepeatTemperatureChannel(CSerial &serial);
  void ExecuteRepeatTemperatureInterval(CSerial &serial);
  void ExecuteRepeatAllTemperatures(CSerial &serial);
  //  
  void Init();
  Boolean DetectRxdLine(CSerial &serial);
  Boolean Analyse(CSerial &serial);
  void CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate);
  Boolean Execute(CSerial &serial);
  Boolean Handle(CSerial &serial);
};
//
#endif // Command_h
