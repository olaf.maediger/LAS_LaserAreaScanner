#include "Error.h"
#include "Command.h"
//
extern CCommand MTCCommand;
//
CError::CError()
{
  SetCode(INIT_ERRORCODE);
}

EErrorCode CError::GetCode()
{
  return FErrorCode;  
}
void CError::SetCode(EErrorCode errorcode)
{
  FErrorCode = errorcode;
}
  
Boolean CError::Open()
{
  return false; 
}
  
Boolean CError::Close()
{
  return false; 
}

Boolean CError::Handle(CSerial &serial)
{
  if (ecNone != FErrorCode)
  {
    serial.WriteAnswer();
    serial.Write((char*)" Error[");
    Int16ToAsciiDecimal(GetCode(), MTCCommand.GetTxdBuffer()); 
    serial.Write(MTCCommand.GetTxdBuffer());
    serial.Write((char*)"]: ");
    switch (GetCode())
    {
      case ecNone:
        serial.Write((char*)"No Error");
        break;
      case ecUnknown:
        serial.Write((char*)"Unknown");
        break;
      case ecInvalidCommand:
        serial.Write((char*)"Invalid Command");
        break;
      case ecToManyParameters:
        serial.Write((char*)"To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.Write((char*)"Missing Target Parameter");
        break;              
      default:
        serial.Write((char*)"Unknown Error");
        break;
    }
    serial.Write((char*)"!");
    serial.WriteNewLine();
    serial.WritePrompt();
    FErrorCode = ecNone;
    return true;
  }
  return false;
}
