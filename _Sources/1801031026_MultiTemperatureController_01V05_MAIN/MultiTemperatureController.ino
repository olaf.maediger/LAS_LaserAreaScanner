#include "Defines.h"
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Led.h"
#include "Serial.h"
#include "LCDisplayI2C.h"
#include "Max31865.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
CError MTCError;
CProcess MTCProcess;
CCommand MTCCommand;
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort und CommandPort!
CSerial SerialCommand(Serial);
// Serial1 - NC
// Serial2 - NC
// Serial3 - NC
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Display
//---------------------------------------------------
//
CLCDisplayI2C LCDisplay(LCD_ADDRESSI2C, LCD_COLCOUNT, LCD_ROWCOUNT);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Max31865
//---------------------------------------------------
//
CMax31865 Max31865T0(PIN_SPI_CS_TEMPERATURE0);
CMax31865 Max31865T1(PIN_SPI_CS_TEMPERATURE1);
CMax31865 Max31865T2(PIN_SPI_CS_TEMPERATURE2);
CMax31865 Max31865T3(PIN_SPI_CS_TEMPERATURE3);
CMax31865 Max31865T4(PIN_SPI_CS_TEMPERATURE4);
CMax31865 Max31865T5(PIN_SPI_CS_TEMPERATURE5);
CMax31865 Max31865T6(PIN_SPI_CS_TEMPERATURE6);
CMax31865 Max31865T7(PIN_SPI_CS_TEMPERATURE7);
//
//
//---------------------------------------------------
// Segment - Forward-Declaration
//---------------------------------------------------
//

//
//---------------------------------------------------
// Segment - Setup
//---------------------------------------------------
//
void setup() 
{ // Serial
  SerialCommand.Open(115200);
  // debug SerialCommand.SetRxdEcho(RXDECHO_ON);
  SerialCommand.SetRxdEcho(RXDECHO_OFF);
  delay(100);
  // Led
  LedSystem.Open();
  // Display
  LCDisplay.Open();
  // Command
  MTCCommand.WriteProgramHeader(SerialCommand);
  MTCCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  // Temperature-Sensor
  Max31865T0.Open(MAX31865_4WIRE);
  Max31865T1.Open(MAX31865_4WIRE);
  Max31865T2.Open(MAX31865_3WIRE);
  Max31865T3.Open(MAX31865_3WIRE);
  Max31865T4.Open(MAX31865_3WIRE);
  Max31865T5.Open(MAX31865_2WIRE);
  Max31865T6.Open(MAX31865_2WIRE);
  Max31865T7.Open(MAX31865_2WIRE);
  // Process 
  MTCProcess.Open();
  MTCProcess.SetState(spWelcome);
}
//
//---------------------------------------------------
// Segment - Loop
//---------------------------------------------------
//
void loop() 
{ 
  MTCError.Handle(SerialCommand);    
  MTCCommand.Handle(SerialCommand);
  MTCProcess.Handle(SerialCommand);  
}




