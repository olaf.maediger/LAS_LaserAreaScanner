//
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Float float
#define Double double
//
#define TRUE 1
#define FALSE 0
//
// Init-Values - Global
#define INIT_ERRORCODE ecNone
#define INIT_RXDECHO false
//
//----------------------------------
// ARDUINOUNO :
// #define PROCESSOR_ARDUINOUNO
//
//----------------------------------
// Arduino Mega 2560
// #define PROCESSOR_ARDUINOMEGA
//
//----------------------------------
// Arduino Due
// #define PROCESSOR_ARDUINODUE
//
//----------------------------------
// STM32F103C8 : no DACs!
// 
#define PROCESSOR_STM32F103C8
//
//----------------------------------
// Teensy 3.2
// #define PROCESSOR_TEENSY32
//
//----------------------------------
// Teensy 3.6
// #define PROCESSOR_TEENSY36
//
//----------------------------------
// Arduino Uno R3
// #define PROCESSOR_ARDUINOUNO
//
//##########################################
//  PROCESSOR_ARDUINOUNO
//##########################################
#if defined(PROCESSOR_ARDUINOUNO)
// SPI
const int PIN_SPI_CS_TEMPERATURE0 = 10;
const int PIN_SPI_CS_TEMPERATURE1 =  9;
const int PIN_SPI_CS_TEMPERATURE2 =  8;
const int PIN_SPI_CS_TEMPERATURE3 =  7;
const int PIN_SPI_CS_TEMPERATURE4 =  6;
const int PIN_SPI_CS_TEMPERATURE5 =  5;
const int PIN_SPI_CS_TEMPERATURE6 =  4;
const int PIN_SPI_CS_TEMPERATURE7 =  3;
// I2C
const int PIN_I2C_SCL = 19;
const int PIN_I2C_SDA = 18;
// System Led 
// used from SPI const int PIN_LEDSYSTEM = 13;
// const bool LEDSYSTEM_INVERTED = false;
//
#endif // PROCESSOR_ARDUINOUNO
////
////##########################################
////  PROCESSOR_ARDUINOMEGA
////##########################################
//#if defined(PROCESSOR_ARDUINOMEGA)
//// System Led 
//const int PIN_LEDSYSTEM   = 13;
//#endif // PROCESSOR_ARDUINOMEGA
////
////##########################################
////  PROCESSOR_ARDUINODUE
////##########################################
#if defined(PROCESSOR_ARDUINODUE)
// System Led 
const int PIN_LEDSYSTEM   = 13;
const bool LEDSYSTEM_INVERTED = false;
#endif // PROCESSOR_ARDUINODUE
//
//##########################################
//  PROCESSOR_STM32F103C8
//##########################################
#if defined(PROCESSOR_STM32F103C8)
// SPI
const int PIN_SPI_CS_TEMPERATURE0 = PB12;
const int PIN_SPI_CS_TEMPERATURE1 = PB13;
const int PIN_SPI_CS_TEMPERATURE2 = PB14;
const int PIN_SPI_CS_TEMPERATURE3 = PB5;
const int PIN_SPI_CS_TEMPERATURE4 = PB4;
const int PIN_SPI_CS_TEMPERATURE5 = PB3;
const int PIN_SPI_CS_TEMPERATURE6 = PA15;
const int PIN_SPI_CS_TEMPERATURE7 = PA12;
// I2C
const int PIN_I2C_SCL = PB6;
const int PIN_I2C_SDA = PB7;
// System Led 
const int PIN_LEDSYSTEM = PC13;
const bool LEDSYSTEM_INVERTED = true;
//
#endif // PROCESSOR_STM32F103C8
//
////
////##########################################
////  PROCESSOR_TEENSY32
////##########################################
#if defined(PROCESSOR_TEENSY32)
//
// Single Led System (fixed)
const int PIN_LEDSYSTEM   = 13;
const bool LEDSYSTEM_INVERTED = false;
//
#endif // PROCESSOR_TEENSY32
////
////##########################################
////  PROCESSOR_TEENSY36
////##########################################
//#if defined(PROCESSOR_TEENSY36)
////
//// Single Led System (fixed)
//const int PIN_LEDSYSTEM   = 13;
////
//#endif // PROCESSOR_TEENSY36
//
//--------------------------------
//  Section - Constant - Error
//--------------------------------
//
//--------------------------------
//  Section - Constant - Process
//--------------------------------
// Init-Values - PulsePosition
//const UInt32 INIT_LASERPULSEPERIOD = 1000000; // [us]
//
#endif // Defines_h





