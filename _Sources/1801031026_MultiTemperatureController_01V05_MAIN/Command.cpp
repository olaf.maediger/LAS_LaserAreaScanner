//
#include "Command.h"
#include "Led.h"
#include "Error.h"
#include "Process.h"
//
//-------------------------------------------
//  External Global Variables 
//-------------------------------------------
//  
extern CLed LedSystem;
extern CSerial SerialCommand;
extern CError MTCError;
extern CProcess MTCProcess;
extern CCommand MTCCommand;
//
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand()
{
  Init();
}

CCommand::~CCommand()
{
  Init();
}
//
//#########################################################
//  Segment - Helper
//#########################################################
//
void CCommand::ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}

void CCommand::ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}

void CCommand::ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdCommandLine[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Management
//#########################################################
//
void CCommand::Init()
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
}

Boolean CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CR:
        FRxdBuffer[FRxdBufferIndex] = ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FRxdCommandLine, FRxdBuffer);
        return true;
      case LF: // ignore
        break;
      default: 
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean CCommand::Analyse(CSerial &serial)
{
  if (DetectRxdLine(serial))
  {
    char *PTerminal = (char*)" \t\r\n";
    char *PRxdCommandLine = FRxdCommandLine;
    FPRxdCommand = strtok(FRxdCommandLine, PTerminal);
    if (FPRxdCommand)
    {
      FRxdParameterCount = 0;
      char *PRxdParameter;
      while (PRxdParameter = strtok(0, PTerminal))
      {
        FPRxdParameters[FRxdParameterCount] = PRxdParameter;
        FRxdParameterCount++;
        if (COUNT_RXDPARAMETERS < FRxdParameterCount)
        {
          MTCError.SetCode(ecToManyParameters);
          ZeroRxdBuffer();
          // debug serialtarget.WriteLine("error[ecToManyParameters]");
          serial.WriteNewLine();
          serial.WritePrompt();
          return false;
        }
      }  
//      debug sprintf(TxdBuffer, "\n\r# RxdCommand<%s>", PRxdCommand);
//      debug serial.Write(TxdBuffer);
//      if (0 < RxdParameterCount)
//      {
//        int II;
//        for (II = 0; II < RxdParameterCount; II++)
//        {
//          sprintf(TxdBuffer, " RxdParameter[%i]<%s>", II, PRxdParameters[II]);
//          serial.Write(TxdBuffer);
//        }
//      }
      //
      ZeroRxdBuffer();
      serial.WriteNewLine();
      return true;
    }
    ZeroRxdBuffer();
    serial.WriteNewLine();
    serial.WritePrompt();
  }
  return false;
}
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PROJECT, ARGUMENT_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_HARDWARE, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_DATE, ARGUMENT_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_TIME, ARGUMENT_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_AUTHOR, ARGUMENT_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PORT, ARGUMENT_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PARAMETER, ARGUMENT_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void CCommand::WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
}

void CCommand::WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
}

void CCommand::WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.WriteLine(MASK_H, SHORT_H);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPH, SHORT_GPH);
  serial.WriteAnswer(); serial.WriteLine(MASK_GSV, SHORT_GSV);
  serial.WriteAnswer(); serial.WriteLine(MASK_GHV, SHORT_GHV);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPC, SHORT_SPC);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPP, SHORT_SPP);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPE, SHORT_SPE);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LEDSYSTEM);
  serial.WriteAnswer(); serial.WriteLine(MASK_GLS, SHORT_GLS);
  serial.WriteAnswer(); serial.WriteLine(MASK_LSH, SHORT_LSH);
  serial.WriteAnswer(); serial.WriteLine(MASK_LSL, SHORT_LSL);
  serial.WriteAnswer(); serial.WriteLine(MASK_BLS, SHORT_BLS);
//
  serial.WriteAnswer(); serial.WriteLine(HELP_MEASUREMENTSINGLE);
  serial.WriteAnswer(); serial.WriteLine(MASK_GTC, SHORT_GTC);
  serial.WriteAnswer(); serial.WriteLine(MASK_GTI, SHORT_GTI);
  serial.WriteAnswer(); serial.WriteLine(MASK_GAT, SHORT_GAT);
//
  serial.WriteAnswer(); serial.WriteLine(HELP_MEASUREMENTPERIODIC);
  serial.WriteAnswer(); serial.WriteLine(MASK_RTC, SHORT_RTC);
  serial.WriteAnswer(); serial.WriteLine(MASK_RTI, SHORT_RTI);
  serial.WriteAnswer(); serial.WriteLine(MASK_RAT, SHORT_RAT);
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
void CCommand::ExecuteGetHelp(CSerial &serial)
{
  MTCProcess.SetState(spGetHelp);
  // Analyse parameters: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %i", 
          PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HELP);
  serial.WriteLine(GetTxdBuffer());         
  WriteHelp(serial);
  serial.WritePrompt();
}

void CCommand::ExecuteGetProgramHeader(CSerial &serial)
{
  MTCProcess.SetState(spGetProgramHeader);
  // Analyse parameters: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %i", 
          PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HEADER);
  serial.WriteLine(GetTxdBuffer());   
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void CCommand::ExecuteGetSoftwareVersion(CSerial &serial)
{
  MTCProcess.SetState(spGetSoftwareVersion);
  // Analyse parameters: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %i", 
          PROMPT_RESPONSE, GetPRxdCommand(), COUNT_SOFTWAREVERSION);
  serial.WriteLine(GetTxdBuffer());   
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{
  MTCProcess.SetState(spGetHardwareVersion);
  // Analyse parameters: -
  // Response
  sprintf(GetTxdBuffer(), "%s %s %i", 
          PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HARDWAREVERSION);
  serial.WriteLine(GetTxdBuffer());   
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}

void CCommand::ExecuteSetProcessCount(CSerial &serial)
{ 
  MTCProcess.SetState(spSetProcessCount);
  // Analyse parameters:
  UInt32 PC = atol(GetPRxdParameters(0));
  // Execute:
  MTCProcess.SetProcessCount(PC);
  PC = MTCProcess.GetProcessCount();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %lu", 
          PROMPT_RESPONSE, GetPRxdCommand(), PC);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteSetProcessPeriod(CSerial &serial)
{ 
  MTCProcess.SetState(spSetProcessPeriod);
  // Analyse parameters:
  UInt32 PP = atol(GetPRxdParameters(0));
  // Execute:
  MTCProcess.SetProcessPeriod(PP);
  PP = MTCProcess.GetProcessPeriod();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %lu", 
          PROMPT_RESPONSE, GetPRxdCommand(), PP);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteStopProcessExecution(CSerial &serial)
{ 
  MTCProcess.SetState(spStopProcessExecution);
  // Analyse parameters:
  // Execute:
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LedLaser
//#########################################################
//
void CCommand::ExecuteGetLedSystem(CSerial &serial)
{ 
  MTCProcess.SetState(spGetLedSystem);
  // Analyse parameters:
  // Execute:
  int State = LedSystem.GetState();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %i", 
          PROMPT_RESPONSE, GetPRxdCommand(), State);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteLedSystemOn(CSerial &serial)
{ 
  MTCProcess.SetState(spLedSystemOn);
  // Analyse parameters:
  // Execute:
  LedSystem.On();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteLedSystemOff(CSerial &serial)
{ 
  MTCProcess.SetState(spLedSystemOff);
  // Analyse parameters:
  // Execute:
  LedSystem.Off();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteBlinkLedSystem(CSerial &serial)
{ 
  MTCProcess.SetState(spBlinkLedSystem);
  // Analyse parameters:
  UInt32 Period = atol(GetPRxdParameters(0));
  UInt32 Count = atol(GetPRxdParameters(1));
  // Execute:
  MTCProcess.SetProcessPeriod(Period);
  MTCProcess.SetProcessCount(Count);
  MTCProcess.SetProcessIndex(0);
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
          PROMPT_RESPONSE, GetPRxdCommand(), Period, Count);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Measurement - Single
//#########################################################
//
void CCommand::ExecuteGetTemperatureChannel(CSerial &serial)
{ 
  MTCProcess.SetState(spGetTemperatureChannel);
  // Analyse parameters:
  UInt32 CI = atol(GetPRxdParameters(0));
  // Execute:
  MTCProcess.SetProcessCount(1);
  MTCProcess.SetProcessIndex(0);
  MTCProcess.SetChannelLow(CI);
  MTCProcess.SetChannelHigh(CI);  
  CI = MTCProcess.GetChannelLow();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %u", PROMPT_RESPONSE, GetPRxdCommand(), CI);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteGetTemperatureInterval(CSerial &serial)
{ 
  MTCProcess.SetState(spGetTemperatureInterval);
  // Analyse parameters:
  UInt8 CL = atoi(GetPRxdParameters(0));
  UInt8 CH = atoi(GetPRxdParameters(1));
  // Execute:
  MTCProcess.SetProcessCount(1);
  MTCProcess.SetProcessIndex(0);
  MTCProcess.SetChannelLow(CL);
  MTCProcess.SetChannelHigh(CH);
  CL = MTCProcess.GetChannelLow();
  CH = MTCProcess.GetChannelHigh();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %u %u", PROMPT_RESPONSE, GetPRxdCommand(), CL, CH);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteGetAllTemperatures(CSerial &serial)
{ 
  MTCProcess.SetState(spGetAllTemperatures);
  // Analyse parameters:
  // Execute:
  MTCProcess.SetProcessCount(1);
  MTCProcess.SetProcessIndex(0);
  MTCProcess.SetChannelLow(0);
  MTCProcess.SetChannelHigh(7);
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Measurement - Periodic
//#########################################################
//
void CCommand::ExecuteRepeatTemperatureChannel(CSerial &serial)
{ 
  MTCProcess.SetState(spRepeatTemperatureChannel);
  // Analyse parameters:
  UInt8 CI = atoi(GetPRxdParameters(0));
  // Execute:
  MTCProcess.SetProcessIndex(0);
  MTCProcess.SetChannelLow(CI);
  MTCProcess.SetChannelHigh(CI);
  CI = MTCProcess.GetChannelLow();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %u", PROMPT_RESPONSE, GetPRxdCommand(), CI);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteRepeatTemperatureInterval(CSerial &serial)
{ 
  MTCProcess.SetState(spRepeatTemperatureInterval);
  // Analyse parameters:
  UInt8 CL = atoi(GetPRxdParameters(0));
  UInt8 CH = atoi(GetPRxdParameters(1));
  // Execute:
  MTCProcess.SetProcessIndex(0);
  MTCProcess.SetChannelLow(CL);
  MTCProcess.SetChannelHigh(CH);
  CL = MTCProcess.GetChannelLow();
  CH = MTCProcess.GetChannelHigh();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %u %u", PROMPT_RESPONSE, GetPRxdCommand(), CL, CH);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteRepeatAllTemperatures(CSerial &serial)
{ 
  MTCProcess.SetState(spRepeatAllTemperatures);
  // Analyse parameters:
  // Execute:
  MTCProcess.SetProcessIndex(0);
  MTCProcess.SetChannelLow(0);
  MTCProcess.SetChannelHigh(7);
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
void CCommand::CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate)
{
//  switch (stateprocess)
//  { // Base
//    case spUndefined:
//      break; 
//    case spIdle:
//      break; 
//    default:
//      break;
//  }  
//  serial.WriteNewLine();
//  serial.WritePrompt();
  sprintf(MTCCommand.GetTxdBuffer(), MASK_STATEPROCESS, PROMPT_EVENT, stateprocess, substate);
  serial.WriteLine(MTCCommand.GetTxdBuffer());
  serial.WritePrompt();
}

Boolean CCommand::Execute(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
// debug Serial.WriteLine(TxdBuffer);
  if (!strcmp(SHORT_H, GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPH, GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GSV, GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GHV, GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPC, GetPRxdCommand()))
  {
    ExecuteSetProcessCount(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPP, GetPRxdCommand()))
  {
    ExecuteSetProcessPeriod(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPE, GetPRxdCommand()))
  {
    ExecuteStopProcessExecution(serial);
    return true;
  } else 
  // ----------------------------------
  // LedSystem
  // ---------------------------------- 
  if (!strcmp(SHORT_GLS, GetPRxdCommand()))
  {
    ExecuteGetLedSystem(serial);
    return true;
  } else 
  if (!strcmp(SHORT_LSH, GetPRxdCommand()))
  {
    ExecuteLedSystemOn(serial);
    return true;
  } else   
  if (!strcmp(SHORT_LSL, GetPRxdCommand()))
  {
    ExecuteLedSystemOff(serial);
    return true;
  } else   
  if (!strcmp(SHORT_BLS, GetPRxdCommand()))
  {
    ExecuteBlinkLedSystem(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - Single
  // ----------------------------------
  if (!strcmp(SHORT_GTC, GetPRxdCommand()))
  {
    ExecuteGetTemperatureChannel(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GTI, GetPRxdCommand()))
  {
    ExecuteGetTemperatureInterval(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GAT, GetPRxdCommand()))
  {
    ExecuteGetAllTemperatures(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - Periodic
  // ----------------------------------
  if (!strcmp(SHORT_RTC, GetPRxdCommand()))
  {
    ExecuteRepeatTemperatureChannel(serial);
    return true;
  } else   
  if (!strcmp(SHORT_RTI, GetPRxdCommand()))
  {
    ExecuteRepeatTemperatureInterval(serial);
    return true;
  } else   
  if (!strcmp(SHORT_RAT, GetPRxdCommand()))
  {
    ExecuteRepeatAllTemperatures(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    MTCError.SetCode(ecInvalidCommand);
  }
  return false;  
}

Boolean CCommand::Handle(CSerial &serial)
{
  if (Analyse(serial))
  {
    return Execute(serial);
  }  
  return false;
}



