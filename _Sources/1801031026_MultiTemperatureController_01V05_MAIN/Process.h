#ifndef Process_h
#define Process_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
//
const UInt32 INIT_PROCESSCOUNT = 1000;    // []
const UInt32 INIT_PROCESSPERIOD = 1000;   // [ms]
const Boolean INIT_MEASUREMENTRESPONSE = true;
//
#define MASK_STATEPROCESS "%s STP %i %i"
//
//
enum EStateProcess
{ // Common
  spUndefined = -1,
  spIdle = 0,
  spWelcome = 1,
  spGetHelp = 2,
  spGetProgramHeader = 3,
  spGetSoftwareVersion = 4,
  spGetHardwareVersion = 5,
  spSetProcessCount = 6,
  spSetProcessPeriod = 7,
  spStopProcessExecution = 8,
  // LedSystem
  spGetLedSystem = 9,  
  spLedSystemOn = 10,  
  spLedSystemOff = 11,  
  spBlinkLedSystem = 12,  
  // Measurement
  spGetTemperatureChannel = 13,
  spGetTemperatureInterval = 14,
  spGetAllTemperatures = 15,
  spRepeatTemperatureChannel = 16,
  spRepeatTemperatureInterval = 17,
  spRepeatAllTemperatures = 18
};
//
class CProcess
{ //
  //  Segment - Field
  //
  private:
  // Common
  EStateProcess FState;
  UInt8 FSubstate;
  UInt32 FStateTick;
  UInt32 FTimeStamp; // [ms]
  UInt32 FTimeMark; // [ms]
  UInt32 FProcessCount;
  UInt32 FProcessIndex;
  UInt32 FProcessPeriod; // [ms]
  UInt32 FChannelLow, FChannelHigh;
  Boolean FMeasurementResponse;
  //
  void LedSystemState();
  void MeasureTemperatures(CSerial &serial);
  //
  public:
  CProcess();
  // Property
  EStateProcess GetState();
  void SetState(EStateProcess stateprocess, UInt8 substate = 0);
  // Measurement - Time
  // First: TimeStamp!!!
  inline void SetTimeStamp()
  {
    FTimeStamp = millis();
    FTimeMark = FTimeStamp;
  }
  // Later: TimeMarker!!!
  inline void SetTimeMark()
  {
    FTimeMark = millis();
  }
  inline UInt32 GetTimeSpan()
  {
    return FTimeMark - FTimeStamp;
  }

  inline void SetProcessCount(UInt32 count)
  {
    FProcessCount = count;
  }
  inline UInt32 GetProcessCount(void)
  {
    return FProcessCount;
  }

  inline void SetProcessIndex(UInt32 index)
  {
    FProcessIndex = index;
  }
  inline UInt32 GetProcessIndex(void)
  {
    return FProcessIndex;
  }

  inline void SetProcessPeriod(UInt32 period)
  {
    FProcessPeriod = period;
  }
  inline UInt32 GetProcessPeriod(void)
  {
    return FProcessPeriod;
  }

  inline UInt32 GetChannelLow()
  {
    return FChannelLow;
  }
  inline void SetChannelLow(UInt32 value)
  {
    FChannelLow = value;
  }

  inline UInt32 GetChannelHigh()
  {
    return FChannelHigh;
  }
  inline void SetChannelHigh(UInt32 value)
  {
    FChannelHigh = value;
  }

  inline Boolean IsMeasurementResponseEnabled()
  {
    return FMeasurementResponse;
  }
  // Management
  Boolean Open();
  Boolean Close();
  private:
  // System
  void HandleUndefined(CSerial &serial);
  void HandleIdle(CSerial &serial);
  void HandleWelcome(CSerial &serial);
  void HandleGetHelp(CSerial &serial);
  void HandleGetProgramHeader(CSerial &serial);
  void HandleGetSoftwareVersion(CSerial &serial);
  void HandleGetHardwareVersion(CSerial &serial);
  void HandleSetProcessCount(CSerial &serial);
  void HandleSetProcessPeriod(CSerial &serial);
  void HandleStopProcessExecution(CSerial &serial);
  // LedSystem
  void HandleGetLedSystem(CSerial &serial);
  void HandleLedSystemOn(CSerial &serial);
  void HandleLedSystemOff(CSerial &serial);
  void HandleBlinkLedSystem(CSerial &serial);
  // Measurement
  void HandleGetTemperatureChannel(CSerial &serial);
  void HandleGetTemperatureInterval(CSerial &serial);
  void HandleGetAllTemperatures(CSerial &serial);
  void HandleRepeatTemperatureChannel(CSerial &serial);
  void HandleRepeatTemperatureInterval(CSerial &serial);
  void HandleRepeatAllTemperatures(CSerial &serial);
  public:
  // Collector
  void Handle(CSerial &serial);
};
//
#endif // Process_h
