#include "Led.h"
#include "Process.h"
#include "Command.h"
#include "LCDisplayI2C.h"
#include "Max31865.h"
//
extern CLed LedSystem;
extern CSerial SerialCommand;
extern CProcess MTCProcess;
extern CCommand MTCCommand;
extern CLCDisplayI2C LCDisplay;
extern CMax31865 Max31865T0;
extern CMax31865 Max31865T1;
extern CMax31865 Max31865T2;
extern CMax31865 Max31865T3;
extern CMax31865 Max31865T4;
extern CMax31865 Max31865T5;
extern CMax31865 Max31865T6;
extern CMax31865 Max31865T7;
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CProcess::CProcess()
{
  FState = spUndefined;
  FStateTick = 0;
  FTimeStamp = millis();
  FTimeMark = FTimeStamp;
  FProcessCount = INIT_PROCESSCOUNT;
  FProcessPeriod = INIT_PROCESSPERIOD;
  FProcessIndex = 0;
  FMeasurementResponse = INIT_MEASUREMENTRESPONSE;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateProcess CProcess::GetState()
{
  return FState;
}
void CProcess::SetState(EStateProcess state, UInt8 substate)
{
  if ((state != FState) || (substate != FSubstate))
  {     
    if (state != FState)
    {
      FStateTick = 0;
    }
    FState = state;
    FSubstate = substate;
    switch (FState)
    {
      case spStopProcessExecution:
        break;
    }
    // Message to Manager(PC):
    MTCCommand.CallbackStateProcess(SerialCommand, FState, substate);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CProcess::Open()
{
  SetState(spIdle);
  return true;
}

Boolean CProcess::Close()
{
  SetState(spUndefined);
  return true;
}

//
//----------------------------------------------------------
//  Segment - Common
//----------------------------------------------------------
//
void CProcess::HandleUndefined(CSerial &serial)
{
  delay(1000);
}

void CProcess::HandleIdle(CSerial &serial)
{ // do nothing
  SetTimeStamp();
}

void CProcess::HandleWelcome(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:        
      SetTimeStamp();
      FStateTick++;
      LCDisplay.ClearScreen();
      break;
    case 1: case 3: case 5: 
      if (330 < GetTimeSpan())
      {
        SetTimeStamp();
        FStateTick++;        
        LCDisplay.SetBacklightOff();
      }
      break;
    case 2: case 4: case 6:  
      if (330 < GetTimeSpan())
      {
        SetTimeStamp();
        FStateTick++;        
        LCDisplay.SetBacklightOn();
      }
      break;
    case 7:  
      FStateTick++;      
      LCDisplay.ClearScreen();
      LCDisplay.SetCursorPosition(0, 0); 
      LCDisplay.Write("*** MTC");
      LCDisplay.SetCursorPosition(1, 0);
      LCDisplay.Write("M - Multi ");
      LCDisplay.SetCursorPosition(2, 0);
      LCDisplay.Write("T - Temperature");
      LCDisplay.SetCursorPosition(3, 0);
      LCDisplay.Write("C - Controller");
      SetTimeStamp();      
      break;
    case 8:   
      if (1000 < GetTimeSpan())
      {   
        FStateTick++;
        LCDisplay.ClearScreen();
        LCDisplay.SetCursorPosition(0, 0);
        LCDisplay.Write("*** MTC");
        LCDisplay.SetCursorPosition(1, 0);
        LCDisplay.Write("Version.: 00V11");
        LCDisplay.SetCursorPosition(2, 0);
        LCDisplay.Write("DateTime: 1712290602");
        LCDisplay.SetCursorPosition(3, 0);
        LCDisplay.Write("Author..: OM");
        SetTimeStamp();
      }
      break;
    case 9:   
      if (1000 < GetTimeSpan())
      {   
        FStateTick++;
        LCDisplay.ClearScreen();
        // alternative : SetState(spIdle);
        UInt8 CL = 0;
        UInt8 CH = 7;
        MTCProcess.SetChannelLow(CL);
        MTCProcess.SetChannelHigh(CH);
        MTCProcess.SetState(spRepeatAllTemperatures);
        MTCProcess.SetProcessIndex(0);
        SetState(spRepeatAllTemperatures);
        SetTimeStamp();
      }
      break;
  }
}

void CProcess::HandleGetHelp(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProgramHeader(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSoftwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetHardwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessCount(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessPeriod(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleStopProcessExecution(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - LedSystem
//----------------------------------------------------------
//
void CProcess::LedSystemState()
{
  LCDisplay.ClearScreen();
  LCDisplay.SetCursorPosition(0, 0);        
  LCDisplay.Write("LedSystem:");
  LCDisplay.SetCursorPosition(1, 0);        
  switch (LedSystem.GetState())
  {
    case slOn:
      LCDisplay.Write("State - On");
      break;
    case slOff:
      LCDisplay.Write("State - Off");
      break;
    default:
      LCDisplay.Write("State - Undefined");
      break;
  }
  SetState(spIdle, LedSystem.GetState());
}

void CProcess::HandleGetLedSystem(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOn(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOff(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleBlinkLedSystem(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:              
      LCDisplay.ClearScreen();
      FStateTick++;
      SetTimeStamp();
      break;
    case 1: 
      if (GetProcessPeriod() < GetTimeSpan())
      {
        LCDisplay.ClearScreen();
        LCDisplay.SetCursorPosition(0, 0);        
        LCDisplay.Write("LedSystem blinking:");
        LCDisplay.SetCursorPosition(1, 0);        
        LCDisplay.Write("State - On");
        char Buffer[20];
        sprintf(Buffer, "Step %u / %u", 1 + FProcessIndex, FProcessCount); 
        LCDisplay.SetCursorPosition(2, 0);        
        LCDisplay.Write(Buffer);
        LedSystem.On();
        SetState(spBlinkLedSystem, 1);
        FStateTick = 2;
        SetTimeStamp();
      }
      break;
    case 2: 
      if (GetProcessPeriod() < GetTimeSpan())
      {
        LCDisplay.ClearScreen();
        LCDisplay.SetCursorPosition(0, 0);
        LCDisplay.Write("LedSystem blinking:");
        LCDisplay.SetCursorPosition(1, 0);
        LCDisplay.Write("State - Off");
        LedSystem.Off();
        SetState(spBlinkLedSystem, 0);
        char Buffer[20];
        sprintf(Buffer, "Step %u / %u", 1 + FProcessIndex, FProcessCount); 
        LCDisplay.SetCursorPosition(2, 0);        
        LCDisplay.Write(Buffer);
        FProcessIndex++;
        if (FProcessIndex < FProcessCount)
        {
          FStateTick = 1;          
        }
        else
        {
          SetState(spIdle);
        }
        SetTimeStamp(); 
      }
      break;
  } 
}
//
//----------------------------------------------------------
// Segment - Measurement - Helper
//----------------------------------------------------------
//
void CProcess::MeasureTemperatures(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:              
      LCDisplay.ClearScreen();
      LCDisplay.SetCursorPosition(0, 0);
      LCDisplay.Write("0:      C 1:      C");
      LCDisplay.SetCursorPosition(1, 0);
      LCDisplay.Write("2:      C 3:      C");
      LCDisplay.SetCursorPosition(2, 0);
      LCDisplay.Write("4:      C 5:      C");
      LCDisplay.SetCursorPosition(3, 0);
      LCDisplay.Write("6:      C 7:      C");
      FStateTick++;
      SetTimeStamp();
      break;
    case 1: 
      if ((0 == GetProcessIndex()) || (GetProcessPeriod() < GetTimeSpan()))
      { 
        Double Temperature;
        Byte Fault;
        String SValue;
        char Buffer[SIZE_TXDBUFFER];
        if (MTCProcess.IsMeasurementResponseEnabled())
        { 
          serial.WriteNewLine();
          serial.WritePrompt();
          sprintf(Buffer, "%s T[%u-%u]", PROMPT_EVENT, GetChannelLow(), GetChannelHigh());
          serial.Write(Buffer);
        }  
        //
        //-------------------------------------- 
        // T0
        //-------------------------------------- 
        if ((FChannelLow <= 0) && (0 <= FChannelHigh))
        {
          LCDisplay.SetCursorPosition(0, 0);
          LCDisplay.Write("*");
          Temperature = Max31865T0.ReadTemperatureCelsius();
          Fault = Max31865T0.ReadFault();
          if (0 < Fault)
          {
            SValue = String(Fault, HEX);
            SValue = "E[" + SValue + "]!";
          }
          else
          {
            SValue = TemperatureDoubleString(Temperature);
          }
          LCDisplay.SetCursorPosition(0, 2);
          LCDisplay.Write(SValue);    
          if (MTCProcess.IsMeasurementResponseEnabled())
          { 
            sprintf(Buffer, " %s", SValue.c_str());
            serial.Write(Buffer); 
          }  
        }
        //
        //-------------------------------------- 
        // T1
        //-------------------------------------- 
        if ((FChannelLow <= 1) && (1 <= FChannelHigh))
        {
          LCDisplay.SetCursorPosition(0, 10);
          LCDisplay.Write("*");
          Temperature = Max31865T1.ReadTemperatureCelsius();
          Fault = Max31865T1.ReadFault();
          if (0 < Fault)
          {
            SValue = String(Fault, HEX);
            SValue = "E[" + SValue + "]!";
          }
          else
          {
            SValue = TemperatureDoubleString(Temperature);
          }
          LCDisplay.SetCursorPosition(0, 12);
          LCDisplay.Write(SValue);    
          if (MTCProcess.IsMeasurementResponseEnabled())
          { 
            sprintf(Buffer, " %s", SValue.c_str());
            serial.Write(Buffer); 
          }  
        }
        //
        //-------------------------------------- 
        // T2
        //-------------------------------------- 
        if ((FChannelLow <= 2) && (2 <= FChannelHigh))
        {
          LCDisplay.SetCursorPosition(1, 0);
          LCDisplay.Write("*");
          Temperature = Max31865T2.ReadTemperatureCelsius();
          Fault = Max31865T2.ReadFault();
          if (0 < Fault)
          {
            SValue = String(Fault, HEX);
            SValue = "E[" + SValue + "]!";
          }
          else
          {
            SValue = TemperatureDoubleString(Temperature);
          }
          LCDisplay.SetCursorPosition(1, 2);
          LCDisplay.Write(SValue);   
          if (MTCProcess.IsMeasurementResponseEnabled())
          { 
            sprintf(Buffer, " %s", SValue.c_str());
            serial.Write(Buffer); 
          }  
        }
        //
        //-------------------------------------- 
        // T3
        //-------------------------------------- 
        if ((FChannelLow <= 3) && (3 <= FChannelHigh))
        {
          LCDisplay.SetCursorPosition(1, 10);
          LCDisplay.Write("*");
          Temperature = Max31865T3.ReadTemperatureCelsius();
          Fault = Max31865T3.ReadFault();
          if (0 < Fault)
          {
            SValue = String(Fault, HEX);
            SValue = "E[" + SValue + "]!";
          }
          else
          {
            SValue = TemperatureDoubleString(Temperature);
          }
          LCDisplay.SetCursorPosition(1, 12);
          LCDisplay.Write(SValue);    
          if (MTCProcess.IsMeasurementResponseEnabled())
          { 
            sprintf(Buffer, " %s", SValue.c_str());
            serial.Write(Buffer); 
          } 
        } 
        //
        //-------------------------------------- 
        // T4
        //-------------------------------------- 
        if ((FChannelLow <= 4) && (4 <= FChannelHigh))
        {
          LCDisplay.SetCursorPosition(2, 0);
          LCDisplay.Write("*");
          Temperature = Max31865T4.ReadTemperatureCelsius();
          Fault = Max31865T4.ReadFault();
          if (0 < Fault)
          {
            SValue = String(Fault, HEX);
            SValue = "E[" + SValue + "]!";
          }
          else
          {
            SValue = TemperatureDoubleString(Temperature);
          }
          LCDisplay.SetCursorPosition(2, 2);
          LCDisplay.Write(SValue);    
          if (MTCProcess.IsMeasurementResponseEnabled())
          { 
            sprintf(Buffer, " %s", SValue.c_str());
            serial.Write(Buffer); 
          }  
        }
        //
        //-------------------------------------- 
        // T5
        //-------------------------------------- 
        if ((FChannelLow <= 5) && (5 <= FChannelHigh))
        {
          LCDisplay.SetCursorPosition(2, 10);
          LCDisplay.Write("*");
          Temperature = Max31865T5.ReadTemperatureCelsius();
          Fault = Max31865T5.ReadFault();
          if (0 < Fault)
          {
            SValue = String(Fault, HEX);
            SValue = "E[" + SValue + "]!";
          }
          else
          {
            SValue = TemperatureDoubleString(Temperature);
          }
          LCDisplay.SetCursorPosition(2, 12);
          LCDisplay.Write(SValue);    
          if (MTCProcess.IsMeasurementResponseEnabled())
          { 
            sprintf(Buffer, " %s", SValue.c_str());
            serial.Write(Buffer); 
          }  
        }
        //
        //-------------------------------------- 
        // T6
        //-------------------------------------- 
        if ((FChannelLow <= 6) && (6 <= FChannelHigh))
        {
          LCDisplay.SetCursorPosition(3, 0);
          LCDisplay.Write("*");
          Temperature = Max31865T6.ReadTemperatureCelsius();
          Fault = Max31865T6.ReadFault();
          if (0 < Fault)
          {
            SValue = String(Fault, HEX);
            SValue = "E[" + SValue + "]!";
          }
          else
          {
            SValue = TemperatureDoubleString(Temperature);
          }
          LCDisplay.SetCursorPosition(3, 2);
          LCDisplay.Write(SValue);    
          if (MTCProcess.IsMeasurementResponseEnabled())
          { 
            sprintf(Buffer, " %s", SValue.c_str());
            serial.Write(Buffer); 
          } 
        } 
        //
        //-------------------------------------- 
        // T7
        //-------------------------------------- 
        if ((FChannelLow <= 7) && (7 <= FChannelHigh))
        {
          LCDisplay.SetCursorPosition(3, 10);
          LCDisplay.Write("*");
          Temperature = Max31865T7.ReadTemperatureCelsius();
          Fault = Max31865T7.ReadFault();
          if (0 < Fault)
          {
            SValue = String(Fault, HEX);
            SValue = "E[" + SValue + "]!";
          }
          else
          {
            SValue = TemperatureDoubleString(Temperature);
          }
          LCDisplay.SetCursorPosition(3, 12);
          LCDisplay.Write(SValue);    
          if (MTCProcess.IsMeasurementResponseEnabled())
          { 
            sprintf(Buffer, " %s", SValue.c_str());
            serial.Write(Buffer); 
          }
        }  
        //
        LCDisplay.SetCursorPosition(0, 0);
        LCDisplay.Write("0");
        LCDisplay.SetCursorPosition(0, 10);
        LCDisplay.Write("1");
        LCDisplay.SetCursorPosition(1, 0);
        LCDisplay.Write("2");
        LCDisplay.SetCursorPosition(1, 10);
        LCDisplay.Write("3");
        LCDisplay.SetCursorPosition(2, 0);
        LCDisplay.Write("4");
        LCDisplay.SetCursorPosition(2, 10);
        LCDisplay.Write("5");
        LCDisplay.SetCursorPosition(3, 0);
        LCDisplay.Write("6");
        LCDisplay.SetCursorPosition(3, 10);
        LCDisplay.Write("7");
        // 
        FProcessIndex++;
        if (FProcessCount <= FProcessIndex)
        {
          SerialCommand.WriteNewLine();
          SetState(spIdle);
        }
        SetTimeStamp(); 
      }
      break;
  } 
}
//
//----------------------------------------------------------
// Segment - Measurement
//----------------------------------------------------------
//
void CProcess::HandleGetTemperatureChannel(CSerial &serial)
{
  MeasureTemperatures(serial);
}

void CProcess::HandleGetTemperatureInterval(CSerial &serial)
{
  MeasureTemperatures(serial);
}

void CProcess::HandleGetAllTemperatures(CSerial &serial)
{
  MeasureTemperatures(serial);
}

void CProcess::HandleRepeatTemperatureChannel(CSerial &serial)
{
  MeasureTemperatures(serial);
}

void CProcess::HandleRepeatTemperatureInterval(CSerial &serial)
{
  MeasureTemperatures(serial);
}

void CProcess::HandleRepeatAllTemperatures(CSerial &serial)
{
  MeasureTemperatures(serial);  
}
//
//----------------------------------------------------------
//  Segment - Collector
//----------------------------------------------------------
//
void CProcess::Handle(CSerial &serial)
{
  switch (MTCProcess.GetState())
  { // Common
    case spIdle:
      HandleIdle(serial);
      break;
    case spWelcome:
      HandleWelcome(serial);
      break;
    case spGetHelp:
      HandleGetHelp(serial);
      break;
    case spGetProgramHeader:
      HandleGetProgramHeader(serial);
      break;
    case spGetSoftwareVersion:
      HandleGetSoftwareVersion(serial);
      break;
    case spGetHardwareVersion:
      HandleGetHardwareVersion(serial);
      break;
    case spSetProcessCount:
      HandleSetProcessCount(serial);
      break;
    case spSetProcessPeriod:
      HandleSetProcessPeriod(serial);
      break;
    case spStopProcessExecution:
      HandleStopProcessExecution(serial);
      break;
    // LedSystem
    case spGetLedSystem:
      HandleGetLedSystem(serial);
      break;
    case spLedSystemOn:
      HandleLedSystemOn(serial);
      break;
    case spLedSystemOff:
      HandleLedSystemOff(serial);
      break;
    case spBlinkLedSystem:
      HandleBlinkLedSystem(serial);
      break;
    // Measurement - Single
    case spGetTemperatureChannel:
      HandleGetTemperatureChannel(serial);
      break;
    case spGetTemperatureInterval:
      HandleGetTemperatureInterval(serial);
      break;
    case spGetAllTemperatures:
      HandleGetAllTemperatures(serial);
      break;
    // Measurement - Periodic
    case spRepeatTemperatureChannel:
      HandleRepeatTemperatureChannel(serial);
      break;
    case spRepeatTemperatureInterval:
      HandleRepeatTemperatureInterval(serial);
      break;
    case spRepeatAllTemperatures:
      HandleRepeatAllTemperatures(serial);
      break;
    default: // spUndefined
      HandleUndefined(serial);
      break;
  }  
}
