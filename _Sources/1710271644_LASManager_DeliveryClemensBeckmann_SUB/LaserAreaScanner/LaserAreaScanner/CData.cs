﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LaserAreaScanner
{
  public class CData1D
  {
    protected String FName;
    protected Double FX, FValue;
    //
    public CData1D(String name, Double x, Double value)
    {
      FName = name;
      FX = x;
      FValue = value;
    }
    //
    public Double X
    {
      get { return FX; }
      set { FX = value; }
    }
    public Double Value
    {
      get { return FValue; }
      set { FValue = value; }
    }
  }

  public class CData2D : CData1D
  {
    protected Double FY;
    //
    public CData2D(String name, Double x, Double y, Double value)
      : base(name, x, value)
    {
      FY = y;
    }
    //
    public Double Y
    {
      get { return FY; }
      set { FY = value; }
    }
  }

  public class CData3D : CData2D
  {
    protected Double FZ;
    //
    public CData3D(String name, Double x, Double y, Double z, Double value)
      : base(name, x, y, value)
    {
      FZ = z;
    }
    //
    public Double Z
    {
      get { return FZ; }
      set { FZ = value; }
    }
  }

  public class CData1DVector : List<CData1D>
  {
  }

  public class CData2DVector : List<CData2D>
  {
  }

  public class CData3DVector : List<CData3D>
  {
  }

  public class CData2DMatrix : List<CData2DVector>
  {
  }

  public class CData3DTensor : List<CData3DVector>
  {
  }

}
