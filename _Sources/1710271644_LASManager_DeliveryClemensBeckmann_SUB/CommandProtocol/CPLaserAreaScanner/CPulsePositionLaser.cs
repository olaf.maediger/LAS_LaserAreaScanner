﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using CommandProtocol;
//
namespace CPLaserAreaScanner
{
  public class CPulsePositionLaser : CCommand
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const String HEADER = "PPL";
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private Int32 FPositionX, FPositionY, FPeriod, FCount;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CPulsePositionLaser(Int32 positionx, Int32 positiony, 
                               Int32 period, Int32 count)
      : base(HEADER)
    {
      FPositionX = positionx;
      FPositionY = positiony;
      FPeriod = period;
      FCount = count;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public override string GetCommandHeader()
    {
      return FHeader;
    }

    public override string GetCommandText()
    {
      return String.Format("{0} {1} {2} {3} {4}", FHeader, FPositionX, FPositionY, FPeriod, FCount);
    }


  }
}