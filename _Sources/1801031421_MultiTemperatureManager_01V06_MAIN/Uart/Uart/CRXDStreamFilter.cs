using System;
using System.Collections.Generic;
using System.Text;

namespace Uart
{ //
	// prinzipiell konzipiert als 1:1-Filter
	//
	public class CRXDStreamFilter : CStreamFilter
	{
		//
		//------------------------
		//	Constructor
		//------------------------
		//
		public CRXDStreamFilter():
			base()
		{ 
		}
		//
		//------------------------
		//	Task
		//------------------------
		//
		public override String Execute(Char newcharacter)
		{
			return base.Execute(newcharacter);
		}
		public override String Execute(String newtext)
		{
			return base.Execute(newtext);
		}

	}
}
