﻿namespace UCTemperature
{
  partial class CUCTemperatureSegment
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblHeader = new System.Windows.Forms.Label();
      this.lblUnit = new System.Windows.Forms.Label();
      this.lblValue = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Left;
      this.lblHeader.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(53, 72);
      this.lblHeader.TabIndex = 1;
      this.lblHeader.Text = "T0";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblUnit
      // 
      this.lblUnit.BackColor = System.Drawing.SystemColors.Info;
      this.lblUnit.Dock = System.Windows.Forms.DockStyle.Right;
      this.lblUnit.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblUnit.Location = new System.Drawing.Point(294, 0);
      this.lblUnit.Name = "lblUnit";
      this.lblUnit.Size = new System.Drawing.Size(56, 72);
      this.lblUnit.TabIndex = 3;
      this.lblUnit.Text = "°C";
      this.lblUnit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblValue
      // 
      this.lblValue.BackColor = System.Drawing.SystemColors.Info;
      this.lblValue.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblValue.Font = new System.Drawing.Font("Consolas", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblValue.Location = new System.Drawing.Point(53, 0);
      this.lblValue.Name = "lblValue";
      this.lblValue.Size = new System.Drawing.Size(241, 72);
      this.lblValue.TabIndex = 4;
      this.lblValue.Text = "+123.4";
      this.lblValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCTemperatureSegment
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.Controls.Add(this.lblValue);
      this.Controls.Add(this.lblUnit);
      this.Controls.Add(this.lblHeader);
      this.Name = "CUCTemperatureSegment";
      this.Size = new System.Drawing.Size(350, 72);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Label lblUnit;
    private System.Windows.Forms.Label lblValue;
  }
}
