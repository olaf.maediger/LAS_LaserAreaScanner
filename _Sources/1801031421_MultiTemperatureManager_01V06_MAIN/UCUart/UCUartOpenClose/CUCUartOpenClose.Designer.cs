﻿namespace UCUartOpenClose
{
  partial class CUCUartOpenClose
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnRefresh = new System.Windows.Forms.Button();
      this.pnlControls = new System.Windows.Forms.Panel();
      this.FUCPortName = new UCUartOpenClose.CUCPortName();
      this.FUCBaudrate = new UCUartOpenClose.CUCBaudrate();
      this.FUCParity = new UCUartOpenClose.CUCParity();
      this.FUCStopbits = new UCUartOpenClose.CUCStopbits();
      this.FUCDatabits = new UCUartOpenClose.CUCDatabits();
      this.FUCHandshake = new UCUartOpenClose.CUCHandshake();
      this.FUCOpenClose = new UCUartOpenClose.CUCOpenClose();
      this.FUCPortsSelectable = new UCUartOpenClose.CUCPortsSelectable();
      this.FUCPortsInstalled = new UCUartOpenClose.CUCPortsInstalled();
      this.pnlControls.SuspendLayout();
      this.SuspendLayout();
      // 
      // btnRefresh
      // 
      this.btnRefresh.BackColor = System.Drawing.Color.LavenderBlush;
      this.btnRefresh.Location = new System.Drawing.Point(7, 3);
      this.btnRefresh.Name = "btnRefresh";
      this.btnRefresh.Size = new System.Drawing.Size(53, 23);
      this.btnRefresh.TabIndex = 20;
      this.btnRefresh.Text = "Refresh";
      this.btnRefresh.UseVisualStyleBackColor = false;
      this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
      // 
      // pnlControls
      // 
      this.pnlControls.Controls.Add(this.FUCPortName);
      this.pnlControls.Controls.Add(this.FUCBaudrate);
      this.pnlControls.Controls.Add(this.FUCParity);
      this.pnlControls.Controls.Add(this.FUCStopbits);
      this.pnlControls.Controls.Add(this.FUCDatabits);
      this.pnlControls.Controls.Add(this.FUCHandshake);
      this.pnlControls.Location = new System.Drawing.Point(0, 29);
      this.pnlControls.Name = "pnlControls";
      this.pnlControls.Size = new System.Drawing.Size(335, 78);
      this.pnlControls.TabIndex = 21;
      // 
      // FUCPortName
      // 
      this.FUCPortName.Location = new System.Drawing.Point(4, 3);
      this.FUCPortName.Name = "FUCPortName";
      this.FUCPortName.Size = new System.Drawing.Size(328, 21);
      this.FUCPortName.TabIndex = 16;
      // 
      // FUCBaudrate
      // 
      this.FUCBaudrate.Location = new System.Drawing.Point(5, 27);
      this.FUCBaudrate.Name = "FUCBaudrate";
      this.FUCBaudrate.Size = new System.Drawing.Size(147, 21);
      this.FUCBaudrate.TabIndex = 11;
      // 
      // FUCParity
      // 
      this.FUCParity.Location = new System.Drawing.Point(244, 27);
      this.FUCParity.Name = "FUCParity";
      this.FUCParity.Size = new System.Drawing.Size(90, 21);
      this.FUCParity.TabIndex = 12;
      // 
      // FUCStopbits
      // 
      this.FUCStopbits.Location = new System.Drawing.Point(5, 54);
      this.FUCStopbits.Name = "FUCStopbits";
      this.FUCStopbits.Size = new System.Drawing.Size(85, 21);
      this.FUCStopbits.TabIndex = 13;
      // 
      // FUCDatabits
      // 
      this.FUCDatabits.Location = new System.Drawing.Point(157, 27);
      this.FUCDatabits.Name = "FUCDatabits";
      this.FUCDatabits.Size = new System.Drawing.Size(82, 21);
      this.FUCDatabits.TabIndex = 14;
      // 
      // FUCHandshake
      // 
      this.FUCHandshake.Location = new System.Drawing.Point(95, 54);
      this.FUCHandshake.Name = "FUCHandshake";
      this.FUCHandshake.Size = new System.Drawing.Size(135, 21);
      this.FUCHandshake.TabIndex = 15;
      // 
      // FUCOpenClose
      // 
      this.FUCOpenClose.BackColor = System.Drawing.Color.Red;
      this.FUCOpenClose.Location = new System.Drawing.Point(235, 81);
      this.FUCOpenClose.Name = "FUCOpenClose";
      this.FUCOpenClose.Size = new System.Drawing.Size(99, 23);
      this.FUCOpenClose.TabIndex = 22;
      // 
      // FUCPortsSelectable
      // 
      this.FUCPortsSelectable.Location = new System.Drawing.Point(210, 5);
      this.FUCPortsSelectable.Name = "FUCPortsSelectable";
      this.FUCPortsSelectable.Size = new System.Drawing.Size(124, 24);
      this.FUCPortsSelectable.TabIndex = 19;
      // 
      // FUCPortsInstalled
      // 
      this.FUCPortsInstalled.ImeMode = System.Windows.Forms.ImeMode.AlphaFull;
      this.FUCPortsInstalled.Location = new System.Drawing.Point(65, 5);
      this.FUCPortsInstalled.Name = "FUCPortsInstalled";
      this.FUCPortsInstalled.Size = new System.Drawing.Size(139, 24);
      this.FUCPortsInstalled.TabIndex = 18;
      // 
      // CUCUartOpenClose
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCOpenClose);
      this.Controls.Add(this.btnRefresh);
      this.Controls.Add(this.pnlControls);
      this.Controls.Add(this.FUCPortsSelectable);
      this.Controls.Add(this.FUCPortsInstalled);
      this.Name = "CUCUartOpenClose";
      this.Size = new System.Drawing.Size(335, 107);
      this.pnlControls.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnRefresh;
    private UCUartOpenClose.CUCPortsSelectable FUCPortsSelectable;
    private UCUartOpenClose.CUCPortsInstalled FUCPortsInstalled;
    private UCUartOpenClose.CUCPortName FUCPortName;
    private UCUartOpenClose.CUCHandshake FUCHandshake;
    private UCUartOpenClose.CUCDatabits FUCDatabits;
    private UCUartOpenClose.CUCStopbits FUCStopbits;
    private UCUartOpenClose.CUCParity FUCParity;
    private UCUartOpenClose.CUCBaudrate FUCBaudrate;
    private System.Windows.Forms.Panel pnlControls;
    private CUCOpenClose FUCOpenClose;
  }
}
