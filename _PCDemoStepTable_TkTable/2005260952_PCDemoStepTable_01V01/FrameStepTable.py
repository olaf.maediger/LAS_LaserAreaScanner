#
# ------------------------------------------------------------------
#   PCDemoTable - FrameStepTable
# ------------------------------------------------------------------
#   Version: 00V01
#   Date   : 200525
#   Time   : 0852
#   Author : OMDevelop
#
import tkinter as tk
from tksheet import Sheet
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION                = "FrameStepTable"
#
# NAME_PULSEPERIOD_MS             = "PulsePeriod[ms]"
#
# INIT_PULSEPERIOD_MS             = "1000"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameStepTable(tk.Frame):
#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 880, height = 269, \
                          padx = 6, pady = 6, background = "#ffffee")
        self.grid_propagate(0)
        #--------------------------------------------------------------
        # Controls...
        #--------------------------------------------------------------
        super().grid_columnconfigure(0, weight = 1)
        super().grid_rowconfigure(0, weight = 1)
        self.sheet = Sheet(self,
                           #empty_vertical = 0,
                           #empty_horizontal = 0,
                           show_vertical_grid = True,#False,
                           #show_horizontal_grid = False,
                           #auto_resize_numerical_row_index = False,
                           header_height = "1",
                           #row_index_width = 100,
                           align = "center",
                           #header_align = "w",
                            #row_index_align = "w",
                            theme = "green",
                            data = [[f"R{r}C{c}" for c in range(6)] for r in range(1000)], 
                           #data = [[1,2,3,4,5], [1,2,3]],
                            #headers = [f"Column {c}\nnewline1\nnewline2" for c in range(30)],
                            #row_index = [f"Row {r}\nnewline1\nnewline2" for r in range(2000)],
                            #set_all_heights_and_widths = True, #to fit all cell sizes to text at start up
                            #headers = 0, #to set headers as first row at startup
                            #headers = [f"Column {c}\nnewline1\nnewline2" for c in range(30)],
                           #header_background = "black",
                            #row_index = 0, #to set row_index as first column at startup
                            #total_rows = 2000, #if you want to set empty sheet dimensions at startup
                            #total_columns = 30, #if you want to set empty sheet dimensions at startup
                            height = 140, #height and width arguments are optional
                            width = 120 #For full startup arguments see DOCUMENTATION.md
                            )
        #self.sheet.hide("row_index")
        #self.sheet.hide("header")
        #self.sheet.hide("top_left")
        self.sheet.enable_bindings(("single_select", #"single_select" or "toggle_select"
                                    "drag_select",   #enables shift click selection as well
                                    "column_drag_and_drop",
                                    "row_drag_and_drop",
                                    "column_select",
                                    "row_select",
                                    "column_width_resize",
                                    "double_click_column_resize",
                                    #"row_width_resize",
                                    #"column_height_resize",
                                    "arrowkeys",
                                    "row_height_resize",
                                    "double_click_row_resize",
                                    "right_click_popup_menu",
                                    "rc_select",
                                    "rc_insert_column",
                                    "rc_delete_column",
                                    "rc_insert_row",
                                    "rc_delete_row",
                                    "copy",
                                    "cut",
                                    "paste",
                                    "delete",
                                    "undo",
                                    "edit_cell",
                                    ""))
        self.sheet.enable_bindings("enable_all")
        #self.sheet.disable_bindings() #uses the same strings
        #self.bind("<Configure>", self.window_resized)
        self.sheet.set_all_cell_sizes_to_text()
        super().grid(row = 0, column = 0, sticky = "wnse")
        self.sheet.grid(row = 0, column = 0, sticky = "wnse")
        
        # """_________________________ EXAMPLES _________________________ """
        # """_____________________________________________________________"""

        # __________ CHANGING THEME __________

        #self.sheet.change_theme("dark")

        # __________ HIGHLIGHT / DEHIGHLIGHT CELLS __________
        
        #self.sheet.highlight_cells(row = 5, column = 5, bg = "#ed4337", fg = "white")
        #self.sheet.highlight_cells(row = 5, column = 1, bg = "#ed4337", fg = "white")
        #self.sheet.highlight_cells(row = 5, bg = "#ed4337", fg = "white", canvas = "row_index")
        #self.sheet.highlight_cells(column = 0, bg = "#ed4337", fg = "white", canvas = "header")

        # __________ DISPLAY SUBSET OF COLUMNS __________

        #self.sheet.display_subset_of_columns(indexes = [1, 0, 2], enable = True) #any order

        # __________ DATA AND DISPLAY DIMENSIONS __________

        #self.sheet.total_rows(4) #will delete rows if set to less than current data rows
        #self.sheet.total_columns(2) #will delete columns if set to less than current data columns
        #self.sheet.sheet_data_dimensions(total_rows = 4, total_columns = 2)
        #self.sheet.sheet_display_dimensions(total_rows = 4, total_columns = 6) #currently resets widths and heights
        #self.sheet.set_sheet_data_and_display_dimensions(total_rows = 4, total_columns = 2) #currently resets widths and heights

        # __________ SETTING OR RESETTING TABLE DATA __________

        #.set_sheet_data() function returns the object you use as argument
        #verify checks if your data is a list of lists, raises error if not
        #self.data = self.sheet.set_sheet_data([[f"Row {r} Column {c}" for c in range(30)] for r in range(2000)], verify = False)

        # __________ SETTING ROW HEIGHTS AND COLUMN WIDTHS __________

        #self.sheet.set_cell_data(0, 0, "\n".join([f"Line {x}" for x in range(500)]))
        #self.sheet.set_column_data(1, ("" for i in range(2000)))
        #self.sheet.row_index((f"Row {r}" for r in range(2000))) #any iterable works
        #self.sheet.row_index("\n".join([f"Line {x}" for x in range(500)]), 2)
        self.sheet.column_width(0, width = 110)
        self.sheet.column_width(1, width = 110)
        self.sheet.column_width(2, width = 110)
        self.sheet.column_width(3, width = 110)
        self.sheet.column_width(4, width = 110)
        self.sheet.column_width(5, width = 110)
        #self.sheet.row_height(row = 0, height = 12)
        #self.sheet.set_column_widths([120 for c in range(30)])
        #self.sheet.set_row_heights([30 for r in range(2000)])
        #self.sheet.set_all_column_widths()
        #self.sheet.set_all_row_heights()
        #self.sheet.set_all_cell_sizes_to_text()
        
        # __________ BINDING A FUNCTIONS TO USER ACTIONS __________

        self.sheet.extra_bindings([("cell_select", self.cell_select),
                                    #("begin_edit_cell", self.begin_edit_cell),
                                ("shift_cell_select", self.shift_select_cells),
                                ("drag_select_cells", self.drag_select_cells),
                                ("ctrl_a", self.ctrl_a),
                                ("row_select", self.row_select),
                                ("shift_row_select", self.shift_select_rows),
                                ("drag_select_rows", self.drag_select_rows),
                                ("column_select", self.column_select),
                                ("shift_column_select", self.shift_select_columns),
                                ("drag_select_columns", self.drag_select_columns),
                                ("deselect", self.deselect)
                                ])
                                  
        
        #self.sheet.extra_bindings([("cell_select", None)]) #unbind cell select
        #self.sheet.extra_bindings("unbind_all") #remove all functions set by extra_bindings()

        # __________ BINDING NEW RIGHT CLICK FUNCTION __________
    
        self.sheet.bind("<3>", self.rc)

        # __________ SETTING HEADERS __________
        HeaderList = ["Index[1]", "PositionX[stp]", "PositionY[stp]", "PulseCount[1]", "PulsePeriod[ms]", "DelayMotion[ms]"]
        self.sheet.headers((f"{c}" for c in HeaderList))
        self.sheet.headers("StepIndex[1]", 0)
        self.sheet.headers("PositionX[stp]", 1)
        self.sheet.headers("PositionY[stp]", 2)
        self.sheet.headers("PulseCount[1]", 3)
        self.sheet.headers("PulsePeriod[ms]", 4)
        self.sheet.headers("DelayMotion[ms]", 5)
        print(self.sheet.headers())
        print(self.sheet.headers(index = 2))

        # # __________ SETTING ROW INDEX __________
        # self.sheet.row_index((f"Row {r}" for r in range(2000))) #any iterable works
        # self.sheet.row_index("Change index example", 2)
        # print (self.sheet.row_index())
        # print (self.sheet.row_index(index = 2))

        # __________ INSERTING A ROW __________
        #self.sheet.insert_row(values = (f"my new row here {c}" for c in range(30)), idx = 0) # a filled row at the start
        #self.sheet.insert_row() # an empty row at the end

        # __________ INSERTING A COLUMN __________
        #self.sheet.insert_column(values = (f"my new col here {r}" for r in range(2050)), idx = 0) # a filled column at the start
        #self.sheet.insert_column() # an empty column at the end

        # __________ SETTING A COLUMNS DATA __________
        # any iterable works
        #self.sheet.set_column_data(0, values = (i for i in range(2050)))

        # __________ SETTING A ROWS DATA __________
        # any iterable works
        #self.sheet.set_row_data(0, values = (0 for i in range(35)))

        # __________ SETTING A CELLS DATA __________        
        RowCount = self.sheet.total_rows()
        ColCount = self.sheet.total_columns()
        SPulsePeriod = str(10)
        PulseCount = 10
        DelayCell = 10
        DelayLine = 155
        DelayMotion = DelayLine
        IY = 0
        IX = 0
        for RI in range(0, RowCount):
            Index = RI
            PositionX = 1800 + 100 * IX 
            PositionY = 1800 + 100 * IY
            self.sheet.set_cell_data(RI, 0, str(Index))
            self.sheet.set_cell_data(RI, 1, str(PositionX))
            self.sheet.set_cell_data(RI, 2, str(PositionY))
            #
            if (0 == IX):
                DelayMotion = DelayLine
            else:
                DelayMotion = DelayCell
            IX += 1
            PulseCount += 10
            if (10 < IX):
                IX = 0                
                PulseCount = 100
                IY += 1
                if (5 < IY):
                    IY = 0
            self.sheet.set_cell_data(RI, 3, str(PulseCount))
            self.sheet.set_cell_data(RI, 4, SPulsePeriod)
            self.sheet.set_cell_data(RI, 5, str(DelayMotion))

        # # __________ GETTING FULL SHEET DATA __________

        # #self.all_data = self.sheet.get_sheet_data()

        # # __________ GETTING CELL DATA __________

        # #print (self.sheet.get_cell_data(0, 0))

        # # __________ GETTING ROW DATA __________

        # #print (self.sheet.get_row_data(0)) # only accessible by index

        # # __________ GETTING COLUMN DATA __________

        # #print (self.sheet.get_column_data(0)) # only accessible by index

        # # __________ GETTING SELECTED __________

        # #print (self.sheet.get_currently_selected())
        # #print (self.sheet.get_selected_cells())
        # #print (self.sheet.get_selected_rows())
        # #print (self.sheet.get_selected_columns())
        # #print (self.sheet.get_selection_boxes())
        # #print (self.sheet.get_selection_boxes_with_types())

        # # __________ SETTING SELECTED __________

        # #self.sheet.deselect("all")
        # #self.sheet.create_selection_box(0, 0, 2, 2, type_ = "cells") #type here is "cells", "cols" or "rows"
        # #self.sheet.set_currently_selected(0, 0)
        # #self.sheet.set_currently_selected("row", 0)
        # #self.sheet.set_currently_selected("column", 0)

        # # __________ CHECKING SELECTED __________

        # #print (self.sheet.is_cell_selected(0, 0))
        # #print (self.sheet.is_row_selected(0))
        # #print (self.sheet.is_column_selected(0))
        # #print (self.sheet.anything_selected())

        # # __________ HIDING THE ROW INDEX AND HEADERS __________

        # #self.sheet.hide("row_index")
        # #self.sheet.hide("top_left")
        # #self.sheet.hide("header")

        # # __________ ADDITIONAL BINDINGS __________

    #"""
    #UNTIL DOCUMENTATION IS COMPLETE, PLEASE BROWSE THE FILE
    #_tksheet.py FOR A FULL LIST OF FUNCTIONS AND THEIR PARAMETERS
    #"""
    
    def begin_edit_cell(self, event):
        pass

    def window_resized(self, event):
        pass
        #print (event)

    def mouse_motion(self, event):
        region = self.sheet.identify_region(event)
        row = self.sheet.identify_row(event, allow_end = False)
        column = self.sheet.identify_column(event, allow_end = False)
        print (region, row, column)

    def deselect(self, event):
        print (event, self.sheet.get_selected_cells())

    def rc(self, event):
        print (event)
        
    def cell_select(self, response):
        #print (response)
        pass

    def shift_select_cells(self, response):
        print (response)

    def drag_select_cells(self, response):
        pass
        #print (response)

    def ctrl_a(self, response):
        print (response)

    def row_select(self, response):
        print (response)

    def shift_select_rows(self, response):
        print (response)

    def drag_select_rows(self, response):
        pass
        #print (response)
        
    def column_select(self, response):
        print (response)
        #for i in range(50):
        #    self.sheet.create_dropdown(i, response[1], values=[f"{i}" for i in range(200)], set_value="100",
        #                               destroy_on_select = False, destroy_on_leave = False, see = False)
        #print (self.sheet.get_cell_data(0, 0))
        #self.sheet.refresh()

    def shift_select_columns(self, response):
        print (response)

    def drag_select_columns(self, response):
        pass
        #print (response)
        
    #print(f"Header {c}" for c in range(30))

    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    # def SetCBOnSetPulsePeriod(self, callback):
    #     self.CBOnSetPulsePeriod = callback
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    # def OnSetPulsePeriod(self):
    #     if (self.CBOnSetPulsePeriod):
    #         #self.RemoteSwitchOn = True
    #         PP = self.spbPulsePeriod.get()
    #         self.CBOnSetPulsePeriod(PP)
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        # PP = readinitdata.ReadValueInit(INITDATA_SECTION, \
        #                                 NAME_PULSEPERIOD_MS, \
        #                                 INIT_PULSEPERIOD_MS)
        # self.spbPulsePeriod.delete(0, tk.END)
        # self.OnSet...
        return True
        #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        # writeinitdata.WriteValue(INITDATA_SECTION, \
        #                          NAME_PULSEPERIOD_MS, \
        #                          self.spbPulsePeriod.get())
        return True
        #
    #
    #---------------------------------------------------------------
    # Management - Event/Response...
    #--------------------------------------------------------------    
