#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - UartClient
# ------------------------------------------------------------------
#   Version: 01V04
#   Date   : 200303
#   Time   : 0948
#   Author : OMDevelop
#
"""project: ..."""
import threading
import time
import serial
import serial.tools.list_ports
#
import sys
import glob
#
# ------------------------------------------------------------------
#   Global Constant
# ------------------------------------------------------------------
#
COMPORTS =   ["COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", 
              "COM10", "COM11", "COM12", "COM13", "COM14", "COM15", "COM16", "COM17", 
              "COM18", "COM19", "COM20", "COM21", "COM22", "COM23", "COM24", "COM25", 
              "COM26", "COM27", "COM28", "COM29"]
#
BAUDRATES =  ["50", "75", "110", "134", "150", "200", "300", "600", "1200", "1800", 
              "2400", "4800", "9600", "19200", "38400", "57600", "115200", "230400", 
              "460800", "500000", "576000", "921600", "1000000", "1152000", "1500000", 
              "2000000", "2500000", "3000000", "3500000", "4000000"]
INDEX_BAUDRATE_DEFAULT = 16
#
PARITIES =   ["NONE", "EVEN", "ODD", "MARK", "SPACE"]
INDEX_PARITY_DEFAULT = 0
#
DATABITS =   ["FIVEBITS", "SIXBITS", "SEVENBITS", "EIGHTBITS"]
INDEX_DATABITS_DEFAULT = 3
#
STOPBITS =   ["ONE", "ONEPOINTFIVE", "TWO"]
INDEX_STOPBITS_DEFAULT = 0
#
HANDSHAKES = ["NONE", "XONXOFF", "RTSCTS", "DSRDTR"]
INDEX_HANDSHAKE_DEFAULT = 0
#
#-------------------------------------------------------------------
#   Global Type
# ------------------------------------------------------------------
#

class CUartClient:
    """CUartClient: ..."""

# ---------------------------------------------------------------------
# CUartClient - Field
# ---------------------------------------------------------------------
    Serial = serial.Serial()
    Thread = threading.Thread()
    ContinueReading = False
#
# ---------------------------------------------------------------------
# CUartClient - Constructor
# ---------------------------------------------------------------------
    def GetListSerialPorts(self):
        """ Lists serial port names
    
            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')
    
        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result
    #
    def __init__(self):
        self.Thread = None
        self.ContinueReading = False;
        self.OnLineReceived = None
        self.ComPort = ""
#
# ---------------------------------------------------------------------
# CUartClient - Helper
# ---------------------------------------------------------------------
    def TextBaudrate(self, text):
        switcher = {
            "50"     : 50,
            "75"     : 75,
            "110"    : 110,
            "134"    : 134,
            "150"    : 150,
            "200"    : 200,
            "300"    : 300,
            "600"    : 600,
            "1200"   : 1200,
            "1800"   : 1800,
            "2400"   : 2400,
            "4800"   : 4800,
            "9600"   : 9600,
            "19200"  : 19200,
            "38400"  : 38400,
            "57600"  : 57600,
            "115200" : 115200,
            "230400" : 230400,
            "460800" : 460800,
            "500000" : 500000,
            "576000" : 576000,
            "921600" : 921600,
            "1000000": 1000000,
            "1152000": 1152000,
            "1500000": 1500000,
            "2000000": 2000000,
            "2500000": 2500000,
            "3000000": 3000000,
            "3500000": 3500000,
            "4000000": 4000000
        }
        return switcher.get(text, 115200)
    #
    def TextParity(self, text):
        switcher = {
            "NONE" : serial.PARITY_NONE,
            "EVEN" : serial.PARITY_EVEN,
            "ODD"  : serial.PARITY_ODD,
            "MARK" : serial.PARITY_MARK,
            "SPACE": serial.PARITY_SPACE
        }
        return switcher.get(text, serial.PARITY_NONE)
    #
    def TextDatabits(self, text):
        switcher = {
            "FIVEBITS" : serial.FIVEBITS,
            "SIXBITS" : serial.SIXBITS,
            "SEVENBITS": serial.SEVENBITS,
            "EIGHTBITS": serial.EIGHTBITS
        }
        return switcher.get(text, serial.EIGHTBITS)
    #
    def TextStopbits(self, text):
        switcher = {
            "ONE"         : serial.STOPBITS_ONE,
            "ONEPOINTFIVE": serial.STOPBITS_ONE_POINT_FIVE,
            "TWO"         : serial.STOPBITS_TWO
        }
        return switcher.get(text, serial.STOPBITS_ONE)
#
# ---------------------------------------------------------------------
# CUartClient - Property
# --------------------------------------------------------------------
    def SetCBOnLineReceived(self, cbonlinereceived):
        """CSerial.SetCBOnLineReceived: ..."""
        self.CBOnLineReceived = cbonlinereceived
#
# ---------------------------------------------------------------------
# CUartClient - Callback - Main
# ---------------------------------------------------------------------
    def OnLineReceived(self, line):
        """CSerial.OnLineReceived: ..."""
        if self.Serial.is_open:
            if (None != self.CBOnLineReceived):
                self.CBOnLineReceived(line)
#
# ---------------------------------------------------------------------
# CUartClient - Callback - pySerial
# ---------------------------------------------------------------------
    def CBSerialOnReceiveData(self):
        RxLine = ""
        self.ContinueReading = True
        while (self.ContinueReading):
            while (0 < self.Serial.inWaiting()):
                RxData = self.Serial.read(1)
                if (b'\x0A' == RxData):
                    if (0 < len(RxLine)):
                        #debug: print("NL>>>>>>>>>" + RxLine + "<<<<<<<<<<<")
                        self.CBOnLineReceived(RxLine)
                    RxLine = ""
                else:
                    if (b'\x0D' == RxData):
                        if (0 < len(RxLine)):
                            #debug: print("CR>>>>>>>>>" + RxLine + "<<<<<<<<<<<")
                            self.CBOnLineReceived(RxLine)
                        RxLine = ""
                    else:
                        RxLine += RxData.decode()
#
# ---------------------------------------------------------------------
# CUartClient - Handler
# ---------------------------------------------------------------------
    def Open(self, comport, baudrate, parity, databits, stopbits, handshake):
        """CSerial.Open: ..."""
        Result = False
        if not self.Serial.is_open:
            self.Serial.port = comport
            self.Serial.baudrate = self.TextBaudrate(baudrate)
            self.Serial.parity = self.TextParity(parity)
            self.Serial.bytesize = self.TextDatabits(databits)
            self.Serial.stopbits = self.TextStopbits(stopbits)
            self.Serial.timeout = 0.1
            self.Serial.xonxoff = False
            self.Serial.rtscts = False
            self.Serial.dsrdtr = False
            if ("XONXOFF" == handshake):
                self.Serial.xonxoff = True
            if ("RTSCTS" == handshake):
                self.Serial.rtscts = True
            if ("DSRDTR" == handshake):
                self.Serial.dsrdtr = True
            self.Serial.open()
            #
            if (None == self.Thread):
                self.Thread = threading.Thread(target = self.CBSerialOnReceiveData)
            self.Thread.start()
            #
            Result = self.Serial.is_open            
        print("SerialPort[" + comport + "]BR[" + baudrate + "]PA[" + \
              parity + "]DB[" + databits + "]SB[" + stopbits + "]HS[" + \
              handshake + "] IsOpened[" + str(Result) + "]")
        if (not Result):
            self.ComPort = ""
        return Result
    #
    def Close(self, comport = "COM"):
        """CSerial.Close: ..."""
        Result = False
        if self.Serial.is_open:
            # self.Serial.flush()
            self.ContinueReading = False
            time.sleep(0.1)
            self.Serial.close()
            #
            if (not (None == self.Thread)):
                self.Thread.running = False        
            self.Thread = None
            #
            Result = not self.Serial.is_open
        #
        print("SerialPort[" + self.ComPort + "] IsClosed[" + str(Result) + "]")
        self.ComPort = ""
        return Result
    #
    def WriteLine(self, line):
        """CSerial.WriteLine: ..."""
        if self.Serial.is_open:
            Line = line + "\r\n"
            self.Serial.write(Line.encode())
