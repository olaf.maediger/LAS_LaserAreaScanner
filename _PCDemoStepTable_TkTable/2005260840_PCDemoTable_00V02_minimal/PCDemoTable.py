#
# ------------------------------------------------------------------
# PCDemoTable
# ------------------------------------------------------------------
#   Version: 00V01
#   Date   : 200525
#   Time   : 0852
#   Author : OMDevelop
#
import WindowMain as WM
#
TITLE_APPLICATION                  = "PCDemoTable"
#
###################################################################
# Main
###################################################################
print("*** " + TITLE_APPLICATION + ": begin")
#
WindowMainUart = WM.CWindowMainUart()
WindowMainUart.Execute()
#
print("*** " + TITLE_APPLICATION + ": end")
#



