#
#------------------------------------------------------------------
#   <short> - <project>
#------------------------------------------------------------------
#   Date   : 20mmdd
#   Time   : hhmm
#   Author : OMDevelop
#
#import threading
#

#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CSteplist:
    #
    def __init__(self):
        self.Steplist = []
        self.HeaderRow = ['StepIndex[1]', 'PositionX', 'PositionY', 'PulseCount[1]', \
                          'PulsePeriod[ms]', 'DelayMotion[ms]']
        self.StepIndex = -1
#
#------------------------------------------------------------------
#   Property
#------------------------------------------------------------------
    def Clear(self):
        self.Steplist = []
    #
    def GetStepCount(self):
        return len(self.Steplist)
    #
    def GetStep(self, stepindex):
        return self.Steplist[stepindex]
    #
    def AddStep(self, step):
        self.Steplist.append(step)
    #
    def ResetStep(self):
        self.StepIndex = 0
    #
    def NextStep(self):
        self.StepIndex += 1
        if (self.StepIndex <= len(self.Steplist) - 1):
            return self.StepIndex
        return -1
    #
#------------------------------------------------------------------
#   Handler
#------------------------------------------------------------------
    def InitSimple(self, stepcount = 100):
        self.Steplist = []
        for SI in range(0, stepcount):
            Step = [1800 + SI, 1800 + SI, 123, 10, 1.234]
            self.AddStep(Step)
    #
    def Debug(self, headerbegin, headerend):
        SC = len(self.Steplist)
        print(headerbegin + "[{0}]:".format(SC))
        print(self.HeaderRow)
        for SI in range(0, SC):
            print(self.Steplist[SI])
        print(headerend)

#
#------------------------------------------------------------------
#
#------------------------------------------------------------------
#
