#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameMachining
# ------------------------------------------------------------------
#   Version: 00V01
#   Date   : 200526
#   Time   : 1043
#   Author : OMDevelop
#
import tkinter as tk
from tkinter import filedialog as FD
import os
#
import Initdata as ID
import Steplist as SL
import FrameStepTable as FST
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
#
CONTROL_WIDTH = 16
CONTROL_HEIGHT = 1
#
MASK_FILE = "{0} \t{1} \t{2} \t{3} \t{4}\n"
#
INITDATA_SECTION    = "FrameMicroMachining"
#NAME_PERIOD_MS      = "Period[ms]"
#
#INIT_PERIOD_MS      = "1000"


#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameMicroMachining(tk.Frame):
    #---------------------------------------------------------------------
    # Constructor
    #---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, bg = "#ffeeee", padx = 4, pady = 4) # here NO W/H-sizes!
        self.grid_propagate(0)
        #---------------------------------------------------------------
        self.Steplist = SL.CSteplist()
        self.Steplist.InitSimple(100)
        self.Steplist.Debug(headerbegin = "--- Steplist", headerend = "-----------")
        #---------------------------------------------------------------
        self.StepTable = FST.CFrameStepTable(self)
        self.StepTable.grid(row = 0, column = 0, columnspan = 115)
        #---------------------------------------------------------------
        self.btnReadSteplist = tk.Button(self, text = "Read Steplist", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT,
                                  command = self.OnReadSteplistClick)
        self.btnReadSteplist.grid(row = 1, column = 0, padx = 0, pady = 4)
        #---------------------------------------------------------------
        self.btnWriteSteplist = tk.Button(self, text = "Write Steplist", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT,
                                  command = self.OnWriteSteplistClick)
        self.btnWriteSteplist.grid(row = 1, column = 1, padx = 0, pady = 4)
        #---------------------------------------------------------------      
        self.RefreshStepTable()
        #---------------------------------------------------------------
        self.btnClearSteplist = tk.Button(self, text = "Clear Steplist", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT,
                                  command = self.OnClearSteplistClick)
        self.btnClearSteplist.grid(row = 1, column = 2, padx = 0, pady = 4)
        #---------------------------------------------------------------
        self.btnTestA = tk.Button(self, text = "Up", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT,
                                  command = self.OnTestAClick)
        self.btnTestA.grid(row = 1, column = 3, padx = 0, pady = 4)
        #---------------------------------------------------------------
        self.btnTestB = tk.Button(self, text = "Middle", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT,
                                  command = self.OnTestBClick)
        self.btnTestB.grid(row = 1, column = 4, padx = 0, pady = 4)
        #---------------------------------------------------------------
        self.btnTestC = tk.Button(self, text = "Down", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT,
                                  command = self.OnTestCClick)
        self.btnTestC.grid(row = 1, column = 5, padx = 0, pady = 4)
        #---------------------------------------------------------------
        self.SuperIndex = 0;
        #---------------------------------------------------------------
        self.btnStartSteplist = tk.Button(self, text = "Start Steplist", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT,
                                  command = self.OnStartSteplistClick)
        self.btnStartSteplist.grid(row = 2, column = 0, padx = 0, pady = 4)
        #---------------------------------------------------------------
        self.btnAbortSteplist = tk.Button(self, text = "Abort Steplist", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT,
                                  command = self.OnAbortSteplistClick)
        self.btnAbortSteplist.grid(row = 2, column = 1, padx = 0, pady = 4)
        #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    #
    def SetCBOnStartSteplist(self, cbonstartsteplist):
        self.CBOnStartSteplist = cbonstartsteplist
    #
    def SetCBOnAbortSteplist(self, cbonabortsteplist):
        self.CBOnAbortSteplist = cbonabortsteplist
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        self.FrameStepTable.ReadInitdata(readinitdata)
    #
    def WriteInitdata(self, writeinitdata):
        self.FrameStepTable.WriteInitdata(writeinitdata)
    #
    def ResetStep(self):
        self.Steplist.ResetStep()
    #
    def NextStep(self):
        StepIndex = self.Steplist.NextStep()
        if StepIndex < 0:            
            return False
        self.StepTable.SetStepMarked(StepIndex)
        return True
    #
    #
    #-----------------------------------------------------------------------------------
    # Helper - Control
    #-----------------------------------------------------------------------------------
    def RefreshStepTable(self):
        SC = self.Steplist.GetStepCount()
        self.StepTable.Clear()
        for SI in range(0, SC):
            S = self.Steplist.GetStep(SI)
            self.StepTable.AddRow(S[0], S[1], S[2], S[3], S[4])
    #
    #-----------------------------------------------------------------------------------
    # Helper - File
    #-----------------------------------------------------------------------------------
    def GetFilePositionEnd(self, file):
        file.seek(0, 2)
        EP = file.tell()
        file.seek(0)
        return EP
    #
    def IsEndOfFile(self, file, filepositionend):
        return (filepositionend <= file.tell())
    #
    #-----------------------------------------------------------------------------------
    # Event-Handler
    #-----------------------------------------------------------------------------------    
    def OnStartSteplistClick(self):
        if (self.CBOnStartSteplist):
            self.CBOnStartSteplist()
    #
    def OnAbortSteplistClick(self):
        if (self.CBOnAbortSteplist):
            self.CBOnAbortSteplist()
    #
    def OnTestAClick(self):
        self.StepTable.SetStepMarked(0)
    #
    def OnTestBClick(self):
        if (99 <= self.SuperIndex):
            self.SuperIndex = 0
        self.StepTable.SetStepMarked(self.SuperIndex)
        self.SuperIndex += 10;
    #
    def OnTestCClick(self):        
        self.StepTable.SetStepMarked(99)
    #
    def OnClearSteplistClick(self):
        self.StepTable.Clear()
    #       
    def OnWriteSteplistClick(self):        
        IF = 'MicroMachining.stt'
        T = 'Write Steplist to File'
        UD = '.\\'
        FT = (('StepTables','.stt'), ('All Files', '*.*'))
        FileEntry = FD.asksaveasfilename(initialfile = IF, title = T,
                                        initialdir = UD, filetypes = FT)
        if (FileEntry):
            SC = self.Steplist.GetStepCount()
            F = open(FileEntry, "w")
            for SI in range(0, SC):
                S = self.Steplist.GetStep(SI)
                F.write(MASK_FILE.format(S[0], S[1], S[2], S[3], S[4]))
            F.close()
    #
    def OnReadSteplistClick(self):
        T = 'Read Steplist from File'
        UD = '.\\'
        FT = (('StepTables','.stt'), ('All Files', '*.*'))
        FileEntry = FD.askopenfilename(initialdir = UD, filetypes = FT, title = T)
        if (FileEntry):
            self.Steplist.Clear()
            F = open(FileEntry, "r")
            FPE = self.GetFilePositionEnd(F)
            while (not self.IsEndOfFile(F, FPE)):
                Line = F.readline().strip()
                if (0 < len(Line)):
                    TL = Line.split()
                    Step = [int(TL[0]), int(TL[1]), int(TL[2]), float(TL[3]), float(TL[4])]
                    self.Steplist.AddStep(Step)
            F.close()
            self.RefreshStepTable()

