#
import threading
import time
#
class CTask:
    def __init__(self):
        self.FThread = None
        self.DoLoop = True
        self.CBOnExecute = None
        self.CBOnStart = None
        self.CBOnAbort = None
    #
    def SetCBOnExecute(self, callback):
        self.CBOnExecute = callback
    #
    def Start(self):
        #debug print("Task###Start")
        if isinstance(self.FThread, threading.Thread):
            self.Abort()
        self.FThread = None
        self.FThread = threading.Thread(target = self.OnExecute)
        self.FThread.start()
        if (None != self.CBOnStart):
            self.CBOnStart()
    #
    def Abort(self):
        self.DoLoop = False
        if (None != self.CBOnAbort):
            self.CBOnAbort()
        # HangsUp self.FThread.join()
        #debug print("Task###Abort")
    #
    def OnExecute(self):
        #debug time.sleep(1.0)
        time.sleep(0.5)
        print("TaskOnExecute: start")
        self.DoLoop = True;
        while (True == self.DoLoop):
            if (None != self.CBOnExecute):
                if (not self.CBOnExecute()):
                    self.DoLoop = False
        print("TaskOnExecute: end")
        
