#
# ------------------------------------------------------------------
# PCStepTable
# ------------------------------------------------------------------
#   Version: 00V01
#   Date   : 200526
#   Time   : 1120
#   Author : OMDevelop
#
import WindowMain as WM
#
TITLE_APPLICATION                  = "PCStepTable"
#
###################################################################
# Main
###################################################################
print("*** " + TITLE_APPLICATION + ": begin")
#
WindowMain = WM.CWindowMain()
WindowMain.Execute()
#
print("*** " + TITLE_APPLICATION + ": end")
#



