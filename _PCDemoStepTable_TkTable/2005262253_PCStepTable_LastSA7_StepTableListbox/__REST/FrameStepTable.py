#
# ------------------------------------------------------------------
#   PCDemoTable - FrameStepTable
# ------------------------------------------------------------------
#   Version: 00V01
#   Date   : 200525
#   Time   : 0852
#   Author : OMDevelop
#
import tkinter as TK
import random as RND
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION                = "FrameStepTable"
#
# NAME_PULSEPERIOD_MS             = "PulsePeriod[ms]"
#
# INIT_PULSEPERIOD_MS             = "1000"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
# FrameWidth = 200
# FrameHeight = 500



class CFrameStepTable(TK.Frame):
    #---------------------------------------------------------------------
    # Field
    #---------------------------------------------------------------
    #
    #---------------------------------------------------------------------
    # Constructor
    #---------------------------------------------------------------
    def __init__(self, window): # width = FrameWidth, height = FrameHeight, \
        TK.Frame.__init__(self, window, padx = 14, pady = 14, background = "#aa8888")
        #???self.grid_propagate(0)
        #--------------------------------------------------------------
        # Controls...
        #--------------------------------------------------------------
        # self.RowCount = 10
        # self.ColCount = 6
        # #
        # self.RowHeight = 20
        # self.ColWidth = 110
        # #
        # self.CanvasWidth = 640
        # self.CanvasHeight = 480
        #
        # self.Canvas = TK.Canvas(self, width = self.CanvasWidth, height = self.CanvasHeight, background = "#ffffff")
        # self.Canvas.pack()
        # self.Draw()
        
        
        
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    # def SetCBOnSetPulsePeriod(self, callback):
    #     self.CBOnSetPulsePeriod = callback
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    # def OnSetPulsePeriod(self):
    #     if (self.CBOnSetPulsePeriod):
    #         #self.RemoteSwitchOn = True
    #         PP = self.spbPulsePeriod.get()
    #         self.CBOnSetPulsePeriod(PP)
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        # PP = readinitdata.ReadValueInit(INITDATA_SECTION, \
        #                                 NAME_PULSEPERIOD_MS, \
        #                                 INIT_PULSEPERIOD_MS)
        # self.spbPulsePeriod.delete(0, tk.END)
        # self.OnSet...
        return True
        #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        # writeinitdata.WriteValue(INITDATA_SECTION, \
        #                          NAME_PULSEPERIOD_MS, \
        #                          self.spbPulsePeriod.get())
        return True
        #
    #
    #---------------------------------------------------------------
    # Management - Event/Response...
    #--------------------------------------------------------------    
    #def Draw(self):
        #self.Canvas = tk.Canvas(self, width = 300, height = 200, background = "#ffff00")
        #self.Canvas.pack()
        # CR = (int)(256 * RND.random())
        # CG = (int)(256 * RND.random())
        # CB = (int)(256 * RND.random())
        # SC = "#{0:02x}{1:02x}{2:02x}".format(CR, CG, CB)
        # SC = "#CCCCCC"
        # # self.Canvas.create_line(0, 0, 100, 100, fill = SC)
        # for RI in range(0, self.RowCount):
        #     for CI in range(0, self.ColCount):
                
        #         self.Canvas.create_line(0 * RI, 0, 100, 100, fill = SC)      
        #         self.Canvas.create_line(10 * RI, 0, 100, 100, fill = SC)      
                




