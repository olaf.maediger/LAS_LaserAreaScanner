#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameMachining
# ------------------------------------------------------------------
#   Version: 00V01
#   Date   : 200526
#   Time   : 1043
#   Author : OMDevelop
#
import tkinter as tk
#
import Initdata as ID
import FrameStepTable as FST
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
#
CONTROL_WIDTH = 16
CONTROL_HEIGHT = 1
#
INITDATA_SECTION    = "FrameMicroMachining"
#NAME_PERIOD_MS      = "Period[ms]"
#
#INIT_PERIOD_MS      = "1000"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameMicroMachining(tk.Frame):

#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, bg = "#ffeeee", padx = 4, pady = 4) # here NO W/H-sizes!
        self.grid_propagate(0)
        #---------------------------------------------------------------
        self.FrameStepTable = FST.CFrameStepTable(self)
        self.FrameStepTable.grid(row = 0, column = 0, columnspan = 115)
        #---------------------------------------------------------------        
        self.btnReadFile = tk.Button(self, text = "ReadFile", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT)
        self.btnReadFile.grid(row = 1, column = 0, padx = 0, pady = 4)
        #---------------------------------------------------------------        
        self.btnWriteFile = tk.Button(self, text = "WriteFile", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT)
        self.btnWriteFile.grid(row = 1, column = 1, padx = 0, pady = 4)
        #---------------------------------------------------------------       
        # self.F = tk.Frame(self, bg = "green", width = 100, height = 100)
        # self.F.grid(row = 2, column = 0)
        #---------------------------------------------------------------
        #---------------------------------------------------------------
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    #
    #-----------------------------------------------------------------------------------
    # Helper
    #-----------------------------------------------------------------------------------
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        self.FrameStepTable.ReadInitdata(readinitdata)
    #
    def WriteInitdata(self, writeinitdata):
        self.FrameStepTable.WriteInitdata(writeinitdata)
    #