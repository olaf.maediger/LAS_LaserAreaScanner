#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameMachining
# ------------------------------------------------------------------
#   Version: 00V01
#   Date   : 200526
#   Time   : 1043
#   Author : OMDevelop
#
import tkinter as tk
from tkinter import filedialog as FD
#import os
#
import Initdata as ID
import Steplist as SL
import FrameStepTable as FST
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
#
CONTROL_WIDTH = 16
CONTROL_HEIGHT = 1
#
MASK_FILE = "{0} \t{1} \t{2} \t{3} \t{4}\n"
#
INITDATA_SECTION    = "FrameMicroMachining"
#NAME_PERIOD_MS      = "Period[ms]"
#
#INIT_PERIOD_MS      = "1000"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameMicroMachining(tk.Frame):

#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, bg = "#ffeeee", padx = 4, pady = 4) # here NO W/H-sizes!
        self.grid_propagate(0)
        #---------------------------------------------------------------
        self.Steplist = SL.CSteplist()
        self.Steplist.InitSimple(100)
        self.Steplist.Debug(headerbegin = "--- Steplist", headerend = "-----------")
        #---------------------------------------------------------------
        self.StepTable = FST.CFrameStepTable(self)
        self.StepTable.grid(row = 0, column = 0, columnspan = 115)
        #---------------------------------------------------------------
        self.btnReadFile = tk.Button(self, text = "ReadFile", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT,
                                  command = self.OnReadFileClick)
        self.btnReadFile.grid(row = 1, column = 0, padx = 0, pady = 4)
        #---------------------------------------------------------------
        self.btnWriteFile = tk.Button(self, text = "WriteFile", fg = "#000000", \
                                  width = CONTROL_WIDTH, height = CONTROL_HEIGHT,
                                  command = self.OnWriteFileClick)
        self.btnWriteFile.grid(row = 1, column = 1, padx = 0, pady = 4)
        #---------------------------------------------------------------
        # self.F = tk.Frame(self, bg = "green", width = 100, height = 100)
        # self.F.grid(row = 2, column = 0)
        #---------------------------------------------------------------
        self.RefreshStepTable()
        #---------------------------------------------------------------
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    def OnWriteFileClick(self):
        IF = 'MicroMachining.stt'
        T = 'Write StepTable to File'
        UD = '.\\'
        FT = (('StepTables','.stt'), ('All Files', '*.*'))
        FileEntry = FD.asksaveasfilename(initialfile = IF, title = T,
                                        initialdir = UD, filetypes = FT)
        if (FileEntry):
            SC = self.Steplist.GetStepCount()
            F = open(FileEntry, "w")
            for SI in range(0, SC):
                S = self.Steplist.GetStep(SI)
                F.write(MASK_FILE.format(S[0], S[1], S[2], S[3], S[4]))
            F.close()
    #
    def GetFilePositionEnd(self, file):
        file.seek(0, 2)
        EP = file.tell()
        file.seek(0)
        return EP
    #
    def IsEndOfFile(self, file, filepositionend):
        return (filepositionend <= file.tell())
    #
    def OnReadFileClick(self):
        T = 'Read StepTable from File'
        UD = '.\\'
        FT = (('StepTables','.stt'), ('All Files', '*.*'))
        FileEntry = FD.askopenfilename(initialdir = UD, filetypes = FT, title = T)
        if (FileEntry):
            self.Steplist.Clear()
            F = open(FileEntry, "r")
            FPE = self.GetFilePositionEnd(F)
            while (not self.IsEndOfFile(F, FPE)):
                Line = F.readline().strip()
                if (0 < len(Line)):
                    TL = Line.split()
                    #print(TL)
                    Step = [int(TL[0]), int(TL[1]), int(TL[2]), float(TL[3]), float(TL[4])]
                    self.Steplist.AddStep(Step)
            F.close()
            self.RefreshStepTable()
    #
    #-----------------------------------------------------------------------------------
    # Helper
    #-----------------------------------------------------------------------------------
    def RefreshStepTable(self):
        SC = self.Steplist.GetStepCount()
        self.StepTable.Clear()
        for SI in range(0, SC):
            S = self.Steplist.GetStep(SI)
            self.StepTable.AddRow(S[0], S[1], S[2], S[3], S[4])


    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        self.FrameStepTable.ReadInitdata(readinitdata)
    #
    def WriteInitdata(self, writeinitdata):
        self.FrameStepTable.WriteInitdata(writeinitdata)
    #