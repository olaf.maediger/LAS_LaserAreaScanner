#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameMachining
# ------------------------------------------------------------------
#   Version: 00V01
#   Date   : 200526
#   Time   : 1043
#   Author : OMDevelop
#
import tkinter as tk
#
import Initdata as ID
import FrameStepTable as FST
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
#
INITDATA_SECTION    = "FrameMicroMachining"
#NAME_PERIOD_MS      = "Period[ms]"
#
#INIT_PERIOD_MS      = "1000"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameMicroMachining(tk.Frame):

#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 680, height = 520, \
                          padx = 0, pady = 0, background = "#777777")
        self.grid_propagate(0)
        #---------------------------------------------------------------
        self.FrameStepTable = FST.CFrameStepTable(self)
        self.FrameStepTable.grid(row = 0, column = 0, sticky = "w")
        #---------------------------------------------------------------
        #---------------------------------------------------------------
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    #
    #-----------------------------------------------------------------------------------
    # Helper
    #-----------------------------------------------------------------------------------
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        self.FrameStepTable.ReadInitdata(readinitdata)
    #
    def WriteInitdata(self, writeinitdata):
        self.FrameStepTable.WriteInitdata(writeinitdata)
    #