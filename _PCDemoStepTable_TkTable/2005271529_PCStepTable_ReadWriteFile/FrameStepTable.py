#
# ------------------------------------------------------------------
#   PCDemoTable - FrameStepTable
# ------------------------------------------------------------------
#   Version: 00V01
#   Date   : 200525
#   Time   : 0852
#   Author : OMDevelop
#
import tkinter as tk
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION                = "FrameStepTable"
#
# NAME_PULSEPERIOD_MS             = "PulsePeriod[ms]"
#
# INIT_PULSEPERIOD_MS             = "1000"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
# FrameWidth = 200
# FrameHeight = 500
FORMAT_STEP = " %8d      %6d     %6d       %6d    %9.3f     %9.3f"


class CFrameStepTable(tk.Frame):
    #
    #---------------------------------------------------------------------
    # Constructor
    #---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 610, height = 804, padx = 2, pady = 2, bg = "#F2F2F2")
        self.grid_propagate(0)
        #--------------------------------------------------------------
        # Controls...
        #--------------------------------------------------------------
        self.lblHSIN = tk.Label(self, text = " StepIndex", font = "Courier 10")
        self.lblHSIN.grid(row = 0, column = 0, padx = 2, pady = 0)
        self.lblHSIU = tk.Label(self, text = "       [1]", font = "Courier 10")
        self.lblHSIU.grid(row = 1, column = 0, padx = 2, pady = 0)
        #
        self.lblHPXN = tk.Label(self, text = " PositionX", font = "Courier 10")
        self.lblHPXN.grid(row = 0, column = 1, padx = 2, pady = 0)
        self.lblHPXU = tk.Label(self, text = "     [stp]", font = "Courier 10")
        self.lblHPXU.grid(row = 1, column = 1, padx = 2, pady = 0)
        #
        self.lblHPYN = tk.Label(self, text = " PositionY", font = "Courier 10")
        self.lblHPYN.grid(row = 0, column = 2, padx = 2, pady = 0)
        self.lblHPYU = tk.Label(self, text = "     [stp]", font = "Courier 10")
        self.lblHPYU.grid(row = 1, column = 2, padx = 2, pady = 0)
        #
        self.lblHPCN = tk.Label(self, text = " PulseCount", font = "Courier 10")
        self.lblHPCN.grid(row = 0, column = 3, padx = 2, pady = 0)
        self.lblHPCU = tk.Label(self, text = "        [1]", font = "Courier 10")
        self.lblHPCU.grid(row = 1, column = 3, padx = 2, pady = 0)
        #
        self.lblHPPN = tk.Label(self, text = " PulsePeriod", font = "Courier 10")
        self.lblHPPN.grid(row = 0, column = 4, padx = 2, pady = 0)
        self.lblHPPU = tk.Label(self, text = "        [ms]", font = "Courier 10")
        self.lblHPPU.grid(row = 1, column = 4, padx = 2, pady = 0)
        #
        self.lblHDMN = tk.Label(self, text = " DelayMotion", font = "Courier 10")
        self.lblHDMN.grid(row = 0, column = 5, padx = 2, pady = 0)
        self.lblHDMU = tk.Label(self, text = "        [ms]", font = "Courier 10")
        self.lblHDMU.grid(row = 1, column = 5, padx = 2, pady = 0)
        #
        self.lbxSteps = tk.Listbox(self, width = 100, height = 44,
                                   font = "Courier 10", activestyle = tk.NONE)
        #self.lbxSteps["selectmode"] = "extended"
        self.lbxSteps["selectmode"] = "single"
        self.lbxSteps.grid(row = 2, column = 0, columnspan = 123, padx = 2, pady = 2)
        #
        self.scbSteps = tk.Scrollbar(self)
        self.scbSteps.grid(row = 2, column = 6, sticky = "ns")
        #
        self.lbxSteps["yscrollcommand"] = self.scbSteps.set
        self.scbSteps["command"] = self.lbxSteps.yview
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        # PP = readinitdata.ReadValueInit(INITDATA_SECTION, \
        #                                 NAME_PULSEPERIOD_MS, \
        #                                 INIT_PULSEPERIOD_MS)
        # self.spbPulsePeriod.delete(0, tk.END)
        # self.OnSet...
        return True
        #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        # writeinitdata.WriteValue(INITDATA_SECTION, \
        #                          NAME_PULSEPERIOD_MS, \
        #                          self.spbPulsePeriod.get())
        return True
        #

    #---------------------------------------------------------------
    # Management - Get/Set
    #--------------------------------------------------------------
    def AddRow(self, px, py, pc, pp, dm):
        NS = self.lbxSteps.size()
        self.lbxSteps.insert(tk.END, FORMAT_STEP % (NS, px, py, pc, pp, dm))
    #
    def Clear(self):
        self.lbxSteps.delete(0, tk.END)

    # def Draw(self):
    #     for SI in range(0, len(self.StepList)):
    #         PX = self.StepList[SI][0]
    #         PY = self.StepList[SI][1]
    #         PC = self.StepList[SI][2]
    #         PP = self.StepList[SI][3]
    #         DM = self.StepList[SI][4]
    #         self.lbxSteps.insert(tk.END, " %8d      %6d     %6d       %6d    %9.3f     %9.3f" % (SI, PX, PY, PC, PP, DM))
 #
    #---------------------------------------------------------------
    # Management - Event/Response...
    #--------------------------------------------------------------
    #!!! def Draw(self):
    #!!!     #self.Canvas = tk.Canvas(self, width = 2, height = 3, background = "#ffff00")
    #!!!     #self.Canvas.pack()
    #!!!     CR = (int)(256 * RND.random())
    #!!!     CG = (int)(256 * RND.random())
    #!!!     CB = (int)(256 * RND.random())
    #!!!     SC = "#{0:02x}{1:02x}{2:02x}".format(CR, CG, CB)
    #!!!     SC = "#CCCCCC"
    #!!!     # self.Canvas.create_line(0, 0, 100, 100, fill = SC)
    #!!!     for RI in range(0, self.RowCount):
    #!!!         for CI in range(0, self.ColCount):
    #!!!
    #!!!             self.Canvas.create_line(0 * RI, 0, 100, 100, fill = SC)
    #!!!             self.Canvas.create_line(10 * RI, 0, 100, 100, fill = SC)






        # external!!!
        #--------------------------------------------------------------
        # # Initdata
        # #--------------------------------------------------------------
        # self.HeaderRow = ['StepIndex[1]', 'PositionX', 'PositionY', 'PulseCount[1]', \
        #                   'PulsePeriod[ms]', 'DelayMotion[ms]']
        # self.StepList = []
        # for SI in range(0, 100):
        #     self.Step = [1800 + SI, 1800 + SI, 123, 10, 1.234]
        #     self.StepList.append(self.Step)
        # #
        # self.SetPositionX(0, 1234)
        # self.SetPositionY(0, 4321)
        # self.SetDelayMotion(1, 0.1)
        # self.SetRow(len(self.StepList) - 1, 1, 2, 3, 4, 3.332)
        # #
        # print("---StepList:")
        # print(self.HeaderRow)
        # for SI in range(0, len(self.StepList)):
        #     print(self.StepList[SI])
        # print("----")

        # self.Draw()

        #!!! exchange Listbox !
        #!!! self.RowCount = 10
        #!!! self.ColCount = 6
        #!!! #
        #!!! self.RowHeight = 20
        #!!! self.ColWidth = 110
        #!!! #
        #!!! self.CanvasWidth = 640
        #!!! self.CanvasHeight = 480
        #!!! #
        #!!! self.Canvas = tk.Canvas(self, width = self.CanvasWidth, height = self.CanvasHeight, background = "#335577")
        #!!! self.Canvas.grid(row = 1, column = 0, padx = 2, pady = 2)
        #!!! self.Draw()






    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    # def SetCBOnSetPulsePeriod(self, callback):
    #     self.CBOnSetPulsePeriod = callback
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    # def OnSetPulsePeriod(self):
    #     if (self.CBOnSetPulsePeriod):
    #         #self.RemoteSwitchOn = True
    #         PP = self.spbPulsePeriod.get()
    #         self.CBOnSetPulsePeriod(PP)



 #    # def SetPositionX(self, stepindex, positionx):
 #    #     self.StepList[stepindex][0] = positionx
 #    # #
 #    # def SetPositionY(self, stepindex, positiony):
 #    #     self.StepList[stepindex][1] = positiony
 #    # #
 #    # def SetPulseCount(self, stepindex, pulsecount):
 #    #     self.StepList[stepindex][2] = pulsecount
 #    # #
 #    # def SetPulsePeriod(self, stepindex, pulseperiod):
 #    #     self.StepList[stepindex][3] = pulseperiod
 #    # #
 #    # def SetDelayMotion(self, stepindex, delaymotion):
 #    #     self.StepList[stepindex][4] = delaymotion
 #    #
 #    def AddRow(self, positionx, positiony, pulsecount, pulseperiod, delaymotion):
 #        self.lbxSteps.insert(tk.END, " %8d      %6d     %6d       %6d    %9.3f     %9.3f" % (SI, PX, PY, PC, PP, DM))append([positionx, positiony, pulsecount, pulseperiod, delaymotion])
 #    #
 #    # def SetRow(self, stepindex, positionx, positiony, pulsecount, pulseperiod, delaymotion):
 #    #     self.StepList[stepindex] = [positionx, positiony, pulsecount, pulseperiod, delaymotion]

 #    def Clear(self):
 #        self.StepList = []

 #    def Draw(self):
 #        for SI in range(0, len(self.StepList)):
 #            PX = self.StepList[SI][0]
 #            PY = self.StepList[SI][1]
 #            PC = self.StepList[SI][2]
 #            PP = self.StepList[SI][3]
 #            DM = self.StepList[SI][4]
 #            self.lbxSteps.insert(tk.END, " %8d      %6d     %6d       %6d    %9.3f     %9.3f" % (SI, PX, PY, PC, PP, DM))
 # #
    # def SetRow(self, stepindex, positionx, positiony, pulsecount, pulseperiod, delaymotion):
    #     self.StepList[stepindex] = [positionx, positiony, pulsecount, pulseperiod, delaymotion]
    # def SetPositionX(self, stepindex, positionx):
    #     self.StepList[stepindex][0] = positionx
    # #
    # def SetPositionY(self, stepindex, positiony):
    #     self.StepList[stepindex][1] = positiony
    # #
    # def SetPulseCount(self, stepindex, pulsecount):
    #     self.StepList[stepindex][2] = pulsecount
    # #
    # def SetPulsePeriod(self, stepindex, pulseperiod):
    #     self.StepList[stepindex][3] = pulseperiod
    # #
    # def SetDelayMotion(self, stepindex, delaymotion):
    #     self.StepList[stepindex][4] = delaymotion
