#ifndef Pulse_h
#define Pulse_h
//
#include "Defines.h"
#include "IrqTimerDue.h"
#include "IrqTimerStm.h"
//
const long unsigned WIDTH_PULSECOUNT = 1;
//
class CPulse
{
  protected:
  // Period
  int FChannelPeriod;
#if defined(PROCESSOR_STM32F103C8)
  CIrqTimerStm* FPTimerPulsePeriod;
#elif defined(PROCESSOR_ARDUINODUE)
  CIrqTimerDue* FPTimerPulsePeriod;
#endif
  void (*FPIrqHandlerPeriod)(void);
  long unsigned FPulsePeriodus;  
  long unsigned FPulseCount;
  // Width
  int FChannelWidth;
#if defined(PROCESSOR_STM32F103C8)
  CIrqTimerStm* FPTimerPulseWidth;
#elif defined(PROCESSOR_ARDUINODUE)
  CIrqTimerDue* FPTimerPulseWidth;
#endif
  void (*FPIrqHandlerWidth)(void);
  long unsigned FPulseWidthus;
  //
  public:
  CPulse(int channelperiod, int channelwidth);
  //  Period
  void SetInterruptHandlerPeriod(void (*pirqhandlerperiod)(void));
  void SetPulsePeriodus(long unsigned pulseperiodus);
  void Start();
  void Stop();
  void SetPulseCount(long unsigned pulsecount);
  bool DecrementStopPulseCount();
  //  Width
  void SetInterruptHandlerWidth(void (*pirqhandlerwidth)(void));
  void SetPulseWidthus(long unsigned pulsewidthus);
  void StartWidth();
  void StopWidth();
};
//
#endif  // Pulse_h
