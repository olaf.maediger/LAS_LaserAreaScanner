#ifndef IrqTimerDue_h
#define IrqTimerDue_h
//
#include "Defines.h"
#if defined(PROCESSOR_ARDUINODUE)
//
// ... #include <HardwareTimer.h>
#include "IrqTimerBase.h"
//
class CIrqTimerDue : public CIrqTimerBase
{
  protected:
  int FChannel;
  // ..... HardwareTimer* FPHardwareTimer;
  void (*FPIrqHandler)(void);
  long unsigned FPulseCount;
  //
  public:
  CIrqTimerDue(int channel);
  void SetInterruptHandler(void (*pirqhandler)(void));
  void SetPeriodus(long unsigned periodus);
  void Start();
  void Stop();
  void SetPulseCount(long unsigned pulsecount);
  void DecrementStopPulseCount();
};
//
#endif  // defined(PROCESSOR_ARDUINODUE)
//
#endif  // IrqTimerDue_h
