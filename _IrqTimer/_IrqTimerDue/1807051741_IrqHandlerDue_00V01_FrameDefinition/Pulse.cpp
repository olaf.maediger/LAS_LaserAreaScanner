#include "Pulse.h"
//
CPulse::CPulse(int channelperiod, int channelwidth)
{
#if defined(PROCESSOR_STM32F103C8)
  FChannelPeriod = channelperiod;
  FPTimerPulsePeriod = new CIrqTimerStm(channelperiod);
  FChannelWidth = channelwidth;
  FPTimerPulseWidth = new CIrqTimerStm(FChannelWidth);
#elif defined(PROCESSOR_ARDUINODUE)
  FChannelPeriod = channelperiod;
  FPTimerPulsePeriod = new CIrqTimerDue(channelperiod);
  FChannelWidth = channelwidth;
  FPTimerPulseWidth = new CIrqTimerDue(FChannelWidth);
#endif  
}
//
//-------------------------------------------------------------------
//  Segment - Period
//-------------------------------------------------------------------
//
void CPulse::SetInterruptHandlerPeriod(void (*pirqhandlerperiod)(void))
{
  FPIrqHandlerPeriod = pirqhandlerperiod;
  FPTimerPulsePeriod->SetInterruptHandler(FPIrqHandlerPeriod);  
}

void CPulse::SetPulsePeriodus(long unsigned pulseperiodus)
{
  FPulsePeriodus = pulseperiodus;
  FPTimerPulsePeriod->SetPeriodus(FPulsePeriodus);  
}

void CPulse::Start()
{
  FPTimerPulsePeriod->SetPeriodus(FPulsePeriodus);
  FPTimerPulsePeriod->Start();  
}

void CPulse::Stop()
{
  FPTimerPulseWidth->Stop();
  FPTimerPulsePeriod->Stop();
}

void CPulse::SetPulseCount(long unsigned pulsecount)
{ 
  FPulseCount = pulsecount;
}

bool CPulse::DecrementStopPulseCount()
{
  if (0 < FPulseCount)
  {
    FPulseCount--;
  }
  if (0 == FPulseCount)
  {
    Stop();
    return true; 
  }
  return false;
}
//
//-------------------------------------------------------------------
//  Segment - Width
//-------------------------------------------------------------------
//
void CPulse::SetInterruptHandlerWidth(void (*pirqhandlerwidth)(void))
{
  FPIrqHandlerWidth = pirqhandlerwidth;
  FPTimerPulseWidth->SetInterruptHandler(FPIrqHandlerWidth);
}

void CPulse::SetPulseWidthus(long unsigned pulsewidthus)
{
  FPulseWidthus = pulsewidthus;
  FPTimerPulseWidth->SetPeriodus(pulsewidthus);
}

void CPulse::StartWidth()
{
  FPTimerPulseWidth->SetPeriodus(FPulseWidthus);
  FPTimerPulseWidth->SetPulseCount(WIDTH_PULSECOUNT);
  FPTimerPulseWidth->Start();
}

void CPulse::StopWidth()
{
  FPTimerPulseWidth->Stop();  
}



