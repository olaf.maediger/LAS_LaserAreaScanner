#include "Defines.h"
#if defined(PROCESSOR_ARDUINODUE)
//
#include "IrqTimerDue.h"
//
CIrqTimerDue::CIrqTimerDue(int channel)
  :CIrqTimerBase()
{
  FChannel = channel;
// .....   FPHardwareTimer = new HardwareTimer(channel);
// .....   FPHardwareTimer->setChannelMode(channel, TIMER_OUTPUTCOMPARE);
}

void CIrqTimerDue::SetInterruptHandler(void (*pirqhandler)(void))
{
  FPIrqHandler = pirqhandler;
// .....   FPHardwareTimer->attachInterrupt(FChannel, FPIrqHandler);
}

void CIrqTimerDue::SetPeriodus(long unsigned periodus)
{
// .....   FPHardwareTimer->setPeriod(periodus);
// .....   FPHardwareTimer->setCompare1(0x0000);
// .....   FPHardwareTimer->setCompare2(0x0000);
// .....   FPHardwareTimer->setCompare3(0x0000);
// .....   FPHardwareTimer->setCompare4(0x0000);
}

void CIrqTimerDue::Start()
{
// .....   FPHardwareTimer->resume();
}

void CIrqTimerDue::Stop()
{
// .....   FPHardwareTimer->pause(); 
}

void CIrqTimerDue::SetPulseCount(long unsigned pulsecount)
{ 
  FPulseCount = pulsecount;
}
void CIrqTimerDue::DecrementStopPulseCount()
{
  if (0 < FPulseCount)
  {
    FPulseCount--;
  }
  if (0 == FPulseCount)
  {
    Stop();
  }
}
//
#endif  // defined(PROCESSOR_ARDUINODUE)


