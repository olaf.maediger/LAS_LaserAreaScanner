#include "Pulse.h"
//
const int TIMERCHANNEL_PULSEPERIOD                   = 3;
const int TIMERCHANNEL_PULSEWIDTH                    = 4;
const long unsigned PERIOD_PULSECOUNT_INITIALISATION =       1;
const long unsigned PERIOD_PULSEPERIODUS             =  400000;
const long unsigned WIDTH_PULSEPERIODUS              =  PERIOD_PULSEPERIODUS / 4;
const long unsigned PERIOD_PULSECOUNT                =       3;
//
CPulse PulseTrigger(TIMERCHANNEL_PULSEPERIOD, TIMERCHANNEL_PULSEWIDTH);
//
// IrqHandler - Initialisation
//
void IrqHandlerPulsePeriodInitialisation(void)
{ // no operation!
  // PulseTrigger - PulseWidth - Start
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidthInitialisation(void)
{ // no operation!
  // PulseTrigger - PulseWidth - Stop
  PulseTrigger.StopWidth();
  PulseTrigger.DecrementStopPulseCount();
}
//
// IrqHandler - Process
//
void IrqHandlerPulsePeriod(void)
{ // LedSystem - On
  digitalWrite(PIN_LEDSYSTEM, false);
  // PulseTrigger - PulseWidth - Start
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidth(void)
{ // LedSystem - Off
  digitalWrite(PIN_LEDSYSTEM, true);
  // PulseTrigger - PulseWidth - Stop
  PulseTrigger.StopWidth();
  PulseTrigger.DecrementStopPulseCount();
}
//
// Main - Initialisation
//
void setup()
{
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, false);
  delay(2000);
  //
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, true);
  delay(2000);
  // 
  // PulseTrigger - PulseWidth
  PulseTrigger.StopWidth();
  PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidthInitialisation);
  PulseTrigger.SetPulseWidthus(WIDTH_PULSEPERIODUS);
  //
  // PulseTrigger - PulsePeriod/PulseCount
  PulseTrigger.Stop();
  PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriodInitialisation);
  PulseTrigger.SetPulsePeriodus(PERIOD_PULSEPERIODUS);
  PulseTrigger.SetPulseCount(PERIOD_PULSECOUNT_INITIALISATION);
  PulseTrigger.Start();  
  delay(1000);
  PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
  PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
}
//
// Main - Process
//
void loop()
{
  delay(2000);
  //
  // PulseTrigger - PulsePeriod/PulseCount
  PulseTrigger.Stop();
  PulseTrigger.SetPulsePeriodus(PERIOD_PULSEPERIODUS);
  PulseTrigger.SetPulseCount(PERIOD_PULSECOUNT);
  PulseTrigger.Start(); 
}









