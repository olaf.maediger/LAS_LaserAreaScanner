#ifndef IrqTimerStm_h
#define IrqTimerStm_h
//
#include "Defines.h"
#if defined(PROCESSOR_STM32F103C8)
//
#include <HardwareTimer.h>
#include "IrqTimerBase.h"
//
class CIrqTimerStm : public CIrqTimerBase
{
  protected:
  int FChannel;
  HardwareTimer* FPHardwareTimer;
  void (*FPIrqHandler)(void);
  long unsigned FPulseCount;
  //
  public:
  CIrqTimerStm(int channel);
  void SetInterruptHandler(void (*pirqhandler)(void));
  void SetPeriodus(long unsigned periodus);
  void Start();
  void Stop();
  void SetPulseCount(long unsigned pulsecount);
  void DecrementStopPulseCount();
};
//
#endif  // defined(PROCESSOR_STM32F103C8)
//
#endif  // IrqTimerStm_h
