#ifndef IrqTimerDue_h
#define IrqTimerDue_h
//
#include "Defines.h"
#if defined(PROCESSOR_ARDUINODUE)
//
#include "IrqTimerBase.h"
#include "IrqTimerBaseDue.h"
//
class CIrqTimerDue : public CIrqTimerBase
{
  protected:
  int FChannel;
  CIrqTimerBaseDue* FPIrqTimerBaseDue;
  void (*FPIrqHandler)(void);
  long unsigned FPulseCount;
  //
  public:
  CIrqTimerDue(int channel);
  void SetInterruptHandler(void (*pirqhandler)(void));
  void SetPeriodus(long unsigned periodus);
  void Start();
  void Stop();
  void SetPulseCount(long unsigned pulsecount);
  void DecrementStopPulseCount();
};
//
#endif  // defined(PROCESSOR_ARDUINODUE)
//
#endif  // IrqTimerDue_h
