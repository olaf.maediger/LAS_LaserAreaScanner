#include "Defines.h"
#if defined(PROCESSOR_ARDUINODUE)
//
#include "IrqTimerDue.h"
//
CIrqTimerDue::CIrqTimerDue(int channel)
  :CIrqTimerBase()
{
  FChannel = channel;
  FPIrqTimerBaseDue = new CIrqTimerBaseDue(channel);
  // FPIrqTimerBaseDue->setChannelMode(channel, TIMER_OUTPUTCOMPARE);
}

void CIrqTimerDue::SetInterruptHandler(void (*pirqhandler)(void))
{
  FPIrqHandler = pirqhandler;
  FPIrqTimerBaseDue->SetInterruptHandler(FPIrqHandler);
}

void CIrqTimerDue::SetPeriodus(long unsigned periodus)
{
  FPIrqTimerBaseDue->SetPeriodus(periodus);
}

void CIrqTimerDue::Start()
{
  FPIrqTimerBaseDue->Start();
}

void CIrqTimerDue::Stop()
{
  FPIrqTimerBaseDue->Stop(); 
}

void CIrqTimerDue::SetPulseCount(long unsigned pulsecount)
{ 
  FPulseCount = pulsecount;
}
void CIrqTimerDue::DecrementStopPulseCount()
{
  if (0 < FPulseCount)
  {
    FPulseCount--;
  }
  if (0 == FPulseCount)
  {
    Stop();
  }
}
//
#endif  // defined(PROCESSOR_ARDUINODUE)


