#include "Defines.h"
#if defined(PROCESSOR_ARDUINODUE)
#include "HardwareTimerDue.h"
//
const CHardwareTimerDue::Timer CHardwareTimerDue::Timers[NUM_TIMERS] = 
{
	{TC0,0,TC0_IRQn},
	{TC0,1,TC1_IRQn},
	{TC0,2,TC2_IRQn},
	{TC1,0,TC3_IRQn},
	{TC1,1,TC4_IRQn},
	{TC1,2,TC5_IRQn},
#if NUM_TIMERS > 6
	{TC2,0,TC6_IRQn},
	{TC2,1,TC7_IRQn},
	{TC2,2,TC8_IRQn},
#endif
};
//
// Fix for compatibility with Servo library
#ifdef USING_SERVO_LIB
	// Set callbacks as used, allowing DueTimer::getAvailable() to work
	void (*CHardwareTimerDue::FCallbacks[NUM_TIMERS])() = 
	{
		(void (*)()) 1, // Timer 0 - Occupied
		(void (*)()) 0, // Timer 1
		(void (*)()) 1, // Timer 2 - Occupied
		(void (*)()) 1, // Timer 3 - Occupied
		(void (*)()) 1, // Timer 4 - Occupied
		(void (*)()) 1, // Timer 5 - Occupied
#if NUM_TIMERS > 6
		(void (*)()) 0, // Timer 6
		(void (*)()) 0, // Timer 7
		(void (*)()) 0  // Timer 8
#endif
	};
#else
	void (*CHardwareTimerDue::FCallbacks[NUM_TIMERS])() = {};
#endif
		
#if NUM_TIMERS > 6
double CHardwareTimerDue::FFrequency[NUM_TIMERS] = {-1,-1,-1,-1,-1,-1,-1,-1,-1};
#else
double CHardwareTimerDue::FFrequency[NUM_TIMERS] = {-1,-1,-1,-1,-1,-1};
#endif
//
/*
	Initializing all timers, so you can use them like this: Timer0.start();
*/
CHardwareTimerDue Timer(0);

CHardwareTimerDue Timer1(1);
// Fix for compatibility with Servo library
#ifndef USING_SERVO_LIB
	CHardwareTimerDue Timer0(0);
	CHardwareTimerDue Timer2(2);
	CHardwareTimerDue Timer3(3);
	CHardwareTimerDue Timer4(4);
	CHardwareTimerDue Timer5(5);
#endif
#if NUM_TIMERS > 6
  CHardwareTimerDue Timer6(6);
  CHardwareTimerDue Timer7(7);
  CHardwareTimerDue Timer8(8);
#endif

CHardwareTimerDue::CHardwareTimerDue(unsigned short timerid) : FTimerID(timerid)
{
}

CHardwareTimerDue CHardwareTimerDue::GetAvailable(void)
{
	for (int i = 0; i < NUM_TIMERS; i++)
	{
		if (!FCallbacks[i]) return CHardwareTimerDue(i);
	}	// Default, return Timer0;
	return CHardwareTimerDue(0);
}
//
CHardwareTimerDue& CHardwareTimerDue::SetInterruptHandler(void (*isr)())
{
	FCallbacks[FTimerID] = isr;
	return *this;
}

CHardwareTimerDue& CHardwareTimerDue::ResetInterruptHandler(void)
{
	Stop(); // Stop the currently running timer
	FCallbacks[FTimerID] = NULL;
	return *this;
}

CHardwareTimerDue& CHardwareTimerDue::Start(double microseconds)
{
	if(microseconds > 0) SetPeriodus(microseconds);
	if(FFrequency[FTimerID] <= 0) SetFrequencyHz(1);
	NVIC_ClearPendingIRQ(Timers[FTimerID].irq);
	NVIC_EnableIRQ(Timers[FTimerID].irq);	
	TC_Start(Timers[FTimerID].tc, Timers[FTimerID].channel);
	return *this;
}

CHardwareTimerDue& CHardwareTimerDue::Stop(void)
{
	NVIC_DisableIRQ(Timers[FTimerID].irq);	
	TC_Stop(Timers[FTimerID].tc, Timers[FTimerID].channel);
	return *this;
}
//
uint8_t CHardwareTimerDue::GetBestClock(double frequency, uint32_t& retRC)
{ /*
		Pick the best Clock, thanks to Ogle Basil Hall!
		Timer		Definition
		TIMER_CLOCK1	MCK /  2
		TIMER_CLOCK2	MCK /  8
		TIMER_CLOCK3	MCK / 32
		TIMER_CLOCK4	MCK /128
	*/
	const struct 
	{
		uint8_t flag;
		uint8_t divisor;
	} clockConfig[] = 
	{
		{ TC_CMR_TCCLKS_TIMER_CLOCK1,   2 },
		{ TC_CMR_TCCLKS_TIMER_CLOCK2,   8 },
		{ TC_CMR_TCCLKS_TIMER_CLOCK3,  32 },
		{ TC_CMR_TCCLKS_TIMER_CLOCK4, 128 }
	};
	float ticks;
	float error;
	int clkId = 3;
	int bestClock = 3;
	float bestError = 9.999e99;
	do
	{
		ticks = (float) SystemCoreClock / frequency / (float) clockConfig[clkId].divisor;
		// error = abs(ticks - round(ticks));
		error = clockConfig[clkId].divisor * abs(ticks - round(ticks));	// Error comparison needs scaling
		if (error < bestError)
		{
			bestClock = clkId;
			bestError = error;
		}
	} while (clkId-- > 0);
	ticks = (float) SystemCoreClock / frequency / (float) clockConfig[bestClock].divisor;
	retRC = (uint32_t) round(ticks);
	return clockConfig[bestClock].flag;
}

CHardwareTimerDue& CHardwareTimerDue::SetFrequencyHz(double frequency)
{	// Prevent negative frequencies
	if(frequency <= 0) { frequency = 1; }
	// Remember the frequency — see below how the exact frequency is reported instead
	//_frequency[timer] = frequency;
	// Get current timer configuration
	Timer t = Timers[FTimerID];
	uint32_t rc = 0;
	uint8_t clock;
	// Tell the Power Management Controller to disable 
	// the write protection of the (Timer/Counter) registers:
	pmc_set_writeprotect(false);
	// Enable clock for the timer
	pmc_enable_periph_clk((uint32_t)t.irq);
	// Find the best clock for the wanted frequency
	clock = GetBestClock(frequency, rc);
	switch (clock) 
	{
	  case TC_CMR_TCCLKS_TIMER_CLOCK1:
	    FFrequency[FTimerID] = (double)SystemCoreClock / 2.0 / (double)rc;
	    break;
	  case TC_CMR_TCCLKS_TIMER_CLOCK2:
	    FFrequency[FTimerID] = (double)SystemCoreClock / 8.0 / (double)rc;
	    break;
	  case TC_CMR_TCCLKS_TIMER_CLOCK3:
	    FFrequency[FTimerID] = (double)SystemCoreClock / 32.0 / (double)rc;
	    break;
	  default: // TC_CMR_TCCLKS_TIMER_CLOCK4
	    FFrequency[FTimerID] = (double)SystemCoreClock / 128.0 / (double)rc;
	    break;
	}
	// Set up the Timer in waveform mode which creates a PWM
	// in UP mode with automatic trigger on RC Compare
	// and sets it up with the determined internal clock as clock input.
	TC_Configure(t.tc, t.channel, TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC | clock);
	// Reset counter and fire interrupt when RC value is matched:
	TC_SetRC(t.tc, t.channel, rc);
	// Enable the RC Compare Interrupt...
	t.tc->TC_CHANNEL[t.channel].TC_IER=TC_IER_CPCS;
	// ... and disable all others.
	t.tc->TC_CHANNEL[t.channel].TC_IDR=~TC_IER_CPCS;
	return *this;
}

CHardwareTimerDue& CHardwareTimerDue::SetPeriodus(double microseconds)
{	// Convert period in microseconds to frequency in Hz
	double F = 1000000.0 / microseconds;	
	SetFrequencyHz(F);
	return *this;
}

double CHardwareTimerDue::GetFrequencyHz(void) const 
{
	return FFrequency[FTimerID];
}

double CHardwareTimerDue::GetPeriodus(void) const 
{
	return 1.0 / GetFrequencyHz()*1000000;
}

/*
	Implementation of the timer callbacks defined in 
	arduino-1.5.2/hardware/arduino/sam/system/CMSIS/Device/ATMEL/sam3xa/include/sam3x8e.h
*/
// Fix for compatibility with Servo library
#ifndef USING_SERVO_LIB
void TC0_Handler(void)
{
	TC_GetStatus(TC0, 0);
	CHardwareTimerDue::FCallbacks[0]();
}
#endif
void TC1_Handler(void)
{
	TC_GetStatus(TC0, 1);
	CHardwareTimerDue::FCallbacks[1]();
}
// Fix for compatibility with Servo library
#ifndef USING_SERVO_LIB
void TC2_Handler(void)
{
	TC_GetStatus(TC0, 2);
	CHardwareTimerDue::FCallbacks[2]();
}
void TC3_Handler(void)
{
	TC_GetStatus(TC1, 0);
	CHardwareTimerDue::FCallbacks[3]();
}
void TC4_Handler(void)
{
	TC_GetStatus(TC1, 1);
	CHardwareTimerDue::FCallbacks[4]();
}
void TC5_Handler(void)
{
	TC_GetStatus(TC1, 2);
	CHardwareTimerDue::FCallbacks[5]();
}
#endif
#if NUM_TIMERS > 6
void TC6_Handler(void)
{
	TC_GetStatus(TC2, 0);
	CHardwareTimerDue::FCallbacks[6]();
}
void TC7_Handler(void)
{
	TC_GetStatus(TC2, 1);
	CHardwareTimerDue::FCallbacks[7]();
}
void TC8_Handler(void)
{
	TC_GetStatus(TC2, 2);
	CHardwareTimerDue::FCallbacks[8]();
}
#endif
//
#endif // defined(PROCESSOR_ARDUINODUE)

