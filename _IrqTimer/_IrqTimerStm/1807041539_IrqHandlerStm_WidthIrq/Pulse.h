#ifndef Pulse_h
#define Pulse_h
//
#include "IrqTimerStm.h"
//
const long unsigned WIDTH_PULSECOUNT = 1;
//
class CPulse
{
  protected:
  int FChannelWidth;
  CIrqTimerStm* FPTimerPulseWidth;
  void (*FPIrqHandlerWidth)(void);
  long unsigned FPulseCount;
  //
  public:
  CPulse(int channelwidth, CIrqTimerStm* ptimerpulsewidth);
  //
  void SetInterruptHandlerWidth(void (*pirqhandlerwidth)(void));
  void SetPeriodWidthus(long unsigned periodwidthus);
  void StartWidth();
  void StopWidth();
  //
//  void Start();
//  void Stop();
  void SetPulseCount(long unsigned pulsecount);
  bool DecrementStopPulseCount();
};
//
#endif  // Pulse_h
