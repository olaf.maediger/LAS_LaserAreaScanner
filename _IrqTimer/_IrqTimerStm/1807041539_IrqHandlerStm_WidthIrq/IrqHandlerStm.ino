#include "IrqTimerStm.h"
#include "Pulse.h"
//
const int PIN_LEDSYSTEM = PC13;
const long unsigned PERIOD_PULSEPERIODUS  = 1000000;
const long unsigned PERIOD_PULSECOUNT     = 3;
const long unsigned WIDTH_PULSEPERIODUS   = 50000;
// const long unsigned WIDTH_PULSECOUNT      = 1;
//
CIrqTimerStm TimerPulsePeriod(3);
//CIrqTimerStm TimerPulseWidth(4);
CIrqTimerStm TPW(4);
CPulse PulseTrigger(4, &TPW);
//
void IrqHandlerPulsePeriod(void)
{
  digitalWrite(PIN_LEDSYSTEM, false);
  PulseTrigger.StopWidth();
  PulseTrigger.SetPeriodWidthus(WIDTH_PULSEPERIODUS);
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidth(void)
{
  digitalWrite(PIN_LEDSYSTEM, true);
  //TimerPulsePeriod.DecrementStopPulseCount();
  if (PulseTrigger.DecrementStopPulseCount())
  {
    TimerPulsePeriod.Stop();
  }
  //TimerPulseWidth.Stop();
  PulseTrigger.StopWidth();
}
//
void setup()
{
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, false);
  delay(1000);
  //
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, true);
  delay(2000);
  //
  PulseTrigger.StopWidth();
  PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
  PulseTrigger.SetPeriodWidthus(WIDTH_PULSEPERIODUS);
  PulseTrigger.StartWidth();
//  TimerPulseWidth.Stop();
//  TimerPulseWidth.SetInterruptHandler(IrqHandlerPulseWidth);
//  TimerPulseWidth.SetPeriodus(WIDTH_PULSEPERIODUS);
//  TimerPulseWidth.SetPulseCount(WIDTH_PULSECOUNT);
  //
  TimerPulsePeriod.Stop();
  TimerPulsePeriod.SetInterruptHandler(IrqHandlerPulsePeriod);
  TimerPulsePeriod.SetPeriodus(PERIOD_PULSEPERIODUS);
  TimerPulsePeriod.SetPulseCount(1 + PERIOD_PULSECOUNT); // ??? Necessary Correction +1 !!!
  TimerPulsePeriod.Start();
  //
  PulseTrigger.SetPulseCount(1 + PERIOD_PULSECOUNT);
}
//
void loop()
{
  delay(5000);
  TimerPulsePeriod.Stop();
  TimerPulsePeriod.SetInterruptHandler(IrqHandlerPulsePeriod);
  TimerPulsePeriod.SetPeriodus(PERIOD_PULSEPERIODUS);
  TimerPulsePeriod.SetPulseCount(PERIOD_PULSECOUNT);
  TimerPulsePeriod.Start();
  PulseTrigger.SetPulseCount(PERIOD_PULSECOUNT);
}










//
//HardwareTimer TimerFrequency(2);
////
//void IrqHandlerFrequency(void)
//{
//  StateLed = !StateLed;
//  digitalWrite(PIN_LEDSYSTEM, StateLed);
//}
////
//void setup() 
//{
//  pinMode(PIN_LEDSYSTEM, OUTPUT);
//  digitalWrite(PIN_LEDSYSTEM, StateLed);
//  //
////  //TimerFrequency.pause();
////  TimerFrequency.setChannel1Mode(TIMER_OUTPUTCOMPARE);  
//  TimerFrequency.setPeriod(500000);
////  TimerFrequency.setCompare1(1);
//  TimerFrequency.attachInterrupt(2, IrqHandlerFrequency);
////  //TimerFrequency.resume();
//}
//
//void loop() 
//{
//  delay(500);
////  digitalWrite(PIN_LEDSYSTEM, HIGH);
////  delay(500);
////  digitalWrite(PIN_LEDSYSTEM, LOW);
//}
