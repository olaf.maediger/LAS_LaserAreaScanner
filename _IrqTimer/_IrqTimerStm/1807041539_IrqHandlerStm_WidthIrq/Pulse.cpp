#include "Pulse.h"
//
CPulse::CPulse(int channelwidth, CIrqTimerStm* ptimerpulsewidth)
{
  FChannelWidth = channelwidth;
  FPTimerPulseWidth = ptimerpulsewidth;
}

void CPulse::SetInterruptHandlerWidth(void (*pirqhandlerwidth)(void))
{
  FPIrqHandlerWidth = pirqhandlerwidth;
  FPTimerPulseWidth->SetInterruptHandler(FPIrqHandlerWidth);
}

void CPulse::SetPeriodWidthus(long unsigned periodwidthus)
{
  //FPeriodWidthus = periodwidthus;
  FPTimerPulseWidth->SetPeriodus(periodwidthus);//FPeriodWidthus);
}

void CPulse::StartWidth()
{
  FPTimerPulseWidth->SetPulseCount(WIDTH_PULSECOUNT);
  FPTimerPulseWidth->Start();
}

void CPulse::StopWidth()
{
  FPTimerPulseWidth->Stop();  
}



void CPulse::SetPulseCount(long unsigned pulsecount)
{ 
  FPulseCount = pulsecount;
}

bool CPulse::DecrementStopPulseCount()
{
  if (0 < FPulseCount)
  {
    FPulseCount--;
  }
  if (0 == FPulseCount)
  {
    return true; //Stop();
  }
  return false;
}



