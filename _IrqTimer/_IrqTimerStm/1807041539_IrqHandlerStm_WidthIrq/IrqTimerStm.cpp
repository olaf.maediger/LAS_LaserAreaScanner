#include "IrqTimerStm.h"
//
CIrqTimerStm::CIrqTimerStm(int channel)
  :CIrqTimerBase()
{
  FChannel = channel;
  FPHardwareTimer = new HardwareTimer(channel);
  FPHardwareTimer->setChannelMode(channel, TIMER_OUTPUTCOMPARE);
}

void CIrqTimerStm::SetInterruptHandler(void (*pirqhandler)(void))
{
  FPIrqHandler = pirqhandler;
  FPHardwareTimer->attachInterrupt(FChannel, FPIrqHandler);
}

void CIrqTimerStm::SetPeriodus(long unsigned periodus)
{
  FPHardwareTimer->setPeriod(periodus);
  //???FPHardwareTimer->setPrescaleFactor(0xFFFF);
  FPHardwareTimer->setCompare1(0x0000);
  FPHardwareTimer->setCompare2(0x0000);
  FPHardwareTimer->setCompare3(0x0000);
  FPHardwareTimer->setCompare4(0x0000);
}

void CIrqTimerStm::Start()
{
  FPHardwareTimer->resume();
}

void CIrqTimerStm::Stop()
{
  FPHardwareTimer->pause(); 
}

void CIrqTimerStm::SetPulseCount(long unsigned pulsecount)
{ 
  FPulseCount = pulsecount;
}
void CIrqTimerStm::DecrementStopPulseCount()
{
  if (0 < FPulseCount)
  {
    FPulseCount--;
  }
  if (0 == FPulseCount)
  {
    Stop();
  }
}



