#include "IrqTimerStm.h"
//
CIrqTimerStm::CIrqTimerStm(int channel)
  :CIrqTimerBase(channel)
{
  FPHardwareTimer = new HardwareTimer(channel);
}

void CIrqTimerStm::SetInterruptHandler(void (*pirqhandler)(void))
{
  FPIrqHandler = pirqhandler;
  FPHardwareTimer->attachInterrupt(FChannel, FPIrqHandler);
}

void CIrqTimerStm::SetPeriodus(long unsigned periodus)
{
  FPHardwareTimer->setPeriod(periodus);
}

void CIrqTimerStm::Start()
{
  FPHardwareTimer->resume();
}

void CIrqTimerStm::Stop()
{
  FPHardwareTimer->pause(); 
}


