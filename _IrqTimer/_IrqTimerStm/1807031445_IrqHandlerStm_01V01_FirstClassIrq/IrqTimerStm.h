#ifndef IrqTimerStm_h
#define IrqTimerStm_h
//
#include <HardwareTimer.h>
#include "IrqTimerBase.h"
//
class CIrqTimerStm : public CIrqTimerBase
{
  protected:
  int FChannel;
  HardwareTimer* FPHardwareTimer;
  void (*FPIrqHandler)(void);
  //
  public:
  CIrqTimerStm(int channel);
  void SetInterruptHandler(void (*pirqhandler)(void));
  void SetPeriodus(long unsigned periodus);
  void Start();
  void Stop();
};
//
#endif  // IrqTimerStm_h
