#ifndef Pulse_h
#define Pulse_h
//
#include "IrqTimerStm.h"
//
const long unsigned WIDTH_PULSECOUNT = 1;
//
class CPulse
{
  protected:
  long unsigned FPeriodus, FWidthus;
  HardwareTimer* FPTimerPeriod;
  //CIrqTimerStm* FPTimerPeriod;
  //CIrqTimerStm* FPTimerWidth = 0;
  //
  //
  public:
  CPulse(void);//int channelperiod, int channelwidth);
  void SetInterruptHandler(void (*pirqhandlerperiod)(void)); 
                           //void (*pirqhandlerwidth)(void));
  void SetPeriodus(long unsigned periodus, 
                   long unsigned widthus);

  void DecrementStopPulseCount();
                   
  void Start(long unsigned pulsecount);
  void Stop();
//  void StartWidth();
//  void StopWidth();
};
//  
#endif  // Pulse_h
