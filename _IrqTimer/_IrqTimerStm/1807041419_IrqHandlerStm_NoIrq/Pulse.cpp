#include "Pulse.h"
//
CPulse::CPulse(void)//int channelperiod, int channelwidth)
{
  FPTimerPeriod = new HardwareTimer(1);
  //FPTimerPeriod = new CIrqTimerStm(channelperiod);
  // FPTimerWidth = new CIrqTimerStm(channelwidth);
}

void CPulse::SetInterruptHandler(void (*pirqhandlerperiod)(void))
                                 //void (*pirqhandlerwidth)(void))
{
  //FPTimerPeriod->pause();
  FPTimerPeriod->attachInterrupt(2, pirqhandlerperiod);
  
  //FPTimerPeriod->Stop();
  //FPTimerPeriod->SetInterruptHandler(pirqhandlerperiod); 
  //
  //FTimerWidth->Stop();
  //FTimerWidth->SetInterruptHandler(pirqhandlerwidth);
}

void CPulse::SetPeriodus(long unsigned periodus, 
                         long unsigned widthus)
{  
  FPTimerPeriod->pause();
  //  
  //FPTimerPeriod->Stop();
//  FPTimerWidth->Stop();
  //
  FPeriodus = periodus;
  FWidthus = widthus;
}

void CPulse::DecrementStopPulseCount()
{
  //FPTimerPeriod->DecrementStopPulseCount();
}

void CPulse::Start(long unsigned pulsecount)
{
  //FPTimerPeriod->pause();
  FPTimerPeriod->setPeriod(100000);
  FPTimerPeriod->resume();
  //
  //FPTimerPeriod->Stop();
  //FPTimerWidth->Stop();
  //
//  FPTimerWidth->SetPeriodus(500000);//FWidthus);
//  FPTimerWidth->SetPulseCount(WIDTH_PULSECOUNT);
  //
//  FPTimerPeriod->SetPeriodus(100000);//FPeriodus);
//  FPTimerPeriod->SetPulseCount(10);//pulsecount);
//  //
//  FPTimerPeriod->Start();  
}

void CPulse::Stop()
{
  FPTimerPeriod->pause();
  //FPTimerPeriod->Stop();
  // FPTimerWidth->Stop();
}

//void CPulse::StartWidth()
//{
//  FPTimerWidth->Stop();
//  FPTimerWidth->SetPeriodus(500000);//FWidthus);
//  FPTimerWidth->SetPulseCount(WIDTH_PULSECOUNT);
//  FPTimerWidth->Start();
//}
//
//void CPulse::StopWidth()
//{
//  FPTimerWidth->Stop();
//  // FPTimerPeriod->DecrementStopPulseCount();
//}

  


