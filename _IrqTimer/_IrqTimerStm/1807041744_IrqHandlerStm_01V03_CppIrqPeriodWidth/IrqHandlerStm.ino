#include "IrqTimerStm.h"
#include "Pulse.h"
//
const int PIN_LEDSYSTEM = PC13;
const long unsigned PERIOD_PULSEPERIODUS  = 1000000;
const long unsigned PERIOD_PULSECOUNT     = 3;
const long unsigned WIDTH_PULSEPERIODUS   = 50000;
// const long unsigned WIDTH_PULSECOUNT      = 1;
//
//CIrqTimerStm TimerPulsePeriod(3);
//CIrqTimerStm TimerPulseWidth(4);
CIrqTimerStm TPP(3);
CIrqTimerStm TPW(4);
CPulse PulseTrigger(3, &TPP, 4, &TPW);
//
void IrqHandlerPulsePeriod(void)
{
  digitalWrite(PIN_LEDSYSTEM, false);
  PulseTrigger.StopWidth();
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidth(void)
{
  digitalWrite(PIN_LEDSYSTEM, true);
  //TimerPulsePeriod.DecrementStopPulseCount();
  if (PulseTrigger.DecrementStopPulseCount())
  {
    /// TimerPulsePeriod.Stop();
    PulseTrigger.Stop();
  }
  //TimerPulseWidth.Stop();
  PulseTrigger.StopWidth();
}
//
void setup()
{
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, false);
  delay(1000);
  //
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, true);
  delay(2000);
  // 
//  TimerPulseWidth.Stop();
//  TimerPulseWidth.SetInterruptHandler(IrqHandlerPulseWidth);
//  TimerPulseWidth.SetPeriodus(WIDTH_PULSEPERIODUS);
//  TimerPulseWidth.SetPulseCount(WIDTH_PULSECOUNT);
  //
//  TimerPulsePeriod.Stop();
//  TimerPulsePeriod.SetInterruptHandler(IrqHandlerPulsePeriod);
//  TimerPulsePeriod.SetPeriodus(PERIOD_PULSEPERIODUS);
//  TimerPulsePeriod.SetPulseCount(1 + PERIOD_PULSECOUNT); // ??? Necessary Correction +1 !!!
//  TimerPulsePeriod.Start();
  //
  PulseTrigger.StopWidth();
  PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
  PulseTrigger.SetPulseWidthus(WIDTH_PULSEPERIODUS);
  //  
  PulseTrigger.Stop();
  PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
  PulseTrigger.SetPulsePeriodus(PERIOD_PULSEPERIODUS);
  PulseTrigger.SetPulseCount(1 + PERIOD_PULSECOUNT);
  PulseTrigger.Start();  
}
//
void loop()
{
  delay(5000);
//  TimerPulsePeriod.Stop();
//  TimerPulsePeriod.SetInterruptHandler(IrqHandlerPulsePeriod);
//  TimerPulsePeriod.SetPeriodus(PERIOD_PULSEPERIODUS);
//  TimerPulsePeriod.SetPulseCount(PERIOD_PULSECOUNT);
//  TimerPulsePeriod.Start();
  //
  PulseTrigger.Stop();
  PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
  PulseTrigger.SetPulsePeriodus(PERIOD_PULSEPERIODUS);
  PulseTrigger.SetPulseCount(PERIOD_PULSECOUNT);
  PulseTrigger.Start(); 
}









