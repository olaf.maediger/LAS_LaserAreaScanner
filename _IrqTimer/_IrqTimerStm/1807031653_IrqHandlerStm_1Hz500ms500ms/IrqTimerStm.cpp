#include "IrqTimerStm.h"
//
CIrqTimerStm::CIrqTimerStm(int channel)
  :CIrqTimerBase(channel)
{
  FPHardwareTimer = new HardwareTimer(channel);
}

void CIrqTimerStm::SetInterruptHandler(void (*pirqhandler)(void))
{
  FPIrqHandler = pirqhandler;
  FPHardwareTimer->attachInterrupt(FChannel, FPIrqHandler);
}

void CIrqTimerStm::SetPeriodus(long unsigned periodus)
{
  FPHardwareTimer->setPeriod(periodus);
}

void CIrqTimerStm::Start()
{
  FPHardwareTimer->resume();
}

void CIrqTimerStm::Stop()
{
  FPHardwareTimer->pause(); 
}

void CIrqTimerStm::SetPulseCount(long unsigned pulsecount)
{
  FPulseCount = pulsecount;
}
void CIrqTimerStm::DecrementStopPulseCount()
{
  if (0 < FPulseCount)
  {
    FPulseCount--;
  }
  if (0 == FPulseCount)
  {
    Stop();
  }
}



