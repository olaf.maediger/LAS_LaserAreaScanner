#include "IrqTimerStm.h"
#include "Pulse.h"
//
const int PIN_LEDSYSTEM = PC13;
const long unsigned PERIOD_PULSECOUNT_INITIALISATION =       1;
const long unsigned PERIOD_PULSEPERIODUS             = 1000000;
const long unsigned WIDTH_PULSEPERIODUS              =  500000;
const long unsigned PERIOD_PULSECOUNT                =       3;
//
CIrqTimerStm TPP(3);
CIrqTimerStm TPW(4);
CPulse PulseTrigger(3, &TPP, 4, &TPW);
//
void IrqHandlerPulsePeriodInitialisation(void)
{ // no operation!
  // PulseTrigger - PulseWidth - Start
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidthInitialisation(void)
{ // no operation!
  // PulseTrigger - PulseWidth - Stop
  PulseTrigger.StopWidth();
  PulseTrigger.DecrementStopPulseCount();
}
//
void IrqHandlerPulsePeriod(void)
{
  digitalWrite(PIN_LEDSYSTEM, false);
  // PulseTrigger - PulseWidth - Start
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidth(void)
{
  digitalWrite(PIN_LEDSYSTEM, true);
  // PulseTrigger - PulseWidth - Stop
  PulseTrigger.StopWidth();
  PulseTrigger.DecrementStopPulseCount();
}
//
void setup()
{
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, false);
  delay(2000);
  //
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, true);
  delay(2000);
  // 
  // PulseTrigger - PulseWidth
  PulseTrigger.StopWidth();
  PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidthInitialisation);
  PulseTrigger.SetPulseWidthus(WIDTH_PULSEPERIODUS);
  //
  // PulseTrigger - PulsePeriod/PulseCount
  PulseTrigger.Stop();
  PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriodInitialisation);
  PulseTrigger.SetPulsePeriodus(PERIOD_PULSEPERIODUS);
  PulseTrigger.SetPulseCount(PERIOD_PULSECOUNT_INITIALISATION);
  PulseTrigger.Start();  
  delay(1000);
  PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
  PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
}
//
void loop()
{
  delay(5000);
  //
  // PulseTrigger - PulsePeriod/PulseCount
  PulseTrigger.Stop();
  PulseTrigger.SetPulsePeriodus(PERIOD_PULSEPERIODUS);
  PulseTrigger.SetPulseCount(PERIOD_PULSECOUNT);
  PulseTrigger.Start(); 
}









