#ifndef Pulse_h
#define Pulse_h
//
#include "IrqTimerStm.h"
//
const long unsigned WIDTH_PULSECOUNT = 1;
//
class CPulse
{
  protected:
  // Period
  int FChannelPeriod;
  CIrqTimerStm* FPTimerPulsePeriod;
  void (*FPIrqHandlerPeriod)(void);
  long unsigned FPulsePeriodus;  
  long unsigned FPulseCount;
  // Width
  int FChannelWidth;
  CIrqTimerStm* FPTimerPulseWidth;
  void (*FPIrqHandlerWidth)(void);
  long unsigned FPulseWidthus;
  //
  public:
  CPulse(int channelperiod, CIrqTimerStm* ptimerpulseperiod,
         int channelwidth, CIrqTimerStm* ptimerpulsewidth);
  //  Period
  void SetInterruptHandlerPeriod(void (*pirqhandlerperiod)(void));
  void SetPulsePeriodus(long unsigned pulseperiodus);
  void Start();
  void Stop();
  void SetPulseCount(long unsigned pulsecount);
  bool DecrementStopPulseCount();
  //  Width
  void SetInterruptHandlerWidth(void (*pirqhandlerwidth)(void));
  void SetPulseWidthus(long unsigned pulsewidthus);
  void StartWidth();
  void StopWidth();
};
//
#endif  // Pulse_h
