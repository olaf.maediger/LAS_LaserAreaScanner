#include "Defines.h"
//
#if defined(PROCESSOR_ARDUINODUE)
//
#ifndef IrqTimerDue_h
#define IrqTimerDue_h
//
#include "Arduino.h"
//
#define NUM_TIMERS  9
//
class CIrqTimerBase
{
  public:
	const unsigned short timer;
	static double _frequency[NUM_TIMERS];
	static uint8_t bestClock(double frequency, uint32_t& retRC);
  friend void TC0_Handler(void);
  friend void TC1_Handler(void);
  friend void TC2_Handler(void);
  friend void TC3_Handler(void);
  friend void TC4_Handler(void);
  friend void TC5_Handler(void);
  friend void TC6_Handler(void);
  friend void TC7_Handler(void);
  friend void TC8_Handler(void);
  //
  static void (*callbacks[NUM_TIMERS])(CIrqTimer* pirqtimer);
  //
  struct Timer
  {
    Tc *tc;
  	uint32_t channel;
    IRQn_Type irq;
  };

  static const Timer Timers[NUM_TIMERS];
  //
  public:
	static CIrqTimerBase getAvailable(void);
	CIrqTimerBase(unsigned short _timer);
  void SetInterruptHandler(PIrqHandler pirqhandler); //void (*isr)());
  CIrqTimerBase& detachInterrupt(void);
  CIrqTimerBase& start(double microseconds = -1);
  CIrqTimerBase& stop(void);
  CIrqTimerBase& setFrequency(double frequency);
  CIrqTimerBase& setPeriod(double microseconds);
	double getFrequency(void) const;
  double getPeriod(void) const;
  inline __attribute__((always_inline)) bool operator== (const CIrqTimerBase& rhs) const
    {return timer == rhs.timer; };
  inline __attribute__((always_inline)) bool operator!= (const CIrqTimerBase& rhs) const
    {return timer != rhs.timer; };
};

//// Just to call Timer.getAvailable instead of Timer::getAvailable() :
//extern DueTimer Timer;
//
extern CIrqTimerBase Timer1;
extern CIrqTimerBase Timer0;
extern CIrqTimerBase Timer2;
extern CIrqTimerBase Timer3;
extern CIrqTimerBase Timer4;
extern CIrqTimerBase Timer5;
extern CIrqTimerBase Timer6;
extern CIrqTimerBase Timer7;
extern CIrqTimerBase Timer8;
//
#endif // IrqTimerDue
//
#endif // defined(PROCESSOR_ARDUINODUE)

