#include "Defines.h"
//
#if defined(PROCESSOR_ARDUINOUNO)
//
#include <avr/io.h>
#include <avr/interrupt.h>
//
#define RESOLUTION 65536    // Timer1 is 16 bit

class CIrqTimerBase
{
  public:  
  // properties
  unsigned int pwmPeriod;
  unsigned char clockSelectBits;
  // methods
  void initialize(long microseconds=1000000);
  void start();
  void stop();
  void restart();
  void pwm(char pin, int duty, long microseconds=-1);
  void disablePwm(char pin);
  void attachInterrupt(void (*isr)(), long microseconds=-1);
  void detachInterrupt();
  void setPeriod(long microseconds);
  void setPwmDuty(char pin, int duty);
  void (*isrCallback)();
};
//
#endif // defined(ARDUINOUNO)
