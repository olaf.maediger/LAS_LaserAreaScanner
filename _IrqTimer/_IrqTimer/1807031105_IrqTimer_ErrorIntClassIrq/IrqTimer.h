#ifndef IrqTimer_h
#define IrqTimer_h
//
#include "Defines.h"
//
#include "IrqTimerUno.h"
#include "IrqTimerMega.h"
#include "IrqTimerDue.h"
#include "IrqTimerStm.h"
//
class CIrqTimer
{
  private:
  CIrqTimerBase* FPIrqTimerBase;
  PIrqHandler FPIrqHandler;
  //
  public:
//  Boolean Open();
//  Boolean Close();
  Boolean Start(UInt32 periodus);
  Boolean Stop();
  void SetInterruptHandler(PIrqHandler pirqhandler);
};
//
#endif // IrqTimer_h

