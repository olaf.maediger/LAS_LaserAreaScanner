#include "Defines.h"
#include "IrqTimer.h"
//
bool StateLedSystem = false;
CIrqTimer IrqTimer;
//
void IrqHandler(CIrqTimer* pirqtimer)
{
  StateLedSystem = !StateLedSystem;
  digitalWrite(PIN_LEDSYSTEM, StateLedSystem);
}
//
void setup()
{
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, StateLedSystem);
  //
  IrqTimer.SetInterruptHandler(IrqHandler);
  IrqTimer.Start(500000);
}

void loop() 
{  
  delay(1000);
}
