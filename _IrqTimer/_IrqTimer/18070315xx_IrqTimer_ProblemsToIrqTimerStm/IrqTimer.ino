#include "Defines.h"
#include "IrqTimer.h"
//
class CDemo;
typedef void (CDemo::*PHandler)(void);
//
bool StateLedSystem = false;
//
void IrqHandler()
{
  StateLedSystem = !StateLedSystem;
  digitalWrite(PIN_LEDSYSTEM, StateLedSystem);
}


class CDemo
{
  public:
  void Handler(void)
  {
    //
  }

  void SetHandler()
  {
   
  }

};

CDemo Demo;
//
void setup()
{
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, StateLedSystem);
  Timer1.attachInterrupt(IrqHandler);
  Timer1.start(50000);

}

void loop() 
{  
  delay(1000);
}
