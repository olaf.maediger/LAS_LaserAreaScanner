#ifndef IrqTimer_h
#define IrqTimer_h
//
#include "Defines.h"
//
#include "IrqTimerUno.h"
#include "IrqTimerMega.h"
#include "IrqTimerDue.h"
#include "IrqTimerStm.h"
//
//typedef void (*PIrqHandler)(void);
//
class CIrqTimer
{
  private:
  CIrqTimerBase* FPIrqTimerBase;
  //
  public:
  Boolean Open();
  Boolean Close();
  Boolean Start();
  Boolean Stop();
  //void SetInterruptHandler(void (*isr)());
  //void SetInterruptHandler(PIrqHandler pirqhandler);
};
//
#endif // IrqTimer_h

