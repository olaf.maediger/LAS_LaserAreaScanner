#include "Defines.h"
#include "IrqTimer.h"
//
// int timer1_counter;

// only Stm32F103C8T6 : HardwareTimer HWTimer(3);

bool StateLedSystem = false;

void IrqHandler()
{
  StateLedSystem = !StateLedSystem;
  digitalWrite(PIN_LEDSYSTEM, StateLedSystem);
}

void setup()
{
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, StateLedSystem);
  Timer1.attachInterrupt(IrqHandler);
  Timer1.start(50000);
}

void loop() 
{  
  delay(1000);
}
