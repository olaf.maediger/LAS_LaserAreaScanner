#ifndef IrqTimer_h
#define IrqTimer_h
//
#include "Defines.h"
//
#include "IrqTimerUno.h"
#include "IrqTimerMega.h"
#include "IrqTimerDue.h"
#include "IrqTimerStm.h"
//
class CIrqTimer
{
  private:
  CIrqTimerBase* FPIrqTimerBase;
  //
  public:
  Boolean Open();
  Boolean Close();
  Boolean Start();
  Boolean Stop();
  void SetInterruptHandler(void (*isr)());
};
//
#endif // IrqTimer_h

