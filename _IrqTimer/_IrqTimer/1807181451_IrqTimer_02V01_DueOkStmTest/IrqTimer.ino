#include "Defines.h"
#include "Pulse.h"
//
const int TIMERCHANNEL_PULSEPERIOD        =       3;
const int TIMERCHANNEL_PULSEWIDTH         =       4;
const long unsigned TRIGGER_PULSEPERIODUS =  400000;
const long unsigned TRIGGER_PULSEWIDTHUS  =   50000;
const long unsigned TRIGGER_PULSECOUNT    =       5;
//
CPulse PulseTrigger(TIMERCHANNEL_PULSEPERIOD, TIMERCHANNEL_PULSEWIDTH);
//
// IrqHandler - Process
//
void IrqHandlerPulsePeriod(void)
{ // LedSystem - On
  digitalWrite(PIN_LEDSYSTEM, true);
  // PulseTrigger - PulseWidth - Start
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidth(void)
{ // LedSystem - Off
  digitalWrite(PIN_LEDSYSTEM, false);
  // PulseTrigger - PulseWidth - Stop
  PulseTrigger.StopWidth();
  PulseTrigger.DecrementStopPulseCount();
}

//
// Main - Initialisation
//
void setup()
{
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, true);
  delay(2000);
  //
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, false);
  delay(2000);
  // 
  PulseTrigger.Stop();
  PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
  PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
  PulseTrigger.SetPulsePeriodus(TRIGGER_PULSEPERIODUS);
  PulseTrigger.SetPulseWidthus(TRIGGER_PULSEWIDTHUS);
  PulseTrigger.SetPulseCount(TRIGGER_PULSECOUNT);
  PulseTrigger.Start();
}
//
// Main - Process
//
void loop()
{
  delay(5000);
  PulseTrigger.Stop();
  PulseTrigger.SetPulsePeriodus(TRIGGER_PULSEPERIODUS);
  PulseTrigger.SetPulseWidthus(TRIGGER_PULSEWIDTHUS);
  PulseTrigger.SetPulseCount(TRIGGER_PULSECOUNT);
  PulseTrigger.Start();
}

//
////
////CPulse PulseTrigger(TIMERCHANNEL_PULSEPERIOD, TIMERCHANNEL_PULSEWIDTH);
//////
////// IrqHandler - Initialisation
//////
////void IrqHandlerPulsePeriodInitialisation(void)
////{ // no operation!
////  // PulseTrigger - PulseWidth - Start
////  PulseTrigger.StartWidth();
////}
//////
////void IrqHandlerPulseWidthInitialisation(void)
////{ // no operation!
////  // PulseTrigger - PulseWidth - Stop
////  PulseTrigger.StopWidth();
////  PulseTrigger.DecrementStopPulseCount();
////}
//////
////// IrqHandler - Process
//////
////void IrqHandlerPulsePeriod(void)
////{ // LedSystem - On
////  digitalWrite(PIN_LEDSYSTEM, false);
////  // PulseTrigger - PulseWidth - Start
////  PulseTrigger.StartWidth();
////}
//////
////void IrqHandlerPulseWidth(void)
////{ // LedSystem - Off
////  digitalWrite(PIN_LEDSYSTEM, true);
////  // PulseTrigger - PulseWidth - Stop
////  PulseTrigger.StopWidth();
////  PulseTrigger.DecrementStopPulseCount();
////}
////
//// Main - Initialisation
////
//void setup()
//{
//  pinMode(PIN_LEDSYSTEM, OUTPUT);
//  digitalWrite(PIN_LEDSYSTEM, false);
//  delay(2000);
//  //
//  pinMode(PIN_LEDSYSTEM, OUTPUT);
//  digitalWrite(PIN_LEDSYSTEM, true);
//  delay(2000);
//  // 
////  // PulseTrigger - PulseWidth
////  PulseTrigger.StopWidth();
////  PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidthInitialisation);
////  PulseTrigger.SetPulseWidthus(WIDTH_PULSEPERIODUS);
////  //
////  // PulseTrigger - PulsePeriod/PulseCount
////  PulseTrigger.Stop();
////  PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriodInitialisation);
////  PulseTrigger.SetPulsePeriodus(PERIOD_PULSEPERIODUS);
////  PulseTrigger.SetPulseCount(PERIOD_PULSECOUNT_INITIALISATION);
////  PulseTrigger.Start();  
////  delay(1000);
////  PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
////  PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
//}
////
//// Main - Process
////
//void loop()
//{
//  delay(2000);
//  //
////  // PulseTrigger - PulsePeriod/PulseCount
////  PulseTrigger.Stop();
////  PulseTrigger.SetPulsePeriodus(PERIOD_PULSEPERIODUS);
////  PulseTrigger.SetPulseCount(PERIOD_PULSECOUNT);
////  PulseTrigger.Start(); 
//}
//








