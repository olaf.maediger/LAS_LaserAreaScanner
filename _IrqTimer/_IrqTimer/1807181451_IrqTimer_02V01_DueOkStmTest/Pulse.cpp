#include "Defines.h"
#include "Pulse.h"
//
CPulse::CPulse(int channelperiod, int channelwidth)
{
  FChannelPeriod = channelperiod;
  FChannelWidth = channelwidth;
  FPTimerPulsePeriod = new CIrqTimer(FChannelPeriod);
  FPTimerPulseWidth = new CIrqTimer(FChannelWidth);
}
//
//-------------------------------------------------------------------
//  Segment - Period
//-------------------------------------------------------------------
//
void CPulse::SetInterruptHandlerPeriod(void (*pirqhandlerperiod)(void))
{
  FPIrqHandlerPeriod = pirqhandlerperiod;
  FPTimerPulsePeriod->SetInterruptHandler(FPIrqHandlerPeriod);  
}

void CPulse::SetPulsePeriodus(long unsigned pulseperiodus)
{
  FPulsePeriodus = pulseperiodus;
  FPTimerPulsePeriod->SetPeriodus(FPulsePeriodus);  
}

void CPulse::Start()
{
  FPTimerPulsePeriod->SetPeriodus(FPulsePeriodus);
  FPTimerPulsePeriod->Start();  
}

void CPulse::Stop()
{
  FPTimerPulseWidth->Stop();
  FPTimerPulsePeriod->Stop();
}

void CPulse::SetPulseCount(long unsigned pulsecount)
{ 
  FPulseCount = pulsecount;
}

bool CPulse::DecrementStopPulseCount()
{
  if (0 < FPulseCount)
  {
    FPulseCount--;
  }
  if (0 == FPulseCount)
  {
    Stop();
    return true; 
  }
  return false;
}
//
//-------------------------------------------------------------------
//  Segment - Width
//-------------------------------------------------------------------
//
void CPulse::SetInterruptHandlerWidth(void (*pirqhandlerwidth)(void))
{
  FPIrqHandlerWidth = pirqhandlerwidth;
  FPTimerPulseWidth->SetInterruptHandler(FPIrqHandlerWidth);
}

void CPulse::SetPulseWidthus(long unsigned pulsewidthus)
{
  FPulseWidthus = pulsewidthus;
  FPTimerPulseWidth->SetPeriodus(pulsewidthus);
}

void CPulse::StartWidth()
{
  FPTimerPulseWidth->SetPeriodus(FPulseWidthus);
  FPTimerPulseWidth->SetPulseCount(WIDTH_PULSECOUNT);
  FPTimerPulseWidth->Start(); 
}

void CPulse::StopWidth()
{
  FPTimerPulseWidth->Stop(); 
  
//  TimerPulseWidth.Stop();     
}



