﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Observable
{ // BaseClass
  public class CObservableInt32
  {
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //		
    protected String FUnit;
    protected Int32 FMinimum, FMaximum;
    protected Int32 FTarget, FActual;
    protected Int32 FPreset, FDelta;
    protected Int32 FStep, FResolution, FError;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //		
    public CObservableInt32(String unit)
    {
      Unit = unit;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //		
    public String Unit
    {
      get { return FUnit; }
      set { FUnit = value; }
    }
    public Int32 Minimum
    {
      get { return FMinimum; }
      set { FMinimum = value; }
    }
    public Int32 Maximum
    {
      get { return FMaximum; }
      set { FMaximum = value; }
    }
    public Int32 Target
    {
      get { return FTarget; }
      set { FTarget = value; }
    }
    public Int32 Actual
    {
      get { return FActual; }
      set { FActual = value; }
    }
    public Int32 Preset
    {
      get { return FPreset; }
      set { FPreset = value; }
    }
    public Int32 Delta
    {
      get { return FDelta; }
      set { FDelta = value; }
    }
    public Int32 Step
    {
      get { return FStep; }
      set { FStep = value; }
    }
    public Int32 Resolution
    {
      get { return FResolution; }
      set { FResolution = value; }
    }
    public Int32 Error
    {
      get { return FError; }
      set { FError = value; }
    }


  }
}
