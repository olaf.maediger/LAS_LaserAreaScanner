﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using BitmapImage;
//
namespace BitmapAnalyser
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICETYPE = "BitmapAnalyser";
    private const String INIT_DEVICENAME = "OMBitmapAnalyser";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    Random FRandom;
    CBitmap FBitmap;
    //CBitmapAnalyser FBitmapAnalyser;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FRandom = new Random((Int32)DateTime.Now.Ticks);
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      //FStepCalculation = new CStepCalculation(2);
      //FStepCalculation.SetOnCalculateParameter(0, StepCalculationOnCalculateParameter0);
      //FStepCalculation.SetOnCalculateParameter(1, StepCalculationOnCalculateParameter1);
      //
      nudScaleFactor.Value = 1;
      pbxImage.Dock = DockStyle.None;
      pbxImage.SizeMode = PictureBoxSizeMode.AutoSize;
      cbxZoom.Checked = true;
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //

    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      ////// /////////////////////////?????FUdpTextArrayServer.Close();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    // 

    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //#########################################################################
    //  Segment - Callback -
    //#########################################################################
    //
    private Double StepCalculationOnCalculateParameter0(Double parameter)
    {
      return 1.0 + 1.0 * parameter;
    }
    private Double StepCalculationOnCalculateParameter1(Double parameter)
    {
      return 1.0 + 2.0 * parameter;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          tbcMain.SelectedIndex = 1;
          return true;
        case 1:

          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5: // THIS NOT!!!!!!!!!!!!!!!
          ////////////////////FUCNotifier.Write("*** UdpText - Transmit Vector2DDouble:");
          ////////////////////Int32 L0 = 3;
          ////////////////////Int32 L1 = 2;
          ////////////////////Double[,] Vector2D = new Double[L0, L1];
          ////////////////////for (Int32 I0 = 0; I0 < L0; I0++)
          ////////////////////{
          ////////////////////  for (Int32 I1 = 0; I1 < L1; I1++)
          ////////////////////  {
          ////////////////////    Vector2D[I0, I1] = I0 + 100 + (Double)(0.123456 + I1 / 10.0);
          ////////////////////  }
          ////////////////////}
          ////////////////////for (Int32 I0 = 0; I0 < L0; I0++)
          ////////////////////{
          ////////////////////  String Line = String.Format("Vector[{0}] =", I0); 
          ////////////////////  for (Int32 I1 = 0; I1 < L1; I1++)
          ////////////////////  {
          ////////////////////    Line += String.Format(" {0}:{1:000.000}", I1, Vector2D[I0, I1]); 
          ////////////////////  }
          ////////////////////  FUCNotifier.Write(Line);
          ////////////////////}
          ////////////////////FUdpTextDeviceServer.PresetVector2DDouble(Vector2D);
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }

    private void btnLoadImage_Click(object sender, EventArgs e)
    {
      DialogLoadImage.FileName = "Image.bmp";
      if (DialogResult.OK == DialogLoadImage.ShowDialog())
      {
        String FileName = DialogLoadImage.FileName;
        FBitmap = new CBitmap(FileName);
        Int32 SF = (Int32)nudScaleFactor.Value;
        Bitmap BS = FBitmap.GetBitmap();
        Bitmap BT = new Bitmap(FBitmap.GetBitmap(), SF * FBitmap.GetWidth(), SF * FBitmap.GetHeight());
        pbxImage.Image = BT;
      }
    }

    private void btnSaveImage_Click(object sender, EventArgs e)
    {
      DialogSaveImage.FileName = "ImageNew.bmp";
      if (DialogResult.OK == DialogSaveImage.ShowDialog())
      {
        String FileName = DialogSaveImage.FileName;
        FBitmap.GetBitmap().Save(FileName);
      }      
    }


    private void cbxZoom_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxZoom.Checked)
      {
        pbxImage.Dock = DockStyle.Fill;
        pbxImage.SizeMode = PictureBoxSizeMode.Zoom;
      }
      else
      {
        pbxImage.Dock = DockStyle.None;
        pbxImage.SizeMode = PictureBoxSizeMode.AutoSize;
      }

    }

    private void nudScaleFactor_ValueChanged(object sender, EventArgs e)
    {
      if ((FBitmap is CBitmap) && (FBitmap.GetBitmap() is Bitmap))
      {
        Int32 SF = (Int32)nudScaleFactor.Value;
        Bitmap BS = FBitmap.GetBitmap();
        Bitmap BT = new Bitmap(FBitmap.GetBitmap(), SF * FBitmap.GetWidth(), SF * FBitmap.GetHeight());
        pbxImage.Image = BT;
      }
    }

    private void btnConvertGreyScale_Click(object sender, EventArgs e)
    {
      FBitmap.ConvertGreyScale();
      nudScaleFactor_ValueChanged(this, null);
    }

    private void btnConvertMonoChrome_Click(object sender, EventArgs e)
    {
      Int32 TH = (Int32)nudThreshold.Value;
      FBitmap.ConvertMonoChrome(TH);
      nudScaleFactor_ValueChanged(this, null);
    }

 
  }
}
