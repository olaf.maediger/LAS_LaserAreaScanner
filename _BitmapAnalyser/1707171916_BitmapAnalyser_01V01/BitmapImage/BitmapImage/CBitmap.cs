﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
//
namespace BitmapImage
{
  public class CBitmap
  {
    const Int32 BITS_PER_PIXEL = 32;
    const PixelFormat PIXEL_FORMAT = PixelFormat.Format32bppArgb;
    const Int32 INIT_WIDTH = 200;
    const Int32 INIT_HEIGHT = 200;
    //
    protected System.Drawing.Bitmap FBitmap; 
    //
    public CBitmap(Int32 width, Int32 height)
    {
      FBitmap = new System.Drawing.Bitmap(width, height, PIXEL_FORMAT);
    }

    public CBitmap(String filename)
    {
      if (File.Exists(filename))
      {
        FBitmap = new System.Drawing.Bitmap(filename);
        switch (FBitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            FBitmap = CBitmap.Convert24RgbTo32Argb(FBitmap);
            break;
        }
      }
      else
      {
        FBitmap = new System.Drawing.Bitmap(INIT_WIDTH, INIT_HEIGHT, PIXEL_FORMAT);
      }
    }

    public Bitmap GetBitmap()
    {
      return FBitmap;
    }

    public Int32 GetWidth()
    {
      return FBitmap.Width;
    }
    public Int32 GetHeight()
    {
      return FBitmap.Height;
    }

    protected unsafe Boolean SetBitmapColor(Byte red, Byte green, Byte blue)
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadWrite, PF);
        Int32 VectorSize = BD.Stride * BD.Height;
        Int32 YI = 0; 
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          { // B
            *PBD = blue;
            PBD++;
            // G
            *PBD = green;
            PBD++;
            // R
            *PBD = red;
            PBD++;
            // A
            *PBD = 0xFF;
            PBD++;
            //
            XI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }

    protected unsafe Boolean SetBitmapGrey(Byte grey)
    {
      return SetBitmapColor(grey, grey, grey);
    }

    public unsafe Boolean ConvertGreyScale()
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
        Int32 YI = 0;
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          { 
            Byte CG = (Byte)((*(0 + PBD)) * 0.114f + (*(1 + PBD)) * 0.587f + (*(2 + PBD)) * 0.299f);
            // B
            *PBD = CG;
            PBD++;
            // G
            *PBD = CG;
            PBD++;
            // R
            *PBD = CG;
            PBD++;
            // A
            *PBD = CG;
            PBD++;
            //
            XI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }

    public unsafe Boolean ConvertMonoChrome(Int32 threshold)
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
        Int32 YI = 0;
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          { 
            Byte CG = (Byte)((*(0 + PBD)) * 0.114f + (*(1 + PBD)) * 0.587f + (*(2 + PBD)) * 0.299f);
            if (threshold <= CG)
            {
              CG = 0xFF;
            }
            else
            {
              CG = 0x00;
            }
            // B
            *PBD = CG;
            PBD++;
            // G
            *PBD = CG;
            PBD++;
            // R
            *PBD = CG;
            PBD++;
            // A
            *PBD = CG;
            PBD++;
            //
            XI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }




    public unsafe Byte[] GetVectorDataGrey()
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
        Int32 VectorSize = 1 * BW * BH;
        Byte[] VectorData = new Byte[VectorSize];
        Byte CG = 0;
        Int32 VI = 0;
        Int32 YI = 0; 
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          { // B
            CG = *PBD;
            PBD++;
            // G
            CG += *PBD;
            PBD++;
            // R
            CG += *PBD;
            PBD++;
            // A
            PBD++;
            //
            VectorData[VI] = (Byte)(CG / 3);
            VI++;
            XI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return VectorData;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return null;
      }
    }

    public unsafe Boolean SetVectorDataGrey(Byte[] vectordata)
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
        Int32 VectorSize = vectordata.Length;
        Int32 VI = 0;
        Int32 YI = 0;
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          { // B
            *PBD = vectordata[VI];
            PBD++;
            // G
            *PBD = vectordata[VI];
            PBD++;
            // R
            *PBD = vectordata[VI];
            PBD++;
            // A
            PBD++;
            //
            XI++;
            VI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }

    public unsafe Boolean SetVectorDataRgb24(Byte[] vectordata)
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
        Int32 VectorSize = vectordata.Length;
        Int32 VI = 0;
        Int32 YI = 0;
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          { // B
            *PBD = vectordata[VI];
            PBD++;
            VI++;
            // G
            *PBD = vectordata[VI];
            PBD++;
            VI++;
            // R
            *PBD = vectordata[VI];
            PBD++;
            VI++;
            // A
            *PBD = 0xFF;
            PBD++;
            //
            XI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }


    public static unsafe Bitmap Convert24RgbTo32Argb(Bitmap bitmapsource)
    {
      try
      {
        Int32 BSW = bitmapsource.Width;
        Int32 BSH = bitmapsource.Height;
        PixelFormat PFS = bitmapsource.PixelFormat; // Rgb24
        Rectangle RS = new Rectangle(0, 0, BSW, BSH);
        BitmapData BDS = bitmapsource.LockBits(RS, ImageLockMode.ReadOnly, PFS);
        //
        Bitmap BitmapTarget = new Bitmap(BSW, BSH, PIXEL_FORMAT);
        Int32 BDW = BitmapTarget.Width;
        Int32 BDH = BitmapTarget.Height;
        PixelFormat PFD = BitmapTarget.PixelFormat; // Argb32
        Rectangle RD = new Rectangle(0, 0, BDW, BDH);
        BitmapData BDD = BitmapTarget.LockBits(RD, ImageLockMode.WriteOnly, PFD);
        //
        Int32 YI = 0;
        while (YI < BSH)
        {
          Byte* PBS = (Byte*)(BDS.Scan0 + YI * BDS.Stride);
          Byte* PBD = (Byte*)(BDD.Scan0 + YI * BDD.Stride);
          //
          Int32 XI = 0;
          while (XI < BSW)
          { // B
            *PBD = *PBS;
            PBS++;
            PBD++;
            // G
            *PBD = *PBS;
            PBS++;
            PBD++;
            // R
            *PBD = *PBS;
            PBS++;
            PBD++;
            // A
            *PBD = 0xFF;
            // NC PBS++;
            PBD++;
            //
            XI++;
          }
          YI++;
        }
        bitmapsource.UnlockBits(BDS);
        BitmapTarget.UnlockBits(BDD);
        //
        return BitmapTarget;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }




  }
}
