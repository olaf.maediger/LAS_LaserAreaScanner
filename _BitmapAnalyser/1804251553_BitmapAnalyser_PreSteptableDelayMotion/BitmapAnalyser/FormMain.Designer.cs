﻿namespace BitmapAnalyser
{
  partial class FormMain
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.DialogLoadInitdata = new System.Windows.Forms.OpenFileDialog();
      this.FHelpProvider = new System.Windows.Forms.HelpProvider();
      this.DialogSaveInitdata = new System.Windows.Forms.SaveFileDialog();
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.mitSystem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.mitConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCDefault = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCResetToDefaultConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveAutomaticAtProgramEnd = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCStartup = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitElevator = new System.Windows.Forms.ToolStripMenuItem();
      this.mitStartSimulation = new System.Windows.Forms.ToolStripMenuItem();
      this.mitProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPEditParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPClearProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
      this.diagnosticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPLoadDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPSaveDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSendDiagnosticEmail = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHelp = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHContents = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHTopic = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHSearch = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterApplicationProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterDeviceProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.tmrStartup = new System.Windows.Forms.Timer(this.components);
      this.pnlProtocol = new System.Windows.Forms.Panel();
      this.FUCSerialNumber = new UCSerialNumber.CUCSerialNumber();
      this.splProtocol = new System.Windows.Forms.Splitter();
      this.tbcMain = new System.Windows.Forms.TabControl();
      this.tbpMain = new System.Windows.Forms.TabPage();
      this.pnlImage = new System.Windows.Forms.Panel();
      this.pbxImage = new System.Windows.Forms.PictureBox();
      this.pnlLeft = new System.Windows.Forms.Panel();
      this.rbtOriginalColor = new System.Windows.Forms.RadioButton();
      this.rbtConvertMonochrome = new System.Windows.Forms.RadioButton();
      this.rbtConvertGreyscale = new System.Windows.Forms.RadioButton();
      this.label10 = new System.Windows.Forms.Label();
      this.nudYMaximum = new System.Windows.Forms.NumericUpDown();
      this.label11 = new System.Windows.Forms.Label();
      this.nudYMinimum = new System.Windows.Forms.NumericUpDown();
      this.label8 = new System.Windows.Forms.Label();
      this.nudXMaximum = new System.Windows.Forms.NumericUpDown();
      this.label9 = new System.Windows.Forms.Label();
      this.nudXMinimum = new System.Windows.Forms.NumericUpDown();
      this.label7 = new System.Windows.Forms.Label();
      this.nudPulsePeriodms = new System.Windows.Forms.NumericUpDown();
      this.nudPulsesMaximum = new System.Windows.Forms.NumericUpDown();
      this.nudPulsesMinimum = new System.Windows.Forms.NumericUpDown();
      this.btnSaveSteptable = new System.Windows.Forms.Button();
      this.lblColorLow = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.lblColorHigh = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.hsbThreshold = new System.Windows.Forms.HScrollBar();
      this.label2 = new System.Windows.Forms.Label();
      this.nudThreshold = new System.Windows.Forms.NumericUpDown();
      this.btnSaveImage = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.nudScaleFactor = new System.Windows.Forms.NumericUpDown();
      this.cbxZoom = new System.Windows.Forms.CheckBox();
      this.btnLoadImage = new System.Windows.Forms.Button();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.cbxAutomate = new System.Windows.Forms.CheckBox();
      this.DialogLoadImage = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveImage = new System.Windows.Forms.SaveFileDialog();
      this.DialogColorPixel = new System.Windows.Forms.ColorDialog();
      this.DialogSaveSteptable = new System.Windows.Forms.SaveFileDialog();
      this.mstMain.SuspendLayout();
      this.pnlProtocol.SuspendLayout();
      this.tbcMain.SuspendLayout();
      this.tbpMain.SuspendLayout();
      this.pnlImage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).BeginInit();
      this.pnlLeft.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudYMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudYMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudXMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudXMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriodms)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsesMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsesMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudThreshold)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleFactor)).BeginInit();
      this.SuspendLayout();
      // 
      // DialogLoadInitdata
      // 
      this.DialogLoadInitdata.DefaultExt = "ini.xml";
      this.DialogLoadInitdata.FileName = "Initdata";
      this.DialogLoadInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogLoadInitdata.Title = "MDesign: Load Initdata";
      // 
      // DialogSaveInitdata
      // 
      this.DialogSaveInitdata.DefaultExt = "ini.xml";
      this.DialogSaveInitdata.FileName = "Initdata";
      this.DialogSaveInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogSaveInitdata.Title = "MDesign: Save Initdata";
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSystem,
            this.mitConfiguration,
            this.mitElevator,
            this.mitProtocol,
            this.mitHelp});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(749, 24);
      this.mstMain.TabIndex = 137;
      this.mstMain.Text = "System";
      // 
      // mitSystem
      // 
      this.mitSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSQuit});
      this.mitSystem.Name = "mitSystem";
      this.mitSystem.Size = new System.Drawing.Size(57, 20);
      this.mitSystem.Text = "&System";
      // 
      // mitSQuit
      // 
      this.mitSQuit.Name = "mitSQuit";
      this.mitSQuit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
      this.mitSQuit.Size = new System.Drawing.Size(140, 22);
      this.mitSQuit.Text = "&Quit";
      // 
      // mitConfiguration
      // 
      this.mitConfiguration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitCDefault,
            this.mitCResetToDefaultConfiguration,
            this.mitCSaveAutomaticAtProgramEnd,
            this.mitCStartup,
            this.mitCLoadStartupConfiguration,
            this.mitCSaveStartupConfiguration,
            this.mitCShowEditStartupConfiguration,
            this.toolStripMenuItem7,
            this.mitCLoadSelectableConfiguration,
            this.mitCSaveSelectableConfiguration,
            this.mitCShowEditSelectableConfiguration});
      this.mitConfiguration.Name = "mitConfiguration";
      this.mitConfiguration.Size = new System.Drawing.Size(93, 20);
      this.mitConfiguration.Text = "&Configuration";
      // 
      // mitCDefault
      // 
      this.mitCDefault.Enabled = false;
      this.mitCDefault.Name = "mitCDefault";
      this.mitCDefault.Size = new System.Drawing.Size(300, 22);
      this.mitCDefault.Text = "- Default -";
      // 
      // mitCResetToDefaultConfiguration
      // 
      this.mitCResetToDefaultConfiguration.Name = "mitCResetToDefaultConfiguration";
      this.mitCResetToDefaultConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
      this.mitCResetToDefaultConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCResetToDefaultConfiguration.Text = "Reset to Default-Configuration";
      // 
      // mitCSaveAutomaticAtProgramEnd
      // 
      this.mitCSaveAutomaticAtProgramEnd.Name = "mitCSaveAutomaticAtProgramEnd";
      this.mitCSaveAutomaticAtProgramEnd.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveAutomaticAtProgramEnd.Text = "Save automatic at program end";
      // 
      // mitCStartup
      // 
      this.mitCStartup.Enabled = false;
      this.mitCStartup.Name = "mitCStartup";
      this.mitCStartup.Size = new System.Drawing.Size(300, 22);
      this.mitCStartup.Text = "- Startup -";
      // 
      // mitCLoadStartupConfiguration
      // 
      this.mitCLoadStartupConfiguration.Name = "mitCLoadStartupConfiguration";
      this.mitCLoadStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
      this.mitCLoadStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadStartupConfiguration.Text = "Load Startup-Configuration";
      // 
      // mitCSaveStartupConfiguration
      // 
      this.mitCSaveStartupConfiguration.Name = "mitCSaveStartupConfiguration";
      this.mitCSaveStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
      this.mitCSaveStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveStartupConfiguration.Text = "Save Startup-Configuration";
      // 
      // mitCShowEditStartupConfiguration
      // 
      this.mitCShowEditStartupConfiguration.Name = "mitCShowEditStartupConfiguration";
      this.mitCShowEditStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditStartupConfiguration.Text = "Show / Edit Startup-Configuration";
      // 
      // toolStripMenuItem7
      // 
      this.toolStripMenuItem7.Enabled = false;
      this.toolStripMenuItem7.Name = "toolStripMenuItem7";
      this.toolStripMenuItem7.Size = new System.Drawing.Size(300, 22);
      this.toolStripMenuItem7.Text = "- Named -";
      // 
      // mitCLoadSelectableConfiguration
      // 
      this.mitCLoadSelectableConfiguration.Name = "mitCLoadSelectableConfiguration";
      this.mitCLoadSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadSelectableConfiguration.Text = "Load Selectable-Configuration ";
      // 
      // mitCSaveSelectableConfiguration
      // 
      this.mitCSaveSelectableConfiguration.Name = "mitCSaveSelectableConfiguration";
      this.mitCSaveSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveSelectableConfiguration.Text = "Save Selectable-Configuration";
      // 
      // mitCShowEditSelectableConfiguration
      // 
      this.mitCShowEditSelectableConfiguration.Name = "mitCShowEditSelectableConfiguration";
      this.mitCShowEditSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditSelectableConfiguration.Text = "Show / Edit Selectable-Configuration";
      // 
      // mitElevator
      // 
      this.mitElevator.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitStartSimulation});
      this.mitElevator.Name = "mitElevator";
      this.mitElevator.Size = new System.Drawing.Size(106, 20);
      this.mitElevator.Text = "Communication";
      // 
      // mitStartSimulation
      // 
      this.mitStartSimulation.Name = "mitStartSimulation";
      this.mitStartSimulation.Size = new System.Drawing.Size(98, 22);
      this.mitStartSimulation.Text = "Start";
      // 
      // mitProtocol
      // 
      this.mitProtocol.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitPEditParameter,
            this.mitPClearProtocol,
            this.mitPCopyToClipboard,
            this.diagnosticToolStripMenuItem,
            this.mitPLoadDiagnosticFile,
            this.mitPSaveDiagnosticFile,
            this.mitSendDiagnosticEmail});
      this.mitProtocol.Name = "mitProtocol";
      this.mitProtocol.Size = new System.Drawing.Size(64, 20);
      this.mitProtocol.Text = "&Protocol";
      // 
      // mitPEditParameter
      // 
      this.mitPEditParameter.Name = "mitPEditParameter";
      this.mitPEditParameter.Size = new System.Drawing.Size(171, 22);
      this.mitPEditParameter.Text = "Edit Parameter";
      // 
      // mitPClearProtocol
      // 
      this.mitPClearProtocol.Name = "mitPClearProtocol";
      this.mitPClearProtocol.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
      this.mitPClearProtocol.Size = new System.Drawing.Size(171, 22);
      this.mitPClearProtocol.Text = "Clear";
      // 
      // mitPCopyToClipboard
      // 
      this.mitPCopyToClipboard.Name = "mitPCopyToClipboard";
      this.mitPCopyToClipboard.Size = new System.Drawing.Size(171, 22);
      this.mitPCopyToClipboard.Text = "Copy to Clipboard";
      // 
      // diagnosticToolStripMenuItem
      // 
      this.diagnosticToolStripMenuItem.Enabled = false;
      this.diagnosticToolStripMenuItem.Name = "diagnosticToolStripMenuItem";
      this.diagnosticToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
      this.diagnosticToolStripMenuItem.Text = "- Diagnostic -";
      // 
      // mitPLoadDiagnosticFile
      // 
      this.mitPLoadDiagnosticFile.Name = "mitPLoadDiagnosticFile";
      this.mitPLoadDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPLoadDiagnosticFile.Text = "Load File";
      // 
      // mitPSaveDiagnosticFile
      // 
      this.mitPSaveDiagnosticFile.Name = "mitPSaveDiagnosticFile";
      this.mitPSaveDiagnosticFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
      this.mitPSaveDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPSaveDiagnosticFile.Text = "Save File";
      // 
      // mitSendDiagnosticEmail
      // 
      this.mitSendDiagnosticEmail.Name = "mitSendDiagnosticEmail";
      this.mitSendDiagnosticEmail.Size = new System.Drawing.Size(171, 22);
      this.mitSendDiagnosticEmail.Text = "Send Email";
      // 
      // mitHelp
      // 
      this.mitHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitHContents,
            this.mitHTopic,
            this.mitHSearch,
            this.mitHEnterApplicationProductKey,
            this.mitHEnterDeviceProductKey,
            this.mitHAbout});
      this.mitHelp.Name = "mitHelp";
      this.mitHelp.Size = new System.Drawing.Size(44, 20);
      this.mitHelp.Text = "Help";
      // 
      // mitHContents
      // 
      this.mitHContents.Name = "mitHContents";
      this.mitHContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.mitHContents.Size = new System.Drawing.Size(234, 22);
      this.mitHContents.Text = "Contents";
      // 
      // mitHTopic
      // 
      this.mitHTopic.Name = "mitHTopic";
      this.mitHTopic.Size = new System.Drawing.Size(234, 22);
      this.mitHTopic.Text = "Topic";
      // 
      // mitHSearch
      // 
      this.mitHSearch.Name = "mitHSearch";
      this.mitHSearch.Size = new System.Drawing.Size(234, 22);
      this.mitHSearch.Text = "Search";
      // 
      // mitHEnterApplicationProductKey
      // 
      this.mitHEnterApplicationProductKey.Name = "mitHEnterApplicationProductKey";
      this.mitHEnterApplicationProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterApplicationProductKey.Text = "Enter Application Product-Key";
      // 
      // mitHEnterDeviceProductKey
      // 
      this.mitHEnterDeviceProductKey.Name = "mitHEnterDeviceProductKey";
      this.mitHEnterDeviceProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterDeviceProductKey.Text = "Enter Device Product-Key";
      // 
      // mitHAbout
      // 
      this.mitHAbout.Name = "mitHAbout";
      this.mitHAbout.ShortcutKeys = System.Windows.Forms.Keys.F2;
      this.mitHAbout.Size = new System.Drawing.Size(234, 22);
      this.mitHAbout.Text = "About";
      // 
      // tmrStartup
      // 
      this.tmrStartup.Interval = 1;
      // 
      // pnlProtocol
      // 
      this.pnlProtocol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlProtocol.Controls.Add(this.FUCSerialNumber);
      this.pnlProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlProtocol.Location = new System.Drawing.Point(0, 595);
      this.pnlProtocol.Name = "pnlProtocol";
      this.pnlProtocol.Size = new System.Drawing.Size(749, 143);
      this.pnlProtocol.TabIndex = 138;
      // 
      // FUCSerialNumber
      // 
      this.FUCSerialNumber.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCSerialNumber.Location = new System.Drawing.Point(0, 0);
      this.FUCSerialNumber.Name = "FUCSerialNumber";
      this.FUCSerialNumber.Size = new System.Drawing.Size(747, 64);
      this.FUCSerialNumber.TabIndex = 113;
      this.FUCSerialNumber.Visible = false;
      // 
      // splProtocol
      // 
      this.splProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splProtocol.Location = new System.Drawing.Point(0, 592);
      this.splProtocol.Name = "splProtocol";
      this.splProtocol.Size = new System.Drawing.Size(749, 3);
      this.splProtocol.TabIndex = 139;
      this.splProtocol.TabStop = false;
      // 
      // tbcMain
      // 
      this.tbcMain.Alignment = System.Windows.Forms.TabAlignment.Bottom;
      this.tbcMain.Controls.Add(this.tbpMain);
      this.tbcMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcMain.Location = new System.Drawing.Point(0, 24);
      this.tbcMain.Margin = new System.Windows.Forms.Padding(0);
      this.tbcMain.Name = "tbcMain";
      this.tbcMain.Padding = new System.Drawing.Point(0, 0);
      this.tbcMain.SelectedIndex = 0;
      this.tbcMain.Size = new System.Drawing.Size(749, 568);
      this.tbcMain.TabIndex = 140;
      // 
      // tbpMain
      // 
      this.tbpMain.BackColor = System.Drawing.Color.Silver;
      this.tbpMain.Controls.Add(this.pnlImage);
      this.tbpMain.Controls.Add(this.pnlLeft);
      this.tbpMain.Location = new System.Drawing.Point(4, 4);
      this.tbpMain.Name = "tbpMain";
      this.tbpMain.Size = new System.Drawing.Size(741, 542);
      this.tbpMain.TabIndex = 6;
      this.tbpMain.Text = "Main";
      // 
      // pnlImage
      // 
      this.pnlImage.AutoScroll = true;
      this.pnlImage.BackColor = System.Drawing.Color.DarkRed;
      this.pnlImage.Controls.Add(this.pbxImage);
      this.pnlImage.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlImage.Location = new System.Drawing.Point(147, 0);
      this.pnlImage.Name = "pnlImage";
      this.pnlImage.Size = new System.Drawing.Size(594, 542);
      this.pnlImage.TabIndex = 2;
      // 
      // pbxImage
      // 
      this.pbxImage.BackColor = System.Drawing.Color.Silver;
      this.pbxImage.Location = new System.Drawing.Point(0, 0);
      this.pbxImage.Name = "pbxImage";
      this.pbxImage.Size = new System.Drawing.Size(554, 284);
      this.pbxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pbxImage.TabIndex = 5;
      this.pbxImage.TabStop = false;
      // 
      // pnlLeft
      // 
      this.pnlLeft.BackColor = System.Drawing.Color.Gainsboro;
      this.pnlLeft.Controls.Add(this.rbtOriginalColor);
      this.pnlLeft.Controls.Add(this.rbtConvertMonochrome);
      this.pnlLeft.Controls.Add(this.rbtConvertGreyscale);
      this.pnlLeft.Controls.Add(this.label10);
      this.pnlLeft.Controls.Add(this.nudYMaximum);
      this.pnlLeft.Controls.Add(this.label11);
      this.pnlLeft.Controls.Add(this.nudYMinimum);
      this.pnlLeft.Controls.Add(this.label8);
      this.pnlLeft.Controls.Add(this.nudXMaximum);
      this.pnlLeft.Controls.Add(this.label9);
      this.pnlLeft.Controls.Add(this.nudXMinimum);
      this.pnlLeft.Controls.Add(this.label7);
      this.pnlLeft.Controls.Add(this.nudPulsePeriodms);
      this.pnlLeft.Controls.Add(this.nudPulsesMaximum);
      this.pnlLeft.Controls.Add(this.nudPulsesMinimum);
      this.pnlLeft.Controls.Add(this.btnSaveSteptable);
      this.pnlLeft.Controls.Add(this.lblColorLow);
      this.pnlLeft.Controls.Add(this.label6);
      this.pnlLeft.Controls.Add(this.lblColorHigh);
      this.pnlLeft.Controls.Add(this.label3);
      this.pnlLeft.Controls.Add(this.hsbThreshold);
      this.pnlLeft.Controls.Add(this.label2);
      this.pnlLeft.Controls.Add(this.nudThreshold);
      this.pnlLeft.Controls.Add(this.btnSaveImage);
      this.pnlLeft.Controls.Add(this.label1);
      this.pnlLeft.Controls.Add(this.nudScaleFactor);
      this.pnlLeft.Controls.Add(this.cbxZoom);
      this.pnlLeft.Controls.Add(this.btnLoadImage);
      this.pnlLeft.Controls.Add(this.label5);
      this.pnlLeft.Controls.Add(this.label4);
      this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlLeft.Location = new System.Drawing.Point(0, 0);
      this.pnlLeft.Name = "pnlLeft";
      this.pnlLeft.Size = new System.Drawing.Size(147, 542);
      this.pnlLeft.TabIndex = 1;
      // 
      // rbtOriginalColor
      // 
      this.rbtOriginalColor.AutoSize = true;
      this.rbtOriginalColor.Location = new System.Drawing.Point(11, 76);
      this.rbtOriginalColor.Name = "rbtOriginalColor";
      this.rbtOriginalColor.Size = new System.Drawing.Size(84, 17);
      this.rbtOriginalColor.TabIndex = 36;
      this.rbtOriginalColor.TabStop = true;
      this.rbtOriginalColor.Text = "OriginalColor";
      this.rbtOriginalColor.UseVisualStyleBackColor = true;
      this.rbtOriginalColor.CheckedChanged += new System.EventHandler(this.rbtOriginal_CheckedChanged);
      // 
      // rbtConvertMonochrome
      // 
      this.rbtConvertMonochrome.AutoSize = true;
      this.rbtConvertMonochrome.Location = new System.Drawing.Point(11, 122);
      this.rbtConvertMonochrome.Name = "rbtConvertMonochrome";
      this.rbtConvertMonochrome.Size = new System.Drawing.Size(127, 17);
      this.rbtConvertMonochrome.TabIndex = 35;
      this.rbtConvertMonochrome.TabStop = true;
      this.rbtConvertMonochrome.Text = "Convert Monochrome";
      this.rbtConvertMonochrome.UseVisualStyleBackColor = true;
      this.rbtConvertMonochrome.CheckedChanged += new System.EventHandler(this.rbtConvertMonochrome_CheckedChanged);
      // 
      // rbtConvertGreyscale
      // 
      this.rbtConvertGreyscale.AutoSize = true;
      this.rbtConvertGreyscale.Location = new System.Drawing.Point(11, 99);
      this.rbtConvertGreyscale.Name = "rbtConvertGreyscale";
      this.rbtConvertGreyscale.Size = new System.Drawing.Size(112, 17);
      this.rbtConvertGreyscale.TabIndex = 34;
      this.rbtConvertGreyscale.TabStop = true;
      this.rbtConvertGreyscale.Text = "Convert Greyscale";
      this.rbtConvertGreyscale.UseVisualStyleBackColor = true;
      this.rbtConvertGreyscale.CheckedChanged += new System.EventHandler(this.rbtConvertGreyscale_CheckedChanged);
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(9, 324);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(58, 13);
      this.label10.TabIndex = 33;
      this.label10.Text = "YMaximum";
      // 
      // nudYMaximum
      // 
      this.nudYMaximum.Location = new System.Drawing.Point(88, 321);
      this.nudYMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudYMaximum.Name = "nudYMaximum";
      this.nudYMaximum.Size = new System.Drawing.Size(49, 20);
      this.nudYMaximum.TabIndex = 32;
      this.nudYMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudYMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(9, 302);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(55, 13);
      this.label11.TabIndex = 31;
      this.label11.Text = "YMinimum";
      // 
      // nudYMinimum
      // 
      this.nudYMinimum.Location = new System.Drawing.Point(88, 299);
      this.nudYMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudYMinimum.Name = "nudYMinimum";
      this.nudYMinimum.Size = new System.Drawing.Size(49, 20);
      this.nudYMinimum.TabIndex = 30;
      this.nudYMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudYMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(9, 277);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(58, 13);
      this.label8.TabIndex = 29;
      this.label8.Text = "XMaximum";
      // 
      // nudXMaximum
      // 
      this.nudXMaximum.Location = new System.Drawing.Point(88, 274);
      this.nudXMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudXMaximum.Name = "nudXMaximum";
      this.nudXMaximum.Size = new System.Drawing.Size(49, 20);
      this.nudXMaximum.TabIndex = 28;
      this.nudXMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudXMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(9, 255);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(55, 13);
      this.label9.TabIndex = 27;
      this.label9.Text = "XMinimum";
      // 
      // nudXMinimum
      // 
      this.nudXMinimum.Location = new System.Drawing.Point(88, 252);
      this.nudXMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudXMinimum.Name = "nudXMinimum";
      this.nudXMinimum.Size = new System.Drawing.Size(49, 20);
      this.nudXMinimum.TabIndex = 26;
      this.nudXMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudXMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(9, 349);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(59, 13);
      this.label7.TabIndex = 25;
      this.label7.Text = "Period [ms]";
      // 
      // nudPulsePeriodms
      // 
      this.nudPulsePeriodms.Location = new System.Drawing.Point(88, 346);
      this.nudPulsePeriodms.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudPulsePeriodms.Name = "nudPulsePeriodms";
      this.nudPulsePeriodms.Size = new System.Drawing.Size(49, 20);
      this.nudPulsePeriodms.TabIndex = 24;
      this.nudPulsePeriodms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulsePeriodms.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // nudPulsesMaximum
      // 
      this.nudPulsesMaximum.Location = new System.Drawing.Point(88, 393);
      this.nudPulsesMaximum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudPulsesMaximum.Name = "nudPulsesMaximum";
      this.nudPulsesMaximum.Size = new System.Drawing.Size(49, 20);
      this.nudPulsesMaximum.TabIndex = 22;
      this.nudPulsesMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulsesMaximum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // nudPulsesMinimum
      // 
      this.nudPulsesMinimum.Location = new System.Drawing.Point(88, 371);
      this.nudPulsesMinimum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudPulsesMinimum.Name = "nudPulsesMinimum";
      this.nudPulsesMinimum.Size = new System.Drawing.Size(49, 20);
      this.nudPulsesMinimum.TabIndex = 20;
      this.nudPulsesMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnSaveSteptable
      // 
      this.btnSaveSteptable.Location = new System.Drawing.Point(7, 415);
      this.btnSaveSteptable.Name = "btnSaveSteptable";
      this.btnSaveSteptable.Size = new System.Drawing.Size(132, 23);
      this.btnSaveSteptable.TabIndex = 19;
      this.btnSaveSteptable.Text = "Save Steptable";
      this.btnSaveSteptable.UseVisualStyleBackColor = true;
      this.btnSaveSteptable.Click += new System.EventHandler(this.btnSaveSteptable_Click);
      // 
      // lblColorLow
      // 
      this.lblColorLow.BackColor = System.Drawing.Color.Black;
      this.lblColorLow.Location = new System.Drawing.Point(55, 152);
      this.lblColorLow.Name = "lblColorLow";
      this.lblColorLow.Size = new System.Drawing.Size(15, 13);
      this.lblColorLow.TabIndex = 18;
      this.lblColorLow.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblColorLow_MouseClick);
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(6, 152);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(51, 13);
      this.label6.TabIndex = 17;
      this.label6.Text = "ColorLow";
      // 
      // lblColorHigh
      // 
      this.lblColorHigh.BackColor = System.Drawing.Color.White;
      this.lblColorHigh.Location = new System.Drawing.Point(123, 152);
      this.lblColorHigh.Name = "lblColorHigh";
      this.lblColorHigh.Size = new System.Drawing.Size(15, 13);
      this.lblColorHigh.TabIndex = 16;
      this.lblColorHigh.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblColorHigh_MouseClick);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(72, 152);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(53, 13);
      this.label3.TabIndex = 15;
      this.label3.Text = "ColorHigh";
      // 
      // hsbThreshold
      // 
      this.hsbThreshold.Location = new System.Drawing.Point(7, 170);
      this.hsbThreshold.Maximum = 256;
      this.hsbThreshold.Name = "hsbThreshold";
      this.hsbThreshold.Size = new System.Drawing.Size(132, 17);
      this.hsbThreshold.TabIndex = 13;
      this.hsbThreshold.Value = 128;
      this.hsbThreshold.ValueChanged += new System.EventHandler(this.hsbThreshold_ValueChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(9, 194);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(54, 13);
      this.label2.TabIndex = 10;
      this.label2.Text = "Threshold";
      // 
      // nudThreshold
      // 
      this.nudThreshold.Location = new System.Drawing.Point(76, 191);
      this.nudThreshold.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
      this.nudThreshold.Name = "nudThreshold";
      this.nudThreshold.Size = new System.Drawing.Size(49, 20);
      this.nudThreshold.TabIndex = 9;
      this.nudThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudThreshold.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
      this.nudThreshold.ValueChanged += new System.EventHandler(this.nudThreshold_ValueChanged);
      // 
      // btnSaveImage
      // 
      this.btnSaveImage.Location = new System.Drawing.Point(8, 219);
      this.btnSaveImage.Name = "btnSaveImage";
      this.btnSaveImage.Size = new System.Drawing.Size(132, 23);
      this.btnSaveImage.TabIndex = 7;
      this.btnSaveImage.Text = "Save Image";
      this.btnSaveImage.UseVisualStyleBackColor = true;
      this.btnSaveImage.Click += new System.EventHandler(this.btnSaveImage_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(9, 54);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(64, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "ScaleFactor";
      // 
      // nudScaleFactor
      // 
      this.nudScaleFactor.Location = new System.Drawing.Point(76, 51);
      this.nudScaleFactor.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudScaleFactor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudScaleFactor.Name = "nudScaleFactor";
      this.nudScaleFactor.Size = new System.Drawing.Size(49, 20);
      this.nudScaleFactor.TabIndex = 1;
      this.nudScaleFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudScaleFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudScaleFactor.ValueChanged += new System.EventHandler(this.nudScaleFactor_ValueChanged);
      // 
      // cbxZoom
      // 
      this.cbxZoom.AutoSize = true;
      this.cbxZoom.Location = new System.Drawing.Point(12, 32);
      this.cbxZoom.Name = "cbxZoom";
      this.cbxZoom.Size = new System.Drawing.Size(110, 17);
      this.cbxZoom.TabIndex = 2;
      this.cbxZoom.Text = "ZoomFit / Original";
      this.cbxZoom.UseVisualStyleBackColor = true;
      this.cbxZoom.CheckedChanged += new System.EventHandler(this.cbxZoom_CheckedChanged);
      // 
      // btnLoadImage
      // 
      this.btnLoadImage.Location = new System.Drawing.Point(7, 7);
      this.btnLoadImage.Name = "btnLoadImage";
      this.btnLoadImage.Size = new System.Drawing.Size(132, 23);
      this.btnLoadImage.TabIndex = 1;
      this.btnLoadImage.Text = "Load Image";
      this.btnLoadImage.UseVisualStyleBackColor = true;
      this.btnLoadImage.Click += new System.EventHandler(this.btnLoadImage_Click);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(9, 396);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(82, 13);
      this.label5.TabIndex = 23;
      this.label5.Text = "PulsesMaximum";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(9, 374);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(79, 13);
      this.label4.TabIndex = 21;
      this.label4.Text = "PulsesMinimum";
      // 
      // cbxAutomate
      // 
      this.cbxAutomate.AutoSize = true;
      this.cbxAutomate.Location = new System.Drawing.Point(390, 4);
      this.cbxAutomate.Name = "cbxAutomate";
      this.cbxAutomate.Size = new System.Drawing.Size(71, 17);
      this.cbxAutomate.TabIndex = 141;
      this.cbxAutomate.Text = "Automate";
      this.cbxAutomate.UseVisualStyleBackColor = true;
      // 
      // DialogLoadImage
      // 
      this.DialogLoadImage.DefaultExt = "bmp";
      this.DialogLoadImage.Filter = "Image Files|*.bmp;*.jpg;*.tif;*.png;*.gif|All files (*.*)|*.*";
      this.DialogLoadImage.Title = "Load Image";
      // 
      // DialogSaveImage
      // 
      this.DialogSaveImage.DefaultExt = "bmp";
      this.DialogSaveImage.FileName = "Image";
      this.DialogSaveImage.Filter = "Bmp-File|*.bmp|Jpg-File|*.jpg|Tif-File|*.tif|Png-File|*.png|Gif-File|*.gif";
      this.DialogSaveImage.Title = "Save Image";
      // 
      // DialogSaveSteptable
      // 
      this.DialogSaveSteptable.DefaultExt = "stp";
      this.DialogSaveSteptable.FileName = "Image";
      this.DialogSaveSteptable.Filter = "Stp-File|*.stp|Txt-File|*.txt";
      this.DialogSaveSteptable.Title = "Save Steptable";
      // 
      // FormMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(749, 738);
      this.Controls.Add(this.cbxAutomate);
      this.Controls.Add(this.tbcMain);
      this.Controls.Add(this.splProtocol);
      this.Controls.Add(this.pnlProtocol);
      this.Controls.Add(this.mstMain);
      this.Name = "FormMain";
      this.Text = "BitmapAnalyser";
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.pnlProtocol.ResumeLayout(false);
      this.tbcMain.ResumeLayout(false);
      this.tbpMain.ResumeLayout(false);
      this.pnlImage.ResumeLayout(false);
      this.pnlImage.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).EndInit();
      this.pnlLeft.ResumeLayout(false);
      this.pnlLeft.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudYMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudYMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudXMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudXMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriodms)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsesMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsesMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudThreshold)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleFactor)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.OpenFileDialog DialogLoadInitdata;
    private System.Windows.Forms.HelpProvider FHelpProvider;
    private System.Windows.Forms.SaveFileDialog DialogSaveInitdata;
    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem mitSystem;
    private System.Windows.Forms.ToolStripMenuItem mitSQuit;
    private System.Windows.Forms.ToolStripMenuItem mitConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCDefault;
    private System.Windows.Forms.ToolStripMenuItem mitCResetToDefaultConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveAutomaticAtProgramEnd;
    private System.Windows.Forms.ToolStripMenuItem mitCStartup;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitElevator;
    private System.Windows.Forms.ToolStripMenuItem mitStartSimulation;
    private System.Windows.Forms.ToolStripMenuItem mitProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPEditParameter;
    private System.Windows.Forms.ToolStripMenuItem mitPClearProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPCopyToClipboard;
    private System.Windows.Forms.ToolStripMenuItem diagnosticToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitPLoadDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitPSaveDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitSendDiagnosticEmail;
    private System.Windows.Forms.ToolStripMenuItem mitHelp;
    private System.Windows.Forms.ToolStripMenuItem mitHContents;
    private System.Windows.Forms.ToolStripMenuItem mitHTopic;
    private System.Windows.Forms.ToolStripMenuItem mitHSearch;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterApplicationProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterDeviceProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHAbout;
    private System.Windows.Forms.Timer tmrStartup;
    private System.Windows.Forms.Panel pnlProtocol;
    private UCSerialNumber.CUCSerialNumber FUCSerialNumber;
    private System.Windows.Forms.Splitter splProtocol;
    private System.Windows.Forms.TabControl tbcMain;
    private System.Windows.Forms.TabPage tbpMain;
    private System.Windows.Forms.CheckBox cbxAutomate;
    private System.Windows.Forms.OpenFileDialog DialogLoadImage;
    private System.Windows.Forms.Panel pnlLeft;
    private System.Windows.Forms.Button btnLoadImage;
    private System.Windows.Forms.Panel pnlImage;
    private System.Windows.Forms.PictureBox pbxImage;
    private System.Windows.Forms.CheckBox cbxZoom;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudScaleFactor;
    private System.Windows.Forms.Button btnSaveImage;
    private System.Windows.Forms.SaveFileDialog DialogSaveImage;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown nudThreshold;
    private System.Windows.Forms.HScrollBar hsbThreshold;
    private System.Windows.Forms.Label lblColorLow;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label lblColorHigh;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.ColorDialog DialogColorPixel;
    private System.Windows.Forms.Button btnSaveSteptable;
    private System.Windows.Forms.SaveFileDialog DialogSaveSteptable;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.NumericUpDown nudYMaximum;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.NumericUpDown nudYMinimum;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.NumericUpDown nudXMaximum;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.NumericUpDown nudXMinimum;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.NumericUpDown nudPulsePeriodms;
    private System.Windows.Forms.NumericUpDown nudPulsesMaximum;
    private System.Windows.Forms.NumericUpDown nudPulsesMinimum;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.RadioButton rbtConvertMonochrome;
    private System.Windows.Forms.RadioButton rbtConvertGreyscale;
    private System.Windows.Forms.RadioButton rbtOriginalColor;
  }
}

