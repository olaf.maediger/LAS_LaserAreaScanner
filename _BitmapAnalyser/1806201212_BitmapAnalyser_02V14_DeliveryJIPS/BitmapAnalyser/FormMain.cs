﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using BitmapImage;
//
namespace BitmapAnalyser
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICETYPE = "BitmapAnalyser";
    private const String INIT_DEVICENAME = "OMBitmapAnalyser";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    private const String NAME_STARTIMAGE = "StartImage";
    private const String INIT_STARTIMAGE = "Vegetables.bmp";
    private const String NAME_ZOOMFIT = "ZoomFit";
    private const Boolean INIT_ZOOMFIT = false;
    private const String NAME_SCALEFACTOR = "ScaleFactor";
    private const UInt16 INIT_SCALEFACTOR = 1;
    private const String NAME_COLORLOW = "ColorLow";
    private Color INIT_COLORLOW = Color.Black;
    private const String NAME_COLORHIGH = "ColorHigh";
    private Color INIT_COLORHIGH = Color.White;
    private const String NAME_THRESHOLD = "Threshold";
    private const UInt16 INIT_THRESHOLD = 128;
    private const String NAME_XMINIMUM = "XMinimum";
    private const UInt16 INIT_XMINIMUM = 1800;
    private const String NAME_XMAXIMUM = "XMaximum";
    private const UInt16 INIT_XMAXIMUM = 2200;
    private const String NAME_YMINIMUM = "YMinimum";
    private const UInt16 INIT_YMINIMUM = 1800;
    private const String NAME_YMAXIMUM = "YMaximum";
    private const UInt16 INIT_YMAXIMUM = 2200;
    private const String NAME_PULSEPERIODMS = "PulsePeriodms";
    private const UInt16 INIT_PULSEPERIODMS = 100;
    private const String NAME_PULSESMINIMUM = "PulsesMinimum";
    private const UInt16 INIT_PULSESMINIMUM = 0;
    private const String NAME_PULSESMAXIMUM = "PulsesMaximum";
    private const UInt16 INIT_PULSESMAXIMUM = 1;
    private const String NAME_DELAYMOTIONSHORTMS = "DelayMotionShortms";
    private const UInt16 INIT_DELAYMOTIONSHORTMS = 10;
    private const String NAME_DELAYMOTIONLONGMS = "DelayMotionLongms";
    private const UInt16 INIT_DELAYMOTIONLONGMS = 100;
    private const String NAME_STEPORDER = "StepOrder";
    private const Byte INIT_STEPORDER = 0;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    Random FRandom;
    CBitmap FBitmapSource;
    CBitmap FBitmapTarget;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FRandom = new Random((Int32)DateTime.Now.Ticks);
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      nudScaleFactor.Value = 150;
      pbxImage.Dock = DockStyle.None;
      pbxImage.SizeMode = PictureBoxSizeMode.AutoSize;
      cbxZoom.Checked = false;// true;
      lblColorLow.BackColor = Color.Black;
      lblColorHigh.BackColor = Color.White;
      cbxStepOrder.SelectedIndex = 0;
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //

    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      String SValue = INIT_STARTIMAGE;
      Result &= initdata.ReadString(NAME_STARTIMAGE, out SValue, SValue);
      DialogLoadImage.FileName = SValue;
      //
      Boolean BValue = INIT_ZOOMFIT;
      Result &= initdata.ReadBoolean(NAME_ZOOMFIT, out BValue, BValue);
      cbxZoom.Checked = BValue;
      //
      UInt16 UValue = INIT_SCALEFACTOR;
      Result &= initdata.ReadUInt16(NAME_SCALEFACTOR, out UValue, UValue);
      nudScaleFactor.Value = UValue;
      //
      Color CValue = INIT_COLORLOW;
      Result &= initdata.ReadColor(NAME_COLORLOW, out CValue, CValue);
      lblColorLow.BackColor = CValue;
      //
      CValue = INIT_COLORHIGH;
      Result &= initdata.ReadColor(NAME_COLORHIGH, out CValue, CValue);
      lblColorHigh.BackColor = CValue;
      //
      UValue = INIT_THRESHOLD;
      Result &= initdata.ReadUInt16(NAME_THRESHOLD, out UValue, UValue);
      nudThreshold.Value = UValue;
      //
      UValue = INIT_XMINIMUM;
      Result &= initdata.ReadUInt16(NAME_XMINIMUM, out UValue, UValue);
      nudXMinimum.Value = UValue;
      //
      UValue = INIT_XMAXIMUM;
      Result &= initdata.ReadUInt16(NAME_XMAXIMUM, out UValue, UValue);
      nudXMaximum.Value = UValue;
      //
      UValue = INIT_YMINIMUM;
      Result &= initdata.ReadUInt16(NAME_YMINIMUM, out UValue, UValue);
      nudYMinimum.Value = UValue;
      //
      UValue = INIT_YMAXIMUM;
      Result &= initdata.ReadUInt16(NAME_YMAXIMUM, out UValue, UValue);
      nudYMaximum.Value = UValue;
      //
      UValue = INIT_PULSEPERIODMS;
      Result &= initdata.ReadUInt16(NAME_PULSEPERIODMS, out UValue, UValue);
      nudPulsePeriodms.Value = UValue;
      //
      UValue = INIT_PULSESMINIMUM;
      Result &= initdata.ReadUInt16(NAME_PULSESMINIMUM, out UValue, UValue);
      nudPulsesMinimum.Value = UValue;
      //
      UValue = INIT_PULSESMAXIMUM;
      Result &= initdata.ReadUInt16(NAME_PULSESMAXIMUM, out UValue, UValue);
      nudPulsesMaximum.Value = UValue;
      //
      UValue = INIT_DELAYMOTIONSHORTMS;
      Result &= initdata.ReadUInt16(NAME_DELAYMOTIONSHORTMS, out UValue, UValue);
      nudDelayMotionShortms.Value = UValue;
      //
      UValue = INIT_DELAYMOTIONLONGMS;
      Result &= initdata.ReadUInt16(NAME_DELAYMOTIONLONGMS, out UValue, UValue);
      nudDelayMotionLongms.Value = UValue;
      //
      Byte XValue = INIT_STEPORDER;
      Result &= initdata.ReadByte(NAME_STEPORDER, out XValue, XValue);
      switch (XValue)
      {
        case 1:
          cbxStepOrder.SelectedIndex = 1;
          break;
        default:
          cbxStepOrder.SelectedIndex = 0;
          break;
      }
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      Result &= initdata.WriteString(NAME_STARTIMAGE, DialogLoadImage.FileName);
      //
      Result &= initdata.WriteBoolean(NAME_ZOOMFIT, cbxZoom.Checked);
      //
      Result &= initdata.WriteUInt16(NAME_SCALEFACTOR, (UInt16)nudScaleFactor.Value);
      //
      Result &= initdata.WriteColor(NAME_COLORLOW, lblColorLow.BackColor);
      //
      Result &= initdata.WriteColor(NAME_COLORHIGH, lblColorHigh.BackColor);
      //
      Result &= initdata.WriteUInt16(NAME_THRESHOLD, (UInt16)nudThreshold.Value);
      //
      Result &= initdata.WriteUInt16(NAME_XMINIMUM, (UInt16)nudXMinimum.Value);
      //
      Result &= initdata.WriteUInt16(NAME_XMAXIMUM, (UInt16)nudXMaximum.Value);
      //
      Result &= initdata.WriteUInt16(NAME_YMINIMUM, (UInt16)nudYMinimum.Value);
      //
      Result &= initdata.WriteUInt16(NAME_YMAXIMUM, (UInt16)nudYMaximum.Value);
      //
      Result &= initdata.WriteUInt16(NAME_PULSEPERIODMS, (UInt16)nudPulsePeriodms.Value);
      //
      Result &= initdata.WriteUInt16(NAME_PULSESMINIMUM, (UInt16)nudPulsesMinimum.Value);
      //
      Result &= initdata.WriteUInt16(NAME_PULSESMAXIMUM, (UInt16)nudPulsesMaximum.Value);
      //
      Result &= initdata.WriteUInt16(NAME_DELAYMOTIONSHORTMS, (UInt16)nudDelayMotionShortms.Value);
      //
      Result &= initdata.WriteUInt16(NAME_DELAYMOTIONLONGMS, (UInt16)nudDelayMotionLongms.Value);
      //
      Result &= initdata.WriteByte(NAME_STEPORDER, (Byte)cbxStepOrder.SelectedIndex);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    // 
    private void ConvertBitmapTargetMonochrome()
    {
      if ((FBitmapSource is CBitmap) && (FBitmapSource.GetBitmap() is Bitmap))
      {
        UInt16 SF = (UInt16)nudScaleFactor.Value;
        Bitmap BS = FBitmapSource.GetBitmap();
        FBitmapTarget = new CBitmap(BS, ref SF);
        if (SF < nudScaleFactor.Value)
        {
          nudScaleFactor.Value = SF;
        }
        FBitmapTarget.ConvertMonoChrome(cbxInvertTarget.Checked, GetThreshold(), 
                                        lblColorLow.BackColor, lblColorHigh.BackColor);
        if (cbxCompressTarget.Checked)
        {
          FBitmapTarget.CompressFourToOne();
        }
        pbxImage.Image = FBitmapTarget.GetBitmap();
        rbtConvertMonochrome.Checked = true;
      }
    }

    private void ConvertBitmapTargetGreyscale()
    {
      if ((FBitmapSource is CBitmap) && (FBitmapSource.GetBitmap() is Bitmap))
      {
        UInt16 SF = (UInt16)nudScaleFactor.Value;
        Bitmap BS = FBitmapSource.GetBitmap();
        FBitmapTarget = new CBitmap(BS, ref SF);
        if (SF < nudScaleFactor.Value)
        {
          nudScaleFactor.Value = SF;
        }
        FBitmapTarget.ConvertGreyScale(cbxInvertTarget.Checked);
        if (cbxCompressTarget.Checked)
        {
          FBitmapTarget.CompressFourToOne();
        }
        pbxImage.Image = FBitmapTarget.GetBitmap();
        rbtConvertGreyscale.Checked = true;
      }
    }

    private void RedrawBitmap()
    {
      if ((FBitmapSource is CBitmap) && (FBitmapSource.GetBitmap() is Bitmap))
      {
        Bitmap BS = FBitmapSource.GetBitmap();
        UInt16 SF = (UInt16)nudScaleFactor.Value;
        FBitmapTarget = new CBitmap(BS, ref SF);
        if (SF < nudScaleFactor.Value)
        {
          nudScaleFactor.Value = SF;
        }
        if (rbtConvertMonochrome.Checked)
        {
          ConvertBitmapTargetMonochrome();
        }
        else
        {
          if (rbtConvertGreyscale.Checked)
          {
            ConvertBitmapTargetGreyscale();
          }
          else
          {
            pbxImage.Image = FBitmapTarget.GetBitmap();
          }
        }
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //#########################################################################
    //  Segment - Callback -
    //#########################################################################
    //


    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxZoom_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxZoom.Checked)
      {
        pbxImage.Dock = DockStyle.Fill;
        pbxImage.SizeMode = PictureBoxSizeMode.Zoom;
      }
      else
      {
        pbxImage.Dock = DockStyle.None;
        pbxImage.SizeMode = PictureBoxSizeMode.AutoSize;
      }

    }

    Int32 GetScaleFactor()
    {
      return (Int32)nudScaleFactor.Value;
    }
    void SetScaleFactor(Int32 value)
    {
      nudScaleFactor.Value = value;
      RedrawBitmap();
    }

    Int32 GetThreshold()
    {
      return (Int32)nudThreshold.Value;
    }
    void SetThreshold(Int32 value)
    {
      nudThreshold.Value = value;
      hsbThreshold.Value = value;
    }

    private void nudScaleFactor_ValueChanged(object sender, EventArgs e)
    {
      SetScaleFactor((Int32)nudScaleFactor.Value);
    }

    private void hsbThreshold_ValueChanged(object sender, EventArgs e)
    {
      SetThreshold((Int32)hsbThreshold.Value);
      ConvertBitmapTargetMonochrome();
    }

    private void nudThreshold_ValueChanged(object sender, EventArgs e)
    {
      SetThreshold((Int32)nudThreshold.Value);
    }

    private void lblColorLow_MouseClick(object sender, MouseEventArgs e)
    {
      DialogColorPixel.Color = lblColorLow.BackColor;
      if (DialogResult.OK == DialogColorPixel.ShowDialog())
      {
        lblColorLow.BackColor = DialogColorPixel.Color;
        RedrawBitmap();
      }
    }

    private void lblColorHigh_MouseClick(object sender, MouseEventArgs e)
    {
      DialogColorPixel.Color = lblColorHigh.BackColor;
      if (DialogResult.OK == DialogColorPixel.ShowDialog())
      {
        lblColorHigh.BackColor = DialogColorPixel.Color;
        RedrawBitmap();
      }
    }

    private void rbtOriginal_CheckedChanged(object sender, EventArgs e)
    {
      RedrawBitmap();
    }

    private void rbtConvertGreyscale_CheckedChanged(object sender, EventArgs e)
    {
      RedrawBitmap();
    }

    private void rbtConvertMonochrome_CheckedChanged(object sender, EventArgs e)
    {
      RedrawBitmap();
    }

    private void cbxInvertTarget_CheckedChanged(object sender, EventArgs e)
    {
      RedrawBitmap();
    }

    private void cbxCompressTarget_CheckedChanged(object sender, EventArgs e)
    {
      RedrawBitmap();
    }
    //
    //------------------------------------------------------
    //  Section - Load File
    //------------------------------------------------------
    //	 
    private void btnLoadImage_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadImage.ShowDialog())
      {
        String FileName = DialogLoadImage.FileName;
        FBitmapSource = new CBitmap(FileName);
        rbtOriginalColor.Checked = true;
        RedrawBitmap();
      }
    }
    //
    //------------------------------------------------------
    //  Section - Save File
    //------------------------------------------------------
    //	 
    private void btnSaveTarget_Click(object sender, EventArgs e)
    {
      DialogSaveTarget.FileName = "Image.bmp";
      Int32 SF = (Int32)nudScaleFactor.Value;
      nudScaleFactor.Value = 1;
      if (DialogResult.OK == DialogSaveTarget.ShowDialog())
      {
        String FileName = DialogSaveTarget.FileName;
        FBitmapTarget.GetBitmap().Save(FileName);
      }
      nudScaleFactor.Value = SF;
    }

    private void btnSaveSteptable_Click(object sender, EventArgs e)
    {
      DialogSaveSteptable.FileName = "Image.stp";
      if (DialogResult.OK == DialogSaveSteptable.ShowDialog())
      {
        String FileName = DialogSaveSteptable.FileName;
        UInt16 SF = (UInt16)nudScaleFactor.Value;
        nudScaleFactor.Value = 1;
        UInt32 PulsePeriodms = (UInt32)nudPulsePeriodms.Value;
        UInt32 DelayMotionShortms = (UInt32)nudDelayMotionShortms.Value;
        UInt32 DelayMotionLongms = (UInt32)nudDelayMotionLongms.Value;
        UInt16 PulsesMinimum = (UInt16)nudPulsesMinimum.Value;
        UInt16 PulsesMaximum = (UInt16)nudPulsesMaximum.Value;
        UInt16 XMinimum = (UInt16)nudXMinimum.Value;
        UInt16 XMaximum = (UInt16)nudXMaximum.Value;
        UInt16 YMinimum = (UInt16)nudYMinimum.Value;
        UInt16 YMaximum = (UInt16)nudYMaximum.Value;
        switch (cbxStepOrder.SelectedIndex)
        {
          case 0: // LeftRight
            FBitmapTarget.SaveSteptableShortLong(FileName, PulsePeriodms,
                                                 DelayMotionShortms, DelayMotionLongms,
                                                 PulsesMinimum, PulsesMaximum,
                                                 XMinimum, XMaximum,
                                                 YMinimum, YMaximum);
            break;
          case 1: // Meander
            FBitmapTarget.SaveSteptableMeander(FileName, PulsePeriodms,
                                               DelayMotionShortms, DelayMotionLongms,
                                               PulsesMinimum, PulsesMaximum,
                                               XMinimum, XMaximum,
                                               YMinimum, YMaximum);
            break;
        }
        nudScaleFactor.Value = SF;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          tbcMain.SelectedIndex = 1;
          return true;
        case 1:
          String FN = DialogLoadImage.FileName;
          FBitmapSource = new CBitmap(FN);
          rbtOriginalColor.Checked = true;
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }

  }
}
