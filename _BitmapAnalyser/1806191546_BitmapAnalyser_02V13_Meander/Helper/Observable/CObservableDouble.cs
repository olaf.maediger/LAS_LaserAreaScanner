﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Observable
{ // BaseClass
  public class CObservableDouble
  {
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //		
    protected String FUnit;
    protected Double FMinimum, FMaximum;
    protected Double FTarget, FActual;
    protected Double FPreset, FDelta;
    protected Double FStep, FResolution, FError;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //		
    public CObservableDouble(String unit)
    {
      Unit = unit;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //		
    public String Unit
    {
      get { return FUnit; }
      set { FUnit = value; }
    }
    public Double Minimum
    {
      get { return FMinimum; }
      set { FMinimum = value; }
    }
    public Double Maximum
    {
      get { return FMaximum; }
      set { FMaximum = value; }
    }
    public Double Target
    {
      get { return FTarget; }
      set { FTarget = value; }
    }
    public Double Actual
    {
      get { return FActual; }
      set { FActual = value; }
    }
    public Double Preset
    {
      get { return FPreset; }
      set { FPreset = value; }
    }
    public Double Delta
    {
      get { return FDelta; }
      set { FDelta = value; }
    }
    public Double Step
    {
      get { return FStep; }
      set { FStep = value; }
    }
    public Double Resolution
    {
      get { return FResolution; }
      set { FResolution = value; }
    }
    public Double Error
    {
      get { return FError; }
      set { FError = value; }
    }


  }
}
