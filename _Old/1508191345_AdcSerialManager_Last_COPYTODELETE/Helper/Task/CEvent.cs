﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
namespace Task
{
	public class CEvent
	{
		private ManualResetEvent FEvent;

		public CEvent()
		{
			FEvent = new ManualResetEvent(false);
		}

		#pragma warning disable
		public IntPtr Handle
		{
			get { return FEvent.Handle; }
		}
		#pragma warning restore

		public virtual Boolean Set()
		{
			FEvent.Set();
			return true;
		}

		public virtual Boolean Reset()
		{
			FEvent.Reset();
			return true;
		}

    public virtual Boolean Pulse()
    {
      FEvent.Set();
      Thread.Sleep(1);
      FEvent.Reset();
      return true;
    }

    public virtual Boolean WaitFor(Int32 timeout)
		{
			if (FEvent.WaitOne(timeout, false))
			{
				return true;
			}
			return false;
		}

	}
}
