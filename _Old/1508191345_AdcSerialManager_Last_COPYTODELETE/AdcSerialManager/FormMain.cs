﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
//
using Initdata;
using HWComPort;
using UCNotifier;
using UCComPort;
using UCSystem;
using UCSystemLed;
using UCSystemAdc;
//
namespace AdcSerialManager
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  {
    //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //  Command - Common
    private const String COMMAND_HELP = "!H";
    //  Command - Led
    private const String COMMAND_GETLEDACTIVE = "!GLA";
    private const String COMMAND_SETLEDACTIVE = "!SLA";
    private const String COMMAND_SETLEDTOGGLING = "!SLT";
    //  Command - Adc
    private const String COMMAND_GETANALOGVALUE = "!GAV";
    private const String COMMAND_GETCHANNELACTIVE = "!GCA";
    private const String COMMAND_SETCHANNELACTIVE = "!SCA";
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;
    private CUCSystem FUCSystem;
    private CUCSystemLed FUCSystemLed;
    private CUCSystemAdc FUCSystemAdc;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      tbpSerialNumberProductKey.Controls.Add(FUCSerialNumber);
      tbcMain.Controls.Remove(tbpSerialNumberProductKey);
      //
      FUCComPort.SetNotifier(FUCNotifier);
      FUCComPort.SetOnOpenCloseChanged(UCComPortOnOpenCloseChanged);
      FUCComPort.SetOnComPortLineReceived(UCComPortOnLineReceived);
      FUCComPort.RefreshComPortControls();
      //
      FUCTerminal.SetNotifier(FUCNotifier);
      FUCTerminal.SetOnSendData(UCTerminalOnSendData);
      //
      FUCSystem = new CUCSystem();
      tbpSystem.Controls.Add(FUCSystem);
      //
      FUCSystemLed = new CUCSystemLed();
      tbpSystemLed.Controls.Add(FUCSystemLed);
      //
      FUCSystemAdc = new CUCSystemAdc();
      tbpSystemAdc.Controls.Add(FUCSystemAdc);
      //
      tmrStartup.Interval = 1000;
      // tmrStartup.Start();
      //
      FRandom = new Random();
      //
      UCComPortOnOpenCloseChanged(false);
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //     
      FUCComPort.CloseComPort();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result &= FUCComPort.LoadInitdata(initdata);
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result &= FUCComPort.SaveInitdata(initdata);
      //
      return Result;
    }


    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUCComPort
    //------------------------------------------------------------------------
    // 
    private void UCComPortOnOpenCloseChanged(Boolean opened)
    {
      RUCComPortData UCComPortData;
      FUCComPort.GetUCComPortData(out UCComPortData);
      FUCTerminal.EnableControls(UCComPortData.ComPortData.IsOpen);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUCTerminal
    //------------------------------------------------------------------------
    // 
    private void UCTerminalOnSendData(String data, Int32 delaycharacter, Int32 delaycr, Int32 delaylf)
    {
      FUCComPort.WriteText(data + "\r\n");
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCLambdaPlateController
    //------------------------------------------------------------------------
    // 
    /* NC private void UCLambdaPlateControllerOnValueChanged(Double valueactual, 
                                                       Double valuepreset,
                                                       Double valuedelta)
    {
      String StatementLine = String.Format("MA {0}\r", valueactual);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUCComPort.Write(StatementLine);
    }


    private void UCLambdaPlateControllerOnAnglePresetChanged(Double value)
    {
      String StatementLine = String.Format("MA {0}\r", value);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUCComPort.Write(StatementLine);
    }
    private void UCLambdaPlateControllerOnAngleActualChanged(Double value)
    {
      // NC 
    }

    private void UCLambdaPlateControllerOnAngleDeltaChanged(Double value)
    {
      // NC 
    }

    private void UCLambdaPlateControllerOnPeriodHalfChanged(Int32 periodhalf,
                                                            Int32 period,
                                                            Double frequency)
    {
      String StatementLine = String.Format("SPH {0}\r", periodhalf);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUCComPort.Write(StatementLine);
    }

    private void UCLambdaPlateControllerOnEnableMotion(Boolean enable)
    {
      String StatementLine = "EM ";
      if (enable)
      {
        StatementLine += String.Format("1\r");
      }
      else
      {
        StatementLine += String.Format("0\r");
      }
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUCComPort.Write(StatementLine);
    }*/
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCComPort
    //------------------------------------------------------------------------
    /*/ //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    private void UCComPortOnOpenComPort(CComPort comport)
    {
      FUCLambdaPlateControllerOld.Open();
      FUCLambdaPlateControllerNew.Open();
    }

    private void UCComPortOnCloseComPort(CComPort comport)
    {
      FUCLambdaPlateControllerOld.Close();
      FUCLambdaPlateControllerNew.Close();
    }
    */
    //???!!!private void UCComPortOnLineReceived(RComPortData comportdata,
    //???!!!                             String rxdline)
    //
    //  No Parameter
    private Boolean HandleRxdStatement(String statement)
    { // Common
      if (COMMAND_HELP == statement)
      {
        return FUCSystem.GetHelp();
      }
      // Led
      if (COMMAND_GETLEDACTIVE == statement)
      {
        return FUCSystemLed.GetLedActive();
      }
      return false;
    }
    //
    // One Parameter
    private Boolean HandleRxdStatement(String statement, String parameter1)
    { // Led
      if (COMMAND_SETLEDACTIVE == statement)
      {
        return FUCSystemLed.SetLedActive(parameter1);
      }
      if (COMMAND_SETLEDTOGGLING == statement)
      {
        return FUCSystemLed.SetLedToggling(parameter1);
      }
      // Adc
      if (COMMAND_GETANALOGVALUE == statement)
      {
        return FUCSystemAdc.GetAnalogValue(parameter1);
      }
      if (COMMAND_GETCHANNELACTIVE == statement)
      {
        return FUCSystemAdc.GetChannelActive(parameter1);
      }
      return false;
    }
    //
    // Two Parameter
    private Boolean HandleRxdStatement(String statement, String parameter1, String parameter2)
    { // Adc
      if (COMMAND_SETCHANNELACTIVE == statement)
      {
        return FUCSystemAdc.SetChannelActive(parameter1, parameter2);
      }
      return false;
    }

    private delegate void CBUCComPortOnLineReceived(Guid comportid, String line);
    private void UCComPortOnLineReceived(Guid comportid, String line)
    {
      if (this.InvokeRequired)
      {
        CBUCComPortOnLineReceived CB = new CBUCComPortOnLineReceived(UCComPortOnLineReceived);
        Invoke(CB, new object[] { comportid, line });
      }
      else
      {
        Boolean Result = false;
        // show in terminal
        FUCTerminal.AddRxData(line);
        //
        String Line = line.ToUpper();
        Char[] Delimiter = new Char[] { ' ', '\r', '\n' };
        String[] TokenList = Line.Split(Delimiter, StringSplitOptions.RemoveEmptyEntries);
        if (TokenList is String[])
        {
          if (0 < TokenList.Length)
          {
            String Statement = TokenList[0];
            if (1 < TokenList.Length)
            {
              String Parameter1 = TokenList[1];
              if (2 < TokenList.Length)
              { // <command> <parameter1> <parameter2>
                String Parameter2 = TokenList[2];
                Result = HandleRxdStatement(Statement, Parameter1, Parameter2);
              }
              else
              {
                Result = HandleRxdStatement(Statement, Parameter1);
              }
            }
            else
            {
              Result = HandleRxdStatement(Statement);
            }
          }
        }
        if (!Result)
        {
          // Error Out
        }
      }
    }
    //
    //----------------------------------------
    //  Section - Startup
    //----------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          // FUCAuroraDisplay.ClosePort();
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        default:
          return false;
      }

    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }

    //
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    //

  }
}
