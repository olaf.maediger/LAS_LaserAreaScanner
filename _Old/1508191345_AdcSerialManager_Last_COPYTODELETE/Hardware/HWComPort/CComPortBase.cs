﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;
using UCNotifier;

namespace HWComPort
{
  public class CComPortBase
  {
    //
    //------------------------------------------------------
    //  Constant
    //------------------------------------------------------
    //
    public static readonly String HEADER_LIBRARY = "ComPort";
		
		public static readonly String[] ERRORS = 
    { 
      "None",
      "Child",
      "Open failed",
      "Close failed",
      "Read failed",
      "Write failed",
      "Access failed",
      "Port failed",
  	  "Invalid"
    };

		//
    //------------------------------------------------------
    //  Member
    //------------------------------------------------------
    //
    private CNotifier FNotifier;
    //
    protected RComPortData FData;
    //
    private SerialPort FSerial = null;
    private CPRingBuffer FRXDRingBuffer = null;
    private CPRingBuffer FTXDRingBuffer = null;
    //
    private String FLineBuffer = "";
    //
    private Int32 RXDBytesThreshold
    {
      get 
      {
        FData.RXDBytesThreshold = FSerial.ReceivedBytesThreshold;
        return FData.RXDBytesThreshold;
      }
      set 
      {
        FData.RXDBytesThreshold = value;
        FSerial.ReceivedBytesThreshold = FData.RXDBytesThreshold;
      }
    }
		//
		//
		//-----------------------------
		// RXD-Filter
		//-----------------------------
		//
		private CPRXDStreamFilter FRXDStreamFilter = null;
		public CPRXDStreamFilter RXDStreamFilter
		{
			get { return FRXDStreamFilter; }
		}
		//
		//
		//-----------------------------
		// TXD-Filter
		//-----------------------------
		//
		private CPTXDStreamFilter FTXDStreamFilter = null;
		public CPTXDStreamFilter TXDStreamFilter
		{
			get { return FTXDStreamFilter; }
		}	
		//
		//
    //---------------------------
    //  Constructor
    //---------------------------
    //
		protected CComPortBase() 
		{
      FNotifier = null;
      //
      FData = new RComPortData();
      InitComPortData();
      //
      FRXDRingBuffer = new CPRingBuffer(CComPort.INIT_RXDBUFFERSIZE);
      FTXDRingBuffer = new CPRingBuffer(CComPort.INIT_TXDBUFFERSIZE);
			//
			FRXDStreamFilter = new CPRXDStreamFilter();
			FTXDStreamFilter = new CPTXDStreamFilter();
			//
			FSerial = new SerialPort();
      InitSerialInterface();
      //
		}

    private void InitComPortData()
    {
      FData.ID = Guid.Empty;
      FData.Name = CComPort.INIT_PORTNAME;
      FData.ComPort = CComPort.INIT_COMPORT;
      FData.Baudrate = CComPort.INIT_BAUDRATE;
      FData.Parity = CComPort.INIT_PARITY;
      FData.Databits = CComPort.INIT_DATABITS;
      FData.Stopbits = CComPort.INIT_STOPBITS;
      FData.Handshake = CComPort.INIT_HANDSHAKE;
      FData.RXDBufferSize = CComPort.INIT_RXDBUFFERSIZE;
      FData.RXDBytesThreshold = CComPort.INIT_RXDBYTESTHRESHOLD;
      FData.TXDBufferSize = CComPort.INIT_TXDBUFFERSIZE;
      FData.TXDDelayCarriageReturn = CComPort.INIT_TXDDELAYCARRIAGERETURN;
      FData.TXDDelayCharacter = CComPort.INIT_TXDDELAYCHARACTER;
      FData.TXDDelayLineFeed = CComPort.INIT_TXDDELAYLINEFEED;
      FData.TransmitEnabled = CComPort.INIT_TRANSMITENABLED;
      FData.IsOpen = CComPort.INIT_ISOPEN;
      FData.OnComPortDataChanged = null;
      FData.OnDataReceived = null;
      FData.OnDataTransmitted = null;
      FData.OnErrorDetected = null;
      FData.OnLineReceived = null;
      FData.OnPinChanged = null;
    }

    public virtual void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }
    //
    //-------------------------------------------
    //
    //-------------------------------------------
    //
    protected Guid ID
    {
      get { return FData.ID; }
    }
		//
		//
		//	Level-Access CTS/RTS
		//
    private Boolean GetCTS()
		{
			return FSerial.CtsHolding;
		}
    private void SetRTS(Boolean value)
		{
			FSerial.RtsEnable = value;
		}
    private Boolean GetRTS()
		{
			return FSerial.RtsEnable;
		}
		//
		//
		//	Level-Access DSR/DTR
		//
    private Boolean GetDSR()
		{
			return FSerial.DsrHolding;
		}
    private void SetDTR(Boolean newvalue)
		{
			FSerial.DtrEnable = newvalue;
		}
    private Boolean GetDTR()
		{
			return FSerial.DtrEnable;
		}
		//
		//
		//	Level-Access DCD
		//
    public Boolean GetDCD()
		{
			return FSerial.CDHolding;
		}
    //
		//
		//---------------------------
		//  Local Management
		//---------------------------
		//
    private void InitSerialInterface()
    { // Callback Low-Level
      FSerial.ErrorReceived += new SerialErrorReceivedEventHandler(SelfSerialErrorDetected);
      FSerial.PinChanged += new SerialPinChangedEventHandler(SelfSerialPinChanged);
      // own function OnDataTransmitted;
      FSerial.DataReceived += new SerialDataReceivedEventHandler(SelfSerialReceiveData);
      // NC erst bei Define FOnLineReceived += new DOnLineReceived(newonlinereceived);
      // 
      FSerial.PortName = CComPort.INIT_PORTNAME;
      FSerial.BaudRate = (Int32)CComPort.INIT_BAUDRATE;
      FSerial.DataBits = (Int32)CComPort.INIT_DATABITS;
      FSerial.StopBits = (StopBits)CComPort.INIT_STOPBITS;
      FSerial.Parity = (Parity)CComPort.INIT_PARITY;
      FSerial.DtrEnable = CComPort.INIT_DTRENABLE;
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FSerial.RtsEnable = CComPort.INIT_RTSENABLE; // true - sonst sendet Device oft keine Zeichen!!! 
      FSerial.ReceivedBytesThreshold = CComPort.INIT_RXDBYTESTHRESHOLD;
      FSerial.Handshake = (Handshake)CComPort.INIT_HANDSHAKE;
    }

    private Boolean DefineBufferSize(Int32 rxdbuffersize,
																		 Int32 txdbuffersize)
		{
			FRXDRingBuffer = new CPRingBuffer(rxdbuffersize);
			FTXDRingBuffer = new CPRingBuffer(txdbuffersize);
			return ((FTXDRingBuffer is CPRingBuffer) &&
							(FRXDRingBuffer is CPRingBuffer));
		}

    public static String[] GetPossiblePorts()
    {
      String[] PortsPossible = new String[CComPort.COMPORTS.Length];
      for (Int32 SI = 0; SI < CComPort.COMPORTS.Length; SI++)
      {
        PortsPossible[SI] = CComPort.COMPORTS[SI];
      }
      return PortsPossible;
    }
		//
		//--------------------------------------------------
		//	Messages
		//--------------------------------------------------
		//
		private String BuildHeader()
		{
			return String.Format("{0}-{1}", HEADER_LIBRARY, CComPort.ComPortName(FData.ComPort));
		}

		protected void _Error(EErrorCode code)
		{
			if (FNotifier is CNotifier)
			{
				Int32 Index = (Int32)code;
				if ((Index < 0) && (ERRORS.Length <= Index))
				{
					Index = ERRORS.Length - 1;
				}
				String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
				FNotifier.Write(BuildHeader(), Line);
			}
		}

		protected void _Protocol(String line)
		{
			if (FNotifier is CNotifier)
			{
				FNotifier.Write(BuildHeader(), line);
			}
		}
		//
		//
		//-------------------------------------------------
		//  Local Event-Handler: Error
		//-------------------------------------------------
		//
		private void SelfSerialErrorDetected(Object sender,
																				 SerialErrorReceivedEventArgs parameter)
		{ // Beachtung des User-Events UserDataReceived
			_Error(EErrorCode.PortFailed);
			return;
		}
    //
    //
    //-------------------------------------------------
    //  Local Event-Handler: SelfSerialDataReceived
    //-------------------------------------------------
    //  also: LineReceived!
    private void SelfSerialReceiveData(Object sender,
                                       SerialDataReceivedEventArgs parameter)
    { // Transfer der Daten des API-Buffers -> RXDRingBuffer
      Int32 RXDPreset = FSerial.BytesToRead;
      if (0 < RXDPreset)
      { // kein Überlaufschutz!!! überflüssige Zeichen werden ignoriert!
        Char[] Buffer = new Char[RXDPreset];
        FSerial.Read(Buffer, 0, RXDPreset);
        for (Int32 CI = 0; CI < RXDPreset; CI++)
        { // nur hier: Filterung der eingehenden Daten!!!
          // nur hier: Filterung der eingehenden Daten!!!
          // nur hier: Filterung der eingehenden Daten!!!
          String FilterText = RXDStreamFilter.Execute(Buffer[CI]);
          // nur hier: Filterung der eingehenden Daten!!!
          // nur hier: Filterung der eingehenden Daten!!!
          // nur hier: Filterung der eingehenden Daten!!!
          for (Int32 FI = 0; FI < FilterText.Length; FI++)
          {
            FRXDRingBuffer.WriteCharacter(FilterText[FI]);
          }
        }
      }
      // Beachtung des User-Events UserDataReceived
      if (FData.OnDataReceived is DOnDataReceived)
      {
        RXDPreset = FRXDRingBuffer.FillCount;
        String Data = "";
        for (Int32 CI = 0; CI < RXDPreset; CI++)
        {
          Char Character = CComPort.CHARACTER_ZERO;
          if (FRXDRingBuffer.ReadCharacter(out Character))
          {
            Data += Character;
          }
        }
        if (0 < Data.Length)
        {
          FData.OnDataReceived(ID, Data);
        }
      }
      // holt Daten einer vollständigen Zeile aus dem RXDRingBuffer
      if (FData.OnLineReceived is DOnLineReceived)
      {
        RXDPreset = FRXDRingBuffer.FillCount;
        for (Int32 CI = 0; CI < RXDPreset; CI++)
        {
          Char Character = CComPort.CHARACTER_ZERO;
          if (FRXDRingBuffer.ReadCharacter(out Character))
          {
            switch (Character)
            {
              case CComPort.CHARACTER_CR:
								if (FData.OnLineReceived is DOnLineReceived)
								{
									FData.OnLineReceived(ID, FLineBuffer);
								}
                FLineBuffer = "";
                break;
              case CComPort.CHARACTER_LF:
                // ignore...
                break;
              default:
                FLineBuffer += Character;
                break;
            }
          }
        }
      }
    }
    //
    //
    //-------------------------------------------------
    //  Local Event-Handler: SelfOnDataTransmitted
    //-------------------------------------------------
    //
    private void SelfOnDataTransmitted(Guid comportid,
                                       String data)
    {
      if (FData.OnDataTransmitted is DOnDataTransmitted)
      {
        FData.OnDataTransmitted(comportid, data);
      }
    }
    //
    //
    //-------------------------------------------------
    //  Local Event-Handler: SelfSerialPinChanged
    //-------------------------------------------------
    //
    private void SelfSerialPinChanged(Object sender,
                                      SerialPinChangedEventArgs parameter)
    {
      // Beachtung des User-Events UserPinChanged
      if (FData.OnPinChanged is DOnPinChanged)
      {
        RPinData PinData = new RPinData();
        _GetPinData(ref PinData);
        FData.OnPinChanged(ID, PinData);
        return;
      }
    }
    //
    //
    //---------------------------
    //  Static Management
    //---------------------------
    //
    public static String[] GetInstalledPorts()
    { // return: sortierte Liste!
      String[] PortsInstalled = SerialPort.GetPortNames();
      String[] PortsSorted = new String[PortsInstalled.Length];
      Int32 SortIndex = 0;
      // Sortieren...(force Sortlist of COMPORTS!)
      for (Int32 PI = 0; PI < CComPort.COMPORTS.Length; PI++)
      {
        for (Int32 AI = 0; AI < PortsInstalled.Length; AI++)
        { // sortieren nach CComPort.COMPORTS-Vorgabe!
          if (CComPort.COMPORTS[PI] == PortsInstalled[AI])
          {
            PortsSorted[SortIndex] = PortsInstalled[AI];
            SortIndex++;
          }
        }
      }
      return PortsSorted;

    }

    public static String[] GetSelectablePorts()
    {
      String[] PortsInstalled = GetInstalledPorts();
      ArrayList PortsSelectable = new ArrayList();

      SerialPort SP = new SerialPort();
      for (Int32 PI = 0; PI < PortsInstalled.Length; PI++)
      {
        try
        {
          SP.PortName = PortsInstalled[PI];
          SP.Open();
          SP.Close();
          PortsSelectable.Add(SP.PortName);
        }
        catch (Exception e)
        {
          Console.WriteLine("!!! Controlled Exception !!!");
          Console.WriteLine(e);
          Console.WriteLine("!!! Controlled Exception !!!");
        }
      }
      // Umkopieren
      String[] Result = null;
      if (0 < PortsSelectable.Count)
      {
        Result = new String[PortsSelectable.Count];
        for (Int32 SI = 0; SI < PortsSelectable.Count; SI++)
        {
          Result[SI] = (String)PortsSelectable[SI];
        }
        return Result;
      }
      return Result;
    }

    public static Boolean IsPortSelectable(String portname)
    {
      String[] PortsSelectable = GetSelectablePorts();
      if (PortsSelectable is String[])
      {
        foreach (String Port in PortsSelectable)
        {
          if (portname == Port)
          {
            return true;
          }
        }
      }
      return false;
    }
    /* much to slow!!!
    public static Boolean IsPortOpened(String portname)
    {
      String[] PortsSelectable = GetSelectablePorts();
      foreach (String Port in PortsSelectable)
      {
        if (portname == Port)
        {
          return false;
        }
      }
      return true;
    }*/
    //
    //
    //---------------------------
    //  Static Conversion
    //---------------------------
    //
    public static Int32 PortSetIndex(String value)
    {
      for (Int32 Index = 0; Index < CComPort.PORTSETS.Length; Index++)
      {
        if (value == CComPort.PORTSETS[Index])
          return Index;
      }
      return (Int32)EPortSet.Installed;
    }

    public static Int32 BaudrateTextIndex(String value)
    {
      for (Int32 Index = 0; Index < CComPort.BAUDRATES.Length; Index++)
      {
        if (value == CComPort.BAUDRATES[Index])
        {
          return Index;
        }
      }
      return 6; // 9600
    }
    public static EBaudrate BaudrateTextBaudrate(String value)
    {
      Int32 BI = BaudrateTextIndex(value);
      return CComPort.Baudrates[BI];
    }

    public static Int32 ParityTextIndex(String value)
    {
      for (Int32 Index = 0; Index < CComPort.PARITIES.Length; Index++)
      {
        if (value == CComPort.PARITIES[Index])
        {
          return Index;
        }
      }
      return 0; // None
    }
    public static EParity ParityTextParity(String value)
    {
      Int32 PI = ParityTextIndex(value);
      return CComPort.Parities[PI];
    }

    public static Int32 DatabitsTextIndex(String value)
    {
      for (Int32 Index = 0; Index < CComPort.DATABITS.Length; Index++)
      {
        if (value == CComPort.DATABITS[Index])
        {
          return Index;
        }
      }
      return 3; // 8
    }
    public static EDatabits DatabitsTextDatabits(String value)
    {
      Int32 DI = DatabitsTextIndex(value);
      return CComPort.Databits[DI];
    }

    public static Int32 StopbitsTextIndex(String value)
    {
      for (Int32 Index = 0; Index < CComPort.STOPBITS.Length; Index++)
      {
        if (value == CComPort.STOPBITS[Index])
        {
          return Index;
        }
      }
      return 1; // 1
    }
    public static EStopbits StopbitsTextStopbits(String value)
    {
      Int32 SI = StopbitsTextIndex(value);
      return CComPort.Stopbits[SI];
    }

    public static Int32 HandshakeTextIndex(String value)
    {
      for (Int32 Index = 0; Index < CComPort.HANDSHAKES.Length; Index++)
      {
        if (value == CComPort.HANDSHAKES[Index])
        {
          return Index;
        }
      }
      return 0; // None
    }
    public static EHandshake HandshakeTextHandshake(String value)
    {
      Int32 HI = HandshakeTextIndex(value);
      return CComPort.Handshakes[HI];
    }
    //
    //
    //-----------------------------------------
    //  Public Management: Open/Close
    //-----------------------------------------
    //
    protected Boolean _Open(RComPortData data)
		{
      if (!FSerial.IsOpen)
      {
        FData.Name = data.Name;
        //
        FData.ComPort = data.ComPort;
        FSerial.PortName = CComPort.COMPORTS[(Int32)FData.ComPort];
        //
        FData.Baudrate = data.Baudrate;
        FSerial.BaudRate = (Int32)FData.Baudrate;
        //
        FData.Databits = data.Databits;
        FSerial.DataBits = (Int32)FData.Databits;
        //
        FData.Stopbits = data.Stopbits;
        FSerial.StopBits = (StopBits)FData.Stopbits;
        //
        FData.Parity = data.Parity;
        FSerial.Parity = (Parity)FData.Parity;
        //
        FData.Handshake = data.Handshake;
        FSerial.Handshake = (Handshake)data.Handshake;
        //
        FData.TXDBufferSize = data.TXDBufferSize;
        FData.RXDBufferSize = data.RXDBufferSize;
        DefineBufferSize(FData.RXDBufferSize, FData.TXDBufferSize);
        //
        FData.OnComPortDataChanged = data.OnComPortDataChanged;
        FData.OnErrorDetected = data.OnErrorDetected;
        FData.OnDataTransmitted = data.OnDataTransmitted;
        FData.OnDataReceived = data.OnDataReceived;
        FData.OnLineReceived = data.OnLineReceived;
        FData.OnPinChanged = data.OnPinChanged;
        //
        FData.TXDDelayCharacter = data.TXDDelayCharacter;
        FData.TXDDelayCarriageReturn = data.TXDDelayCarriageReturn;
        FData.TXDDelayLineFeed = data.TXDDelayLineFeed;
        FData.TransmitEnabled = data.TransmitEnabled;
        //
        FSerial.Open();
        _PurgeBuffers(EBufferAccess.baReadWrite);
        FData.IsOpen = FSerial.IsOpen;
        if (FData.OnComPortDataChanged is DOnComPortDataChanged)
        {
          FData.OnComPortDataChanged(FData.ID, FData);
        }
        return FData.IsOpen;
      }
      return false;
		}

    protected Boolean _Close()
		{
      if (FSerial.IsOpen)
      {
        FSerial.Close();
        FData.IsOpen = FSerial.IsOpen;
        return !FData.IsOpen;
      }
      return false;
    }

    protected Boolean _IsOpen()
    {
      return (FSerial.IsOpen);
    }

    protected Boolean _IsClosed()
    {
			return !(FSerial.IsOpen);
    }
    //
    //
    //-----------------------------------------
    //  Public Management: Get/SetData
    //-----------------------------------------
    //
    protected Boolean _GetComPortData(out RComPortData data)
    {
      data = FData;
      return true;
    }
    protected Boolean _SetComPortData(RComPortData data)
    {
      // RO FDdata.ID
      FData.Name = data.Name;
      // RO wegen Open data.ComPort;
      // RO wegen Open data.Baudrate;
      // RO wegen Open data.Parity;
      // RO wegen Open FData.Databits;
      // RO wegen Open FData.Stopbits;
      // RO wegen Open FData.RXDBufferSize;
      // RO wegen Open FData.TXDBufferSize;
      // RO wegen Open FData.HandshakeRTSCTS;
      // RO wegen Open FData.HandshakeDTRDSR;
      FData.OnErrorDetected = data.OnErrorDetected;
      FData.OnDataReceived = data.OnDataReceived;
      FData.OnDataTransmitted = data.OnDataTransmitted;
      FData.OnLineReceived = data.OnLineReceived;
      FData.OnPinChanged = data.OnPinChanged;
      FData.TXDDelayCharacter = data.TXDDelayCharacter;
      FData.TXDDelayCarriageReturn = data.TXDDelayCarriageReturn;
      FData.TXDDelayLineFeed = data.TXDDelayLineFeed;
      FData.TransmitEnabled = data.TransmitEnabled;
      return true;
    }

    protected Boolean _GetPinData(ref RPinData data)
    {
      data.ID = ID;
      if (FSerial.IsOpen)
      {
        data.CTS = GetCTS();
        data.RTS = GetRTS();
        data.DSR = GetDSR();
        data.DTR = GetDTR();
        data.DCD = GetDCD();
        return true;
      }
      return false;
    }
    protected Boolean _SetPinData(RPinData data)
    {
      if (FSerial.IsOpen)
      {
        // RO data.ID 
        // RO data.CTS
        SetRTS(data.RTS);
        // RO data.DSR
        SetDTR(data.DTR);
        // RO data.DCD
        return true;
      }
      return false;
    }
    //
    //-----------------------------------------
    //  Public Management: Buffer-Access
    //-----------------------------------------
    //
    protected void _PurgeBuffers(EBufferAccess access)
		{
			switch (access)
			{
				case EBufferAccess.baRead:
          if (FSerial.IsOpen)
          {
            FSerial.DiscardInBuffer();
          }
					return;
				case EBufferAccess.baWrite:
          if (FSerial.IsOpen)
          {
            FSerial.DiscardOutBuffer();
          }
					return;
				default:
          if (FSerial.IsOpen)
          {
            FSerial.DiscardInBuffer();
            FSerial.DiscardOutBuffer();
          }
					return;
			}
		}
    //
    //
    //---------------------------
    //  Local writing to HWComPort
    //---------------------------
    //
    private Boolean WriteString(String filtertext)
    {
      if (FSerial.IsOpen)
      {
        if (FData.TransmitEnabled)
        {
          if ((0 == FData.TXDDelayCharacter) &&
              (0 == FData.TXDDelayLineFeed) &&
              (0 == FData.TXDDelayCarriageReturn))
          { // no delays -> quick out 
            SelfOnDataTransmitted(this.ID, filtertext);
            FSerial.Write(filtertext); // WITHOUT WriteLine!!!
          } else
          { // out with delays
            for (Int32 CI = 0; CI < filtertext.Length; CI++)
            {
              Char Character = filtertext[CI];
              if (0 < FData.TXDDelayCharacter)
              {
                SelfOnDataTransmitted(this.ID, Character.ToString());
                FSerial.Write(Character.ToString());
                Thread.Sleep(FData.TXDDelayCharacter);
              }
              if ((CComPort.CHARACTER_CR == Character) &&
                  (0 < FData.TXDDelayCarriageReturn))
              {
                SelfOnDataTransmitted(this.ID, Character.ToString());
                FSerial.Write(Character.ToString());
                Thread.Sleep(FData.TXDDelayCarriageReturn);
              } else
                if ((CComPort.CHARACTER_LF == Character) &&
                    (0 < FData.TXDDelayLineFeed))
                {
                  SelfOnDataTransmitted(this.ID, Character.ToString());
                  FSerial.Write(Character.ToString());
                  Thread.Sleep(FData.TXDDelayLineFeed);
                }
            }
          }          
        }
        return true;
      }
      return false;
    }

    private Boolean WriteBuffer(Byte[] filterdata,
                                Int32 offset,
                                Int32 length)
    {
      if (FSerial.IsOpen)
      {
        if (FData.TransmitEnabled)
        {
          Int32 Size = Math.Max(0, length - offset);
          if (0 < Size)
          {
            String TXData = "";
            for (Int32 BI = offset; BI < length; BI++)
            {
              TXData += (Char)filterdata[BI];
            }
            SelfOnDataTransmitted(this.ID, TXData);
            return WriteString(TXData);
          }
          return false;
        }
        return true;
      }
      return false;
    }
    //
    //-----------------------------------------
    //  Public Management: Write to HWComPort
    //-----------------------------------------
    //
    protected Boolean _EnableTransmit(Boolean enable)
    {
      if (FSerial.IsOpen)
      {
        FData.TransmitEnabled = enable;
        return true;
      }
      return false;
    }

    protected Boolean _WriteByte(Byte value)
    {
      if (FSerial.IsOpen)
      {
        if (FData.TransmitEnabled)
        { // Zeichen durch TXDFilter schicken
          String FilterText = TXDStreamFilter.Execute((char)value);
          return WriteString(FilterText);
        }
        return true;
      }
      return false;
    }

    protected Boolean _WriteCharacter(Char character)
    {
      if (FSerial.IsOpen)
      {
        if (FData.TransmitEnabled)
        { // Zeichen durch TXDFilter schicken
          String FilterText = TXDStreamFilter.Execute(character);
          return WriteString(FilterText);
        }
        return true;
      }
      return false;
    }

    protected Boolean _WriteLine(String line)
    {
      if (FSerial.IsOpen)
      {
        if (FData.TransmitEnabled)
        { // Zeichen durch TXDFilter schicken
          String FilterText = TXDStreamFilter.Execute(line);
          FilterText += TXDStreamFilter.Execute((Char)CComPort.INIT_TERMINATOR_CR);
          FilterText += TXDStreamFilter.Execute((Char)CComPort.CHARACTER_LF);
          return WriteString(FilterText);
        }
        return true;
      }
      return false;
    }

    protected Boolean _WriteText(String text)
    {
      if (FSerial.IsOpen)
      {
        if (FData.TransmitEnabled)
        { // Zeichen durch TXDFilter schicken
  			  String FilterText = TXDStreamFilter.Execute(text);
          return WriteString(FilterText);
        }
        return true;
      }
      return false;
    }

    protected Boolean _WriteBufferFilter(Byte[] buffer)
		{
      if (FSerial.IsOpen)
      {
        if (FData.TransmitEnabled)
        { // Zeichen durch TXDFilter schicken
  			  Byte[] FilterText = TXDStreamFilter.Execute(buffer);
          return WriteBuffer(FilterText, 0, FilterText.Length);
        }
        return true;
      }
      return false;
		}

    protected Boolean _WriteBuffer(Byte[] buffer)
		{
      if (FSerial.IsOpen)
      {
        if (FData.TransmitEnabled)
        { // Zeichen durch TXDFilter schicken
          return WriteBuffer(buffer, 0, buffer.Length);
        }
        return true;
      }
      return false;
		}

    protected Boolean _WriteBuffer(Byte[] buffer, 
															     Int32 offset, 
                                   Int32 length)
		{
      if (FSerial.IsOpen)
      {
        if (FData.TransmitEnabled)
        { // Zeichen durch TXDFilter schicken
          return WriteBuffer(buffer, offset, length);
        }
        return true;
      }
      return false;
    }
    //
    //-------------------------------------------------
    //  Public Management: ReadFrom HWComPort (Polling!)
    //-------------------------------------------------
    //
    protected Boolean _ReadCharacter(out Char character)
    {
      character = CComPort.CHARACTER_ZERO;
			return FRXDRingBuffer.ReadCharacter(out character);
    }

    protected Boolean _ReadLine(out String line)
    {
      line = "";
      return FRXDRingBuffer.ReadLine(out line);
    }

    protected Boolean _ReadText(out String text)
    {
      text = "";
      return FRXDRingBuffer.ReadText(out text);
    }
  
  }
}
