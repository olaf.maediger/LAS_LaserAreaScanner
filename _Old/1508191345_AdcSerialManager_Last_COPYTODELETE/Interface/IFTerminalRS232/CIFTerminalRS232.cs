﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using Initdata;
using HWComPort;
//
namespace IFTerminalRS232
{
  //
  //--------------------------------------
  //	Section - Type - Delegate
  //--------------------------------------
  //
  public delegate void DOnCommonDataChanged(RCommonData data);
  public delegate void DOnChannelDataChanged(RChannelData data);
  public delegate void DOnLedDataChanged(RLedData data);
  //
  //-----------------------------------------------------
  //  Segment - Type - ChannelData - A/B
  //-----------------------------------------------------
  //
  public struct RCommonData
  {
    public String SoftwareVersion;
    public String HardwareVersion;
    public String DeviceType;
    public DOnCommonDataChanged OnCommonDataChanged;
    //
    public RCommonData(Int32 init)
    {
      SoftwareVersion = CIFTerminalRS232.INIT_SOFTWAREVERSION;
      HardwareVersion = CIFTerminalRS232.INIT_HARDWAREVERSION;
      DeviceType = CIFTerminalRS232.INIT_DEVICETYPE;
      OnCommonDataChanged = null;
    }
  };
  //
  //-----------------------------------------------------
  //  Segment - Type - ChannelData - A/B
  //-----------------------------------------------------
  //
  public struct RChannelData
  {
    public Int32 Delay;
    // NC public ESweepState SweepState;
    public Int32 LimitLow;
    public Int32 LimitHigh;
    public Int32 StepDelta;
    public Int32 StepCount;
    public Int32 StepIndex;
    public Int32 RepetitionCount;
    public Int32 RepetitionIndex;
    //
    public DOnChannelDataChanged OnChannelDataChanged;
    //
    public RChannelData(Int32 init)
    {
      Delay = CIFTerminalRS232.INIT_DELAYSTEPS10PS;
      // NC SweepState = CIFTerminalRS232.INIT_SWEEPSTATE;
      LimitLow = CIFTerminalRS232.INIT_LIMITLOW;
      LimitHigh = CIFTerminalRS232.INIT_LIMITHIGH;
      StepDelta = CIFTerminalRS232.INIT_STEPDELTA;
      StepCount = CIFTerminalRS232.INIT_STEPCOUNT;
      StepIndex = CIFTerminalRS232.INIT_STEPINDEX;
      RepetitionCount = CIFTerminalRS232.INIT_REPETITIONCOUNT;
      RepetitionIndex = CIFTerminalRS232.INIT_REPETITIONINDEX;
      //
      OnChannelDataChanged = null;
    }
  }
  //
  //-----------------------------------------------------
  //  Segment - Type - LedData
  //-----------------------------------------------------
  //
  public struct RLedData
  {
    public Int32 Index;
    public Boolean Active;
    public DOnLedDataChanged OnLedDataChanged;
    //
    public RLedData(Int32 init)
    {
      Index = CIFTerminalRS232.INIT_LEDINDEX;
      Active = CIFTerminalRS232.INIT_LEDACTIVE;
      OnLedDataChanged = null;
    }
  };
  //
  //-----------------------------------------------------
  //  Segment - Type - Container ConfigurationData
  //-----------------------------------------------------
  //
  public struct RConfigurationData
  { // HWComPort
    public RComPortData ComPortData;
    public DOnComPortDataChanged OnComPortDataChanged;
    // Common
    public RCommonData CommonData;
    // DelayChannelA
    public RChannelData ChannelDataA;
    // DelayChannelB
    public RChannelData ChannelDataB;
    // Led
    public RLedData LedData3;
    public RLedData LedData7;
    //
    public RConfigurationData(Int32 init)
    {
      ComPortData = new RComPortData(init);
      OnComPortDataChanged = null;
      //
      CommonData = new RCommonData(init);
      //
      ChannelDataA = new RChannelData(init);
      ChannelDataB = new RChannelData(init);
      //
      LedData3 = new RLedData(init);
      LedData7 = new RLedData(init);
    }
  };
  //
  //#######################################################################################
  //  Segment - Main - CIFTerminalRS232
  //#######################################################################################
  //
  public class CIFTerminalRS232 : CIFTerminalRS232Base
  {
    //
    //--------------------------------------
    //	Section - Constant
    //--------------------------------------
    //

    //
    //--------------------------------------
    //	Section - Member
    //--------------------------------------
    //

    //
    //--------------------------------------
    //	Section - Constructor
    //--------------------------------------
    //
    public CIFTerminalRS232()
      : base()
    {
    }
		//
		//--------------------------------------
		//	Section - Properties
		//--------------------------------------
		//
		//
		public new void SetNotifier(CNotifier notifier)
		{
			try
			{
				base.SetNotifier(notifier);
			}
			catch (Exception)
			{
        //!!!!!!!!!!!!!!!!!!!!!_Error(EErrorCode.AccessFailed);
			}
		}
    //
		//--------------------------------------
		//	Section - Get/SetData
		//--------------------------------------
		//
    public new Boolean GetConfigurationData(ref RConfigurationData data)
    {
      try
      {
        return base.GetConfigurationData(ref data);
      }
      catch (Exception)
      {
        //!!!!!!!!!!!!!!!!!!!!!_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    public new Boolean SetConfigurationData(RConfigurationData data)
    {
      try
      {
        return base.SetConfigurationData(data);
      }
      catch (Exception)
      {
        //!!!!!!!!!!!!!!!!!!!!!_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
		//--------------------------------------
		//	Section - Management
		//--------------------------------------
		//
    /*public new void SetOnRXDLineReceived(DOnRXDLineReceived value)
    {
      try
      {
        base.SetOnRXDLineReceived(value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
      }
    }

    public new void SetOnDataTransmitted(DOnDataTransmitted value)
    {
      try
      {
        base.SetOnDataTransmitted(value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
      }
    }*/




		/*!!!!!!!!!!!!!public new Boolean OpenComPort(EComPort comport)
		{
			try
			{
				return base.OpenComPort(comport);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean CloseComPort()
		{
			try
			{
				return base.CloseComPort();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

    public new Boolean IsComPortOpen()
    {
      try
      {
        return base.IsComPortOpen();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }*/

		public new Boolean LoadInitdata(CInitdataReader initdata)
		{
			try
			{
				return base.LoadInitdata(initdata);
			}
			catch (Exception)
			{
        //_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean SaveInitdata(CInitdataWriter initdata)
		{
			try
			{
				return base.SaveInitdata(initdata);
			}
			catch (Exception)
			{
        //_Error(EErrorCode.AccessFailed);
				return false;
			}
		}
    //
    //--------------------------------------
    //	Section - Statement - Common
    //--------------------------------------
    /*/
    public new Boolean StatementGetHelp()
    {
      try
      {
        return base.StatementGetHelp();
      }
      catch (Exception)
      {
        //!!!!!!!!!!!!!!!!!!!!!_Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean StatementGetSoftwareVersion()
    {
      try
      {
        return base.StatementGetSoftwareVersion();
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean StatementGetHardwareVersion()
    {
      try
      {
        return base.StatementGetHardwareVersion();
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    //
    //--------------------------------------
    //	Section - Statement - Delay - ChannelA
    //--------------------------------------
    //
    public new Boolean StatementGetDelayChannelA()
    {
      try
      {
        return base.StatementGetDelayChannelA();
      }
      catch (Exception)
      {
        //_Error(EErrorCodeTerminal.AccessFailed);
        return false;
      }
    }

    public new Boolean StatementSetDelayChannelA(Int32 value)
    {
      try
      {
        return base.StatementSetDelayChannelA(value);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Statement - Delay - ChannelB
    //--------------------------------------
    //
    public new Boolean StatementGetDelayChannelB()
    {
      try
      {
        return base.StatementGetDelayChannelB();
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean StatementSetDelayChannelB(Int32 value)
    {
      try
      {
        return base.StatementSetDelayChannelB(value);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Statement - Led
    //--------------------------------------
    //
    public new Boolean StatementGetLed(Byte index)
    {
      try
      {
        return base.StatementGetLed(index);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean StatementClearLed(Byte index)
    {
      try
      {
        return base.StatementClearLed(index);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean StatementSetLed(Byte index)
    {
      try
      {
        return base.StatementSetLed(index);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Statement - SSA
    //--------------------------------------
    //
    public new Boolean StatementSetSweepParameterChannelA(Int32 index, Int32 value)
    {
      try
      {
        return base.StatementSetSweepParameterChannelA(index, value);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Statement - GWA
    //--------------------------------------
    //
    public new Boolean StatementGetSweepStateChannelA()
    {
      try
      {
        return base.StatementGetSweepStateChannelA();
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Statement - SWA
    //--------------------------------------
    //
    public new Boolean StatementSetSweepStateChannelA(ESweepState value)
    {
      try
      {
        return base.StatementSetSweepStateChannelA(value);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Statement - GSA
    //--------------------------------------
    //
    public new Boolean StatementGetSweepParameterChannelA(Int32 index)
    {
      try
      {
        return base.StatementGetSweepParameterChannelA(index);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Statement - SSB
    //--------------------------------------
    //
    public new Boolean StatementSetSweepParameterChannelB(Int32 index, Int32 value)
    {
      try
      {
        return base.StatementSetSweepParameterChannelB(index, value);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Statement - GSB
    //--------------------------------------
    //
    public new Boolean StatementGetSweepParameterChannelB(Int32 index)
    {
      try
      {
        return base.StatementGetSweepParameterChannelB(index);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Statement - GWB
    //--------------------------------------
    //
    public new Boolean StatementGetSweepStateChannelB()
    {
      try
      {
        return base.StatementGetSweepStateChannelB();
      }
      catch (Exception)
      {
        //  _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Statement - SWB
    //--------------------------------------
    //
    public new Boolean StatementSetSweepStateChannelB(ESweepState value)
    {
      try
      {
        return base.StatementSetSweepStateChannelB(value);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    /*/
    //--------------------------------------
    //	Section - Common
    //--------------------------------------
    /*/
    public new Boolean SendLine(String line)
    {
      try
      {
        return base.SendLine(line);
      }
      catch (Exception)
      {
        //_Error(EErrorCode.AccessFailed);
        return false;
      }
    }*/
    

	}
}
