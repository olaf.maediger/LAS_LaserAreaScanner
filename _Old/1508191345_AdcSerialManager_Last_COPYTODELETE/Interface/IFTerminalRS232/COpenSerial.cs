﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  public class COpenSerial : CStatement
  {
    public const String INIT_NAME = "OpenSerial";

    private String FPortName;
    private EComPort FComPort;
    private EBaudrate FBaudrate;
    private EParity FParity;
    private EDatabits FDatabits;
    private EStopbits FStopbits;
    private EHandshake FHandshake;

		public COpenSerial(CStatementlist parent,
									     CNotifier notifier,
									     CDevice device,
									     CComPort comport,
                       String portname,
                       EComPort ecomport,
                       EBaudrate baudrate, 
                       EParity parity,
                       EDatabits databits,
                       EStopbits stopbits,
                       EHandshake handshake)
      : base(parent, INIT_NAME,
             "",
             notifier, device, comport)
    {
      SetParent(this);
      //SetOnStatementExecuteBegin(null);
      //SetOnStatementExecuteBusy(ThreadExecuteBusy);
      //SetOnStatementExecuteResponse(null);
      //SetOnStatementExecuteEnd(null);
      FPortName = portname;
      FComPort = ecomport;
      FBaudrate = baudrate;
      FParity = parity;
      FDatabits = databits;
      FStopbits = stopbits;
      FHandshake = handshake;
    }

    protected void ThreadExecuteBusy()
    {
      RComPortData ComPortData;
      Boolean Result = HWComPort.GetComPortData(out ComPortData);
      ComPortData.Name = FPortName;
      ComPortData.ComPort = FComPort;
      ComPortData.Baudrate = FBaudrate;
      ComPortData.Parity = FParity;
      ComPortData.Databits = FDatabits;
      ComPortData.Stopbits = FStopbits;
      ComPortData.Handshake = FHandshake;
      Result &= HWComPort.Open(ComPortData);
      //
      Variable = new CVariableTextVector(8);
      Variable.Name = INIT_NAME;
      Variable.Time = CNotifier.BuildDateTimeHeader();
      String SValue = String.Format("Opened[{0}]", HWComPort.IsOpen().ToString());
      ((CVariableTextVector)Variable).SetIndexText(0, SValue);
      ((CVariableTextVector)Variable).SetIndexText(1, FPortName);
      ((CVariableTextVector)Variable).SetIndexText(2, CComPort.ComPortName(FComPort));
      ((CVariableTextVector)Variable).SetIndexText(3, FBaudrate.ToString());
      ((CVariableTextVector)Variable).SetIndexText(4, FParity.ToString());
      ((CVariableTextVector)Variable).SetIndexText(5, FDatabits.ToString());
      ((CVariableTextVector)Variable).SetIndexText(6, FStopbits.ToString());
      ((CVariableTextVector)Variable).SetIndexText(7, FHandshake.ToString());
    }


  }
}
