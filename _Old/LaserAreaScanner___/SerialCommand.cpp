//
#include "SerialCommand.h"
//
//-------------------------------------------
//  External Global Variables 
//-------------------------------------------
//  
extern enum EErrorCode GlobalErrorCode;
//
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CSerialCommand::CSerialCommand()
{
  InitSerialCommand();
}

CSerialCommand::~CSerialCommand()
{
  InitSerialCommand();
}
//
//#########################################################
//  Segment - Helper
//#########################################################
//
void CSerialCommand::ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}

void CSerialCommand::ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}

void CSerialCommand::ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdCommandLine[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Serial - Command 
//#########################################################
//
void CSerialCommand::InitSerialCommand()
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
}

Boolean CSerialCommand::DetectRxdCommandLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CARRIAGE_RETURN:
        FRxdBuffer[FRxdBufferIndex] = ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FRxdCommandLine, FRxdBuffer);
        // debug serial.WriteText("<CR>true");
        // debug delay(1000);
        return true;
      case LINE_FEED: // ignore
        // debug serial.WriteText("<LF>");
        // debug delay(1000);
        break;
      default: 
        // debug serial.WriteText("<");
        // debug serial.WriteCharacter(C);
        // debug serial.WriteText(">");
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean CSerialCommand::AnalyseRxdCommand(CSerial &serial)
{
  if (DetectRxdCommandLine(serial))
  {
    char *PTerminal = " \t\r\n";
    char *PRxdCommandLine = FRxdCommandLine;
    FPRxdCommand = strtok(FRxdCommandLine, PTerminal);
    if (FPRxdCommand)
    {
      FRxdParameterCount = 0;
      char *PRxdParameter;
      while (PRxdParameter = strtok(0, PTerminal))
      {
        FPRxdParameters[FRxdParameterCount] = PRxdParameter;
        FRxdParameterCount++;
        if (COUNT_RXDPARAMETERS < FRxdParameterCount)
        {
          GlobalErrorCode = ecToManyParameters;
          ZeroRxdBuffer();
          // debug serialtarget.WriteLine("error[ecToManyParameters]");
          serial.WriteNewLine();
          serial.WritePrompt();
          return false;
        }
      }  
//      debug sprintf(TxdBuffer, "\n\r# RxdCommand<%s>", PRxdCommand);
//      debug serial.WriteText(TxdBuffer);
//      if (0 < RxdParameterCount)
//      {
//        int II;
//        for (II = 0; II < RxdParameterCount; II++)
//        {
//          sprintf(TxdBuffer, " RxdParameter[%i]<%s>", II, PRxdParameters[II]);
//          serial.WriteText(TxdBuffer);
//        }
//      }
      //
      ZeroRxdBuffer();
      serial.WriteNewLine();
      return true;
    }
    ZeroRxdBuffer();
    serial.WriteNewLine();
    serial.WritePrompt();
  }
  return false;
}



