//
//--------------------------------
//  Library Pwm
//--------------------------------
//
#include "Pwm.h"
//
CPwm::CPwm(bool channel0, bool channel1, bool channel2, bool channel3)
{
  FChannel[0] = channel0;
  FChannel[1] = channel1;
  FChannel[2] = channel2;
  FChannel[3] = channel3;
}
CPwm::CPwm(bool channel0, bool channel1, bool channel2, bool channel3,
           bool channel4, bool channel5, bool channel6, bool channel7)
{
  FChannel[0] = channel0;
  FChannel[1] = channel1;
  FChannel[2] = channel2;
  FChannel[3] = channel3;
  FChannel[4] = channel0;
  FChannel[5] = channel1;
  FChannel[6] = channel2;
  FChannel[7] = channel3;
}
CPwm::CPwm(bool channel0, bool channel1, bool channel2, bool channel3,
           bool channel4, bool channel5, bool channel6, bool channel7,
           bool channel8, bool channel9, bool channel10, bool channel11)
{
  FChannel[0] = channel0;
  FChannel[1] = channel1;
  FChannel[2] = channel2;
  FChannel[3] = channel3;
  FChannel[4] = channel4;
  FChannel[5] = channel5;
  FChannel[6] = channel6;
  FChannel[7] = channel7;
  FChannel[8] = channel8;
  FChannel[9] = channel0;
  FChannel[10] = channel10;
  FChannel[11] = channel11;
}

Boolean CPwm::Open()
{
  analogWriteResolution(12);
  return true;
}

Boolean CPwm::Close()
{
  return true;
}

UInt16 CPwm::GetValue(UInt8 channel)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
      case 0:
        return FValue[0];
      case 1:
        return FValue[1];
      case 2:
        return FValue[2];
      case 3:
        return FValue[3];
      case 4:
        return FValue[4];
      case 5:
        return FValue[5];
      case 6:
        return FValue[6];
      case 7:
        return FValue[7];
      case 8:
        return FValue[8];
      case 9:
        return FValue[9];
      case 10:
        return FValue[10];
      case 11:
        return FValue[11];
    }    
  }
  return 0;
}

void CPwm::SetValue(UInt8 channel, UInt16 value)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
      case 0:
        FValue[0] = (0x0FFF & value);
        analogWrite(PIN_PWM0, value);
        break;
      case 1:
        FValue[1] = (0x0FFF & value);
        analogWrite(PIN_PWM1, value);
        break;
      case 2:
        FValue[2] = (0x0FFF & value);
        analogWrite(PIN_PWM2, value);
        break;
      case 3:
        FValue[3] = (0x0FFF & value);
        analogWrite(PIN_PWM3, value);
        break;
      case 4:
        FValue[4] = (0x0FFF & value);
        analogWrite(PIN_PWM4, value);
        break;
      case 5:
        FValue[5] = (0x0FFF & value);
        analogWrite(PIN_PWM5, value);
        break;
      case 6:
        FValue[6] = (0x0FFF & value);
        analogWrite(PIN_PWM6, value);
        break;
      case 7:
        FValue[7] = (0x0FFF & value);
        analogWrite(PIN_PWM7, value);
        break;
      case 8:
        FValue[8] = (0x0FFF & value);
        analogWrite(PIN_PWM8, value);
        break;
      case 9:
        FValue[9] = (0x0FFF & value);
        analogWrite(PIN_PWM9, value);
        break;
      case 10:
        FValue[10] = (0x0FFF & value);
        analogWrite(PIN_PWM10, value);
        break;
      case 11:
        FValue[11] = (0x0FFF & value);
        analogWrite(PIN_PWM11, value);
        break;
    }    
  }
}


