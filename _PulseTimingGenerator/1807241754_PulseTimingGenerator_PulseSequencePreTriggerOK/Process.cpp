#include "Led.h"
#include "Process.h"
#include "Command.h"
#include "Pulse.h"
//
extern CLed LedSystem;
extern CSerial SerialCommand;
extern CProcess PTGProcess;
extern CCommand PTGCommand;
extern CPulse PulseTrigger;
//
//---------------------------------------------------
// Segment - IrqCallback - PulseTimer
//---------------------------------------------------
//
// IrqHandler - Process
//
void IrqHandlerPulsePeriod(void)
{ // LedLaser/System - On
  LedSystem.On();
  // PulseTrigger - PulseWidth - Start
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidth(void)
{ // LedLaser/System - Off
  LedSystem.Off();
  // PulseTrigger - PulseWidth - Stop
  PulseTrigger.StopWidth();
  PulseTrigger.IncrementStopPulseStep();
}
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CProcess::CProcess()
{
  FState = spUndefined;
  FStateTick = 0;
  FTimeStampms = millis();
  FTimeMarkms = FTimeStampms;
  //
  FSystemPeriodus = INIT_SYSTEMPERIOD_US;
  FSystemCountPreset = INIT_SYSTEMCOUNTPRESET;
  FSystemCountActual = INIT_SYSTEMCOUNTACTUAL;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateProcess CProcess::GetState()
{
  return FState;
}

void CProcess::SetState(EStateProcess state)
{
  FStateTick = 0;
  FState = state;
  FSubstate = 0;
  // Message to Manager(PC):
  PTGCommand.CallbackStateProcess(SerialCommand, FState, FSubstate);
}

void CProcess::SetState(EStateProcess state, UInt8 substate)
{
  if ((state != FState) || (substate != FSubstate))
  {     
    if (state != FState)
    {
      FStateTick = 0;
    }
    FState = state;
    FSubstate = substate;
    switch (FState)
    {
      case spStopSystemExecution:
        break;
    }
    // Message to Manager(PC):
    PTGCommand.CallbackStateProcess(SerialCommand, FState, substate);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CProcess::Open()
{
  SetState(spIdle);
  return true;
}

Boolean CProcess::Close()
{
  SetState(spUndefined);
  return true;
}

//
//----------------------------------------------------------
//  Segment - Common
//----------------------------------------------------------
//
void CProcess::HandleUndefined(CSerial &serial)
{
  delay(1000);
}

void CProcess::HandleIdle(CSerial &serial)
{ // do nothing
  SetTimeStamp();
}

void CProcess::HandleWelcome(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:        
      SetTimeStamp();
      FStateTick++;
      break;
    case 1: case 3: case 5: 
      // if (330000 < GetTimeSpan())
      if (330 < GetTimeSpanms())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 2: case 4: case 6:  
      // if (330000 < GetTimeSpan())
      if (330 < GetTimeSpanms())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 7:  
      FStateTick++;      
      SetTimeStamp();      
      break;
    case 8:   
      // if (1000000 < GetTimeSpan())
      if (1000 < GetTimeSpanms())
      {   
        FStateTick++;
        SetTimeStamp();
      }
      break;
    case 9:   
      // if (1000000 < GetTimeSpan())
      if (1000 < GetTimeSpanms())
      {   
        FStateTick++;
        
        SetState(spIdle);
// alternative : 
//        UInt8 CL = 0;
//        UInt8 CH = 7;
//        PTGProcess.SetChannelLow(CL);
//        PTGProcess.SetChannelHigh(CH);
//        PTGProcess.SetState(spRepeatAllTemperatures);
//        PTGProcess.SetProcessIndex(0);
//        SetState(spRepeatAllTemperatures);
        SetTimeStamp();
      }
      break;
  }
}

void CProcess::HandleGetHelp(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProgramHeader(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSoftwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetHardwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSystemPeriodus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetSystemPeriodus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSystemCountPreset(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetSystemCountPreset(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleStopSystemExecution(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - PulseTiming
//----------------------------------------------------------
//
void CProcess::HandleGetPulsePeriodus(CSerial &serial)
{
  SetState(spIdle);
}
void CProcess::HandleSetPulsePeriodus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetPulseWidthus(CSerial &serial)
{
  SetState(spIdle);
}
void CProcess::HandleSetPulseWidthus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetPulseCount(CSerial &serial)
{
  SetState(spIdle);
}
void CProcess::HandleSetPulseCount(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - LedSystem
//----------------------------------------------------------
//
void CProcess::LedSystemState()
{
  switch (LedSystem.GetState())
  {
    case slOn:
      break;
    case slOff:
      break;
    default:
      break;
  }
  SetState(spIdle, LedSystem.GetState());
}

void CProcess::HandleGetLedSystem(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOn(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOff(CSerial &serial)
{
  LedSystemState();
}

// 1807241529 - New WITHOUT PulseIRQ!!!
void CProcess::HandleBlinkLedSystem(CSerial &serial)
{
  const int STATE_INIT        = 0;
  const int STATE_PULSEHIGH   = 1;
  const int STATE_PULSEWIDTH  = 2;
  const int STATE_PULSEPERIOD = 3;
  const int STATE_END         = 4;
  //
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      PulseTrigger.SetPulseStep(0);
      PulseTrigger.SetPulsePeriodus(max(1, PulseTrigger.GetPulsePeriodus()));
      PulseTrigger.SetPulseWidthus(max(1, PulseTrigger.GetPulseWidthus()));
      if (0 < PulseTrigger.GetPulseCount())
      {
        LedSystem.Off();
        SetState(spBlinkLedSystem, 0);
        FStateTick = STATE_PULSEHIGH;
      }
      else
      {
        FStateTick = STATE_END;
      }
      break;
    case STATE_PULSEHIGH: 
      SetTimeStamp();
      LedSystem.On();
      SetState(spBlinkLedSystem, 1);
      FStateTick = STATE_PULSEWIDTH;
      break;
    case STATE_PULSEWIDTH:
      if (PulseTrigger.GetPulseWidthus() < GetTimeSpanus())
      {
        LedSystem.Off();
        SetState(spBlinkLedSystem, 0);
        FStateTick = STATE_PULSEPERIOD;
      }
      break;
    case STATE_PULSEPERIOD:
      if (PulseTrigger.GetPulsePeriodus() < GetTimeSpanus())
      {
        if (PulseTrigger.IncrementStopPulseStep() < PulseTrigger.GetPulseCount())
        {
          FStateTick = STATE_PULSEHIGH;
        }
        else
        {
          FStateTick = STATE_END;
        }
      }
      break;
    case STATE_END:
      SetState(spIdle);
      break;
  } 
}
//
//----------------------------------------------------------
// Segment - PulseSequence
//----------------------------------------------------------
// 
//void CProcess::HandleStartPulseSequence(CSerial &serial)
//{ 
//  const int STATE_INIT  = 0;
//  const int STATE_START = 1;
//  const int STATE_BUSY  = 2;
//  const int STATE_END   = 3;
//  //  
//  SetTimeMark();
//  switch (FStateTick)
//  {
//    case STATE_INIT: 
//      SetTimeStamp();
//      LedSystem.Off();
//      PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
//      PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
//      FStateTick = STATE_START;
//      break;
//    case STATE_START:
//      if (0 < PulseTrigger.GetPulseCount())
//      {
//        FStateTick = STATE_BUSY;
//        PulseTrigger.Start();        
//      }
//      else
//      {
//        FStateTick = STATE_END;
//      }
//      break;
//    case STATE_BUSY: 
//      if (PulseTrigger.GetPulseCount() <= PulseTrigger.GetPulseStep())
//      {
//        FStateTick = STATE_END;        
//      }
//      break;
//    case STATE_END:      
//      PulseTrigger.Stop();
//      SetState(spIdle);      
//      SetTimeStamp(); 
//      break;      
//  }
//}

void CProcess::HandleStartPulseSequence(CSerial &serial)
{ 
  const int STATE_INIT  = 0;
  const int STATE_START = 1;
  const int STATE_BUSY  = 2;
  const int STATE_END   = 3;
  //  
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT: 
      SetTimeStamp();
      LedSystem.Off();
      PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
      PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
      FStateTick = STATE_START;
      break;
    case STATE_START:
      if (0 < PulseTrigger.GetPulseCount())
      {
        FStateTick = STATE_BUSY;
        PulseTrigger.Start();        
        IrqHandlerPulsePeriod();
      }
      else
      {
        FStateTick = STATE_END;
      }
      break;
    case STATE_BUSY: 
      if (PulseTrigger.GetPulseCount() <= PulseTrigger.GetPulseStep())
      {
        FStateTick = STATE_END;        
      }
      break;
    case STATE_END:      
      PulseTrigger.Stop();
      SetState(spIdle);      
      SetTimeStamp(); 
      break;      
  }
}

void CProcess::HandleAbortPulseSequence(CSerial &serial)
{
  LedSystem.Off();
  SetState(spIdle);
  SetTimeStamp(); 
}
//
//----------------------------------------------------------
//  Segment - Collector
//----------------------------------------------------------
//
void CProcess::Handle(CSerial &serial)
{
  switch (GetState())
  { // Common
    case spIdle:
      HandleIdle(serial);
      break;
    case spWelcome:
      HandleWelcome(serial);
      break;
    case spGetHelp:
      HandleGetHelp(serial);
      break;
    case spGetProgramHeader:
      HandleGetProgramHeader(serial);
      break;
    case spGetSoftwareVersion:
      HandleGetSoftwareVersion(serial);
      break;
    case spGetHardwareVersion:
      HandleGetHardwareVersion(serial);
      break;
    case spGetSystemPeriod:
      HandleGetSystemPeriodus(serial);
      break;
    case spSetSystemPeriod:
      HandleSetSystemPeriodus(serial);
      break;
    case spGetSystemCountPreset:
      HandleGetSystemCountPreset(serial);
      break;
    case spSetSystemCountPreset:
      HandleSetSystemCountPreset(serial);
      break;
    case spStopSystemExecution:
      HandleStopSystemExecution(serial);
      break;
    // Measurement - PulseTiming
    case spGetPulsePeriod:
      HandleGetPulsePeriodus(serial);
      break;
    case spSetPulsePeriod:
      HandleSetPulsePeriodus(serial);
      break;
    case spGetPulseWidth:
      HandleGetPulseWidthus(serial);
      break;
    case spSetPulseWidth:
      HandleSetPulseWidthus(serial);
      break;
    case spGetPulseCount:
      HandleGetPulseCount(serial);
      break;
    case spSetPulseCount:
      HandleSetPulseCount(serial);
      break;
    // LedSystem
    case spGetLedSystem:
      HandleGetLedSystem(serial);
      break;
    case spLedSystemOn:
      HandleLedSystemOn(serial);
      break;
    case spLedSystemOff:
      HandleLedSystemOff(serial);
      break;
    case spBlinkLedSystem:
      HandleBlinkLedSystem(serial);
      break;
    // PulseSequence
    case spStartPulseSequence:
      HandleStartPulseSequence(serial);
      break;
    case spAbortPulseSequence:
      HandleAbortPulseSequence(serial);
      break;
    default: // spUndefined
      HandleUndefined(serial);
      break;
  }  
}
