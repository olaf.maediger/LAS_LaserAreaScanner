//
#include "Command.h"
#include "Led.h"
#include "Error.h"
#include "Process.h"
#include "Pulse.h"
//
//-------------------------------------------
//  External Global Variables 
//-------------------------------------------
//  
extern CError PTGError;
extern CProcess PTGProcess;
extern CCommand PTGCommand;
extern CLed LedSystem;
extern CSerial SerialCommand;
extern CPulse PulseTrigger;
//
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand()
{
  Init();
}

CCommand::~CCommand()
{
  Init();
}
//
//#########################################################
//  Segment - Helper
//#########################################################
//
void CCommand::ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}

void CCommand::ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}

void CCommand::ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdCommandLine[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Management
//#########################################################
//
void CCommand::Init()
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
}

Boolean CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CR:
        FRxdBuffer[FRxdBufferIndex] = ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FRxdCommandLine, FRxdBuffer);
        return true;
      case LF: // ignore
        break;
      default: 
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean CCommand::Analyse(CSerial &serial)
{
  if (DetectRxdLine(serial))
  {
    char *PTerminal = (char*)" \t\r\n";
    char *PRxdCommandLine = FRxdCommandLine;
    FPRxdCommand = strtok(FRxdCommandLine, PTerminal);
    if (FPRxdCommand)
    {
      FRxdParameterCount = 0;
      char *PRxdParameter;
      while (PRxdParameter = strtok(0, PTerminal))
      {
        FPRxdParameters[FRxdParameterCount] = PRxdParameter;
        FRxdParameterCount++;
        if (COUNT_RXDPARAMETERS < FRxdParameterCount)
        {
          PTGError.SetCode(ecToManyParameters);
          ZeroRxdBuffer();
          // debug serialtarget.WriteLine("error[ecToManyParameters]");
          serial.WriteNewLine();
          serial.WritePrompt();
          return false;
        }
      }  
//      debug sprintf(TxdBuffer, "\n\r# RxdCommand<%s>", PRxdCommand);
//      debug serial.Write(TxdBuffer);
//      if (0 < RxdParameterCount)
//      {
//        int II;
//        for (II = 0; II < RxdParameterCount; II++)
//        {
//          sprintf(TxdBuffer, " RxdParameter[%i]<%s>", II, PRxdParameters[II]);
//          serial.Write(TxdBuffer);
//        }
//      }
      //
      ZeroRxdBuffer();
      serial.WriteNewLine();
      return true;
    }
    ZeroRxdBuffer();
    serial.WriteNewLine();
    serial.WritePrompt();
  }
  return false;
}
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PROJECT, ARGUMENT_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_HARDWARE, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_DATE, ARGUMENT_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_TIME, ARGUMENT_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_AUTHOR, ARGUMENT_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PORT, ARGUMENT_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PARAMETER, ARGUMENT_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void CCommand::WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
}

void CCommand::WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
}

void CCommand::WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.WriteLine(MASK_H, SHORT_H);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPH, SHORT_GPH);
  serial.WriteAnswer(); serial.WriteLine(MASK_GSV, SHORT_GSV);
  serial.WriteAnswer(); serial.WriteLine(MASK_GHV, SHORT_GHV);
  serial.WriteAnswer(); serial.WriteLine(MASK_GSP, SHORT_GSP);
  serial.WriteAnswer(); serial.WriteLine(MASK_SSP, SHORT_SSP);
  serial.WriteAnswer(); serial.WriteLine(MASK_GSC, SHORT_GSC);
  serial.WriteAnswer(); serial.WriteLine(MASK_SSC, SHORT_SSC);
  serial.WriteAnswer(); serial.WriteLine(MASK_A, SHORT_A);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_PULSETIMING);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPP, SHORT_GPP);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPP, SHORT_SPP);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPW, SHORT_GPW);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPW, SHORT_SPW);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPC, SHORT_GPC);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPC, SHORT_SPC);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LEDSYSTEM);
  serial.WriteAnswer(); serial.WriteLine(MASK_GLS, SHORT_GLS);
  serial.WriteAnswer(); serial.WriteLine(MASK_LSH, SHORT_LSH);
  serial.WriteAnswer(); serial.WriteLine(MASK_LSL, SHORT_LSL);
  serial.WriteAnswer(); serial.WriteLine(MASK_BLS, SHORT_BLS);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_PULSESEQUENCE);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPS, SHORT_SPS);
  serial.WriteAnswer(); serial.WriteLine(MASK_APS, SHORT_APS);
//  //
//  serial.WriteAnswer(); serial.WriteLine(HELP_LASERLINE);
//  serial.WriteAnswer(); serial.WriteLine(MASK_PLR, SHORT_PLR);
//  serial.WriteAnswer(); serial.WriteLine(MASK_ALR, SHORT_ALR);
//  serial.WriteAnswer(); serial.WriteLine(MASK_PLC, SHORT_PLC);
//  serial.WriteAnswer(); serial.WriteLine(MASK_ALC, SHORT_ALC);
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
void CCommand::ExecuteGetHelp(CSerial &serial)
{
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spGetHelp);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HELP);
    serial.WriteLine(GetTxdBuffer());         
    WriteHelp(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetProgramHeader(CSerial &serial)
{
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spGetProgramHeader);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HEADER);
    serial.WriteLine(GetTxdBuffer());   
    WriteProgramHeader(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetSoftwareVersion(CSerial &serial)
{
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spGetSoftwareVersion);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_SOFTWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteSoftwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spGetHardwareVersion);
    // Analyse parameters: -
    // Response
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HARDWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteHardwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetSystemPeriodus(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spGetSystemPeriod);
    // Analyse parameters: -
    // Execute:
    UInt32 SP = PTGProcess.GetSystemPeriodus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), SP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetSystemPeriodus(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spSetSystemPeriod);
    // Analyse parameters:
    UInt32 SP = atol(GetPRxdParameters(0));
    // Execute:
    PTGProcess.SetSystemPeriodus(SP);
    SP = PTGProcess.GetSystemPeriodus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), SP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetSystemCountPreset(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spGetSystemCountPreset);
    // Analyse parameters: -
    // Execute:
    UInt32 SCP = PTGProcess.GetSystemCountPreset();    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), SCP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetSystemCountPreset(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spSetSystemCountPreset);
    // Analyse parameters:
    UInt32 SCP = atol(GetPRxdParameters(0));
    // Execute:
    PTGProcess.SetSystemCountPreset(SCP);
    SCP = PTGProcess.GetSystemCountPreset();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), SCP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteAbortAll(CSerial &serial)
{ 
  PTGProcess.SetState(spIdle);
  // Analyse parameters:
  // Execute:
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - PulseTiming
//#########################################################
//
void CCommand::ExecuteGetPulsePeriodus(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spGetPulsePeriod);
    // Analyse parameters: - ( GPP )
    // Execute:
    UInt32 PP = PulseTrigger.GetPulsePeriodus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetPulsePeriodus(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spSetPulsePeriod);
    // Analyse parameters ( SPP <period> ):
    UInt32 PP = atol(GetPRxdParameters(0));    
    // Execute:
    PulseTrigger.SetPulsePeriodus(PP);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetPulseWidthus(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spGetPulseWidth);
    // Analyse parameters: - ( GPW )
    // Execute:
    UInt32 PW = PulseTrigger.GetPulseWidthus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PW);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetPulseWidthus(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spSetPulseWidth);
    // Analyse parameters ( SPW <width> ):
    UInt32 PW = atol(GetPRxdParameters(0));    
    // Execute:
    PulseTrigger.SetPulseWidthus(PW);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PW);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetPulseCount(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spGetPulseCount);
    // Analyse parameters: - ( GPC )
    // Execute:
    UInt32 PC = PulseTrigger.GetPulseCount();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetPulseCount(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spSetPulseCount);
    // Analyse parameters ( SPC <count> ):
    UInt32 PC = atol(GetPRxdParameters(0));    
    // Execute:
    PulseTrigger.SetPulseCount(PC);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - LedSystem
//#########################################################
//
void CCommand::ExecuteGetLedSystem(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spGetLedSystem);
    // Analyse parameters:
    // Execute:
    int State = LedSystem.GetState();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), State);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteLedSystemOn(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spLedSystemOn);
    // Analyse parameters:
    // Execute:
    LedSystem.On();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteLedSystemOff(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spLedSystemOff);
    // Analyse parameters:
    // Execute:
    LedSystem.Off();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteBlinkLedSystem(CSerial &serial)
{ 
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spBlinkLedSystem);
//    // Analyse parameters:
//    UInt32 Period = atol(GetPRxdParameters(0)); // [us]!!!!!!!!
//    UInt32 Count = atol(GetPRxdParameters(1));  // [1] 
//    // Execute:
//    PTGProcess.SetProcessPeriod(Period);
//    PTGProcess.SetProcessCount(Count);    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", // %lu %lu", 
            PROMPT_RESPONSE, GetPRxdCommand()); //, Period, Count);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - PulseSequence
//#########################################################
//
void CCommand::ExecuteStartPulseSequence(CSerial &serial)
{
  if (spIdle == PTGProcess.GetState())
  {
    PTGProcess.SetState(spStartPulseSequence);
//    // Analyse parameters ( SPS ): - 
//    UInt32 PP = atol(GetPRxdParameters(0));
//    UInt32 PC = atol(GetPRxdParameters(1));
//    UInt32 PW = PTGProcess.GetPulseWidthus();
//    PulseTrigger.SetPulsePeriodus(1000 * PP);
//    PulseTrigger.SetPulseCount(PC);
//    PulseTrigger.SetPulseWidthus(PW);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", // %lu %lu", 
            PROMPT_RESPONSE, GetPRxdCommand());//, PP, PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteAbortPulseSequence(CSerial &serial)
{
  PTGProcess.SetState(spAbortPulseSequence);
//    PulseTrigger.Stop();
////    PTGProcess.SetProcessStep(0);
////    PTGProcess.SetProcessCount(0);
//    PTGProcess.SetState(spIdle);
//    // Analyse parameters : -    
//    // Execute : -    
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
}


//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
void CCommand::CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate)
{
//  switch (stateprocess)
//  { // Base
//    case spUndefined:
//      break; 
//    case spIdle:
//      break; 
//    default:
//      break;
//  }  
//  serial.WriteNewLine();
//  serial.WritePrompt();
  sprintf(PTGCommand.GetTxdBuffer(), MASK_STATEPROCESS, PROMPT_EVENT, stateprocess, substate);
  serial.WriteLine(PTGCommand.GetTxdBuffer());
  serial.WritePrompt();
}

Boolean CCommand::Execute(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
// debug Serial.WriteLine(TxdBuffer);
  if (!strcmp(SHORT_H, GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPH, GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GSV, GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GHV, GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GSP, GetPRxdCommand()))
  {
    ExecuteGetSystemPeriodus(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SSP, GetPRxdCommand()))
  {
    ExecuteSetSystemPeriodus(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GSC, GetPRxdCommand()))
  {
    ExecuteGetSystemCountPreset(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SSC, GetPRxdCommand()))
  {
    ExecuteSetSystemCountPreset(serial);
    return true;
  } else 
  if (!strcmp(SHORT_A, GetPRxdCommand()))
  {
    ExecuteAbortAll(serial);
    return true;
  } else 
  // ----------------------------------
  // Measurement - PulseTiming
  // ----------------------------------
  if (!strcmp(SHORT_GPP, GetPRxdCommand()))
  {
    ExecuteGetPulsePeriodus(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPP, GetPRxdCommand()))
  {
    ExecuteSetPulsePeriodus(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GPW, GetPRxdCommand()))
  {
    ExecuteGetPulseWidthus(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPW, GetPRxdCommand()))
  {
    ExecuteSetPulseWidthus(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GPC, GetPRxdCommand()))
  {
    ExecuteGetPulseCount(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPC, GetPRxdCommand()))
  {
    ExecuteSetPulseCount(serial);
    return true;
  } else   
  // ----------------------------------
  // LedSystem
  // ---------------------------------- 
  if (!strcmp(SHORT_GLS, GetPRxdCommand()))
  {
    ExecuteGetLedSystem(serial);
    return true;
  } else 
  if (!strcmp(SHORT_LSH, GetPRxdCommand()))
  {
    ExecuteLedSystemOn(serial);
    return true;
  } else   
  if (!strcmp(SHORT_LSL, GetPRxdCommand()))
  {
    ExecuteLedSystemOff(serial);
    return true;
  } else   
  if (!strcmp(SHORT_BLS, GetPRxdCommand()))
  {
    ExecuteBlinkLedSystem(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - PulseSequence
  // ----------------------------------
  if (!strcmp(SHORT_SPS, GetPRxdCommand()))
  {
    ExecuteStartPulseSequence(serial);
    return true;
  } else   
  if (!strcmp(SHORT_APS, GetPRxdCommand()))
  {
    ExecuteAbortPulseSequence(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    PTGError.SetCode(ecInvalidCommand);
  }
  return false;  
}

Boolean CCommand::Handle(CSerial &serial)
{
  if (Analyse(serial))
  {
    return Execute(serial);
  }  
  return false;
}



