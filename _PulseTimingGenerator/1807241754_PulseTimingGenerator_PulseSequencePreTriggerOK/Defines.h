 //
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
//
#define TRUE 1
#define FALSE 0
//
// Init-Values - Global
#define INIT_ERRORCODE ecNone
#define INIT_RXDECHO false
//
//----------------------------------
// ARDUINOUNO :
// #define PROCESSOR_ARDUINOUNO
//
//----------------------------------
// Arduino Mega 2560
// #define PROCESSOR_ARDUINOMEGA
//
//----------------------------------
// Arduino Due
// 
#define PROCESSOR_ARDUINODUE
//
//----------------------------------
// STM32F103C8 : no DACs!
// #define PROCESSOR_STM32F103C8
//
//----------------------------------
// Teensy 3.2
// #define PROCESSOR_TEENSY32
//
//----------------------------------
// Teensy 3.6
// #define PROCESSOR_TEENSY36
//
//----------------------------------
// Arduino Uno R3
// #define PROCESSOR_ARDUINOUNO
//
//##########################################
//  PROCESSOR_ARDUINOUNO
//##########################################
#if defined(PROCESSOR_ARDUINOUNO)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
// System Led 
const int PIN_LEDSYSTEM       = 13; // used from SPI
const bool LEDSYSTEM_INVERTED = false;
//
// SPI
const int PIN_SPI_SS    = 10;
const int PIN_SPI_SCK   = 13; // used from LedSystem
const int PIN_SPI_MISO  = 12;
const int PIN_SPI_MOSI  = 11;
//
// I2C
const int PIN_I2C_SCL   = 5;
const int PIN_I2C_SDA   = 4;
//
#endif // PROCESSOR_ARDUINOUNO
//
//##########################################
//  PROCESSOR_ARDUINOMEGA
//##########################################
#if defined(PROCESSOR_ARDUINOMEGA)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
// System Led 
const int PIN_LEDSYSTEM       = 13;
const bool LEDSYSTEM_INVERTED = false;
//
#endif // PROCESSOR_ARDUINOMEGA
////
////##########################################
////  PROCESSOR_ARDUINODUE
////##########################################
#if defined(PROCESSOR_ARDUINODUE)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
// Led System
const int PIN_LEDSYSTEM           = 13;
const bool LEDSYSTEM_INVERTED     = false;
//
// SPI
// const int PIN_SPI_CS              = ;
//const int PIN_SPI_SCK             = 76;
//const int PIN_SPI_MISO            = 74;
//const int PIN_SPI_MOSI            = 75;
//
// I2C
const int PIN_I2C_SCL             = 21;
const int PIN_I2C_SDA             = 20;
//
#endif // PROCESSOR_ARDUINODUE
//
//##########################################
//  PROCESSOR_STM32F103C8
//##########################################
#if defined(PROCESSOR_STM32F103C8)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
// System Led 
const int PIN_LEDSYSTEM = PC13;
const bool LEDSYSTEM_INVERTED = true;
//
// SPI
const int PIN_SPI_CS    = 10;
const int PIN_SPI_CLK   = 11;
const int PIN_SPI_MISO  = 12;
const int PIN_SPI_MOSI  = 13;
//
// I2C
const int PIN_I2C_SCL = PB6;
const int PIN_I2C_SDA = PB7;
//
#endif // PROCESSOR_STM32F103C8
//
////
////##########################################
////  PROCESSOR_TEENSY32
////##########################################
#if defined(PROCESSOR_TEENSY32)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
// Single Led System (fixed)
const int PIN_LEDSYSTEM       = 13;
const bool LEDSYSTEM_INVERTED = false;
//
#endif // PROCESSOR_TEENSY32
//
////##########################################
////  PROCESSOR_TEENSY36
////##########################################
#if defined(PROCESSOR_TEENSY36)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
// Single Led System (fixed)
const int PIN_LEDSYSTEM   = 13;
//
#endif // PROCESSOR_TEENSY36
//
//###################################################
// Segment - Global Constant - Assignment
//###################################################
//
//--------------------------------
//  Section - Hardware
//--------------------------------
//
const int TIMERID_PULSEPERIOD = 3;
const int TIMERID_PULSEWIDTH  = 4;
//
//--------------------------------
//  Section - Initialisation
//--------------------------------
//
const UInt32 INIT_SYSTEMPERIOD_US   =    1000;  // [us]
const UInt32 INIT_SYSTEMCOUNTPRESET =       1;  // [1]
const UInt32 INIT_SYSTEMCOUNTACTUAL =       0;  // [1]
//
const UInt32 INIT_PULSEPERIOD_US    = 1000000;  // [us]
const UInt32 INIT_PULSEWIDTH_US     =  500000;  // [us]
const UInt32 INIT_PULSECOUNT        =       3;  // [1]
//
#endif // Defines_h





