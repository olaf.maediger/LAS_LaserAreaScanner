#include "Defines.h"
#include "Pulse.h"
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Serial.h"
#include "Led.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Instance - System 
//---------------------------------------------------
//
/*
GPW
SPW 1000000
BLS 2000 2

BLS 1000 2
SPW 1000000
PLP 2000 2000 2000 3

SPW 500000
PLP 2000 2000 1000 3
SPW 50000
PLP 2000 2000 100 3
PLI 2000 2000 1000000 3
*/
//
CError LASError;
CProcess LASProcess;
CCommand LASCommand;
//
//---------------------------------------------------
// Segment - Global Instance - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED);
//
//---------------------------------------------------
// Segment - Global Instance - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort und CommandPort!
// NC - CSerial SerialDebug(Serial);
// normal CSerial SerialCommand(Serial1);
CSerial SerialCommand(Serial);
//
//---------------------------------------------------
// Segment - Global Instance - Pulse
//---------------------------------------------------
//
CPulse PulseTrigger(TIMERID_PULSEPERIOD, TIMERID_PULSEWIDTH);
//
//---------------------------------------------------
// Segment - Forward-Declaration
//---------------------------------------------------
//

//
//---------------------------------------------------
// Segment - Setup
//---------------------------------------------------
//
void setup() 
{ //-------------------------------------------
  // Device - Led - LedSystem
  //-------------------------------------------
  LedSystem.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.Off();
    delay(40);
    LedSystem.On();
    delay(40);
  }
  LedSystem.Off();
  //-------------------------------------------
  // Device - Serial
  //-------------------------------------------
  // 
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_OFF);
  //-------------------------------------------
  // Device - Command
  //-------------------------------------------
  LASCommand.WriteProgramHeader(SerialCommand);
  LASCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //-------------------------------------------
  // Device - Process 
  //-------------------------------------------
  LASProcess.Open();
  LASProcess.SetState(spWelcome);
}
//
//---------------------------------------------------
// Segment - Loop
//---------------------------------------------------
//
void loop() 
{ 
  LASError.Handle(SerialCommand);    
  LASCommand.Handle(SerialCommand);
  LASProcess.Handle(SerialCommand);  
}




