#include "Led.h"
#include "Process.h"
#include "Command.h"
#include "Pulse.h"
//
extern CLed LedSystem;
extern CSerial SerialCommand;
extern CProcess LASProcess;
extern CCommand LASCommand;
extern CPulse PulseTrigger;
//
//---------------------------------------------------
// Segment - IrqCallback - PulseTimer
//---------------------------------------------------
//
// IrqHandler - Process
//
void IrqHandlerPulsePeriod(void)
{ // LedLaser/System - On
  LedSystem.On();
  // PulseTrigger - PulseWidth - Start
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidth(void)
{ // LedLaser/System - Off
  LedSystem.Off();
  // PulseTrigger - PulseWidth - Stop
  PulseTrigger.StopWidth();
  PulseTrigger.IncrementStopPulseStep();
}
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CProcess::CProcess()
{
  FState = spUndefined;
  FStateTick = 0;
  FTimeStampms = millis();
  FTimeMarkms = FTimeStampms;
  //
  FProcessTicksPreset  = INIT_PROCESSCOUNT;
  FProcessPeriodus = INIT_PROCESSPERIOD_US;
  FProcessTicksActual   = 0;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateProcess CProcess::GetState()
{
  return FState;
}

void CProcess::SetState(EStateProcess state)
{
  FStateTick = 0;
  FState = state;
  FSubstate = 0;
  // Message to Manager(PC):
  LASCommand.CallbackStateProcess(SerialCommand, FState, FSubstate);
}

void CProcess::SetState(EStateProcess state, UInt8 substate)
{
  if ((state != FState) || (substate != FSubstate))
  {     
    if (state != FState)
    {
      FStateTick = 0;
    }
    FState = state;
    FSubstate = substate;
    switch (FState)
    {
      case spStopProcessExecution:
        break;
    }
    // Message to Manager(PC):
    LASCommand.CallbackStateProcess(SerialCommand, FState, substate);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CProcess::Open()
{
  SetState(spIdle);
  return true;
}

Boolean CProcess::Close()
{
  SetState(spUndefined);
  return true;
}

//
//----------------------------------------------------------
//  Segment - Common
//----------------------------------------------------------
//
void CProcess::HandleUndefined(CSerial &serial)
{
  delay(1000);
}

void CProcess::HandleIdle(CSerial &serial)
{ // do nothing
  SetTimeStamp();
}

void CProcess::HandleWelcome(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:        
      SetTimeStamp();
      FStateTick++;
      break;
    case 1: case 3: case 5: 
      // if (330000 < GetTimeSpan())
      if (330 < GetTimeSpanms())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 2: case 4: case 6:  
      // if (330000 < GetTimeSpan())
      if (330 < GetTimeSpanms())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 7:  
      FStateTick++;      
      SetTimeStamp();      
      break;
    case 8:   
      // if (1000000 < GetTimeSpan())
      if (1000 < GetTimeSpanms())
      {   
        FStateTick++;
        SetTimeStamp();
      }
      break;
    case 9:   
      // if (1000000 < GetTimeSpan())
      if (1000 < GetTimeSpanms())
      {   
        FStateTick++;
        
        SetState(spIdle);
// alternative : 
//        UInt8 CL = 0;
//        UInt8 CH = 7;
//        LASProcess.SetChannelLow(CL);
//        LASProcess.SetChannelHigh(CH);
//        LASProcess.SetState(spRepeatAllTemperatures);
//        LASProcess.SetProcessIndex(0);
//        SetState(spRepeatAllTemperatures);
        SetTimeStamp();
      }
      break;
  }
}

void CProcess::HandleGetHelp(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProgramHeader(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSoftwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetHardwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessTicksPreset(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessTicksPreset(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessPeriodus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessPeriodus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleStopProcessExecution(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - LedSystem
//----------------------------------------------------------
//
void CProcess::LedSystemState()
{
  switch (LedSystem.GetState())
  {
    case slOn:
      break;
    case slOff:
      break;
    default:
      break;
  }
  SetState(spIdle, LedSystem.GetState());
}

void CProcess::HandleGetLedSystem(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOn(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOff(CSerial &serial)
{
  LedSystemState();
}

// 1807190809 - FSM corrected
void CProcess::HandleBlinkLedSystem(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0: // PulseH
      SetTimeStamp();
      LedSystem.On();
      SetState(spBlinkLedSystem, 1);
      FStateTick = 1;
      break;
    case 1: // PulseWidth
      if (PulseTrigger.GetPulseWidthus() < GetTimeSpanus())
      {
        LedSystem.Off();
        SetState(spBlinkLedSystem, 0);
        FStateTick = 2; // PulsePeriod
      }
      break;
    case 2: // PulsePeriod
      if (GetProcessPeriodus() < GetTimeSpanus())
      {
        FProcessTicksActual++;
        if (FProcessTicksActual < FProcessTicksPreset)
        {
          FStateTick = 0; // PulseH
        }
        else
        {
          SetState(spIdle);
        }
      }
      break;
  } 
}
////
////----------------------------------------------------------
//// Segment - Measurement - LaserPosition
////----------------------------------------------------------
//// Command: PulseLaserPosition PLP <x> <y> <p> <c> : Pulse Laser <x><y> <p>eriod <c>ount"
//// Command: Definition of XPositionActual, YPositionActual, ProcessPeriod, ProcessCount
//// 1805231334 - DelayMotion
//// 1807191143 - IrqTimer -> PulsePeriod & PulseWidt
//void CProcess::HandlePulseLaserPosition(CSerial &serial)
//{ 
//  const int STATE_POSITIONXY    = 0;
//  const int STATE_STARTTRIGGER  = 2;
//  const int STATE_PULSETRIGGER  = 3;
//  const int STATE_END           = 4;
//  //
//  SetTimeMark();
//  switch (FStateTick)
//  {
//    case STATE_POSITIONXY:
//      SetTimeStamp();
//      LedSystem.Off();
//      SetProcessStep(0);   
//      FStateTick = STATE_STARTTRIGGER;
//      break;
//    case STATE_STARTTRIGGER: 
//      PulseTrigger.Stop();
//      PulseTrigger.SetPulsePeriodus(1000 * GetProcessPeriod()); // us ???????????
//      PulseTrigger.SetPulseWidthus(GetPulseWidthus());
//      PulseTrigger.SetPulseCount(GetProcessCount());
//      PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
//      PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
//      // start Timer for second, .. Pulses:
//      PulseTrigger.Start();
//      // First Pulse starts from here:
//      IrqHandlerPulsePeriod();
//      FStateTick = STATE_PULSETRIGGER;
//      break;
//    case STATE_PULSETRIGGER: 
//      if (GetProcessCount() <= GetProcessStep())
//      {
//        FStateTick = STATE_END; 
//      }
//      break;
//    case STATE_END:
//      SetTimeStamp();
//      LedSystem.Off();
//      SetState(spIdle);
//      break;
//  }
//}
//
//void CProcess::HandleAbortLaserPosition(CSerial &serial)
//{  
//  PulseTrigger.Stop();
//  SetState(spIdle);
//}
//
//----------------------------------------------------------
// Segment - Measurement - PulseTiming
//----------------------------------------------------------
//
void CProcess::HandleGetPulsePeriodus(CSerial &serial)
{
  SetState(spIdle);
}
void CProcess::HandleSetPulsePeriodus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetPulseWidthus(CSerial &serial)
{
  SetState(spIdle);
}
void CProcess::HandleSetPulseWidthus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetPulseCount(CSerial &serial)
{
  SetState(spIdle);
}
void CProcess::HandleSetPulseCount(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - PulseSequence
//----------------------------------------------------------
// 
void CProcess::HandleStartPulseSequence(CSerial &serial)
{ 
  const int STATE_INITMOTION    = 0;
  const int STATE_POSITIONX     = 1;
  const int STATE_PULSEHIGH     = 2;
  const int STATE_PULSELOW      = 3;
  const int STATE_END           = 4;
  //  
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INITMOTION: 
      FStateTick = STATE_POSITIONX;
      LedSystem.Off();
      SetProcessTicksActual(0);
      SetTimeStamp();
      break;
    case STATE_POSITIONX: 
      if (GetProcessTicksActual() < GetProcessTicksPreset())
      {
        FStateTick = STATE_PULSEHIGH;
      }
      else
      {
        FStateTick = STATE_PULSELOW;
      }
      SetTimeStamp();
      break;
    case STATE_PULSEHIGH: 
      if (slOn != LedSystem.GetState()) 
      { 
        LedSystem.On();
        IncrementProcessTicksActual();
      }      
      if (PulseTrigger.GetPulseWidthus() <= GetTimeSpanus())      
      { 
        FStateTick = STATE_PULSELOW;
      }
      break;
    case STATE_PULSELOW: // more Pulses?
      if (slOff != LedSystem.GetState()) 
      { 
        LedSystem.Off();
      }
      if (GetProcessPeriodus() <= GetTimeSpanus())
      { 
        if (GetProcessTicksPreset() <= GetProcessTicksActual())
        { 
          FStateTick = STATE_END;
        }
        else
        { // more Pulses
          FStateTick = STATE_PULSEHIGH; 
        }
        SetTimeStamp(); 
      }      
      break;
    case STATE_END: 
      LedSystem.Off();
      SetState(spIdle);      
      SetTimeStamp(); 
      break;      
  }
}

void CProcess::HandleAbortPulseSequence(CSerial &serial)
{
  LedSystem.Off();
  SetState(spIdle);
  SetTimeStamp(); 
}
//
//----------------------------------------------------------
//  Segment - Collector
//----------------------------------------------------------
//
void CProcess::Handle(CSerial &serial)
{
  switch (LASProcess.GetState())
  { // Common
    case spIdle:
      HandleIdle(serial);
      break;
    case spWelcome:
      HandleWelcome(serial);
      break;
    case spGetHelp:
      HandleGetHelp(serial);
      break;
    case spGetProgramHeader:
      HandleGetProgramHeader(serial);
      break;
    case spGetSoftwareVersion:
      HandleGetSoftwareVersion(serial);
      break;
    case spGetHardwareVersion:
      HandleGetHardwareVersion(serial);
      break;
    case spGetProcessTicksPreset:
      HandleGetProcessTicksPreset(serial);
      break;
    case spSetProcessTicksPreset:
      HandleSetProcessTicksPreset(serial);
      break;
    case spGetProcessPeriod:
      HandleGetProcessPeriodus(serial);
      break;
    case spSetProcessPeriod:
      HandleSetProcessPeriodus(serial);
      break;
    case spStopProcessExecution:
      HandleStopProcessExecution(serial);
      break;
    // Measurement - PulseTiming
    case spGetPulsePeriod:
      HandleGetPulsePeriodus(serial);
      break;
    case spSetPulsePeriod:
      HandleSetPulsePeriodus(serial);
      break;
    case spGetPulseWidth:
      HandleGetPulseWidthus(serial);
      break;
    case spSetPulseWidth:
      HandleSetPulseWidthus(serial);
      break;
    case spGetPulseCount:
      HandleGetPulseCount(serial);
      break;
    case spSetPulseCount:
      HandleSetPulseCount(serial);
      break;
    // LedSystem
    case spGetLedSystem:
      HandleGetLedSystem(serial);
      break;
    case spLedSystemOn:
      HandleLedSystemOn(serial);
      break;
    case spLedSystemOff:
      HandleLedSystemOff(serial);
      break;
    case spBlinkLedSystem:
      HandleBlinkLedSystem(serial);
      break;
    // PulseSequence
    case spStartPulseSequence:
      HandleStartPulseSequence(serial);
      break;
    case spAbortPulseSequence:
      HandleAbortPulseSequence(serial);
      break;
    default: // spUndefined
      HandleUndefined(serial);
      break;
  }  
}
