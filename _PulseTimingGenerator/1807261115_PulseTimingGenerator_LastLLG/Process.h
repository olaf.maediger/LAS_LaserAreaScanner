#ifndef Process_h
#define Process_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
#include "Pulse.h"
//
#define MASK_STATEPROCESS "%s STP %i %i"
//
extern CPulse PulseTrigger;
//
enum EStateProcess
{ // Common
  spUndefined = -1,
  spIdle = 0,
  spWelcome = 1,
  spGetHelp = 2,
  spGetProgramHeader = 3,
  spGetSoftwareVersion = 4,
  spGetHardwareVersion = 5,
  spGetSystemPeriod = 6,
  spSetSystemPeriod = 7,
  spGetSystemCountPreset = 8,
  spSetSystemCountPreset = 9,
  spStopSystemExecution = 10,
  // PulseTiming
  spGetPulsePeriod = 11,
  spSetPulsePeriod = 12,
  spGetPulseWidth = 13,
  spSetPulseWidth = 14,
  spGetPulseCount = 15,
  spSetPulseCount = 16,
  // LedSystem
  spGetLedSystem = 17,
  spLedSystemOn = 18,  
  spLedSystemOff = 19,  
  spBlinkLedSystem = 20,  
  // PulseSequence
  spStartPulseSequence = 21,
  spAbortPulseSequence = 22,
  // PulseVector
  spStartPulseVector = 23,
  spAbortPulseVector = 24 
};
//
class CProcess
{ //
  //  Segment - Field
  //
  private:
  // Common
  EStateProcess FState;
  UInt8 FSubstate;
  UInt32 FStateTick;                  // [1]
  UInt32 FTimeStampms;                // [ms]
  UInt32 FTimeMarkms;                 // [ms]
  UInt64 FTimeStampus;                // [us]
  UInt64 FTimeMarkus;                 // [us]
  UInt32 FSystemPeriodus;             // [us] 
  UInt32 FSystemCountPreset;          // [1]
  UInt32 FSystemCountActual;          // [1]
  //  
  // Range
  // -> Pulse !!! UInt32 FPulseWidthus;               // [us]
  //  
  void LedSystemState();
  //
  public:
  CProcess();
  // Property
  EStateProcess GetState();
  void SetState(EStateProcess stateprocess);
  void SetState(EStateProcess stateprocess, UInt8 substate);
  // Measurement - Time
  // First: TimeStamp!!!
  inline void SetTimeStamp()
  {
    FTimeStampms = millis();
    FTimeMarkms = FTimeStampms;
    FTimeStampus = micros();
    FTimeMarkus = FTimeStampus;
  }
  // Later: TimeMarker!!!
  inline void SetTimeMark()
  {
    FTimeMarkms = millis();
    FTimeMarkus = micros();
  }
  inline UInt32 GetTimeSpanms()
  {
    return FTimeMarkms - FTimeStampms;
  }
  inline UInt32 GetTimeSpanus()
  {
    return FTimeMarkus - FTimeStampus;
  }

  inline void SetSystemCountPreset(UInt32 value)
  {
    FSystemCountPreset = value;
  }
  inline UInt32 GetSystemCountPreset(void)
  {
    return FSystemCountPreset;
  }

  inline void SetSystemCountActual(UInt32 value)
  {
    FSystemCountActual = value;
  }
  inline UInt32 GetSystemCountActual(void)
  {
    return FSystemCountActual;
  }

  inline Boolean IncrementSystemCountActual()
  {
    FSystemCountActual++;
    return (FSystemCountPreset <= FSystemCountActual);
  }

  inline UInt32 GetSystemPeriodus(void)
  {
    return FSystemPeriodus;
  }
  inline void SetSystemPeriodus(UInt32 periodus)
  {
    FSystemPeriodus = periodus;
  }

  // Management
  Boolean Open();
  Boolean Close();
  private:
  // System
  void HandleUndefined(CSerial &serial);
  void HandleIdle(CSerial &serial);
  void HandleWelcome(CSerial &serial);
  void HandleGetHelp(CSerial &serial);
  void HandleGetProgramHeader(CSerial &serial);
  void HandleGetSoftwareVersion(CSerial &serial);
  void HandleGetHardwareVersion(CSerial &serial);
  void HandleGetSystemPeriodus(CSerial &serial);
  void HandleSetSystemPeriodus(CSerial &serial);
  void HandleGetSystemCountPreset(CSerial &serial);
  void HandleSetSystemCountPreset(CSerial &serial);
  void HandleStopSystemExecution(CSerial &serial);
  // Measurement - PulseTiming
  void HandleGetPulsePeriodus(CSerial &serial); 
  void HandleSetPulsePeriodus(CSerial &serial);   
  void HandleGetPulseWidthus(CSerial &serial); 
  void HandleSetPulseWidthus(CSerial &serial);   
  void HandleGetPulseCount(CSerial &serial); 
  void HandleSetPulseCount(CSerial &serial);   
  // LedSystem
  void HandleGetLedSystem(CSerial &serial);
  void HandleLedSystemOn(CSerial &serial);
  void HandleLedSystemOff(CSerial &serial);
  void HandleBlinkLedSystem(CSerial &serial);
  // PulseSequence
  void HandleStartPulseSequence(CSerial &serial);
  void HandleAbortPulseSequence(CSerial &serial);
  // PulseVector
  void HandleStartPulseVector(CSerial &serial);
  void HandleAbortPulseVector(CSerial &serial);
  public:
  // Collector
  void Handle(CSerial &serial);
};
//
#endif // Process_h
