#include "Led.h"
#include "Process.h"
#include "Command.h"
#include "Pulse.h"
#include "RamBuffer.h"
//
extern CLed LedSystem;
extern CLed LedVector0;
extern CLed LedVector1;
extern CLed LedVector2;
extern CLed LedVector3;
extern CLed LedVector4;
extern CLed LedVector5;
extern CLed LedVector6;
extern CLed LedVector7;
extern CSerial SerialCommand;
extern CProcess PTGProcess;
extern CCommand PTGCommand;
extern CPulse PulseTrigger;
extern CRamBuffer RamBuffer;
//
//---------------------------------------------------
// Segment - IrqCallback - PulseTimer
//---------------------------------------------------
//
void IrqHandlerPulsePeriod(void)
{ // LedLaser/System - On
  LedSystem.On();
  // PulseTrigger - PulseWidth - Start
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidth(void)
{ // LedLaser/System - Off
  LedSystem.Off();
  // PulseTrigger - PulseWidth - Stop
  PulseTrigger.StopWidth();
  PulseTrigger.IncrementStopPulseStep();
}
//
//---------------------------------------------------
// Segment - IrqCallback - PulseVector
//---------------------------------------------------
//
void IrqHandlerVectorPeriod(void)
{ 
  // debug LedSystem.Off();
  LedVector0.Off();
  LedVector1.Off();
  LedVector2.Off();
  LedVector3.Off();
  LedVector4.Off();
  LedVector5.Off();
  LedVector6.Off();
  LedVector7.Off();
  UInt8 LevelLeds = RamBuffer.Read();
  // debug if (0 < (0x01 & LevelLeds)) { LedSystem.On(); }
  if (0 < (0x01 & LevelLeds)) { LedVector0.On(); }
  if (0 < (0x02 & LevelLeds)) { LedVector1.On(); }
  if (0 < (0x04 & LevelLeds)) { LedVector2.On(); }
  if (0 < (0x08 & LevelLeds)) { LedVector3.On(); }
  if (0 < (0x10 & LevelLeds)) { LedVector4.On(); }
  if (0 < (0x20 & LevelLeds)) { LedVector5.On(); }
  if (0 < (0x40 & LevelLeds)) { LedVector6.On(); }
  if (0 < (0x80 & LevelLeds)) { LedVector7.On(); }
  PulseTrigger.StartWidth();
}
//
void IrqHandlerVectorWidth(void)
{ 
  // debug LedSystem.Off();
  LedVector0.Off();
  LedVector1.Off();
  LedVector2.Off();
  LedVector3.Off();
  LedVector4.Off();
  LedVector5.Off();
  LedVector6.Off();
  LedVector7.Off();  
  PulseTrigger.StopWidth();
  PulseTrigger.IncrementStopPulseStep();
}
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CProcess::CProcess()
{
  FState = spUndefined;
  FStateTick = 0;
  FTimeStampms = millis();
  FTimeMarkms = FTimeStampms;
  //
  FSystemPeriodus = INIT_SYSTEMPERIOD_US;
  FSystemCountPreset = INIT_SYSTEMCOUNTPRESET;
  FSystemCountActual = INIT_SYSTEMCOUNTACTUAL;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateProcess CProcess::GetState()
{
  return FState;
}

void CProcess::SetState(EStateProcess state)
{
  FStateTick = 0;
  FState = state;
  FSubstate = 0;
  // Message to Manager(PC):
  PTGCommand.CallbackStateProcess(SerialCommand, FState, FSubstate);
}

void CProcess::SetState(EStateProcess state, UInt8 substate)
{
  if ((state != FState) || (substate != FSubstate))
  {     
    if (state != FState)
    {
      FStateTick = 0;
    }
    FState = state;
    FSubstate = substate;
    switch (FState)
    {
      case spStopSystemExecution:
        break;
    }
    // Message to Manager(PC):
    PTGCommand.CallbackStateProcess(SerialCommand, FState, substate);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CProcess::Open()
{
  SetState(spIdle);
  return true;
}

Boolean CProcess::Close()
{
  SetState(spUndefined);
  return true;
}

//
//----------------------------------------------------------
//  Segment - Common
//----------------------------------------------------------
//
void CProcess::HandleUndefined(CSerial &serial)
{
  delay(1000);
}

void CProcess::HandleIdle(CSerial &serial)
{ // do nothing
  SetTimeStamp();
}

void CProcess::HandleWelcome(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:        
      SetTimeStamp();
      FStateTick = 1;
      break;
    case 1: 
      if (666 < GetTimeSpanms())
      {          
        SetTimeStamp();
        FStateTick = 2;
        SetState(spIdle);
      }
      break;
  }
}

void CProcess::HandleGetHelp(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProgramHeader(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSoftwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetHardwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSystemPeriodus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetSystemPeriodus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSystemCountPreset(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetSystemCountPreset(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleStopSystemExecution(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - PulseTiming
//----------------------------------------------------------
//
void CProcess::HandleGetPulsePeriodus(CSerial &serial)
{
  SetState(spIdle);
}
void CProcess::HandleSetPulsePeriodus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetPulseWidthus(CSerial &serial)
{
  SetState(spIdle);
}
void CProcess::HandleSetPulseWidthus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetPulseCount(CSerial &serial)
{
  SetState(spIdle);
}
void CProcess::HandleSetPulseCount(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - LedSystem
//----------------------------------------------------------
//
void CProcess::LedSystemState()
{
  switch (LedSystem.GetState())
  {
    case slOn:
      break;
    case slOff:
      break;
    default:
      break;
  }
  SetState(spIdle, LedSystem.GetState());
}

void CProcess::HandleGetLedSystem(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOn(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOff(CSerial &serial)
{
  LedSystemState();
}

// 1807241529 - New WITHOUT PulseIRQ!!!
void CProcess::HandleBlinkLedSystem(CSerial &serial)
{
  const int STATE_INIT        = 0;
  const int STATE_PULSEHIGH   = 1;
  const int STATE_PULSEWIDTH  = 2;
  const int STATE_PULSEPERIOD = 3;
  const int STATE_END         = 4;
  //
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT:
      PulseTrigger.SetPulseStep(0);
      PulseTrigger.SetPulsePeriodus(max(1, PulseTrigger.GetPulsePeriodus()));
      PulseTrigger.SetPulseWidthus(max(1, PulseTrigger.GetPulseWidthus()));
      if (0 < PulseTrigger.GetPulseCount())
      {
        LedSystem.Off();
        SetState(spBlinkLedSystem, 0);
        FStateTick = STATE_PULSEHIGH;
      }
      else
      {
        FStateTick = STATE_END;
      }
      break;
    case STATE_PULSEHIGH: 
      SetTimeStamp();
      LedSystem.On();
      SetState(spBlinkLedSystem, 1);
      FStateTick = STATE_PULSEWIDTH;
      break;
    case STATE_PULSEWIDTH:
      if (PulseTrigger.GetPulseWidthus() < GetTimeSpanus())
      {
        LedSystem.Off();
        SetState(spBlinkLedSystem, 0);
        FStateTick = STATE_PULSEPERIOD;
      }
      break;
    case STATE_PULSEPERIOD:
      if (PulseTrigger.GetPulsePeriodus() < GetTimeSpanus())
      {
        if (PulseTrigger.IncrementStopPulseStep() < PulseTrigger.GetPulseCount())
        {
          FStateTick = STATE_PULSEHIGH;
        }
        else
        {
          FStateTick = STATE_END;
        }
      }
      break;
    case STATE_END:
      SetState(spIdle);
      break;
  } 
}
//
//----------------------------------------------------------
// Segment - PulseSequence
//----------------------------------------------------------
// 
void CProcess::HandleStartPulseSequence(CSerial &serial)
{ 
  const int STATE_INIT  = 0;
  const int STATE_START = 1;
  const int STATE_BUSY  = 2;
  const int STATE_END   = 3;
  //  
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT: 
      SetTimeStamp();
      LedSystem.Off();
      PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
      PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
      FStateTick = STATE_START;
      break;
    case STATE_START:
      if (0 < PulseTrigger.GetPulseCount())
      {
        FStateTick = STATE_BUSY;
        PulseTrigger.Start();        
        IrqHandlerPulsePeriod();
      }
      else
      {
        FStateTick = STATE_END;
      }
      break;
    case STATE_BUSY: 
      if (PulseTrigger.GetPulseCount() <= PulseTrigger.GetPulseStep())
      {
        FStateTick = STATE_END;        
      }
      break;
    case STATE_END:      
      PulseTrigger.Stop();
      SetState(spIdle);      
      SetTimeStamp(); 
      break;      
  }
}

void CProcess::HandleAbortPulseSequence(CSerial &serial)
{
  LedSystem.Off();
  SetState(spIdle);
  SetTimeStamp(); 
}
//
//----------------------------------------------------------
// Segment - PulseVector
//----------------------------------------------------------
// 
void CProcess::HandleStartPulseVector(CSerial &serial)
{ 
  const int STATE_INIT  = 0;
  const int STATE_START = 1;
  const int STATE_BUSY  = 2;
  const int STATE_END   = 3;
  //  
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INIT: 
      SetTimeStamp();
      PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerVectorPeriod);
      PulseTrigger.SetInterruptHandlerWidth(IrqHandlerVectorWidth);
      RamBuffer.ResetReadIndex();
      FStateTick = STATE_START;
      break;
    case STATE_START:
      if (0 < PulseTrigger.GetPulseCount())
      {
        FStateTick = STATE_BUSY;
        PulseTrigger.Start();
        IrqHandlerVectorPeriod();
      }
      else
      {
        FStateTick = STATE_END;
      }
      break;
    case STATE_BUSY: 
      if (PulseTrigger.GetPulseCount() <= PulseTrigger.GetPulseStep())
      {
        FStateTick = STATE_END;        
      }
      break;
    case STATE_END:      
      PulseTrigger.Stop();
      SetState(spIdle);      
      SetTimeStamp(); 
      break;      
  }
}

void CProcess::HandleAbortPulseVector(CSerial &serial)
{
  SetState(spIdle);
  SetTimeStamp(); 
}
//
//----------------------------------------------------------
//  Segment - Collector
//----------------------------------------------------------
//
void CProcess::Handle(CSerial &serial)
{
  switch (GetState())
  { // Common
    case spIdle:
      HandleIdle(serial);
      break;
    case spWelcome:
      HandleWelcome(serial);
      break;
    case spGetHelp:
      HandleGetHelp(serial);
      break;
    case spGetProgramHeader:
      HandleGetProgramHeader(serial);
      break;
    case spGetSoftwareVersion:
      HandleGetSoftwareVersion(serial);
      break;
    case spGetHardwareVersion:
      HandleGetHardwareVersion(serial);
      break;
    case spGetSystemPeriod:
      HandleGetSystemPeriodus(serial);
      break;
    case spSetSystemPeriod:
      HandleSetSystemPeriodus(serial);
      break;
    case spGetSystemCountPreset:
      HandleGetSystemCountPreset(serial);
      break;
    case spSetSystemCountPreset:
      HandleSetSystemCountPreset(serial);
      break;
    case spStopSystemExecution:
      HandleStopSystemExecution(serial);
      break;
    // Measurement - PulseTiming
    case spGetPulsePeriod:
      HandleGetPulsePeriodus(serial);
      break;
    case spSetPulsePeriod:
      HandleSetPulsePeriodus(serial);
      break;
    case spGetPulseWidth:
      HandleGetPulseWidthus(serial);
      break;
    case spSetPulseWidth:
      HandleSetPulseWidthus(serial);
      break;
    case spGetPulseCount:
      HandleGetPulseCount(serial);
      break;
    case spSetPulseCount:
      HandleSetPulseCount(serial);
      break;
    // LedSystem
    case spGetLedSystem:
      HandleGetLedSystem(serial);
      break;
    case spLedSystemOn:
      HandleLedSystemOn(serial);
      break;
    case spLedSystemOff:
      HandleLedSystemOff(serial);
      break;
    case spBlinkLedSystem:
      HandleBlinkLedSystem(serial);
      break;
    // PulseSequence
    case spStartPulseSequence:
      HandleStartPulseSequence(serial);
      break;
    case spAbortPulseSequence:
      HandleAbortPulseSequence(serial);
      break;
    // PulseVector
    case spStartPulseVector:
      HandleStartPulseVector(serial);
      break;
    case spAbortPulseVector:
      HandleAbortPulseVector(serial);
      break;
    default: // spUndefined
      HandleUndefined(serial);
      break;
  }  
}
