//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "Led.h"
//
CLed::CLed(int pin)
{
  FPin = pin;
  FInverted = false;
  FState = slUndefined;
}

CLed::CLed(int pin, bool inverted)
{
  FPin = pin;
  FInverted = inverted;
  FState = slUndefined;
}

EStateLed CLed::GetState()
{
  return FState;
}

Boolean CLed::Open()
{
  pinMode(FPin, OUTPUT);
  Off();
  return true;
}

Boolean CLed::Close()
{
  Off();
  pinMode(FPin, INPUT);
  FState = slUndefined;
  return true;
}

void CLed::On()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FState = slOn;
}

void CLed::Off()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FState = slOff;
}



