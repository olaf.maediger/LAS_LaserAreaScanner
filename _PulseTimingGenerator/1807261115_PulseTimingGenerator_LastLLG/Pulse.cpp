#include "Defines.h"
#include "Pulse.h"
//
CPulse::CPulse(int channelperiod, int channelwidth)
{
  FChannelPeriod = channelperiod;
  FChannelWidth = channelwidth;
  FPTimerPulsePeriod = new CIrqTimer(FChannelPeriod);
  FPTimerPulseWidth = new CIrqTimer(FChannelWidth);
  //
  SetPulsePeriodus(INIT_PULSEPERIOD_US);
  SetPulseWidthus(INIT_PULSEWIDTH_US);
  SetPulseCount(INIT_PULSECOUNT);
}
//
//-------------------------------------------------------------------
//  Segment - Period
//-------------------------------------------------------------------
//
void CPulse::SetInterruptHandlerPeriod(void (*pirqhandlerperiod)(void))
{
  FPIrqHandlerPeriod = pirqhandlerperiod;
  FPTimerPulsePeriod->SetInterruptHandler(FPIrqHandlerPeriod);  
}

long unsigned CPulse::GetPulsePeriodus()
{
  return FPulsePeriodus;
}

void CPulse::SetPulsePeriodus(long unsigned pulseperiodus)
{
  FPulsePeriodus = pulseperiodus;
  FPTimerPulsePeriod->SetPeriodus(FPulsePeriodus);  
}

void CPulse::Start()
{
  FPTimerPulsePeriod->SetPeriodus(FPulsePeriodus);
  FPulseStep = 0;
  FPTimerPulsePeriod->Start();  
}

void CPulse::Stop()
{
  FPTimerPulseWidth->Stop();
  FPTimerPulsePeriod->Stop();
}

long unsigned CPulse::GetPulseStep()
{
  return FPulseStep;
}
void CPulse::SetPulseStep(long unsigned pulsestep)
{ 
  FPulseStep = pulsestep;
}

long unsigned CPulse::GetPulseCount()
{
  return FPulseCount;
}
void CPulse::SetPulseCount(long unsigned pulsecount)
{ 
  FPulseCount = pulsecount;
}

long unsigned CPulse::IncrementStopPulseStep()
{
  if (FPulseStep < FPulseCount)
  {
    FPulseStep++;
  }
  if (FPulseStep == FPulseCount)
  {
    Stop();
  }
  return FPulseStep;
}
//
//-------------------------------------------------------------------
//  Segment - Width
//-------------------------------------------------------------------
//
void CPulse::SetInterruptHandlerWidth(void (*pirqhandlerwidth)(void))
{
  FPIrqHandlerWidth = pirqhandlerwidth;
  FPTimerPulseWidth->SetInterruptHandler(FPIrqHandlerWidth);
}

long unsigned CPulse::GetPulseWidthus()
{
  return FPulseWidthus;
}

void CPulse::SetPulseWidthus(long unsigned pulsewidthus)
{
  FPulseWidthus = pulsewidthus;
  FPTimerPulseWidth->SetPeriodus(FPulseWidthus);
}

void CPulse::StartWidth()
{
  FPTimerPulseWidth->SetPeriodus(FPulseWidthus);
  FPTimerPulseWidth->SetPulseCount(WIDTH_PULSECOUNT);
  FPTimerPulseWidth->Start(); 
}

void CPulse::StopWidth()
{
  FPTimerPulseWidth->Stop(); 
}



