//
//--------------------------------
//  Library RamBuffer
//--------------------------------
//
#ifndef RamBuffer_h
#define RamBuffer_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
enum EStateRamBuffer
{
  rbError = -2,
  rbUndefined = -1,
  rbInitialize = 0,
  rbIdle = 1,
  rbRead = 2,
  rbWrite = 3
};
//
class CRamBuffer
{
  private:
  EStateRamBuffer FState;
  UInt16 FSize;
  UInt8* FPBuffer;
  UInt16 FReadIndex;
  //  
  public:
  CRamBuffer(UInt16 ramsize); 
  //
  Boolean Open();
  Boolean Close();
  EStateRamBuffer GetState();
  //
  inline void ResetReadIndex(void)
  {
    FReadIndex = 0;
  }
  inline UInt8 Read(UInt16 index)
  {
    FReadIndex = index;
    return Read();
  }
  inline UInt8 Read()
  {
    UInt8 Value = FPBuffer[FReadIndex];
    FReadIndex++;
    if (FReadIndex == FSize)
    {
      FReadIndex = 0;
    }
    return Value;
  }
  inline void Write(UInt16 index, UInt8 value)
  {    
    FPBuffer[index] = value;
  } 
};
//
#endif // RamBuffer_h
