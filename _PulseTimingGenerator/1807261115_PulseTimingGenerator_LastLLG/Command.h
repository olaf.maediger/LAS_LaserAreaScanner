#ifndef Command_h
#define Command_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
#include "Process.h"
//
#define ARGUMENT_PROJECT    "PTG - PulseTimingGenerator"
#define ARGUMENT_SOFTWARE   "00V03"
#define ARGUMENT_DATE       "180725"
#define ARGUMENT_TIME       "1209"
#define ARGUMENT_AUTHOR     "OM"
#define ARGUMENT_PORT       "SerialCommand (Serial-USB)"
#define ARGUMENT_PARAMETER  "115200, N, 8, 1"
//
#ifdef PROCESSOR_ARDUINOUNO
#define ARGUMENT_HARDWARE "ArduinoUnoR3"
#endif
#ifdef PROCESSOR_ARDUINOMEGA 
#define ARGUMENT_HARDWARE "ArduinoMega328"
#endif
#ifdef PROCESSOR_ARDUINODUE
#define ARGUMENT_HARDWARE "ArduinoDue3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
// 
// HELP_COMMON
#define SHORT_H     "H"
#define SHORT_GPH   "GPH"
#define SHORT_GSV   "GSV"
#define SHORT_GHV   "GHV"
#define SHORT_GSP   "GSP"
#define SHORT_SSP   "SSP"
#define SHORT_GSC   "GSC"
#define SHORT_SSC   "SSC"
#define SHORT_A     "A"
//
// HELP_PULSETIMING
#define SHORT_GPW   "GPW"
#define SHORT_SPW   "SPW"
#define SHORT_GPP   "GPP"
#define SHORT_SPP   "SPP"
#define SHORT_GPC   "GPC"
#define SHORT_SPC   "SPC"
//
// HELP_LEDSYSTEM
#define SHORT_GLS   "GLS"
#define SHORT_LSH   "LSH"
#define SHORT_LSL   "LSL"
#define SHORT_BLS   "BLS"
//
// HELP_PULSESEQUENCE
#define SHORT_SPS   "SPS"
#define SHORT_APS   "APS"
//
// HELP_PULSEVECTOR
#define SHORT_SPV   "SPV"
#define SHORT_APV   "APV"
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HEADER = 12;
//
#define TITLE_LINE            "--------------------------------------------------"
#define MASK_PROJECT          "- Project:   %-32s -"
#define MASK_SOFTWARE         "- Version:   %-32s -"
#define MASK_HARDWARE         "- Hardware:  %-32s -"
#define MASK_DATE             "- Date:      %-32s -"
#define MASK_TIME             "- Time:      %-32s -"
#define MASK_AUTHOR           "- Author:    %-32s -"
#define MASK_PORT             "- Port:      %-32s -"
#define MASK_PARAMETER        "- Parameter: %-32s -"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HELP = 25;
// 0
#define HELP_COMMON               " Help (Common):"
#define MASK_H                    " %-3s : This Help"
#define MASK_GPH                  " %-3s : Get Program Header"
#define MASK_GSV                  " %-3s : Get Software-Version"
#define MASK_GHV                  " %-3s : Get Hardware-Version"
#define MASK_GSP                  " %-3s : Get System Period{us}"
#define MASK_SSP                  " %-3s <p> : Set System <p>eriod{us}"
#define MASK_GSC                  " %-3s : Get System Count{1}"
#define MASK_SSC                  " %-3s <t> : Set System <c>Count{1}"
#define MASK_A                    " %-1s : Abort System Execution"
// 10
#define HELP_PULSETIMING          " Help (PulseTiming):"
#define MASK_GPP                  " %-3s : Get PulsePeriod{us}"
#define MASK_SPP                  " %-3s <p> : Set Pulse<p>eriod{us}"
#define MASK_GPW                  " %-3s : Get PulseWidth{us}"
#define MASK_SPW                  " %-3s <w> : Set Pulse<w>idth{us}"
#define MASK_GPC                  " %-3s : Get PulseCount{1}"
#define MASK_SPC                  " %-3s <c> : Set Pulse<c>ount{1}"
// 17
#define HELP_LEDSYSTEM            " Help (LedSystem):"
#define MASK_GLS                  " %-3s : Get State LedSystem"
#define MASK_LSH                  " %-3s : Switch LedSystem On"
#define MASK_LSL                  " %-3s : Switch LedSystem Off"
#define MASK_BLS                  " %-3s : Blink LedSystem"
// 22
#define HELP_PULSESEQUENCE        " Help (PulseSequence):"
#define MASK_SPS                  " %-3s : Start Pulse Sequence"
#define MASK_APS                  " %-3s : Abort Pulse Sequence"
// 25
#define HELP_PULSEVECTOR          " Help (PulseVector):"
#define MASK_SPV                  " %-3s : Start Pulse Vector"
#define MASK_APV                  " %-3s : Abort Pulse Vector"
// 28
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_SOFTWAREVERSION = 1;
//
#define MASK_SOFTWAREVERSION      " Software-Version PTG%s"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HARDWAREVERSION = 1;
//
#define MASK_HARDWAREVERSION      " Hardware-Version %s"
//
//-----------------------------------------------------------------------------------------
//
// Command-Parameter
#define COUNT_RXDPARAMETERS 5
#define SIZE_RXDPARAMETER   8
//
//-----------------------------------------------------------------------------------------
//
class CCommand
{
  private:
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Character FRxdCommandLine[SIZE_RXDBUFFER];
  PCharacter FPRxdCommand;
  PCharacter FPRxdParameters[COUNT_RXDPARAMETERS];
  Int16 FRxdParameterCount = 0;
  Int16 FRxdBufferIndex = 0;

  public:
  CCommand();
  ~CCommand();
  //
  PCharacter GetPRxdParameters(UInt8 index)
  {
    return FPRxdParameters[index];
  }
  PCharacter GetTxdBuffer()
  {
    return FTxdBuffer;
  }
  PCharacter GetPRxdCommand()
  {
    return FPRxdCommand;
  }
  //
  void ZeroRxdBuffer();
  void ZeroTxdBuffer();
  void ZeroRxdCommandLine();
  //
  //  Segment - Execution - Basic Output
  void WriteProgramHeader(CSerial &serial);
  void WriteSoftwareVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
  void WriteHelp(CSerial &serial);
  //
  //  Segment - Execution - Common
  void ExecuteGetHelp(CSerial &serial);
  void ExecuteGetProgramHeader(CSerial &serial);
  void ExecuteGetSoftwareVersion(CSerial &serial);
  void ExecuteGetHardwareVersion(CSerial &serial);
  void ExecuteGetSystemPeriodus(CSerial &serial);
  void ExecuteSetSystemPeriodus(CSerial &serial);
  void ExecuteGetSystemCountPreset(CSerial &serial);
  void ExecuteSetSystemCountPreset(CSerial &serial);
  void ExecuteAbortAll(CSerial &serial);
  //
  //  Segment - Execution - PulseRange
  void ExecuteGetPulsePeriodus(CSerial &serial); 
  void ExecuteSetPulsePeriodus(CSerial &serial); 
  void ExecuteGetPulseWidthus(CSerial &serial); 
  void ExecuteSetPulseWidthus(CSerial &serial);
  void ExecuteGetPulseCount(CSerial &serial);
  void ExecuteSetPulseCount(CSerial &serial);
  //
  //  Segment - Execution - LedSystem
  void ExecuteGetLedSystem(CSerial &serial);
  void ExecuteLedSystemOn(CSerial &serial);
  void ExecuteLedSystemOff(CSerial &serial);
  void ExecuteBlinkLedSystem(CSerial &serial);
  //
  //  Segment - Execution - PulseSequence
  void ExecuteStartPulseSequence(CSerial &serial);
  void ExecuteAbortPulseSequence(CSerial &serial);
  //
  //  Segment - Execution - PulseVector
  void ExecuteStartPulseVector(CSerial &serial);
  void ExecuteAbortPulseVector(CSerial &serial);
  //  
  // Main
  void Init();
  Boolean DetectRxdLine(CSerial &serial);
  Boolean Analyse(CSerial &serial);
  void CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate);
  Boolean Execute(CSerial &serial);
  Boolean Handle(CSerial &serial);
};
//
#endif // Command_h
