//
//--------------------------------
//  Library Led
//--------------------------------
//
#ifndef Led_h
#define Led_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
enum EStateLed
{
  slUndefined = -1,
  slOff = 0,
  slOn = 1
};
//
class CLed
{
  private:
  int FPin;
  bool FInverted;
  EStateLed FState;
  //  
  public:
  CLed(int pin);
  CLed(int pin, bool inverted);
  //
  EStateLed GetState();
  Boolean Open();
  Boolean Close();
  void On();
  void Off();
};
//
#endif
