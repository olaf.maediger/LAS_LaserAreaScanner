//
//--------------------------------
//  Library RamBuffer
//--------------------------------
//
#include "RamBuffer.h"
//
CRamBuffer::CRamBuffer(UInt16 ramsize)
{
  FState = rbUndefined;
  FSize = ramsize;
  FPBuffer = NULL;
}

EStateRamBuffer CRamBuffer::GetState()
{
  return FState;
}

Boolean CRamBuffer::Open()
{
  FState = rbIdle;
  FPBuffer = new UInt8[FSize];
  ResetReadIndex();
  return true;
}

Boolean CRamBuffer::Close()
{
  FState = rbUndefined;
  FPBuffer = NULL;
  return true;
}



