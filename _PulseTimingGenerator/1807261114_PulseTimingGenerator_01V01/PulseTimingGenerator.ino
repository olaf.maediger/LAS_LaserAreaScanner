#include "Defines.h"
#include "Pulse.h"
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Serial.h"
#include "Led.h"
#include "RamBuffer.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Instance - System 
//---------------------------------------------------
//
/*
GPW
SPW 1000000
BLS 2000 2

BLS 1000 2
SPW 1000000
PLP 2000 2000 2000 3

SPW 500000
PLP 2000 2000 1000 3
SPW 50000
PLP 2000 2000 100 3
PLI 2000 2000 1000000 3
*/
//
CError PTGError;
CProcess PTGProcess;
CCommand PTGCommand;
//
//---------------------------------------------------
// Segment - Global Instance - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED);
CLed LedVector0(PIN_LEDVECTOR0, LEDVECTOR0_INVERTED);
CLed LedVector1(PIN_LEDVECTOR1, LEDVECTOR1_INVERTED);
CLed LedVector2(PIN_LEDVECTOR2, LEDVECTOR2_INVERTED);
CLed LedVector3(PIN_LEDVECTOR3, LEDVECTOR3_INVERTED);
CLed LedVector4(PIN_LEDVECTOR4, LEDVECTOR4_INVERTED);
CLed LedVector5(PIN_LEDVECTOR5, LEDVECTOR5_INVERTED);
CLed LedVector6(PIN_LEDVECTOR6, LEDVECTOR6_INVERTED);
CLed LedVector7(PIN_LEDVECTOR7, LEDVECTOR7_INVERTED);
//
//---------------------------------------------------
// Segment - Global Instance - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort und CommandPort!
// NC - CSerial SerialDebug(Serial);
// normal CSerial SerialCommand(Serial1);
CSerial SerialCommand(Serial);
//
//---------------------------------------------------
// Segment - Global Instance - Pulse
//---------------------------------------------------
//
CPulse PulseTrigger(TIMERID_PULSEPERIOD, TIMERID_PULSEWIDTH);
//
//---------------------------------------------------
// Segment - Global Instance - RamBuffer
//---------------------------------------------------
//
CRamBuffer RamBuffer(SIZE_RAMBUFFER);
//
//---------------------------------------------------
// Segment - Forward-Declaration
//---------------------------------------------------
//

//
//---------------------------------------------------
// Segment - Setup
//---------------------------------------------------
//
void setup() 
{ //-------------------------------------------
  // Device - Led 
  //-------------------------------------------
  LedSystem.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.Off();
    delay(40);
    LedSystem.On();
    delay(40);
  }
  LedSystem.Off();
  //
  LedVector0.Open();
  LedVector1.Open();
  LedVector2.Open();
  LedVector3.Open();
  LedVector4.Open();
  LedVector5.Open();
  LedVector6.Open();
  LedVector7.Open();
  LedVector0.On();
  LedVector1.On();
  LedVector2.On();
  LedVector3.On();
  LedVector4.On();
  LedVector5.On();
  LedVector6.On();
  LedVector7.On();
  delay(1000);
  LedVector0.Off();
  LedVector1.Off();
  LedVector2.Off();
  LedVector3.Off();
  LedVector4.Off();
  LedVector5.Off();
  LedVector6.Off();
  LedVector7.Off();  
  //-------------------------------------------
  // Device - RamBuffer
  //-------------------------------------------
  RamBuffer.Open();
  RamBuffer.Write(0, 0x55);
  RamBuffer.Write(1, 0xAA);
  RamBuffer.Write(2, 0x55);
  RamBuffer.Write(3, 0xAA);
  RamBuffer.Write(4, 0x55);
  RamBuffer.Write(5, 0xAA);
  RamBuffer.Write(6, 0x55);
  RamBuffer.Write(7, 0xAA);
  //-------------------------------------------
  // Device - Serial
  //-------------------------------------------
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_OFF);
  //-------------------------------------------
  // Device - Command
  //-------------------------------------------
  PTGCommand.WriteProgramHeader(SerialCommand);
  PTGCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //-------------------------------------------
  // Device - Process 
  //-------------------------------------------
  PTGProcess.Open();
  PTGProcess.SetState(spWelcome);  
}
//
//---------------------------------------------------
// Segment - Loop
//---------------------------------------------------
//
void loop() 
{ 
  PTGError.Handle(SerialCommand);    
  PTGCommand.Handle(SerialCommand);
  PTGProcess.Handle(SerialCommand);  
}




