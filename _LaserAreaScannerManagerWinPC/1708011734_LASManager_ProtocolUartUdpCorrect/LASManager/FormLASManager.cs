﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using Network;
using NetworkDevice;
using UdpTextDevice;
using CommandProtocol;
using CPLaserAreaScanner;
//
namespace LASManager
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormClient : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICETYPE = "LASManager";
    private const String INIT_DEVICENAME = "OMDevelopDebugUdpTextBinaryClient";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    private CDeviceParameter FDeviceParameter;
    private CUdpTextDeviceClient FUdpTextDeviceClient;
    //
    private CCommandList FCommandList;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      //####################################################################################
      // Init - Instance - Client
      //####################################################################################
      //
      FDeviceParameter = new CDeviceParameter();
      FDeviceParameter.Type = INIT_DEVICETYPE;
      FDeviceParameter.Name = INIT_DEVICENAME;
      FDeviceParameter.ID = Guid.NewGuid();
      IPAddress IPALocal;
      CNetwork.FindIPAddressLocal(out IPALocal);
      FDeviceParameter.IPA = IPALocal;
      IPAddress IPATarget;
      CNetwork.FindIPAddressLocalBroadcast(out IPATarget);
      //
      // UdpTextDeviceClient
      FUdpTextDeviceClient = new CUdpTextDeviceClient();
      FUdpTextDeviceClient.SetNotifier(FUCNotifier);
      FUdpTextDeviceClient.SetOnDatagramTransmitted(UdpTextDeviceClientOnDatagramTransmitted);
      FUdpTextDeviceClient.SetOnDatagramReceived(UdpTextDeviceClientOnDatagramReceived);
      FUdpTextDeviceClient.SetSourceIpAddress(IPALocal);
      FUdpTextDeviceClient.SetSourceIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_CLIENT_TRANSMITTER);
      FUdpTextDeviceClient.SetTargetIpAddress(IPATarget);
      FUdpTextDeviceClient.SetTargetIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_CLIENT_RECEIVER);
      FUdpTextDeviceClient.Open();
      //
      FCommandList = new CCommandList();
      // -> Main! FCommandList.SetUdpTextDeviceClient(FUdpTextDeviceClient);
      FCommandList.SetNotifier(FUCNotifier);
      FCommandList.SetOnExecutionStart(CommandListOnExecutionStart);
      FCommandList.SetOnExecutionBusy(CommandListOnExecutionBusy);
      FCommandList.SetOnExecutionEnd(CommandListOnExecutionEnd);
      FCommandList.SetOnExecutionAbort(CommandListOnExecutionAbort);
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      // Start Parallel-Application: LanUdpTextServer.exe
      // Process.Start("LanUdpTextServer.exe");
      Process.Start("LanUartUdpTextServer.exe");
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FUdpTextDeviceClient.Close();
      // -> LanUdpTextServer
      //Process[] ProcessesLanUdpServer = Process.GetProcessesByName("LanUdpTextServer");
      //foreach (Process ProcessLanUdpServer in ProcessesLanUdpServer)
      //{
      //  ProcessLanUdpServer.Kill();
      //}
      Process[] PS = Process.GetProcessesByName("LanUartUdpTextServer");
      foreach (Process P in PS)
      {
        P.Kill();
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //#########################################################################
    //  Segment - Callback - UdpTextDeviceClient
    //#########################################################################
    //
    private void UdpTextDeviceClientOnDatagramTransmitted(IPEndPoint ipendpointtarget,
                                                           Byte[] datagram, Int32 size)
    {
      String SDatagram = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
      String Line = String.Format("UdpTxd[{0}]:<{1}>", CNetwork.IPEndPointToText(ipendpointtarget), SDatagram);
      FUCNotifier.Write(Line);
    }

    private void UdpTextDeviceClientOnDatagramReceived(IPEndPoint ipendpointsource,
                                                        Byte[] datagram, Int32 size)
    {
      String SDatagram = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
      String Line = String.Format("UdpRxd[{0}]:<{1}>", CNetwork.IPEndPointToText(ipendpointsource), SDatagram);
      FUCNotifier.Write(Line);
      //
      if (0 < FCommandList.Count)
      {
        CCommand Command = FCommandList.Peek();
        if (Command is CCommand)
        {
          Command.SignalTextReceived();
          //String CommandText = Command.GetCommandText();
          // later!!! if ('!' == SDatagram[0])
          //if ('@' == SDatagram[0])
          //if ('#' == SDatagram[0])
          //if (SDatagram.Find('>'))
          //{

          //  String RxResponse = SDatagram.Remove(0, 1);
          //  if (CommandText.Contains(RxResponse))
          //  {
          //    Command.SignalTextReceived();
          //    RxResponse = RxResponse.Substring(3);
          //    FUCNotifier.Write(String.Format("<<<{0}>>>", RxResponse));
          //  }
          //}
        }
      }
    }
    //
    //#########################################################################
    //  Segment - Callback - CommandList
    //#########################################################################
    //
    private delegate void CBCommandListOnExecutionStart(RTaskData data);
    private void CommandListOnExecutionStart(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionStart CB = new CBCommandListOnExecutionStart(CommandListOnExecutionStart);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionStart", data.Name));
        CCommand Command = FCommandList.Peek();
        if (Command is CCommand)
        {
          String CommandText = Command.GetCommandText();
          FUdpTextDeviceClient.AddMessage(CommandText);
        }
      }
    }
    private delegate Boolean CBCommandListOnExecutionBusy(RTaskData data);
    private Boolean CommandListOnExecutionBusy(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionBusy CB = new CBCommandListOnExecutionBusy(CommandListOnExecutionBusy);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionBusy", data.Name));
      }
      return false;
    }
    private delegate void CBCommandListOnExecutionEnd(RTaskData data);
    private void CommandListOnExecutionEnd(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionEnd CB = new CBCommandListOnExecutionEnd(CommandListOnExecutionEnd);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionEnd", data.Name));
      }
    }
    private delegate void CBCommandListOnExecutionAbort(RTaskData data);
    private void CommandListOnExecutionAbort(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionAbort CB = new CBCommandListOnExecutionAbort(CommandListOnExecutionAbort);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionAbort", data.Name));
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          //tbcMain.SelectedIndex = 1;
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:        
          return true;
        case 6:
          //FUdpTextDeviceClient.Preset("GSV");
          return true;
        case 7:
          return true;
        case 8:
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! First (Client -> Server)");
          return true;
        case 9:
          //FUdpTextDeviceClient.Preset("GHV");
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! Second (Client -> Server)");
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }



    private void btnGetSoftwareVersion_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetSoftwareVersion());
      FCommandList.Execute();
    }

    private void btnGetHardwareVersion_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetHardwareVersion());
      FCommandList.Execute();
    }

    private void btnGetHelp_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetHelp());
      FCommandList.Execute();
    }

    private void btnGetProgramHeader_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetProgramHeader());
      FCommandList.Execute();
    }

    private void btnSetLed_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CSetLed());
      FCommandList.Execute();
    }

    private void btnGetLed_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetLed());
      FCommandList.Execute();
    }

  }
}

