﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
//
using UCNotifier;
using Task;
using Network;
//
namespace UdpTextTransfer
{
  public class CUdpTextTransmitter : CUdpTextBase
  {
    //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    protected DOnDatagramTransmitted FOnDatagramTransmitted;
    protected CStringQueue FStringQueue;
    
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public CUdpTextTransmitter()
      : base()
    {
      FTask = null;
      FOnDatagramTransmitted = null;
      FStringQueue = new CStringQueue();
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public void SetOnDatagramTransmitted(DOnDatagramTransmitted value)
    {
      FOnDatagramTransmitted = value;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //
    private void TaskOnExecutionStart(RTaskData data)
    {
    }

    private Boolean TaskOnExecutionBusy(RTaskData data)
    {
      try
      {
        while (0 < FStringQueue.Count)
        {
          String Message = FStringQueue.Dequeue();
          Byte[] ByteVector = Encoding.ASCII.GetBytes(Message);
          if (ByteVector is Byte[])
          {
            IPEndPoint IPETarget = new IPEndPoint(FIpEndPoint.Address, FIpEndPoint.Port);
            if (FOnDatagramTransmitted is DOnDatagramTransmitted)
            {
              FOnDatagramTransmitted(IPETarget, ByteVector, ByteVector.Length);
            }
            FUdpClient.Send(ByteVector, ByteVector.Length);
          }
        }
        return false;
      }
      catch (Exception)
      {
        FNotifier.Error("UdpTextTransmitter", 1, "Invalid UdpDatagram");
        return false;
      }
    }

    private void TaskOnExecutionEnd(RTaskData data)
    {
    }

    private void TaskOnExecutionAbort(RTaskData data)
    {
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Method
    //-------------------------------------------------------------------
    //
    public override Boolean Open(IPAddress ipaddresstarget, UInt16 ipporttarget)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        FStringQueue = new CStringQueue();
        FIpEndPoint = new IPEndPoint(ipaddresstarget, ipporttarget);
        FUdpClient = new UdpClient();
        FUdpClient.Connect(FIpEndPoint);
        //
        FTask = null;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Open(String sipendpointtarget)
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        FStringQueue = new CStringQueue();
        CNetwork.TextToIPEndPoint(sipendpointtarget, out FIpEndPoint);
        FUdpClient = new UdpClient();
        // open UdpConnection
        FUdpClient.Connect(FIpEndPoint);
        //
        FTask = null;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public override Boolean Close()
    {
      try
      {
        if (FTask is CTask)
        {
          FTask.Abort();
          FTask = null;
        }
        if (FUdpClient is UdpClient)
        { // close UdpConnection
          FStringQueue = null;
          FUdpClient.Close();
          FUdpClient = null;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean AddMessage(String message)
    {
      try
      {
        if (0 == message.Length)
        {
          return true;
        }
        FStringQueue.Enqueue(message);
        if (FTask is CTask)
        {
          if (FTask.IsFinished())
          {
            FTask = null;
          }
        }
        if (!(FTask is CTask))
        {
          FTask = new CTask("UdpTextTransmitter",
                            TaskOnExecutionStart,
                            TaskOnExecutionBusy,
                            TaskOnExecutionEnd,
                            TaskOnExecutionAbort);
          FTask.Start();
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
  }
}


//!!!FUdpTextDatagramContainerTransmit = new CUdpTextDatagramContainerTransmit();

// first time without container! protected CUdpTextDatagramContainerTransmit FUdpTextDatagramContainerTransmit;
//!!!!!!!
//public Boolean PresetUdpTextDatagramContainerTransmit(CUdpTextDatagramContainerTransmit container)
//{
//  try
//  {//!!!
//    //foreach (CUdpTextDatagramListTransmit UTDLT in container)
//    //{
//    //  FUdpTextDatagramContainerTransmit.Add(UTDLT);
//    //}
//    if (FTask is CTask)
//    {
//      if (FTask.IsFinished())
//      {
//        FTask = null;
//      }
//    }
//    if (!(FTask is CTask))
//    {
//      FTask = new CTask("UdpTextTransmitter",
//                        TaskOnExecutionStart,
//                        TaskOnExecutionBusy,
//                        TaskOnExecutionEnd,
//                        TaskOnExecutionAbort);
//      FTask.Start();
//    }
//    return true;
//  }
//  catch (Exception)
//  {
//    return false;
//  }
//}

//private Boolean TaskOnExecutionBusy(ref RTaskData data)
//{
//  try
//  {
//    Boolean Ready = false;
//    do
//    {
//      if (0 < FUdpTextDatagramContainerTransmit.Count)
//      {
//        CUdpTextDatagramListTransmit UTDLT = FUdpTextDatagramContainerTransmit[0];
//        if (0 < UTDLT.Count)
//        {
//          CUdpTextDatagramTransmit UTDT = (CUdpTextDatagramTransmit)UTDLT[0];
//          Byte[] ByteVector = UTDT.GetByteVector();
//          if (ByteVector is Byte[])
//          {
//            IPEndPoint IPETarget = new IPEndPoint(FIpEndPoint.Address, FIpEndPoint.Port);
//            if (FOnDatagramTransmitted is DOnDatagramTransmitted)
//            {
//              FOnDatagramTransmitted(IPETarget, ByteVector, ByteVector.Length);
//            }
//            FUdpClient.Send(ByteVector, ByteVector.Length);
//            Thread.Sleep(100);
//          }
//          UTDLT.Remove(UTDT);
//        }
//        else
//        {
//          FUdpTextDatagramContainerTransmit.Remove(UTDLT);
//        }
//      }
//      else
//      {
//        Ready = true;
//      }
//    }
//    while (!Ready);
//    return false;
//  }
//  catch (Exception)
//  {
//    FNotifier.Error("UdpTextTransmitter", 1, "Invalid UdpDatagram");
//    return false;
//  }
//}