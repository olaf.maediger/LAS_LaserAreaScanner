﻿namespace UCTextEditor
{
	partial class CUCEditTextEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.components = new System.ComponentModel.Container();
      this.DialogSelectFont = new System.Windows.Forms.FontDialog();
      this.cmsEditor = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mitText = new System.Windows.Forms.ToolStripMenuItem();
      this.mitLoad = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSave = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSaveAs = new System.Windows.Forms.ToolStripMenuItem();
      this.mitShow = new System.Windows.Forms.ToolStripMenuItem();
      this.mitParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSelectFont = new System.Windows.Forms.ToolStripMenuItem();
      this.mitShowLegend = new System.Windows.Forms.ToolStripMenuItem();
      this.mitProjection = new System.Windows.Forms.ToolStripMenuItem();
      this.mitDisplayProjection = new System.Windows.Forms.ToolStripMenuItem();
      this.DialogLoadText = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveText = new System.Windows.Forms.SaveFileDialog();
      this.stsLegend = new System.Windows.Forms.StatusStrip();
      this.stsRow = new System.Windows.Forms.ToolStripStatusLabel();
      this.stsCol = new System.Windows.Forms.ToolStripStatusLabel();
      this.stsFileName = new System.Windows.Forms.ToolStripStatusLabel();
      this.stsWorkingDirectory = new System.Windows.Forms.ToolStripStatusLabel();
      this.pnlFont = new System.Windows.Forms.Panel();
      this.label5 = new System.Windows.Forms.Label();
      this.nudFontOpacity = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.nudFontPositionY = new System.Windows.Forms.NumericUpDown();
      this.nudFontPositionX = new System.Windows.Forms.NumericUpDown();
      this.btnSelectFontColor = new System.Windows.Forms.Button();
      this.btnSelectFont = new System.Windows.Forms.Button();
      this.btnHideEditorFont = new System.Windows.Forms.Button();
      this.btnClosePanelFont = new System.Windows.Forms.Button();
      this.pnlParameter = new System.Windows.Forms.Panel();
      this.btnHideEditorParameter = new System.Windows.Forms.Button();
      this.btnClosePanelParameter = new System.Windows.Forms.Button();
      this.lblTabulator = new System.Windows.Forms.Label();
      this.cbxShowLegend = new System.Windows.Forms.CheckBox();
      this.cbxIsReadOnly = new System.Windows.Forms.CheckBox();
      this.scbTabulator = new System.Windows.Forms.HScrollBar();
      this.pnlSave = new System.Windows.Forms.Panel();
      this.btnHideEditorSave = new System.Windows.Forms.Button();
      this.btnClosePanelSave = new System.Windows.Forms.Button();
      this.btnClearSaveList = new System.Windows.Forms.Button();
      this.btnSaveToFile = new System.Windows.Forms.Button();
      this.cbxSaveToFile = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.pnlLoad = new System.Windows.Forms.Panel();
      this.btnHideEditorLoad = new System.Windows.Forms.Button();
      this.btnClosePanelLoad = new System.Windows.Forms.Button();
      this.btnClearLoadList = new System.Windows.Forms.Button();
      this.btnLoadFromFile = new System.Windows.Forms.Button();
      this.cbxLoadFromFile = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.DialogSelectFontColor = new System.Windows.Forms.ColorDialog();
      this.FRTBEditor = new UCTextEditor.CUCRichTextBox();
      this.cmsEditor.SuspendLayout();
      this.stsLegend.SuspendLayout();
      this.pnlFont.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudFontOpacity)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudFontPositionY)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudFontPositionX)).BeginInit();
      this.pnlParameter.SuspendLayout();
      this.pnlSave.SuspendLayout();
      this.pnlLoad.SuspendLayout();
      this.SuspendLayout();
      // 
      // cmsEditor
      // 
      this.cmsEditor.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitText,
            this.mitLoad,
            this.mitSave,
            this.mitSaveAs,
            this.mitShow,
            this.mitParameter,
            this.mitSelectFont,
            this.mitShowLegend,
            this.mitProjection,
            this.mitDisplayProjection});
      this.cmsEditor.Name = "cmsEditor";
      this.cmsEditor.Size = new System.Drawing.Size(153, 246);
      // 
      // mitText
      // 
      this.mitText.Enabled = false;
      this.mitText.Name = "mitText";
      this.mitText.Size = new System.Drawing.Size(152, 22);
      this.mitText.Text = "- Text -";
      // 
      // mitLoad
      // 
      this.mitLoad.Name = "mitLoad";
      this.mitLoad.Size = new System.Drawing.Size(152, 22);
      this.mitLoad.Text = "Load";
      this.mitLoad.Click += new System.EventHandler(this.mitLoadText_Click);
      // 
      // mitSave
      // 
      this.mitSave.Name = "mitSave";
      this.mitSave.Size = new System.Drawing.Size(152, 22);
      this.mitSave.Text = "Save";
      this.mitSave.Click += new System.EventHandler(this.mitSaveText_Click);
      // 
      // mitSaveAs
      // 
      this.mitSaveAs.Name = "mitSaveAs";
      this.mitSaveAs.Size = new System.Drawing.Size(152, 22);
      this.mitSaveAs.Text = "Save as";
      this.mitSaveAs.Click += new System.EventHandler(this.mitSaveTextAs_Click);
      // 
      // mitShow
      // 
      this.mitShow.Enabled = false;
      this.mitShow.Name = "mitShow";
      this.mitShow.Size = new System.Drawing.Size(152, 22);
      this.mitShow.Text = "- Show -";
      // 
      // mitParameter
      // 
      this.mitParameter.Name = "mitParameter";
      this.mitParameter.Size = new System.Drawing.Size(152, 22);
      this.mitParameter.Text = "Parameter";
      this.mitParameter.Click += new System.EventHandler(this.mitShowParameter_Click);
      // 
      // mitSelectFont
      // 
      this.mitSelectFont.Name = "mitSelectFont";
      this.mitSelectFont.Size = new System.Drawing.Size(152, 22);
      this.mitSelectFont.Text = "Font";
      this.mitSelectFont.Click += new System.EventHandler(this.mitSelectFont_Click);
      // 
      // mitShowLegend
      // 
      this.mitShowLegend.Name = "mitShowLegend";
      this.mitShowLegend.Size = new System.Drawing.Size(152, 22);
      this.mitShowLegend.Text = "Legend";
      this.mitShowLegend.Click += new System.EventHandler(this.mitShowLegend_Click);
      // 
      // mitProjection
      // 
      this.mitProjection.Enabled = false;
      this.mitProjection.Name = "mitProjection";
      this.mitProjection.Size = new System.Drawing.Size(152, 22);
      this.mitProjection.Text = "- Projection -";
      this.mitProjection.Visible = false;
      // 
      // mitDisplayProjection
      // 
      this.mitDisplayProjection.Name = "mitDisplayProjection";
      this.mitDisplayProjection.Size = new System.Drawing.Size(152, 22);
      this.mitDisplayProjection.Text = "Display";
      this.mitDisplayProjection.Visible = false;
      this.mitDisplayProjection.Click += new System.EventHandler(this.mitDisplayProjection_Click);
      // 
      // DialogLoadText
      // 
      this.DialogLoadText.DefaultExt = "cmt.txt";
      this.DialogLoadText.Filter = "Commentfiles (*.cmt.txt)|*.cmt.txt|Textfiles (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogLoadText.Title = "Load Text";
      // 
      // DialogSaveText
      // 
      this.DialogSaveText.DefaultExt = "cmt.txt";
      this.DialogSaveText.Filter = "Commentfiles (*.cmt.txt)|*.cmt.txt|Textfiles (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogSaveText.Title = "Save Text";
      // 
      // stsLegend
      // 
      this.stsLegend.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stsRow,
            this.stsCol,
            this.stsFileName,
            this.stsWorkingDirectory});
      this.stsLegend.Location = new System.Drawing.Point(0, 573);
      this.stsLegend.Name = "stsLegend";
      this.stsLegend.Size = new System.Drawing.Size(614, 24);
      this.stsLegend.SizingGrip = false;
      this.stsLegend.TabIndex = 5;
      this.stsLegend.Resize += new System.EventHandler(this.stsEditor_Resize);
      // 
      // stsRow
      // 
      this.stsRow.BackColor = System.Drawing.SystemColors.Info;
      this.stsRow.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)));
      this.stsRow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.stsRow.Name = "stsRow";
      this.stsRow.Size = new System.Drawing.Size(46, 19);
      this.stsRow.Text = "Row: 0";
      // 
      // stsCol
      // 
      this.stsCol.BackColor = System.Drawing.SystemColors.Info;
      this.stsCol.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)));
      this.stsCol.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.stsCol.Name = "stsCol";
      this.stsCol.Size = new System.Drawing.Size(41, 19);
      this.stsCol.Text = "Col: 0";
      // 
      // stsFileName
      // 
      this.stsFileName.BackColor = System.Drawing.SystemColors.Info;
      this.stsFileName.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)));
      this.stsFileName.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.stsFileName.Name = "stsFileName";
      this.stsFileName.Size = new System.Drawing.Size(43, 19);
      this.stsFileName.Text = "<file>";
      this.stsFileName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // stsWorkingDirectory
      // 
      this.stsWorkingDirectory.AutoSize = false;
      this.stsWorkingDirectory.BackColor = System.Drawing.SystemColors.Info;
      this.stsWorkingDirectory.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)));
      this.stsWorkingDirectory.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.stsWorkingDirectory.Name = "stsWorkingDirectory";
      this.stsWorkingDirectory.Size = new System.Drawing.Size(163, 19);
      this.stsWorkingDirectory.Text = "<workingdirectory>";
      this.stsWorkingDirectory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // pnlFont
      // 
      this.pnlFont.BackColor = System.Drawing.SystemColors.Control;
      this.pnlFont.Controls.Add(this.label5);
      this.pnlFont.Controls.Add(this.nudFontOpacity);
      this.pnlFont.Controls.Add(this.label4);
      this.pnlFont.Controls.Add(this.label3);
      this.pnlFont.Controls.Add(this.nudFontPositionY);
      this.pnlFont.Controls.Add(this.nudFontPositionX);
      this.pnlFont.Controls.Add(this.btnSelectFontColor);
      this.pnlFont.Controls.Add(this.btnSelectFont);
      this.pnlFont.Controls.Add(this.btnHideEditorFont);
      this.pnlFont.Controls.Add(this.btnClosePanelFont);
      this.pnlFont.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlFont.Location = new System.Drawing.Point(0, 87);
      this.pnlFont.Name = "pnlFont";
      this.pnlFont.Size = new System.Drawing.Size(614, 29);
      this.pnlFont.TabIndex = 14;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(366, 8);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(60, 13);
      this.label5.TabIndex = 23;
      this.label5.Text = "Opacity [%]";
      // 
      // nudFontOpacity
      // 
      this.nudFontOpacity.Location = new System.Drawing.Point(428, 5);
      this.nudFontOpacity.Name = "nudFontOpacity";
      this.nudFontOpacity.Size = new System.Drawing.Size(42, 20);
      this.nudFontOpacity.TabIndex = 22;
      this.nudFontOpacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudFontOpacity.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(286, 8);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(31, 13);
      this.label4.TabIndex = 21;
      this.label4.Text = "Y [%]";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(166, 8);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(71, 13);
      this.label3.TabIndex = 20;
      this.label3.Text = "Position X [%]";
      // 
      // nudFontPositionY
      // 
      this.nudFontPositionY.Location = new System.Drawing.Point(319, 5);
      this.nudFontPositionY.Name = "nudFontPositionY";
      this.nudFontPositionY.Size = new System.Drawing.Size(42, 20);
      this.nudFontPositionY.TabIndex = 19;
      this.nudFontPositionY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudFontPositionY.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
      // 
      // nudFontPositionX
      // 
      this.nudFontPositionX.Location = new System.Drawing.Point(239, 5);
      this.nudFontPositionX.Name = "nudFontPositionX";
      this.nudFontPositionX.Size = new System.Drawing.Size(42, 20);
      this.nudFontPositionX.TabIndex = 18;
      this.nudFontPositionX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudFontPositionX.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
      // 
      // btnSelectFontColor
      // 
      this.btnSelectFontColor.Location = new System.Drawing.Point(92, 3);
      this.btnSelectFontColor.Name = "btnSelectFontColor";
      this.btnSelectFontColor.Size = new System.Drawing.Size(71, 23);
      this.btnSelectFontColor.TabIndex = 17;
      this.btnSelectFontColor.Text = "FontColor";
      this.btnSelectFontColor.UseVisualStyleBackColor = true;
      this.btnSelectFontColor.Click += new System.EventHandler(this.btnSelectFontColor_Click);
      // 
      // btnSelectFont
      // 
      this.btnSelectFont.Location = new System.Drawing.Point(16, 3);
      this.btnSelectFont.Name = "btnSelectFont";
      this.btnSelectFont.Size = new System.Drawing.Size(71, 23);
      this.btnSelectFont.TabIndex = 16;
      this.btnSelectFont.Text = "Font";
      this.btnSelectFont.UseVisualStyleBackColor = true;
      this.btnSelectFont.Click += new System.EventHandler(this.btnSelectFont_Click);
      // 
      // btnHideEditorFont
      // 
      this.btnHideEditorFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Bold);
      this.btnHideEditorFont.Location = new System.Drawing.Point(1, 0);
      this.btnHideEditorFont.Name = "btnHideEditorFont";
      this.btnHideEditorFont.Size = new System.Drawing.Size(13, 15);
      this.btnHideEditorFont.TabIndex = 15;
      this.btnHideEditorFont.Text = "X";
      this.btnHideEditorFont.UseVisualStyleBackColor = true;
      // 
      // btnClosePanelFont
      // 
      this.btnClosePanelFont.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Bold);
      this.btnClosePanelFont.Location = new System.Drawing.Point(1, 14);
      this.btnClosePanelFont.Name = "btnClosePanelFont";
      this.btnClosePanelFont.Size = new System.Drawing.Size(13, 15);
      this.btnClosePanelFont.TabIndex = 8;
      this.btnClosePanelFont.Text = "X";
      this.btnClosePanelFont.UseVisualStyleBackColor = true;
      // 
      // pnlParameter
      // 
      this.pnlParameter.BackColor = System.Drawing.SystemColors.Control;
      this.pnlParameter.Controls.Add(this.btnHideEditorParameter);
      this.pnlParameter.Controls.Add(this.btnClosePanelParameter);
      this.pnlParameter.Controls.Add(this.lblTabulator);
      this.pnlParameter.Controls.Add(this.cbxShowLegend);
      this.pnlParameter.Controls.Add(this.cbxIsReadOnly);
      this.pnlParameter.Controls.Add(this.scbTabulator);
      this.pnlParameter.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlParameter.Location = new System.Drawing.Point(0, 58);
      this.pnlParameter.Name = "pnlParameter";
      this.pnlParameter.Size = new System.Drawing.Size(614, 29);
      this.pnlParameter.TabIndex = 13;
      // 
      // btnHideEditorParameter
      // 
      this.btnHideEditorParameter.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Bold);
      this.btnHideEditorParameter.Location = new System.Drawing.Point(1, 0);
      this.btnHideEditorParameter.Name = "btnHideEditorParameter";
      this.btnHideEditorParameter.Size = new System.Drawing.Size(13, 15);
      this.btnHideEditorParameter.TabIndex = 15;
      this.btnHideEditorParameter.Text = "X";
      this.btnHideEditorParameter.UseVisualStyleBackColor = true;
      // 
      // btnClosePanelParameter
      // 
      this.btnClosePanelParameter.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Bold);
      this.btnClosePanelParameter.Location = new System.Drawing.Point(1, 14);
      this.btnClosePanelParameter.Name = "btnClosePanelParameter";
      this.btnClosePanelParameter.Size = new System.Drawing.Size(13, 15);
      this.btnClosePanelParameter.TabIndex = 8;
      this.btnClosePanelParameter.Text = "X";
      this.btnClosePanelParameter.UseVisualStyleBackColor = true;
      // 
      // lblTabulator
      // 
      this.lblTabulator.AutoSize = true;
      this.lblTabulator.Location = new System.Drawing.Point(183, 8);
      this.lblTabulator.Name = "lblTabulator";
      this.lblTabulator.Size = new System.Drawing.Size(61, 13);
      this.lblTabulator.TabIndex = 3;
      this.lblTabulator.Text = "Tabulator []";
      // 
      // cbxShowLegend
      // 
      this.cbxShowLegend.AutoSize = true;
      this.cbxShowLegend.Location = new System.Drawing.Point(95, 6);
      this.cbxShowLegend.Name = "cbxShowLegend";
      this.cbxShowLegend.Size = new System.Drawing.Size(92, 17);
      this.cbxShowLegend.TabIndex = 7;
      this.cbxShowLegend.Text = "Show Legend";
      this.cbxShowLegend.UseVisualStyleBackColor = true;
      // 
      // cbxIsReadOnly
      // 
      this.cbxIsReadOnly.AutoSize = true;
      this.cbxIsReadOnly.Location = new System.Drawing.Point(18, 6);
      this.cbxIsReadOnly.Name = "cbxIsReadOnly";
      this.cbxIsReadOnly.Size = new System.Drawing.Size(76, 17);
      this.cbxIsReadOnly.TabIndex = 6;
      this.cbxIsReadOnly.Text = "Read Only";
      this.cbxIsReadOnly.UseVisualStyleBackColor = true;
      // 
      // scbTabulator
      // 
      this.scbTabulator.LargeChange = 2;
      this.scbTabulator.Location = new System.Drawing.Point(251, 7);
      this.scbTabulator.Maximum = 32;
      this.scbTabulator.Minimum = 1;
      this.scbTabulator.Name = "scbTabulator";
      this.scbTabulator.Size = new System.Drawing.Size(157, 16);
      this.scbTabulator.TabIndex = 4;
      this.scbTabulator.Value = 4;
      // 
      // pnlSave
      // 
      this.pnlSave.BackColor = System.Drawing.SystemColors.Control;
      this.pnlSave.Controls.Add(this.btnHideEditorSave);
      this.pnlSave.Controls.Add(this.btnClosePanelSave);
      this.pnlSave.Controls.Add(this.btnClearSaveList);
      this.pnlSave.Controls.Add(this.btnSaveToFile);
      this.pnlSave.Controls.Add(this.cbxSaveToFile);
      this.pnlSave.Controls.Add(this.label2);
      this.pnlSave.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlSave.Location = new System.Drawing.Point(0, 29);
      this.pnlSave.Name = "pnlSave";
      this.pnlSave.Size = new System.Drawing.Size(614, 29);
      this.pnlSave.TabIndex = 12;
      // 
      // btnHideEditorSave
      // 
      this.btnHideEditorSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Bold);
      this.btnHideEditorSave.Location = new System.Drawing.Point(1, 0);
      this.btnHideEditorSave.Name = "btnHideEditorSave";
      this.btnHideEditorSave.Size = new System.Drawing.Size(13, 15);
      this.btnHideEditorSave.TabIndex = 15;
      this.btnHideEditorSave.Text = "X";
      this.btnHideEditorSave.UseVisualStyleBackColor = true;
      // 
      // btnClosePanelSave
      // 
      this.btnClosePanelSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Bold);
      this.btnClosePanelSave.Location = new System.Drawing.Point(1, 14);
      this.btnClosePanelSave.Name = "btnClosePanelSave";
      this.btnClosePanelSave.Size = new System.Drawing.Size(13, 15);
      this.btnClosePanelSave.TabIndex = 13;
      this.btnClosePanelSave.Text = "X";
      this.btnClosePanelSave.UseVisualStyleBackColor = true;
      // 
      // btnClearSaveList
      // 
      this.btnClearSaveList.Location = new System.Drawing.Point(412, 3);
      this.btnClearSaveList.Name = "btnClearSaveList";
      this.btnClearSaveList.Size = new System.Drawing.Size(44, 23);
      this.btnClearSaveList.TabIndex = 12;
      this.btnClearSaveList.Text = "Clear";
      this.btnClearSaveList.UseVisualStyleBackColor = true;
      // 
      // btnSaveToFile
      // 
      this.btnSaveToFile.Location = new System.Drawing.Point(16, 2);
      this.btnSaveToFile.Name = "btnSaveToFile";
      this.btnSaveToFile.Size = new System.Drawing.Size(93, 23);
      this.btnSaveToFile.TabIndex = 11;
      this.btnSaveToFile.Text = "Save to File";
      this.btnSaveToFile.UseVisualStyleBackColor = true;
      // 
      // cbxSaveToFile
      // 
      this.cbxSaveToFile.BackColor = System.Drawing.SystemColors.Info;
      this.cbxSaveToFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxSaveToFile.FormattingEnabled = true;
      this.cbxSaveToFile.Location = new System.Drawing.Point(161, 4);
      this.cbxSaveToFile.Name = "cbxSaveToFile";
      this.cbxSaveToFile.Size = new System.Drawing.Size(247, 21);
      this.cbxSaveToFile.Sorted = true;
      this.cbxSaveToFile.TabIndex = 6;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(111, 7);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(49, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "Filename";
      // 
      // pnlLoad
      // 
      this.pnlLoad.BackColor = System.Drawing.SystemColors.Control;
      this.pnlLoad.Controls.Add(this.btnHideEditorLoad);
      this.pnlLoad.Controls.Add(this.btnClosePanelLoad);
      this.pnlLoad.Controls.Add(this.btnClearLoadList);
      this.pnlLoad.Controls.Add(this.btnLoadFromFile);
      this.pnlLoad.Controls.Add(this.cbxLoadFromFile);
      this.pnlLoad.Controls.Add(this.label1);
      this.pnlLoad.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlLoad.Location = new System.Drawing.Point(0, 0);
      this.pnlLoad.Name = "pnlLoad";
      this.pnlLoad.Size = new System.Drawing.Size(614, 29);
      this.pnlLoad.TabIndex = 11;
      // 
      // btnHideEditorLoad
      // 
      this.btnHideEditorLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Bold);
      this.btnHideEditorLoad.Location = new System.Drawing.Point(1, 0);
      this.btnHideEditorLoad.Name = "btnHideEditorLoad";
      this.btnHideEditorLoad.Size = new System.Drawing.Size(13, 15);
      this.btnHideEditorLoad.TabIndex = 15;
      this.btnHideEditorLoad.Text = "X";
      this.btnHideEditorLoad.UseVisualStyleBackColor = true;
      // 
      // btnClosePanelLoad
      // 
      this.btnClosePanelLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 5.25F, System.Drawing.FontStyle.Bold);
      this.btnClosePanelLoad.Location = new System.Drawing.Point(1, 14);
      this.btnClosePanelLoad.Name = "btnClosePanelLoad";
      this.btnClosePanelLoad.Size = new System.Drawing.Size(13, 15);
      this.btnClosePanelLoad.TabIndex = 14;
      this.btnClosePanelLoad.Text = "X";
      this.btnClosePanelLoad.UseVisualStyleBackColor = true;
      // 
      // btnClearLoadList
      // 
      this.btnClearLoadList.Location = new System.Drawing.Point(412, 3);
      this.btnClearLoadList.Name = "btnClearLoadList";
      this.btnClearLoadList.Size = new System.Drawing.Size(44, 23);
      this.btnClearLoadList.TabIndex = 10;
      this.btnClearLoadList.Text = "Clear";
      this.btnClearLoadList.UseVisualStyleBackColor = true;
      // 
      // btnLoadFromFile
      // 
      this.btnLoadFromFile.Location = new System.Drawing.Point(16, 2);
      this.btnLoadFromFile.Name = "btnLoadFromFile";
      this.btnLoadFromFile.Size = new System.Drawing.Size(93, 23);
      this.btnLoadFromFile.TabIndex = 9;
      this.btnLoadFromFile.Text = "Load from File";
      this.btnLoadFromFile.UseVisualStyleBackColor = true;
      // 
      // cbxLoadFromFile
      // 
      this.cbxLoadFromFile.BackColor = System.Drawing.SystemColors.Info;
      this.cbxLoadFromFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxLoadFromFile.FormattingEnabled = true;
      this.cbxLoadFromFile.Location = new System.Drawing.Point(161, 4);
      this.cbxLoadFromFile.Name = "cbxLoadFromFile";
      this.cbxLoadFromFile.Size = new System.Drawing.Size(247, 21);
      this.cbxLoadFromFile.Sorted = true;
      this.cbxLoadFromFile.TabIndex = 6;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(111, 7);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(49, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "Filename";
      // 
      // DialogSelectFontColor
      // 
      this.DialogSelectFontColor.AnyColor = true;
      this.DialogSelectFontColor.Color = System.Drawing.Color.DarkGray;
      // 
      // FRTBEditor
      // 
      this.FRTBEditor.AcceptsTab = true;
      this.FRTBEditor.ContextMenuStrip = this.cmsEditor;
      this.FRTBEditor.Font = new System.Drawing.Font("Courier New", 9.75F);
      this.FRTBEditor.Location = new System.Drawing.Point(18, 145);
      this.FRTBEditor.Name = "FRTBEditor";
      this.FRTBEditor.Size = new System.Drawing.Size(581, 400);
      this.FRTBEditor.TabIndex = 15;
      this.FRTBEditor.Text = "";
      // 
      // CUCEditTextEditor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FRTBEditor);
      this.Controls.Add(this.pnlFont);
      this.Controls.Add(this.pnlParameter);
      this.Controls.Add(this.pnlSave);
      this.Controls.Add(this.pnlLoad);
      this.Controls.Add(this.stsLegend);
      this.Name = "CUCEditTextEditor";
      this.Size = new System.Drawing.Size(614, 597);
      this.Load += new System.EventHandler(this.CUCEditTextEditor_Load);
      this.Resize += new System.EventHandler(this.CUCEditTextEditor_Resize);
      this.cmsEditor.ResumeLayout(false);
      this.stsLegend.ResumeLayout(false);
      this.stsLegend.PerformLayout();
      this.pnlFont.ResumeLayout(false);
      this.pnlFont.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudFontOpacity)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudFontPositionY)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudFontPositionX)).EndInit();
      this.pnlParameter.ResumeLayout(false);
      this.pnlParameter.PerformLayout();
      this.pnlSave.ResumeLayout(false);
      this.pnlSave.PerformLayout();
      this.pnlLoad.ResumeLayout(false);
      this.pnlLoad.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

		}

		#endregion

    private System.Windows.Forms.FontDialog DialogSelectFont;
		private System.Windows.Forms.OpenFileDialog DialogLoadText;
		private System.Windows.Forms.SaveFileDialog DialogSaveText;
    private System.Windows.Forms.ContextMenuStrip cmsEditor;
    private System.Windows.Forms.ToolStripMenuItem mitLoad;
    private System.Windows.Forms.ToolStripMenuItem mitSave;
    private System.Windows.Forms.ToolStripMenuItem mitSaveAs;
    private System.Windows.Forms.ToolStripMenuItem mitParameter;
    private System.Windows.Forms.StatusStrip stsLegend;
    private System.Windows.Forms.ToolStripStatusLabel stsRow;
    private System.Windows.Forms.ToolStripStatusLabel stsCol;
    private System.Windows.Forms.ToolStripStatusLabel stsFileName;
    private System.Windows.Forms.ToolStripStatusLabel stsWorkingDirectory;
    private System.Windows.Forms.ToolStripMenuItem mitText;
    private System.Windows.Forms.ToolStripMenuItem mitShowLegend;
    private System.Windows.Forms.ToolStripMenuItem mitShow;
    private System.Windows.Forms.ToolStripMenuItem mitProjection;
    private System.Windows.Forms.ToolStripMenuItem mitDisplayProjection;
    private System.Windows.Forms.ToolStripMenuItem mitSelectFont;
    private System.Windows.Forms.Panel pnlFont;
    private System.Windows.Forms.Button btnSelectFontColor;
    private System.Windows.Forms.Button btnSelectFont;
    private System.Windows.Forms.Button btnHideEditorFont;
    private System.Windows.Forms.Button btnClosePanelFont;
    private System.Windows.Forms.Panel pnlParameter;
    private System.Windows.Forms.Button btnHideEditorParameter;
    private System.Windows.Forms.Button btnClosePanelParameter;
    private System.Windows.Forms.Label lblTabulator;
    private System.Windows.Forms.CheckBox cbxShowLegend;
    private System.Windows.Forms.CheckBox cbxIsReadOnly;
    private System.Windows.Forms.HScrollBar scbTabulator;
    private System.Windows.Forms.Panel pnlSave;
    private System.Windows.Forms.Button btnHideEditorSave;
    private System.Windows.Forms.Button btnClosePanelSave;
    private System.Windows.Forms.Button btnClearSaveList;
    private System.Windows.Forms.Button btnSaveToFile;
    private System.Windows.Forms.ComboBox cbxSaveToFile;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Panel pnlLoad;
    private System.Windows.Forms.Button btnHideEditorLoad;
    private System.Windows.Forms.Button btnClosePanelLoad;
    private System.Windows.Forms.Button btnClearLoadList;
    private System.Windows.Forms.Button btnLoadFromFile;
    private System.Windows.Forms.ComboBox cbxLoadFromFile;
    private System.Windows.Forms.Label label1;
    private CUCRichTextBox FRTBEditor;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown nudFontPositionY;
    private System.Windows.Forms.NumericUpDown nudFontPositionX;
    private System.Windows.Forms.ColorDialog DialogSelectFontColor;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.NumericUpDown nudFontOpacity;
	}
}
