﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserAreaScannerTable
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlBottom = new System.Windows.Forms.Panel();
      this.cbxSkipZeroPulsesTable = new System.Windows.Forms.CheckBox();
      this.btnLoadLaserSteptable = new System.Windows.Forms.Button();
      this.btnAbortSteptable = new System.Windows.Forms.Button();
      this.btnPulseSteptable = new System.Windows.Forms.Button();
      this.DialogLoadLaserSteptable = new System.Windows.Forms.OpenFileDialog();
      this.lblHeader = new System.Windows.Forms.Label();
      this.FUCLaserStepTable = new UCLaserAreaScanner.CUCLaserStepTable();
      this.pnlBottom.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlBottom
      // 
      this.pnlBottom.Controls.Add(this.cbxSkipZeroPulsesTable);
      this.pnlBottom.Controls.Add(this.btnLoadLaserSteptable);
      this.pnlBottom.Controls.Add(this.btnAbortSteptable);
      this.pnlBottom.Controls.Add(this.btnPulseSteptable);
      this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlBottom.Location = new System.Drawing.Point(0, 413);
      this.pnlBottom.Name = "pnlBottom";
      this.pnlBottom.Size = new System.Drawing.Size(639, 37);
      this.pnlBottom.TabIndex = 1;
      // 
      // cbxSkipZeroPulsesTable
      // 
      this.cbxSkipZeroPulsesTable.AutoSize = true;
      this.cbxSkipZeroPulsesTable.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.cbxSkipZeroPulsesTable.Enabled = false;
      this.cbxSkipZeroPulsesTable.Location = new System.Drawing.Point(119, 11);
      this.cbxSkipZeroPulsesTable.Name = "cbxSkipZeroPulsesTable";
      this.cbxSkipZeroPulsesTable.Size = new System.Drawing.Size(100, 17);
      this.cbxSkipZeroPulsesTable.TabIndex = 147;
      this.cbxSkipZeroPulsesTable.Text = "SkipZeroPulses";
      this.cbxSkipZeroPulsesTable.UseVisualStyleBackColor = true;
      // 
      // btnLoadLaserSteptable
      // 
      this.btnLoadLaserSteptable.Location = new System.Drawing.Point(7, 6);
      this.btnLoadLaserSteptable.Name = "btnLoadLaserSteptable";
      this.btnLoadLaserSteptable.Size = new System.Drawing.Size(106, 25);
      this.btnLoadLaserSteptable.TabIndex = 146;
      this.btnLoadLaserSteptable.Text = "Load Steptable";
      this.btnLoadLaserSteptable.UseVisualStyleBackColor = true;
      this.btnLoadLaserSteptable.Click += new System.EventHandler(this.btnLoadLaserSteptable_Click);
      // 
      // btnAbortSteptable
      // 
      this.btnAbortSteptable.Location = new System.Drawing.Point(343, 6);
      this.btnAbortSteptable.Name = "btnAbortSteptable";
      this.btnAbortSteptable.Size = new System.Drawing.Size(106, 25);
      this.btnAbortSteptable.TabIndex = 145;
      this.btnAbortSteptable.Text = "Abort Steptable";
      this.btnAbortSteptable.UseVisualStyleBackColor = true;
      this.btnAbortSteptable.Click += new System.EventHandler(this.btnAbortSteptable_Click);
      // 
      // btnPulseSteptable
      // 
      this.btnPulseSteptable.Location = new System.Drawing.Point(231, 6);
      this.btnPulseSteptable.Name = "btnPulseSteptable";
      this.btnPulseSteptable.Size = new System.Drawing.Size(106, 25);
      this.btnPulseSteptable.TabIndex = 144;
      this.btnPulseSteptable.Text = "Pulse Steptable";
      this.btnPulseSteptable.UseVisualStyleBackColor = true;
      this.btnPulseSteptable.Click += new System.EventHandler(this.btnPulseSteptable_Click);
      // 
      // DialogLoadLaserSteptable
      // 
      this.DialogLoadLaserSteptable.Filter = "Steptable (*.stp)|*.stp|All Files (*.*)|*.*";
      this.DialogLoadLaserSteptable.Title = "Load Steptable";
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(639, 23);
      this.lblHeader.TabIndex = 107;
      this.lblHeader.Text = "LaserAreaScanner - Table";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FUCLaserStepTable
      // 
      this.FUCLaserStepTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCLaserStepTable.Location = new System.Drawing.Point(0, 23);
      this.FUCLaserStepTable.Name = "FUCLaserStepTable";
      this.FUCLaserStepTable.Size = new System.Drawing.Size(639, 390);
      this.FUCLaserStepTable.TabIndex = 108;
      // 
      // CUCLaserAreaScannerTable
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCLaserStepTable);
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.pnlBottom);
      this.Name = "CUCLaserAreaScannerTable";
      this.Size = new System.Drawing.Size(639, 450);
      this.pnlBottom.ResumeLayout(false);
      this.pnlBottom.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion
    private System.Windows.Forms.Panel pnlBottom;
    private System.Windows.Forms.Button btnLoadLaserSteptable;
    private System.Windows.Forms.Button btnAbortSteptable;
    private System.Windows.Forms.Button btnPulseSteptable;
    private System.Windows.Forms.OpenFileDialog DialogLoadLaserSteptable;
    private System.Windows.Forms.CheckBox cbxSkipZeroPulsesTable;
    private System.Windows.Forms.Label lblHeader;
    private CUCLaserStepTable FUCLaserStepTable;
  }
}
