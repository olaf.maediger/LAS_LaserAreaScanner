﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPLaserAreaScanner;
using LaserScanner;
//
namespace UCLaserAreaScanner
{
  public partial class CUCLaserAreaScannerTable : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    // 
    private DOnLoadLaserSteptable FOnLoadLaserSteptable;
    private DOnPulseSteptable FOnPulseSteptable;
    private DOnAbortSteptable FOnAbortSteptable;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCLaserAreaScannerTable()
    {
      InitializeComponent();
    }


    // LaserImage
    public void SetOnLoadLaserSteptable(DOnLoadLaserSteptable value)
    {
      FOnLoadLaserSteptable = value;
    }
    public void SetOnPulseSteptable(DOnPulseSteptable value)
    {
      FOnPulseSteptable = value;
    }
    public void SetOnAbortSteptable(DOnAbortSteptable value)
    {
      FOnAbortSteptable = value;
    }

    public void SetLaserStepList(CLaserStepList lasersteplist)
    {
      if (lasersteplist is CLaserStepList)
      {
        if (0 < lasersteplist.Count)
        {
          FUCLaserStepTable.SetLaserStepList(lasersteplist);
        }
      }
    }

    public void SelectLaserStep(UInt32 index)
    {
      FUCLaserStepTable.SelectLaserStep(index);
    }

    public Boolean SkipZeroPulsesTable
    {
      get { return cbxSkipZeroPulsesTable.Checked; }
      set { cbxSkipZeroPulsesTable.Checked = value; }
    }



    private void btnLoadLaserSteptable_Click(object sender, EventArgs e)
    {
      if (FOnLoadLaserSteptable is DOnLoadLaserSteptable)
      {
        if (DialogResult.OK == DialogLoadLaserSteptable.ShowDialog())
        {
          FOnLoadLaserSteptable(DialogLoadLaserSteptable.FileName);
        }
      }
    }

    private void btnPulseSteptable_Click(object sender, EventArgs e)
    {
      if (FOnPulseSteptable is DOnPulseSteptable)
      {
        FOnPulseSteptable();
      }
    }

    private void btnAbortSteptable_Click(object sender, EventArgs e)
    {
      if (FOnAbortSteptable is DOnAbortSteptable)
      {
        FOnAbortSteptable();
      }
    }

  }
}
