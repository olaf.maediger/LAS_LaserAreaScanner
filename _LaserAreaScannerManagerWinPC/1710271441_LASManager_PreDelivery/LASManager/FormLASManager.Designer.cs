﻿namespace LASManager
{
  partial class FormClient
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlProtocol = new System.Windows.Forms.Panel();
      this.FUCSerialNumber = new UCSerialNumber.CUCSerialNumber();
      this.splProtocol = new System.Windows.Forms.Splitter();
      this.tmrStartup = new System.Windows.Forms.Timer(this.components);
      this.FHelpProvider = new System.Windows.Forms.HelpProvider();
      this.DialogSaveInitdata = new System.Windows.Forms.SaveFileDialog();
      this.DialogLoadInitdata = new System.Windows.Forms.OpenFileDialog();
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.mitSystem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.mitConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCDefault = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCResetToDefaultConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveAutomaticAtProgramEnd = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCStartup = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitElevator = new System.Windows.Forms.ToolStripMenuItem();
      this.mitStartSimulation = new System.Windows.Forms.ToolStripMenuItem();
      this.mitProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPEditParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPClearProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
      this.diagnosticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPLoadDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPSaveDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSendDiagnosticEmail = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHelp = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHContents = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHTopic = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHSearch = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterApplicationProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterDeviceProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.tbcMain = new System.Windows.Forms.TabControl();
      this.tbpCommands = new System.Windows.Forms.TabPage();
      this.FUCEditorLaserStepFile = new UCTextEditor.CUCEditTextEditor();
      this.btnAbortVariableLaser = new System.Windows.Forms.Button();
      this.btnPulseVariableLaser = new System.Windows.Forms.Button();
      this.btnEnterVariableLaser = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.label9 = new System.Windows.Forms.Label();
      this.lblStateLaserAreaScannerIndex = new System.Windows.Forms.Label();
      this.lblStateLaserAreaScannerText = new System.Windows.Forms.Label();
      this.label19 = new System.Windows.Forms.Label();
      this.btnPulsePositionLaser = new System.Windows.Forms.Button();
      this.label17 = new System.Windows.Forms.Label();
      this.nudPositionY = new System.Windows.Forms.NumericUpDown();
      this.btnSetPositionY = new System.Windows.Forms.Button();
      this.btnGetPositionY = new System.Windows.Forms.Button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.pnlLedLaserState = new System.Windows.Forms.Panel();
      this.label18 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.nudDelayPulse = new System.Windows.Forms.NumericUpDown();
      this.label7 = new System.Windows.Forms.Label();
      this.nudDelayMotion = new System.Windows.Forms.NumericUpDown();
      this.label6 = new System.Windows.Forms.Label();
      this.nudDelta = new System.Windows.Forms.NumericUpDown();
      this.label5 = new System.Windows.Forms.Label();
      this.nudMax = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.nudMin = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.nudPositionX = new System.Windows.Forms.NumericUpDown();
      this.tbxMessages = new System.Windows.Forms.TextBox();
      this.tbxHardwareVersion = new System.Windows.Forms.TextBox();
      this.tbxSoftwareVersion = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.nudPulseCount = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.nudPulsePeriod = new System.Windows.Forms.NumericUpDown();
      this.btnGetHelp = new System.Windows.Forms.Button();
      this.btnAbortMatrixLaser = new System.Windows.Forms.Button();
      this.btnPulseMatrixLaser = new System.Windows.Forms.Button();
      this.btnSetDelayPulse = new System.Windows.Forms.Button();
      this.btnSetDelayMotion = new System.Windows.Forms.Button();
      this.btnSetMotionParameterY = new System.Windows.Forms.Button();
      this.btnSetMotionParameterX = new System.Windows.Forms.Button();
      this.btnSetPositionX = new System.Windows.Forms.Button();
      this.btnGetPositionX = new System.Windows.Forms.Button();
      this.btnPulseLedLaser = new System.Windows.Forms.Button();
      this.btnSetLedLaserOff = new System.Windows.Forms.Button();
      this.btnSetLedLaserOn = new System.Windows.Forms.Button();
      this.btnGetLedLaser = new System.Windows.Forms.Button();
      this.btnGetHardwareVersion = new System.Windows.Forms.Button();
      this.btnGetSoftwareVersion = new System.Windows.Forms.Button();
      this.btnGetProgramHeader = new System.Windows.Forms.Button();
      this.tbpBitmap = new System.Windows.Forms.TabPage();
      this.FUCLaserStepText = new UCTextEditor.CUCEditTextEditor();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.pnlImage = new System.Windows.Forms.Panel();
      this.pbxImage = new System.Windows.Forms.PictureBox();
      this.pnlBottom = new System.Windows.Forms.Panel();
      this.label23 = new System.Windows.Forms.Label();
      this.nudBmpPulsePeriod = new System.Windows.Forms.NumericUpDown();
      this.label21 = new System.Windows.Forms.Label();
      this.nudBmpPositionYMaximum = new System.Windows.Forms.NumericUpDown();
      this.label22 = new System.Windows.Forms.Label();
      this.nudBmpPositionYMinimum = new System.Windows.Forms.NumericUpDown();
      this.label16 = new System.Windows.Forms.Label();
      this.nudBmpPositionXMaximum = new System.Windows.Forms.NumericUpDown();
      this.label20 = new System.Windows.Forms.Label();
      this.nudBmpPositionXMinimum = new System.Windows.Forms.NumericUpDown();
      this.label15 = new System.Windows.Forms.Label();
      this.nudBmpPulseCountHigh = new System.Windows.Forms.NumericUpDown();
      this.label14 = new System.Windows.Forms.Label();
      this.nudBmpPulseCountLow = new System.Windows.Forms.NumericUpDown();
      this.btnBmpSaveLaserStepList = new System.Windows.Forms.Button();
      this.btnBmpCopyLaserStepList = new System.Windows.Forms.Button();
      this.btnBmpAbortMaterialProcessing = new System.Windows.Forms.Button();
      this.btnBmpPulseMaterialProcessing = new System.Windows.Forms.Button();
      this.btnBmpEnterMaterialProcessing = new System.Windows.Forms.Button();
      this.lblBmpColorLow = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.lblBmpColorHigh = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.hsbBmpThreshold = new System.Windows.Forms.HScrollBar();
      this.btnBmpConvertMonoChrome = new System.Windows.Forms.Button();
      this.label12 = new System.Windows.Forms.Label();
      this.nudBmpThreshold = new System.Windows.Forms.NumericUpDown();
      this.btnBmpSaveImage = new System.Windows.Forms.Button();
      this.btnBmpConvertGreyScale = new System.Windows.Forms.Button();
      this.label13 = new System.Windows.Forms.Label();
      this.nudBmpScaleFactor = new System.Windows.Forms.NumericUpDown();
      this.cbxBmpZoom = new System.Windows.Forms.CheckBox();
      this.btnBmpLoadImage = new System.Windows.Forms.Button();
      this.cbxAutomate = new System.Windows.Forms.CheckBox();
      this.tmrAutomation = new System.Windows.Forms.Timer(this.components);
      this.tmrLaserStepList = new System.Windows.Forms.Timer(this.components);
      this.DialogLoadImage = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveImage = new System.Windows.Forms.SaveFileDialog();
      this.DialogColorPixel = new System.Windows.Forms.ColorDialog();
      this.DialogSaveLaserStepList = new System.Windows.Forms.SaveFileDialog();
      this.pnlProtocol.SuspendLayout();
      this.mstMain.SuspendLayout();
      this.tbcMain.SuspendLayout();
      this.tbpCommands.SuspendLayout();
      this.panel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionY)).BeginInit();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayPulse)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayMotion)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelta)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMax)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMin)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionX)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriod)).BeginInit();
      this.tbpBitmap.SuspendLayout();
      this.pnlImage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).BeginInit();
      this.pnlBottom.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPulsePeriod)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPositionYMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPositionYMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPositionXMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPositionXMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPulseCountHigh)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPulseCountLow)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpThreshold)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpScaleFactor)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlProtocol
      // 
      this.pnlProtocol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlProtocol.Controls.Add(this.FUCSerialNumber);
      this.pnlProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlProtocol.Location = new System.Drawing.Point(0, 705);
      this.pnlProtocol.Name = "pnlProtocol";
      this.pnlProtocol.Size = new System.Drawing.Size(832, 165);
      this.pnlProtocol.TabIndex = 138;
      // 
      // FUCSerialNumber
      // 
      this.FUCSerialNumber.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCSerialNumber.Location = new System.Drawing.Point(0, 0);
      this.FUCSerialNumber.Name = "FUCSerialNumber";
      this.FUCSerialNumber.Size = new System.Drawing.Size(830, 64);
      this.FUCSerialNumber.TabIndex = 113;
      this.FUCSerialNumber.Visible = false;
      // 
      // splProtocol
      // 
      this.splProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splProtocol.Location = new System.Drawing.Point(0, 702);
      this.splProtocol.Name = "splProtocol";
      this.splProtocol.Size = new System.Drawing.Size(832, 3);
      this.splProtocol.TabIndex = 139;
      this.splProtocol.TabStop = false;
      // 
      // tmrStartup
      // 
      this.tmrStartup.Interval = 1;
      // 
      // DialogSaveInitdata
      // 
      this.DialogSaveInitdata.DefaultExt = "ini.xml";
      this.DialogSaveInitdata.FileName = "Initdata";
      this.DialogSaveInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogSaveInitdata.Title = "MDesign: Save Initdata";
      // 
      // DialogLoadInitdata
      // 
      this.DialogLoadInitdata.DefaultExt = "ini.xml";
      this.DialogLoadInitdata.FileName = "Initdata";
      this.DialogLoadInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogLoadInitdata.Title = "MDesign: Load Initdata";
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSystem,
            this.mitConfiguration,
            this.mitElevator,
            this.mitProtocol,
            this.mitHelp});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(832, 24);
      this.mstMain.TabIndex = 140;
      this.mstMain.Text = "System";
      // 
      // mitSystem
      // 
      this.mitSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSQuit});
      this.mitSystem.Name = "mitSystem";
      this.mitSystem.Size = new System.Drawing.Size(57, 20);
      this.mitSystem.Text = "&System";
      // 
      // mitSQuit
      // 
      this.mitSQuit.Name = "mitSQuit";
      this.mitSQuit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
      this.mitSQuit.Size = new System.Drawing.Size(140, 22);
      this.mitSQuit.Text = "&Quit";
      // 
      // mitConfiguration
      // 
      this.mitConfiguration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitCDefault,
            this.mitCResetToDefaultConfiguration,
            this.mitCSaveAutomaticAtProgramEnd,
            this.mitCStartup,
            this.mitCLoadStartupConfiguration,
            this.mitCSaveStartupConfiguration,
            this.mitCShowEditStartupConfiguration,
            this.toolStripMenuItem7,
            this.mitCLoadSelectableConfiguration,
            this.mitCSaveSelectableConfiguration,
            this.mitCShowEditSelectableConfiguration});
      this.mitConfiguration.Name = "mitConfiguration";
      this.mitConfiguration.Size = new System.Drawing.Size(93, 20);
      this.mitConfiguration.Text = "&Configuration";
      // 
      // mitCDefault
      // 
      this.mitCDefault.Enabled = false;
      this.mitCDefault.Name = "mitCDefault";
      this.mitCDefault.Size = new System.Drawing.Size(300, 22);
      this.mitCDefault.Text = "- Default -";
      // 
      // mitCResetToDefaultConfiguration
      // 
      this.mitCResetToDefaultConfiguration.Name = "mitCResetToDefaultConfiguration";
      this.mitCResetToDefaultConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
      this.mitCResetToDefaultConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCResetToDefaultConfiguration.Text = "Reset to Default-Configuration";
      // 
      // mitCSaveAutomaticAtProgramEnd
      // 
      this.mitCSaveAutomaticAtProgramEnd.Name = "mitCSaveAutomaticAtProgramEnd";
      this.mitCSaveAutomaticAtProgramEnd.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveAutomaticAtProgramEnd.Text = "Save automatic at program end";
      // 
      // mitCStartup
      // 
      this.mitCStartup.Enabled = false;
      this.mitCStartup.Name = "mitCStartup";
      this.mitCStartup.Size = new System.Drawing.Size(300, 22);
      this.mitCStartup.Text = "- Startup -";
      // 
      // mitCLoadStartupConfiguration
      // 
      this.mitCLoadStartupConfiguration.Name = "mitCLoadStartupConfiguration";
      this.mitCLoadStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
      this.mitCLoadStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadStartupConfiguration.Text = "Load Startup-Configuration";
      // 
      // mitCSaveStartupConfiguration
      // 
      this.mitCSaveStartupConfiguration.Name = "mitCSaveStartupConfiguration";
      this.mitCSaveStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
      this.mitCSaveStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveStartupConfiguration.Text = "Save Startup-Configuration";
      // 
      // mitCShowEditStartupConfiguration
      // 
      this.mitCShowEditStartupConfiguration.Name = "mitCShowEditStartupConfiguration";
      this.mitCShowEditStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditStartupConfiguration.Text = "Show / Edit Startup-Configuration";
      // 
      // toolStripMenuItem7
      // 
      this.toolStripMenuItem7.Enabled = false;
      this.toolStripMenuItem7.Name = "toolStripMenuItem7";
      this.toolStripMenuItem7.Size = new System.Drawing.Size(300, 22);
      this.toolStripMenuItem7.Text = "- Named -";
      // 
      // mitCLoadSelectableConfiguration
      // 
      this.mitCLoadSelectableConfiguration.Name = "mitCLoadSelectableConfiguration";
      this.mitCLoadSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadSelectableConfiguration.Text = "Load Selectable-Configuration ";
      // 
      // mitCSaveSelectableConfiguration
      // 
      this.mitCSaveSelectableConfiguration.Name = "mitCSaveSelectableConfiguration";
      this.mitCSaveSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveSelectableConfiguration.Text = "Save Selectable-Configuration";
      // 
      // mitCShowEditSelectableConfiguration
      // 
      this.mitCShowEditSelectableConfiguration.Name = "mitCShowEditSelectableConfiguration";
      this.mitCShowEditSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditSelectableConfiguration.Text = "Show / Edit Selectable-Configuration";
      // 
      // mitElevator
      // 
      this.mitElevator.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitStartSimulation});
      this.mitElevator.Name = "mitElevator";
      this.mitElevator.Size = new System.Drawing.Size(106, 20);
      this.mitElevator.Text = "Communication";
      // 
      // mitStartSimulation
      // 
      this.mitStartSimulation.Name = "mitStartSimulation";
      this.mitStartSimulation.Size = new System.Drawing.Size(98, 22);
      this.mitStartSimulation.Text = "Start";
      // 
      // mitProtocol
      // 
      this.mitProtocol.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitPEditParameter,
            this.mitPClearProtocol,
            this.mitPCopyToClipboard,
            this.diagnosticToolStripMenuItem,
            this.mitPLoadDiagnosticFile,
            this.mitPSaveDiagnosticFile,
            this.mitSendDiagnosticEmail});
      this.mitProtocol.Name = "mitProtocol";
      this.mitProtocol.Size = new System.Drawing.Size(64, 20);
      this.mitProtocol.Text = "&Protocol";
      // 
      // mitPEditParameter
      // 
      this.mitPEditParameter.Name = "mitPEditParameter";
      this.mitPEditParameter.Size = new System.Drawing.Size(171, 22);
      this.mitPEditParameter.Text = "Edit Parameter";
      // 
      // mitPClearProtocol
      // 
      this.mitPClearProtocol.Name = "mitPClearProtocol";
      this.mitPClearProtocol.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
      this.mitPClearProtocol.Size = new System.Drawing.Size(171, 22);
      this.mitPClearProtocol.Text = "Clear";
      // 
      // mitPCopyToClipboard
      // 
      this.mitPCopyToClipboard.Name = "mitPCopyToClipboard";
      this.mitPCopyToClipboard.Size = new System.Drawing.Size(171, 22);
      this.mitPCopyToClipboard.Text = "Copy to Clipboard";
      // 
      // diagnosticToolStripMenuItem
      // 
      this.diagnosticToolStripMenuItem.Enabled = false;
      this.diagnosticToolStripMenuItem.Name = "diagnosticToolStripMenuItem";
      this.diagnosticToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
      this.diagnosticToolStripMenuItem.Text = "- Diagnostic -";
      // 
      // mitPLoadDiagnosticFile
      // 
      this.mitPLoadDiagnosticFile.Name = "mitPLoadDiagnosticFile";
      this.mitPLoadDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPLoadDiagnosticFile.Text = "Load File";
      // 
      // mitPSaveDiagnosticFile
      // 
      this.mitPSaveDiagnosticFile.Name = "mitPSaveDiagnosticFile";
      this.mitPSaveDiagnosticFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
      this.mitPSaveDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPSaveDiagnosticFile.Text = "Save File";
      // 
      // mitSendDiagnosticEmail
      // 
      this.mitSendDiagnosticEmail.Name = "mitSendDiagnosticEmail";
      this.mitSendDiagnosticEmail.Size = new System.Drawing.Size(171, 22);
      this.mitSendDiagnosticEmail.Text = "Send Email";
      // 
      // mitHelp
      // 
      this.mitHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitHContents,
            this.mitHTopic,
            this.mitHSearch,
            this.mitHEnterApplicationProductKey,
            this.mitHEnterDeviceProductKey,
            this.mitHAbout});
      this.mitHelp.Name = "mitHelp";
      this.mitHelp.Size = new System.Drawing.Size(44, 20);
      this.mitHelp.Text = "Help";
      // 
      // mitHContents
      // 
      this.mitHContents.Name = "mitHContents";
      this.mitHContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.mitHContents.Size = new System.Drawing.Size(234, 22);
      this.mitHContents.Text = "Contents";
      // 
      // mitHTopic
      // 
      this.mitHTopic.Name = "mitHTopic";
      this.mitHTopic.Size = new System.Drawing.Size(234, 22);
      this.mitHTopic.Text = "Topic";
      // 
      // mitHSearch
      // 
      this.mitHSearch.Name = "mitHSearch";
      this.mitHSearch.Size = new System.Drawing.Size(234, 22);
      this.mitHSearch.Text = "Search";
      // 
      // mitHEnterApplicationProductKey
      // 
      this.mitHEnterApplicationProductKey.Name = "mitHEnterApplicationProductKey";
      this.mitHEnterApplicationProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterApplicationProductKey.Text = "Enter Application Product-Key";
      // 
      // mitHEnterDeviceProductKey
      // 
      this.mitHEnterDeviceProductKey.Name = "mitHEnterDeviceProductKey";
      this.mitHEnterDeviceProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterDeviceProductKey.Text = "Enter Device Product-Key";
      // 
      // mitHAbout
      // 
      this.mitHAbout.Name = "mitHAbout";
      this.mitHAbout.ShortcutKeys = System.Windows.Forms.Keys.F2;
      this.mitHAbout.Size = new System.Drawing.Size(234, 22);
      this.mitHAbout.Text = "About";
      // 
      // tbcMain
      // 
      this.tbcMain.Alignment = System.Windows.Forms.TabAlignment.Bottom;
      this.tbcMain.Controls.Add(this.tbpCommands);
      this.tbcMain.Controls.Add(this.tbpBitmap);
      this.tbcMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcMain.Location = new System.Drawing.Point(0, 24);
      this.tbcMain.Name = "tbcMain";
      this.tbcMain.SelectedIndex = 0;
      this.tbcMain.Size = new System.Drawing.Size(832, 678);
      this.tbcMain.TabIndex = 141;
      // 
      // tbpCommands
      // 
      this.tbpCommands.Controls.Add(this.FUCEditorLaserStepFile);
      this.tbpCommands.Controls.Add(this.btnAbortVariableLaser);
      this.tbpCommands.Controls.Add(this.btnPulseVariableLaser);
      this.tbpCommands.Controls.Add(this.btnEnterVariableLaser);
      this.tbpCommands.Controls.Add(this.panel2);
      this.tbpCommands.Controls.Add(this.btnPulsePositionLaser);
      this.tbpCommands.Controls.Add(this.label17);
      this.tbpCommands.Controls.Add(this.nudPositionY);
      this.tbpCommands.Controls.Add(this.btnSetPositionY);
      this.tbpCommands.Controls.Add(this.btnGetPositionY);
      this.tbpCommands.Controls.Add(this.panel1);
      this.tbpCommands.Controls.Add(this.label8);
      this.tbpCommands.Controls.Add(this.nudDelayPulse);
      this.tbpCommands.Controls.Add(this.label7);
      this.tbpCommands.Controls.Add(this.nudDelayMotion);
      this.tbpCommands.Controls.Add(this.label6);
      this.tbpCommands.Controls.Add(this.nudDelta);
      this.tbpCommands.Controls.Add(this.label5);
      this.tbpCommands.Controls.Add(this.nudMax);
      this.tbpCommands.Controls.Add(this.label4);
      this.tbpCommands.Controls.Add(this.nudMin);
      this.tbpCommands.Controls.Add(this.label3);
      this.tbpCommands.Controls.Add(this.nudPositionX);
      this.tbpCommands.Controls.Add(this.tbxMessages);
      this.tbpCommands.Controls.Add(this.tbxHardwareVersion);
      this.tbpCommands.Controls.Add(this.tbxSoftwareVersion);
      this.tbpCommands.Controls.Add(this.label2);
      this.tbpCommands.Controls.Add(this.nudPulseCount);
      this.tbpCommands.Controls.Add(this.label1);
      this.tbpCommands.Controls.Add(this.nudPulsePeriod);
      this.tbpCommands.Controls.Add(this.btnGetHelp);
      this.tbpCommands.Controls.Add(this.btnAbortMatrixLaser);
      this.tbpCommands.Controls.Add(this.btnPulseMatrixLaser);
      this.tbpCommands.Controls.Add(this.btnSetDelayPulse);
      this.tbpCommands.Controls.Add(this.btnSetDelayMotion);
      this.tbpCommands.Controls.Add(this.btnSetMotionParameterY);
      this.tbpCommands.Controls.Add(this.btnSetMotionParameterX);
      this.tbpCommands.Controls.Add(this.btnSetPositionX);
      this.tbpCommands.Controls.Add(this.btnGetPositionX);
      this.tbpCommands.Controls.Add(this.btnPulseLedLaser);
      this.tbpCommands.Controls.Add(this.btnSetLedLaserOff);
      this.tbpCommands.Controls.Add(this.btnSetLedLaserOn);
      this.tbpCommands.Controls.Add(this.btnGetLedLaser);
      this.tbpCommands.Controls.Add(this.btnGetHardwareVersion);
      this.tbpCommands.Controls.Add(this.btnGetSoftwareVersion);
      this.tbpCommands.Controls.Add(this.btnGetProgramHeader);
      this.tbpCommands.Location = new System.Drawing.Point(4, 4);
      this.tbpCommands.Margin = new System.Windows.Forms.Padding(0);
      this.tbpCommands.Name = "tbpCommands";
      this.tbpCommands.Size = new System.Drawing.Size(824, 652);
      this.tbpCommands.TabIndex = 5;
      this.tbpCommands.Text = "Commands";
      this.tbpCommands.SizeChanged += new System.EventHandler(this.tbpCommands_SizeChanged);
      // 
      // FUCEditorLaserStepFile
      // 
      this.FUCEditorLaserStepFile.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.FUCEditorLaserStepFile.FileName = "";
      this.FUCEditorLaserStepFile.IsReadOnly = false;
      this.FUCEditorLaserStepFile.Lines = new string[0];
      this.FUCEditorLaserStepFile.LoadFileExtension = "lsf.txt";
      this.FUCEditorLaserStepFile.LoadFileFilter = "LaserStepFiles (*.lsf.txt)|*.lsf.txt|Textfiles (*.txt)|*.txt|All files (*.*)|*.*";
      this.FUCEditorLaserStepFile.LoadFileName = "";
      this.FUCEditorLaserStepFile.Location = new System.Drawing.Point(0, 352);
      this.FUCEditorLaserStepFile.Name = "FUCEditorLaserStepFile";
      this.FUCEditorLaserStepFile.SaveFileExtension = "cmt.txt";
      this.FUCEditorLaserStepFile.SaveFileFilter = "Commentfiles (*.cmt.txt)|*.cmt.txt|Textfiles (*.txt)|*.txt|All files (*.*)|*.*";
      this.FUCEditorLaserStepFile.SaveFileName = "";
      this.FUCEditorLaserStepFile.Size = new System.Drawing.Size(824, 300);
      this.FUCEditorLaserStepFile.TabIndex = 69;
      this.FUCEditorLaserStepFile.WorkingDirectory = "";
      // 
      // btnAbortVariableLaser
      // 
      this.btnAbortVariableLaser.Location = new System.Drawing.Point(226, 323);
      this.btnAbortVariableLaser.Name = "btnAbortVariableLaser";
      this.btnAbortVariableLaser.Size = new System.Drawing.Size(105, 23);
      this.btnAbortVariableLaser.TabIndex = 68;
      this.btnAbortVariableLaser.Text = "AbortVariableLaser";
      this.btnAbortVariableLaser.UseVisualStyleBackColor = true;
      this.btnAbortVariableLaser.Click += new System.EventHandler(this.btnAbortVariableLaser_Click);
      // 
      // btnPulseVariableLaser
      // 
      this.btnPulseVariableLaser.Location = new System.Drawing.Point(115, 323);
      this.btnPulseVariableLaser.Name = "btnPulseVariableLaser";
      this.btnPulseVariableLaser.Size = new System.Drawing.Size(105, 23);
      this.btnPulseVariableLaser.TabIndex = 67;
      this.btnPulseVariableLaser.Text = "PulseVariableLaser";
      this.btnPulseVariableLaser.UseVisualStyleBackColor = true;
      this.btnPulseVariableLaser.Click += new System.EventHandler(this.btnPulseVariableLaser_Click);
      // 
      // btnEnterVariableLaser
      // 
      this.btnEnterVariableLaser.Location = new System.Drawing.Point(4, 323);
      this.btnEnterVariableLaser.Name = "btnEnterVariableLaser";
      this.btnEnterVariableLaser.Size = new System.Drawing.Size(105, 23);
      this.btnEnterVariableLaser.TabIndex = 66;
      this.btnEnterVariableLaser.Text = "EnterVariableLaser";
      this.btnEnterVariableLaser.UseVisualStyleBackColor = true;
      this.btnEnterVariableLaser.Click += new System.EventHandler(this.btnEnterVariableLaser_Click);
      // 
      // panel2
      // 
      this.panel2.BackColor = System.Drawing.SystemColors.Info;
      this.panel2.Controls.Add(this.label9);
      this.panel2.Controls.Add(this.lblStateLaserAreaScannerIndex);
      this.panel2.Controls.Add(this.lblStateLaserAreaScannerText);
      this.panel2.Controls.Add(this.label19);
      this.panel2.Location = new System.Drawing.Point(249, 31);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(311, 27);
      this.panel2.TabIndex = 65;
      // 
      // label9
      // 
      this.label9.Location = new System.Drawing.Point(85, 7);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(8, 13);
      this.label9.TabIndex = 68;
      this.label9.Text = "-";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblStateLaserAreaScannerIndex
      // 
      this.lblStateLaserAreaScannerIndex.BackColor = System.Drawing.Color.PeachPuff;
      this.lblStateLaserAreaScannerIndex.Location = new System.Drawing.Point(58, 4);
      this.lblStateLaserAreaScannerIndex.Name = "lblStateLaserAreaScannerIndex";
      this.lblStateLaserAreaScannerIndex.Size = new System.Drawing.Size(25, 19);
      this.lblStateLaserAreaScannerIndex.TabIndex = 67;
      this.lblStateLaserAreaScannerIndex.Text = "- - -";
      this.lblStateLaserAreaScannerIndex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblStateLaserAreaScannerText
      // 
      this.lblStateLaserAreaScannerText.BackColor = System.Drawing.Color.PeachPuff;
      this.lblStateLaserAreaScannerText.Location = new System.Drawing.Point(95, 4);
      this.lblStateLaserAreaScannerText.Name = "lblStateLaserAreaScannerText";
      this.lblStateLaserAreaScannerText.Size = new System.Drawing.Size(210, 19);
      this.lblStateLaserAreaScannerText.TabIndex = 66;
      this.lblStateLaserAreaScannerText.Text = "- - -";
      this.lblStateLaserAreaScannerText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label19
      // 
      this.label19.AutoSize = true;
      this.label19.Location = new System.Drawing.Point(3, 7);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(55, 13);
      this.label19.TabIndex = 65;
      this.label19.Text = "State LAS";
      // 
      // btnPulsePositionLaser
      // 
      this.btnPulsePositionLaser.Location = new System.Drawing.Point(4, 202);
      this.btnPulsePositionLaser.Name = "btnPulsePositionLaser";
      this.btnPulsePositionLaser.Size = new System.Drawing.Size(117, 23);
      this.btnPulsePositionLaser.TabIndex = 64;
      this.btnPulsePositionLaser.Text = "PulsePositionLaser";
      this.btnPulsePositionLaser.UseVisualStyleBackColor = true;
      this.btnPulsePositionLaser.Click += new System.EventHandler(this.btnPulsePositionLaser_Click);
      // 
      // label17
      // 
      this.label17.AutoSize = true;
      this.label17.Location = new System.Drawing.Point(123, 120);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(66, 13);
      this.label17.TabIndex = 63;
      this.label17.Text = "PositionY [1]";
      // 
      // nudPositionY
      // 
      this.nudPositionY.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudPositionY.Location = new System.Drawing.Point(190, 118);
      this.nudPositionY.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionY.Name = "nudPositionY";
      this.nudPositionY.Size = new System.Drawing.Size(48, 20);
      this.nudPositionY.TabIndex = 62;
      this.nudPositionY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionY.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // btnSetPositionY
      // 
      this.btnSetPositionY.Location = new System.Drawing.Point(247, 144);
      this.btnSetPositionY.Name = "btnSetPositionY";
      this.btnSetPositionY.Size = new System.Drawing.Size(77, 23);
      this.btnSetPositionY.TabIndex = 61;
      this.btnSetPositionY.Text = "SetPositionY";
      this.btnSetPositionY.UseVisualStyleBackColor = true;
      this.btnSetPositionY.Click += new System.EventHandler(this.btnSetPositionY_Click);
      // 
      // btnGetPositionY
      // 
      this.btnGetPositionY.Location = new System.Drawing.Point(166, 144);
      this.btnGetPositionY.Name = "btnGetPositionY";
      this.btnGetPositionY.Size = new System.Drawing.Size(77, 23);
      this.btnGetPositionY.TabIndex = 60;
      this.btnGetPositionY.Text = "GetPositionY";
      this.btnGetPositionY.UseVisualStyleBackColor = true;
      this.btnGetPositionY.Click += new System.EventHandler(this.btnGetPositionY_Click);
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.SystemColors.Info;
      this.panel1.Controls.Add(this.pnlLedLaserState);
      this.panel1.Controls.Add(this.label18);
      this.panel1.Location = new System.Drawing.Point(457, 115);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(83, 27);
      this.panel1.TabIndex = 59;
      // 
      // pnlLedLaserState
      // 
      this.pnlLedLaserState.BackColor = System.Drawing.Color.DarkRed;
      this.pnlLedLaserState.Location = new System.Drawing.Point(60, 6);
      this.pnlLedLaserState.Name = "pnlLedLaserState";
      this.pnlLedLaserState.Size = new System.Drawing.Size(16, 15);
      this.pnlLedLaserState.TabIndex = 0;
      // 
      // label18
      // 
      this.label18.AutoSize = true;
      this.label18.Location = new System.Drawing.Point(3, 7);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(58, 13);
      this.label18.TabIndex = 65;
      this.label18.Text = "LaserState";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(318, 271);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(54, 13);
      this.label8.TabIndex = 40;
      this.label8.Text = "Delay [us]";
      // 
      // nudDelayPulse
      // 
      this.nudDelayPulse.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudDelayPulse.Location = new System.Drawing.Point(375, 268);
      this.nudDelayPulse.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudDelayPulse.Name = "nudDelayPulse";
      this.nudDelayPulse.Size = new System.Drawing.Size(69, 20);
      this.nudDelayPulse.TabIndex = 39;
      this.nudDelayPulse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelayPulse.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(95, 270);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(54, 13);
      this.label7.TabIndex = 38;
      this.label7.Text = "Delay [us]";
      // 
      // nudDelayMotion
      // 
      this.nudDelayMotion.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudDelayMotion.Location = new System.Drawing.Point(152, 267);
      this.nudDelayMotion.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudDelayMotion.Name = "nudDelayMotion";
      this.nudDelayMotion.Size = new System.Drawing.Size(69, 20);
      this.nudDelayMotion.TabIndex = 37;
      this.nudDelayMotion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelayMotion.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(440, 241);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(47, 13);
      this.label6.TabIndex = 36;
      this.label6.Text = "Delta [1]";
      // 
      // nudDelta
      // 
      this.nudDelta.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudDelta.Location = new System.Drawing.Point(487, 239);
      this.nudDelta.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudDelta.Name = "nudDelta";
      this.nudDelta.Size = new System.Drawing.Size(48, 20);
      this.nudDelta.TabIndex = 35;
      this.nudDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelta.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(344, 241);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(42, 13);
      this.label5.TabIndex = 34;
      this.label5.Text = "Max [1]";
      // 
      // nudMax
      // 
      this.nudMax.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudMax.Location = new System.Drawing.Point(387, 239);
      this.nudMax.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudMax.Name = "nudMax";
      this.nudMax.Size = new System.Drawing.Size(48, 20);
      this.nudMax.TabIndex = 33;
      this.nudMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMax.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(251, 241);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(39, 13);
      this.label4.TabIndex = 32;
      this.label4.Text = "Min [1]";
      // 
      // nudMin
      // 
      this.nudMin.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudMin.Location = new System.Drawing.Point(291, 239);
      this.nudMin.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudMin.Name = "nudMin";
      this.nudMin.Size = new System.Drawing.Size(48, 20);
      this.nudMin.TabIndex = 31;
      this.nudMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMin.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(5, 120);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(66, 13);
      this.label3.TabIndex = 30;
      this.label3.Text = "PositionX [1]";
      // 
      // nudPositionX
      // 
      this.nudPositionX.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudPositionX.Location = new System.Drawing.Point(72, 118);
      this.nudPositionX.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionX.Name = "nudPositionX";
      this.nudPositionX.Size = new System.Drawing.Size(48, 20);
      this.nudPositionX.TabIndex = 29;
      this.nudPositionX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionX.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // tbxMessages
      // 
      this.tbxMessages.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxMessages.Location = new System.Drawing.Point(4, 62);
      this.tbxMessages.Multiline = true;
      this.tbxMessages.Name = "tbxMessages";
      this.tbxMessages.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.tbxMessages.Size = new System.Drawing.Size(556, 49);
      this.tbxMessages.TabIndex = 27;
      // 
      // tbxHardwareVersion
      // 
      this.tbxHardwareVersion.Enabled = false;
      this.tbxHardwareVersion.Location = new System.Drawing.Point(125, 6);
      this.tbxHardwareVersion.Name = "tbxHardwareVersion";
      this.tbxHardwareVersion.Size = new System.Drawing.Size(85, 20);
      this.tbxHardwareVersion.TabIndex = 24;
      this.tbxHardwareVersion.Text = "-";
      this.tbxHardwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tbxSoftwareVersion
      // 
      this.tbxSoftwareVersion.Enabled = false;
      this.tbxSoftwareVersion.Location = new System.Drawing.Point(337, 7);
      this.tbxSoftwareVersion.Name = "tbxSoftwareVersion";
      this.tbxSoftwareVersion.Size = new System.Drawing.Size(85, 20);
      this.tbxSoftwareVersion.TabIndex = 23;
      this.tbxSoftwareVersion.Text = "-";
      this.tbxSoftwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(351, 120);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(50, 13);
      this.label2.TabIndex = 22;
      this.label2.Text = "Count [1]";
      // 
      // nudPulseCount
      // 
      this.nudPulseCount.Location = new System.Drawing.Point(402, 118);
      this.nudPulseCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudPulseCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPulseCount.Name = "nudPulseCount";
      this.nudPulseCount.Size = new System.Drawing.Size(48, 20);
      this.nudPulseCount.TabIndex = 21;
      this.nudPulseCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulseCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(241, 120);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(59, 13);
      this.label1.TabIndex = 20;
      this.label1.Text = "Period [ms]";
      // 
      // nudPulsePeriod
      // 
      this.nudPulsePeriod.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudPulsePeriod.Location = new System.Drawing.Point(301, 118);
      this.nudPulsePeriod.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
      this.nudPulsePeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPulsePeriod.Name = "nudPulsePeriod";
      this.nudPulsePeriod.Size = new System.Drawing.Size(48, 20);
      this.nudPulsePeriod.TabIndex = 18;
      this.nudPulsePeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulsePeriod.Value = new decimal(new int[] {
            333,
            0,
            0,
            0});
      // 
      // btnGetHelp
      // 
      this.btnGetHelp.Location = new System.Drawing.Point(4, 33);
      this.btnGetHelp.Name = "btnGetHelp";
      this.btnGetHelp.Size = new System.Drawing.Size(117, 23);
      this.btnGetHelp.TabIndex = 16;
      this.btnGetHelp.Text = "GetHelp";
      this.btnGetHelp.UseVisualStyleBackColor = true;
      this.btnGetHelp.Click += new System.EventHandler(this.btnGetHelp_Click);
      // 
      // btnAbortMatrixLaser
      // 
      this.btnAbortMatrixLaser.Location = new System.Drawing.Point(104, 294);
      this.btnAbortMatrixLaser.Name = "btnAbortMatrixLaser";
      this.btnAbortMatrixLaser.Size = new System.Drawing.Size(96, 23);
      this.btnAbortMatrixLaser.TabIndex = 15;
      this.btnAbortMatrixLaser.Text = "AbortMatrixLaser";
      this.btnAbortMatrixLaser.UseVisualStyleBackColor = true;
      this.btnAbortMatrixLaser.Click += new System.EventHandler(this.btnAbortMatrixLaser_Click);
      // 
      // btnPulseMatrixLaser
      // 
      this.btnPulseMatrixLaser.Location = new System.Drawing.Point(3, 294);
      this.btnPulseMatrixLaser.Name = "btnPulseMatrixLaser";
      this.btnPulseMatrixLaser.Size = new System.Drawing.Size(97, 23);
      this.btnPulseMatrixLaser.TabIndex = 14;
      this.btnPulseMatrixLaser.Text = "PulseMatrixLaser";
      this.btnPulseMatrixLaser.UseVisualStyleBackColor = true;
      this.btnPulseMatrixLaser.Click += new System.EventHandler(this.btnPulseMatrixLaser_Click);
      // 
      // btnSetDelayPulse
      // 
      this.btnSetDelayPulse.Location = new System.Drawing.Point(227, 265);
      this.btnSetDelayPulse.Name = "btnSetDelayPulse";
      this.btnSetDelayPulse.Size = new System.Drawing.Size(90, 23);
      this.btnSetDelayPulse.TabIndex = 13;
      this.btnSetDelayPulse.Text = "SetDelayPulse";
      this.btnSetDelayPulse.UseVisualStyleBackColor = true;
      this.btnSetDelayPulse.Click += new System.EventHandler(this.btnSetDelayPulse_Click);
      // 
      // btnSetDelayMotion
      // 
      this.btnSetDelayMotion.Location = new System.Drawing.Point(3, 265);
      this.btnSetDelayMotion.Name = "btnSetDelayMotion";
      this.btnSetDelayMotion.Size = new System.Drawing.Size(90, 23);
      this.btnSetDelayMotion.TabIndex = 12;
      this.btnSetDelayMotion.Text = "SetDelayMotion";
      this.btnSetDelayMotion.UseVisualStyleBackColor = true;
      this.btnSetDelayMotion.Click += new System.EventHandler(this.btnSetDelayMotion_Click);
      // 
      // btnSetMotionParameterY
      // 
      this.btnSetMotionParameterY.Location = new System.Drawing.Point(127, 236);
      this.btnSetMotionParameterY.Name = "btnSetMotionParameterY";
      this.btnSetMotionParameterY.Size = new System.Drawing.Size(118, 23);
      this.btnSetMotionParameterY.TabIndex = 11;
      this.btnSetMotionParameterY.Text = "SetMotionParameterY";
      this.btnSetMotionParameterY.UseVisualStyleBackColor = true;
      this.btnSetMotionParameterY.Click += new System.EventHandler(this.btnSetMotionParameterY_Click);
      // 
      // btnSetMotionParameterX
      // 
      this.btnSetMotionParameterX.Location = new System.Drawing.Point(3, 236);
      this.btnSetMotionParameterX.Name = "btnSetMotionParameterX";
      this.btnSetMotionParameterX.Size = new System.Drawing.Size(118, 23);
      this.btnSetMotionParameterX.TabIndex = 10;
      this.btnSetMotionParameterX.Text = "SetMotionParameterX";
      this.btnSetMotionParameterX.UseVisualStyleBackColor = true;
      this.btnSetMotionParameterX.Click += new System.EventHandler(this.btnSetMotionParameterX_Click);
      // 
      // btnSetPositionX
      // 
      this.btnSetPositionX.Location = new System.Drawing.Point(85, 144);
      this.btnSetPositionX.Name = "btnSetPositionX";
      this.btnSetPositionX.Size = new System.Drawing.Size(77, 23);
      this.btnSetPositionX.TabIndex = 9;
      this.btnSetPositionX.Text = "SetPositionX";
      this.btnSetPositionX.UseVisualStyleBackColor = true;
      this.btnSetPositionX.Click += new System.EventHandler(this.btnSetPositionX_Click);
      // 
      // btnGetPositionX
      // 
      this.btnGetPositionX.Location = new System.Drawing.Point(4, 144);
      this.btnGetPositionX.Name = "btnGetPositionX";
      this.btnGetPositionX.Size = new System.Drawing.Size(77, 23);
      this.btnGetPositionX.TabIndex = 8;
      this.btnGetPositionX.Text = "GetPositionX";
      this.btnGetPositionX.UseVisualStyleBackColor = true;
      this.btnGetPositionX.Click += new System.EventHandler(this.btnGetPositionX_Click);
      // 
      // btnPulseLedLaser
      // 
      this.btnPulseLedLaser.Location = new System.Drawing.Point(247, 173);
      this.btnPulseLedLaser.Name = "btnPulseLedLaser";
      this.btnPulseLedLaser.Size = new System.Drawing.Size(67, 23);
      this.btnPulseLedLaser.TabIndex = 7;
      this.btnPulseLedLaser.Text = "PulseLaser";
      this.btnPulseLedLaser.UseVisualStyleBackColor = true;
      this.btnPulseLedLaser.Click += new System.EventHandler(this.btnPulseLedLaser_Click);
      // 
      // btnSetLedLaserOff
      // 
      this.btnSetLedLaserOff.Location = new System.Drawing.Point(172, 173);
      this.btnSetLedLaserOff.Name = "btnSetLedLaserOff";
      this.btnSetLedLaserOff.Size = new System.Drawing.Size(71, 23);
      this.btnSetLedLaserOff.TabIndex = 5;
      this.btnSetLedLaserOff.Text = "SetLaserOff";
      this.btnSetLedLaserOff.UseVisualStyleBackColor = true;
      this.btnSetLedLaserOff.Click += new System.EventHandler(this.btnSetLedLaserOff_Click);
      // 
      // btnSetLedLaserOn
      // 
      this.btnSetLedLaserOn.Location = new System.Drawing.Point(97, 173);
      this.btnSetLedLaserOn.Name = "btnSetLedLaserOn";
      this.btnSetLedLaserOn.Size = new System.Drawing.Size(71, 23);
      this.btnSetLedLaserOn.TabIndex = 4;
      this.btnSetLedLaserOn.Text = "SetLaserOn";
      this.btnSetLedLaserOn.UseVisualStyleBackColor = true;
      this.btnSetLedLaserOn.Click += new System.EventHandler(this.btnSetLedLaserOn_Click);
      // 
      // btnGetLedLaser
      // 
      this.btnGetLedLaser.Location = new System.Drawing.Point(4, 173);
      this.btnGetLedLaser.Name = "btnGetLedLaser";
      this.btnGetLedLaser.Size = new System.Drawing.Size(89, 23);
      this.btnGetLedLaser.TabIndex = 3;
      this.btnGetLedLaser.Text = "GetLaserState";
      this.btnGetLedLaser.UseVisualStyleBackColor = true;
      this.btnGetLedLaser.Click += new System.EventHandler(this.btnGetLedLaser_Click);
      // 
      // btnGetHardwareVersion
      // 
      this.btnGetHardwareVersion.Location = new System.Drawing.Point(3, 4);
      this.btnGetHardwareVersion.Name = "btnGetHardwareVersion";
      this.btnGetHardwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetHardwareVersion.TabIndex = 2;
      this.btnGetHardwareVersion.Text = "GetHardwareVersion";
      this.btnGetHardwareVersion.UseVisualStyleBackColor = true;
      this.btnGetHardwareVersion.Click += new System.EventHandler(this.btnGetHardwareVersion_Click);
      // 
      // btnGetSoftwareVersion
      // 
      this.btnGetSoftwareVersion.Location = new System.Drawing.Point(215, 5);
      this.btnGetSoftwareVersion.Name = "btnGetSoftwareVersion";
      this.btnGetSoftwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetSoftwareVersion.TabIndex = 1;
      this.btnGetSoftwareVersion.Text = "GetSoftwareVersion";
      this.btnGetSoftwareVersion.UseVisualStyleBackColor = true;
      this.btnGetSoftwareVersion.Click += new System.EventHandler(this.btnGetSoftwareVersion_Click);
      // 
      // btnGetProgramHeader
      // 
      this.btnGetProgramHeader.Location = new System.Drawing.Point(125, 33);
      this.btnGetProgramHeader.Name = "btnGetProgramHeader";
      this.btnGetProgramHeader.Size = new System.Drawing.Size(117, 23);
      this.btnGetProgramHeader.TabIndex = 0;
      this.btnGetProgramHeader.Text = "GetProgramHeader";
      this.btnGetProgramHeader.UseVisualStyleBackColor = true;
      this.btnGetProgramHeader.Click += new System.EventHandler(this.btnGetProgramHeader_Click);
      // 
      // tbpBitmap
      // 
      this.tbpBitmap.Controls.Add(this.FUCLaserStepText);
      this.tbpBitmap.Controls.Add(this.splitter1);
      this.tbpBitmap.Controls.Add(this.pnlImage);
      this.tbpBitmap.Controls.Add(this.pnlBottom);
      this.tbpBitmap.Location = new System.Drawing.Point(4, 4);
      this.tbpBitmap.Name = "tbpBitmap";
      this.tbpBitmap.Size = new System.Drawing.Size(824, 652);
      this.tbpBitmap.TabIndex = 6;
      this.tbpBitmap.Text = "Bitmap";
      this.tbpBitmap.UseVisualStyleBackColor = true;
      // 
      // FUCLaserStepText
      // 
      this.FUCLaserStepText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCLaserStepText.FileName = "";
      this.FUCLaserStepText.Font = new System.Drawing.Font("Courier New", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.FUCLaserStepText.IsReadOnly = false;
      this.FUCLaserStepText.Lines = new string[0];
      this.FUCLaserStepText.LoadFileExtension = "lsf.txt";
      this.FUCLaserStepText.LoadFileFilter = "LaserStepFiles (*.lsf.txt)|*.lsf.txt|Textfiles (*.txt)|*.txt|All files (*.*)|*.*";
      this.FUCLaserStepText.LoadFileName = "";
      this.FUCLaserStepText.Location = new System.Drawing.Point(485, 0);
      this.FUCLaserStepText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
      this.FUCLaserStepText.Name = "FUCLaserStepText";
      this.FUCLaserStepText.SaveFileExtension = "cmt.txt";
      this.FUCLaserStepText.SaveFileFilter = "Commentfiles (*.cmt.txt)|*.cmt.txt|Textfiles (*.txt)|*.txt|All files (*.*)|*.*";
      this.FUCLaserStepText.SaveFileName = "";
      this.FUCLaserStepText.Size = new System.Drawing.Size(339, 517);
      this.FUCLaserStepText.TabIndex = 70;
      this.FUCLaserStepText.WorkingDirectory = "";
      // 
      // splitter1
      // 
      this.splitter1.Location = new System.Drawing.Point(482, 0);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(3, 517);
      this.splitter1.TabIndex = 4;
      this.splitter1.TabStop = false;
      // 
      // pnlImage
      // 
      this.pnlImage.AutoScroll = true;
      this.pnlImage.BackColor = System.Drawing.Color.DarkRed;
      this.pnlImage.Controls.Add(this.pbxImage);
      this.pnlImage.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlImage.Location = new System.Drawing.Point(0, 0);
      this.pnlImage.Name = "pnlImage";
      this.pnlImage.Size = new System.Drawing.Size(482, 517);
      this.pnlImage.TabIndex = 3;
      // 
      // pbxImage
      // 
      this.pbxImage.BackColor = System.Drawing.Color.Silver;
      this.pbxImage.Location = new System.Drawing.Point(0, 0);
      this.pbxImage.Name = "pbxImage";
      this.pbxImage.Size = new System.Drawing.Size(554, 284);
      this.pbxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pbxImage.TabIndex = 5;
      this.pbxImage.TabStop = false;
      // 
      // pnlBottom
      // 
      this.pnlBottom.BackColor = System.Drawing.Color.Gainsboro;
      this.pnlBottom.Controls.Add(this.label23);
      this.pnlBottom.Controls.Add(this.nudBmpPulsePeriod);
      this.pnlBottom.Controls.Add(this.label21);
      this.pnlBottom.Controls.Add(this.nudBmpPositionYMaximum);
      this.pnlBottom.Controls.Add(this.label22);
      this.pnlBottom.Controls.Add(this.nudBmpPositionYMinimum);
      this.pnlBottom.Controls.Add(this.label16);
      this.pnlBottom.Controls.Add(this.nudBmpPositionXMaximum);
      this.pnlBottom.Controls.Add(this.label20);
      this.pnlBottom.Controls.Add(this.nudBmpPositionXMinimum);
      this.pnlBottom.Controls.Add(this.label15);
      this.pnlBottom.Controls.Add(this.nudBmpPulseCountHigh);
      this.pnlBottom.Controls.Add(this.label14);
      this.pnlBottom.Controls.Add(this.nudBmpPulseCountLow);
      this.pnlBottom.Controls.Add(this.btnBmpSaveLaserStepList);
      this.pnlBottom.Controls.Add(this.btnBmpCopyLaserStepList);
      this.pnlBottom.Controls.Add(this.btnBmpAbortMaterialProcessing);
      this.pnlBottom.Controls.Add(this.btnBmpPulseMaterialProcessing);
      this.pnlBottom.Controls.Add(this.btnBmpEnterMaterialProcessing);
      this.pnlBottom.Controls.Add(this.lblBmpColorLow);
      this.pnlBottom.Controls.Add(this.label10);
      this.pnlBottom.Controls.Add(this.lblBmpColorHigh);
      this.pnlBottom.Controls.Add(this.label11);
      this.pnlBottom.Controls.Add(this.hsbBmpThreshold);
      this.pnlBottom.Controls.Add(this.btnBmpConvertMonoChrome);
      this.pnlBottom.Controls.Add(this.label12);
      this.pnlBottom.Controls.Add(this.nudBmpThreshold);
      this.pnlBottom.Controls.Add(this.btnBmpSaveImage);
      this.pnlBottom.Controls.Add(this.btnBmpConvertGreyScale);
      this.pnlBottom.Controls.Add(this.label13);
      this.pnlBottom.Controls.Add(this.nudBmpScaleFactor);
      this.pnlBottom.Controls.Add(this.cbxBmpZoom);
      this.pnlBottom.Controls.Add(this.btnBmpLoadImage);
      this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlBottom.Location = new System.Drawing.Point(0, 517);
      this.pnlBottom.Name = "pnlBottom";
      this.pnlBottom.Size = new System.Drawing.Size(824, 135);
      this.pnlBottom.TabIndex = 2;
      // 
      // label23
      // 
      this.label23.AutoSize = true;
      this.label23.Location = new System.Drawing.Point(14, 84);
      this.label23.Name = "label23";
      this.label23.Size = new System.Drawing.Size(59, 13);
      this.label23.TabIndex = 88;
      this.label23.Text = "Period [ms]";
      // 
      // nudBmpPulsePeriod
      // 
      this.nudBmpPulsePeriod.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudBmpPulsePeriod.Location = new System.Drawing.Point(74, 82);
      this.nudBmpPulsePeriod.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
      this.nudBmpPulsePeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudBmpPulsePeriod.Name = "nudBmpPulsePeriod";
      this.nudBmpPulsePeriod.Size = new System.Drawing.Size(48, 20);
      this.nudBmpPulsePeriod.TabIndex = 87;
      this.nudBmpPulsePeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudBmpPulsePeriod.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      // 
      // label21
      // 
      this.label21.AutoSize = true;
      this.label21.Location = new System.Drawing.Point(370, 60);
      this.label21.Name = "label21";
      this.label21.Size = new System.Drawing.Size(73, 13);
      this.label21.TabIndex = 86;
      this.label21.Text = "YMaximum [1]";
      // 
      // nudBmpPositionYMaximum
      // 
      this.nudBmpPositionYMaximum.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudBmpPositionYMaximum.Location = new System.Drawing.Point(444, 58);
      this.nudBmpPositionYMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudBmpPositionYMaximum.Name = "nudBmpPositionYMaximum";
      this.nudBmpPositionYMaximum.Size = new System.Drawing.Size(48, 20);
      this.nudBmpPositionYMaximum.TabIndex = 85;
      this.nudBmpPositionYMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudBmpPositionYMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // label22
      // 
      this.label22.AutoSize = true;
      this.label22.Location = new System.Drawing.Point(249, 60);
      this.label22.Name = "label22";
      this.label22.Size = new System.Drawing.Size(70, 13);
      this.label22.TabIndex = 84;
      this.label22.Text = "YMinimum [1]";
      // 
      // nudBmpPositionYMinimum
      // 
      this.nudBmpPositionYMinimum.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudBmpPositionYMinimum.Location = new System.Drawing.Point(320, 58);
      this.nudBmpPositionYMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudBmpPositionYMinimum.Name = "nudBmpPositionYMinimum";
      this.nudBmpPositionYMinimum.Size = new System.Drawing.Size(48, 20);
      this.nudBmpPositionYMinimum.TabIndex = 83;
      this.nudBmpPositionYMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudBmpPositionYMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Location = new System.Drawing.Point(125, 60);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(73, 13);
      this.label16.TabIndex = 82;
      this.label16.Text = "XMaximum [1]";
      // 
      // nudBmpPositionXMaximum
      // 
      this.nudBmpPositionXMaximum.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudBmpPositionXMaximum.Location = new System.Drawing.Point(199, 58);
      this.nudBmpPositionXMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudBmpPositionXMaximum.Name = "nudBmpPositionXMaximum";
      this.nudBmpPositionXMaximum.Size = new System.Drawing.Size(48, 20);
      this.nudBmpPositionXMaximum.TabIndex = 81;
      this.nudBmpPositionXMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudBmpPositionXMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // label20
      // 
      this.label20.AutoSize = true;
      this.label20.Location = new System.Drawing.Point(4, 60);
      this.label20.Name = "label20";
      this.label20.Size = new System.Drawing.Size(70, 13);
      this.label20.TabIndex = 80;
      this.label20.Text = "XMinimum [1]";
      // 
      // nudBmpPositionXMinimum
      // 
      this.nudBmpPositionXMinimum.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudBmpPositionXMinimum.Location = new System.Drawing.Point(75, 58);
      this.nudBmpPositionXMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudBmpPositionXMinimum.Name = "nudBmpPositionXMinimum";
      this.nudBmpPositionXMinimum.Size = new System.Drawing.Size(48, 20);
      this.nudBmpPositionXMinimum.TabIndex = 79;
      this.nudBmpPositionXMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudBmpPositionXMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // label15
      // 
      this.label15.AutoSize = true;
      this.label15.Location = new System.Drawing.Point(265, 84);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(101, 13);
      this.label15.TabIndex = 78;
      this.label15.Text = "PulseCount High [1]";
      // 
      // nudBmpPulseCountHigh
      // 
      this.nudBmpPulseCountHigh.Location = new System.Drawing.Point(367, 81);
      this.nudBmpPulseCountHigh.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudBmpPulseCountHigh.Name = "nudBmpPulseCountHigh";
      this.nudBmpPulseCountHigh.Size = new System.Drawing.Size(38, 20);
      this.nudBmpPulseCountHigh.TabIndex = 77;
      this.nudBmpPulseCountHigh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudBmpPulseCountHigh.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(124, 84);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(99, 13);
      this.label14.TabIndex = 76;
      this.label14.Text = "PulseCount Low [1]";
      // 
      // nudBmpPulseCountLow
      // 
      this.nudBmpPulseCountLow.Location = new System.Drawing.Point(225, 81);
      this.nudBmpPulseCountLow.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudBmpPulseCountLow.Name = "nudBmpPulseCountLow";
      this.nudBmpPulseCountLow.Size = new System.Drawing.Size(38, 20);
      this.nudBmpPulseCountLow.TabIndex = 75;
      this.nudBmpPulseCountLow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnBmpSaveLaserStepList
      // 
      this.btnBmpSaveLaserStepList.Location = new System.Drawing.Point(498, 81);
      this.btnBmpSaveLaserStepList.Name = "btnBmpSaveLaserStepList";
      this.btnBmpSaveLaserStepList.Size = new System.Drawing.Size(120, 23);
      this.btnBmpSaveLaserStepList.TabIndex = 74;
      this.btnBmpSaveLaserStepList.Text = "Save LaserStepList";
      this.btnBmpSaveLaserStepList.UseVisualStyleBackColor = true;
      this.btnBmpSaveLaserStepList.Click += new System.EventHandler(this.btnBmpSaveLaserStepList_Click);
      // 
      // btnBmpCopyLaserStepList
      // 
      this.btnBmpCopyLaserStepList.Location = new System.Drawing.Point(498, 56);
      this.btnBmpCopyLaserStepList.Name = "btnBmpCopyLaserStepList";
      this.btnBmpCopyLaserStepList.Size = new System.Drawing.Size(120, 23);
      this.btnBmpCopyLaserStepList.TabIndex = 72;
      this.btnBmpCopyLaserStepList.Text = "Copy LaserStepList";
      this.btnBmpCopyLaserStepList.UseVisualStyleBackColor = true;
      this.btnBmpCopyLaserStepList.Click += new System.EventHandler(this.btnBmpCopyLaserStepList_Click);
      // 
      // btnBmpAbortMaterialProcessing
      // 
      this.btnBmpAbortMaterialProcessing.Location = new System.Drawing.Point(277, 106);
      this.btnBmpAbortMaterialProcessing.Name = "btnBmpAbortMaterialProcessing";
      this.btnBmpAbortMaterialProcessing.Size = new System.Drawing.Size(132, 23);
      this.btnBmpAbortMaterialProcessing.TabIndex = 71;
      this.btnBmpAbortMaterialProcessing.Text = "Abort MaterialProcessing";
      this.btnBmpAbortMaterialProcessing.UseVisualStyleBackColor = true;
      this.btnBmpAbortMaterialProcessing.Click += new System.EventHandler(this.btnBmpAbortMaterialProcessing_Click);
      // 
      // btnBmpPulseMaterialProcessing
      // 
      this.btnBmpPulseMaterialProcessing.Location = new System.Drawing.Point(140, 106);
      this.btnBmpPulseMaterialProcessing.Name = "btnBmpPulseMaterialProcessing";
      this.btnBmpPulseMaterialProcessing.Size = new System.Drawing.Size(133, 23);
      this.btnBmpPulseMaterialProcessing.TabIndex = 70;
      this.btnBmpPulseMaterialProcessing.Text = "Pulse MaterialProcessing";
      this.btnBmpPulseMaterialProcessing.UseVisualStyleBackColor = true;
      this.btnBmpPulseMaterialProcessing.Click += new System.EventHandler(this.btnBmpPulseMaterialProcessing_Click);
      // 
      // btnBmpEnterMaterialProcessing
      // 
      this.btnBmpEnterMaterialProcessing.Location = new System.Drawing.Point(4, 106);
      this.btnBmpEnterMaterialProcessing.Name = "btnBmpEnterMaterialProcessing";
      this.btnBmpEnterMaterialProcessing.Size = new System.Drawing.Size(132, 23);
      this.btnBmpEnterMaterialProcessing.TabIndex = 69;
      this.btnBmpEnterMaterialProcessing.Text = "Enter MaterialProcessing";
      this.btnBmpEnterMaterialProcessing.UseVisualStyleBackColor = true;
      this.btnBmpEnterMaterialProcessing.Click += new System.EventHandler(this.btnBmpEnterMaterialProcessing_Click);
      // 
      // lblBmpColorLow
      // 
      this.lblBmpColorLow.BackColor = System.Drawing.Color.Black;
      this.lblBmpColorLow.Location = new System.Drawing.Point(53, 36);
      this.lblBmpColorLow.Name = "lblBmpColorLow";
      this.lblBmpColorLow.Size = new System.Drawing.Size(15, 13);
      this.lblBmpColorLow.TabIndex = 18;
      this.lblBmpColorLow.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblBmpColorLow_MouseClick);
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(4, 36);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(51, 13);
      this.label10.TabIndex = 17;
      this.label10.Text = "ColorLow";
      // 
      // lblBmpColorHigh
      // 
      this.lblBmpColorHigh.BackColor = System.Drawing.Color.White;
      this.lblBmpColorHigh.Location = new System.Drawing.Point(121, 36);
      this.lblBmpColorHigh.Name = "lblBmpColorHigh";
      this.lblBmpColorHigh.Size = new System.Drawing.Size(15, 13);
      this.lblBmpColorHigh.TabIndex = 16;
      this.lblBmpColorHigh.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lblBmpColorHigh_MouseClick);
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(70, 36);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(53, 13);
      this.label11.TabIndex = 15;
      this.label11.Text = "ColorHigh";
      // 
      // hsbBmpThreshold
      // 
      this.hsbBmpThreshold.Location = new System.Drawing.Point(141, 34);
      this.hsbBmpThreshold.Maximum = 256;
      this.hsbBmpThreshold.Name = "hsbBmpThreshold";
      this.hsbBmpThreshold.Size = new System.Drawing.Size(130, 17);
      this.hsbBmpThreshold.TabIndex = 13;
      this.hsbBmpThreshold.Value = 128;
      this.hsbBmpThreshold.ValueChanged += new System.EventHandler(this.hsbBmpThreshold_ValueChanged);
      // 
      // btnBmpConvertMonoChrome
      // 
      this.btnBmpConvertMonoChrome.Location = new System.Drawing.Point(373, 32);
      this.btnBmpConvertMonoChrome.Name = "btnBmpConvertMonoChrome";
      this.btnBmpConvertMonoChrome.Size = new System.Drawing.Size(120, 23);
      this.btnBmpConvertMonoChrome.TabIndex = 12;
      this.btnBmpConvertMonoChrome.Text = "Convert MonoChrome";
      this.btnBmpConvertMonoChrome.UseVisualStyleBackColor = true;
      this.btnBmpConvertMonoChrome.Click += new System.EventHandler(this.btnBmpConvertMonoChrome_Click);
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(274, 36);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(54, 13);
      this.label12.TabIndex = 10;
      this.label12.Text = "Threshold";
      // 
      // nudBmpThreshold
      // 
      this.nudBmpThreshold.Location = new System.Drawing.Point(329, 34);
      this.nudBmpThreshold.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudBmpThreshold.Name = "nudBmpThreshold";
      this.nudBmpThreshold.Size = new System.Drawing.Size(38, 20);
      this.nudBmpThreshold.TabIndex = 9;
      this.nudBmpThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudBmpThreshold.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
      this.nudBmpThreshold.ValueChanged += new System.EventHandler(this.nudBmpThreshold_ValueChanged);
      // 
      // btnBmpSaveImage
      // 
      this.btnBmpSaveImage.Location = new System.Drawing.Point(246, 7);
      this.btnBmpSaveImage.Name = "btnBmpSaveImage";
      this.btnBmpSaveImage.Size = new System.Drawing.Size(124, 23);
      this.btnBmpSaveImage.TabIndex = 7;
      this.btnBmpSaveImage.Text = "Save Image Converted";
      this.btnBmpSaveImage.UseVisualStyleBackColor = true;
      this.btnBmpSaveImage.Click += new System.EventHandler(this.btnBmpSaveImage_Click);
      // 
      // btnBmpConvertGreyScale
      // 
      this.btnBmpConvertGreyScale.Location = new System.Drawing.Point(373, 7);
      this.btnBmpConvertGreyScale.Name = "btnBmpConvertGreyScale";
      this.btnBmpConvertGreyScale.Size = new System.Drawing.Size(120, 23);
      this.btnBmpConvertGreyScale.TabIndex = 6;
      this.btnBmpConvertGreyScale.Text = "( Convert GreyScale )";
      this.btnBmpConvertGreyScale.UseVisualStyleBackColor = true;
      this.btnBmpConvertGreyScale.Click += new System.EventHandler(this.btnBmpConvertGreyScale_Click);
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Location = new System.Drawing.Point(3, 12);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(64, 13);
      this.label13.TabIndex = 5;
      this.label13.Text = "ScaleFactor";
      // 
      // nudBmpScaleFactor
      // 
      this.nudBmpScaleFactor.Location = new System.Drawing.Point(70, 9);
      this.nudBmpScaleFactor.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudBmpScaleFactor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudBmpScaleFactor.Name = "nudBmpScaleFactor";
      this.nudBmpScaleFactor.Size = new System.Drawing.Size(38, 20);
      this.nudBmpScaleFactor.TabIndex = 4;
      this.nudBmpScaleFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudBmpScaleFactor.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudBmpScaleFactor.ValueChanged += new System.EventHandler(this.nudBmpScaleFactor_ValueChanged);
      // 
      // cbxBmpZoom
      // 
      this.cbxBmpZoom.AutoSize = true;
      this.cbxBmpZoom.Location = new System.Drawing.Point(114, 11);
      this.cbxBmpZoom.Name = "cbxBmpZoom";
      this.cbxBmpZoom.Size = new System.Drawing.Size(53, 17);
      this.cbxBmpZoom.TabIndex = 2;
      this.cbxBmpZoom.Text = "Zoom";
      this.cbxBmpZoom.UseVisualStyleBackColor = true;
      // 
      // btnBmpLoadImage
      // 
      this.btnBmpLoadImage.Location = new System.Drawing.Point(165, 7);
      this.btnBmpLoadImage.Name = "btnBmpLoadImage";
      this.btnBmpLoadImage.Size = new System.Drawing.Size(79, 23);
      this.btnBmpLoadImage.TabIndex = 1;
      this.btnBmpLoadImage.Text = "Load Image";
      this.btnBmpLoadImage.UseVisualStyleBackColor = true;
      this.btnBmpLoadImage.Click += new System.EventHandler(this.btnBmpLoadImage_Click);
      // 
      // cbxAutomate
      // 
      this.cbxAutomate.AutoSize = true;
      this.cbxAutomate.Location = new System.Drawing.Point(376, 5);
      this.cbxAutomate.Name = "cbxAutomate";
      this.cbxAutomate.Size = new System.Drawing.Size(71, 17);
      this.cbxAutomate.TabIndex = 142;
      this.cbxAutomate.Text = "Automate";
      this.cbxAutomate.UseVisualStyleBackColor = true;
      this.cbxAutomate.CheckedChanged += new System.EventHandler(this.cbxAutomate_CheckedChanged);
      // 
      // tmrAutomation
      // 
      this.tmrAutomation.Interval = 1000;
      // 
      // tmrLaserStepList
      // 
      this.tmrLaserStepList.Interval = 1000;
      this.tmrLaserStepList.Tick += new System.EventHandler(this.tmrLaserStepList_Tick);
      // 
      // DialogLoadImage
      // 
      this.DialogLoadImage.DefaultExt = "bmp";
      this.DialogLoadImage.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
      this.DialogLoadImage.Title = "Load Image";
      // 
      // DialogSaveImage
      // 
      this.DialogSaveImage.DefaultExt = "bmp";
      this.DialogSaveImage.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
      this.DialogSaveImage.Title = "Save Image";
      // 
      // DialogSaveLaserStepList
      // 
      this.DialogSaveLaserStepList.DefaultExt = "lsf.txt";
      this.DialogSaveLaserStepList.Filter = "LaserStepFiles (*.lsf.txt)|*.lsf.txt|All files (*.*)|*.*";
      this.DialogSaveLaserStepList.Title = "Save LaserStepList";
      // 
      // FormClient
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(832, 870);
      this.Controls.Add(this.cbxAutomate);
      this.Controls.Add(this.tbcMain);
      this.Controls.Add(this.mstMain);
      this.Controls.Add(this.splProtocol);
      this.Controls.Add(this.pnlProtocol);
      this.Name = "FormClient";
      this.Text = "Form1";
      this.pnlProtocol.ResumeLayout(false);
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.tbcMain.ResumeLayout(false);
      this.tbpCommands.ResumeLayout(false);
      this.tbpCommands.PerformLayout();
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionY)).EndInit();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayPulse)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayMotion)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelta)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMax)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMin)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionX)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriod)).EndInit();
      this.tbpBitmap.ResumeLayout(false);
      this.pnlImage.ResumeLayout(false);
      this.pnlImage.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).EndInit();
      this.pnlBottom.ResumeLayout(false);
      this.pnlBottom.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPulsePeriod)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPositionYMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPositionYMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPositionXMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPositionXMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPulseCountHigh)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpPulseCountLow)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpThreshold)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudBmpScaleFactor)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel pnlProtocol;
    private UCSerialNumber.CUCSerialNumber FUCSerialNumber;
    private System.Windows.Forms.Splitter splProtocol;
    private System.Windows.Forms.Timer tmrStartup;
    private System.Windows.Forms.HelpProvider FHelpProvider;
    private System.Windows.Forms.SaveFileDialog DialogSaveInitdata;
    private System.Windows.Forms.OpenFileDialog DialogLoadInitdata;
    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem mitSystem;
    private System.Windows.Forms.ToolStripMenuItem mitSQuit;
    private System.Windows.Forms.ToolStripMenuItem mitConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCDefault;
    private System.Windows.Forms.ToolStripMenuItem mitCResetToDefaultConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveAutomaticAtProgramEnd;
    private System.Windows.Forms.ToolStripMenuItem mitCStartup;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitElevator;
    private System.Windows.Forms.ToolStripMenuItem mitStartSimulation;
    private System.Windows.Forms.ToolStripMenuItem mitProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPEditParameter;
    private System.Windows.Forms.ToolStripMenuItem mitPClearProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPCopyToClipboard;
    private System.Windows.Forms.ToolStripMenuItem diagnosticToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitPLoadDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitPSaveDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitSendDiagnosticEmail;
    private System.Windows.Forms.ToolStripMenuItem mitHelp;
    private System.Windows.Forms.ToolStripMenuItem mitHContents;
    private System.Windows.Forms.ToolStripMenuItem mitHTopic;
    private System.Windows.Forms.ToolStripMenuItem mitHSearch;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterApplicationProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterDeviceProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHAbout;
    private System.Windows.Forms.TabControl tbcMain;
    private System.Windows.Forms.TabPage tbpCommands;
    private System.Windows.Forms.CheckBox cbxAutomate;
    private System.Windows.Forms.Button btnAbortMatrixLaser;
    private System.Windows.Forms.Button btnPulseMatrixLaser;
    private System.Windows.Forms.Button btnSetDelayPulse;
    private System.Windows.Forms.Button btnSetDelayMotion;
    private System.Windows.Forms.Button btnSetMotionParameterY;
    private System.Windows.Forms.Button btnSetMotionParameterX;
    private System.Windows.Forms.Button btnSetPositionX;
    private System.Windows.Forms.Button btnGetPositionX;
    private System.Windows.Forms.Button btnPulseLedLaser;
    private System.Windows.Forms.Button btnSetLedLaserOff;
    private System.Windows.Forms.Button btnSetLedLaserOn;
    private System.Windows.Forms.Button btnGetLedLaser;
    private System.Windows.Forms.Button btnGetHardwareVersion;
    private System.Windows.Forms.Button btnGetSoftwareVersion;
    private System.Windows.Forms.Button btnGetProgramHeader;
    private System.Windows.Forms.Button btnGetHelp;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown nudPulseCount;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudPulsePeriod;
    private System.Windows.Forms.TextBox tbxHardwareVersion;
    private System.Windows.Forms.TextBox tbxSoftwareVersion;
    private System.Windows.Forms.TextBox tbxMessages;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown nudPositionX;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.NumericUpDown nudDelta;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.NumericUpDown nudMax;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.NumericUpDown nudMin;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.NumericUpDown nudDelayPulse;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.NumericUpDown nudDelayMotion;
    private System.Windows.Forms.Timer tmrAutomation;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel pnlLedLaserState;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.NumericUpDown nudPositionY;
    private System.Windows.Forms.Button btnSetPositionY;
    private System.Windows.Forms.Button btnGetPositionY;
    private System.Windows.Forms.Button btnPulsePositionLaser;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Label lblStateLaserAreaScannerText;
    private System.Windows.Forms.Label label19;
    private UCTextEditor.CUCEditTextEditor FUCEditorLaserStepFile;
    private System.Windows.Forms.Button btnAbortVariableLaser;
    private System.Windows.Forms.Button btnPulseVariableLaser;
    private System.Windows.Forms.Button btnEnterVariableLaser;
    private System.Windows.Forms.Timer tmrLaserStepList;
    private System.Windows.Forms.Label lblStateLaserAreaScannerIndex;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TabPage tbpBitmap;
    private System.Windows.Forms.Panel pnlImage;
    private System.Windows.Forms.PictureBox pbxImage;
    private System.Windows.Forms.Panel pnlBottom;
    private System.Windows.Forms.Label lblBmpColorLow;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label lblBmpColorHigh;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.HScrollBar hsbBmpThreshold;
    private System.Windows.Forms.Button btnBmpConvertMonoChrome;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.NumericUpDown nudBmpThreshold;
    private System.Windows.Forms.Button btnBmpSaveImage;
    private System.Windows.Forms.Button btnBmpConvertGreyScale;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.NumericUpDown nudBmpScaleFactor;
    private System.Windows.Forms.CheckBox cbxBmpZoom;
    private System.Windows.Forms.Button btnBmpLoadImage;
    private System.Windows.Forms.OpenFileDialog DialogLoadImage;
    private System.Windows.Forms.SaveFileDialog DialogSaveImage;
    private System.Windows.Forms.ColorDialog DialogColorPixel;
    private System.Windows.Forms.Button btnBmpAbortMaterialProcessing;
    private System.Windows.Forms.Button btnBmpPulseMaterialProcessing;
    private System.Windows.Forms.Button btnBmpEnterMaterialProcessing;
    private System.Windows.Forms.Button btnBmpSaveLaserStepList;
    private System.Windows.Forms.Button btnBmpCopyLaserStepList;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.NumericUpDown nudBmpPulseCountHigh;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.NumericUpDown nudBmpPulseCountLow;
    private UCTextEditor.CUCEditTextEditor FUCLaserStepText;
    private System.Windows.Forms.Splitter splitter1;
    private System.Windows.Forms.SaveFileDialog DialogSaveLaserStepList;
    private System.Windows.Forms.Label label23;
    private System.Windows.Forms.NumericUpDown nudBmpPulsePeriod;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.NumericUpDown nudBmpPositionYMaximum;
    private System.Windows.Forms.Label label22;
    private System.Windows.Forms.NumericUpDown nudBmpPositionYMinimum;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.NumericUpDown nudBmpPositionXMaximum;
    private System.Windows.Forms.Label label20;
    private System.Windows.Forms.NumericUpDown nudBmpPositionXMinimum;
    
  }
}

