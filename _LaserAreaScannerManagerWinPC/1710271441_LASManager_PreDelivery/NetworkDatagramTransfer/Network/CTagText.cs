﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace Network
{ 
  //
  //--------------------------------------------------------------------
  //  Segment - Common
  //--------------------------------------------------------------------
  //
  public abstract class CTagText : CTagBase
  {
    protected String[] FValues;
    public String[] Values
    {
      get { return FValues; }
      set { FValues = value; }
    }

    public void BuildByteList(String text, ref CByteList bytelist)
    {
      foreach (Char C in text)
      {
        bytelist.Add((Byte)C);
      }
    }
    public void BuildByteList(Char character, ref CByteList bytelist)
    {
      bytelist.Add((Byte)character);
    }

    public CByteList GetByteList()
    {
      CByteList Result = new CByteList();
      BuildByteList(GetSType(FType), ref Result);
      foreach (String S in FValues)
      {
        BuildByteList(String.Format("[{0}]", S), ref Result);
      }
      return Result;
    }
  };

  public class CTagTextList : List<CTagText>
  { // only linear lists!
    public CByteList GetByteList()
    {
      CByteList Result = new CByteList();
      foreach (CTagText TagText in this)
      {
        CByteList TagByteList = TagText.GetByteList();
        Result.AddRange(TagByteList);
      }
      return Result;
    }

    public CStringList GetInfo()
    {
      CStringList StringList = new CStringList();
      foreach (CTagText TagText in this)
      {
        String Line = String.Format("{0} {1} {2}",
                                    TagText.GetGroup(), TagText.GetName(), TagText.GetValue());
        StringList.Add(Line);
      }
      return StringList;
    }
  };
  //
  //--------------------------------------------------------------------
  //  Segment - MagicWord
  //--------------------------------------------------------------------
  // !!!!!!!!!!!!!!!! REDEFINE CTBMagicWord AS CTTMagicWord !!!!!!!!!!!!!!!!!!!
  public class CTTMagicWord : CTTScalar
  {
    public CTTMagicWord(Char[] values)
    {
      FType = TYPE_MAGIC_WORD;
      FValues = new String[6];
      for (Int32 CI = 0; CI < 6; CI++)
      {
        Char C = values[CI];
        FValues[CI] = new String(C, 1);
      }
    }

    public CTTMagicWord(Byte[] values, ref int index)
    {
      FType = TYPE_MAGIC_WORD;
      FValues = new String[6];
      for (Int32 CI = 0; CI < 6; CI++)
      {
        Char C = (Char)values[CI];
        FValues[CI] = new String(C, 1);
      }
      index += 6;
    }

    public override String GetName()
    {
      return "MagicWord";
    }
    public override String GetValue()
    {
      String Result = FValues[0];
      for (Int32 VI = 1; VI < 6; VI++)
      {
        Result += FValues[VI];
      }
      return Result;
    }

    public Char[] GetVectorChar()
    {
      Char[] Result = new Char[6];
      Result[0] = (Char)FValues[0][0];
      Result[1] = (Char)FValues[1][0];
      Result[2] = (Char)FValues[2][0];
      Result[3] = (Char)FValues[3][0];
      Result[4] = (Char)FValues[4][0];
      Result[5] = (Char)FValues[5][0];
      return Result;
    }
  };
  //
  //--------------------------------------------------------------------
  //  Segment - Scalar
  //--------------------------------------------------------------------
  //
  public class CTTScalarByte : CTTScalar
  {
    public CTTScalarByte(Byte value)
    {
      FType = TYPE_SCALAR_BYTE;
      FValues = new String[1];
      FValues[0] = String.Format("0x{0:00}", (Byte)value);
    }

    public override String GetName()
    {
      return "Byte";
    }
    public override String GetValue()
    {
      return FValues[0];
    }
  };

  public class CTTScalarChar : CTTScalar
  {
    public CTTScalarChar(Char value)
    {
      FType = TYPE_SCALAR_CHAR;
      FValues = new String[1];
      FValues[0] = new String(value, 1);
    }

    public override String GetName()
    {
      return "Char";
    }
    public override String GetValue()
    {
      return FValues[0];
    }
  };

  public class CTTScalarInt16 : CTTScalar
  {
    public CTTScalarInt16(Int16 value)
    {
      FType = TYPE_SCALAR_INT16;
      FValues = new String[1];
      FValues[0] = String.Format("{0}", value);
      
    }

    public override String GetName()
    {
      return "Int16";
    }
    public override String GetValue()
    {
      return FValues[0];
    }
  };

  public class CTTScalarString : CTTScalar
  {
    public CTTScalarString(String value)
    {
      FType = TYPE_SCALAR_STRING;
      Int32 L = value.Length;
      FValues = new String[1];
      FValues[0] = value;
    }

    public override String GetName()
    {
      return "String";
    }
  };
  //
  //--------------------------------------------------------------------
  //  Segment - Vector
  //--------------------------------------------------------------------
  //
  public class CTTVectorByte : CTTVector
  {
    public CTTVectorByte(Byte[] values)
    {
      FType = TYPE_VECTOR_BYTE;
      FSize0 = values.Length;
      FValues = new String[FSize0];
      for (Int32 VI = 0; VI < FSize0; VI++)
      {
        FValues[VI] = String.Format("0x{0:00}", (Byte)values[VI]);
      }
    }

    public override String GetName()
    {
      return "Byte";
    }
  };

  public class CTTVectorChar : CTTVector
  {
    public CTTVectorChar(Char[] values)
    {
      FType = TYPE_VECTOR_CHAR;
      FSize0 = values.GetLength(0);
      FValues = new String[FSize0];
      for (Int32 VI = 0; VI < FSize0; VI++)
      {
        FValues[VI] = new String(values[VI], 1);
      }
    }

    public override String GetName()
    {
      return "Char";
    }
  };

  public class CTTVectorDouble : CTTVector
  {
    public CTTVectorDouble(Double[] values)
    {
      FType = TYPE_VECTOR_DOUBLE;
      FSize0 = values.GetLength(0);
      FValues = new String[FSize0];
      for (Int32 VI = 0; VI < FSize0; VI++)
      {
        FValues[VI] = String.Format("{0}", values[VI]);
      }
    }

    public override String GetName()
    {
      return "Double";
    }
  };
  //
  //--------------------------------------------------------------------
  //  Segment - Matrix
  //--------------------------------------------------------------------
  //
  public class CTTMatrixByte : CTTMatrix
  {
    public CTTMatrixByte(Byte[,] values)
    {
      FType = TYPE_MATRIX_BYTE;
      FSize0 = values.GetLength(0);
      FSize1 = values.GetLength(1);
      FValues = new String[FSize0 * FSize1];
      Int32 SI = 0;
      for (Int32 V0I = 0; V0I < FSize0; V0I++)
      {
        for (Int32 V1I = 0; V1I < FSize1; V1I++)
        {
          FValues[SI] = String.Format("0x{0:00}", (Byte)values[V0I, V1I]);
          SI++;
        }
      }
    }

    public override String GetName()
    {
      return "Byte";
    }
  };

  public class CTTMatrixDouble : CTTMatrix
  {
    public CTTMatrixDouble(Double[,] values)
    {
      FType = TYPE_MATRIX_DOUBLE;
      FSize0 = values.GetLength(0);
      FSize1 = values.GetLength(1);
      FValues = new String[2 + FSize0 * FSize1];
      FValues[0] = String.Format("{0}", FSize0);
      FValues[1] = String.Format("{0}", FSize1);
      Int32 SI = 2;
      for (Int32 V0I = 0; V0I < FSize0; V0I++)
      {
        for (Int32 V1I = 0; V1I < FSize1; V1I++)
        {
          FValues[SI] = String.Format("{0}", (Double)values[V0I, V1I]);
          SI++;
        }
      }
    }

    public override String GetName()
    {
      return "Double";
    }
  };


 //////FValues[0] = new String.Format("{0:0}", (Char)value);
  //!!!!!!!!!!!!!!!!!!FValues[1] = (Byte)((0xFF00 & value) >> 8);


  ////////////////public class CTagText : List<String>
  ////////////////{
  ////////////////  public const Int32 INDEX_HEADER = 0;
  ////////////////  public const Int32 INDEX_VALUE0 = 1;
  ////////////////  public const Int32 INDEX_VALUE1 = 2;
  ////////////////  public const Int32 INDEX_VALUE2 = 3;
  ////////////////  public const Int32 INDEX_VALUE3 = 4;

  ////////////////  public String GetHeader()
  ////////////////  {
  ////////////////    if (0 < Count)
  ////////////////    {
  ////////////////      return base[0];
  ////////////////    }
  ////////////////    return "";
  ////////////////  }
  ////////////////  public String GetValue(Int32 index)
  ////////////////  {
  ////////////////    if ((0 <= index) && (index < Count))
  ////////////////    {
  ////////////////      return base[index];
  ////////////////    }
  ////////////////    return "";
  ////////////////  }

  ////////////////  public Boolean Build(String text)
  ////////////////  {
  ////////////////    String SBuffer = "";
  ////////////////    foreach (Char C in text)
  ////////////////    {
  ////////////////      switch (C)
  ////////////////      {
  ////////////////        case '[':
  ////////////////          if (0 < SBuffer.Length)
  ////////////////          {
  ////////////////            base.Add(SBuffer);
  ////////////////          }
  ////////////////          SBuffer = "";
  ////////////////          break;
  ////////////////        case ']':
  ////////////////          base.Add(SBuffer);
  ////////////////          SBuffer = "";
  ////////////////          break;
  ////////////////        default:
  ////////////////          SBuffer += C;
  ////////////////          break;
  ////////////////      }
  ////////////////    }
  ////////////////    if (0 < SBuffer.Length)
  ////////////////    {
  ////////////////      base.Add(SBuffer);
  ////////////////    }
  ////////////////    return true;
  ////////////////  }
  ////////////////}

  ////////////////public class CTagTextList : List<CTagText>
  ////////////////{
  ////////////////  public Boolean Build(String datagram)
  ////////////////  {
  ////////////////    String SBuffer = "";
  ////////////////    foreach (Char C in datagram)
  ////////////////    {
  ////////////////      switch (C)
  ////////////////      {
  ////////////////        case ';':
  ////////////////          CTagText Tag = new CTagText();
  ////////////////          if (Tag.Build(SBuffer))
  ////////////////          {
  ////////////////            base.Add(Tag);
  ////////////////          }
  ////////////////          SBuffer = "";
  ////////////////          break;
  ////////////////        default:
  ////////////////          SBuffer += C;
  ////////////////          break;
  ////////////////      }
  ////////////////    }
  ////////////////    return true;
  ////////////////  }
  ////////////////}

}
