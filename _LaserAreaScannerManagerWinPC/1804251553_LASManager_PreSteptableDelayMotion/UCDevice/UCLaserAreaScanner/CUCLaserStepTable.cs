﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCLaserAreaScanner
{
  public partial class CUCLaserStepTable : UserControl
  {
    public const int XLOCATION = 0;
    public const int YLOCATION = 1;
    public const int PULSEPERIODMS = 2;
    public const int PULSECOUNT = 3;

    public CUCLaserStepTable()
    {
      InitializeComponent();
    }

    public void ClearAllSteps()
    {
      dgvSteptable.Rows.Clear();
    }

    public void PassivateRefresh()
    {
      dgvSteptable.SuspendLayout();
    }
    public void ActivateRefresh()
    {
      dgvSteptable.ResumeLayout();
    }

    public void SetLaserPoints(UInt32 rowcount, UInt32 colcount, UInt16[,] laserpoints)
    {
      try
      {
        Int32 LPS = laserpoints.GetLength(0);
        if (0 < LPS)
        {
          dgvSteptable.Rows.Clear();
          dgvSteptable.SuspendLayout();
          for (Int32 PI = 0; PI < LPS; PI++)
          {
            UInt16 PX = laserpoints[PI, 0];
            UInt16 PY = laserpoints[PI, 1];
            UInt16 PP = laserpoints[PI, 2];
            UInt16 PC = laserpoints[PI, 3];
            dgvSteptable.Rows.Add(String.Format("{0}", PI),
                                  String.Format("{0}", PX),
                                  String.Format("{0}", PY),
                                  String.Format("{0}", PP),
                                  String.Format("{0}", PC));
          }
          dgvSteptable.ResumeLayout();          
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
      }
    }


    public void AddStep(UInt16 xposition, UInt16 yposition,
                        UInt16 pulseperiod, UInt16 pulsecount)
    {
      dgvSteptable.Rows.Add(String.Format("{0}", dgvSteptable.RowCount),
                            String.Format("{0}", xposition),
                            String.Format("{0}", yposition),
                            String.Format("{0}", pulseperiod),
                            String.Format("{0}", pulsecount));
    }

    public void AddStepRange(DataGridViewRow steptable)
    {
      dgvSteptable.Rows.AddRange(steptable);
    }

    private delegate void CBSelectLaserStep(UInt32 index);
    public void SelectLaserStep(UInt32 index)
    {
      if (this.InvokeRequired)
      {
        CBSelectLaserStep CB = new CBSelectLaserStep(SelectLaserStep);
        Invoke(CB, new object[] { index });
      }
      else
      {
        if (index < dgvSteptable.RowCount)
        {
          dgvSteptable.Rows[(int)index].Selected = true;
          //dgvSteptable.FirstDisplayedScrollingRowIndex = dgvSteptable.SelectedRows[0].Index;
          dgvSteptable.FirstDisplayedScrollingRowIndex = (int)index;
        }
      }        
    }

  }
}