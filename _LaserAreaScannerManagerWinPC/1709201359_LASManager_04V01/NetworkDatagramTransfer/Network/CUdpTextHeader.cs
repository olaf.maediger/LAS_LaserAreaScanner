﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Network
{
  public class CUdpTextHeader
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const String TOKEN_PROTOCOL = "Protocol";
    public const String TOKEN_TIMESTAMP = "TimeStamp";
    public const String TOKEN_SOURCEIPADDRESS = "SourceIPAddress";
    public const String TOKEN_SOURCETYPE = "SourceType";
    public const String TOKEN_SOURCEID = "SourceID";
    public const String TOKEN_SOURCENAME = "SourceName";
    public const String TOKEN_TARGETIPADDRESS = "TargetIPAddress";
    public const String TOKEN_TARGETTYPE = "TargetType";
    public const String TOKEN_TARGETID = "TargetID";
    public const String TOKEN_TARGETNAME = "Targetname";
    public const String TOKEN_DATAGRAMTYPE = "DatagramType";
    public const String TOKEN_DATAGRAMKEY = "DatagramKey";
    //
    public const String FORMAT_PROTOCOL = TOKEN_PROTOCOL + "[{0}]";
    public const String FORMAT_TIMESTAMP = TOKEN_TIMESTAMP + "[{0}]";
    public const String FORMAT_SOURCEIPADDRESS = TOKEN_SOURCEIPADDRESS + "[{0}]";
    public const String FORMAT_SOURCETYPE = TOKEN_SOURCETYPE + "[{0}]";
    public const String FORMAT_SOURCEID = TOKEN_SOURCEID + "[{0}]";
    public const String FORMAT_SOURCENAME = TOKEN_SOURCENAME + "[{0}]";
    public const String FORMAT_TARGETIPADDRESS = TOKEN_TARGETIPADDRESS + "[{0}]";
    public const String FORMAT_TARGETTYPE = TOKEN_TARGETTYPE + "[{0}]";
    public const String FORMAT_TARGETID = TOKEN_TARGETID + "[{0}]";
    public const String FORMAT_TARGETNAME = TOKEN_TARGETNAME + "[{0}]";
    public const String FORMAT_DATAGRAMTYPE = TOKEN_DATAGRAMTYPE + "[{0}]";
    public const String FORMAT_DATAGRAMKEY = TOKEN_DATAGRAMKEY + "[{0}]";
  }

  public struct SUdpTextDatagram
  {
    public String Protocol;
    public String TimeStamp;
    public String SourceIpAddress;
    public String SourceID;
    public String SourceType;
    public String SourceName;
    public String TargetIpAddress;
    public String TargetID;
    public String TargetType;
    public String TargetName;
    public String DatagramType;
    public String DatagramKey;
    public CTagTextList Content;
  };


}
