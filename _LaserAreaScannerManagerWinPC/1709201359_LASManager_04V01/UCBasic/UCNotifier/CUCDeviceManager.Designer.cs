﻿namespace UCNotifier
{
  partial class CUCDeviceManager
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlKeys = new System.Windows.Forms.Panel();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnClose = new System.Windows.Forms.Button();
      this.btnEditDevice = new System.Windows.Forms.Button();
      this.btnDeleteDevice = new System.Windows.Forms.Button();
      this.btnAddDevice = new System.Windows.Forms.Button();
      this.pnlTop = new System.Windows.Forms.Panel();
      this.label3 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.DialogSaveToXmlFile = new System.Windows.Forms.SaveFileDialog();
      this.DialogLoadFromXmlFile = new System.Windows.Forms.OpenFileDialog();
      this.FUCListBoxDevices = new UCNotifier.CUCListBox();
      this.FUCDeleteDevice = new UCNotifier.CUCDevice();
      this.FUCEditDevice = new UCNotifier.CUCDevice();
      this.FUCAddDevice = new UCNotifier.CUCDevice();
      this.pnlKeys.SuspendLayout();
      this.pnlTop.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlKeys
      // 
      this.pnlKeys.Controls.Add(this.btnCancel);
      this.pnlKeys.Controls.Add(this.btnClose);
      this.pnlKeys.Controls.Add(this.btnEditDevice);
      this.pnlKeys.Controls.Add(this.btnDeleteDevice);
      this.pnlKeys.Controls.Add(this.btnAddDevice);
      this.pnlKeys.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlKeys.Location = new System.Drawing.Point(0, 587);
      this.pnlKeys.Name = "pnlKeys";
      this.pnlKeys.Size = new System.Drawing.Size(598, 34);
      this.pnlKeys.TabIndex = 17;
      this.pnlKeys.Resize += new System.EventHandler(this.pnlKeys_Resize);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(429, 6);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 6;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnClose
      // 
      this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnClose.Location = new System.Drawing.Point(348, 6);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(75, 23);
      this.btnClose.TabIndex = 5;
      this.btnClose.Text = "Close";
      this.btnClose.UseVisualStyleBackColor = true;
      this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // btnEditDevice
      // 
      this.btnEditDevice.Location = new System.Drawing.Point(185, 6);
      this.btnEditDevice.Name = "btnEditDevice";
      this.btnEditDevice.Size = new System.Drawing.Size(75, 23);
      this.btnEditDevice.TabIndex = 2;
      this.btnEditDevice.Text = "Edit";
      this.btnEditDevice.UseVisualStyleBackColor = true;
      this.btnEditDevice.Click += new System.EventHandler(this.btnEditDevice_Click);
      // 
      // btnDeleteDevice
      // 
      this.btnDeleteDevice.Location = new System.Drawing.Point(266, 6);
      this.btnDeleteDevice.Name = "btnDeleteDevice";
      this.btnDeleteDevice.Size = new System.Drawing.Size(75, 23);
      this.btnDeleteDevice.TabIndex = 3;
      this.btnDeleteDevice.Text = "Delete";
      this.btnDeleteDevice.UseVisualStyleBackColor = true;
      this.btnDeleteDevice.Click += new System.EventHandler(this.btnDeleteDevice_Click);
      // 
      // btnAddDevice
      // 
      this.btnAddDevice.Location = new System.Drawing.Point(104, 6);
      this.btnAddDevice.Name = "btnAddDevice";
      this.btnAddDevice.Size = new System.Drawing.Size(75, 23);
      this.btnAddDevice.TabIndex = 1;
      this.btnAddDevice.Text = "Add";
      this.btnAddDevice.UseVisualStyleBackColor = true;
      this.btnAddDevice.Click += new System.EventHandler(this.btnAddDevice_Click);
      // 
      // pnlTop
      // 
      this.pnlTop.Controls.Add(this.label3);
      this.pnlTop.Controls.Add(this.label11);
      this.pnlTop.Controls.Add(this.label4);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(598, 24);
      this.pnlTop.TabIndex = 25;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 6);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(72, 13);
      this.label3.TabIndex = 153;
      this.label3.Text = "Name Device";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(211, 6);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(84, 13);
      this.label11.TabIndex = 152;
      this.label11.Text = "|   Serial-Number";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(321, 6);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(76, 13);
      this.label4.TabIndex = 151;
      this.label4.Text = "|   Product-Key";
      // 
      // DialogSaveToXmlFile
      // 
      this.DialogSaveToXmlFile.DefaultExt = "dev.xml";
      this.DialogSaveToXmlFile.FileName = "Device";
      this.DialogSaveToXmlFile.Filter = "Device-Data Xml files (*.dev.xml)|*.dev.xml|All Xml files (*.xml)|*.xml";
      this.DialogSaveToXmlFile.Title = "Save Device-Data to Xml-File";
      // 
      // DialogLoadFromXmlFile
      // 
      this.DialogLoadFromXmlFile.DefaultExt = "dev.xml";
      this.DialogLoadFromXmlFile.FileName = "Device";
      this.DialogLoadFromXmlFile.Filter = "Application-Data Xml files (*.dev.xml)|*.dev.xml|All Xml files (*.xml)|*.xml";
      this.DialogLoadFromXmlFile.Title = "Load Device-Data from Xml-File";
      // 
      // FUCListBoxDevices
      // 
      this.FUCListBoxDevices.BackColor = System.Drawing.SystemColors.Info;
      this.FUCListBoxDevices.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCListBoxDevices.Font = new System.Drawing.Font("Courier New", 12F);
      this.FUCListBoxDevices.FormattingEnabled = true;
      this.FUCListBoxDevices.IntegralHeight = false;
      this.FUCListBoxDevices.ItemHeight = 18;
      this.FUCListBoxDevices.Location = new System.Drawing.Point(0, 24);
      this.FUCListBoxDevices.Name = "FUCListBoxDevices";
      this.FUCListBoxDevices.Size = new System.Drawing.Size(598, 80);
      this.FUCListBoxDevices.TabIndex = 40;
      this.FUCListBoxDevices.SelectedIndexChanged += new System.EventHandler(this.FUCListBoxDevices_SelectedIndexChanged);
      // 
      // FUCDeleteDevice
      // 
      this.FUCDeleteDevice.ConfirmTitle = "Delete";
      this.FUCDeleteDevice.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.FUCDeleteDevice.Location = new System.Drawing.Point(0, 104);
      this.FUCDeleteDevice.ModeDelete = true;
      this.FUCDeleteDevice.Name = "FUCDeleteDevice";
      this.FUCDeleteDevice.Size = new System.Drawing.Size(598, 161);
      this.FUCDeleteDevice.TabIndex = 28;
      this.FUCDeleteDevice.Title = " Delete Device ";
      // 
      // FUCEditDevice
      // 
      this.FUCEditDevice.ConfirmTitle = "Confirm";
      this.FUCEditDevice.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.FUCEditDevice.Location = new System.Drawing.Point(0, 265);
      this.FUCEditDevice.ModeDelete = false;
      this.FUCEditDevice.Name = "FUCEditDevice";
      this.FUCEditDevice.Size = new System.Drawing.Size(598, 161);
      this.FUCEditDevice.TabIndex = 27;
      this.FUCEditDevice.Title = " Edit Device ";
      // 
      // FUCAddDevice
      // 
      this.FUCAddDevice.ConfirmTitle = "Add";
      this.FUCAddDevice.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.FUCAddDevice.Location = new System.Drawing.Point(0, 426);
      this.FUCAddDevice.ModeDelete = false;
      this.FUCAddDevice.Name = "FUCAddDevice";
      this.FUCAddDevice.Size = new System.Drawing.Size(598, 161);
      this.FUCAddDevice.TabIndex = 26;
      this.FUCAddDevice.Title = " Add Device ";
      // 
      // CUCDeviceManager
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCListBoxDevices);
      this.Controls.Add(this.FUCDeleteDevice);
      this.Controls.Add(this.FUCEditDevice);
      this.Controls.Add(this.FUCAddDevice);
      this.Controls.Add(this.pnlTop);
      this.Controls.Add(this.pnlKeys);
      this.Name = "CUCDeviceManager";
      this.Size = new System.Drawing.Size(598, 621);
      this.pnlKeys.ResumeLayout(false);
      this.pnlTop.ResumeLayout(false);
      this.pnlTop.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlKeys;
    private System.Windows.Forms.Button btnEditDevice;
    private System.Windows.Forms.Button btnDeleteDevice;
    private System.Windows.Forms.Button btnAddDevice;
    // NC private CUCDevice FUCDeleteDevice;
    // NC private CUCDevice FUCEditDevice;
    // NC private CUCDevice FUCAddDevice;
    private System.Windows.Forms.Panel pnlTop;
    // NC private CUCListBox FUCListBoxDevices;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.SaveFileDialog DialogSaveToXmlFile;
    private System.Windows.Forms.OpenFileDialog DialogLoadFromXmlFile;
    private CUCDevice FUCAddDevice;
    private CUCDevice FUCEditDevice;
    private CUCDevice FUCDeleteDevice;
    private CUCListBox FUCListBoxDevices;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnClose;
  }
}
