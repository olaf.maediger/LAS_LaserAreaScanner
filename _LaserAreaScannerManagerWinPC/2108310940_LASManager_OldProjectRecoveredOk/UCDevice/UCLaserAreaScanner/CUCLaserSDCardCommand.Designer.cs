﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserSDCardCommand
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnSetRangeY = new System.Windows.Forms.Button();
      this.btnSetRangeX = new System.Windows.Forms.Button();
      this.nudPositionYDelta = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.nudPositionXDelta = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.nudPositionYMaximum = new System.Windows.Forms.NumericUpDown();
      this.nudPositionYMinimum = new System.Windows.Forms.NumericUpDown();
      this.nudPositionXMaximum = new System.Windows.Forms.NumericUpDown();
      this.nudPositionXMinimum = new System.Windows.Forms.NumericUpDown();
      this.lblHeader = new System.Windows.Forms.Label();
      this.btnSetPositionY = new System.Windows.Forms.Button();
      this.btnSetPositionX = new System.Windows.Forms.Button();
      this.label8 = new System.Windows.Forms.Label();
      this.nudPositionYActual = new System.Windows.Forms.NumericUpDown();
      this.nudPositionXActual = new System.Windows.Forms.NumericUpDown();
      this.btnSetDelayMotionms = new System.Windows.Forms.Button();
      this.nudDelayMotionms = new System.Windows.Forms.NumericUpDown();
      this.btnSetPulsePeriodms = new System.Windows.Forms.Button();
      this.nudPulseCount = new System.Windows.Forms.NumericUpDown();
      this.nudPulsePeriodms = new System.Windows.Forms.NumericUpDown();
      this.btnSetPulseCount = new System.Windows.Forms.Button();
      this.btnSetPulseWidthus = new System.Windows.Forms.Button();
      this.nudPulseWidthus = new System.Windows.Forms.NumericUpDown();
      this.btnPulseLaserRow = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.button1 = new System.Windows.Forms.Button();
      this.tbxCommandFile = new System.Windows.Forms.TextBox();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.button2 = new System.Windows.Forms.Button();
      this.button3 = new System.Windows.Forms.Button();
      this.button4 = new System.Windows.Forms.Button();
      this.label5 = new System.Windows.Forms.Label();
      this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
      this.label6 = new System.Windows.Forms.Label();
      this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
      this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
      this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
      this.label7 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
      this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
      this.label10 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.button5 = new System.Windows.Forms.Button();
      this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
      this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
      this.label12 = new System.Windows.Forms.Label();
      this.label13 = new System.Windows.Forms.Label();
      this.button6 = new System.Windows.Forms.Button();
      this.numericUpDown9 = new System.Windows.Forms.NumericUpDown();
      this.numericUpDown10 = new System.Windows.Forms.NumericUpDown();
      this.label14 = new System.Windows.Forms.Label();
      this.label15 = new System.Windows.Forms.Label();
      this.button7 = new System.Windows.Forms.Button();
      this.numericUpDown11 = new System.Windows.Forms.NumericUpDown();
      this.numericUpDown12 = new System.Windows.Forms.NumericUpDown();
      this.numericUpDown13 = new System.Windows.Forms.NumericUpDown();
      this.numericUpDown14 = new System.Windows.Forms.NumericUpDown();
      this.label16 = new System.Windows.Forms.Label();
      this.label17 = new System.Windows.Forms.Label();
      this.label18 = new System.Windows.Forms.Label();
      this.button8 = new System.Windows.Forms.Button();
      this.label19 = new System.Windows.Forms.Label();
      this.numericUpDown15 = new System.Windows.Forms.NumericUpDown();
      this.label20 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYDelta)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXDelta)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYActual)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXActual)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayMotionms)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriodms)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseWidthus)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown13)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown14)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown15)).BeginInit();
      this.SuspendLayout();
      // 
      // btnSetRangeY
      // 
      this.btnSetRangeY.Location = new System.Drawing.Point(6, 185);
      this.btnSetRangeY.Name = "btnSetRangeY";
      this.btnSetRangeY.Size = new System.Drawing.Size(120, 24);
      this.btnSetRangeY.TabIndex = 178;
      this.btnSetRangeY.Text = "SetRangeY [stp]";
      this.btnSetRangeY.UseVisualStyleBackColor = true;
      // 
      // btnSetRangeX
      // 
      this.btnSetRangeX.Location = new System.Drawing.Point(6, 157);
      this.btnSetRangeX.Name = "btnSetRangeX";
      this.btnSetRangeX.Size = new System.Drawing.Size(120, 24);
      this.btnSetRangeX.TabIndex = 177;
      this.btnSetRangeX.Text = "SetRangeX [stp]";
      this.btnSetRangeX.UseVisualStyleBackColor = true;
      // 
      // nudPositionYDelta
      // 
      this.nudPositionYDelta.Location = new System.Drawing.Point(256, 188);
      this.nudPositionYDelta.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYDelta.Name = "nudPositionYDelta";
      this.nudPositionYDelta.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYDelta.TabIndex = 174;
      this.nudPositionYDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYDelta.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(260, 146);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(51, 13);
      this.label4.TabIndex = 171;
      this.label4.Text = "Delta";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPositionXDelta
      // 
      this.nudPositionXDelta.Location = new System.Drawing.Point(256, 162);
      this.nudPositionXDelta.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXDelta.Name = "nudPositionXDelta";
      this.nudPositionXDelta.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXDelta.TabIndex = 170;
      this.nudPositionXDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXDelta.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(199, 146);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(51, 13);
      this.label3.TabIndex = 169;
      this.label3.Text = "Maximum";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(139, 146);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(51, 13);
      this.label2.TabIndex = 167;
      this.label2.Text = "Minimum";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPositionYMaximum
      // 
      this.nudPositionYMaximum.Location = new System.Drawing.Point(196, 187);
      this.nudPositionYMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYMaximum.Name = "nudPositionYMaximum";
      this.nudPositionYMaximum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYMaximum.TabIndex = 173;
      this.nudPositionYMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // nudPositionYMinimum
      // 
      this.nudPositionYMinimum.Location = new System.Drawing.Point(136, 187);
      this.nudPositionYMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYMinimum.Name = "nudPositionYMinimum";
      this.nudPositionYMinimum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYMinimum.TabIndex = 172;
      this.nudPositionYMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // nudPositionXMaximum
      // 
      this.nudPositionXMaximum.Location = new System.Drawing.Point(196, 161);
      this.nudPositionXMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXMaximum.Name = "nudPositionXMaximum";
      this.nudPositionXMaximum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXMaximum.TabIndex = 168;
      this.nudPositionXMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // nudPositionXMinimum
      // 
      this.nudPositionXMinimum.Location = new System.Drawing.Point(136, 161);
      this.nudPositionXMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXMinimum.Name = "nudPositionXMinimum";
      this.nudPositionXMinimum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXMinimum.TabIndex = 166;
      this.nudPositionXMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(651, 23);
      this.lblHeader.TabIndex = 179;
      this.lblHeader.Text = "LaserAreaScanner - LaserSDCardCommand";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnSetPositionY
      // 
      this.btnSetPositionY.Location = new System.Drawing.Point(6, 113);
      this.btnSetPositionY.Name = "btnSetPositionY";
      this.btnSetPositionY.Size = new System.Drawing.Size(120, 24);
      this.btnSetPositionY.TabIndex = 187;
      this.btnSetPositionY.Text = "SetPositionY [stp]";
      this.btnSetPositionY.UseVisualStyleBackColor = true;
      // 
      // btnSetPositionX
      // 
      this.btnSetPositionX.Location = new System.Drawing.Point(6, 87);
      this.btnSetPositionX.Name = "btnSetPositionX";
      this.btnSetPositionX.Size = new System.Drawing.Size(120, 24);
      this.btnSetPositionX.TabIndex = 186;
      this.btnSetPositionX.Text = "SetPositionX [stp]";
      this.btnSetPositionX.UseVisualStyleBackColor = true;
      // 
      // label8
      // 
      this.label8.Location = new System.Drawing.Point(133, 66);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(58, 13);
      this.label8.TabIndex = 185;
      this.label8.Text = "Position";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPositionYActual
      // 
      this.nudPositionYActual.Location = new System.Drawing.Point(137, 115);
      this.nudPositionYActual.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYActual.Name = "nudPositionYActual";
      this.nudPositionYActual.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYActual.TabIndex = 184;
      this.nudPositionYActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYActual.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // nudPositionXActual
      // 
      this.nudPositionXActual.Location = new System.Drawing.Point(137, 89);
      this.nudPositionXActual.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXActual.Name = "nudPositionXActual";
      this.nudPositionXActual.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXActual.TabIndex = 181;
      this.nudPositionXActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXActual.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // btnSetDelayMotionms
      // 
      this.btnSetDelayMotionms.Location = new System.Drawing.Point(331, 168);
      this.btnSetDelayMotionms.Name = "btnSetDelayMotionms";
      this.btnSetDelayMotionms.Size = new System.Drawing.Size(120, 24);
      this.btnSetDelayMotionms.TabIndex = 189;
      this.btnSetDelayMotionms.Text = "SetDelayMotion [ms]";
      this.btnSetDelayMotionms.UseVisualStyleBackColor = true;
      // 
      // nudDelayMotionms
      // 
      this.nudDelayMotionms.Location = new System.Drawing.Point(462, 171);
      this.nudDelayMotionms.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
      this.nudDelayMotionms.Name = "nudDelayMotionms";
      this.nudDelayMotionms.Size = new System.Drawing.Size(55, 20);
      this.nudDelayMotionms.TabIndex = 188;
      this.nudDelayMotionms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelayMotionms.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      // 
      // btnSetPulsePeriodms
      // 
      this.btnSetPulsePeriodms.Location = new System.Drawing.Point(331, 112);
      this.btnSetPulsePeriodms.Name = "btnSetPulsePeriodms";
      this.btnSetPulsePeriodms.Size = new System.Drawing.Size(120, 24);
      this.btnSetPulsePeriodms.TabIndex = 195;
      this.btnSetPulsePeriodms.Text = "SetPulsePeriod [ms]";
      this.btnSetPulsePeriodms.UseVisualStyleBackColor = true;
      // 
      // nudPulseCount
      // 
      this.nudPulseCount.Location = new System.Drawing.Point(462, 87);
      this.nudPulseCount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudPulseCount.Name = "nudPulseCount";
      this.nudPulseCount.Size = new System.Drawing.Size(55, 20);
      this.nudPulseCount.TabIndex = 194;
      this.nudPulseCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulseCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // nudPulsePeriodms
      // 
      this.nudPulsePeriodms.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudPulsePeriodms.Location = new System.Drawing.Point(462, 114);
      this.nudPulsePeriodms.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudPulsePeriodms.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPulsePeriodms.Name = "nudPulsePeriodms";
      this.nudPulsePeriodms.Size = new System.Drawing.Size(55, 20);
      this.nudPulsePeriodms.TabIndex = 193;
      this.nudPulsePeriodms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulsePeriodms.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // btnSetPulseCount
      // 
      this.btnSetPulseCount.Location = new System.Drawing.Point(331, 85);
      this.btnSetPulseCount.Name = "btnSetPulseCount";
      this.btnSetPulseCount.Size = new System.Drawing.Size(120, 24);
      this.btnSetPulseCount.TabIndex = 192;
      this.btnSetPulseCount.Text = "SetPulseCount [1]";
      this.btnSetPulseCount.UseVisualStyleBackColor = true;
      // 
      // btnSetPulseWidthus
      // 
      this.btnSetPulseWidthus.Location = new System.Drawing.Point(331, 140);
      this.btnSetPulseWidthus.Name = "btnSetPulseWidthus";
      this.btnSetPulseWidthus.Size = new System.Drawing.Size(120, 24);
      this.btnSetPulseWidthus.TabIndex = 191;
      this.btnSetPulseWidthus.Text = "SetPulseWidth [us]";
      this.btnSetPulseWidthus.UseVisualStyleBackColor = true;
      // 
      // nudPulseWidthus
      // 
      this.nudPulseWidthus.Location = new System.Drawing.Point(462, 142);
      this.nudPulseWidthus.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
      this.nudPulseWidthus.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPulseWidthus.Name = "nudPulseWidthus";
      this.nudPulseWidthus.Size = new System.Drawing.Size(75, 20);
      this.nudPulseWidthus.TabIndex = 190;
      this.nudPulseWidthus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulseWidthus.Value = new decimal(new int[] {
            500000,
            0,
            0,
            0});
      // 
      // btnPulseLaserRow
      // 
      this.btnPulseLaserRow.Location = new System.Drawing.Point(6, 34);
      this.btnPulseLaserRow.Name = "btnPulseLaserRow";
      this.btnPulseLaserRow.Size = new System.Drawing.Size(159, 25);
      this.btnPulseLaserRow.TabIndex = 196;
      this.btnPulseLaserRow.Text = "Open CommandFile for writing";
      this.btnPulseLaserRow.UseVisualStyleBackColor = true;
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(3, 62);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(129, 21);
      this.label1.TabIndex = 199;
      this.label1.Text = "Write Command to File";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(328, 34);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(159, 25);
      this.button1.TabIndex = 200;
      this.button1.Text = "Close CommandFile";
      this.button1.UseVisualStyleBackColor = true;
      // 
      // tbxCommandFile
      // 
      this.tbxCommandFile.Location = new System.Drawing.Point(170, 37);
      this.tbxCommandFile.Name = "tbxCommandFile";
      this.tbxCommandFile.Size = new System.Drawing.Size(153, 20);
      this.tbxCommandFile.TabIndex = 201;
      this.tbxCommandFile.Text = "command.txt";
      this.tbxCommandFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // textBox1
      // 
      this.textBox1.Location = new System.Drawing.Point(170, 481);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(153, 20);
      this.textBox1.TabIndex = 203;
      this.textBox1.Text = "command.txt";
      this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // button2
      // 
      this.button2.Location = new System.Drawing.Point(6, 478);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(159, 25);
      this.button2.TabIndex = 202;
      this.button2.Text = "Execute CommandFile";
      this.button2.UseVisualStyleBackColor = true;
      // 
      // button3
      // 
      this.button3.Location = new System.Drawing.Point(328, 478);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(159, 25);
      this.button3.TabIndex = 204;
      this.button3.Text = "Abort CommandFile";
      this.button3.UseVisualStyleBackColor = true;
      // 
      // button4
      // 
      this.button4.Location = new System.Drawing.Point(6, 326);
      this.button4.Name = "button4";
      this.button4.Size = new System.Drawing.Size(120, 24);
      this.button4.TabIndex = 207;
      this.button4.Text = "Pulse Laser Position";
      this.button4.UseVisualStyleBackColor = true;
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(130, 332);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(38, 13);
      this.label5.TabIndex = 206;
      this.label5.Text = "X [stp]";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // numericUpDown1
      // 
      this.numericUpDown1.Location = new System.Drawing.Point(168, 330);
      this.numericUpDown1.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.numericUpDown1.Name = "numericUpDown1";
      this.numericUpDown1.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown1.TabIndex = 205;
      this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown1.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // label6
      // 
      this.label6.Location = new System.Drawing.Point(229, 332);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(38, 13);
      this.label6.TabIndex = 209;
      this.label6.Text = "Y [stp]";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // numericUpDown2
      // 
      this.numericUpDown2.Location = new System.Drawing.Point(267, 330);
      this.numericUpDown2.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.numericUpDown2.Name = "numericUpDown2";
      this.numericUpDown2.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown2.TabIndex = 208;
      this.numericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown2.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // numericUpDown3
      // 
      this.numericUpDown3.Location = new System.Drawing.Point(459, 330);
      this.numericUpDown3.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.numericUpDown3.Name = "numericUpDown3";
      this.numericUpDown3.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown3.TabIndex = 211;
      this.numericUpDown3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // numericUpDown4
      // 
      this.numericUpDown4.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.numericUpDown4.Location = new System.Drawing.Point(364, 330);
      this.numericUpDown4.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.numericUpDown4.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericUpDown4.Name = "numericUpDown4";
      this.numericUpDown4.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown4.TabIndex = 210;
      this.numericUpDown4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown4.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label7
      // 
      this.label7.Location = new System.Drawing.Point(328, 332);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(36, 13);
      this.label7.TabIndex = 212;
      this.label7.Text = "P [ms]";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label9
      // 
      this.label9.Location = new System.Drawing.Point(425, 332);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(38, 13);
      this.label9.TabIndex = 213;
      this.label9.Text = "C [1]";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // numericUpDown5
      // 
      this.numericUpDown5.Location = new System.Drawing.Point(267, 358);
      this.numericUpDown5.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.numericUpDown5.Name = "numericUpDown5";
      this.numericUpDown5.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown5.TabIndex = 220;
      this.numericUpDown5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // numericUpDown6
      // 
      this.numericUpDown6.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.numericUpDown6.Location = new System.Drawing.Point(168, 358);
      this.numericUpDown6.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.numericUpDown6.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericUpDown6.Name = "numericUpDown6";
      this.numericUpDown6.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown6.TabIndex = 219;
      this.numericUpDown6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown6.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label10
      // 
      this.label10.Location = new System.Drawing.Point(233, 360);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(38, 13);
      this.label10.TabIndex = 222;
      this.label10.Text = "C [1]";
      this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label11
      // 
      this.label11.Location = new System.Drawing.Point(132, 360);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(36, 13);
      this.label11.TabIndex = 221;
      this.label11.Text = "P [ms]";
      this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // button5
      // 
      this.button5.Location = new System.Drawing.Point(6, 354);
      this.button5.Name = "button5";
      this.button5.Size = new System.Drawing.Size(120, 24);
      this.button5.TabIndex = 216;
      this.button5.Text = "Pulse Laser Row";
      this.button5.UseVisualStyleBackColor = true;
      // 
      // numericUpDown7
      // 
      this.numericUpDown7.Location = new System.Drawing.Point(267, 386);
      this.numericUpDown7.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.numericUpDown7.Name = "numericUpDown7";
      this.numericUpDown7.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown7.TabIndex = 225;
      this.numericUpDown7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown7.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // numericUpDown8
      // 
      this.numericUpDown8.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.numericUpDown8.Location = new System.Drawing.Point(168, 386);
      this.numericUpDown8.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.numericUpDown8.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericUpDown8.Name = "numericUpDown8";
      this.numericUpDown8.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown8.TabIndex = 224;
      this.numericUpDown8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown8.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label12
      // 
      this.label12.Location = new System.Drawing.Point(233, 388);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(38, 13);
      this.label12.TabIndex = 227;
      this.label12.Text = "C [1]";
      this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label13
      // 
      this.label13.Location = new System.Drawing.Point(132, 388);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(36, 13);
      this.label13.TabIndex = 226;
      this.label13.Text = "P [ms]";
      this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // button6
      // 
      this.button6.Location = new System.Drawing.Point(6, 382);
      this.button6.Name = "button6";
      this.button6.Size = new System.Drawing.Size(120, 24);
      this.button6.TabIndex = 223;
      this.button6.Text = "Pulse Laser Col";
      this.button6.UseVisualStyleBackColor = true;
      // 
      // numericUpDown9
      // 
      this.numericUpDown9.Location = new System.Drawing.Point(267, 414);
      this.numericUpDown9.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.numericUpDown9.Name = "numericUpDown9";
      this.numericUpDown9.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown9.TabIndex = 230;
      this.numericUpDown9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown9.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // numericUpDown10
      // 
      this.numericUpDown10.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.numericUpDown10.Location = new System.Drawing.Point(168, 414);
      this.numericUpDown10.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.numericUpDown10.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericUpDown10.Name = "numericUpDown10";
      this.numericUpDown10.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown10.TabIndex = 229;
      this.numericUpDown10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown10.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label14
      // 
      this.label14.Location = new System.Drawing.Point(233, 416);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(38, 13);
      this.label14.TabIndex = 232;
      this.label14.Text = "C [1]";
      this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label15
      // 
      this.label15.Location = new System.Drawing.Point(132, 416);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(36, 13);
      this.label15.TabIndex = 231;
      this.label15.Text = "P [ms]";
      this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // button7
      // 
      this.button7.Location = new System.Drawing.Point(6, 410);
      this.button7.Name = "button7";
      this.button7.Size = new System.Drawing.Size(120, 24);
      this.button7.TabIndex = 228;
      this.button7.Text = "Pulse Laser Matrix";
      this.button7.UseVisualStyleBackColor = true;
      // 
      // numericUpDown11
      // 
      this.numericUpDown11.Location = new System.Drawing.Point(459, 442);
      this.numericUpDown11.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.numericUpDown11.Name = "numericUpDown11";
      this.numericUpDown11.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown11.TabIndex = 239;
      this.numericUpDown11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown11.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // numericUpDown12
      // 
      this.numericUpDown12.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.numericUpDown12.Location = new System.Drawing.Point(364, 442);
      this.numericUpDown12.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.numericUpDown12.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericUpDown12.Name = "numericUpDown12";
      this.numericUpDown12.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown12.TabIndex = 238;
      this.numericUpDown12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown12.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // numericUpDown13
      // 
      this.numericUpDown13.Location = new System.Drawing.Point(267, 442);
      this.numericUpDown13.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.numericUpDown13.Name = "numericUpDown13";
      this.numericUpDown13.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown13.TabIndex = 236;
      this.numericUpDown13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown13.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // numericUpDown14
      // 
      this.numericUpDown14.Location = new System.Drawing.Point(168, 442);
      this.numericUpDown14.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.numericUpDown14.Name = "numericUpDown14";
      this.numericUpDown14.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown14.TabIndex = 233;
      this.numericUpDown14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown14.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // label16
      // 
      this.label16.Location = new System.Drawing.Point(425, 444);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(38, 13);
      this.label16.TabIndex = 241;
      this.label16.Text = "C [1]";
      this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label17
      // 
      this.label17.Location = new System.Drawing.Point(328, 444);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(36, 13);
      this.label17.TabIndex = 240;
      this.label17.Text = "P [ms]";
      this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label18
      // 
      this.label18.Location = new System.Drawing.Point(229, 444);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(38, 13);
      this.label18.TabIndex = 237;
      this.label18.Text = "Y [stp]";
      this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // button8
      // 
      this.button8.Location = new System.Drawing.Point(6, 438);
      this.button8.Name = "button8";
      this.button8.Size = new System.Drawing.Size(120, 24);
      this.button8.TabIndex = 235;
      this.button8.Text = "Pulse Laser Image";
      this.button8.UseVisualStyleBackColor = true;
      // 
      // label19
      // 
      this.label19.Location = new System.Drawing.Point(130, 444);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(38, 13);
      this.label19.TabIndex = 234;
      this.label19.Text = "X [stp]";
      this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // numericUpDown15
      // 
      this.numericUpDown15.Location = new System.Drawing.Point(557, 442);
      this.numericUpDown15.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
      this.numericUpDown15.Name = "numericUpDown15";
      this.numericUpDown15.Size = new System.Drawing.Size(55, 20);
      this.numericUpDown15.TabIndex = 242;
      this.numericUpDown15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown15.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      // 
      // label20
      // 
      this.label20.Location = new System.Drawing.Point(520, 444);
      this.label20.Name = "label20";
      this.label20.Size = new System.Drawing.Size(36, 13);
      this.label20.TabIndex = 243;
      this.label20.Text = "P [ms]";
      this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // CUCLaserSDCardCommand
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label20);
      this.Controls.Add(this.numericUpDown15);
      this.Controls.Add(this.numericUpDown11);
      this.Controls.Add(this.numericUpDown12);
      this.Controls.Add(this.numericUpDown13);
      this.Controls.Add(this.numericUpDown14);
      this.Controls.Add(this.label16);
      this.Controls.Add(this.label17);
      this.Controls.Add(this.label18);
      this.Controls.Add(this.button8);
      this.Controls.Add(this.label19);
      this.Controls.Add(this.numericUpDown9);
      this.Controls.Add(this.numericUpDown10);
      this.Controls.Add(this.label14);
      this.Controls.Add(this.label15);
      this.Controls.Add(this.button7);
      this.Controls.Add(this.numericUpDown7);
      this.Controls.Add(this.numericUpDown8);
      this.Controls.Add(this.label12);
      this.Controls.Add(this.label13);
      this.Controls.Add(this.button6);
      this.Controls.Add(this.numericUpDown5);
      this.Controls.Add(this.numericUpDown6);
      this.Controls.Add(this.label10);
      this.Controls.Add(this.label11);
      this.Controls.Add(this.button5);
      this.Controls.Add(this.numericUpDown3);
      this.Controls.Add(this.numericUpDown4);
      this.Controls.Add(this.numericUpDown2);
      this.Controls.Add(this.numericUpDown1);
      this.Controls.Add(this.label9);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.button4);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.button3);
      this.Controls.Add(this.textBox1);
      this.Controls.Add(this.button2);
      this.Controls.Add(this.tbxCommandFile);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.btnPulseLaserRow);
      this.Controls.Add(this.btnSetDelayMotionms);
      this.Controls.Add(this.nudDelayMotionms);
      this.Controls.Add(this.btnSetPulsePeriodms);
      this.Controls.Add(this.nudPulseCount);
      this.Controls.Add(this.nudPulsePeriodms);
      this.Controls.Add(this.btnSetPulseCount);
      this.Controls.Add(this.btnSetPulseWidthus);
      this.Controls.Add(this.nudPulseWidthus);
      this.Controls.Add(this.btnSetPositionY);
      this.Controls.Add(this.btnSetPositionX);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.nudPositionYActual);
      this.Controls.Add(this.nudPositionXActual);
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.btnSetRangeY);
      this.Controls.Add(this.btnSetRangeX);
      this.Controls.Add(this.nudPositionYDelta);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.nudPositionXDelta);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.nudPositionYMaximum);
      this.Controls.Add(this.nudPositionYMinimum);
      this.Controls.Add(this.nudPositionXMaximum);
      this.Controls.Add(this.nudPositionXMinimum);
      this.Name = "CUCLaserSDCardCommand";
      this.Size = new System.Drawing.Size(651, 529);
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYDelta)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXDelta)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYActual)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXActual)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayMotionms)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriodms)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseWidthus)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown12)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown13)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown14)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown15)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnSetRangeY;
    private System.Windows.Forms.Button btnSetRangeX;
    private System.Windows.Forms.NumericUpDown nudPositionYDelta;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.NumericUpDown nudPositionXDelta;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown nudPositionYMaximum;
    private System.Windows.Forms.NumericUpDown nudPositionYMinimum;
    private System.Windows.Forms.NumericUpDown nudPositionXMaximum;
    private System.Windows.Forms.NumericUpDown nudPositionXMinimum;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Button btnSetPositionY;
    private System.Windows.Forms.Button btnSetPositionX;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.NumericUpDown nudPositionYActual;
    private System.Windows.Forms.NumericUpDown nudPositionXActual;
    private System.Windows.Forms.Button btnSetDelayMotionms;
    private System.Windows.Forms.NumericUpDown nudDelayMotionms;
    private System.Windows.Forms.Button btnSetPulsePeriodms;
    private System.Windows.Forms.NumericUpDown nudPulseCount;
    private System.Windows.Forms.NumericUpDown nudPulsePeriodms;
    private System.Windows.Forms.Button btnSetPulseCount;
    private System.Windows.Forms.Button btnSetPulseWidthus;
    private System.Windows.Forms.NumericUpDown nudPulseWidthus;
    private System.Windows.Forms.Button btnPulseLaserRow;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.TextBox tbxCommandFile;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.Button button4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.NumericUpDown numericUpDown1;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.NumericUpDown numericUpDown2;
    private System.Windows.Forms.NumericUpDown numericUpDown3;
    private System.Windows.Forms.NumericUpDown numericUpDown4;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.NumericUpDown numericUpDown5;
    private System.Windows.Forms.NumericUpDown numericUpDown6;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Button button5;
    private System.Windows.Forms.NumericUpDown numericUpDown7;
    private System.Windows.Forms.NumericUpDown numericUpDown8;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Button button6;
    private System.Windows.Forms.NumericUpDown numericUpDown9;
    private System.Windows.Forms.NumericUpDown numericUpDown10;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.Button button7;
    private System.Windows.Forms.NumericUpDown numericUpDown11;
    private System.Windows.Forms.NumericUpDown numericUpDown12;
    private System.Windows.Forms.NumericUpDown numericUpDown13;
    private System.Windows.Forms.NumericUpDown numericUpDown14;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Button button8;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.NumericUpDown numericUpDown15;
    private System.Windows.Forms.Label label20;
  }
}
