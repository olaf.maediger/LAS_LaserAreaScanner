﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using Network;
using NetworkDevice;
using UdpTextTransfer;
using UdpTextDevice;
using Uart;
using UCUartOpenClose;
using UCUartTerminal;
//
namespace LanUartUdpTextServer
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormServer : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICETYPE = "LanUartUdpTextServer";
    private const String INIT_DEVICENAME = "OMDevelopUdpTextServer";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    // Color UDP - RXD 
    private const Byte COLOR_UDP_RXD_A = 0xFF;
    private const Byte COLOR_UDP_RXD_R = 0xCC;
    private const Byte COLOR_UDP_RXD_G = 0x00;
    private const Byte COLOR_UDP_RXD_B = 0x00;
    // Color UDP - TXD
    private const Byte COLOR_UDP_TXD_A = 0xFF;
    private const Byte COLOR_UDP_TXD_R = 0x00;
    private const Byte COLOR_UDP_TXD_G = 0x00;
    private const Byte COLOR_UDP_TXD_B = 0xFF;
    //------------------------------------------------------
    // Color UART - RXD 
    private const Byte COLOR_UART_RXD_A = 0xFF;
    private const Byte COLOR_UART_RXD_R = 0x00;
    private const Byte COLOR_UART_RXD_G = 0xFF;
    private const Byte COLOR_UART_RXD_B = 0x00;
    // Color UART - TXD
    private const Byte COLOR_UART_TXD_A = 0xFF;
    private const Byte COLOR_UART_TXD_R = 0xAA;
    private const Byte COLOR_UART_TXD_G = 0xAA;
    private const Byte COLOR_UART_TXD_B = 0xAA;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    private CDeviceParameter FDeviceParameter;
    private Random FRandom;
    private CUdpTextDeviceServer FUdpTextDeviceServer;
    private CUart FUart;
    private CUCUartTerminal FUCUartTerminal;
    private Color FColorUartRxd = Color.Red;//FromArgb(COLOR_UART_RXD_A, COLOR_UART_RXD_R,
                                            //         COLOR_UART_RXD_G, COLOR_UART_RXD_B);
    private Color FColorUartTxd = Color.Green;//FromArgb(COLOR_UART_TXD_A, COLOR_UART_TXD_R,
                                                //       COLOR_UART_TXD_G, COLOR_UART_TXD_B);
    private Color FColorUdpRxd = Color.Blue;//FromArgb(COLOR_UDP_RXD_A, COLOR_UDP_RXD_R,
                                            //         COLOR_UDP_RXD_G, COLOR_UDP_RXD_B);
    private Color FColorUdpTxd = Color.Black;//FromArgb(COLOR_UDP_TXD_A, COLOR_UDP_TXD_R,
                                             //         COLOR_UDP_TXD_G, COLOR_UDP_TXD_B);
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FRandom = new Random((Int32)DateTime.Now.Ticks);
      //
      //####################################################################################
      // Init - Instance - Server
      //####################################################################################
      //
      FDeviceParameter = new CDeviceParameter();
      FDeviceParameter.Type = INIT_DEVICETYPE;
      FDeviceParameter.Name = INIT_DEVICENAME;
      FDeviceParameter.ID = Guid.NewGuid();
      IPAddress IPALocal;
      CNetwork.FindIPAddressLocal(out IPALocal);
      FDeviceParameter.IPA = IPALocal;
      IPAddress IPATarget;
      CNetwork.FindIPAddressLocalBroadcast(out IPATarget);
      //
      // UdpTextDeviceServer
      FUdpTextDeviceServer = new CUdpTextDeviceServer();
      FUdpTextDeviceServer.SetNotifier(FUCNotifier);
      FUdpTextDeviceServer.SetOnDatagramTransmitted(UdpTextDeviceServerOnDatagramTransmitted);
      FUdpTextDeviceServer.SetOnDatagramReceived(UdpTextDeviceServerOnDatagramReceived);
      FUdpTextDeviceServer.SetSourceIpAddress(IPALocal);
      FUdpTextDeviceServer.SetSourceIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_SERVER_TRANSMITTER);
      FUdpTextDeviceServer.SetTargetIpAddress(IPATarget);
      FUdpTextDeviceServer.SetTargetIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_SERVER_RECEIVER);
      if (FUdpTextDeviceServer.Open())
      {
        FUCNotifier.Write("UdpTextDeviceServer correct opened!");
      }
      //
      // Uart
      FUart = new CUart();
      FUart.SetOnLineReceived(UartOnLineReceived);
      FUart.SetOnLineTransmitted(UartOnLineTransmitted);
      // open from Uartterminal!
      //if (FUart.Open("UartLan",
      //               EComPort.cp36, EBaudrate.br115200, EParity.paNone,
      //               EDatabits.db8, EStopbits.sb1, EHandshake.hsNone))
      //{
      //  FUCNotifier.Write("Uart[COM36, 115200, N, 8, 1, N] correct opened!");
      //}
      //
      // UCUartTerminal
      FUCUartTerminal = new CUCUartTerminal();
      tbpUdpTextProtocolText.Controls.Add(FUCUartTerminal);
      FUCUartTerminal.Dock = DockStyle.Fill;
      FUCUartTerminal.SetOnTerminalOpen(UCUartTerminalOnTerminalOpen);
      FUCUartTerminal.SetOnTerminalClose(UCUartTerminalOnTerminalClose);
      FUCUartTerminal.SetOnTransmitText(UCUartTerminalOnTransmitText);
      // Open with first Uart
      //FUCUartTerminal.OpenAutomatic();
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      ////
      ////  Start Parallel-Application: LanUdpClient.exe
      //Process.Start("LanUdpTextClient.exe");
      //
      //  Start Parallel-Application: LASManager.exe
      Process.Start("LASManager.exe");      
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FUdpTextDeviceServer.Close();
      FUart.Close();
      ////
      //// -> LanUdpTextClient
      //Process[] ProcessesLanUdpClient = Process.GetProcessesByName("LanUdpTextClient");
      //foreach (Process ProcessLanUdpClient in ProcessesLanUdpClient)
      //{
      //  ProcessLanUdpClient.Kill();
      //}
      // -> LASManager
      Process[] ProcessesLASManager = Process.GetProcessesByName("LASManager");
      foreach (Process ProcessLASManager in ProcessesLASManager)
      {
        ProcessLASManager.Kill();
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      ////// /////////////////////////?????FUdpTextArrayServer.Close();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    // 

    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //#########################################################################
    //  Segment - Callback - UdpTextDeviceServer
    //#########################################################################
    //
    private delegate void CBUdpTextDeviceServerOnDatagramTransmitted(IPEndPoint ipendpointtarget,
                                                                     Byte[] datagram, Int32 size);
    private void UdpTextDeviceServerOnDatagramTransmitted(IPEndPoint ipendpointtarget,
                                                          Byte[] datagram, Int32 size)
    {
      if (this.InvokeRequired)
      {
        CBUdpTextDeviceServerOnDatagramTransmitted CB = 
          new CBUdpTextDeviceServerOnDatagramTransmitted(UdpTextDeviceServerOnDatagramTransmitted);
        Invoke(CB, new object[] { ipendpointtarget, datagram, size });
      }
      else
      {
        String SDatagram = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
        String Line = String.Format("UdpTxd[{0}]:<{1}>", CNetwork.IPEndPointToText(ipendpointtarget), SDatagram);
        FUCNotifier.Write(Line);
        // ok FUCUartTerminal.AddColoredLine(Color.Black, "UdpTxd>" + SDatagram);
        FUCUartTerminal.AddColoredLine(FColorUdpTxd, "UdpTxd>" + SDatagram);
      }
    }

    private delegate void CBUdpTextDeviceServerOnDatagramReceived(IPEndPoint ipendpointsource,
                                                                  Byte[] datagram, Int32 size);
    private void UdpTextDeviceServerOnDatagramReceived(IPEndPoint ipendpointsource,
                                                       Byte[] datagram, Int32 size)
    {
      if (this.InvokeRequired)
      {
        CBUdpTextDeviceServerOnDatagramReceived CB = 
          new CBUdpTextDeviceServerOnDatagramReceived(UdpTextDeviceServerOnDatagramReceived);
        Invoke(CB, new object[] { ipendpointsource, datagram, size });
      }
      else
      {
        String SDatagram = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
        String Line = String.Format("UdpRxd[{0}]:<{1}>", CNetwork.IPEndPointToText(ipendpointsource), SDatagram);
        FUCNotifier.Write(Line);
        // ok FUCUartTerminal.AddColoredLine(Color.Blue, "UdpRxd>" + SDatagram);
        FUCUartTerminal.AddColoredLine(FColorUdpRxd, "UdpRxd>" + SDatagram);
        // Directing: UdpRxd -> UartTxd
        FUart.WriteLine(SDatagram);
      }
    }
    //
    //#########################################################################
    //  Segment - Callback - Uart
    //#########################################################################
    //
    private delegate void CBUartOnLineTransmitted(EComPort comport, String line);
    private void UartOnLineTransmitted(EComPort comport, String line)
    {
      if (this.InvokeRequired)
      {
        CBUartOnLineTransmitted CB = new CBUartOnLineTransmitted(UartOnLineTransmitted);
        Invoke(CB, new object[] { comport, line });
      }
      else
      {
        String SComPort = CUart.ComPortEnumeratorName(comport);
        String Line = String.Format("UartTxd[{0}]:<{1}>", SComPort, line);
        FUCNotifier.Write(Line);
        // /r/n is integrated in line -> AddColoredText !
        FUCUartTerminal.AddColoredText(FColorUartTxd, "UartTxd>" + line);
      }
    }

    private delegate void CBUartOnLineReceived(EComPort comport, String line);
    private void UartOnLineReceived(EComPort comport, String line)
    {
      if (this.InvokeRequired)
      {
        CBUartOnLineReceived CB = new CBUartOnLineReceived(UartOnLineReceived);
        Invoke(CB, new object[] { comport, line });
      }
      else
      {
        String SComPort = CUart.ComPortEnumeratorName(comport);
        String Line = String.Format("UartRxd[{0}]:<{1}>", SComPort, line);
        FUCNotifier.Write(Line);
        // ok FUCUartTerminal.AddColoredLine(Color.Red, "UartRxd>" + line);
        FUCUartTerminal.AddColoredLine(FColorUartRxd, "UartRxd>" + line);
        // Directing: UartRxd -> UdpTxd
        FUdpTextDeviceServer.Preset(line);
      }
    }
    //
    //#########################################################################
    //  Segment - Callback - UCUartTerminal
    //#########################################################################
    //
    private void UCUartTerminalOnTerminalOpen(String portname, EComPort comport, 
                                              EBaudrate baudrate, EParity parity,
                                              EDatabits databits, EStopbits stopbits, 
                                              EHandshake handshake)
    {
      if (FUart.Open(portname, comport, baudrate, parity, databits, stopbits, handshake))
      {
        String SComPort = CUart.ComPortEnumeratorName(comport);
        String Line = String.Format("Uart[{0}, {1}, {2}, {3}, {4}, {5}] correct opened!",
                                    SComPort, baudrate, parity, databits, stopbits, handshake);
        FUCNotifier.Write(Line);
      }
      else
      {
        FUCNotifier.Error("", 1, "Uart not opened");
      }
    }

    private void UCUartTerminalOnTerminalClose(EComPort comport)
    {
      if (FUart.Close())
      {
        String SComPort = CUart.ComPortEnumeratorName(comport);
        String Line = String.Format("Uart[{0}]] correct closed!", SComPort);
        FUCNotifier.Write(Line);
      }
      else
      {
        FUCNotifier.Error("", 2, "Uart not closed");
      }
    }

    private void UCUartTerminalOnTransmitText(String text, Int32 delaycharacter, Int32 delaycr, Int32 delaylf)
    {
      String SComPort = CUart.ComPortEnumeratorName(FUart.GetComPort());
      String Line = String.Format("UartTxd[{0}]:<{1}>", SComPort, text);
      FUCNotifier.Write(Line);
      // ok FUCUartTerminal.AddColoredText(Color.Magenta, "UartTxd>" + text);
      FUCUartTerminal.AddColoredText(FColorUartTxd, "UartTxd>" + text);      
      // Directing: UCTerminalTxd -> UartTxd
      FUart.WriteText(text);
    }

   

    //private void UCUartTerminalOnLineTransmitted(Guid comportid, String line)
    //{
    //  String Line = String.Format("TxdUart[COM36]:<{0}>", line);
    //  FUart.WriteLine(line);
    //  FUCNotifier.Write(Line);
    //}
    // NC !!!!!!!!!!!!!
    //private void UCUartTerminalOnLineReceived(Guid comportid, String line)
    //{
    //  String Line = String.Format("RxdUart[COM36]:<{0}>", line);
    //  FUCNotifier.Write(Line);
    //  // Send Uart-Line -> Udp-Datagram
    //  FUdpTextDeviceServer.Preset(line);
    //}
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          tbcMain.SelectedIndex = 1;
          return true;
        case 1:
          return true;
        case 2:
          // FUCNotifier.Write("*** UdpTextDeviceServer - Transmit Message:");
          //String Message = "Hello World! (Server -> Client)";
          //FUdpTextDeviceServer.Preset(Message);
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5: 
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }


  }
}

////
////------------------------------------------------------------------------
////  Segment - Callback - Uart
////------------------------------------------------------------------------
////
//private delegate void CBUartOnLineReceived(Guid comportid, String line);
//private void UartOnLineReceived(Guid comportid, String line)
//{
//  if (this.InvokeRequired)
//  {
//    CBUartOnLineReceived CB = new CBUartOnLineReceived(UartOnLineReceived);
//    Invoke(CB, new object[] { comportid, line });
//  }
//  else
//  {
//    AddTerminalRxData("RxD>" + line + "\r\n");
//  }
//}


      //Char[] Separators = new Char[1];
      //Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
      //String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
      //foreach (String Tag in Tags)
      //{
      //  FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
      //}
//Char[] Separators = new Char[1];
//Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//foreach (String Tag in Tags)
//{
//  FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//}

//  ---------------------------------------------------
//  UdpTextDeviceServer
//  ---------------------------------------------------
//FUdpTextDeviceServer = new CUdpTextDeviceServer();
//// UdpTextDeviceServer - Common
//FUdpTextDeviceServer.SetNotifier(FUCNotifier);
//FUdpTextDeviceServer.SetOnDatagramTransmitted(UdpTextDeviceServerOnDatagramTransmitted);
//FUdpTextDeviceServer.SetOnDatagramReceived(UdpTextDeviceServerOnDatagramReceived);
//// UdpTextDeviceServer - Source
//FUdpTextDeviceServer.SetSourceIpAddress(IPALocal);
//FUdpTextDeviceServer.SetSourceIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_SERVER_RECEIVER);
//FUdpTextDeviceServer.SetSourceID(FDeviceParameter.ID);
//FUdpTextDeviceServer.SetSourceType("NhdDataMessageServer");
//FUdpTextDeviceServer.SetSourceName("PSimonLaborDataCollection");
//// UdpTextDeviceServer - Target
//FUdpTextDeviceServer.SetTargetIpAddress(IPATarget);
//FUdpTextDeviceServer.SetTargetIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_SERVER_TRANSMITTER);
//FUdpTextDeviceServer.SetTargetID(Guid.Empty);
//FUdpTextDeviceServer.SetTargetType("NhdDataMessageClient");
//FUdpTextDeviceServer.SetTargetName("PSimonOfficeDataAnalysis");
//// UdpTextDeviceServer - Open
//FUdpTextDeviceServer.Open();
////////////////////////
////////////////////////  ---------------------------------------------------
////////////////////////  UdpTextArrayServer 
////////////////////////  ---------------------------------------------------
//////////////////////FUdpTextArrayServer = new CUdpTextArrayServer();
//////////////////////// UdpTextArrayServer - Common
//////////////////////FUdpTextArrayServer.SetNotifier(FUCNotifier);
//////////////////////FUdpTextArrayServer.SetOnDatagramTransmitted(UdpTextArrayServerOnDatagramTransmitted);
//////////////////////FUdpTextArrayServer.SetOnDatagramReceived(UdpTextArrayServerOnDatagramReceived);
//////////////////////FUdpTextArrayServer.SetOnBuildDoubleArray2D(UdpTextArrayServerOnBuildDoubleArray2D);
//////////////////////// UdpTextArrayServer - Source
//////////////////////FUdpTextArrayServer.SetSourceIpAddress(IPALocal);
//////////////////////FUdpTextArrayServer.SetSourceIpPort(CNetworkDevice.PORT_UDPTEXT_ARRAY_SERVER_RECEIVER);
//////////////////////FUdpTextArrayServer.SetSourceID(FDeviceParameter.ID);
//////////////////////FUdpTextArrayServer.SetSourceType("NhdDataArrayServer");
//////////////////////FUdpTextArrayServer.SetSourceName("PSimonLaborDataCollection");
//////////////////////// UdpTextArrayServer - Target
//////////////////////FUdpTextArrayServer.SetTargetIpAddress(IPATarget);
//////////////////////FUdpTextArrayServer.SetTargetIpPort(CNetworkDevice.PORT_UDPTEXT_ARRAY_SERVER_TRANSMITTER);
//////////////////////FUdpTextArrayServer.SetTargetID(Guid.Empty);
//////////////////////FUdpTextArrayServer.SetTargetType("NhdDataArrayClient");
//////////////////////FUdpTextArrayServer.SetTargetName("PSimonOfficeDataAnalysis");
//////////////////////// UdpTextArrayServer - Open
//////////////////////FUdpTextArrayServer.Open();
////
////  ---------------------------------------------------
////  UdpBinaryDeviceServer
////  ---------------------------------------------------
//FUdpBinaryDeviceServer = new CUdpBinaryDeviceServer();
//// UdpTextDeviceServer - Common
//FUdpBinaryDeviceServer.SetNotifier(FUCNotifier);
//FUdpBinaryDeviceServer.SetOnDatagramTransmitted(UdpBinaryDeviceServerOnDatagramTransmitted);
//FUdpBinaryDeviceServer.SetOnDatagramReceived(UdpBinaryDeviceServerOnDatagramReceived);
//// UdpBinaryDeviceServer - Source
//FUdpBinaryDeviceServer.SetSourceIpAddress(IPALocal);
//FUdpBinaryDeviceServer.SetSourceIpPort(CNetworkDevice.PORT_UDPBINARY_MESSAGE_SERVER_RECEIVER);
//FUdpBinaryDeviceServer.SetSourceID(FDeviceParameter.ID);
//FUdpBinaryDeviceServer.SetSourceType("NhdDataMessageServer");
//FUdpBinaryDeviceServer.SetSourceName("PSimonLaborDataCollection");
//// UdpBinaryDeviceServer - Target
//FUdpBinaryDeviceServer.SetTargetIpAddress(IPATarget);
//FUdpBinaryDeviceServer.SetTargetIpPort(CNetworkDevice.PORT_UDPBINARY_MESSAGE_SERVER_TRANSMITTER);
//FUdpBinaryDeviceServer.SetTargetID(Guid.Empty);
//FUdpBinaryDeviceServer.SetTargetType("NhdDataMessageClient");
//FUdpBinaryDeviceServer.SetTargetName("PSimonOfficeDataAnalysis");
//// UdpBinaryDeviceServer - Open
//FUdpBinaryDeviceServer.Open();
//////////////////////////////
//////////////////////////////####################################################################################
////////////////////////////// Init - UserControls - Server
//////////////////////////////####################################################################################
//////////////////////////////

//////////////////////FUCNhdUdpTextServer.Close();
////// ///////////////////////// FUdpTextArrayServer.Close();
/////////////FUdpBinaryDeviceServer.Close();
//////////////FUdpBinaryDeviceServer.Close();

//////////////////////////////
////////////////////////////// UCNhdUdpArrayServer
////////////////////////////FUCNhdUdpArrayServer.SetIpAddressLocal(IPALocal);
////////////////////////////FUCNhdUdpArrayServer.SetIpPortTransmit(CNetworkDevice.PORT_UDPTEXT_ARRAY_SERVER_TRANSMITTER);
////////////////////////////FUCNhdUdpArrayServer.SetIpPortReceive(CNetworkDevice.PORT_UDPTEXT_ARRAY_SERVER_RECEIVER);
////////////////////////////FUCNhdUdpArrayServer.SetIpAddressTarget(IPATarget);
////////////////////////////FUCNhdUdpArrayServer.SetOnSendArray2DSmall(UCNhdUdpArrayServerOnSendArray2DSmall);
////////////////////////////FUCNhdUdpArrayServer.SetOnSendArray2DLarge(UCNhdUdpArrayServerOnSendArray2DLarge);
//////////////////////////////
//------------------------------------------------------------------------------



//
//------------------------------------------------------------------------
//  Section - Helper
//------------------------------------------------------------------------
// 
////////////private String[] BuildMessages(UInt16 messagecount)
////////////{ // Content Messages
////////////  String[] Messages = new String[messagecount];
////////////  for (Int32 MI = 0; MI < messagecount; MI++)
////////////  {
////////////    Messages[MI] = String.Format("#{0}/{1}<{2}>", 1 + MI, messagecount, FUCNhdUdpTextServer.GetMessage());
////////////  }
////////////  return Messages;
////////////}

////////////private Double[,] BuildArrayDouble2D(UInt16 tupelcount)
////////////{ // Content Array2D
////////////  Double[,] DataArray = new Double[tupelcount, 2];
////////////  for (Int32 TI = 0; TI < tupelcount; TI++)
////////////  {
////////////    DataArray[TI, 0] = TI + 0.5 - FRandom.NextDouble();
////////////    DataArray[TI, 1] = 0.5 + 1.0 * DataArray[TI, 0];
////////////  }
////////////  return DataArray;
////////////}

//////////////private void BuildArrayDouble2D(Int32 datasize, String[] datanames,
//////////////                                String[] dataunits, out Double[,] datavector)
//////////////{
//////////////  datavector = new Double[datasize, CUdpTextArrayBase.DIMENSION_VECTOR2D];
//////////////  for (Int32 TI = 0; TI < datasize; TI++)
//////////////  {
//////////////    datavector[TI, 0] = TI + 0.5 - FRandom.NextDouble();
//////////////    datavector[TI, 1] = 0.5 + 1.0 * datavector[TI, 0];
//////////////  }
//////////////}

//////////////private void UdpTextArrayServerOnBuildDoubleArray2D(Int32 datasize,
//////////////                                                    String[] datanames,
//////////////                                                    String[] dataunits,
//////////////                                                    out Double[,] datavector)
//////////////{
//////////////  BuildArrayDouble2D(datasize, datanames, dataunits, out datavector);
//////////////}

////////////////
////////////////------------------------------------------------------------------------
////////////////  Section - Callback - UdpTextArrayServer
////////////////------------------------------------------------------------------------
//////////////// 
//////////////private void UdpTextArrayServerOnDatagramTransmitted(IPEndPoint ipendpointtarget,
//////////////                                                     Byte[] datagram, Int32 size)
//////////////{
//////////////  String Line = String.Format("TxdUdpTextArrayServer[{0}]:",
//////////////                              CNetwork.IPEndPointToText(ipendpointtarget));
//////////////  FUCNotifier.Write(Line);
//////////////  Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
//////////////  Char[] Separators = new Char[1];
//////////////  Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//////////////  String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//////////////  foreach (String Tag in Tags)
//////////////  {
//////////////    FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//////////////  }
//////////////}

//////////////private void UdpTextArrayServerOnDatagramReceived(IPEndPoint ipendpointsource,
//////////////                                                  Byte[] datagram, Int32 size)
//////////////{
//////////////  String Line = String.Format("RxdUdpTextArrayServer[{0}]:",
//////////////                              CNetwork.IPEndPointToText(ipendpointsource));
//////////////  FUCNotifier.Write(Line);
//////////////  Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
//////////////  Char[] Separators = new Char[1];
//////////////  Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//////////////  String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//////////////  foreach (String Tag in Tags)
//////////////  {
//////////////    FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//////////////  }
//////////////}    
//
//------------------------------------------------------------------------
//  Section - Callback - UdpBinaryDeviceServer
//------------------------------------------------------------------------
// 
//////////private void UdpBinaryDeviceServerOnDatagramTransmitted(IPEndPoint ipendpointtarget,
//////////                                                         Byte[] datagram, Int32 size)
//////////{
//////////  String Line = String.Format("TxdUdpBinaryDeviceServer[{0}]:",
//////////                              CNetwork.IPEndPointToText(ipendpointtarget));
//////////  FUCNotifier.Write(Line);
//////////  // NOT HERE Byte[]-Analysis !!!!
//////////  //Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
//////////  //Char[] Separators = new Char[1];
//////////  //Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//////////  //String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//////////  //foreach (String Tag in Tags)
//////////  //{
//////////  //  FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//////////  //}
//////////}

//////////private void UdpBinaryDeviceServerOnDatagramReceived(IPEndPoint ipendpointsource,
//////////                                                      Byte[] datagram, Int32 size)
//////////{
//////////  String Line = String.Format("RxdUdpBinaryDeviceServer[{0}]:",
//////////                              CNetwork.IPEndPointToText(ipendpointsource));
//////////  FUCNotifier.Write(Line);
//////////  // NOT HERE Byte[]-Analysis !!!!
//////////  //Line = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
//////////  //Char[] Separators = new Char[1];
//////////  //Separators[0] = CUdpTextBase.SEPARATOR_UDPTEXT;
//////////  //String[] Tags = Line.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
//////////  //foreach (String Tag in Tags)
//////////  //{
//////////  //  FUCNotifier.Write(Tag + CUdpTextBase.SEPARATOR_UDPTEXT);
//////////  //}
//////////}

////////////////////////////////
////////////////////////////////------------------------------------------------------------------------
////////////////////////////////  Section - Callback - UCNhdUdpArrayServer
////////////////////////////////------------------------------------------------------------------------
//////////////////////////////// 
//////////////////////////////private void UCNhdUdpArrayServerOnSendArray2DSmall()
//////////////////////////////{
//////////////////////////////  const Int32 TUPELCOUNT = 4;
//////////////////////////////  //////////////////String[] DataNames = new String[2];
//////////////////////////////  //////////////////DataNames[0] = "Location"; DataNames[1] = "Signal";
//////////////////////////////  //////////////////String[] DataUnits = new String[2];
//////////////////////////////  //////////////////DataUnits[0] = "mm"; DataUnits[1] = "V";
//////////////////////////////  Double[,] DataArray = BuildArrayDouble2D(TUPELCOUNT);
//////////////////////////////  //////////////////FUdpTextArrayServer.PresetArrayDouble2D(DataNames, DataUnits, DataArray);
//////////////////////////////  FUdpTextDeviceServer.PresetVector2DDouble(DataArray);
//////////////////////////////}

//////////////////////////////private void UCNhdUdpArrayServerOnSendArray2DLarge()
//////////////////////////////{
//////////////////////////////  const Int32 TUPELCOUNT = 128;
//////////////////////////////  ////////////String[] DataNames = new String[2];
//////////////////////////////  ////////////DataNames[0] = "Location"; DataNames[1] = "Signal";
//////////////////////////////  ////////////String[] DataUnits = new String[2];
//////////////////////////////  ////////////DataUnits[0] = "mm"; DataUnits[1] = "V";
//////////////////////////////  Double[,] DataArray = BuildArrayDouble2D(TUPELCOUNT);
//////////////////////////////  //////////// FUdpTextArrayServer.PresetArrayDouble2D(DataNames, DataUnits, DataArray);
//////////////////////////////  FUdpTextDeviceServer.PresetVector2DDouble(DataArray);
//////////////////////////////}
//}