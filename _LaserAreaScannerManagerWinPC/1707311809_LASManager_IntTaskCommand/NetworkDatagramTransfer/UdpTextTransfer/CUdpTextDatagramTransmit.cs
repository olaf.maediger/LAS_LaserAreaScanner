﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Network;
//
namespace UdpTextTransfer
{
  public class CUdpTextDatagramTransmit : CUdpTextDatagramBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private CTagTextList FContent;  // Taglist of elementary Datatypes specific to Type/Key
    private Byte[] FByteVector;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpTextDatagramTransmit(RUdpTextDatagramHeader header,
                                    Byte[] datagramcontent)
    {
      FByteVector = BuildByteVector(header, datagramcontent);
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public CTagTextList GetContent()
    {
      return FContent;
    }
    public void SetContent(CTagTextList value)
    {
      FContent = value;
    }

    public Byte[] GetByteVector()
    {
      return FByteVector;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    // StringList -> String[] -> Byte[]
    private Byte[] BuildByteVector(RUdpTextDatagramHeader header,
                                   Byte[] datagramcontent)
    {
      CByteList ByteList = new CByteList();
      // ProtocolType [6xByte]
      CTTMagicWord TMW = new CTTMagicWord(CNetwork.PROTOCOLTYPE_UDPBINARY);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ByteList.Add(TMW.GetSType);
      //ByteList.AddRange(TMW.Values);
      //// TimeStamp [DateTime]
      //CTBScalarDateTime TDT = new CTBScalarDateTime(timestamp);
      //ByteList.Add(TDT.Type);
      //ByteList.AddRange(TDT.Values);
      //// SourceIpAddress [Byte[]]
      //CTBVectorByte TVB = new CTBVectorByte(sourceipaddress);
      //ByteList.Add(TVB.Type);
      //ByteList.AddRange(TVB.Values);
      //// SourceID
      //CTBScalarGuid TSG = new CTBScalarGuid(sourceid);
      //ByteList.Add(TSG.Type);
      //ByteList.AddRange(TSG.Values);
      //// SourceType [String]
      //CTBScalarString TSS = new CTBScalarString(sourcetype);
      //ByteList.Add(TSS.Type);
      //ByteList.AddRange(TSS.Values);
      //// SourceName [String]
      //TSS = new CTBScalarString(sourcename);
      //ByteList.Add(TSS.Type);
      //ByteList.AddRange(TSS.Values);
      //// TargetIpAddress
      //TVB = new CTBVectorByte(targetipaddress);
      //ByteList.Add(TVB.Type);
      //ByteList.AddRange(TVB.Values);
      //// TargetID [Guid]
      //TSG = new CTBScalarGuid(targetid);
      //ByteList.Add(TSG.Type);
      //ByteList.AddRange(TSG.Values);
      //// TargetType [String]
      //TSS = new CTBScalarString(targettype);
      //ByteList.Add(TSS.Type);
      //ByteList.AddRange(TSS.Values);
      //// TargetName [String]
      //TSS = new CTBScalarString(targetname);
      //ByteList.Add(TSS.Type);
      //ByteList.AddRange(TSS.Values);
      //// DatagramID [Guid]
      //TSG = new CTBScalarGuid(datagramid);
      //ByteList.Add(TSG.Type);
      //ByteList.AddRange(TSG.Values);
      //// BlockIndex
      //CTBScalarInt32 TSI = new CTBScalarInt32(blockindex);
      //ByteList.Add(TSI.Type);
      //ByteList.AddRange(TSI.Values);
      //// BlockCount [Int32]
      //TSI = new CTBScalarInt32(blockcount);
      //ByteList.Add(TSI.Type);
      //ByteList.AddRange(TSI.Values);
      //// DatagramType [String]
      //TSS = new CTBScalarString(datagramtype);
      //ByteList.Add(TSS.Type);
      //ByteList.AddRange(TSS.Values);
      //// DatagramKey [String]
      //TSS = new CTBScalarString(datagramkey);
      //ByteList.Add(TSS.Type);
      //ByteList.AddRange(TSS.Values);
      //// DatagramContent {Byte[]}
      //Int32 CountBytes = datagramcontent.Length;
      //for (Int32 BI = 0; BI < CountBytes; BI++)
      //{
      //  ByteList.Add(datagramcontent[BI]);
      //}
      //// List -> []
      //Int32 BufferCount = ByteList.Count;
      //Byte[] Result = new Byte[BufferCount];
      //for (Int32 BI = 0; BI < BufferCount; BI++)
      //{
      //  Result[BI] = ByteList[BI];
      //}
      //return Result;
      return null;
    }

  }
}
