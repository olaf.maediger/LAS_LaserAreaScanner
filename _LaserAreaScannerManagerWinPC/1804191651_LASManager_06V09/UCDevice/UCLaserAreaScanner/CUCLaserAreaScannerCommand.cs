﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPLaserAreaScanner;
//
namespace UCLaserAreaScanner
{
  //
  //--------------------------------------------------------------------------
  //  Segment - Definition - UCLaserAreaScanner
  //--------------------------------------------------------------------------
  //
  public partial class CUCLaserAreaScannerCommand : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    // Common
    private DOnGetHelp FOnGetHelp;
    private DOnGetProgramHeader FOnGetProgramHeader;
    private DOnGetHardwareVersion FOnGetHardwareVersion;
    private DOnGetSoftwareVersion FOnGetSoftwareVersion;
    private DOnGetProcessCount FOnGetProcessCount;
    private DOnSetProcessCount FOnSetProcessCount;
    private DOnGetProcessPeriod FOnGetProcessPeriod;
    private DOnSetProcessPeriod FOnSetProcessPeriod;
    private DOnStopProcessExecution FOnStopProcessExecution;
    // LedSystem
    private DOnGetLedSystem FOnGetLedSystem;
    private DOnSetLedSystemLow FOnSetLedSystemLow;
    private DOnSetLedSystemHigh FOnSetLedSystemHigh;
    private DOnBlinkLedSystem FOnBlinkLedSystem;
    // LaserRange
    private DOnGetPositionX FOnGetPositionX;
    private DOnSetPositionX FOnSetPositionX;
    private DOnGetPositionY FOnGetPositionY;
    private DOnSetPositionY FOnSetPositionY;
    private DOnGetRangeX FOnGetRangeX;
    private DOnSetRangeX FOnSetRangeX;
    private DOnGetRangeY FOnGetRangeY;
    private DOnSetRangeY FOnSetRangeY;
    private DOnGetDelayMotion FOnGetDelayMotion;
    private DOnSetDelayMotion FOnSetDelayMotion;
    // LaserPosition
    private DOnPulseLaserPosition FOnPulseLaserPosition;
    private DOnAbortLaserPosition FOnAbortLaserPosition;
    // LaserMatrix
    private DOnPulseLaserMatrix FOnPulseLaserMatrix;
    private DOnAbortLaserMatrix FOnAbortLaserMatrix;
    // LaserImage
    private DOnLoadLaserSteptable FOnLoadLaserSteptable;
    private DOnPulseLaserImage FOnPulseLaserImage;
    private DOnAbortLaserImage FOnAbortLaserImage;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCLaserAreaScannerCommand()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    // Common
    public void SetOnGetHelp(DOnGetHelp value)
    {
      FOnGetHelp = value;
    }
    public void SetOnGetProgramHeader(DOnGetProgramHeader value)
    {
      FOnGetProgramHeader = value;
    }
    public void SetOnGetHardwareVersion(DOnGetHardwareVersion value)
    {
      FOnGetHardwareVersion = value;
    }
    public void SetHardwareVersion(String value)
    {
      tbxHardwareVersion.Text = value;
    }
    public void SetOnGetSoftwareVersion(DOnGetSoftwareVersion value)
    {
      FOnGetSoftwareVersion = value;
    }
    public void SetSoftwareVersion(String value)
    {
      tbxSoftwareVersion.Text = value;
    }
    public void SetOnGetProcessCount(DOnGetProcessCount value)
    {
      FOnGetProcessCount = value;
    }
    public void SetOnSetProcessCount(DOnSetProcessCount value)
    {
      FOnSetProcessCount = value;
    }
    public void SetOnGetProcessPeriod(DOnGetProcessPeriod value)
    {
      FOnGetProcessPeriod = value;
    }
    public void SetOnSetProcessPeriod(DOnSetProcessPeriod value)
    {
      FOnSetProcessPeriod = value;
    }
    public void SetOnStopProcessExecution(DOnStopProcessExecution value)
    {
      FOnStopProcessExecution = value;
    }
    // LedSystem
    public void SetOnGetLedSystem(DOnGetLedSystem value)
    {
      FOnGetLedSystem = value;
    }
    public void SetOnSetLedSystemLow(DOnSetLedSystemLow value)
    {
      FOnSetLedSystemLow = value;
    }
    public void SetOnSetLedSystemHigh(DOnSetLedSystemHigh value)
    {
      FOnSetLedSystemHigh = value;
    }
    public void SetOnBlinkLedSystem(DOnBlinkLedSystem value)
    {
      FOnBlinkLedSystem = value;
    }
    // LaserRange
    public void SetOnGetPositionX(DOnGetPositionX value)
    {
      FOnGetPositionX = value;
    }
    public void SetOnSetPositionX(DOnSetPositionX value)
    {
      FOnSetPositionX = value;
    }
    public void SetOnGetRangeX(DOnGetRangeX value)
    {
      FOnGetRangeX = value;
    }
    public void SetOnSetRangeX(DOnSetRangeX value)
    {
      FOnSetRangeX = value;
    }
    public void SetOnGetPositionY(DOnGetPositionY value)
    {
      FOnGetPositionY = value;
    }
    public void SetOnSetPositionY(DOnSetPositionY value)
    {
      FOnSetPositionY = value;
    }
    public void SetOnGetRangeY(DOnGetRangeY value)
    {
      FOnGetRangeY = value;
    }
    public void SetOnSetRangeY(DOnSetRangeY value)
    {
      FOnSetRangeY = value;
    }
    public void SetOnGetDelayMotion(DOnGetDelayMotion value)
    {
      FOnGetDelayMotion = value;
    }
    public void SetOnSetDelayMotion(DOnSetDelayMotion value)
    {
      FOnSetDelayMotion = value;
    }
    // LaserPosition
    public void SetOnPulseLaserPosition(DOnPulseLaserPosition value)
    {
      FOnPulseLaserPosition = value;
    }
    public void SetOnAbortLaserPosition(DOnAbortLaserPosition value)
    {
      FOnAbortLaserPosition = value;
    }
    // LaserMatrix
    public void SetOnPulseLaserMatrix(DOnPulseLaserMatrix value)
    {
      FOnPulseLaserMatrix = value;
    }
    public void SetOnAbortLaserMatrix(DOnAbortLaserMatrix value)
    {
      FOnAbortLaserMatrix = value;
    }
    // LaserImage
    public void SetOnLoadLaserSteptable(DOnLoadLaserSteptable value)
    {
      FOnLoadLaserSteptable = value;
    }
    public void SetOnPulseLaserImage(DOnPulseLaserImage value)
    {
      FOnPulseLaserImage = value;
    }
    public void SetOnAbortLaserImage(DOnAbortLaserImage value)
    {
      FOnAbortLaserImage = value;
    }






  public void SetState(String[] tokens)
    {
      Int32 TL = tokens.Length;
      if (4 <= TL)
      {
        if (CDefine.PROMPT_RESPONSE == tokens[0])
        {
          Int32 SMTC;
          Int32.TryParse(tokens[2], out SMTC);
          Int32 SubState;
          Int32.TryParse(tokens[3], out SubState);
          lblStateMTCIndex.Text = String.Format("{0} | {1}", SMTC, SubState);
          switch ((EStateProcess)SMTC)
          { // Common
            case EStateProcess.Idle:
              lblStateMTControllerText.Text = "Idle";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            case EStateProcess.Welcome:
              lblStateMTControllerText.Text = "Welcome";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            case EStateProcess.GetHelp:
              lblStateMTControllerText.Text = "GetHelp";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetProgramHeader:
              lblStateMTControllerText.Text = "GetProgramHeader";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetSoftwareVersion:
              lblStateMTControllerText.Text = "GetSoftwareVersion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetHardwareVersion:
              lblStateMTControllerText.Text = "GetHardwareVersion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetProcessCount:
              lblStateMTControllerText.Text = "GetProcessCount";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetProcessCount:
              lblStateMTControllerText.Text = "SetProcessCount";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetProcessPeriod:
              lblStateMTControllerText.Text = "SetProcessPeriod";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetProcessPeriod:
              lblStateMTControllerText.Text = "SetProcessPeriod";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.StopProcessExecution:
              lblStateMTControllerText.Text = "StopProcessExecution";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            // Measurement - LedSystem
            case EStateProcess.GetLedSystem:
              lblStateMTControllerText.Text = "GetLedSystem";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.LedSystemOn:
              lblStateMTControllerText.Text = "LedSystemOn";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.LedSystemOff:
              lblStateMTControllerText.Text = "LedSystemOff";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.BlinkLedSystem:
              lblStateMTControllerText.Text = "BlinkLedSystem";
              lblStateMTControllerText.BackColor = Color.LightPink;
              SetStateLedSystem(SubState);
              break;
            // LaserRange
            case EStateProcess.GetPositionX:
              lblStateMTControllerText.Text = "GetPositionX";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetPositionX:
              lblStateMTControllerText.Text = "SetPositionX";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetPositionY:
              lblStateMTControllerText.Text = "GetPositionY";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetPositionY:
              lblStateMTControllerText.Text = "SetPositionY";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetRangeX:
              lblStateMTControllerText.Text = "GetRangeX";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetRangeX:
              lblStateMTControllerText.Text = "SetRangeX";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetRangeY:
              lblStateMTControllerText.Text = "GetRangeY";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetRangeY:
              lblStateMTControllerText.Text = "SetRangeY";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetDelayMotion:
              lblStateMTControllerText.Text = "GetDelayMotion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetDelayMotion:
              lblStateMTControllerText.Text = "SetDelayMotion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            // LaserPosition
            case EStateProcess.PulseLaserPosition:
              lblStateMTControllerText.Text = "PulseLaserPosition";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.AbortLaserPosition:
              lblStateMTControllerText.Text = "AbortLaserPosition";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            // LaserMatrix
            case EStateProcess.PulseLaserMatrix:
              lblStateMTControllerText.Text = "PulseLaserMatrix";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.AbortLaserMatrix:
              lblStateMTControllerText.Text = "AbortLaserMatrix";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            // LaserImage
            case EStateProcess.PulseLaserImage:
              lblStateMTControllerText.Text = "PulseLaserImage";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.AbortLaserImage:
              lblStateMTControllerText.Text = "AbortLaserImage";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            default: // saUndefined
              lblStateMTControllerText.Text = "???Undefined???";
              lblStateMTControllerText.BackColor = Color.Red;
              break;
          }
        }
      }
      else
      {
        lblStateMTCIndex.Text = "???";
      }
    }

    public void SetProcessTime(UInt16 hours, UInt16 minutes, UInt16 seconds, UInt16 millis)
    {
      lblProcessTime.Text = String.Format("{0:00}:{1:00}:{2:00}.{3:000}", 
                                          hours, minutes, seconds, millis);
    }

    public void SetProcessCount(UInt32 value)
    {
      nudProcessCount.Value = value;
    }

    public void SetProcessPeriod(UInt32 periodus)
    {
      nudProcessPeriod.Value = periodus;
    }

     public void SetStateLedSystem(Int32 state)
    {
      switch (state)
      {
        case 0: // EStateLed.Off:
          pnlStateLedSystem.BackColor = Color.MediumBlue;
          break;
        case 1: //EStateLed.On:
          pnlStateLedSystem.BackColor = Color.GreenYellow;
          break;
        default: // EStateLed.Undefined:
          pnlStateLedSystem.BackColor = Color.Fuchsia;
          break;
      }
    }




    public UInt16 GetPositionXActual()
    {
      return (UInt16)nudPositionXActual.Value;
    }
    public void SetPositionXActual(UInt16 positionx)
    {
      nudPositionXActual.Value = positionx;
    }
    public UInt16 GetPositionYActual()
    {
      return (UInt16)nudPositionYActual.Value;
    }
    public void SetPositionYActual(UInt16 positiony)
    {
      nudPositionYActual.Value = positiony;
    }
    public UInt16 GetPositionXMinimum()
    {
      return (UInt16)nudPositionXMinimum.Value;
    }
    public void SetPositionXMinimum(UInt16 positionx)
    {
      nudPositionXMinimum.Value = positionx;
    }
    public UInt16 GetPositionYMinimum()
    {
      return (UInt16)nudPositionYMinimum.Value;
    }
    public void SetPositionYMinimum(UInt16 positiony)
    {
      nudPositionYMinimum.Value = positiony;
    }
    public UInt16 GetPositionXMaximum()
    {
      return (UInt16)nudPositionXMaximum.Value;
    }
    public void SetPositionXMaximum(UInt16 positionx)
    {
      nudPositionXMaximum.Value = positionx;
    }
    public UInt16 GetPositionYMaximum()
    {
      return (UInt16)nudPositionYMaximum.Value;
    }
    public void SetPositionYMaximum(UInt16 positiony)
    {
      nudPositionYMaximum.Value = positiony;
    }
    public UInt16 GetPositionXDelta()
    {
      return (UInt16)nudPositionXDelta.Value;
    }
    public void SetPositionXDelta(UInt16 positionx)
    {
      nudPositionXDelta.Value = positionx;
    }
    public UInt16 GetPositionYDelta()
    {
      return (UInt16)nudPositionYDelta.Value;
    }
    public void SetPositionYDelta(UInt16 positiony)
    {
      nudPositionYDelta.Value = positiony;
    }


    public UInt32 GetDelayMotion()
    {
      return (UInt32)nudDelayMotion.Value;
    }
    public void SetDelayMotion(UInt32 delay)
    {
      nudDelayMotion.Value = delay;
    }


    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public void ClearMessages()
    {
      tbxMessages.Text = "";
    }

    public void AddMessageLine(String line)
    {
      tbxMessages.Text += line;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //
    private void btnGetHelp_Click(object sender, EventArgs e)
    {
      if (FOnGetHelp is DOnGetHelp)
      {
        FOnGetHelp();
      }
    }

    private void btnGetProgramHeader_Click(object sender, EventArgs e)
    {
      if (FOnGetProgramHeader is DOnGetProgramHeader)
      {
        FOnGetProgramHeader();
      }
    }

    private void btnGetHardwareVersion_Click(object sender, EventArgs e)
    {
      if (FOnGetHardwareVersion is DOnGetHardwareVersion)
      {
        FOnGetHardwareVersion();
      }
    }

    private void btnGetSoftwareVersion_Click(object sender, EventArgs e)
    {
      if (FOnGetSoftwareVersion is DOnGetSoftwareVersion)
      {
        FOnGetSoftwareVersion();
      }
    }

    private void btnGetProcessCount_Click(object sender, EventArgs e)
    {
      if (FOnGetProcessCount is DOnGetProcessCount)
      {
        FOnGetProcessCount();
      }
    }

    private void btnSetProcessCount_Click(object sender, EventArgs e)
    {
      if (FOnSetProcessCount is DOnSetProcessCount)
      {
        UInt32 Count = (UInt32)nudProcessCount.Value;
        FOnSetProcessCount(Count);
      }
    }

    private void btnGetProcessPeriod_Click(object sender, EventArgs e)
    {
      if (FOnGetProcessPeriod is DOnGetProcessPeriod)
      {
        FOnGetProcessPeriod();
      }
    }

    private void btnSetProcessPeriod_Click(object sender, EventArgs e)
    {
      if (FOnSetProcessPeriod is DOnSetProcessPeriod)
      {
        UInt32 Period = (UInt32)nudProcessPeriod.Value;
        FOnSetProcessPeriod(Period);
      }
    }

    private void btnStopProcessExecution_Click(object sender, EventArgs e)
    {
      if (FOnStopProcessExecution is DOnStopProcessExecution)
      {
        FOnStopProcessExecution();
      }
    }

    private void btnGetLedSystem_Click(object sender, EventArgs e)
    {
      if (FOnGetLedSystem is DOnGetLedSystem)
      {
        FOnGetLedSystem();
      }
    }

    private void btnSetLedSystemLow_Click(object sender, EventArgs e)
    {
      if (FOnSetLedSystemLow is DOnSetLedSystemLow)
      {
        FOnSetLedSystemLow();
      }
    }

    private void btnSetLedSystemHigh_Click(object sender, EventArgs e)
    {
      if (FOnSetLedSystemHigh is DOnSetLedSystemHigh)
      {
        FOnSetLedSystemHigh();
      }
    }

    private void btnBlinkLedSystem_Click(object sender, EventArgs e)
    {
      if (FOnBlinkLedSystem is DOnBlinkLedSystem)
      {
        UInt32 Period = (UInt32)nudProcessPeriod.Value;
        UInt32 Count = (UInt32)nudProcessCount.Value;
        FOnBlinkLedSystem(Period, Count);
      }
    }

    private void btnGetPositionX_Click(object sender, EventArgs e)
    {
      if (FOnGetPositionX is DOnGetPositionX)
      {
        FOnGetPositionX();
      }
    }

    private void btnSetPositionX_Click(object sender, EventArgs e)
    {
      if (FOnSetPositionX is DOnSetPositionX)
      {
        UInt16 Position = (UInt16)nudPositionXActual.Value;
        FOnSetPositionX(Position);
      }
    }

    private void btnGetRangeX_Click(object sender, EventArgs e)
    {
      if (FOnGetRangeX is DOnGetRangeX)
      {
        FOnGetRangeX();
      }
    }

    private void btnSetRangeX_Click(object sender, EventArgs e)
    {
      if (FOnSetRangeX is DOnSetRangeX)
      {
        UInt16 Minimum = (UInt16)nudPositionXMinimum.Value;
        UInt16 Maximum = (UInt16)nudPositionXMaximum.Value;
        UInt16 Delta = (UInt16)nudPositionXDelta.Value;
        FOnSetRangeX(Minimum, Maximum, Delta);
      }
    }

    private void btnGetPositionY_Click(object sender, EventArgs e)
    {
      if (FOnGetPositionY is DOnGetPositionY)
      {
        FOnGetPositionY();
      }
    }

    private void btnSetPositionY_Click(object sender, EventArgs e)
    {
      if (FOnSetPositionY is DOnSetPositionY)
      {
        UInt16 Position = (UInt16)nudPositionYActual.Value;
        FOnSetPositionY(Position);
      }
    }

    private void btnGetRangeY_Click(object sender, EventArgs e)
    {
      if (FOnGetRangeY is DOnGetRangeY)
      {
        FOnGetRangeY();
      }
    }

    private void btnSetRangeY_Click(object sender, EventArgs e)
    {
      if (FOnSetRangeY is DOnSetRangeY)
      {
        UInt16 Minimum = (UInt16)nudPositionYMinimum.Value;
        UInt16 Maximum = (UInt16)nudPositionYMaximum.Value;
        UInt16 Delta = (UInt16)nudPositionYDelta.Value;
        FOnSetRangeY(Minimum, Maximum, Delta);
      }
    }

    private void btnGetDelayMotion_Click(object sender, EventArgs e)
    {
      if (FOnGetDelayMotion is DOnGetDelayMotion)
      {
        FOnGetDelayMotion();
      }
    }

    private void btnSetDelayMotion_Click(object sender, EventArgs e)
    {      
      if (FOnSetDelayMotion is DOnSetDelayMotion)
      {
        UInt32 Delay = (UInt16)nudDelayMotion.Value;
        FOnSetDelayMotion(Delay);
      }
    }

    private void btnPulseLaserPosition_Click(object sender, EventArgs e)
    {
      if (FOnPulseLaserPosition is DOnPulseLaserPosition)
      {
        UInt16 PositionX = (UInt16)nudPositionXActual.Value;
        UInt16 PositionY = (UInt16)nudPositionYActual.Value;
        UInt32 Period = (UInt32)nudProcessPeriod.Value;
        UInt32 Count = (UInt32)nudProcessCount.Value;
        FOnPulseLaserPosition(PositionX, PositionY, Period, Count);
      }
    }

    private void btnAbortLaserPosition_Click(object sender, EventArgs e)
    {
      if (FOnAbortLaserPosition is DOnAbortLaserPosition)
      {        
        FOnAbortLaserPosition();
      }
    }

    private void btnPulseLaserMatrix_Click(object sender, EventArgs e)
    {
      if (FOnPulseLaserMatrix is DOnPulseLaserMatrix)
      {
        UInt32 Period = (UInt32)nudProcessPeriod.Value;
        UInt32 Count = (UInt32)nudProcessCount.Value;
        // Starts thread !!!!
        FOnPulseLaserMatrix(Period, Count);
      }
    }

    private void btnAbortLaserMatrix_Click(object sender, EventArgs e)
    {
      if (FOnAbortLaserMatrix is DOnAbortLaserMatrix)
      {
        FOnAbortLaserMatrix();
      }
    }

    private void btnLoadLaserSteptable_Click(object sender, EventArgs e)
    {
      if (FOnLoadLaserSteptable is DOnLoadLaserSteptable)
      {
        if (DialogResult.OK == DialogLoadLaserSteptable.ShowDialog())
        {
          FOnLoadLaserSteptable(DialogLoadLaserSteptable.FileName);
        }
      }
    }

    private void btnPulseLaserImage_Click(object sender, EventArgs e)
    {
      if (FOnPulseLaserImage is DOnPulseLaserImage)
      {
        FOnPulseLaserImage();
      }
    }

    private void btnAbortLaserImage_Click(object sender, EventArgs e)
    {
      if (FOnAbortLaserImage is DOnAbortLaserImage)
      {
        FOnAbortLaserImage();
      }
    }

  }
}
