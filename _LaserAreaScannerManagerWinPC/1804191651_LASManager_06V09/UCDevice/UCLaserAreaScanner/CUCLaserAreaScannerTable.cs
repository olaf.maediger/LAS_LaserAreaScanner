﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPLaserAreaScanner;
//
namespace UCLaserAreaScanner
{
  public partial class CUCLaserAreaScannerTable : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    // 
    private DOnLoadLaserSteptable FOnLoadLaserSteptable;
    private DOnPulseLaserImage FOnPulseLaserImage;
    private DOnAbortLaserImage FOnAbortLaserImage;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCLaserAreaScannerTable()
    {
      InitializeComponent();
    }


    // LaserImage
    public void SetOnLoadLaserSteptable(DOnLoadLaserSteptable value)
    {
      FOnLoadLaserSteptable = value;
    }
    public void SetOnPulseLaserImage(DOnPulseLaserImage value)
    {
      FOnPulseLaserImage = value;
    }
    public void SetOnAbortLaserImage(DOnAbortLaserImage value)
    {
      FOnAbortLaserImage = value;
    }

    public void SetLaserPoints(UInt16[,] laserpoints)
    {
      if (laserpoints is UInt16[,])
      {
        Int32 Size = laserpoints.Length;
        if (0 < Size)
        {
          FUCLaserStepTable.ClearAllSteps();
          for (Int32 PI = 0; PI < Size; PI++)
          {
            UInt16 PX = laserpoints[PI, 0];
            UInt16 PY = laserpoints[PI, 1];
            UInt16 PP = laserpoints[PI, 2];
            UInt16 PC = laserpoints[PI, 3];
            FUCLaserStepTable.AddStep(PX, PY, PP, PC);
          }
        }
      }
    }

    public void SelectLaserStep(UInt32 index)
    {
      FUCLaserStepTable.SelectLaserStep(index);
    }



    private void btnLoadLaserSteptable_Click(object sender, EventArgs e)
    {
      if (FOnLoadLaserSteptable is DOnLoadLaserSteptable)
      {
        if (DialogResult.OK == DialogLoadLaserSteptable.ShowDialog())
        {
          FOnLoadLaserSteptable(DialogLoadLaserSteptable.FileName);
        }
      }
    }

    private void btnPulseLaserImage_Click(object sender, EventArgs e)
    {
      if (FOnPulseLaserImage is DOnPulseLaserImage)
      {
        FOnPulseLaserImage();
      }
    }

    private void btnAbortLaserImage_Click(object sender, EventArgs e)
    {
      if (FOnAbortLaserImage is DOnAbortLaserImage)
      {
        FOnAbortLaserImage();
      }
    }

  }
}
