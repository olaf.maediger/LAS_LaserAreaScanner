﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserStepMatrix
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pbxLaserStepMatrix = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pbxLaserStepMatrix)).BeginInit();
      this.SuspendLayout();
      // 
      // pbxLaserStepMatrix
      // 
      this.pbxLaserStepMatrix.BackColor = System.Drawing.SystemColors.Info;
      this.pbxLaserStepMatrix.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxLaserStepMatrix.Location = new System.Drawing.Point(0, 0);
      this.pbxLaserStepMatrix.Name = "pbxLaserStepMatrix";
      this.pbxLaserStepMatrix.Size = new System.Drawing.Size(361, 352);
      this.pbxLaserStepMatrix.TabIndex = 0;
      this.pbxLaserStepMatrix.TabStop = false;
      // 
      // CUCLaserStepMatrix
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pbxLaserStepMatrix);
      this.Name = "CUCLaserStepMatrix";
      this.Size = new System.Drawing.Size(361, 352);
      ((System.ComponentModel.ISupportInitialize)(this.pbxLaserStepMatrix)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox pbxLaserStepMatrix;
  }
}
