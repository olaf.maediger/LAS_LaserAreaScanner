﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserAreaScannerCommand
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pbxButton = new System.Windows.Forms.PictureBox();
      this.pnlState = new System.Windows.Forms.Panel();
      this.label9 = new System.Windows.Forms.Label();
      this.lblStateMTCIndex = new System.Windows.Forms.Label();
      this.lblStateMTControllerText = new System.Windows.Forms.Label();
      this.lblHeaderState = new System.Windows.Forms.Label();
      this.btnGetLedSystem = new System.Windows.Forms.Button();
      this.tbxMessages = new System.Windows.Forms.TextBox();
      this.tbxHardwareVersion = new System.Windows.Forms.TextBox();
      this.tbxSoftwareVersion = new System.Windows.Forms.TextBox();
      this.btnGetHelp = new System.Windows.Forms.Button();
      this.btnSwitchLedSystemOn = new System.Windows.Forms.Button();
      this.btnGetHardwareVersion = new System.Windows.Forms.Button();
      this.btnGetSoftwareVersion = new System.Windows.Forms.Button();
      this.btnGetProgramHeader = new System.Windows.Forms.Button();
      this.btnStopProcessExecution = new System.Windows.Forms.Button();
      this.btnSwitchLedSystemOff = new System.Windows.Forms.Button();
      this.btnBlinkLedSystem = new System.Windows.Forms.Button();
      this.label7 = new System.Windows.Forms.Label();
      this.btnPulseLaserPosition = new System.Windows.Forms.Button();
      this.nudProcessCount = new System.Windows.Forms.NumericUpDown();
      this.nudProcessPeriod = new System.Windows.Forms.NumericUpDown();
      this.btnSetProcessPeriod = new System.Windows.Forms.Button();
      this.btnSetProcessCount = new System.Windows.Forms.Button();
      this.pnlLedSystem = new System.Windows.Forms.Panel();
      this.pnlStateLedSystem = new System.Windows.Forms.Panel();
      this.lblHeaderLedSystem = new System.Windows.Forms.Label();
      this.lblHeader = new System.Windows.Forms.Label();
      this.btnGetProcessCount = new System.Windows.Forms.Button();
      this.btnGetProcessPeriod = new System.Windows.Forms.Button();
      this.lblProcessTime = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.nudPositionXMinimum = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.nudPositionXMaximum = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.nudPositionXDelta = new System.Windows.Forms.NumericUpDown();
      this.label5 = new System.Windows.Forms.Label();
      this.nudPositionXActual = new System.Windows.Forms.NumericUpDown();
      this.nudPositionYActual = new System.Windows.Forms.NumericUpDown();
      this.nudPositionYDelta = new System.Windows.Forms.NumericUpDown();
      this.nudPositionYMaximum = new System.Windows.Forms.NumericUpDown();
      this.nudPositionYMinimum = new System.Windows.Forms.NumericUpDown();
      this.label6 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.btnPulseLaserMatrix = new System.Windows.Forms.Button();
      this.btnGetPositionX = new System.Windows.Forms.Button();
      this.btnGetPositionY = new System.Windows.Forms.Button();
      this.btnGetRangeY = new System.Windows.Forms.Button();
      this.btnGetRangeX = new System.Windows.Forms.Button();
      this.btnPulseLaserImage = new System.Windows.Forms.Button();
      this.btnAbortLaserPosition = new System.Windows.Forms.Button();
      this.btnAbortLaserMatrix = new System.Windows.Forms.Button();
      this.btnAbortLaserImage = new System.Windows.Forms.Button();
      this.btnSetRangeY = new System.Windows.Forms.Button();
      this.btnSetRangeX = new System.Windows.Forms.Button();
      this.btnSetPositionY = new System.Windows.Forms.Button();
      this.btnSetPositionX = new System.Windows.Forms.Button();
      this.btnSetDelayMotion = new System.Windows.Forms.Button();
      this.btnGetDelayMotion = new System.Windows.Forms.Button();
      this.nudDelayMotion = new System.Windows.Forms.NumericUpDown();
      this.DialogLoadLaserSteptable = new System.Windows.Forms.OpenFileDialog();
      this.btnLoadLaserSteptable = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.pbxButton)).BeginInit();
      this.pnlState.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessPeriod)).BeginInit();
      this.pnlLedSystem.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXDelta)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXActual)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYActual)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYDelta)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayMotion)).BeginInit();
      this.SuspendLayout();
      // 
      // pbxButton
      // 
      this.pbxButton.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxButton.Location = new System.Drawing.Point(0, 0);
      this.pbxButton.Name = "pbxButton";
      this.pbxButton.Size = new System.Drawing.Size(565, 672);
      this.pbxButton.TabIndex = 0;
      this.pbxButton.TabStop = false;
      // 
      // pnlState
      // 
      this.pnlState.BackColor = System.Drawing.SystemColors.Info;
      this.pnlState.Controls.Add(this.label9);
      this.pnlState.Controls.Add(this.lblStateMTCIndex);
      this.pnlState.Controls.Add(this.lblStateMTControllerText);
      this.pnlState.Controls.Add(this.lblHeaderState);
      this.pnlState.Location = new System.Drawing.Point(249, 53);
      this.pnlState.Name = "pnlState";
      this.pnlState.Size = new System.Drawing.Size(311, 27);
      this.pnlState.TabIndex = 86;
      // 
      // label9
      // 
      this.label9.Location = new System.Drawing.Point(111, 7);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(8, 13);
      this.label9.TabIndex = 68;
      this.label9.Text = "-";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblStateMTCIndex
      // 
      this.lblStateMTCIndex.BackColor = System.Drawing.Color.PeachPuff;
      this.lblStateMTCIndex.Location = new System.Drawing.Point(61, 4);
      this.lblStateMTCIndex.Name = "lblStateMTCIndex";
      this.lblStateMTCIndex.Size = new System.Drawing.Size(47, 19);
      this.lblStateMTCIndex.TabIndex = 67;
      this.lblStateMTCIndex.Text = "- - -";
      this.lblStateMTCIndex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblStateMTControllerText
      // 
      this.lblStateMTControllerText.BackColor = System.Drawing.Color.PeachPuff;
      this.lblStateMTControllerText.Location = new System.Drawing.Point(121, 4);
      this.lblStateMTControllerText.Name = "lblStateMTControllerText";
      this.lblStateMTControllerText.Size = new System.Drawing.Size(184, 19);
      this.lblStateMTControllerText.TabIndex = 66;
      this.lblStateMTControllerText.Text = "- - -";
      this.lblStateMTControllerText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblHeaderState
      // 
      this.lblHeaderState.AutoSize = true;
      this.lblHeaderState.Location = new System.Drawing.Point(3, 7);
      this.lblHeaderState.Name = "lblHeaderState";
      this.lblHeaderState.Size = new System.Drawing.Size(55, 13);
      this.lblHeaderState.TabIndex = 65;
      this.lblHeaderState.Text = "State LAS";
      // 
      // btnGetLedSystem
      // 
      this.btnGetLedSystem.Location = new System.Drawing.Point(4, 441);
      this.btnGetLedSystem.Name = "btnGetLedSystem";
      this.btnGetLedSystem.Size = new System.Drawing.Size(89, 24);
      this.btnGetLedSystem.TabIndex = 82;
      this.btnGetLedSystem.Text = "GetLedSystem";
      this.btnGetLedSystem.UseVisualStyleBackColor = true;
      this.btnGetLedSystem.Click += new System.EventHandler(this.btnGetLedSystem_Click);
      // 
      // tbxMessages
      // 
      this.tbxMessages.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxMessages.Location = new System.Drawing.Point(4, 84);
      this.tbxMessages.Multiline = true;
      this.tbxMessages.Name = "tbxMessages";
      this.tbxMessages.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.tbxMessages.Size = new System.Drawing.Size(556, 321);
      this.tbxMessages.TabIndex = 78;
      // 
      // tbxHardwareVersion
      // 
      this.tbxHardwareVersion.Enabled = false;
      this.tbxHardwareVersion.Location = new System.Drawing.Point(125, 28);
      this.tbxHardwareVersion.Name = "tbxHardwareVersion";
      this.tbxHardwareVersion.Size = new System.Drawing.Size(106, 20);
      this.tbxHardwareVersion.TabIndex = 77;
      this.tbxHardwareVersion.Text = "-";
      this.tbxHardwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tbxSoftwareVersion
      // 
      this.tbxSoftwareVersion.Enabled = false;
      this.tbxSoftwareVersion.Location = new System.Drawing.Point(358, 29);
      this.tbxSoftwareVersion.Name = "tbxSoftwareVersion";
      this.tbxSoftwareVersion.Size = new System.Drawing.Size(78, 20);
      this.tbxSoftwareVersion.TabIndex = 76;
      this.tbxSoftwareVersion.Text = "-";
      this.tbxSoftwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnGetHelp
      // 
      this.btnGetHelp.Location = new System.Drawing.Point(3, 55);
      this.btnGetHelp.Name = "btnGetHelp";
      this.btnGetHelp.Size = new System.Drawing.Size(117, 23);
      this.btnGetHelp.TabIndex = 71;
      this.btnGetHelp.Text = "GetHelp";
      this.btnGetHelp.UseVisualStyleBackColor = true;
      this.btnGetHelp.Click += new System.EventHandler(this.btnGetHelp_Click);
      // 
      // btnSwitchLedSystemOn
      // 
      this.btnSwitchLedSystemOn.Location = new System.Drawing.Point(96, 441);
      this.btnSwitchLedSystemOn.Name = "btnSwitchLedSystemOn";
      this.btnSwitchLedSystemOn.Size = new System.Drawing.Size(118, 24);
      this.btnSwitchLedSystemOn.TabIndex = 70;
      this.btnSwitchLedSystemOn.Text = "SwitchLedSystemOn";
      this.btnSwitchLedSystemOn.UseVisualStyleBackColor = true;
      this.btnSwitchLedSystemOn.Click += new System.EventHandler(this.btnSetLedSystemHigh_Click);
      // 
      // btnGetHardwareVersion
      // 
      this.btnGetHardwareVersion.Location = new System.Drawing.Point(3, 26);
      this.btnGetHardwareVersion.Name = "btnGetHardwareVersion";
      this.btnGetHardwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetHardwareVersion.TabIndex = 68;
      this.btnGetHardwareVersion.Text = "GetHardwareVersion";
      this.btnGetHardwareVersion.UseVisualStyleBackColor = true;
      this.btnGetHardwareVersion.Click += new System.EventHandler(this.btnGetHardwareVersion_Click);
      // 
      // btnGetSoftwareVersion
      // 
      this.btnGetSoftwareVersion.Location = new System.Drawing.Point(237, 27);
      this.btnGetSoftwareVersion.Name = "btnGetSoftwareVersion";
      this.btnGetSoftwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetSoftwareVersion.TabIndex = 67;
      this.btnGetSoftwareVersion.Text = "GetSoftwareVersion";
      this.btnGetSoftwareVersion.UseVisualStyleBackColor = true;
      this.btnGetSoftwareVersion.Click += new System.EventHandler(this.btnGetSoftwareVersion_Click);
      // 
      // btnGetProgramHeader
      // 
      this.btnGetProgramHeader.Location = new System.Drawing.Point(125, 55);
      this.btnGetProgramHeader.Name = "btnGetProgramHeader";
      this.btnGetProgramHeader.Size = new System.Drawing.Size(118, 23);
      this.btnGetProgramHeader.TabIndex = 66;
      this.btnGetProgramHeader.Text = "GetProgramHeader";
      this.btnGetProgramHeader.UseVisualStyleBackColor = true;
      this.btnGetProgramHeader.Click += new System.EventHandler(this.btnGetProgramHeader_Click);
      // 
      // btnStopProcessExecution
      // 
      this.btnStopProcessExecution.Location = new System.Drawing.Point(458, 411);
      this.btnStopProcessExecution.Name = "btnStopProcessExecution";
      this.btnStopProcessExecution.Size = new System.Drawing.Size(80, 24);
      this.btnStopProcessExecution.TabIndex = 88;
      this.btnStopProcessExecution.Text = "StopProcess";
      this.btnStopProcessExecution.UseVisualStyleBackColor = true;
      this.btnStopProcessExecution.Click += new System.EventHandler(this.btnStopProcessExecution_Click);
      // 
      // btnSwitchLedSystemOff
      // 
      this.btnSwitchLedSystemOff.Location = new System.Drawing.Point(218, 441);
      this.btnSwitchLedSystemOff.Name = "btnSwitchLedSystemOff";
      this.btnSwitchLedSystemOff.Size = new System.Drawing.Size(117, 24);
      this.btnSwitchLedSystemOff.TabIndex = 92;
      this.btnSwitchLedSystemOff.Text = "SwitchLedSystemOff";
      this.btnSwitchLedSystemOff.UseVisualStyleBackColor = true;
      this.btnSwitchLedSystemOff.Click += new System.EventHandler(this.btnSetLedSystemLow_Click);
      // 
      // btnBlinkLedSystem
      // 
      this.btnBlinkLedSystem.Location = new System.Drawing.Point(339, 441);
      this.btnBlinkLedSystem.Name = "btnBlinkLedSystem";
      this.btnBlinkLedSystem.Size = new System.Drawing.Size(97, 24);
      this.btnBlinkLedSystem.TabIndex = 93;
      this.btnBlinkLedSystem.Text = "BlinkLedSystem";
      this.btnBlinkLedSystem.UseVisualStyleBackColor = true;
      this.btnBlinkLedSystem.Click += new System.EventHandler(this.btnBlinkLedSystem_Click);
      // 
      // label7
      // 
      this.label7.Location = new System.Drawing.Point(117, 494);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(48, 13);
      this.label7.TabIndex = 98;
      this.label7.Text = "MirrorX";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnPulseLaserPosition
      // 
      this.btnPulseLaserPosition.Location = new System.Drawing.Point(4, 576);
      this.btnPulseLaserPosition.Name = "btnPulseLaserPosition";
      this.btnPulseLaserPosition.Size = new System.Drawing.Size(106, 25);
      this.btnPulseLaserPosition.TabIndex = 101;
      this.btnPulseLaserPosition.Text = "PulseLaserPosition";
      this.btnPulseLaserPosition.UseVisualStyleBackColor = true;
      this.btnPulseLaserPosition.Click += new System.EventHandler(this.btnPulseLaserPosition_Click);
      // 
      // nudProcessCount
      // 
      this.nudProcessCount.Location = new System.Drawing.Point(156, 413);
      this.nudProcessCount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudProcessCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudProcessCount.Name = "nudProcessCount";
      this.nudProcessCount.Size = new System.Drawing.Size(55, 20);
      this.nudProcessCount.TabIndex = 74;
      this.nudProcessCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudProcessCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // nudProcessPeriod
      // 
      this.nudProcessPeriod.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudProcessPeriod.Location = new System.Drawing.Point(380, 413);
      this.nudProcessPeriod.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
      this.nudProcessPeriod.Name = "nudProcessPeriod";
      this.nudProcessPeriod.Size = new System.Drawing.Size(72, 20);
      this.nudProcessPeriod.TabIndex = 72;
      this.nudProcessPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudProcessPeriod.Value = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      // 
      // btnSetProcessPeriod
      // 
      this.btnSetProcessPeriod.Location = new System.Drawing.Point(253, 411);
      this.btnSetProcessPeriod.Name = "btnSetProcessPeriod";
      this.btnSetProcessPeriod.Size = new System.Drawing.Size(123, 24);
      this.btnSetProcessPeriod.TabIndex = 87;
      this.btnSetProcessPeriod.Text = "SetProcessPeriod [us]";
      this.btnSetProcessPeriod.UseVisualStyleBackColor = true;
      this.btnSetProcessPeriod.Click += new System.EventHandler(this.btnSetProcessPeriod_Click);
      // 
      // btnSetProcessCount
      // 
      this.btnSetProcessCount.Location = new System.Drawing.Point(39, 411);
      this.btnSetProcessCount.Name = "btnSetProcessCount";
      this.btnSetProcessCount.Size = new System.Drawing.Size(113, 24);
      this.btnSetProcessCount.TabIndex = 69;
      this.btnSetProcessCount.Text = "SetProcessCount [1]";
      this.btnSetProcessCount.UseVisualStyleBackColor = true;
      this.btnSetProcessCount.Click += new System.EventHandler(this.btnSetProcessCount_Click);
      // 
      // pnlLedSystem
      // 
      this.pnlLedSystem.BackColor = System.Drawing.SystemColors.Info;
      this.pnlLedSystem.Controls.Add(this.pnlStateLedSystem);
      this.pnlLedSystem.Controls.Add(this.lblHeaderLedSystem);
      this.pnlLedSystem.Location = new System.Drawing.Point(442, 440);
      this.pnlLedSystem.Name = "pnlLedSystem";
      this.pnlLedSystem.Size = new System.Drawing.Size(96, 28);
      this.pnlLedSystem.TabIndex = 104;
      // 
      // pnlStateLedSystem
      // 
      this.pnlStateLedSystem.BackColor = System.Drawing.Color.DarkRed;
      this.pnlStateLedSystem.Location = new System.Drawing.Point(68, 6);
      this.pnlStateLedSystem.Name = "pnlStateLedSystem";
      this.pnlStateLedSystem.Size = new System.Drawing.Size(17, 15);
      this.pnlStateLedSystem.TabIndex = 0;
      // 
      // lblHeaderLedSystem
      // 
      this.lblHeaderLedSystem.AutoSize = true;
      this.lblHeaderLedSystem.Location = new System.Drawing.Point(6, 7);
      this.lblHeaderLedSystem.Name = "lblHeaderLedSystem";
      this.lblHeaderLedSystem.Size = new System.Drawing.Size(59, 13);
      this.lblHeaderLedSystem.TabIndex = 65;
      this.lblHeaderLedSystem.Text = "LedSystem";
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(565, 23);
      this.lblHeader.TabIndex = 105;
      this.lblHeader.Text = "LaserAreaScanner Command";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnGetProcessCount
      // 
      this.btnGetProcessCount.Location = new System.Drawing.Point(4, 411);
      this.btnGetProcessCount.Name = "btnGetProcessCount";
      this.btnGetProcessCount.Size = new System.Drawing.Size(32, 24);
      this.btnGetProcessCount.TabIndex = 106;
      this.btnGetProcessCount.Text = "Get";
      this.btnGetProcessCount.UseVisualStyleBackColor = true;
      this.btnGetProcessCount.Click += new System.EventHandler(this.btnGetProcessCount_Click);
      // 
      // btnGetProcessPeriod
      // 
      this.btnGetProcessPeriod.Location = new System.Drawing.Point(218, 411);
      this.btnGetProcessPeriod.Name = "btnGetProcessPeriod";
      this.btnGetProcessPeriod.Size = new System.Drawing.Size(32, 24);
      this.btnGetProcessPeriod.TabIndex = 107;
      this.btnGetProcessPeriod.Text = "Get";
      this.btnGetProcessPeriod.UseVisualStyleBackColor = true;
      this.btnGetProcessPeriod.Click += new System.EventHandler(this.btnGetProcessPeriod_Click);
      // 
      // lblProcessTime
      // 
      this.lblProcessTime.Enabled = false;
      this.lblProcessTime.Location = new System.Drawing.Point(475, 29);
      this.lblProcessTime.Name = "lblProcessTime";
      this.lblProcessTime.Size = new System.Drawing.Size(84, 20);
      this.lblProcessTime.TabIndex = 108;
      this.lblProcessTime.Text = "-";
      this.lblProcessTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(442, 32);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(30, 13);
      this.label1.TabIndex = 109;
      this.label1.Text = "Time";
      // 
      // nudPositionXMinimum
      // 
      this.nudPositionXMinimum.Location = new System.Drawing.Point(223, 492);
      this.nudPositionXMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXMinimum.Name = "nudPositionXMinimum";
      this.nudPositionXMinimum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXMinimum.TabIndex = 111;
      this.nudPositionXMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(226, 476);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(51, 13);
      this.label2.TabIndex = 112;
      this.label2.Text = "Minimum";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(284, 476);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(51, 13);
      this.label3.TabIndex = 114;
      this.label3.Text = "Maximum";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPositionXMaximum
      // 
      this.nudPositionXMaximum.Location = new System.Drawing.Point(283, 492);
      this.nudPositionXMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXMaximum.Name = "nudPositionXMaximum";
      this.nudPositionXMaximum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXMaximum.TabIndex = 113;
      this.nudPositionXMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(345, 476);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(51, 13);
      this.label4.TabIndex = 116;
      this.label4.Text = "Delta";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPositionXDelta
      // 
      this.nudPositionXDelta.Location = new System.Drawing.Point(343, 492);
      this.nudPositionXDelta.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXDelta.Name = "nudPositionXDelta";
      this.nudPositionXDelta.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXDelta.TabIndex = 115;
      this.nudPositionXDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXDelta.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(166, 476);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(51, 13);
      this.label5.TabIndex = 118;
      this.label5.Text = "Actual";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPositionXActual
      // 
      this.nudPositionXActual.Location = new System.Drawing.Point(163, 492);
      this.nudPositionXActual.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXActual.Name = "nudPositionXActual";
      this.nudPositionXActual.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXActual.TabIndex = 117;
      this.nudPositionXActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXActual.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // nudPositionYActual
      // 
      this.nudPositionYActual.Location = new System.Drawing.Point(163, 518);
      this.nudPositionYActual.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYActual.Name = "nudPositionYActual";
      this.nudPositionYActual.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYActual.TabIndex = 123;
      this.nudPositionYActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYActual.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // nudPositionYDelta
      // 
      this.nudPositionYDelta.Location = new System.Drawing.Point(343, 518);
      this.nudPositionYDelta.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYDelta.Name = "nudPositionYDelta";
      this.nudPositionYDelta.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYDelta.TabIndex = 122;
      this.nudPositionYDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYDelta.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // nudPositionYMaximum
      // 
      this.nudPositionYMaximum.Location = new System.Drawing.Point(283, 518);
      this.nudPositionYMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYMaximum.Name = "nudPositionYMaximum";
      this.nudPositionYMaximum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYMaximum.TabIndex = 121;
      this.nudPositionYMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // nudPositionYMinimum
      // 
      this.nudPositionYMinimum.Location = new System.Drawing.Point(223, 518);
      this.nudPositionYMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYMinimum.Name = "nudPositionYMinimum";
      this.nudPositionYMinimum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYMinimum.TabIndex = 120;
      this.nudPositionYMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // label6
      // 
      this.label6.Location = new System.Drawing.Point(117, 520);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(48, 13);
      this.label6.TabIndex = 119;
      this.label6.Text = "MirrorY";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label8
      // 
      this.label8.Location = new System.Drawing.Point(104, 476);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(79, 13);
      this.label8.TabIndex = 124;
      this.label8.Text = "Position";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnPulseLaserMatrix
      // 
      this.btnPulseLaserMatrix.Location = new System.Drawing.Point(228, 576);
      this.btnPulseLaserMatrix.Name = "btnPulseLaserMatrix";
      this.btnPulseLaserMatrix.Size = new System.Drawing.Size(106, 25);
      this.btnPulseLaserMatrix.TabIndex = 125;
      this.btnPulseLaserMatrix.Text = "PulseLaserMatrix";
      this.btnPulseLaserMatrix.UseVisualStyleBackColor = true;
      this.btnPulseLaserMatrix.Click += new System.EventHandler(this.btnPulseLaserMatrix_Click);
      // 
      // btnGetPositionX
      // 
      this.btnGetPositionX.Location = new System.Drawing.Point(4, 489);
      this.btnGetPositionX.Name = "btnGetPositionX";
      this.btnGetPositionX.Size = new System.Drawing.Size(34, 24);
      this.btnGetPositionX.TabIndex = 126;
      this.btnGetPositionX.Text = "GetPositionX";
      this.btnGetPositionX.UseVisualStyleBackColor = true;
      this.btnGetPositionX.Click += new System.EventHandler(this.btnGetPositionX_Click);
      // 
      // btnGetPositionY
      // 
      this.btnGetPositionY.Location = new System.Drawing.Point(4, 514);
      this.btnGetPositionY.Name = "btnGetPositionY";
      this.btnGetPositionY.Size = new System.Drawing.Size(34, 24);
      this.btnGetPositionY.TabIndex = 127;
      this.btnGetPositionY.Text = "GetPositionY";
      this.btnGetPositionY.UseVisualStyleBackColor = true;
      this.btnGetPositionY.Click += new System.EventHandler(this.btnGetPositionY_Click);
      // 
      // btnGetRangeY
      // 
      this.btnGetRangeY.Location = new System.Drawing.Point(403, 515);
      this.btnGetRangeY.Name = "btnGetRangeY";
      this.btnGetRangeY.Size = new System.Drawing.Size(34, 24);
      this.btnGetRangeY.TabIndex = 129;
      this.btnGetRangeY.Text = "Get";
      this.btnGetRangeY.UseVisualStyleBackColor = true;
      this.btnGetRangeY.Click += new System.EventHandler(this.btnGetRangeY_Click);
      // 
      // btnGetRangeX
      // 
      this.btnGetRangeX.Location = new System.Drawing.Point(403, 490);
      this.btnGetRangeX.Name = "btnGetRangeX";
      this.btnGetRangeX.Size = new System.Drawing.Size(34, 24);
      this.btnGetRangeX.TabIndex = 128;
      this.btnGetRangeX.Text = "Get";
      this.btnGetRangeX.UseVisualStyleBackColor = true;
      this.btnGetRangeX.Click += new System.EventHandler(this.btnGetRangeX_Click);
      // 
      // btnPulseLaserImage
      // 
      this.btnPulseLaserImage.Location = new System.Drawing.Point(116, 607);
      this.btnPulseLaserImage.Name = "btnPulseLaserImage";
      this.btnPulseLaserImage.Size = new System.Drawing.Size(106, 25);
      this.btnPulseLaserImage.TabIndex = 130;
      this.btnPulseLaserImage.Text = "PulseLaserImage";
      this.btnPulseLaserImage.UseVisualStyleBackColor = true;
      this.btnPulseLaserImage.Click += new System.EventHandler(this.btnPulseLaserImage_Click);
      // 
      // btnAbortLaserPosition
      // 
      this.btnAbortLaserPosition.Location = new System.Drawing.Point(116, 576);
      this.btnAbortLaserPosition.Name = "btnAbortLaserPosition";
      this.btnAbortLaserPosition.Size = new System.Drawing.Size(106, 25);
      this.btnAbortLaserPosition.TabIndex = 131;
      this.btnAbortLaserPosition.Text = "AbortLaserPosition";
      this.btnAbortLaserPosition.UseVisualStyleBackColor = true;
      this.btnAbortLaserPosition.Click += new System.EventHandler(this.btnAbortLaserPosition_Click);
      // 
      // btnAbortLaserMatrix
      // 
      this.btnAbortLaserMatrix.Location = new System.Drawing.Point(339, 576);
      this.btnAbortLaserMatrix.Name = "btnAbortLaserMatrix";
      this.btnAbortLaserMatrix.Size = new System.Drawing.Size(106, 25);
      this.btnAbortLaserMatrix.TabIndex = 132;
      this.btnAbortLaserMatrix.Text = "AbortLaserMatrix";
      this.btnAbortLaserMatrix.UseVisualStyleBackColor = true;
      this.btnAbortLaserMatrix.Click += new System.EventHandler(this.btnAbortLaserMatrix_Click);
      // 
      // btnAbortLaserImage
      // 
      this.btnAbortLaserImage.Location = new System.Drawing.Point(228, 607);
      this.btnAbortLaserImage.Name = "btnAbortLaserImage";
      this.btnAbortLaserImage.Size = new System.Drawing.Size(106, 25);
      this.btnAbortLaserImage.TabIndex = 133;
      this.btnAbortLaserImage.Text = "AbortLaserImage";
      this.btnAbortLaserImage.UseVisualStyleBackColor = true;
      this.btnAbortLaserImage.Click += new System.EventHandler(this.btnAbortLaserImage_Click);
      // 
      // btnSetRangeY
      // 
      this.btnSetRangeY.Location = new System.Drawing.Point(440, 515);
      this.btnSetRangeY.Name = "btnSetRangeY";
      this.btnSetRangeY.Size = new System.Drawing.Size(76, 24);
      this.btnSetRangeY.TabIndex = 136;
      this.btnSetRangeY.Text = "SetRangeY";
      this.btnSetRangeY.UseVisualStyleBackColor = true;
      this.btnSetRangeY.Click += new System.EventHandler(this.btnSetRangeY_Click);
      // 
      // btnSetRangeX
      // 
      this.btnSetRangeX.Location = new System.Drawing.Point(440, 490);
      this.btnSetRangeX.Name = "btnSetRangeX";
      this.btnSetRangeX.Size = new System.Drawing.Size(76, 24);
      this.btnSetRangeX.TabIndex = 135;
      this.btnSetRangeX.Text = "SetRangeX";
      this.btnSetRangeX.UseVisualStyleBackColor = true;
      this.btnSetRangeX.Click += new System.EventHandler(this.btnSetRangeX_Click);
      // 
      // btnSetPositionY
      // 
      this.btnSetPositionY.Location = new System.Drawing.Point(42, 514);
      this.btnSetPositionY.Name = "btnSetPositionY";
      this.btnSetPositionY.Size = new System.Drawing.Size(79, 24);
      this.btnSetPositionY.TabIndex = 138;
      this.btnSetPositionY.Text = "SetPositionY";
      this.btnSetPositionY.UseVisualStyleBackColor = true;
      this.btnSetPositionY.Click += new System.EventHandler(this.btnSetPositionY_Click);
      // 
      // btnSetPositionX
      // 
      this.btnSetPositionX.Location = new System.Drawing.Point(42, 489);
      this.btnSetPositionX.Name = "btnSetPositionX";
      this.btnSetPositionX.Size = new System.Drawing.Size(79, 24);
      this.btnSetPositionX.TabIndex = 137;
      this.btnSetPositionX.Text = "SetPositionX";
      this.btnSetPositionX.UseVisualStyleBackColor = true;
      this.btnSetPositionX.Click += new System.EventHandler(this.btnSetPositionX_Click);
      // 
      // btnSetDelayMotion
      // 
      this.btnSetDelayMotion.Location = new System.Drawing.Point(42, 541);
      this.btnSetDelayMotion.Name = "btnSetDelayMotion";
      this.btnSetDelayMotion.Size = new System.Drawing.Size(115, 24);
      this.btnSetDelayMotion.TabIndex = 142;
      this.btnSetDelayMotion.Text = "SetDelayMotion [us]";
      this.btnSetDelayMotion.UseVisualStyleBackColor = true;
      this.btnSetDelayMotion.Click += new System.EventHandler(this.btnSetDelayMotion_Click);
      // 
      // btnGetDelayMotion
      // 
      this.btnGetDelayMotion.Location = new System.Drawing.Point(4, 541);
      this.btnGetDelayMotion.Name = "btnGetDelayMotion";
      this.btnGetDelayMotion.Size = new System.Drawing.Size(34, 24);
      this.btnGetDelayMotion.TabIndex = 141;
      this.btnGetDelayMotion.Text = "GetPositionX";
      this.btnGetDelayMotion.UseVisualStyleBackColor = true;
      this.btnGetDelayMotion.Click += new System.EventHandler(this.btnGetDelayMotion_Click);
      // 
      // nudDelayMotion
      // 
      this.nudDelayMotion.Location = new System.Drawing.Point(163, 544);
      this.nudDelayMotion.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudDelayMotion.Name = "nudDelayMotion";
      this.nudDelayMotion.Size = new System.Drawing.Size(55, 20);
      this.nudDelayMotion.TabIndex = 140;
      this.nudDelayMotion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelayMotion.Value = new decimal(new int[] {
            9000,
            0,
            0,
            0});
      // 
      // btnLoadLaserText
      // 
      this.btnLoadLaserSteptable.Location = new System.Drawing.Point(4, 607);
      this.btnLoadLaserSteptable.Name = "btnLoadLaserSteptable";
      this.btnLoadLaserSteptable.Size = new System.Drawing.Size(106, 25);
      this.btnLoadLaserSteptable.TabIndex = 143;
      this.btnLoadLaserSteptable.Text = "Load Steptable";
      this.btnLoadLaserSteptable.UseVisualStyleBackColor = true;
      this.btnLoadLaserSteptable.Click += new System.EventHandler(this.btnLoadLaserSteptable_Click);
      // 
      // CUCLaserAreaScannerCommand
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnLoadLaserSteptable);
      this.Controls.Add(this.btnSetDelayMotion);
      this.Controls.Add(this.btnGetDelayMotion);
      this.Controls.Add(this.nudDelayMotion);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.btnSetPositionY);
      this.Controls.Add(this.btnSetPositionX);
      this.Controls.Add(this.btnSetRangeY);
      this.Controls.Add(this.btnSetRangeX);
      this.Controls.Add(this.btnAbortLaserImage);
      this.Controls.Add(this.btnAbortLaserMatrix);
      this.Controls.Add(this.btnAbortLaserPosition);
      this.Controls.Add(this.btnPulseLaserImage);
      this.Controls.Add(this.btnGetRangeY);
      this.Controls.Add(this.btnGetRangeX);
      this.Controls.Add(this.btnGetPositionY);
      this.Controls.Add(this.btnGetPositionX);
      this.Controls.Add(this.btnPulseLaserMatrix);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.nudPositionYActual);
      this.Controls.Add(this.nudPositionYDelta);
      this.Controls.Add(this.nudPositionYMaximum);
      this.Controls.Add(this.nudPositionYMinimum);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.nudPositionXActual);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.nudPositionXDelta);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.nudPositionXMaximum);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.nudPositionXMinimum);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.lblProcessTime);
      this.Controls.Add(this.btnGetProcessPeriod);
      this.Controls.Add(this.btnGetProcessCount);
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.pnlLedSystem);
      this.Controls.Add(this.btnPulseLaserPosition);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.btnBlinkLedSystem);
      this.Controls.Add(this.btnSwitchLedSystemOff);
      this.Controls.Add(this.btnStopProcessExecution);
      this.Controls.Add(this.btnSetProcessPeriod);
      this.Controls.Add(this.pnlState);
      this.Controls.Add(this.btnGetLedSystem);
      this.Controls.Add(this.tbxMessages);
      this.Controls.Add(this.tbxHardwareVersion);
      this.Controls.Add(this.tbxSoftwareVersion);
      this.Controls.Add(this.nudProcessCount);
      this.Controls.Add(this.nudProcessPeriod);
      this.Controls.Add(this.btnGetHelp);
      this.Controls.Add(this.btnSwitchLedSystemOn);
      this.Controls.Add(this.btnSetProcessCount);
      this.Controls.Add(this.btnGetHardwareVersion);
      this.Controls.Add(this.btnGetSoftwareVersion);
      this.Controls.Add(this.btnGetProgramHeader);
      this.Controls.Add(this.pbxButton);
      this.Name = "CUCLaserAreaScannerCommand";
      this.Size = new System.Drawing.Size(565, 672);
      ((System.ComponentModel.ISupportInitialize)(this.pbxButton)).EndInit();
      this.pnlState.ResumeLayout(false);
      this.pnlState.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessPeriod)).EndInit();
      this.pnlLedSystem.ResumeLayout(false);
      this.pnlLedSystem.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXDelta)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXActual)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYActual)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYDelta)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayMotion)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox pbxButton;
    private System.Windows.Forms.Panel pnlState;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label lblStateMTCIndex;
    private System.Windows.Forms.Label lblStateMTControllerText;
    private System.Windows.Forms.Label lblHeaderState;
    private System.Windows.Forms.Button btnGetLedSystem;
    private System.Windows.Forms.TextBox tbxMessages;
    private System.Windows.Forms.TextBox tbxHardwareVersion;
    private System.Windows.Forms.TextBox tbxSoftwareVersion;
    private System.Windows.Forms.Button btnGetHelp;
    private System.Windows.Forms.Button btnSwitchLedSystemOn;
    private System.Windows.Forms.Button btnGetHardwareVersion;
    private System.Windows.Forms.Button btnGetSoftwareVersion;
    private System.Windows.Forms.Button btnGetProgramHeader;
    private System.Windows.Forms.Button btnStopProcessExecution;
    private System.Windows.Forms.Button btnSwitchLedSystemOff;
    private System.Windows.Forms.Button btnBlinkLedSystem;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Button btnPulseLaserPosition;
    private System.Windows.Forms.NumericUpDown nudProcessCount;
    private System.Windows.Forms.NumericUpDown nudProcessPeriod;
    private System.Windows.Forms.Button btnSetProcessPeriod;
    private System.Windows.Forms.Button btnSetProcessCount;
    private System.Windows.Forms.Panel pnlLedSystem;
    private System.Windows.Forms.Panel pnlStateLedSystem;
    private System.Windows.Forms.Label lblHeaderLedSystem;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Button btnGetProcessCount;
    private System.Windows.Forms.Button btnGetProcessPeriod;
    private System.Windows.Forms.TextBox lblProcessTime;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudPositionXMinimum;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown nudPositionXMaximum;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.NumericUpDown nudPositionXDelta;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.NumericUpDown nudPositionXActual;
    private System.Windows.Forms.NumericUpDown nudPositionYActual;
    private System.Windows.Forms.NumericUpDown nudPositionYDelta;
    private System.Windows.Forms.NumericUpDown nudPositionYMaximum;
    private System.Windows.Forms.NumericUpDown nudPositionYMinimum;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Button btnPulseLaserMatrix;
    private System.Windows.Forms.Button btnGetPositionX;
    private System.Windows.Forms.Button btnGetPositionY;
    private System.Windows.Forms.Button btnGetRangeY;
    private System.Windows.Forms.Button btnGetRangeX;
    private System.Windows.Forms.Button btnPulseLaserImage;
    private System.Windows.Forms.Button btnAbortLaserPosition;
    private System.Windows.Forms.Button btnAbortLaserMatrix;
    private System.Windows.Forms.Button btnAbortLaserImage;
    private System.Windows.Forms.Button btnSetRangeY;
    private System.Windows.Forms.Button btnSetRangeX;
    private System.Windows.Forms.Button btnSetPositionY;
    private System.Windows.Forms.Button btnSetPositionX;
    private System.Windows.Forms.Button btnSetDelayMotion;
    private System.Windows.Forms.Button btnGetDelayMotion;
    private System.Windows.Forms.NumericUpDown nudDelayMotion;
    private System.Windows.Forms.OpenFileDialog DialogLoadLaserSteptable;
    private System.Windows.Forms.Button btnLoadLaserSteptable;
  }
}
