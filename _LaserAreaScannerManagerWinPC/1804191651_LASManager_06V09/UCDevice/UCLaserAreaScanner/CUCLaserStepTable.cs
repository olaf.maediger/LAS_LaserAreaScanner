﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCLaserAreaScanner
{
  public partial class CUCLaserStepTable : UserControl
  {
    public const int XLOCATION = 0;
    public const int YLOCATION = 1;
    public const int PULSEPERIODMS = 2;
    public const int PULSECOUNT = 3;

    public CUCLaserStepTable()
    {
      InitializeComponent();
    }

    public void ClearAllSteps()
    {
      dgvSteptable.Rows.Clear();
    }

    public void AddStep(UInt16 xposition, UInt16 yposition,
                        UInt16 pulseperiod, UInt16 pulsecount)
    {
      dgvSteptable.Rows.Add(String.Format("{0}", dgvSteptable.RowCount),
                            String.Format("{0}", xposition),
                            String.Format("{0}", yposition),
                            String.Format("{0}", pulseperiod),
                            String.Format("{0}", pulsecount));
    }

    public void SelectLaserStep(UInt32 index)
    {
      if (index < dgvSteptable.RowCount)
      {
        dgvSteptable.Rows[(int)index].Selected = true;
      }        
    }




  }
}