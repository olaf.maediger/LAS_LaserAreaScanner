﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
namespace Task
{
  public class CProcess : CTask
  {
    //private CTask FTask;
    private static DOnExecutionStart FOnExecutionStart;
    private static DOnExecutionBusy FOnExecutionBusy;
    private static DOnExecutionEnd FOnExecutionEnd;
    private static DOnExecutionAbort FOnExecutionAbort;

    public CProcess(String name, 
                    DOnExecutionStart onstart,
                    DOnExecutionBusy onbusy,
                    DOnExecutionEnd onend,
                    DOnExecutionAbort onabort)
      : base(name,
             TaskOnExecutionStart,
             TaskOnExecutionBusy,
             TaskOnExecutionEnd,
             TaskOnExecutionAbort)
    {
      FOnExecutionStart = onstart;
      FOnExecutionBusy = onbusy;
      FOnExecutionEnd = onend;
      FOnExecutionAbort = onabort;
    }

    //public Boolean Start()
    //{
    //  if (!FTask.IsStarted())
    //  {
    //    return FTask.Start();
    //  }
    //  return false;
    //}
    //public Boolean Abort()
    //{
    //  if (FTask.IsActive())
    //  {
    //    return FTask.Abort();
    //  }
    //  return false;
    //}

    protected static void TaskOnExecutionStart(RTaskData data)
    {
      if (FOnExecutionStart is DOnExecutionStart)
      {
        FOnExecutionStart(data);
      }
    }
    protected static Boolean TaskOnExecutionBusy(RTaskData data)
    {
      if (FOnExecutionBusy is DOnExecutionBusy)
      {
        return FOnExecutionBusy(data);
      }
      return false;
    }
    protected static void TaskOnExecutionEnd(RTaskData data)
    {
      if (FOnExecutionEnd is DOnExecutionEnd)
      {
        FOnExecutionEnd(data);
      }
    }
    protected static void TaskOnExecutionAbort(RTaskData data)
    {
      if (FOnExecutionAbort is DOnExecutionAbort)
      {
        FOnExecutionAbort(data);
      }
    }



  }
}
