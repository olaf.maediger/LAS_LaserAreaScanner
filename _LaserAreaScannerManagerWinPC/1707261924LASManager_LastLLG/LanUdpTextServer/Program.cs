﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
//
namespace LanUdpTextServer
{

  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static Mutex MutexSingleApplication = new Mutex(false, "LanUdpTextServer");
    //
    [STAThread]
    static void Main()
    {
      if (!MutexSingleApplication.WaitOne(TimeSpan.FromMilliseconds(1000), false))
      { // Aplication is still running!
        return;
      }
      // this will be the only running Application:
      try
      {
        Application.EnableVisualStyles();
        Application.SetCompatibleTextRenderingDefault(false);
        Application.Run(new FormServer());
      }
      finally
      {
        MutexSingleApplication.ReleaseMutex();
      }
    }
  }
}
