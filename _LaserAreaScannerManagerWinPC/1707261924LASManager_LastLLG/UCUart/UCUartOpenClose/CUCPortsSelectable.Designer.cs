﻿namespace UCUartOpenClose
{
  partial class CUCPortsSelectable
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxPortsSelectable = new System.Windows.Forms.ComboBox();
      this.label5 = new System.Windows.Forms.Label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.SuspendLayout();
      // 
      // cbxPortsSelectable
      // 
      this.cbxPortsSelectable.Dock = System.Windows.Forms.DockStyle.Right;
      this.cbxPortsSelectable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPortsSelectable.FormattingEnabled = true;
      this.cbxPortsSelectable.Location = new System.Drawing.Point(58, 0);
      this.cbxPortsSelectable.Name = "cbxPortsSelectable";
      this.cbxPortsSelectable.Size = new System.Drawing.Size(61, 21);
      this.cbxPortsSelectable.TabIndex = 54;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label5.Location = new System.Drawing.Point(0, 4);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(58, 21);
      this.label5.TabIndex = 64;
      this.label5.Text = "Selectable";
      // 
      // panel1
      // 
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(58, 4);
      this.panel1.TabIndex = 63;
      // 
      // CUCPortsSelectable
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label5);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.cbxPortsSelectable);
      this.Name = "CUCPortsSelectable";
      this.Size = new System.Drawing.Size(119, 25);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ComboBox cbxPortsSelectable;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Panel panel1;
  }
}
