﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserAreaScannerTable
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlBottom = new System.Windows.Forms.Panel();
      this.btnLoadLaserSteptable = new System.Windows.Forms.Button();
      this.btnAbortLaserImage = new System.Windows.Forms.Button();
      this.btnPulseLaserImage = new System.Windows.Forms.Button();
      this.DialogLoadLaserSteptable = new System.Windows.Forms.OpenFileDialog();
      this.FUCLaserStepTable = new UCLaserAreaScanner.CUCLaserStepTable();
      this.cbxSkipZeroPulses = new System.Windows.Forms.CheckBox();
      this.pnlBottom.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlBottom
      // 
      this.pnlBottom.Controls.Add(this.cbxSkipZeroPulses);
      this.pnlBottom.Controls.Add(this.btnLoadLaserSteptable);
      this.pnlBottom.Controls.Add(this.btnAbortLaserImage);
      this.pnlBottom.Controls.Add(this.btnPulseLaserImage);
      this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlBottom.Location = new System.Drawing.Point(0, 413);
      this.pnlBottom.Name = "pnlBottom";
      this.pnlBottom.Size = new System.Drawing.Size(639, 37);
      this.pnlBottom.TabIndex = 1;
      // 
      // btnLoadLaserSteptable
      // 
      this.btnLoadLaserSteptable.Location = new System.Drawing.Point(7, 6);
      this.btnLoadLaserSteptable.Name = "btnLoadLaserSteptable";
      this.btnLoadLaserSteptable.Size = new System.Drawing.Size(106, 25);
      this.btnLoadLaserSteptable.TabIndex = 146;
      this.btnLoadLaserSteptable.Text = "Load Steptable";
      this.btnLoadLaserSteptable.UseVisualStyleBackColor = true;
      this.btnLoadLaserSteptable.Click += new System.EventHandler(this.btnLoadLaserSteptable_Click);
      // 
      // btnAbortLaserImage
      // 
      this.btnAbortLaserImage.Location = new System.Drawing.Point(343, 6);
      this.btnAbortLaserImage.Name = "btnAbortLaserImage";
      this.btnAbortLaserImage.Size = new System.Drawing.Size(106, 25);
      this.btnAbortLaserImage.TabIndex = 145;
      this.btnAbortLaserImage.Text = "Abort LaserImage";
      this.btnAbortLaserImage.UseVisualStyleBackColor = true;
      this.btnAbortLaserImage.Click += new System.EventHandler(this.btnAbortLaserImage_Click);
      // 
      // btnPulseLaserImage
      // 
      this.btnPulseLaserImage.Location = new System.Drawing.Point(231, 6);
      this.btnPulseLaserImage.Name = "btnPulseLaserImage";
      this.btnPulseLaserImage.Size = new System.Drawing.Size(106, 25);
      this.btnPulseLaserImage.TabIndex = 144;
      this.btnPulseLaserImage.Text = "Pulse LaserImage";
      this.btnPulseLaserImage.UseVisualStyleBackColor = true;
      this.btnPulseLaserImage.Click += new System.EventHandler(this.btnPulseLaserImage_Click);
      // 
      // DialogLoadLaserSteptable
      // 
      this.DialogLoadLaserSteptable.Filter = "Steptable (*.stp)|*.stp|All Files (*.*)|*.*";
      this.DialogLoadLaserSteptable.Title = "Load Steptable";
      // 
      // FUCLaserStepTable
      // 
      this.FUCLaserStepTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCLaserStepTable.Location = new System.Drawing.Point(0, 0);
      this.FUCLaserStepTable.Name = "FUCLaserStepTable";
      this.FUCLaserStepTable.Size = new System.Drawing.Size(639, 413);
      this.FUCLaserStepTable.TabIndex = 2;
      // 
      // cbxSkipZeroPulses
      // 
      this.cbxSkipZeroPulses.AutoSize = true;
      this.cbxSkipZeroPulses.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.cbxSkipZeroPulses.Location = new System.Drawing.Point(119, 11);
      this.cbxSkipZeroPulses.Name = "cbxSkipZeroPulses";
      this.cbxSkipZeroPulses.Size = new System.Drawing.Size(100, 17);
      this.cbxSkipZeroPulses.TabIndex = 147;
      this.cbxSkipZeroPulses.Text = "SkipZeroPulses";
      this.cbxSkipZeroPulses.UseVisualStyleBackColor = true;
      // 
      // CUCLaserAreaScannerTable
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCLaserStepTable);
      this.Controls.Add(this.pnlBottom);
      this.Name = "CUCLaserAreaScannerTable";
      this.Size = new System.Drawing.Size(639, 450);
      this.pnlBottom.ResumeLayout(false);
      this.pnlBottom.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion
    private System.Windows.Forms.Panel pnlBottom;
    private System.Windows.Forms.Button btnLoadLaserSteptable;
    private System.Windows.Forms.Button btnAbortLaserImage;
    private System.Windows.Forms.Button btnPulseLaserImage;
    private System.Windows.Forms.OpenFileDialog DialogLoadLaserSteptable;
    private CUCLaserStepTable FUCLaserStepTable;
    private System.Windows.Forms.CheckBox cbxSkipZeroPulses;
  }
}
