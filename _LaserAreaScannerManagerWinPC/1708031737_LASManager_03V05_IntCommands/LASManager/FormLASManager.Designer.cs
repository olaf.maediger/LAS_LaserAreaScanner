﻿namespace LASManager
{
  partial class FormClient
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlProtocol = new System.Windows.Forms.Panel();
      this.FUCSerialNumber = new UCSerialNumber.CUCSerialNumber();
      this.splProtocol = new System.Windows.Forms.Splitter();
      this.tmrStartup = new System.Windows.Forms.Timer(this.components);
      this.FHelpProvider = new System.Windows.Forms.HelpProvider();
      this.DialogSaveInitdata = new System.Windows.Forms.SaveFileDialog();
      this.DialogLoadInitdata = new System.Windows.Forms.OpenFileDialog();
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.mitSystem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.mitConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCDefault = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCResetToDefaultConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveAutomaticAtProgramEnd = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCStartup = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitElevator = new System.Windows.Forms.ToolStripMenuItem();
      this.mitStartSimulation = new System.Windows.Forms.ToolStripMenuItem();
      this.mitProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPEditParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPClearProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
      this.diagnosticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPLoadDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPSaveDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSendDiagnosticEmail = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHelp = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHContents = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHTopic = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHSearch = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterApplicationProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterDeviceProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.tbcMain = new System.Windows.Forms.TabControl();
      this.tbpLaserAreaScanner = new System.Windows.Forms.TabPage();
      this.tbxMessages = new System.Windows.Forms.TextBox();
      this.tbxHardwareVersion = new System.Windows.Forms.TextBox();
      this.tbxSoftwareVersion = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.nudLedTimes = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.nudLedDelay = new System.Windows.Forms.NumericUpDown();
      this.nudLedIndex = new System.Windows.Forms.NumericUpDown();
      this.btnGetHelp = new System.Windows.Forms.Button();
      this.button16 = new System.Windows.Forms.Button();
      this.button15 = new System.Windows.Forms.Button();
      this.button14 = new System.Windows.Forms.Button();
      this.button13 = new System.Windows.Forms.Button();
      this.button12 = new System.Windows.Forms.Button();
      this.button11 = new System.Windows.Forms.Button();
      this.button10 = new System.Windows.Forms.Button();
      this.button9 = new System.Windows.Forms.Button();
      this.btnPulseLed = new System.Windows.Forms.Button();
      this.btnToggleLed = new System.Windows.Forms.Button();
      this.btnClearLed = new System.Windows.Forms.Button();
      this.btnSetLed = new System.Windows.Forms.Button();
      this.btnGetLed = new System.Windows.Forms.Button();
      this.btnGetHardwareVersion = new System.Windows.Forms.Button();
      this.btnGetSoftwareVersion = new System.Windows.Forms.Button();
      this.btnGetProgramHeader = new System.Windows.Forms.Button();
      this.cbxAutomate = new System.Windows.Forms.CheckBox();
      this.pnlProtocol.SuspendLayout();
      this.mstMain.SuspendLayout();
      this.tbcMain.SuspendLayout();
      this.tbpLaserAreaScanner.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudLedTimes)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudLedDelay)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudLedIndex)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlProtocol
      // 
      this.pnlProtocol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlProtocol.Controls.Add(this.FUCSerialNumber);
      this.pnlProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlProtocol.Location = new System.Drawing.Point(0, 330);
      this.pnlProtocol.Name = "pnlProtocol";
      this.pnlProtocol.Size = new System.Drawing.Size(746, 281);
      this.pnlProtocol.TabIndex = 138;
      // 
      // FUCSerialNumber
      // 
      this.FUCSerialNumber.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCSerialNumber.Location = new System.Drawing.Point(0, 0);
      this.FUCSerialNumber.Name = "FUCSerialNumber";
      this.FUCSerialNumber.Size = new System.Drawing.Size(744, 64);
      this.FUCSerialNumber.TabIndex = 113;
      this.FUCSerialNumber.Visible = false;
      // 
      // splProtocol
      // 
      this.splProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splProtocol.Location = new System.Drawing.Point(0, 327);
      this.splProtocol.Name = "splProtocol";
      this.splProtocol.Size = new System.Drawing.Size(746, 3);
      this.splProtocol.TabIndex = 139;
      this.splProtocol.TabStop = false;
      // 
      // tmrStartup
      // 
      this.tmrStartup.Interval = 1;
      // 
      // DialogSaveInitdata
      // 
      this.DialogSaveInitdata.DefaultExt = "ini.xml";
      this.DialogSaveInitdata.FileName = "Initdata";
      this.DialogSaveInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogSaveInitdata.Title = "MDesign: Save Initdata";
      // 
      // DialogLoadInitdata
      // 
      this.DialogLoadInitdata.DefaultExt = "ini.xml";
      this.DialogLoadInitdata.FileName = "Initdata";
      this.DialogLoadInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogLoadInitdata.Title = "MDesign: Load Initdata";
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSystem,
            this.mitConfiguration,
            this.mitElevator,
            this.mitProtocol,
            this.mitHelp});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(746, 24);
      this.mstMain.TabIndex = 140;
      this.mstMain.Text = "System";
      // 
      // mitSystem
      // 
      this.mitSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSQuit});
      this.mitSystem.Name = "mitSystem";
      this.mitSystem.Size = new System.Drawing.Size(57, 20);
      this.mitSystem.Text = "&System";
      // 
      // mitSQuit
      // 
      this.mitSQuit.Name = "mitSQuit";
      this.mitSQuit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
      this.mitSQuit.Size = new System.Drawing.Size(140, 22);
      this.mitSQuit.Text = "&Quit";
      // 
      // mitConfiguration
      // 
      this.mitConfiguration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitCDefault,
            this.mitCResetToDefaultConfiguration,
            this.mitCSaveAutomaticAtProgramEnd,
            this.mitCStartup,
            this.mitCLoadStartupConfiguration,
            this.mitCSaveStartupConfiguration,
            this.mitCShowEditStartupConfiguration,
            this.toolStripMenuItem7,
            this.mitCLoadSelectableConfiguration,
            this.mitCSaveSelectableConfiguration,
            this.mitCShowEditSelectableConfiguration});
      this.mitConfiguration.Name = "mitConfiguration";
      this.mitConfiguration.Size = new System.Drawing.Size(93, 20);
      this.mitConfiguration.Text = "&Configuration";
      // 
      // mitCDefault
      // 
      this.mitCDefault.Enabled = false;
      this.mitCDefault.Name = "mitCDefault";
      this.mitCDefault.Size = new System.Drawing.Size(300, 22);
      this.mitCDefault.Text = "- Default -";
      // 
      // mitCResetToDefaultConfiguration
      // 
      this.mitCResetToDefaultConfiguration.Name = "mitCResetToDefaultConfiguration";
      this.mitCResetToDefaultConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
      this.mitCResetToDefaultConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCResetToDefaultConfiguration.Text = "Reset to Default-Configuration";
      // 
      // mitCSaveAutomaticAtProgramEnd
      // 
      this.mitCSaveAutomaticAtProgramEnd.Name = "mitCSaveAutomaticAtProgramEnd";
      this.mitCSaveAutomaticAtProgramEnd.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveAutomaticAtProgramEnd.Text = "Save automatic at program end";
      // 
      // mitCStartup
      // 
      this.mitCStartup.Enabled = false;
      this.mitCStartup.Name = "mitCStartup";
      this.mitCStartup.Size = new System.Drawing.Size(300, 22);
      this.mitCStartup.Text = "- Startup -";
      // 
      // mitCLoadStartupConfiguration
      // 
      this.mitCLoadStartupConfiguration.Name = "mitCLoadStartupConfiguration";
      this.mitCLoadStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
      this.mitCLoadStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadStartupConfiguration.Text = "Load Startup-Configuration";
      // 
      // mitCSaveStartupConfiguration
      // 
      this.mitCSaveStartupConfiguration.Name = "mitCSaveStartupConfiguration";
      this.mitCSaveStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
      this.mitCSaveStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveStartupConfiguration.Text = "Save Startup-Configuration";
      // 
      // mitCShowEditStartupConfiguration
      // 
      this.mitCShowEditStartupConfiguration.Name = "mitCShowEditStartupConfiguration";
      this.mitCShowEditStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditStartupConfiguration.Text = "Show / Edit Startup-Configuration";
      // 
      // toolStripMenuItem7
      // 
      this.toolStripMenuItem7.Enabled = false;
      this.toolStripMenuItem7.Name = "toolStripMenuItem7";
      this.toolStripMenuItem7.Size = new System.Drawing.Size(300, 22);
      this.toolStripMenuItem7.Text = "- Named -";
      // 
      // mitCLoadSelectableConfiguration
      // 
      this.mitCLoadSelectableConfiguration.Name = "mitCLoadSelectableConfiguration";
      this.mitCLoadSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadSelectableConfiguration.Text = "Load Selectable-Configuration ";
      // 
      // mitCSaveSelectableConfiguration
      // 
      this.mitCSaveSelectableConfiguration.Name = "mitCSaveSelectableConfiguration";
      this.mitCSaveSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveSelectableConfiguration.Text = "Save Selectable-Configuration";
      // 
      // mitCShowEditSelectableConfiguration
      // 
      this.mitCShowEditSelectableConfiguration.Name = "mitCShowEditSelectableConfiguration";
      this.mitCShowEditSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditSelectableConfiguration.Text = "Show / Edit Selectable-Configuration";
      // 
      // mitElevator
      // 
      this.mitElevator.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitStartSimulation});
      this.mitElevator.Name = "mitElevator";
      this.mitElevator.Size = new System.Drawing.Size(106, 20);
      this.mitElevator.Text = "Communication";
      // 
      // mitStartSimulation
      // 
      this.mitStartSimulation.Name = "mitStartSimulation";
      this.mitStartSimulation.Size = new System.Drawing.Size(98, 22);
      this.mitStartSimulation.Text = "Start";
      // 
      // mitProtocol
      // 
      this.mitProtocol.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitPEditParameter,
            this.mitPClearProtocol,
            this.mitPCopyToClipboard,
            this.diagnosticToolStripMenuItem,
            this.mitPLoadDiagnosticFile,
            this.mitPSaveDiagnosticFile,
            this.mitSendDiagnosticEmail});
      this.mitProtocol.Name = "mitProtocol";
      this.mitProtocol.Size = new System.Drawing.Size(64, 20);
      this.mitProtocol.Text = "&Protocol";
      // 
      // mitPEditParameter
      // 
      this.mitPEditParameter.Name = "mitPEditParameter";
      this.mitPEditParameter.Size = new System.Drawing.Size(171, 22);
      this.mitPEditParameter.Text = "Edit Parameter";
      // 
      // mitPClearProtocol
      // 
      this.mitPClearProtocol.Name = "mitPClearProtocol";
      this.mitPClearProtocol.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
      this.mitPClearProtocol.Size = new System.Drawing.Size(171, 22);
      this.mitPClearProtocol.Text = "Clear";
      // 
      // mitPCopyToClipboard
      // 
      this.mitPCopyToClipboard.Name = "mitPCopyToClipboard";
      this.mitPCopyToClipboard.Size = new System.Drawing.Size(171, 22);
      this.mitPCopyToClipboard.Text = "Copy to Clipboard";
      // 
      // diagnosticToolStripMenuItem
      // 
      this.diagnosticToolStripMenuItem.Enabled = false;
      this.diagnosticToolStripMenuItem.Name = "diagnosticToolStripMenuItem";
      this.diagnosticToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
      this.diagnosticToolStripMenuItem.Text = "- Diagnostic -";
      // 
      // mitPLoadDiagnosticFile
      // 
      this.mitPLoadDiagnosticFile.Name = "mitPLoadDiagnosticFile";
      this.mitPLoadDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPLoadDiagnosticFile.Text = "Load File";
      // 
      // mitPSaveDiagnosticFile
      // 
      this.mitPSaveDiagnosticFile.Name = "mitPSaveDiagnosticFile";
      this.mitPSaveDiagnosticFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
      this.mitPSaveDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPSaveDiagnosticFile.Text = "Save File";
      // 
      // mitSendDiagnosticEmail
      // 
      this.mitSendDiagnosticEmail.Name = "mitSendDiagnosticEmail";
      this.mitSendDiagnosticEmail.Size = new System.Drawing.Size(171, 22);
      this.mitSendDiagnosticEmail.Text = "Send Email";
      // 
      // mitHelp
      // 
      this.mitHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitHContents,
            this.mitHTopic,
            this.mitHSearch,
            this.mitHEnterApplicationProductKey,
            this.mitHEnterDeviceProductKey,
            this.mitHAbout});
      this.mitHelp.Name = "mitHelp";
      this.mitHelp.Size = new System.Drawing.Size(44, 20);
      this.mitHelp.Text = "Help";
      // 
      // mitHContents
      // 
      this.mitHContents.Name = "mitHContents";
      this.mitHContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.mitHContents.Size = new System.Drawing.Size(234, 22);
      this.mitHContents.Text = "Contents";
      // 
      // mitHTopic
      // 
      this.mitHTopic.Name = "mitHTopic";
      this.mitHTopic.Size = new System.Drawing.Size(234, 22);
      this.mitHTopic.Text = "Topic";
      // 
      // mitHSearch
      // 
      this.mitHSearch.Name = "mitHSearch";
      this.mitHSearch.Size = new System.Drawing.Size(234, 22);
      this.mitHSearch.Text = "Search";
      // 
      // mitHEnterApplicationProductKey
      // 
      this.mitHEnterApplicationProductKey.Name = "mitHEnterApplicationProductKey";
      this.mitHEnterApplicationProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterApplicationProductKey.Text = "Enter Application Product-Key";
      // 
      // mitHEnterDeviceProductKey
      // 
      this.mitHEnterDeviceProductKey.Name = "mitHEnterDeviceProductKey";
      this.mitHEnterDeviceProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterDeviceProductKey.Text = "Enter Device Product-Key";
      // 
      // mitHAbout
      // 
      this.mitHAbout.Name = "mitHAbout";
      this.mitHAbout.ShortcutKeys = System.Windows.Forms.Keys.F2;
      this.mitHAbout.Size = new System.Drawing.Size(234, 22);
      this.mitHAbout.Text = "About";
      // 
      // tbcMain
      // 
      this.tbcMain.Alignment = System.Windows.Forms.TabAlignment.Bottom;
      this.tbcMain.Controls.Add(this.tbpLaserAreaScanner);
      this.tbcMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcMain.Location = new System.Drawing.Point(0, 24);
      this.tbcMain.Name = "tbcMain";
      this.tbcMain.SelectedIndex = 0;
      this.tbcMain.Size = new System.Drawing.Size(746, 303);
      this.tbcMain.TabIndex = 141;
      // 
      // tbpLaserAreaScanner
      // 
      this.tbpLaserAreaScanner.Controls.Add(this.tbxMessages);
      this.tbpLaserAreaScanner.Controls.Add(this.tbxHardwareVersion);
      this.tbpLaserAreaScanner.Controls.Add(this.tbxSoftwareVersion);
      this.tbpLaserAreaScanner.Controls.Add(this.label2);
      this.tbpLaserAreaScanner.Controls.Add(this.nudLedTimes);
      this.tbpLaserAreaScanner.Controls.Add(this.label1);
      this.tbpLaserAreaScanner.Controls.Add(this.nudLedDelay);
      this.tbpLaserAreaScanner.Controls.Add(this.nudLedIndex);
      this.tbpLaserAreaScanner.Controls.Add(this.btnGetHelp);
      this.tbpLaserAreaScanner.Controls.Add(this.button16);
      this.tbpLaserAreaScanner.Controls.Add(this.button15);
      this.tbpLaserAreaScanner.Controls.Add(this.button14);
      this.tbpLaserAreaScanner.Controls.Add(this.button13);
      this.tbpLaserAreaScanner.Controls.Add(this.button12);
      this.tbpLaserAreaScanner.Controls.Add(this.button11);
      this.tbpLaserAreaScanner.Controls.Add(this.button10);
      this.tbpLaserAreaScanner.Controls.Add(this.button9);
      this.tbpLaserAreaScanner.Controls.Add(this.btnPulseLed);
      this.tbpLaserAreaScanner.Controls.Add(this.btnToggleLed);
      this.tbpLaserAreaScanner.Controls.Add(this.btnClearLed);
      this.tbpLaserAreaScanner.Controls.Add(this.btnSetLed);
      this.tbpLaserAreaScanner.Controls.Add(this.btnGetLed);
      this.tbpLaserAreaScanner.Controls.Add(this.btnGetHardwareVersion);
      this.tbpLaserAreaScanner.Controls.Add(this.btnGetSoftwareVersion);
      this.tbpLaserAreaScanner.Controls.Add(this.btnGetProgramHeader);
      this.tbpLaserAreaScanner.Location = new System.Drawing.Point(4, 4);
      this.tbpLaserAreaScanner.Margin = new System.Windows.Forms.Padding(0);
      this.tbpLaserAreaScanner.Name = "tbpLaserAreaScanner";
      this.tbpLaserAreaScanner.Size = new System.Drawing.Size(738, 277);
      this.tbpLaserAreaScanner.TabIndex = 5;
      this.tbpLaserAreaScanner.Text = "LaserAreaScanner";
      // 
      // tbxMessages
      // 
      this.tbxMessages.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxMessages.Location = new System.Drawing.Point(4, 62);
      this.tbxMessages.Multiline = true;
      this.tbxMessages.Name = "tbxMessages";
      this.tbxMessages.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.tbxMessages.Size = new System.Drawing.Size(504, 49);
      this.tbxMessages.TabIndex = 27;
      // 
      // tbxHardwareVersion
      // 
      this.tbxHardwareVersion.Enabled = false;
      this.tbxHardwareVersion.Location = new System.Drawing.Point(125, 6);
      this.tbxHardwareVersion.Name = "tbxHardwareVersion";
      this.tbxHardwareVersion.Size = new System.Drawing.Size(85, 20);
      this.tbxHardwareVersion.TabIndex = 24;
      this.tbxHardwareVersion.Text = "-";
      this.tbxHardwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tbxSoftwareVersion
      // 
      this.tbxSoftwareVersion.Enabled = false;
      this.tbxSoftwareVersion.Location = new System.Drawing.Point(337, 7);
      this.tbxSoftwareVersion.Name = "tbxSoftwareVersion";
      this.tbxSoftwareVersion.Size = new System.Drawing.Size(85, 20);
      this.tbxSoftwareVersion.TabIndex = 23;
      this.tbxSoftwareVersion.Text = "-";
      this.tbxSoftwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(458, 113);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(50, 13);
      this.label2.TabIndex = 22;
      this.label2.Text = "Times [1]";
      // 
      // nudLedTimes
      // 
      this.nudLedTimes.Location = new System.Drawing.Point(461, 129);
      this.nudLedTimes.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudLedTimes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudLedTimes.Name = "nudLedTimes";
      this.nudLedTimes.Size = new System.Drawing.Size(48, 20);
      this.nudLedTimes.TabIndex = 21;
      this.nudLedTimes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudLedTimes.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(402, 113);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(56, 13);
      this.label1.TabIndex = 20;
      this.label1.Text = "Delay [ms]";
      // 
      // nudLedDelay
      // 
      this.nudLedDelay.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudLedDelay.Location = new System.Drawing.Point(405, 129);
      this.nudLedDelay.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
      this.nudLedDelay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudLedDelay.Name = "nudLedDelay";
      this.nudLedDelay.Size = new System.Drawing.Size(48, 20);
      this.nudLedDelay.TabIndex = 18;
      this.nudLedDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudLedDelay.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      // 
      // nudLedIndex
      // 
      this.nudLedIndex.Location = new System.Drawing.Point(75, 129);
      this.nudLedIndex.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
      this.nudLedIndex.Name = "nudLedIndex";
      this.nudLedIndex.Size = new System.Drawing.Size(33, 20);
      this.nudLedIndex.TabIndex = 17;
      this.nudLedIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnGetHelp
      // 
      this.btnGetHelp.Location = new System.Drawing.Point(4, 33);
      this.btnGetHelp.Name = "btnGetHelp";
      this.btnGetHelp.Size = new System.Drawing.Size(117, 23);
      this.btnGetHelp.TabIndex = 16;
      this.btnGetHelp.Text = "GetHelp";
      this.btnGetHelp.UseVisualStyleBackColor = true;
      this.btnGetHelp.Click += new System.EventHandler(this.btnGetHelp_Click);
      // 
      // button16
      // 
      this.button16.Location = new System.Drawing.Point(165, 243);
      this.button16.Name = "button16";
      this.button16.Size = new System.Drawing.Size(80, 23);
      this.button16.TabIndex = 15;
      this.button16.Text = "AbortMotion";
      this.button16.UseVisualStyleBackColor = true;
      // 
      // button15
      // 
      this.button15.Location = new System.Drawing.Point(3, 243);
      this.button15.Name = "button15";
      this.button15.Size = new System.Drawing.Size(156, 23);
      this.button15.TabIndex = 14;
      this.button15.Text = "StartTriggerPulsesRepetitions";
      this.button15.UseVisualStyleBackColor = true;
      // 
      // button14
      // 
      this.button14.Location = new System.Drawing.Point(99, 214);
      this.button14.Name = "button14";
      this.button14.Size = new System.Drawing.Size(90, 23);
      this.button14.TabIndex = 13;
      this.button14.Text = "SetDelayPulse";
      this.button14.UseVisualStyleBackColor = true;
      // 
      // button13
      // 
      this.button13.Location = new System.Drawing.Point(3, 214);
      this.button13.Name = "button13";
      this.button13.Size = new System.Drawing.Size(90, 23);
      this.button13.TabIndex = 12;
      this.button13.Text = "SetDelayMotion";
      this.button13.UseVisualStyleBackColor = true;
      // 
      // button12
      // 
      this.button12.Location = new System.Drawing.Point(127, 185);
      this.button12.Name = "button12";
      this.button12.Size = new System.Drawing.Size(118, 23);
      this.button12.TabIndex = 11;
      this.button12.Text = "SetMotionParameterY";
      this.button12.UseVisualStyleBackColor = true;
      // 
      // button11
      // 
      this.button11.Location = new System.Drawing.Point(3, 185);
      this.button11.Name = "button11";
      this.button11.Size = new System.Drawing.Size(118, 23);
      this.button11.TabIndex = 10;
      this.button11.Text = "SetMotionParameterX";
      this.button11.UseVisualStyleBackColor = true;
      // 
      // button10
      // 
      this.button10.Location = new System.Drawing.Point(90, 156);
      this.button10.Name = "button10";
      this.button10.Size = new System.Drawing.Size(81, 23);
      this.button10.TabIndex = 9;
      this.button10.Text = "SetDACValue";
      this.button10.UseVisualStyleBackColor = true;
      // 
      // button9
      // 
      this.button9.Location = new System.Drawing.Point(3, 156);
      this.button9.Name = "button9";
      this.button9.Size = new System.Drawing.Size(81, 23);
      this.button9.TabIndex = 8;
      this.button9.Text = "GetDACValue";
      this.button9.UseVisualStyleBackColor = true;
      // 
      // btnPulseLed
      // 
      this.btnPulseLed.Location = new System.Drawing.Point(333, 127);
      this.btnPulseLed.Name = "btnPulseLed";
      this.btnPulseLed.Size = new System.Drawing.Size(66, 23);
      this.btnPulseLed.TabIndex = 7;
      this.btnPulseLed.Text = "PulseLed";
      this.btnPulseLed.UseVisualStyleBackColor = true;
      this.btnPulseLed.Click += new System.EventHandler(this.btnPulseLed_Click);
      // 
      // btnToggleLed
      // 
      this.btnToggleLed.Location = new System.Drawing.Point(260, 127);
      this.btnToggleLed.Name = "btnToggleLed";
      this.btnToggleLed.Size = new System.Drawing.Size(66, 23);
      this.btnToggleLed.TabIndex = 6;
      this.btnToggleLed.Text = "ToggleLed";
      this.btnToggleLed.UseVisualStyleBackColor = true;
      this.btnToggleLed.Click += new System.EventHandler(this.btnToggleLed_Click);
      // 
      // btnClearLed
      // 
      this.btnClearLed.Location = new System.Drawing.Point(187, 127);
      this.btnClearLed.Name = "btnClearLed";
      this.btnClearLed.Size = new System.Drawing.Size(66, 23);
      this.btnClearLed.TabIndex = 5;
      this.btnClearLed.Text = "ClearLed";
      this.btnClearLed.UseVisualStyleBackColor = true;
      this.btnClearLed.Click += new System.EventHandler(this.btnClearLed_Click);
      // 
      // btnSetLed
      // 
      this.btnSetLed.Location = new System.Drawing.Point(114, 127);
      this.btnSetLed.Name = "btnSetLed";
      this.btnSetLed.Size = new System.Drawing.Size(66, 23);
      this.btnSetLed.TabIndex = 4;
      this.btnSetLed.Text = "SetLed";
      this.btnSetLed.UseVisualStyleBackColor = true;
      this.btnSetLed.Click += new System.EventHandler(this.btnSetLed_Click);
      // 
      // btnGetLed
      // 
      this.btnGetLed.Location = new System.Drawing.Point(3, 127);
      this.btnGetLed.Name = "btnGetLed";
      this.btnGetLed.Size = new System.Drawing.Size(66, 23);
      this.btnGetLed.TabIndex = 3;
      this.btnGetLed.Text = "GetLed";
      this.btnGetLed.UseVisualStyleBackColor = true;
      this.btnGetLed.Click += new System.EventHandler(this.btnGetLed_Click);
      // 
      // btnGetHardwareVersion
      // 
      this.btnGetHardwareVersion.Location = new System.Drawing.Point(3, 4);
      this.btnGetHardwareVersion.Name = "btnGetHardwareVersion";
      this.btnGetHardwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetHardwareVersion.TabIndex = 2;
      this.btnGetHardwareVersion.Text = "GetHardwareVersion";
      this.btnGetHardwareVersion.UseVisualStyleBackColor = true;
      this.btnGetHardwareVersion.Click += new System.EventHandler(this.btnGetHardwareVersion_Click);
      // 
      // btnGetSoftwareVersion
      // 
      this.btnGetSoftwareVersion.Location = new System.Drawing.Point(215, 5);
      this.btnGetSoftwareVersion.Name = "btnGetSoftwareVersion";
      this.btnGetSoftwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetSoftwareVersion.TabIndex = 1;
      this.btnGetSoftwareVersion.Text = "GetSoftwareVersion";
      this.btnGetSoftwareVersion.UseVisualStyleBackColor = true;
      this.btnGetSoftwareVersion.Click += new System.EventHandler(this.btnGetSoftwareVersion_Click);
      // 
      // btnGetProgramHeader
      // 
      this.btnGetProgramHeader.Location = new System.Drawing.Point(125, 33);
      this.btnGetProgramHeader.Name = "btnGetProgramHeader";
      this.btnGetProgramHeader.Size = new System.Drawing.Size(117, 23);
      this.btnGetProgramHeader.TabIndex = 0;
      this.btnGetProgramHeader.Text = "GetProgramHeader";
      this.btnGetProgramHeader.UseVisualStyleBackColor = true;
      this.btnGetProgramHeader.Click += new System.EventHandler(this.btnGetProgramHeader_Click);
      // 
      // cbxAutomate
      // 
      this.cbxAutomate.AutoSize = true;
      this.cbxAutomate.Location = new System.Drawing.Point(376, 5);
      this.cbxAutomate.Name = "cbxAutomate";
      this.cbxAutomate.Size = new System.Drawing.Size(71, 17);
      this.cbxAutomate.TabIndex = 142;
      this.cbxAutomate.Text = "Automate";
      this.cbxAutomate.UseVisualStyleBackColor = true;
      this.cbxAutomate.CheckedChanged += new System.EventHandler(this.cbxAutomate_CheckedChanged);
      // 
      // FormClient
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(746, 611);
      this.Controls.Add(this.cbxAutomate);
      this.Controls.Add(this.tbcMain);
      this.Controls.Add(this.mstMain);
      this.Controls.Add(this.splProtocol);
      this.Controls.Add(this.pnlProtocol);
      this.Name = "FormClient";
      this.Text = "Form1";
      this.pnlProtocol.ResumeLayout(false);
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.tbcMain.ResumeLayout(false);
      this.tbpLaserAreaScanner.ResumeLayout(false);
      this.tbpLaserAreaScanner.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudLedTimes)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudLedDelay)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudLedIndex)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel pnlProtocol;
    private UCSerialNumber.CUCSerialNumber FUCSerialNumber;
    private System.Windows.Forms.Splitter splProtocol;
    private System.Windows.Forms.Timer tmrStartup;
    private System.Windows.Forms.HelpProvider FHelpProvider;
    private System.Windows.Forms.SaveFileDialog DialogSaveInitdata;
    private System.Windows.Forms.OpenFileDialog DialogLoadInitdata;
    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem mitSystem;
    private System.Windows.Forms.ToolStripMenuItem mitSQuit;
    private System.Windows.Forms.ToolStripMenuItem mitConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCDefault;
    private System.Windows.Forms.ToolStripMenuItem mitCResetToDefaultConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveAutomaticAtProgramEnd;
    private System.Windows.Forms.ToolStripMenuItem mitCStartup;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitElevator;
    private System.Windows.Forms.ToolStripMenuItem mitStartSimulation;
    private System.Windows.Forms.ToolStripMenuItem mitProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPEditParameter;
    private System.Windows.Forms.ToolStripMenuItem mitPClearProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPCopyToClipboard;
    private System.Windows.Forms.ToolStripMenuItem diagnosticToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitPLoadDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitPSaveDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitSendDiagnosticEmail;
    private System.Windows.Forms.ToolStripMenuItem mitHelp;
    private System.Windows.Forms.ToolStripMenuItem mitHContents;
    private System.Windows.Forms.ToolStripMenuItem mitHTopic;
    private System.Windows.Forms.ToolStripMenuItem mitHSearch;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterApplicationProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterDeviceProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHAbout;
    private System.Windows.Forms.TabControl tbcMain;
    private System.Windows.Forms.TabPage tbpLaserAreaScanner;
    private System.Windows.Forms.CheckBox cbxAutomate;
    private System.Windows.Forms.Button button16;
    private System.Windows.Forms.Button button15;
    private System.Windows.Forms.Button button14;
    private System.Windows.Forms.Button button13;
    private System.Windows.Forms.Button button12;
    private System.Windows.Forms.Button button11;
    private System.Windows.Forms.Button button10;
    private System.Windows.Forms.Button button9;
    private System.Windows.Forms.Button btnPulseLed;
    private System.Windows.Forms.Button btnToggleLed;
    private System.Windows.Forms.Button btnClearLed;
    private System.Windows.Forms.Button btnSetLed;
    private System.Windows.Forms.Button btnGetLed;
    private System.Windows.Forms.Button btnGetHardwareVersion;
    private System.Windows.Forms.Button btnGetSoftwareVersion;
    private System.Windows.Forms.Button btnGetProgramHeader;
    private System.Windows.Forms.Button btnGetHelp;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown nudLedTimes;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudLedDelay;
    private System.Windows.Forms.NumericUpDown nudLedIndex;
    private System.Windows.Forms.TextBox tbxHardwareVersion;
    private System.Windows.Forms.TextBox tbxSoftwareVersion;
    private System.Windows.Forms.TextBox tbxMessages;
    
  }
}

