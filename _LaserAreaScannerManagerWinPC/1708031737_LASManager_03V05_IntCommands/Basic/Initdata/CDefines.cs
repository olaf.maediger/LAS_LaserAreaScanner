using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Threading;

namespace Initdata
{ //
  //------------------------------------------------------
  //  Section - Type
  //------------------------------------------------------
  //	
  public delegate Boolean DOnStartupTick(Int32 startupstate);
  //
  //------------------------------------------------------
  //  Section - MAIN - CDefines
  //------------------------------------------------------
  //	 
	static class CDefines
	{
    public const String INIT_DECIMALSEPARATOR = ".";
    //
    static public void Init()
    { // Fix DecimalSeparator to '.':      
      String CN = Thread.CurrentThread.CurrentCulture.Name;
      CultureInfo CC = new CultureInfo(CN, false);
      CC.NumberFormat.NumberDecimalSeparator = INIT_DECIMALSEPARATOR;
      Thread.CurrentThread.CurrentCulture = CC;
    }
    //
    //------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------
    //	 
		static public Boolean IsEven(Int32 newvalue)
		{
			return (0 == (0x00000001 & newvalue));
		}
		static public Boolean IsOdd(Int32 newvalue)
		{
			return (1 == (0x00000001 & newvalue));
		}

	}
}
