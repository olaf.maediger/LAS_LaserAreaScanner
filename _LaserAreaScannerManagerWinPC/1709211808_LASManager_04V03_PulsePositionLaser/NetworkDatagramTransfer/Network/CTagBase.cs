﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Network
{
  public class CByteList : List<Byte>
  {
    public Byte[] GetByteVector()
    {
      Byte[] Result = null;
      if (0 < Count)
      {
        int Size = this.Count;
        Result = new Byte[Size];
        for (int BI = 0; BI < Size; BI++)
        {
          Result[BI] = this[BI];
        }
      }
      return Result;
    }
  }

  public abstract class CTagBase
  { //
    //----------------------------------------------------------------------------------------------------------------
    //  Syntax - Text
    //----------------------------------------------------------------------------------------------------------------
    //  CTagTextList : <taglist> := <tag>[nt] // nt in [0 .. N] 
    //  CTagText     : <tag> := <header><value>[nv] // nv in [0 .. N] 
    //  String       : <header> := <character>[nc] // nc in [0 .. N] 
    //  String       : <value> := <character>[nc] // nc in [0 .. N] 
    //
    //----------------------------------------------------------------------------------------------------------------
    //  Syntax - Binary
    //----------------------------------------------------------------------------------------------------------------
    //  <tagbinary> := <tagscalar> | <tagvector> | <tagmatrix>
    //  <tagscalar> := <type><value>
    //  <tagvector> := <type><count><value>..<value>[count]
    //  <tagmatrix> := <type><countrow><countcol><value>..<value>[1..countrow][1..countcol]
    //  <type> := <Byte>, <UInteger16>, <UInteger32>, <Integer16>, <Integer32>, <Double>, <Character>, <String>
    //            <Byte[]> .. <String[]>, <Byte[][]> .. <String[][]>
    //  <count> := <UInteger32>
    //  <value> := <0x00..0xFF>[nv := sizof(type)]
    //  <Byte> := <0x11><0x00..0xFF>
    //  <Byte[]> := <0x31><nc in UInt32><0x00..0xFF>..<0x00..0xFF>[nc]
    //  <Byte[][]> := <0x51><nr in UInt32><nc in UInt32><0x00..0xFF>..<0x00..0xFF>[nr, nc]
    //  <Double> := <0x16><nc in UInt32><value in Double>
    //  <Double[]> := <0x36><nc in UInt32><value in Double>
    //  <Double[][]> := <0x56><nr in UInt32><nc in UInt32><value in Double>[nr, nc]
    //  <String> := <0x18><nv in UInt32><character>[nc]
    //  <String[]> := <0x38><nc in UInt32><<nv in UInt32><character>>..<<nv in UInt32><character>>[nr, nc]
    //  <String[][]> := <0x58><nr in UInt32><nc in UInt32><<nv in UInt32><character>>..<<nv in UInt32><character>>[nr, nc]
    //  <Character> := <0x17><character>
    //  <Character> := <0x37><n in UInt32><character>[n]
    //  <Character> := <0x57><nr in UInt32><nc in UInt32><character>[nr, nc]
    // 
    //  <character> := 0x00..0xFF
    //
    //--------------------------------------------------------------------
    //  Segment - Common
    //--------------------------------------------------------------------
    //
    // MagicWord
    public const Byte TYPE_MAGIC_WORD = 0x01; 
    // Scalar
    public const Byte TYPE_SCALAR_BYTE = 0x11;
    public const Byte TYPE_SCALAR_CHAR = 0x12;
    public const Byte TYPE_SCALAR_UINT16 = 0x13;
    public const Byte TYPE_SCALAR_UINT32 = 0x14;
    public const Byte TYPE_SCALAR_INT16 = 0x15;
    public const Byte TYPE_SCALAR_INT32 = 0x16;
    public const Byte TYPE_SCALAR_DOUBLE = 0x17;
    public const Byte TYPE_SCALAR_STRING = 0x18;
    public const Byte TYPE_SCALAR_DATETIME = 0x19;
    public const Byte TYPE_SCALAR_GUID = 0x1A;
    // Vector
    public const Byte TYPE_VECTOR_BYTE = 0x31;
    public const Byte TYPE_VECTOR_CHAR = 0x32;
    public const Byte TYPE_VECTOR_UINT16 = 0x33;
    public const Byte TYPE_VECTOR_UINT32 = 0x34;
    public const Byte TYPE_VECTOR_INT16 = 0x35;
    public const Byte TYPE_VECTOR_INT32 = 0x36;
    public const Byte TYPE_VECTOR_DOUBLE = 0x37;
    public const Byte TYPE_VECTOR_STRING = 0x38;
    // Matrix
    public const Byte TYPE_MATRIX_BYTE = 0x51;
    public const Byte TYPE_MATRIX_CHAR = 0x52;
    public const Byte TYPE_MATRIX_UINT16 = 0x53;
    public const Byte TYPE_MATRIX_UINT32 = 0x54;
    public const Byte TYPE_MATRIX_INT16 = 0x55;
    public const Byte TYPE_MATRIX_INT32 = 0x56;
    public const Byte TYPE_MATRIX_DOUBLE = 0x57;
    public const Byte TYPE_MATRIX_STRING = 0x58;
    //
    //
    public const String STYPE_MAGIC_WORD = "0x01";
    //
    public const String STYPE_SCALAR_BYTE = "0x11";
    public const String STYPE_SCALAR_CHAR = "0x12";
    public const String STYPE_SCALAR_UINT16 = "0x13";
    public const String STYPE_SCALAR_UINT32 = "0x14";
    public const String STYPE_SCALAR_INT16 = "0x15";
    public const String STYPE_SCALAR_INT32 = "0x16";
    public const String STYPE_SCALAR_DOUBLE = "0x17";
    public const String STYPE_SCALAR_STRING = "0x18";
    public const String STYPE_SCALAR_DATETIME = "0x19";
    public const String STYPE_SCALAR_GUID = "0x1A";
    // Vector
    public const String STYPE_VECTOR_BYTE = "0x31";
    public const String STYPE_VECTOR_CHAR = "0x32";
    public const String STYPE_VECTOR_UINT16 = "0x33";
    public const String STYPE_VECTOR_UINT32 = "0x34";
    public const String STYPE_VECTOR_INT16 = "0x35";
    public const String STYPE_VECTOR_INT32 = "0x36";
    public const String STYPE_VECTOR_DOUBLE = "0x37";
    public const String STYPE_VECTOR_STRING = "0x38";
    // Matrix
    public const String STYPE_MATRIX_BYTE = "0x51";
    public const String STYPE_MATRIX_CHAR = "0x52";
    public const String STYPE_MATRIX_UINT16 = "0x53";
    public const String STYPE_MATRIX_UINT32 = "0x54";
    public const String STYPE_MATRIX_INT16 = "0x55";
    public const String STYPE_MATRIX_INT32 = "0x56";
    public const String STYPE_MATRIX_DOUBLE = "0x57";
    public const String STYPE_MATRIX_STRING = "0x58";

    protected Byte FType;
    public Byte Type
    {
      get { return FType; } 
      set { FType = value; }
    }

    public static String GetSType(Byte type)
    {
      switch (type)
      { // Scalar
        case TYPE_SCALAR_BYTE:
          return STYPE_SCALAR_BYTE;
        case TYPE_SCALAR_CHAR:
          return STYPE_SCALAR_CHAR;
        case TYPE_SCALAR_UINT16:
          return STYPE_SCALAR_UINT16;
        case TYPE_SCALAR_UINT32:
          return STYPE_SCALAR_UINT32;
        case TYPE_SCALAR_INT16:
          return STYPE_SCALAR_INT16;
        case TYPE_SCALAR_INT32:
          return STYPE_SCALAR_INT32;
        case TYPE_SCALAR_DOUBLE:
          return STYPE_SCALAR_DOUBLE;
        case TYPE_SCALAR_STRING:
          return STYPE_SCALAR_STRING;
        case TYPE_SCALAR_DATETIME:
          return STYPE_SCALAR_DATETIME;
        case TYPE_SCALAR_GUID:
          return STYPE_SCALAR_GUID;
        // Vector
        case TYPE_VECTOR_BYTE:
          return STYPE_VECTOR_BYTE;
        case TYPE_VECTOR_CHAR:
          return STYPE_VECTOR_CHAR;
        case TYPE_VECTOR_UINT16:
          return STYPE_VECTOR_UINT16;
        case TYPE_VECTOR_UINT32:
          return STYPE_VECTOR_UINT32;
        case TYPE_VECTOR_INT16:
          return STYPE_VECTOR_INT16;
        case TYPE_VECTOR_INT32:
          return STYPE_VECTOR_INT32;
        case TYPE_VECTOR_DOUBLE:
          return STYPE_VECTOR_DOUBLE;
        case TYPE_VECTOR_STRING:
          return STYPE_VECTOR_STRING;
        // Matrix
        case TYPE_MATRIX_BYTE:
          return STYPE_MATRIX_BYTE;
        case TYPE_MATRIX_CHAR:
          return STYPE_MATRIX_CHAR;
        case TYPE_MATRIX_UINT16:
          return STYPE_MATRIX_UINT16;
        case TYPE_MATRIX_UINT32:
          return STYPE_MATRIX_UINT32;
        case TYPE_MATRIX_INT16:
          return STYPE_MATRIX_INT16;
        case TYPE_MATRIX_INT32:
          return STYPE_MATRIX_INT32;
        case TYPE_MATRIX_DOUBLE:
          return STYPE_MATRIX_DOUBLE;
        case TYPE_MATRIX_STRING:
          return STYPE_MATRIX_STRING;
      }
      return"???";
    }
    public String SType
    {
      get { return GetSType(FType); }
    }

    public abstract String GetGroup();
    public abstract String GetName();
    public abstract String GetValue();

    public String GetText()
    {
      return String.Format("0x{0:00}{1}", FType, GetValue());
    }
  }
  //
  //####################################################################
  //  Segment - Basic - Scalar/Vector/Matrix
  //####################################################################
  //
  //--------------------------------------------------------------------
  //  Segment - Basic - Scalar
  //--------------------------------------------------------------------
  //
  public abstract class CTTScalar : CTagText
  {
    public override String GetGroup()
    {
      return "TScalar";
    }

    public override String GetValue()
    {
      String Result = "";
      if (null == FValues)
      {
        Result = "[]";
      }
      else
      {
        Result = String.Format("[{0}]", FValues[0]);
      }
      return Result;
    }
  };
  //
  //--------------------------------------------------------------------
  //  Segment - Basic - Vector
  //--------------------------------------------------------------------
  //
  public abstract class CTTVector : CTagText
  {
    protected Int32 FSize0;

    public override String GetGroup()
    {
      return "TVector";
    }

    public override String GetValue()
    {
      String Result = String.Format("[{0}]", FSize0);
      if (null == FValues)
      {
        Result = "[]";
      }
      else
      {
        Int32 L = FSize0;
        for (Int32 VI = 0; VI < L; VI++)
        {
          Result += String.Format("[{0}]", FValues[VI]);
        }
      }
      return Result;
    }
  };
  //
  //--------------------------------------------------------------------
  //  Segment - Basic - Matrix
  //--------------------------------------------------------------------
  //
  public abstract class CTTMatrix : CTagText
  {
    protected Int32 FSize0, FSize1;

    public override String GetGroup()
    {
      return "TMatrix";
    }

    public override String GetValue()
    {
      String Result = String.Format("[{0}][{1}]", FSize0, FSize1);
      if (null == FValues)
      {
        Result = "[]";
      }
      else
      {
        Int32 L = FSize0 * FSize1;
        for (Int32 VI = 0; VI < L; VI++)
        {
          Result += String.Format("[{0}]", FValues[VI]);
        }
      }
      return Result;
    }
  };

}


    
    //public virtual String GetValue()
    //{ return ""; }
    //public virtual String GetValue(Int32 index0)
    //{ return ""; }
    //public virtual String GetValue(Int32 index0, Int32 index1)
    //{ return ""; }

    //public virtual String GetValue()
    //{
    //  String Result = "";
    //  Int32 L = FValues.Length;
    //  for (Int32 VI = 0; VI < L; VI++)
    //  {
    //    Result += String.Format("[{0}]", FValues[VI]);
    //  }
    //  return Result;
    //}


