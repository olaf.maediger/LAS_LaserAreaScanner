﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace UdpTextTransfer
{
  public struct RUdpTextDatagramHeader
  {
    public Char[] ProtocolType;     // MagicWord in [UDPTXT] // [UDPTXT, UDPBIN, TCPTXT, TCPBIN]
    public DateTime TimeStamp;      // DateTime when created this Datagram
    public Byte[] SourceIpAddress;  // Source of Datagram (Transmitter)
    public Guid SourceID;           // Guid of Source-Program (no change on Runtime)
    public String SourceType;
    public String SourceName;
    public Byte[] TargetIpAddress;  // Target of Datagram
    public Guid TargetID;           // Guid of Target-Program (no change on Runtime)
    public String TargetType;
    public String TargetName;
    public Guid DatagramID;         // Multiple Blocks of ONE Datagram have the same DatagramID
    public Int32 BlockIndex;        // BlockIndex / BlockCount
    public Int32 BlockCount;        // TotalCount of Blocks for ONE Datagram
    public String DatagramType;
    public String DatagramKey;
    //
    public RUdpTextDatagramHeader(Char[] protocoltype,
                                  DateTime timestamp,
                                  Byte[] sourceipaddress,
                                  Guid sourceid,
                                  String sourcetype,
                                  String sourcename,
                                  Byte[] targetipaddress,
                                  Guid targetid,
                                  String targettype,
                                  String targetname,
                                  Guid datagramid,
                                  Int32 blockindex,
                                  Int32 blockcount,
                                  String datagramtype,
                                  String datagramkey)
    {
      ProtocolType = protocoltype;
      TimeStamp = timestamp;
      SourceIpAddress = sourceipaddress;
      SourceID = sourceid;
      SourceType = sourcetype;
      SourceName = sourcename;
      TargetIpAddress = targetipaddress;
      TargetID = targetid;
      TargetType = targettype;
      TargetName = targetname;
      DatagramID = datagramid;
      BlockIndex = blockindex;
      BlockCount = blockcount;
      DatagramType = datagramtype;
      DatagramKey = datagramkey;
    }

  }
}

