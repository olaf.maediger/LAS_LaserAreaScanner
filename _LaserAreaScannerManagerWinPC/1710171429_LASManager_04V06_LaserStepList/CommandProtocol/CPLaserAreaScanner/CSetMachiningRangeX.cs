﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using CommandProtocol;
//
namespace CPLaserAreaScanner
{
  public class CSetMachiningRangeX : CCommand
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const String HEADER = "SRX";
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private Int32 FXMin, FXMax, FDX;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CSetMachiningRangeX(Int32 xmin, Int32 xmax, Int32 dx)
      : base(HEADER)
    {
      FXMin = xmin;
      FXMax = xmax;
      FDX = dx;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public override string GetCommandHeader()
    {
      return FHeader;
    }

    public override string GetCommandText()
    {
      return String.Format("{0} {1} {2} {3}", FHeader, FXMin, FXMax, FDX);
    }


  }
}