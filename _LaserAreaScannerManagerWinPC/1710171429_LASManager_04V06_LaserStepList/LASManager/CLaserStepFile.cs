﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LASManager
{
  public class CLaserStep
  {
    private Int32 FPositionX, FPositionY, FPulsePeriod, FPulseCount;

    public CLaserStep(Int32 positionx, Int32 positiony,
                      Int32 pulseperiod, Int32 pulsecount)
    {
      FPositionX = positionx;
      FPositionY = positiony;
      FPulsePeriod = pulseperiod;
      FPulseCount = pulsecount;
    }

    public Int32 PositionX
    {
      get { return FPositionX; }
    }
    public Int32 PositionY
    {
      get { return FPositionY; }
    }
    public Int32 PulsePeriod
    {
      get { return FPulsePeriod; }
    }
    public Int32 PulseCount
    {
      get { return FPulseCount; }
    }
  }

  public class CLaserStepList : List<CLaserStep>
  {
  }

  public class CLaserStepFile
  {


    private Boolean CompileLine(String line,
                                out Int32 positionx, out Int32 positiony,
                                out Int32 pulseperiod, out Int32 pulsecount)
    {
      Char[] Separators = {' ', '\t' };
      String[] Tokens = line.Split(Separators);
      if (4 == Tokens.Length)
      {
        positionx = Int32.Parse(Tokens[0]);
        positiony = Int32.Parse(Tokens[1]);
        pulseperiod = Int32.Parse(Tokens[2]);
        pulsecount = Int32.Parse(Tokens[3]);
        return true;
      }
      positionx = 0;
      positiony = 0;
      pulseperiod = 0;
      pulsecount = 0;
      return false;
    }

    public Boolean Compile(String[] lines,
                           out CLaserStepList lasersteplist)
    {
      lasersteplist = null;
      try
      {
        Boolean Result = true;
        Int32 PositionX, PositionY, PulsePeriod, PulseCount;
        lasersteplist = new CLaserStepList();
        Int32 LC = lines.Length;
        for (Int32 LI = 0; LI < LC; LI++)
        {
          String Line = lines[LI];
          if (1 <= Line.Length)
          {
            if (';' != Line[0])
            {
              Result &= CompileLine(Line, out PositionX, out PositionY, out PulsePeriod, out PulseCount);
              if (Result)
              {
                lasersteplist.Add(new CLaserStep(PositionX, PositionY, PulsePeriod, PulseCount));
              }
            }
          }
        }
        return Result;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
