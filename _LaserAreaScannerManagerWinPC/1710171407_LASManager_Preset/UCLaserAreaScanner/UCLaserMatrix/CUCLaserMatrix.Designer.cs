﻿namespace UCLaserMatrix
{
  partial class CUCLaserMatrix
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pbxMatrix = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix)).BeginInit();
      this.SuspendLayout();
      // 
      // pbxMatrix
      // 
      this.pbxMatrix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxMatrix.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxMatrix.Location = new System.Drawing.Point(0, 0);
      this.pbxMatrix.Name = "pbxMatrix";
      this.pbxMatrix.Size = new System.Drawing.Size(314, 297);
      this.pbxMatrix.TabIndex = 0;
      this.pbxMatrix.TabStop = false;
      // 
      // CUCLaserMatrix
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pbxMatrix);
      this.Name = "CUCLaserMatrix";
      this.Size = new System.Drawing.Size(314, 297);
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox pbxMatrix;
  }
}
