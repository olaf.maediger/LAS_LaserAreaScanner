﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Network
{
  public class CReceivedDatagramCheck
  {
    //private Guid FDatagramID; // have all the same DatagramID!!!
    private Boolean[] FValues;

    public CReceivedDatagramCheck(Int32 blockcount)//Guid datagramid, Int32 datagramcount)
    {
      //FDatagramID = datagramid;
      FValues = new Boolean[blockcount];
      Int32 Length = FValues.Length;
      for (Int32 VI = 0; VI < Length; VI++)
      {
        FValues[VI] = false;
      }
    }

    //public Guid DatagramID
    //{
    //  get { return FDatagramID; }
    //}

    public Int32 GetCount()
    {
      return FValues.Length;
    }

    public void SetCheck(Int32 datagramindex)
    {
      Int32 Length = FValues.Length;
      if ((0 <= datagramindex) && (datagramindex < Length))
      {
        FValues[datagramindex] = true;
      }
    }

    public Boolean CheckAll()
    {
      Boolean Result = true;
      Int32 Length = FValues.Length;
      for (Int32 VI = 0; VI < Length; VI++)
      {
        Result &= (true == FValues[VI]);
      }
      return Result;
    }
  }

  //public class CReceivedDatagramCheckList : List<CReceivedDatagramCheck>
  //{
  //  public Int32 GetDatagramCount(Guid datagramid)
  //  {
  //    Int32 Result = 0;
  //    foreach (CReceivedDatagramCheck RDC in this)
  //    {
  //      if (datagramid == RDC.DatagramID)
  //      {
  //        Result = RDC.L;
  //      }
  //    }
  //    return Result;
  //  }

  //  public void SetReceivedDatagramCheck(Guid datagramid, Int32 datagramindex, Int32 datagramcount)
  //  {
  //    foreach (CReceivedDatagramCheck ReceivedDatagramCheck in this)
  //    {
  //      if (datagramid == ReceivedDatagramCheck.DatagramID)
  //      { // DatagramID exists
  //        ReceivedDatagramCheck.SetCheck(datagramindex);
  //        return;
  //      }
  //    }
  //    // DatagramID dont exist until now -> create ReceivedDatagramCheck
  //    CReceivedDatagramCheck RDC = new CReceivedDatagramCheck(datagramid, datagramcount);
  //    RDC.SetCheck(datagramindex);
  //    this.Add(RDC);
  //  }

  //  public Boolean CheckReceivedDatagramAll()
  //  {
  //    foreach (CReceivedDatagramCheck ReceivedDatagramCheck in this)
  //    {
  //      if (datagramid == ReceivedDatagramCheck.DatagramID)
  //      {
  //        return ReceivedDatagramCheck.CheckAll();
  //      }
  //    }
  //    return false;
  //  }

  //  public void RemoveReceivedDatagramCheck(Guid datagramid)
  //  {
  //    foreach (CReceivedDatagramCheck ReceivedDatagramCheck in this)
  //    {
  //      if (datagramid == ReceivedDatagramCheck.DatagramID)
  //      { // DatagramID exists
  //        this.Remove(ReceivedDatagramCheck);
  //      }
  //    }
  //  }

 // }


}
