﻿namespace LASManager
{
  partial class FormClient
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlProtocol = new System.Windows.Forms.Panel();
      this.FUCSerialNumber = new UCSerialNumber.CUCSerialNumber();
      this.splProtocol = new System.Windows.Forms.Splitter();
      this.tmrStartup = new System.Windows.Forms.Timer(this.components);
      this.FHelpProvider = new System.Windows.Forms.HelpProvider();
      this.DialogSaveInitdata = new System.Windows.Forms.SaveFileDialog();
      this.DialogLoadInitdata = new System.Windows.Forms.OpenFileDialog();
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.mitSystem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.mitConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCDefault = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCResetToDefaultConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveAutomaticAtProgramEnd = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCStartup = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitElevator = new System.Windows.Forms.ToolStripMenuItem();
      this.mitStartSimulation = new System.Windows.Forms.ToolStripMenuItem();
      this.mitProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPEditParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPClearProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
      this.diagnosticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPLoadDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPSaveDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSendDiagnosticEmail = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHelp = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHContents = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHTopic = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHSearch = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterApplicationProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterDeviceProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.tbcMain = new System.Windows.Forms.TabControl();
      this.tbpPulses = new System.Windows.Forms.TabPage();
      this.btnAbortVariableLaser = new System.Windows.Forms.Button();
      this.btnPulseVariableLaser = new System.Windows.Forms.Button();
      this.btnEnterVariableLaser = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.lblStatelaserAreaScanner = new System.Windows.Forms.Label();
      this.label19 = new System.Windows.Forms.Label();
      this.btnPulsePositionLaser = new System.Windows.Forms.Button();
      this.label17 = new System.Windows.Forms.Label();
      this.nudPositionY = new System.Windows.Forms.NumericUpDown();
      this.btnSetPositionY = new System.Windows.Forms.Button();
      this.btnGetPositionY = new System.Windows.Forms.Button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.pnlLedLaserState = new System.Windows.Forms.Panel();
      this.label18 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.nudDelayPulse = new System.Windows.Forms.NumericUpDown();
      this.label7 = new System.Windows.Forms.Label();
      this.nudDelayMotion = new System.Windows.Forms.NumericUpDown();
      this.label6 = new System.Windows.Forms.Label();
      this.nudDelta = new System.Windows.Forms.NumericUpDown();
      this.label5 = new System.Windows.Forms.Label();
      this.nudMax = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.nudMin = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.nudPositionX = new System.Windows.Forms.NumericUpDown();
      this.tbxMessages = new System.Windows.Forms.TextBox();
      this.tbxHardwareVersion = new System.Windows.Forms.TextBox();
      this.tbxSoftwareVersion = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.nudPulseCount = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.nudPulsePeriod = new System.Windows.Forms.NumericUpDown();
      this.btnGetHelp = new System.Windows.Forms.Button();
      this.btnAbortMatrixLaser = new System.Windows.Forms.Button();
      this.btnPulseMatrixLaser = new System.Windows.Forms.Button();
      this.btnSetDelayPulse = new System.Windows.Forms.Button();
      this.btnSetDelayMotion = new System.Windows.Forms.Button();
      this.btnSetMotionParameterY = new System.Windows.Forms.Button();
      this.btnSetMotionParameterX = new System.Windows.Forms.Button();
      this.btnSetPositionX = new System.Windows.Forms.Button();
      this.btnGetPositionX = new System.Windows.Forms.Button();
      this.btnPulseLedLaser = new System.Windows.Forms.Button();
      this.btnSetLedLaserOff = new System.Windows.Forms.Button();
      this.btnSetLedLaserOn = new System.Windows.Forms.Button();
      this.btnGetLedLaser = new System.Windows.Forms.Button();
      this.btnGetHardwareVersion = new System.Windows.Forms.Button();
      this.btnGetSoftwareVersion = new System.Windows.Forms.Button();
      this.btnGetProgramHeader = new System.Windows.Forms.Button();
      this.cbxAutomate = new System.Windows.Forms.CheckBox();
      this.tmrAutomation = new System.Windows.Forms.Timer(this.components);
      this.richTextBox1 = new System.Windows.Forms.RichTextBox();
      this.FUCEditorLaserStepFile = new UCTextEditor.CUCEditTextEditor();
      this.tmrLaserStepList = new System.Windows.Forms.Timer(this.components);
      this.pnlProtocol.SuspendLayout();
      this.mstMain.SuspendLayout();
      this.tbcMain.SuspendLayout();
      this.tbpPulses.SuspendLayout();
      this.panel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionY)).BeginInit();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayPulse)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayMotion)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelta)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMax)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMin)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionX)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriod)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlProtocol
      // 
      this.pnlProtocol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlProtocol.Controls.Add(this.FUCSerialNumber);
      this.pnlProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlProtocol.Location = new System.Drawing.Point(0, 756);
      this.pnlProtocol.Name = "pnlProtocol";
      this.pnlProtocol.Size = new System.Drawing.Size(735, 165);
      this.pnlProtocol.TabIndex = 138;
      // 
      // FUCSerialNumber
      // 
      this.FUCSerialNumber.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCSerialNumber.Location = new System.Drawing.Point(0, 0);
      this.FUCSerialNumber.Name = "FUCSerialNumber";
      this.FUCSerialNumber.Size = new System.Drawing.Size(733, 64);
      this.FUCSerialNumber.TabIndex = 113;
      this.FUCSerialNumber.Visible = false;
      // 
      // splProtocol
      // 
      this.splProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splProtocol.Location = new System.Drawing.Point(0, 753);
      this.splProtocol.Name = "splProtocol";
      this.splProtocol.Size = new System.Drawing.Size(735, 3);
      this.splProtocol.TabIndex = 139;
      this.splProtocol.TabStop = false;
      // 
      // tmrStartup
      // 
      this.tmrStartup.Interval = 1;
      // 
      // DialogSaveInitdata
      // 
      this.DialogSaveInitdata.DefaultExt = "ini.xml";
      this.DialogSaveInitdata.FileName = "Initdata";
      this.DialogSaveInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogSaveInitdata.Title = "MDesign: Save Initdata";
      // 
      // DialogLoadInitdata
      // 
      this.DialogLoadInitdata.DefaultExt = "ini.xml";
      this.DialogLoadInitdata.FileName = "Initdata";
      this.DialogLoadInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogLoadInitdata.Title = "MDesign: Load Initdata";
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSystem,
            this.mitConfiguration,
            this.mitElevator,
            this.mitProtocol,
            this.mitHelp});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(735, 24);
      this.mstMain.TabIndex = 140;
      this.mstMain.Text = "System";
      // 
      // mitSystem
      // 
      this.mitSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSQuit});
      this.mitSystem.Name = "mitSystem";
      this.mitSystem.Size = new System.Drawing.Size(57, 20);
      this.mitSystem.Text = "&System";
      // 
      // mitSQuit
      // 
      this.mitSQuit.Name = "mitSQuit";
      this.mitSQuit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
      this.mitSQuit.Size = new System.Drawing.Size(140, 22);
      this.mitSQuit.Text = "&Quit";
      // 
      // mitConfiguration
      // 
      this.mitConfiguration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitCDefault,
            this.mitCResetToDefaultConfiguration,
            this.mitCSaveAutomaticAtProgramEnd,
            this.mitCStartup,
            this.mitCLoadStartupConfiguration,
            this.mitCSaveStartupConfiguration,
            this.mitCShowEditStartupConfiguration,
            this.toolStripMenuItem7,
            this.mitCLoadSelectableConfiguration,
            this.mitCSaveSelectableConfiguration,
            this.mitCShowEditSelectableConfiguration});
      this.mitConfiguration.Name = "mitConfiguration";
      this.mitConfiguration.Size = new System.Drawing.Size(93, 20);
      this.mitConfiguration.Text = "&Configuration";
      // 
      // mitCDefault
      // 
      this.mitCDefault.Enabled = false;
      this.mitCDefault.Name = "mitCDefault";
      this.mitCDefault.Size = new System.Drawing.Size(300, 22);
      this.mitCDefault.Text = "- Default -";
      // 
      // mitCResetToDefaultConfiguration
      // 
      this.mitCResetToDefaultConfiguration.Name = "mitCResetToDefaultConfiguration";
      this.mitCResetToDefaultConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
      this.mitCResetToDefaultConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCResetToDefaultConfiguration.Text = "Reset to Default-Configuration";
      // 
      // mitCSaveAutomaticAtProgramEnd
      // 
      this.mitCSaveAutomaticAtProgramEnd.Name = "mitCSaveAutomaticAtProgramEnd";
      this.mitCSaveAutomaticAtProgramEnd.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveAutomaticAtProgramEnd.Text = "Save automatic at program end";
      // 
      // mitCStartup
      // 
      this.mitCStartup.Enabled = false;
      this.mitCStartup.Name = "mitCStartup";
      this.mitCStartup.Size = new System.Drawing.Size(300, 22);
      this.mitCStartup.Text = "- Startup -";
      // 
      // mitCLoadStartupConfiguration
      // 
      this.mitCLoadStartupConfiguration.Name = "mitCLoadStartupConfiguration";
      this.mitCLoadStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
      this.mitCLoadStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadStartupConfiguration.Text = "Load Startup-Configuration";
      // 
      // mitCSaveStartupConfiguration
      // 
      this.mitCSaveStartupConfiguration.Name = "mitCSaveStartupConfiguration";
      this.mitCSaveStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
      this.mitCSaveStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveStartupConfiguration.Text = "Save Startup-Configuration";
      // 
      // mitCShowEditStartupConfiguration
      // 
      this.mitCShowEditStartupConfiguration.Name = "mitCShowEditStartupConfiguration";
      this.mitCShowEditStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditStartupConfiguration.Text = "Show / Edit Startup-Configuration";
      // 
      // toolStripMenuItem7
      // 
      this.toolStripMenuItem7.Enabled = false;
      this.toolStripMenuItem7.Name = "toolStripMenuItem7";
      this.toolStripMenuItem7.Size = new System.Drawing.Size(300, 22);
      this.toolStripMenuItem7.Text = "- Named -";
      // 
      // mitCLoadSelectableConfiguration
      // 
      this.mitCLoadSelectableConfiguration.Name = "mitCLoadSelectableConfiguration";
      this.mitCLoadSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadSelectableConfiguration.Text = "Load Selectable-Configuration ";
      // 
      // mitCSaveSelectableConfiguration
      // 
      this.mitCSaveSelectableConfiguration.Name = "mitCSaveSelectableConfiguration";
      this.mitCSaveSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveSelectableConfiguration.Text = "Save Selectable-Configuration";
      // 
      // mitCShowEditSelectableConfiguration
      // 
      this.mitCShowEditSelectableConfiguration.Name = "mitCShowEditSelectableConfiguration";
      this.mitCShowEditSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditSelectableConfiguration.Text = "Show / Edit Selectable-Configuration";
      // 
      // mitElevator
      // 
      this.mitElevator.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitStartSimulation});
      this.mitElevator.Name = "mitElevator";
      this.mitElevator.Size = new System.Drawing.Size(106, 20);
      this.mitElevator.Text = "Communication";
      // 
      // mitStartSimulation
      // 
      this.mitStartSimulation.Name = "mitStartSimulation";
      this.mitStartSimulation.Size = new System.Drawing.Size(98, 22);
      this.mitStartSimulation.Text = "Start";
      // 
      // mitProtocol
      // 
      this.mitProtocol.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitPEditParameter,
            this.mitPClearProtocol,
            this.mitPCopyToClipboard,
            this.diagnosticToolStripMenuItem,
            this.mitPLoadDiagnosticFile,
            this.mitPSaveDiagnosticFile,
            this.mitSendDiagnosticEmail});
      this.mitProtocol.Name = "mitProtocol";
      this.mitProtocol.Size = new System.Drawing.Size(64, 20);
      this.mitProtocol.Text = "&Protocol";
      // 
      // mitPEditParameter
      // 
      this.mitPEditParameter.Name = "mitPEditParameter";
      this.mitPEditParameter.Size = new System.Drawing.Size(171, 22);
      this.mitPEditParameter.Text = "Edit Parameter";
      // 
      // mitPClearProtocol
      // 
      this.mitPClearProtocol.Name = "mitPClearProtocol";
      this.mitPClearProtocol.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
      this.mitPClearProtocol.Size = new System.Drawing.Size(171, 22);
      this.mitPClearProtocol.Text = "Clear";
      // 
      // mitPCopyToClipboard
      // 
      this.mitPCopyToClipboard.Name = "mitPCopyToClipboard";
      this.mitPCopyToClipboard.Size = new System.Drawing.Size(171, 22);
      this.mitPCopyToClipboard.Text = "Copy to Clipboard";
      // 
      // diagnosticToolStripMenuItem
      // 
      this.diagnosticToolStripMenuItem.Enabled = false;
      this.diagnosticToolStripMenuItem.Name = "diagnosticToolStripMenuItem";
      this.diagnosticToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
      this.diagnosticToolStripMenuItem.Text = "- Diagnostic -";
      // 
      // mitPLoadDiagnosticFile
      // 
      this.mitPLoadDiagnosticFile.Name = "mitPLoadDiagnosticFile";
      this.mitPLoadDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPLoadDiagnosticFile.Text = "Load File";
      // 
      // mitPSaveDiagnosticFile
      // 
      this.mitPSaveDiagnosticFile.Name = "mitPSaveDiagnosticFile";
      this.mitPSaveDiagnosticFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
      this.mitPSaveDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPSaveDiagnosticFile.Text = "Save File";
      // 
      // mitSendDiagnosticEmail
      // 
      this.mitSendDiagnosticEmail.Name = "mitSendDiagnosticEmail";
      this.mitSendDiagnosticEmail.Size = new System.Drawing.Size(171, 22);
      this.mitSendDiagnosticEmail.Text = "Send Email";
      // 
      // mitHelp
      // 
      this.mitHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitHContents,
            this.mitHTopic,
            this.mitHSearch,
            this.mitHEnterApplicationProductKey,
            this.mitHEnterDeviceProductKey,
            this.mitHAbout});
      this.mitHelp.Name = "mitHelp";
      this.mitHelp.Size = new System.Drawing.Size(44, 20);
      this.mitHelp.Text = "Help";
      // 
      // mitHContents
      // 
      this.mitHContents.Name = "mitHContents";
      this.mitHContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.mitHContents.Size = new System.Drawing.Size(234, 22);
      this.mitHContents.Text = "Contents";
      // 
      // mitHTopic
      // 
      this.mitHTopic.Name = "mitHTopic";
      this.mitHTopic.Size = new System.Drawing.Size(234, 22);
      this.mitHTopic.Text = "Topic";
      // 
      // mitHSearch
      // 
      this.mitHSearch.Name = "mitHSearch";
      this.mitHSearch.Size = new System.Drawing.Size(234, 22);
      this.mitHSearch.Text = "Search";
      // 
      // mitHEnterApplicationProductKey
      // 
      this.mitHEnterApplicationProductKey.Name = "mitHEnterApplicationProductKey";
      this.mitHEnterApplicationProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterApplicationProductKey.Text = "Enter Application Product-Key";
      // 
      // mitHEnterDeviceProductKey
      // 
      this.mitHEnterDeviceProductKey.Name = "mitHEnterDeviceProductKey";
      this.mitHEnterDeviceProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterDeviceProductKey.Text = "Enter Device Product-Key";
      // 
      // mitHAbout
      // 
      this.mitHAbout.Name = "mitHAbout";
      this.mitHAbout.ShortcutKeys = System.Windows.Forms.Keys.F2;
      this.mitHAbout.Size = new System.Drawing.Size(234, 22);
      this.mitHAbout.Text = "About";
      // 
      // tbcMain
      // 
      this.tbcMain.Alignment = System.Windows.Forms.TabAlignment.Bottom;
      this.tbcMain.Controls.Add(this.tbpPulses);
      this.tbcMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcMain.Location = new System.Drawing.Point(0, 24);
      this.tbcMain.Name = "tbcMain";
      this.tbcMain.SelectedIndex = 0;
      this.tbcMain.Size = new System.Drawing.Size(735, 729);
      this.tbcMain.TabIndex = 141;
      // 
      // tbpPulses
      // 
      this.tbpPulses.Controls.Add(this.richTextBox1);
      this.tbpPulses.Controls.Add(this.FUCEditorLaserStepFile);
      this.tbpPulses.Controls.Add(this.btnAbortVariableLaser);
      this.tbpPulses.Controls.Add(this.btnPulseVariableLaser);
      this.tbpPulses.Controls.Add(this.btnEnterVariableLaser);
      this.tbpPulses.Controls.Add(this.panel2);
      this.tbpPulses.Controls.Add(this.btnPulsePositionLaser);
      this.tbpPulses.Controls.Add(this.label17);
      this.tbpPulses.Controls.Add(this.nudPositionY);
      this.tbpPulses.Controls.Add(this.btnSetPositionY);
      this.tbpPulses.Controls.Add(this.btnGetPositionY);
      this.tbpPulses.Controls.Add(this.panel1);
      this.tbpPulses.Controls.Add(this.label8);
      this.tbpPulses.Controls.Add(this.nudDelayPulse);
      this.tbpPulses.Controls.Add(this.label7);
      this.tbpPulses.Controls.Add(this.nudDelayMotion);
      this.tbpPulses.Controls.Add(this.label6);
      this.tbpPulses.Controls.Add(this.nudDelta);
      this.tbpPulses.Controls.Add(this.label5);
      this.tbpPulses.Controls.Add(this.nudMax);
      this.tbpPulses.Controls.Add(this.label4);
      this.tbpPulses.Controls.Add(this.nudMin);
      this.tbpPulses.Controls.Add(this.label3);
      this.tbpPulses.Controls.Add(this.nudPositionX);
      this.tbpPulses.Controls.Add(this.tbxMessages);
      this.tbpPulses.Controls.Add(this.tbxHardwareVersion);
      this.tbpPulses.Controls.Add(this.tbxSoftwareVersion);
      this.tbpPulses.Controls.Add(this.label2);
      this.tbpPulses.Controls.Add(this.nudPulseCount);
      this.tbpPulses.Controls.Add(this.label1);
      this.tbpPulses.Controls.Add(this.nudPulsePeriod);
      this.tbpPulses.Controls.Add(this.btnGetHelp);
      this.tbpPulses.Controls.Add(this.btnAbortMatrixLaser);
      this.tbpPulses.Controls.Add(this.btnPulseMatrixLaser);
      this.tbpPulses.Controls.Add(this.btnSetDelayPulse);
      this.tbpPulses.Controls.Add(this.btnSetDelayMotion);
      this.tbpPulses.Controls.Add(this.btnSetMotionParameterY);
      this.tbpPulses.Controls.Add(this.btnSetMotionParameterX);
      this.tbpPulses.Controls.Add(this.btnSetPositionX);
      this.tbpPulses.Controls.Add(this.btnGetPositionX);
      this.tbpPulses.Controls.Add(this.btnPulseLedLaser);
      this.tbpPulses.Controls.Add(this.btnSetLedLaserOff);
      this.tbpPulses.Controls.Add(this.btnSetLedLaserOn);
      this.tbpPulses.Controls.Add(this.btnGetLedLaser);
      this.tbpPulses.Controls.Add(this.btnGetHardwareVersion);
      this.tbpPulses.Controls.Add(this.btnGetSoftwareVersion);
      this.tbpPulses.Controls.Add(this.btnGetProgramHeader);
      this.tbpPulses.Location = new System.Drawing.Point(4, 4);
      this.tbpPulses.Margin = new System.Windows.Forms.Padding(0);
      this.tbpPulses.Name = "tbpPulses";
      this.tbpPulses.Size = new System.Drawing.Size(727, 703);
      this.tbpPulses.TabIndex = 5;
      this.tbpPulses.Text = "LaserAreaScanner";
      // 
      // btnAbortVariableLaser
      // 
      this.btnAbortVariableLaser.Location = new System.Drawing.Point(226, 323);
      this.btnAbortVariableLaser.Name = "btnAbortVariableLaser";
      this.btnAbortVariableLaser.Size = new System.Drawing.Size(105, 23);
      this.btnAbortVariableLaser.TabIndex = 68;
      this.btnAbortVariableLaser.Text = "AbortVariableLaser";
      this.btnAbortVariableLaser.UseVisualStyleBackColor = true;
      this.btnAbortVariableLaser.Click += new System.EventHandler(this.btnAbortVariableLaser_Click);
      // 
      // btnPulseVariableLaser
      // 
      this.btnPulseVariableLaser.Location = new System.Drawing.Point(115, 323);
      this.btnPulseVariableLaser.Name = "btnPulseVariableLaser";
      this.btnPulseVariableLaser.Size = new System.Drawing.Size(105, 23);
      this.btnPulseVariableLaser.TabIndex = 67;
      this.btnPulseVariableLaser.Text = "PulseVariableLaser";
      this.btnPulseVariableLaser.UseVisualStyleBackColor = true;
      this.btnPulseVariableLaser.Click += new System.EventHandler(this.btnPulseVariableLaser_Click);
      // 
      // btnEnterVariableLaser
      // 
      this.btnEnterVariableLaser.Location = new System.Drawing.Point(4, 323);
      this.btnEnterVariableLaser.Name = "btnEnterVariableLaser";
      this.btnEnterVariableLaser.Size = new System.Drawing.Size(105, 23);
      this.btnEnterVariableLaser.TabIndex = 66;
      this.btnEnterVariableLaser.Text = "EnterVariableLaser";
      this.btnEnterVariableLaser.UseVisualStyleBackColor = true;
      this.btnEnterVariableLaser.Click += new System.EventHandler(this.btnEnterVariableLaser_Click);
      // 
      // panel2
      // 
      this.panel2.BackColor = System.Drawing.SystemColors.Info;
      this.panel2.Controls.Add(this.lblStatelaserAreaScanner);
      this.panel2.Controls.Add(this.label19);
      this.panel2.Location = new System.Drawing.Point(249, 31);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(311, 27);
      this.panel2.TabIndex = 65;
      // 
      // lblStatelaserAreaScanner
      // 
      this.lblStatelaserAreaScanner.BackColor = System.Drawing.Color.PeachPuff;
      this.lblStatelaserAreaScanner.Location = new System.Drawing.Point(56, 4);
      this.lblStatelaserAreaScanner.Name = "lblStatelaserAreaScanner";
      this.lblStatelaserAreaScanner.Size = new System.Drawing.Size(252, 19);
      this.lblStatelaserAreaScanner.TabIndex = 66;
      this.lblStatelaserAreaScanner.Text = "- - -";
      this.lblStatelaserAreaScanner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label19
      // 
      this.label19.AutoSize = true;
      this.label19.Location = new System.Drawing.Point(3, 7);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(55, 13);
      this.label19.TabIndex = 65;
      this.label19.Text = "State LAS";
      // 
      // btnPulsePositionLaser
      // 
      this.btnPulsePositionLaser.Location = new System.Drawing.Point(4, 202);
      this.btnPulsePositionLaser.Name = "btnPulsePositionLaser";
      this.btnPulsePositionLaser.Size = new System.Drawing.Size(117, 23);
      this.btnPulsePositionLaser.TabIndex = 64;
      this.btnPulsePositionLaser.Text = "PulsePositionLaser";
      this.btnPulsePositionLaser.UseVisualStyleBackColor = true;
      this.btnPulsePositionLaser.Click += new System.EventHandler(this.btnPulsePositionLaser_Click);
      // 
      // label17
      // 
      this.label17.AutoSize = true;
      this.label17.Location = new System.Drawing.Point(123, 120);
      this.label17.Name = "label17";
      this.label17.Size = new System.Drawing.Size(66, 13);
      this.label17.TabIndex = 63;
      this.label17.Text = "PositionY [1]";
      // 
      // nudPositionY
      // 
      this.nudPositionY.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudPositionY.Location = new System.Drawing.Point(190, 118);
      this.nudPositionY.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionY.Name = "nudPositionY";
      this.nudPositionY.Size = new System.Drawing.Size(48, 20);
      this.nudPositionY.TabIndex = 62;
      this.nudPositionY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionY.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // btnSetPositionY
      // 
      this.btnSetPositionY.Location = new System.Drawing.Point(247, 144);
      this.btnSetPositionY.Name = "btnSetPositionY";
      this.btnSetPositionY.Size = new System.Drawing.Size(77, 23);
      this.btnSetPositionY.TabIndex = 61;
      this.btnSetPositionY.Text = "SetPositionY";
      this.btnSetPositionY.UseVisualStyleBackColor = true;
      this.btnSetPositionY.Click += new System.EventHandler(this.btnSetPositionY_Click);
      // 
      // btnGetPositionY
      // 
      this.btnGetPositionY.Location = new System.Drawing.Point(166, 144);
      this.btnGetPositionY.Name = "btnGetPositionY";
      this.btnGetPositionY.Size = new System.Drawing.Size(77, 23);
      this.btnGetPositionY.TabIndex = 60;
      this.btnGetPositionY.Text = "GetPositionY";
      this.btnGetPositionY.UseVisualStyleBackColor = true;
      this.btnGetPositionY.Click += new System.EventHandler(this.btnGetPositionY_Click);
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.SystemColors.Info;
      this.panel1.Controls.Add(this.pnlLedLaserState);
      this.panel1.Controls.Add(this.label18);
      this.panel1.Location = new System.Drawing.Point(457, 115);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(83, 27);
      this.panel1.TabIndex = 59;
      // 
      // pnlLedLaserState
      // 
      this.pnlLedLaserState.BackColor = System.Drawing.Color.DarkRed;
      this.pnlLedLaserState.Location = new System.Drawing.Point(60, 6);
      this.pnlLedLaserState.Name = "pnlLedLaserState";
      this.pnlLedLaserState.Size = new System.Drawing.Size(16, 15);
      this.pnlLedLaserState.TabIndex = 0;
      // 
      // label18
      // 
      this.label18.AutoSize = true;
      this.label18.Location = new System.Drawing.Point(3, 7);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(58, 13);
      this.label18.TabIndex = 65;
      this.label18.Text = "LaserState";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(318, 271);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(54, 13);
      this.label8.TabIndex = 40;
      this.label8.Text = "Delay [us]";
      // 
      // nudDelayPulse
      // 
      this.nudDelayPulse.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudDelayPulse.Location = new System.Drawing.Point(375, 268);
      this.nudDelayPulse.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudDelayPulse.Name = "nudDelayPulse";
      this.nudDelayPulse.Size = new System.Drawing.Size(69, 20);
      this.nudDelayPulse.TabIndex = 39;
      this.nudDelayPulse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelayPulse.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(95, 270);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(54, 13);
      this.label7.TabIndex = 38;
      this.label7.Text = "Delay [us]";
      // 
      // nudDelayMotion
      // 
      this.nudDelayMotion.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudDelayMotion.Location = new System.Drawing.Point(152, 267);
      this.nudDelayMotion.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudDelayMotion.Name = "nudDelayMotion";
      this.nudDelayMotion.Size = new System.Drawing.Size(69, 20);
      this.nudDelayMotion.TabIndex = 37;
      this.nudDelayMotion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelayMotion.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(440, 241);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(47, 13);
      this.label6.TabIndex = 36;
      this.label6.Text = "Delta [1]";
      // 
      // nudDelta
      // 
      this.nudDelta.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudDelta.Location = new System.Drawing.Point(487, 239);
      this.nudDelta.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudDelta.Name = "nudDelta";
      this.nudDelta.Size = new System.Drawing.Size(48, 20);
      this.nudDelta.TabIndex = 35;
      this.nudDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelta.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(344, 241);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(42, 13);
      this.label5.TabIndex = 34;
      this.label5.Text = "Max [1]";
      // 
      // nudMax
      // 
      this.nudMax.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudMax.Location = new System.Drawing.Point(387, 239);
      this.nudMax.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudMax.Name = "nudMax";
      this.nudMax.Size = new System.Drawing.Size(48, 20);
      this.nudMax.TabIndex = 33;
      this.nudMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMax.Value = new decimal(new int[] {
            2100,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(251, 241);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(39, 13);
      this.label4.TabIndex = 32;
      this.label4.Text = "Min [1]";
      // 
      // nudMin
      // 
      this.nudMin.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudMin.Location = new System.Drawing.Point(291, 239);
      this.nudMin.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudMin.Name = "nudMin";
      this.nudMin.Size = new System.Drawing.Size(48, 20);
      this.nudMin.TabIndex = 31;
      this.nudMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMin.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(5, 120);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(66, 13);
      this.label3.TabIndex = 30;
      this.label3.Text = "PositionX [1]";
      // 
      // nudPositionX
      // 
      this.nudPositionX.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudPositionX.Location = new System.Drawing.Point(72, 118);
      this.nudPositionX.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionX.Name = "nudPositionX";
      this.nudPositionX.Size = new System.Drawing.Size(48, 20);
      this.nudPositionX.TabIndex = 29;
      this.nudPositionX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionX.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // tbxMessages
      // 
      this.tbxMessages.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxMessages.Location = new System.Drawing.Point(4, 62);
      this.tbxMessages.Multiline = true;
      this.tbxMessages.Name = "tbxMessages";
      this.tbxMessages.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.tbxMessages.Size = new System.Drawing.Size(556, 49);
      this.tbxMessages.TabIndex = 27;
      // 
      // tbxHardwareVersion
      // 
      this.tbxHardwareVersion.Enabled = false;
      this.tbxHardwareVersion.Location = new System.Drawing.Point(125, 6);
      this.tbxHardwareVersion.Name = "tbxHardwareVersion";
      this.tbxHardwareVersion.Size = new System.Drawing.Size(85, 20);
      this.tbxHardwareVersion.TabIndex = 24;
      this.tbxHardwareVersion.Text = "-";
      this.tbxHardwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tbxSoftwareVersion
      // 
      this.tbxSoftwareVersion.Enabled = false;
      this.tbxSoftwareVersion.Location = new System.Drawing.Point(337, 7);
      this.tbxSoftwareVersion.Name = "tbxSoftwareVersion";
      this.tbxSoftwareVersion.Size = new System.Drawing.Size(85, 20);
      this.tbxSoftwareVersion.TabIndex = 23;
      this.tbxSoftwareVersion.Text = "-";
      this.tbxSoftwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(351, 120);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(50, 13);
      this.label2.TabIndex = 22;
      this.label2.Text = "Count [1]";
      // 
      // nudPulseCount
      // 
      this.nudPulseCount.Location = new System.Drawing.Point(402, 118);
      this.nudPulseCount.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudPulseCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPulseCount.Name = "nudPulseCount";
      this.nudPulseCount.Size = new System.Drawing.Size(48, 20);
      this.nudPulseCount.TabIndex = 21;
      this.nudPulseCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulseCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(241, 120);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(59, 13);
      this.label1.TabIndex = 20;
      this.label1.Text = "Period [ms]";
      // 
      // nudPulsePeriod
      // 
      this.nudPulsePeriod.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudPulsePeriod.Location = new System.Drawing.Point(301, 118);
      this.nudPulsePeriod.Maximum = new decimal(new int[] {
            9000,
            0,
            0,
            0});
      this.nudPulsePeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPulsePeriod.Name = "nudPulsePeriod";
      this.nudPulsePeriod.Size = new System.Drawing.Size(48, 20);
      this.nudPulsePeriod.TabIndex = 18;
      this.nudPulsePeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulsePeriod.Value = new decimal(new int[] {
            333,
            0,
            0,
            0});
      // 
      // btnGetHelp
      // 
      this.btnGetHelp.Location = new System.Drawing.Point(4, 33);
      this.btnGetHelp.Name = "btnGetHelp";
      this.btnGetHelp.Size = new System.Drawing.Size(117, 23);
      this.btnGetHelp.TabIndex = 16;
      this.btnGetHelp.Text = "GetHelp";
      this.btnGetHelp.UseVisualStyleBackColor = true;
      this.btnGetHelp.Click += new System.EventHandler(this.btnGetHelp_Click);
      // 
      // btnAbortMatrixLaser
      // 
      this.btnAbortMatrixLaser.Location = new System.Drawing.Point(104, 294);
      this.btnAbortMatrixLaser.Name = "btnAbortMatrixLaser";
      this.btnAbortMatrixLaser.Size = new System.Drawing.Size(96, 23);
      this.btnAbortMatrixLaser.TabIndex = 15;
      this.btnAbortMatrixLaser.Text = "AbortMatrixLaser";
      this.btnAbortMatrixLaser.UseVisualStyleBackColor = true;
      this.btnAbortMatrixLaser.Click += new System.EventHandler(this.btnAbortMatrixLaser_Click);
      // 
      // btnPulseMatrixLaser
      // 
      this.btnPulseMatrixLaser.Location = new System.Drawing.Point(3, 294);
      this.btnPulseMatrixLaser.Name = "btnPulseMatrixLaser";
      this.btnPulseMatrixLaser.Size = new System.Drawing.Size(97, 23);
      this.btnPulseMatrixLaser.TabIndex = 14;
      this.btnPulseMatrixLaser.Text = "PulseMatrixLaser";
      this.btnPulseMatrixLaser.UseVisualStyleBackColor = true;
      this.btnPulseMatrixLaser.Click += new System.EventHandler(this.btnPulseMatrixLaser_Click);
      // 
      // btnSetDelayPulse
      // 
      this.btnSetDelayPulse.Location = new System.Drawing.Point(227, 265);
      this.btnSetDelayPulse.Name = "btnSetDelayPulse";
      this.btnSetDelayPulse.Size = new System.Drawing.Size(90, 23);
      this.btnSetDelayPulse.TabIndex = 13;
      this.btnSetDelayPulse.Text = "SetDelayPulse";
      this.btnSetDelayPulse.UseVisualStyleBackColor = true;
      this.btnSetDelayPulse.Click += new System.EventHandler(this.btnSetDelayPulse_Click);
      // 
      // btnSetDelayMotion
      // 
      this.btnSetDelayMotion.Location = new System.Drawing.Point(3, 265);
      this.btnSetDelayMotion.Name = "btnSetDelayMotion";
      this.btnSetDelayMotion.Size = new System.Drawing.Size(90, 23);
      this.btnSetDelayMotion.TabIndex = 12;
      this.btnSetDelayMotion.Text = "SetDelayMotion";
      this.btnSetDelayMotion.UseVisualStyleBackColor = true;
      this.btnSetDelayMotion.Click += new System.EventHandler(this.btnSetDelayMotion_Click);
      // 
      // btnSetMotionParameterY
      // 
      this.btnSetMotionParameterY.Location = new System.Drawing.Point(127, 236);
      this.btnSetMotionParameterY.Name = "btnSetMotionParameterY";
      this.btnSetMotionParameterY.Size = new System.Drawing.Size(118, 23);
      this.btnSetMotionParameterY.TabIndex = 11;
      this.btnSetMotionParameterY.Text = "SetMotionParameterY";
      this.btnSetMotionParameterY.UseVisualStyleBackColor = true;
      this.btnSetMotionParameterY.Click += new System.EventHandler(this.btnSetMotionParameterY_Click);
      // 
      // btnSetMotionParameterX
      // 
      this.btnSetMotionParameterX.Location = new System.Drawing.Point(3, 236);
      this.btnSetMotionParameterX.Name = "btnSetMotionParameterX";
      this.btnSetMotionParameterX.Size = new System.Drawing.Size(118, 23);
      this.btnSetMotionParameterX.TabIndex = 10;
      this.btnSetMotionParameterX.Text = "SetMotionParameterX";
      this.btnSetMotionParameterX.UseVisualStyleBackColor = true;
      this.btnSetMotionParameterX.Click += new System.EventHandler(this.btnSetMotionParameterX_Click);
      // 
      // btnSetPositionX
      // 
      this.btnSetPositionX.Location = new System.Drawing.Point(85, 144);
      this.btnSetPositionX.Name = "btnSetPositionX";
      this.btnSetPositionX.Size = new System.Drawing.Size(77, 23);
      this.btnSetPositionX.TabIndex = 9;
      this.btnSetPositionX.Text = "SetPositionX";
      this.btnSetPositionX.UseVisualStyleBackColor = true;
      this.btnSetPositionX.Click += new System.EventHandler(this.btnSetPositionX_Click);
      // 
      // btnGetPositionX
      // 
      this.btnGetPositionX.Location = new System.Drawing.Point(4, 144);
      this.btnGetPositionX.Name = "btnGetPositionX";
      this.btnGetPositionX.Size = new System.Drawing.Size(77, 23);
      this.btnGetPositionX.TabIndex = 8;
      this.btnGetPositionX.Text = "GetPositionX";
      this.btnGetPositionX.UseVisualStyleBackColor = true;
      this.btnGetPositionX.Click += new System.EventHandler(this.btnGetPositionX_Click);
      // 
      // btnPulseLedLaser
      // 
      this.btnPulseLedLaser.Location = new System.Drawing.Point(247, 173);
      this.btnPulseLedLaser.Name = "btnPulseLedLaser";
      this.btnPulseLedLaser.Size = new System.Drawing.Size(67, 23);
      this.btnPulseLedLaser.TabIndex = 7;
      this.btnPulseLedLaser.Text = "PulseLaser";
      this.btnPulseLedLaser.UseVisualStyleBackColor = true;
      this.btnPulseLedLaser.Click += new System.EventHandler(this.btnPulseLedLaser_Click);
      // 
      // btnSetLedLaserOff
      // 
      this.btnSetLedLaserOff.Location = new System.Drawing.Point(172, 173);
      this.btnSetLedLaserOff.Name = "btnSetLedLaserOff";
      this.btnSetLedLaserOff.Size = new System.Drawing.Size(71, 23);
      this.btnSetLedLaserOff.TabIndex = 5;
      this.btnSetLedLaserOff.Text = "SetLaserOff";
      this.btnSetLedLaserOff.UseVisualStyleBackColor = true;
      this.btnSetLedLaserOff.Click += new System.EventHandler(this.btnSetLedLaserOff_Click);
      // 
      // btnSetLedLaserOn
      // 
      this.btnSetLedLaserOn.Location = new System.Drawing.Point(97, 173);
      this.btnSetLedLaserOn.Name = "btnSetLedLaserOn";
      this.btnSetLedLaserOn.Size = new System.Drawing.Size(71, 23);
      this.btnSetLedLaserOn.TabIndex = 4;
      this.btnSetLedLaserOn.Text = "SetLaserOn";
      this.btnSetLedLaserOn.UseVisualStyleBackColor = true;
      this.btnSetLedLaserOn.Click += new System.EventHandler(this.btnSetLedLaserOn_Click);
      // 
      // btnGetLedLaser
      // 
      this.btnGetLedLaser.Location = new System.Drawing.Point(4, 173);
      this.btnGetLedLaser.Name = "btnGetLedLaser";
      this.btnGetLedLaser.Size = new System.Drawing.Size(89, 23);
      this.btnGetLedLaser.TabIndex = 3;
      this.btnGetLedLaser.Text = "GetLaserState";
      this.btnGetLedLaser.UseVisualStyleBackColor = true;
      this.btnGetLedLaser.Click += new System.EventHandler(this.btnGetLedLaser_Click);
      // 
      // btnGetHardwareVersion
      // 
      this.btnGetHardwareVersion.Location = new System.Drawing.Point(3, 4);
      this.btnGetHardwareVersion.Name = "btnGetHardwareVersion";
      this.btnGetHardwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetHardwareVersion.TabIndex = 2;
      this.btnGetHardwareVersion.Text = "GetHardwareVersion";
      this.btnGetHardwareVersion.UseVisualStyleBackColor = true;
      this.btnGetHardwareVersion.Click += new System.EventHandler(this.btnGetHardwareVersion_Click);
      // 
      // btnGetSoftwareVersion
      // 
      this.btnGetSoftwareVersion.Location = new System.Drawing.Point(215, 5);
      this.btnGetSoftwareVersion.Name = "btnGetSoftwareVersion";
      this.btnGetSoftwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetSoftwareVersion.TabIndex = 1;
      this.btnGetSoftwareVersion.Text = "GetSoftwareVersion";
      this.btnGetSoftwareVersion.UseVisualStyleBackColor = true;
      this.btnGetSoftwareVersion.Click += new System.EventHandler(this.btnGetSoftwareVersion_Click);
      // 
      // btnGetProgramHeader
      // 
      this.btnGetProgramHeader.Location = new System.Drawing.Point(125, 33);
      this.btnGetProgramHeader.Name = "btnGetProgramHeader";
      this.btnGetProgramHeader.Size = new System.Drawing.Size(117, 23);
      this.btnGetProgramHeader.TabIndex = 0;
      this.btnGetProgramHeader.Text = "GetProgramHeader";
      this.btnGetProgramHeader.UseVisualStyleBackColor = true;
      this.btnGetProgramHeader.Click += new System.EventHandler(this.btnGetProgramHeader_Click);
      // 
      // cbxAutomate
      // 
      this.cbxAutomate.AutoSize = true;
      this.cbxAutomate.Location = new System.Drawing.Point(376, 5);
      this.cbxAutomate.Name = "cbxAutomate";
      this.cbxAutomate.Size = new System.Drawing.Size(71, 17);
      this.cbxAutomate.TabIndex = 142;
      this.cbxAutomate.Text = "Automate";
      this.cbxAutomate.UseVisualStyleBackColor = true;
      this.cbxAutomate.CheckedChanged += new System.EventHandler(this.cbxAutomate_CheckedChanged);
      // 
      // tmrAutomation
      // 
      this.tmrAutomation.Interval = 1000;
      // 
      // richTextBox1
      // 
      this.richTextBox1.Location = new System.Drawing.Point(556, 173);
      this.richTextBox1.Name = "richTextBox1";
      this.richTextBox1.Size = new System.Drawing.Size(100, 96);
      this.richTextBox1.TabIndex = 70;
      this.richTextBox1.Text = "";
      // 
      // FUCEditorLaserStepFile
      // 
      this.FUCEditorLaserStepFile.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.FUCEditorLaserStepFile.FileName = "";
      this.FUCEditorLaserStepFile.IsReadOnly = false;
      this.FUCEditorLaserStepFile.Lines = new string[0];
      this.FUCEditorLaserStepFile.LoadFileExtension = "lsf.txt";
      this.FUCEditorLaserStepFile.LoadFileFilter = "LaserStepFiles (*.lsf.txt)|*.lsf.txt|Textfiles (*.txt)|*.txt|All files (*.*)|*.*";
      this.FUCEditorLaserStepFile.LoadFileName = "";
      this.FUCEditorLaserStepFile.Location = new System.Drawing.Point(0, 355);
      this.FUCEditorLaserStepFile.Name = "FUCEditorLaserStepFile";
      this.FUCEditorLaserStepFile.SaveFileExtension = "cmt.txt";
      this.FUCEditorLaserStepFile.SaveFileFilter = "Commentfiles (*.cmt.txt)|*.cmt.txt|Textfiles (*.txt)|*.txt|All files (*.*)|*.*";
      this.FUCEditorLaserStepFile.SaveFileName = "";
      this.FUCEditorLaserStepFile.Size = new System.Drawing.Size(727, 348);
      this.FUCEditorLaserStepFile.TabIndex = 69;
      this.FUCEditorLaserStepFile.WorkingDirectory = "";
      // 
      // tmrLaserStepList
      // 
      this.tmrLaserStepList.Interval = 1000;
      this.tmrLaserStepList.Tick += new System.EventHandler(this.tmrLaserStepList_Tick);
      // 
      // FormClient
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(735, 921);
      this.Controls.Add(this.cbxAutomate);
      this.Controls.Add(this.tbcMain);
      this.Controls.Add(this.mstMain);
      this.Controls.Add(this.splProtocol);
      this.Controls.Add(this.pnlProtocol);
      this.Name = "FormClient";
      this.Text = "Form1";
      this.pnlProtocol.ResumeLayout(false);
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.tbcMain.ResumeLayout(false);
      this.tbpPulses.ResumeLayout(false);
      this.tbpPulses.PerformLayout();
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionY)).EndInit();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayPulse)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayMotion)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelta)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMax)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMin)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionX)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriod)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel pnlProtocol;
    private UCSerialNumber.CUCSerialNumber FUCSerialNumber;
    private System.Windows.Forms.Splitter splProtocol;
    private System.Windows.Forms.Timer tmrStartup;
    private System.Windows.Forms.HelpProvider FHelpProvider;
    private System.Windows.Forms.SaveFileDialog DialogSaveInitdata;
    private System.Windows.Forms.OpenFileDialog DialogLoadInitdata;
    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem mitSystem;
    private System.Windows.Forms.ToolStripMenuItem mitSQuit;
    private System.Windows.Forms.ToolStripMenuItem mitConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCDefault;
    private System.Windows.Forms.ToolStripMenuItem mitCResetToDefaultConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveAutomaticAtProgramEnd;
    private System.Windows.Forms.ToolStripMenuItem mitCStartup;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitElevator;
    private System.Windows.Forms.ToolStripMenuItem mitStartSimulation;
    private System.Windows.Forms.ToolStripMenuItem mitProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPEditParameter;
    private System.Windows.Forms.ToolStripMenuItem mitPClearProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPCopyToClipboard;
    private System.Windows.Forms.ToolStripMenuItem diagnosticToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitPLoadDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitPSaveDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitSendDiagnosticEmail;
    private System.Windows.Forms.ToolStripMenuItem mitHelp;
    private System.Windows.Forms.ToolStripMenuItem mitHContents;
    private System.Windows.Forms.ToolStripMenuItem mitHTopic;
    private System.Windows.Forms.ToolStripMenuItem mitHSearch;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterApplicationProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterDeviceProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHAbout;
    private System.Windows.Forms.TabControl tbcMain;
    private System.Windows.Forms.TabPage tbpPulses;
    private System.Windows.Forms.CheckBox cbxAutomate;
    private System.Windows.Forms.Button btnAbortMatrixLaser;
    private System.Windows.Forms.Button btnPulseMatrixLaser;
    private System.Windows.Forms.Button btnSetDelayPulse;
    private System.Windows.Forms.Button btnSetDelayMotion;
    private System.Windows.Forms.Button btnSetMotionParameterY;
    private System.Windows.Forms.Button btnSetMotionParameterX;
    private System.Windows.Forms.Button btnSetPositionX;
    private System.Windows.Forms.Button btnGetPositionX;
    private System.Windows.Forms.Button btnPulseLedLaser;
    private System.Windows.Forms.Button btnSetLedLaserOff;
    private System.Windows.Forms.Button btnSetLedLaserOn;
    private System.Windows.Forms.Button btnGetLedLaser;
    private System.Windows.Forms.Button btnGetHardwareVersion;
    private System.Windows.Forms.Button btnGetSoftwareVersion;
    private System.Windows.Forms.Button btnGetProgramHeader;
    private System.Windows.Forms.Button btnGetHelp;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown nudPulseCount;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudPulsePeriod;
    private System.Windows.Forms.TextBox tbxHardwareVersion;
    private System.Windows.Forms.TextBox tbxSoftwareVersion;
    private System.Windows.Forms.TextBox tbxMessages;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown nudPositionX;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.NumericUpDown nudDelta;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.NumericUpDown nudMax;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.NumericUpDown nudMin;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.NumericUpDown nudDelayPulse;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.NumericUpDown nudDelayMotion;
    private System.Windows.Forms.Timer tmrAutomation;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel pnlLedLaserState;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.NumericUpDown nudPositionY;
    private System.Windows.Forms.Button btnSetPositionY;
    private System.Windows.Forms.Button btnGetPositionY;
    private System.Windows.Forms.Button btnPulsePositionLaser;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Label lblStatelaserAreaScanner;
    private System.Windows.Forms.Label label19;
    private UCTextEditor.CUCEditTextEditor FUCEditorLaserStepFile;
    private System.Windows.Forms.Button btnAbortVariableLaser;
    private System.Windows.Forms.Button btnPulseVariableLaser;
    private System.Windows.Forms.Button btnEnterVariableLaser;
    private System.Windows.Forms.RichTextBox richTextBox1;
    private System.Windows.Forms.Timer tmrLaserStepList;
    
  }
}

