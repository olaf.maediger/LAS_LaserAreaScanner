﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using UCNotifier;
using Network;
using UdpTextTransfer;
//
namespace UdpTextDevice
{
  public abstract class CUdpTextDeviceBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const String TOKEN_MESSAGES = "Messages";
    public const String FORMAT_MESSAGE = "Message[{0}][{1}]";
    //
    public const String FORMAT_MESSAGECOUNT = "MessageCount[{0}]";
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    protected IPAddress FSourceIpAddress, FTargetIpAddress;
    protected UInt16 FSourceIpPort, FTargetIpPort;
    protected Guid FSourceID, FTargetID;
    protected String FSourceType, FTargetType;
    protected String FSourceName, FTargetName;
    protected CUdpTextTransmitter FUdpTextTransmitter;
    protected CUdpTextReceiver FUdpTextReceiver;
    //!!!protected CUdpTextDatagramContainerReceive FUdpTextDatagramContainerReceive;
    //!!!protected CUdpTextDatagramContainerTransmit FUdpTextDatagramContainerTransmit;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUdpTextDeviceBase()
    {
      FUdpTextTransmitter = new CUdpTextTransmitter();
      FUdpTextReceiver = new CUdpTextReceiver();
      //FUdpTextDatagramContainerReceive = new CUdpTextDatagramContainerReceive();
      //FUdpTextDatagramContainerTransmit = new CUdpTextDatagramContainerTransmit();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FUdpTextTransmitter.SetNotifier(value);
      FUdpTextReceiver.SetNotifier(value);
    }

    public void SetOnDatagramTransmitted(DOnDatagramTransmitted value)
    {
      FUdpTextTransmitter.SetOnDatagramTransmitted(value);
    }
    
    public void SetOnDatagramReceived(DOnDatagramReceived value)
    {
      FUdpTextReceiver.SetOnDatagramReceived(value);
    }

    public void SetSourceIpAddress(IPAddress value)
    {
      FSourceIpAddress = value;
    }
    public void SetTargetIpAddress(IPAddress value)
    {
      FTargetIpAddress = value;
    }

    public void SetSourceIpPort(UInt16 value)
    {
      FSourceIpPort = value;
    }
    public void SetTargetIpPort(UInt16 value)
    {
      FTargetIpPort = value;
    }
    
    public void SetSourceType(String value)
    {
      FSourceType = value;
    }
    public void SetTargetType(String value)
    {
      FTargetType = value;
    }

    public void SetSourceName(String value)
    {
      FSourceName = value;
    }
    public void SetTargetName(String value)
    {
      FTargetName = value;
    }

    public void SetSourceID(Guid value)
    {
      FSourceID = value;
    }
    public void SetTargetID(Guid value)
    {
      FTargetID = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    protected Boolean BuildHeader(String datagramtype,
                                  String datagramkey,
                                  out String header)
    {
      header = "";
      try
      {
        header += String.Format(CUdpTextHeader.FORMAT_PROTOCOL, CDatagram.TOKEN_UDPTEXT);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_TIMESTAMP, CDateTime.GetNow());
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_SOURCEIPADDRESS, FSourceIpAddress.ToString());
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_SOURCEID, FSourceID.ToString());
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_SOURCETYPE, FSourceType);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_SOURCENAME, FSourceName);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_TARGETIPADDRESS, FTargetIpAddress.ToString());
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_TARGETID, FTargetID);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_TARGETTYPE, FTargetType);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_TARGETNAME, FTargetName);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_DATAGRAMTYPE, datagramtype);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        header += String.Format(CUdpTextHeader.FORMAT_DATAGRAMKEY, datagramkey);
        header += CUdpTextBase.SEPARATOR_UDPTEXT;
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    protected Boolean BuildDatagram(String datagramtype,
                                    String datagramkey,
                                    String[] messages,
                                    out String sdatagram)
    {
      sdatagram = "";
      try
      { // Header
        BuildHeader(datagramtype, datagramkey, out sdatagram);
        // Content
        Int32 MC = messages.Length;
        sdatagram += String.Format(FORMAT_MESSAGECOUNT, MC);
        sdatagram += CUdpTextBase.SEPARATOR_UDPTEXT;
        for (Int32 MI = 0; MI < MC; MI++)
        {
          sdatagram += String.Format(FORMAT_MESSAGE, MI, messages[MI]);
          sdatagram += CUdpTextBase.SEPARATOR_UDPTEXT;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public abstract Boolean Open();
    public abstract Boolean Close();
    //
    //--------------------------------------------------------------------------
    //  Segment - 
    //--------------------------------------------------------------------------
    //

  }
}
