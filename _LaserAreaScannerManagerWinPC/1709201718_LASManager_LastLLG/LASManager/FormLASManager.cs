﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using Network;
using NetworkDevice;
using UdpTextDevice;
using CommandProtocol;
using CPLaserAreaScanner;
//
namespace LASManager
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormClient : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICETYPE = "LASManager";
    private const String INIT_DEVICENAME = "OMDevelopDebugUdpTextBinaryClient";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    private CDeviceParameter FDeviceParameter;
    private CUdpTextDeviceClient FUdpTextDeviceClient;
    //
    private CCommandList FCommandList;
    private Boolean FResult = false;
    private Int32 FRxdCount = 0;
    private Int32 FRxdIndex = 0;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      //####################################################################################
      // Init - Instance - Client
      //####################################################################################
      //
      FDeviceParameter = new CDeviceParameter();
      FDeviceParameter.Type = INIT_DEVICETYPE;
      FDeviceParameter.Name = INIT_DEVICENAME;
      FDeviceParameter.ID = Guid.NewGuid();
      IPAddress IPALocal;
      CNetwork.FindIPAddressLocal(out IPALocal);
      FDeviceParameter.IPA = IPALocal;
      IPAddress IPATarget;
      CNetwork.FindIPAddressLocalBroadcast(out IPATarget);
      //
      // UdpTextDeviceClient
      FUdpTextDeviceClient = new CUdpTextDeviceClient();
      FUdpTextDeviceClient.SetNotifier(FUCNotifier);
      FUdpTextDeviceClient.SetOnDatagramTransmitted(UdpTextDeviceClientOnDatagramTransmitted);
      FUdpTextDeviceClient.SetOnDatagramReceived(UdpTextDeviceClientOnDatagramReceived);
      FUdpTextDeviceClient.SetSourceIpAddress(IPALocal);
      FUdpTextDeviceClient.SetSourceIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_CLIENT_TRANSMITTER);
      FUdpTextDeviceClient.SetTargetIpAddress(IPATarget);
      FUdpTextDeviceClient.SetTargetIpPort(CNetworkDevice.PORT_UDPTEXT_MESSAGE_CLIENT_RECEIVER);
      FUdpTextDeviceClient.Open();
      //
      FCommandList = new CCommandList();
      // -> Main! FCommandList.SetUdpTextDeviceClient(FUdpTextDeviceClient);
      FCommandList.SetNotifier(FUCNotifier);
      FCommandList.SetOnExecutionStart(CommandListOnExecutionStart);
      FCommandList.SetOnExecutionBusy(CommandListOnExecutionBusy);
      FCommandList.SetOnExecutionEnd(CommandListOnExecutionEnd);
      FCommandList.SetOnExecutionAbort(CommandListOnExecutionAbort);
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      // Start Parallel-Application: LanUdpTextServer.exe
      // Process.Start("LanUdpTextServer.exe");
      Process.Start("LanUartUdpTextServer.exe");
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FUdpTextDeviceClient.Close();
      // -> LanUdpTextServer
      //Process[] ProcessesLanUdpServer = Process.GetProcessesByName("LanUdpTextServer");
      //foreach (Process ProcessLanUdpServer in ProcessesLanUdpServer)
      //{
      //  ProcessLanUdpServer.Kill();
      //}
      Process[] PS = Process.GetProcessesByName("LanUartUdpTextServer");
      foreach (Process P in PS)
      {
        P.Kill();
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //
    //
    //###########################################################################################
    //  Segment - Analyse - Common
    //###########################################################################################
    //
    private delegate Boolean CBAnalyseDateTimePrompt(String text);
    private Boolean AnalyseDateTimePrompt(String text)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseDateTimePrompt CB = new CBAnalyseDateTimePrompt(AnalyseDateTimePrompt);
        Invoke(CB, new object[] { text });
        return FResult;
      }
      else
      {
        FResult = false;
        if (13 == text.Length)
        {
          String[] Tokens = text.Split(new Char[] { ':', '.', '>' }, StringSplitOptions.RemoveEmptyEntries);
          if (3 < Tokens.Length)
          {
            FUCNotifier.Write(String.Format("DATETIME[{0}:{1}:{2}.{3}]", Tokens[0], Tokens[1], Tokens[2], Tokens[3]));
            FResult = true;
          }
        }
        return FResult;
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - Help
    //###########################################################################################
    //
    private delegate Boolean CBAnalyseGetHelp(CCommand command, String datagram);
    private Boolean AnalyseGetHelp(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetHelp CB = new CBAnalyseGetHelp(AnalyseGetHelp);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        FResult = false;
        if (command is CGetHelp)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (1 <= Tokens.Length)
          {
            if ('#' == Tokens[0][0])
            {
              FRxdIndex++;
              String Line = String.Format("HELP[{0}|{1}]<{2}>", FRxdIndex, FRxdCount, datagram);
              FUCNotifier.Write(Line);
              if (FRxdCount <= FRxdIndex)
              {
                command.SignalTextReceived();
                tbxMessages.Text += datagram;
              }
              else
              {
                tbxMessages.Text += datagram + "\r\n";
              }
            }
            else
              if (3 <= Tokens.Length)
              {
                if (":" == Tokens[0])
                {
                  if (CGetHelp.HEADER != Tokens[1])
                  {
                    FUCNotifier.Error("Main", 1, CGetHelp.HEADER + " expected");
                  }
                  Int32.TryParse(Tokens[2], out FRxdCount);
                  tbxMessages.Text = "";
                  FRxdIndex = 0;
                }
              }
            return true;
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseGetProgramHeader(CCommand command, String datagram);
    private Boolean AnalyseGetProgramHeader(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetProgramHeader CB = new CBAnalyseGetProgramHeader(AnalyseGetProgramHeader);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        FResult = false;
        if (command is CGetProgramHeader)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (1 <= Tokens.Length)
          {
            if ('#' == Tokens[0][0])
            {
              FRxdIndex++;
              String Line = String.Format("PROGRAMHEADER[{0}|{1}]<{2}>", FRxdIndex, FRxdCount, datagram);
              FUCNotifier.Write(Line);              
              if (FRxdCount <= FRxdIndex)
              {
                command.SignalTextReceived();
                tbxMessages.Text += datagram;
              }
              else
              {
                tbxMessages.Text += datagram + "\r\n";
              }
            }
            else
              if (3 <= Tokens.Length)
              {
                if (":" == Tokens[0])
                {
                  if (CGetProgramHeader.HEADER != Tokens[1])
                  {
                    FUCNotifier.Error("Main", 1, CGetProgramHeader.HEADER + " expected");
                  }
                  Int32.TryParse(Tokens[2], out FRxdCount);
                  tbxMessages.Text = "";
                  FRxdIndex = 0;
                }
              }
            return true;
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseGetHardwareVersion(CCommand command, String datagram);
    private Boolean AnalyseGetHardwareVersion(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetHardwareVersion CB = new CBAnalyseGetHardwareVersion(AnalyseGetHardwareVersion);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        FResult = false;
        if (command is CGetHardwareVersion)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CGetHardwareVersion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetHardwareVersion.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FRxdIndex = 0;
            }
            else
              if ("#" == Tokens[0])
              {
                FRxdIndex++;
                FUCNotifier.Write(String.Format("HARDWAREVERSION[{0}]", Tokens[2]));
                tbxHardwareVersion.Text = Tokens[2];
                if (FRxdCount <= FRxdIndex) command.SignalTextReceived();
              }
            return true;
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseGetSoftwareVersion(CCommand command, String datagram);
    private Boolean AnalyseGetSoftwareVersion(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetSoftwareVersion CB = new CBAnalyseGetSoftwareVersion(AnalyseGetSoftwareVersion);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CGetSoftwareVersion)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CGetSoftwareVersion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetSoftwareVersion.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FRxdIndex = 0;
            }
            else
              if ("#" == Tokens[0])
              {
                FRxdIndex++;
                FUCNotifier.Write(String.Format("SOFTWAREVERSION[{0}]", Tokens[2]));
                tbxSoftwareVersion.Text = Tokens[2];
                if (FRxdCount <= FRxdIndex) command.SignalTextReceived();
              }
            return true;
          }
        }
        return false;
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LedLaser
    //###########################################################################################
    //
    private delegate Boolean CBAnalyseGetLedLaser(CCommand command, String datagram);
    private Boolean AnalyseGetLedLaser(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetLedLaser CB = new CBAnalyseGetLedLaser(AnalyseGetLedLaser);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CGetLedLaser)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CGetLedLaser.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetLedLaser.HEADER + " expected");
              }
              Int32 State;
              Int32.TryParse(Tokens[2], out State);
              FUCNotifier.Write(String.Format("GETLEDLASER[{0}]", State));
              command.SignalTextReceived();
              //
              if (0 < State)
              {
                pnlLedLaserState.BackColor = Color.LightPink;
              }
              else
              {
                pnlLedLaserState.BackColor = Color.DarkRed;
              }
              //
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseSetLedLaserOn(CCommand command, String datagram);
    private Boolean AnalyseSetLedLaserOn(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetLedLaserOn CB = new CBAnalyseSetLedLaserOn(AnalyseSetLedLaserOn);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CSetLedLaserOn)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CSetLedLaserOn.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetLedLaserOn.HEADER + " expected");
              }
              FUCNotifier.Write(String.Format("SETLEDLASERON[]"));
              command.SignalTextReceived();
              //
              pnlLedLaserState.BackColor = Color.LightPink;
              //
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseSetLedLaserOff(CCommand command, String datagram);
    private Boolean AnalyseSetLedLaserOff(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetLedLaserOff CB = new CBAnalyseSetLedLaserOff(AnalyseSetLedLaserOff);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CSetLedLaserOff)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CSetLedLaserOff.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetLedLaserOff.HEADER + " expected");
              }
              FUCNotifier.Write(String.Format("SETLEDLASEROFF[]"));
              command.SignalTextReceived();
              //
              pnlLedLaserState.BackColor = Color.DarkRed;
              //
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalysePulseLedLaser(CCommand command, String datagram);
    private Boolean AnalysePulseLedLaser(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalysePulseLedLaser CB = new CBAnalysePulseLedLaser(AnalysePulseLedLaser);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CPulseLedLaser)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (4 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CPulseLedLaser.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CPulseLedLaser.HEADER + " expected");
              }
              Int32 Period;
              Int32.TryParse(Tokens[2], out Period);
              Int32 Count;
              Int32.TryParse(Tokens[3], out Count);
              FUCNotifier.Write(String.Format("PULSELEDLASER[Delay<{0}>Times<{1}>", Period, Count));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - Position
    //###########################################################################################
    //
    private delegate Boolean CBAnalyseGetPositionX(CCommand command, String datagram);
    private Boolean AnalyseGetPositionX(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetPositionX CB = new CBAnalyseGetPositionX(AnalyseGetPositionX);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CGetPositionX)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CGetPositionX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetPositionX.HEADER + " expected");
              }
              Int32 Position;
              Int32.TryParse(Tokens[2], out Position);
              FUCNotifier.Write(String.Format("GETPOSITIONX[{0}]", Position));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseSetPositionX(CCommand command, String datagram);
    private Boolean AnalyseSetPositionX(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetPositionX CB = new CBAnalyseSetPositionX(AnalyseSetPositionX);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CSetPositionX)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CSetPositionX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetPositionX.HEADER + " expected");
              }
              Int32 Position;
              Int32.TryParse(Tokens[2], out Position);
              FUCNotifier.Write(String.Format("SETPOSITIONX[{0}]", Position));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseGetPositionY(CCommand command, String datagram);
    private Boolean AnalyseGetPositionY(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetPositionY CB = new CBAnalyseGetPositionY(AnalyseGetPositionY);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CGetPositionY)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CGetPositionY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetPositionY.HEADER + " expected");
              }
              Int32 Position;
              Int32.TryParse(Tokens[2], out Position);
              FUCNotifier.Write(String.Format("GETPOSITIONY[{0}]", Position));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseSetPositionY(CCommand command, String datagram);
    private Boolean AnalyseSetPositionY(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetPositionY CB = new CBAnalyseSetPositionY(AnalyseSetPositionY);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CSetPositionY)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CSetPositionY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetPositionY.HEADER + " expected");
              }
              Int32 Position;
              Int32.TryParse(Tokens[2], out Position);
              FUCNotifier.Write(String.Format("SETPOSITIONY[{0}]", Position));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - Automation
    //###########################################################################################
    //
    private delegate Boolean CBAnalyseSetMotionParameterX(CCommand command, String datagram);
    private Boolean AnalyseSetMotionParameterX(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetMotionParameterX CB = new CBAnalyseSetMotionParameterX(AnalyseSetMotionParameterX);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CSetMotionParameterX)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (4 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CSetMotionParameterX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetMotionParameterX.HEADER + " expected");
              }
              Int32 Min;
              Int32.TryParse(Tokens[2], out Min);
              Int32 Max;
              Int32.TryParse(Tokens[3], out Max);
              Int32 Delta;
              Int32.TryParse(Tokens[4], out Delta);
              FUCNotifier.Write(String.Format("SETMOTIONPARAMETERX[{0}|{1}|{2}]", Min, Max, Delta));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseSetMotionParameterY(CCommand command, String datagram);
    private Boolean AnalyseSetMotionParameterY(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetMotionParameterY CB = new CBAnalyseSetMotionParameterY(AnalyseSetMotionParameterY);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CSetMotionParameterY)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (4 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CSetMotionParameterY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetMotionParameterY.HEADER + " expected");
              }
              Int32 Min;
              Int32.TryParse(Tokens[2], out Min);
              Int32 Max;
              Int32.TryParse(Tokens[3], out Max);
              Int32 Delta;
              Int32.TryParse(Tokens[4], out Delta);
              FUCNotifier.Write(String.Format("SETMOTIONPARAMETERY[{0}|{1}|{2}]", Min, Max, Delta));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseSetDelayMotion(CCommand command, String datagram);
    private Boolean AnalyseSetDelayMotion(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetDelayMotion CB = new CBAnalyseSetDelayMotion(AnalyseSetDelayMotion);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CSetDelayMotion)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CSetDelayMotion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetDelayMotion.HEADER + " expected");
              }
              Int32 Delay;
              Int32.TryParse(Tokens[2], out Delay);
              FUCNotifier.Write(String.Format("SETDELAYMOTION[{0}]", Delay));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseSetDelayPulse(CCommand command, String datagram);
    private Boolean AnalyseSetDelayPulse(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetDelayPulse CB = new CBAnalyseSetDelayPulse(AnalyseSetDelayPulse);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CSetDelayPulse)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CSetDelayPulse.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetDelayPulse.HEADER + " expected");
              }
              Int32 Delay;
              Int32.TryParse(Tokens[2], out Delay);
              FUCNotifier.Write(String.Format("SETDELAYPULSE[{0}]", Delay));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseStartTrigger(CCommand command, String datagram);
    private Boolean AnalyseStartTrigger(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseStartTrigger CB = new CBAnalyseStartTrigger(AnalyseStartTrigger);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CStartTrigger)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CStartTrigger.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CStartTrigger.HEADER + " expected");
              }
              Int32 Pulses;
              Int32.TryParse(Tokens[2], out Pulses);
              Int32 Repetitions;
              Int32.TryParse(Tokens[3], out Repetitions);
              FUCNotifier.Write(String.Format("STARTTRIGGER[{0}|{1}]", Pulses, Repetitions));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }

    private delegate Boolean CBAnalyseAbortTrigger(CCommand command, String datagram);
    private Boolean AnalyseAbortTrigger(CCommand command, String datagram)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseAbortTrigger CB = new CBAnalyseAbortTrigger(AnalyseAbortTrigger);
        Invoke(CB, new object[] { command, datagram });
        return FResult;
      }
      else
      {
        if (command is CAbortTrigger)
        {
          String[] Tokens = datagram.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
          if (1 <= Tokens.Length)
          {
            if (":" == Tokens[0])
            {
              if (CAbortTrigger.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CAbortTrigger.HEADER + " expected");
              }
              FUCNotifier.Write(String.Format("ABORTTRIGGER[]"));
              command.SignalTextReceived();
              return true;
            }
          }
        }
        return false;
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse -
    //###########################################################################################
    //

    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //#########################################################################
    //  Segment - Callback - UdpTextDeviceClient
    //#########################################################################
    //
    private void UdpTextDeviceClientOnDatagramTransmitted(IPEndPoint ipendpointtarget,
                                                           Byte[] datagram, Int32 size)
    {
      String SDatagram = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
      String Line = String.Format("UdpTxd[{0}]:<{1}>", CNetwork.IPEndPointToText(ipendpointtarget), SDatagram);
      FUCNotifier.Write(Line);
    }

    private void UdpTextDeviceClientOnDatagramReceived(IPEndPoint ipendpointsource,
                                                        Byte[] datagram, Int32 size)
    {
      String SDatagram = System.Text.Encoding.UTF8.GetString(datagram, 0, datagram.Length);
      String Line = String.Format("UdpRxd[{0}]:<{1}>", CNetwork.IPEndPointToText(ipendpointsource), SDatagram);
      FUCNotifier.Write(Line);
      //
      if (0 < FCommandList.Count)
      {
        CCommand Command = FCommandList.Peek();
        if (Command is CCommand)
        { // Analyse DateTimePrompt:
          if (AnalyseDateTimePrompt(SDatagram)) return;
          // Analyse Help:
          if (AnalyseGetHelp(Command, SDatagram)) return;
          if (AnalyseGetProgramHeader(Command, SDatagram)) return;
          if (AnalyseGetSoftwareVersion(Command, SDatagram)) return;
          if (AnalyseGetHardwareVersion(Command, SDatagram)) return;
          // Analyse Led:
          if (AnalyseGetLedLaser(Command, SDatagram)) return;
          if (AnalyseSetLedLaserOn(Command, SDatagram)) return;
          if (AnalyseSetLedLaserOff(Command, SDatagram)) return;
          if (AnalysePulseLedLaser(Command, SDatagram)) return;
          // Analyse Dac:
          if (AnalyseGetPositionX(Command, SDatagram)) return;
          if (AnalyseSetPositionX(Command, SDatagram)) return;
          if (AnalyseGetPositionY(Command, SDatagram)) return;
          if (AnalyseSetPositionY(Command, SDatagram)) return;
          // Analyse Automation:
          if (AnalyseSetMotionParameterX(Command, SDatagram)) return;
          if (AnalyseSetMotionParameterY(Command, SDatagram)) return;
          if (AnalyseSetDelayMotion(Command, SDatagram)) return;
          if (AnalyseSetDelayPulse(Command, SDatagram)) return;
          if (AnalyseStartTrigger(Command, SDatagram)) return;
          if (AnalyseAbortTrigger(Command, SDatagram)) return;
        }
      }
    }
    //
    //#########################################################################
    //  Segment - Callback - CommandList
    //#########################################################################
    //
    private delegate void CBCommandListOnExecutionStart(RTaskData data);
    private void CommandListOnExecutionStart(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionStart CB = new CBCommandListOnExecutionStart(CommandListOnExecutionStart);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionStart", data.Name));
        CCommand Command = FCommandList.Peek();
        if (Command is CCommand)
        {
          String CommandText = Command.GetCommandText();
          FUdpTextDeviceClient.AddMessage(CommandText);
        }
      }
    }
    private delegate Boolean CBCommandListOnExecutionBusy(RTaskData data);
    private Boolean CommandListOnExecutionBusy(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionBusy CB = new CBCommandListOnExecutionBusy(CommandListOnExecutionBusy);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionBusy", data.Name));
      }
      return false;
    }
    private delegate void CBCommandListOnExecutionEnd(RTaskData data);
    private void CommandListOnExecutionEnd(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionEnd CB = new CBCommandListOnExecutionEnd(CommandListOnExecutionEnd);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionEnd", data.Name));
      }
    }
    private delegate void CBCommandListOnExecutionAbort(RTaskData data);
    private void CommandListOnExecutionAbort(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionAbort CB = new CBCommandListOnExecutionAbort(CommandListOnExecutionAbort);
        Invoke(CB, new object[] { data });
      }
      else
      {
        FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionAbort", data.Name));
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          //tbcMain.SelectedIndex = 1;
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:        
          return true;
        case 6:
          //FUdpTextDeviceClient.Preset("GSV");
          return true;
        case 7:
          return true;
        case 8:
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! First (Client -> Server)");
          return true;
        case 9:
          //FUdpTextDeviceClient.Preset("GHV");
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! Second (Client -> Server)");
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }



    private void btnGetSoftwareVersion_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetSoftwareVersion());
      FCommandList.Execute();
    }

    private void btnGetHardwareVersion_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetHardwareVersion());
      FCommandList.Execute();
    }

    private void btnGetHelp_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetHelp());
      FCommandList.Execute();
    }

    private void btnGetProgramHeader_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetProgramHeader());
      FCommandList.Execute();
    }

    private void btnGetLedLaser_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetLedLaser());
      FCommandList.Execute();
    }

    private void btnSetLedLaserOn_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CSetLedLaserOn());
      FCommandList.Execute();
    }

    private void btnSetLedLaserOff_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CSetLedLaserOff());
      FCommandList.Execute();
    }

    private void btnPulseLedLaser_Click(object sender, EventArgs e)
    {
      Int32 Period = (Int32)nudLedLaserPeriod.Value;
      Int32 Count = (Int32)nudLedLaserCount.Value;
      FCommandList.Enqueue(new CPulseLedLaser(Period, Count));
      FCommandList.Execute();
    }

    private void btnGetPositionX_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetPositionX());
      FCommandList.Execute();
    }

    private void btnSetPositionX_Click(object sender, EventArgs e)
    {
      Int32 Value = (Int32)nudPositionX.Value;
      FCommandList.Enqueue(new CSetPositionX(Value));
      FCommandList.Execute();
    }

    private void btnGetPositionY_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CGetPositionY());
      FCommandList.Execute();
    }

    private void btnSetPositionY_Click(object sender, EventArgs e)
    {
      Int32 Value = (Int32)nudPositionY.Value;
      FCommandList.Enqueue(new CSetPositionY(Value));
      FCommandList.Execute();
    }

    private void btnSetMotionParameterX_Click(object sender, EventArgs e)
    {
      Int32 Min = (Int32)nudMin.Value;
      Int32 Max = (Int32)nudMax.Value;
      Int32 Delta = (Int32)nudDelta.Value;
      FCommandList.Enqueue(new CSetMotionParameterX(Min, Max, Delta));
      FCommandList.Execute();
    }

    private void btnSetMotionParameterY_Click(object sender, EventArgs e)
    {
      Int32 Min = (Int32)nudMin.Value;
      Int32 Max = (Int32)nudMax.Value;
      Int32 Delta = (Int32)nudDelta.Value;
      FCommandList.Enqueue(new CSetMotionParameterY(Min, Max, Delta));
      FCommandList.Execute();
    }

    private void btnSetDelayMotion_Click(object sender, EventArgs e)
    {
      Int32 Delay = (Int32)nudDelayMotion.Value;
      FCommandList.Enqueue(new CSetDelayMotion(Delay));
      FCommandList.Execute();
    }

    private void btnSetDelayPulse_Click(object sender, EventArgs e)
    {
      Int32 Delay = (Int32)nudDelayPulse.Value;
      FCommandList.Enqueue(new CSetDelayPulse(Delay));
      FCommandList.Execute();
    }

    private void btnStartTrigger_Click(object sender, EventArgs e)
    {
      Int32 Pulses = (Int32)nudPulses.Value;
      Int32 Repetitions = (Int32)nudRepetitions.Value;
      FCommandList.Enqueue(new CStartTrigger(Pulses, Repetitions));
      FCommandList.Execute();
    }

    private void btnAbortTrigger_Click(object sender, EventArgs e)
    {
      FCommandList.Enqueue(new CAbortTrigger());
      FCommandList.Execute();
    }

    private void cbxLaserMatrix_CheckedChanged(object sender, EventArgs e)
    {

    }

  }
}

