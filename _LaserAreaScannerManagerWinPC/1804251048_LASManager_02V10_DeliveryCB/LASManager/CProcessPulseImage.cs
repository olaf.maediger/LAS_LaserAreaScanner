﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Task;
using CommandProtocol;
using CPLaserAreaScanner;
//
namespace LASManager
{
  public delegate void DOnPulseImageStart();
  public delegate Boolean DOnPulseImageBusy();
  public delegate void DOnPulseImageEnd();
  public delegate void DOnPulseImageAbort();

  public class CProcessPulseImage
  {
    private CProcess FProcess;
    private DOnPulseImageStart FOnPulseImageStart;
    private DOnPulseImageBusy FOnPulseImageBusy;
    private DOnPulseImageEnd FOnPulseImageEnd;
    private DOnPulseImageAbort FOnPulseImageAbort;
    //
    private UInt32 FPointIndex;
    UInt16[,] FLaserPoints;
    public CProcessPulseImage(DOnPulseImageStart onpulseimagestart,
                              DOnPulseImageBusy onpulseimagebusy,
                              DOnPulseImageEnd onpulseimageend,
                              DOnPulseImageAbort onpulseimageabort)
    {
      FOnPulseImageStart = onpulseimagestart;
      FOnPulseImageBusy = onpulseimagebusy;
      FOnPulseImageEnd = onpulseimageend;
      FOnPulseImageAbort = onpulseimageabort;
      FProcess = new CProcess("PulseImage",
                              SelfOnExecutionStart,
                              SelfOnExecutionBusy,
                              SelfOnExecutionEnd,
                              SelfOnExecutionAbort);
      FPointIndex = 0;
    }


    public void SetLaserPoints(UInt32 rowcount, UInt32 colcount, UInt16[,] laserpoints)
    {
      FLaserPoints = laserpoints;
    }

    public Boolean Start()//CCommandList commandlist)
    {
      // FCommandList = commandlist;
      if (FLaserPoints is UInt16[,])
      {        
        FPointIndex = 0;
        return FProcess.Start();
      }
      return false;
    }

    public Boolean Abort()
    {
      if (FProcess.IsActive())
      {
        return FProcess.Abort();
      }
      return false;
    }

    protected void SelfOnExecutionStart(RTaskData data)
    {
      if (FOnPulseImageStart is DOnPulseImageStart)
      {
        FOnPulseImageStart();
      }
    }
    protected Boolean SelfOnExecutionBusy(RTaskData data)
    {
      if (FOnPulseImageBusy is DOnPulseImageBusy)
      {
        return FOnPulseImageBusy();
      }
      return false;
    }
    protected void SelfOnExecutionEnd(RTaskData data) 
    {
      if (FOnPulseImageEnd is DOnPulseImageEnd)
      {
        FOnPulseImageEnd();
      }
    }
    protected void SelfOnExecutionAbort(RTaskData data)
    {
      if (FOnPulseImageAbort is DOnPulseImageAbort)
      {
        FOnPulseImageAbort();
      }
    }


  }
}
