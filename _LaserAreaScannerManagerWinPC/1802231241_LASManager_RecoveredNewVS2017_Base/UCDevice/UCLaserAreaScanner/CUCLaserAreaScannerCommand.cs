﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPLaserAreaScanner;
//
namespace UCLaserAreaScanner
{
  //
  //--------------------------------------------------------------------------
  //  Segment - Definition - UCLaserAreaScanner
  //--------------------------------------------------------------------------
  //
  public partial class CUCLaserAreaScannerCommand : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private DOnGetHelp FOnGetHelp;
    private DOnGetProgramHeader FOnGetProgramHeader;
    private DOnGetHardwareVersion FOnGetHardwareVersion;
    private DOnGetSoftwareVersion FOnGetSoftwareVersion;
    private DOnGetLedSystem FOnGetLedSystem;
    private DOnSwitchLedSystemOn FOnSwitchLedSystemOn;
    private DOnSwitchLedSystemOff FOnSwitchLedSystemOff;
    private DOnBlinkLedSystem FOnBlinkLedSystem;
    private DOnSetProcessCount FOnSetProcessCount;
    private DOnSetProcessPeriod FOnSetProcessPeriod;
    private DOnStopProcessExecution FOnStopProcessExecution;
    private DOnGetLaserAreaScannerChannel FOnGetLaserAreaScannerChannel;
    private DOnGetLaserAreaScannerInterval FOnGetLaserAreaScannerInterval;
    private DOnGetAllLaserAreaScanners FOnGetAllLaserAreaScanners;
    private DOnRepeatLaserAreaScannerChannel FOnRepeatLaserAreaScannerChannel;
    private DOnRepeatLaserAreaScannerInterval FOnRepeatLaserAreaScannerInterval;
    private DOnRepeatAllLaserAreaScanners FOnRepeatAllLaserAreaScanners;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCLaserAreaScannerCommand()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetOnGetHelp(DOnGetHelp value)
    {
      FOnGetHelp = value;
    }
    public void SetOnGetProgramHeader(DOnGetProgramHeader value)
    {
      FOnGetProgramHeader = value;
    }
    public void SetOnGetHardwareVersion(DOnGetHardwareVersion value)
    {
      FOnGetHardwareVersion = value;
    }
    public void SetHardwareVersion(String value)
    {
      tbxHardwareVersion.Text = value;
    }
    public void SetOnGetSoftwareVersion(DOnGetSoftwareVersion value)
    {
      FOnGetSoftwareVersion = value;
    }
    public void SetSoftwareVersion(String value)
    {
      tbxSoftwareVersion.Text = value;
    }
    public void SetOnGetLedSystem(DOnGetLedSystem value)
    {
      FOnGetLedSystem = value;
    }
    public void SetOnSwitchLedSystemOn(DOnSwitchLedSystemOn value)
    {
      FOnSwitchLedSystemOn = value;
    }
    public void SetOnSwitchLedSystemOff(DOnSwitchLedSystemOff value)
    {
      FOnSwitchLedSystemOff = value;
    }
    public void SetOnBlinkLedSystem(DOnBlinkLedSystem value)
    {
      FOnBlinkLedSystem = value;
    }
    public void SetOnSetProcessCount(DOnSetProcessCount value)
    {
      FOnSetProcessCount = value;
    }
    public void SetOnSetProcessPeriod(DOnSetProcessPeriod value)
    {
      FOnSetProcessPeriod = value;
    }
    public void SetOnStopProcessExecution(DOnStopProcessExecution value)
    {
      FOnStopProcessExecution = value;
    }
    public void SetOnGetLaserAreaScannerChannel(DOnGetLaserAreaScannerChannel value)
    {
      FOnGetLaserAreaScannerChannel = value;
    }
    public void SetOnGetLaserAreaScannerInterval(DOnGetLaserAreaScannerInterval value)
    {
      FOnGetLaserAreaScannerInterval = value;
    }
    public void SetOnGetAllLaserAreaScanners(DOnGetAllLaserAreaScanners value)
    {
      FOnGetAllLaserAreaScanners = value;
    }
    public void SetOnRepeatLaserAreaScannerChannel(DOnRepeatLaserAreaScannerChannel value)
    {
      FOnRepeatLaserAreaScannerChannel = value;
    }
    public void SetOnRepeatLaserAreaScannerInterval(DOnRepeatLaserAreaScannerInterval value)
    {
      FOnRepeatLaserAreaScannerInterval = value;
    }
    public void SetOnRepeatAllLaserAreaScanners(DOnRepeatAllLaserAreaScanners value)
    {
      FOnRepeatAllLaserAreaScanners = value;
    }

    public void SetState(String[] tokens)
    {
      Int32 TL = tokens.Length;
      if (4 <= TL)
      {
        if ((":" == tokens[0])) //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! && (CGetStateMultiLaserAreaScannerController.HEADER == tokens[1]))
        {
          Int32 SMTC;
          Int32.TryParse(tokens[2], out SMTC);
          Int32 SubState;
          Int32.TryParse(tokens[3], out SubState);
          lblStateMTCIndex.Text = String.Format("{0} | {1}", SMTC, SubState);
          switch ((EStateProcess)SMTC)
          { // Common
            case EStateProcess.Idle:
              lblStateMTControllerText.Text = "Idle";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            case EStateProcess.Welcome:
              lblStateMTControllerText.Text = "Welcome";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            case EStateProcess.GetHelp:
              lblStateMTControllerText.Text = "GetHelp";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetProgramHeader:
              lblStateMTControllerText.Text = "GetProgramHeader";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetSoftwareVersion:
              lblStateMTControllerText.Text = "GetSoftwareVersion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetHardwareVersion:
              lblStateMTControllerText.Text = "GetHardwareVersion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetProcessCount:
              lblStateMTControllerText.Text = "SetProcessCount";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetProcessPeriod:
              lblStateMTControllerText.Text = "SetProcessPeriod";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.StopProcessExecution:
              lblStateMTControllerText.Text = "StopProcessExecution";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            // Measurement - LedSystem
            case EStateProcess.GetLedSystem:
              lblStateMTControllerText.Text = "GetLedSystem";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.LedSystemOn:
              lblStateMTControllerText.Text = "LedSystemOn";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.LedSystemOff:
              lblStateMTControllerText.Text = "LedSystemOff";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.BlinkLedSystem:
              lblStateMTControllerText.Text = "BlinkLedSystem";
              lblStateMTControllerText.BackColor = Color.LightPink;
              SetStateLedSystem(SubState);
              break;
            //// Measurement - LaserAreaScanner
            //case EStateProcess.GetLaserAreaScannerChannel:
            //  lblStateMTControllerText.Text = "GetLaserAreaScannerChannel";
            //  lblStateMTControllerText.BackColor = Color.LightPink;
            //  break;
            //case EStateProcess.GetLaserAreaScannerInterval:
            //  lblStateMTControllerText.Text = "GetLaserAreaScannerInterval";
            //  lblStateMTControllerText.BackColor = Color.LightPink;
            //  break;
            //case EStateProcess.GetAllLaserAreaScanners:
            //  lblStateMTControllerText.Text = "GetAllLaserAreaScanners";
            //  lblStateMTControllerText.BackColor = Color.LightPink;
            //  break;
            //case EStateProcess.RepeatLaserAreaScannerChannel:
            //  lblStateMTControllerText.Text = "RepeatLaserAreaScannerChannel";
            //  lblStateMTControllerText.BackColor = Color.LightPink;
            //  break;
            //case EStateProcess.RepeatLaserAreaScannerInterval:
            //  lblStateMTControllerText.Text = "RepeatLaserAreaScannerInterval";
            //  lblStateMTControllerText.BackColor = Color.LightPink;
            //  break;
            //case EStateProcess.RepeatAllLaserAreaScanners:
            //  lblStateMTControllerText.Text = "RepeatAllLaserAreaScanners";
            //  lblStateMTControllerText.BackColor = Color.LightPink;
            //  break;
            //default: // saUndefined
            //  lblStateMTControllerText.Text = "???Undefined???";
            //  lblStateMTControllerText.BackColor = Color.Red;
            //  break;
          }
        }
      }
      else
      {
        lblStateMTCIndex.Text = "???";
      }
    }

    public void SetProcessCount(Int32 value)
    {
      nudProcessCount.Value = value;
    }

    public void SetProcessPeriod(Int32 value)
    {
      nudProcessPeriod.Value = value;
    }

    public void SetStateLedSystem(Int32 state)
    {
      switch (state)
      {
        case 0: // EStateLed.Off:
          pnlStateLedSystem.BackColor = Color.MediumBlue;
          break;
        case 1: //EStateLed.On:
          pnlStateLedSystem.BackColor = Color.GreenYellow;
          break;
        default: // EStateLed.Undefined:
          pnlStateLedSystem.BackColor = Color.Fuchsia;
          break;
      }
    }

    public void SetChannelLow(Int32 value)
    {
      nudChannelLow.Value = value;
    }

    public void SetChannelHigh(Int32 value)
    {
      nudChannelHigh.Value = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public void ClearMessages()
    {
      tbxMessages.Text = "";
    }

    public void AddMessageLine(String line)
    {
      tbxMessages.Text += line;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //
    private void btnGetHelp_Click(object sender, EventArgs e)
    {
      if (FOnGetHelp is DOnGetHelp)
      {
        FOnGetHelp();
      }
    }

    private void btnGetProgramHeader_Click(object sender, EventArgs e)
    {
      if (FOnGetProgramHeader is DOnGetProgramHeader)
      {
        FOnGetProgramHeader();
      }
    }

    private void btnGetHardwareVersion_Click(object sender, EventArgs e)
    {
      if (FOnGetHardwareVersion is DOnGetHardwareVersion)
      {
        FOnGetHardwareVersion();
      }
    }

    private void btnGetSoftwareVersion_Click(object sender, EventArgs e)
    {
      if (FOnGetSoftwareVersion is DOnGetSoftwareVersion)
      {
        FOnGetSoftwareVersion();
      }
    }

    private void btnGetLedSystem_Click(object sender, EventArgs e)
    {
      if (FOnGetLedSystem is DOnGetLedSystem)
      {
        FOnGetLedSystem();
      }
    }

    private void btnSwitchLedSystemOn_Click(object sender, EventArgs e)
    {
      if (FOnSwitchLedSystemOn is DOnSwitchLedSystemOn)
      {
        FOnSwitchLedSystemOn();
      }

    }

    private void btnSwitchLedSystemOff_Click(object sender, EventArgs e)
    {
      if (FOnSwitchLedSystemOff is DOnSwitchLedSystemOff)
      {
        FOnSwitchLedSystemOff();
      }

    }

    private void btnBlinkLedSystem_Click(object sender, EventArgs e)
    {
      if (FOnBlinkLedSystem is DOnBlinkLedSystem)
      {
        Int32 Period = (Int32)nudProcessPeriod.Value;
        Int32 Count = (Int32)nudProcessCount.Value;
        FOnBlinkLedSystem(Period, Count);
      }

    }

    private void btnSetProcessCount_Click(object sender, EventArgs e)
    {
      if (FOnSetProcessCount is DOnSetProcessCount)
      {
        Int32 Count = (Int32)nudProcessCount.Value;
        FOnSetProcessCount(Count);
      }

    }

    private void btnSetProcessPeriod_Click(object sender, EventArgs e)
    {
      if (FOnSetProcessPeriod is DOnSetProcessPeriod)
      {
        Int32 Period = (Int32)nudProcessPeriod.Value;
        FOnSetProcessPeriod(Period);
      }

    }

    private void btnStopProcessExecution_Click(object sender, EventArgs e)
    {
      if (FOnStopProcessExecution is DOnStopProcessExecution)
      {
        FOnStopProcessExecution();
      }

    }

    private void btnGetLaserAreaScannerChannel_Click(object sender, EventArgs e)
    {
      if (FOnGetLaserAreaScannerChannel is DOnGetLaserAreaScannerChannel)
      {
        Int32 Channel = (Int32)nudChannelLow.Value;
        FOnGetLaserAreaScannerChannel(Channel);
      }

    }

    private void btnGetLaserAreaScannerInterval_Click(object sender, EventArgs e)
    {
      if (FOnGetLaserAreaScannerInterval is DOnGetLaserAreaScannerInterval)
      {
        Int32 ChannelLow = (Int32)nudChannelLow.Value;
        Int32 ChannelHigh = (Int32)nudChannelHigh.Value;
        FOnGetLaserAreaScannerInterval(ChannelLow, ChannelHigh);
      }

    }

    private void btnGetAllLaserAreaScanners_Click(object sender, EventArgs e)
    {
      if (FOnGetAllLaserAreaScanners is DOnGetAllLaserAreaScanners)
      {
        FOnGetAllLaserAreaScanners();
      }

    }

    private void btnRepeatLaserAreaScannerChannel_Click(object sender, EventArgs e)
    {
      if (FOnRepeatLaserAreaScannerChannel is DOnRepeatLaserAreaScannerChannel)
      {
        Int32 Channel = (Int32)nudChannelLow.Value;
        FOnRepeatLaserAreaScannerChannel(Channel);
      }

    }

    private void btnRepeatLaserAreaScannerInterval_Click(object sender, EventArgs e)
    {
      if (FOnRepeatLaserAreaScannerInterval is DOnRepeatLaserAreaScannerInterval)
      {
        Int32 ChannelLow = (Int32)nudChannelLow.Value;
        Int32 ChannelHigh = (Int32)nudChannelHigh.Value;
        FOnRepeatLaserAreaScannerInterval(ChannelLow, ChannelHigh);
      }

    }

    private void btnRepeatAllLaserAreaScanners_Click(object sender, EventArgs e)
    {
      if (FOnRepeatAllLaserAreaScanners is DOnRepeatAllLaserAreaScanners)
      {
        FOnRepeatAllLaserAreaScanners();
      }

    }


  }
}
