﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPLaserAreaScanner;
//
namespace UCLaserAreaScanner
{
  //
  //--------------------------------------------------------------------------
  //  Segment - Definition - UCLaserAreaScannerDisplay
  //--------------------------------------------------------------------------
  //
  public partial class CUCLaserAreaScannerDisplay : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCLaserAreaScannerDisplay()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetLaserAreaScannerValues(Int32 channellow, Int32 channelhigh, Double[] temperatures)
    {
      FUCLaserAreaScannerSegment0.SetValue(temperatures[0]);
      FUCLaserAreaScannerSegment1.SetValue(temperatures[1]);
      FUCLaserAreaScannerSegment2.SetValue(temperatures[2]);
      FUCLaserAreaScannerSegment3.SetValue(temperatures[3]);
      FUCLaserAreaScannerSegment4.SetValue(temperatures[4]);
      FUCLaserAreaScannerSegment5.SetValue(temperatures[5]);
      FUCLaserAreaScannerSegment6.SetValue(temperatures[6]);
      FUCLaserAreaScannerSegment7.SetValue(temperatures[7]);
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
 
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //

  }
}
