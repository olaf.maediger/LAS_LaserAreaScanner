﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using CommandProtocol;
//
namespace CPLaserAreaScanner
{
  public class CPulseLaserMatrix : CCommand
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const String HEADER = CDefine.SHORT_PLM;
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private Int32 FPeriod;
    private Int32 FPulses;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CPulseLaserMatrix(Int32 period, Int32 pulses)
      : base(HEADER)
    {
      FPeriod = period;
      FPulses = pulses;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public override string GetCommandHeader()
    {
      return FHeader;
    }

    public override string GetCommandText()
    {
      return String.Format("{0} {1} {2}", FHeader, FPeriod, FPulses);
    }


  }
}
