﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserAreaScannerDisplay
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pbxMatrix = new System.Windows.Forms.PictureBox();
      this.lblHeader = new System.Windows.Forms.Label();
      this.FUCLaserAreaScannerSegment7 = new UCLaserAreaScanner.CUCLaserAreaScannerSegment();
      this.FUCLaserAreaScannerSegment6 = new UCLaserAreaScanner.CUCLaserAreaScannerSegment();
      this.FUCLaserAreaScannerSegment5 = new UCLaserAreaScanner.CUCLaserAreaScannerSegment();
      this.FUCLaserAreaScannerSegment4 = new UCLaserAreaScanner.CUCLaserAreaScannerSegment();
      this.FUCLaserAreaScannerSegment3 = new UCLaserAreaScanner.CUCLaserAreaScannerSegment();
      this.FUCLaserAreaScannerSegment2 = new UCLaserAreaScanner.CUCLaserAreaScannerSegment();
      this.FUCLaserAreaScannerSegment1 = new UCLaserAreaScanner.CUCLaserAreaScannerSegment();
      this.FUCLaserAreaScannerSegment0 = new UCLaserAreaScanner.CUCLaserAreaScannerSegment();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix)).BeginInit();
      this.SuspendLayout();
      // 
      // pbxMatrix
      // 
      this.pbxMatrix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxMatrix.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxMatrix.Location = new System.Drawing.Point(0, 0);
      this.pbxMatrix.Name = "pbxMatrix";
      this.pbxMatrix.Size = new System.Drawing.Size(742, 384);
      this.pbxMatrix.TabIndex = 0;
      this.pbxMatrix.TabStop = false;
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(742, 23);
      this.lblHeader.TabIndex = 1;
      this.lblHeader.Text = "LaserAreaScanner Display";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FUCLaserAreaScannerSegment7
      // 
      this.FUCLaserAreaScannerSegment7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCLaserAreaScannerSegment7.Header = "T7";
      this.FUCLaserAreaScannerSegment7.Location = new System.Drawing.Point(376, 296);
      this.FUCLaserAreaScannerSegment7.Name = "FUCLaserAreaScannerSegment7";
      this.FUCLaserAreaScannerSegment7.Size = new System.Drawing.Size(354, 76);
      this.FUCLaserAreaScannerSegment7.TabIndex = 111;
      this.FUCLaserAreaScannerSegment7.Unit = "°C";
      // 
      // FUCLaserAreaScannerSegment6
      // 
      this.FUCLaserAreaScannerSegment6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCLaserAreaScannerSegment6.Header = "T6";
      this.FUCLaserAreaScannerSegment6.Location = new System.Drawing.Point(12, 296);
      this.FUCLaserAreaScannerSegment6.Name = "FUCLaserAreaScannerSegment6";
      this.FUCLaserAreaScannerSegment6.Size = new System.Drawing.Size(354, 76);
      this.FUCLaserAreaScannerSegment6.TabIndex = 110;
      this.FUCLaserAreaScannerSegment6.Unit = "°C";
      // 
      // FUCLaserAreaScannerSegment5
      // 
      this.FUCLaserAreaScannerSegment5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCLaserAreaScannerSegment5.Header = "T5";
      this.FUCLaserAreaScannerSegment5.Location = new System.Drawing.Point(376, 209);
      this.FUCLaserAreaScannerSegment5.Name = "FUCLaserAreaScannerSegment5";
      this.FUCLaserAreaScannerSegment5.Size = new System.Drawing.Size(354, 76);
      this.FUCLaserAreaScannerSegment5.TabIndex = 109;
      this.FUCLaserAreaScannerSegment5.Unit = "°C";
      // 
      // FUCLaserAreaScannerSegment4
      // 
      this.FUCLaserAreaScannerSegment4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCLaserAreaScannerSegment4.Header = "T4";
      this.FUCLaserAreaScannerSegment4.Location = new System.Drawing.Point(12, 209);
      this.FUCLaserAreaScannerSegment4.Name = "FUCLaserAreaScannerSegment4";
      this.FUCLaserAreaScannerSegment4.Size = new System.Drawing.Size(354, 76);
      this.FUCLaserAreaScannerSegment4.TabIndex = 108;
      this.FUCLaserAreaScannerSegment4.Unit = "°C";
      // 
      // FUCLaserAreaScannerSegment3
      // 
      this.FUCLaserAreaScannerSegment3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCLaserAreaScannerSegment3.Header = "T3";
      this.FUCLaserAreaScannerSegment3.Location = new System.Drawing.Point(376, 122);
      this.FUCLaserAreaScannerSegment3.Name = "FUCLaserAreaScannerSegment3";
      this.FUCLaserAreaScannerSegment3.Size = new System.Drawing.Size(354, 76);
      this.FUCLaserAreaScannerSegment3.TabIndex = 107;
      this.FUCLaserAreaScannerSegment3.Unit = "°C";
      // 
      // FUCLaserAreaScannerSegment2
      // 
      this.FUCLaserAreaScannerSegment2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCLaserAreaScannerSegment2.Header = "T2";
      this.FUCLaserAreaScannerSegment2.Location = new System.Drawing.Point(12, 122);
      this.FUCLaserAreaScannerSegment2.Name = "FUCLaserAreaScannerSegment2";
      this.FUCLaserAreaScannerSegment2.Size = new System.Drawing.Size(354, 76);
      this.FUCLaserAreaScannerSegment2.TabIndex = 106;
      this.FUCLaserAreaScannerSegment2.Unit = "°C";
      // 
      // FUCLaserAreaScannerSegment1
      // 
      this.FUCLaserAreaScannerSegment1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCLaserAreaScannerSegment1.Header = "T1";
      this.FUCLaserAreaScannerSegment1.Location = new System.Drawing.Point(376, 34);
      this.FUCLaserAreaScannerSegment1.Name = "FUCLaserAreaScannerSegment1";
      this.FUCLaserAreaScannerSegment1.Size = new System.Drawing.Size(354, 76);
      this.FUCLaserAreaScannerSegment1.TabIndex = 105;
      this.FUCLaserAreaScannerSegment1.Unit = "°C";
      // 
      // FUCLaserAreaScannerSegment0
      // 
      this.FUCLaserAreaScannerSegment0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCLaserAreaScannerSegment0.Header = "T0";
      this.FUCLaserAreaScannerSegment0.Location = new System.Drawing.Point(12, 34);
      this.FUCLaserAreaScannerSegment0.Name = "FUCLaserAreaScannerSegment0";
      this.FUCLaserAreaScannerSegment0.Size = new System.Drawing.Size(354, 76);
      this.FUCLaserAreaScannerSegment0.TabIndex = 104;
      this.FUCLaserAreaScannerSegment0.Unit = "°C";
      // 
      // CUCLaserAreaScannerDisplay
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCLaserAreaScannerSegment7);
      this.Controls.Add(this.FUCLaserAreaScannerSegment6);
      this.Controls.Add(this.FUCLaserAreaScannerSegment5);
      this.Controls.Add(this.FUCLaserAreaScannerSegment4);
      this.Controls.Add(this.FUCLaserAreaScannerSegment3);
      this.Controls.Add(this.FUCLaserAreaScannerSegment2);
      this.Controls.Add(this.FUCLaserAreaScannerSegment1);
      this.Controls.Add(this.FUCLaserAreaScannerSegment0);
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.pbxMatrix);
      this.Name = "CUCLaserAreaScannerDisplay";
      this.Size = new System.Drawing.Size(742, 384);
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox pbxMatrix;
    private System.Windows.Forms.Label lblHeader;
    private CUCLaserAreaScannerSegment FUCLaserAreaScannerSegment0;
    private CUCLaserAreaScannerSegment FUCLaserAreaScannerSegment1;
    private CUCLaserAreaScannerSegment FUCLaserAreaScannerSegment3;
    private CUCLaserAreaScannerSegment FUCLaserAreaScannerSegment2;
    private CUCLaserAreaScannerSegment FUCLaserAreaScannerSegment5;
    private CUCLaserAreaScannerSegment FUCLaserAreaScannerSegment4;
    private CUCLaserAreaScannerSegment FUCLaserAreaScannerSegment7;
    private CUCLaserAreaScannerSegment FUCLaserAreaScannerSegment6;
  }
}
