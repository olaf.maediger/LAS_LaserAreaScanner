﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPLaserAreaScanner;
//
namespace UCLaserAreaScanner
{
  //
  //--------------------------------------------------------------------------
  //  Segment - Definition - UCLaserAreaScanner
  //--------------------------------------------------------------------------
  //
  public partial class CUCLaserAreaScannerCommand : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    // Common
    private DOnGetHelp FOnGetHelp;
    private DOnGetProgramHeader FOnGetProgramHeader;
    private DOnGetHardwareVersion FOnGetHardwareVersion;
    private DOnGetSoftwareVersion FOnGetSoftwareVersion;
    private DOnGetProcessCount FOnGetProcessCount;
    private DOnSetProcessCount FOnSetProcessCount;
    private DOnGetProcessPeriod FOnGetProcessPeriod;
    private DOnSetProcessPeriod FOnSetProcessPeriod;
    private DOnStopProcessExecution FOnStopProcessExecution;
    // LedSystem
    private DOnGetLedSystem FOnGetLedSystem;
    private DOnSetLedSystemLow FOnSetLedSystemLow;
    private DOnSetLedSystemHigh FOnSetLedSystemHigh;
    private DOnBlinkLedSystem FOnBlinkLedSystem;
    // ...
    //private DOnGetLaserAreaScannerChannel FOnGetLaserAreaScannerChannel;
    //private DOnGetLaserAreaScannerInterval FOnGetLaserAreaScannerInterval;
    //private DOnGetAllLaserAreaScanners FOnGetAllLaserAreaScanners;
    //private DOnRepeatLaserAreaScannerChannel FOnRepeatLaserAreaScannerChannel;
    //private DOnRepeatLaserAreaScannerInterval FOnRepeatLaserAreaScannerInterval;
    //private DOnRepeatAllLaserAreaScanners FOnRepeatAllLaserAreaScanners;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCLaserAreaScannerCommand()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetOnGetHelp(DOnGetHelp value)
    {
      FOnGetHelp = value;
    }
    public void SetOnGetProgramHeader(DOnGetProgramHeader value)
    {
      FOnGetProgramHeader = value;
    }
    public void SetOnGetHardwareVersion(DOnGetHardwareVersion value)
    {
      FOnGetHardwareVersion = value;
    }
    public void SetHardwareVersion(String value)
    {
      tbxHardwareVersion.Text = value;
    }
    public void SetOnGetSoftwareVersion(DOnGetSoftwareVersion value)
    {
      FOnGetSoftwareVersion = value;
    }
    public void SetSoftwareVersion(String value)
    {
      tbxSoftwareVersion.Text = value;
    }
    public void SetOnGetProcessCount(DOnGetProcessCount value)
    {
      FOnGetProcessCount = value;
    }
    public void SetOnSetProcessCount(DOnSetProcessCount value)
    {
      FOnSetProcessCount = value;
    }
    public void SetOnGetProcessPeriod(DOnGetProcessPeriod value)
    {
      FOnGetProcessPeriod = value;
    }
    public void SetOnSetProcessPeriod(DOnSetProcessPeriod value)
    {
      FOnSetProcessPeriod = value;
    }
    public void SetOnStopProcessExecution(DOnStopProcessExecution value)
    {
      FOnStopProcessExecution = value;
    }
    public void SetOnGetLedSystem(DOnGetLedSystem value)
    {
      FOnGetLedSystem = value;
    }
    public void SetOnSetLedSystemLow(DOnSetLedSystemLow value)
    {
      FOnSetLedSystemLow = value;
    }
    public void SetOnSetLedSystemHigh(DOnSetLedSystemHigh value)
    {
      FOnSetLedSystemHigh = value;
    }
    public void SetOnBlinkLedSystem(DOnBlinkLedSystem value)
    {
      FOnBlinkLedSystem = value;
    }







    //public void SetOnGetLaserAreaScannerInterval(DOnGetLaserAreaScannerInterval value)
    //{
    //  FOnGetLaserAreaScannerInterval = value;
    //}
    //public void SetOnGetAllLaserAreaScanners(DOnGetAllLaserAreaScanners value)
    //{
    //  FOnGetAllLaserAreaScanners = value;
    //}
    //public void SetOnRepeatLaserAreaScannerChannel(DOnRepeatLaserAreaScannerChannel value)
    //{
    //  FOnRepeatLaserAreaScannerChannel = value;
    //}
    //public void SetOnRepeatLaserAreaScannerInterval(DOnRepeatLaserAreaScannerInterval value)
    //{
    //  FOnRepeatLaserAreaScannerInterval = value;
    //}
    //public void SetOnRepeatAllLaserAreaScanners(DOnRepeatAllLaserAreaScanners value)
    //{
    //  FOnRepeatAllLaserAreaScanners = value;
    //}

    public void SetState(String[] tokens)
    {
      Int32 TL = tokens.Length;
      if (4 <= TL)
      {
        if (CDefine.PROMPT_RESPONSE == tokens[0])
        {
          Int32 SMTC;
          Int32.TryParse(tokens[2], out SMTC);
          Int32 SubState;
          Int32.TryParse(tokens[3], out SubState);
          lblStateMTCIndex.Text = String.Format("{0} | {1}", SMTC, SubState);
          switch ((EStateProcess)SMTC)
          { // Common
            case EStateProcess.Idle:
              lblStateMTControllerText.Text = "Idle";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            case EStateProcess.Welcome:
              lblStateMTControllerText.Text = "Welcome";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            case EStateProcess.GetHelp:
              lblStateMTControllerText.Text = "GetHelp";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetProgramHeader:
              lblStateMTControllerText.Text = "GetProgramHeader";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetSoftwareVersion:
              lblStateMTControllerText.Text = "GetSoftwareVersion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetHardwareVersion:
              lblStateMTControllerText.Text = "GetHardwareVersion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetProcessCount:
              lblStateMTControllerText.Text = "GetProcessCount";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetProcessCount:
              lblStateMTControllerText.Text = "SetProcessCount";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetProcessPeriod:
              lblStateMTControllerText.Text = "SetProcessPeriod";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetProcessPeriod:
              lblStateMTControllerText.Text = "SetProcessPeriod";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.StopProcessExecution:
              lblStateMTControllerText.Text = "StopProcessExecution";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            // Measurement - LedSystem
            case EStateProcess.GetLedSystem:
              lblStateMTControllerText.Text = "GetLedSystem";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.LedSystemOn:
              lblStateMTControllerText.Text = "LedSystemOn";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.LedSystemOff:
              lblStateMTControllerText.Text = "LedSystemOff";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.BlinkLedSystem:
              lblStateMTControllerText.Text = "BlinkLedSystem";
              lblStateMTControllerText.BackColor = Color.LightPink;
              SetStateLedSystem(SubState);
              break;
            // LaserPosition
            case EStateProcess.PulseLaserPosition:
              lblStateMTControllerText.Text = "PulseLaserPosition";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.AbortLaserPosition:
              lblStateMTControllerText.Text = "AbortLaserPosition";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            // LaserRange
            case EStateProcess.GetPositionX:
              lblStateMTControllerText.Text = "GetPositionX";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetRangeX:
              lblStateMTControllerText.Text = "GetRangeX";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetRangeX:
              lblStateMTControllerText.Text = "SetRangeX";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetPositionY:
              lblStateMTControllerText.Text = "GetPositionY";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetRangeY:
              lblStateMTControllerText.Text = "GetRangeY";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetRangeY:
              lblStateMTControllerText.Text = "SetRangeY";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetDelayMotion:
              lblStateMTControllerText.Text = "GetDelayMotion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetDelayMotion:
              lblStateMTControllerText.Text = "SetDelayMotion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            // LaserMatrix
            case EStateProcess.PulseLaserMatrix:
              lblStateMTControllerText.Text = "PulseLaserMatrix";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.AbortLaserMatrix:
              lblStateMTControllerText.Text = "AbortLaserMatrix";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            // LaserImage
            case EStateProcess.PulseLaserImage:
              lblStateMTControllerText.Text = "PulseLaserImage";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.AbortLaserImage:
              lblStateMTControllerText.Text = "AbortLaserImage";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            default: // saUndefined
              lblStateMTControllerText.Text = "???Undefined???";
              lblStateMTControllerText.BackColor = Color.Red;
              break;
          }
        }
      }
      else
      {
        lblStateMTCIndex.Text = "???";
      }
    }

    public void SetProcessCount(Int32 value)
    {
      nudProcessCount.Value = value;
    }

    public void SetProcessPeriod(Int32 value)
    {
      nudProcessPeriod.Value = value;
    }

    public void SetStateLedSystem(Int32 state)
    {
      switch (state)
      {
        case 0: // EStateLed.Off:
          pnlStateLedSystem.BackColor = Color.MediumBlue;
          break;
        case 1: //EStateLed.On:
          pnlStateLedSystem.BackColor = Color.GreenYellow;
          break;
        default: // EStateLed.Undefined:
          pnlStateLedSystem.BackColor = Color.Fuchsia;
          break;
      }
    }

    public void SetChannelLow(Int32 value)
    {
      nudChannelLow.Value = value;
    }

    public void SetChannelHigh(Int32 value)
    {
      nudChannelHigh.Value = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public void ClearMessages()
    {
      tbxMessages.Text = "";
    }

    public void AddMessageLine(String line)
    {
      tbxMessages.Text += line;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //
    private void btnGetHelp_Click(object sender, EventArgs e)
    {
      if (FOnGetHelp is DOnGetHelp)
      {
        FOnGetHelp();
      }
    }

    private void btnGetProgramHeader_Click(object sender, EventArgs e)
    {
      if (FOnGetProgramHeader is DOnGetProgramHeader)
      {
        FOnGetProgramHeader();
      }
    }

    private void btnGetHardwareVersion_Click(object sender, EventArgs e)
    {
      if (FOnGetHardwareVersion is DOnGetHardwareVersion)
      {
        FOnGetHardwareVersion();
      }
    }

    private void btnGetSoftwareVersion_Click(object sender, EventArgs e)
    {
      if (FOnGetSoftwareVersion is DOnGetSoftwareVersion)
      {
        FOnGetSoftwareVersion();
      }
    }

    private void btnGetProcessCount_Click(object sender, EventArgs e)
    {
      if (FOnGetProcessCount is DOnGetProcessCount)
      {
        FOnGetProcessCount();
      }
    }

    private void btnSetProcessCount_Click(object sender, EventArgs e)
    {
      if (FOnSetProcessCount is DOnSetProcessCount)
      {
        Int32 Count = (Int32)nudProcessCount.Value;
        FOnSetProcessCount(Count);
      }
    }

    private void btnGetProcessPeriod_Click(object sender, EventArgs e)
    {
      if (FOnGetProcessPeriod is DOnGetProcessPeriod)
      {
        FOnGetProcessPeriod();
      }
    }

    private void btnSetProcessPeriod_Click(object sender, EventArgs e)
    {
      if (FOnSetProcessPeriod is DOnSetProcessPeriod)
      {
        Int32 Period = (Int32)nudProcessPeriod.Value;
        FOnSetProcessPeriod(Period);
      }
    }

    private void btnStopProcessExecution_Click(object sender, EventArgs e)
    {
      if (FOnStopProcessExecution is DOnStopProcessExecution)
      {
        FOnStopProcessExecution();
      }
    }

    private void btnGetLedSystem_Click(object sender, EventArgs e)
    {
      if (FOnGetLedSystem is DOnGetLedSystem)
      {
        FOnGetLedSystem();
      }
    }

    private void btnSetLedSystemLow_Click(object sender, EventArgs e)
    {
      if (FOnSetLedSystemLow is DOnSetLedSystemLow)
      {
        FOnSetLedSystemLow();
      }
    }

    private void btnSetLedSystemHigh_Click(object sender, EventArgs e)
    {
      if (FOnSetLedSystemHigh is DOnSetLedSystemHigh)
      {
        FOnSetLedSystemHigh();
      }
    }

    private void btnBlinkLedSystem_Click(object sender, EventArgs e)
    {
      if (FOnBlinkLedSystem is DOnBlinkLedSystem)
      {
        Int32 Period = (Int32)nudProcessPeriod.Value;
        Int32 Count = (Int32)nudProcessCount.Value;
        FOnBlinkLedSystem(Period, Count);
      }
    }






    //private void btnGetLaserAreaScannerChannel_Click(object sender, EventArgs e)
    //{
    //  if (FOnGetLaserAreaScannerChannel is DOnGetLaserAreaScannerChannel)
    //  {
    //    Int32 Channel = (Int32)nudChannelLow.Value;
    //    FOnGetLaserAreaScannerChannel(Channel);
    //  }

    //}

    //private void btnGetLaserAreaScannerInterval_Click(object sender, EventArgs e)
    //{
    //  if (FOnGetLaserAreaScannerInterval is DOnGetLaserAreaScannerInterval)
    //  {
    //    Int32 ChannelLow = (Int32)nudChannelLow.Value;
    //    Int32 ChannelHigh = (Int32)nudChannelHigh.Value;
    //    FOnGetLaserAreaScannerInterval(ChannelLow, ChannelHigh);
    //  }

    //}

    //private void btnGetAllLaserAreaScanners_Click(object sender, EventArgs e)
    //{
    //  if (FOnGetAllLaserAreaScanners is DOnGetAllLaserAreaScanners)
    //  {
    //    FOnGetAllLaserAreaScanners();
    //  }

    //}

    //private void btnRepeatLaserAreaScannerChannel_Click(object sender, EventArgs e)
    //{
    //  if (FOnRepeatLaserAreaScannerChannel is DOnRepeatLaserAreaScannerChannel)
    //  {
    //    Int32 Channel = (Int32)nudChannelLow.Value;
    //    FOnRepeatLaserAreaScannerChannel(Channel);
    //  }

    //}

    //private void btnRepeatLaserAreaScannerInterval_Click(object sender, EventArgs e)
    //{
    //  if (FOnRepeatLaserAreaScannerInterval is DOnRepeatLaserAreaScannerInterval)
    //  {
    //    Int32 ChannelLow = (Int32)nudChannelLow.Value;
    //    Int32 ChannelHigh = (Int32)nudChannelHigh.Value;
    //    FOnRepeatLaserAreaScannerInterval(ChannelLow, ChannelHigh);
    //  }

    //}

    //private void btnRepeatAllLaserAreaScanners_Click(object sender, EventArgs e)
    //{
    //  if (FOnRepeatAllLaserAreaScanners is DOnRepeatAllLaserAreaScanners)
    //  {
    //    FOnRepeatAllLaserAreaScanners();
    //  }

    //}

  }
}
