﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserAreaScannerCommand
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pbxButton = new System.Windows.Forms.PictureBox();
      this.pnlState = new System.Windows.Forms.Panel();
      this.label9 = new System.Windows.Forms.Label();
      this.lblStateMTCIndex = new System.Windows.Forms.Label();
      this.lblStateMTControllerText = new System.Windows.Forms.Label();
      this.lblHeaderState = new System.Windows.Forms.Label();
      this.btnGetLedSystem = new System.Windows.Forms.Button();
      this.tbxMessages = new System.Windows.Forms.TextBox();
      this.tbxHardwareVersion = new System.Windows.Forms.TextBox();
      this.tbxSoftwareVersion = new System.Windows.Forms.TextBox();
      this.btnGetHelp = new System.Windows.Forms.Button();
      this.btnSwitchLedSystemOn = new System.Windows.Forms.Button();
      this.btnGetHardwareVersion = new System.Windows.Forms.Button();
      this.btnGetSoftwareVersion = new System.Windows.Forms.Button();
      this.btnGetProgramHeader = new System.Windows.Forms.Button();
      this.btnStopProcessExecution = new System.Windows.Forms.Button();
      this.btnSwitchLedSystemOff = new System.Windows.Forms.Button();
      this.btnBlinkLedSystem = new System.Windows.Forms.Button();
      this.btnGetLaserAreaScannerChannel = new System.Windows.Forms.Button();
      this.btnGetLaserAreaScannerInterval = new System.Windows.Forms.Button();
      this.btnGetAllLaserAreaScanners = new System.Windows.Forms.Button();
      this.label6 = new System.Windows.Forms.Label();
      this.nudChannelHigh = new System.Windows.Forms.NumericUpDown();
      this.label7 = new System.Windows.Forms.Label();
      this.nudChannelLow = new System.Windows.Forms.NumericUpDown();
      this.btnRepeatAllLaserAreaScanners = new System.Windows.Forms.Button();
      this.btnRepeatLaserAreaScannerInterval = new System.Windows.Forms.Button();
      this.btnRepeatLaserAreaScannerChannel = new System.Windows.Forms.Button();
      this.nudProcessCount = new System.Windows.Forms.NumericUpDown();
      this.nudProcessPeriod = new System.Windows.Forms.NumericUpDown();
      this.btnSetProcessPeriod = new System.Windows.Forms.Button();
      this.btnSetProcessCount = new System.Windows.Forms.Button();
      this.pnlLedSystem = new System.Windows.Forms.Panel();
      this.pnlStateLedSystem = new System.Windows.Forms.Panel();
      this.lblHeaderLedSystem = new System.Windows.Forms.Label();
      this.lblHeader = new System.Windows.Forms.Label();
      this.btnGetProcessCount = new System.Windows.Forms.Button();
      this.btnGetProcessPeriod = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.pbxButton)).BeginInit();
      this.pnlState.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudChannelHigh)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudChannelLow)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessPeriod)).BeginInit();
      this.pnlLedSystem.SuspendLayout();
      this.SuspendLayout();
      // 
      // pbxButton
      // 
      this.pbxButton.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxButton.Location = new System.Drawing.Point(0, 0);
      this.pbxButton.Name = "pbxButton";
      this.pbxButton.Size = new System.Drawing.Size(565, 617);
      this.pbxButton.TabIndex = 0;
      this.pbxButton.TabStop = false;
      // 
      // pnlState
      // 
      this.pnlState.BackColor = System.Drawing.SystemColors.Info;
      this.pnlState.Controls.Add(this.label9);
      this.pnlState.Controls.Add(this.lblStateMTCIndex);
      this.pnlState.Controls.Add(this.lblStateMTControllerText);
      this.pnlState.Controls.Add(this.lblHeaderState);
      this.pnlState.Location = new System.Drawing.Point(249, 53);
      this.pnlState.Name = "pnlState";
      this.pnlState.Size = new System.Drawing.Size(311, 27);
      this.pnlState.TabIndex = 86;
      // 
      // label9
      // 
      this.label9.Location = new System.Drawing.Point(111, 7);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(8, 13);
      this.label9.TabIndex = 68;
      this.label9.Text = "-";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblStateMTCIndex
      // 
      this.lblStateMTCIndex.BackColor = System.Drawing.Color.PeachPuff;
      this.lblStateMTCIndex.Location = new System.Drawing.Point(61, 4);
      this.lblStateMTCIndex.Name = "lblStateMTCIndex";
      this.lblStateMTCIndex.Size = new System.Drawing.Size(47, 19);
      this.lblStateMTCIndex.TabIndex = 67;
      this.lblStateMTCIndex.Text = "- - -";
      this.lblStateMTCIndex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblStateMTControllerText
      // 
      this.lblStateMTControllerText.BackColor = System.Drawing.Color.PeachPuff;
      this.lblStateMTControllerText.Location = new System.Drawing.Point(121, 4);
      this.lblStateMTControllerText.Name = "lblStateMTControllerText";
      this.lblStateMTControllerText.Size = new System.Drawing.Size(184, 19);
      this.lblStateMTControllerText.TabIndex = 66;
      this.lblStateMTControllerText.Text = "- - -";
      this.lblStateMTControllerText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblHeaderState
      // 
      this.lblHeaderState.AutoSize = true;
      this.lblHeaderState.Location = new System.Drawing.Point(3, 7);
      this.lblHeaderState.Name = "lblHeaderState";
      this.lblHeaderState.Size = new System.Drawing.Size(55, 13);
      this.lblHeaderState.TabIndex = 65;
      this.lblHeaderState.Text = "State LAS";
      // 
      // btnGetLedSystem
      // 
      this.btnGetLedSystem.Location = new System.Drawing.Point(4, 441);
      this.btnGetLedSystem.Name = "btnGetLedSystem";
      this.btnGetLedSystem.Size = new System.Drawing.Size(89, 24);
      this.btnGetLedSystem.TabIndex = 82;
      this.btnGetLedSystem.Text = "GetLedSystem";
      this.btnGetLedSystem.UseVisualStyleBackColor = true;
      this.btnGetLedSystem.Click += new System.EventHandler(this.btnGetLedSystem_Click);
      // 
      // tbxMessages
      // 
      this.tbxMessages.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxMessages.Location = new System.Drawing.Point(4, 84);
      this.tbxMessages.Multiline = true;
      this.tbxMessages.Name = "tbxMessages";
      this.tbxMessages.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.tbxMessages.Size = new System.Drawing.Size(556, 321);
      this.tbxMessages.TabIndex = 78;
      // 
      // tbxHardwareVersion
      // 
      this.tbxHardwareVersion.Enabled = false;
      this.tbxHardwareVersion.Location = new System.Drawing.Point(125, 28);
      this.tbxHardwareVersion.Name = "tbxHardwareVersion";
      this.tbxHardwareVersion.Size = new System.Drawing.Size(117, 20);
      this.tbxHardwareVersion.TabIndex = 77;
      this.tbxHardwareVersion.Text = "-";
      this.tbxHardwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tbxSoftwareVersion
      // 
      this.tbxSoftwareVersion.Enabled = false;
      this.tbxSoftwareVersion.Location = new System.Drawing.Point(370, 29);
      this.tbxSoftwareVersion.Name = "tbxSoftwareVersion";
      this.tbxSoftwareVersion.Size = new System.Drawing.Size(85, 20);
      this.tbxSoftwareVersion.TabIndex = 76;
      this.tbxSoftwareVersion.Text = "-";
      this.tbxSoftwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnGetHelp
      // 
      this.btnGetHelp.Location = new System.Drawing.Point(3, 55);
      this.btnGetHelp.Name = "btnGetHelp";
      this.btnGetHelp.Size = new System.Drawing.Size(117, 23);
      this.btnGetHelp.TabIndex = 71;
      this.btnGetHelp.Text = "GetHelp";
      this.btnGetHelp.UseVisualStyleBackColor = true;
      this.btnGetHelp.Click += new System.EventHandler(this.btnGetHelp_Click);
      // 
      // btnSwitchLedSystemOn
      // 
      this.btnSwitchLedSystemOn.Location = new System.Drawing.Point(96, 441);
      this.btnSwitchLedSystemOn.Name = "btnSwitchLedSystemOn";
      this.btnSwitchLedSystemOn.Size = new System.Drawing.Size(118, 24);
      this.btnSwitchLedSystemOn.TabIndex = 70;
      this.btnSwitchLedSystemOn.Text = "SwitchLedSystemOn";
      this.btnSwitchLedSystemOn.UseVisualStyleBackColor = true;
      this.btnSwitchLedSystemOn.Click += new System.EventHandler(this.btnSetLedSystemHigh_Click);
      // 
      // btnGetHardwareVersion
      // 
      this.btnGetHardwareVersion.Location = new System.Drawing.Point(3, 26);
      this.btnGetHardwareVersion.Name = "btnGetHardwareVersion";
      this.btnGetHardwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetHardwareVersion.TabIndex = 68;
      this.btnGetHardwareVersion.Text = "GetHardwareVersion";
      this.btnGetHardwareVersion.UseVisualStyleBackColor = true;
      this.btnGetHardwareVersion.Click += new System.EventHandler(this.btnGetHardwareVersion_Click);
      // 
      // btnGetSoftwareVersion
      // 
      this.btnGetSoftwareVersion.Location = new System.Drawing.Point(248, 27);
      this.btnGetSoftwareVersion.Name = "btnGetSoftwareVersion";
      this.btnGetSoftwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetSoftwareVersion.TabIndex = 67;
      this.btnGetSoftwareVersion.Text = "GetSoftwareVersion";
      this.btnGetSoftwareVersion.UseVisualStyleBackColor = true;
      this.btnGetSoftwareVersion.Click += new System.EventHandler(this.btnGetSoftwareVersion_Click);
      // 
      // btnGetProgramHeader
      // 
      this.btnGetProgramHeader.Location = new System.Drawing.Point(125, 55);
      this.btnGetProgramHeader.Name = "btnGetProgramHeader";
      this.btnGetProgramHeader.Size = new System.Drawing.Size(118, 23);
      this.btnGetProgramHeader.TabIndex = 66;
      this.btnGetProgramHeader.Text = "GetProgramHeader";
      this.btnGetProgramHeader.UseVisualStyleBackColor = true;
      this.btnGetProgramHeader.Click += new System.EventHandler(this.btnGetProgramHeader_Click);
      // 
      // btnStopProcessExecution
      // 
      this.btnStopProcessExecution.Location = new System.Drawing.Point(440, 411);
      this.btnStopProcessExecution.Name = "btnStopProcessExecution";
      this.btnStopProcessExecution.Size = new System.Drawing.Size(92, 24);
      this.btnStopProcessExecution.TabIndex = 88;
      this.btnStopProcessExecution.Text = "StopProcess";
      this.btnStopProcessExecution.UseVisualStyleBackColor = true;
      this.btnStopProcessExecution.Click += new System.EventHandler(this.btnStopProcessExecution_Click);
      // 
      // btnSwitchLedSystemOff
      // 
      this.btnSwitchLedSystemOff.Location = new System.Drawing.Point(218, 441);
      this.btnSwitchLedSystemOff.Name = "btnSwitchLedSystemOff";
      this.btnSwitchLedSystemOff.Size = new System.Drawing.Size(117, 24);
      this.btnSwitchLedSystemOff.TabIndex = 92;
      this.btnSwitchLedSystemOff.Text = "SwitchLedSystemOff";
      this.btnSwitchLedSystemOff.UseVisualStyleBackColor = true;
      this.btnSwitchLedSystemOff.Click += new System.EventHandler(this.btnSetLedSystemLow_Click);
      // 
      // btnBlinkLedSystem
      // 
      this.btnBlinkLedSystem.Location = new System.Drawing.Point(339, 441);
      this.btnBlinkLedSystem.Name = "btnBlinkLedSystem";
      this.btnBlinkLedSystem.Size = new System.Drawing.Size(97, 24);
      this.btnBlinkLedSystem.TabIndex = 93;
      this.btnBlinkLedSystem.Text = "BlinkLedSystem";
      this.btnBlinkLedSystem.UseVisualStyleBackColor = true;
      this.btnBlinkLedSystem.Click += new System.EventHandler(this.btnBlinkLedSystem_Click);
      //// 
      //// btnGetLaserAreaScannerChannel
      //// 
      this.btnGetLaserAreaScannerChannel.Location = new System.Drawing.Point(4, 533);
      this.btnGetLaserAreaScannerChannel.Name = "btnGetLaserAreaScannerChannel";
      this.btnGetLaserAreaScannerChannel.Size = new System.Drawing.Size(172, 24);
      this.btnGetLaserAreaScannerChannel.TabIndex = 94;
      this.btnGetLaserAreaScannerChannel.Text = "GetLaserAreaScannerChannel";
      this.btnGetLaserAreaScannerChannel.UseVisualStyleBackColor = true;
      //this.btnGetLaserAreaScannerChannel.Click += new System.EventHandler(this.btnGetLaserAreaScannerChannel_Click);
      //// 
      //// btnGetLaserAreaScannerInterval
      //// 
      this.btnGetLaserAreaScannerInterval.Location = new System.Drawing.Point(181, 533);
      this.btnGetLaserAreaScannerInterval.Name = "btnGetLaserAreaScannerInterval";
      this.btnGetLaserAreaScannerInterval.Size = new System.Drawing.Size(172, 24);
      this.btnGetLaserAreaScannerInterval.TabIndex = 95;
      this.btnGetLaserAreaScannerInterval.Text = "GetLaserAreaScannerInterval";
      this.btnGetLaserAreaScannerInterval.UseVisualStyleBackColor = true;
      //this.btnGetLaserAreaScannerInterval.Click += new System.EventHandler(this.btnGetLaserAreaScannerInterval_Click);
      //// 
      //// btnGetAllLaserAreaScanners
      //// 
      this.btnGetAllLaserAreaScanners.Location = new System.Drawing.Point(358, 533);
      this.btnGetAllLaserAreaScanners.Name = "btnGetAllLaserAreaScanners";
      this.btnGetAllLaserAreaScanners.Size = new System.Drawing.Size(172, 24);
      this.btnGetAllLaserAreaScanners.TabIndex = 96;
      this.btnGetAllLaserAreaScanners.Text = "GetAllLaserAreaScanners";
      this.btnGetAllLaserAreaScanners.UseVisualStyleBackColor = true;
      //this.btnGetAllLaserAreaScanners.Click += new System.EventHandler(this.btnGetAllLaserAreaScanners_Click);
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(146, 508);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(97, 13);
      this.label6.TabIndex = 100;
      this.label6.Text = "ChannelIndex High";
      // 
      // nudChannelHigh
      // 
      this.nudChannelHigh.Location = new System.Drawing.Point(244, 506);
      this.nudChannelHigh.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
      this.nudChannelHigh.Name = "nudChannelHigh";
      this.nudChannelHigh.Size = new System.Drawing.Size(34, 20);
      this.nudChannelHigh.TabIndex = 99;
      this.nudChannelHigh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudChannelHigh.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(9, 508);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(95, 13);
      this.label7.TabIndex = 98;
      this.label7.Text = "ChannelIndex Low";
      // 
      // nudChannelLow
      // 
      this.nudChannelLow.Location = new System.Drawing.Point(105, 506);
      this.nudChannelLow.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
      this.nudChannelLow.Name = "nudChannelLow";
      this.nudChannelLow.Size = new System.Drawing.Size(34, 20);
      this.nudChannelLow.TabIndex = 97;
      this.nudChannelLow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      //// 
      //// btnRepeatAllLaserAreaScanners
      //// 
      this.btnRepeatAllLaserAreaScanners.Location = new System.Drawing.Point(358, 563);
      this.btnRepeatAllLaserAreaScanners.Name = "btnRepeatAllLaserAreaScanners";
      this.btnRepeatAllLaserAreaScanners.Size = new System.Drawing.Size(172, 24);
      this.btnRepeatAllLaserAreaScanners.TabIndex = 103;
      this.btnRepeatAllLaserAreaScanners.Text = "RepeatAllLaserAreaScanners";
      this.btnRepeatAllLaserAreaScanners.UseVisualStyleBackColor = true;
      //this.btnRepeatAllLaserAreaScanners.Click += new System.EventHandler(this.btnRepeatAllLaserAreaScanners_Click);
      //// 
      //// btnRepeatLaserAreaScannerInterval
      //// 
      this.btnRepeatLaserAreaScannerInterval.Location = new System.Drawing.Point(181, 563);
      this.btnRepeatLaserAreaScannerInterval.Name = "btnRepeatLaserAreaScannerInterval";
      this.btnRepeatLaserAreaScannerInterval.Size = new System.Drawing.Size(172, 24);
      this.btnRepeatLaserAreaScannerInterval.TabIndex = 102;
      this.btnRepeatLaserAreaScannerInterval.Text = "RepeatLaserAreaScannerInterval";
      this.btnRepeatLaserAreaScannerInterval.UseVisualStyleBackColor = true;
      //this.btnRepeatLaserAreaScannerInterval.Click += new System.EventHandler(this.btnRepeatLaserAreaScannerInterval_Click);
      //// 
      //// btnRepeatLaserAreaScannerChannel
      //// 
      this.btnRepeatLaserAreaScannerChannel.Location = new System.Drawing.Point(4, 563);
      this.btnRepeatLaserAreaScannerChannel.Name = "btnRepeatLaserAreaScannerChannel";
      this.btnRepeatLaserAreaScannerChannel.Size = new System.Drawing.Size(172, 24);
      this.btnRepeatLaserAreaScannerChannel.TabIndex = 101;
      this.btnRepeatLaserAreaScannerChannel.Text = "RepeatLaserAreaScannerChannel";
      this.btnRepeatLaserAreaScannerChannel.UseVisualStyleBackColor = true;
      //this.btnRepeatLaserAreaScannerChannel.Click += new System.EventHandler(this.btnRepeatLaserAreaScannerChannel_Click);
      // 
      // nudProcessCount
      // 
      this.nudProcessCount.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudProcessCount.Location = new System.Drawing.Point(156, 413);
      this.nudProcessCount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudProcessCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudProcessCount.Name = "nudProcessCount";
      this.nudProcessCount.Size = new System.Drawing.Size(55, 20);
      this.nudProcessCount.TabIndex = 74;
      this.nudProcessCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudProcessCount.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      // 
      // nudProcessPeriod
      // 
      this.nudProcessPeriod.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudProcessPeriod.Location = new System.Drawing.Point(380, 413);
      this.nudProcessPeriod.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudProcessPeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudProcessPeriod.Name = "nudProcessPeriod";
      this.nudProcessPeriod.Size = new System.Drawing.Size(52, 20);
      this.nudProcessPeriod.TabIndex = 72;
      this.nudProcessPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudProcessPeriod.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      // 
      // btnSetProcessPeriod
      // 
      this.btnSetProcessPeriod.Location = new System.Drawing.Point(253, 411);
      this.btnSetProcessPeriod.Name = "btnSetProcessPeriod";
      this.btnSetProcessPeriod.Size = new System.Drawing.Size(123, 24);
      this.btnSetProcessPeriod.TabIndex = 87;
      this.btnSetProcessPeriod.Text = "SetProcessPeriod [ms]";
      this.btnSetProcessPeriod.UseVisualStyleBackColor = true;
      this.btnSetProcessPeriod.Click += new System.EventHandler(this.btnSetProcessPeriod_Click);
      // 
      // btnSetProcessCount
      // 
      this.btnSetProcessCount.Location = new System.Drawing.Point(39, 411);
      this.btnSetProcessCount.Name = "btnSetProcessCount";
      this.btnSetProcessCount.Size = new System.Drawing.Size(113, 24);
      this.btnSetProcessCount.TabIndex = 69;
      this.btnSetProcessCount.Text = "SetProcessCount [1]";
      this.btnSetProcessCount.UseVisualStyleBackColor = true;
      this.btnSetProcessCount.Click += new System.EventHandler(this.btnSetProcessCount_Click);
      // 
      // pnlLedSystem
      // 
      this.pnlLedSystem.BackColor = System.Drawing.SystemColors.Info;
      this.pnlLedSystem.Controls.Add(this.pnlStateLedSystem);
      this.pnlLedSystem.Controls.Add(this.lblHeaderLedSystem);
      this.pnlLedSystem.Location = new System.Drawing.Point(442, 441);
      this.pnlLedSystem.Name = "pnlLedSystem";
      this.pnlLedSystem.Size = new System.Drawing.Size(90, 27);
      this.pnlLedSystem.TabIndex = 104;
      // 
      // pnlStateLedSystem
      // 
      this.pnlStateLedSystem.BackColor = System.Drawing.Color.DarkRed;
      this.pnlStateLedSystem.Location = new System.Drawing.Point(66, 6);
      this.pnlStateLedSystem.Name = "pnlStateLedSystem";
      this.pnlStateLedSystem.Size = new System.Drawing.Size(17, 15);
      this.pnlStateLedSystem.TabIndex = 0;
      // 
      // lblHeaderLedSystem
      // 
      this.lblHeaderLedSystem.AutoSize = true;
      this.lblHeaderLedSystem.Location = new System.Drawing.Point(4, 7);
      this.lblHeaderLedSystem.Name = "lblHeaderLedSystem";
      this.lblHeaderLedSystem.Size = new System.Drawing.Size(59, 13);
      this.lblHeaderLedSystem.TabIndex = 65;
      this.lblHeaderLedSystem.Text = "LedSystem";
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(565, 23);
      this.lblHeader.TabIndex = 105;
      this.lblHeader.Text = "LaserAreaScanner Command";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnGetProcessCount
      // 
      this.btnGetProcessCount.Location = new System.Drawing.Point(4, 411);
      this.btnGetProcessCount.Name = "btnGetProcessCount";
      this.btnGetProcessCount.Size = new System.Drawing.Size(32, 24);
      this.btnGetProcessCount.TabIndex = 106;
      this.btnGetProcessCount.Text = "Get";
      this.btnGetProcessCount.UseVisualStyleBackColor = true;
      this.btnGetProcessCount.Click += new System.EventHandler(this.btnGetProcessCount_Click);
      // 
      // btnGetProcessPeriod
      // 
      this.btnGetProcessPeriod.Location = new System.Drawing.Point(218, 411);
      this.btnGetProcessPeriod.Name = "btnGetProcessPeriod";
      this.btnGetProcessPeriod.Size = new System.Drawing.Size(32, 24);
      this.btnGetProcessPeriod.TabIndex = 107;
      this.btnGetProcessPeriod.Text = "Get";
      this.btnGetProcessPeriod.UseVisualStyleBackColor = true;
      this.btnGetProcessPeriod.Click += new System.EventHandler(this.btnGetProcessPeriod_Click);
      // 
      // CUCLaserAreaScannerCommand
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnGetProcessPeriod);
      this.Controls.Add(this.btnGetProcessCount);
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.pnlLedSystem);
      this.Controls.Add(this.btnRepeatAllLaserAreaScanners);
      this.Controls.Add(this.btnRepeatLaserAreaScannerInterval);
      this.Controls.Add(this.btnRepeatLaserAreaScannerChannel);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.nudChannelHigh);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.nudChannelLow);
      this.Controls.Add(this.btnGetAllLaserAreaScanners);
      this.Controls.Add(this.btnGetLaserAreaScannerInterval);
      this.Controls.Add(this.btnGetLaserAreaScannerChannel);
      this.Controls.Add(this.btnBlinkLedSystem);
      this.Controls.Add(this.btnSwitchLedSystemOff);
      this.Controls.Add(this.btnStopProcessExecution);
      this.Controls.Add(this.btnSetProcessPeriod);
      this.Controls.Add(this.pnlState);
      this.Controls.Add(this.btnGetLedSystem);
      this.Controls.Add(this.tbxMessages);
      this.Controls.Add(this.tbxHardwareVersion);
      this.Controls.Add(this.tbxSoftwareVersion);
      this.Controls.Add(this.nudProcessCount);
      this.Controls.Add(this.nudProcessPeriod);
      this.Controls.Add(this.btnGetHelp);
      this.Controls.Add(this.btnSwitchLedSystemOn);
      this.Controls.Add(this.btnSetProcessCount);
      this.Controls.Add(this.btnGetHardwareVersion);
      this.Controls.Add(this.btnGetSoftwareVersion);
      this.Controls.Add(this.btnGetProgramHeader);
      this.Controls.Add(this.pbxButton);
      this.Name = "CUCLaserAreaScannerCommand";
      this.Size = new System.Drawing.Size(565, 617);
      ((System.ComponentModel.ISupportInitialize)(this.pbxButton)).EndInit();
      this.pnlState.ResumeLayout(false);
      this.pnlState.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudChannelHigh)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudChannelLow)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessPeriod)).EndInit();
      this.pnlLedSystem.ResumeLayout(false);
      this.pnlLedSystem.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox pbxButton;
    private System.Windows.Forms.Panel pnlState;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label lblStateMTCIndex;
    private System.Windows.Forms.Label lblStateMTControllerText;
    private System.Windows.Forms.Label lblHeaderState;
    private System.Windows.Forms.Button btnGetLedSystem;
    private System.Windows.Forms.TextBox tbxMessages;
    private System.Windows.Forms.TextBox tbxHardwareVersion;
    private System.Windows.Forms.TextBox tbxSoftwareVersion;
    private System.Windows.Forms.Button btnGetHelp;
    private System.Windows.Forms.Button btnSwitchLedSystemOn;
    private System.Windows.Forms.Button btnGetHardwareVersion;
    private System.Windows.Forms.Button btnGetSoftwareVersion;
    private System.Windows.Forms.Button btnGetProgramHeader;
    private System.Windows.Forms.Button btnStopProcessExecution;
    private System.Windows.Forms.Button btnSwitchLedSystemOff;
    private System.Windows.Forms.Button btnBlinkLedSystem;
    private System.Windows.Forms.Button btnGetLaserAreaScannerChannel;
    private System.Windows.Forms.Button btnGetLaserAreaScannerInterval;
    private System.Windows.Forms.Button btnGetAllLaserAreaScanners;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.NumericUpDown nudChannelHigh;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.NumericUpDown nudChannelLow;
    private System.Windows.Forms.Button btnRepeatAllLaserAreaScanners;
    private System.Windows.Forms.Button btnRepeatLaserAreaScannerInterval;
    private System.Windows.Forms.Button btnRepeatLaserAreaScannerChannel;
    private System.Windows.Forms.NumericUpDown nudProcessCount;
    private System.Windows.Forms.NumericUpDown nudProcessPeriod;
    private System.Windows.Forms.Button btnSetProcessPeriod;
    private System.Windows.Forms.Button btnSetProcessCount;
    private System.Windows.Forms.Panel pnlLedSystem;
    private System.Windows.Forms.Panel pnlStateLedSystem;
    private System.Windows.Forms.Label lblHeaderLedSystem;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Button btnGetProcessCount;
    private System.Windows.Forms.Button btnGetProcessPeriod;
  }
}
