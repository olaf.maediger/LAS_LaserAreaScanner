﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCTemperature
{
  public partial class CUCTemperatureSegment : UserControl
  {
    //
    //---------------------------------------------------------
    //  Segment - Constant
    //---------------------------------------------------------
    //
    private const String MASK_VALUE = "{0,6:0.0}";
    private const Double INIT_VALUE = 0.0;
    //
    //---------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------
    //
    //
    //---------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------
    //
    public CUCTemperatureSegment()
    {
      InitializeComponent();
      DValue = INIT_VALUE;
    }
    //
    //---------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------
    //
    public  String Header
    {
      get { return lblHeader.Text; }
      set { lblHeader.Text = value; }
    }

    public void SetValue(Double value)
    {
      lblValue.Text = String.Format("{0:0.0}", value);
      lblValue.Refresh();
    }

    public Double DValue
    {
      get { return Double.Parse(lblValue.Text); }
      set 
      { 
        lblValue.Text = String.Format("{0:0.0}", value);
        lblValue.Refresh();
      }
    }
    
    public String Unit
    {
      get { return lblUnit.Text; }
      set { lblUnit.Text = value; }
    }
    //
    //---------------------------------------------------------
    //  Segment - Handler
    //---------------------------------------------------------
    //
  }
}
