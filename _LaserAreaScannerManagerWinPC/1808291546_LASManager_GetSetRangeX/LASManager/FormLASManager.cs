﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
using System.Threading;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using Uart;
using UCUartTerminal;
using CommandProtocol;
using LaserScanner;
using CPLaserAreaScanner;
using UCLaserAreaScanner;
//
namespace LASManager
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormClient : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_DEVICETYPE = "LaserAreaScanner";
    private const String INIT_DEVICENAME = "LAS";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    private const String NAME_DEBUG = "Debug";
    private const Boolean INIT_DEBUG = true;
    private const String NAME_SHOWPAGE = "ShowPage";
    private const Int32 INIT_SHOWPAGE = 0;
    private const String NAME_COMPORT = "ComPort";
    private const EComPort INIT_COMPORT = EComPort.cp1;
    private const String NAME_SKIPZEROPULSESSTEPLINE = "SkipZeroPulsesStepLine";
    private const Boolean INIT_SKIPZEROPULSESSTEPLINE = false;
    private const String NAME_SKIPZEROPULSESSTEPTABLE = "SkipZeroPulsesStepTable";
    private const Boolean INIT_SKIPZEROPULSESSTEPTABLE = true;
    //
    private const String NAME_PROCESSCOUNT = "ProcessCount";
    private const Int32 INIT_PROCESSCOUNT = 1;
    private const String NAME_PROCESSPERIODMS = "ProcessPeriodms";
    private const Int32 INIT_PROCESSPERIODMS = 1000;
    private const String NAME_POSITIONXACTUAL = "PositionXActual";
    private const Int32 INIT_POSITIONXACTUAL = 2000;
    private const String NAME_POSITIONXMINIMUM = "PositionXMinimum";
    private const Int32 INIT_POSITIONXMINIMUM = 1800;
    private const String NAME_POSITIONXMAXIMUM = "PositionXMaximum";
    private const Int32 INIT_POSITIONXMAXIMUM = 2200;
    private const String NAME_POSITIONXDELTA = "PositionXDelta";
    private const Int32 INIT_POSITIONXDELTA = 100;
    private const String NAME_POSITIONYACTUAL = "PositionYActual";
    private const Int32 INIT_POSITIONYACTUAL = 2000;
    private const String NAME_POSITIONYMINIMUM = "PositionYMinimum";
    private const Int32 INIT_POSITIONYMINIMUM = 1800;
    private const String NAME_POSITIONYMAXIMUM = "PositionYMaximum";
    private const Int32 INIT_POSITIONYMAXIMUM = 2200;
    private const String NAME_POSITIONYDELTA = "PositionYDelta";
    private const Int32 INIT_POSITIONYDELTA = 100;
    private const String NAME_DELAYMOTIONMS = "DelayMotionms";
    private const Int32 INIT_DELAYMOTIONMS = 10;
    private const String NAME_PULSEWIDTHUS = "PulseWidthus";
    private const Int32 INIT_PULSEWIDTHUS = 500000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    private CUart FUart;
    private CCommandList FCommandList;
    private Int32 FRxdCount = 0;
    private Int32 FRxdIndex = 0;
    private String FRxdLine = "";
    private Boolean FResult = false;
    private CProcessPulseStepList FProcessPulseStepLine;
    private CProcessPulseStepList FProcessPulseStepTable;
    private CLaserStepList FLaserStepList;
    private UInt32 FLaserStepIndex;
    private UInt32 FColCount, FRowCount;
    private UInt32 FDelayMotionShortms = 10;
    private UInt32 FDelayMotionLongms = 100;
    private String FStepOrder = "LeftRight";
    private Boolean FZeroPulseSkippedStepLine = false;
    private Boolean FZeroPulseSkippedStepTable = false;
    private UInt32 FLaserStepIndexRefresh = 0;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      //####################################################################################
      // Init - Instance - Client
      //####################################################################################
      //
      FUart = new CUart();
      FUart.SetOnErrorDetected(UartOnErrorDetected);
      FUart.SetOnLineReceived(UartOnLineReceived);
      FUart.SetOnLineTransmitted(UartOnLineTransmitted);
      FUart.SetOnPinChanged(UartOnPinChanged);
      FUart.SetOnTextReceived(UartOnTextReceived);
      FUart.SetOnTextTransmitted(UartOnTextTransmitted);
      //
      // already exist FUCUartTerminal = new CUCUartTerminal....
      FUCUartTerminal.SetNotifier(FUCNotifier);
      FUCUartTerminal.SetOnTerminalOpen(UCUartTerminalOnTerminalOpen);
      FUCUartTerminal.SetOnTerminalClose(UCUartTerminalOnTerminalClose);
      FUCUartTerminal.SetOnTransmitText(UCUartTerminalOnTransmitText);
      //
      FCommandList = new CCommandList();
      // -> Main! FCommandList.SetUdpTextDeviceClient(FUdpTextDeviceClient);
      FCommandList.SetNotifier(FUCNotifier);
      FCommandList.SetOnExecutionStart(CommandListOnExecutionStart);
      FCommandList.SetOnExecutionBusy(CommandListOnExecutionBusy);
      FCommandList.SetOnExecutionEnd(CommandListOnExecutionEnd);
      FCommandList.SetOnExecutionAbort(CommandListOnExecutionAbort);
      //---------------------------------------------------------------
      // UCLaserAreaScannerCommand - Common
      FUCLaserAreaScannerCommand.SetOnGetHelp(UCLaserAreaScannerCommandOnGetHelp);
      FUCLaserAreaScannerCommand.SetOnGetProgramHeader(UCLaserAreaScannerCommandOnGetProgramHeader);
      FUCLaserAreaScannerCommand.SetOnGetHardwareVersion(UCLaserAreaScannerCommandOnGetHardwareVersion);
      FUCLaserAreaScannerCommand.SetOnGetSoftwareVersion(UCLaserAreaScannerCommandOnGetSoftwareVersion);
      FUCLaserAreaScannerCommand.SetOnGetProcessCount(UCLaserAreaScannerCommandOnGetProcessCount);
      FUCLaserAreaScannerCommand.SetOnSetProcessCount(UCLaserAreaScannerCommandOnSetProcessCount);
      FUCLaserAreaScannerCommand.SetOnGetProcessPeriodms(UCLaserAreaScannerCommandOnGetProcessPeriodms);
      FUCLaserAreaScannerCommand.SetOnSetProcessPeriodms(UCLaserAreaScannerCommandOnSetProcessPeriodms);
      FUCLaserAreaScannerCommand.SetOnAbortProcessExecution(UCLaserAreaScannerCommandOnAbortProcessExecution);
      // UCLaserAreaScannerCommand - LedSystem
      FUCLaserAreaScannerCommand.SetOnGetLedSystem(UCLaserAreaScannerCommandOnGetLedSystem);
      FUCLaserAreaScannerCommand.SetOnSetLedSystemLow(UCLaserAreaScannerCommandOnSetLedSystemLow);
      FUCLaserAreaScannerCommand.SetOnSetLedSystemHigh(UCLaserAreaScannerCommandOnSetLedSystemHigh);
      FUCLaserAreaScannerCommand.SetOnBlinkLedSystem(UCLaserAreaScannerCommandOnBlinkLedSystem);
      // UCLaserAreaScannerCommand - LaserRange
      FUCLaserAreaScannerCommand.SetOnGetPositionX(UCLaserAreaScannerCommandOnGetPositionX);
      FUCLaserAreaScannerCommand.SetOnSetPositionX(UCLaserAreaScannerCommandOnSetPositionX);
      FUCLaserAreaScannerCommand.SetOnGetPositionY(UCLaserAreaScannerCommandOnGetPositionY);
      FUCLaserAreaScannerCommand.SetOnSetPositionY(UCLaserAreaScannerCommandOnSetPositionY);
      FUCLaserAreaScannerCommand.SetOnGetRangeX(UCLaserAreaScannerCommandOnGetRangeX);
      FUCLaserAreaScannerCommand.SetOnSetRangeX(UCLaserAreaScannerCommandOnSetRangeX);
      FUCLaserAreaScannerCommand.SetOnGetRangeY(UCLaserAreaScannerCommandOnGetRangeY);
      FUCLaserAreaScannerCommand.SetOnSetRangeY(UCLaserAreaScannerCommandOnSetRangeY);
      FUCLaserAreaScannerCommand.SetOnGetDelayMotionms(UCLaserAreaScannerCommandOnGetDelayMotionms);
      FUCLaserAreaScannerCommand.SetOnSetDelayMotionms(UCLaserAreaScannerCommandOnSetDelayMotionms);
      FUCLaserAreaScannerCommand.SetOnGetPulseWidthus(UCLaserAreaScannerCommandOnGetPulseWidthus);
      FUCLaserAreaScannerCommand.SetOnSetPulseWidthus(UCLaserAreaScannerCommandOnSetPulseWidthus);
      //---------------------------------------------------------------
      // UCLaserAreaScannerCommand - LaserPosition
      FUCLaserAreaScannerCommand.SetOnPulseLaserPosition(UCLaserAreaScannerCommandOnPulseLaserPosition);
      FUCLaserAreaScannerCommand.SetOnAbortLaserPosition(UCLaserAreaScannerCommandOnAbortLaserPosition);
      //---------------------------------------------------------------
      // UCLaserStepLine
      FUCLaserStepLine.SetOnGetPositionX(UCLaserStepLineOnGetPositionX);
      FUCLaserStepLine.SetOnSetPositionX(UCLaserStepLineOnSetPositionX);
      FUCLaserStepLine.SetOnGetPositionY(UCLaserStepLineOnGetPositionY);
      FUCLaserStepLine.SetOnSetPositionY(UCLaserStepLineOnSetPositionY);
      FUCLaserStepLine.SetOnGetRangeX(UCLaserStepLineOnGetRangeX);
      FUCLaserStepLine.SetOnSetRangeX(UCLaserStepLineOnSetRangeX);
      FUCLaserStepLine.SetOnGetRangeY(UCLaserStepLineOnGetRangeY);
      FUCLaserStepLine.SetOnSetRangeY(UCLaserStepLineOnSetRangeY);
      FUCLaserStepLine.SetOnGetProcessCount(UCLaserStepLineOnGetProcessCount);
      FUCLaserStepLine.SetOnSetProcessCount(UCLaserStepLineOnSetProcessCount);
      FUCLaserStepLine.SetOnGetProcessPeriodms(UCLaserStepLineOnGetProcessPeriodms);
      FUCLaserStepLine.SetOnSetProcessPeriodms(UCLaserStepLineOnSetProcessPeriodms);
      FUCLaserStepLine.SetOnGetPulseWidthus(UCLaserStepLineOnGetPulseWidthus);
      FUCLaserStepLine.SetOnSetPulseWidthus(UCLaserStepLineOnSetPulseWidthus);
      FUCLaserStepLine.SetOnGetDelayMotionms(UCLaserStepLineOnGetDelayMotionms);
      FUCLaserStepLine.SetOnSetDelayMotionms(UCLaserStepLineOnSetDelayMotionms);
      FUCLaserStepLine.SetOnPulseLaserRow(UCLaserStepLineOnPulseLaserRow);
      FUCLaserStepLine.SetOnAbortLaserRow(UCLaserStepLineOnAbortLaserRow);
      FUCLaserStepLine.SetOnPulseLaserCol(UCLaserStepLineOnPulseLaserCol);
      FUCLaserStepLine.SetOnAbortLaserCol(UCLaserStepLineOnAbortLaserCol);
      //---------------------------------------------------------------
      // UCLaserMatrix
      FUCLaserAreaScannerCommand.SetOnPulseLaserMatrix(UCLaserAreaScannerCommandOnPulseLaserMatrix);
      FUCLaserAreaScannerCommand.SetOnAbortLaserMatrix(UCLaserAreaScannerCommandOnAbortLaserMatrix);
      //---------------------------------------------------------------
      // UCLaserImage
      FUCLaserAreaScannerCommand.SetOnLoadLaserSteptable(UCLaserAreaScannerCommandOnLoadLaserSteptable);
      FUCLaserAreaScannerCommand.SetOnPulseLaserImage(UCLaserAreaScannerCommandOnPulseLaserImage);
      FUCLaserAreaScannerCommand.SetOnAbortLaserImage(UCLaserAreaScannerCommandOnAbortLaserImage);
      //---------------------------------------------------------------
      // UCLaserStepTable
      FUCLaserAreaScannerTable.SetOnLoadLaserSteptable(UCLaserAreaScannerCommandOnLoadLaserSteptable);
      FUCLaserAreaScannerTable.SetOnPulseLaserImage(UCLaserAreaScannerCommandOnPulseLaserImage);
      FUCLaserAreaScannerTable.SetOnAbortLaserImage(UCLaserAreaScannerCommandOnAbortLaserImage);
      FUCLaserAreaScannerMatrix.SetOnLoadLaserSteptable(UCLaserAreaScannerCommandOnLoadLaserSteptable);
      FUCLaserAreaScannerMatrix.SetOnPulseLaserImage(UCLaserAreaScannerCommandOnPulseLaserImage);
      FUCLaserAreaScannerMatrix.SetOnAbortLaserImage(UCLaserAreaScannerCommandOnAbortLaserImage);
      //
      // UCLaserSDCardCommand
      //.....
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      tbcMain.SelectedIndex = 0;
      ////
      //FProcessPulseStepLine = new CProcessPulseStepList(PulseStepLineOnExecutionStart,
      //                                                  PulseStepLineOnExecutionBusy,
      //                                                  PulseStepLineOnExecutionEnd,
      //                                                  PulseStepLineOnExecutionAbort);
      //FProcessPulseStepTable = new CProcessPulseStepList(PulseStepTableOnExecutionStart,
      //                                                   PulseStepTableOnExecutionBusy,
      //                                                   PulseStepTableOnExecutionEnd,
      //                                                   PulseStepTableOnExecutionAbort);
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FUart.Close();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      BValue = INIT_DEBUG;
      Result &= initdata.ReadBoolean(NAME_DEBUG, out BValue, BValue);
      cbxDebug.Checked = BValue;
      //
      Int32 IValue = INIT_SHOWPAGE;
      Result &= initdata.ReadInt32(NAME_SHOWPAGE, out IValue, IValue);
      tbcMain.SelectedIndex = IValue;
      //
      String SCP = CUart.COMPORTS[(Int32)INIT_COMPORT];
      Result &= initdata.ReadEnumeration(NAME_COMPORT, out SCP, SCP);
      EComPort ECP = CUart.ComPortTextEnumerator(SCP);
      FUCUartTerminal.SetComPort(ECP);
      //
      BValue = INIT_SKIPZEROPULSESSTEPLINE;
      Result &= initdata.ReadBoolean(NAME_SKIPZEROPULSESSTEPLINE, out BValue, BValue);
      FUCLaserStepLine.SkipZeroPulses = BValue;
      //
      BValue = INIT_SKIPZEROPULSESSTEPTABLE;
      Result &= initdata.ReadBoolean(NAME_SKIPZEROPULSESSTEPTABLE, out BValue, BValue);
      FUCLaserAreaScannerTable.SkipZeroPulses = BValue;
      //
      //  LaserAreaScanner
      //
      IValue = INIT_PROCESSCOUNT;
      Result &= initdata.ReadInt32(NAME_PROCESSCOUNT, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetProcessCount((UInt32)IValue);
      FUCLaserStepLine.SetProcessCount((UInt32)IValue);
      FCommandList.Enqueue(new CSetProcessCount((UInt32)IValue));
      //
      IValue = INIT_PROCESSPERIODMS;
      Result &= initdata.ReadInt32(NAME_PROCESSPERIODMS, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetProcessPeriodms((UInt32)IValue);
      FUCLaserStepLine.SetProcessPeriodms((UInt32)IValue);
      FCommandList.Enqueue(new CSetProcessPeriod((UInt32)IValue));
      //
      IValue = INIT_POSITIONXACTUAL;
      Result &= initdata.ReadInt32(NAME_POSITIONXACTUAL, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionXActual((UInt16)IValue);
      FUCLaserStepLine.SetPositionXActual((UInt16)IValue);
      //
      IValue = INIT_POSITIONXMINIMUM;
      Result &= initdata.ReadInt32(NAME_POSITIONXMINIMUM, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionXMinimum((UInt16)IValue);
      FUCLaserStepLine.SetPositionXMinimum((UInt16)IValue);
      //
      IValue = INIT_POSITIONXMAXIMUM;
      Result &= initdata.ReadInt32(NAME_POSITIONXMAXIMUM, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionXMaximum((UInt16)IValue);
      FUCLaserStepLine.SetPositionXMaximum((UInt16)IValue);
      //
      IValue = INIT_POSITIONXDELTA;
      Result &= initdata.ReadInt32(NAME_POSITIONXDELTA, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionXDelta((UInt16)IValue);
      FUCLaserStepLine.SetPositionXDelta((UInt16)IValue);
      //
      IValue = INIT_POSITIONYACTUAL;
      Result &= initdata.ReadInt32(NAME_POSITIONYACTUAL, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionYActual((UInt16)IValue);
      FUCLaserStepLine.SetPositionYActual((UInt16)IValue);
      //
      IValue = INIT_POSITIONYMINIMUM;
      Result &= initdata.ReadInt32(NAME_POSITIONYMINIMUM, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionYMinimum((UInt16)IValue);
      FUCLaserStepLine.SetPositionYMinimum((UInt16)IValue);
      //
      IValue = INIT_POSITIONYMAXIMUM;
      Result &= initdata.ReadInt32(NAME_POSITIONYMAXIMUM, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionYMaximum((UInt16)IValue);
      FUCLaserStepLine.SetPositionYMaximum((UInt16)IValue);
      //
      IValue = INIT_POSITIONYDELTA;
      Result &= initdata.ReadInt32(NAME_POSITIONYDELTA, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionYDelta((UInt16)IValue);
      FUCLaserStepLine.SetPositionYDelta((UInt16)IValue);
      //
      IValue = INIT_DELAYMOTIONMS;
      Result &= initdata.ReadInt32(NAME_DELAYMOTIONMS, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetDelayMotionms((UInt32)IValue);
      FUCLaserStepLine.SetDelayMotionms((UInt32)IValue);
      FCommandList.Enqueue(new CSetDelayMotion((UInt32)IValue));
      //
      IValue = INIT_PULSEWIDTHUS;
      Result &= initdata.ReadInt32(NAME_PULSEWIDTHUS, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPulseWidthus((UInt32)IValue);
      FUCLaserStepLine.SetPulseWidthus((UInt32)IValue);
      FCommandList.Enqueue(new CSetPulseWidthus((UInt32)IValue));
      //
      FCommandList.Execute();
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      Result &= initdata.WriteBoolean(NAME_DEBUG, cbxDebug.Checked);
      Result &= initdata.WriteInt32(NAME_SHOWPAGE, tbcMain.SelectedIndex);
      String SCP = CUart.ComPortEnumeratorName(FUCUartTerminal.GetComPort());
      Result &= initdata.WriteEnumeration(NAME_COMPORT, SCP);      
      Result &= initdata.WriteBoolean(NAME_SKIPZEROPULSESSTEPLINE, FUCLaserStepLine.SkipZeroPulses);
      Result &= initdata.WriteBoolean(NAME_SKIPZEROPULSESSTEPTABLE, FUCLaserAreaScannerTable.SkipZeroPulses);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      //  LaserAreaScanner
      //
      Result &= initdata.WriteInt32(NAME_PROCESSCOUNT,
                                    (Int32)FUCLaserAreaScannerCommand.GetProcessCount());
      //
      Result &= initdata.WriteInt32(NAME_PROCESSPERIODMS,
                                    (Int32)FUCLaserAreaScannerCommand.GetProcessPeriodms());
      //
      Result &= initdata.WriteInt32(NAME_POSITIONXACTUAL, 
                                    FUCLaserAreaScannerCommand.GetPositionXActual());      
      //
      Result &= initdata.WriteInt32(NAME_POSITIONXMINIMUM, 
                                    FUCLaserAreaScannerCommand.GetPositionXMinimum());      
      //
      Result &= initdata.WriteInt32(NAME_POSITIONXMAXIMUM, 
                                    FUCLaserAreaScannerCommand.GetPositionXMaximum());      
      //
      Result &= initdata.WriteInt32(NAME_POSITIONXDELTA, 
                                    FUCLaserAreaScannerCommand.GetPositionXDelta());      
      //
      Result &= initdata.WriteInt32(NAME_POSITIONYACTUAL, 
                                    FUCLaserAreaScannerCommand.GetPositionYActual());      
      //
      Result &= initdata.WriteInt32(NAME_POSITIONYMINIMUM, 
                                    FUCLaserAreaScannerCommand.GetPositionYMinimum());      
      //
      Result &= initdata.WriteInt32(NAME_POSITIONYMAXIMUM, 
                                    FUCLaserAreaScannerCommand.GetPositionYMaximum());      
      //
      Result &= initdata.WriteInt32(NAME_POSITIONYDELTA, 
                                    FUCLaserAreaScannerCommand.GetPositionYDelta());      
      //
      Result &= initdata.WriteInt32(NAME_DELAYMOTIONMS,
                                    (Int32)FUCLaserAreaScannerCommand.GetDelayMotionms());      
      //
      Result &= initdata.WriteInt32(NAME_PULSEWIDTHUS,
                                    (Int32)FUCLaserAreaScannerCommand.GetPulseWidthus());
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //

    //
    //
    //#########################################################################
    //  Segment - Callback - CommandList
    //#########################################################################
    //
    private delegate void CBCommandListOnExecutionStart(RTaskData data);
    private void CommandListOnExecutionStart(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionStart CB = new CBCommandListOnExecutionStart(CommandListOnExecutionStart);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          // FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionStart", data.Name));
        }
        if (0 < FCommandList.Count)
        { 
          CCommand Command = FCommandList.Peek();
          if (Command is CCommand)
          {
            String CommandText = Command.GetCommandText();
            FUart.WriteLine(CommandText);
          }
        }
      }
    }
    private delegate Boolean CBCommandListOnExecutionBusy(RTaskData data);
    private Boolean CommandListOnExecutionBusy(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionBusy CB = new CBCommandListOnExecutionBusy(CommandListOnExecutionBusy);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          // FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionBusy", data.Name));
        }
      }
      return false;
    }
    private delegate void CBCommandListOnExecutionEnd(RTaskData data);
    private void CommandListOnExecutionEnd(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionEnd CB = new CBCommandListOnExecutionEnd(CommandListOnExecutionEnd);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          // FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionEnd", data.Name));
        }
      }
    }
    private delegate void CBCommandListOnExecutionAbort(RTaskData data);
    private void CommandListOnExecutionAbort(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionAbort CB = new CBCommandListOnExecutionAbort(CommandListOnExecutionAbort);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          // FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionAbort", data.Name));
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - Common
    //###########################################################################################
    //
    private delegate void CBAnalyseDateTimePrompt();
    private void AnalyseDateTimePrompt()
    {
      if (this.InvokeRequired)
      {
        CBAnalyseDateTimePrompt CB = new CBAnalyseDateTimePrompt(AnalyseDateTimePrompt);
        Invoke(CB, new object[] { });
      }
      else
      {
        if (14 <= FRxdLine.Length)
        { // test ":hh:mm:ss.mmm>"
          if ((CDefine.SEPARATOR_COLON == FRxdLine[0]) && (CDefine.SEPARATOR_GREATER == FRxdLine[13]))
          {
            Char[] Separators = new Char[] { CDefine.SEPARATOR_MARK, 
                                             CDefine.SEPARATOR_COLON, 
                                             CDefine.SEPARATOR_DOT, 
                                             CDefine.SEPARATOR_GREATER };
            String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
            if (3 < Tokens.Length)
            {
              UInt16 Hours, Minutes, Seconds, Millis;
              Hours = UInt16.Parse(Tokens[0]);
              Minutes = UInt16.Parse(Tokens[1]);
              Seconds = UInt16.Parse(Tokens[2]);
              Millis = UInt16.Parse(Tokens[3]);
              FUCLaserAreaScannerCommand.SetProcessTime(Hours, Minutes, Seconds, Millis);
              if (cbxDebug.Checked)
              {
                //FUCNotifier.Write(String.Format("DATETIME[{0}:{1}:{2}.{3}]",
                //                                Tokens[0], Tokens[1], Tokens[2], Tokens[3]));                
              }
              // debug Console.WriteLine("VVV" + rxdline + "VVV");
              FRxdLine = FRxdLine.Substring(14);
              // debug Console.WriteLine("NNN" + rxdline + "NNN");
              // Event -> NO command.SignalTextReceived();
              
            }
          }
        }
      }
    }

    private delegate void CBAnalyseStateLaserAreaScanner();
    private void AnalyseStateLaserAreaScanner()
    {
      if (this.InvokeRequired)
      {
        CBAnalyseStateLaserAreaScanner CB = 
          new CBAnalyseStateLaserAreaScanner(AnalyseStateLaserAreaScanner);
        Invoke(CB, new object[] { });
      }
      else
      {
        if (7 <= FRxdLine.Length)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          Int32 TL = Tokens.Length;
          if (4 <= TL)
          {
            if (CDefine.PROMPT_RESPONSE == Tokens[0]) 
            {
              Int32 SMTC;
              Int32.TryParse(Tokens[2], out SMTC);
              Int32 SubState;
              Int32.TryParse(Tokens[3], out SubState);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("STATELASERAREASCANNER[{0}|{1}]", SMTC, SubState));
              }
              FUCLaserAreaScannerCommand.SetState(Tokens);
              // deleting STP-Information from RxdLine
              Int32 S = 3 + Tokens[0].Length + Tokens[1].Length + Tokens[2].Length + Tokens[3].Length;
              FRxdLine = FRxdLine.Substring(S);
              //
              if ((0 == SMTC) && (0 == SubState))
              {
                if (0 < FCommandList.Count)
                {
                  CCommand Command = (CCommand)FCommandList.Peek();
                  if (Command is CCommand)
                  {
                    Command.SignalTextReceived();
                  }                  
                }
              }
            }
          }
        }
      }
    }

    //private delegate void CBAnalyseTemperatureValues();
    //private void AnalyseTemperatureValues()
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBAnalyseTemperatureValues CB = new CBAnalyseTemperatureValues(AnalyseTemperatureValues);
    //    Invoke(CB, new object[] { });
    //  }
    //  else
    //  {
    //    if (15 <= FRxdLine.Length)
    //    {
    //      Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
    //      String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //      Int32 TL = Tokens.Length;
    //      if (3 <= TL)
    //      { // <: T[0-7] +020.1> ...
    //        if ((CDefine.PROMPT_EVENT == Tokens[0]) && ("T[" == Tokens[1].Substring(0, 2)))
    //        {
    //          Char[] SubSeparators = new Char[] { '[', ']', '-' };
    //          String[] SubTokens = Tokens[1].Split(SubSeparators, StringSplitOptions.RemoveEmptyEntries);
    //          if (3 == SubTokens.Length)
    //          {
    //            if ("T" == SubTokens[0])
    //            {
    //              Int32 CL;
    //              Int32.TryParse(SubTokens[1], out CL);
    //              Int32 CH;
    //              Int32.TryParse(SubTokens[2], out CH);
    //              if (cbxDebug.Checked)
    //              {
    //                FUCNotifier.Write(String.Format("TEMPERATURERVALUES[{0}-{1}]", CL, CH));
    //              }
    //              Double[] Temperatures = new Double[8];
    //              for (Int32 TI = 0; TI < 8; TI++)
    //              {
    //                if ((CL <= TI) && (TI <= CH))
    //                {
    //                  Double DValue;
    //                  if (Double.TryParse(Tokens[2 + TI], out DValue))
    //                  {
    //                    Temperatures[TI] = DValue;
    //                  }
    //                  else
    //                  {
    //                    //Temperatures[TI] = -999.9;
    //                  }
    //                }
    //                else
    //                {
    //                  Temperatures[TI] = 0.0;
    //                }
    //              }
    //              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommandDisplay.SetTemperatureValues(CL, CH, Temperatures);
    //              // deleting TValues-Information from RxdLine                  
    //              FRxdLine = "";
    //            }
    //          }
    //        }
    //      }
    //    }
    //  }
    //}
    //
    //###########################################################################################
    //  Segment - Analyse - Common
    //###########################################################################################
    //
    private delegate void CBAnalyseGetHelp(CCommand command);//, String rxdline);
    private void AnalyseGetHelp(CCommand command)//, String rxdline)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetHelp CB = new CBAnalyseGetHelp(AnalyseGetHelp);
        Invoke(CB, new object[] { command });//, rxdline });
      }
      else
      {
        FResult = false;
        if (command is CGetHelp)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetHelp.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetHelp.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FUCLaserAreaScannerCommand.ClearMessages();
              FUCLaserAreaScannerCommand.AddMessageLine("");
              FRxdIndex = 0;
              FResult = true;
            }
          }
          if (1 <= Tokens.Length)
          {
            if (CDefine.SEPARATOR_NUMBER == Tokens[0][0])
            {
              FRxdIndex++;
              if (cbxDebug.Checked)
              {
                String Line = String.Format("HELP[{0}|{1}]<{2}>", FRxdIndex, FRxdCount, FRxdLine);
                FUCNotifier.Write(Line);
              }
              if (FRxdCount <= FRxdIndex)
              {
                command.SignalTextReceived();
                FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine);
              }
              else
              {
                FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine + "\r\n");
              }
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetProgramHeader(CCommand command);
    private void AnalyseGetProgramHeader(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetProgramHeader CB = new CBAnalyseGetProgramHeader(AnalyseGetProgramHeader);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetProgramHeader)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetProgramHeader.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetProgramHeader.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FUCLaserAreaScannerCommand.ClearMessages();
              FUCLaserAreaScannerCommand.AddMessageLine("");
              FRxdIndex = 0;
              FResult = true;
            }
          }
          if (1 <= Tokens.Length)
          {
            if (CDefine.SEPARATOR_NUMBER == Tokens[0][0])
            {
              FRxdIndex++;
              if (cbxDebug.Checked)
              {
                String Line = String.Format("PROGRAMHEADER[{0}|{1}]<{2}>", FRxdIndex, FRxdCount, FRxdLine);
                FUCNotifier.Write(Line);
              }
              if (FRxdCount <= FRxdIndex)
              {
                command.SignalTextReceived();
                FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine);
              }
              else
              {
                FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine + "\r\n");
              }
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetHardwareVersion(CCommand command);
    private void AnalyseGetHardwareVersion(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetHardwareVersion CB = new CBAnalyseGetHardwareVersion(AnalyseGetHardwareVersion);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetHardwareVersion)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetHardwareVersion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetHardwareVersion.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FRxdIndex = 0;
              FResult = true;
            }
            else
              if (CDefine.TOKEN_NUMBER == Tokens[0])
              {
                FRxdIndex++;
                if (cbxDebug.Checked)
                {
                  FUCNotifier.Write(String.Format("HARDWAREVERSION[{0}]", Tokens[2]));
                }
              FUCLaserAreaScannerCommand.SetHardwareVersion(Tokens[2]);
              if (FRxdCount <= FRxdIndex) command.SignalTextReceived();
                FResult = true;
              }
           
          }
        }
      }
    }

    private delegate void CBAnalyseGetSoftwareVersion(CCommand command);
    private void AnalyseGetSoftwareVersion(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetSoftwareVersion CB = new CBAnalyseGetSoftwareVersion(AnalyseGetSoftwareVersion);
        Invoke(CB, new object[] { command }); 
      }
      else
      {
        FResult = false;
        if (command is CGetSoftwareVersion)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetSoftwareVersion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetSoftwareVersion.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FRxdIndex = 0;
              FResult = true;
            }
            else
              if (CDefine.TOKEN_NUMBER == Tokens[0])
              {
                FRxdIndex++;
                if (cbxDebug.Checked)
                {
                  FUCNotifier.Write(String.Format("SOFTWAREVERSION[{0}]", Tokens[2]));
                }
              FUCLaserAreaScannerCommand.SetSoftwareVersion(Tokens[2]);
              if (FRxdCount <= FRxdIndex) command.SignalTextReceived();
                FResult = true;
              }
          }
        }
      }
    }

    private delegate void CBAnalyseGetProcessCount(CCommand command);
    private void AnalyseGetProcessCount(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetProcessCount CB = new CBAnalyseGetProcessCount(AnalyseGetProcessCount);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetProcessCount)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetProcessCount.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetProcessCount.HEADER + " expected");
              }
              UInt32 ProcessCount;
              UInt32.TryParse(Tokens[2], out ProcessCount);
              FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              FUCLaserStepLine.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPROCESSCOUNT[{0}]", ProcessCount));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetProcessCount(CCommand command);
    private void AnalyseSetProcessCount(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetProcessCount CB = new CBAnalyseSetProcessCount(AnalyseSetProcessCount);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetProcessCount)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetProcessCount.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetProcessCount.HEADER + " expected");
              }
              UInt32 ProcessCount;
              UInt32.TryParse(Tokens[2], out ProcessCount);
              FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              FUCLaserStepLine.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPROCESSCOUNT[{0}]", ProcessCount));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetProcessPeriodms(CCommand command);
    private void AnalyseGetProcessPeriodms(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetProcessPeriodms CB = new CBAnalyseGetProcessPeriodms(AnalyseGetProcessPeriodms);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetProcessPeriod)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetProcessPeriod.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetProcessPeriod.HEADER + " expected");
              }
              UInt32 ProcessPeriod;
              UInt32.TryParse(Tokens[2], out ProcessPeriod);
              FUCLaserAreaScannerCommand.SetProcessPeriodms(ProcessPeriod);
              FUCLaserStepLine.SetProcessPeriodms(ProcessPeriod);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPROCESSPERIOD[{0}]", ProcessPeriod));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetProcessPeriodms(CCommand command);
    private void AnalyseSetProcessPeriodms(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetProcessPeriodms CB = new CBAnalyseSetProcessPeriodms(AnalyseSetProcessPeriodms);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetProcessPeriod)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetProcessPeriod.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetProcessPeriod.HEADER + " expected");
              }
              UInt32 ProcessPeriod;
              UInt32.TryParse(Tokens[2], out ProcessPeriod);
              FUCLaserAreaScannerCommand.SetProcessPeriodms(ProcessPeriod);
              FUCLaserStepLine.SetProcessPeriodms(ProcessPeriod);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPROCESSPERIOD[{0}]", ProcessPeriod));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseStopProcessExecution(CCommand command);
    private void AnalyseStopProcessExecution(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseStopProcessExecution CB = new CBAnalyseStopProcessExecution(AnalyseStopProcessExecution);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CStopProcessExecution)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CStopProcessExecution.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CStopProcessExecution.HEADER + " expected");
              }
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("STOPPROCESSEXECUTION");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LedSystem
    //###########################################################################################
    //
    private delegate void CBAnalyseGetLedSystem(CCommand command);
    private void AnalyseGetLedSystem(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetLedSystem CB = new CBAnalyseGetLedSystem(AnalyseGetLedSystem);
        Invoke(CB, new object[] { command }); 
      }
      else
      {
        FResult = false;
        if (command is CGetLedSystem)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetLedSystem.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetLedSystem.HEADER + " expected");
              }
              Int32 State;
              Int32.TryParse(Tokens[2], out State);
              FUCLaserAreaScannerCommand.SetStateLedSystem(State);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETSTATELEDSYSTEM[{0}]", State));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetLedSystemHigh(CCommand command);
    private void AnalyseSetLedSystemHigh(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetLedSystemHigh CB = new CBAnalyseSetLedSystemHigh(AnalyseSetLedSystemHigh);
        Invoke(CB, new object[] { command }); 
      }
      else
      {
        FResult = false;
        if (command is CSetLedSystemHigh)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetLedSystemHigh.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetLedSystemHigh.HEADER + " expected");
              }
              FUCLaserAreaScannerCommand.SetStateLedSystem((int)EStateLed.On);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("SETLEDSYSTEMHIGH");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetLedSystemLow(CCommand command);
    private void AnalyseSetLedSystemLow(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetLedSystemLow CB = new CBAnalyseSetLedSystemLow(AnalyseSetLedSystemLow);
        Invoke(CB, new object[] { command }); 
      }
      else
      {
        FResult = false;
        if (command is CSetLedSystemLow)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetLedSystemLow.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetLedSystemLow.HEADER + " expected");
              }
              FUCLaserAreaScannerCommand.SetStateLedSystem((int)EStateLed.Off);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("SETLEDSYSTEMLOW");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseBlinkLedSystem(CCommand command);
    private void AnalyseBlinkLedSystem(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseBlinkLedSystem CB = new CBAnalyseBlinkLedSystem(AnalyseBlinkLedSystem);
        Invoke(CB, new object[] { command }); 
      }
      else
      {
        FResult = false;
        if (command is CBlinkLedSystem)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (4 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CBlinkLedSystem.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CBlinkLedSystem.HEADER + " expected");
              }
              UInt32 ProcessPeriod;
              UInt32.TryParse(Tokens[2], out ProcessPeriod);
              FUCLaserAreaScannerCommand.SetProcessPeriodms(ProcessPeriod);
              FUCLaserStepLine.SetProcessPeriodms(ProcessPeriod);
              UInt32 ProcessCount;
              UInt32.TryParse(Tokens[3], out ProcessCount);
              FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              FUCLaserStepLine.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("BLINKLEDSYSTEM[{0}|{1}]", ProcessPeriod, ProcessCount));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LaserRange
    //###########################################################################################
    //
    private delegate void CBAnalyseGetPositionX(CCommand command);
    private void AnalyseGetPositionX(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetPositionX CB = new CBAnalyseGetPositionX(AnalyseGetPositionX);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetPositionX)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetPositionX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetPositionX.HEADER + " expected");
              }
              UInt16 Position;
              UInt16.TryParse(Tokens[2], out Position);
              FUCLaserAreaScannerCommand.SetPositionXActual(Position);
              FUCLaserStepLine.SetPositionXActual(Position);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPOSITIONX[{0}]", Position));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetPositionX(CCommand command);
    private void AnalyseSetPositionX(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetPositionX CB = new CBAnalyseSetPositionX(AnalyseSetPositionX);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetPositionX)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetPositionX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetPositionX.HEADER + " expected");
              }
              UInt16 Position;
              UInt16.TryParse(Tokens[2], out Position);
              FUCLaserAreaScannerCommand.SetPositionXActual(Position);
              FUCLaserStepLine.SetPositionXActual(Position);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPOSITIONX[{0}]", Position));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetPositionY(CCommand command);
    private void AnalyseGetPositionY(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetPositionY CB = new CBAnalyseGetPositionY(AnalyseGetPositionY);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetPositionY)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetPositionY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetPositionY.HEADER + " expected");
              }
              UInt16 Position;
              UInt16.TryParse(Tokens[2], out Position);
              FUCLaserAreaScannerCommand.SetPositionYActual(Position);
              FUCLaserStepLine.SetPositionYActual(Position);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPOSITIONY[{0}]", Position));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetPositionY(CCommand command);
    private void AnalyseSetPositionY(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetPositionY CB = new CBAnalyseSetPositionY(AnalyseSetPositionY);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetPositionY)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetPositionY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetPositionY.HEADER + " expected");
              }
              UInt16 Position;
              UInt16.TryParse(Tokens[2], out Position);
              FUCLaserAreaScannerCommand.SetPositionYActual(Position);
              FUCLaserStepLine.SetPositionYActual(Position);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPOSITIONY[{0}]", Position));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetRangeX(CCommand command);
    private void AnalyseGetRangeX(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetRangeX CB = new CBAnalyseGetRangeX(AnalyseGetRangeX);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetRangeX)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (5 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetRangeX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetRangeX.HEADER + " expected");
              }
              UInt16 Minimum, Maximum, Delta;
              UInt16.TryParse(Tokens[2], out Minimum);
              UInt16.TryParse(Tokens[3], out Maximum);
              UInt16.TryParse(Tokens[4], out Delta);
              FUCLaserAreaScannerCommand.SetPositionXMinimum(Minimum);
              FUCLaserAreaScannerCommand.SetPositionXMaximum(Maximum);
              FUCLaserAreaScannerCommand.SetPositionXDelta(Delta);
              FUCLaserStepLine.SetPositionXMinimum(Minimum);
              FUCLaserStepLine.SetPositionXMaximum(Maximum);
              FUCLaserStepLine.SetPositionXDelta(Delta);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETRANGEX[{0}, {1}, {2}]",
                                                Minimum, Maximum, Delta));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetRangeX(CCommand command);
    private void AnalyseSetRangeX(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetRangeX CB = new CBAnalyseSetRangeX(AnalyseSetRangeX);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetRangeX)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (5 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetRangeX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetRangeX.HEADER + " expected");
              }
              UInt16 Minimum, Maximum, Delta;
              UInt16.TryParse(Tokens[2], out Minimum);
              UInt16.TryParse(Tokens[3], out Maximum);
              UInt16.TryParse(Tokens[4], out Delta);
              FUCLaserAreaScannerCommand.SetPositionXMinimum(Minimum);
              FUCLaserAreaScannerCommand.SetPositionXMaximum(Maximum);
              FUCLaserAreaScannerCommand.SetPositionXDelta(Delta);
              FUCLaserStepLine.SetPositionXMinimum(Minimum);
              FUCLaserStepLine.SetPositionXMaximum(Maximum);
              FUCLaserStepLine.SetPositionXDelta(Delta);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETRANGEX[{0}, {1}, {2}]",
                                                Minimum, Maximum, Delta));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetRangeY(CCommand command);
    private void AnalyseGetRangeY(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetRangeY CB = new CBAnalyseGetRangeY(AnalyseGetRangeY);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetRangeY)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (5 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetRangeY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetRangeY.HEADER + " expected");
              }
              UInt16 Minimum, Maximum, Delta;
              UInt16.TryParse(Tokens[2], out Minimum);
              UInt16.TryParse(Tokens[3], out Maximum);
              UInt16.TryParse(Tokens[4], out Delta);
              FUCLaserAreaScannerCommand.SetPositionYMinimum(Minimum);
              FUCLaserAreaScannerCommand.SetPositionYMaximum(Maximum);
              FUCLaserAreaScannerCommand.SetPositionYDelta(Delta);
              FUCLaserStepLine.SetPositionYMinimum(Minimum);
              FUCLaserStepLine.SetPositionYMaximum(Maximum);
              FUCLaserStepLine.SetPositionYDelta(Delta);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETRANGEY[{0}, {1}, {2}]",
                                                Minimum, Maximum, Delta));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetRangeY(CCommand command);
    private void AnalyseSetRangeY(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetRangeY CB = new CBAnalyseSetRangeY(AnalyseSetRangeY);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetRangeY)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (5 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetRangeY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetRangeY.HEADER + " expected");
              }
              UInt16 Minimum, Maximum, Delta;
              UInt16.TryParse(Tokens[2], out Minimum);
              UInt16.TryParse(Tokens[3], out Maximum);
              UInt16.TryParse(Tokens[4], out Delta);
              FUCLaserAreaScannerCommand.SetPositionYMinimum(Minimum);
              FUCLaserAreaScannerCommand.SetPositionYMaximum(Maximum);
              FUCLaserAreaScannerCommand.SetPositionYDelta(Delta);
              FUCLaserStepLine.SetPositionYMinimum(Minimum);
              FUCLaserStepLine.SetPositionYMaximum(Maximum);
              FUCLaserStepLine.SetPositionYDelta(Delta);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETRANGEY[{0}, {1}, {2}]",
                                                Minimum, Maximum, Delta));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetDelayMotionms(CCommand command);
    private void AnalyseGetDelayMotionms(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetDelayMotionms CB = new CBAnalyseGetDelayMotionms(AnalyseGetDelayMotionms);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetDelayMotion)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetDelayMotion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetDelayMotion.HEADER + " expected");
              }
              UInt32 Delay;
              UInt32.TryParse(Tokens[2], out Delay);
              FUCLaserAreaScannerCommand.SetDelayMotionms(Delay);
              FUCLaserStepLine.SetDelayMotionms(Delay);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETDELAYMOTION[{0}]", Delay));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetDelayMotionms(CCommand command);
    private void AnalyseSetDelayMotionms(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetDelayMotionms CB = new CBAnalyseSetDelayMotionms(AnalyseSetDelayMotionms);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetDelayMotion)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetDelayMotion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetDelayMotion.HEADER + " expected");
              }
              UInt32 Delay;
              UInt32.TryParse(Tokens[2], out Delay);
              FUCLaserAreaScannerCommand.SetDelayMotionms(Delay);
              FUCLaserStepLine.SetDelayMotionms(Delay);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETDELAYMOTION[{0}]", Delay));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetPulseWidthus(CCommand command);
    private void AnalyseGetPulseWidthus(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetPulseWidthus CB = new CBAnalyseGetPulseWidthus(AnalyseGetPulseWidthus);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetPulseWidthus)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetPulseWidthus.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetPulseWidthus.HEADER + " expected");
              }
              UInt32 PW;
              UInt32.TryParse(Tokens[2], out PW);
              FUCLaserAreaScannerCommand.SetPulseWidthus(PW);
              FUCLaserStepLine.SetPulseWidthus(PW);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPULSEWIDTHUS[{0}]", PW));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetPulseWidthus(CCommand command);
    private void AnalyseSetPulseWidthus(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetPulseWidthus CB = new CBAnalyseSetPulseWidthus(AnalyseSetPulseWidthus);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetPulseWidthus)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetPulseWidthus.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetDelayMotion.HEADER + " expected");
              }
              UInt32 PW;
              UInt32.TryParse(Tokens[2], out PW);
              FUCLaserAreaScannerCommand.SetPulseWidthus(PW);
              FUCLaserStepLine.SetPulseWidthus(PW);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPULSEWIDTHUS[{0}]", PW));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LaserPosition
    //###########################################################################################
    //
    private delegate void CBAnalysePulseLaserPosition(CCommand command);
    private void AnalysePulseLaserPosition(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalysePulseLaserPosition CB = new CBAnalysePulseLaserPosition(AnalysePulseLaserPosition);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CPulseLaserPosition)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (6 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CPulseLaserPosition.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CPulseLaserPosition.HEADER + " expected");
              }
              UInt16 PositionX;
              UInt16.TryParse(Tokens[2], out PositionX);
              FUCLaserAreaScannerCommand.SetPositionXActual(PositionX);
              FUCLaserStepLine.SetPositionXActual(PositionX);
              UInt16 PositionY;
              UInt16.TryParse(Tokens[3], out PositionY);
              FUCLaserAreaScannerCommand.SetPositionYActual(PositionY);
              FUCLaserStepLine.SetPositionYActual(PositionY);
              UInt32 ProcessPeriod;
              UInt32.TryParse(Tokens[4], out ProcessPeriod);
              FUCLaserAreaScannerCommand.SetProcessPeriodms(ProcessPeriod);
              FUCLaserStepLine.SetProcessPeriodms(ProcessPeriod);
              UInt32 ProcessCount;
              UInt32.TryParse(Tokens[5], out ProcessCount);
              FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              FUCLaserStepLine.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("PULSELASERPOSITION[{0}|{1}|{2}|{3}]",
                                                PositionX, PositionY, ProcessPeriod, ProcessCount));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseAbortLaserPosition(CCommand command);
    private void AnalyseAbortLaserPosition(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseAbortLaserPosition CB = new CBAnalyseAbortLaserPosition(AnalyseAbortLaserPosition);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CAbortLaserPosition)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CAbortLaserPosition.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CAbortLaserPosition.HEADER + " expected");
              }
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("ABORTLASERPOSITION[]");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LaserLine
    //###########################################################################################
    //
    private delegate void CBAnalysePulseLaserRow(CCommand command);
    private void AnalysePulseLaserRow(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalysePulseLaserRow CB = new CBAnalysePulseLaserRow(AnalysePulseLaserRow);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CPulseLaserRow)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (4 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CPulseLaserRow.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CPulseLaserRow.HEADER + " expected");
              }
              UInt32 ProcessPeriod;
              UInt32.TryParse(Tokens[2], out ProcessPeriod);
              FUCLaserAreaScannerCommand.SetProcessPeriodms(ProcessPeriod);
              FUCLaserStepLine.SetProcessPeriodms(ProcessPeriod);
              UInt32 ProcessCount;
              UInt32.TryParse(Tokens[3], out ProcessCount);
              FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              FUCLaserStepLine.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("PULSELASERROW[{0}|{1}]",
                                                ProcessPeriod, ProcessCount));
              }
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseAbortLaserRow(CCommand command);
    private void AnalyseAbortLaserRow(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseAbortLaserRow CB = new CBAnalyseAbortLaserRow(AnalyseAbortLaserRow);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CAbortLaserRow)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CAbortLaserRow.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CAbortLaserRow.HEADER + " expected");
              }
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("ABORTLASERROW[]");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalysePulseLaserCol(CCommand command);
    private void AnalysePulseLaserCol(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalysePulseLaserCol CB = new CBAnalysePulseLaserCol(AnalysePulseLaserCol);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CPulseLaserCol)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (4 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CPulseLaserCol.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CPulseLaserCol.HEADER + " expected");
              }
              UInt32 ProcessPeriod;
              UInt32.TryParse(Tokens[2], out ProcessPeriod);
              FUCLaserAreaScannerCommand.SetProcessPeriodms(ProcessPeriod);
              FUCLaserStepLine.SetProcessPeriodms(ProcessPeriod);
              UInt32 ProcessCount;
              UInt32.TryParse(Tokens[3], out ProcessCount);
              FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              FUCLaserStepLine.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("PULSELASERCOL[{0}|{1}]",
                                                ProcessPeriod, ProcessCount));
              }
              //????????????????????????????????????????????command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseAbortLaserCol(CCommand command);
    private void AnalyseAbortLaserCol(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseAbortLaserCol CB = new CBAnalyseAbortLaserCol(AnalyseAbortLaserCol);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CAbortLaserCol)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CAbortLaserCol.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CAbortLaserCol.HEADER + " expected");
              }
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("ABORTLASERCOL[]");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LaserMatrix
    //###########################################################################################
    //
    private delegate void CBAnalysePulseLaserMatrix(CCommand command);
    private void AnalysePulseLaserMatrix(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalysePulseLaserMatrix CB = new CBAnalysePulseLaserMatrix(AnalysePulseLaserMatrix);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CPulseLaserMatrix)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (4 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CPulseLaserMatrix.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CPulseLaserMatrix.HEADER + " expected");
              }
              UInt32 ProcessPeriod;
              UInt32.TryParse(Tokens[2], out ProcessPeriod);
              FUCLaserAreaScannerCommand.SetProcessPeriodms(ProcessPeriod);
              FUCLaserStepLine.SetProcessPeriodms(ProcessPeriod);
              UInt32 ProcessCount;
              UInt32.TryParse(Tokens[3], out ProcessCount);
              FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              FUCLaserStepLine.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("PULSELASERMATRIX[{0}|{1}]",
                                                ProcessPeriod, ProcessCount));
              }
              //????????????????????????????????????????????command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseAbortLaserMatrix(CCommand command);
    private void AnalyseAbortLaserMatrix(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseAbortLaserMatrix CB = new CBAnalyseAbortLaserMatrix(AnalyseAbortLaserMatrix);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CAbortLaserMatrix)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CAbortLaserMatrix.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CAbortLaserMatrix.HEADER + " expected");
              }
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("ABORTLASERMATRIX[]");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LaserImage
    //###########################################################################################
    //
    private delegate void CBAnalysePulseLaserImage(CCommand command);
    private void AnalysePulseLaserImage(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalysePulseLaserImage CB = new CBAnalysePulseLaserImage(AnalysePulseLaserImage);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CPulseLaserImage)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CPulseLaserImage.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CPulseLaserImage.HEADER + " expected");
              }
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("PULSELASERIMAGE[]");
              }

              // start Thread!!!! !!!!!!!!!!!!!!!!!!!!!!!

              //????????????????????????????????????????????command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseAbortLaserImage(CCommand command);
    private void AnalyseAbortLaserImage(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseAbortLaserImage CB = new CBAnalyseAbortLaserImage(AnalyseAbortLaserImage);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CAbortLaserImage)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CAbortLaserImage.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CAbortLaserImage.HEADER + " expected");
              }
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("ABORTLASERIMAGE[]");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - Uart
    //------------------------------------------------------------------------
    //
    private void UartOnErrorDetected(EComPort comport, Int32 errorcode, String errortext)
    {
    }

    private void UartOnLineReceived(EComPort comport, String line)
    {
      if (cbxDebug.Checked)
      {
        FUCNotifier.Write(String.Format("ComPort[{0}] Rxd<{1}>", CUart.ComPortEnumeratorName(comport), line));
      }
      FRxdLine = line;
      AnalyseDateTimePrompt();
      AnalyseStateLaserAreaScanner();
      //  change with ...  - AnalyseTemperatureValues();
      if (0 < FCommandList.Count)
      {
        CCommand Command = FCommandList.Peek();
        if (Command is CCommand)
        { // Analyse Command:
          if (1 <= FRxdLine.Length)
          {
            FResult = false;
            // Common
            AnalyseGetHelp(Command);
            if (FResult) return;
            AnalyseGetProgramHeader(Command);
            if (FResult) return;
            AnalyseGetSoftwareVersion(Command);
            if (FResult) return;
            AnalyseGetHardwareVersion(Command);
            if (FResult) return;
            AnalyseGetProcessCount(Command);
            if (FResult) return;
            AnalyseSetProcessCount(Command);
            if (FResult) return;
            AnalyseGetProcessPeriodms(Command);
            if (FResult) return;
            AnalyseSetProcessPeriodms(Command);
            if (FResult) return;
            AnalyseStopProcessExecution(Command);
            if (FResult) return;
            // LedSystem
            AnalyseGetLedSystem(Command);
            if (FResult) return;
            AnalyseSetLedSystemHigh(Command);
            if (FResult) return;
            AnalyseSetLedSystemLow(Command);
            if (FResult) return;
            AnalyseBlinkLedSystem(Command);
            if (FResult) return;
            // LaserRange
            AnalyseGetPositionX(Command);
            if (FResult) return;
            AnalyseSetPositionX(Command);
            if (FResult) return;
            AnalyseGetPositionY(Command);
            if (FResult) return;
            AnalyseSetPositionY(Command);
            if (FResult) return;
            AnalyseGetRangeX(Command);
            if (FResult) return;
            AnalyseSetRangeX(Command);
            if (FResult) return;
            AnalyseGetRangeY(Command);
            if (FResult) return;
            AnalyseSetRangeY(Command);
            if (FResult) return;
            AnalyseGetDelayMotionms(Command);
            if (FResult) return;
            AnalyseSetDelayMotionms(Command);
            if (FResult) return;
            AnalyseGetPulseWidthus(Command);
            if (FResult) return;
            AnalyseSetPulseWidthus(Command);
            if (FResult) return;
            // LaserPosition
            AnalysePulseLaserPosition(Command);
            if (FResult) return;
            AnalyseAbortLaserPosition(Command);
            if (FResult) return;
            // LaserLine
            AnalysePulseLaserRow(Command);
            if (FResult) return;
            AnalyseAbortLaserRow(Command);
            if (FResult) return;
            AnalysePulseLaserCol(Command);
            if (FResult) return;
            AnalyseAbortLaserCol(Command);
            if (FResult) return;
            // LaserMatrix
            AnalysePulseLaserMatrix(Command);
            if (FResult) return;
            AnalyseAbortLaserMatrix(Command);
            if (FResult) return;
            // LaserImage
            // no Command LoadImage!!!
            AnalysePulseLaserImage(Command);
            if (FResult) return;
            AnalyseAbortLaserImage(Command);
            if (FResult) return;
          }
        }
      }
    }

    private void UartOnLineTransmitted(EComPort comport, String line)
    {
      if (cbxDebug.Checked)
      {
        FUCNotifier.Write(String.Format("ComPort[{0}] Txd<{1}>", CUart.ComPortEnumeratorName(comport), line));
      }
    }

    private void UartOnPinChanged(EComPort comport, Boolean pincts, Boolean pinrts, 
                                  Boolean pindsr, Boolean pindtr, Boolean pindcd)
    {
    }

    private void UartOnTextReceived(EComPort comport, String text)
    {
    }

    private void UartOnTextTransmitted(EComPort comport, String text)
    {
      if (cbxDebug.Checked)
      {
        FUCNotifier.Write(String.Format("ComPort[{0}] Txd<{1}>", CUart.ComPortEnumeratorName(comport), text));
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCUartTerminal
    //------------------------------------------------------------------------
    //
    private void UCUartTerminalOnTerminalOpen(String portname, EComPort comport, EBaudrate baudrate, EParity parity,
                                              EDatabits databits, EStopbits stopbits, EHandshake handshake)
    {
      String Line;
      if (FUart.Open(portname, comport, baudrate, parity, databits, stopbits, handshake))
      {
        if (cbxDebug.Checked)
        {
          Line = String.Format("ComPort[{0}]:({1}/{2}/{3}/{4}/{5}/{6}) correctly opened!",
                               portname, CUart.ComPortEnumeratorName(comport),
                               CUart.BaudrateEnumeratorName(baudrate), CUart.ParityEnumeratorName(parity),
                               CUart.DatabitsEnumeratorName(databits), CUart.StopbitsEnumeratorName(stopbits),
                               CUart.HandshakeEnumeratorName(handshake));
          FUCNotifier.Write(Line);
        }
        FUCUartTerminal.SetUartOpened(true);
      }
      else
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Error(HEADER_LIBRARY, 1, String.Format("Cannot open ComPort[{0}]",
                                                             CUart.ComPortEnumeratorName(comport)));
        }
        FUCUartTerminal.SetUartOpened(false);
      }
    }

    private void UCUartTerminalOnTerminalClose(EComPort comport)
    {
      if (FUart.Close())
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Write(String.Format("ComPort[{0}] correctly closed!", CUart.ComPortEnumeratorName(comport)));
        }
      }
      else
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Error(HEADER_LIBRARY, 1, String.Format("Cannot close ComPort[{0}]",
                                                            CUart.ComPortEnumeratorName(comport)));
        }
      }
    }

    private void UCUartTerminalOnTransmitText(string text, int delaycharacter, int delaycr, int delaylf)
    {
      if (cbxDebug.Checked)
      {
        FUart.WriteText(text);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCLaserAreaScannerCommand
    //------------------------------------------------------------------------
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetHelp();
    private void UCLaserAreaScannerCommandOnGetHelp()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetHelp CB = 
          new CBUCLaserAreaScannerCommandOnGetHelp(UCLaserAreaScannerCommandOnGetHelp);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetHelp());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetProgramHeader();
    private void UCLaserAreaScannerCommandOnGetProgramHeader()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetProgramHeader CB = 
          new CBUCLaserAreaScannerCommandOnGetProgramHeader(UCLaserAreaScannerCommandOnGetProgramHeader);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetProgramHeader());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetHardwareVersion();
    private void UCLaserAreaScannerCommandOnGetHardwareVersion()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetHardwareVersion CB = 
          new CBUCLaserAreaScannerCommandOnGetHardwareVersion(UCLaserAreaScannerCommandOnGetHardwareVersion);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetHardwareVersion());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetSoftwareVersion();
    private void UCLaserAreaScannerCommandOnGetSoftwareVersion()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetSoftwareVersion CB = 
          new CBUCLaserAreaScannerCommandOnGetSoftwareVersion(UCLaserAreaScannerCommandOnGetSoftwareVersion);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetSoftwareVersion());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetProcessCount();
    private void UCLaserAreaScannerCommandOnGetProcessCount()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetProcessCount CB =
          new CBUCLaserAreaScannerCommandOnGetProcessCount(UCLaserAreaScannerCommandOnGetProcessCount);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetProcessCount());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetProcessCount(UInt32 count);
    private void UCLaserAreaScannerCommandOnSetProcessCount(UInt32 count)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetProcessCount CB =
          new CBUCLaserAreaScannerCommandOnSetProcessCount(UCLaserAreaScannerCommandOnSetProcessCount);
        Invoke(CB, new object[] { count });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetProcessCount(count));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetProcessPeriodms();
    private void UCLaserAreaScannerCommandOnGetProcessPeriodms()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetProcessPeriodms CB =
          new CBUCLaserAreaScannerCommandOnGetProcessPeriodms(UCLaserAreaScannerCommandOnGetProcessPeriodms);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetProcessPeriod());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetProcessPeriodms(UInt32 period);
    private void UCLaserAreaScannerCommandOnSetProcessPeriodms(UInt32 period)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetProcessPeriodms CB =
          new CBUCLaserAreaScannerCommandOnSetProcessPeriodms(UCLaserAreaScannerCommandOnSetProcessPeriodms);
        Invoke(CB, new object[] { period });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetProcessPeriod(period));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnAbortProcessExecution();
    private void UCLaserAreaScannerCommandOnAbortProcessExecution()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnAbortProcessExecution CB = 
          new CBUCLaserAreaScannerCommandOnAbortProcessExecution(UCLaserAreaScannerCommandOnAbortProcessExecution);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        if (FProcessPulseStepLine is CProcessPulseStepList)
        {
          FProcessPulseStepLine.Abort();
        }
        if (FProcessPulseStepTable is CProcessPulseStepList)
        {
          FProcessPulseStepTable.Abort();
        }
        FCommandList.Clear(); //!!!!!!!!!!!!! Only Clear with Abort!!!
        FCommandList.Enqueue(new CAbortProcessExecution());
        FCommandList.Execute();
      }
    }


    private delegate void CBUCLaserAreaScannerCommandOnGetLedSystem();
    private void UCLaserAreaScannerCommandOnGetLedSystem()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetLedSystem CB =
          new CBUCLaserAreaScannerCommandOnGetLedSystem(UCLaserAreaScannerCommandOnGetLedSystem);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetLedSystem());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetLedSystemLow();
    private void UCLaserAreaScannerCommandOnSetLedSystemLow()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetLedSystemLow CB =
          new CBUCLaserAreaScannerCommandOnSetLedSystemLow(UCLaserAreaScannerCommandOnSetLedSystemLow);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetLedSystemLow());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetLedSystemHigh();
    private void UCLaserAreaScannerCommandOnSetLedSystemHigh()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetLedSystemHigh CB =
          new CBUCLaserAreaScannerCommandOnSetLedSystemHigh(UCLaserAreaScannerCommandOnSetLedSystemHigh);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetLedSystemHigh());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnBlinkLedSystem(UInt32 period, UInt32 count);
    private void UCLaserAreaScannerCommandOnBlinkLedSystem(UInt32 period, UInt32 count)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnBlinkLedSystem CB =
          new CBUCLaserAreaScannerCommandOnBlinkLedSystem(UCLaserAreaScannerCommandOnBlinkLedSystem);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CBlinkLedSystem(period, count));
        FCommandList.Execute();
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Callback - LaserRange
    //----------------------------------------------------------------------
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetPositionX();
    private void UCLaserAreaScannerCommandOnGetPositionX()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetPositionX CB =
          new CBUCLaserAreaScannerCommandOnGetPositionX(UCLaserAreaScannerCommandOnGetPositionX);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetPositionX());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetPositionX(UInt16 position);
    private void UCLaserAreaScannerCommandOnSetPositionX(UInt16 position)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetPositionX CB =
          new CBUCLaserAreaScannerCommandOnSetPositionX(UCLaserAreaScannerCommandOnSetPositionX);
        Invoke(CB, new object[] { position });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetPositionX(position));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetPositionY();
    private void UCLaserAreaScannerCommandOnGetPositionY()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetPositionY CB =
          new CBUCLaserAreaScannerCommandOnGetPositionY(UCLaserAreaScannerCommandOnGetPositionY);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetPositionY());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetPositionY(UInt16 position);
    private void UCLaserAreaScannerCommandOnSetPositionY(UInt16 position)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetPositionY CB =
          new CBUCLaserAreaScannerCommandOnSetPositionY(UCLaserAreaScannerCommandOnSetPositionY);
        Invoke(CB, new object[] { position });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetPositionY(position));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetRangeX();
    private void UCLaserAreaScannerCommandOnGetRangeX()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetRangeX CB =
          new CBUCLaserAreaScannerCommandOnGetRangeX(UCLaserAreaScannerCommandOnGetRangeX);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetRangeX());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetRangeX(UInt16 minimum, UInt16 maximum, UInt16 delta);
    private void UCLaserAreaScannerCommandOnSetRangeX(UInt16 minimum, UInt16 maximum, UInt16 delta)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetRangeX CB =
          new CBUCLaserAreaScannerCommandOnSetRangeX(UCLaserAreaScannerCommandOnSetRangeX);
        Invoke(CB, new object[] { minimum, maximum, delta });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetRangeX(minimum, maximum, delta));
        FCommandList.Execute();
      }
    }


    private delegate void CBUCLaserAreaScannerCommandOnGetRangeY();
    private void UCLaserAreaScannerCommandOnGetRangeY()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetRangeY CB =
          new CBUCLaserAreaScannerCommandOnGetRangeY(UCLaserAreaScannerCommandOnGetRangeY);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetRangeY());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetRangeY(UInt16 minimum, UInt16 maximum, UInt16 delta);
    private void UCLaserAreaScannerCommandOnSetRangeY(UInt16 minimum, UInt16 maximum, UInt16 delta)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetRangeY CB =
          new CBUCLaserAreaScannerCommandOnSetRangeY(UCLaserAreaScannerCommandOnSetRangeY);
        Invoke(CB, new object[] { minimum, maximum, delta });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetRangeY(minimum, maximum, delta));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetDelayMotionms();
    private void UCLaserAreaScannerCommandOnGetDelayMotionms()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetDelayMotionms CB =
          new CBUCLaserAreaScannerCommandOnGetDelayMotionms(UCLaserAreaScannerCommandOnGetDelayMotionms);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetDelayMotion());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetDelayMotionms(UInt32 delay);
    private void UCLaserAreaScannerCommandOnSetDelayMotionms(UInt32 delay)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetDelayMotionms CB =
          new CBUCLaserAreaScannerCommandOnSetDelayMotionms(UCLaserAreaScannerCommandOnSetDelayMotionms);
        Invoke(CB, new object[] { delay });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetDelayMotion(delay));
        FCommandList.Execute();
      }
    }


    private delegate void CBUCLaserAreaScannerCommandOnGetPulseWidthus();
    private void UCLaserAreaScannerCommandOnGetPulseWidthus()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetPulseWidthus CB =
          new CBUCLaserAreaScannerCommandOnGetPulseWidthus(UCLaserAreaScannerCommandOnGetPulseWidthus);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetPulseWidthus());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetPulseWidthus(UInt32 pulsewidth);
    private void UCLaserAreaScannerCommandOnSetPulseWidthus(UInt32 pulsewidth)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetPulseWidthus CB =
          new CBUCLaserAreaScannerCommandOnSetPulseWidthus(UCLaserAreaScannerCommandOnSetPulseWidthus);
        Invoke(CB, new object[] { pulsewidth });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetPulseWidthus(pulsewidth));
        FCommandList.Execute();
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Callback - LaserPosition
    //----------------------------------------------------------------------
    //
    private delegate void CBUCLaserAreaScannerCommandOnPulseLaserPosition(UInt16 positionx, UInt16 positiony,
                                                                          UInt32 period, UInt32 count);
    private void UCLaserAreaScannerCommandOnPulseLaserPosition(UInt16 positionx, UInt16 positiony,
                                                               UInt32 period, UInt32 count)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnPulseLaserPosition CB =
          new CBUCLaserAreaScannerCommandOnPulseLaserPosition(UCLaserAreaScannerCommandOnPulseLaserPosition);
        Invoke(CB, new object[] { positionx, positiony, period, count });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CPulseLaserPosition(positionx, positiony, period, count));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnAbortLaserPosition();
    private void UCLaserAreaScannerCommandOnAbortLaserPosition()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnAbortLaserPosition CB =
          new CBUCLaserAreaScannerCommandOnAbortLaserPosition(UCLaserAreaScannerCommandOnAbortLaserPosition);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        if (FProcessPulseStepLine is CProcessPulseStepList)
        {
          FProcessPulseStepLine.Abort();
        }
        if (FProcessPulseStepTable is CProcessPulseStepList)
        {
          FProcessPulseStepTable.Abort();
        }
        FCommandList.Clear(); //!!!!!!!!!!!!! Only Clear with Abort!!!
        FCommandList.Enqueue(new CAbortLaserPosition());
        FCommandList.Execute();
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Callback - LaserStepLine
    //----------------------------------------------------------------------
    //
    private delegate void CBUCLaserStepLineOnGetPositionX();
    private void UCLaserStepLineOnGetPositionX()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnGetPositionX CB =
          new CBUCLaserStepLineOnGetPositionX(UCLaserStepLineOnGetPositionX);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetPositionX());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnSetPositionX(UInt16 position);
    private void UCLaserStepLineOnSetPositionX(UInt16 position)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnSetPositionX CB =
          new CBUCLaserStepLineOnSetPositionX(UCLaserStepLineOnSetPositionX);
        Invoke(CB, new object[] { position });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetPositionX(position));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnGetPositionY();
    private void UCLaserStepLineOnGetPositionY()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnGetPositionY CB =
          new CBUCLaserStepLineOnGetPositionY(UCLaserStepLineOnGetPositionY);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetPositionY());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnSetPositionY(UInt16 position);
    private void UCLaserStepLineOnSetPositionY(UInt16 position)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnSetPositionY CB =
          new CBUCLaserStepLineOnSetPositionY(UCLaserStepLineOnSetPositionY);
        Invoke(CB, new object[] { position });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetPositionY(position));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnGetRangeX();
    private void UCLaserStepLineOnGetRangeX()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnGetRangeX CB =
          new CBUCLaserStepLineOnGetRangeX(UCLaserStepLineOnGetRangeX);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetRangeX());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnSetRangeX(UInt16 minimum, UInt16 maximum, UInt16 delta);
    private void UCLaserStepLineOnSetRangeX(UInt16 minimum, UInt16 maximum, UInt16 delta)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnSetRangeX CB =
          new CBUCLaserStepLineOnSetRangeX(UCLaserStepLineOnSetRangeX);
        Invoke(CB, new object[] { minimum, maximum, delta });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetRangeX(minimum, maximum, delta));
        FCommandList.Execute();
      }
    }


    private delegate void CBUCLaserStepLineOnGetRangeY();
    private void UCLaserStepLineOnGetRangeY()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnGetRangeY CB =
          new CBUCLaserStepLineOnGetRangeY(UCLaserStepLineOnGetRangeY);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetRangeY());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnSetRangeY(UInt16 minimum, UInt16 maximum, UInt16 delta);
    private void UCLaserStepLineOnSetRangeY(UInt16 minimum, UInt16 maximum, UInt16 delta)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnSetRangeY CB =
          new CBUCLaserStepLineOnSetRangeY(UCLaserStepLineOnSetRangeY);
        Invoke(CB, new object[] { minimum, maximum, delta });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetRangeY(minimum, maximum, delta));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnGetProcessCount();
    private void UCLaserStepLineOnGetProcessCount()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnGetProcessCount CB =
          new CBUCLaserStepLineOnGetProcessCount(UCLaserStepLineOnGetProcessCount);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetProcessCount());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnSetProcessCount(UInt32 processcount);
    private void UCLaserStepLineOnSetProcessCount(UInt32 processcount)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnSetProcessCount CB =
          new CBUCLaserStepLineOnSetProcessCount(UCLaserStepLineOnSetProcessCount);
        Invoke(CB, new object[] { processcount });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetProcessCount(processcount));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnGetProcessPeriodms();
    private void UCLaserStepLineOnGetProcessPeriodms()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnGetProcessPeriodms CB =
          new CBUCLaserStepLineOnGetProcessPeriodms(UCLaserStepLineOnGetProcessPeriodms);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetProcessPeriod());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnSetProcessPeriodms(UInt32 processperiod);
    private void UCLaserStepLineOnSetProcessPeriodms(UInt32 processperiod)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnSetProcessPeriodms CB =
          new CBUCLaserStepLineOnSetProcessPeriodms(UCLaserStepLineOnSetProcessPeriodms);
        Invoke(CB, new object[] { processperiod });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetProcessPeriod(processperiod));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnGetPulseWidthus();
    private void UCLaserStepLineOnGetPulseWidthus()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnGetPulseWidthus CB =
          new CBUCLaserStepLineOnGetPulseWidthus(UCLaserStepLineOnGetPulseWidthus);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetPulseWidthus());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnSetPulseWidthus(UInt32 pulsewidth);
    private void UCLaserStepLineOnSetPulseWidthus(UInt32 pulsewidth)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnSetPulseWidthus CB =
          new CBUCLaserStepLineOnSetPulseWidthus(UCLaserStepLineOnSetPulseWidthus);
        Invoke(CB, new object[] { pulsewidth });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetPulseWidthus(pulsewidth));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnGetDelayMotionms();
    private void UCLaserStepLineOnGetDelayMotionms()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnGetDelayMotionms CB =
          new CBUCLaserStepLineOnGetDelayMotionms(UCLaserStepLineOnGetDelayMotionms);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetDelayMotion());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnSetDelayMotionms(UInt32 delay);
    private void UCLaserStepLineOnSetDelayMotionms(UInt32 delay)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnSetDelayMotionms CB =
          new CBUCLaserStepLineOnSetDelayMotionms(UCLaserStepLineOnSetDelayMotionms);
        Invoke(CB, new object[] { delay });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetDelayMotion(delay));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnPulseLaserRow(UInt32 period, UInt32 count);
    private void UCLaserStepLineOnPulseLaserRow(UInt32 period, UInt32 count)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnPulseLaserRow CB =
          new CBUCLaserStepLineOnPulseLaserRow(UCLaserStepLineOnPulseLaserRow);
        Invoke(CB, new object[] { period, count });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CPulseLaserRow(period, count));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnAbortLaserRow();
    private void UCLaserStepLineOnAbortLaserRow()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnAbortLaserRow CB =
          new CBUCLaserStepLineOnAbortLaserRow(UCLaserStepLineOnAbortLaserRow);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        if (FProcessPulseStepLine is CProcessPulseStepList)
        {
          FProcessPulseStepLine.Abort();
        }
        if (FProcessPulseStepTable is CProcessPulseStepList)
        {
          FProcessPulseStepTable.Abort();
        }
        FCommandList.Clear(); //!!!!!!!!!!!!! Only Clear with Abort!!!
        FCommandList.Enqueue(new CAbortLaserMatrix());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnPulseLaserCol(UInt32 period, UInt32 count);
    private void UCLaserStepLineOnPulseLaserCol(UInt32 period, UInt32 count)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnPulseLaserCol CB =
          new CBUCLaserStepLineOnPulseLaserCol(UCLaserStepLineOnPulseLaserCol);
        Invoke(CB, new object[] { period, count });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CPulseLaserCol(period, count));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserStepLineOnAbortLaserCol();
    private void UCLaserStepLineOnAbortLaserCol()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserStepLineOnAbortLaserCol CB =
          new CBUCLaserStepLineOnAbortLaserCol(UCLaserStepLineOnAbortLaserCol);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        if (FProcessPulseStepLine is CProcessPulseStepList)
        {
          FProcessPulseStepLine.Abort();
        }
        if (FProcessPulseStepTable is CProcessPulseStepList)
        {
          FProcessPulseStepTable.Abort();
        }
        FCommandList.Clear(); //!!!!!!!!!!!!! Only Clear with Abort!!!
        FCommandList.Enqueue(new CAbortLaserMatrix());
        FCommandList.Execute();
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Callback - LaserMatrix
    //----------------------------------------------------------------------
    //
    private delegate void CBUCLaserAreaScannerCommandOnPulseLaserMatrix(UInt32 period, UInt32 count);
    private void UCLaserAreaScannerCommandOnPulseLaserMatrix(UInt32 period, UInt32 count)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnPulseLaserMatrix CB =
          new CBUCLaserAreaScannerCommandOnPulseLaserMatrix(UCLaserAreaScannerCommandOnPulseLaserMatrix);
        Invoke(CB, new object[] { period, count });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CPulseLaserMatrix(period, count));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnAbortLaserMatrix();
    private void UCLaserAreaScannerCommandOnAbortLaserMatrix()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnAbortLaserMatrix CB =
          new CBUCLaserAreaScannerCommandOnAbortLaserMatrix(UCLaserAreaScannerCommandOnAbortLaserMatrix);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        if (FProcessPulseStepLine is CProcessPulseStepList)
        {
          FProcessPulseStepLine.Abort();
        }
        if (FProcessPulseStepTable is CProcessPulseStepList)
        {
          FProcessPulseStepTable.Abort();
        }
        FCommandList.Clear(); //!!!!!!!!!!!!! Only Clear with Abort!!!
        FCommandList.Enqueue(new CAbortLaserMatrix());
        FCommandList.Execute();
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Callback - LaserSteptable
    //----------------------------------------------------------------------
    //
    private delegate void CBUCLaserAreaScannerCommandOnLoadLaserSteptable(String filename);
    private void UCLaserAreaScannerCommandOnLoadLaserSteptable(String filename)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnLoadLaserSteptable CB =
          new CBUCLaserAreaScannerCommandOnLoadLaserSteptable(UCLaserAreaScannerCommandOnLoadLaserSteptable);
        Invoke(CB, new object[] { filename });
      }
      else
      {
        CTextFile TextFile = new CTextFile();
        try
        {
          if (TextFile.OpenRead(filename))
          {
            if (TextFile.Read(out FRowCount))
            {
              if (TextFile.Read(out FColCount))
              {
                if (TextFile.Read(out FDelayMotionShortms))
                {
                  if (TextFile.Read(out FDelayMotionLongms))
                  {
                    if (TextFile.Read(out FStepOrder))
                    {
                      FLaserStepList = new CLaserStepList(FRowCount, FColCount, 
                                                          FDelayMotionShortms, FDelayMotionLongms,
                                                          FStepOrder);
                      //
                      Boolean EndOfFile = false;
                      while (!EndOfFile)
                      {
                        UInt32 PX, PY, PPms, PC, DMms;
                        EndOfFile = !TextFile.Read(out PX);
                        if (!EndOfFile)
                        {
                          EndOfFile = !TextFile.Read(out PY);
                          if (!EndOfFile)
                          {
                            EndOfFile = !TextFile.Read(out PPms);
                            if (!EndOfFile)
                            {
                              EndOfFile = !TextFile.Read(out PC);
                              if (!EndOfFile)
                              {
                                if (!TextFile.Read(out DMms))
                                { // default == long
                                  DMms = FDelayMotionLongms;
                                }
                                CLaserStep LaserStep = new CLaserStep(PX, PY, PPms, PC, DMms);
                                EndOfFile = !(LaserStep is CLaserStep);
                                if (!EndOfFile)
                                {
                                  FLaserStepList.Add(LaserStep);
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }                  
                }
              }
            }
          }
          // NC NC NC NC NC NC NC FProcessPulseStepList.SetLaserStepList(FLaserStepList);
          FUCLaserAreaScannerTable.SetLaserStepList(FLaserStepList);
          //FUCLaserAreaScannerMatrix.SetLaserPoints(FRowCount, FColCount, FLaserPoints,
          //0, 1);
        }
        catch (Exception e)
        {
          Console.WriteLine(e.Message);
        }
        finally
        {
          TextFile.Close();
        }
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Callback - LaserImage
    //----------------------------------------------------------------------
    //
    private delegate void CBUCLaserAreaScannerCommandOnLoadLaserImage(Bitmap bitmap);
    private void UCLaserAreaScannerCommandOnLoadLaserImage(Bitmap bitmap)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnLoadLaserImage CB =
          new CBUCLaserAreaScannerCommandOnLoadLaserImage(UCLaserAreaScannerCommandOnLoadLaserImage);
        Invoke(CB, new object[] { bitmap });
      }
      else
      { // Create / Execute Command
        // Start Thread, analyse Pixels from Bitmap and call CPulseLaserImage(x, y, p, c)!
        // FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnPulseLaserImage();
    private void UCLaserAreaScannerCommandOnPulseLaserImage()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnPulseLaserImage CB =
          new CBUCLaserAreaScannerCommandOnPulseLaserImage(UCLaserAreaScannerCommandOnPulseLaserImage);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command;
        // Call from Thread: FCommandList.Enqueue(new CPulseLaserImage());
        FProcessPulseStepTable.Start();
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnAbortLaserImage();
    private void UCLaserAreaScannerCommandOnAbortLaserImage()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnAbortLaserImage CB =
          new CBUCLaserAreaScannerCommandOnAbortLaserImage(UCLaserAreaScannerCommandOnAbortLaserImage);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        if (FProcessPulseStepLine is CProcessPulseStepList)
        {
          FProcessPulseStepLine.Abort();
        }
        if (FProcessPulseStepTable is CProcessPulseStepList)
        {
          FProcessPulseStepTable.Abort();
        }
        FCommandList.Clear(); //!!!!!!!!!!!!! Only Clear with Abort!!!
        FCommandList.Enqueue(new CAbortLaserImage());
        FCommandList.Execute();
      }
    }

    
    
    
    
    //// 
    //private void PulseStepLineOnExecutionStart()
    //{
    //  FLaserStepIndex = 0;
    //  FLaserStepIndexRefresh = 0;
    //  if (!(FLaserStepLine is CLaserStepList))
    //  {
    //    FProcessPulseStepLine.Abort();
    //  }
    //}
    private Boolean PulseStepLineOnExecutionBusy()
    {
      if (FLaserStepList is CLaserStepList)
      {
        if (0 == FCommandList.Count)
        {
          if (FLaserStepList.Count <= FLaserStepIndex)
          {
            return false;
          }
          CLaserStep LS = FLaserStepList[(int)FLaserStepIndex];
          if (LS is CLaserStep)
          {
            UInt32 PC = LS.PulseCount;
            if  (FUCLaserAreaScannerTable.SkipZeroPulses)
            {
              if (0 < PC)
              {
                UInt16 PX = (UInt16)LS.PositionX;
                UInt16 PY = (UInt16)LS.PositionY;
                UInt32 PP = LS.PulsePeriodms;
                UInt32 DM = LS.DelayMotionms;
                if (FZeroPulseSkippedStepLine)
                { // Extend DelayMotion to longest Value:
                  DM = FLaserStepList.DelayMotionLongms;
                  FZeroPulseSkippedStepLine = false;
                }
                CCommand Command = new CPulseLaserImage(PX, PY, PP, PC, DM);
                FCommandList.Enqueue(Command);
                FCommandList.Execute();
              }
              else
              {
                FZeroPulseSkippedStepLine = true;
              }
            }
            else
            {
              UInt16 PX = (UInt16)LS.PositionX;
              UInt16 PY = (UInt16)LS.PositionY;
              UInt32 PP = LS.PulsePeriodms;
              UInt32 DM = LS.DelayMotionms;
              CCommand Command = new CPulseLaserImage(PX, PY, PP, PC, DM);
              FCommandList.Enqueue(Command);
              FCommandList.Execute();
            }
          }
          //
          FLaserStepIndexRefresh++;
          if (1000 <= FLaserStepIndexRefresh)
          {
            FLaserStepIndexRefresh = 0;
            FUCLaserAreaScannerTable.SelectLaserStep(FLaserStepIndex);
            FUCLaserAreaScannerMatrix.SelectLaserStep(FLaserStepIndex);
          }
          //
          FLaserStepIndex++;
        }
        return true;
      }
      return false;
    }

    private void PulseStepLineOnExecutionEnd()
    {
      if (FLaserStepList is CLaserStepList)
      {
        FLaserStepIndex = (UInt32)(FLaserStepList.Count - 1);
        FUCLaserAreaScannerTable.SelectLaserStep(FLaserStepIndex);
        FUCLaserAreaScannerMatrix.SelectLaserStep(FLaserStepIndex);
      }
    }

    private void PulseStepLineOnExecutionAbort()
    {
      if (FLaserStepList is CLaserStepList)
      {
        FUCLaserAreaScannerTable.SelectLaserStep(FLaserStepIndex);
        FUCLaserAreaScannerMatrix.SelectLaserStep(FLaserStepIndex);
      }
    }









    //// 
    //private void PulseStepTableOnExecutionStart()
    //{
    //  FLaserStepIndex = 0;
    //  FLaserStepIndexRefresh = 0;
    //  if (!(FLaserStepList is CLaserStepList))
    //  {
    //    FProcessPulseStepList.Abort();
    //  }
    //}
    private Boolean PulseStepTableOnExecutionBusy()
    {
      if (FLaserStepList is CLaserStepList)
      {
        if (0 == FCommandList.Count)
        {
          if (FLaserStepList.Count <= FLaserStepIndex)
          {
            return false;
          }
          CLaserStep LS = FLaserStepList[(int)FLaserStepIndex];
          if (LS is CLaserStep)
          {
            UInt32 PC = LS.PulseCount;
            if (FUCLaserAreaScannerTable.SkipZeroPulses)
            {
              if (0 < PC)
              {
                UInt16 PX = (UInt16)LS.PositionX;
                UInt16 PY = (UInt16)LS.PositionY;
                UInt32 PP = LS.PulsePeriodms;
                UInt32 DM = LS.DelayMotionms;
                if (FZeroPulseSkippedStepTable)
                { // Extend DelayMotion to longest Value:
                  DM = FLaserStepList.DelayMotionLongms;
                  FZeroPulseSkippedStepTable = false;
                }
                CCommand Command = new CPulseLaserImage(PX, PY, PP, PC, DM);
                FCommandList.Enqueue(Command);
                FCommandList.Execute();
              }
              else
              {
                FZeroPulseSkippedStepTable = true;
              }
            }
            else
            {
              UInt16 PX = (UInt16)LS.PositionX;
              UInt16 PY = (UInt16)LS.PositionY;
              UInt32 PP = LS.PulsePeriodms;
              UInt32 DM = LS.DelayMotionms;
              CCommand Command = new CPulseLaserImage(PX, PY, PP, PC, DM);
              FCommandList.Enqueue(Command);
              FCommandList.Execute();
            }
          }
          //
          FLaserStepIndexRefresh++;
          if (1000 <= FLaserStepIndexRefresh)
          {
            FLaserStepIndexRefresh = 0;
            FUCLaserAreaScannerTable.SelectLaserStep(FLaserStepIndex);
            FUCLaserAreaScannerMatrix.SelectLaserStep(FLaserStepIndex);
          }
          //
          FLaserStepIndex++;
        }
        return true;
      }
      return false;
    }

    private void PulseStepTableOnExecutionEnd()
    {
      if (FLaserStepList is CLaserStepList)
      {
        FLaserStepIndex = (UInt32)(FLaserStepList.Count - 1);
        FUCLaserAreaScannerTable.SelectLaserStep(FLaserStepIndex);
        FUCLaserAreaScannerMatrix.SelectLaserStep(FLaserStepIndex);
      }
    }

    private void PulseStepTableOnExecutionAbort()
    {
      if (FLaserStepList is CLaserStepList)
      {
        FUCLaserAreaScannerTable.SelectLaserStep(FLaserStepIndex);
        FUCLaserAreaScannerMatrix.SelectLaserStep(FLaserStepIndex);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          //tbcMain.SelectedIndex = 1;
          //FUCEditorLaserStepFile.LoadFromFile("Matrix18002200100.lsf.txt");
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          //FUdpTextDeviceClient.Preset("GSV");
          return true;
        case 7:
          return true;
        case 8:
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! First (Client -> Server)");
          return true;
        case 9:
          //FUdpTextDeviceClient.Preset("GHV");
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! Second (Client -> Server)");
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }

  }
}
