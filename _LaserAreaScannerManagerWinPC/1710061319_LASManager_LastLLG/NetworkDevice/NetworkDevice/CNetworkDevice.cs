﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NetworkDevice
{ //
  //--------------------------------------------------------------------------
  //  Segment - Type
  //--------------------------------------------------------------------------
  //
  public delegate void DOnSendMessage(String message);
  public delegate void DOnMessageReceived(String message);
  public delegate void DOnActivateDatagram();
  //
  public class CNetworkDevice
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    //--------------------------------------------------------------------------
    //  Segment - UDP - Text Datagram
    //--------------------------------------------------------------------------
    //  UDP - Text - Message
    public const UInt16 PORT_UDPTEXT_MESSAGE_SERVER_RECEIVER = 12111;
    public const UInt16 PORT_UDPTEXT_MESSAGE_SERVER_TRANSMITTER = 12112;
    public const UInt16 PORT_UDPTEXT_MESSAGE_CLIENT_RECEIVER = 12112;
    public const UInt16 PORT_UDPTEXT_MESSAGE_CLIENT_TRANSMITTER = 12111;
    //  UDP - Text - Array
    public const UInt16 PORT_UDPTEXT_ARRAY_SERVER_RECEIVER = 12121;
    public const UInt16 PORT_UDPTEXT_ARRAY_SERVER_TRANSMITTER = 12122;
    public const UInt16 PORT_UDPTEXT_ARRAY_CLIENT_RECEIVER = 12122;
    public const UInt16 PORT_UDPTEXT_ARRAY_CLIENT_TRANSMITTER = 12121;
    //
    //  Segment - UDP - Binary Datagram
    //  UDP - Binary - Message
    public const UInt16 PORT_UDPBINARY_MESSAGE_SERVER_RECEIVER = 12211;
    public const UInt16 PORT_UDPBINARY_MESSAGE_SERVER_TRANSMITTER = 12212;
    public const UInt16 PORT_UDPBINARY_MESSAGE_CLIENT_RECEIVER = 12212;
    public const UInt16 PORT_UDPBINARY_MESSAGE_CLIENT_TRANSMITTER = 12211;
    //  UDP - Binary - Vector
    public const UInt16 PORT_UDPBINARY_ARRAY_SERVER_RECEIVER = 12221;
    public const UInt16 PORT_UDPBINARY_ARRAY_SERVER_TRANSMITTER = 12222;
    public const UInt16 PORT_UDPBINARY_ARRAY_CLIENT_RECEIVER = 12222;
    public const UInt16 PORT_UDPBINARY_ARRAY_CLIENT_TRANSMITTER = 12221;
    //  UDP - Binary - Image
    public const UInt16 PORT_UDPBINARY_IMAGE_SERVER_RECEIVER = 12231;
    public const UInt16 PORT_UDPBINARY_IMAGE_SERVER_TRANSMITTER = 12232;
    public const UInt16 PORT_UDPBINARY_IMAGE_CLIENT_RECEIVER = 12232;
    public const UInt16 PORT_UDPBINARY_IMAGE_CLIENT_TRANSMITTER = 12231;
    //  Segment - UDP - Text Datagram
    //
    //--------------------------------------------------------------------------
    //  Segment - TCP - Text Datagram
    //--------------------------------------------------------------------------
    //  UDP - Text - Message
    public const UInt16 PORT_TCPTEXT_MESSAGE_SERVER_RECEIVER = 13111;
    public const UInt16 PORT_TCPTEXT_MESSAGE_SERVER_TRANSMITTER = 13112;
    public const UInt16 PORT_TCPTEXT_MESSAGE_CLIENT_RECEIVER = 13112;
    public const UInt16 PORT_TCPTEXT_MESSAGE_CLIENT_TRANSMITTER = 13111;
    //
    //--------------------------------------------------------------------------
    //  Segment - TCP - Binary Datagram
    //--------------------------------------------------------------------------
    //
  }
}
