﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Task;
using CommandProtocol;
using CPLaserAreaScanner;
//
namespace LASManager
{
  public class CProcessPulseImage
  {
    const int LOCATIONX = 0;
    const int LOCATIONY = 1;
    const int PERIODMS = 2;
    const int PULSECOUNT = 3;
    //const int COUNT_POINTS = 9;
    //const int SIZE_DIMENSION = 2;
    //
    private CProcess FProcess;
    private CCommandList FCommandList;
    private UInt32 FPointIndex;
    UInt16[,] FLaserPoints;
    public CProcessPulseImage()
    {
      FProcess = new CProcess("PulseImage",
                              OnExecutionStart,
                              OnExecutionBusy,
                              OnExecutionEnd,
                              OnExecutionAbort);
      //FPoints = new UInt16[COUNT_POINTS, SIZE_DIMENSION];
      FPointIndex = 0;
    }


    public void SetLaserPoints(UInt16[,] laserpoints)
    {
      FLaserPoints = laserpoints;
    }

    public Boolean Start(CCommandList commandlist)
    {
      FCommandList = commandlist;
      if (FLaserPoints is UInt16[,])
      {        
        //if (!FProcess.IsStarted())
        //{
        //  int XI = 0;
        //  int YI = 0;
        //  for (int PI = 0; PI < COUNT_POINTS; PI++)
        //  {
        //    FPoints[PI, X] = (UInt16)(1800 + (XI % 3) * 200);
        //    FPoints[PI, Y] = (UInt16)(1800 + (YI % 3) * 200);
        //    XI++;
        //    if (2 == (PI % 3))
        //    {
        //      YI++;
        //    }
        //  }
        FPointIndex = 0;
        return FProcess.Start();
      }
      return false;
    }

    public Boolean Abort()
    {
      if (FProcess.IsActive())
      {
        return FProcess.Abort();
      }
      return false;
    }

    protected void OnExecutionStart(RTaskData data)
    {
      FPointIndex = 0;
    }
    protected Boolean OnExecutionBusy(RTaskData data)
    {
      if (0 == FCommandList.Count)
      {
        if (FLaserPoints.GetLength(0) <= FPointIndex)
        {
          return false;
        }
        FCommandList.Enqueue(new CPulseLaserImage(FLaserPoints[FPointIndex, LOCATIONX],
                                            FLaserPoints[FPointIndex, LOCATIONY],
                                            (Int32)(1000 * (UInt32)FLaserPoints[FPointIndex, PERIODMS]),
                                            FLaserPoints[FPointIndex, PULSECOUNT]));
        FPointIndex++;
      }
      return true;
    }
    protected void OnExecutionEnd(RTaskData data)
    {
    }
    protected void OnExecutionAbort(RTaskData data)
    {

    }


  }
}
