﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CPLaserAreaScanner
{
  public enum EStateProcess
  { // Common
    Undefined = -1,
    Idle = 0,
    Welcome = 1,
    GetHelp = 2,
    GetProgramHeader = 3,
    GetSoftwareVersion = 4,
    GetHardwareVersion = 5,
    GetProcessCount = 6,
    SetProcessCount = 7,
    GetProcessPeriod = 8,
    SetProcessPeriod = 9,
    StopProcessExecution = 10,
    // LedSystem
    GetLedSystem = 11,
    LedSystemOn = 12,
    LedSystemOff = 13,
    BlinkLedSystem = 14,
    // LaserPosition
    PulseLaserPosition = 15,
    AbortLaserPosition = 16,
    // LaserRange
    GetPositionX = 17,
    GetRangeX = 18,
    SetRangeX = 19,
    GetPositionY = 20,
    GetRangeY = 21,
    SetRangeY = 22,
    GetDelayMotion = 23,
    SetDelayMotion = 24,
    // LaserMatrix
    PulseLaserMatrix = 25,
    AbortLaserMatrix = 26,
    // LaserImage
    PulseLaserImage = 27,
    AbortLaserImage = 28
  };

  public enum EStateLed
  {
    Undefined = -1,
    Off = 0,
    On = 1
  };

  //
  //--------------------------------------------------------------------------
  //  Segment - Definition - Callback
  //--------------------------------------------------------------------------
  //
  public delegate void DOnGetHelp();
  public delegate void DOnGetProgramHeader();
  public delegate void DOnGetHardwareVersion();
  public delegate void DOnGetSoftwareVersion();
  public delegate void DOnGetLedSystem();
  public delegate void DOnSwitchLedSystemOn();
  public delegate void DOnSwitchLedSystemOff();
  public delegate void DOnBlinkLedSystem(Int32 period, Int32 count);
  public delegate void DOnSetProcessCount(Int32 count);
  public delegate void DOnSetProcessPeriod(Int32 period);
  public delegate void DOnStopProcessExecution();
  public delegate void DOnGetLaserAreaScannerChannel(Int32 channel);
  public delegate void DOnGetLaserAreaScannerInterval(Int32 channellow, Int32 channelhigh);
  public delegate void DOnGetAllLaserAreaScanners();
  public delegate void DOnRepeatLaserAreaScannerChannel(Int32 channel);
  public delegate void DOnRepeatLaserAreaScannerInterval(Int32 channellow, Int32 channelhigh);
  public delegate void DOnRepeatAllLaserAreaScanners();
  //
  public class CDefine
  {
    public const String PROMPT_NEWLINE = "\r\n";
    public const String PROMPT_INPUT = ">";
    public const String PROMPT_ANSWER = "#";
    public const String PROMPT_RESPONSE = "!";
    public const String PROMPT_EVENT = ":";
    //
    public const Char SEPARATOR_SPACE = ' ';
    public const Char SEPARATOR_MARK = '!';
    public const Char SEPARATOR_COLON = ':';
    public const Char SEPARATOR_DOT = '.';
    public const Char SEPARATOR_GREATER = '>';
    public const Char SEPARATOR_NUMBER = '#';
    //
    public const String TOKEN_SPACE = " ";
    public const String TOKEN_MARK = "!";
    public const String TOKEN_COLON = ":";
    public const String TOKEN_DOT = ".";
    public const String TOKEN_GREATER = ">";
    public const String TOKEN_NUMBER = "#";
    //
    // Common
    public const String SHORT_H = "H";
    public const String SHORT_GPH = "GPH";
    public const String SHORT_GSV = "GSV";
    public const String SHORT_GHV = "GHV";
    public const String SHORT_GPC = "GPC";
    public const String SHORT_SPC = "SPC";
    public const String SHORT_GPP = "GPP";
    public const String SHORT_SPP = "SPP";
    public const String SHORT_SPE = "SPE";
    public const String SHORT_A   = "A";
    // LedSystem                    
    public const String SHORT_GLS = "GLS";
    public const String SHORT_LSH = "LSH";
    public const String SHORT_LSL = "LSL";
    public const String SHORT_BLS = "BLS";
    //
    // LaserPosition
    public const String SHORT_PLP = "PLP";
    public const String SHORT_ALP = "ALP";
    //
    // LaserRange
    public const String SHORT_GPX = "GPX";
    public const String SHORT_GRX = "GRX";
    public const String SHORT_SRX = "SRX";
    public const String SHORT_GPY = "GPY";
    public const String SHORT_GRY = "GRY";
    public const String SHORT_SRY = "SRY";
    public const String SHORT_GDM = "GDM";
    public const String SHORT_SDM = "SDM";
    //
    // LaserMatrix
    public const String SHORT_PLM = "PLM";
    public const String SHORT_ALM = "ALM";
    //
    // LaserImage
    public const String SHORT_PLI = "PLI";
    public const String SHORT_ALI = "ALI";
    //
  }
}
