﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
using System.Threading;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using Uart;
using UCUartTerminal;
using CommandProtocol;
using CPLaserAreaScanner;
using UCLaserAreaScanner;
//
namespace LASManager
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //


  public partial class FormClient : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    private const String INIT_DEVICETYPE = "LaserAreaScanner";
    private const String INIT_DEVICENAME = "LAS";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    private const String NAME_DEBUG = "Debug";
    private const Boolean INIT_DEBUG = true;
    private const String NAME_SHOWPAGE = "ShowPage";
    private const Int32 INIT_SHOWPAGE = 0;

    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    private CUart FUart;
    private CCommandList FCommandList;
    private Int32 FRxdCount = 0;
    private Int32 FRxdIndex = 0;
    private String FRxdLine = "";
    private Boolean FResult = false;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      //####################################################################################
      // Init - Instance - Client
      //####################################################################################
      //
      FUart = new CUart();
      FUart.SetOnErrorDetected(UartOnErrorDetected);
      FUart.SetOnLineReceived(UartOnLineReceived);
      FUart.SetOnLineTransmitted(UartOnLineTransmitted);
      FUart.SetOnPinChanged(UartOnPinChanged);
      FUart.SetOnTextReceived(UartOnTextReceived);
      FUart.SetOnTextTransmitted(UartOnTextTransmitted);
      //
      // already exist FUCUartTerminal = new CUCUartTerminal....
      FUCUartTerminal.SetNotifier(FUCNotifier);
      FUCUartTerminal.SetOnTerminalOpen(UCUartTerminalOnTerminalOpen);
      FUCUartTerminal.SetOnTerminalClose(UCUartTerminalOnTerminalClose);
      FUCUartTerminal.SetOnTransmitText(UCUartTerminalOnTransmitText);
      //
      FCommandList = new CCommandList();
      // -> Main! FCommandList.SetUdpTextDeviceClient(FUdpTextDeviceClient);
      FCommandList.SetNotifier(FUCNotifier);
      FCommandList.SetOnExecutionStart(CommandListOnExecutionStart);
      FCommandList.SetOnExecutionBusy(CommandListOnExecutionBusy);
      FCommandList.SetOnExecutionEnd(CommandListOnExecutionEnd);
      FCommandList.SetOnExecutionAbort(CommandListOnExecutionAbort);
      //
      FUCLaserAreaScannerCommand.SetOnGetHelp(UCLaserAreaScannerCommandOnGetHelp);
      FUCLaserAreaScannerCommand.SetOnGetProgramHeader(UCLaserAreaScannerCommandOnGetProgramHeader);
      FUCLaserAreaScannerCommand.SetOnGetHardwareVersion(UCLaserAreaScannerCommandOnGetHardwareVersion);
      FUCLaserAreaScannerCommand.SetOnGetSoftwareVersion(UCLaserAreaScannerCommandOnGetSoftwareVersion);
      FUCLaserAreaScannerCommand.SetOnGetLedSystem(UCLaserAreaScannerCommandOnGetLedSystem);
      FUCLaserAreaScannerCommand.SetOnSwitchLedSystemOn(UCLaserAreaScannerCommandOnSwitchLedSystemOn);
      FUCLaserAreaScannerCommand.SetOnSwitchLedSystemOff(UCLaserAreaScannerCommandOnSwitchLedSystemOff);
      FUCLaserAreaScannerCommand.SetOnBlinkLedSystem(UCLaserAreaScannerCommandOnBlinkLedSystem);
      FUCLaserAreaScannerCommand.SetOnSetProcessCount(UCLaserAreaScannerCommandOnSetProcessCount);
      FUCLaserAreaScannerCommand.SetOnSetProcessPeriod(UCLaserAreaScannerCommandOnSetProcessPeriod);
      FUCLaserAreaScannerCommand.SetOnStopProcessExecution(UCLaserAreaScannerCommandOnStopProcessExecution);
      //.SetOnGetTemperatureChannel(UCLaserAreaScannerCommandOnGetTemperatureChannel);
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      tbcMain.SelectedIndex = 0;
      //
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FUart.Close();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      BValue = INIT_DEBUG;
      Result &= initdata.ReadBoolean(NAME_DEBUG, out BValue, BValue);
      cbxDebug.Checked = BValue;
      //
      Int32 IValue = INIT_SHOWPAGE;
      Result &= initdata.ReadInt32(NAME_SHOWPAGE, out IValue, IValue);
      tbcMain.SelectedIndex = IValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      Result &= initdata.WriteBoolean(NAME_DEBUG, cbxDebug.Checked);
      Result &= initdata.WriteInt32(NAME_SHOWPAGE, tbcMain.SelectedIndex);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //

    //
    //
    //#########################################################################
    //  Segment - Callback - CommandList
    //#########################################################################
    //
    private delegate void CBCommandListOnExecutionStart(RTaskData data);
    private void CommandListOnExecutionStart(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionStart CB = new CBCommandListOnExecutionStart(CommandListOnExecutionStart);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionStart", data.Name));
        }
        CCommand Command = FCommandList.Peek();
        if (Command is CCommand)
        {
          String CommandText = Command.GetCommandText();
          FUart.WriteLine(CommandText);
        }
      }
    }
    private delegate Boolean CBCommandListOnExecutionBusy(RTaskData data);
    private Boolean CommandListOnExecutionBusy(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionBusy CB = new CBCommandListOnExecutionBusy(CommandListOnExecutionBusy);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionBusy", data.Name));
        }
      }
      return false;
    }
    private delegate void CBCommandListOnExecutionEnd(RTaskData data);
    private void CommandListOnExecutionEnd(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionEnd CB = new CBCommandListOnExecutionEnd(CommandListOnExecutionEnd);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionEnd", data.Name));
        }
      }
    }
    private delegate void CBCommandListOnExecutionAbort(RTaskData data);
    private void CommandListOnExecutionAbort(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionAbort CB = new CBCommandListOnExecutionAbort(CommandListOnExecutionAbort);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionAbort", data.Name));
        }
      }
    }


    //
    //###########################################################################################
    //  Segment - Analyse - Common
    //###########################################################################################
    //
    private delegate void CBAnalyseDateTimePrompt();
    private void AnalyseDateTimePrompt()
    {
      if (this.InvokeRequired)
      {
        CBAnalyseDateTimePrompt CB = new CBAnalyseDateTimePrompt(AnalyseDateTimePrompt);
        Invoke(CB, new object[] { });
      }
      else
      {
        if (14 <= FRxdLine.Length)
        { // test "!hh:mm:ss.mmm>"
          if ((CDefine.SEPARATOR_MARK == FRxdLine[0]) && (CDefine.SEPARATOR_GREATER == FRxdLine[13]))
          {
            Char[] Separators = new Char[] { CDefine.SEPARATOR_MARK, 
                                             CDefine.SEPARATOR_COLON, 
                                             CDefine.SEPARATOR_DOT, 
                                             CDefine.SEPARATOR_GREATER };
            String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
            if (3 < Tokens.Length)
            {
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("DATETIME[{0}:{1}:{2}.{3}]",
                                                Tokens[0], Tokens[1], Tokens[2], Tokens[3]));
              }
              // debug Console.WriteLine("VVV" + rxdline + "VVV");
              FRxdLine = FRxdLine.Substring(14);
              // debug Console.WriteLine("NNN" + rxdline + "NNN");
              // Event -> NO command.SignalTextReceived();
              
            }
          }
        }
      }
    }

    private delegate void CBAnalyseStateLaserAreaScanner();
    private void AnalyseStateLaserAreaScanner()
    {
      if (this.InvokeRequired)
      {
        CBAnalyseStateLaserAreaScanner CB = 
          new CBAnalyseStateLaserAreaScanner(AnalyseStateLaserAreaScanner);
        Invoke(CB, new object[] { });
      }
      else
      {
        if (7 <= FRxdLine.Length)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          Int32 TL = Tokens.Length;
          if (4 <= TL)
          {
            if ((CDefine.PROMPT_EVENT == Tokens[0]) && true) //!!!!!!!!!!!!!!!!!!!!(CGetStateLaserAreaScanner.HEADER == Tokens[1]))
            {
              Int32 SMTC;
              Int32.TryParse(Tokens[2], out SMTC);
              Int32 SubState;
              Int32.TryParse(Tokens[3], out SubState);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("STATEMULTITEMPERATURERCONTROLLER[{0}|{1}]", SMTC, SubState));
              }
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetState(Tokens);
              // deleting STP-Information from RxdLine
              Int32 S = 3 + Tokens[0].Length + Tokens[1].Length + Tokens[2].Length + Tokens[3].Length;
              FRxdLine = FRxdLine.Substring(S);
            }
          }
        }
      }
    }

    private delegate void CBAnalyseTemperatureValues();
    private void AnalyseTemperatureValues()
    {
      if (this.InvokeRequired)
      {
        CBAnalyseTemperatureValues CB = new CBAnalyseTemperatureValues(AnalyseTemperatureValues);
        Invoke(CB, new object[] { });
      }
      else
      {
        if (15 <= FRxdLine.Length)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          Int32 TL = Tokens.Length;
          if (3 <= TL)
          { // <: T[0-7] +020.1> ...
            if ((CDefine.PROMPT_EVENT == Tokens[0]) && ("T[" == Tokens[1].Substring(0, 2)))
            {
              Char[] SubSeparators = new Char[] { '[', ']', '-' };
              String[] SubTokens = Tokens[1].Split(SubSeparators, StringSplitOptions.RemoveEmptyEntries);
              if (3 == SubTokens.Length)
              {
                if ("T" == SubTokens[0])
                {
                  Int32 CL;
                  Int32.TryParse(SubTokens[1], out CL);
                  Int32 CH;
                  Int32.TryParse(SubTokens[2], out CH);
                  if (cbxDebug.Checked)
                  {
                    FUCNotifier.Write(String.Format("TEMPERATURERVALUES[{0}-{1}]", CL, CH));
                  }
                  Double[] Temperatures = new Double[8];
                  for (Int32 TI = 0; TI < 8; TI++)
                  {
                    if ((CL <= TI) && (TI <= CH))
                    {
                      Double DValue;
                      if (Double.TryParse(Tokens[2 + TI], out DValue))
                      {
                        Temperatures[TI] = DValue;
                      }
                      else
                      {
                        //Temperatures[TI] = -999.9;
                      }
                    }
                    else
                    {
                      Temperatures[TI] = 0.0;
                    }
                  }
                  //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommandDisplay.SetTemperatureValues(CL, CH, Temperatures);
                  // deleting TValues-Information from RxdLine                  
                  FRxdLine = "";
                }
              }
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - Common
    //###########################################################################################
    //
    private delegate void CBAnalyseGetHelp(CCommand command);//, String rxdline);
    private void AnalyseGetHelp(CCommand command)//, String rxdline)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetHelp CB = new CBAnalyseGetHelp(AnalyseGetHelp);
        Invoke(CB, new object[] { command });//, rxdline });
      }
      else
      {
        FResult = false;
        if (command is CGetHelp)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CGetHelp.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetHelp.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.ClearMessages();
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.AddMessageLine("");
              FRxdIndex = 0;
              FResult = true;
            }
          }
          if (1 <= Tokens.Length)
          {
            if (CDefine.SEPARATOR_NUMBER == Tokens[0][0])
            {
              FRxdIndex++;
              if (cbxDebug.Checked)
              {
                String Line = String.Format("HELP[{0}|{1}]<{2}>", FRxdIndex, FRxdCount, FRxdLine);
                FUCNotifier.Write(Line);
              }
              if (FRxdCount <= FRxdIndex)
              {
                command.SignalTextReceived();
                //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine);
              }
              else
              {
                //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine + "\r\n");
              }
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetProgramHeader(CCommand command);
    private void AnalyseGetProgramHeader(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetProgramHeader CB = new CBAnalyseGetProgramHeader(AnalyseGetProgramHeader);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetProgramHeader)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CGetProgramHeader.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetProgramHeader.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.ClearMessages();
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.AddMessageLine("");
              FRxdIndex = 0;
              FResult = true;
            }
          }
          if (1 <= Tokens.Length)
          {
            if (CDefine.SEPARATOR_NUMBER == Tokens[0][0])
            {
              FRxdIndex++;
              if (cbxDebug.Checked)
              {
                String Line = String.Format("PROGRAMHEADER[{0}|{1}]<{2}>", FRxdIndex, FRxdCount, FRxdLine);
                FUCNotifier.Write(Line);
              }
              if (FRxdCount <= FRxdIndex)
              {
                command.SignalTextReceived();
                //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine);
              }
              else
              {
                //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine + "\r\n");
              }
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetHardwareVersion(CCommand command);
    private void AnalyseGetHardwareVersion(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetHardwareVersion CB = new CBAnalyseGetHardwareVersion(AnalyseGetHardwareVersion);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetHardwareVersion)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CGetHardwareVersion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetHardwareVersion.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FRxdIndex = 0;
              FResult = true;
            }
            else
              if (CDefine.TOKEN_NUMBER == Tokens[0])
              {
                FRxdIndex++;
                if (cbxDebug.Checked)
                {
                  FUCNotifier.Write(String.Format("HARDWAREVERSION[{0}]", Tokens[2]));
                }
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetHardwareVersion(Tokens[2]);
              if (FRxdCount <= FRxdIndex) command.SignalTextReceived();
                FResult = true;
              }
           
          }
        }
      }
    }

    private delegate void CBAnalyseGetSoftwareVersion(CCommand command);
    private void AnalyseGetSoftwareVersion(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetSoftwareVersion CB = new CBAnalyseGetSoftwareVersion(AnalyseGetSoftwareVersion);
        Invoke(CB, new object[] { command }); 
      }
      else
      {
        FResult = false;
        if (command is CGetSoftwareVersion)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CGetSoftwareVersion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetSoftwareVersion.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FRxdIndex = 0;
              FResult = true;
            }
            else
              if (CDefine.TOKEN_NUMBER == Tokens[0])
              {
                FRxdIndex++;
                if (cbxDebug.Checked)
                {
                  FUCNotifier.Write(String.Format("SOFTWAREVERSION[{0}]", Tokens[2]));
                }
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetSoftwareVersion(Tokens[2]);
              if (FRxdCount <= FRxdIndex) command.SignalTextReceived();
                FResult = true;
              }
          }
        }
      }
    }

    private delegate void CBAnalyseGetProcessCount(CCommand command);
    private void AnalyseGetProcessCount(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetProcessCount CB = new CBAnalyseGetProcessCount(AnalyseGetProcessCount);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetProcessCount)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CGetProcessCount.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetProcessCount.HEADER + " expected");
              }
              Int32 ProcessCount;
              Int32.TryParse(Tokens[2], out ProcessCount);
              //!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPROCESSCOUNT[{0}]", ProcessCount));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetProcessCount(CCommand command);
    private void AnalyseSetProcessCount(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetProcessCount CB = new CBAnalyseSetProcessCount(AnalyseSetProcessCount);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetProcessCount)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CSetProcessCount.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetProcessCount.HEADER + " expected");
              }
              Int32 ProcessCount;
              Int32.TryParse(Tokens[2], out ProcessCount);
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPROCESSCOUNT[{0}]", ProcessCount));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseGetProcessPeriod(CCommand command);
    private void AnalyseGetProcessPeriod(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetProcessPeriod CB = new CBAnalyseGetProcessPeriod(AnalyseGetProcessPeriod);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetProcessPeriod)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CGetProcessPeriod.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetProcessPeriod.HEADER + " expected");
              }
              Int32 ProcessPeriod;
              Int32.TryParse(Tokens[2], out ProcessPeriod);
              //!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetProcessPeriod(ProcessPeriod);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPROCESSPERIOD[{0}]", ProcessPeriod));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetProcessPeriod(CCommand command);
    private void AnalyseSetProcessPeriod(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetProcessPeriod CB = new CBAnalyseSetProcessPeriod(AnalyseSetProcessPeriod);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetProcessPeriod)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CSetProcessPeriod.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetProcessPeriod.HEADER + " expected");
              }
              Int32 ProcessPeriod;
              Int32.TryParse(Tokens[2], out ProcessPeriod);
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetProcessPeriod(ProcessPeriod);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPROCESSPERIOD[{0}]", ProcessPeriod));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseStopProcessExecution(CCommand command);
    private void AnalyseStopProcessExecution(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseStopProcessExecution CB = new CBAnalyseStopProcessExecution(AnalyseStopProcessExecution);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CStopProcessExecution)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CStopProcessExecution.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CStopProcessExecution.HEADER + " expected");
              }
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("STOPPROCESSEXECUTION");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseAbortProcessExecution(CCommand command);
    private void AnalyseAbortProcessExecution(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseAbortProcessExecution CB = new CBAnalyseAbortProcessExecution(AnalyseAbortProcessExecution);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CAbortProcessExecution)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CAbortProcessExecution.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CAbortProcessExecution.HEADER + " expected");
              }
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("ABORTPROCESSEXECUTION");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LedSystem
    //###########################################################################################
    //
    private delegate void CBAnalyseGetLedSystem(CCommand command);
    private void AnalyseGetLedSystem(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetLedSystem CB = new CBAnalyseGetLedSystem(AnalyseGetLedSystem);
        Invoke(CB, new object[] { command }); 
      }
      else
      {
        FResult = false;
        if (command is CGetLedSystem)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CGetLedSystem.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetLedSystem.HEADER + " expected");
              }
              Int32 State;
              Int32.TryParse(Tokens[2], out State);
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetStateLedSystem(State);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("STATELEDSYSTEM[{0}]", State));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetLedSystemHigh(CCommand command);
    private void AnalyseSetLedSystemHigh(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetLedSystemHigh CB = new CBAnalyseSetLedSystemHigh(AnalyseSetLedSystemHigh);
        Invoke(CB, new object[] { command }); 
      }
      else
      {
        FResult = false;
        if (command is CSetLedSystemHigh)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CSetLedSystemHigh.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetLedSystemHigh.HEADER + " expected");
              }
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetStateLedSystem((int)EStateLed.On);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("SETLEDSYSTEMHIGH");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseSetLedSystemLow(CCommand command);
    private void AnalyseSetLedSystemLow(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetLedSystemLow CB = new CBAnalyseSetLedSystemLow(AnalyseSetLedSystemLow);
        Invoke(CB, new object[] { command }); 
      }
      else
      {
        FResult = false;
        if (command is CSetLedSystemLow)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CSetLedSystemLow.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetLedSystemLow.HEADER + " expected");
              }
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetStateLedSystem((int)EStateLed.Off);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("SETLEDSYSTEMLOW");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }

    private delegate void CBAnalyseBlinkLedSystem(CCommand command);
    private void AnalyseBlinkLedSystem(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseBlinkLedSystem CB = new CBAnalyseBlinkLedSystem(AnalyseBlinkLedSystem);
        Invoke(CB, new object[] { command }); 
      }
      else
      {
        FResult = false;
        if (command is CBlinkLedSystem)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (4 <= Tokens.Length)
          {
            if (CDefine.TOKEN_MARK == Tokens[0])
            {
              if (CBlinkLedSystem.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CBlinkLedSystem.HEADER + " expected");
              }
              Int32 ProcessPeriod;
              Int32.TryParse(Tokens[2], out ProcessPeriod);
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetProcessCount(ProcessPeriod);
              Int32 ProcessCount;
              Int32.TryParse(Tokens[3], out ProcessCount);
              //!!!!!!!!!!!!!!!FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("BLINKLEDSYSTEM[{0}|{1}]", ProcessPeriod, ProcessCount));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LaserPosition
    //###########################################################################################
    //

    //
    //###########################################################################################
    //  Segment - Analyse - LaserRange
    //###########################################################################################
    //

    //
    //###########################################################################################
    //  Segment - Analyse - LaserMatrix
    //###########################################################################################
    //

    //
    //###########################################################################################
    //  Segment - Analyse - LaserImage
    //###########################################################################################
    //

    //private delegate void CBAnalyseGetTemperatureChannel(CCommand command);
    //private void AnalyseGetTemperatureChannel(CCommand command)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBAnalyseGetTemperatureChannel CB = new CBAnalyseGetTemperatureChannel(AnalyseGetTemperatureChannel);
    //    Invoke(CB, new object[] { command }); 
    //  }
    //  else
    //  {
    //    FResult = false; 
    //    if (command is CGetTemperatureChannel)
    //    {
    //      Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
    //      String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //      if (3 <= Tokens.Length)
    //      {
    //        if (CDefine.TOKEN_MARK == Tokens[0])
    //        {
    //          if (CGetTemperatureChannel.HEADER != Tokens[1])
    //          {
    //            FUCNotifier.Error("Main", 1, CGetTemperatureChannel.HEADER + " expected");
    //          }
    //          Int32 Channel;
    //          Int32.TryParse(Tokens[2], out Channel);
    //          FUCLaserAreaScannerCommand.SetChannelLow(Channel);
    //          if (cbxDebug.Checked)
    //          {
    //            FUCNotifier.Write(String.Format("GETTEMPERATURECHANNEL[{0}]", Channel));
    //          }
    //          command.SignalTextReceived();
    //          FResult = true;
    //        }
    //      }
    //    }
    //  }
    //}

    //private delegate void CBAnalyseGetTemperatureInterval(CCommand command);
    //private void AnalyseGetTemperatureInterval(CCommand command)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBAnalyseGetTemperatureInterval CB = new CBAnalyseGetTemperatureInterval(AnalyseGetTemperatureInterval);
    //    Invoke(CB, new object[] { command }); 
    //  }
    //  else
    //  {
    //    FResult = false;
    //    if (command is CGetTemperatureInterval)
    //    {
    //      Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
    //      String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //      if (4 <= Tokens.Length)
    //      {
    //        if (CDefine.TOKEN_MARK == Tokens[0])
    //        {
    //          if (CGetTemperatureInterval.HEADER != Tokens[1])
    //          {
    //            FUCNotifier.Error("Main", 1, CGetTemperatureInterval.HEADER + " expected");
    //          }
    //          Int32 ChannelLow;
    //          Int32.TryParse(Tokens[2], out ChannelLow);
    //          FUCLaserAreaScannerCommand.SetChannelLow(ChannelLow);
    //          Int32 ChannelHigh;
    //          Int32.TryParse(Tokens[3], out ChannelHigh);
    //          FUCLaserAreaScannerCommand.SetChannelHigh(ChannelHigh);
    //          if (cbxDebug.Checked)
    //          {
    //            FUCNotifier.Write(String.Format("GETTEMPERATUREINTERVAL[{0}|{1}]", ChannelLow, ChannelHigh));
    //          }
    //          command.SignalTextReceived();
    //          FResult = true;
    //        }
    //      }
    //    }
    //  }
    //}

    //private delegate void CBAnalyseGetAllTemperatures(CCommand command);
    //private void AnalyseGetAllTemperatures(CCommand command)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBAnalyseGetAllTemperatures CB = new CBAnalyseGetAllTemperatures(AnalyseGetAllTemperatures);
    //    Invoke(CB, new object[] { command }); 
    //  }
    //  else
    //  {
    //    FResult = false; 
    //    if (command is CGetAllTemperatures)
    //    {
    //      Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
    //      String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //      if (2 <= Tokens.Length)
    //      {
    //        if (CDefine.TOKEN_MARK == Tokens[0])
    //        {
    //          if (CGetAllTemperatures.HEADER != Tokens[1])
    //          {
    //            FUCNotifier.Error("Main", 1, CGetAllTemperatures.HEADER + " expected");
    //          }
    //          if (cbxDebug.Checked)
    //          {
    //            FUCNotifier.Write("GETALLTEMPERATURES");
    //          }
    //          command.SignalTextReceived();
    //          FResult = true;
    //        }
    //      }
    //    }
    //  }
    //}

    //private delegate void CBAnalyseRepeatTemperatureChannel(CCommand command);
    //private void AnalyseRepeatTemperatureChannel(CCommand command)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBAnalyseRepeatTemperatureChannel CB = new CBAnalyseRepeatTemperatureChannel(AnalyseRepeatTemperatureChannel);
    //    Invoke(CB, new object[] { command }); 
    //  }
    //  else
    //  {
    //    FResult = false; 
    //    if (command is CRepeatTemperatureChannel)
    //    {
    //      Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
    //      String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //      if (3 <= Tokens.Length)
    //      {
    //        if (CDefine.TOKEN_MARK == Tokens[0])
    //        {
    //          if (CRepeatTemperatureChannel.HEADER != Tokens[1])
    //          {
    //            FUCNotifier.Error("Main", 1, CRepeatTemperatureChannel.HEADER + " expected");
    //          }
    //          Int32 Channel;
    //          Int32.TryParse(Tokens[2], out Channel);
    //          FUCLaserAreaScannerCommand.SetChannelLow(Channel);
    //          if (cbxDebug.Checked)
    //          {
    //            FUCNotifier.Write(String.Format("REPEATTEMPERATURECHANNEL[{0}]", Channel));
    //          }
    //          command.SignalTextReceived();
    //          FResult = true;
    //        }
    //      }
    //    }
    //  }
    //}
    
    //private delegate void CBAnalyseRepeatTemperatureInterval(CCommand command);
    //private void AnalyseRepeatTemperatureInterval(CCommand command)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBAnalyseRepeatTemperatureInterval CB = new CBAnalyseRepeatTemperatureInterval(AnalyseRepeatTemperatureInterval);
    //    Invoke(CB, new object[] { command }); 
    //  }
    //  else
    //  {
    //    FResult = false; 
    //    if (command is CRepeatTemperatureInterval)
    //    {
    //      Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
    //      String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //      if (4 <= Tokens.Length)
    //      {
    //        if (CDefine.TOKEN_MARK == Tokens[0])
    //        {
    //          if (CRepeatTemperatureInterval.HEADER != Tokens[1])
    //          {
    //            FUCNotifier.Error("Main", 1, CRepeatTemperatureInterval.HEADER + " expected");
    //          }
    //          Int32 ChannelLow;
    //          Int32.TryParse(Tokens[2], out ChannelLow);
    //          FUCLaserAreaScannerCommand.SetChannelLow(ChannelLow);
    //          Int32 ChannelHigh;
    //          Int32.TryParse(Tokens[3], out ChannelHigh);
    //          FUCLaserAreaScannerCommand.SetChannelHigh(ChannelHigh);
    //          if (cbxDebug.Checked)
    //          {
    //            FUCNotifier.Write(String.Format("REPEATTEMPERATUREINTERVAL[{0}|{1}]", ChannelLow, ChannelHigh));
    //          }
    //          command.SignalTextReceived();
    //          FResult = true;
    //        }
    //      }
    //    }
    //  }
    //}
    
    //private delegate void CBAnalyseRepeatAllTemperatures(CCommand command);
    //private void AnalyseRepeatAllTemperatures(CCommand command)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBAnalyseRepeatAllTemperatures CB = new CBAnalyseRepeatAllTemperatures(AnalyseRepeatAllTemperatures);
    //    Invoke(CB, new object[] { command }); 
    //  }
    //  else
    //  {
    //    FResult = false; 
    //    if (command is CRepeatAllTemperatures)
    //    {
    //      Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
    //      String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
    //      if (2 <= Tokens.Length)
    //      {
    //        if (CDefine.TOKEN_MARK == Tokens[0])
    //        {
    //          if (CRepeatAllTemperatures.HEADER != Tokens[1])
    //          {
    //            FUCNotifier.Error("Main", 1, CRepeatAllTemperatures.HEADER + " expected");
    //          }
    //          if (cbxDebug.Checked)
    //          {
    //            FUCNotifier.Write("REPEATALLTEMPERATURES");
    //          }
    //          command.SignalTextReceived();
    //          FResult = true;
    //        }
    //      }
    //    }
    //  }
    //}
    //
    //------------------------------------------------------------------------
    //  Section - Callback - Uart
    //------------------------------------------------------------------------
    //
    private void UartOnErrorDetected(EComPort comport, Int32 errorcode, String errortext)
    {
    }

    private void UartOnLineReceived(EComPort comport, String line)
    {
      if (cbxDebug.Checked)
      {
        FUCNotifier.Write(String.Format("ComPort[{0}] Rxd<{1}>", CUart.ComPortEnumeratorName(comport), line));
      }
      FRxdLine = line;
      AnalyseDateTimePrompt();
      AnalyseStateLaserAreaScanner();
      AnalyseTemperatureValues();
      if (0 < FCommandList.Count)
      {
        CCommand Command = FCommandList.Peek();
        if (Command is CCommand)
        { // Analyse Command:
          if (1 <= FRxdLine.Length)
          {
            FResult = false;
            // Common
            AnalyseGetHelp(Command);
            if (FResult) return;
            AnalyseGetProgramHeader(Command);
            if (FResult) return;
            AnalyseGetSoftwareVersion(Command);
            if (FResult) return;
            AnalyseGetHardwareVersion(Command);
            if (FResult) return;
            AnalyseGetProcessCount(Command);
            if (FResult) return;
            AnalyseSetProcessCount(Command);
            if (FResult) return;
            AnalyseGetProcessPeriod(Command);
            if (FResult) return;
            AnalyseSetProcessPeriod(Command);
            if (FResult) return;
            AnalyseStopProcessExecution(Command);
            if (FResult) return;
            AnalyseAbortProcessExecution(Command);
            if (FResult) return;
            // LedSystem
            AnalyseGetLedSystem(Command);
            if (FResult) return;
            AnalyseSetLedSystemHigh(Command);
            if (FResult) return;
            AnalyseSetLedSystemLow(Command);
            if (FResult) return;
            AnalyseBlinkLedSystem(Command);
            if (FResult) return;
            //// LaserPosition
            //AnalysePulseLaserPosition(Command);
            //if (FResult) return;
            //AnalyseAbortLaserPosition(Command);
            //if (FResult) return;
            //// LaserRange
            //AnalyseGetPositionX(Command);
            //if (FResult) return;
            //AnalyseGetRangeX(Command);
            //if (FResult) return;
            //AnalyseSetRangeX(Command);
            //if (FResult) return;
            //AnalyseGetPositionY(Command);
            //if (FResult) return;
            //AnalyseGetRangeY(Command);
            //if (FResult) return;
            //AnalyseSetRangeY(Command);
            //if (FResult) return;
            //AnalyseGetDelayMotion(Command);
            //if (FResult) return;
            //AnalyseSetDelayMotion(Command);
            //if (FResult) return;
            //// LaserMatrix
            //AnalysePulseLaserMatrix(Command);
            //if (FResult) return;
            //AnalyseAbortLaserMatrix(Command);
            //if (FResult) return;
            //// LaserImage
            //AnalysePulseLaserImage(Command);
            //if (FResult) return;
            //AnalyseAbortLaserImage(Command);
            //if (FResult) return;
          }
        }
      }
    }

    private void UartOnLineTransmitted(EComPort comport, String line)
    {
      if (cbxDebug.Checked)
      {
        FUCNotifier.Write(String.Format("ComPort[{0}] Txd<{1}>", CUart.ComPortEnumeratorName(comport), line));
      }
    }

    private void UartOnPinChanged(EComPort comport, Boolean pincts, Boolean pinrts, 
                                  Boolean pindsr, Boolean pindtr, Boolean pindcd)
    {
    }

    private void UartOnTextReceived(EComPort comport, String text)
    {
    }

    private void UartOnTextTransmitted(EComPort comport, String text)
    {
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCUartTerminal
    //------------------------------------------------------------------------
    //
    private void UCUartTerminalOnTerminalOpen(String portname, EComPort comport, EBaudrate baudrate, EParity parity,
                                              EDatabits databits, EStopbits stopbits, EHandshake handshake)
    {
      String Line;
      if (FUart.Open(portname, comport, baudrate, parity, databits, stopbits, handshake))
      {
        if (cbxDebug.Checked)
        {
          Line = String.Format("ComPort[{0}]:({1}/{2}/{3}/{4}/{5}/{6}) correctly opened!",
                               portname, CUart.ComPortEnumeratorName(comport),
                               CUart.BaudrateEnumeratorName(baudrate), CUart.ParityEnumeratorName(parity),
                               CUart.DatabitsEnumeratorName(databits), CUart.StopbitsEnumeratorName(stopbits),
                               CUart.HandshakeEnumeratorName(handshake));
          FUCNotifier.Write(Line);
        }
        FUCUartTerminal.SetUartOpened(true);
      }
      else
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Error(HEADER_LIBRARY, 1, String.Format("Cannot open ComPort[{0}]",
                                                             CUart.ComPortEnumeratorName(comport)));
        }
        FUCUartTerminal.SetUartOpened(false);
      }
    }

    private void UCUartTerminalOnTerminalClose(EComPort comport)
    {
      if (FUart.Close())
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Write(String.Format("ComPort[{0}] correctly closed!", CUart.ComPortEnumeratorName(comport)));
        }
      }
      else
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Error(HEADER_LIBRARY, 1, String.Format("Cannot close ComPort[{0}]",
                                                            CUart.ComPortEnumeratorName(comport)));
        }
      }
    }

    private void UCUartTerminalOnTransmitText(string text, int delaycharacter, int delaycr, int delaylf)
    {
      if (cbxDebug.Checked)
      {
        FUart.WriteText(text);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCLaserAreaScannerCommand
    //------------------------------------------------------------------------
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetHelp();
    private void UCLaserAreaScannerCommandOnGetHelp()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetHelp CB = 
          new CBUCLaserAreaScannerCommandOnGetHelp(UCLaserAreaScannerCommandOnGetHelp);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetHelp());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetProgramHeader();
    private void UCLaserAreaScannerCommandOnGetProgramHeader()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetProgramHeader CB = 
          new CBUCLaserAreaScannerCommandOnGetProgramHeader(UCLaserAreaScannerCommandOnGetProgramHeader);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetProgramHeader());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetHardwareVersion();
    private void UCLaserAreaScannerCommandOnGetHardwareVersion()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetHardwareVersion CB = 
          new CBUCLaserAreaScannerCommandOnGetHardwareVersion(UCLaserAreaScannerCommandOnGetHardwareVersion);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetHardwareVersion());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetSoftwareVersion();
    private void UCLaserAreaScannerCommandOnGetSoftwareVersion()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetSoftwareVersion CB = 
          new CBUCLaserAreaScannerCommandOnGetSoftwareVersion(UCLaserAreaScannerCommandOnGetSoftwareVersion);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetSoftwareVersion());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnGetLedSystem();
    private void UCLaserAreaScannerCommandOnGetLedSystem()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetLedSystem CB = 
          new CBUCLaserAreaScannerCommandOnGetLedSystem(UCLaserAreaScannerCommandOnGetLedSystem);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetLedSystem());
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSwitchLedSystemOn();
    private void UCLaserAreaScannerCommandOnSwitchLedSystemOn()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSwitchLedSystemOn CB = 
          new CBUCLaserAreaScannerCommandOnSwitchLedSystemOn(UCLaserAreaScannerCommandOnSwitchLedSystemOn);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetLedSystemHigh());
        FCommandList.Execute();
      }
    }
    
    private delegate void CBUCLaserAreaScannerCommandOnSwitchLedSystemOff();
    private void UCLaserAreaScannerCommandOnSwitchLedSystemOff()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSwitchLedSystemOff CB = 
          new CBUCLaserAreaScannerCommandOnSwitchLedSystemOff(UCLaserAreaScannerCommandOnSwitchLedSystemOff);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetLedSystemLow());
        FCommandList.Execute();
      }
    }
    
    private delegate void CBUCLaserAreaScannerCommandOnBlinkLedSystem(Int32 period, Int32 count);
    private void UCLaserAreaScannerCommandOnBlinkLedSystem(Int32 period, Int32 count)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnBlinkLedSystem CB = 
          new CBUCLaserAreaScannerCommandOnBlinkLedSystem(UCLaserAreaScannerCommandOnBlinkLedSystem);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CBlinkLedSystem(period, count));
        FCommandList.Execute();
      }
    }
    
    private delegate void CBUCLaserAreaScannerCommandOnSetProcessCount(Int32 count);
    private void UCLaserAreaScannerCommandOnSetProcessCount(Int32 count)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetProcessCount CB = 
          new CBUCLaserAreaScannerCommandOnSetProcessCount(UCLaserAreaScannerCommandOnSetProcessCount);
        Invoke(CB, new object[] { count });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetProcessCount(count));
        FCommandList.Execute();
      }
    }

    private delegate void CBUCLaserAreaScannerCommandOnSetProcessPeriod(Int32 period);
    private void UCLaserAreaScannerCommandOnSetProcessPeriod(Int32 period)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetProcessPeriod CB = 
          new CBUCLaserAreaScannerCommandOnSetProcessPeriod(UCLaserAreaScannerCommandOnSetProcessPeriod);
        Invoke(CB, new object[] { period });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetProcessPeriod(period));
        FCommandList.Execute();
      }
    }
    
    private delegate void CBUCLaserAreaScannerCommandOnStopProcessExecution();
    private void UCLaserAreaScannerCommandOnStopProcessExecution()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnStopProcessExecution CB = 
          new CBUCLaserAreaScannerCommandOnStopProcessExecution(UCLaserAreaScannerCommandOnStopProcessExecution);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CStopProcessExecution());
        FCommandList.Execute();
      }
    }
    
    //private delegate void CBUCLaserAreaScannerCommandOnGetTemperatureChannel(Int32 channel);
    //private void UCLaserAreaScannerCommandOnGetTemperatureChannel(Int32 channel)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBUCLaserAreaScannerCommandOnGetTemperatureChannel CB = 
    //      new CBUCLaserAreaScannerCommandOnGetTemperatureChannel(UCLaserAreaScannerCommandOnGetTemperatureChannel);
    //    Invoke(CB, new object[] { channel });
    //  }
    //  else
    //  { // Create / Execute Command
    //    FCommandList.Enqueue(new CGetTemperatureChannel(channel));
    //    FCommandList.Execute();
    //  }
    //}
    
    //private delegate void CBUCLaserAreaScannerCommandOnGetTemperatureInterval(Int32 channellow, Int32 channelhigh);
    //private void UCLaserAreaScannerCommandOnGetTemperatureInterval(Int32 channellow, Int32 channelhigh)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBUCLaserAreaScannerCommandOnGetTemperatureInterval CB = 
    //      new CBUCLaserAreaScannerCommandOnGetTemperatureInterval(UCLaserAreaScannerCommandOnGetTemperatureInterval);
    //    Invoke(CB, new object[] { channellow, channelhigh });
    //  }
    //  else
    //  { // Create / Execute Command
    //    FCommandList.Enqueue(new CGetTemperatureInterval(channellow, channelhigh));
    //    FCommandList.Execute();
    //  }
    //}
    
    //private delegate void CBUCLaserAreaScannerCommandOnGetAllTemperatures();
    //private void UCLaserAreaScannerCommandOnGetAllTemperatures()
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBUCLaserAreaScannerCommandOnGetAllTemperatures CB = 
    //      new CBUCLaserAreaScannerCommandOnGetAllTemperatures(UCLaserAreaScannerCommandOnGetAllTemperatures);
    //    Invoke(CB, new object[] { });
    //  }
    //  else
    //  { // Create / Execute Command
    //    FCommandList.Enqueue(new CGetAllTemperatures());
    //    FCommandList.Execute();
    //  }
    //}

    //private delegate void CBUCLaserAreaScannerCommandOnRepeatTemperatureChannel(Int32 channel);
    //private void UCLaserAreaScannerCommandOnRepeatTemperatureChannel(Int32 channel)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBUCLaserAreaScannerCommandOnRepeatTemperatureChannel CB = 
    //      new CBUCLaserAreaScannerCommandOnRepeatTemperatureChannel(UCLaserAreaScannerCommandOnRepeatTemperatureChannel);
    //    Invoke(CB, new object[] { channel });
    //  }
    //  else
    //  { // Create / Execute Command
    //    FCommandList.Enqueue(new CRepeatTemperatureChannel(channel));
    //    FCommandList.Execute();
    //  }
    //}

    //private delegate void CBUCLaserAreaScannerCommandOnRepeatTemperatureInterval(Int32 channellow, Int32 channelhigh);
    //private void UCLaserAreaScannerCommandOnRepeatTemperatureInterval(Int32 channellow, Int32 channelhigh)
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBUCLaserAreaScannerCommandOnRepeatTemperatureInterval CB = 
    //      new CBUCLaserAreaScannerCommandOnRepeatTemperatureInterval(UCLaserAreaScannerCommandOnRepeatTemperatureInterval);
    //    Invoke(CB, new object[] { channellow, channelhigh });
    //  }
    //  else
    //  { // Create / Execute Command
    //    FCommandList.Enqueue(new CRepeatTemperatureInterval(channellow, channelhigh));
    //    FCommandList.Execute();
    //  }
    //}

    //private delegate void CBUCLaserAreaScannerCommandOnRepeatAllTemperatures();
    //private void UCLaserAreaScannerCommandOnRepeatAllTemperatures()
    //{
    //  if (this.InvokeRequired)
    //  {
    //    CBUCLaserAreaScannerCommandOnRepeatAllTemperatures CB = 
    //      new CBUCLaserAreaScannerCommandOnRepeatAllTemperatures(UCLaserAreaScannerCommandOnRepeatAllTemperatures);
    //    Invoke(CB, new object[] { });
    //  }
    //  else
    //  { // Create / Execute Command
    //    FCommandList.Enqueue(new CRepeatAllTemperatures());
    //    FCommandList.Execute();
    //  }
    //}
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          //tbcMain.SelectedIndex = 1;
          //FUCEditorLaserStepFile.LoadFromFile("Matrix18002200100.lsf.txt");
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          //FUdpTextDeviceClient.Preset("GSV");
          return true;
        case 7:
          return true;
        case 8:
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! First (Client -> Server)");
          return true;
        case 9:
          //FUdpTextDeviceClient.Preset("GHV");
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! Second (Client -> Server)");
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }

  }
}





