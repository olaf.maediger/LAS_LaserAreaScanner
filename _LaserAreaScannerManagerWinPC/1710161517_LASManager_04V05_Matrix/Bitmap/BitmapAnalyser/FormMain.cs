﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using BitmapImage;
//
namespace BitmapAnalyser
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICETYPE = "BitmapAnalyser";
    private const String INIT_DEVICENAME = "OMBitmapAnalyser";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    // 
    Random FRandom;
    CBitmap FBitmapSource;
    CBitmap FBitmapTarget;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      FRandom = new Random((Int32)DateTime.Now.Ticks);
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      nudScaleFactor.Value = 4;
      pbxImage.Dock = DockStyle.None;
      pbxImage.SizeMode = PictureBoxSizeMode.AutoSize;
      cbxZoom.Checked = false;// true;
      lblColorLow.BackColor = Color.Blue;
      lblColorHigh.BackColor = Color.Yellow;
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //

    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    // 

    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //#########################################################################
    //  Segment - Callback -
    //#########################################################################
    //

    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          tbcMain.SelectedIndex = 1;
          return true;
        case 1:
          //String FileName = "Image.bmp";
          String FileName = "Vegetables.bmp";
          FBitmapSource = new CBitmap(FileName);
          Int32 SF = (Int32)nudScaleFactor.Value;
          Bitmap BS = FBitmapSource.GetBitmap();
          Bitmap BT = new Bitmap(BS, SF * BS.Width, SF * BS.Height);
          FBitmapTarget = new CBitmap(BT);
          pbxImage.Image = FBitmapTarget.GetBitmap();
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5: 
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }

    private void btnLoadImage_Click(object sender, EventArgs e)
    {
      DialogLoadImage.FileName = "Image.bmp";
      if (DialogResult.OK == DialogLoadImage.ShowDialog())
      {
        String FileName = DialogLoadImage.FileName;
        FBitmapSource = new CBitmap(FileName);
        Int32 SF = (Int32)nudScaleFactor.Value;
        Bitmap BS = FBitmapSource.GetBitmap();
        Bitmap BT = new Bitmap(BS, SF * BS.Width, SF * BS.Height);
        FBitmapTarget = new CBitmap(BT);
        pbxImage.Image = FBitmapTarget.GetBitmap();
      }
    }

    private void btnSaveImage_Click(object sender, EventArgs e)
    {
      DialogSaveImage.FileName = "ImageNew.bmp";
      if (DialogResult.OK == DialogSaveImage.ShowDialog())
      {
        String FileName = DialogSaveImage.FileName;
        FBitmapTarget.GetBitmap().Save(FileName);
      }      
    }


    private void cbxZoom_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxZoom.Checked)
      {
        pbxImage.Dock = DockStyle.Fill;
        pbxImage.SizeMode = PictureBoxSizeMode.Zoom;
      }
      else
      {
        pbxImage.Dock = DockStyle.None;
        pbxImage.SizeMode = PictureBoxSizeMode.AutoSize;
      }

    }

    Int32 GetScaleFactor()
    {
      return (Int32)nudScaleFactor.Value;
    }
    void SetScaleFactor(Int32 value)
    {
      nudScaleFactor.Value = value;
      RedrawBitmap();
    }

    private void nudScaleFactor_ValueChanged(object sender, EventArgs e)
    {
      SetScaleFactor((Int32)nudScaleFactor.Value);
    }

    private void btnConvertGreyScale_Click(object sender, EventArgs e)
    {
      if ((FBitmapSource is CBitmap) && (FBitmapSource.GetBitmap() is Bitmap))
      {
        Int32 SF = GetScaleFactor();
        Bitmap BS = FBitmapSource.GetBitmap();
        Bitmap BT = new Bitmap(BS, SF * BS.Width, SF * BS.Height);
        FBitmapTarget = new CBitmap(BT);
        FBitmapTarget.ConvertGreyScale();
        pbxImage.Image = FBitmapTarget.GetBitmap();
      }
    }



    private void RedrawBitmap()
    {
      if ((FBitmapSource is CBitmap) && (FBitmapSource.GetBitmap() is Bitmap))
      {
        Int32 SF = GetScaleFactor();
        Bitmap BS = FBitmapSource.GetBitmap();
        Bitmap BT = new Bitmap(BS, SF * BS.Width, SF * BS.Height);
        Int32 TH = GetThreshold();
        //Console.WriteLine(TH);
        FBitmapTarget = new CBitmap(BT);
        FBitmapTarget.ConvertMonoChrome(TH, lblColorLow.BackColor, lblColorHigh.BackColor);
        pbxImage.Image = FBitmapTarget.GetBitmap();
      }
    }



    Int32 GetThreshold()
    {
      return (Int32)nudThreshold.Value;
    }
    void SetThreshold(Int32 value)
    {
      nudThreshold.Value = value;
      hsbThreshold.Value = value;
      RedrawBitmap();
    }

    private void nudThreshold_ValueChanged(object sender, EventArgs e)
    {
      SetThreshold((Int32)nudThreshold.Value);
    }

    private void btnConvertMonoChrome_Click(object sender, EventArgs e)
    {
      SetThreshold((Int32)nudThreshold.Value);
    }

    private void hsbThreshold_ValueChanged(object sender, EventArgs e)
    {
      SetThreshold(hsbThreshold.Value);
    }

    private void lblColorLow_MouseClick(object sender, MouseEventArgs e)
    {
      DialogColorPixel.Color = lblColorLow.BackColor;
      if (DialogResult.OK == DialogColorPixel.ShowDialog())
      {
        lblColorLow.BackColor = DialogColorPixel.Color;
        RedrawBitmap();
      }
    }

    private void lblColorHigh_MouseClick(object sender, MouseEventArgs e)
    {
      DialogColorPixel.Color = lblColorHigh.BackColor;
      if (DialogResult.OK == DialogColorPixel.ShowDialog())
      {
        lblColorHigh.BackColor = DialogColorPixel.Color;
        RedrawBitmap();
      }
    }


  }
}
