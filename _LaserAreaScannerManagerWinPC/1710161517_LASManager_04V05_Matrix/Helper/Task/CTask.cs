﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
//-------------------------------------------------------------------
//  Segment - Type - Task
//-------------------------------------------------------------------
//
namespace Task
{
  //
  //-------------------------------------------------------------------
  //  Segment - Delegate
  //-------------------------------------------------------------------
  //
  public delegate void DOnExecutionStart(RTaskData data);
  public delegate Boolean DOnExecutionBusy(RTaskData data);
  public delegate void DOnExecutionEnd(RTaskData data);
  public delegate void DOnExecutionAbort(RTaskData data);
  //
  //-------------------------------------------------------------------
  //  Segment - Type - RTaskData
  //-------------------------------------------------------------------
  //
  public struct RTaskData
  {
    public String Name;
    public Int32 Counter;
    public Boolean IsActive;
    public Boolean IsAborted;
    public Boolean IsFinished;
    public DOnExecutionStart OnExecutionStart;
    public DOnExecutionBusy OnExecutionBusy;
    public DOnExecutionEnd OnExecutionEnd;
    public DOnExecutionAbort OnExecutionAbort;
  }
  //
  //-------------------------------------------------------------------
  //  Segment - Type - CTask
  //-------------------------------------------------------------------
  //
  public class CTask
  { //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    private RTaskData FData;
    private Thread FThread;
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    public CTask(String name,
                 DOnExecutionStart onexecutionstart,
                 DOnExecutionBusy onexecutionbusy,
                 DOnExecutionEnd onexecutionend,
                 DOnExecutionAbort onexecutionabort)
    {
      FData.Name = name;
      FData.OnExecutionStart = onexecutionstart;
      FData.OnExecutionBusy = onexecutionbusy;
      FData.OnExecutionEnd = onexecutionend;
      FData.OnExecutionAbort = onexecutionabort;
      FData.Counter = 0;
      FData.IsAborted = false;
      FData.IsActive = false;
      FData.IsFinished = false;
      FThread = null;
      // debug String Line = String.Format("### Task[{0}].Created", FData.Name);
      // debug Console.WriteLine(Line);
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public Boolean IsActive()
    {
      return FData.IsActive;
    }

    public Boolean IsAborted()
    {
      return FData.IsAborted;
    }

    public Boolean IsFinished()
    {
      return FData.IsFinished;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //
    private void OnExecute()
    {
      // debug String Line = String.Format("### Task[{0}].OnExecute() - S", FData.Name);
      // debug Console.WriteLine(Line);
      FData.IsFinished = false;
      FData.IsActive = true;
      if (FData.OnExecutionStart is DOnExecutionStart)
      {
        FData.OnExecutionStart(FData);
      }
      if (FData.OnExecutionBusy is DOnExecutionBusy)
      {
        FData.Counter = 0;
        do
        {
          FData.Counter += 1;
          FData.IsActive = FData.OnExecutionBusy(FData);
        }
        while (FData.IsActive);
      }
      FData.IsActive = false;
      FData.IsFinished = true;
      if (FData.OnExecutionEnd is DOnExecutionEnd)
      {
        FData.OnExecutionEnd(FData);
      }
      if (FThread is Thread)
      {
        Abort();
      }
      FThread = null;
      // debug Line = String.Format("### Task[{0}].OnExecute() - E", FData.Name);
      // debug Console.WriteLine(Line);
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Public
    //-------------------------------------------------------------------
    //
    public Boolean Start()
    {
      // debug String Line = String.Format("### Task[{0}].Start() - S", FData.Name);
      // debug Console.WriteLine(Line);
      FData.Counter += 1;
      // RO FData.Name 
      FThread = new Thread(OnExecute);
      FThread.Name = FData.Name;
      FThread.Start();
      // debug Line = String.Format("### Task[{0}].Start() - E", FData.Name);
      // debug Console.WriteLine(Line);
      return true;
    }

    public Boolean Abort()
    {
      // debug String Line = String.Format("### Task[{0}].Abort() - S", FData.Name);
      // debug Console.WriteLine(Line);
     try
      {
        if (FThread is Thread)
        {
          try
          {
            FThread.Abort();
          }
          catch (Exception)
          {
          }
          //if (ThreadState.Aborted == FThread.ThreadState)
          //{
          //  FThread = null;
          //  // debug Line = String.Format("### Task[{0}].Abort() - E", FData.Name);
          //  // debug Console.WriteLine(Line);
          //  return true;
          //}
        }
        FThread = null;
        FData.IsAborted = true;
        FData.IsActive = false;
        if (FData.OnExecutionAbort is DOnExecutionAbort)
        {
          FData.OnExecutionAbort(FData);
        }
        // debug Line = String.Format("### Task[{0}].Abort() - Error <null>!!!", FData.Name);
        // debug Console.WriteLine(Line);
        return false;
      }
      catch (Exception)
      {
        FThread = null;
        FData.IsAborted = true;
        FData.IsActive = false;
        if (FData.OnExecutionAbort is DOnExecutionAbort)
        {
          FData.OnExecutionAbort(FData);
        }
        // debug Line = String.Format("### Task[{0}].Abort() - Error<exception>!!!", FData.Name);
        // debug Console.WriteLine(Line);
        return false;
      }
    }

  }
}
