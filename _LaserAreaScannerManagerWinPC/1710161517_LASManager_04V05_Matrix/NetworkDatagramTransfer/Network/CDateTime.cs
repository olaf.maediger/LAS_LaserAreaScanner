﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Network
{
  public class CDateTime
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------------------
    //
    public static String GetNow()
    {
      DateTime DT = DateTime.Now;
      String Result = String.Format("{0:00}{1:00}{2:00}.{3:00}{4:00}{5:00}.{6:000}", 
                                    DT.Year, DT.Month, DT.Day, 
                                    DT.Hour, DT.Minute, DT.Second, 
                                    DT.Millisecond);
      return Result;
    }
  }
}
