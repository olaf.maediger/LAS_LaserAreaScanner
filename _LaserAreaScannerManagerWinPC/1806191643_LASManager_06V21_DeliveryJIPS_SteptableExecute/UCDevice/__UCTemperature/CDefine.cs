﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCTemperature
{
  //
  //--------------------------------------------------------------------------
  //  Segment - Definition - Callback
  //--------------------------------------------------------------------------
  //
  public delegate void DOnGetHelp();
  public delegate void DOnGetProgramHeader();
  public delegate void DOnGetHardwareVersion();
  public delegate void DOnGetSoftwareVersion();
  public delegate void DOnGetLedSystem();
  public delegate void DOnSwitchLedSystemOn();
  public delegate void DOnSwitchLedSystemOff();
  public delegate void DOnBlinkLedSystem(Int32 period, Int32 count);
  public delegate void DOnSetProcessCount(Int32 count);
  public delegate void DOnSetProcessPeriod(Int32 period);
  public delegate void DOnStopProcessExecution();
  public delegate void DOnGetTemperatureChannel(Int32 channel);
  public delegate void DOnGetTemperatureInterval(Int32 channellow, Int32 channelhigh);
  public delegate void DOnGetAllTemperatures();
  public delegate void DOnRepeatTemperatureChannel(Int32 channel);
  public delegate void DOnRepeatTemperatureInterval(Int32 channellow, Int32 channelhigh);
  public delegate void DOnRepeatAllTemperatures();

  public class CDefine
  {
  }
}
