﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using Initdata;
//
namespace Programdata
{
  public class CProgramdata 
  {
    // 
    //-------------------------------
    //  Section - Constant
    //-------------------------------
    //
    // -> public const String Initfile = "Programdata.xml";
    // -> public const String NodeBase = "Configuration";
    //
    // -> public const String NAME_MAINWINDOW = "MainWindow";
    // -> public const String NAME_TITLE = "Title";
    // -> public const String NAME_WINDOWSTATE = "WindowState";
    // -> public const String NAME_VISIBLE = "Visible"; // not for MainWindow!!!
    // -> public const String NAME_ENABLE = "Enable";
    // -> public const String NAME_LEFT = "Left";
    // -> public const String NAME_TOP = "Top";
    // -> public const String NAME_WIDTH = "Width";
    // -> public const String NAME_HEIGHT = "Height";
    // -> public const String NAME_ABOUTDIALOG = "AboutDialog";
    // -> public const String NAME_SEPARATOR = "Separator";
    // 
    //-------------------------------
    //  Section - Member
    //-------------------------------
    //
    private DialogAbout FDialogAbout = null;
    // 
    //-------------------------------
    //  Section - Constructor
    //-------------------------------
    //
    public CProgramdata()
    {
      FDialogAbout = new DialogAbout();
    }
    //
    //---------------------------------------------------------
    //  Section - Static Helper
    //---------------------------------------------------------
    //
    public static void Init()
    {
      CInitdata.Init();
    }

    public static Boolean BuildProgramdataDirectory(String subdirectorytarget,
                                                 String filenamepreset,
                                                 String fileextensionpreset,
                                                 out String initdatadirectory,
                                                 out String initdatafilename)
    {
      initdatadirectory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
      initdatadirectory = Path.Combine(initdatadirectory, subdirectorytarget);
      initdatafilename = filenamepreset + fileextensionpreset;
      try
      {
        // Programdata
        // Test if "All User\...company\Project" is present
        if (!Directory.Exists(initdatadirectory))
        { // not -> create
          Directory.CreateDirectory(initdatadirectory);
        }
        // Test All User\...company\Project\project.ini.xml
        String ProgramdataEntry = Path.Combine(initdatadirectory, initdatafilename);
        if (!File.Exists(ProgramdataEntry))
        {
          // Try to copy default project.ini.xml from local _Exe
          if (File.Exists(initdatafilename))
          { // Copy default project.ini.xml -> All User\...company\Project\project.ini.xml
            File.Copy(initdatafilename, ProgramdataEntry);
            return true;
          }
          return false;
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean DeleteProgramdataFile(String subdirectorytarget,
                                             String filenamepreset,
                                             String fileextensionpreset)
    {
      String ProgramdataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
      ProgramdataDirectory = Path.Combine(ProgramdataDirectory, subdirectorytarget);
      String ProgramdataFilename = filenamepreset + fileextensionpreset;
      try
      {
        // Programdata
        // Test if "All User\...company\Project" is present
        if (Directory.Exists(ProgramdataDirectory))
        { // Test All User\...company\Project\project.ini.xml
          String ProgramdataEntry = Path.Combine(ProgramdataDirectory, ProgramdataFilename);
          if (File.Exists(ProgramdataEntry))
          { 
            File.Delete(ProgramdataEntry);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    // 
    //
    //-------------------------------
    //
    //  Dialog About
    //
    //-------------------------------
    //
    public void SetApplicationData(String application,
                                   String version,
                                   String date)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetApplicationData(application,
                                        version,
                                        date);
      }
    }

    public void SetDescriptionData(String description,
                                   String point1,
                                   String point2,
                                   String point3)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetDescriptionData(description,
                                        point1,
                                        point2,
                                        point3);
      }
    }

    public void SetCompanySourceData(String organisationtop,
                                     String organisationbottom,
                                     String street,
                                     String city)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetCompanySourceData(organisationtop,
                                          organisationbottom,
                                          street,
                                          city);
      }
    }

    public void SetCompanyTargetData(String organisationtop,
                                     String organisationbottom,
                                     String street,
                                     String city)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetCompanyTargetData(organisationtop,
                                          organisationbottom,
                                          street,
                                          city);
      }
    }

    public void SetIcon(Icon icon)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetIcon(icon);
      }
    }

    public void SetProductKey(String productkey)
    {
      if (FDialogAbout is DialogAbout)
      {
        FDialogAbout.SetProductKey(productkey);
      }
    }

    public DialogResult ShowModalDialogAbout()
    {
      return FDialogAbout.ShowDialog();
    }

  }
}
