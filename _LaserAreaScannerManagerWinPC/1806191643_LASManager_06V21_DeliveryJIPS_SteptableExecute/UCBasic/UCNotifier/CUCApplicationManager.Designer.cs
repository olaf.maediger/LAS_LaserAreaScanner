﻿namespace UCNotifier
{
  partial class CUCApplicationManager
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlKeys = new System.Windows.Forms.Panel();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnClose = new System.Windows.Forms.Button();
      this.btnEditApplication = new System.Windows.Forms.Button();
      this.btnDeleteApplication = new System.Windows.Forms.Button();
      this.btnAddApplication = new System.Windows.Forms.Button();
      this.label12 = new System.Windows.Forms.Label();
      this.pnlTop = new System.Windows.Forms.Panel();
      this.label7 = new System.Windows.Forms.Label();
      this.FUCListBoxApplications = new UCNotifier.CUCListBox();
      this.FUCAddApplication = new UCNotifier.CUCApplication();
      this.FUCDeleteApplication = new UCNotifier.CUCApplication();
      this.FUCEditApplication = new UCNotifier.CUCApplication();
      this.pnlKeys.SuspendLayout();
      this.pnlTop.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlKeys
      // 
      this.pnlKeys.Controls.Add(this.btnCancel);
      this.pnlKeys.Controls.Add(this.btnClose);
      this.pnlKeys.Controls.Add(this.btnEditApplication);
      this.pnlKeys.Controls.Add(this.btnDeleteApplication);
      this.pnlKeys.Controls.Add(this.btnAddApplication);
      this.pnlKeys.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlKeys.Location = new System.Drawing.Point(0, 431);
      this.pnlKeys.Name = "pnlKeys";
      this.pnlKeys.Size = new System.Drawing.Size(591, 34);
      this.pnlKeys.TabIndex = 23;
      this.pnlKeys.Resize += new System.EventHandler(this.pnlKeys_Resize);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(421, 6);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 4;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnClose
      // 
      this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnClose.Location = new System.Drawing.Point(340, 6);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(75, 23);
      this.btnClose.TabIndex = 0;
      this.btnClose.Text = "Close";
      this.btnClose.UseVisualStyleBackColor = true;
      this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // btnEditApplication
      // 
      this.btnEditApplication.Location = new System.Drawing.Point(178, 6);
      this.btnEditApplication.Name = "btnEditApplication";
      this.btnEditApplication.Size = new System.Drawing.Size(75, 23);
      this.btnEditApplication.TabIndex = 2;
      this.btnEditApplication.Text = "Edit";
      this.btnEditApplication.UseVisualStyleBackColor = true;
      this.btnEditApplication.Click += new System.EventHandler(this.btnEditApplication_Click);
      // 
      // btnDeleteApplication
      // 
      this.btnDeleteApplication.Location = new System.Drawing.Point(259, 6);
      this.btnDeleteApplication.Name = "btnDeleteApplication";
      this.btnDeleteApplication.Size = new System.Drawing.Size(75, 23);
      this.btnDeleteApplication.TabIndex = 3;
      this.btnDeleteApplication.Text = "Delete";
      this.btnDeleteApplication.UseVisualStyleBackColor = true;
      this.btnDeleteApplication.Click += new System.EventHandler(this.btnDeleteApplication_Click);
      // 
      // btnAddApplication
      // 
      this.btnAddApplication.Location = new System.Drawing.Point(97, 6);
      this.btnAddApplication.Name = "btnAddApplication";
      this.btnAddApplication.Size = new System.Drawing.Size(75, 23);
      this.btnAddApplication.TabIndex = 1;
      this.btnAddApplication.Text = "Add";
      this.btnAddApplication.UseVisualStyleBackColor = true;
      this.btnAddApplication.Click += new System.EventHandler(this.btnAddApplication_Click);
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(12, 6);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(90, 13);
      this.label12.TabIndex = 148;
      this.label12.Text = "Name Application";
      // 
      // pnlTop
      // 
      this.pnlTop.Controls.Add(this.label12);
      this.pnlTop.Controls.Add(this.label7);
      this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
      this.pnlTop.Location = new System.Drawing.Point(0, 0);
      this.pnlTop.Name = "pnlTop";
      this.pnlTop.Size = new System.Drawing.Size(591, 24);
      this.pnlTop.TabIndex = 35;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(321, 6);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(76, 13);
      this.label7.TabIndex = 147;
      this.label7.Text = "|   Product-Key";
      // 
      // FUCListBoxApplications
      // 
      this.FUCListBoxApplications.BackColor = System.Drawing.SystemColors.Info;
      this.FUCListBoxApplications.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCListBoxApplications.Font = new System.Drawing.Font("Courier New", 12F);
      this.FUCListBoxApplications.FormattingEnabled = true;
      this.FUCListBoxApplications.IntegralHeight = false;
      this.FUCListBoxApplications.ItemHeight = 18;
      this.FUCListBoxApplications.Location = new System.Drawing.Point(0, 24);
      this.FUCListBoxApplications.Name = "FUCListBoxApplications";
      this.FUCListBoxApplications.Size = new System.Drawing.Size(591, 44);
      this.FUCListBoxApplications.TabIndex = 47;
      this.FUCListBoxApplications.SelectedIndexChanged += new System.EventHandler(this.FUCListBoxApplications_SelectedIndexChanged);
      // 
      // FUCAddApplication
      // 
      this.FUCAddApplication.ConfirmTitle = "Add";
      this.FUCAddApplication.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.FUCAddApplication.Location = new System.Drawing.Point(0, 68);
      this.FUCAddApplication.ModeDelete = false;
      this.FUCAddApplication.Name = "FUCAddApplication";
      this.FUCAddApplication.Size = new System.Drawing.Size(591, 121);
      this.FUCAddApplication.TabIndex = 43;
      this.FUCAddApplication.Title = " Add Application ";
      // 
      // FUCDeleteApplication
      // 
      this.FUCDeleteApplication.ConfirmTitle = "Delete";
      this.FUCDeleteApplication.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.FUCDeleteApplication.Location = new System.Drawing.Point(0, 189);
      this.FUCDeleteApplication.ModeDelete = true;
      this.FUCDeleteApplication.Name = "FUCDeleteApplication";
      this.FUCDeleteApplication.Size = new System.Drawing.Size(591, 121);
      this.FUCDeleteApplication.TabIndex = 44;
      this.FUCDeleteApplication.Title = " Delete Application ";
      // 
      // FUCEditApplication
      // 
      this.FUCEditApplication.ConfirmTitle = "Confirm";
      this.FUCEditApplication.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.FUCEditApplication.Location = new System.Drawing.Point(0, 310);
      this.FUCEditApplication.ModeDelete = false;
      this.FUCEditApplication.Name = "FUCEditApplication";
      this.FUCEditApplication.Size = new System.Drawing.Size(591, 121);
      this.FUCEditApplication.TabIndex = 45;
      this.FUCEditApplication.Title = " Edit Application ";
      // 
      // CUCApplicationManager
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCListBoxApplications);
      this.Controls.Add(this.FUCAddApplication);
      this.Controls.Add(this.FUCDeleteApplication);
      this.Controls.Add(this.FUCEditApplication);
      this.Controls.Add(this.pnlTop);
      this.Controls.Add(this.pnlKeys);
      this.Name = "CUCApplicationManager";
      this.Size = new System.Drawing.Size(591, 465);
      this.pnlKeys.ResumeLayout(false);
      this.pnlTop.ResumeLayout(false);
      this.pnlTop.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlKeys;
    private System.Windows.Forms.Button btnClose;
    private System.Windows.Forms.Button btnEditApplication;
    private System.Windows.Forms.Button btnDeleteApplication;
    private System.Windows.Forms.Button btnAddApplication;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Panel pnlTop;
    private System.Windows.Forms.Label label7;
    private CUCApplication FUCAddApplication;
    private CUCApplication FUCDeleteApplication;
    private CUCApplication FUCEditApplication;
    private CUCListBox FUCListBoxApplications;
    private System.Windows.Forms.Button btnCancel;
  }
}
