﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Task;
using CommandProtocol;
using CPLaserAreaScanner;
using LaserScanner;
//
namespace LASManager
{
  public delegate void DOnPulseStepListStart();
  public delegate Boolean DOnPulseStepListBusy();
  public delegate void DOnPulseStepListEnd();
  public delegate void DOnPulseStepListAbort();

  public class CProcessPulseStepList
  {
    private CProcess FProcess;
    private DOnPulseStepListStart FOnPulseStepListStart;
    private DOnPulseStepListBusy FOnPulseStepListBusy;
    private DOnPulseStepListEnd FOnPulseStepListEnd;
    private DOnPulseStepListAbort FOnPulseStepListAbort;
    //
    // NC CLaserStepList FLaserStepList;
    public CProcessPulseStepList(DOnPulseStepListStart onpulseStepListstart,
                              DOnPulseStepListBusy onpulseStepListbusy,
                              DOnPulseStepListEnd onpulseStepListend,
                              DOnPulseStepListAbort onpulseStepListabort)
    {
      FOnPulseStepListStart = onpulseStepListstart;
      FOnPulseStepListBusy = onpulseStepListbusy;
      FOnPulseStepListEnd = onpulseStepListend;
      FOnPulseStepListAbort = onpulseStepListabort;
      FProcess = new CProcess("PulseStepList",
                              SelfOnExecutionStart,
                              SelfOnExecutionBusy,
                              SelfOnExecutionEnd,
                              SelfOnExecutionAbort);
    }

    // NC 
    //public void SetLaserStepList(CLaserStepList lasersteplist)
    //{
    //  FLaserStepList = lasersteplist;
    //}

    public Boolean Start()
    {
      //if (FLaserStepList is CLaserStepList)
      //{        
        return FProcess.Start();
      //}
      //return false;
    }

    public Boolean Abort()
    {
      if (FProcess.IsActive())
      {
        return FProcess.Abort();
      }
      return false;
    }

    protected void SelfOnExecutionStart(RTaskData data)
    {
      if (FOnPulseStepListStart is DOnPulseStepListStart)
      {
        FOnPulseStepListStart();
      }
    }
    protected Boolean SelfOnExecutionBusy(RTaskData data)
    {
      if (FOnPulseStepListBusy is DOnPulseStepListBusy)
      {
        return FOnPulseStepListBusy();
      }
      return false;
    }
    protected void SelfOnExecutionEnd(RTaskData data) 
    {
      if (FOnPulseStepListEnd is DOnPulseStepListEnd)
      {
        FOnPulseStepListEnd();
      }
    }
    protected void SelfOnExecutionAbort(RTaskData data)
    {
      if (FOnPulseStepListAbort is DOnPulseStepListAbort)
      {
        FOnPulseStepListAbort();
      }
    }


  }
}
