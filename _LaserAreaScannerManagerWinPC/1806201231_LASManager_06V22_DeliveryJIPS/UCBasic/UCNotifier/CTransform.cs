﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCNotifier
{
  public class CTransform
  {
    //
    //---------------------------------------------------------------
    //  Segment - Const
    //---------------------------------------------------------------
    //
    const Int32 SIZE_CHARACTERSET = 30;
    const Int32 OFFSET_POSITIVE = 2;
    const Int32 OFFSET_NEGATIVE = -1;
    const Int32 INIT_BASEINDEX = 1492; // change nevermore!!!!
    //
    //---------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------
    //
    private Char[] FCharacterSet;
    private Int32 FIndex;
    private Boolean FToggle = true;
    private Int32[] FSegmentSizes = { 4, 6, 5, 4 };
    //
    //---------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------
    //
    public CTransform()
    {
      Int32 Size = SIZE_CHARACTERSET; 
      FCharacterSet = new Char[Size];
      FCharacterSet[0] = 'H';     
      FCharacterSet[1] = '0';
      FCharacterSet[2] = 'M';
      FCharacterSet[3] = 'D';      
      FCharacterSet[4] = 'Q';
      FCharacterSet[5] = '9';      
      FCharacterSet[6] = 'B';
      FCharacterSet[7] = 'X';      
      FCharacterSet[8] = 'E';
      FCharacterSet[9] = '6';
      FCharacterSet[10] = '2';
      FCharacterSet[11] = 'Z';      
      FCharacterSet[12] = 'C';
      FCharacterSet[13] = '1';
      FCharacterSet[14] = 'N';      
      FCharacterSet[15] = '6';
      FCharacterSet[16] = 'A';
      FCharacterSet[17] = 'T';     
      FCharacterSet[18] = 'Y';
      FCharacterSet[19] = 'K';      
      FCharacterSet[20] = '3';      
      FCharacterSet[21] = 'W';
      FCharacterSet[22] = 'U';
      FCharacterSet[23] = '7';      
      FCharacterSet[24] = 'R';      
      FCharacterSet[25] = 'F';      
      FCharacterSet[26] = '5';
      FCharacterSet[27] = 'P';
      FCharacterSet[28] = 'S';      
      FCharacterSet[29] = '8';
    }
    //
    //---------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------

    //
    //---------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------
    //
    public void Reset(Int32 productnumber)
    {
      FIndex = productnumber;
      FToggle = true;
    }

    public void Reset(Int32 productnumber, Int32 baseindex)
    {
      FIndex = productnumber + baseindex;
      FToggle = true;
    }

    private Int32 BuildIndex(Int32 index)
    {
      if (FToggle)
      {
        FToggle = false;
        FIndex += index + OFFSET_POSITIVE;
      } else
      {
        FToggle = true;
        FIndex += index + OFFSET_NEGATIVE;
      }
      FIndex = FIndex % SIZE_CHARACTERSET;
      FIndex = Math.Max(0, FIndex);
      FIndex = Math.Min(SIZE_CHARACTERSET - 1, FIndex);
      // debug FNotifier.Write("{0} -> {1}", index, FIndex);
      return FIndex;
    }

    private String BuildSegment(String productnumberdevice, Int32 targetsize)
    {
      String Result = "";
      Int32 SI = 0;
      Int32 SL = productnumberdevice.Length;
      Int32 Index = 3;
      for (Int32 CI = targetsize - 1; 0 <= CI; CI--)
      {
        Char CS = productnumberdevice[SI];
        switch (CS)
        {
          case '0':
            Index = BuildIndex(9 + Index);
            Result += FCharacterSet[Index];
            break;
          case '1':
            Index = BuildIndex(4 + Index);
            Result += FCharacterSet[Index];
            break;
          case '2':
            Index = BuildIndex(0 + Index);
            Result += FCharacterSet[Index];
            break;
          case '3':
            Index = BuildIndex(7 + Index);
            Result += FCharacterSet[Index];
            break;
          case '4':
            Index = BuildIndex(2 + Index);
            Result += FCharacterSet[Index];
            break;
          case '5':
            Index = BuildIndex(6 + Index);
            Result += FCharacterSet[Index];
            break;
          case '6':
            Index = BuildIndex(3 + Index);
            Result += FCharacterSet[Index];
            break;
          case '7':
            Index = BuildIndex(1 + Index);
            Result += FCharacterSet[Index];
            break;
          case '8':
            Index = BuildIndex(8 + Index);
            Result += FCharacterSet[Index];
            break;
          case '9':
            Index = BuildIndex(5 + Index);
            Result += FCharacterSet[Index];
            break;
        }
        SI++;
        if (SL <= SI)
        {
          SI = 0;
        } 
      }
      return Result;
    }
    //
    //---------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------
    //
    private Int32 Build(String text, Int32 sum)
    {
      Int32 Result = 0;
      for (Int32 CI = 0; CI < text.Length; CI++)
      {
        Byte B = (Byte)text[CI];
        Result += (Int32)B;
        Result += (Int32)(B << 6);
        Result += (Int32)(B << 11);
      }
      return Result;
    }

    public String BuildProductKey(Int32 productnumberdevice)
    {
      String ProductNumberCoded = "";
      Reset(productnumberdevice);
      String ProductNumberDevice = productnumberdevice.ToString();
      String[] CodeSegments = new String[FSegmentSizes.Length];
      for (Int32 SI = CodeSegments.Length - 1; 0 <= SI; SI--)
      {
        CodeSegments[SI] = BuildSegment(ProductNumberDevice, FSegmentSizes[SI]);
      }
      for (Int32 CI = 0; CI < CodeSegments.Length; CI++)
      {
        if (0 == CI)
        {
          ProductNumberCoded = String.Format("{0}", CodeSegments[CI]);
        } else
        {
          ProductNumberCoded += String.Format("-{0}", CodeSegments[CI]);
        }
      }
      return ProductNumberCoded;
    }

    public String BuildProductKey(Int32 productnumberdevice, Int32[] segmentsizes)
    {
      FSegmentSizes = segmentsizes;
      String ProductNumberCoded = "";
      Reset(productnumberdevice);
      String ProductNumberDevice = productnumberdevice.ToString();
      String[] CodeSegments = new String[segmentsizes.Length];
      for (Int32 SI = CodeSegments.Length - 1; 0 <= SI; SI--)
      {
        CodeSegments[SI] = BuildSegment(ProductNumberDevice, segmentsizes[SI]);
      }
      for (Int32 CI = 0; CI < CodeSegments.Length; CI++)
      {
        if (0 == CI)
        {
          ProductNumberCoded = String.Format("{0}", CodeSegments[CI]);
        } else
        {
          ProductNumberCoded += String.Format("-{0}", CodeSegments[CI]);
        }
      }
      return ProductNumberCoded;
    }

    public Int32 BuildDevice(String applicationkey, 
                             String devicename, 
                             String version, 
                             String date,
                             Boolean discountuser, 
                             String user, 
                             Int32 serialnumber)
    {
      Int32 Result = INIT_BASEINDEX;
      Result += 5 * Build(applicationkey, Result);
      Result += 6 * Build(devicename, Result);
      Result += 4 * Build(version, Result);
      Result += 2 * Build(date, Result);
      if (!discountuser)
      {
        Result += 3 * Build(user, Result);
      }
      Result += serialnumber;
      return Result;
    }

    public Int32 BuildApplication(String applicationname, String version, String date,
                                  String company, String street, String streetnumber,
                                  String postalcode, String city,
                                  Boolean discountuser, String user)
    {
      Int32 Result = INIT_BASEINDEX;
      Result += 7 * Build(applicationname, Result);
      Result += 5 * Build(version, Result);
      Result += 8 * Build(date, Result);
      Result += 2 * Build(company, Result);
      Result += 4 * Build(street, Result);
      Result += 1 * Build(streetnumber, Result);
      Result += 9 * Build(postalcode, Result);
      Result += 6 * Build(city, Result);
      if (!discountuser)
      {
        Result += 3 * Build(user, Result);
      }
      return Result;
    }

    public String BuildApplication(Int32 productnumberdevice, Int32 baseindex)
    {

      String ProductNumberCoded = "";
      Reset(productnumberdevice, baseindex);
      String ProductNumberDevice = productnumberdevice.ToString();
      String[] CodeSegments = new String[FSegmentSizes.Length];
      for (Int32 SI = CodeSegments.Length - 1; 0 <= SI; SI--)
      {
        CodeSegments[SI] = BuildSegment(ProductNumberDevice, FSegmentSizes[SI]);
      }
      for (Int32 CI = 0; CI < CodeSegments.Length; CI++)
      {
        if (0 == CI)
        {
          ProductNumberCoded = String.Format("{0}", CodeSegments[CI]);
        } else
        {
          ProductNumberCoded += String.Format("-{0}", CodeSegments[CI]);
        }
      }
      return ProductNumberCoded;
    }
  }
}
