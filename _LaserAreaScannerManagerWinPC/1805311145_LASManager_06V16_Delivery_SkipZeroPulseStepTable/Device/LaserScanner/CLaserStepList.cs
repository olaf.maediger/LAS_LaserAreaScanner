﻿using System;
using System.Collections.Generic;
using System.Text;
//
using TextFile;
//
namespace LaserScanner
{
  public class CLaserStepList : List<CLaserStep>
  {
    private UInt32 FRowCount, FColCount;
    private UInt32 FDelayMotionShortms, FDelayMotionLongms;
    private String FStepOrder;

    public CLaserStepList(UInt32 rowcount, UInt32 colcount,
                          UInt32 delaymotionshortms, UInt32 delaymotionlongms,
                          String steporder)
    {
      FRowCount = (UInt32)Math.Max(1, (UInt32)rowcount);
      FColCount = (UInt32)Math.Max(1, (UInt32)colcount);
      FDelayMotionShortms = delaymotionshortms;
      FDelayMotionLongms = delaymotionlongms;
      FStepOrder = steporder;
    }
    public UInt32 RowCount
    {
      get { return FRowCount; }
      set { FRowCount = value; }
    }
    public UInt32 ColCount
    {
      get { return FColCount; }
      set { FColCount = value; }
    }
    public UInt32 DelayMotionShortms
    {
      get { return FDelayMotionShortms; }
      set { FDelayMotionShortms = value; }
    }
    public UInt32 DelayMotionLongms
    {
      get { return FDelayMotionLongms; }
      set { FDelayMotionLongms = value; }
    }
    public String StepOrder
    {
      get { return FStepOrder; }
      set { FStepOrder = value; }
    }
    public Boolean WriteToFile(String filename)
    {
      try
      {
        CTextFile TextFile = new CTextFile();
        if (TextFile.OpenWrite(filename))
        {
          TextFile.Write(String.Format("{0,5} {1,5} {2,5} {3,5} {4}\r\n", 
                                       FRowCount, FColCount,
                                       FDelayMotionShortms, FDelayMotionLongms, 
                                       FStepOrder));
          foreach (CLaserStep LS in this)
          {
            TextFile.Write(String.Format("{0,5}{1,5}{2,5}{3,5}{4,5}\r\n",
                                         LS.PositionX, LS.PositionY,
                                         LS.PulsePeriodms, LS.PulseCount,
                                         LS.DelayMotionms));
          }
          return TextFile.Close();
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public Boolean ReadFromFile(String filename)
    {
      try
      {
        CTextFile TextFile = new CTextFile();
        if (TextFile.OpenRead(filename))
        {
          Boolean Result = TextFile.Read(out FRowCount);
          Result &= TextFile.Read(out FColCount);
          Result &= TextFile.Read(out FDelayMotionShortms);
          Result &= TextFile.Read(out FDelayMotionLongms);
          Result &= TextFile.Read(out FStepOrder);
          UInt32 LSC = FRowCount * FColCount;
          for (UInt32 LSI = 0; LSI < LSC; LSI++)
          {
            UInt16 PX, PY, PP, PC, DMMS;
            Result &= TextFile.Read(out PX);
            Result &= TextFile.Read(out PY);
            Result &= TextFile.Read(out PP);
            Result &= TextFile.Read(out PC);
            Result &= TextFile.Read(out DMMS);
            if (Result)
            {
              CLaserStep LS = new CLaserStep(PX, PY, PP, PC, DMMS);
              if (LS is CLaserStep)
              {
                this.Add(LS);
              }
            }
          }
          Result &= TextFile.Close();
          Result &= (LSC == this.Count);
          return Result;
        }
        return false;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

  }
}

