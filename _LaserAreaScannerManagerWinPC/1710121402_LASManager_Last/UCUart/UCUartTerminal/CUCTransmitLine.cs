﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
//
using UCNotifier;
using Uart;
using TextFile;
//
namespace UCUartTerminal
{
  public delegate void DOnTransmitText(String text, Int32 delaycharacter, Int32 delaycr, Int32 delaylf);
  public delegate void DOnShowDialogOpenClose(Boolean show);
  //
  public partial class CUCTransmitLine : UserControl
  { //
    //-------------------------------
    //  Segment - Constant
    //-------------------------------
    //
    private String FILENAME_TRANSMITLINES = "TransmitLines.txt";
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnTransmitText FOnTransmitText;
    private DOnShowDialogOpenClose FOnShowDialogOpenClose;
    //
    //-------------------------------
    //  Segment - Constructor
    //-------------------------------
    //
    public CUCTransmitLine()
    {
      InitializeComponent();
      //
      DialogLoadTransmitLines.FileName = FILENAME_TRANSMITLINES;
      DialogSaveTransmitLines.FileName = FILENAME_TRANSMITLINES;
      //
      cbxTransmitLine.Items.Clear();
      LoadTransmitLines(FILENAME_TRANSMITLINES);
      //
      cbxShowOpenClose.Checked = true;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnShowDialogOpenClose(DOnShowDialogOpenClose value)
    {
      FOnShowDialogOpenClose = value;
    }
    public void SetOnTransmitText(DOnTransmitText value)
    {
      FOnTransmitText = value;
    }

    public Int32 DelayCharacter
    {
      get { return (Int32)nudDelayCharacter.Value; }
      set { nudDelayCharacter.Value = value; }
    }
    public Int32 DelayCR
    {
      get { return (Int32)nudDelayCR.Value; }
      set { nudDelayCR.Value = value; }
    }
    public Int32 DelayLF
    {
      get { return (Int32)nudDelayLF.Value; }
      set { nudDelayLF.Value = value; }
    }
    //
    //-------------------------------
    //  Segment - Helper
    //-------------------------------
    //
    private Boolean LoadTransmitLines(String filename)
    {
      if (File.Exists(filename))
      {
        CTextFile TextFile = new CTextFile();
        if (TextFile.OpenRead(filename))
        {
          String Line;
          while (!TextFile.IsEndOfFile())
          {
            if (TextFile.Read(out Line))
            {
              cbxTransmitLine.Items.Add(Line);
            }
          }
          TextFile.Close();
        }
        return true;
      }
      return false;
    }

    private Boolean SaveTransmitLines(String filename)
    {
      CTextFile TextFile = new CTextFile();
      TextFile.OpenWrite(filename);
      foreach (String Line in cbxTransmitLine.Items)
      {
        TextFile.Write(Line + CUart.CHARACTER_CR + CUart.CHARACTER_LF);
      }
      TextFile.Close();
      return true;
    }

    private void TransmitText()
    {
      String Text = cbxTransmitLine.Text;
      if (cbxCR.Checked)
      {
        Text += CUart.CHARACTER_CR;
      }
      if (cbxLF.Checked)
      {
        Text += CUart.CHARACTER_LF;
      }
      if (FOnTransmitText is DOnTransmitText)
      {
        FOnTransmitText(Text, DelayCharacter, DelayCR, DelayLF);
      }
    }
    //
    //-------------------------------
    //  Segment - Event
    //-------------------------------
    // cbxSendLine: Enter -> Send
    private void cbxSendData_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        TransmitText();
      }
    }

    private void btnTransmitLine_Click(object sender, EventArgs e)
    {
      TransmitText();
    }

    private void cbxShowDialogOpenClose_CheckedChanged(object sender, EventArgs e)
    {
      if (FOnShowDialogOpenClose is DOnShowDialogOpenClose)
      {
        FOnShowDialogOpenClose(cbxShowOpenClose.Checked);
      }
    }
  
    private void mitSaveTransmitLines_Click(object sender, EventArgs e)
    {
      // NC DialogSaveTransmitLines.FileName = "";
      if (DialogResult.OK == DialogSaveTransmitLines.ShowDialog())
      {
        SaveTransmitLines(DialogSaveTransmitLines.FileName);
      }
    }

    private void mitLoadTransmitLines_Click(object sender, EventArgs e)
    {
      // NC DialogLoadTransmitLines.FileName = "";
      if (DialogResult.OK == DialogLoadTransmitLines.ShowDialog())
      {
        LoadTransmitLines(DialogLoadTransmitLines.FileName);
      }
    }
    //
    //-------------------------------
    //  Segment - Public Management
    //-------------------------------
    //


  }
}

    ////
    ////-------------------------------
    ////  Segment - Property
    ////-------------------------------
    ////

    //public void SetNotifier(CNotifier notifier)
    //{
    //  FNotifier = notifier;
    //}



   //
   // private void cbxShowOpenClose_CheckedChanged(object sender, EventArgs e)
   // {
   //   FUCUartOpenClose.Visible = cbxShowOpenClose.Checked;
   // }
   // //
   // //-------------------------------
   // //  Segment - Public Management
   // //-------------------------------
   // //
   // public void TransmitLine(String line)
   // {
   //   String Line = line;
   //   if (cbxCR.Checked)
   //   {
   //     Line += CUart.CHARACTER_CR;
   //   }
   //   if (cbxLF.Checked)
   //   {
   //     Line += CUart.CHARACTER_LF;
   //   }
   //   FUart.WriteText(Line);
   // }

   // public void ClearTerminalText()
   // {
   //   FUCMultiColorText.Clear();
   // }

   // public void EnableTerminalText(Boolean enable)
   // {
   //   FUCMultiColorText.EnableText(enable);
   // }

   // public void AddTerminalLine(Color color, String text)
   // {
   //   AddColoredText(color, text);
   // }

   // public void AddTerminalRxData(String text)
   // {
   //   AddColoredText(FRxdColor, text);
   // }

   // public void AddTerminalTxData(String text)
   // {
   //   AddColoredText(FTxdColor, text);
   // }

