﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using CommandProtocol;
//
namespace CPLaserAreaScanner
{
  public class CPulseMatrixLaser : CCommand
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const String HEADER = "PML";
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private Int32 FPeriod;
    private Int32 FPulses;
    private Int32 FRepetitions;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CPulseMatrixLaser(Int32 period, Int32 pulses, Int32 repetitions)
      : base(HEADER)
    {
      FPeriod = period;
      FPulses = pulses;
      FRepetitions = repetitions;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public override string GetCommandHeader()
    {
      return FHeader;
    }

    public override string GetCommandText()
    {
      return String.Format("{0} {1} {2} {3}", FHeader, FPeriod, FPulses, FRepetitions);
    }


  }
}
