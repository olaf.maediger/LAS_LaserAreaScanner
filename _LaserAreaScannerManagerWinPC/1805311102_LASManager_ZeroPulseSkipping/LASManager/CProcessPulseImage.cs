﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using Task;
using CommandProtocol;
using CPLaserAreaScanner;
using LaserScanner;
//
namespace LASManager
{
  public delegate void DOnPulseImageStart();
  public delegate Boolean DOnPulseImageBusy();
  public delegate void DOnPulseImageEnd();
  public delegate void DOnPulseImageAbort();

  public class CProcessPulseImage
  {
    private CProcess FProcess;
    private DOnPulseImageStart FOnPulseImageStart;
    private DOnPulseImageBusy FOnPulseImageBusy;
    private DOnPulseImageEnd FOnPulseImageEnd;
    private DOnPulseImageAbort FOnPulseImageAbort;
    //
    // NC CLaserStepList FLaserStepList;
    public CProcessPulseImage(DOnPulseImageStart onpulseimagestart,
                              DOnPulseImageBusy onpulseimagebusy,
                              DOnPulseImageEnd onpulseimageend,
                              DOnPulseImageAbort onpulseimageabort)
    {
      FOnPulseImageStart = onpulseimagestart;
      FOnPulseImageBusy = onpulseimagebusy;
      FOnPulseImageEnd = onpulseimageend;
      FOnPulseImageAbort = onpulseimageabort;
      FProcess = new CProcess("PulseImage",
                              SelfOnExecutionStart,
                              SelfOnExecutionBusy,
                              SelfOnExecutionEnd,
                              SelfOnExecutionAbort);
    }

    // NC 
    //public void SetLaserStepList(CLaserStepList lasersteplist)
    //{
    //  FLaserStepList = lasersteplist;
    //}

    public Boolean Start()
    {
      //if (FLaserStepList is CLaserStepList)
      //{        
        return FProcess.Start();
      //}
      //return false;
    }

    public Boolean Abort()
    {
      if (FProcess.IsActive())
      {
        return FProcess.Abort();
      }
      return false;
    }

    protected void SelfOnExecutionStart(RTaskData data)
    {
      if (FOnPulseImageStart is DOnPulseImageStart)
      {
        FOnPulseImageStart();
      }
    }
    protected Boolean SelfOnExecutionBusy(RTaskData data)
    {
      if (FOnPulseImageBusy is DOnPulseImageBusy)
      {
        return FOnPulseImageBusy();
      }
      return false;
    }
    protected void SelfOnExecutionEnd(RTaskData data) 
    {
      if (FOnPulseImageEnd is DOnPulseImageEnd)
      {
        FOnPulseImageEnd();
      }
    }
    protected void SelfOnExecutionAbort(RTaskData data)
    {
      if (FOnPulseImageAbort is DOnPulseImageAbort)
      {
        FOnPulseImageAbort();
      }
    }


  }
}
