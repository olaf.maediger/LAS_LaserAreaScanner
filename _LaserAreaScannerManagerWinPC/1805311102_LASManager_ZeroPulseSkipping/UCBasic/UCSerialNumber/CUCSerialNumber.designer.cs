﻿namespace UCSerialNumber
{
  partial class CUCSerialNumber
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label2 = new System.Windows.Forms.Label();
      this.lblTitle = new System.Windows.Forms.Label();
      this.cbxSerialNumberDevices = new System.Windows.Forms.ComboBox();
      this.SuspendLayout();
      // 
      // label2
      // 
      this.label2.BackColor = System.Drawing.SystemColors.Info;
      this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(0, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(469, 64);
      this.label2.TabIndex = 152;
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblTitle
      // 
      this.lblTitle.AutoSize = true;
      this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
      this.lblTitle.Location = new System.Drawing.Point(15, 12);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(341, 15);
      this.lblTitle.TabIndex = 154;
      this.lblTitle.Text = "Select connected ...-Device by SerialNumber and ProductKey:";
      // 
      // cbxSerialNumberDevices
      // 
      this.cbxSerialNumberDevices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxSerialNumberDevices.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbxSerialNumberDevices.FormattingEnabled = true;
      this.cbxSerialNumberDevices.Location = new System.Drawing.Point(14, 30);
      this.cbxSerialNumberDevices.Name = "cbxSerialNumberDevices";
      this.cbxSerialNumberDevices.Size = new System.Drawing.Size(438, 24);
      this.cbxSerialNumberDevices.TabIndex = 155;
      this.cbxSerialNumberDevices.SelectedIndexChanged += new System.EventHandler(this.cbxSerialNumberDevices_SelectedIndexChanged);
      // 
      // CUCSerialNumber
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.cbxSerialNumberDevices);
      this.Controls.Add(this.lblTitle);
      this.Controls.Add(this.label2);
      this.Name = "CUCSerialNumber";
      this.Size = new System.Drawing.Size(469, 64);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.ComboBox cbxSerialNumberDevices;
  }
}
