using System;
using System.Collections.Generic;
using System.Text;

namespace Uart
{
  public class CRingBuffer 
  {
    public const Char CARRIAGE_RETURN = (Char)0x0D;

    private Char[] FValues = null;
    private Int32 FReadIndex = 0;
    private Int32 FWriteIndex = 0;
    private Int32 FLineDetected = 0;

    public Int32 Size
    {
      get { return FValues.Length; }
    }
    public Boolean IsEmpty
    {
      get { return (FReadIndex == FWriteIndex); }
    }
    public Boolean IsFull
    {
      get { return (FWriteIndex == (FReadIndex - 1)); }
    }
    public Int32 FillCount
    {
      get
      {
        if (FReadIndex < FWriteIndex)
        {
          return FWriteIndex - FReadIndex;
        } else
          if (FReadIndex == FWriteIndex)
          {
            return 0;
          } else
          {
          return Size + FWriteIndex - FReadIndex;
        }
      }
    }
    public Int32 EmptyCount
    {
      get
      {
        return Size - FillCount;
      }
    }
    // Line detected (RO)
    public Boolean LineDetected
    {
      get { return (0 < FLineDetected); }
    }


    public CRingBuffer(Int32 newcount)
    {
      FValues = new Char[newcount];
      FReadIndex = 0;
      FWriteIndex = 0;
    }

    public Boolean WriteCharacter(Char newvalue)
    {
      if (IsFull) return false;
      FValues[FWriteIndex] = newvalue;
      if (CARRIAGE_RETURN == newvalue)
      {
        FLineDetected++;
      }
      if (FWriteIndex < Size - 1)
      {
        FWriteIndex++;
      } else
      {
        FWriteIndex = 0;
      }
      return true;
    }

    // holt genau ein Zeichen (falls vorhanden) aus RXDFifo 
    public Boolean ReadCharacter(out Char newcharacter)
    {
      newcharacter = (Char)0x00;
      if (IsEmpty) return false;
      newcharacter = FValues[FReadIndex];
      if (CARRIAGE_RETURN == newcharacter)
      {
        FLineDetected--;
      }
      if (FReadIndex < Size - 1)
      {
        FReadIndex++;
      } else
      {
        FReadIndex = 0;
      }
      return true;
    }

    // holt genau eine Zeile (falls vorhanden) aus RXDFifo (OHNE CR)
    public Boolean ReadLine(out String newline)
    {
      newline = "";
      if (LineDetected)
      {
        Boolean Endloop = false;
        while (!Endloop)
        {
          Char Character;
          if (!(ReadCharacter(out Character)))
          {
            return false;
          }
          if (CARRIAGE_RETURN == Character)
          {
            return true;
          }
          newline += Character;
        }
      }
      return false;
    }

    // holt allen angesammelten Text (falls vorhanden) aus RXDFifo
    // inclusive ALLER Sonderzeichen!
    public Boolean ReadText(out String newtext)
    {
      newtext = "";
      if (IsEmpty)
      {
        return false;
      }
      Boolean Endloop = false;
      while (!Endloop)
      {
        Char Character;
        if (ReadCharacter(out Character))
        {
          newtext += Character;
        } else
        {
          Endloop = true;
        }
      }
      return (0 < newtext.Length);
    }


  }
}
