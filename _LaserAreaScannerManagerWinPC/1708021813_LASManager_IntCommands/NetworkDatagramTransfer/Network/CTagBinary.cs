﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace Network
{ 
  //
  //--------------------------------------------------------------------
  //  Segment - Common
  //--------------------------------------------------------------------
  //
  public abstract class CTagBinary : CTagBase
  {
    protected Byte[] FValues;
    public Byte[] Values
    {
      get { return FValues; }
      set { FValues = value; }
    }

    public CByteList GetByteList()
    {
      CByteList Result = new CByteList();
      Result.Add(FType);
      Int32 Count = FValues.Length;
      for (Int32 VI = 0; VI < Count; VI++)
      {
        Result.Add(Values[VI]);
      }
      return Result;
    }
  };

  public class CTagBinaryList : List<CTagBinary>
  { // only linear lists!
    public CByteList GetByteList()
    {
      CByteList Result = new CByteList();
      foreach (CTagBinary TagBinary in this)
      {
        CByteList TagByteList = TagBinary.GetByteList();
        foreach (Byte BValue in TagByteList)
        {
          Result.Add(BValue);
        }
      }
      return Result;
    }

    public CStringList GetInfo()
    {
      CStringList StringList = new CStringList();
      foreach (CTagBinary TagBinary in this)
      {
        String Line = String.Format("{0} {1} {2}",
                                    TagBinary.GetGroup(), TagBinary.GetName(), TagBinary.GetValue());
        StringList.Add(Line);
      }
      return StringList;
    }
  };
  //
  //--------------------------------------------------------------------
  //  Segment - Basic - Scalar/Vector/Matrix
  //--------------------------------------------------------------------
  //
  public abstract class CTBScalar : CTagBinary
  {
    public override String GetGroup()
    {
      return "BScalar";
    }
  };

  public abstract class CTBVector : CTagBinary
  {
    public override String GetGroup()
    {
      return "BVector";
    }
  };

  public abstract class CTBMatrix : CTagBinary
  {
    public override String GetGroup()
    {
      return "BMatrix";
    }
  }; 
  //
  //--------------------------------------------------------------------
  //  Segment - MagicWord
  //--------------------------------------------------------------------
  //// !!!!!!!!!!!!!!!! REDEFINE CTBMagicWord AS CTTMagicWord !!!!!!!!!!!!!!!!!!!
  public class CTBMagicWord : CTBScalar
  {
    public CTBMagicWord(Char[] values)
    {
      FType = (Byte)values[0];
      FValues = new Byte[5];
      FValues[0] = (Byte)values[1];
      FValues[1] = (Byte)values[2];
      FValues[2] = (Byte)values[3];
      FValues[3] = (Byte)values[4];
      FValues[4] = (Byte)values[5];
    }

    public CTBMagicWord(Byte[] values, ref int index)
    {
      FType = values[0];
      FValues = new Byte[5];
      FValues[0] = values[1];
      FValues[1] = values[2];
      FValues[2] = values[3];
      FValues[3] = values[4];
      FValues[4] = values[5];
      index += 6;
    }

    public override String GetName()
    {
      return "MagicWord";
    }
    public override String GetValue()
    {
      return "";
    }

    public Char[] GetVectorChar()
    {
      Char[] Result = new Char[6];
      Result[0] = (Char)FType;
      Result[1] = (Char)FValues[0];
      Result[2] = (Char)FValues[1];
      Result[3] = (Char)FValues[2];
      Result[4] = (Char)FValues[3];
      Result[5] = (Char)FValues[4];
      return Result;
    }
  };
  //
  //--------------------------------------------------------------------
  //  Segment - Scalar
  //--------------------------------------------------------------------
  //
  public class CTBScalarByte : CTBScalar
  {
    public CTBScalarByte(Byte value)
    {
      FType = TYPE_SCALAR_BYTE;
      FValues = new Byte[1];
      FValues[0] = value;
    }

    public override String GetName()
    {
      return "Byte";
    }
    public override String GetValue()
    {
      return String.Format("0x{0:00}", FValues[0]);
    }
  };

  public class CTBScalarChar : CTBScalar
  {
    public CTBScalarChar(Char value)
    {
      FType = TYPE_SCALAR_CHAR;
      FValues = new Byte[1];
      FValues[0] = (Byte)value;
    }

    public override String GetName()
    {
      return "Char";
    }
    public override String GetValue()
    {
      return String.Format("{0}", (Char)FValues[0]);
    }
  };

  public class CTBScalarInt16 : CTBScalar
  {
    public CTBScalarInt16(Int16 value)
    {
      FType = TYPE_SCALAR_INT16;
      FValues = new Byte[2];
      FValues[0] = (Byte)((0x00FF & value) >> 0);
      FValues[1] = (Byte)((0xFF00 & value) >> 8);
    }

    public override String GetName()
    {
      return "Int16";
    }
    public override String GetValue()
    {
      return String.Format("...");
    }
  };

  public class CTBScalarInt32 : CTBScalar
  {
    public CTBScalarInt32(Int32 value)
    {
      FType = TYPE_SCALAR_INT32;
      FValues = new Byte[4];
      FValues[0] = (Byte)((0x000000FF & value) >> 0);
      FValues[1] = (Byte)((0x0000FF00 & value) >> 8);
      FValues[2] = (Byte)((0x00FF0000 & value) >> 16);
      FValues[3] = (Byte)((0xFF000000 & value) >> 24);
    }
    public CTBScalarInt32(Byte[] values)
    {
      FType = TYPE_SCALAR_INT32;
      FValues = values;
    }

    public Int32 GetInt32()
    {
      Int32 Result = FValues[0] << 0;
      Result |= FValues[1] << 8;
      Result |= FValues[2] << 16;
      Result |= FValues[3] << 24;
      return Result;
    }

    public override String GetName()
    {
      return "Int32";
    }
    public override String GetValue()
    {
      return String.Format("{0}", GetInt32());
    }
  };

  public class CTBScalarDouble : CTBScalar
  {
    public CTBScalarDouble(Double value)
    {
      FType = TYPE_SCALAR_DOUBLE;
      FValues = BitConverter.GetBytes(value);
    }
    public CTBScalarDouble(Byte[] values)
    {
      FType = TYPE_SCALAR_DOUBLE;
      FValues = values;
    }

    public Double GetDouble()
    {
      Double Result = BitConverter.ToDouble(FValues, 0);
      return Result;
    }

    public override String GetName()
    {
      return "Double";
    }
    public override String GetValue()
    {
      return String.Format("{0}", GetDouble());
    }
  };

  public class CTBScalarString : CTBScalar
  {
    public CTBScalarString(String value)
    {
      FType = TYPE_SCALAR_STRING;
      Int32 SizeSource = value.Length;
      Int32 SizeTarget = 4 + 1 + SizeSource * sizeof(Byte);// sizeof(Char);
      FValues = new Byte[SizeTarget];
      FValues[0] = (Byte)((0x000000FF & SizeSource) >> 0);
      FValues[1] = (Byte)((0x0000FF00 & SizeSource) >> 8);
      FValues[2] = (Byte)((0x00FF0000 & SizeSource) >> 16);
      FValues[3] = (Byte)((0xFF000000 & SizeSource) >> 24);
      Int32 SI = 0;
      for (int TI = 4; TI < SizeTarget - 1; TI++)
      {
        FValues[TI] = (Byte)value[SI];
        SI++;
      }
      FValues[SizeTarget - 1] = 0x00;
    }

    public CTBScalarString(Byte[] values)
    {
      FType = TYPE_SCALAR_STRING;
      Int32 SizeSource = values.Length;
      Int32 SizeTarget = 4 + 1 + SizeSource * sizeof(Byte);
      FValues = new Byte[SizeTarget];
      FValues[0] = (Byte)((0x000000FF & SizeSource) >> 0);
      FValues[1] = (Byte)((0x0000FF00 & SizeSource) >> 8);
      FValues[2] = (Byte)((0x00FF0000 & SizeSource) >> 16);
      FValues[3] = (Byte)((0xFF000000 & SizeSource) >> 24);
      Int32 SI = 0;
      for (int TI = 4; TI < SizeTarget - 1; TI++)
      {
        FValues[TI] = (Byte)values[SI];
        SI++;
      }
      FValues[SizeTarget - 1] = 0x00;
    }

    public String GetString()
    {
      String Result = "";
      int L = FValues.Length - 1;
      for (int VI = 4; VI < L; VI++)
      {
        Result += (Char)FValues[VI];
      }
      return Result;
    }

    public override String GetName()
    {
      return "String";
    }
    public override String GetValue()
    {
      return String.Format("{0}", GetString());
    }
  };


  public class CTBScalarDateTime : CTBScalar
  {
    public CTBScalarDateTime(DateTime value)
    {
      FType = TYPE_SCALAR_DATETIME;
      FValues = Convert(value);
    }
    public CTBScalarDateTime(Byte[] values)
    {
      FType = TYPE_SCALAR_DATETIME;
      int Size = sizeof(UInt64);
      FValues = new Byte[Size];
      for (int VI = 0; VI < Size; VI++)
      {
        FValues[VI] = values[VI];
      }
    }

    private DateTime Convert(Byte[] value)
    {
      UInt64 LValue = BitConverter.ToUInt64(value, 0);
      return DateTime.FromBinary((Int64)LValue);
    }
    private Byte[] Convert(DateTime value)
    {
      UInt64 LValue = (UInt64)value.ToBinary();
      Byte[] BValue = BitConverter.GetBytes(LValue);
      return BValue;
    }

    public DateTime GetDateTime()
    {
      return Convert(FValues);
    }

    public override String GetName()
    {
      return "DateTime";
    }
    public override String GetValue()
    {
      return String.Format("{0}", GetDateTime());
    }
  }

  public class CTBScalarGuid : CTBScalar
  {
    public CTBScalarGuid(Guid value)
    {
      FType = TYPE_SCALAR_GUID;
      FValues = Convert(value);
    }
    public CTBScalarGuid(Byte[] values)
    {
      FType = TYPE_SCALAR_GUID;
      FValues = values;
    }

    private Guid Convert(Byte[] value)
    {
      Guid G = new Guid(value);
      return G;
    }
    private Byte[] Convert(Guid value)
    {
      Byte[] BValue = value.ToByteArray();
      return BValue;
    }

    public Guid GetGuid()
    {
      return Convert(FValues);
    }

    public override String GetName()
    {
      return "Guid";
    }
    public override String GetValue()
    {
      return String.Format("{0}", GetGuid());
    }
  }
  //
  //--------------------------------------------------------------------
  //  Segment - Vector
  //--------------------------------------------------------------------
  //
  public class CTBVectorByte : CTBVector
  {
    public CTBVectorByte(Byte[] values)
    {
      FType = TYPE_VECTOR_BYTE;
      Int32 Size = values.Length;
      FValues = new Byte[4 + Size  * sizeof(Byte)];
      FValues[0] = (Byte)((0x000000FF & Size) >> 0);
      FValues[1] = (Byte)((0x0000FF00 & Size) >> 8);
      FValues[2] = (Byte)((0x00FF0000 & Size) >> 16);
      FValues[3] = (Byte)((0xFF000000 & Size) >> 24);
      for (UInt32 BI = 0; BI < Size; BI++)
      {
        FValues[4 + BI] = values[BI];
      }
    }

    public override String GetName()
    {
      return "Byte";
    }
    public override String GetValue()
    {
      return String.Format("...");
    }
  };

  public class CTBVectorChar : CTBVector
  {
    public CTBVectorChar(Char[] values)
    {
      FType = TYPE_VECTOR_CHAR;
      Int32 Size = values.Length;
      FValues = new Byte[4 + Size  * sizeof(Byte)];
      FValues[0] = (Byte)((0x000000FF & Size) >> 0);
      FValues[1] = (Byte)((0x0000FF00 & Size) >> 8);
      FValues[2] = (Byte)((0x00FF0000 & Size) >> 16);
      FValues[3] = (Byte)((0xFF000000 & Size) >> 24);
      for (UInt32 BI = 0; BI < Size; BI++)
      {
        FValues[4 + BI] = (Byte)values[BI];
      }
    }

    public override String GetName()
    {
      return "Char";
    }
    public override String GetValue()
    {
      return String.Format("...");
    }
  };


  public class CTBVectorInt32 : CTBVector
  {
    public CTBVectorInt32(Int32[] values)
    {
      FType = TYPE_VECTOR_INT32;
      Int32 Size = values.Length;
      FValues = new Byte[4 + Size  * sizeof(Int32)];
      FValues[0] = (Byte)((0x000000FF & Size) >> 0);
      FValues[1] = (Byte)((0x0000FF00 & Size) >> 8);
      FValues[2] = (Byte)((0x00FF0000 & Size) >> 16);
      FValues[3] = (Byte)((0xFF000000 & Size) >> 24);
      Int32 SI = 0;
      Int32 TI = 4;
      for (UInt32 BI = 0; BI < Size; BI++)
      {
        Int32 Value = values[SI];
        FValues[TI] = (Byte)((0x000000FF & Value) >> 0);
        TI++;
        FValues[TI] = (Byte)((0x0000FF00 & Value) >> 8);
        TI++;
        FValues[TI] = (Byte)((0x00FF0000 & Value) >> 16);
        TI++;
        FValues[TI] = (Byte)((0xFF000000 & Value) >> 24);
        TI++;
        SI++;
      }
    }

    public override String GetName()
    {
      return "Int32";
    }
    public override String GetValue()
    {
      return String.Format("...");
    }
  };

  public class CTBVectorString : CTBVector
  {    
    public CTBVectorString(String[] values)
    {
      FType = TYPE_VECTOR_STRING;
      Int32 CountValues = values.Length;
      Int32 SizeValues = 0;
      foreach (String Value in values)
      {
        SizeValues += 1 + Value.Length;
      }
      Int32 SizeTotal = 4 + 4 * CountValues + SizeValues * sizeof(Byte);
      FValues = new Byte[SizeTotal];
      // Count Values
      FValues[0] = (Byte)((0x000000FF & CountValues) >> 0);
      FValues[1] = (Byte)((0x0000FF00 & CountValues) >> 8);
      FValues[2] = (Byte)((0x00FF0000 & CountValues) >> 16);
      FValues[3] = (Byte)((0xFF000000 & CountValues) >> 24);
      Int32 TI = 4;
      for (Int32 VI = 0; VI < CountValues; VI++)
      { // Size Value
        String Value = values[VI];
        Int32 SizeValue = Value.Length;
        FValues[TI] = (Byte)((0x000000FF & SizeValue) >> 0);
        TI++;
        FValues[TI] = (Byte)((0x0000FF00 & SizeValue) >> 8);
        TI++;
        FValues[TI] = (Byte)((0x00FF0000 & SizeValue) >> 16);
        TI++;
        FValues[TI] = (Byte)((0xFF000000 & SizeValue) >> 24);
        TI++;
        // Value
        foreach (Char C in Value)
        {
          FValues[TI] = (Byte)C;
          TI++;
        }
        FValues[TI] = 0x00;
        TI++;
      }
    }

    public override String GetName()
    {
      return "String";
    }
    public override String GetValue()
    {
      return String.Format("...");
    }
  }
  //
  //--------------------------------------------------------------------
  //  Segment - Matrix
  //--------------------------------------------------------------------
  //
  public class CTBMatrixByte : CTBMatrix
  {
    public CTBMatrixByte(Byte[,] value)
    {
      FType = TYPE_MATRIX_BYTE;
      Int32 RowCount = value.GetLength(0);
      Int32 ColCount = value.GetLength(1);
      FValues = new Byte[1 + 4 + 4 + RowCount * ColCount * sizeof(Byte)];
      FValues[0] = (Byte)((0x000000FF & RowCount) >> 0);
      FValues[1] = (Byte)((0x0000FF00 & RowCount) >> 8);
      FValues[2] = (Byte)((0x00FF0000 & RowCount) >> 16);
      FValues[3] = (Byte)((0xFF000000 & RowCount) >> 24);
      FValues[4] = (Byte)((0x000000FF & ColCount) >> 0);
      FValues[5] = (Byte)((0x0000FF00 & ColCount) >> 8);
      FValues[6] = (Byte)((0x00FF0000 & ColCount) >> 16);
      FValues[7] = (Byte)((0xFF000000 & ColCount) >> 24);
      Int32 BI = 8;
      for (UInt32 RI = 0; RI < RowCount; RI++)
      {
        for (UInt32 CI = 0; CI < ColCount; CI++)
        {
          FValues[BI] = value[RI, CI];
          BI++;
        }
      }
    }

    public override String GetName()
    {
      return "Byte";
    }
    public override String GetValue()
    {
      return String.Format("...");
    }
  };

  public class CTBMatrixInt32 : CTBMatrix
  {
    public CTBMatrixInt32(Int32[,] value)
    {
      FType = TYPE_MATRIX_INT32;
      Int32 RowCount = value.GetLength(0);
      Int32 ColCount = value.GetLength(1);
      FValues = new Byte[1 + 4 + 4 + RowCount * ColCount * sizeof(Byte)];
      FValues[0] = (Byte)((0x000000FF & RowCount) >> 0);
      FValues[1] = (Byte)((0x0000FF00 & RowCount) >> 8);
      FValues[2] = (Byte)((0x00FF0000 & RowCount) >> 16);
      FValues[3] = (Byte)((0xFF000000 & RowCount) >> 24);
      FValues[4] = (Byte)((0x000000FF & ColCount) >> 0);
      FValues[5] = (Byte)((0x0000FF00 & ColCount) >> 8);
      FValues[6] = (Byte)((0x00FF0000 & ColCount) >> 16);
      FValues[7] = (Byte)((0xFF000000 & ColCount) >> 24);
      Int32 SI = 0;
      Int32 TI = 8;
      for (UInt32 RI = 0; RI < RowCount; RI++)
      {
        for (UInt32 CI = 0; CI < ColCount; CI++)
        {
          Int32 Value = value[RI, CI];
          FValues[TI] = (Byte)((0x000000FF & Value) >> 0);
          TI++;
          FValues[TI] = (Byte)((0x0000FF00 & Value) >> 8);
          TI++;
          FValues[TI] = (Byte)((0x00FF0000 & Value) >> 16);
          TI++;
          FValues[TI] = (Byte)((0xFF000000 & Value) >> 24);
          TI++;
          SI++;
        }
      }
    }

    public override String GetName()
    {
      return "Int32";
    }
    public override String GetValue()
    {
      return String.Format("...");
    }
  };

  public class CTBMatrixDouble : CTBMatrix
  {
    public CTBMatrixDouble(Double[,] value)
    {
      FType = TYPE_MATRIX_DOUBLE;
      Int32 RowCount = value.GetLength(0);
      Int32 ColCount = value.GetLength(1);
      FValues = new Byte[4 + 4 + RowCount * ColCount * sizeof(Double)];
      FValues[0] = (Byte)((0x000000FF & RowCount) >> 0);
      FValues[1] = (Byte)((0x0000FF00 & RowCount) >> 8);
      FValues[2] = (Byte)((0x00FF0000 & RowCount) >> 16);
      FValues[3] = (Byte)((0xFF000000 & RowCount) >> 24);
      FValues[4] = (Byte)((0x000000FF & ColCount) >> 0);
      FValues[5] = (Byte)((0x0000FF00 & ColCount) >> 8);
      FValues[6] = (Byte)((0x00FF0000 & ColCount) >> 16);
      FValues[7] = (Byte)((0xFF000000 & ColCount) >> 24);
      Int32 TI = 8;
      for (UInt32 RI = 0; RI < RowCount; RI++)
      {
        for (UInt32 CI = 0; CI < ColCount; CI++)
        {
          Byte[] VD = BitConverter.GetBytes(value[RI, CI]);
          for (Int32 BI = 0; BI < 8; BI++)
          {
            FValues[TI] = VD[BI];
            TI++;
          }
        }
      }
    }

    public override String GetName()
    {
      return "Double";
    }
    public override String GetValue()
    {
      return "";// String.Format("{0}", GetDouble());
    }
  };
  
  
//  Int32 L0 = values.GetLength(0);
//      Int32 L1 = values.GetLength(1);
//      for (Int32 VI0 = 0; VI0 < L0; VI0++)
//      {
//        for (Int32 VI1 = 0; VI1 < L1; VI1++)
//        {
//          CTBMatrixDouble TSD = new CTBMatrixDouble(values[VI0, VI1]);
//          TagBinaryList.Add(TSD);  
//        }
//      }


//public CTBScalarDouble(Double value)
//    {
//      FType = TYPE_SCALAR_DOUBLE;
//      FValues = 
//    }
//    public CTBScalarDouble(Byte[] values)
//    {
//      FType = TYPE_SCALAR_DOUBLE;
//      FValues = values;
//    }

//    public Double GetDouble()
//    {
//      Double Result = BitConverter.ToDouble(FValues, 0);
//      return Result;
//    }

//    public override String GetName()
//    {
//      return "Double";
//    }
//    public override String GetValue()
//    {
  //      return String.Format("{0}", GetDouble());
//    }



}

