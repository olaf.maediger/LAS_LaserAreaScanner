﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Timers;
//
using Task;
using UCNotifier;
//
namespace Network
{
  public abstract class CTcpBase
  { //
    //-------------------------------------------------------------------
    //  Segment - Constant
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Field
    //-------------------------------------------------------------------
    //
    protected CNotifier FNotifier;
    protected TcpClient FTcpClient;
    protected IPEndPoint FIpEndPoint;
    protected CTask FTask;
    //
    //-------------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------------
    //
    protected CTcpBase()
    {
      FNotifier = null;
      FTcpClient = null;
      FIpEndPoint = null;
      FTask = null;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Property
    //-------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }
    //
    //-------------------------------------------------------------------
    //  Segment - Callback
    //-------------------------------------------------------------------
    //

    //
    //-------------------------------------------------------------------
    //  Segment - Method - Public
    //-------------------------------------------------------------------
    //
    public abstract Boolean Open(IPAddress ipaddresstarget, UInt16 ipporttarget);
    public abstract Boolean Open(String sipendpointtarget);
    public abstract Boolean Close();


  }
}
