﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using CommandProtocol;
//
namespace CPLaserAreaScanner
{
  public class CPulseLed : CCommand
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const String HEADER = "PLD";
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CPulseLed()
      : base(HEADER)
    {
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public override string GetCommandText()
    {
     // return "@" + FHeader;
      return FHeader;
    }


  }
}