﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using UCNotifier;
using HWComPort;
//
namespace IFCommand
{
  public class CTransmitTextDelay : CCommand
  {
    public const String INIT_NAME = "TransmitTextDelay";

    private String FTxdText;
    private Int32 FDelayCharacter;
    private Int32 FDelayCR;
    private Int32 FDelayLF;
    private String FTxdBuffer = "";

    public CTransmitTextDelay(CCommandList parent,
                              String[] arguments)
      : base(INIT_NAME, parent, arguments)
    {
      SetOnCommandExecutionStart(ThreadOnExecutionStart);
      SetOnCommandExecutionBusy(ThreadOnExecutionBusy);
      SetOnCommandExecutionEnd(null);
      SetOnCommandExecutionAbort(ThreadOnExecutionAbort);
      SetOnCommandResponseLine(OnCommandResponseLine);
      //
      if (0 < FArguments.Length)
      {
        FTxdText = FArguments[0];
      }
      if (1 < FArguments.Length)
      {
        Int32.TryParse(FArguments[1], out FDelayCharacter);
      }
      if (2 < FArguments.Length)
      {
        Int32.TryParse(FArguments[1], out FDelayCR);
      }
      if (3 < FArguments.Length)
      {
        Int32.TryParse(FArguments[1], out FDelayLF);
      }
    }

    protected void ThreadOnExecutionStart()
    {      
      foreach (Char C in FTxdText)
      {
        switch (C)
        {
          case '\r':
            if (0 < FTxdBuffer.Length)
            {
              ComPort.WriteText(FTxdBuffer);
            }
            FTxdBuffer = "";
            ComPort.WriteCharacter('\r');
            if (0 < FDelayCR)
            {
              Thread.Sleep(FDelayCR);
            }
            break;
          case '\n':
            ComPort.WriteCharacter('\n');
            if (0 < FDelayLF)
            {
              Thread.Sleep(FDelayLF);
            }
            break;
          default:
            if (0 == FDelayCharacter)
            {
              FTxdBuffer += C;
            }
            else
            {
              ComPort.WriteCharacter(C);
              Thread.Sleep(FDelayCharacter);
            }
            break;
        }
      }
    }

    protected Boolean ThreadOnExecutionBusy()
    {
      return true;
    }

    protected void ThreadOnExecutionAbort()
    {
     
    }

    public void OnCommandResponseLine(Guid comportid, String data)
    {
      Notifier.Write(data);
      if (1 == data.Length)
      {
        if ('#' == data[0])
        {
          base.Abort();
        }
      }
    }

  }
}
