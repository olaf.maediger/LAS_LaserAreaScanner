﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Task;
using HWComPort;
//
namespace IFCommand
{
  public enum ECommandState
  {
    Error = -1,
    Init = 0,
    Begin = 1,
    Busy = 2,
    Response = 3,
    Abort = 4,
    End = 5
  };

  public delegate void DOnCommandExecutionStart();
  public delegate Boolean DOnCommandExecutionBusy();
  public delegate void DOnCommandExecutionEnd(CCommand command);
  public delegate void DOnCommandExecutionAbort();

  public delegate void DOnCommandResponseLine(Guid comportid, 
                                              String line);

 // NC  public delegate void DOnAddCommand(CCommand command);

  public abstract class CCommand
  {
    //
    //--------------------------------------
    //	Section - Constant
    //--------------------------------------
    //
    static public String HEADER_LIBRARY = "MHFDL";
    //
    //--------------------------------------
    //	Section - Field
    //--------------------------------------
    //
    private String FName;
    private ECommandState FState;
    private CCommandList FParent;
    protected CTask FTask;
    private DOnCommandResponseLine FOnCommandResponseLine;
    private DOnCommandExecutionStart FOnCommandExecutionStart;
    private DOnCommandExecutionBusy FOnCommandExecutionBusy;
    private DOnCommandExecutionEnd FOnCommandExecutionEnd;
    private DOnCommandExecutionAbort FOnCommandExecutionAbort;
    protected String[] FArguments;
    //
    //--------------------------------------
    //	Section - Constructor
    //--------------------------------------
    //
    public CCommand(String name,
                    CCommandList parent,
                    String[] arguments)
    {
      FName = name;
      FParent = parent;
      FArguments = arguments;
      ComPort.SetOnLineReceived(ComPortOnLineReceived);
      FTask = null;
      FState = ECommandState.Init;
    }
    //
    //--------------------------------------
    //	Section - Property
    //--------------------------------------
    //
    protected CNotifier Notifier
    {
      get { return FParent.Notifier; }
    }

    protected CComPort ComPort
    {
      get { return FParent.ComPort; }
    }

    public void SetOnCommandExecutionStart(DOnCommandExecutionStart value)
    {
      FOnCommandExecutionStart = value;
    }
    public void SetOnCommandExecutionBusy(DOnCommandExecutionBusy value)
    {
      FOnCommandExecutionBusy = value;
    }
    public void SetOnCommandExecutionEnd(DOnCommandExecutionEnd value)
    {
      FOnCommandExecutionEnd = value;
    }
    public void SetOnCommandExecutionAbort(DOnCommandExecutionAbort value)
    {
      FOnCommandExecutionAbort = value;
    }

    protected void SetOnCommandResponseLine(DOnCommandResponseLine value)
    {
      FOnCommandResponseLine = value;
    }

    private void SetCommandState(ECommandState value)
    {
      if (value != FState)
      {
        // debug:
        String Line = String.Format("CommandState[{0}] (<-[{1}])", value, FState);
        Notifier.Write(Line);
        FState = value;
      }
    }
    public ECommandState State
    {
      get { return FState; }
      set { SetCommandState(value); }
    }
    //
    //--------------------------------------
    //	Section - Callback
    //--------------------------------------
    //
    private void ComPortOnLineReceived(Guid comportid, String line)
    {
      if (FOnCommandResponseLine is DOnCommandResponseLine)
      {
        FOnCommandResponseLine(comportid, line);
      }
    }

    //protected virtual void OnSerialResponseReceived(String text)
    //{
    //  //String Line = String.Format("{0}: {1}", FName, text);
    //  //FNotifier.Write(HEADER_LIBRARY, Line);
    //  //// Analyse Response to refresh Gui
    //  //String[] Tokenlist = text.Split(' ');
    //  //if (1 < Tokenlist.Length)
    //  //{
    //  //  //Commandlist.Library.RefreshHelp();//Tokenlist);
    //  //}
    //}

    private void OnExecutionStart(RTaskData data)
    {
      State = ECommandState.Begin;
      if (FOnCommandExecutionStart is DOnCommandExecutionStart)
      {
        FOnCommandExecutionStart();
      }
    }

    private Boolean OnExecutionBusy(ref RTaskData data)
    {
      State = ECommandState.Busy;
      if (FOnCommandExecutionBusy is DOnCommandExecutionBusy)
      {
        FOnCommandExecutionBusy();
      }
      return true;
    }

    private void OnExecutionEnd(RTaskData data)
    {
      State = ECommandState.End;
      if (FOnCommandExecutionEnd is DOnCommandExecutionEnd)
      {
        FOnCommandExecutionEnd(this);
      }
    }

    private void OnExecutionAbort(RTaskData data)
    {
      State = ECommandState.Abort;
      if (FOnCommandExecutionAbort is DOnCommandExecutionAbort)
      {
        FOnCommandExecutionAbort();
      }
      FParent.Remove(this);
    }
    //
    //--------------------------------------
    //	Section - Management
    //--------------------------------------
    //
    public Boolean Start()//String name)
    {
      if (null == FTask)
      {
        FTask = new CTask(FName, OnExecutionStart, OnExecutionBusy, OnExecutionEnd, OnExecutionAbort);
        return FTask.Start();
      }
      return false;
    }

    public Boolean Abort()
    {
      if (FTask is CTask)
      {
        return FTask.Abort();
      }
      return false;
    }

  }
}
