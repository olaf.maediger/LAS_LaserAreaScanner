﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UCNotifier;
using HWComPort;

namespace IFCommand
{
  // NC 	public delegate void DOnCommandExecuteEnd(CCommand command);
  // NC public delegate void DOnSerialResponseReceived(Guid comportid,
  // NC                                          String response);

  public delegate void DOnReceived(String line);

  public delegate void DOnComPortDataTransmitted(String data);
  public delegate void DOnComPortDataReceived(String data);

  public class CCommandList : List<CCommand>
	{
    // NC private CIFCommandBase FLibrary;
		private CNotifier FNotifier;
    private CComPort FComPort;
   // NC  private Int32 FCommandIndex;
    // NC private CVariablelist FVariablelist;

    public CCommandList()//CIFCommandBase library)
		{
      // NC FLibrary = library;
      // NC FVariablelist = new CVariablelist();
		}

    //public CIFCommandBase Library
    //{
    //  get { return FLibrary; }
    //}

    //public CVariablelist Variablelist
    //{
    //  get { return FVariablelist; }
    //}

    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
    }
    public CNotifier Notifier
    {
      get { return FNotifier; }
      set { SetNotifier(value); }
    }

    public void SetComPort(CComPort comport)
    {
      FComPort = comport;
    }
    public CComPort ComPort
    {
      get { return FComPort; }
      set { SetComPort(value); }
    }

    public new Boolean Add(CCommand command)
    {
      // debug 
      FNotifier.Write("Add Command: " + command.ToString());
      base.Add(command);
      return (0 < this.Count);
    }

    public new Boolean Clear()
    {
      base.Clear();
      return true;
    }

    public CCommand GetCommandActual()
    {
      CCommand Result = null;
      if (0 < Count)
      {
        Result = base[0];
      }
      if (0 == Count)
      {
        AbortExecution();
      }
      return Result;
    }

    public Boolean StartExecution()
    { // debug 
      Notifier.Write("Commandlist.StartExecution");
      return ActivateCommand();
    }

    public Boolean AbortExecution()
    { // debug Notifier.Write("Commandlist.AbortExecution");
      CCommand Command = null;
			if (0 < Count)
			{
        Command = base[0];
        if (Command is CCommand)
				{
					Command.Abort();
				}
			}
      Clear();
			return true;
		}

		public void SelfOnCommandExecutionEnd(CCommand command)
		{ // prepare next command
      CCommand Command = null;
      if (0 < Count)
      {
        Command = base[0];
        base.Remove(Command);
      }
      ActivateCommand();
		}


    private Boolean ActivateCommand()
    {
      if (0 < Count)
      {
        CCommand Command = base[0];
        if (Command is CCommand)
        {
          String Line = String.Format("ActivateCommand[{0}]", Command.ToString());
          FNotifier.Write(Line);
          Command.SetOnCommandExecutionEnd(SelfOnCommandExecutionEnd);
          return Command.Start();
        }
      }
      return false;
    }


    //public void DebugVariablelist()
    //{
    //  FVariablelist.Debug();
    //}


    /* ?????public void SetOnSerialResponseReceived(DOnSerialResponseReceived value)
    {
      FLibrary.SetOnSerialResponseReceived(value);
    }*/

    /*ublic void SetOnRXDLineReceived(DOnRXDLineReceived onrxdlinereceived)
    {
      FLibrary.SetOnRXDLineReceived(onrxdlinereceived);
    }*/

    /* NC public Boolean LoadProgramFromXml(String programentry)
    {

    }*/

	}
}
