﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using UCNotifier;
//
namespace Xml
{
  public class CXmlWriter : CXmlBase
  {
		//
		//-------------------------------------
    //	Segment - Constructor
		//-------------------------------------
		//
		public CXmlWriter()
      : base()
		{
		}
    //
    //------------------------------------------
    //	Segment - Write - LowLevel
    //------------------------------------------
    //
    protected void WriteNodeAttribute(XmlNode node)
    {
      String Line = String.Format("<{0}", node.Name);
      foreach (XmlAttribute XMLAttribute in node.Attributes)
      {
        Line += String.Format(" {0}=\"{1}\"", XMLAttribute.Name, XMLAttribute.Value);
      }
      Line += ">";
      Write(Line);
    }

    public Boolean AddNode(Int32 countincrement,
                           XmlDocument document,
                           XmlNode nodeparent,
                           out XmlNode nodechild,
                           String nodename)
    {
      CountIncrement(countincrement);
      nodechild = document.CreateElement(nodename);
      if (nodeparent is XmlNode)
      {
        nodeparent.AppendChild(nodechild);
        WriteNodeAttribute(nodechild);
      }
      else
      { // create BaseNode
        document.AppendChild(nodechild);
        WriteNodeAttribute(nodechild);
      }
      CountDecrement(countincrement);
      return true;
    }

    public Boolean AddNodeAttribute(Int32 countincrement,
                                    XmlDocument document,
                                    XmlNode nodeparent,
                                    out XmlElement nodechild,
                                    String nodename,
                                    String attributeheader,
                                    String attributetext)
    {
      CountIncrement(countincrement);
      nodechild = document.CreateElement(nodename);
      nodechild.SetAttribute(attributeheader, attributetext);    
      if (nodeparent is XmlNode)
      {
        nodeparent.AppendChild(nodechild);
        WriteNodeAttribute(nodechild);
      }
      else
      { // create BaseNode
        document.AppendChild(nodechild);
        WriteNodeAttribute(nodechild);
      }
      CountDecrement(countincrement);
      return true;
    }

    public Boolean AddNode(Int32 countincrement,
                           XmlDocument document,
                           XmlNode nodeparent,
                           out XmlNode nodechild, 
                           String nodename, 
                           Object nodevalue)
    {
      CountIncrement(countincrement);
      nodechild = document.CreateElement(nodename);
      if (nodeparent is XmlNode)
      {
        nodeparent.AppendChild(nodechild);
        nodechild.InnerText = nodevalue.ToString();
        WriteIncrement();
        String Line = String.Format("<{0}>[{1}]", nodechild.Name, nodechild.InnerText);
        Write(Line);
        WriteDecrement();
      }
      else
      { // create BaseNode
        document.AppendChild(nodechild);
        nodechild.InnerText = nodevalue.ToString();
        WriteIncrement();
        String Line = String.Format("<{0}>[{1}]", nodechild.Name, nodechild.InnerText);
        Write(Line);
        WriteDecrement();
      }
      CountDecrement(countincrement);
      return true;
    }
    //
    //------------------------------------------
    //	Segment - Write - Scalar-Datatypes
    //------------------------------------------
    //
    public Boolean WriteBoolean(XmlDocument xmldocument,
                                XmlNode nodebase,
                                String header,
                                Boolean value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteString(XmlDocument xmldocument,
                               XmlNode nodebase,
                               String header,
                               String value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value;
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteInt16(XmlDocument xmldocument,
                              XmlNode nodebase,
                              String header,
                              Int16 value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteInt32(XmlDocument xmldocument,
                              XmlNode nodebase,
                              String header,
                              Int32 value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteDouble(XmlDocument xmldocument,
                               XmlNode nodebase,
                               String header,
                               Double value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteByte(XmlDocument xmldocument,
                             XmlNode nodebase,
                             String header,
                             Byte value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }

    public Boolean WriteGuid(XmlDocument xmldocument,
                             XmlNode nodebase,
                             String header,
                             Guid value)
    {
      XmlNode NodeChild = xmldocument.CreateElement(header);
      NodeChild.InnerText = value.ToString();
      nodebase.AppendChild(NodeChild);
      String Line = String.Format("<{0}>[{1}]", NodeChild.Name, NodeChild.InnerText);
      Write(Line);
      return true;
    }
    //
    //----------------------------------------------
    //	Segment - Write - Common - High-Level
    //----------------------------------------------
    //
    //public Boolean WriteBegin(out XmlDocument xmldocument, 
    //                          out XmlNode xmlnode,
    //                          Boolean enableprotocol)
    //{
    //  Boolean Result = true;
    //  EnableProtocol(enableprotocol);
    //  // Root
    //  xmldocument = new XmlDocument();
    //  xmlnode = xmldocument.CreateElement(CXmlHeader.MAIN);
    //  xmldocument.AppendChild(xmlnode);
    //  String Line = String.Format("<{0}>[{1}]", xmlnode.Name, xmlnode.InnerText);
    //  Write(Line);
    //  // Declaration
    //  XmlDeclaration XMLDeclaration = xmldocument.CreateXmlDeclaration(CXmlHeader.TITLEVERSION,
    //                                                                   CXmlHeader.TITLEENCODING,
    //                                                                   CXmlHeader.TITLESTANDALONE);
    //  xmldocument.InsertBefore(XMLDeclaration, xmlnode);
    //  // Comment
    //  XmlComment XMLComment = xmldocument.CreateComment(CXmlHeader.COMMENT);
    //  xmldocument.InsertBefore(XMLComment, xmlnode);
    //  //
    //  WriteIncrement();
    //  //	
    //  return Result;
    //}

    //public Boolean WriteHeader(XmlDocument xmldocument,
    //                           XmlNode xmlnode,
    //                           RXmlHeader data)
    //{
    //  Boolean Result = true;
    //  // Base
    //  XmlNode NodeBase = xmldocument.CreateElement(CXmlHeader.HEADER);
    //  xmlnode.AppendChild(NodeBase);
    //  String Line = String.Format("<{0}>[{1}]", NodeBase.Name, NodeBase.InnerText);
    //  Write(Line);
    //  WriteIncrement();
    //  // Project
    //  WriteString(xmldocument, NodeBase, CXmlHeader.PROJECT, data.Project);
    //  // Version
    //  WriteString(xmldocument, NodeBase, CXmlHeader.VERSION, data.Version);
    //  // Year
    //  WriteInt32(xmldocument, NodeBase, CXmlHeader.YEAR, data.Year);
    //  // Month
    //  WriteInt32(xmldocument, NodeBase, CXmlHeader.MONTH, data.Month);
    //  // Day
    //  WriteInt32(xmldocument, NodeBase, CXmlHeader.DAY, data.Day);
    //  // Hour
    //  WriteInt32(xmldocument, NodeBase, CXmlHeader.HOUR, data.Hour);
    //  // Minute
    //  WriteInt32(xmldocument, NodeBase, CXmlHeader.MINUTE, data.Minute);
    //  // Second
    //  WriteInt32(xmldocument, NodeBase, CXmlHeader.SECOND, data.Second);
    //  // Author
    //  WriteString(xmldocument, NodeBase, CXmlHeader.AUTHOR, data.Author);
    //  //
    //  WriteDecrement();
    //  return Result;
    //}

    //public Boolean WriteEnd(XmlDocument xmldocument)
    //{
    //  Boolean Result = true;
    //  WriteDecrement();
    //  return Result;
    //}
    //
    //
    //----------------------------------------------
    //	Segment - Write - Basic-Tools
    //----------------------------------------------
    //


  }
}
