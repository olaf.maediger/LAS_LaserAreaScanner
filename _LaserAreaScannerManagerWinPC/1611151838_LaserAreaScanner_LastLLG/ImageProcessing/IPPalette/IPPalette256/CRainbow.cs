﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPPaletteBase;
//
namespace IPPalette256
{
  public class CRainbow : CPalette256
  {
    //
    //------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------
    //
    protected static String NAME_PALETTE = CPalette256.PALETTEKINDS[(int)EPaletteKind.Rainbow];
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CRainbow()
      : base(NAME_PALETTE, EPaletteKind.Rainbow)
    {
      BuildPaletteColors();
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------
    //
    protected override Boolean BuildPaletteColors()
    {
      FColors[0xFF] = Color.FromArgb(0xFF, 137, 0, 0);
      FColors[0xFE] = Color.FromArgb(0xFF, 137, 2, 0);
      FColors[0xFD] = Color.FromArgb(0xFF, 137, 4, 0);
      FColors[0xFC] = Color.FromArgb(0xFF, 137, 6, 0);
      FColors[0xFB] = Color.FromArgb(0xFF, 143, 12, 0);
      FColors[0xFA] = Color.FromArgb(0xFF, 143, 14, 0);
      FColors[0xF9] = Color.FromArgb(0xFF, 154, 16, 2);
      FColors[0xF8] = Color.FromArgb(0xFF, 154, 18, 2);
      FColors[0xF7] = Color.FromArgb(0xFF, 160, 22, 3);
      FColors[0xF6] = Color.FromArgb(0xFF, 160, 24, 3);
      FColors[0xF5] = Color.FromArgb(0xFF, 165, 26, 4);
      FColors[0xF4] = Color.FromArgb(0xFF, 165, 28, 4);
      FColors[0xF3] = Color.FromArgb(0xFF, 171, 30, 5);
      FColors[0xF2] = Color.FromArgb(0xFF, 171, 32, 5);
      FColors[0xF1] = Color.FromArgb(0xFF, 176, 34, 5);
      FColors[0xF0] = Color.FromArgb(0xFF, 176, 36, 5);
      //
      FColors[0xEF] = Color.FromArgb(0xFF, 182, 38, 6);
      FColors[0xEE] = Color.FromArgb(0xFF, 182, 40, 6);
      FColors[0xED] = Color.FromArgb(0xFF, 187, 42, 7);
      FColors[0xEC] = Color.FromArgb(0xFF, 187, 44, 7);
      FColors[0xEB] = Color.FromArgb(0xFF, 193, 46, 8);
      FColors[0xEA] = Color.FromArgb(0xFF, 193, 48, 8);
      FColors[0xE9] = Color.FromArgb(0xFF, 198, 50, 9);
      FColors[0xE8] = Color.FromArgb(0xFF, 198, 52, 9);
      FColors[0xE7] = Color.FromArgb(0xFF, 204, 54, 10);
      FColors[0xE6] = Color.FromArgb(0xFF, 204, 56, 10);
      FColors[0xE5] = Color.FromArgb(0xFF, 209, 58, 10);
      FColors[0xE4] = Color.FromArgb(0xFF, 209, 60, 10);
      FColors[0xE3] = Color.FromArgb(0xFF, 215, 62, 11);
      FColors[0xE2] = Color.FromArgb(0xFF, 215, 64, 11);
      FColors[0xE1] = Color.FromArgb(0xFF, 220, 66, 12);
      FColors[0xE0] = Color.FromArgb(0xFF, 220, 68, 12);
      //
      FColors[0xDF] = Color.FromArgb(0xFF, 226, 70, 13);
      FColors[0xDE] = Color.FromArgb(0xFF, 226, 72, 13);
      FColors[0xDD] = Color.FromArgb(0xFF, 231, 74, 14);
      FColors[0xDC] = Color.FromArgb(0xFF, 231, 76, 14);
      FColors[0xDB] = Color.FromArgb(0xFF, 237, 78, 15);
      FColors[0xDA] = Color.FromArgb(0xFF, 237, 80, 15);
      FColors[0xD9] = Color.FromArgb(0xFF, 242, 82, 16);
      FColors[0xD8] = Color.FromArgb(0xFF, 242, 84, 16);
      FColors[0xD7] = Color.FromArgb(0xFF, 248, 86, 16);
      FColors[0xD6] = Color.FromArgb(0xFF, 248, 88, 16);
      FColors[0xD5] = Color.FromArgb(0xFF, 254, 90, 17);
      FColors[0xD4] = Color.FromArgb(0xFF, 254, 92, 17);
      FColors[0xD3] = Color.FromArgb(0xFF, 255, 94, 17);
      FColors[0xD2] = Color.FromArgb(0xFF, 255, 96, 17);
      FColors[0xD1] = Color.FromArgb(0xFF, 255, 98, 16);
      FColors[0xD0] = Color.FromArgb(0xFF, 255, 99, 16);
      //
      FColors[0xCF] = Color.FromArgb(0xFF, 255, 102, 16);
      FColors[0xCE] = Color.FromArgb(0xFF, 255, 104, 16);
      FColors[0xCD] = Color.FromArgb(0xFF, 255, 107, 15);
      FColors[0xCC] = Color.FromArgb(0xFF, 255, 109, 15);
      FColors[0xCB] = Color.FromArgb(0xFF, 255, 111, 14);
      FColors[0xCA] = Color.FromArgb(0xFF, 255, 113, 14);
      FColors[0xC9] = Color.FromArgb(0xFF, 255, 115, 13);
      FColors[0xC8] = Color.FromArgb(0xFF, 255, 118, 13);
      FColors[0xC7] = Color.FromArgb(0xFF, 255, 121, 13);
      FColors[0xC6] = Color.FromArgb(0xFF, 255, 123, 13);
      FColors[0xC5] = Color.FromArgb(0xFF, 255, 125, 12);
      FColors[0xC4] = Color.FromArgb(0xFF, 255, 127, 12);
      FColors[0xC3] = Color.FromArgb(0xFF, 255, 131, 11);
      FColors[0xC2] = Color.FromArgb(0xFF, 255, 133, 11);
      FColors[0xC1] = Color.FromArgb(0xFF, 255, 135, 10);
      FColors[0xC0] = Color.FromArgb(0xFF, 255, 137, 10);
      //          
      FColors[0xBF] = Color.FromArgb(0xFF, 255, 140, 10);
      FColors[0xBE] = Color.FromArgb(0xFF, 255, 142, 10);
      FColors[0xBD] = Color.FromArgb(0xFF, 255, 144, 9);
      FColors[0xBC] = Color.FromArgb(0xFF, 255, 146, 9);
      FColors[0xBB] = Color.FromArgb(0xFF, 255, 148, 8);
      FColors[0xBA] = Color.FromArgb(0xFF, 255, 151, 8);
      FColors[0xB9] = Color.FromArgb(0xFF, 255, 153, 7);
      FColors[0xB8] = Color.FromArgb(0xFF, 255, 156, 7);
      FColors[0xB7] = Color.FromArgb(0xFF, 255, 159, 7);
      FColors[0xB6] = Color.FromArgb(0xFF, 255, 162, 7);
      FColors[0xB5] = Color.FromArgb(0xFF, 255, 165, 6);
      FColors[0xB4] = Color.FromArgb(0xFF, 255, 168, 6);
      FColors[0xB3] = Color.FromArgb(0xFF, 255, 170, 5);
      FColors[0xB2] = Color.FromArgb(0xFF, 255, 172, 5);
      FColors[0xB1] = Color.FromArgb(0xFF, 255, 174, 4);
      FColors[0xB0] = Color.FromArgb(0xFF, 255, 176, 4);
      //          
      FColors[0xAF] = Color.FromArgb(0xFF, 255, 178, 4);
      FColors[0xAE] = Color.FromArgb(0xFF, 255, 179, 4);
      FColors[0xAD] = Color.FromArgb(0xFF, 255, 182, 3);
      FColors[0xAC] = Color.FromArgb(0xFF, 255, 184, 3);
      FColors[0xAB] = Color.FromArgb(0xFF, 255, 188, 2);
      FColors[0xAA] = Color.FromArgb(0xFF, 255, 188, 2);
      FColors[0xA9] = Color.FromArgb(0xFF, 255, 191, 1);
      FColors[0xA8] = Color.FromArgb(0xFF, 255, 193, 1);
      FColors[0xA7] = Color.FromArgb(0xFF, 255, 196, 1);
      FColors[0xA6] = Color.FromArgb(0xFF, 255, 198, 1);
      FColors[0xA5] = Color.FromArgb(0xFF, 255, 201, 0);
      FColors[0xA4] = Color.FromArgb(0xFF, 255, 203, 0);
      FColors[0xA3] = Color.FromArgb(0xFF, 252, 205, 0);
      FColors[0xA2] = Color.FromArgb(0xFF, 252, 207, 0);
      FColors[0xA1] = Color.FromArgb(0xFF, 243, 208, 0);
      FColors[0xA0] = Color.FromArgb(0xFF, 243, 209, 0);
      //          
      FColors[0x9F] = Color.FromArgb(0xFF, 235, 209, 0);
      FColors[0x9E] = Color.FromArgb(0xFF, 235, 210, 0);
      FColors[0x9D] = Color.FromArgb(0xFF, 227, 211, 0);
      FColors[0x9C] = Color.FromArgb(0xFF, 227, 211, 0);
      FColors[0x9B] = Color.FromArgb(0xFF, 219, 212, 0);
      FColors[0x9A] = Color.FromArgb(0xFF, 219, 213, 0);
      FColors[0x99] = Color.FromArgb(0xFF, 203, 215, 0);
      FColors[0x98] = Color.FromArgb(0xFF, 203, 216, 0);
      FColors[0x97] = Color.FromArgb(0xFF, 195, 217, 0);
      FColors[0x96] = Color.FromArgb(0xFF, 195, 217, 0);
      FColors[0x95] = Color.FromArgb(0xFF, 187, 218, 0);
      FColors[0x94] = Color.FromArgb(0xFF, 187, 219, 0);
      FColors[0x93] = Color.FromArgb(0xFF, 179, 220, 0);
      FColors[0x92] = Color.FromArgb(0xFF, 179, 221, 0);
      FColors[0x91] = Color.FromArgb(0xFF, 163, 222, 0);
      FColors[0x90] = Color.FromArgb(0xFF, 163, 223, 0);
      //                   
      FColors[0x8F] = Color.FromArgb(0xFF, 152, 224, 0);
      FColors[0x8E] = Color.FromArgb(0xFF, 147, 225, 0);
      FColors[0x8D] = Color.FromArgb(0xFF, 139, 226, 0);
      FColors[0x8C] = Color.FromArgb(0xFF, 135, 227, 0);
      FColors[0x8B] = Color.FromArgb(0xFF, 131, 228, 0);
      FColors[0x8A] = Color.FromArgb(0xFF, 128, 229, 0);
      FColors[0x89] = Color.FromArgb(0xFF, 123, 230, 0);
      FColors[0x88] = Color.FromArgb(0xFF, 118, 231, 0);
      FColors[0x87] = Color.FromArgb(0xFF, 115, 232, 0);
      FColors[0x86] = Color.FromArgb(0xFF, 112, 232, 0);
      FColors[0x85] = Color.FromArgb(0xFF, 107, 233, 0);
      FColors[0x84] = Color.FromArgb(0xFF, 103, 233, 0);
      FColors[0x83] = Color.FromArgb(0xFF, 99, 234, 0);
      FColors[0x82] = Color.FromArgb(0xFF, 96, 235, 0);
      FColors[0x81] = Color.FromArgb(0xFF, 91, 236, 0);
      FColors[0x80] = Color.FromArgb(0xFF, 98, 237, 0);
      //                    
      FColors[0x7F] = Color.FromArgb(0xFF, 83, 238, 0);
      FColors[0x7E] = Color.FromArgb(0xFF, 80, 239, 0);
      FColors[0x7D] = Color.FromArgb(0xFF, 75, 240, 0);
      FColors[0x7C] = Color.FromArgb(0xFF, 73, 240, 0);
      FColors[0x7B] = Color.FromArgb(0xFF, 72, 240, 8);
      FColors[0x7A] = Color.FromArgb(0xFF, 71, 240, 8);
      FColors[0x79] = Color.FromArgb(0xFF, 69, 241, 16);
      FColors[0x78] = Color.FromArgb(0xFF, 65, 241, 16);
      FColors[0x77] = Color.FromArgb(0xFF, 62, 242, 32);
      FColors[0x76] = Color.FromArgb(0xFF, 60, 242, 32);
      FColors[0x75] = Color.FromArgb(0xFF, 58, 243, 41);
      FColors[0x74] = Color.FromArgb(0xFF, 57, 243, 41);
      FColors[0x73] = Color.FromArgb(0xFF, 56, 244, 49);
      FColors[0x72] = Color.FromArgb(0xFF, 55, 244, 49);
      FColors[0x71] = Color.FromArgb(0xFF, 53, 245, 57);
      FColors[0x70] = Color.FromArgb(0xFF, 52, 245, 57);
      //                    
      FColors[0x6F] = Color.FromArgb(0xFF, 49, 245, 65);
      FColors[0x6E] = Color.FromArgb(0xFF, 48, 245, 65);
      FColors[0x6D] = Color.FromArgb(0xFF, 47, 246, 73);
      FColors[0x6C] = Color.FromArgb(0xFF, 45, 246, 73);
      FColors[0x6B] = Color.FromArgb(0xFF, 43, 247, 81);
      FColors[0x6A] = Color.FromArgb(0xFF, 41, 247, 81);
      FColors[0x69] = Color.FromArgb(0xFF, 39, 247, 90);
      FColors[0x68] = Color.FromArgb(0xFF, 38, 247, 90);
      FColors[0x67] = Color.FromArgb(0xFF, 37, 248, 98);
      FColors[0x66] = Color.FromArgb(0xFF, 35, 248, 98);
      FColors[0x65] = Color.FromArgb(0xFF, 33, 249, 106);
      FColors[0x64] = Color.FromArgb(0xFF, 31, 249, 106);
      FColors[0x63] = Color.FromArgb(0xFF, 28, 250, 114);
      FColors[0x62] = Color.FromArgb(0xFF, 26, 250, 114);
      FColors[0x61] = Color.FromArgb(0xFF, 24, 250, 122);
      FColors[0x60] = Color.FromArgb(0xFF, 23, 250, 122);
      //                    
      FColors[0x5F] = Color.FromArgb(0xFF, 21, 251, 130);
      FColors[0x5E] = Color.FromArgb(0xFF, 20, 251, 130);
      FColors[0x5D] = Color.FromArgb(0xFF, 19, 252, 138);
      FColors[0x5C] = Color.FromArgb(0xFF, 17, 252, 138);
      FColors[0x5B] = Color.FromArgb(0xFF, 16, 252, 147);
      FColors[0x5A] = Color.FromArgb(0xFF, 14, 252, 147);
      FColors[0x59] = Color.FromArgb(0xFF, 12, 253, 155);
      FColors[0x58] = Color.FromArgb(0xFF, 10, 253, 155);
      FColors[0x57] = Color.FromArgb(0xFF, 8, 254, 163);
      FColors[0x56] = Color.FromArgb(0xFF, 6, 254, 163);
      FColors[0x55] = Color.FromArgb(0xFF, 4, 255, 171);
      FColors[0x54] = Color.FromArgb(0xFF, 3, 255, 171);
      FColors[0x53] = Color.FromArgb(0xFF, 1, 255, 179);
      FColors[0x52] = Color.FromArgb(0xFF, 0, 255, 179);
      FColors[0x51] = Color.FromArgb(0xFF, 1, 249, 181);
      FColors[0x50] = Color.FromArgb(0xFF, 1, 246, 181);
      //                    
      FColors[0x4F] = Color.FromArgb(0xFF, 1, 234, 180);
      FColors[0x4E] = Color.FromArgb(0xFF, 1, 230, 180);
      FColors[0x4D] = Color.FromArgb(0xFF, 1, 222, 179);
      FColors[0x4C] = Color.FromArgb(0xFF, 1, 220, 179);
      FColors[0x4B] = Color.FromArgb(0xFF, 2, 210, 177);
      FColors[0x4A] = Color.FromArgb(0xFF, 2, 204, 177);
      FColors[0x49] = Color.FromArgb(0xFF, 2, 198, 176);
      FColors[0x48] = Color.FromArgb(0xFF, 2, 194, 176);
      FColors[0x47] = Color.FromArgb(0xFF, 2, 186, 175);
      FColors[0x46] = Color.FromArgb(0xFF, 2, 180, 175);
      FColors[0x45] = Color.FromArgb(0xFF, 3, 174, 174);
      FColors[0x44] = Color.FromArgb(0xFF, 3, 170, 174);
      FColors[0x43] = Color.FromArgb(0xFF, 3, 162, 173);
      FColors[0x42] = Color.FromArgb(0xFF, 3, 156, 173);
      FColors[0x41] = Color.FromArgb(0xFF, 3, 150, 172);
      FColors[0x40] = Color.FromArgb(0xFF, 3, 145, 172);
      //                    
      FColors[0x3F] = Color.FromArgb(0xFF, 4, 139, 171);
      FColors[0x3E] = Color.FromArgb(0xFF, 4, 130, 171);
      FColors[0x3D] = Color.FromArgb(0xFF, 4, 127, 170);
      FColors[0x3C] = Color.FromArgb(0xFF, 4, 120, 170);
      FColors[0x3B] = Color.FromArgb(0xFF, 4, 115, 169);
      FColors[0x3A] = Color.FromArgb(0xFF, 4, 110, 169);
      FColors[0x39] = Color.FromArgb(0xFF, 5, 103, 168);
      FColors[0x38] = Color.FromArgb(0xFF, 5, 99, 168);
      FColors[0x37] = Color.FromArgb(0xFF, 5, 91, 167);
      FColors[0x36] = Color.FromArgb(0xFF, 5, 85, 167);
      FColors[0x35] = Color.FromArgb(0xFF, 5, 79, 166);
      FColors[0x34] = Color.FromArgb(0xFF, 5, 73, 166);
      FColors[0x33] = Color.FromArgb(0xFF, 6, 67, 165);
      FColors[0x32] = Color.FromArgb(0xFF, 6, 60, 165);
      FColors[0x31] = Color.FromArgb(0xFF, 6, 55, 163);
      FColors[0x30] = Color.FromArgb(0xFF, 6, 50, 163);
      //                    
      FColors[0x2F] = Color.FromArgb(0xFF, 6, 47, 162);
      FColors[0x2E] = Color.FromArgb(0xFF, 6, 43, 162);
      FColors[0x2D] = Color.FromArgb(0xFF, 7, 39, 161);
      FColors[0x2C] = Color.FromArgb(0xFF, 7, 31, 161);
      FColors[0x2B] = Color.FromArgb(0xFF, 7, 25, 160);
      FColors[0x2A] = Color.FromArgb(0xFF, 7, 19, 160);
      FColors[0x29] = Color.FromArgb(0xFF, 7, 12, 159);
      FColors[0x28] = Color.FromArgb(0xFF, 7, 7, 159);
      FColors[0x27] = Color.FromArgb(0xFF, 8, 6, 157);
      FColors[0x26] = Color.FromArgb(0xFF, 8, 5, 155);
      FColors[0x25] = Color.FromArgb(0xFF, 8, 4, 153);
      FColors[0x24] = Color.FromArgb(0xFF, 8, 3, 151);
      FColors[0x23] = Color.FromArgb(0xFF, 8, 2, 149);
      FColors[0x22] = Color.FromArgb(0xFF, 8, 1, 158);
      FColors[0x21] = Color.FromArgb(0xFF, 8, 0, 150);
      FColors[0x20] = Color.FromArgb(0xFF, 8, 0, 146);
      //                    
      FColors[0x1F] = Color.FromArgb(0xFF, 8, 0, 145);
      FColors[0x1E] = Color.FromArgb(0xFF, 8, 0, 143);
      FColors[0x1D] = Color.FromArgb(0xFF, 8, 0, 141);
      FColors[0x1C] = Color.FromArgb(0xFF, 8, 0, 139);
      FColors[0x1B] = Color.FromArgb(0xFF, 8, 0, 136);
      FColors[0x1A] = Color.FromArgb(0xFF, 8, 0, 134);
      FColors[0x19] = Color.FromArgb(0xFF, 8, 0, 132);
      FColors[0x18] = Color.FromArgb(0xFF, 8, 0, 130);
      FColors[0x17] = Color.FromArgb(0xFF, 8, 0, 128);
      FColors[0x16] = Color.FromArgb(0xFF, 8, 0, 126);
      FColors[0x15] = Color.FromArgb(0xFF, 9, 0, 124);
      FColors[0x14] = Color.FromArgb(0xFF, 9, 0, 122);
      FColors[0x13] = Color.FromArgb(0xFF, 9, 0, 120);
      FColors[0x12] = Color.FromArgb(0xFF, 9, 0, 118);
      FColors[0x11] = Color.FromArgb(0xFF, 9, 0, 116);
      FColors[0x10] = Color.FromArgb(0xFF, 9, 0, 114);
      //                    
      FColors[0x0F] = Color.FromArgb(0xFF, 10, 0, 113);
      FColors[0x0E] = Color.FromArgb(0xFF, 10, 0, 112);
      FColors[0x0D] = Color.FromArgb(0xFF, 10, 0, 111);
      FColors[0x0C] = Color.FromArgb(0xFF, 10, 0, 110);
      FColors[0x0B] = Color.FromArgb(0xFF, 10, 0, 108);
      FColors[0x0A] = Color.FromArgb(0xFF, 10, 0, 106);
      FColors[0x09] = Color.FromArgb(0xFF, 10, 0, 104);
      FColors[0x08] = Color.FromArgb(0xFF, 10, 0, 102);
      FColors[0x07] = Color.FromArgb(0xFF, 10, 0, 100);
      FColors[0x06] = Color.FromArgb(0xFF, 10, 0, 97);
      FColors[0x05] = Color.FromArgb(0xFF, 10, 0, 96);
      FColors[0x04] = Color.FromArgb(0xFF, 10, 0, 95);
      FColors[0x03] = Color.FromArgb(0xFF, 10, 0, 94);
      FColors[0x02] = Color.FromArgb(0xFF, 10, 0, 93);
      FColors[0x01] = Color.FromArgb(0xFF, 10, 0, 92);
      FColors[0x00] = Color.FromArgb(0xFF, 10, 0, 91);
      //
      return true;
    }

  }
}
