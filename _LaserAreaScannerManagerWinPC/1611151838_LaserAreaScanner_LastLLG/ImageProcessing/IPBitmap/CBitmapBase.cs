﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPMemory;
using IPMatrix;
using IPPaletteBase;
//
namespace IPBitmap
{
  public abstract class CBitmapBase : CMemoryBase
  {
    //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //
    public static Int32 INIT_BITMAPWIDTH = 1;
    public static Int32 INIT_BITMAPHEIGHT = 1;
    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    protected Bitmap FBitmap; // Extern, SpecialType // Bitmap -> Display (24bitRgb or 32bitArgb)
    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmapBase(Int32 width, Int32 height, PixelFormat pixelformat)
    {
      Int32 W = Math.Max(INIT_BITMAPWIDTH, width);
			Int32 H = Math.Max(INIT_BITMAPHEIGHT, height);
      FBitmap = new System.Drawing.Bitmap(W, H, pixelformat);
    }
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //   
    public Bitmap Bitmap
    {
      get { return FBitmap; }
    }

    public PixelFormat PixelFormat
    {
      get { return FBitmap.PixelFormat; }
    }

    public Int32 Width
    {
      get { return FBitmap.Width; }
    }
    public Int32 Height
    {
      get { return FBitmap.Height; }
    }
    //
    //--------------------------------------------------------
    //	Segment - Public Management
    //--------------------------------------------------------
    //   
    public Boolean ConvertFromMatrix(CMatrixBase matrix)
    {
      if (matrix is CMatrix08Bit)
      {
        //?????????????????????????????????????????????????????????? BuildBitmap08BitFromMatrix08Bit(
        return false;
      }
      if (matrix is CMatrix10Bit)
      {
        return false;
      }
      if (matrix is CMatrix12Bit)
      {
        return false;
      }
      return false;
    }

    public Boolean ConvertToMatrix(ref CMatrixBase matrix)
    {
      if (matrix is CMatrix08Bit)
      {
        // -> 
        return false;
      }
      if (matrix is CMatrix10Bit)
      {
        // -> 
        return false;
      }
      if (matrix is CMatrix12Bit)
      {
        // -> 
        return false;
      }
      return false;
    }

  }
}
