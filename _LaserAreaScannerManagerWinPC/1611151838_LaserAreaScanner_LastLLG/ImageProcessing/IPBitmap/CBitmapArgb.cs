﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPMatrix;
using IPBitmap;
//
namespace IPBitmap
{
  public class CBitmapArgb : CBitmapBase
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    // no Palette!
    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmapArgb(Int32 width, Int32 height)
      : base(width, height, PixelFormat.Format32bppArgb)
    {
    }

    public CBitmapArgb(Bitmap bitmap)
      : base(bitmap.Width, bitmap.Height, PixelFormat.Format32bppArgb)
    {
      CBitmapConversion.ConvertBitmapToBitmap32bppArgb(bitmap, out FBitmap);
    }

    public CBitmapArgb(CMatrix08Bit matrix)
      : base(matrix.ColCount, matrix.RowCount, PixelFormat.Format32bppArgb)
    {
      CBitmapConversion.ConvertMatrix08BitToBitmap32bppArgb(matrix, out FBitmap);
    }

    public CBitmapArgb(CMatrix10Bit matrix)
      : base(matrix.ColCount, matrix.RowCount, PixelFormat.Format32bppArgb)
    {
      CBitmapConversion.ConvertMatrix10BitToBitmap32bppArgb(matrix, out FBitmap);
    }

    public CBitmapArgb(CMatrix12Bit matrix)
      : base(matrix.ColCount, matrix.RowCount, PixelFormat.Format32bppArgb)
    {
      CBitmapConversion.ConvertMatrix12BitToBitmap32bppArgb(matrix, out FBitmap);
    }
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Helper
    //--------------------------------------------------------
    //
 

  }
}
