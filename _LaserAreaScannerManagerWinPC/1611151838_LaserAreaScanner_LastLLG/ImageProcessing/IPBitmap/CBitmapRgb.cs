﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
//
using IPMatrix;
using IPBitmap;
//
namespace IPBitmap
{
  public class CBitmapRgb : CBitmapBase
  { //
    //--------------------------------------------------------
    //	Segment - Constant
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //	Segment - Field
    //--------------------------------------------------------
    //
    // no Palette!
    //
    //--------------------------------------------------------
    //	Segment - Constructor
    //--------------------------------------------------------
    //
    public CBitmapRgb(Int32 width, Int32 height)
      : base(width, height, PixelFormat.Format24bppRgb)
    {
    }

    public CBitmapRgb(Bitmap bitmap)
      : base(bitmap.Width, bitmap.Height, PixelFormat.Format32bppArgb)
    {
      CBitmapConversion.ConvertBitmapToBitmap24bppRgb(bitmap, out FBitmap);
    }

    public CBitmapRgb(CMatrix08Bit matrix)
      : base(matrix.ColCount, matrix.RowCount, PixelFormat.Format32bppArgb)
    {
      CBitmapConversion.ConvertMatrix08BitToBitmap24bppRgb(matrix, out FBitmap);
    }

    public CBitmapRgb(CMatrix10Bit matrix)
      : base(matrix.ColCount, matrix.RowCount, PixelFormat.Format32bppArgb)
    {
      CBitmapConversion.ConvertMatrix10BitToBitmap24bppRgb(matrix, out FBitmap);
    }

    public CBitmapRgb(CMatrix12Bit matrix)
      : base(matrix.ColCount, matrix.RowCount, PixelFormat.Format32bppArgb)
    {
      CBitmapConversion.ConvertMatrix12BitToBitmap24bppRgb(matrix, out FBitmap);
    }

    //public CBitmapRgb(Int32 colcount, Int32 rowcount, UInt16[] vector)
    //  : base(colcount, rowcount, PixelFormat.Format32bppArgb)
    //{
    //  CBitmapConversion.ConvertVectorUInt16ToBitmap24bppRgb(colcount, rowcount, vector, out FBitmap);
    //}
    //
    //--------------------------------------------------------
    //	Segment - Property
    //--------------------------------------------------------
    //
    //public void SetRgb(Byte[] rgb)
    //{
    //  if (rgb is Byte[])
    //  {
    //    if (3 == rgb.Length)
    //    {
    //      FBitmap.
    //    }
    //  }
    //}
    //
    //--------------------------------------------------------
    //	Segment - Helper
    //--------------------------------------------------------
    //
 
    //
    //--------------------------------------------------------
    //	Segment - Public
    //--------------------------------------------------------
    //

  }
}
