﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPPaletteBase;
using IPPalette256;
//
namespace IPMatrix
{
  public class CMatrixConversionPalette
  { //
    //-----------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Callback
    //-----------------------------------------------------------
    //

    //
    //###################################################################################
    //  Segment - Conversion Matrix08Bit + Palette256 -> Matrix...
    //###################################################################################
    //
    //-----------------------------------------------------------
    //  Section - Conversion Matrix08Bit + Palette256 -> Matrix....
    //-----------------------------------------------------------
    //
    public static Boolean ConvertMatrix08BitPalette256ToMatrix10Bit(CMatrix08Bit matrixsource,
                                                                    CPalette256 palette256,
                                                                    out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix08BitPalette256ToMatrix12Bit(CMatrix08Bit matrixsource,
                                                                    CPalette256 palette256,
                                                                    out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue12Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix08BitPalette256ToMatrixDouble(CMatrix08Bit matrixsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrixDouble matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixDouble(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValueDouble(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------
    //  Section - Public Method - Conversion Matrix10Bit -> ...
    //-----------------------------------------------------------
    //
    public static Boolean ConvertMatrix10BitPalette256ToMatrix08Bit(CMatrix10Bit matrixsource,
                                                                    CPalette256 palette256,
                                                                    out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue10Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix10BitPalette256ToMatrix12Bit(CMatrix10Bit matrixsource,
                                                                    CPalette256 palette256,
                                                                    out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue10Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix10BitPalette256ToMatrixInt32(CMatrix10Bit matrixsource,
                                                                    CPalette256 palette256,
                                                                    out CMatrixInt32 matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixInt32(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue10Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix10BitPalette256ToMatrixDouble(CMatrix10Bit matrixsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrixDouble matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixDouble(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue10Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------
    //  Section - Public Method - Conversion Matrix12Bit -> ...
    //-----------------------------------------------------------
    //
    public static Boolean ConvertMatrix12BitPalette256ToMatrix08Bit(CMatrix12Bit matrixsource,
                                                                    CPalette256 palette256,
                                                                    out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue12Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix12BitPalette256ToMatrix10Bit(CMatrix12Bit matrixsource,
                                                                    CPalette256 palette256,
                                                                    out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue12Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix12BitPalette256ToMatrixInt32(CMatrix12Bit matrixsource,
                                                                    CPalette256 palette256,
                                                                    out CMatrixInt32 matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixInt32(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue12Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix12BitPalette256ToMatrixDouble(CMatrix12Bit matrixsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrixDouble matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixDouble(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue12Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------
    //  Section - Public Method - Conversion MatrixDouble -> ...
    //-----------------------------------------------------------
    //
    public static Boolean ConvertMatrixDoublePalette256ToMatrix08Bit(CMatrixDouble matrixsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValueDouble(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixDoublePalette256ToMatrix10Bit(CMatrixDouble matrixsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValueDouble(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixDoublePalette256ToMatrix12Bit(CMatrixDouble matrixsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValueDouble(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixDoublePalette256ToMatrixInt32(CMatrixDouble matrixsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrixInt32 matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixInt32(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = matrixsource.GetValue08Bit(CI, RI);
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValueDouble(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
