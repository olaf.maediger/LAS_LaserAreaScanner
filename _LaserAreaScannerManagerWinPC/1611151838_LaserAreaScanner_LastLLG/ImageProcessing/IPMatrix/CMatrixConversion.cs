﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMatrix;
//
namespace IPMatrix
{
  public class CMatrixConversion
  { //
    //-----------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Callback
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Public Method - Conversion Matrix08Bit -> ...
    //-----------------------------------------------------------
    //
    public static Boolean Convert08BitTo10Bit(CMatrix08Bit matrixsource,
                                              out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValue10Bit(CI, RI, matrixsource.GetValue10Bit(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Convert08BitTo12Bit(CMatrix08Bit matrixsource,
                                              out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValue12Bit(CI, RI, matrixsource.GetValue12Bit(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    
    public static Boolean Convert08BitToDouble(CMatrix08Bit matrixsource,
                                               out CMatrixDouble matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixDouble(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValueDouble(CI, RI, matrixsource.GetValueDouble(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------
    //  Section - Public Method - Conversion Matrix10Bit -> ...
    //-----------------------------------------------------------
    //
    public static Boolean Convert10BitTo08Bit(CMatrix10Bit matrixsource,
                                              out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValue10Bit(CI, RI, matrixsource.GetValue10Bit(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Convert10BitTo12Bit(CMatrix10Bit matrixsource,
                                              out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValue10Bit(CI, RI, matrixsource.GetValue10Bit(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Convert10BitToInt32(CMatrix10Bit matrixsource,
                                              out CMatrixInt32 matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixInt32(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValue10Bit(CI, RI, matrixsource.GetValue10Bit(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Convert10BitToDouble(CMatrix10Bit matrixsource,
                                               out CMatrixDouble matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixDouble(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValue10Bit(CI, RI, matrixsource.GetValue10Bit(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------
    //  Section - Public Method - Conversion Matrix12Bit -> ...
    //-----------------------------------------------------------
    //
    public static Boolean Convert12BitTo08Bit(CMatrix12Bit matrixsource,
                                              out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValue12Bit(CI, RI, matrixsource.GetValue12Bit(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Convert12BitTo10Bit(CMatrix12Bit matrixsource,
                                              out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValue12Bit(CI, RI, matrixsource.GetValue12Bit(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Convert12BitToInt32(CMatrix12Bit matrixsource,
                                              out CMatrixInt32 matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixInt32(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValue12Bit(CI, RI, matrixsource.GetValue12Bit(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Convert12BitToDouble(CMatrix12Bit matrixsource,
                                               out CMatrixDouble matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixDouble(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValue12Bit(CI, RI, matrixsource.GetValue12Bit(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------
    //  Section - Public Method - Conversion MatrixDouble -> ...
    //-----------------------------------------------------------
    //
    public static Boolean ConvertDoubleTo08Bit(CMatrixDouble matrixsource,
                                               out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValueDouble(CI, RI, matrixsource.GetValueDouble(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertDoubleTo10Bit(CMatrixDouble matrixsource,
                                               out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValueDouble(CI, RI, matrixsource.GetValueDouble(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertDoubleTo12Bit(CMatrixDouble matrixsource,
                                               out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValueDouble(CI, RI, matrixsource.GetValueDouble(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertDoubleToInt32(CMatrixDouble matrixsource,
                                               out CMatrixInt32 matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = matrixsource.ColCount;
        Int32 RC = matrixsource.RowCount;
        matrixtarget = new CMatrixInt32(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            matrixtarget.SetValueDouble(CI, RI, matrixsource.GetValueDouble(CI, RI));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
