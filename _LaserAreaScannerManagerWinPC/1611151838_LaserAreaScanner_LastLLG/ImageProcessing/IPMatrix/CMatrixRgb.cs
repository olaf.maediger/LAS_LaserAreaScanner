﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMemory;
//
namespace IPMatrix
{
  class CMatrixRgb : CMatrixBase
  { //  
    //----------------------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------------------
    //  

    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private Byte[,] FMatrixR;
    private Byte[,] FMatrixG;
    private Byte[,] FMatrixB;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrixRgb(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrixR = new Byte[FColCount, FRowCount];
      FMatrixG = new Byte[FColCount, FRowCount];
      FMatrixB = new Byte[FColCount, FRowCount];
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //
    //-------------------------------------------------------------------------------
    //
    public override Byte GetValue08Bit(Int32 indexcol, Int32 indexrow)
    { // 08Bit <- 08BitRGB
      float Value = FACTOR_RED * (float)(FMatrixR[indexcol, indexrow] << 0) + 
                    FACTOR_GREEN * (float)(FMatrixG[indexcol, indexrow] << 0) +
                    FACTOR_BLUE * (float)(FMatrixB[indexcol, indexrow] << 0);
      return (Byte)Value;
    }
    public override void SetValue08Bit(Int32 indexcol, Int32 indexrow, Byte value)
    { // 08BitRGB <- 08Bit  
      Byte Value = (Byte)(value >> 0);
      FMatrixR[indexcol, indexrow] = Value;
      FMatrixG[indexcol, indexrow] = Value;
      FMatrixB[indexcol, indexrow] = Value;
    }

    public override UInt16 GetValue10Bit(Int32 indexcol, Int32 indexrow)
    { // 10Bit <- 08BitRGB
      float Value = FACTOR_RED * (float)(FMatrixR[indexcol, indexrow] << 2) +
                    FACTOR_GREEN * (float)(FMatrixG[indexcol, indexrow] << 2) +
                    FACTOR_BLUE * (float)(FMatrixB[indexcol, indexrow] << 2);
      return (UInt16)Value;
    }
    public override void SetValue10Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 08BitRGB <- 10Bit
      Byte Value = (Byte)(value >> 2);
      FMatrixR[indexcol, indexrow] = Value;
      FMatrixG[indexcol, indexrow] = Value;
      FMatrixB[indexcol, indexrow] = Value;
    }

    public override UInt16 GetValue12Bit(Int32 indexcol, Int32 indexrow)
    { // 12Bit <- 08BitRGB
      float Value = FACTOR_RED * (float)(FMatrixR[indexcol, indexrow] << 4) +
                    FACTOR_GREEN * (float)(FMatrixG[indexcol, indexrow] << 4) +
                    FACTOR_BLUE * (float)(FMatrixB[indexcol, indexrow] << 4);
      return (UInt16)Value;
    }
    public override void SetValue12Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 08BitRGB <- 12Bit
      Byte Value = (Byte)(value >> 4);
      FMatrixR[indexcol, indexrow] = Value; 
      FMatrixG[indexcol, indexrow] = Value;
      FMatrixB[indexcol, indexrow] = Value;
    }

    public override UInt16 GetValue16Bit(Int32 indexcol, Int32 indexrow)
    { // 16Bit <- 08BitRGB
      float Value = FACTOR_RED * (float)(FMatrixR[indexcol, indexrow] << 8) +
                    FACTOR_GREEN * (float)(FMatrixG[indexcol, indexrow] << 8) +
                    FACTOR_BLUE * (float)(FMatrixB[indexcol, indexrow] << 8);
      return (UInt16)Value;
    }
    public override void SetValue16Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 08BitRGB <- 16Bit
      Byte Value = (Byte)(value >> 8);
      FMatrixR[indexcol, indexrow] = Value;
      FMatrixG[indexcol, indexrow] = Value;
      FMatrixB[indexcol, indexrow] = Value;
    }
    //
    //-------------------------------------------------------------------------------
    //
    public override Int32 GetValueInt32(Int32 indexcol, Int32 indexrow)
    { // Int32 <- 08BitRGB
      float Value = FACTOR_RED * (float)(FMatrixR[indexcol, indexrow] << 0) +
                    FACTOR_GREEN * (float)(FMatrixG[indexcol, indexrow] << 0) +
                    FACTOR_BLUE * (float)(FMatrixB[indexcol, indexrow] << 0);
      return (Int32)Value;
    }
    public override void SetValueInt32(Int32 indexcol, Int32 indexrow, Int32 value)
    { // 08BitRGB <- Int32
      Byte Value = (Byte)(0x000000FF & value);
      FMatrixR[indexcol, indexrow] = Value; 
      FMatrixG[indexcol, indexrow] = Value; 
      FMatrixB[indexcol, indexrow] = Value; 
    }

    public override Double GetValueDouble(Int32 indexcol, Int32 indexrow)
    { // Double <- 08BitRGB
      Double Value = FACTOR_RED * (float)(FMatrixR[indexcol, indexrow] << 0) +
                     FACTOR_GREEN * (float)(FMatrixG[indexcol, indexrow] << 0) +
                     FACTOR_BLUE * (float)(FMatrixB[indexcol, indexrow] << 0);
      return (Double)Value;
    }
    public override void SetValueDouble(Int32 indexcol, Int32 indexrow, Double value)
    { // 08BitRGB <- Double
      Byte Value = (Byte)(0.5 + value);
      FMatrixR[indexcol, indexrow] = Value; 
      FMatrixG[indexcol, indexrow] = Value; 
      FMatrixB[indexcol, indexrow] = Value; 
    }
    //
    //-------------------------------------------------------------------------------
    //
    public Byte[] GetValueRgb(Int32 indexcol, Int32 indexrow)
    {
      Byte[] Value = new Byte[3];
      Value[0] = FMatrixR[indexcol, indexrow];
      Value[0] = FMatrixG[indexcol, indexrow];
      Value[0] = FMatrixB[indexcol, indexrow];
      return Value;
    }
    public void SetValueRgb(Int32 indexcol, Int32 indexrow, Byte[] rgb)
    {
      if (rgb is Byte[])
      {
        if (3 == rgb.Length)
        {
          FMatrixR[indexcol, indexrow] = rgb[0];
          FMatrixG[indexcol, indexrow] = rgb[1];
          FMatrixB[indexcol, indexrow] = rgb[2];
        }
      }
    }
    public void SetValueRgb(Int32 indexcol, Int32 indexrow, Byte r, Byte g, Byte b)
    {
      FMatrixR[indexcol, indexrow] = r;
      FMatrixG[indexcol, indexrow] = g;
      FMatrixB[indexcol, indexrow] = b;
    }

  }
}
