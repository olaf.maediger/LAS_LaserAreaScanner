﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
//
namespace IPMatrix
{
  //
  //--------------------------------------------------
  //	Types
  //--------------------------------------------------
  //
  public partial class CMatrixFileText
  {
    //
    //--------------------------------------------------
    //	Section - File-Management
    //--------------------------------------------------
    //
    public Boolean OpenWrite(String fileentry)
    {
      if (!(FStreamWriter is StreamWriter))
      {
        FStreamWriter = new StreamWriter(fileentry, false, Encoding.ASCII);
        return true;
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Section - LowLevel - Write
    //--------------------------------------------------
    //
    protected Boolean WriteTokenDelimiter()
    {
      if (FStreamWriter is StreamWriter)
      {
        FStreamWriter.Write(FData.TokenDelimiters[0]);
        return true;
      }
      return false;
    }

    protected Boolean WriteLineDelimiter()
    {
      if (FStreamWriter is StreamWriter)
      {
        FStreamWriter.Write(FData.LineDelimiters[0]);
        return true;
      }
      return false;
    }

    protected Boolean Write(String value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatString, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(Char value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatCharacter, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(Byte value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatByte, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(Int16 value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatInt16, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(UInt16 value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatUInt16, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(Int32 value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatInt32, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(UInt32 value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatUInt32, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(Single value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatSingle, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(Double value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatDouble, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(Decimal value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatDecimal, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(DateTime value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = value.ToString();
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(TimeSpan value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = value.ToString();
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Segment - HighLevel - Write - Class-specific
    //--------------------------------------------------
    //
    public Boolean WriteMatrix08Bit(CMatrix08Bit matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("08Bit");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix.GetValue08Bit(CI, RI));
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrix08BitWrite is DOnMatrix08BitWrite)
        {
          FData.OnMatrix08BitWrite(matrix);
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrix10Bit(CMatrix10Bit matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("10Bit");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix.GetValue10Bit(CI, RI));
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrix10BitWrite is DOnMatrix10BitWrite)
        {
          FData.OnMatrix10BitWrite(matrix);
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrix12Bit(CMatrix12Bit matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("12Bit");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix.GetValue12Bit(CI, RI));
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrix12BitWrite is DOnMatrix12BitWrite)
        {
          FData.OnMatrix12BitWrite(matrix);
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrix16Bit(CMatrix16Bit matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("16Bit");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix.GetValue12Bit(CI, RI));
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrix16BitWrite is DOnMatrix16BitWrite)
        {
          FData.OnMatrix16BitWrite(matrix);
        }
        return true;
      }
      return false;
    }
    //
    //------------------------------------------------------------
    //
    public Boolean WriteMatrixByte(Byte[,] matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("Byte");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix[CI, RI]);
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrixByteWrite is DOnMatrixByteWrite)
        {
          FData.OnMatrixByteWrite(matrix);
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrixUInt10(UInt16[,] matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("UInt10");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatUInt10, matrix[CI, RI]);
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrixUInt10Write is DOnMatrixUInt10Write)
        {
          FData.OnMatrixUInt10Write(matrix);
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrixUInt12(UInt16[,] matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("UInt12");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatUInt12, matrix[CI, RI]);
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrixUInt12Write is DOnMatrixUInt12Write)
        {
          FData.OnMatrixUInt12Write(matrix);
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrixUInt16(UInt16[,] matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("UInt16");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatUInt16, matrix[CI, RI]);
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrixUInt16Write is DOnMatrixUInt16Write)
        {
          FData.OnMatrixUInt16Write(matrix);
        }
        return true;
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Segment - HighLevel - Write - Class-independent
    //--------------------------------------------------
    //
    public Boolean Write(CMatrix08Bit matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("08Bit");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix.GetValue08Bit(CI, RI));
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrix08BitWrite is DOnMatrix08BitWrite)
        {
          FData.OnMatrix08BitWrite(matrix);
        }
        return true;
      }
      return false;
    }

    public Boolean Write(CMatrix10Bit matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("10Bit");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix.GetValue10Bit(CI, RI));
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrix10BitWrite is DOnMatrix10BitWrite)
        {
          FData.OnMatrix10BitWrite(matrix);
        }
        return true;
      }
      return false;
    }

    public Boolean Write(CMatrix12Bit matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("12Bit");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix.GetValue12Bit(CI, RI));
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrix12BitWrite is DOnMatrix12BitWrite)
        {
          FData.OnMatrix12BitWrite(matrix);
        }
        return true;
      }
      return false;
    }

    public Boolean Write(CMatrix16Bit matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("12Bit");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix.GetValue12Bit(CI, RI));
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrix16BitWrite is DOnMatrix16BitWrite)
        {
          FData.OnMatrix16BitWrite(matrix);
        }
        return true;
      }
      return false;
    }
    //
    //------------------------------------------------------------
    //
    public Boolean Write(Byte[,] matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("Byte");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix[CI, RI]);
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        if (FData.OnMatrixByteWrite is DOnMatrixByteWrite)
        {
          FData.OnMatrixByteWrite(matrix);
        }
        return true;
      }
      return false;
    }

    public Boolean Write(Int32[,] matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("Int32");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatInt32, matrix[CI, RI]);
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        //if (FData.OnMatrixInt32Write is DOnMatrixInt32Write)
        //{
        //  FData.OnMatrixInt32Write(matrix);
        //}
        return true;
      }
      return false;
    }

    public Boolean Write(Double[,] matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        String Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("Double");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatDouble, matrix[CI, RI]);
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        //if (FData.OnMatrixDoubleWrite is DOnMatrixDoubleWrite)
        //{
        //  FData.OnMatrixDoubleWrite(matrix);
        //}
        return true;
      }
      return false;
    }
  }
}
