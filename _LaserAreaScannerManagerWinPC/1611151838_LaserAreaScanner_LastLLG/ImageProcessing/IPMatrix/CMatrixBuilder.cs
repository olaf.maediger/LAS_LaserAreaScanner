﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMatrix;
//
namespace IPMatrix
{
  public class CMatrixBuilder
  {
    //
    //-------------------------------------------------------------------------------
    //
    public static Boolean CreateMatrixConstant(Int32 countx, Int32 county,
                                               Int32 zconstant,
                                               out CMatrix08Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix08Bit(countx, county);
        for (Int32 YI = 0; YI < county; YI++)
        {
          for (Int32 XI = 0; XI < countx; XI++)
          {
            matrix.SetValue08Bit(XI, YI, (Byte)(0x000000FF & zconstant));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CreateMatrixConstant(Int32 countx, Int32 county,
                                               Int32 zconstant,
                                               out CMatrix10Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix10Bit(countx, county);
        for (Int32 YI = 0; YI < county; YI++)
        {
          for (Int32 XI = 0; XI < countx; XI++)
          {
            matrix.SetValue10Bit(XI, YI, (UInt16)(0x000003FF & zconstant));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CreateMatrixConstant(Int32 countx, Int32 county,
                                               Int32 zconstant,
                                               out CMatrix12Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix12Bit(countx, county);
        for (Int32 YI = 0; YI < county; YI++)
        {
          for (Int32 XI = 0; XI < countx; XI++)
          {
            matrix.SetValue12Bit(XI, YI, (UInt16)(0x00000FFF & zconstant));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-------------------------------------------------------------------------------
    //
    public static Boolean CreateMatrixNoise(Int32 countx, Int32 county,
                                            Int32 zminimum, Int32 zmaximum,
                                            out CMatrix08Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix08Bit(countx, county);
        Random R = new Random((int)DateTime.Now.Ticks);
        for (Int32 YI = 0; YI < county; YI++)
        {
          for (Int32 XI = 0; XI < countx; XI++)
          {
            matrix.SetValue08Bit(XI, YI, (Byte)(0x000000FF & R.Next(zminimum, zmaximum)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CreateMatrixNoise(Int32 countx, Int32 county,
                                            Int32 zminimum, Int32 zmaximum,
                                            out CMatrix10Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix10Bit(countx, county);
        Random R = new Random((int)DateTime.Now.Ticks);
        for (Int32 YI = 0; YI < county; YI++)
        {
          for (Int32 XI = 0; XI < countx; XI++)
          {
            matrix.SetValue10Bit(XI, YI, (UInt16)(0x000003FF & R.Next(zminimum, zmaximum)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CreateMatrixNoise(Int32 countx, Int32 county,
                                            Int32 zminimum, Int32 zmaximum,
                                            out CMatrix12Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix12Bit(countx, county);
        Random R = new Random((int)DateTime.Now.Ticks);
        for (Int32 YI = 0; YI < county; YI++)
        {
          for (Int32 XI = 0; XI < countx; XI++)
          {
            matrix.SetValue12Bit(XI, YI, (UInt16)(0x00000FFF & R.Next(zminimum, zmaximum)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-------------------------------------------------------------------------------
    //
    public static Boolean CreateMatrixLinear(Int32 countx, Int32 county,
                                             Int32 zxminimum, Int32 zxmaximum, 
                                             Int32 zyminimum, Int32 zymaximum, 
                                             out CMatrix08Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix08Bit(countx, county);
        Double FX = (Double)(zxmaximum - zxminimum) / (Double)countx;
        Double FY = (Double)(zymaximum - zyminimum) / (Double)county;
        for (Int32 YI = 0; YI < county; YI++)
        {
          Double ZY = YI * FY / 2.0;
          for (Int32 XI = 0; XI < countx; XI++)
          {
            Double ZX = XI * FX / 2.0;
            matrix.SetValue08Bit(XI, YI, (Byte)(0x000000FF & (Int32)(0.5 + ZX + ZY)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CreateMatrixLinear(Int32 countx, Int32 county,
                                             Int32 zxminimum, Int32 zxmaximum,
                                             Int32 zyminimum, Int32 zymaximum,
                                             out CMatrix10Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix10Bit(countx, county);
        Double FX = (Double)(zxmaximum - zxminimum) / (Double)countx;
        Double FY = (Double)(zymaximum - zyminimum) / (Double)county;
        for (Int32 YI = 0; YI < county; YI++)
        {
          Double ZY = YI * FY / 2.0;
          for (Int32 XI = 0; XI < countx; XI++)
          {
            Double ZX = XI * FX / 2.0;
            matrix.SetValue10Bit(XI, YI, (UInt16)(0x000003FF & (Int32)(0.5 + ZX + ZY)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CreateMatrixLinear(Int32 countx, Int32 county,
                                             Int32 zxminimum, Int32 zxmaximum,
                                             Int32 zyminimum, Int32 zymaximum,
                                             out CMatrix12Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix12Bit(countx, county);
        Double FX = (Double)(zxmaximum - zxminimum) / (Double)countx;
        Double FY = (Double)(zymaximum - zyminimum) / (Double)county;
        for (Int32 YI = 0; YI < county; YI++)
        {
          Double ZY = YI * FY / 2.0;
          for (Int32 XI = 0; XI < countx; XI++)
          {
            Double ZX = XI * FX / 2.0;
            matrix.SetValue12Bit(XI, YI, (UInt16)(0x00000FFF & (Int32)(0.5 + ZX + ZY)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-------------------------------------------------------------------------------
    //
    public static Boolean CreateMatrixCircular(Int32 countx, Int32 county,
                                               Int32 zminimum, Int32 zmaximum,
                                               out CMatrix08Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix08Bit(countx, county);
        Double XC = countx / 2.0;
        Double YC = county / 2.0;
        Double RH2 = XC * XC + YC * YC;
        for (Int32 YI = 0; YI < county; YI++)
        {
          for (Int32 XI = 0; XI < countx; XI++)
          {
            Double DX = XI - XC;
            Double DY = YI - YC;
            Double R2 = DX * DX + DY * DY;
            Double Z = zmaximum - (zminimum + R2 / RH2 * (zmaximum - zminimum));
            matrix.SetValue08Bit(XI, YI, (Byte)(0x000000FF & (Int32)(0.5 + Z)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CreateMatrixCircular(Int32 countx, Int32 county,
                                               Int32 zminimum, Int32 zmaximum,
                                               out CMatrix10Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix10Bit(countx, county);
        Double XC = countx / 2.0;
        Double YC = county / 2.0;
        Double RH2 = XC * XC + YC * YC;
        for (Int32 YI = 0; YI < county; YI++)
        {
          for (Int32 XI = 0; XI < countx; XI++)
          {
            Double DX = XI - XC;
            Double DY = YI - YC;
            Double R2 = DX * DX + DY * DY;
            Double Z = zmaximum - (zminimum + R2 / RH2 * (zmaximum - zminimum));
            matrix.SetValue10Bit(XI, YI, (UInt16)(0x000003FF & (Int32)(0.5 + Z)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean CreateMatrixCircular(Int32 countx, Int32 county,
                                               Int32 zminimum, Int32 zmaximum,
                                               out CMatrix12Bit matrix)
    {
      matrix = null;
      try
      {
        matrix = new CMatrix12Bit(countx, county);
        Double XC = countx / 2.0;
        Double YC = county / 2.0;
        Double RH2 = XC * XC + YC * YC;
        for (Int32 YI = 0; YI < county; YI++)
        {
          for (Int32 XI = 0; XI < countx; XI++)
          {
            Double DX = XI - XC;
            Double DY = YI - YC;
            Double R2 = DX * DX + DY * DY;
            Double Z = zmaximum - (zminimum + R2 / RH2 * (zmaximum - zminimum));
            matrix.SetValue12Bit(XI, YI, (UInt16)(0x00000FFF & (Int32)(0.5 + Z)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-------------------------------------------------------------------------------
    //

  }
}
