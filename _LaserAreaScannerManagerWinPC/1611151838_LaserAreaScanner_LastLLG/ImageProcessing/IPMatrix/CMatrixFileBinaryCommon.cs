﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
//
namespace IPMatrix
{
  //
  //--------------------------------------------------
  //	Types
  //--------------------------------------------------
  //
  public struct RBinaryFileData
  {
    public String Entry;
    public String Drive;
    public String Path;
    public String Name;
    public String Extension;
    //
    public DOnMatrix08BitRead OnMatrix08BitRead;
    public DOnMatrix10BitRead OnMatrix10BitRead;
    public DOnMatrix12BitRead OnMatrix12BitRead;
    public DOnMatrix16BitRead OnMatrix16BitRead;
    public DOnMatrixByteRead OnMatrixByteRead;
    public DOnMatrixUInt10Read OnMatrixUInt10Read;
    public DOnMatrixUInt12Read OnMatrixUInt12Read;
    public DOnMatrixUInt16Read OnMatrixUInt16Read;
    //
    public DOnMatrix08BitWrite OnMatrix08BitWrite;
    public DOnMatrix10BitWrite OnMatrix10BitWrite;
    public DOnMatrix12BitWrite OnMatrix12BitWrite;
    public DOnMatrix16BitWrite OnMatrix16BitWrite;
    public DOnMatrixByteWrite OnMatrixByteWrite;
    public DOnMatrixUInt10Write OnMatrixUInt10Write;
    public DOnMatrixUInt12Write OnMatrixUInt12Write;
    public DOnMatrixUInt16Write OnMatrixUInt16Write;
    //
    public RBinaryFileData(Int32 init)
    {
      Entry = CMatrixFileBinary.INIT_ENTRY;
      Drive = CMatrixFileBinary.INIT_DRIVE;
      Path = CMatrixFileBinary.INIT_PATH;
      Name = CMatrixFileBinary.INIT_HEADER;
      Extension = CMatrixFileBinary.INIT_EXTENSION;
      //
      OnMatrix08BitRead = null;
      OnMatrix10BitRead = null;
      OnMatrix12BitRead = null;
      OnMatrix16BitRead = null;
      OnMatrixByteRead = null;
      OnMatrixUInt10Read = null;
      OnMatrixUInt12Read = null;
      OnMatrixUInt16Read = null;
      //
      OnMatrix08BitWrite = null;
      OnMatrix10BitWrite = null;
      OnMatrix12BitWrite = null;
      OnMatrix16BitWrite = null;
      OnMatrixByteWrite = null;
      OnMatrixUInt10Write = null;
      OnMatrixUInt12Write = null;
      OnMatrixUInt16Write = null;
    }
  };
  //
  public partial class CMatrixFileBinary
  {
    //	
    //--------------------------------------------------
    //	Section - Constant
    //--------------------------------------------------
    //
    public static String HEADER_LIBRARY = "TextFile";
    public static String SECTION_LIBRARY = HEADER_LIBRARY;
    //
    public const String INIT_DRIVESEPARATOR = ":";
    public const String INIT_HEADERSEPARATOR = ".";
    public const String INIT_DRIVE = "C";
    public const String INIT_PATH = INIT_HEADERSEPARATOR;
    public const String INIT_HEADER = "filename";
    public const String INIT_EXTENSION = "txt";
    public const String INIT_ENTRY = INIT_HEADER + INIT_HEADERSEPARATOR + INIT_EXTENSION;
    //
    public const String INIT_STRING = "";
    public const Char INIT_CHAR = (Char)0x00;
    public const Int16 INIT_INT16 = 0;
    public const Int32 INIT_INT32 = 0;
    public const Byte INIT_BYTE = 0x00;
    public const UInt16 INIT_UINT10 = 0;
    public const UInt16 INIT_UINT12 = 0;
    public const UInt16 INIT_UINT16 = 0;
    public const UInt32 INIT_UINT32 = 0;
    public const Single INIT_SINGLE = 0;
    public const Double INIT_DOUBLE = 0;
    public const Decimal INIT_DECIMAL = 0;
    public DateTime INIT_DATETIME = DateTime.MinValue;
    public TimeSpan INIT_TIMESPAN = TimeSpan.MinValue;
    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //
    private RBinaryFileData FData;
    private BinaryWriter FBinaryWriter;
    private BinaryReader FBinaryReader;
    //
    //--------------------------------------------------
    //	Section - Constructor
    //--------------------------------------------------
    //
    public CMatrixFileBinary()
    {
      FData = new RBinaryFileData(0);
    }
    //
    //--------------------------------------------------
    //	Section - Get/SetData
    //--------------------------------------------------
    //
    public Boolean GetData(out RBinaryFileData data)
    {
      data = FData;
      return true;
    }

    public Boolean SetData(RBinaryFileData data)
    {
      FData = data;
      return true;
    }
    //
    //--------------------------------------------------
    //	Section - File-Management
    //--------------------------------------------------
    //
    public Boolean Close()
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Close();
        FBinaryWriter = null;
      }
      if (FBinaryReader is BinaryReader)
      {
        FBinaryReader.Close();
        FBinaryReader = null;
      }
      return true;
    }
  }
}
