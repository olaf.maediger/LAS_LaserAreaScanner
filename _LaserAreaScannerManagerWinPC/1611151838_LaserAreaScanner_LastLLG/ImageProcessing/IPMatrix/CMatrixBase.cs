﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMemory;
//
namespace IPMatrix
{
  //
  // Abstract Basic Class for all Matrices of ImageProcessing
  //
  public abstract class CMatrixBase : CMemoryBase
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------------------
    //  
    public const Int32 INDEX_COL = 0;
    public const Int32 INDEX_ROW = 1;
    //  
    public const float FACTOR_RED = 0.299f;
    public const float FACTOR_GREEN = 0.587f;
    public const float FACTOR_BLUE = 0.114f;
    //  
    public const float DIVISOR_RED = 3.345f;
    public const float DIVISOR_GREEN = 1.704f;
    public const float DIVISOR_BLUE = 8.772f;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    protected String FName;
    protected Int32 FColCount;
    protected Int32 FRowCount;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrixBase(Int32 colcount, Int32 rowcount)
    {
      FName = "Matrix";
      FColCount = Math.Max(1, colcount);
      FRowCount = Math.Max(1, rowcount);
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    public Int32 ColCount
    {
      get { return FColCount; }
    }

    public Int32 RowCount
    {
      get { return FRowCount; }
    }

    public String Name
    {
      get { return FName; }
      set { FName = value; }
    }
    //
    public abstract Byte GetValue08Bit(Int32 indexcol, Int32 indexrow);
    public abstract void SetValue08Bit(Int32 indexcol, Int32 indexrow, Byte value);
    public abstract UInt16 GetValue10Bit(Int32 indexcol, Int32 indexrow);
    public abstract void SetValue10Bit(Int32 indexcol, Int32 indexrow, UInt16 value);
    public abstract UInt16 GetValue12Bit(Int32 indexcol, Int32 indexrow);
    public abstract void SetValue12Bit(Int32 indexcol, Int32 indexrow, UInt16 value);
    public abstract UInt16 GetValue16Bit(Int32 indexcol, Int32 indexrow);
    public abstract void SetValue16Bit(Int32 indexcol, Int32 indexrow, UInt16 value);
    //
    public abstract Int32 GetValueInt32(Int32 indexcol, Int32 indexrow);
    public abstract void SetValueInt32(Int32 indexcol, Int32 indexrow, Int32 value);
    public abstract Double GetValueDouble(Int32 indexcol, Int32 indexrow);
    public abstract void SetValueDouble(Int32 indexcol, Int32 indexrow, Double value);
    //  
    //----------------------------------------------------------------------------
    //  Segment - Static Conversion
    //----------------------------------------------------------------------------
    //  

  }
}
