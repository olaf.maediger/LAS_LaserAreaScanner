﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMemory;
//
namespace IPMatrix
{
  public class CMatrixDouble : CMatrixBase
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------------------
    //  
    //public const UInt32 MASK_RANGE = 0x00000FFF;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private Double[,] FMatrix;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrixDouble(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrix = new Double[FColCount, FRowCount];
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    public override Byte GetValue08Bit(Int32 indexcol, Int32 indexrow)
    {
      return (Byte)(0x000000FF & (Byte)FMatrix[indexcol, indexrow]);
    }
    public override void SetValue08Bit(Int32 indexcol, Int32 indexrow, Byte value)
    {
      FMatrix[indexcol, indexrow] = (Double)value;
    }

    public override UInt16 GetValue10Bit(Int32 indexcol, Int32 indexrow)
    {
      return (UInt16)(0x000003FF & (UInt16)FMatrix[indexcol, indexrow]);
    }
    public override void SetValue10Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    {
      FMatrix[indexcol, indexrow] = (Double)value;
    }

    public override UInt16 GetValue12Bit(Int32 indexcol, Int32 indexrow)
    {
      return (UInt16)(0x00000FFF & (UInt16)FMatrix[indexcol, indexrow]);
    }
    public override void SetValue12Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    {
      FMatrix[indexcol, indexrow] = (Double)value;
    }

    public override UInt16 GetValue16Bit(Int32 indexcol, Int32 indexrow)
    {
      return (UInt16)(0x0000FFFF & (UInt16)FMatrix[indexcol, indexrow]);
    }
    public override void SetValue16Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    {
      FMatrix[indexcol, indexrow] = (Double)value;
    }
    //
    //-------------------------------------------------------------------------------
    //
    public override Int32 GetValueInt32(Int32 indexcol, Int32 indexrow)
    {
      return (Int32)(0xFFFF & (Int32)FMatrix[indexcol, indexrow]);
    }
    public override void SetValueInt32(Int32 indexcol, Int32 indexrow, Int32 value)
    {
      FMatrix[indexcol, indexrow] = (Double)value;
    }

    public override Double GetValueDouble(Int32 indexcol, Int32 indexrow)
    {
      return (Double)FMatrix[indexcol, indexrow];
    }
    public override void SetValueDouble(Int32 indexcol, Int32 indexrow, Double value)
    {
      FMatrix[indexcol, indexrow] = (Double)value;
    }
    //
    //-------------------------------------------------------------------------------
    //
    //public Boolean Add(CMatrixDouble matrix01, CMatrixDouble matrix02, out CMatrixDouble matrixresult)
    //{
    //  matrixresult = null;
    //  try
    //  {
    //    Int32 CC = Math.Min(matrix01.ColCount, matrix02.ColCount);
    //    Int32 RC = Math.Min(matrix01.RowCount, matrix02.RowCount);
    //    matrixresult = new CMatrixDouble(CC, RC);
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //      }
    //    }
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}


  }
}

