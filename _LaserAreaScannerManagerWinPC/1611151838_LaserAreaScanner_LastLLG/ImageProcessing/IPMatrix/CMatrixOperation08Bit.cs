﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMemory;
using IPMatrix;
//
namespace IPMatrix
{
  public partial class CMatrixOperation
  {
    public static Boolean Add(CMatrix08Bit operanda,
                              CMatrix08Bit operandb,
                              out CMatrix08Bit result)
    {
      result = null;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        Int32 CCB = operanda.ColCount;
        Int32 RCB = operanda.RowCount;
        result = new CMatrix08Bit(CCA, RCA);
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            result.SetValue08Bit(CIA, RIA,
                                 (Byte)(operanda.GetValue08Bit(CIA, RIA) + operandb.GetValue08Bit(CIA, RIA)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Sub(CMatrix08Bit operanda,
                              CMatrix08Bit operandb,
                              out CMatrix08Bit result)
    {
      result = null;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        result = new CMatrix08Bit(CCA, RCA);
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            result.SetValue08Bit(CIA, RIA,
                                 (Byte)(operanda.GetValue08Bit(CIA, RIA) - operandb.GetValue08Bit(CIA, RIA)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Mul(CMatrix08Bit operanda,
                              CMatrix08Bit operandb,
                              out CMatrix08Bit result)
    {
      result = null;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        result = new CMatrix08Bit(CCA, RCA);
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            result.SetValue08Bit(CIA, RIA,
                                 (Byte)(operanda.GetValue08Bit(CIA, RIA) * operandb.GetValue08Bit(CIA, RIA)));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Mul(CMatrix08Bit operanda,
                              Byte operandb,
                              out CMatrix08Bit result)
    {
      result = null;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        result = new CMatrix08Bit(CCA, RCA);
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            result.SetValue08Bit(CIA, RIA,
                                 (Byte)(operanda.GetValue08Bit(CIA, RIA) * operandb));
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean FindExtrema(CMatrix08Bit operanda,
                                      out Byte minimum,
                                      out Byte maximum)
    {
      minimum = 0xFF;
      maximum = 0x00;
      try
      {
        Int32 CCA = operanda.ColCount;
        Int32 RCA = operanda.RowCount;
        for (Int32 RIA = 0; RIA < RCA; RIA++)
        {
          for (Int32 CIA = 0; CIA < CCA; CIA++)
          {
            Byte Value = operanda.GetValue08Bit(CIA, RIA);
            if (Value < minimum)
            {
              minimum = Value;
            }
            if (maximum < Value)
            {
              maximum = Value;
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean FindExtrema(CMatrix08Bit operanda,
                                      out Byte minimum, out Int32 indexcolminimum, out Int32 indexrowminimum,
                                      out Byte maximum, out Int32 indexcolmaximum, out Int32 indexrowmaximum)
    {
      minimum = 0xFF;
      indexcolminimum = 0;
      indexrowminimum = 0;
      maximum = 0x00;
      indexcolmaximum = 0;
      indexrowmaximum = 0;
      try
      {
        Int32 CC = operanda.ColCount;
        Int32 RC = operanda.RowCount;
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte Value = operanda.GetValue08Bit(CI, RI);
            if (Value < minimum)
            {
              minimum = Value;
              indexcolminimum = CI;
              indexrowminimum = RI;
            }
            if (maximum < Value)
            {
              maximum = Value;
              indexcolmaximum = CI;
              indexrowmaximum = RI;
            }
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean Norm(CMatrix08Bit operand, 
                               Int32 zminimum, Int32 zmaximum,
                               out CMatrix08Bit result)
    {      
      result = null;
      try
      {
        Byte Minimum, Maximum;
        if (FindExtrema(operand, out Minimum, out Maximum))
        {
          Int32 CC = operand.ColCount;
          Int32 RC = operand.RowCount;
          result = new CMatrix08Bit(CC, RC);
          Double S = (zmaximum - zminimum) / (Maximum - Minimum);
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Byte Value = operand.GetValue08Bit(CI, RI);
              result.SetValue08Bit(CI, RI, (Byte)(S * (Double)(Value - Minimum)));
            }
          }
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }




  }
}
