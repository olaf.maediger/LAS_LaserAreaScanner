﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
//
using UCNotifier;
//
namespace NetworkDevice
{
  public class CDeviceParameter
  { //
    //--------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------
    //
    public const String INIT_DEVICETYPE = "<devicetype>";
    public const String INIT_DEVICENAME = "<devicename>";
    //
    //--------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------
    //
    private String FType;
    private String FName;
    private Guid FID;
    private IPAddress FIPAddress;
    //
    //--------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------
    //    
    public CDeviceParameter()
    {
      FName = INIT_DEVICENAME;
      FType = INIT_DEVICETYPE;
      FID = Guid.NewGuid();
    }

    public CDeviceParameter(String devicename,
                            String devicetype,
                            Guid deviceid,
                            IPAddress deviceipaddress)
    {
      FName = devicename;
      FType = devicetype;
      FID = deviceid;
      FIPAddress = deviceipaddress;
    }

    public CDeviceParameter(CDeviceParameter deviceparameter)
    {
      FName = deviceparameter.Name;
      FType = deviceparameter.Type;
      FID = deviceparameter.ID;
      FIPAddress = deviceparameter.IPA;
    }
    //
    //--------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------
    //    
    private String GetName()
    {
      return FName;
    }
    private void SetName(String value)
    {
      FName = value;
    }
    public String Name
    {
      get { return GetName(); }
      set { SetName(value); }
    }

    private String GetTType()
    {
      return FType;
    }
    private void SetType(String value)
    {
      FType = value;
    }
    public String Type
    {
      get { return GetTType(); }
      set { SetType(value); }
    }

    private Guid GetID()
    {
      return FID;
    }
    private void SetID(Guid value)
    {
      FID = value;
    }
    public Guid ID
    {
      get { return GetID(); }
      set { SetID(value); }
    }

    private IPAddress GetIPAddress()
    {
      return FIPAddress;
    }
    private void SetIPAddress(IPAddress value)
    {
      FIPAddress = value;
    }
    public IPAddress IPA
    {
      get { return GetIPAddress(); }
      set { SetIPAddress(value); }
    }
    //
    //--------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------
    //    
    public void Info(CNotifier notifier)
    {
      notifier.Write(String.Format("DeviceParameter[{0}]:", Type));
      notifier.Write(String.Format("  Name: {0}", Name));
      notifier.Write(String.Format("  ID: {0}", ID));
      notifier.Write(String.Format("  IPAddress: {0}", IPA));
    }
  }

}
