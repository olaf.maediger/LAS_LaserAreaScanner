﻿using System;

namespace LaserScanner
{
  public class CLaserStep
  {
    private UInt32 FPositionX, FPositionY;
    private UInt32 FPulsePeriodms, FPulseCount;
    private UInt32 FDelayMotionms;

    public CLaserStep(UInt32 positionx, UInt32 positiony, 
                      UInt32 pulseperiodms, UInt32 pulsecount,
                      UInt32 delaymotionms)
    {
      FPositionX = (UInt16)(Math.Min(4095, Math.Max(1, (UInt32)positionx)));
      FPositionY = (UInt16)(Math.Min(4095, Math.Max(1, (UInt32)positiony)));
      FPulsePeriodms = pulseperiodms;
      FPulseCount = pulsecount;
      FDelayMotionms = delaymotionms;
    }

    public UInt32 PositionX
    {
      get { return FPositionX; }
      set { FPositionX = value; }
    }
    public UInt32 PositionY
    {
      get { return FPositionY; }
      set { FPositionY = value; }
    }
    public UInt32 DelayMotionms
    {
      get { return FDelayMotionms; }
      set { FDelayMotionms = value; }
    }
    public UInt32 PulsePeriodms
    {
      get { return FPulsePeriodms; }
      set { FPulsePeriodms = value; }
    }
    public UInt32 PulseCount
    {
      get { return FPulseCount; }
      set { FPulseCount = value; }
    }


  }
}
