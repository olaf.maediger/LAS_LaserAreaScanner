﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserStepLine
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnSetDelayMotionms = new System.Windows.Forms.Button();
      this.btnGetDelayMotionms = new System.Windows.Forms.Button();
      this.nudDelayMotionms = new System.Windows.Forms.NumericUpDown();
      this.label5 = new System.Windows.Forms.Label();
      this.btnSetPositionY = new System.Windows.Forms.Button();
      this.btnSetPositionX = new System.Windows.Forms.Button();
      this.btnSetRangeY = new System.Windows.Forms.Button();
      this.btnSetRangeX = new System.Windows.Forms.Button();
      this.btnAbortLaserRow = new System.Windows.Forms.Button();
      this.btnGetRangeY = new System.Windows.Forms.Button();
      this.btnGetRangeX = new System.Windows.Forms.Button();
      this.btnGetPositionY = new System.Windows.Forms.Button();
      this.btnGetPositionX = new System.Windows.Forms.Button();
      this.btnPulseLaserRow = new System.Windows.Forms.Button();
      this.label8 = new System.Windows.Forms.Label();
      this.nudPositionYActual = new System.Windows.Forms.NumericUpDown();
      this.nudPositionYDelta = new System.Windows.Forms.NumericUpDown();
      this.label6 = new System.Windows.Forms.Label();
      this.nudPositionXActual = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.nudPositionXDelta = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.nudPositionYMaximum = new System.Windows.Forms.NumericUpDown();
      this.nudPositionYMinimum = new System.Windows.Forms.NumericUpDown();
      this.nudPositionXMaximum = new System.Windows.Forms.NumericUpDown();
      this.nudPositionXMinimum = new System.Windows.Forms.NumericUpDown();
      this.btnAbortLaserColumn = new System.Windows.Forms.Button();
      this.btnPulseLaserColumn = new System.Windows.Forms.Button();
      this.btnSetPulseWidthus = new System.Windows.Forms.Button();
      this.btnGetPulseWidthus = new System.Windows.Forms.Button();
      this.nudPulseWidthus = new System.Windows.Forms.NumericUpDown();
      this.lblHeader = new System.Windows.Forms.Label();
      this.btnGetPulsePeriodms = new System.Windows.Forms.Button();
      this.btnGetPulseCount = new System.Windows.Forms.Button();
      this.btnSetPulsePeriodms = new System.Windows.Forms.Button();
      this.nudPulseCount = new System.Windows.Forms.NumericUpDown();
      this.nudPulsePeriodms = new System.Windows.Forms.NumericUpDown();
      this.btnSetPulseCount = new System.Windows.Forms.Button();
      this.cbxSkipZeroPulses = new System.Windows.Forms.CheckBox();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayMotionms)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYActual)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYDelta)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXActual)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXDelta)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseWidthus)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriodms)).BeginInit();
      this.SuspendLayout();
      // 
      // btnSetDelayMotionms
      // 
      this.btnSetDelayMotionms.Location = new System.Drawing.Point(42, 254);
      this.btnSetDelayMotionms.Name = "btnSetDelayMotionms";
      this.btnSetDelayMotionms.Size = new System.Drawing.Size(115, 24);
      this.btnSetDelayMotionms.TabIndex = 170;
      this.btnSetDelayMotionms.Text = "SetDelayMotion [ms]";
      this.btnSetDelayMotionms.UseVisualStyleBackColor = true;
      this.btnSetDelayMotionms.Click += new System.EventHandler(this.btnSetDelayMotionms_Click);
      // 
      // btnGetDelayMotionms
      // 
      this.btnGetDelayMotionms.Location = new System.Drawing.Point(5, 254);
      this.btnGetDelayMotionms.Name = "btnGetDelayMotionms";
      this.btnGetDelayMotionms.Size = new System.Drawing.Size(34, 24);
      this.btnGetDelayMotionms.TabIndex = 169;
      this.btnGetDelayMotionms.Text = "Get";
      this.btnGetDelayMotionms.UseVisualStyleBackColor = true;
      this.btnGetDelayMotionms.Click += new System.EventHandler(this.btnGetDelayMotionms_Click);
      // 
      // nudDelayMotionms
      // 
      this.nudDelayMotionms.Location = new System.Drawing.Point(163, 257);
      this.nudDelayMotionms.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
      this.nudDelayMotionms.Name = "nudDelayMotionms";
      this.nudDelayMotionms.Size = new System.Drawing.Size(55, 20);
      this.nudDelayMotionms.TabIndex = 168;
      this.nudDelayMotionms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudDelayMotionms.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      // 
      // label5
      // 
      this.label5.Location = new System.Drawing.Point(168, 28);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(51, 13);
      this.label5.TabIndex = 151;
      this.label5.Text = "Actual";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnSetPositionY
      // 
      this.btnSetPositionY.Location = new System.Drawing.Point(43, 66);
      this.btnSetPositionY.Name = "btnSetPositionY";
      this.btnSetPositionY.Size = new System.Drawing.Size(79, 24);
      this.btnSetPositionY.TabIndex = 167;
      this.btnSetPositionY.Text = "SetPositionY";
      this.btnSetPositionY.UseVisualStyleBackColor = true;
      this.btnSetPositionY.Click += new System.EventHandler(this.btnSetPositionY_Click);
      // 
      // btnSetPositionX
      // 
      this.btnSetPositionX.Location = new System.Drawing.Point(43, 41);
      this.btnSetPositionX.Name = "btnSetPositionX";
      this.btnSetPositionX.Size = new System.Drawing.Size(79, 24);
      this.btnSetPositionX.TabIndex = 166;
      this.btnSetPositionX.Text = "SetPositionX";
      this.btnSetPositionX.UseVisualStyleBackColor = true;
      this.btnSetPositionX.Click += new System.EventHandler(this.btnSetPositionX_Click);
      // 
      // btnSetRangeY
      // 
      this.btnSetRangeY.Location = new System.Drawing.Point(42, 137);
      this.btnSetRangeY.Name = "btnSetRangeY";
      this.btnSetRangeY.Size = new System.Drawing.Size(76, 24);
      this.btnSetRangeY.TabIndex = 165;
      this.btnSetRangeY.Text = "SetRangeY";
      this.btnSetRangeY.UseVisualStyleBackColor = true;
      this.btnSetRangeY.Click += new System.EventHandler(this.btnSetRangeY_Click);
      // 
      // btnSetRangeX
      // 
      this.btnSetRangeX.Location = new System.Drawing.Point(42, 112);
      this.btnSetRangeX.Name = "btnSetRangeX";
      this.btnSetRangeX.Size = new System.Drawing.Size(76, 24);
      this.btnSetRangeX.TabIndex = 164;
      this.btnSetRangeX.Text = "SetRangeX";
      this.btnSetRangeX.UseVisualStyleBackColor = true;
      this.btnSetRangeX.Click += new System.EventHandler(this.btnSetRangeX_Click);
      // 
      // btnAbortLaserRow
      // 
      this.btnAbortLaserRow.Location = new System.Drawing.Point(130, 306);
      this.btnAbortLaserRow.Name = "btnAbortLaserRow";
      this.btnAbortLaserRow.Size = new System.Drawing.Size(120, 25);
      this.btnAbortLaserRow.TabIndex = 163;
      this.btnAbortLaserRow.Text = "AbortLaserRow";
      this.btnAbortLaserRow.UseVisualStyleBackColor = true;
      this.btnAbortLaserRow.Click += new System.EventHandler(this.btnAbortLaserRow_Click);
      // 
      // btnGetRangeY
      // 
      this.btnGetRangeY.Location = new System.Drawing.Point(5, 137);
      this.btnGetRangeY.Name = "btnGetRangeY";
      this.btnGetRangeY.Size = new System.Drawing.Size(34, 24);
      this.btnGetRangeY.TabIndex = 162;
      this.btnGetRangeY.Text = "Get";
      this.btnGetRangeY.UseVisualStyleBackColor = true;
      this.btnGetRangeY.Click += new System.EventHandler(this.btnGetRangeY_Click);
      // 
      // btnGetRangeX
      // 
      this.btnGetRangeX.Location = new System.Drawing.Point(5, 112);
      this.btnGetRangeX.Name = "btnGetRangeX";
      this.btnGetRangeX.Size = new System.Drawing.Size(34, 24);
      this.btnGetRangeX.TabIndex = 161;
      this.btnGetRangeX.Text = "Get";
      this.btnGetRangeX.UseVisualStyleBackColor = true;
      this.btnGetRangeX.Click += new System.EventHandler(this.btnGetRangeX_Click);
      // 
      // btnGetPositionY
      // 
      this.btnGetPositionY.Location = new System.Drawing.Point(6, 66);
      this.btnGetPositionY.Name = "btnGetPositionY";
      this.btnGetPositionY.Size = new System.Drawing.Size(34, 24);
      this.btnGetPositionY.TabIndex = 160;
      this.btnGetPositionY.Text = "GetPositionY";
      this.btnGetPositionY.UseVisualStyleBackColor = true;
      this.btnGetPositionY.Click += new System.EventHandler(this.btnGetPositionY_Click);
      // 
      // btnGetPositionX
      // 
      this.btnGetPositionX.Location = new System.Drawing.Point(6, 41);
      this.btnGetPositionX.Name = "btnGetPositionX";
      this.btnGetPositionX.Size = new System.Drawing.Size(34, 24);
      this.btnGetPositionX.TabIndex = 159;
      this.btnGetPositionX.Text = "GetPositionX";
      this.btnGetPositionX.UseVisualStyleBackColor = true;
      this.btnGetPositionX.Click += new System.EventHandler(this.btnGetPositionX_Click);
      // 
      // btnPulseLaserRow
      // 
      this.btnPulseLaserRow.Location = new System.Drawing.Point(5, 306);
      this.btnPulseLaserRow.Name = "btnPulseLaserRow";
      this.btnPulseLaserRow.Size = new System.Drawing.Size(120, 25);
      this.btnPulseLaserRow.TabIndex = 158;
      this.btnPulseLaserRow.Text = "PulseLaserRow";
      this.btnPulseLaserRow.UseVisualStyleBackColor = true;
      this.btnPulseLaserRow.Click += new System.EventHandler(this.btnPulseLaserRow_Click);
      // 
      // label8
      // 
      this.label8.Location = new System.Drawing.Point(106, 28);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(79, 13);
      this.label8.TabIndex = 157;
      this.label8.Text = "Position";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPositionYActual
      // 
      this.nudPositionYActual.Location = new System.Drawing.Point(165, 70);
      this.nudPositionYActual.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYActual.Name = "nudPositionYActual";
      this.nudPositionYActual.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYActual.TabIndex = 156;
      this.nudPositionYActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYActual.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // nudPositionYDelta
      // 
      this.nudPositionYDelta.Location = new System.Drawing.Point(243, 141);
      this.nudPositionYDelta.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYDelta.Name = "nudPositionYDelta";
      this.nudPositionYDelta.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYDelta.TabIndex = 155;
      this.nudPositionYDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYDelta.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label6
      // 
      this.label6.Location = new System.Drawing.Point(119, 72);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(48, 13);
      this.label6.TabIndex = 152;
      this.label6.Text = "MirrorY";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPositionXActual
      // 
      this.nudPositionXActual.Location = new System.Drawing.Point(165, 44);
      this.nudPositionXActual.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXActual.Name = "nudPositionXActual";
      this.nudPositionXActual.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXActual.TabIndex = 150;
      this.nudPositionXActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXActual.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.Location = new System.Drawing.Point(247, 99);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(51, 13);
      this.label4.TabIndex = 149;
      this.label4.Text = "Delta";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPositionXDelta
      // 
      this.nudPositionXDelta.Location = new System.Drawing.Point(243, 115);
      this.nudPositionXDelta.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXDelta.Name = "nudPositionXDelta";
      this.nudPositionXDelta.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXDelta.TabIndex = 148;
      this.nudPositionXDelta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXDelta.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(186, 99);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(51, 13);
      this.label3.TabIndex = 147;
      this.label3.Text = "Maximum";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(126, 99);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(51, 13);
      this.label2.TabIndex = 145;
      this.label2.Text = "Minimum";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label7
      // 
      this.label7.Location = new System.Drawing.Point(119, 46);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(48, 13);
      this.label7.TabIndex = 143;
      this.label7.Text = "MirrorX";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // nudPositionYMaximum
      // 
      this.nudPositionYMaximum.Location = new System.Drawing.Point(183, 140);
      this.nudPositionYMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYMaximum.Name = "nudPositionYMaximum";
      this.nudPositionYMaximum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYMaximum.TabIndex = 154;
      this.nudPositionYMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // nudPositionYMinimum
      // 
      this.nudPositionYMinimum.Location = new System.Drawing.Point(123, 140);
      this.nudPositionYMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionYMinimum.Name = "nudPositionYMinimum";
      this.nudPositionYMinimum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionYMinimum.TabIndex = 153;
      this.nudPositionYMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionYMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // nudPositionXMaximum
      // 
      this.nudPositionXMaximum.Location = new System.Drawing.Point(183, 114);
      this.nudPositionXMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXMaximum.Name = "nudPositionXMaximum";
      this.nudPositionXMaximum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXMaximum.TabIndex = 146;
      this.nudPositionXMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // nudPositionXMinimum
      // 
      this.nudPositionXMinimum.Location = new System.Drawing.Point(123, 114);
      this.nudPositionXMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionXMinimum.Name = "nudPositionXMinimum";
      this.nudPositionXMinimum.Size = new System.Drawing.Size(55, 20);
      this.nudPositionXMinimum.TabIndex = 144;
      this.nudPositionXMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionXMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // btnAbortLaserColumn
      // 
      this.btnAbortLaserColumn.Location = new System.Drawing.Point(130, 341);
      this.btnAbortLaserColumn.Name = "btnAbortLaserColumn";
      this.btnAbortLaserColumn.Size = new System.Drawing.Size(120, 25);
      this.btnAbortLaserColumn.TabIndex = 172;
      this.btnAbortLaserColumn.Text = "AbortLaserColumn";
      this.btnAbortLaserColumn.UseVisualStyleBackColor = true;
      this.btnAbortLaserColumn.Click += new System.EventHandler(this.btnAbortLaserCol_Click);
      // 
      // btnPulseLaserColumn
      // 
      this.btnPulseLaserColumn.Location = new System.Drawing.Point(5, 341);
      this.btnPulseLaserColumn.Name = "btnPulseLaserColumn";
      this.btnPulseLaserColumn.Size = new System.Drawing.Size(120, 25);
      this.btnPulseLaserColumn.TabIndex = 171;
      this.btnPulseLaserColumn.Text = "PulseLaserColumn";
      this.btnPulseLaserColumn.UseVisualStyleBackColor = true;
      this.btnPulseLaserColumn.Click += new System.EventHandler(this.btnPulseLaserCol_Click);
      // 
      // btnSetPulseWidthus
      // 
      this.btnSetPulseWidthus.Location = new System.Drawing.Point(42, 226);
      this.btnSetPulseWidthus.Name = "btnSetPulseWidthus";
      this.btnSetPulseWidthus.Size = new System.Drawing.Size(115, 24);
      this.btnSetPulseWidthus.TabIndex = 175;
      this.btnSetPulseWidthus.Text = "SetPulseWidth [us]";
      this.btnSetPulseWidthus.UseVisualStyleBackColor = true;
      this.btnSetPulseWidthus.Click += new System.EventHandler(this.btnSetPulseWidthus_Click);
      // 
      // btnGetPulseWidthus
      // 
      this.btnGetPulseWidthus.Location = new System.Drawing.Point(5, 226);
      this.btnGetPulseWidthus.Name = "btnGetPulseWidthus";
      this.btnGetPulseWidthus.Size = new System.Drawing.Size(34, 24);
      this.btnGetPulseWidthus.TabIndex = 174;
      this.btnGetPulseWidthus.Text = "Get";
      this.btnGetPulseWidthus.UseVisualStyleBackColor = true;
      this.btnGetPulseWidthus.Click += new System.EventHandler(this.btnGetPulseWidthus_Click);
      // 
      // nudPulseWidthus
      // 
      this.nudPulseWidthus.Location = new System.Drawing.Point(163, 228);
      this.nudPulseWidthus.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
      this.nudPulseWidthus.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPulseWidthus.Name = "nudPulseWidthus";
      this.nudPulseWidthus.Size = new System.Drawing.Size(75, 20);
      this.nudPulseWidthus.TabIndex = 173;
      this.nudPulseWidthus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulseWidthus.Value = new decimal(new int[] {
            500000,
            0,
            0,
            0});
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(462, 23);
      this.lblHeader.TabIndex = 176;
      this.lblHeader.Text = "LaserAreaScanner - LaserStepLine";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnGetPulsePeriodms
      // 
      this.btnGetPulsePeriodms.Location = new System.Drawing.Point(6, 198);
      this.btnGetPulsePeriodms.Name = "btnGetPulsePeriodms";
      this.btnGetPulsePeriodms.Size = new System.Drawing.Size(32, 24);
      this.btnGetPulsePeriodms.TabIndex = 182;
      this.btnGetPulsePeriodms.Text = "Get";
      this.btnGetPulsePeriodms.UseVisualStyleBackColor = true;
      this.btnGetPulsePeriodms.Click += new System.EventHandler(this.btnGetPulsePeriodms_Click);
      // 
      // btnGetPulseCount
      // 
      this.btnGetPulseCount.Location = new System.Drawing.Point(6, 171);
      this.btnGetPulseCount.Name = "btnGetPulseCount";
      this.btnGetPulseCount.Size = new System.Drawing.Size(32, 24);
      this.btnGetPulseCount.TabIndex = 181;
      this.btnGetPulseCount.Text = "Get";
      this.btnGetPulseCount.UseVisualStyleBackColor = true;
      this.btnGetPulseCount.Click += new System.EventHandler(this.btnGetPulseCount_Click);
      // 
      // btnSetPulsePeriodms
      // 
      this.btnSetPulsePeriodms.Location = new System.Drawing.Point(42, 198);
      this.btnSetPulsePeriodms.Name = "btnSetPulsePeriodms";
      this.btnSetPulsePeriodms.Size = new System.Drawing.Size(115, 24);
      this.btnSetPulsePeriodms.TabIndex = 180;
      this.btnSetPulsePeriodms.Text = "SetPulsePeriod [ms]";
      this.btnSetPulsePeriodms.UseVisualStyleBackColor = true;
      this.btnSetPulsePeriodms.Click += new System.EventHandler(this.btnSetPulsePeriodms_Click);
      // 
      // nudPulseCount
      // 
      this.nudPulseCount.Location = new System.Drawing.Point(163, 173);
      this.nudPulseCount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudPulseCount.Name = "nudPulseCount";
      this.nudPulseCount.Size = new System.Drawing.Size(55, 20);
      this.nudPulseCount.TabIndex = 179;
      this.nudPulseCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulseCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // nudPulsePeriodms
      // 
      this.nudPulsePeriodms.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudPulsePeriodms.Location = new System.Drawing.Point(163, 200);
      this.nudPulsePeriodms.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudPulsePeriodms.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudPulsePeriodms.Name = "nudPulsePeriodms";
      this.nudPulsePeriodms.Size = new System.Drawing.Size(55, 20);
      this.nudPulsePeriodms.TabIndex = 178;
      this.nudPulsePeriodms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulsePeriodms.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // btnSetPulseCount
      // 
      this.btnSetPulseCount.Location = new System.Drawing.Point(42, 171);
      this.btnSetPulseCount.Name = "btnSetPulseCount";
      this.btnSetPulseCount.Size = new System.Drawing.Size(115, 24);
      this.btnSetPulseCount.TabIndex = 177;
      this.btnSetPulseCount.Text = "SetPulseCount [1]";
      this.btnSetPulseCount.UseVisualStyleBackColor = true;
      this.btnSetPulseCount.Click += new System.EventHandler(this.btnSetPulseCount_Click);
      // 
      // cbxSkipZeroPulses
      // 
      this.cbxSkipZeroPulses.AutoSize = true;
      this.cbxSkipZeroPulses.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.cbxSkipZeroPulses.Location = new System.Drawing.Point(18, 284);
      this.cbxSkipZeroPulses.Name = "cbxSkipZeroPulses";
      this.cbxSkipZeroPulses.Size = new System.Drawing.Size(100, 17);
      this.cbxSkipZeroPulses.TabIndex = 183;
      this.cbxSkipZeroPulses.Text = "SkipZeroPulses";
      this.cbxSkipZeroPulses.UseVisualStyleBackColor = true;
      // 
      // CUCLaserStepLine
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.cbxSkipZeroPulses);
      this.Controls.Add(this.btnSetDelayMotionms);
      this.Controls.Add(this.btnGetDelayMotionms);
      this.Controls.Add(this.nudDelayMotionms);
      this.Controls.Add(this.btnGetPulsePeriodms);
      this.Controls.Add(this.btnGetPulseCount);
      this.Controls.Add(this.btnSetPulsePeriodms);
      this.Controls.Add(this.nudPulseCount);
      this.Controls.Add(this.nudPulsePeriodms);
      this.Controls.Add(this.btnSetPulseCount);
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.btnSetPulseWidthus);
      this.Controls.Add(this.btnGetPulseWidthus);
      this.Controls.Add(this.nudPulseWidthus);
      this.Controls.Add(this.btnAbortLaserColumn);
      this.Controls.Add(this.btnPulseLaserColumn);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.btnSetPositionY);
      this.Controls.Add(this.btnSetPositionX);
      this.Controls.Add(this.btnSetRangeY);
      this.Controls.Add(this.btnSetRangeX);
      this.Controls.Add(this.btnAbortLaserRow);
      this.Controls.Add(this.btnGetRangeY);
      this.Controls.Add(this.btnGetRangeX);
      this.Controls.Add(this.btnGetPositionY);
      this.Controls.Add(this.btnGetPositionX);
      this.Controls.Add(this.btnPulseLaserRow);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.nudPositionYActual);
      this.Controls.Add(this.nudPositionYDelta);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.nudPositionXActual);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.nudPositionXDelta);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.nudPositionYMaximum);
      this.Controls.Add(this.nudPositionYMinimum);
      this.Controls.Add(this.nudPositionXMaximum);
      this.Controls.Add(this.nudPositionXMinimum);
      this.Name = "CUCLaserStepLine";
      this.Size = new System.Drawing.Size(462, 374);
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayMotionms)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYActual)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYDelta)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXActual)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXDelta)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionYMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionXMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseWidthus)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulseCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriodms)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnSetDelayMotionms;
    private System.Windows.Forms.Button btnGetDelayMotionms;
    private System.Windows.Forms.NumericUpDown nudDelayMotionms;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Button btnSetPositionY;
    private System.Windows.Forms.Button btnSetPositionX;
    private System.Windows.Forms.Button btnSetRangeY;
    private System.Windows.Forms.Button btnSetRangeX;
    private System.Windows.Forms.Button btnAbortLaserRow;
    private System.Windows.Forms.Button btnGetRangeY;
    private System.Windows.Forms.Button btnGetRangeX;
    private System.Windows.Forms.Button btnGetPositionY;
    private System.Windows.Forms.Button btnGetPositionX;
    private System.Windows.Forms.Button btnPulseLaserRow;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.NumericUpDown nudPositionYActual;
    private System.Windows.Forms.NumericUpDown nudPositionYDelta;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.NumericUpDown nudPositionXActual;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.NumericUpDown nudPositionXDelta;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.NumericUpDown nudPositionYMaximum;
    private System.Windows.Forms.NumericUpDown nudPositionYMinimum;
    private System.Windows.Forms.NumericUpDown nudPositionXMaximum;
    private System.Windows.Forms.NumericUpDown nudPositionXMinimum;
    private System.Windows.Forms.Button btnAbortLaserColumn;
    private System.Windows.Forms.Button btnPulseLaserColumn;
    private System.Windows.Forms.Button btnGetPulseWidthus;
    private System.Windows.Forms.Button btnSetPulseWidthus;
    private System.Windows.Forms.NumericUpDown nudPulseWidthus;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Button btnGetPulsePeriodms;
    private System.Windows.Forms.Button btnGetPulseCount;
    private System.Windows.Forms.Button btnSetPulsePeriodms;
    private System.Windows.Forms.NumericUpDown nudPulseCount;
    private System.Windows.Forms.NumericUpDown nudPulsePeriodms;
    private System.Windows.Forms.Button btnSetPulseCount;
    private System.Windows.Forms.CheckBox cbxSkipZeroPulses;
  }
}
