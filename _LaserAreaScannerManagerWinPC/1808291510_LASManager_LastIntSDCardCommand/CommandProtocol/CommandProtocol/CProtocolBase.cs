﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
//
using UCNotifier;
using Task;
//
namespace CommandProtocol
{
  public abstract class CProtocolBase
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    protected String FHeader;
    private CTask FTask;
    private DOnExecutionStart FOnExecutionStart;
    private DOnExecutionBusy FOnExecutionBusy;
    private DOnExecutionEnd FOnExecutionEnd;
    private DOnExecutionAbort FOnExecutionAbort;
    private CSignal FSignal;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CProtocolBase(String header)
    {
      FHeader = header;
      FTask = new CTask(FHeader, OnExecutionStart, OnExecutionBusy, OnExecutionEnd, OnExecutionAbort);
      FSignal = new CSignal();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnExecutionStart(DOnExecutionStart value)
    {
      FOnExecutionStart = value;
    }
    public DOnExecutionStart GetOnExecutionStart()
    {
      return FOnExecutionStart;
    }

    public void SetOnExecutionBusy(DOnExecutionBusy value)
    {
      FOnExecutionBusy = value;
    }
    public DOnExecutionBusy GetOnExecutionBusy()
    {
      return FOnExecutionBusy;
    }

    public void SetOnExecutionEnd(DOnExecutionEnd value)
    {
      FOnExecutionEnd = value;
    }
    public DOnExecutionEnd GetOnExecutionEnd()
    {
      return FOnExecutionEnd;
    }

    public void SetOnExecutionAbort(DOnExecutionAbort value)
    {
      FOnExecutionAbort = value;
    }
    public DOnExecutionAbort GetOnExecutionAbort()
    {
      return FOnExecutionAbort;
    }


    public abstract String GetCommandHeader();
    public abstract String GetCommandText();

    private void OnExecutionStart(RTaskData data)
    {
      // debug FNotifier.Write(String.Format("Base[{0}] - OnExecutionStart", data.Name));
      FSignal.Reset();
      if (FOnExecutionStart is DOnExecutionStart)
      {
        FOnExecutionStart(data);
      }
    }

    private Boolean OnExecutionBusy(RTaskData data)
    {
      // debug FNotifier.Write(String.Format("Base[{0}] - OnExecutionBusy - Start", data.Name));
      if (FOnExecutionBusy is DOnExecutionBusy)
      {
        FOnExecutionBusy(data);
      }
      FSignal.WaitFor(10000);
      //Thread.Sleep(1000);
      // debug FNotifier.Write(String.Format("OnExecutionBusy - End", data.Name));
      return false;
    }

    private void OnExecutionEnd(RTaskData data)
    {
      // debug FNotifier.Write(String.Format("Base[{0}] - OnExecutionEnd", data.Name));
      if (FOnExecutionEnd is DOnExecutionEnd)
      {
        FOnExecutionEnd(data);
      }
    }

    private void OnExecutionAbort(RTaskData data)
    {
      // debug FNotifier.Write(String.Format("Base[{0}] - OnExecutionAbort", data.Name));
      if (FOnExecutionAbort is DOnExecutionAbort)
      {
        FOnExecutionAbort(data);
      }
    }



    public Boolean Execute()
    {
      return FTask.Start();
    }

    public void Abort()
    {
      FTask.Abort();
    }

    public void SignalTextReceived()
    {
      FSignal.Set();
    }

  }
}
