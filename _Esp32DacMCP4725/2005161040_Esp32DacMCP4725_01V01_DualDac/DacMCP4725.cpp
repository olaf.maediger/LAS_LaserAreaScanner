//
#include "DacMCP4725.h"
//
CDacMCP4725::CDacMCP4725(uint8_t i2caddress) 
{
  FI2CAddress = i2caddress;
}

void CDacMCP4725::Open(void)
{  
  Wire.begin();
}
 
void CDacMCP4725::Close(void)
{  
  //Wire.end();
}
 
void CDacMCP4725::WriteLevel(uint16_t level)
{
  Wire.beginTransmission(FI2CAddress);
  Wire.write(MCP4726_CMD_WRITEDAC);
  Wire.write(level / 16);                   
  Wire.write((level % 16) << 4);            
  Wire.endTransmission();
}
