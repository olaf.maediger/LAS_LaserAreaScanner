//
#include <Arduino.h>
#include "DacMCP4725.h"
//
#define SerialPC Serial
//
CDacMCP4725 DacMCP4725_X(0x62);
CDacMCP4725 DacMCP4725_Y(0x63);
//
void setup() 
{
  SerialPC.begin(115200);
  delay(300);
  SerialPC.println("\r\n");
  SerialPC.println("************************");
  SerialPC.println("* Esp32DacMCP4725      *");
  SerialPC.println("************************");
  SerialPC.println("* Version : 01V01      *");
  SerialPC.println("* DateTime: 2005161036 *");
  SerialPC.println("* Author  : OMDevelop  *");
  SerialPC.println("************************");
  //
  DacMCP4725_X.Open();
  DacMCP4725_Y.Open();
}

void loop() 
{
  for (int LI = 0; LI <= 4096; LI += 128)
  {
    int L = min(4095, LI);
    DacMCP4725_X.WriteLevel(L);
  }
  for (int LI = 4095; 0 <= LI; LI -= 128)
  {
    int L = max(0, LI);
    DacMCP4725_X.WriteLevel(L);
  }
  //
  for (int LI = 0; LI <= 4096; LI += 128)
  {
    int L = min(4095, LI);
    DacMCP4725_Y.WriteLevel(L);
  }
  for (int LI = 4095; 0 <= LI; LI -= 128)
  {
    int L = max(0, LI);
    DacMCP4725_Y.WriteLevel(L);
  }
  //
//  DacMCP4725.WriteLevel(4095);
//  DacMCP4725.WriteLevel(4095 - 1024);
//  DacMCP4725.WriteLevel(0 + 1024);
//  DacMCP4725.WriteLevel(0);
}
