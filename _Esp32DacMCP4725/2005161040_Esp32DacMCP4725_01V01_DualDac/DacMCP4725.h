
#include "Arduino.h"

#include <Wire.h>

#define MCP4726_CMD_WRITEDAC                  0x40

class CDacMCP4725
{
  private:
  uint8_t FI2CAddress;
  //
  public:
  CDacMCP4725(uint8_t i2caddress);
  void Open(void);
  void Close(void);
  void WriteLevel(uint16_t level);

};
