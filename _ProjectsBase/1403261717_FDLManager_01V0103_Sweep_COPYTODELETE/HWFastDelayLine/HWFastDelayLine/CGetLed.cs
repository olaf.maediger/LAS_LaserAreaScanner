﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;
//
namespace HWFastDelayLine
{
  class CGetLed : CCommand
  {
    public const String INIT_NAME = "GetLed";
		public const String COMMAND_TEXT = "GLD";

		public CGetLed(CCommandlist parent,
								   Byte index, 
									 CNotifier notifier,
									 CDevice device,
									 CComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, index),
             notifier, device, comport)
    {
      SetParent(this);
    }

    public override void SelfOnLineReceived(Guid comportid,
                                            String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CHWFastDelayLine.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
			String[] Tokenlist = line.Split(' ');
      if (3 < Tokenlist.Length)
      {
        if ("1" == Tokenlist[3])
        {
          Commandlist.Library.RefreshLed(Tokenlist[2], true);
        }
        else
        {
          Commandlist.Library.RefreshLed(Tokenlist[2], false);
        }
      }
    }

  }
}
