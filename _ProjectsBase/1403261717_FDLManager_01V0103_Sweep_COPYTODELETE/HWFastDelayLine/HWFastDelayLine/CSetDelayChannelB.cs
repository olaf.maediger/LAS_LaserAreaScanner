﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;
//
namespace HWFastDelayLine
{
  public class CSetDelayChannelB : CCommand
  {
    public const String INIT_NAME = "SetDelayChannelB";
    public const String COMMAND_TEXT = "SDB";

    public CSetDelayChannelB(CCommandlist parent,
                             Int32 delay,
                             CNotifier notifier,
                             CDevice device,
                             CComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, delay),
             notifier, device, comport)
    {
      SetParent(this);
    }

    public override void SelfOnLineReceived(Guid comportid,
                                            String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CHWFastDelayLine.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = line.Split(' ');
      if (2 < Tokenlist.Length)
      {
        Commandlist.Library.RefreshDelayChannelB(Tokenlist[2]);
      }
    }

  }
}
