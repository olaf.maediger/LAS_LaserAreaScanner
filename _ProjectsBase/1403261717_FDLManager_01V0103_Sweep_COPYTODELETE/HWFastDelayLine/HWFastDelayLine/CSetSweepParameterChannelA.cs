﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using ComPort;
//
namespace HWFastDelayLine
{
  public class CSetSweepParameterChannelA : CCommand
  {
    public const String INIT_NAME = "SetSweepParameterChannelA";
    public const String COMMAND_TEXT = "SSA";

    private Int32 FIndex;
    private Int32 FValue;

    public CSetSweepParameterChannelA(CCommandlist parent,
                                      Int32 index,
                                      Int32 value,
                                      CNotifier notifier,
                                      CDevice device,
                                      CComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1} {2}", COMMAND_TEXT, index, value),
             notifier, device, comport)
    {
      SetParent(this);
      FIndex = index;
      FValue = value;
    }

    public override void SelfOnLineReceived(Guid comportid,
                                            String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CHWFastDelayLine.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = line.Split(' ');
      if (3 < Tokenlist.Length)
      {
        if (FIndex == Int32.Parse(Tokenlist[2]))
        {
          if (FValue == Int32.Parse(Tokenlist[3]))
          {
            Commandlist.Library.RefreshSweepParameterChannelA(FIndex, FValue);
          }
        }
      }
    }
  }
}
