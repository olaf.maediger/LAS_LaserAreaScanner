﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using ComPort;
//
namespace HWFastDelayLine
{
  public class CSetSweepStateChannelA : CCommand
  {
    public const String INIT_NAME = "SetSweepStateChannelA";
    public const String COMMAND_TEXT = "SWA";

    private ESweepState FSweepState;

    public CSetSweepStateChannelA(CCommandlist parent,
                                  ESweepState value,
                                  CNotifier notifier,
                                  CDevice device,
                                  CComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, (Int32)value),
             notifier, device, comport)
    {
      SetParent(this);
      FSweepState = value;
    }

    public override void SelfOnLineReceived(Guid comportid,
                                            String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CHWFastDelayLine.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = line.Split(' ');
      if (2 < Tokenlist.Length)
      {
        ESweepState SS = CHWFastDelayLine.StringIndexToSweepState(Tokenlist[2]);
        Commandlist.Library.RefreshSweepStateChannelA(SS);
      }
    }
  }
}
