﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;
//
namespace HWFastDelayLine
{
  class CGetDelayChannelB : CCommand
  {
    public const String INIT_NAME = "GetDelayChannelB";
    public const String COMMAND_TEXT = "GDB";

    public CGetDelayChannelB(CCommandlist parent,
                             CNotifier notifier,
                             CDevice device,
                             CComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT),
             notifier, device, comport)
    {
      SetParent(this);
    }

    public override void SelfOnLineReceived(Guid comportid,
                                            String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CHWFastDelayLine.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = line.Split(' ');
      if (2 < Tokenlist.Length)
      {
        Commandlist.Library.RefreshDelayChannelA(Tokenlist[2]);
      }
    }

  }
}
