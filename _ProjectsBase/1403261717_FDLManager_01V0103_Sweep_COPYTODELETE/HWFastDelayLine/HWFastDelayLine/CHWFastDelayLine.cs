﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using Initdata;
using ComPort;
//
namespace HWFastDelayLine
{
  //
  //--------------------------------------
  //	Section - Type - Delegate
  //--------------------------------------
  //
  public delegate void DOnCommonDataChanged(RCommonData data);
  public delegate void DOnChannelDataChanged(RChannelData data);
  public delegate void DOnLedDataChanged(RLedData data);
  //
  //-----------------------------------------------------
  //  Segment - Type - ChannelData - A/B
  //-----------------------------------------------------
  //
  public struct RCommonData
  {
    public String SoftwareVersion;
    public String HardwareVersion;
    public String DeviceType;
    public DOnCommonDataChanged OnCommonDataChanged;
    //
    public RCommonData(Int32 init)
    {
      SoftwareVersion = CHWFastDelayLine.INIT_SOFTWAREVERSION;
      HardwareVersion = CHWFastDelayLine.INIT_HARDWAREVERSION;
      DeviceType = CHWFastDelayLine.INIT_DEVICETYPE;
      OnCommonDataChanged = null;
    }
  };
  //
  //-----------------------------------------------------
  //  Segment - Type - ChannelData - A/B
  //-----------------------------------------------------
  //
  public struct RChannelData
  {
    public Int32 Delay;
    public ESweepState SweepState;
    public Int32 LimitLow;
    public Int32 LimitHigh;
    public Int32 StepDelta;
    public Int32 StepCount;
    public Int32 StepIndex;
    public Int32 RepetitionCount;
    public Int32 RepetitionIndex;
    //
    public DOnChannelDataChanged OnChannelDataChanged;
    //
    public RChannelData(Int32 init)
    {
      Delay = CHWFastDelayLine.INIT_DELAYSTEPS10PS;
      SweepState = CHWFastDelayLine.INIT_SWEEPSTATE;
      LimitLow = CHWFastDelayLine.INIT_LIMITLOW;
      LimitHigh = CHWFastDelayLine.INIT_LIMITHIGH;
      StepDelta = CHWFastDelayLine.INIT_STEPDELTA;
      StepCount = CHWFastDelayLine.INIT_STEPCOUNT;
      StepIndex = CHWFastDelayLine.INIT_STEPINDEX;
      RepetitionCount = CHWFastDelayLine.INIT_REPETITIONCOUNT;
      RepetitionIndex = CHWFastDelayLine.INIT_REPETITIONINDEX;
      //
      OnChannelDataChanged = null;
    }
  }
  //
  //-----------------------------------------------------
  //  Segment - Type - LedData
  //-----------------------------------------------------
  //
  public struct RLedData
  {
    public Int32 Index;
    public Boolean Active;
    public DOnLedDataChanged OnLedDataChanged;
    //
    public RLedData(Int32 init)
    {
      Index = CHWFastDelayLine.INIT_LEDINDEX;
      Active = CHWFastDelayLine.INIT_LEDACTIVE;
      OnLedDataChanged = null;
    }
  };
  //
  //-----------------------------------------------------
  //  Segment - Type - Container ConfigurationData
  //-----------------------------------------------------
  //
  public struct RConfigurationData
  { // ComPort
    public RComPortData ComPortData;
    public DOnComPortDataChanged OnComPortDataChanged;
    // Common
    public RCommonData CommonData;
    // DelayChannelA
    public RChannelData ChannelDataA;
    // DelayChannelB
    public RChannelData ChannelDataB;
    // Led
    public RLedData LedData3;
    public RLedData LedData7;
    //
    public RConfigurationData(Int32 init)
    {
      ComPortData = new RComPortData(init);
      OnComPortDataChanged = null;
      //
      CommonData = new RCommonData(init);
      //
      ChannelDataA = new RChannelData(init);
      ChannelDataB = new RChannelData(init);
      //
      LedData3 = new RLedData(init);
      LedData7 = new RLedData(init);
    }
  };
  //
  //#######################################################################################
  //  Segment - Main - CHWFastDelayLine
  //#######################################################################################
  //
  public class CHWFastDelayLine : CHWFastDelayLineBase
  {
    //
    //--------------------------------------
    //	Section - Constant
    //--------------------------------------
    //

    //
    //--------------------------------------
    //	Section - Member
    //--------------------------------------
    //

    //
    //--------------------------------------
    //	Section - Constructor
    //--------------------------------------
    //
    public CHWFastDelayLine()
      : base()
    {
    }
		//
		//--------------------------------------
		//	Section - Properties
		//--------------------------------------
		//
		//
		public new void SetNotifier(CNotifier notifier)
		{
			try
			{
				base.SetNotifier(notifier);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
			}
		}
    //
		//--------------------------------------
		//	Section - Get/SetData
		//--------------------------------------
		//
    public new Boolean GetConfigurationData(ref RConfigurationData data)
    {
      try
      {
        return base.GetConfigurationData(ref data);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    public new Boolean SetConfigurationData(RConfigurationData data)
    {
      try
      {
        return base.SetConfigurationData(data);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
		//--------------------------------------
		//	Section - Management
		//--------------------------------------
		//
    public new void SetOnRXDLineReceived(DOnRXDLineReceived value)
    {
      try
      {
        base.SetOnRXDLineReceived(value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
      }
    }

    public new void SetOnDataTransmitted(DOnDataTransmitted value)
    {
      try
      {
        base.SetOnDataTransmitted(value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
      }
    }




		public new Boolean OpenComPort(EComPort comport)
		{
			try
			{
				return base.OpenComPort(comport);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean CloseComPort()
		{
			try
			{
				return base.CloseComPort();
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

    public new Boolean IsComPortOpen()
    {
      try
      {
        return base.IsComPortOpen();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

		public new Boolean LoadInitdata(CInitdataReader initdata)
		{
			try
			{
				return base.LoadInitdata(initdata);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}

		public new Boolean SaveInitdata(CInitdataWriter initdata)
		{
			try
			{
				return base.SaveInitdata(initdata);
			}
			catch (Exception)
			{
				_Error(EErrorCode.AccessFailed);
				return false;
			}
		}
    //
    //--------------------------------------
    //	Section - Command - Common
    //--------------------------------------
    //
    public new Boolean CommandGetHelp()
    {
      try
      {
        return base.CommandGetHelp();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean CommandGetSoftwareVersion()
    {
      try
      {
        return base.CommandGetSoftwareVersion();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean CommandGetHardwareVersion()
    {
      try
      {
        return base.CommandGetHardwareVersion();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    //
    //--------------------------------------
    //	Section - Command - Delay - ChannelA
    //--------------------------------------
    //
    public new Boolean CommandGetDelayChannelA()
    {
      try
      {
        return base.CommandGetDelayChannelA();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean CommandSetDelayChannelA(Int32 value)
    {
      try
      {
        return base.CommandSetDelayChannelA(value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Command - Delay - ChannelB
    //--------------------------------------
    //
    public new Boolean CommandGetDelayChannelB()
    {
      try
      {
        return base.CommandGetDelayChannelB();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean CommandSetDelayChannelB(Int32 value)
    {
      try
      {
        return base.CommandSetDelayChannelB(value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Command - Led
    //--------------------------------------
    //
    public new Boolean CommandGetLed(Byte index)
    {
      try
      {
        return base.CommandGetLed(index);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean CommandClearLed(Byte index)
    {
      try
      {
        return base.CommandClearLed(index);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public new Boolean CommandSetLed(Byte index)
    {
      try
      {
        return base.CommandSetLed(index);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Command - SSA
    //--------------------------------------
    //
    public new Boolean CommandSetSweepParameterChannelA(Int32 index, Int32 value)
    {
      try
      {
        return base.CommandSetSweepParameterChannelA(index, value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Command - GWA
    //--------------------------------------
    //
    public new Boolean CommandGetSweepStateChannelA()
    {
      try
      {
        return base.CommandGetSweepStateChannelA();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Command - SWA
    //--------------------------------------
    //
    public new Boolean CommandSetSweepStateChannelA(ESweepState value)
    {
      try
      {
        return base.CommandSetSweepStateChannelA(value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Command - GSA
    //--------------------------------------
    //
    public new Boolean CommandGetSweepParameterChannelA(Int32 index)
    {
      try
      {
        return base.CommandGetSweepParameterChannelA(index);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Command - SSB
    //--------------------------------------
    //
    public new Boolean CommandSetSweepParameterChannelB(Int32 index, Int32 value)
    {
      try
      {
        return base.CommandSetSweepParameterChannelB(index, value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Command - GSB
    //--------------------------------------
    //
    public new Boolean CommandGetSweepParameterChannelB(Int32 index)
    {
      try
      {
        return base.CommandGetSweepParameterChannelB(index);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Command - GWB
    //--------------------------------------
    //
    public new Boolean CommandGetSweepStateChannelB()
    {
      try
      {
        return base.CommandGetSweepStateChannelB();
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Command - SWB
    //--------------------------------------
    //
    public new Boolean CommandSetSweepStateChannelB(ESweepState value)
    {
      try
      {
        return base.CommandSetSweepStateChannelB(value);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //--------------------------------------
    //	Section - Common
    //--------------------------------------
    //
    public new Boolean SendLine(String line)
    {
      try
      {
        return base.SendLine(line);
      }
      catch (Exception)
      {
        _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    

	}
}
