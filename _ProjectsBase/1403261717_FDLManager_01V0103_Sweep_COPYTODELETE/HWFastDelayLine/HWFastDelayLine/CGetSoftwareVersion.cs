﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;

namespace HWFastDelayLine
{
	public class CGetSoftwareVersion : CCommand
	{
    public const String INIT_NAME = "GetSoftwareVersion";
    public const String COMMAND_TEXT = "GSV";

		public CGetSoftwareVersion(CCommandlist parent,
											 CNotifier notifier,
											 CDevice device,
											 CComPort comport)
      : base(parent, INIT_NAME, COMMAND_TEXT, notifier, device, comport)
    {
      SetParent(this);
    }

    public override void SelfOnLineReceived(Guid comportid,
                                            String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
			Notifier.Write(CHWFastDelayLine.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
			String[] Tokenlist = line.Split(' ');
			if (2 < Tokenlist.Length)
			{
        Commandlist.Library.RefreshSoftwareVersion(Tokenlist[2]);
			}
		}

	}
}
