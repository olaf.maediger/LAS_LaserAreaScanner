﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UCNotifier;
using ComPort;

namespace HWFastDelayLine
{
	public delegate void DOnEndExecute(CCommand command);

	public enum ECommandState 
	{ 
		Error = -1,
		Init = 0,
		Begin = 1, 
		Busy = 2, 
		Abort = 3,
		End = 4
	};

	public abstract class CCommand
	{
    protected const Int32 INIT_WAITFOR = 1000;

    private CNotifier FNotifier;        // Only Reference!
		protected CNotifier Notifier
		{
			get { return FNotifier; }
		}

		private CCommandlist FCommandlist;  // Only Reference!
		protected CCommandlist Commandlist
		{
			get { return FCommandlist; }
		}

    private CCommand FParent;           // Only Reference!
    protected void SetParent(CCommand parent)
    {
      FParent = parent;
    }


    private String FName;
    protected String Name
    {
      get { return FName; }
    }

    private String FCommandText;
    protected String CommandText
    {
      get { return FCommandText; }
    }
    public void SetCommandText(String value)
    {
      FCommandText = value;
    }

    private ECommandState FState; // Threadlist-Manager!

		private void SetState(ECommandState value)
		{
			if (value != FState)
			{
				FState = value;
        //
				// Debug 
        String Line = String.Format("Command[{0}]: State[{1}]", Name, State);
        // Debug 
        FNotifier.Write(Line);
        // Debug Console.WriteLine(Line);
        //
				if (FOnEndExecute is DOnEndExecute)
				{
					FOnEndExecute(this);
				}
			}
		}
		public ECommandState State
		{
			get { return FState; }
			set { SetState(value); }
		}
		protected Thread FThread;
		private CDevice FDevice;
		protected CDevice Device
		{
			get { return FDevice; }
		}

		private CComPort FComPort;
		protected CComPort ComPort
		{
			get { return FComPort; }
		}

		private DOnEndExecute FOnEndExecute;

    private CEvent FWaitResponse;
    private Int32 FLineCount;

    public CCommand(CCommandlist commandlist,
										String name,
                    String commandtext,
										CNotifier notifier,
									  CDevice device,
										CComPort comport)
		{
      FCommandlist = commandlist;
      FName = name;
      FCommandText = commandtext;
      FNotifier = notifier;
			FDevice = device;
			FComPort = comport;
			FThread = new Thread(new ThreadStart(Execute));
      FWaitResponse = new CEvent();
		}

    protected void IncrementLineCount()
    {
      FLineCount++;
    }

    protected void BaseOnLineReceived()
    {
      IncrementLineCount();
      if (1 <= FLineCount)
      {
        FWaitResponse.Set();
      }
    }

		public void Abort()
		{
			State = ECommandState.Abort;
      FThread.Abort();
			Passivate();
		}

		public void Join()
		{
			FThread.Join();
		}

    public abstract void SelfOnLineReceived(Guid comportid,
                                            String line);

    protected virtual void Execute()
    {	// Start
      State = ECommandState.Begin;
      Commandlist.SetOnChildLineReceived(FParent.SelfOnLineReceived);
      //
      State = ECommandState.Busy;
      FLineCount = 0;
      FWaitResponse.Reset();
      //
      // debug Thread.Sleep(1);
      ComPort.WriteLine(CommandText);
      if (!FWaitResponse.WaitFor(INIT_WAITFOR))
      {
        State = ECommandState.Error;
        FNotifier.Error(CHWFastDelayLine.HEADER_LIBRARY + " " + FName, 
                        1, "No response of hardware");
      }
      //
      State = ECommandState.End;
      Commandlist.SetOnChildLineReceived(null);
      //
      // debug Thread.Sleep(100);
    }


		public void Activate(DOnEndExecute onendexecute)
		{
      // debug FNotifier.Write(String.Format("Command[{0}].Activate", this.ToString()));
      FOnEndExecute = onendexecute;
			if (ThreadState.Unstarted == FThread.ThreadState)
			{
        // debug FNotifier.Write(String.Format("Thread[{0}].Start", this.ToString()));
        FThread.Start();
			}
		}

		public void Passivate()
		{
			try
			{
				if (ThreadState.Running == FThread.ThreadState)
				{
					State = ECommandState.End;
				}
			}
			catch (Exception)
			{
				// NC Console.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			}
		}
	}

}
