﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
//
using Initdata;
using UCNotifier;
using UCFastDelayLine;
//
namespace FDLManager
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;
    private CUCFastDelayLine FUCFastDelayLine; 
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //
      FUCFastDelayLine = new CUCFastDelayLine();
      FUCFastDelayLine.SetNotifier(FUCNotifier);
      tbpFastDelayLine.Controls.Add(FUCFastDelayLine);
      //
      tbcTop.Controls.Remove(tbpSerialNumberProductKey);
      //
      tmrStartup.Interval = 100;
      //
      tmrStartup.Start();

      FRandom = new Random();
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FUCFastDelayLine.CloseComPort();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      Result &= FUCFastDelayLine.LoadInitdata(initdata);
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      Result &= FUCFastDelayLine.SaveInitdata(initdata);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUC...
    //------------------------------------------------------------------------
    // 
    // NC private void UCPeakDetectionOnProcessStateChanged(EProcessState statepreset, 
    // NC                                                   EProcessState stateactual)
    // NC {
    // NC }
    //
    //------------------------------------------------------------------------
    //  Section - Helper (Common global functions)
    //------------------------------------------------------------------------
    //

    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          tmrStartup.Interval = 100;
          return true;
        case 1:
          //SolveCubicSplineNatural();
          tmrStartup.Interval = 100;
          return true;
        case 2:
          //SolveCubicSplineClamped();
          tmrStartup.Interval = 300;
          return true;
        case 3:
          tmrStartup.Interval = 700;
          return true;
        case 4:
          tmrStartup.Interval = 700;
          return true;
        case 5:
          tmrStartup.Interval = 700;
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        default:
          return false;
      }
    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }
    //
    //##################################################
    //--------------------------------------------------
    //  Helper - ...
    //--------------------------------------------------
    //##################################################
    //

    //
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    //--------------------------------------------
    //############################################
    //--------------------------------------------
    //  Menu-Function - ...
    //--------------------------------------------
    //############################################
    //--------------------------------------------
    // 


  }
}



 