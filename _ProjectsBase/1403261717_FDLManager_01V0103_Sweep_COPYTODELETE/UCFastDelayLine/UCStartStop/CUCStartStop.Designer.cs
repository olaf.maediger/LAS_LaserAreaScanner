﻿namespace UCStartStop
{
  partial class CUCStartStop
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnStartStop = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // btnStartStop
      // 
      this.btnStartStop.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btnStartStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnStartStop.Location = new System.Drawing.Point(0, 0);
      this.btnStartStop.Name = "btnStartStop";
      this.btnStartStop.Size = new System.Drawing.Size(100, 60);
      this.btnStartStop.TabIndex = 1;
      this.btnStartStop.Text = "<startstop>";
      this.btnStartStop.UseVisualStyleBackColor = true;
      this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
      // 
      // CUCStartStop
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnStartStop);
      this.ImeMode = System.Windows.Forms.ImeMode.NoControl;
      this.Name = "CUCStartStop";
      this.Size = new System.Drawing.Size(100, 60);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnStartStop;
  }
}
