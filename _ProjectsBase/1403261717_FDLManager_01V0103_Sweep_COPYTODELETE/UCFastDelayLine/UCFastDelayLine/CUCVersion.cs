﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCFastDelayLine
{
  public delegate void DOnGetVersion();
  public delegate void DOnGetHelp();

  public partial class CUCVersion : UserControl
  {
    //
    //----------------------------------------------
    //	Section - 
    //----------------------------------------------
    //

    //
    //----------------------------------------------
    //	Section - Field
    //----------------------------------------------
    //
    private DOnGetVersion FOnGetVersion;
    private DOnGetHelp FOnGetHelp;
    //
    //----------------------------------------------
    //	Section - Constructor
    //----------------------------------------------
    //
    public CUCVersion()
    {
      InitializeComponent();
    }
    //
    //----------------------------------------------
    //	Section - Property 
    //----------------------------------------------
    //
    public void SetOnGetVersion(DOnGetVersion value)
    {
      FOnGetVersion = value;
    }

    public void SetOnGetHelp(DOnGetHelp value)
    {
      FOnGetHelp = value;
    }
    //
    //----------------------------------------------
    //	Section - Property - Software Version
    //----------------------------------------------
    //
    private String GetSoftwareVersion()
    {
      return lblSoftwareVersion.Text;
    }

    private delegate void CBSetSoftwareVersion(String value);
    private void SetSoftwareVersion(String value)
    {
      if (this.InvokeRequired)
      {
        CBSetSoftwareVersion CB = new CBSetSoftwareVersion(SetSoftwareVersion);
        Invoke(CB, new object[] { value });
      }
      else
      {
        lblSoftwareVersion.Text = value;
      }
    }

    public String SoftwareVersion
    {
      get { return GetSoftwareVersion(); }
      set { SetSoftwareVersion(value); }
    }
    //
    //----------------------------------------------
    //	Section - Property - Hardware Version
    //----------------------------------------------
    //
    private delegate void CBSetHardwareVersion(String value);
    private void SetHardwareVersion(String value)
    {
      if (this.InvokeRequired)
      {
        CBSetHardwareVersion CB = new CBSetHardwareVersion(SetHardwareVersion);
        Invoke(CB, new object[] { value });
      }
      else
      {
        lblHardwareVersion.Text = value;
      }
    }

    private String GetHardwareVersion()
    {
      return lblHardwareVersion.Text;
    }

    public String HardwareVersion
    {
      get { return GetHardwareVersion(); }
      set { SetHardwareVersion(value); }
    }
    //
    //----------------------------------------------
    //	Section - Event
    //----------------------------------------------
    //
    private void btnGetVersion_Click(object sender, EventArgs e)
    {
      if (FOnGetVersion is DOnGetVersion)
      {
        FOnGetVersion();
      }
    }

    private void btnGetHelp_Click(object sender, EventArgs e)
    {
      if (FOnGetHelp is DOnGetHelp)
      {
        FOnGetHelp();
      }
    }




  }
}
