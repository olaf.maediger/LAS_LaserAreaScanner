#include "Defines.h"
#include "Helper.h"
#include "Led.h"
#include "Serial.h"
#include "DBBUtilities.h"
//
//
//-------------------------------------------
//	Global Variables - Hardware
//-------------------------------------------
//	
CLed LedSystem(PIN_LEDSYSTEM);
/* later:
CSerial SerialFDL1(Serial);
CSerial SerialFDL2(Serial1);
CSerial SerialFDL3(Serial2);
CSerial SerialFDL4(Serial3);
CSerial SerialPC(SerialUSB);
*/
// temporary
CSerial SerialFDL1(Serial1);
CSerial SerialFDL2(Serial2);
CSerial SerialFDL3(Serial3);
CSerial SerialFDL4(SerialUSB);
CSerial SerialPC(Serial);
//
//
//-------------------------------------------
//	Global Variables - Common
//-------------------------------------------
//	
//
void setup() 
{
  LedSystem.Open();
  SerialFDL1.Open();
  SerialFDL2.Open();
  SerialFDL3.Open();
  SerialFDL4.Open();
  SerialPC.Open();
  //
  LedSystem.Toggle();
  //
  WriteProgramHeader(SerialPC);
  WriteHelp(SerialPC);
  SerialPC.WritePrompt();
  //
  LedSystem.Toggle();
}

void loop() 
{
  HandleSerialCommands(SerialPC, SerialFDL1);
  HandleError(SerialPC);  
}
