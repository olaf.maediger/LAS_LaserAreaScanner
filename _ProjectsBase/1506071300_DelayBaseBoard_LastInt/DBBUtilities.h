#ifndef DBBUtilities_h
#define DBBUtilities_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
//
#define SIZE_PARAMETER 8
#define SIZE_TXDBUFFER 64
#define SIZE_RXDBUFFER 64

#define COUNT_PARAMETER 4
//
#define TARGET_FDL1 "FDL1"
#define TARGET_FDL2 "FDL2"
#define TARGET_FDL3 "FDL3"
#define TARGET_FDL4 "FDL4"
//
void WriteProgramHeader(CSerial &serialcommand);
void WriteHelp(CSerial &serialcommand);
//  Handler - Error
void HandleError(CSerial &serial);
//  Handler - SerialPC Communication
void HandleSerialCommands(CSerial &serialcommand, CSerial &serialtarget);
//
#endif
