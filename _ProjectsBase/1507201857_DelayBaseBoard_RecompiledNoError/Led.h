//
//--------------------------------
//  Library Led
//--------------------------------
//
#ifndef Led_h
#define Led_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define PIN_LEDSYSTEM 13
//
enum EStateLed
{
  slUndefined = 0,
  slOn = 1,
  slOff = 2
};
//
class CLed
{
  private:
  int FPin;
  EStateLed FState;
  
  public:
  CLed(int pin);
  Boolean Open();
  Boolean Close();
  Boolean On();
  Boolean Off();
  Boolean Toggle();
};
//
#endif
