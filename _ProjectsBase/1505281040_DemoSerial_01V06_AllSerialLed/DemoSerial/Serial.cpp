#include "Serial.h"

//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//

//
//----------------------------------------------------
//  Segment - CSerialFDL
//----------------------------------------------------
//

//
//----------------------------------------------------
//  Segment - CSerialPC
//----------------------------------------------------
//

//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
CSerial::CSerial(UARTClass *serial)
{
  CSerialZ *PSerialZ = (CSerialZ*)new CSerialZ(serial);
  FPSerial = (CSerialBase*)PSerialZ;
}

CSerial::CSerial(UARTClass &serial)
{
  CSerialZ *PSerialZ = (CSerialZ*)new CSerialZ(&serial);
  FPSerial = (CSerialBase*)PSerialZ;
}

CSerial::CSerial(USARTClass *serial)
{
  CSerialS *PSerialS = (CSerialS*)new CSerialS(serial);
  FPSerial = (CSerialBase*)PSerialS;
}

CSerial::CSerial(USARTClass &serial)
{
  CSerialS *PSerialS = (CSerialS*)new CSerialS(&serial);
  FPSerial = (CSerialBase*)PSerialS;
}

CSerial::CSerial(Serial_ *serial)
{ 
  CSerialU *PSerialU = (CSerialU*)new CSerialU(serial);
  FPSerial = (CSerialBase*)PSerialU;
}

CSerial::CSerial(Serial_ &serial)
{ 
  CSerialU *PSerialU = (CSerialU*)new CSerialU(&serial);
  FPSerial = (CSerialBase*)PSerialU;
}

bool CSerial::Open()
{
  return FPSerial->Open();
}

bool CSerial::Write(char *text)
{
  return FPSerial->Write(text);
}

