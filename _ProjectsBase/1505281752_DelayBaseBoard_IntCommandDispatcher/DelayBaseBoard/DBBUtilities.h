#ifndef DBBUtilities_h
#define DBBUtilities_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
//
#define SIZE_PARAMETER 8
#define SIZE_TXDBUFFER 64
#define SIZE_RXDBUFFER 64
//
void WriteProgramHeader(CSerial &serial);
void WriteSoftwareVersion(CSerial &serial);
void WriteHardwareVersion(CSerial &serial);
void WriteHelp(CSerial &serial);
//  Handler - Error
void HandleError(CSerial &serial);
//  Handler - SerialPC Communication
Boolean DetectRxdCommandLine(CSerial &serial);
Boolean AnalyseRXDCommand(CSerial &serial);
Boolean ExecuteRXDCommand(CSerial &serial);
void HandleSerialCommands(CSerial &serial);

#endif
