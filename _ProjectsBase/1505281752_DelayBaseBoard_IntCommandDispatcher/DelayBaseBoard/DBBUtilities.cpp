#include "DBBUtilities.h"
//
extern enum EErrorCode GlobalErrorCode;
//
//-------------------------------------------
//  Global Variables 
//-------------------------------------------
//	
Character TxdBuffer[SIZE_TXDBUFFER];
Character RxdBuffer[SIZE_RXDBUFFER];
Int16 RxdBufferIndex = 0;
Character RxdCommandLine[SIZE_RXDBUFFER];
PCharacter RXDCommand;
PCharacter RXDParameter[SIZE_PARAMETER];
Int16 RXDParameterCount = 0;
//
//
void ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    RxdBuffer[CI] = 0x00;
  }
  RxdBufferIndex = 0;
}

void ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    TxdBuffer[CI] = 0x00;
  }
}

void ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    RxdCommandLine[CI] = 0x00;
  }
}


void WriteProgramHeader(CSerial &serial)
{  
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
  //
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("---------------------------------------");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Project:   DBB - DelayBaseBoard     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Version:   01V01                    -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Hardware:  DBB01V01                 -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Date:      150528                   -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Time:      1555                     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Author:    OM                       -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Port:      SerialPC (SerialUSB)     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Parameter: 115200, N, 8, 1          -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("---------------------------------------");
  serial.WriteNewLine();	
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Software-Version DBB01V01");
  serial.WriteNewLine();
}

void WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Hardware-Version DBB01V02");
  serial.WriteNewLine();
}

void WriteHelp(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Help (Common):");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" H   : This Help");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GPH <fdl> : Get Program Header");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GSV <fdl> : Get Software-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GHV <fdl> : Get Hardware-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Led):");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GLD <fdl> <i> : Get State of Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" SLD <fdl> <i> : Set Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" CLD <fdl> <i> : Clear Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (ChannelA):");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GDA <fdl>         : Get Delay-Data [10ps]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" SDA <fdl> <d>     : Set Delay-Data [10ps]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  /*
  serial.WriteText(" GSA <fdl> <i>     : Get Sweep-Parameter");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" SSA <fdl> <i> <v> : Set Sweep-Parameter");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GWA <fdl>         : Get Sweep-State");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" SWA <fdl> <v>     : Set Sweep-State");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  */
  //
  serial.WriteText(" Help (ChannelB):");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GDB <fdl>         : Get Delay-Data [10ps]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" SDB <fdl> <d>     : Set Delay-Data [10ps]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  /*
  serial.WriteText(" GSB <fdl> <i>     : Get Sweep-Parameter");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" SSB <fdl> <i> <v> : Set Sweep-Parameter");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GWB <fdl>         : Get Sweep-State");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" SWB <fdl> <v>     : Set Sweep-State");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  */
  //
  serial.WriteNewLine();
}
//
//
//----------------------------------------------
//  Segment - Handler - Error
//----------------------------------------------
//
void HandleError(CSerial &serial)
{
  if (ecNoError != GlobalErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(GlobalErrorCode, TxdBuffer);	
    serial.WriteText(TxdBuffer);
    serial.WriteText("]: ");
    switch (GlobalErrorCode)
    {
      case ecNoError:
        serial.WriteText("No Error");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
  /*  case ecInvalid...:
        SERIAL_PC.write("Invalid ...");
        break; */
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    GlobalErrorCode = ecNoError;
  }
}
//
//----------------------------------------------
//  Segment - Handler - SerialPC Communication
//----------------------------------------------
//
Boolean DetectRxdCommandLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    // debug serial.WriteText(".");
    // debug serial.WriteCharacter(C);
    // debug serial.WriteText(".");
    // debug delay(1000);
    switch (C)
    {
      case CARRIAGE_RETURN:
        RxdBuffer[RxdBufferIndex] = ZERO;
        RxdBufferIndex = 0; // restart
        strupr(RxdBuffer);
        strcpy(RxdCommandLine, RxdBuffer);
        // debug serial.WriteText("<CR>true");
        // debug delay(1000);
        return true;
      case LINE_FEED: // ignore
        // debug serial.WriteText("<LF>");
        // debug delay(1000);
        break;
      default: 
        // debug serial.WriteText("<");
        // debug serial.WriteCharacter(C);
        // debug serial.WriteText(">");
        RxdBuffer[RxdBufferIndex] = C;
        RxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean AnalyseRXDCommand(CSerial &serial)
{
  if (DetectRxdCommandLine(serial))
  {
    serial.WriteText("\r\nRxdCommandLine[");
    serial.WriteText(RxdCommandLine);
    serial.WriteText("]");
    delay(5000);
    ZeroRxdBuffer();
    ZeroTxdBuffer();
    ZeroRxdCommandLine();    
    return true;
  }
  return false;
}
  /*
  if (UARTBASE_bCmdCheck())
  {	
    RXDCommand = UARTBASE_szGetParam();
    if (0 < strlen(RXDCommand))
    {
      strupr(RXDCommand);
      RXDParameterCount = 0;
      while (RXDParameter[RXDParameterCount] = UARTBASE_szGetParam())
      {
        RXDParameterCount++;
      }							
      return TRUE;
    }
    return FALSE;
    }
  return FALSE;*/

Boolean ExecuteRXDCommand(CSerial &serial)
{ /* / ----------------------------------
  // Help (Common)
  // ----------------------------------
  if (0 == RXDCommand)
  {
    UARTBASE_CmdReset();
    WritePrompt();
    return FALSE;
  }
  //
  if (!cstrcmp("H", RXDCommand))
  {
    ExecuteGetHelp();
    UARTBASE_CmdReset();
    return TRUE;
  } else 
  if (!cstrcmp("GPH", RXDCommand))
  {
    ExecuteGetProgramHeader();
    UARTBASE_CmdReset();
    return TRUE;
  } else 
  if (!cstrcmp("GSV", RXDCommand))
  {
    ExecuteGetSoftwareVersion();
    UARTBASE_CmdReset();
    return TRUE;
  }*/ /* else 
	if (!cstrcmp("GHV", RXDCommand))
	{
		ExecuteGetHardwareVersion();
		UARTBASE_CmdReset();
		return TRUE;
	} else 
	// ----------------------------------
	// Led
	// ----------------------------------
	if (!cstrcmp("GLD", RXDCommand))
	{
		ExecuteGetLed();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SLD", RXDCommand))
	{
		ExecuteSetLed();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("CLD", RXDCommand))
	{
		ExecuteClearLed();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	// ----------------------------------
	// DelayChannelA
	// ----------------------------------
	if (!cstrcmp("GDA", RXDCommand))
	{
		ExecuteGetDelayChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SDA", RXDCommand))
	{
		ExecuteSetDelayChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("GSA", RXDCommand))
	{
		ExecuteGetSweepParameterChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SSA", RXDCommand))
	{
		ExecuteSetSweepParameterChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("GWA", RXDCommand))
	{
		ExecuteGetSweepStateChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SWA", RXDCommand))
	{
		ExecuteSetSweepStateChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	// ----------------------------------
	// DelayChannelB
	// ----------------------------------
	if (!cstrcmp("GDB", RXDCommand))
	{
		ExecuteGetDelayChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SDB", RXDCommand))
	{
		ExecuteSetDelayChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("GSB", RXDCommand))
	{
		ExecuteGetSweepParameterChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SSB", RXDCommand))
	{
		ExecuteSetSweepParameterChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("GWB", RXDCommand))
	{
		ExecuteGetSweepStateChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SWB", RXDCommand))
	{
		ExecuteSetSweepStateChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	// ----------------------------------
	// Error-Handler
	// ----------------------------------
	{
		GlobalErrorCode = ecInvalidCommand;
	}
	UARTBASE_CmdReset();
	return FALSE;  
  }
*/
  return false;
}

void HandleSerialCommands(CSerial &serial)
{
  if (AnalyseRXDCommand(serial))
  {
    ExecuteRXDCommand(serial);
  }  
}


