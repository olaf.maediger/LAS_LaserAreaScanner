//
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define UInt8 byte
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Float float
#define Double double
//
#define true 1
#define false 0
//
//-------------------------------------------
//	Global Types - Error
//-------------------------------------------
enum EErrorCode
{
  ecNoError = (int)0,	
  ecUnknown = (int)1,
  ecInvalidCommand = (int)2,
};
//
#endif 

