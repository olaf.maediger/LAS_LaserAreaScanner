#include "Defines.h"
#include "Helper.h"
#include "Led.h"
#include "Serial.h"
#include "DBBUtilities.h"
//
//
//-------------------------------------------
//	Global Variables - Hardware
//-------------------------------------------
//	
CLed LedSystem(PIN_LEDSYSTEM);
/*
CSerial SerialFDL1(Serial);
CSerial SerialFDL2(Serial1);
CSerial SerialFDL3(Serial2);
CSerial SerialFDL4(Serial3);
CSerial SerialPC(SerialUSB);
*/
// temporary
CSerial SerialFDL1(Serial);
CSerial SerialFDL2(Serial1);
CSerial SerialFDL3(Serial2);
CSerial SerialFDL4(SerialUSB);
CSerial SerialPC(Serial3);
//
//
//-------------------------------------------
//	Global Variables - Common
//-------------------------------------------
//	
enum EErrorCode GlobalErrorCode = ecNoError;
//
void setup() 
{
  LedSystem.Open();
  SerialFDL1.Open();
  SerialFDL2.Open();
  SerialFDL3.Open();
  SerialFDL4.Open();
  SerialPC.Open();
  //
  LedSystem.Toggle();
  //
  delay(3000);
  WriteProgramHeader(SerialPC);
  delay(1000);
  WriteHelp(SerialPC);
  SerialPC.WritePrompt();
  //
  LedSystem.Toggle();
  // debug GlobalErrorCode = ecInvalidCommand;
}

void loop() 
{
  //
  HandleError(SerialPC);  
  HandleSerialCommands(SerialPC);
  //
  LedSystem.Toggle();
}
