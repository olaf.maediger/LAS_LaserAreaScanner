﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;
//
namespace HWDcMotorController
{
  class CGetLed : CCommand
  {
    public const String INIT_NAME = "GetLed";
		public const String COMMAND_TEXT = "GLD";

    public CGetLed(CHardwareDevice hardwaredevice,
								   Byte index)
      : base(hardwaredevice, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, index))
    {
      SetParent(this);
    }

    public override void OnSerialDataReceived(String data)
    {
      base.OnSerialDataReceived(data);
      String Line = String.Format("{0}: {1}", Name, data);
      Notifier.Write(CHWDcMotorController.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
      String[] Tokenlist = data.Split(' ');
      if (3 < Tokenlist.Length)
      {
        if ("1" == Tokenlist[3])
        {
          // !!!!!!!!!!!! Commandlist.Library.RefreshLed(Tokenlist[2], true);
        }
        else
        {
          // !!!!!!!!!!!! Commandlist.Library.RefreshLed(Tokenlist[2], false);
        }
      }
    }

  }
}
