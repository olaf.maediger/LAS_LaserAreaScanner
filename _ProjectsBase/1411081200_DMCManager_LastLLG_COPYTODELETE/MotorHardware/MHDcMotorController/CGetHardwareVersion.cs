﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;

namespace HWDcMotorController
{
	class CGetHardwareVersion : CCommand
	{
    public const String INIT_NAME = "GetHardwareVersion";
    public const String COMMAND_TEXT = "GHV";

    public CGetHardwareVersion(CHardwareDevice hardwaredevice)
      : base(hardwaredevice, INIT_NAME, COMMAND_TEXT)
    {
      SetParent(this);
    }

    public override void OnSerialDataReceived(String data)
    {
      base.OnSerialDataReceived(data);
      String Line = String.Format("{0}: {1}", Name, data);
			Notifier.Write(CHWDcMotorController.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
      String[] Tokenlist = data.Split(' ');
			if (2 < Tokenlist.Length)
			{
        //??????????????????? Commandlist.HardwareDevice.RefreshHardwareVersion(Tokenlist[2]);
			}
		}

	}
}

