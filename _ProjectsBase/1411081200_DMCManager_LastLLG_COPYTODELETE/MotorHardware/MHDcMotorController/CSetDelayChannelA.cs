﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;
//
namespace HWDcMotorController
{
  public class CSetDelayChannelA : CCommand
  {
    public const String INIT_NAME = "SetDelayChannelA";
    public const String COMMAND_TEXT = "SDA";

    public CSetDelayChannelA(CHardwareDevice hardwaredevice,
                             Int32 delay)
      : base(hardwaredevice, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, delay))
    {
      SetParent(this);
    }

    public override void OnSerialDataReceived(String data)
    {
      base.OnSerialDataReceived(data);
      String Line = String.Format("{0}: {1}", Name, data);
      Notifier.Write(CHWDcMotorController.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = data.Split(' ');
      if (2 < Tokenlist.Length)
      {
        // !!!!!!!!!!!! Commandlist.Library.RefreshDelayChannelA(Tokenlist[2]);
      }
    }

  }
}
