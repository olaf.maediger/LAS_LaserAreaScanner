﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using UCNotifier;
using Initdata;
using ComPort;
//
namespace HWDcMotorController
{
  public delegate void DOnSerialDataReceived(String data);
  public delegate void DOnSerialDataTransmitted(String data);
  public delegate void DOnCommandDataReceived(String data);


  public enum ESweepState
  {
    Idle = (int)0,
    LowHighLowHigh = (int)1,
    HighLowHighLow = (int)2,
    LowHighHighLow = (int)3,
    HighLowLowHigh = (int)4
  };

  public enum ESweepParameter
  {
    LimitLow = (int)0,
    LimitHigh = (int)1,
    StepDelta = (int)2,
    StepCount = (int)3,
    StepIndex = (int)4,
    RepetitionCount = (int)5,
    RepetitionIndex = (int)6
  };


  //
  //#######################################################################################
  //  Segment - Main - CHWDcMotorControllerBase
  //#######################################################################################
  //
  public class CHWDcMotorControllerBase : CHardwareDevice
  {
    //
    //--------------------------------------
    //	Section - Constant
    //--------------------------------------
    //
    static public String HEADER_LIBRARY = "MHDMC";
    static public String TITLE_LIBRARY = "MHDcMotorController";
    static public String SECTION_LIBRARY = TITLE_LIBRARY;
    //
    // Initdata - ComPort
    public const String SECTION_COMPORT = "ComPort";
    private const String NAME_COMPORT = "ComPort";
    // Initdata - InitValue - ComPort
    public const EComPort INIT_COMPORT = EComPort.cp1;						// selectable over Initdata
    private const EBaudrate INIT_BAUDRATE = EBaudrate.br115200;		// fixed to Hardware
    private const EDatabits INIT_DATABITS = EDatabits.db8;				// fixed to Hardware
    private const EParity INIT_PARITY = EParity.paNone;						// fixed to Hardware
    private const EStopbits INIT_STOPBITS = EStopbits.sb1;				// fixed to Hardware
    private const Int32 INIT_BUFFERSIZE = 4096;										// fixed to Hardware
    private const Boolean INIT_TRANSMITENABLED = true;						// fixed to Hardware
    private const Int32 INIT_DELAYCARRRIAGERETURN = 0;						// fixed to Hardware
    private const Int32 INIT_DELAYLINEFEED = 0;										// fixed to Hardware
    private const Int32 INIT_DELAYCHARACTER = 0;									// fixed to Hardware
    //
    // Initdata - Common
    public const String SECTION_COMMON = "Common";
    private const String NAME_DEVICETYPE = "DeviceType";
    private const String NAME_SOFTWAREVERSION = "SoftwareVersion";
    private const String NAME_HARDWAREVERSION = "HardwareVersion";
    // Initdata - InitValue - Common
    public const String INIT_DEVICETYPE = "MHDcMotorController";
    public const String INIT_SOFTWAREVERSION = "01V0103";
    public const String INIT_HARDWAREVERSION = "FDL02V00";
    //
    // Initdata - DelayChannelA
    public const String SECTION_CHANNELA = "ChannelA";
    public const String NAME_DELAYSTEPS10PS = "DelaySteps10ps";
    // Initdata - InitValue - DelayChannelA
    public const Int32 INIT_DELAYSTEPS10PS = 100;
    public const ESweepState INIT_SWEEPSTATE = ESweepState.Idle;
    public const Int32 INIT_LIMITLOW = 100;
    public const Int32 INIT_LIMITHIGH = 900;
    public const Int32 INIT_STEPDELTA = 1;
    public const Int32 INIT_STEPCOUNT = 100;
    public const Int32 INIT_STEPINDEX = 0;
    public const Int32 INIT_REPETITIONCOUNT = 0; // infinite!
    public const Int32 INIT_REPETITIONINDEX = 0; 
    //
    // Initdata - DelayChannelB
    public const String SECTION_CHANNELB = "ChannelB";
    // Initdata - InitValue - DelayChannelB
    //
    // Initdata - Led
    public const String SECTION_LED = "Led";
    private const String NAME_LEDBUSY = "LedBusy";
    private const String NAME_LEDERROR = "LedError";
    // Initdata - InitValue - Led
    private const Boolean INIT_LEDBUSY = false;
    private const Boolean INIT_LEDERROR = false;
    public const Int32 INIT_LEDINDEX_BUSY = 3;
    public const Int32 INIT_LEDINDEX_ERROR = 7;
    //
    public const Int32 INIT_LEDINDEX = 0;
    public const Boolean INIT_LEDACTIVE = false;
    //
    private static readonly String[] ERRORS = 
    { 
      "None",
      "Access failed",
      "Port failed",
      "Open failed",
      "Close failed",
      "Read failed",
      "Write failed",
  	  "Invalid"
    };
    //
    //--------------------------------------
    //	Section - Type
    //--------------------------------------
    //
    protected enum EErrorCode
    {
      None = 0,
      AccessFailed,
      PortFailed,
      OpenFailed,
      CloseFailed,
      ReadFailed,
      WriteFailed,
      Invalid
    };
    //
    //--------------------------------------
    //	Section - Member
    //--------------------------------------
    //
    private DOnSerialDataReceived FOnSerialDataReceived;
    private DOnSerialDataTransmitted FOnSerialDataTransmitted;
    // NC private DOnCommandDataReceived FOnCommandDataReceived;
    private RConfigurationData FData;
    private Random FRandom;
    //
    //--------------------------------------
    //	Section - Constructor
    //--------------------------------------
    //
    public CHWDcMotorControllerBase()
      : base()
    {	// DecimalPoint 
      CInitdata.Init();
      //
      FOnSerialDataReceived = null;
      FOnSerialDataTransmitted = null;
      // NC FOnCommandDataReceived = null;
      FData = new RConfigurationData(0);
      FRandom = new Random((Int32)DateTime.Now.Ticks);
    }
    //
    //--------------------------------------
    //	Section - Property - Common
    //--------------------------------------
    //  Property - SoftwareVersion
    //
    private void SetSoftwareVersion(String value)
    {
      FData.CommonData.SoftwareVersion = value;
      if (FData.CommonData.OnCommonDataChanged is DOnCommonDataChanged)
      {
        FData.CommonData.OnCommonDataChanged(FData.CommonData);
      }
    }
    private String SoftwareVersion
    {
      get { return FData.CommonData.SoftwareVersion; }
      set { SetSoftwareVersion(value); }
    }
    //
    //  Property - HardwareVersion
    //
    private void SetHardwareVersion(String value)
    {
      FData.CommonData.HardwareVersion = value;
      if (FData.CommonData.OnCommonDataChanged is DOnCommonDataChanged)
      {
        FData.CommonData.OnCommonDataChanged(FData.CommonData);
      }
    }
    private String HardwareVersion
    {
      get { return FData.CommonData.HardwareVersion; }
      set { SetHardwareVersion(value); }
    }
    //  Property - DeviceType
    private void SetDeviceType(String value)
    {
      FData.CommonData.DeviceType = value;
      if (FData.CommonData.OnCommonDataChanged is DOnCommonDataChanged)
      {
        FData.CommonData.OnCommonDataChanged(FData.CommonData);
      }
    }
    private String DeviceType
    {
      get { return FData.CommonData.DeviceType; }
      set { SetDeviceType(value); }
    }
    //
    //--------------------------------------
    //	Section - Property - DelayChannelA
    //--------------------------------------
    //
    private void SetDelayChannelA(Int32 value)
    {
      FData.ChannelDataA.Delay = value;
      if (FData.ChannelDataA.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataA.OnChannelDataChanged(FData.ChannelDataA);
      }
    }
    private Int32 DelayChannelA
    {
      get { return FData.ChannelDataA.Delay; }
      set { SetDelayChannelA(value); }
    }
    //
    //--------------------------------------
    //	Section - Property - DelayChannelB
    //--------------------------------------
    //
    private void SetDelayChannelB(Int32 value)
    {
      FData.ChannelDataB.Delay = value;
      if (FData.ChannelDataB.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataB.OnChannelDataChanged(FData.ChannelDataB);
      }
    }
    private Int32 DelayChannelB
    {
      get { return FData.ChannelDataB.Delay; }
      set { SetDelayChannelB(value); }
    }
    //
    //--------------------------------------
    //	Section - Property - Led
    //--------------------------------------
    //
    private void SetLed(Int32 index, Boolean value)
    {
      switch (index)
      {
        case INIT_LEDINDEX_BUSY:
          FData.LedData3.Index = INIT_LEDINDEX_BUSY;
          FData.LedData3.Active = value;
          if (FData.LedData3.OnLedDataChanged is DOnLedDataChanged)
          {
            FData.LedData3.OnLedDataChanged(FData.LedData3);
          }
          break;
        case INIT_LEDINDEX_ERROR:
          FData.LedData7.Index = INIT_LEDINDEX_ERROR;
          FData.LedData7.Active = value;
          if (FData.LedData7.OnLedDataChanged is DOnLedDataChanged)
          {
            FData.LedData7.OnLedDataChanged(FData.LedData7);
          }
          break;
      }
    }
    private Boolean Led3
    {
      get { return FData.LedData3.Active; }
      set { SetLed(INIT_LEDINDEX_BUSY, value); }
    }
    private Boolean Led7
    {
      get { return FData.LedData7.Active; }
      set { SetLed(INIT_LEDINDEX_ERROR, value); }
    }
    //
    //----------------------------------------------
    //	Section - Property - SweepState - ChannelA
    //----------------------------------------------
    //
    private void SetSweepStateChannelA(ESweepState value)
    {
      FData.ChannelDataA.SweepState = value;
      if (FData.ChannelDataA.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataA.OnChannelDataChanged(FData.ChannelDataA);
      }
    }
    private ESweepState SweepStateChannelA
    {
      get { return FData.ChannelDataA.SweepState; }
      set { SetSweepStateChannelA(value); }
    }    
    //
    //-----------------------------------------------
    //	Section - Property - SweepParameterChannelA
    //-----------------------------------------------
    private Int32 GetSweepParameterChannelA(Int32 index)
    {
      switch (index)
      {
        case (int)ESweepParameter.LimitLow:
          return FData.ChannelDataA.LimitLow;
        case (int)ESweepParameter.LimitHigh:
          return FData.ChannelDataA.LimitHigh;
        case (int)ESweepParameter.StepDelta:
          return FData.ChannelDataA.StepDelta;
        case (int)ESweepParameter.StepCount:
          return FData.ChannelDataA.StepCount;
        case (int)ESweepParameter.StepIndex:
          return FData.ChannelDataA.StepIndex;
        case (int)ESweepParameter.RepetitionCount:
          return FData.ChannelDataA.RepetitionCount;
        case (int)ESweepParameter.RepetitionIndex:
          return FData.ChannelDataA.RepetitionIndex;
        default:
          return 0;
      }
    }
    private void SetSweepParameterChannelA(Int32 index, Int32 value)
    {
      switch (index)
      {
        case (int)ESweepParameter.LimitLow:
          FData.ChannelDataA.LimitLow = value;
          break;
        case (int)ESweepParameter.LimitHigh:
          FData.ChannelDataA.LimitHigh = value;
          break;
        case (int)ESweepParameter.StepDelta:
          FData.ChannelDataA.StepDelta = value;
          break;
        case (int)ESweepParameter.StepCount:
          FData.ChannelDataA.StepCount = value;
          break;
        case (int)ESweepParameter.StepIndex:
          FData.ChannelDataA.StepIndex = value;
          break;
        case (int)ESweepParameter.RepetitionCount:
          FData.ChannelDataA.RepetitionCount = value;
          break;
        case (int)ESweepParameter.RepetitionIndex:
          FData.ChannelDataA.RepetitionIndex = value;
          break;
      }
      if (FData.ChannelDataA.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataA.OnChannelDataChanged(FData.ChannelDataA);
      }
    }

    private Int32 SweepLimitLowA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.LimitLow); }
      set { SetSweepParameterChannelA((int)ESweepParameter.LimitLow, value); }
    }
    private Int32 SweepLimitHighA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.LimitHigh); }
      set { SetSweepParameterChannelA((int)ESweepParameter.LimitHigh, value); }
    }
    private Int32 SweepStepDeltaA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.StepDelta); }
      set { SetSweepParameterChannelA((int)ESweepParameter.StepDelta, value); }
    }
    private Int32 SweepStepCountA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.StepCount); }
      set { SetSweepParameterChannelA((int)ESweepParameter.StepCount, value); }
    }
    private Int32 SweepStepIndexA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.StepIndex); }
      set { SetSweepParameterChannelA((int)ESweepParameter.StepIndex, value); }
    }
    private Int32 SweepRepetitionCountA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.RepetitionCount); }
      set { SetSweepParameterChannelA((int)ESweepParameter.RepetitionCount, value); }
    }
    private Int32 SweepRepetitionIndexA
    {
      get { return GetSweepParameterChannelA((int)ESweepParameter.RepetitionIndex); }
      set { SetSweepParameterChannelA((int)ESweepParameter.RepetitionIndex, value); }
    }
    //
    //----------------------------------------------
    //	Section - Property - SweepState - ChannelB
    //----------------------------------------------
    //
    private void SetSweepStateChannelB(ESweepState value)
    {
      FData.ChannelDataB.SweepState = value;
      if (FData.ChannelDataB.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataB.OnChannelDataChanged(FData.ChannelDataB);
      }
    }
    private ESweepState SweepStateChannelB
    {
      get { return FData.ChannelDataB.SweepState; }
      set { SetSweepStateChannelB(value); }
    }
    //
    //-----------------------------------------------
    //	Section - Property - SweepParameterChannelB
    //-----------------------------------------------
    private Int32 GetSweepParameterChannelB(Int32 index)
    {
      switch (index)
      {
        case (int)ESweepParameter.LimitLow:
          return FData.ChannelDataB.LimitLow;
        case (int)ESweepParameter.LimitHigh:
          return FData.ChannelDataB.LimitHigh;
        case (int)ESweepParameter.StepDelta:
          return FData.ChannelDataB.StepDelta;
        case (int)ESweepParameter.StepCount:
          return FData.ChannelDataB.StepCount;
        case (int)ESweepParameter.StepIndex:
          return FData.ChannelDataB.StepIndex;
        case (int)ESweepParameter.RepetitionCount:
          return FData.ChannelDataB.RepetitionCount;
        case (int)ESweepParameter.RepetitionIndex:
          return FData.ChannelDataB.RepetitionIndex;
        default:
          return 0;
      }
    }
    private void SetSweepParameterChannelB(Int32 index, Int32 value)
    {
      switch (index)
      {
        case (int)ESweepParameter.LimitLow:
          FData.ChannelDataB.LimitLow = value;
          break;
        case (int)ESweepParameter.LimitHigh:
          FData.ChannelDataB.LimitHigh = value;
          break;
        case (int)ESweepParameter.StepDelta:
          FData.ChannelDataB.StepDelta = value;
          break;
        case (int)ESweepParameter.StepCount:
          FData.ChannelDataB.StepCount = value;
          break;
        case (int)ESweepParameter.StepIndex:
          FData.ChannelDataB.StepIndex = value;
          break;
        case (int)ESweepParameter.RepetitionCount:
          FData.ChannelDataB.RepetitionCount = value;
          break;
        case (int)ESweepParameter.RepetitionIndex:
          FData.ChannelDataB.RepetitionIndex = value;
          break;
      }
      if (FData.ChannelDataB.OnChannelDataChanged is DOnChannelDataChanged)
      {
        FData.ChannelDataB.OnChannelDataChanged(FData.ChannelDataB);
      }
    }

    private Int32 SweepLimitLowB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.LimitLow); }
      set { SetSweepParameterChannelB((int)ESweepParameter.LimitLow, value); }
    }
    private Int32 SweepLimitHighB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.LimitHigh); }
      set { SetSweepParameterChannelB((int)ESweepParameter.LimitHigh, value); }
    }
    private Int32 SweepStepDeltaB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.StepDelta); }
      set { SetSweepParameterChannelB((int)ESweepParameter.StepDelta, value); }
    }
    private Int32 SweepStepCountB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.StepCount); }
      set { SetSweepParameterChannelB((int)ESweepParameter.StepCount, value); }
    }
    private Int32 SweepStepIndexB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.StepIndex); }
      set { SetSweepParameterChannelB((int)ESweepParameter.StepIndex, value); }
    }
    private Int32 SweepRepetitionCountB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.RepetitionCount); }
      set { SetSweepParameterChannelB((int)ESweepParameter.RepetitionCount, value); }
    }
    private Int32 SweepRepetitionIndexB
    {
      get { return GetSweepParameterChannelB((int)ESweepParameter.RepetitionIndex); }
      set { SetSweepParameterChannelB((int)ESweepParameter.RepetitionIndex, value); }
    }
    //
    //########################################################################
		//
		//--------------------------------------
		//	Section - Properties
		//--------------------------------------
		//
		//

    //
		//--------------------------------------
		//	Section - Get/SetData
		//--------------------------------------
		//
    protected Boolean GetConfigurationData(ref RConfigurationData data)
    {
      RComPortData ComPortData;
      Boolean Result = ComPort.GetComPortData(out ComPortData);
      FData.ComPortData = ComPortData;
      data = FData;
      return Result;
    }
    protected Boolean SetConfigurationData(RConfigurationData data)
    {
      FData.ComPortData = data.ComPortData;
      Boolean Result = ComPort.SetComPortData(FData.ComPortData);
      FData.OnComPortDataChanged = data.OnComPortDataChanged;
      FData.CommonData = data.CommonData;
      FData.ChannelDataA = data.ChannelDataA;
      FData.ChannelDataB = data.ChannelDataB;
      FData.LedData3 = data.LedData3;
      FData.LedData7 = data.LedData7;
      return Result;
    }

		//
		//--------------------------------------
		//	Section - Helper
		//--------------------------------------
		//

		//
		//--------------------------------------
		//	Section - Error
		//--------------------------------------
		//

		//
		//--------------------------------------------------
		//	Messages
		//--------------------------------------------------
		//
		protected void _Error(EErrorCode code)
		{
			if (Notifier is CNotifier)
			{
				Int32 Index = (Int32)code;
				if ((Index < 0) && (ERRORS.Length <= Index))
				{
					Index = ERRORS.Length - 1;
				}
				String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
        Notifier.Write(CHWDcMotorController.HEADER_LIBRARY, Line);
			}
		}

		protected void _Error(Int32 code,
													String text)
		{
			if (Notifier is CNotifier)
			{
        Notifier.Error(CHWDcMotorController.HEADER_LIBRARY, code, text);
			}
		}

		protected void _Protocol(String line)
		{
			if (Notifier is CNotifier)
			{
        Notifier.Write(CHWDcMotorController.HEADER_LIBRARY, line);
			}
		}
		//
		//--------------------------------------
		//	Section - Management
		//--------------------------------------
		//
    protected Boolean OpenComPort(EComPort comport)
		{
      FlushCommandlist();
      ComPort.GetComPortData(out FData.ComPortData);
      FData.ComPortData.Name = INIT_DEVICETYPE;
      FData.ComPortData.ComPort = comport;
      FData.ComPortData.Baudrate = INIT_BAUDRATE;
      FData.ComPortData.Databits = INIT_DATABITS;
      FData.ComPortData.Parity = INIT_PARITY;
      FData.ComPortData.Stopbits = INIT_STOPBITS;
      FData.ComPortData.RXDBufferSize = INIT_BUFFERSIZE;
      FData.ComPortData.TXDBufferSize = INIT_BUFFERSIZE;
      FData.ComPortData.TransmitEnabled = INIT_TRANSMITENABLED;
      FData.ComPortData.TXDDelayCarriageReturn = INIT_DELAYCARRRIAGERETURN;
      FData.ComPortData.TXDDelayLineFeed = INIT_DELAYLINEFEED;
      FData.ComPortData.TXDDelayCharacter = INIT_DELAYCHARACTER;
      FData.ComPortData.OnDataReceived = null;//?????????????? SerialOnDataReceived;
      FData.ComPortData.OnLineReceived = null;//SerialOnLineReceived;
      FData.ComPortData.OnDataTransmitted = null;// SerialOnDataTransmitted;
      FData.ComPortData.OnComPortDataChanged = SerialOnComPortDataChanged;
      Boolean Result = ComPort.Open(FData.ComPortData);
			if (Result)
			{
        String ComPortName = CComPort.ComPortName(FData.ComPortData.ComPort);
				String Line = String.Format("Serial port {0} correct opened", ComPortName);
				_Protocol(Line);
			} else
			{
				_Error(EErrorCode.OpenFailed);
			}
      // Callback-Actualization
      SerialOnComPortDataChanged(FData.ComPortData.ID, FData.ComPortData);
      //
			return Result;
		}

    protected Boolean CloseComPort()
		{
      FlushCommandlist();
      Boolean Result = ComPort.Close();
			if (Result)
			{
				RComPortData ComPortData = new RComPortData();
				ComPort.GetComPortData(out ComPortData);
				String ComPortName = CComPort.ComPortName(ComPortData.ComPort);						
				String Line = String.Format("Serial port {0} correct closed", ComPortName);
				_Protocol(Line);
			} else
			{
				_Error(EErrorCode.CloseFailed);
			}
      // Callback-Actualization
      if (FData.ComPortData.OnComPortDataChanged is DOnComPortDataChanged)
      {
        RComPortData ComPortData = new RComPortData();
        ComPort.GetComPortData(out ComPortData);
        FData.ComPortData.OnComPortDataChanged(ComPortData.ID, ComPortData);
      }
      //
      return Result;
		}

		private Boolean MatchError(String line,
															 ref Int32 errorcode,
															 ref String errortext)
		{
			Regex RE = new Regex("(# Error)(\\[){1,1}([0-9]){1,}(\\]: ){1,1}([^!]{1,})");
			Match M = RE.Match(line);
			if (M.Success)
			{
				if (5 < M.Groups.Count)
				{
					errorcode = Int32.Parse(M.Groups[3].Value);
					errortext = M.Groups[5].Value;
					return true;
				}
			}
			return false;
		}

    /*???public void SetOnLineReceived(DOnLineReceived value)
    {
      FOnLineReceived = value;
    }*/

    public void SetOnSerialDataReceived(DOnSerialDataReceived value)
    {
      FOnSerialDataReceived = value;
    }

    public void SetOnSerialDataTransmitted(DOnSerialDataTransmitted value)
    {
      FOnSerialDataTransmitted = value;
    }

    private void OnSerialDataTransmitted(String data)
    {
      if (FOnSerialDataTransmitted is DOnSerialDataTransmitted)
      {
        FOnSerialDataTransmitted(data);
      }
    }

    private void SerialOnComPortDataChanged(Guid comportid, RComPortData comportdata)
    {
      FData.ComPortData = comportdata;
      if (FData.ComPortData.IsOpen)
      {
        // !!!!!!!!!!!!!!!!!!!!!!! CommandSetLed(CHWDcMotorController.INIT_LEDINDEX_BUSY);
        // !!!!!!!!!!!!!!!!!!!!!!! CommandClearLed(CHWDcMotorController.INIT_LEDINDEX_BUSY);
      }
      if (FData.OnComPortDataChanged is DOnComPortDataChanged)
      {
        FData.OnComPortDataChanged(comportdata.ID, FData.ComPortData);
      }
    }

    /*????????????????????????private void SerialOnDataReceived(Guid comportid, String data)
    {
      // Unfiltered to Parent (UCFDL-Terminal)
      if (FOnSerialDataReceived is DOnSerialDataReceived)
      {
        // Guid ComPortID = Guid.NewGuid();
        // FOnRXDLineReceived(ComPortID, line);
        FOnSerialDataReceived(data);
      }
      //
			// Error-Catching : <#><Error><[><code><]><:><!>
			// Debug Console.WriteLine(line);
			Int32 ErrorCode = 0;
			String ErrorText = "";
			if (MatchError(data, ref ErrorCode, ref ErrorText))
			{
				_Error(ErrorCode, ErrorText);
				return;
			}
      //
      // All other: Callback to Child -> Commandlist -> Command
      //
      if (FOnCommandDataReceived is DOnCommandDataReceived)
      {
        FOnCommandDataReceived(data);
      }
    }*/

    protected Boolean IsComPortOpen()
		{
			return ComPort.IsOpen();
		}

    protected Boolean IsComPortClosed()
		{
			return ComPort.IsClosed();
		}
    //
    //--------------------------------------
    //	Section - Iniitdata
    //--------------------------------------
    //
		protected Boolean LoadInitdata(CInitdataReader initdata)
		{
      Boolean Result = initdata.OpenSection(CHWDcMotorController.SECTION_LIBRARY);
      //
      String SValue;
      //         ..... Boolean BValue;
      //         ..... Int32 IValue;
      //
      //  Common
      Result &= initdata.OpenSection(CHWDcMotorController.SECTION_COMMON);
      SValue = INIT_DEVICETYPE;
      Result &= initdata.ReadString(NAME_DEVICETYPE, out SValue, SValue);
      //RO
      Result &= initdata.CloseSection();
      //
      //  ComPort
      Result &= initdata.OpenSection(CHWDcMotorController.SECTION_COMPORT);
      //
      SValue = CComPort.ComPortName(INIT_COMPORT);
      Result &= initdata.ReadEnumeration(NAME_COMPORT, out SValue, SValue);
      FData.ComPortData.ComPort = CComPort.ComPortEnumerator(SValue);
      //
      Result &= initdata.CloseSection();
      /*/ -> Above
      //  DelayChannelA
      Result &= initdata.OpenSection(CHWDcMotorController.SECTION_CHANNELA);
      //
      IValue = CHWDcMotorController.INIT_DELAYSTEPS10PS;
      Result &= initdata.ReadInt32(CHWDcMotorController.NAME_DELAYSTEPS10PS, out IValue, IValue);
      FData.ChannelDataA.Delay = IValue;
      //
      Result &= initdata.CloseSection();
      //
      //  DelayChannelB
      Result &= initdata.OpenSection(CHWDcMotorController.SECTION_CHANNELB);
      //
      IValue = CHWDcMotorController.INIT_DELAYSTEPS10PS;
      Result &= initdata.ReadInt32(CHWDcMotorController.NAME_DELAYSTEPS10PS, out IValue, IValue);
      FData.ChannelDataB.Delay = IValue;
      //
      Result &= initdata.CloseSection();
      //
      //  Led
      Result &= initdata.OpenSection(CHWDcMotorController.SECTION_LED);
      //  Led - Busy
      BValue = CHWDcMotorController.INIT_LEDBUSY;
      Result &= initdata.ReadBoolean(CHWDcMotorController.NAME_LEDBUSY, out BValue, BValue);
      FData.LedData3.Active = BValue;
      //  Led - Error
      BValue = CHWDcMotorController.INIT_LEDERROR;
      Result &= initdata.ReadBoolean(CHWDcMotorController.NAME_LEDERROR, out BValue, BValue);
      FData.LedData7.Active = BValue;
      //
      Result &= initdata.CloseSection();
      /*/
      //
      // Refresh all Data:
      //  ComPort
      Result &= OpenComPort(FData.ComPortData.ComPort);
      /*/  DelayChannelA
      //         ..... CommandSetDelayChannelA(FData.ChannelDataA.Delay);
      //  DelayChannelB
      //         ..... CommandSetDelayChannelB(FData.ChannelDataB.Delay);
      //  Led - Busy
      if (FData.LedData3.Active)
      {
        CommandSetLed(INIT_LEDINDEX_BUSY);
      }
      else
      {
        CommandClearLed(INIT_LEDINDEX_BUSY);
      }
      //  Led - Error
      if (FData.LedData7.Active)
      {
        CommandSetLed(INIT_LEDINDEX_ERROR);
      }
      else
      {
        CommandClearLed(INIT_LEDINDEX_ERROR);
      } */     
      //
      Result &= initdata.CloseSection();
      return Result;
		}



		protected Boolean SaveInitdata(CInitdataWriter initdata)
		{
      initdata.CreateSection(CHWDcMotorController.SECTION_LIBRARY);
      //
      //  Common
      initdata.CreateSection(CHWDcMotorController.SECTION_COMMON);
      //RO
      FData.CommonData.DeviceType = INIT_DEVICETYPE;
      initdata.WriteString(NAME_DEVICETYPE, FData.CommonData.DeviceType);
      initdata.CloseSection();
      //
      //  ComPort
      initdata.CreateSection(CHWDcMotorController.SECTION_COMPORT);
      initdata.WriteEnumeration(NAME_COMPORT, CComPort.ComPortName(FData.ComPortData.ComPort));
      initdata.CloseSection();
      //
      /*/  DelayChannelA
      initdata.CreateSection(CHWDcMotorController.SECTION_CHANNELA);
      initdata.WriteInt32(CHWDcMotorController.NAME_DELAYSTEPS10PS, FData.ChannelDataA.Delay);
      initdata.CloseSection();
      //
      //  DelayChannelB
      initdata.CreateSection(CHWDcMotorController.SECTION_CHANNELB);
      initdata.WriteInt32(CHWDcMotorController.NAME_DELAYSTEPS10PS, FData.ChannelDataB.Delay);
      initdata.CloseSection();
      //
      //  Led
      initdata.CreateSection(CHWDcMotorController.SECTION_LED);
      //  Led - Busy
      initdata.WriteBoolean(CHWDcMotorController.NAME_LEDBUSY, FData.LedData3.Active);
      //  Led - Error
      initdata.WriteBoolean(CHWDcMotorController.NAME_LEDERROR, FData.LedData7.Active);
      initdata.CloseSection();
      /*/      
      initdata.CloseSection();
      return true;
		}
    //
    //-----------------------------------------------
    //	Section - Helper
    //-----------------------------------------------
    //
    private void FlushCommandlist()
    {
      Commandlist.Abort();
    }


    //
    //-----------------------------------------------
    //	Section - Command - Refresh - Common
    //-----------------------------------------------
    //
    public void RefreshSoftwareVersion(String value)
		{
      SetSoftwareVersion(value);
		}

		public void RefreshHardwareVersion(String value)
		{
      SetHardwareVersion(value);
		}

    public void RefreshDeviceType(String value)
    {
      SetDeviceType(value);
    }

    public void RefreshHelp(String line)//String[] helplist)
    {
      // actual: nothing to do!
      // later!!!!!!!!!!!!!!!!!!!!!!!!
      //!!!!!!!!!!!!!!!!!!FUCConfiguration.SetHelplist(helplist);
    }
    //
    //-----------------------------------------------
    //	Section - Command - Refresh - DeviceState
    //-----------------------------------------------
    /*/
    public void RefreshDeviceState(String value)
    {      
      switch (value)
      {
        case CDEVICESTATES.Idle:
          SetDeviceState(EDeviceState.Idle);
          break;
        case CDEVICESTATES.ADCRead:
          SetDeviceState(EDeviceState.ADCRead);
          break;
        case CDEVICESTATES.ADCWrite:
          SetDeviceState(EDeviceState.ADCWrite);
          break;
        default:
          SetDeviceState(EDeviceState.Undefined);
          break;
      }
    }
     */
    //
    //--------------------------------------------------
    //	Section - Command - Refresh - Delay - ChannelA
    //--------------------------------------------------
    //
    public void RefreshDelayChannelA(String value)
    {
      try
      {
        Int32 Value = Int32.Parse(value);
        DelayChannelA = Value;
      }
      catch (Exception)
      {
      }
    }
    //
    //--------------------------------------------------
    //	Section - Command - Refresh - Delay - ChannelB
    //--------------------------------------------------
    //
    public void RefreshDelayChannelB(String value)
    {
      try
      {
        Int32 Value = Int32.Parse(value);
        DelayChannelB = Value;
      }
      catch (Exception)
      {
      }
    }
    //
    //-----------------------------------------------
    //	Section - Command - Refresh - Led
    //-----------------------------------------------
    //
    public void RefreshLed(String index, Boolean value)
    {
      try
      {
        Int32 Index = Int32.Parse(index);
        switch (Index)
        {
          case 3:
            Led3 = value;
            break;
          case 7:
            Led7 = value;
            break;
        }
      }
      catch (Exception)
      {
      }
    }
    //
    //-----------------------------------------------------------
    //	Section - Command - Refresh - SweepState - ChannelA
    //-----------------------------------------------------------
    //
    public void RefreshSweepStateChannelA(ESweepState value)
    {
      try
      {
        SweepStateChannelA = value;
      }
      catch (Exception)
      {
      }
    }
    //
    //-----------------------------------------------------------
    //	Section - Command - Refresh - SweepParameter - ChannelA
    //-----------------------------------------------------------
    //
    public void RefreshSweepParameterChannelA(Int32 index, Int32 value)
    {
      try
      {
        switch (index)
        {
          case (Int32)ESweepParameter.LimitLow:
            SweepLimitLowA = value;
            break;
          case (Int32)ESweepParameter.LimitHigh:
            SweepLimitHighA = value;
            break;
          case (Int32)ESweepParameter.StepDelta:
            SweepStepDeltaA = value;
            break;
          case (Int32)ESweepParameter.StepCount:
            SweepStepCountA = value;
            break;
          case (Int32)ESweepParameter.StepIndex:
            SweepStepIndexA = value;
            break;
          case (Int32)ESweepParameter.RepetitionCount:
            SweepRepetitionCountA = value;
            break;
          case (Int32)ESweepParameter.RepetitionIndex:
            SweepRepetitionIndexA = value;
            break;
        }
      }
      catch (Exception)
      {
      }
    }
    //
    //-----------------------------------------------------------
    //	Section - Command - Refresh - SweepState - ChannelB
    //-----------------------------------------------------------
    //
    public void RefreshSweepStateChannelB(ESweepState value)
    {
      try
      {
        SweepStateChannelB = value;
      }
      catch (Exception)
      {
      }
    }
    //
    //-----------------------------------------------------------
    //	Section - Command - Refresh - SweepParameter - ChannelB
    //-----------------------------------------------------------
    //
    public void RefreshSweepParameterChannelB(Int32 index, Int32 value)
    {
      try
      {
        switch (index)
        {
          case (Int32)ESweepParameter.LimitLow:
            SweepLimitLowB = value;
            break;
          case (Int32)ESweepParameter.LimitHigh:
            SweepLimitHighB = value;
            break;
          case (Int32)ESweepParameter.StepDelta:
            SweepStepDeltaB = value;
            break;
          case (Int32)ESweepParameter.StepCount:
            SweepStepCountB = value;
            break;
          case (Int32)ESweepParameter.StepIndex:
            SweepStepIndexB = value;
            break;
          case (Int32)ESweepParameter.RepetitionCount:
            SweepRepetitionCountB = value;
            break;
          case (Int32)ESweepParameter.RepetitionIndex:
            SweepRepetitionIndexB = value;
            break;
        }
      }
      catch (Exception)
      {
      }
    }
    //
    //--------------------------------------
    //	Section - Command - Common
    //--------------------------------------
    //
    protected Boolean CommandGetHelp()
    {
      CGetHelp Command = new CGetHelp(this);
      Commandlist.Add(Command);
      return true;
    }

    protected Boolean CommandGetSoftwareVersion()
		{
      CGetSoftwareVersion Command = new CGetSoftwareVersion(this);
			Commandlist.Add(Command);
			return true;
		}

    protected Boolean CommandGetHardwareVersion()
		{
      CGetHardwareVersion Command = new CGetHardwareVersion(this);
			Commandlist.Add(Command);
			return true;
		}

    /*
    protected Boolean CommandGetDeviceType()
    {
      CGetDeviceType Command = new CGetDeviceType(FCommandlist, FNotifier,
                                                FDevice, FComPort);
      FCommandlist.Add(Command);
      return true;
    }*/
    //
    //--------------------------------------
    //	Section - Command - DelayChannelA
    //--------------------------------------
    //
    protected Boolean CommandGetDelayChannelA()
    {
      CGetDelayChannelA Command = new CGetDelayChannelA(this);
      Commandlist.Add(Command);
      return true;
    }

    protected Boolean CommandSetDelayChannelA(Int32 delay)
    {
      CSetDelayChannelA Command = new CSetDelayChannelA(this, delay);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Command - DelayChannelB
    //--------------------------------------
    //
    protected Boolean CommandGetDelayChannelB()
    {
      CGetDelayChannelB Command = new CGetDelayChannelB(this);
      Commandlist.Add(Command);
      return true;
    }

    protected Boolean CommandSetDelayChannelB(Int32 delay)
    {
      CSetDelayChannelB Command = new CSetDelayChannelB(this, delay);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Command - Led
    //--------------------------------------
    //
    protected Boolean CommandGetLed(Byte index)
    {
      CGetLed Command = new CGetLed(this, index);
      Commandlist.Add(Command);
      return true;
    }

    protected Boolean CommandClearLed(Byte index)
    {
      CClearLed Command = new CClearLed(this, index);
      Commandlist.Add(Command);
      return true;
    }

    protected Boolean CommandSetLed(Byte index)
    {
      CSetLed Command = new CSetLed(this, index);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Command - SSA
    //--------------------------------------
    //
    protected Boolean CommandSetSweepParameterChannelA(Int32 index, Int32 value)
    {
      CSetSweepParameterChannelA Command = new CSetSweepParameterChannelA(this, index, value);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Command - GSA
    //--------------------------------------
    //
    protected Boolean CommandGetSweepParameterChannelA(Int32 index)
    {
      CGetSweepParameterChannelA Command = new CGetSweepParameterChannelA(this, index);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Command - GWA
    //--------------------------------------
    //
    protected Boolean CommandGetSweepStateChannelA()
    {
      CGetSweepStateChannelA Command = new CGetSweepStateChannelA(this);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Command - SWA
    //--------------------------------------
    //
    protected Boolean CommandSetSweepStateChannelA(ESweepState value)
    {
      CSetSweepStateChannelA Command = new CSetSweepStateChannelA(this, value);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Command - SSB
    //--------------------------------------
    //
    protected Boolean CommandSetSweepParameterChannelB(Int32 index, Int32 value)
    {
      CSetSweepParameterChannelB Command = new CSetSweepParameterChannelB(this, index, value);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Command - GSB
    //--------------------------------------
    //
    protected Boolean CommandGetSweepParameterChannelB(Int32 index)
    {
      CGetSweepParameterChannelB Command = new CGetSweepParameterChannelB(this, index);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Command - GWB
    //--------------------------------------
    //
    protected Boolean CommandGetSweepStateChannelB()
    {
      CGetSweepStateChannelB Command = new CGetSweepStateChannelB(this);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Command - SWB
    //--------------------------------------
    //
    protected Boolean CommandSetSweepStateChannelB(ESweepState value)
    {
      CSetSweepStateChannelB Command = new CSetSweepStateChannelB(this, value);
      Commandlist.Add(Command);
      return true;
    }
    //
    //--------------------------------------
    //	Section - Analyse
    //--------------------------------------
    //
    private Boolean AnalyseSendCommand(String line)
    {
      String[] Tokens = line.Split(' ');
      if (0 < Tokens.Length)
      {
        Int32 IValue;
        Int16 WIndex;
        Int16 WValue;
        Byte BIndex;
        String CommandToken = Tokens[0].ToUpper();
        switch (CommandToken)
        { // Help
          case CGetHelp.COMMAND_TEXT:
            CGetHelp CH = new CGetHelp(this);
            Commandlist.Add(CH);
            return true;
          // Delay Channel A
          case CSetDelayChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              IValue = Int32.Parse(Tokens[1]);
              CSetDelayChannelA Command = new CSetDelayChannelA(this, IValue);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CGetDelayChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              IValue = Byte.Parse(Tokens[1]);
              CGetDelayChannelA Command = new CGetDelayChannelA(this);
              Commandlist.Add(Command);
              return true;
            }
            break;
          // Delay Channel B
          case CSetDelayChannelB.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              IValue = Int32.Parse(Tokens[1]);
              CSetDelayChannelB Command = new CSetDelayChannelB(this, IValue);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CGetDelayChannelB.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              IValue = Byte.Parse(Tokens[1]);
              CGetDelayChannelB Command = new CGetDelayChannelB(this);
              Commandlist.Add(Command);
              return true;
            }
            break;
          // Led
          case CGetLed.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              BIndex = Byte.Parse(Tokens[1]);
              CGetLed Command = new CGetLed(this, BIndex);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CSetLed.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              BIndex = Byte.Parse(Tokens[1]);
              CSetLed Command = new CSetLed(this, BIndex);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CClearLed.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              BIndex = Byte.Parse(Tokens[1]);
              CClearLed Command = new CClearLed(this, BIndex);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CGetSweepStateChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              CGetSweepStateChannelA Command = new CGetSweepStateChannelA(this);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CSetSweepStateChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              ESweepState SS = (ESweepState)Int16.Parse(Tokens[1]);
              CSetSweepStateChannelA Command = new CSetSweepStateChannelA(this, SS);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CGetSweepParameterChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              WIndex = Int16.Parse(Tokens[1]);
              CGetSweepParameterChannelA Command = new CGetSweepParameterChannelA(this, WIndex);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CSetSweepParameterChannelA.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              WIndex = Int16.Parse(Tokens[1]);
              WValue = Int16.Parse(Tokens[2]);
              CSetSweepParameterChannelA Command = new CSetSweepParameterChannelA(this, WIndex, WValue);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CGetSweepStateChannelB.COMMAND_TEXT:
            if (0 < Tokens.Length)
            {
              CGetSweepStateChannelB Command = new CGetSweepStateChannelB(this);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CSetSweepStateChannelB.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              ESweepState SS = (ESweepState)Int16.Parse(Tokens[1]);
              CSetSweepStateChannelB Command = new CSetSweepStateChannelB(this, SS);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CGetSweepParameterChannelB.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              WIndex = Int16.Parse(Tokens[1]);
              CGetSweepParameterChannelB Command = new CGetSweepParameterChannelB(this, WIndex);
              Commandlist.Add(Command);
              return true;
            }
            break;
          case CSetSweepParameterChannelB.COMMAND_TEXT:
            if (1 < Tokens.Length)
            {
              WIndex = Int16.Parse(Tokens[1]);
              WValue = Int16.Parse(Tokens[2]);
              CSetSweepParameterChannelB Command = new CSetSweepParameterChannelB(this, WIndex, WValue);
              Commandlist.Add(Command);
              return true;
            }
            break;
        }
      }
      return false;
    }


    protected Boolean SendLine(String line)
    { // first: Analyse if command-token is in this line
      if (!AnalyseSendCommand(line))
      {
        return ComPort.WriteLine(line);
      }
      return false;
    }

    //
    //--------------------------------------
    //	Section - Command - Synchronisation
    //--------------------------------------
    //
    public Boolean CommandsFinished()
    {
      return (0 == Commandlist.Count);
    }


    public static ESweepState StringStateToSweepState(String textstate)
    {
      if (textstate == ESweepState.Idle.ToString())
      {
        return ESweepState.Idle;
      }
      if (textstate == ESweepState.LowHighLowHigh.ToString())
      {
        return ESweepState.LowHighLowHigh;
      }
      if (textstate == ESweepState.HighLowHighLow.ToString())
      {
        return ESweepState.HighLowHighLow;
      }
      if (textstate == ESweepState.LowHighHighLow.ToString())
      {
        return ESweepState.LowHighHighLow;
      }
      if (textstate == ESweepState.HighLowLowHigh.ToString())
      {
        return ESweepState.HighLowLowHigh;
      }
      return ESweepState.Idle;
    }

    public static ESweepState StringIndexToSweepState(String textindex)
    {
      try
      {
        ESweepState SS = (ESweepState)Int16.Parse(textindex);
        return SS;
      }
      catch (Exception)
      {
        return ESweepState.Idle;
      }
    }


  }
}
