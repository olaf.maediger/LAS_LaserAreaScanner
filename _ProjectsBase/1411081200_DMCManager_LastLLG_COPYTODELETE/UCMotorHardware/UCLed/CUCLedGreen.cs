﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCLed
{
  public partial class CUCLedGreen : CUCLedBase
  {
    public CUCLedGreen()
    {
      InitializeComponent();
      FColorAreaActive = Color.FromArgb(0xFF, 0x00, 0xFF, 0x00);
      FColorAreaPassive = Color.FromArgb(0xFF, 0x00, 0x90, 0x00);
      Active = true;
      Active = false;
    }
  }
}
