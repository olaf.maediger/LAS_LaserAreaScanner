﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UCNotifier;
using Initdata;
using ComPort;
using HWDcMotorController;
//
namespace UCDcMotorController
{
  public partial class CUCDcMotorControllerBase : UserControl
  {
    //
    //-----------------------------------------------------
    //	Section - Constant
    //-----------------------------------------------------
    //
    // Initdata - Common
    static public String SECTION_HEADER = "UCDcMotorController";
    static public String TITLE_LIBRARY = "UCDcMotorController";
    static public String SECTION_LIBRARY = TITLE_LIBRARY;
    // Initdata - InitValue - Common
    //
    //  ComPort
    private const String TEXT_OPEN = "Open";
    private const String TEXT_CLOSE = "Close";
    //
    // Initdata - UCDelayChannelA
    public const String SECTION_UCDELAYCHANNELA = "UCDelayChannelA";
    public const String NAME_UNITDELAYNS = "UnitDelayNS";
    // Initdata - InitValue - DelayChannelA
    public const Boolean INIT_UNITDELAYNS = true;
    //
    // Initdata - UCDelayChannelB
    public const String SECTION_UCDELAYCHANNELB = "UCDelayChannelB";
    // Initdata - InitValue - DelayChannelB
    //
    // Initdata - UCSweepChannelA
    public const String SECTION_UCSWEEPCHANNELA = "UCSweepChannelA";
    public const String NAME_SWEEPSTATE = "SweepState";
    public const String NAME_LIMITLOW = "LimitLow";
    public const String NAME_LIMITHIGH = "LimitHigh";
    public const String NAME_STEPDELTA = "StepDelta";
    public const String NAME_STEPCOUNT = "StepCount";
    public const String NAME_REPETITIONCOUNT = "RepetitionCount";
    //
    // Initdata - UCSweepChannelB
    public const String SECTION_UCSWEEPCHANNELB = "UCSweepChannelB";
    //
    //-----------------------------------------------------
    //	Section - Type
    //-----------------------------------------------------
    //

    //
    //-----------------------------------------------------
    //	Section - Member
    //-----------------------------------------------------
    //
    private CNotifier FNotifier; // only reference!!!
    private CHWDcMotorController FHWDcMotorController;
    private System.Windows.Forms.Timer FCloser = null;
    private Boolean FComPortOpened;
    //
    //-----------------------------------------------------
    //	Section - Constructor
    //-----------------------------------------------------
    //
    public CUCDcMotorControllerBase()
    {	// DecimalPoint 
      CInitdata.Init();
      //
      InitializeComponent();
      //
      // HWDcMotorController
      FHWDcMotorController = new CHWDcMotorController();
      FHWDcMotorController.SetOnSerialDataReceived(OnSerialDataReceived);
      FHWDcMotorController.SetOnSerialDataTransmitted(OnSerialDataTransmitted);
      RConfigurationData CD = new RConfigurationData();
      FHWDcMotorController.GetConfigurationData(ref CD);
      // ComPort
      CD.OnComPortDataChanged = SelfOnComPortDataChanged; // !!! not in CD.ComPortData.OnComPortDataChanged
      // Common
      CD.CommonData.OnCommonDataChanged = SelfOnCommonDataChanged;
      // ChannelA
      CD.ChannelDataA.OnChannelDataChanged = SelfOnChannelDataAChanged;
      // ChannelB
      CD.ChannelDataB.OnChannelDataChanged = SelfOnChannelDataBChanged;
      // Led
      CD.LedData3.OnLedDataChanged = SelfOnLed3DataChanged;
      CD.LedData7.OnLedDataChanged = SelfOnLed7DataChanged;
      //
      FHWDcMotorController.SetConfigurationData(CD);
      //
      // UCVersion
      FUCVersion.SetOnGetVersion(UCVersionOnGetVersion);
      FUCVersion.SetOnGetHelp(UCVersionOnGetHelp);
      //
      // UCTerminal
      FUCTerminal.SetOnSendLine(UCTerminalOnSendLine);
      //
      // UCStartStop
      // NC FUCStartStop.SetOnStartStopChanged(UCStartStopChanged);
      //
      InitControls();
      RefreshComPortControls();
    }
    //
    //-----------------------------------------------------
    //	Section - Property
    //-----------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      // Child HWDcMotorController
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.SetNotifier(FNotifier);
      }
      // Child UCTerminal
      FUCTerminal.SetNotifier(FNotifier);
    }
    
    private String GetDeviceType()
    {
      return lblDeviceType.Text;
    }

    private delegate void CBSetDeviceType(String value);
    private void SetDeviceType(String value)
    {
      if (this.InvokeRequired)
      {
        CBSetDeviceType CB = new CBSetDeviceType(SetDeviceType);
        Invoke(CB, new object[] { value });
      }
      else
      {
        lblDeviceType.Text = value;
      }
    }
    public String DeviceType
    {
      get { return GetDeviceType(); }
      set { SetDeviceType(value); }
    }

    private EComPort GetComPort()
    {
      EComPort ComPort = CComPort.ComPortEnumerator(cbxPortsSelectable.Text);
      return ComPort;
    }
    private void SetComPort(EComPort comport)
    {
      String DeviceType = CComPort.ComPortName(comport);
      for (Int32 II = 0; II < cbxPortsSelectable.Items.Count; II++)
      {
        String ItemText = cbxPortsSelectable.Items[II].ToString();
        if (DeviceType == ItemText)
        {
          cbxPortsSelectable.SelectedIndex = II;
        }
      }

    }
    public EComPort ComPort
    {
      get { return GetComPort(); }
      set { SetComPort(value); }
    }
    //
    //-----------------------------------------------------
    //	Section - Helper
    //-----------------------------------------------------
    //
    public void RefreshComPortControls()
    {
      String[] PortsDetected = null;
      // Port Possible
      cbxPortsPossible.Items.Clear();
      PortsDetected = CComPort.GetPossiblePorts();
      if (null != PortsDetected)
      {
        for (Int32 SI = 0; SI < PortsDetected.Length; SI++)
        {
          cbxPortsPossible.Items.Add(PortsDetected[SI]);
        }
        if (0 < cbxPortsPossible.Items.Count)
        {
          cbxPortsPossible.SelectedIndex = 0;
        }
      }
      // Port Installed
      cbxPortsInstalled.Items.Clear();
      PortsDetected = CComPort.GetInstalledPorts();
      if (null != PortsDetected)
      {
        for (Int32 SI = 0; SI < PortsDetected.Length; SI++)
        {
          cbxPortsInstalled.Items.Add(PortsDetected[SI]);
        }
        if (0 < cbxPortsInstalled.Items.Count)
        {
          cbxPortsInstalled.SelectedIndex = 0;
        }
      }
      // Port Selectable
      cbxPortsSelectable.Items.Clear();
      PortsDetected = CComPort.GetSelectablePorts();
      if (null != PortsDetected)
      {
        for (Int32 SI = 0; SI < PortsDetected.Length; SI++)
        {
          cbxPortsSelectable.Items.Add(PortsDetected[SI]);
        }
        if (cbxPortsSelectable.SelectedIndex < 0)
        {
          if (0 < cbxPortsSelectable.Items.Count)
          {
            cbxPortsSelectable.SelectedIndex = 0;
          }
        }
      }
    }

    public void EnableControls(Boolean value)
    {
      gbxCOMSelection.Enabled = true; // forever!
      cbxPortsSelectable.Enabled = !value;
      //
      FUCVersion.Enabled = value;
      gbxChannelDataA.Enabled = value;
      gbxChannelDataB.Enabled = value;
      gbxLed.Enabled = value;
      gbxSweepChannelA.Enabled = value;
      gbxSweepChannelB.Enabled = value;
    }
    //
    //######################################################
    //######################################################
    //######################################################
    //
    //
    //######################################################
    //  Section - HWDcMotorController
    //######################################################
    //
    //  Section - Callback - HWDcMotorController
    //
    private delegate void CBOnSerialDataReceived(String data);
    private void OnSerialDataReceived(String data)
    {
      if (this.InvokeRequired)
      {
        CBOnSerialDataReceived CB = new CBOnSerialDataReceived(OnSerialDataReceived);
        Invoke(CB, new object[] { data });
      } else
      {
        FUCTerminal.AddRxData(data);
      }
    }

    private delegate void CBOnSerialDataTransmitted(String data);
    private void OnSerialDataTransmitted(String data)
    {
      if (this.InvokeRequired)
      {
        CBOnSerialDataTransmitted CB = new CBOnSerialDataTransmitted(OnSerialDataTransmitted);
        Invoke(CB, new object[] { data });
      }
      else
      {
        // ???????????????????????????????FUCTerminal.AddTxData('>' + data);
        FUCTerminal.AddTxData(data);
      }
    }
    //
    //######################################################
    //  Section - CommonData / Version
    //######################################################
    //
    private void SelfOnCommonDataChanged(RCommonData data)
    {
      FUCVersion.SoftwareVersion = data.SoftwareVersion;
      FUCVersion.HardwareVersion = data.HardwareVersion;
      SetDeviceType(data.DeviceType);
      // ?????????? FProcessControl.RefreshCommonData(data);
    }

    private void UCVersionOnGetVersion()
    {
      FHWDcMotorController.CommandGetSoftwareVersion();
      FHWDcMotorController.CommandGetHardwareVersion();
    }

    private void UCVersionOnGetHelp()
    {
      FHWDcMotorController.CommandGetHelp();
    }
    //
    //##############################################
    //  Section - ChannelDataA - Delay
    //##############################################
    //
    private void SelfOnChannelDataAChanged(RChannelData data)
    {
      DelayChannelA = data.Delay;
      SweepStateChannelA = data.SweepState;
      SweepLimitLowChannelA = data.LimitLow;
      SweepLimitHighChannelA = data.LimitHigh;
      SweepStepDeltaChannelA = data.StepDelta;
      SweepStepCountChannelA = data.StepCount;
      // NC SweepStepIndexChannelA = data.StepIndex;
      SweepRepetitionCountChannelA = data.RepetitionCount;
      // NC SweepRepetitionIndexChannelA = data.RepetitionIndex;
    }
    //
    //##############################################
    //  Section - ChannelDataB - Delay
    //##############################################
    //
    private void SelfOnChannelDataBChanged(RChannelData data)
    {
      DelayChannelB = data.Delay;
      SweepStateChannelB = data.SweepState;
      SweepLimitLowChannelB = data.LimitLow;
      SweepLimitHighChannelB = data.LimitHigh;
      SweepStepDeltaChannelB = data.StepDelta;
      SweepStepCountChannelB = data.StepCount;
      // NC SweepStepIndexChannelB = data.StepIndex;
      SweepRepetitionCountChannelB = data.RepetitionCount;
      // NC SweepRepetitionIndexChannelB = data.RepetitionIndex;
    }
    //
    //##############################################
    //  Section - Led
    //##############################################
    //
    //	Section - Led - Property
    //
    private Boolean GetLed(Int32 index)
    {
      switch (index)
      {
        case 3:
          return FUCLedRed3Busy.Active;
        case 7:
          return FUCLedRed7Error.Active;
      }
      return false;
    }
    //
    //	Section - Led - Callback
    //
    private delegate void CBSetLed(Int32 index, Boolean value);
    private void SetLed(Int32 index, Boolean value)
    {
      if (this.InvokeRequired)
      {
        CBSetLed CB = new CBSetLed(SetLed);
        Invoke(CB, new object[] { index, value });
      }
      else
      {
        FUCLedRed3Busy.OnActiveChanged -= FUCLedRed3Busy_OnActiveChanged;
        FUCLedRed7Error.OnActiveChanged -= FUCLedRed7Error_OnActiveChanged;
        switch (index)
        {
          case 3:
            FUCLedRed3Busy.Active = value;
            break;
          case 7:
            FUCLedRed7Error.Active = value;
            break;
        }
        FUCLedRed7Error.OnActiveChanged += FUCLedRed7Error_OnActiveChanged;
        FUCLedRed3Busy.OnActiveChanged += FUCLedRed3Busy_OnActiveChanged;
      }
    }

    private void SelfOnLed3DataChanged(RLedData data)
    {
      SetLed(data.Index, data.Active);
    }

    private void SelfOnLed7DataChanged(RLedData data)
    {
      SetLed(data.Index, data.Active);
    }
    //
    //##############################################
    //  Section - DelayChannelA
    //##############################################
    //
    private Int32 GetDelayChannelA()
    {
      if (cbxDelayChannelAUnit.Checked)
      { // [ns]
        return (Int32)(100 * Double.Parse(lblDelayChannelA.Text));
      }
      else
      { // [ps]
        return 10 * Int32.Parse(lblDelayChannelA.Text);
      }
    }
    private delegate void CBSetDelayChannelA(Int32 value);
    private void SetDelayChannelA(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetDelayChannelA CB = new CBSetDelayChannelA(SetDelayChannelA);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != DelayChannelA)
        {
          ForceDelayChannelA(value);
        }
      }
    }
    public Int32 DelayChannelA
    {
      get { return GetDelayChannelA(); }
      set { SetDelayChannelA(value); }
    }

    private void ForceDelayChannelA(Int32 value)
    {
      nudDelayChannelA.ValueChanged -= nudDelayChannelA_ValueChanged;
      cbxDelayChannelAUnit.CheckedChanged -= cbxDelayChannelAUnit_CheckedChanged;
      hsbDelayChannelA.ValueChanged -= hsbDelayChannelA_ValueChanged;
      //
      nudDelayChannelA.Value = value;
      hsbDelayChannelA.Value = value;
      if (cbxDelayChannelAUnit.Checked)
      {
        cbxDelayChannelAUnit.Text = "ns";
        lblDelayChannelA.Text = String.Format("{0:0.000}", 0.01 * value);
      }
      else
      {
        cbxDelayChannelAUnit.Text = "ps";
        lblDelayChannelA.Text = String.Format("{0}", 10 * value);
      }
      //
      nudDelayChannelA.ValueChanged += nudDelayChannelA_ValueChanged;
      cbxDelayChannelAUnit.CheckedChanged += cbxDelayChannelAUnit_CheckedChanged;
      hsbDelayChannelA.ValueChanged += hsbDelayChannelA_ValueChanged;
    }
    //
    //##############################################
    //  Section - DelayChannelB
    //##############################################
    //
    private Int32 GetDelayChannelB()
    {
      if (cbxDelayChannelBUnit.Checked)
      { // [ns]
        return (Int32)(100 * Double.Parse(lblDelayChannelB.Text));
      }
      else
      { // [ps]
        return 10 * Int32.Parse(lblDelayChannelB.Text);
      }
    }
    private delegate void CBSetDelayChannelB(Int32 value);
    private void SetDelayChannelB(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetDelayChannelB CB = new CBSetDelayChannelB(SetDelayChannelB);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != DelayChannelB)
        {
          ForceDelayChannelB(value);
        }
      }
    }
    public Int32 DelayChannelB
    {
      get { return GetDelayChannelB(); }
      set { SetDelayChannelB(value); }
    }

    private void ForceDelayChannelB(Int32 value)
    {
      nudDelayChannelB.ValueChanged -= nudDelayChannelB_ValueChanged;
      cbxDelayChannelBUnit.CheckedChanged -= cbxDelayChannelBUnit_CheckedChanged;
      hsbDelayChannelB.ValueChanged -= hsbDelayChannelB_ValueChanged;
      //
      nudDelayChannelB.Value = value;
      hsbDelayChannelB.Value = value;
      if (cbxDelayChannelBUnit.Checked)
      {
        cbxDelayChannelBUnit.Text = "ns";
        lblDelayChannelB.Text = String.Format("{0:0.000}", 0.01 * value);
      }
      else
      {
        cbxDelayChannelBUnit.Text = "ps";
        lblDelayChannelB.Text = String.Format("{0}", 10 * value);
      }
      nudDelayChannelB.ValueChanged += nudDelayChannelB_ValueChanged;
      cbxDelayChannelBUnit.CheckedChanged += cbxDelayChannelBUnit_CheckedChanged;
      hsbDelayChannelB.ValueChanged += hsbDelayChannelB_ValueChanged;
    }



    //
    //##############################################
    //  Section - SweepStateChannelA
    //##############################################
    //
    private ESweepState GetSweepStateChannelA()
    {
      if (rbtSweepStateLowHighLowHighChannelA.Checked)
      {
        return ESweepState.LowHighLowHigh;
      }
      if (rbtSweepStateHighLowHighLowChannelA.Checked)
      {
        return ESweepState.HighLowHighLow;
      }
      if (rbtSweepStateLowHighHighLowChannelA.Checked)
      {
        return ESweepState.LowHighHighLow;
      }
      if (rbtSweepStateHighLowLowHighChannelA.Checked)
      {
        return ESweepState.HighLowLowHigh;
      }
      // default rbtSweepStateIdleChannelA.Checked
      ForceSweepStateChannelA(ESweepState.Idle);
      return ESweepState.Idle;
    }
    private delegate void CBSetSweepStateChannelA(ESweepState value);
    private void SetSweepStateChannelA(ESweepState value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepStateChannelA CB = new CBSetSweepStateChannelA(SetSweepStateChannelA);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepStateChannelA)
        {
          ForceSweepStateChannelA(value);
        }
      }
    }
    public ESweepState SweepStateChannelA
    {
      get { return GetSweepStateChannelA(); }
      set { SetSweepStateChannelA(value); }
    }

    private void ForceSweepStateChannelA(ESweepState value)
    {
      rbtSweepStateIdleChannelA.CheckedChanged -= rbtSweepStateIdleChannelA_CheckedChanged;
      rbtSweepStateLowHighLowHighChannelA.CheckedChanged -= rbtSweepStateLowHighLowHighChannelA_CheckedChanged;
      rbtSweepStateHighLowHighLowChannelA.CheckedChanged -= rbtSweepStateHighLowHighLowChannelA_CheckedChanged;
      rbtSweepStateLowHighHighLowChannelA.CheckedChanged -= rbtSweepStateLowHighHighLowChannelA_CheckedChanged;
      rbtSweepStateHighLowLowHighChannelA.CheckedChanged -= rbtSweepStateHighLowLowHighChannelA_CheckedChanged;
      switch (value)
      {
        case ESweepState.LowHighLowHigh:
          rbtSweepStateLowHighLowHighChannelA.Checked = true;
          break;
        case ESweepState.HighLowHighLow:
          rbtSweepStateHighLowHighLowChannelA.Checked = true;
          break;
        case ESweepState.LowHighHighLow:
          rbtSweepStateLowHighHighLowChannelA.Checked = true;
          break;
        case ESweepState.HighLowLowHigh:
          rbtSweepStateHighLowLowHighChannelA.Checked = true;
          break;
        default:
          rbtSweepStateIdleChannelA.Checked = true;
          break;
      }
      rbtSweepStateIdleChannelA.CheckedChanged += rbtSweepStateIdleChannelA_CheckedChanged;
      rbtSweepStateLowHighLowHighChannelA.CheckedChanged += rbtSweepStateLowHighLowHighChannelA_CheckedChanged;
      rbtSweepStateHighLowHighLowChannelA.CheckedChanged += rbtSweepStateHighLowHighLowChannelA_CheckedChanged;
      rbtSweepStateLowHighHighLowChannelA.CheckedChanged += rbtSweepStateLowHighHighLowChannelA_CheckedChanged;
      rbtSweepStateHighLowLowHighChannelA.CheckedChanged += rbtSweepStateHighLowLowHighChannelA_CheckedChanged;
    }
    //
    //##############################################
    //  Section - SweepLimitLowChannelA
    //##############################################
    //
    private Int32 GetSweepLimitLowChannelA()
    {
      return (Int32)nudSweepLimitLowChannelA.Value;
    }
    private delegate void CBSetSweepLimitLowChannelA(Int32 value);
    private void SetSweepLimitLowChannelA(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepLimitLowChannelA CB = new CBSetSweepLimitLowChannelA(SetSweepLimitLowChannelA);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepLimitLowChannelA)
        {
          ForceSweepLimitLowChannelA(value);
        }
      }
    }
    public Int32 SweepLimitLowChannelA
    {
      get { return GetSweepLimitLowChannelA(); }
      set { SetSweepLimitLowChannelA(value); }
    }

    private void ForceSweepLimitLowChannelA(Int32 value)
    {
      nudSweepLimitLowChannelA.ValueChanged -= nudSweepLimitLowChannelA_ValueChanged;
      nudSweepLimitLowChannelA.Value = value;
      nudSweepLimitLowChannelA.ValueChanged += nudSweepLimitLowChannelA_ValueChanged;
    }
    //
    //##############################################
    //  Section - SweepLimitHighChannelA
    //##############################################
    //
    private Int32 GetSweepLimitHighChannelA()
    {
      return (Int32)nudSweepLimitHighChannelA.Value;
    }
    private delegate void CBSetSweepLimitHighChannelA(Int32 value);
    private void SetSweepLimitHighChannelA(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepLimitHighChannelA CB = new CBSetSweepLimitHighChannelA(SetSweepLimitHighChannelA);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepLimitHighChannelA)
        {
          ForceSweepLimitHighChannelA(value);
        }
      }
    }
    public Int32 SweepLimitHighChannelA
    {
      get { return GetSweepLimitHighChannelA(); }
      set { SetSweepLimitHighChannelA(value); }
    }

    private void ForceSweepLimitHighChannelA(Int32 value)
    {
      nudSweepLimitHighChannelA.ValueChanged -= nudSweepLimitHighChannelA_ValueChanged;
      nudSweepLimitHighChannelA.Value = value;
      nudSweepLimitHighChannelA.ValueChanged += nudSweepLimitHighChannelA_ValueChanged;
    }
    //
    //##############################################
    //  Section - SweepStepDeltaChannelA
    //##############################################
    //
    private Int32 GetSweepStepDeltaChannelA()
    {
      return (Int32)nudSweepStepDeltaChannelA.Value;
    }
    private delegate void CBSetSweepStepDeltaChannelA(Int32 value);
    private void SetSweepStepDeltaChannelA(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepStepDeltaChannelA CB = new CBSetSweepStepDeltaChannelA(SetSweepStepDeltaChannelA);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepStepDeltaChannelA)
        {
          ForceSweepStepDeltaChannelA(value);
        }
      }
    }
    public Int32 SweepStepDeltaChannelA
    {
      get { return GetSweepStepDeltaChannelA(); }
      set { SetSweepStepDeltaChannelA(value); }
    }

    private void ForceSweepStepDeltaChannelA(Int32 value)
    {
      nudSweepStepDeltaChannelA.ValueChanged -= nudSweepStepDeltaChannelA_ValueChanged;
      nudSweepStepDeltaChannelA.Value = value;
      nudSweepStepDeltaChannelA.ValueChanged += nudSweepStepDeltaChannelA_ValueChanged;
    }
    //
    //##############################################
    //  Section - SweepStepCountChannelA
    //##############################################
    //
    private Int32 GetSweepStepCountChannelA()
    {
      return (Int32)nudSweepStepCountChannelA.Value;
    }
    private delegate void CBSetSweepStepCountChannelA(Int32 value);
    private void SetSweepStepCountChannelA(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepStepCountChannelA CB = new CBSetSweepStepCountChannelA(SetSweepStepCountChannelA);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepStepCountChannelA)
        {
          ForceSweepStepCountChannelA(value);
        }
      }
    }
    public Int32 SweepStepCountChannelA
    {
      get { return GetSweepStepCountChannelA(); }
      set { SetSweepStepCountChannelA(value); }
    }

    private void ForceSweepStepCountChannelA(Int32 value)
    {
      nudSweepStepCountChannelA.ValueChanged -= nudSweepStepCountChannelA_ValueChanged;
      nudSweepStepCountChannelA.Value = value;
      nudSweepStepCountChannelA.ValueChanged += nudSweepStepCountChannelA_ValueChanged;
    }
    //
    //##############################################
    //  Section - SweepStepIndexChannelA
    //##############################################
    //
    /* NC private Int32 GetSweepStepIndexChannelA()
    {
      return (Int32)nudSweepStepIndexChannelA.Value;
    }
    private delegate void CBSetSweepStepIndexChannelA(Int32 value);
    private void SetSweepStepIndexChannelA(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepStepIndexChannelA CB = new CBSetSweepStepIndexChannelA(SetSweepStepIndexChannelA);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepStepIndexChannelA)
        {
          ForceSweepStepIndexChannelA(value);
        }
      }
    }
    public Int32 SweepStepIndexChannelA
    {
      get { return GetSweepStepIndexChannelA(); }
      set { SetSweepStepIndexChannelA(value); }
    }

    private void ForceSweepStepIndexChannelA(Int32 value)
    {
      nudSweepStepIndexChannelA.ValueChanged -= nudSweepStepIndexChannelA_ValueChanged;
      nudSweepStepIndexChannelA.Value = value;
      nudSweepStepIndexChannelA.ValueChanged += nudSweepStepIndexChannelA_ValueChanged;
    }*/
    //
    //##############################################
    //  Section - SweepRepetitionCountChannelA
    //##############################################
    //
    private Int32 GetSweepRepetitionCountChannelA()
    {
      return (Int32)nudSweepRepetitionCountChannelA.Value;
    }
    private delegate void CBSetSweepRepetitionCountChannelA(Int32 value);
    private void SetSweepRepetitionCountChannelA(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepRepetitionCountChannelA CB = new CBSetSweepRepetitionCountChannelA(SetSweepRepetitionCountChannelA);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepRepetitionCountChannelA)
        {
          ForceSweepRepetitionCountChannelA(value);
        }
      }
    }
    public Int32 SweepRepetitionCountChannelA
    {
      get { return GetSweepRepetitionCountChannelA(); }
      set { SetSweepRepetitionCountChannelA(value); }
    }

    private void ForceSweepRepetitionCountChannelA(Int32 value)
    {
      nudSweepRepetitionCountChannelA.ValueChanged -= nudSweepRepetitionCountChannelA_ValueChanged;
      nudSweepRepetitionCountChannelA.Value = value;
      nudSweepRepetitionCountChannelA.ValueChanged += nudSweepRepetitionCountChannelA_ValueChanged;
    }
    //
    //##############################################
    //  Section - SweepRepetitionIndexChannelA
    //##############################################
    //
    /* NC private Int32 GetSweepRepetitionIndexChannelA()
    {
      return (Int32)nudSweepRepetitionIndexChannelA.Value;
    }
    private delegate void CBSetSweepRepetitionIndexChannelA(Int32 value);
    private void SetSweepRepetitionIndexChannelA(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepRepetitionIndexChannelA CB = new CBSetSweepRepetitionIndexChannelA(SetSweepRepetitionIndexChannelA);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepRepetitionIndexChannelA)
        {
          ForceSweepRepetitionIndexChannelA(value);
        }
      }
    }
    public Int32 SweepRepetitionIndexChannelA
    {
      get { return GetSweepRepetitionIndexChannelA(); }
      set { SetSweepRepetitionIndexChannelA(value); }
    }

    private void ForceSweepRepetitionIndexChannelA(Int32 value)
    {
      nudSweepRepetitionIndexChannelA.ValueChanged -= nudSweepRepetitionIndexChannelA_ValueChanged;
      nudSweepRepetitionIndexChannelA.Value = value;
      nudSweepRepetitionIndexChannelA.ValueChanged += nudSweepRepetitionIndexChannelA_ValueChanged;
    }*/




    //
    //##############################################
    //  Section - SweepStateChannelB
    //##############################################
    //
    private ESweepState GetSweepStateChannelB()
    {
      if (rbtSweepStateLowHighLowHighChannelB.Checked)
      {
        return ESweepState.LowHighLowHigh;
      }
      if (rbtSweepStateHighLowHighLowChannelB.Checked)
      {
        return ESweepState.HighLowHighLow;
      }
      if (rbtSweepStateLowHighHighLowChannelB.Checked)
      {
        return ESweepState.LowHighHighLow;
      }
      if (rbtSweepStateHighLowLowHighChannelB.Checked)
      {
        return ESweepState.HighLowLowHigh;
      }
      // default rbtSweepStateIdleChannelB.Checked
      ForceSweepStateChannelB(ESweepState.Idle);
      return ESweepState.Idle;
    }
    private delegate void CBSetSweepStateChannelB(ESweepState value);
    private void SetSweepStateChannelB(ESweepState value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepStateChannelB CB = new CBSetSweepStateChannelB(SetSweepStateChannelB);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepStateChannelB)
        {
          ForceSweepStateChannelB(value);
        }
      }
    }
    public ESweepState SweepStateChannelB
    {
      get { return GetSweepStateChannelB(); }
      set { SetSweepStateChannelB(value); }
    }

    private void ForceSweepStateChannelB(ESweepState value)
    {
      rbtSweepStateIdleChannelB.CheckedChanged -= rbtSweepStateIdleChannelB_CheckedChanged;
      rbtSweepStateLowHighLowHighChannelB.CheckedChanged -= rbtSweepStateLowHighLowHighChannelB_CheckedChanged;
      rbtSweepStateHighLowHighLowChannelB.CheckedChanged -= rbtSweepStateHighLowHighLowChannelB_CheckedChanged;
      rbtSweepStateLowHighHighLowChannelB.CheckedChanged -= rbtSweepStateLowHighHighLowChannelB_CheckedChanged;
      rbtSweepStateHighLowLowHighChannelB.CheckedChanged -= rbtSweepStateHighLowLowHighChannelB_CheckedChanged;
      switch (value)
      {
        case ESweepState.LowHighLowHigh:
          rbtSweepStateLowHighLowHighChannelB.Checked = true;
          break;
        case ESweepState.HighLowHighLow:
          rbtSweepStateHighLowHighLowChannelB.Checked = true;
          break;
        case ESweepState.LowHighHighLow:
          rbtSweepStateLowHighHighLowChannelB.Checked = true;
          break;
        case ESweepState.HighLowLowHigh:
          rbtSweepStateHighLowLowHighChannelB.Checked = true;
          break;
        default:
          rbtSweepStateIdleChannelB.Checked = true;
          break;
      }
      rbtSweepStateIdleChannelB.CheckedChanged += rbtSweepStateIdleChannelB_CheckedChanged;
      rbtSweepStateLowHighLowHighChannelB.CheckedChanged += rbtSweepStateLowHighLowHighChannelB_CheckedChanged;
      rbtSweepStateHighLowHighLowChannelB.CheckedChanged += rbtSweepStateHighLowHighLowChannelB_CheckedChanged;
      rbtSweepStateLowHighHighLowChannelB.CheckedChanged += rbtSweepStateLowHighHighLowChannelB_CheckedChanged;
      rbtSweepStateHighLowLowHighChannelB.CheckedChanged += rbtSweepStateHighLowLowHighChannelB_CheckedChanged;
    }
    //
    //##############################################
    //  Section - SweepLimitLowChannelB
    //##############################################
    //
    private Int32 GetSweepLimitLowChannelB()
    {
      return (Int32)nudSweepLimitLowChannelB.Value;
    }
    private delegate void CBSetSweepLimitLowChannelB(Int32 value);
    private void SetSweepLimitLowChannelB(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepLimitLowChannelB CB = new CBSetSweepLimitLowChannelB(SetSweepLimitLowChannelB);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepLimitLowChannelB)
        {
          ForceSweepLimitLowChannelB(value);
        }
      }
    }
    public Int32 SweepLimitLowChannelB
    {
      get { return GetSweepLimitLowChannelB(); }
      set { SetSweepLimitLowChannelB(value); }
    }

    private void ForceSweepLimitLowChannelB(Int32 value)
    {
      nudSweepLimitLowChannelB.ValueChanged -= nudSweepLimitLowChannelB_ValueChanged;
      nudSweepLimitLowChannelB.Value = value;
      nudSweepLimitLowChannelB.ValueChanged += nudSweepLimitLowChannelB_ValueChanged;
    }
    //
    //##############################################
    //  Section - SweepLimitHighChannelB
    //##############################################
    //
    private Int32 GetSweepLimitHighChannelB()
    {
      return (Int32)nudSweepLimitHighChannelB.Value;
    }
    private delegate void CBSetSweepLimitHighChannelB(Int32 value);
    private void SetSweepLimitHighChannelB(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepLimitHighChannelB CB = new CBSetSweepLimitHighChannelB(SetSweepLimitHighChannelB);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepLimitHighChannelB)
        {
          ForceSweepLimitHighChannelB(value);
        }
      }
    }
    public Int32 SweepLimitHighChannelB
    {
      get { return GetSweepLimitHighChannelB(); }
      set { SetSweepLimitHighChannelB(value); }
    }

    private void ForceSweepLimitHighChannelB(Int32 value)
    {
      nudSweepLimitHighChannelB.ValueChanged -= nudSweepLimitHighChannelB_ValueChanged;
      nudSweepLimitHighChannelB.Value = value;
      nudSweepLimitHighChannelB.ValueChanged += nudSweepLimitHighChannelB_ValueChanged;
    }
    //
    //##############################################
    //  Section - SweepStepDeltaChannelB
    //##############################################
    //
    private Int32 GetSweepStepDeltaChannelB()
    {
      return (Int32)nudSweepStepDeltaChannelB.Value;
    }
    private delegate void CBSetSweepStepDeltaChannelB(Int32 value);
    private void SetSweepStepDeltaChannelB(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepStepDeltaChannelB CB = new CBSetSweepStepDeltaChannelB(SetSweepStepDeltaChannelB);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepStepDeltaChannelB)
        {
          ForceSweepStepDeltaChannelB(value);
        }
      }
    }
    public Int32 SweepStepDeltaChannelB
    {
      get { return GetSweepStepDeltaChannelB(); }
      set { SetSweepStepDeltaChannelB(value); }
    }

    private void ForceSweepStepDeltaChannelB(Int32 value)
    {
      nudSweepStepDeltaChannelB.ValueChanged -= nudSweepStepDeltaChannelB_ValueChanged;
      nudSweepStepDeltaChannelB.Value = value;
      nudSweepStepDeltaChannelB.ValueChanged += nudSweepStepDeltaChannelB_ValueChanged;
    }
    //
    //##############################################
    //  Section - SweepStepCountChannelB
    //##############################################
    //
    private Int32 GetSweepStepCountChannelB()
    {
      return (Int32)nudSweepStepCountChannelB.Value;
    }
    private delegate void CBSetSweepStepCountChannelB(Int32 value);
    private void SetSweepStepCountChannelB(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepStepCountChannelB CB = new CBSetSweepStepCountChannelB(SetSweepStepCountChannelB);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepStepCountChannelB)
        {
          ForceSweepStepCountChannelB(value);
        }
      }
    }
    public Int32 SweepStepCountChannelB
    {
      get { return GetSweepStepCountChannelB(); }
      set { SetSweepStepCountChannelB(value); }
    }

    private void ForceSweepStepCountChannelB(Int32 value)
    {
      nudSweepStepCountChannelB.ValueChanged -= nudSweepStepCountChannelB_ValueChanged;
      nudSweepStepCountChannelB.Value = value;
      nudSweepStepCountChannelB.ValueChanged += nudSweepStepCountChannelB_ValueChanged;
    }
    //
    //##############################################
    //  Section - SweepStepIndexChannelB
    //##############################################
    //
    /* NC private Int32 GetSweepStepIndexChannelB()
    {
      return (Int32)nudSweepStepIndexChannelB.Value;
    }
    private delegate void CBSetSweepStepIndexChannelB(Int32 value);
    private void SetSweepStepIndexChannelB(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepStepIndexChannelB CB = new CBSetSweepStepIndexChannelB(SetSweepStepIndexChannelB);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepStepIndexChannelB)
        {
          ForceSweepStepIndexChannelB(value);
        }
      }
    }
    public Int32 SweepStepIndexChannelB
    {
      get { return GetSweepStepIndexChannelB(); }
      set { SetSweepStepIndexChannelB(value); }
    }

    private void ForceSweepStepIndexChannelB(Int32 value)
    {
      nudSweepStepIndexChannelB.ValueChanged -= nudSweepStepIndexChannelB_ValueChanged;
      nudSweepStepIndexChannelB.Value = value;
      nudSweepStepIndexChannelB.ValueChanged += nudSweepStepIndexChannelB_ValueChanged;
    }*/
    //
    //##############################################
    //  Section - SweepRepetitionCountChannelB
    //##############################################
    //
    private Int32 GetSweepRepetitionCountChannelB()
    {
      return (Int32)nudSweepRepetitionCountChannelB.Value;
    }
    private delegate void CBSetSweepRepetitionCountChannelB(Int32 value);
    private void SetSweepRepetitionCountChannelB(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepRepetitionCountChannelB CB = new CBSetSweepRepetitionCountChannelB(SetSweepRepetitionCountChannelB);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepRepetitionCountChannelB)
        {
          ForceSweepRepetitionCountChannelB(value);
        }
      }
    }
    public Int32 SweepRepetitionCountChannelB
    {
      get { return GetSweepRepetitionCountChannelB(); }
      set { SetSweepRepetitionCountChannelB(value); }
    }

    private void ForceSweepRepetitionCountChannelB(Int32 value)
    {
      nudSweepRepetitionCountChannelB.ValueChanged -= nudSweepRepetitionCountChannelB_ValueChanged;
      nudSweepRepetitionCountChannelB.Value = value;
      nudSweepRepetitionCountChannelB.ValueChanged += nudSweepRepetitionCountChannelB_ValueChanged;
    }
    //
    //##############################################
    //  Section - SweepRepetitionIndexChannelB
    //##############################################
    //
    /* NC private Int32 GetSweepRepetitionIndexChannelB()
    {
      return (Int32)nudSweepRepetitionIndexChannelB.Value;
    }
    private delegate void CBSetSweepRepetitionIndexChannelB(Int32 value);
    private void SetSweepRepetitionIndexChannelB(Int32 value)
    {
      if (this.InvokeRequired)
      {
        CBSetSweepRepetitionIndexChannelB CB = new CBSetSweepRepetitionIndexChannelB(SetSweepRepetitionIndexChannelB);
        Invoke(CB, new object[] { value });
      }
      else
      {
        if (value != SweepRepetitionIndexChannelB)
        {
          ForceSweepRepetitionIndexChannelB(value);
        }
      }
    }
    public Int32 SweepRepetitionIndexChannelB
    {
      get { return GetSweepRepetitionIndexChannelB(); }
      set { SetSweepRepetitionIndexChannelB(value); }
    }

    private void ForceSweepRepetitionIndexChannelB(Int32 value)
    {
      nudSweepRepetitionIndexChannelB.ValueChanged -= nudSweepRepetitionIndexChannelB_ValueChanged;
      nudSweepRepetitionIndexChannelB.Value = value;
      nudSweepRepetitionIndexChannelB.ValueChanged += nudSweepRepetitionIndexChannelB_ValueChanged;
    }*/
    //
    //##############################################
    //  Section - Initialisation
    //##############################################
    //
    private void InitControls()
    { // ComPort
      DeviceType = CHWDcMotorController.INIT_DEVICETYPE;
      ComPort = CHWDcMotorController.INIT_COMPORT;
      EnableControls(false);
    }

    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      Boolean Result = initdata.OpenSection(SECTION_LIBRARY);
      //
      Result &= FHWDcMotorController is CHWDcMotorController;
      if (Result)
      {
        Result &= FHWDcMotorController.LoadInitdata(initdata);
        //
        /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        Boolean BValue;
        String SValue;
        ESweepState SSValue;
        Int32 IValue;
        //  
        //  DelayChannelA - Unit
        //#################################################################
        Result = initdata.OpenSection(SECTION_UCDELAYCHANNELA);
        BValue = INIT_UNITDELAYNS;
        Result &= initdata.ReadBoolean(NAME_UNITDELAYNS, out BValue, BValue);
        cbxDelayChannelAUnit.Checked = BValue;
        Result &= initdata.CloseSection();
        //#################################################################
        //  
        //  DelayChannelB - Unit
        //#################################################################
        Result = initdata.OpenSection(SECTION_UCDELAYCHANNELB);
        BValue = INIT_UNITDELAYNS;
        Result &= initdata.ReadBoolean(NAME_UNITDELAYNS, out BValue, BValue);
        cbxDelayChannelBUnit.Checked = BValue;
        Result &= initdata.CloseSection();
        //#################################################################
        //  
        //  ChannelA - Sweep
        //#################################################################
        Result = initdata.OpenSection(SECTION_UCSWEEPCHANNELA);
        //
        //  ChannelA - SweepLimitLow
        IValue = CHWDcMotorController.INIT_LIMITLOW;
        Result &= initdata.ReadInt32(NAME_LIMITLOW, out IValue, IValue);
        SweepLimitLowChannelA = IValue;
        //
        //  ChannelA - SweepLimitHigh
        IValue = CHWDcMotorController.INIT_LIMITHIGH;
        Result &= initdata.ReadInt32(NAME_LIMITHIGH, out IValue, IValue);
        SweepLimitHighChannelA = IValue;
        //
        //  ChannelA - SweepStepDelta
        IValue = CHWDcMotorController.INIT_STEPDELTA;
        Result &= initdata.ReadInt32(NAME_STEPDELTA, out IValue, IValue);
        SweepStepDeltaChannelA = IValue;
        //
        //  ChannelA - SweepStepCount
        IValue = CHWDcMotorController.INIT_STEPCOUNT;
        Result &= initdata.ReadInt32(NAME_STEPCOUNT, out IValue, IValue);
        SweepStepCountChannelA = IValue;
        //
        //  ChannelA - SweepRepetitionCount
        IValue = CHWDcMotorController.INIT_REPETITIONCOUNT;
        Result &= initdata.ReadInt32(NAME_REPETITIONCOUNT, out IValue, IValue);
        SweepRepetitionCountChannelA = IValue;
        //
        //  ChannelA - SweepState
        SValue = CHWDcMotorController.INIT_SWEEPSTATE.ToString();
        Result &= initdata.ReadEnumeration(NAME_SWEEPSTATE, out SValue, SValue);
        SSValue = CHWDcMotorController.StringStateToSweepState(SValue);
        SweepStateChannelA = SSValue;
        //
        Result &= initdata.CloseSection();
        //#################################################################
        //  
        //  ChannelB - Sweep
        //#################################################################
        Result = initdata.OpenSection(SECTION_UCSWEEPCHANNELB);
        //
        //  ChannelB - SweepLimitLow
        IValue = CHWDcMotorController.INIT_LIMITLOW;
        Result &= initdata.ReadInt32(NAME_LIMITLOW, out IValue, IValue);
        SweepLimitLowChannelB = IValue;
        //
        //  ChannelB - SweepLimitHigh
        IValue = CHWDcMotorController.INIT_LIMITHIGH;
        Result &= initdata.ReadInt32(NAME_LIMITHIGH, out IValue, IValue);
        SweepLimitHighChannelB = IValue;
        //
        //  ChannelB - SweepStepDelta
        IValue = CHWDcMotorController.INIT_STEPDELTA;
        Result &= initdata.ReadInt32(NAME_STEPDELTA, out IValue, IValue);
        SweepStepDeltaChannelB = IValue;
        //
        //  ChannelB - SweepStepCount
        IValue = CHWDcMotorController.INIT_STEPCOUNT;
        Result &= initdata.ReadInt32(NAME_STEPCOUNT, out IValue, IValue);
        SweepStepCountChannelB = IValue;
        //
        //  ChannelB - SweepRepetitionCount
        IValue = CHWDcMotorController.INIT_REPETITIONCOUNT;
        Result &= initdata.ReadInt32(NAME_REPETITIONCOUNT, out IValue, IValue);
        SweepRepetitionCountChannelB = IValue;
        //
        //  ChannelB - SweepState
        SValue = CHWDcMotorController.INIT_SWEEPSTATE.ToString();
        Result &= initdata.ReadEnumeration(NAME_SWEEPSTATE, out SValue, SValue);
        SSValue = CHWDcMotorController.StringStateToSweepState(SValue);
        SweepStateChannelB = SSValue;
        //
        Result &= initdata.CloseSection();
        //#################################################################
         */
      }
      Result &= initdata.CloseSection();
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      //#################################################################
      Boolean Result = initdata.CreateSection(SECTION_LIBRARY);
      //
      Result &= FHWDcMotorController is CHWDcMotorController;
      //
      if (Result)
      {
        Result &= FHWDcMotorController.SaveInitdata(initdata);
        /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/
        //  DelayChannelA - Unit
        //#################################################################
        Result = initdata.CreateSection(SECTION_UCDELAYCHANNELA);
        Result &= initdata.WriteBoolean(NAME_UNITDELAYNS, cbxDelayChannelAUnit.Checked);
        Result &= initdata.CloseSection();
        //#################################################################
        //
        //  DelayChannelB - Unit
        //#################################################################
        Result = initdata.CreateSection(SECTION_UCDELAYCHANNELB);
        Result &= initdata.WriteBoolean(NAME_UNITDELAYNS, cbxDelayChannelBUnit.Checked);
        Result &= initdata.CloseSection();
        //#################################################################
        //  
        //  ChannelA - Sweep
        //#################################################################
        Result = initdata.CreateSection(SECTION_UCSWEEPCHANNELA);
        //
        //  ChannelA - SweepLimitLow
        Result &= initdata.WriteInt32(NAME_LIMITLOW, SweepLimitLowChannelA);
        //
        //  ChannelA - SweepLimitHigh
        Result &= initdata.WriteInt32(NAME_LIMITHIGH, SweepLimitHighChannelA);
        //
        //  ChannelA - SweepStepDelta
        Result &= initdata.WriteInt32(NAME_STEPDELTA, SweepStepDeltaChannelA);
        //
        //  ChannelA - SweepStepCount
        Result &= initdata.WriteInt32(NAME_STEPCOUNT, SweepStepCountChannelA);
        //
        //  ChannelA - SweepRepetitionCount
        Result &= initdata.WriteInt32(NAME_REPETITIONCOUNT, SweepRepetitionCountChannelA);
        //
        //  ChannelA - SweepState
        Result &= initdata.WriteEnumeration(NAME_SWEEPSTATE, SweepStateChannelA.ToString());
        //
        Result &= initdata.CloseSection();
        //#################################################################
        //  
        //  ChannelB - Sweep
        //#################################################################
        Result = initdata.CreateSection(SECTION_UCSWEEPCHANNELB);
        //
        //  ChannelA - SweepLimitLow
        Result &= initdata.WriteInt32(NAME_LIMITLOW, SweepLimitLowChannelB);
        //
        //  ChannelA - SweepLimitHigh
        Result &= initdata.WriteInt32(NAME_LIMITHIGH, SweepLimitHighChannelB);
        //
        //  ChannelA - SweepStepDelta
        Result &= initdata.WriteInt32(NAME_STEPDELTA, SweepStepDeltaChannelB);
        //
        //  ChannelA - SweepStepCount
        Result &= initdata.WriteInt32(NAME_STEPCOUNT, SweepStepCountChannelB);
        //
        //  ChannelA - SweepRepetitionCount
        Result &= initdata.WriteInt32(NAME_REPETITIONCOUNT, SweepRepetitionCountChannelB);
        //
        //  ChannelA - SweepState
        Result &= initdata.WriteEnumeration(NAME_SWEEPSTATE, SweepStateChannelB.ToString());
        //
        Result &= initdata.CloseSection();
        //#################################################################
        */
      }
      Result &= initdata.CloseSection();
      //#################################################################
      return Result;
    }
    //
    //######################################################
    //######################################################
    //######################################################
    //
    //######################################################
    //  Section - Event - ComPort
    //######################################################
    //
    private delegate void CBSetComPortData(RComPortData comportdata);
    private void SetComPortData(RComPortData comportdata)
    {
      if (this.InvokeRequired)
      {
        CBSetComPortData CB = new CBSetComPortData(SetComPortData);
        Invoke(CB, new object[] { comportdata });
      }
      else
      {
        if (comportdata.IsOpen)
        {
          btnOpenClosePort.Text = TEXT_CLOSE;
          lblSelected.Enabled = false;
          cbxPortsSelectable.Enabled = false;
          SetComPort(comportdata.ComPort);
          EnableControls(comportdata.IsOpen);
          if (comportdata.IsOpen != FComPortOpened)
          {
            FComPortOpened = comportdata.IsOpen;
            if (FComPortOpened)
            {
              // !!!!!!!!!!!!!!!!!!!!! Command-Initialisation !!!
              // !!!!!!!!!!!!!!!!!!!!! Command-Initialisation !!!
              // !!!!!!!!!!!!!!!!!!!!! Command-Initialisation !!!
              // !!!!!!!!!!!!!!!!!!!!! Command-Initialisation !!! FHWDcMotorController.CommandGetSoftwareVersion();
              // !!!!!!!!!!!!!!!!!!!!! Command-Initialisation !!!FHWDcMotorController.CommandGetHardwareVersion();
              // !!!!!!!!!!!!!!!!!!!!! Command-Initialisation !!!
              // !!!!!!!!!!!!!!!!!!!!! Command-Initialisation !!!
              // !!!!!!!!!!!!!!!!!!!!! Command-Initialisation !!!
            }
            else
            {
            }
          }
        }
        else
        {
          if (comportdata.IsOpen)
          {
          }
          else
          {
          }
          btnOpenClosePort.Text = TEXT_OPEN;
          lblSelected.Enabled = true;
          cbxPortsSelectable.Enabled = true;
          if (comportdata.IsOpen != FComPortOpened)
          {
            FComPortOpened = comportdata.IsOpen;
          }
        }
      }
    }
    public RComPortData ComPortData
    {
      set { SetComPortData(value); }
    }


    private void SelfOnComPortDataChanged(Guid comportid,
                                          RComPortData value)
    {
      SetComPortData(value);
    }

    protected Boolean OpenComPort(EComPort comport)
    {
      return (FHWDcMotorController.OpenComPort(comport));
    }

    protected Boolean CloseComPort()
    {
      if (FHWDcMotorController.CloseComPort())
      {
        EnableControls(false);
        FCloser.Dispose();
        FCloser = null;
        return true;
      }
      return false;
    }

    protected Boolean IsComPortOpen()
    {
      return FHWDcMotorController.IsComPortOpen();
    }

    protected Boolean PassivateCloseComPort()
    {
      if (null == FCloser)
      { // donot close
        FCloser = new System.Windows.Forms.Timer();
        FCloser.Interval = 500;
        FCloser.Tick += SelfOnClose;
        FCloser.Start();
        PassivateHardware();
      }
      return true;
    }
    //
    //  Callback-Timer-Function to close ComPort after all stopping-commands
    //
    void SelfOnClose(object sender, EventArgs e)
    {
      if (CommandsFinished())
      {
        FCloser.Stop();
        FCloser.Tick -= SelfOnClose;
        CloseComPort();
      }
    }

    protected Boolean PassivateHardware()
    {
      if (FHWDcMotorController.IsComPortOpen())
      { // Disable FDL-Activity
        FHWDcMotorController.CommandSetSweepStateChannelA(ESweepState.Idle);
        FHWDcMotorController.CommandSetSweepStateChannelB(ESweepState.Idle);
      }
      return true;
    }

    public Boolean CommandsFinished()
    {
      return FHWDcMotorController.CommandsFinished();
    }

    private void btnOpenClosePort_Click(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (!FHWDcMotorController.IsComPortOpen())
        {
          Int32 SI = cbxPortsSelectable.SelectedIndex;
          EComPort CP = CComPort.ComPortEnumerator(cbxPortsSelectable.Items[SI].ToString());
          OpenComPort(CP);
        } else
        {
          PassivateCloseComPort();
        }
      }
    }
    //
    //####################################################
    //	Section - Event - Common / Version
    //####################################################
    //
    private void btnGetVersion_Click(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandGetSoftwareVersion();
        FHWDcMotorController.CommandGetHardwareVersion();
        // !!!!!!!! FHWDcMotorController.CommandGetDeviceType();
      }
    }

    private void btnGetHelp_Click(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandGetHelp();
      }
    }
    //
    //####################################################
    //	Section - Event - ChannelA - Delay
    //####################################################
    //
    private void nudDelayChannelA_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshDelayChannelA.Enabled = true;
    }

    private void hsbDelayChannelA_ValueChanged(object sender, EventArgs e)
    {
      nudDelayChannelA.Value = hsbDelayChannelA.Value;
      tmrRefreshDelayChannelA.Enabled = true;
    }

    private void cbxDelayChannelAUnit_CheckedChanged(object sender, EventArgs e)
    {
      ForceDelayChannelA((Int32)nudDelayChannelA.Value);
    }
    
    private void tmrRefreshDelayChannelA_Tick(object sender, EventArgs e)
    {
      tmrRefreshDelayChannelA.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (DelayChannelA != (Int32)nudDelayChannelA.Value)
        {
          FHWDcMotorController.CommandSetDelayChannelA((Int32)nudDelayChannelA.Value);
        }
      }
    }
    //
    //####################################################
    //	Section - Event - ChannelB - Delay
    //####################################################
    //
    private void nudDelayChannelB_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshDelayChannelB.Enabled = true;
    }

    private void hsbDelayChannelB_ValueChanged(object sender, EventArgs e)
    {
      nudDelayChannelB.Value = hsbDelayChannelB.Value;
      tmrRefreshDelayChannelB.Enabled = true;
    }

    private void cbxDelayChannelBUnit_CheckedChanged(object sender, EventArgs e)
    {
      ForceDelayChannelB((Int32)nudDelayChannelB.Value);
    }

    private void tmrRefreshDelayChannelB_Tick(object sender, EventArgs e)
    {
      tmrRefreshDelayChannelB.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (DelayChannelB != (Int32)nudDelayChannelB.Value)
        {
          FHWDcMotorController.CommandSetDelayChannelB((Int32)nudDelayChannelB.Value);
        }
      }
    }
    //
    //####################################################
    //	Section - Event - LED
    //####################################################
    //
    private void FUCLedRed3Busy_OnActiveChanged(object sender, UCLed.COnActiveChangedData data)
    {
      if (data.Active)
      {
        FHWDcMotorController.CommandSetLed(3);
      }
      else
      {
        FHWDcMotorController.CommandClearLed(3);
      }
    }

    private void FUCLedRed7Error_OnActiveChanged(object sender, UCLed.COnActiveChangedData data)
    {
      if (data.Active)
      {
        FHWDcMotorController.CommandSetLed(7);
      }
      else
      {
        FHWDcMotorController.CommandClearLed(7);
      }
    }
    //
    //##############################################
    //  Section - UCTerminal
    //##############################################
    //
    //  Section - UCTerminal - Callback
    //
    private void UCTerminalOnSendLine(String line)
    {
      FHWDcMotorController.SendLine(line);
    }
    //
    //#########################################################
    //	Section - Event - ChannelA - SweepState
    //#########################################################
    //
    private void rbtSweepStateIdleChannelA_CheckedChanged(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (rbtSweepStateIdleChannelA.Checked)
        {
          FHWDcMotorController.CommandSetSweepStateChannelA(ESweepState.Idle);
        }
      }
    }  

    private void rbtSweepStateLowHighLowHighChannelA_CheckedChanged(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (rbtSweepStateLowHighLowHighChannelA.Checked)
        {
          FHWDcMotorController.CommandSetSweepStateChannelA(ESweepState.LowHighLowHigh);
        }
      }
    }
    private void rbtSweepStateHighLowHighLowChannelA_CheckedChanged(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (rbtSweepStateHighLowHighLowChannelA.Checked)
        {
          FHWDcMotorController.CommandSetSweepStateChannelA(ESweepState.HighLowHighLow);
        }
      }
    }

    private void rbtSweepStateLowHighHighLowChannelA_CheckedChanged(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (rbtSweepStateLowHighHighLowChannelA.Checked)
        {
          FHWDcMotorController.CommandSetSweepStateChannelA(ESweepState.LowHighHighLow);
        }
      }
    }
    private void rbtSweepStateHighLowLowHighChannelA_CheckedChanged(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (rbtSweepStateHighLowLowHighChannelA.Checked)
        {
          FHWDcMotorController.CommandSetSweepStateChannelA(ESweepState.HighLowLowHigh);
        }
      }
    }
    //
    //####################################################
    //	Section - Event - ChannelA - Sweep - LimitLow
    //####################################################
    //
    private void nudSweepLimitLowChannelA_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshSweepLimitLowChannelA.Enabled = true;
    }

    private void tmrRefreshSweepLimitLowChannelA_Tick(object sender, EventArgs e)
    {
      tmrRefreshSweepLimitLowChannelA.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandSetSweepParameterChannelA((Int32)ESweepParameter.LimitLow,
                                                          (Int32)nudSweepLimitLowChannelA.Value);
      }     
    }
    //
    //####################################################
    //	Section - Event - ChannelA - Sweep - LimitHigh
    //####################################################
    //
    private void nudSweepLimitHighChannelA_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshSweepLimitHighChannelA.Enabled = true;
    }

    private void tmrRefreshSweepLimitHighChannelA_Tick(object sender, EventArgs e)
    {
      tmrRefreshSweepLimitHighChannelA.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandSetSweepParameterChannelA((Int32)ESweepParameter.LimitHigh,
                                                          (Int32)nudSweepLimitHighChannelA.Value);
      }     
    }
    //
    //#########################################################
    //	Section - Event - ChannelA - Sweep - StepDelta
    //#########################################################
    //
    private void nudSweepStepDeltaChannelA_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshSweepStepDeltaChannelA.Enabled = true;
    }

    private void tmrRefreshSweepStepDeltaChannelA_Tick(object sender, EventArgs e)
    {
      tmrRefreshSweepStepDeltaChannelA.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandSetSweepParameterChannelA((Int32)ESweepParameter.StepDelta,
                                                          (Int32)nudSweepStepDeltaChannelA.Value);
      }
    }
    //
    //#########################################################
    //	Section - Event - ChannelA - Sweep - StepCount
    //#########################################################
    //
    private void nudSweepStepCountChannelA_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshSweepStepCountChannelA.Enabled = true;
    }

    private void tmrRefreshSweepStepCountChannelA_Tick(object sender, EventArgs e)
    {
      tmrRefreshSweepStepCountChannelA.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandSetSweepParameterChannelA((Int32)ESweepParameter.StepCount,
                                                          (Int32)nudSweepStepCountChannelA.Value);
      }
    }
    //
    //#########################################################
    //	Section - Event - ChannelA - Sweep - RepetitionCount
    //#########################################################
    //
    private void nudSweepRepetitionCountChannelA_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshSweepRepetitionCountChannelA.Enabled = true;
    }

    private void tmrRefreshSweepRepetitionCountChannelA_Tick(object sender, EventArgs e)
    {
      tmrRefreshSweepRepetitionCountChannelA.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandSetSweepParameterChannelA((Int32)ESweepParameter.RepetitionCount,
                                                          (Int32)nudSweepRepetitionCountChannelA.Value);
      }
    }
    //
    //#########################################################
    //	Section - Event - ChannelB - SweepState
    //#########################################################
    //
    private void rbtSweepStateIdleChannelB_CheckedChanged(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (rbtSweepStateIdleChannelB.Checked)
        {
          FHWDcMotorController.CommandSetSweepStateChannelB(ESweepState.Idle);
        }
      }
    }

    private void rbtSweepStateLowHighLowHighChannelB_CheckedChanged(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (rbtSweepStateLowHighLowHighChannelB.Checked)
        {
          FHWDcMotorController.CommandSetSweepStateChannelB(ESweepState.LowHighLowHigh);
        }
      }
    }
    private void rbtSweepStateHighLowHighLowChannelB_CheckedChanged(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (rbtSweepStateHighLowHighLowChannelB.Checked)
        {
          FHWDcMotorController.CommandSetSweepStateChannelB(ESweepState.HighLowHighLow);
        }
      }
    }

    private void rbtSweepStateLowHighHighLowChannelB_CheckedChanged(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (rbtSweepStateLowHighHighLowChannelB.Checked)
        {
          FHWDcMotorController.CommandSetSweepStateChannelB(ESweepState.LowHighHighLow);
        }
      }
    }
    private void rbtSweepStateHighLowLowHighChannelB_CheckedChanged(object sender, EventArgs e)
    {
      if (FHWDcMotorController is CHWDcMotorController)
      {
        if (rbtSweepStateHighLowLowHighChannelB.Checked)
        {
          FHWDcMotorController.CommandSetSweepStateChannelB(ESweepState.HighLowLowHigh);
        }
      }
    }
    //
    //####################################################
    //	Section - Event - ChannelB - Sweep - LimitLow
    //####################################################
    //
    private void nudSweepLimitLowChannelB_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshSweepLimitLowChannelB.Enabled = true;
    }

    private void tmrRefreshSweepLimitLowChannelB_Tick(object sender, EventArgs e)
    {
      tmrRefreshSweepLimitLowChannelB.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandSetSweepParameterChannelB((Int32)ESweepParameter.LimitLow,
                                                          (Int32)nudSweepLimitLowChannelB.Value);
      }
    }
    //
    //####################################################
    //	Section - Event - ChannelB - Sweep - LimitHigh
    //####################################################
    //
    private void nudSweepLimitHighChannelB_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshSweepLimitHighChannelB.Enabled = true;
    }

    private void tmrRefreshSweepLimitHighChannelB_Tick(object sender, EventArgs e)
    {
      tmrRefreshSweepLimitHighChannelB.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandSetSweepParameterChannelB((Int32)ESweepParameter.LimitHigh,
                                                          (Int32)nudSweepLimitHighChannelB.Value);
      }
    }
    //
    //#########################################################
    //	Section - Event - ChannelB - Sweep - StepDelta
    //#########################################################
    //
    private void nudSweepStepDeltaChannelB_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshSweepStepDeltaChannelB.Enabled = true;
    }

    private void tmrRefreshSweepStepDeltaChannelB_Tick(object sender, EventArgs e)
    {
      tmrRefreshSweepStepDeltaChannelB.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandSetSweepParameterChannelB((Int32)ESweepParameter.StepDelta,
                                                          (Int32)nudSweepStepDeltaChannelB.Value);
      }
    }
    //
    //#########################################################
    //	Section - Event - ChannelB - Sweep - StepCount
    //#########################################################
    //
    private void nudSweepStepCountChannelB_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshSweepStepCountChannelB.Enabled = true;
    }

    private void tmrRefreshSweepStepCountChannelB_Tick(object sender, EventArgs e)
    {
      tmrRefreshSweepStepCountChannelB.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandSetSweepParameterChannelB((Int32)ESweepParameter.StepCount,
                                                          (Int32)nudSweepStepCountChannelB.Value);
      }
    }
    //
    //#########################################################
    //	Section - Event - ChannelB - Sweep - RepetitionCount
    //#########################################################
    //
    private void nudSweepRepetitionCountChannelB_ValueChanged(object sender, EventArgs e)
    {
      tmrRefreshSweepRepetitionCountChannelB.Enabled = true;
    }

    private void tmrRefreshSweepRepetitionCountChannelB_Tick(object sender, EventArgs e)
    {
      tmrRefreshSweepRepetitionCountChannelB.Stop();
      if (FHWDcMotorController is CHWDcMotorController)
      {
        FHWDcMotorController.CommandSetSweepParameterChannelB((Int32)ESweepParameter.RepetitionCount,
                                                          (Int32)nudSweepRepetitionCountChannelB.Value);
      }
    }


  }
}


