﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ComPort;
using HWDcMotorController;
//
namespace UCDcMotorController
{
  public partial class CUCDcMotorController : CUCDcMotorControllerBase
  {
    //
    //--------------------------------------
    //	Section - Constant
    //--------------------------------------
    //

    //
    //--------------------------------------
    //	Section - Type
    //--------------------------------------
    //

    //
    //--------------------------------------
    //	Section - Member
    //--------------------------------------
    //

    //
    //--------------------------------------
    //	Section - Constructor
    //--------------------------------------
    //    
    public CUCDcMotorController()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------
    //	Section - Management
    //--------------------------------------
    //
    public new Boolean OpenComPort(EComPort comport)
    {
      try
      {
        return base.OpenComPort(comport);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public new Boolean CloseComPort()
    {
      try
      {
        return base.CloseComPort();
      }
      catch (Exception)
      {
        return false;
      }
    }

    public new Boolean IsComPortOpen()
    {
      try
      {
        return base.IsComPortOpen();
      }
      catch (Exception)
      {
        return false;
      }
    }

    public new Boolean PassivateHardware()
    {
      try
      {
        return base.PassivateHardware();
      }
      catch (Exception)
      {
        return false;
      }
    }

    /*????
    public new Boolean CommandSetDeviceStateIdle()
    {
      try
      {
        return base.CommandSetDeviceStateIdle();
      }
      catch (Exception)
      {
        return false;
      }
    }*/

  }
}
