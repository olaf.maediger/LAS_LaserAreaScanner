﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCMultiColorList
{
  //
  //##################################################
  //  Segment - Main - CUCMultiColorList
  //##################################################
  //
  public partial class CUCMultiColorList : UserControl
  { //
    //-------------------------------
    //  Segment - Constant
    //-------------------------------
    // Color Background
    private const Byte COLOR_BACKGROUND_A = 0xFF;
    private const Byte COLOR_BACKGROUND_R = 0xFF;
    private const Byte COLOR_BACKGROUND_G = 0xFF;
    private const Byte COLOR_BACKGROUND_B = 0xDD;
    // Color Focus
    private const Byte COLOR_FOCUS_A = 0xFF;
    private const Byte COLOR_FOCUS_R = 0xAA;
    private const Byte COLOR_FOCUS_G = 0xFF;
    private const Byte COLOR_FOCUS_B = 0xAA;
    //
    //-------------------------------
    //  Segment - Field
    //-------------------------------
    //
    private CColorlist FColorlist;
    //
    //-------------------------------
    //  Segment - Constructor
    //-------------------------------
    //
    public CUCMultiColorList()
    {
      InitializeComponent();
      //
      FColorlist = new CColorlist();
      //
      lbxView.DrawMode = DrawMode.OwnerDrawFixed; // !!!!!!!!!!!!!!!!
      lbxView.DrawItem += new DrawItemEventHandler(lbxView_DrawItem);
      lbxView.BackColor = Color.FromArgb(COLOR_BACKGROUND_A,
                                         COLOR_BACKGROUND_R,
                                         COLOR_BACKGROUND_G,
                                         COLOR_BACKGROUND_B);
      lbxView.SelectedIndexChanged += new EventHandler(lbxView_SelectedIndexChanged);
      //
      // debug Add(Color.Red, "Red");
      // debug Add(Color.Green, "Green");
      // debug Add(Color.Blue, "Blue");
    }
    //
    //-------------------------------
    //  Segment - Event
    //-------------------------------
    //
    private void lbxView_DrawItem(object sender, DrawItemEventArgs e)
    {
      Int32 ItemIndex = e.Index;
      if (ItemIndex == lbxView.SelectedIndex)
      {
        Brush BR = new SolidBrush(Color.FromArgb(COLOR_FOCUS_A,
                                                 COLOR_FOCUS_R,
                                                 COLOR_FOCUS_G,
                                                 COLOR_FOCUS_B));
        e.Graphics.FillRectangle(BR, e.Bounds);
      } else
      {
        e.DrawBackground();
      }
      if (0 <= ItemIndex)
      {
        Color C = FColorlist[ItemIndex];
        Brush B = new SolidBrush(C);
        String Line = (String)lbxView.Items[ItemIndex];
        e.Graphics.DrawString(Line, e.Font, B, e.Bounds, StringFormat.GenericDefault);
        e.DrawFocusRectangle();
      }
    }

    private void lbxView_SelectedIndexChanged(object sender, EventArgs e)
    {
      lbxView.Invalidate();
    }
    //
    //-------------------------------
    //  Segment - Helper
    //-------------------------------
    //
    private void ClearAllLines()
    {
      FColorlist.Clear();
      lbxView.Items.Clear();
    }

    private void AddColoredLine(Color color, String line)
    {
      FColorlist.Add(color);
      lbxView.Items.Add(line);
      lbxView.SelectedIndex = lbxView.Items.Count - 1;
    }
    //
    //-------------------------------
    //  Segment - Menu
    //-------------------------------
    //
    private void mitClearAllLines_Click(object sender, EventArgs e)
    {
      ClearAllLines();
    }

    private void mitAddSpaceline_Click(object sender, EventArgs e)
    {
      Color C = Color.FromArgb(COLOR_BACKGROUND_A, COLOR_BACKGROUND_R,
                               COLOR_BACKGROUND_G, COLOR_BACKGROUND_B);
      AddColoredLine(C, "");
    }
    //
    //-------------------------------
    //  Segment - Management
    //-------------------------------
    //
    public void Clear()
    {
      ClearAllLines();
    }

    public void Add(Color color, String line)
    {
      AddColoredLine(color, line);
    }



  }
}
