﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  class CGetLed : CStatement
  {
    public const String INIT_NAME = "GetLed";
		public const String COMMAND_TEXT = "GLD";

		public CGetLed(CStatementlist parent,
								   Byte index, 
									 CNotifier notifier,
									 CDevice device,
									 CComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, index),
             notifier, device, comport)
    {
      SetParent(this);
      //SetOnStatementExecuteResponse(SelfOnSerialResponseReceived);
    }

    public void SelfOnSerialResponseReceived(Guid comportid,
                                             String line)
    {
      /*base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
			String[] Tokenlist = line.Split(' ');
      if (3 < Tokenlist.Length)
      {
        if ("1" == Tokenlist[3])
        {
          StatementlistParent.Library.RefreshLed(Tokenlist[2], true);
        }
        else
        {
          StatementlistParent.Library.RefreshLed(Tokenlist[2], false);
        }
      }*/
    }

  }
}
