﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  public class CGetSweepStateChannelA : CStatement
  {
    public const String INIT_NAME = "GetSweepStateChannelA";
    public const String COMMAND_TEXT = "GWA";

    public CGetSweepStateChannelA(CStatementlist parent,
                                  CNotifier notifier,
                                  CDevice device,
                                  CComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0}", COMMAND_TEXT),
             notifier, device, comport)
    {
      SetParent(this);
      //SetOnStatementExecuteResponse(SelfOnSerialResponseReceived);
    }

    public void SelfOnSerialResponseReceived(Guid comportid,
                                             String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = line.Split(' ');
      if (2 < Tokenlist.Length)
      {
        // NC ESweepState SS = CIFTerminalRS232.StringIndexToSweepState(Tokenlist[2]);
        //!!!!!!!!!!!!!StatementlistParent.Library.RefreshSweepStateChannelA(SS);
      }
    }
  }
}
