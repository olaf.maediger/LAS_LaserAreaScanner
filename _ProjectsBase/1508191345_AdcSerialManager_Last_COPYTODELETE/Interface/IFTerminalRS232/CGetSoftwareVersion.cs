﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using HWComPort;

namespace IFTerminalRS232
{
	public class CGetSoftwareVersion : CStatement
	{
    public const String INIT_NAME = "GetSoftwareVersion";
    public const String COMMAND_TEXT = "GSV";

		public CGetSoftwareVersion(CStatementlist parent,
											 CNotifier notifier,
											 CDevice device,
											 CComPort comport)
      : base(parent, INIT_NAME, COMMAND_TEXT, notifier, device, comport)
    {
      SetParent(this);
      //SetOnStatementExecuteResponse(SelfOnStatementResponseReceived);
    }

    public void SelfOnStatementResponseReceived(Guid comportid,
                                              String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
			Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
			String[] Tokenlist = line.Split(' ');
			if (2 < Tokenlist.Length)
			{
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Statementlist.Library.RefreshSoftwareVersion(Tokenlist[2]);
			}
		}

	}
}
