﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace IFTerminalRS232
{
  public class CVariablelist : List<CVariable>
  {
    private CNotifier FNotifier;

    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public new Boolean Add(CVariable variable)
    {
      if (variable is CVariable)
      {
        base.Add(variable);
        variable.Debug();
        return true;
      }
      return false;
    }


    public CVariable GetVariable(String variablename)
    {
      for (Int32 VI = Count - 1; 0 <= VI; VI--)
      {
        CVariable Variable = base[VI];
        if (variablename == Variable.Name)
        {
          return Variable;
        }
      }
      return null;
    }

    public void Debug()
    {
      FNotifier.Write(String.Format("*** Variablelist[{0}]:", Count));
      for (Int32 VI = 0; VI < Count; VI++)
      {
        CVariable Variable = base[VI];
        Variable.Debug();
      }
    }


  }
}
