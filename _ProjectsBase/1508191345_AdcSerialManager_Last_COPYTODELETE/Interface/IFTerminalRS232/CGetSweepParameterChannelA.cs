﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using HWComPort;
//
namespace IFTerminalRS232
{
  public class CGetSweepParameterChannelA : CStatement
  {
    public const String INIT_NAME = "GetSweepParameterChannelA";
    public const String COMMAND_TEXT = "GSA";

    private Int32 FIndex;

    public CGetSweepParameterChannelA(CStatementlist parent,
                                      Int32 index,
                                      CNotifier notifier,
                                      CDevice device,
                                      CComPort comport)
      : base(parent, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, index),
             notifier, device, comport)
    {
      SetParent(this);
      //SetOnStatementExecuteResponse(SelfOnSerialResponseReceived);
      FIndex = index;
    }

    public void SelfOnSerialResponseReceived(Guid comportid,
                                             String line)
    {
      /*base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
      Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = line.Split(' ');
      if (3 < Tokenlist.Length)
      {
        if (FIndex == Int32.Parse(Tokenlist[2]))
        {
          Int32 Value = Int32.Parse(Tokenlist[3]);
          StatementlistParent.Library.RefreshSweepParameterChannelA(FIndex, Value);
        }
      }*/
    }
  }
}
