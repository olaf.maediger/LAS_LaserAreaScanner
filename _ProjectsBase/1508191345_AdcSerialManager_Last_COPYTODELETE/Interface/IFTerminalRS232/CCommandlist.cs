﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UCNotifier;
using HWComPort;

namespace IFTerminalRS232
{
  // NC 	public delegate void DOnStatementExecuteEnd(CStatement command);
  // NC public delegate void DOnSerialResponseReceived(Guid comportid,
  // NC                                          String response);

  public delegate void DOnReceived(String line);

  public delegate void DOnComPortDataTransmitted(String data);
  public delegate void DOnComPortDataReceived(String data);

  public class CStatementlist : List<CStatement>
	{
    private CIFTerminalRS232Base FLibrary;
		private CNotifier FNotifier;
   // NC  private Int32 FStatementIndex;
    private CVariablelist FVariablelist;

    public CStatementlist(CIFTerminalRS232Base library)
		{
      FLibrary = library;
      FVariablelist = new CVariablelist();
		}

    public CIFTerminalRS232Base Library
    {
      get { return FLibrary; }
    }

    public CVariablelist Variablelist
    {
      get { return FVariablelist; }
    }

		public void SetNotifier(CNotifier notifier)
		{
			FNotifier = notifier;
		}

    public new Boolean Add(CStatement command)
    {
      // debug 
      FNotifier.Write("Add Statement: " + command.ToString());
      base.Add(command);
      return (0 < this.Count);
    }

    public new Boolean Clear()
    {
      base.Clear();
      return true;
    }

    public CStatement GetStatementActual()
    {
      CStatement Result = null;
      if (0 < Count)
      {
        Result = base[0];
      }
      if (0 == Count)
      {
        AbortExecution();
      }
      return Result;
    }

    public Boolean StartExecution()
    { // debug Console.WriteLine("Statementlist.StartExecution");
      if (0 < Count)
      {
        CStatement Statement = base[0];
        Statement.StartExecution(SelfOnStatementExecuteEnd);
        return true;
      }
      return false;
    }

    public Boolean AbortExecution()
    { // debug Console.WriteLine("Statementlist.AbortExecution");
      CStatement Statement = null;
			if (0 < Count)
			{
        Statement = base[0];
        if (Statement is CStatement)
				{
					Statement.Abort();
				}
			}
      Clear();
			return true;
		}

		public void SelfOnStatementExecuteEnd(CStatement command)
		{ // prepare next command
      CStatement Statement = null;
      if (0 < Count)
      {
        Statement = base[0];
        base.Remove(Statement);
      }
      if (0 < Count)
      {
        Statement = base[0];
        if (Statement is CStatement)
        {
          String Line = String.Format("ActivateStatement[{0}]", Statement.ToString());
          FNotifier.Write(Line);
          Statement.StartExecution(SelfOnStatementExecuteEnd);
        }
      }
		}


    public void DebugVariablelist()
    {
      FVariablelist.Debug();
    }


    /* ?????public void SetOnSerialResponseReceived(DOnSerialResponseReceived value)
    {
      FLibrary.SetOnSerialResponseReceived(value);
    }*/

    /*ublic void SetOnRXDLineReceived(DOnRXDLineReceived onrxdlinereceived)
    {
      FLibrary.SetOnRXDLineReceived(onrxdlinereceived);
    }*/

    /* NC public Boolean LoadProgramFromXml(String programentry)
    {

    }*/

	}
}
