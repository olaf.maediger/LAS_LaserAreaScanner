﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Task;
using HWComPort;
//
namespace Command
{
  public enum ECommandState
  {
    Error = -1,
    Init = 0,
    Begin = 1,
    Busy = 2,
    Response = 3,
    Abort = 4,
    End = 5
  };

  public delegate void DOnCommandResponse(String text);

  public abstract class CCommand
  {
    //
    //--------------------------------------
    //	Section - Constant
    //--------------------------------------
    //
    static public String HEADER_LIBRARY = "MHFDL";
    //
    //--------------------------------------
    //	Section - Field
    //--------------------------------------
    //
    private String FName;
    private CNotifier FNotifier; // only reference
    private ECommandState FState;
    private CComPort FComPort; // only reference
    private CTask FTask;
    private DOnCommandResponse FOnCommandResponse;
    //
    //--------------------------------------
    //	Section - Constructor
    //--------------------------------------
    //
    public CCommand(CNotifier notifier,
                    CComPort comport,
                    String name)
    {
      FName = name;
      FNotifier = notifier;
      FComPort = comport;
      FComPort.SetOnLineReceived(ComPortOnLineReceived);
      FTask = null;
      FState = ECommandState.Init;
    }
    //
    //--------------------------------------
    //	Section - Property
    //--------------------------------------
    //
    protected void SetOnCommandResponse(DOnCommandResponse value)
    {
      FOnCommandResponse = value;
    }
    //
    //--------------------------------------
    //	Section - Callback
    //--------------------------------------
    //
    private void ComPortOnLineReceived(Guid comportid, String line)
    {

    }

    protected virtual void OnSerialResponseReceived(String text)
    {
      String Line = String.Format("{0}: {1}", FName, text);
      FNotifier.Write(HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = text.Split(' ');
      if (1 < Tokenlist.Length)
      {
        //Statementlist.Library.RefreshHelp();//Tokenlist);
      }
    }

    private void OnExecutionBegin(RTaskData data)
    {
      FState = ECommandState.Begin;
    }

    private void OnExecutionBusy(RTaskData data)
    {
      FState = ECommandState.Busy;
    }

    private void OnExecutionEnd(RTaskData data)
    {
      FState = ECommandState.End;
    }

    private void OnExecutionAbort(RTaskData data)
    {
      FState = ECommandState.Abort;
    }
    //
    //--------------------------------------
    //	Section - Management
    //--------------------------------------
    //
    public Boolean Start(String name)
    {
      if (null == FTask)
      {
        FTask = new CTask(name, OnExecutionBegin, OnExecutionBusy, OnExecutionEnd, OnExecutionAbort);
        if (FTask.Begin())
        {
          return true;
        }
      }
      return false;
    }

    public Boolean Abort()
    {
      if (FTask is CTask)
      {
        return FTask.Abort();
      }
      return false;
    }

  }
}
