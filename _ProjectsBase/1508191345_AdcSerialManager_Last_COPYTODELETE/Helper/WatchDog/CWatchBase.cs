﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace WatchDog
{ //
  //--------------------------------------------------------------
  //  Segment - Type
  //--------------------------------------------------------------
  //

  //
  //################################################################
  //  Segment - CPulseDog
  //################################################################
  //
  public abstract class CWatchBase
  { //
    //--------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------
    //
    protected Timer FTimer;
    //
    //--------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------
    //
    public CWatchBase(Int32 refreshtime)
    {
      FTimer = new Timer();
      FTimer.Tick += new EventHandler(OnTimerTick);
      FTimer.Stop();
      FTimer.Interval = refreshtime;
    }
    //
    //--------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------
    //
    protected abstract void OnTimerTick(object sender, EventArgs arguments);
    //
    //--------------------------------------------------------------
    //  Segment - Helper
    //--------------------------------------------------------------
    //
    protected void Start()
    {
      if (!FTimer.Enabled)
      {
        // Force Pulse at Start:
        OnTimerTick(this, null);
        FTimer.Start();
      }
    }

    protected void Stop()
    {
      if (FTimer.Enabled)
      {
        FTimer.Stop();
      }
    }



  }
}
