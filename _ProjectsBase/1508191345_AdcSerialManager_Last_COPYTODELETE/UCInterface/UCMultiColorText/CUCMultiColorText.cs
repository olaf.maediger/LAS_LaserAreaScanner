﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCMultiColorText
{
  //
  //##################################################
  //  Segment - Main - CUCMultiColorText
  //##################################################
  //
  public partial class CUCMultiColorText : UserControl
  { //
    //-------------------------------
    //  Segment - Constant
    //-------------------------------
    // Color Background
    private const Byte COLOR_BACKGROUND_A = 0xFF;
    private const Byte COLOR_BACKGROUND_R = 0xFF;
    private const Byte COLOR_BACKGROUND_G = 0xFF;
    private const Byte COLOR_BACKGROUND_B = 0xDD;
    // Color Focus
    private const Byte COLOR_FOCUS_A = 0xFF;
    private const Byte COLOR_FOCUS_R = 0xAA;
    private const Byte COLOR_FOCUS_G = 0xFF;
    private const Byte COLOR_FOCUS_B = 0xAA;
    //
    //-------------------------------
    //  Segment - Field
    //-------------------------------
    //
    RichTextBox FRichTextBox;
    // NC private List<Int32> FStartIndices;
    // NC private List<Int32> FStopIndices;
    //
    //-------------------------------
    //  Segment - Constructor
    //-------------------------------
    //
    public CUCMultiColorText()
    {
      InitializeComponent();
      //
      FRichTextBox = new RichTextBox();
      this.Controls.Add(FRichTextBox);
      //
      FRichTextBox.BackColor = Color.FromArgb(COLOR_BACKGROUND_A,
                                         COLOR_BACKGROUND_R,
                                         COLOR_BACKGROUND_G,
                                         COLOR_BACKGROUND_B);
      //
      FRichTextBox.SelectionColor = Color.FromArgb(COLOR_FOCUS_A,
                                              COLOR_FOCUS_R,
                                              COLOR_FOCUS_G,
                                              COLOR_FOCUS_B);
      //
      FRichTextBox.Text = "";
    }
    //
    //-------------------------------
    //  Segment - Event
    //-------------------------------
    //

    //
    //-------------------------------
    //  Segment - Helper
    //-------------------------------
    //
    private void ClearAll()
    {
      FRichTextBox.Clear();
    }

    private void AddColoredText(Color color, String text)
    {
      FRichTextBox.AppendText(text);
      try
      {
        //rtbView.SelectionColor = color;
        //rtbView.SelectedText = text;
      }
      catch (Exception)
      {
      }
      // NC FStopIndices.Add(rtbView.TextLength - 1);
    }
    //
    //-------------------------------
    //  Segment - Menu
    //-------------------------------
    //
    private void mitClearAllLines_Click(object sender, EventArgs e)
    {
      ClearAll();
    }

    private void mitAddSpaceline_Click(object sender, EventArgs e)
    {
      Color C = Color.FromArgb(COLOR_BACKGROUND_A, COLOR_BACKGROUND_R,
                               COLOR_BACKGROUND_G, COLOR_BACKGROUND_B);
      AddColoredText(C, "");
    }
    //
    //-------------------------------
    //  Segment - Management
    //-------------------------------
    //
    public void Clear()
    {
      ClearAll();
    }

    public void Add(Color color, String text)
    {
      AddColoredText(color, text);
    }

    public void EnableText(Boolean enable)
    {
      FRichTextBox.Enabled = enable;
    }


  }
}
