#include "Serial.h"
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
void CSerialBase::WriteNewLine()
{
  WriteText(PROMPT_NEWLINE);  
}
  
void CSerialBase::WritePrompt()
{
  WriteText(PROMPT_INPUT);
}
  
void CSerialBase::WriteAnswer()
{
  WriteText(PROMPT_ANSWER);
}
//
//----------------------------------------------------
//  Segment - CSerialZ
//----------------------------------------------------
//
bool CSerialZ::Open()
{
  FPSerial->begin(115200);
  return true;
}
  
void CSerialZ::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialZ::WriteText(char *text)
{
  FPSerial->write(text);
}  

void CSerialZ::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  

int CSerialZ::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialZ::ReadCharacter()
{
  return (char)FPSerial->read();
}
//
//----------------------------------------------------
//  Segment - CSerialS
//----------------------------------------------------
//
bool CSerialS::Open()
  {
    FPSerial->begin(115200);
    return true;
  }
  
void CSerialS::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialS::WriteText(char *text)
{
  FPSerial->write(text);
}  

void CSerialS::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  

int CSerialS::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialS::ReadCharacter()
{
  return (char)FPSerial->read();
}//
//----------------------------------------------------
//  Segment - CSerialU
//----------------------------------------------------
//
bool CSerialU::Open()
  {
    FPSerial->begin(115200);
    return true;
  }
  
void CSerialU::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialU::WriteText(char *text)
{
  FPSerial->write(text);
}  

void CSerialU::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  

int CSerialU::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialU::ReadCharacter()
{
  return (char)FPSerial->read();
}//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
CSerial::CSerial(UARTClass *serial)
{
  CSerialZ *PSerialZ = (CSerialZ*)new CSerialZ(serial);
  FPSerial = (CSerialBase*)PSerialZ;
}

CSerial::CSerial(UARTClass &serial)
{
  CSerialZ *PSerialZ = (CSerialZ*)new CSerialZ(&serial);
  FPSerial = (CSerialBase*)PSerialZ;
}

CSerial::CSerial(USARTClass *serial)
{
  CSerialS *PSerialS = (CSerialS*)new CSerialS(serial);
  FPSerial = (CSerialBase*)PSerialS;
}

CSerial::CSerial(USARTClass &serial)
{
  CSerialS *PSerialS = (CSerialS*)new CSerialS(&serial);
  FPSerial = (CSerialBase*)PSerialS;
}

CSerial::CSerial(Serial_ *serial)
{ 
  CSerialU *PSerialU = (CSerialU*)new CSerialU(serial);
  FPSerial = (CSerialBase*)PSerialU;
}

CSerial::CSerial(Serial_ &serial)
{ 
  CSerialU *PSerialU = (CSerialU*)new CSerialU(&serial);
  FPSerial = (CSerialBase*)PSerialU;
}

bool CSerial::Open()
{
  return FPSerial->Open();
}

void CSerial::WriteCharacter(char character)
{
  FPSerial->WriteCharacter(character);
}

void CSerial::WriteText(char *text)
{
  FPSerial->WriteText(text);
}

void CSerial::WriteLine(char *text)
{
  FPSerial->WriteLine(text);
}

void CSerial::WriteNewLine()
{
  FPSerial->WriteNewLine();
}

void CSerial::WriteAnswer()
{
  FPSerial->WriteAnswer();
}

void CSerial::WritePrompt()
{
  FPSerial->WritePrompt();
}

int CSerial::GetRxdByteCount()
{
  FPSerial->GetRxdByteCount();
}

char CSerial::ReadCharacter()
{
  return (char)FPSerial->ReadCharacter();
}
