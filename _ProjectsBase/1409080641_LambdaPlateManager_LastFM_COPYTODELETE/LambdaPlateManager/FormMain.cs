﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
//
using Initdata;
using HWComPort;
using NLStatement;
using UCNotifier;
using UCComPort;
using UCAngle90Deg;
using UCLambdaPlateController;
//
namespace LambdaPlateManager
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  {
    //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		

    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private Random FRandom;
    // private CUCLambdaPlateController FUCLambdaPlateControllerOld;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	
    private void InstantiateProgrammerControls()
    {
      //
      CInitdata.Init();
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      tbcMain.Controls.Remove(tbpSerialNumberProductKey);
      //
      FUCComPort.SetNotifier(FUCNotifier);
      FUCComPort.SetOnOpenCloseChanged(UCComPortOnOpenCloseChanged);
      //!!!!!!!!!!!!!!??????????????????FUCComPort.SetOnComPortLineReceived(UCComPortOnComPortLineReceived);
      FUCComPort.SetOnAddStatement(UCComPortOnAddStatement);
      FUCComPort.RefreshComPortControls();
      //
      FUCProgram.SetNotifier(FUCNotifier);
      //
      FUCTerminal.SetNotifier(FUCNotifier);
      FUCTerminal.SetOnSendData(UCTerminalOnSendData);
      //
      FUCLambdaPlateControllerOld.SetNotifier(FUCNotifier);
      FUCLambdaPlateControllerOld.SetOnValueChanged(UCLambdaPlateControllerOnValueChanged);
      FUCLambdaPlateControllerOld.SetOnModeMicroStepChanged(UCLambdaPlateControllerOnModeMicroStepChanged);
      FUCLambdaPlateControllerOld.SetOnPeriodHalfChanged(UCLambdaPlateControllerOnPeriodHalfChanged);
      FUCLambdaPlateControllerOld.SetResolution(1.8);
      //
      FUCLambdaPlateControllerNew.SetNotifier(FUCNotifier);
      FUCLambdaPlateControllerNew.SetOnAnglePresetChanged(UCLambdaPlateControllerOnAnglePresetChanged);
      FUCLambdaPlateControllerNew.SetOnAngleActualChanged(UCLambdaPlateControllerOnAngleActualChanged);
      FUCLambdaPlateControllerNew.SetOnAngleDeltaChanged(UCLambdaPlateControllerOnAngleDeltaChanged);
      FUCLambdaPlateControllerNew.SetOnModeMicroStepChanged(UCLambdaPlateControllerOnModeMicroStepChanged);
      FUCLambdaPlateControllerNew.SetOnPeriodHalfChanged(UCLambdaPlateControllerOnPeriodHalfChanged);
      FUCLambdaPlateControllerNew.SetOnEnableMotion(UCLambdaPlateControllerOnEnableMotion);
      FUCLambdaPlateControllerNew.SetResolution(1.8);       
      //
      tmrStartup.Interval = 1000;
      // tmrStartup.Start();
      //
      FRandom = new Random();
      //
      UCComPortOnOpenCloseChanged(false);
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //     
      FUCComPort.CloseComPort();
      FUCLambdaPlateControllerOld.Close();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result &= FUCComPort.LoadInitdata(initdata);
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Result &= FUCComPort.SaveInitdata(initdata);
      //
      return Result;
    }


    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUCComPort
    //------------------------------------------------------------------------
    // 
    private void UCComPortOnOpenCloseChanged(Boolean opened)
    {
      RUCComPortData UCComPortData;
      FUCComPort.GetUCComPortData(out UCComPortData);
      FUCTerminal.EnableControls(UCComPortData.ComPortData.IsOpen);
      // ??? FUCProgram.EnableControls(UCComPortData.ComPortData.IsOpen);
      FUCLambdaPlateControllerNew.EnableControls(UCComPortData.ComPortData.IsOpen);
      FUCLambdaPlateControllerOld.EnableControls(UCComPortData.ComPortData.IsOpen);
    }

    private void UCComPortOnAddStatement(CStatement statement)
    {
      FUCProgram.AddStatement(statement);
      FUCProgram.StartExecution();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUCTerminal
    //------------------------------------------------------------------------
    // 
    private void UCTerminalOnSendData(String data,
                                      Int32 delaycharacter,
                                      Int32 delaycr,
                                      Int32 delaylf)
    {
      FUCComPort.AddStatementTransmitTextDelay(data,
                                             delaycharacter,
                                             delaycr,
                                             delaylf);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCLambdaPlateController
    //------------------------------------------------------------------------
    // 
    private void UCLambdaPlateControllerOnValueChanged(Double valueactual, 
                                                       Double valuepreset,
                                                       Double valuedelta)
    {
      String StatementLine = String.Format("MA {0}\r", valueactual);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUCComPort.Write(StatementLine);
    }


    private void UCLambdaPlateControllerOnAnglePresetChanged(Double value)
    {
      String StatementLine = String.Format("MA {0}\r", value);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUCComPort.Write(StatementLine);
    }
    private void UCLambdaPlateControllerOnAngleActualChanged(Double value)
    {
      // NC 
    }

    private void UCLambdaPlateControllerOnAngleDeltaChanged(Double value)
    {
      // NC 
    }

    private void UCLambdaPlateControllerOnModeMicroStepChanged(EModeMicroStep value)
    {
      String StatementLine = String.Format("SMM {0}\r", (Int32)value);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUCComPort.Write(StatementLine);
    }

    private void UCLambdaPlateControllerOnPeriodHalfChanged(Int32 periodhalf,
                                                            Int32 period,
                                                            Double frequency)
    {
      String StatementLine = String.Format("SPH {0}\r", periodhalf);
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUCComPort.Write(StatementLine);
    }

    private void UCLambdaPlateControllerOnEnableMotion(Boolean enable)
    {
      String StatementLine = "EM ";
      if (enable)
      {
        StatementLine += String.Format("1\r");
      }
      else
      {
        StatementLine += String.Format("0\r");
      }
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FUCComPort.Write(StatementLine);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCComPort
    //------------------------------------------------------------------------
    /*/ //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    private void UCComPortOnOpenComPort(CComPort comport)
    {
      FUCLambdaPlateControllerOld.Open();
      FUCLambdaPlateControllerNew.Open();
    }

    private void UCComPortOnCloseComPort(CComPort comport)
    {
      FUCLambdaPlateControllerOld.Close();
      FUCLambdaPlateControllerNew.Close();
    }
    
    private void UCComPortOnComPortLineReceived(RComPortData comportdata,
                                                String rxdline)
    {
      try
      {
        Char[] Delimiter = new Char[] { ' ', '!', '\r', '\n', '>' };
        String[] TokenList = rxdline.Split(Delimiter, StringSplitOptions.RemoveEmptyEntries);
        if (TokenList is String[])
        {
          if (0 < TokenList.Length)
          {
            String Statement = TokenList[0];
            if (1 < TokenList.Length)
            {
              String SParameter1 = TokenList[1];
              if (2 < TokenList.Length)
              { // <command> <parameter1> <parameter2>
                String SParameter2 = TokenList[2];
                return;
              }
              else
              { // <command> parameter1>
                if ("PA" == Statement)
                {
                  // !!!!!!!!!!!!!!!! FUCLambdaPlateControllerOld.ResponsePositionAbsolute(SParameter1);
                  FUCLambdaPlateControllerNew.ResponsePositionAbsolute(SParameter1);
                  // debug                   Console.WriteLine(rxdline);
                  return;
                }
                if ("PT" == Statement)
                {
                  // !!!!!!!!!!!!!!!! FUCLambdaPlateControllerOld.ResponsePositionAbsolute(SParameter1);
                  FUCLambdaPlateControllerNew.ResponsePositionTarget(SParameter1);
                  // debug                   Console.WriteLine(rxdline);
                  return;
                }
                if ("MA" == Statement)
                {
                  // !!!!!!!!!!!!!!!! FUCLambdaPlateControllerOld.ResponseMoveAbsolute(SParameter1);
                  FUCLambdaPlateControllerNew.ResponseMoveAbsolute(SParameter1);
                  // debug Console.WriteLine(rxdline);
                  return;
                }
                if ("MR" == Statement)
                {
                  // !!!!!!!!!!!!!!!! FUCLambdaPlateControllerOld.ResponseMoveRelative(SParameter1);
                  FUCLambdaPlateControllerNew.ResponseMoveRelative(SParameter1);
                  return;
                }
                if ("SMM" == Statement)
                {
                  // !!!!!!!!!!!!!!!! FUCLambdaPlateControllerOld.ResponseSetModeMicroStep(SParameter1);
                  FUCLambdaPlateControllerNew.ResponseSetModeMicroStep(SParameter1);
                  return;
                }
                if ("SPH" == Statement)
                {
                  // !!!!!!!!!!!!!!!! FUCLambdaPlateControllerOld.ResponseSetPeriodHalf(SParameter1);
                  FUCLambdaPlateControllerNew.ResponseSetPeriodHalf(SParameter1);
                  return;
                }
                if ("EM" == Statement)
                {
                  FUCLambdaPlateControllerNew.ResponseEnableMotion(SParameter1);
                  return;
                }
                // Error
                return;
              }
            }
            else
            { // <command>
              if ("MZ" == Statement)
              {
                // !!!!!!!!!!!!!!!! FUCLambdaPlateControllerOld.ResponseMoveZero();
                FUCLambdaPlateControllerNew.ResponseMoveZero();
                return;
              }
            }
          }
          else
          { // Error
            return;
          }
        }
      }
      catch (Exception)
      {
        // Error
        return;
      }
    }*/
    //
    //----------------------------------------
    //  Section - Startup
    //----------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          // FUCAuroraDisplay.ClosePort();
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        default:
          return false;
      }

    }

    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
    }

    //
    //------------------------------------------------------------------------
    //  Section - Menu
    //------------------------------------------------------------------------
    //

  }
}
