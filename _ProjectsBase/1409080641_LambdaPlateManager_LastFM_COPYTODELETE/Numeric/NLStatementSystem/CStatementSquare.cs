﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using NLExpression;
using NLStatement;
using XmlFile;
using UCNotifier;
//
namespace NLStatementSystem
{ //
  //  Syntax: <commandsquare> <expression>
  //
  public class CStatementSquare : CStatement
  {
    //
    //---------------------------------------------------------------------------------
    //  Segment - Field
    //---------------------------------------------------------------------------------
    //
    //private CConstantlist FConstantlist;
    //private CVariablelist FVariablelist;
    private String FTextExpression;
    private CExpression FExpression;
    //
    //---------------------------------------------------------------------------------
    //  Segment - Constructor
    //---------------------------------------------------------------------------------
    //
    public CStatementSquare(String textexpression)
      : base(CXmlHeaderSystem.NAME_SQUARE)
    {
      //FConstantlist = null;
      //FVariablelist = null;
      FTextExpression = textexpression;
      FExpression = null;
    }
    //
    //---------------------------------------------------------------------------------
    //  Segment - Property
    //---------------------------------------------------------------------------------
    //
		public String TextExpression
		{
			get { return FTextExpression; }
		}
    //
    //---------------------------------------------------------------------------------
    //  Segment - 
    //---------------------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------------------------------
    //
    public override bool Translate(ref CConstantlist constantlist,
                                   ref CVariablelist variablelist)
    {
      //FConstantlist = constantlist;
      //FVariablelist = variablelist;
			String Line = String.Format("Translate Square[{0}]", FTextExpression);
      Notifier.Write(Line);
      FExpression = new CExpression(ref constantlist, ref variablelist);
			return FExpression.Translate(FTextExpression);
    }

    public Boolean ThreadExecuteBusy()
    {
			/*???if (!(FVariablelist is CVariablelist))
			{
				return false;
			}*/
			//
      Double DValue = 0.0;
      if (!FExpression.Calculate(ref DValue))
      {
        return false;
      }
      //
      Double DResult = DValue * DValue;
      //
			String Line = String.Format("Execute Square[{0}] = {1}", DValue, DResult);
      Notifier.Write(Line);
      //
      // -> Push!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
      //
      return true;
    }



    /*public override Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines)
    {
      CVariable Variable = FVariablelist.FindName(FVariableName);
      if (Variable is CVariable)
      {
        String Line = BuildIdentation(identation);
        if (Variable.IsDefined())
        {
          Line += String.Format("{0} = {1};", Variable.Name, FTextExpression);
        } else
        {
          Line += String.Format("{0} {1} = {2};", Variable.Type, Variable.Name, FTextExpression);
        }
        lines.Add(Line);
        return true;
      }
      return false;
    }*/





  }
}
