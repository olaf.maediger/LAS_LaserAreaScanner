﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NLExpression
{
  public class CStack : Stack<Double>
  {
    private Double FValue;

    public Double Value
    {
      get { return FValue; }
      set { FValue = value; }
    }

    public new Boolean Clear()
    {
      base.Clear();
      return true;
    }

    public new void Push(Double value)
    {
      base.Push(value);
    }
    public new Double Pop()
    {
      return base.Pop();
    }


  }
}
