﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace NLExpression
{
  public enum ESeperator
  {
    EOS = 0x00,
    CR = 0x0D,
    LF = 0x0A,
    TAB = 0x09,
    SPACE = 0x20,
  };

  public enum ETerminal
  {
    ZERO = (Byte)0x00,
    SPACE = (Byte)' ',
    TAB = (Byte)0x09,
    ADD = (Byte)'+',
    SUB = (Byte)'-',
    MUL = (Byte)'*',
    DIV = (Byte)'/',
    POPEN = (Byte)'(',
    PCLOSE = (Byte)')',
    BOPEN = (Byte)'[',
    BCLOSE = (Byte)']',
    SOPEN = (Byte)'{',
    SCLOSE = (Byte)'}',
    DPOINT = (Byte)':',
    PCOMMA = (Byte)';',
    COMMA = (Byte)',',
    TECTUM = (Byte)'^',
    DOLLAR = (Byte)'$',
    ALPHA = (Byte)'@',
    SHARP = (Byte)'#',
    EQUAL = (Byte)'=',
    LOWER = (Byte)'<',
    GREATER = (Byte)'>', 
    POINT = (Byte)'.',
    HCOMMA = (Byte)'\'',
    DCOMMA = (Byte)'"',
  };

  public enum EReserved
  {
    EQUAL = 0,        // "==",  --> StringOperator
    UNEQUAL = 1,      // "<>",
    LOWEREQUAL = 2,   // "<=",
    GREATEREQUAL = 3, // ">=", als zusammengesetzte Terminale behandeln
    SINUS = 4,
    COSINUS = 5,
    TANGENS = 6,
    EXPONENTIAL = 7
  };



  public class CParser
  {
    public static readonly String[] RESERVED = 
    {
      "==",
      "<>",
      "<=",
      ">=",
      "SIN",
      "COS",
      "TAN",
      "EXP"
    };

    protected Boolean IsStringOperator(String token)
    {
      if (RESERVED[(Byte)EReserved.LOWEREQUAL] == token)
      {
        return true;
      }
      if (RESERVED[(Byte)EReserved.GREATEREQUAL] == token)
      {
        return true;
      }
      return false;
    }

    protected Boolean IsReserved(String token)
    {
      if (RESERVED[(Byte)EReserved.SINUS] == token)
      {
        return true;
      }
      if (RESERVED[(Byte)EReserved.COSINUS] == token)
      {
        return true;
      }
      if (RESERVED[(Byte)EReserved.TANGENS] == token)
      {
        return true;
      }
      if (RESERVED[(Byte)EReserved.EXPONENTIAL] == token)
      {
        return true;
      }
      return false;
    }

    protected Boolean IsInteger(String token)
    {
      try
      {
        Int32 IValue = 0;
        if (Int32.TryParse(token, out IValue))
        {
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    protected Boolean IsFloating(String token)
    {
      try
      {
				Double DValue = 0;
				if (Double.TryParse(token, out DValue))
				{
					return true;
				}
				return false;
			}
      catch (Exception)
      {
        return false;
      }
    }


    protected ESymbolKind FindKind(String token)
    {
      ESymbolKind Result = ESymbolKind.Identifier;
      if (IsReserved(token))
      {
        return ESymbolKind.Reserved;
      }
      if (IsInteger(token))
      {
        return ESymbolKind.Integer;
      }
      if (IsFloating(token))
      {
        return ESymbolKind.Floating;
      }
      return Result;
    }

    public Boolean Execute(String text,
                           ref Int32 index,
                           ref String token,
                           ref ESymbolKind symbolkind)
    {
      token = "";
      symbolkind = ESymbolKind.Undefined;
      for (Int32 CI = index; CI < text.Length; CI++)
      {
        Char C = text[CI];
        switch (C)
        {
          case (Char)ESeperator.SPACE:
          case (Char)ESeperator.TAB:
          case (Char)ESeperator.CR:
          case (Char)ESeperator.LF:
          case (Char)ESeperator.EOS:
            if (0 < token.Length)
            {
              index = CI;
              token = token.ToUpper();
              symbolkind = FindKind(token);
            } else
            { // only Terminal:
              token = C.ToString();
              index = 1 + CI;
              symbolkind = ESymbolKind.Undefined;
            }
            return true;
          case (Char)ETerminal.ADD:
          case (Char)ETerminal.SUB:
          case (Char)ETerminal.MUL:
          case (Char)ETerminal.DIV:
          case (Char)ETerminal.POPEN:
          case (Char)ETerminal.PCLOSE:
            if (0 < token.Length)
            { // feedback Identifier
              index = CI;
              token = token.ToUpper();
              symbolkind = FindKind(token);
            } else
            { // only Terminal:
              token = C.ToString();
              index = 1 + CI;
              symbolkind = ESymbolKind.Operator;
            }
            return true;
          case (Char)ETerminal.LOWER:
          case (Char)ETerminal.GREATER:
          case (Char)ETerminal.EQUAL:
            if (0 < token.Length)
            { // feedback Identifier
              index = CI;
              token = token.ToUpper();
              symbolkind = FindKind(token);
              return true;
            }
            // only Operator (Terminal or String) - 
            // lookahead for StringOperator ("==", "<>", "<=", ">=")
            if (1 + CI < text.Length)
            { // lookahead for StringOperator
              String StringOperator = C.ToString() + text[1 + CI];
              if ((CParser.RESERVED[(Byte)EReserved.EQUAL] == StringOperator) ||
                  (CParser.RESERVED[(Byte)EReserved.UNEQUAL] == StringOperator) ||
                  (CParser.RESERVED[(Byte)EReserved.LOWEREQUAL] == StringOperator) ||
                  (CParser.RESERVED[(Byte)EReserved.GREATEREQUAL] == StringOperator))
              { // StringOperator detected
                token = StringOperator;
                symbolkind = ESymbolKind.Operator;
                index = StringOperator.Length + CI;
                return true;
              } 
            }
            // only SingleOperator possible
            token = C.ToString();
            index = 1 + CI;
            symbolkind = ESymbolKind.Operator;
            return true;
          case (Char)ETerminal.BOPEN:
          case (Char)ETerminal.BCLOSE:
          case (Char)ETerminal.PCOMMA:
          case (Char)ETerminal.COMMA:
          case (Char)ETerminal.TECTUM:
          case (Char)ETerminal.DOLLAR:
          case (Char)ETerminal.ALPHA:
          case (Char)ETerminal.SHARP:
          case (Char)ETerminal.DPOINT:
          case (Char)ETerminal.HCOMMA:
          case (Char)ETerminal.DCOMMA:
            // invalid character!
            if (0 < token.Length)
            {
              index = CI;
              symbolkind = ESymbolKind.Undefined;
            } else
            {
              index = 1 + CI;
              symbolkind = ESymbolKind.Undefined;
            }
            return false;
          default: // POINT, Character, Number
            token += C;
            break;
        }
      }
      // rest...
      if (0 < token.Length)
      {
        index = 1 + text.Length;
        symbolkind = FindKind(token);
        return true; 
      }
      return false;
    }

  }
}
