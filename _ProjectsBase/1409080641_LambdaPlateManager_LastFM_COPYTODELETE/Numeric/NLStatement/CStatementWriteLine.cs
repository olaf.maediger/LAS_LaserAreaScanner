﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
using UCNotifier;
//
namespace NLStatement
{
	// <while> <expressionboolean> <do> <statement>

	public class CStatementWriteLine : CStatement
	{
		private CVariablelist FVariablelist;
		private String FFormat;
		private String FTextExpression;
		private CExpression FExpression;

		public CStatementWriteLine(CVariablelist variablelist,
														 String format,
														 String textexpression)
      : base(CStatementHeader.NAME_WRITELINE)
		{
      //SetOnStatementExecuteBegin(null);
      //SetOnStatementExecuteBusy(ThreadExecuteBusy);
      //SetOnStatementExecuteResponse(null);
      //SetOnStatementExecuteEnd(null);
      //
      FVariablelist = variablelist;
			FFormat = format;
			FTextExpression = textexpression;
			FExpression = null;
		}


		public String Format
		{
			get { return FFormat; }
		}

		public String Expression
		{
			get { return FTextExpression; }
		}


    public override Boolean Translate(ref CConstantlist constantlist,
                                      ref CVariablelist variablelist)
    {
			try
			{
				String Line = String.Format("Translate WriteLine['{0}' {1}]", FFormat, FTextExpression);
        Notifier.Write(Line);
        if (0 < FTextExpression.Length)
				{
          FExpression = new CExpression(ref constantlist,
                                        ref variablelist);
					FExpression.Translate(FTextExpression);
				} else
				{
					FExpression = null;
				}
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}
    
		public Boolean ThreadExecuteBusy()
		{
      String Line;
      if (FExpression is CExpression)
			{
				Double DValue = 0.0;
				if (!FExpression.Calculate(ref DValue))
				{
					return false;
				}
				Line = String.Format(FFormat, DValue);
			} else
			{ // Only writing Format-String
				Line = FFormat;
			}
      Notifier.Write(String.Format("Execute WriteLine[{0}]", Line));
			//
			RNotifierData NotifierData = new RNotifierData();
			Notifier.GetData(out NotifierData);
			Boolean Preset = NotifierData.EnableProtocol;
			NotifierData.EnableProtocol = true;
			Notifier.SetData(NotifierData);
			//
			Notifier.Write(Line);
			//
			NotifierData.EnableProtocol = Preset;
			Notifier.SetData(NotifierData);
			//
      return true;
		}


    /*public override Boolean WriteToAscii(ref Int32 identation,
                                         ref CLinelist lines)
    {
      String Line = BuildIdentation(identation);
      Line += String.Format("writeline(\"{0}\", {1});", FFormat, FTextExpression);
      lines.Add(Line);
      return true;
    }*/


	}
}