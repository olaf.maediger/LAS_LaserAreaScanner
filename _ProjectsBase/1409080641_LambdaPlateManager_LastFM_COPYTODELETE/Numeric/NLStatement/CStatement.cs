﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
//
using Initdata;
using Task;
using UCNotifier;
using NLExpression;
//
namespace NLStatement
{
  //
  public delegate Boolean DOnStatementExecuteBegin();
  public delegate Boolean DOnStatementExecuteBusy();
  public delegate Boolean DOnStatementExecuteResponse(Guid portid, String data);
  public delegate Boolean DOnStatementExecuteEnd();
  //
  public delegate void DOnAddStatement(CStatement statement);
  //
  public delegate void DParentOnExecuteEnd(CStatement statement);
  //
  public delegate void DOnDeleteOnExecutionEnd(CStatement statement);
  //
  public enum EStatementState
  {
    Error = -1,
    Init = 0,
    Begin = 1,
    Busy = 2,
    Response = 3,
    Abort = 4,
    End = 5
  };

  public enum EExecutionKind
  {
    KeepAlive = 0,
    DeleteAfterExecution = 1
  };
  

  public class CStatement
  {
    //
    //------------------------------------
    //  Segment - Constant
    //------------------------------------
    //
    protected const Char CHARACTER_SPACE = ' ';
    protected const Int32 INIT_WAITFOR = 1000;

    //
    //------------------------------------------------------
    //  Section - Error
    //------------------------------------------------------
    //	
    protected enum EErrorCode
    {
      None = 0,
      InvalidAccess,
      Unknown
    };
    //
    //------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------
    //
		private const String HEADER_LIBRARY = "Statement";

		protected static readonly String[] ERRORS = 
    { 
      "None",
			"Invalid Access",
  	  "Unknown"
    };
    //
    //------------------------------------------------------
    //  Section - Variable
    //------------------------------------------------------
    //
    private CNotifier FNotifier;
    private String FName;
    private String FText;
    private CConstantlist FConstants;
		private CVariablelist FVariables;
    private EStatementState FState; // Threadlist-Manager!
    //
    protected CXmlReader FXmlReader;
    //
    //
    //private DOnStatementExecute FOnStatementExecute;
    private DOnStatementExecuteBegin FOnStatementExecuteBegin;
    private DOnStatementExecuteBusy FOnStatementExecuteBusy;
    private DOnStatementExecuteResponse FOnStatementExecuteResponse;
    private DOnStatementExecuteEnd FOnStatementExecuteEnd;
    //
    private DParentOnExecuteEnd FParentOnExecuteEnd;

    private CEvent FWaitResponse;
    private Int32 FLineCount;

    private EExecutionKind FExecutionKind;
    private DOnDeleteOnExecutionEnd FOnDeleteOnExecutionEnd;
    //
    // protected DOnReadStatementBlock FOnReadStatementBlock;
    //
    //------------------------------------
    //  Segment - Constructor
    //------------------------------------
    //
		public CStatement(String name)
    {
      FConstants = new CConstantlist();// null;//constantlist;
      FVariables = new CVariablelist();// null;//variablelist;
      FName = name;
      FThread = null;
      FWaitResponse = new CEvent();
    }
    //
    //------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------
    //
    protected CNotifier Notifier
    {
      get { return FNotifier; }
    }
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      _Protocol("Library correct loaded.");
    }

    public String Name
    {
      get { return FName; }
      set { FName = value; }
    }

    public EExecutionKind ExecutionKind
    {
      get { return FExecutionKind; }
      set { FExecutionKind = value; }
    }

    public void SetOnDeleteOnExecutionEnd(DOnDeleteOnExecutionEnd value)
    {
      FOnDeleteOnExecutionEnd = value;
    }

    protected String Text
    {
      get { return FText; }
    }
    public void SetText(String value)
    {
      FText = value;
    }


		public CVariablelist Variablelist
		{
			get { return FVariables; }
		}
		
    private void SetState(EStatementState value)
    {
      if (value != FState)
      {
        FState = value;
        //
        // Debug 
        String Line = String.Format("Statement[{0}]: State[{1}]", Name, State);
        // Debug 
        FNotifier.Write(Line);
      }
    }
    public EStatementState State
    {
      get { return FState; }
      set { SetState(value); }
    }
    protected Thread FThread;

		//
    //------------------------------------------------------
    //  Section - Protocol
    //------------------------------------------------------
    //
    private String BuildHeader()
    {
      return String.Format("{0}", HEADER_LIBRARY);
    }

		protected void _Error(EErrorCode code)
    {
      if (FNotifier is CNotifier)
      {
        Int32 Index = (Int32)code;
        if ((Index < 0) && (ERRORS.Length <= Index))
        { // Unknown Error!
          Index = ERRORS.Length - 1;
        }
        String Line = String.Format("Error[{0}]: {1}", Index, ERRORS[Index]);
        FNotifier.Write(BuildHeader(), Line);
      }
    }

		protected void _Protocol(String line)
    {
      if (FNotifier is CNotifier)
      {
        FNotifier.Write(BuildHeader(), line);
      }
    }
		//
		//------------------------------------
		//  Helper
		//------------------------------------
		//
		public Boolean AddVariable(CVariable variable)
		{
			if (variable is CVariable)
			{
				FVariables.Add(variable);
				return true;
			}
			return false;
		}

    /*
		public Boolean AddStatement(CStatement command)
		{
			if (command is CStatement)
			{
				FStatements.Add(command);
				return true;
			}
			return false;
		}

		public Boolean SetStatementlist(CStatement commandblock)
		{
			if (commandblock is CStatement)
			{
				FStatements.Clear();
				foreach (CStatement Statement in commandblock.Statementlist)
				{
					FStatements.Add(Statement);
				}
				return true;
			}
			return false;
		}

		public void ClearStatementlist()
		{
			FStatements.Clear();
		}*/

		public void ClearVariablelist()
		{
			FVariables.Clear();
		}


    //
    //------------------------------------
    //  Public Management
    //------------------------------------
		/*/
		protected Boolean AddStatementSet(String name,
                                    String type,
																	  String expression)
		{
      String Line = String.Format("Add Statement [set {0}({1}) = {2}]", name, type, expression);
      _Protocol(Line);
      CStatement Statement = new CStatementSet(//FVariables, 
                                         name, type, expression);
			if (Statement is CStatement)
			{
        Statement.SetNotifier(FNotifier);
        FStatements.Add(Statement);
				return true;
			}
			return false;
		}
		protected Boolean AddStatementSet(CStatement commandblock,
																		String name,
                                    String type,
																		String expression)
		{
      String Line = String.Format("Add Statement to Block [set {0}({1}) = {2}]", name, type, expression);
      _Protocol(Line);
      CStatement Statement = new CStatementSet(//FVariables, 
                                         name, type, expression);
			if (Statement is CStatement)
			{
        Statement.SetNotifier(FNotifier);
        commandblock.AddStatement(Statement);
				return true;
			}
			return false;
		}


		protected Boolean AddStatementIf(String expression,
																	 CStatement blocktrue,
																	 CStatement blockfalse)
		{
      String Line = String.Format("Add Statement [if {0} ", expression);
      if (blocktrue is CStatement)
      {
        Line += String.Format("then {0} ", blocktrue.ToString());
      }
      if (blockfalse is CStatement)
      {
        Line += String.Format("else {0}", blockfalse.ToString());
      }
      Line += "]";
      _Protocol(Line);
      CStatement Statement = new CStatementIf(FVariables, expression, blocktrue, blockfalse);
			if (Statement is CStatement)
			{
        Statement.SetNotifier(FNotifier);
        FStatements.Add(Statement);
				return true;
			}
			return false;
		}
		protected Boolean AddStatementIf(CStatement commandblock,
																	 String expression,
																	 CStatement blocktrue,
																	 CStatement blockfalse)
		{
      String Line = String.Format("Add Statement to Block [if {0} ", expression);
			if (blocktrue is CStatement)
      {
        Line += String.Format("then {0} ", blocktrue.ToString());
      }
			if (blockfalse is CStatement)
      {
				Line += String.Format("else {0}", blockfalse.ToString());
      }
      Line += "]";
      _Protocol(Line);
			CStatement Statement = new CStatementIf(FVariables, expression, blocktrue, blockfalse);
			if (Statement is CStatement)
			{
        Statement.SetNotifier(FNotifier);
        commandblock.AddStatement(Statement);
				return true;
			}
			return false;
		}


		protected Boolean AddStatementWhile(String expression,
																			CStatement commandblock)
		{
      String Line = String.Format("Add Statement [while {0} do {1}]", expression, commandblock.ToString());
      _Protocol(Line);
      CStatement Statement = new CStatementWhile(FVariables, expression, commandblock);
			if (Statement is CStatement)
			{
        Statement.SetNotifier(FNotifier);
        FStatements.Add(Statement);
				return true;
			}
			return false;
		}
		protected Boolean AddStatementWhile(CStatement commandblock,
																			String expression,
																			CStatement whileblock)
		{
      String Line = String.Format("Add Statement While to Block [while {0} do {1}]", expression, whileblock.ToString());
      _Protocol(Line);
      CStatement Statement = new CStatementWhile(FVariables, expression, whileblock);
			if (Statement is CStatement)
			{
        Statement.SetNotifier(FNotifier);
        commandblock.AddStatement(Statement);
				return true;
			}
			return false;
		}*/

		// for <set> <condition==expression> <block>

		/* NC ???protected Boolean AddStatementFor(CStatementSet commandset,
																		String condition,
																		CStatementSet commandchange,
																		CStatementBlock block)
		{
			String Line = String.Format("Add Statement [for ({0}; {1}; {2})",
																	commandset.ToString(), condition, commandchange.ToString());
			if (block is CStatement)
			{
				Line += String.Format(" {0} ", block.ToString());
			}
			Line += "]";
			_Protocol(Line);
			CStatement Statement = new CStatementFor(FVariables, commandset, condition, commandchange, block);
			if (Statement is CStatement)
			{
				Statement.SetNotifier(FNotifier);
				FStatements.Add(Statement);
				return true;
			}
			return false;
		}
		protected Boolean AddStatementFor(CStatementBlock commandblock,
																		CStatementSet commandset,
																		String condition,
																		CStatementSet commandchange,
																		CStatementBlock block)
		{
			String Line = String.Format("Add Statement to Block [for ({0}; {1}; {2})",
																	commandset.ToString(), condition, commandchange.ToString());
			if (block is CStatement)
			{
				Line += String.Format(" {0} ", block.ToString());
			}
			Line += "]";
			_Protocol(Line);
			CStatement Statement = new CStatementFor(FVariables, commandset, condition, commandchange, block);
			if (Statement is CStatement)
			{
				Statement.SetNotifier(FNotifier);
				commandblock.AddStatement(Statement);
				return true;
			}
			return false;
		}
		*/

    /*

		protected Boolean AddStatementWriteLine(String format,
																					String expression)
		{
      String Line = String.Format("Add Statement [writeln '{0}' {1}]", format, expression);
      _Protocol(Line);
      CStatement Statement = new CStatementWriteLine(FVariables, format, expression);
			if (Statement is CStatement)
			{
				FStatements.Add(Statement);
        Statement.SetNotifier(FNotifier);
				return true;
			}
			return false;
		}
		protected Boolean AddStatementWriteLine(CStatement commandblock,
																					String format,
																					String expression)
		{
      String Line = String.Format("Add Statement to Block [writeln '{0}' {1}]", format, expression);
      _Protocol(Line);
      CStatement Statement = new CStatementWriteLine(FVariables, format, expression);
			if (Statement is CStatement)
			{
        Statement.SetNotifier(FNotifier);
        commandblock.AddStatement(Statement);
				return true;
			}
			return false;
		}*/


		protected CStatement CreateStatementBlock()
		{
			String Line = "Create StatementBlock";
			_Protocol(Line);
			CStatement StatementBlock = new CStatement("Block");//FConstants, FVariables);
			if (StatementBlock is CStatement)
			{
				StatementBlock.SetNotifier(FNotifier);
				// NO ADD !!!!!!!!!!!  FStatements.Add(StatementBlock); --> add only later in Statement
				return StatementBlock;
			}
			return null;
		}
    /*
    protected CStatement CreateAddStatementBlock()
    {
      String Line = "CreateAdd StatementBlock";
      _Protocol(Line);
      CStatement StatementBlock = new CStatement(FName);//FConstants, FVariables);
      if (StatementBlock is CStatement)
      {
        StatementBlock.SetNotifier(FNotifier);
        // additional StatementBlock!!!
        FStatements.Add(StatementBlock);
        return StatementBlock;
      }
      return null;
    }

    public Boolean AddStatementBlock(CStatement commandblock)
    {
      String Line = "Add StatementBlock";
      _Protocol(Line);
      if (commandblock is CStatement)
      {
        commandblock.SetNotifier(FNotifier);
        FStatements.Add(commandblock);
        return true;
      }
      return false;
    }*/


    /*
    public abstract Boolean Translate(ref CConstantlist constantlist,
                                      ref CVariablelist variablelist);
    */

    // translates all Expressions in all Statements
    public virtual Boolean Translate(ref CConstantlist constantlist,
                                        ref CVariablelist variablelist)
    {
      /*String Line = String.Format("Translate Statement[{0}]:", FStatements.Count);
      _Protocol(Line);
      foreach (CStatement Statement in FStatements)
      {
        if (!Statement.Translate(ref constantlist,
                               ref variablelist))
        {
          return false;
        }
      }*/
      return true;
    }
    /*
    protected Boolean Execute()
    {
      String Line = String.Format("Execute Statement[{0}]:", FStatements.Count);
      _Protocol(Line);
      foreach (CStatement Statement in FStatements)
      {
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! if (!Statement.ThreadExecuteBusy())
        {
          return false;
        }
      }
      return true;
    }*/

    /*
    protected Boolean ReadFromXml(String filename)
    {
      FXmlReader = new CXmlReader();
      FXmlReader.SetNotifier(FNotifier);
      FXmlReader.SetOnReadStatementBlock(FOnReadStatementBlock);
      FProgramName = Path.GetFileNameWithoutExtension(filename);
      return FXmlReader.ReadProgram(filename, this);
    }*/
    /*???protected Boolean ReadFromXml(String filename,
                                  DOnReadStatementBlock onreadcommandblock)
    {
      FXmlReader = new CXmlReader();
      FXmlReader.SetNotifier(FNotifier);
      FXmlReader.SetOnReadStatementBlock(onreadcommandblock);
      FProgramName = Path.GetFileNameWithoutExtension(filename);
      return FXmlReader.ReadProgram(filename, this);
    }*/
/*
    protected Boolean WriteToXml(String filename)
    {
      CXmlWriter XmlWriter = new CXmlWriter();
      XmlWriter.SetNotifier(FNotifier);
      FProgramName = Path.GetFileNameWithoutExtension(filename);
      return XmlWriter.WriteProgram(filename, this);
    }
    */
    protected Boolean List()
    {
      // Int32 Identation = 0;
      String Line = String.Format("*** List[{0}]:", FName);
      FNotifier.Write(Line);
      //return FStatements.ListProgram(ref Identation);
      return false;
    }



   /* protected Boolean SaveToAscii(String filename)
    {
      CAsciiWriter AsciiWriter = new CAsciiWriter();
      AsciiWriter.SetNotifier(FNotifier);
      FProgramName = Path.GetFileNameWithoutExtension(filename);
      return AsciiWriter.WriteProgram(filename, this);
    }*/


    protected void IncrementLineCount()
    {
      FLineCount++;
    }

    protected void BaseOnLineReceived()
    {
      IncrementLineCount();
      if (1 <= FLineCount)
      {
        FWaitResponse.Set();
      }
    }

    public void Abort()
    {
      State = EStatementState.Abort;
      FThread.Abort();
      AbortExecution();
    }

    public void Join()
    {
      FThread.Join();
    }

    // -> Member!!!public abstract void SelfOnLineReceived(Guid comportid,
    //                                        String line);

    protected void SetOnStatementExecuteBegin(DOnStatementExecuteBegin value)
    {
      FOnStatementExecuteBegin = value;
    }

    protected void SetOnStatementExecuteBusy(DOnStatementExecuteBusy value)
    {
      FOnStatementExecuteBusy = value;
    }

    protected void SetOnStatementExecuteResponse(DOnStatementExecuteResponse value)
    {
      FOnStatementExecuteResponse = value;
    }

    protected void SetOnStatementExecuteEnd(DOnStatementExecuteEnd value)
    {
      FOnStatementExecuteEnd = value;
    }
    //
    //---------------------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------------------
    //
    protected String BuildIdentation(Int32 identation)
    {
      return new String(CHARACTER_SPACE, 2 * identation);
    }

    //???public abstract Boolean WriteToAscii(ref Int32 identation,
    //???                                     ref CLinelist lines);
    //
    //---------------------------------------------------------------------------
    //  Segment - Execution
    //---------------------------------------------------------------------------
    //
    protected Boolean SelfExecuteBegin()
    {
      Boolean Result = false;
      FNotifier.Write(String.Format("Statement[{0}].ExecuteBegin", this.ToString()));
      State = EStatementState.Begin;
      //
      if (FOnStatementExecuteBegin is DOnStatementExecuteBegin)
      {
        Result = FOnStatementExecuteBegin();
        // debug Thread.Sleep(1000);
      }
      return Result;
    }

    protected Boolean SelfExecuteBusy()
    {
      Boolean Result = false;
      State = EStatementState.Busy;
      FNotifier.Write(String.Format("Statement[{0}].ExecuteBusy", this.ToString()));
      //
      if (FOnStatementExecuteBusy is DOnStatementExecuteBusy)
      {
        Result = FOnStatementExecuteBusy();
        // debug Thread.Sleep(1000);
      }
      return Result;
    }

    protected Boolean SelfExecuteResponse()
    {
      Boolean Result = false;
      if (FOnStatementExecuteResponse is DOnStatementExecuteResponse)
      {
        State = EStatementState.Response;
        FNotifier.Write(String.Format("Statement[{0}].ExecuteResponse", this.ToString()));
        //
        FLineCount = 0;
        FWaitResponse.Reset();
        //
        // debug Thread.Sleep(1);
        // not here!!! HWComPort.WriteLine(StatementText);
        if (!FWaitResponse.WaitFor(INIT_WAITFOR))
        {
          State = EStatementState.Error;
          // -> Parent!!!FNotifier.Error(CIFTerminalRS232.HEADER_LIBRARY + " " + FName,
          //????                1, "No response of hardware");
        }
        else
        {
          Result = FOnStatementExecuteResponse(Guid.Empty, "ABC");
        }
      }
      return Result;
    }

    protected Boolean SelfExecuteEnd()
    {
      Boolean Result = false;
      State = EStatementState.End;
      FNotifier.Write(String.Format("Statement[{0}].ExecuteEnd", this.ToString()));
      //
      if (FOnStatementExecuteEnd is DOnStatementExecuteEnd)
      {
        Result = FOnStatementExecuteEnd();
        // debug Thread.Sleep(1000);
      }
      //
      if (FOnDeleteOnExecutionEnd is DOnDeleteOnExecutionEnd)
      {
        FOnDeleteOnExecutionEnd(this);
      }
      //
      return Result;
    }

    protected void ThreadExecute()
    {
      FNotifier.Write(String.Format("Statement[{0}] startet", this.ToString()));
      Boolean Result = SelfExecuteBegin();
      Result &= SelfExecuteBusy();
      Result &= SelfExecuteResponse();
      Result &= SelfExecuteEnd();
      //
      if (FParentOnExecuteEnd is DParentOnExecuteEnd)
      {
        FParentOnExecuteEnd(this);
      }
      // debug Thread.Sleep(100);
      FThread = null;
      FNotifier.Write(String.Format("Statement[{0}] finnished", this.ToString()));
      //if (FVariable is CVariable)
      {
        //???????????VariablelistParent.Add(FVariable);
      }
      if (!Result)
      {
        // Error.....
      }
    }



    /*  not here public virtual Boolean ExecuteDataReceived(String data)
     {
       return true;
     }*/
    //
    //---------------------------------------------------------------------------
    //  Segment - Public Management
    //---------------------------------------------------------------------------
    //
    public Boolean StartExecution(DParentOnExecuteEnd value)
    { // debug 
      try
      {
        Notifier.Write(String.Format("Statement[{0}].Activate", this.ToString()));
        FParentOnExecuteEnd = value;
        if (null == FThread)
        {
          FThread = new Thread(new ThreadStart(ThreadExecute));
          // debug FNotifier.Write(String.Format("Thread[{0}].Start", this.ToString()));
          Notifier.Write(String.Format("Thread[{0}].Start", this.ToString()));
          FThread.Start();
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        FNotifier.Write(" Cannot start Statement-Execution");
        return false;
      }
    }

    public Boolean AbortExecution()
    {
      try
      {
        if (FThread is Thread)
        {
          State = EStatementState.Abort;
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        FNotifier.Write(" Cannot abort Statement-Execution");
        return false;
      }
    }

    public virtual Boolean List(ref Int32 identation)
    {
      CLinelist Lines = new CLinelist();
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WriteToAscii(ref identation, ref Lines);
      foreach (CLine Line in Lines)
      {
        FNotifier.Write(Line.Value);
      }
      return true;
    }


  }
}
