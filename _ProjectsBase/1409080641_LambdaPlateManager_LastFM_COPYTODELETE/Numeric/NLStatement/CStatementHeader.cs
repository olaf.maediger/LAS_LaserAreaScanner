﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NLStatement
{
  public class CStatementHeader
  {

		//
		//	Common
		//
		public const String NAME_PROGRAM = "Program";
		public const String NAME_STATEMENT = "Statement";
		public const String NAME_CONDITION = "Condition";
		public const String NAME_INCREMENT = "Increment";
		//
		//	Statement
		//
		public const String NAME_BLOCK = "Block";
		public const String NAME_BLOCKTRUE = "BlockTrue";
		public const String NAME_BLOCKFALSE = "BlockFalse";
		public const String NAME_SET = "Set";
		public const String NAME_FOR = "For";
		public const String NAME_WHILE = "While";
		public const String NAME_IF = "If";
		public const String NAME_WRITELINE = "WriteLine";
		//
    //
    //	Statement - Extension (Library)
    //
    // -> external Library !!! public const String NAME_SQUARE = "Square";
	
		
	}
}
