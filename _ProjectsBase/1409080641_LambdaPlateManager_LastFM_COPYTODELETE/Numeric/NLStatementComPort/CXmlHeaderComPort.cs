﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using NLExpression;
using NLStatement;
using XmlFile;
//
namespace NLStatementComPort
{
	//
	//******************************************
	//  --- XmlHeader ---
	//******************************************
	//  
  public class CXmlHeaderComPort : CXmlHeader
  { //
		//------------------------------------------
		//	Constant
		//------------------------------------------
		//
    //	Statement - Extension (Library)
    //
    public const String HEADER_LIBRARY = "NLStatementComPort";
    //
    public const String NAME_TRANSMITTEXTDELAY = "TRANSMITTEXTDELAY";
	
		
	}
}

