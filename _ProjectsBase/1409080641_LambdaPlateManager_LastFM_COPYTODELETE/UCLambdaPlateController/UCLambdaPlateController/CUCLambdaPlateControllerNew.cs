﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;
using UCNotifier;
using UCRotator;
//
namespace UCLambdaPlateController
{
  //
  //----------------------------------------------------------------------
  //  Section - Global Type
  //----------------------------------------------------------------------
  //
  public delegate void DOnEnableMotion(Boolean enable);
  //
  //-------------------------------------------------------------
  //  Segment - Main
  //-------------------------------------------------------------
  //
  public partial class CUCLambdaPlateControllerNew : UserControl
  {
    //
    //----------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------
    //

    //
    //----------------------------------------------------------------------
    //  Section - Field
    //----------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnAnglePresetChanged FOnAnglePresetChanged;
    private DOnAngleActualChanged FOnAngleActualChanged;
    private DOnAngleDeltaChanged FOnAngleDeltaChanged;
    private DOnModeMicroStepChanged FOnModeMicroStepChanged;
    private DOnPeriodHalfChanged FOnPeriodHalfChanged;
    private DOnEnableMotion FOnEnableMotion;
    //
    //-------------------------------------------------------------
    //  Segment - Constructor
    //-------------------------------------------------------------
    //
    public CUCLambdaPlateControllerNew()
    {
      InitializeComponent();
      //
      FUCRotator90Deg.SetOnAnglePresetChanged(UCRotator90DegOnAnglePresetChanged);
      FUCRotator90Deg.SetOnAngleActualChanged(UCRotator90DegOnAngleActualChanged);
      FUCRotator90Deg.SetOnAngleDeltaChanged(UCRotator90DegOnAngleDeltaChanged);
      //
      FUCMicroStepResolution.SetOnModeMicroStepChanged(UCMicroStepResolutionOnModeMicroStepChanged);
      //
      FUCPeriodHalf.SetOnPeriodHalfChanged(UCPeriodHalfOnPeriodHalfChanged);
    }
    //
    //------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnAnglePresetChanged(DOnAnglePresetChanged value)
    {
      FOnAnglePresetChanged = value;
    }

    public void SetOnAngleActualChanged(DOnAngleActualChanged value)
    {
      FOnAngleActualChanged = value;
    }

    public void SetOnAngleDeltaChanged(DOnAngleDeltaChanged value)
    {
      FOnAngleDeltaChanged = value;
    }

    public void SetOnModeMicroStepChanged(DOnModeMicroStepChanged value)
    {
      FOnModeMicroStepChanged = value;
    }

    public void SetOnPeriodHalfChanged(DOnPeriodHalfChanged value)
    {
      FOnPeriodHalfChanged = value;
    }

    public void SetOnEnableMotion(DOnEnableMotion value)
    {
      FOnEnableMotion = value;
    }

    public void SetResolution(Double value)
    {
      FUCRotator90Deg.AngleDelta = value;
    }



    private void UCRotator90DegOnAnglePresetChanged(Double angle)
    {
      if (FOnAnglePresetChanged is DOnAnglePresetChanged)
      {
        FOnAnglePresetChanged(angle);
      }
    }
    private void UCRotator90DegOnAngleActualChanged(Double angle)
    {
      if (FOnAngleActualChanged is DOnAngleActualChanged)
      {
        FOnAngleActualChanged(angle);
      }
    }
    private void UCRotator90DegOnAngleDeltaChanged(Double angle)
    {
      if (FOnAngleDeltaChanged is DOnAngleDeltaChanged)
      {
        FOnAngleDeltaChanged(angle);
      }
    }

    private void UCMicroStepResolutionOnModeMicroStepChanged(EModeMicroStep value)
    {
      if (FOnModeMicroStepChanged is DOnModeMicroStepChanged)
      {
        FOnModeMicroStepChanged(value);
      }
    }

    private void UCPeriodHalfOnPeriodHalfChanged(Int32 periodhalf,
                                                 Int32 period,
                                                 Double frequency)
    {
      if (FOnPeriodHalfChanged is DOnPeriodHalfChanged)
      {
        FOnPeriodHalfChanged(periodhalf, period, frequency);
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Response -> Controls
    //----------------------------------------------------------------------
    //
    private delegate void CBResponseEnableMotion(String value);
    public void ResponseEnableMotion(String value)
    {
      if (this.InvokeRequired)
      {
        CBResponseEnableMotion CB = new CBResponseEnableMotion(ResponseEnableMotion);
        Invoke(CB, new object[] { value });
      }
      else
      {
        try
        {
          Boolean BValue = false;
          if ("1" == value)
          {
            BValue = true;
          }
          if (BValue != cbxEnableMotion.Checked)
          {
            cbxEnableMotion.Checked = BValue;
          }
        }
        catch (Exception)
        {
        }
      }
    }
    
    
    private delegate void CBResponseMoveZero();
    public void ResponseMoveZero()
    {
      if (this.InvokeRequired)
      {
        CBResponseMoveZero CB = new CBResponseMoveZero(ResponseMoveZero);
        Invoke(CB, new object[] { });
      }
      else
      {
        try
        {
          FUCRotator90Deg.AnglePreset = 0.0;
          Invalidate();
        }
        catch (Exception)
        {
        }
      }
    }

    private delegate void CBResponsePositionTarget(String value);
    public void ResponsePositionTarget(String value)
    {
      if (this.InvokeRequired)
      {
        CBResponsePositionTarget CB = new CBResponsePositionTarget(ResponsePositionTarget);
        Invoke(CB, new object[] { value });
      }
      else
      {
        try
        {
          CInitdata.Init();
          Double DValue = Double.Parse(value);
          FUCRotator90Deg.AnglePreset = DValue;
          Invalidate();
        }
        catch (Exception)
        {
        }
      }
    }

    private delegate void CBResponsePositionAbsolute(String value);
    public void ResponsePositionAbsolute(String value)
    {
      if (this.InvokeRequired)
      {
        CBResponsePositionAbsolute CB = new CBResponsePositionAbsolute(ResponsePositionAbsolute);
        Invoke(CB, new object[] { value });
      }
      else
      {
        try
        {
          CInitdata.Init();
          Double DValue = Double.Parse(value);
          FUCRotator90Deg.AngleActual = DValue;
          Invalidate();
        }
        catch (Exception)
        {
        }
      }
    }

    private delegate void CBResponseMoveAbsolute(String value);
    public void ResponseMoveAbsolute(String value)
    {
      if (this.InvokeRequired)
      {
        CBResponseMoveAbsolute CB = new CBResponseMoveAbsolute(ResponseMoveAbsolute);
        Invoke(CB, new object[] { value });
      }
      else
      {
        try
        {
          CInitdata.Init();
          Double DValue = Double.Parse(value);
          FUCRotator90Deg.AnglePreset = DValue;
          Invalidate();
        }
        catch (Exception)
        {
        }
      }
    }

    private delegate void CBResponseMoveRelative(String value);
    public void ResponseMoveRelative(String value)
    {
      if (this.InvokeRequired)
      {
        CBResponseMoveRelative CB = new CBResponseMoveRelative(ResponseMoveRelative);
        Invoke(CB, new object[] { value });
      }
      else
      {
        try
        {
          CInitdata.Init();
          Double DValue = Double.Parse(value);
          FUCRotator90Deg.AnglePreset = FUCRotator90Deg.AnglePreset + DValue;
          Invalidate();
        }
        catch (Exception)
        {
        }
      }
    }

    private delegate void CBResponseSetModeMicroStep(String value);
    public void ResponseSetModeMicroStep(String value)
    {
      if (this.InvokeRequired)
      {
        CBResponseSetModeMicroStep CB = new CBResponseSetModeMicroStep(ResponseSetModeMicroStep);
        Invoke(CB, new object[] { value });
      }
      else
      {
        try
        {
          Int32 IMMS = Int32.Parse(value);
          EModeMicroStep MMS = (EModeMicroStep)IMMS;
          //if (MMS != FUCMicroStepResolution.GetModeMicroStep())
          {
            switch (MMS)
            {
              case EModeMicroStep.Full:
                FUCRotator90Deg.AngleDelta = CUCMicroStepResolution.RESOLUTION_FULLSTEP;
                FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_FULLSTEP);
                FUCMicroStepResolution.SetModeMicroStep(EModeMicroStep.Full);
                break;
              case EModeMicroStep.Half:
                FUCRotator90Deg.AngleDelta = CUCMicroStepResolution.RESOLUTION_HALFSTEP;
                FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_HALFSTEP);
                FUCMicroStepResolution.SetModeMicroStep(EModeMicroStep.Half);
                break;
              case EModeMicroStep.Quarter:
                FUCRotator90Deg.AngleDelta = CUCMicroStepResolution.RESOLUTION_QUARTERSTEP;
                FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_QUARTERSTEP);
                FUCMicroStepResolution.SetModeMicroStep(EModeMicroStep.Quarter);
                break;
              case EModeMicroStep.OnePerEight:
                FUCRotator90Deg.AngleDelta = CUCMicroStepResolution.RESOLUTION_ONEPEREIGHT;
                FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_ONEPEREIGHT);
                FUCMicroStepResolution.SetModeMicroStep(EModeMicroStep.OnePerEight);
                break;
              case EModeMicroStep.OnePerSixteen:
                FUCRotator90Deg.AngleDelta = CUCMicroStepResolution.RESOLUTION_ONEPERSIXTEEN;
                FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_ONEPERSIXTEEN);
                FUCMicroStepResolution.SetModeMicroStep(EModeMicroStep.OnePerSixteen);
                break;
              case EModeMicroStep.OnePerThirtyTwo:
                FUCRotator90Deg.AngleDelta = CUCMicroStepResolution.RESOLUTION_ONEPERTHIRTYTWO;
                FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_ONEPERTHIRTYTWO);
                FUCMicroStepResolution.SetModeMicroStep(EModeMicroStep.OnePerThirtyTwo);
                break;
            }
          }
        }
        catch (Exception)
        {
        }
      }
    }

    private delegate void CBResponseSetPeriodHalf(String value);
    public void ResponseSetPeriodHalf(String value)
    {
      if (this.InvokeRequired)
      {
        CBResponseSetPeriodHalf CB = new CBResponseSetPeriodHalf(ResponseSetPeriodHalf);
        Invoke(CB, new object[] { value });
      }
      else
      {
        try
        {
          Int32 IPH = Int32.Parse(value);
          FUCPeriodHalf.SetPeriodHalf(IPH);
        }
        catch (Exception)
        {
        }
      }
    }
    //
    //-------------------------------------------------------------
    //  Segment - Event - Menu
    //-------------------------------------------------------------
    //
    private void btnMoveMaximum_Click(object sender, EventArgs e)
    {
      FUCRotator90Deg.AnglePreset = 90.0;
    }

    private void btnMove70Deg_Click(object sender, EventArgs e)
    {
      FUCRotator90Deg.AnglePreset = 70.0;
    }

    private void btnMove50Deg_Click(object sender, EventArgs e)
    {
      FUCRotator90Deg.AnglePreset = 50.0;
    }

    private void btnMove45Deg_Click(object sender, EventArgs e)
    {
      FUCRotator90Deg.AnglePreset = 45.0;
    }

    private void btnMove40Deg_Click(object sender, EventArgs e)
    {
      FUCRotator90Deg.AnglePreset = 40.0;
    }

    private void btnMove20Deg_Click(object sender, EventArgs e)
    {
      FUCRotator90Deg.AnglePreset = 20.0;
    }

    private void btnMoveMinimum_Click(object sender, EventArgs e)
    {
      FUCRotator90Deg.AnglePreset = 0.0;
    }


    private void FTimerRepetition_TickUp(object sender, EventArgs e)
    {
      FUCRotator90Deg.MoveStepUp();
    }
    private void btnMoveStepUp_MouseDown(object sender, MouseEventArgs e)
    {
      FUCRotator90Deg.MoveStepUp();
      FTimerRepetition.Tick += FTimerRepetition_TickUp;
      FTimerRepetition.Enabled = true;
    }
    private void btnMoveStepUp_MouseUp(object sender, MouseEventArgs e)
    {
      FTimerRepetition.Enabled = false;
      FTimerRepetition.Tick -= FTimerRepetition_TickUp;
    }


    private void FTimerRepetition_TickDown(object sender, EventArgs e)
    {
      FUCRotator90Deg.MoveStepDown();
    }
    private void btnMoveStepDown_MouseDown(object sender, MouseEventArgs e)
    {
      FUCRotator90Deg.MoveStepDown();
      FTimerRepetition.Tick += FTimerRepetition_TickDown;
      FTimerRepetition.Enabled = true;
    }
    private void btnMoveStepDown_MouseUp(object sender, MouseEventArgs e)
    {
      FTimerRepetition.Enabled = false;
      FTimerRepetition.Tick -= FTimerRepetition_TickDown;
    }


    private void cbxEnableMotion_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxEnableMotion.Checked)
      {
        cbxEnableMotion.Text = "Disable Motion";
      } 
      else
      {
        cbxEnableMotion.Text = "Enable Motion";
      }
      if (FOnEnableMotion is DOnEnableMotion)
      {
        FOnEnableMotion(cbxEnableMotion.Checked);
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Management
    //----------------------------------------------------------------------
    //
    public Boolean Open()
    {
      FUCRotator90Deg.AnglePreset = 0.0;
      FUCRotator90Deg.AngleActual = 0.0;
      FUCRotator90Deg.AngleDelta = CUCMicroStepResolution.RESOLUTION_FULLSTEP;
      FUCMicroStepResolution.SetModeMicroStep(EModeMicroStep.Full);
      cbxEnableMotion.Checked = true;
      return true;
    }

    public Boolean Close()
    {
      FUCMicroStepResolution.SetModeMicroStep(EModeMicroStep.Full);
      cbxEnableMotion.Checked = false;
      return true;
    }

    public void EnableControls(Boolean enable)
    {
      this.Enabled = enable;
    }





  }
}
