﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;
using UCNotifier;
using UCAngle90Deg;
//
namespace UCLambdaPlateController
{
  //
  //----------------------------------------------------------------------
  //  Section - Global Type
  //----------------------------------------------------------------------
  //

  public partial class CUCLambdaPlateControllerOld : UserControl
  {
    //
    //----------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------
    //

    //
    //----------------------------------------------------------------------
    //  Section - Field
    //----------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnValueChanged FOnValueChanged;
    private DOnModeMicroStepChanged FOnModeMicroStepChanged;
    private DOnPeriodHalfChanged FOnPeriodHalfChanged; 
    //
    //----------------------------------------------------------------------
    //  Section - Constructor
    //----------------------------------------------------------------------
    //
    public CUCLambdaPlateControllerOld()
    {
      CInitdata.Init();
      //
      InitializeComponent();
      //
      FUCMicroStepResolution.SetOnModeMicroStepChanged(UCMicroStepResolutionOnModeMicroStepChanged);
      //
      FUCAngle90Deg.SetOnValueChanged(UCAngle90DegOnValueChanged);
      //
      FUCPeriodHalf.SetOnPeriodHalfChanged(UCPeriodHalfOnPeriodHalfChanged);
    }
    //
    //------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnValueChanged(DOnValueChanged value)
    {
      FOnValueChanged = value;
    }

    public void SetOnModeMicroStepChanged(DOnModeMicroStepChanged value)
    {
      FOnModeMicroStepChanged = value;
    }

    public void SetOnPeriodHalfChanged(DOnPeriodHalfChanged value)
    {
      FOnPeriodHalfChanged = value;
    }

    public void SetResolution(Double value)
    {
      FUCAngle90Deg.SetDelta(value);
    }

    private delegate void CBSetAnglePreset(Double value);
    public void SetAnglePreset(Double value)
    {
      if (this.InvokeRequired)
      {
        CBSetAnglePreset CB = new CBSetAnglePreset(SetAnglePreset);
        Invoke(CB, new object[] { value });
      }
      else
      {
        FUCAngle90Deg.SetAngle(value);
      }
    }

    private delegate void CBSetAngleAbsolute(Double value);
    public void SetAngleAbsolute(Double value)
    {
      if (this.InvokeRequired)
      {
        CBSetAngleAbsolute CB = new CBSetAngleAbsolute(SetAngleAbsolute);
        Invoke(CB, new object[] { value });
      }
      else
      {
        FUCAngle90Deg.SetAngle(value);
      }
    }

    private delegate void CBSetAngleRelative(Double value);
    public void SetAngleRelative(Double value)
    {
      if (this.InvokeRequired)
      {
        CBSetAngleRelative CB = new CBSetAngleRelative(SetAngleRelative);
        Invoke(CB, new object[] { value });
      }
      else
      {
        FUCAngle90Deg.Angle += value;
      }
    }

    //
    //----------------------------------------------------------------------
    //  Section - Callback
    //----------------------------------------------------------------------
    //
    private void UCMicroStepResolutionOnModeMicroStepChanged(EModeMicroStep value)
    {
      if (FOnModeMicroStepChanged is DOnModeMicroStepChanged)
      {
        FOnModeMicroStepChanged(value);
      }
    }

    private void UCAngle90DegOnValueChanged(Double valueactual,
                                            Double valuepreset,
                                            Double valuedelta)
    {
      if (FOnValueChanged is DOnValueChanged)
      {
        FOnValueChanged(valueactual, valuepreset, valuedelta);
      }
    }

    private void UCPeriodHalfOnPeriodHalfChanged(Int32 periodhalf,
                                                 Int32 period,
                                                 Double frequency)
    {
      if (FOnPeriodHalfChanged is DOnPeriodHalfChanged)
      {
        FOnPeriodHalfChanged(periodhalf, period, frequency);
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Event
    //----------------------------------------------------------------------
    //
    private void btnMoveMinimum_Click(object sender, EventArgs e)
    {
      FUCAngle90Deg.Angle = 0.0;
    }

    private void btnMoveMaximum_Click(object sender, EventArgs e)
    {
      FUCAngle90Deg.Angle = 90.0;
    }
    //
    //----------------------------------------------------------------------
    //  Section - Response -> Controls
    //----------------------------------------------------------------------
    //
    public void ResponseMoveZero()
    {
      try
      {
        SetAnglePreset(0.0);
      }
      catch (Exception)
      {
      }
    }


    public void ResponsePositionAbsolute(String value)
    {
      try
      {
        CInitdata.Init();
        Double DValue = Double.Parse(value);
        FUCAngleActual.Angle = DValue;
      }
      catch (Exception)
      {
      }
    }

    public void ResponseMoveAbsolute(String value)
    {
      try
      {
        CInitdata.Init();
        Double DValue = Double.Parse(value);
        FUCAnglePreset.Angle = DValue;
      }
      catch (Exception)
      {
      }
    }

    public void ResponseMoveRelative(String value)
    {
      try
      {
        CInitdata.Init();
        Double DValue = Double.Parse(value);
        FUCAnglePreset.Angle += DValue;
      }
      catch (Exception)
      {
      }
    }

    public void ResponseSetModeMicroStep(String value)
    {
      try
      {
        Int32 IMMS = Int32.Parse(value);
        EModeMicroStep MMS = (EModeMicroStep)IMMS;
        switch (MMS)
        {
          case EModeMicroStep.Full:
            FUCAngle90Deg.SetDelta(CUCMicroStepResolution.RESOLUTION_FULLSTEP);
            FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_FULLSTEP);
            break;
          case EModeMicroStep.Half:
            FUCAngle90Deg.SetDelta(CUCMicroStepResolution.RESOLUTION_HALFSTEP);
            FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_HALFSTEP);
            break;
          case EModeMicroStep.Quarter:
            FUCAngle90Deg.SetDelta(CUCMicroStepResolution.RESOLUTION_QUARTERSTEP);
            FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_QUARTERSTEP);
            break;
          case EModeMicroStep.OnePerEight:
            FUCAngle90Deg.SetDelta(CUCMicroStepResolution.RESOLUTION_ONEPEREIGHT);
            FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_ONEPEREIGHT);
            break;
          case EModeMicroStep.OnePerSixteen:
            FUCAngle90Deg.SetDelta(CUCMicroStepResolution.RESOLUTION_ONEPERSIXTEEN);
            FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_ONEPERSIXTEEN);
            break;
          case EModeMicroStep.OnePerThirtyTwo:
            FUCAngle90Deg.SetDelta(CUCMicroStepResolution.RESOLUTION_ONEPERTHIRTYTWO);
            FUCPeriodHalf.SetAngleResolution(CUCMicroStepResolution.RESOLUTION_ONEPERTHIRTYTWO);
            break;
        }
      }
      catch (Exception)
      {
      }
    }

    public void ResponseSetPeriodHalf(String value)
    {
      try
      {
        Int32 IPH = Int32.Parse(value);
        FUCPeriodHalf.SetPeriodHalf(IPH);
      }
      catch (Exception)
      {
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Management
    //----------------------------------------------------------------------
    //
    public Boolean Open()
    {
      // !!!!!!!!! FUCMicroStepResolution.SetMicroStepResolution(EModeMicroStep.Full);
      return true;
    }

    public Boolean Close()
    {
      return true;
    }

    public void EnableControls(Boolean enable)
    {
      this.Enabled = enable;
    }

  }
}
