﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;
//
namespace UCLambdaPlateController
{
  public partial class CUCAngleUnit : UserControl
  {
    public const String INIT_FORMAT = "{0:0.00000}";

    private Double FAngle;
    private String FFormat;

    public CUCAngleUnit()
    {
      CInitdata.Init();
      //
      InitializeComponent();
      //
      FFormat = INIT_FORMAT;
      Angle = 0.0;
    }

    private void SetHeader(String value)
    {
      lblHeader.Text = value;
    }
    public String Header
    {
      get { return lblHeader.Text; }
      set { SetHeader(value); }
    }

    private delegate void CBSetAngle(Double value);
    public void SetAngle(Double value)
    {
      if (this.InvokeRequired)
      {
        CBSetAngle CB = new CBSetAngle(SetAngle);
        Invoke(CB, new object[] { value });
      }
      else
      {
        FAngle = value;
        lblAngle.Text = String.Format(FFormat, FAngle);
      }
    }
    public Double Angle
    {
      get { return FAngle; }
      set { SetAngle(value); }
    }

  }
}
