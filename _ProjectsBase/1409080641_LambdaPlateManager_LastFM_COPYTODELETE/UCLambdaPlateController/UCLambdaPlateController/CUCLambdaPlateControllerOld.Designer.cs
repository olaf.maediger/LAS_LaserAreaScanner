﻿namespace UCLambdaPlateController
{
  partial class CUCLambdaPlateControllerOld
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnMoveMaximum = new System.Windows.Forms.Button();
      this.btnMoveMinimum = new System.Windows.Forms.Button();
      this.FUCAngle90Deg = new UCAngle90Deg.CUCAngle90Deg();
      this.FUCAngleActual = new UCLambdaPlateController.CUCAngleUnit();
      this.FUCAnglePreset = new UCLambdaPlateController.CUCAngleUnit();
      this.FUCPeriodHalf = new UCLambdaPlateController.CUCPeriodHalf();
      this.FUCMicroStepResolution = new UCLambdaPlateController.CUCMicroStepResolution();
      this.SuspendLayout();
      // 
      // btnMoveMaximum
      // 
      this.btnMoveMaximum.Location = new System.Drawing.Point(121, 445);
      this.btnMoveMaximum.Name = "btnMoveMaximum";
      this.btnMoveMaximum.Size = new System.Drawing.Size(108, 31);
      this.btnMoveMaximum.TabIndex = 2;
      this.btnMoveMaximum.Text = "Move Maximum";
      this.btnMoveMaximum.UseVisualStyleBackColor = true;
      this.btnMoveMaximum.Click += new System.EventHandler(this.btnMoveMaximum_Click);
      // 
      // btnMoveMinimum
      // 
      this.btnMoveMinimum.Location = new System.Drawing.Point(121, 476);
      this.btnMoveMinimum.Name = "btnMoveMinimum";
      this.btnMoveMinimum.Size = new System.Drawing.Size(108, 31);
      this.btnMoveMinimum.TabIndex = 3;
      this.btnMoveMinimum.Text = "Move Minimum";
      this.btnMoveMinimum.UseVisualStyleBackColor = true;
      this.btnMoveMinimum.Click += new System.EventHandler(this.btnMoveMinimum_Click);
      // 
      // FUCAngle90Deg
      // 
      this.FUCAngle90Deg.Angle = 0D;
      this.FUCAngle90Deg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.FUCAngle90Deg.Delta = 0.001D;
      this.FUCAngle90Deg.Location = new System.Drawing.Point(3, 3);
      this.FUCAngle90Deg.Name = "FUCAngle90Deg";
      this.FUCAngle90Deg.Size = new System.Drawing.Size(111, 505);
      this.FUCAngle90Deg.TabIndex = 0;
      // 
      // FUCAngleActual
      // 
      this.FUCAngleActual.Angle = 0D;
      this.FUCAngleActual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.FUCAngleActual.Header = "Angle Actual [deg]";
      this.FUCAngleActual.Location = new System.Drawing.Point(120, 321);
      this.FUCAngleActual.Name = "FUCAngleActual";
      this.FUCAngleActual.Size = new System.Drawing.Size(109, 118);
      this.FUCAngleActual.TabIndex = 5;
      // 
      // FUCAnglePreset
      // 
      this.FUCAnglePreset.Angle = 0D;
      this.FUCAnglePreset.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.FUCAnglePreset.Header = "Angle Target [deg]";
      this.FUCAnglePreset.Location = new System.Drawing.Point(120, 6);
      this.FUCAnglePreset.Name = "FUCAnglePreset";
      this.FUCAnglePreset.Size = new System.Drawing.Size(109, 118);
      this.FUCAnglePreset.TabIndex = 4;
      // 
      // FUCPeriodHalf
      // 
      this.FUCPeriodHalf.Location = new System.Drawing.Point(121, 222);
      this.FUCPeriodHalf.Name = "FUCPeriodHalf";
      this.FUCPeriodHalf.Size = new System.Drawing.Size(162, 91);
      this.FUCPeriodHalf.TabIndex = 7;
      // 
      // FUCMicroStepResolution
      // 
      this.FUCMicroStepResolution.Location = new System.Drawing.Point(121, 124);
      this.FUCMicroStepResolution.Name = "FUCMicroStepResolution";
      this.FUCMicroStepResolution.Size = new System.Drawing.Size(134, 96);
      this.FUCMicroStepResolution.TabIndex = 8;
      // 
      // CUCLambdaPlateController
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCMicroStepResolution);
      this.Controls.Add(this.FUCPeriodHalf);
      this.Controls.Add(this.FUCAngleActual);
      this.Controls.Add(this.FUCAnglePreset);
      this.Controls.Add(this.btnMoveMinimum);
      this.Controls.Add(this.btnMoveMaximum);
      this.Controls.Add(this.FUCAngle90Deg);
      this.Name = "CUCLambdaPlateController";
      this.Size = new System.Drawing.Size(313, 538);
      this.ResumeLayout(false);

    }

    #endregion

    private UCAngle90Deg.CUCAngle90Deg FUCAngle90Deg;
    private System.Windows.Forms.Button btnMoveMaximum;
    private System.Windows.Forms.Button btnMoveMinimum;
    private CUCAngleUnit FUCAnglePreset;
    private CUCAngleUnit FUCAngleActual;
    private CUCPeriodHalf FUCPeriodHalf;
    private CUCMicroStepResolution FUCMicroStepResolution;
  }
}
