﻿namespace UCMultiColorText
{
  partial class CUCMultiColorText
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.cmsMultiColorText = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mitClearAllLines = new System.Windows.Forms.ToolStripMenuItem();
      this.addSpacelineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.rtbView = new System.Windows.Forms.RichTextBox();
      this.cmsMultiColorText.SuspendLayout();
      this.SuspendLayout();
      // 
      // cmsMultiColorText
      // 
      this.cmsMultiColorText.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitClearAllLines,
            this.addSpacelineToolStripMenuItem});
      this.cmsMultiColorText.Name = "cmsMultiColorText";
      this.cmsMultiColorText.Size = new System.Drawing.Size(150, 48);
      // 
      // mitClearAllLines
      // 
      this.mitClearAllLines.Name = "mitClearAllLines";
      this.mitClearAllLines.Size = new System.Drawing.Size(149, 22);
      this.mitClearAllLines.Text = "Clear all Lines";
      this.mitClearAllLines.Click += new System.EventHandler(this.mitClearAllLines_Click);
      // 
      // addSpacelineToolStripMenuItem
      // 
      this.addSpacelineToolStripMenuItem.Name = "addSpacelineToolStripMenuItem";
      this.addSpacelineToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
      this.addSpacelineToolStripMenuItem.Text = "Add Spaceline";
      this.addSpacelineToolStripMenuItem.Click += new System.EventHandler(this.mitAddSpaceline_Click);
      // 
      // rtbView
      // 
      this.rtbView.ContextMenuStrip = this.cmsMultiColorText;
      this.rtbView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.rtbView.HideSelection = false;
      this.rtbView.Location = new System.Drawing.Point(0, 0);
      this.rtbView.Name = "rtbView";
      this.rtbView.ReadOnly = true;
      this.rtbView.Size = new System.Drawing.Size(232, 232);
      this.rtbView.TabIndex = 1;
      this.rtbView.TabStop = false;
      this.rtbView.Text = "";
      // 
      // CUCMultiColorText
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.rtbView);
      this.Name = "CUCMultiColorText";
      this.Size = new System.Drawing.Size(232, 232);
      this.cmsMultiColorText.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ContextMenuStrip cmsMultiColorText;
    private System.Windows.Forms.ToolStripMenuItem mitClearAllLines;
    private System.Windows.Forms.ToolStripMenuItem addSpacelineToolStripMenuItem;
    private System.Windows.Forms.RichTextBox rtbView;
  }
}
