﻿namespace UCAngle90Deg
{
  partial class CUCAngle90Deg
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.panel4 = new System.Windows.Forms.Panel();
      this.lblHeader = new System.Windows.Forms.Label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.pnlAngle = new System.Windows.Forms.Panel();
      this.label7 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.trbAngle = new System.Windows.Forms.TrackBar();
      this.panel3 = new System.Windows.Forms.Panel();
      this.btnStepDown = new System.Windows.Forms.Button();
      this.btnStepUp = new System.Windows.Forms.Button();
      this.panel2 = new System.Windows.Forms.Panel();
      this.lblAngle = new System.Windows.Forms.Label();
      this.tmrKeyUp = new System.Windows.Forms.Timer(this.components);
      this.tmrKeyDown = new System.Windows.Forms.Timer(this.components);
      this.panel4.SuspendLayout();
      this.panel1.SuspendLayout();
      this.pnlAngle.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.trbAngle)).BeginInit();
      this.panel3.SuspendLayout();
      this.panel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel4
      // 
      this.panel4.Controls.Add(this.lblHeader);
      this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel4.Location = new System.Drawing.Point(0, 0);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(109, 79);
      this.panel4.TabIndex = 6;
      // 
      // lblHeader
      // 
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold);
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(109, 79);
      this.lblHeader.TabIndex = 0;
      this.lblHeader.Text = "Angle Preset [deg]";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.pnlAngle);
      this.panel1.Controls.Add(this.trbAngle);
      this.panel1.Controls.Add(this.panel3);
      this.panel1.Controls.Add(this.panel2);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 79);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(109, 424);
      this.panel1.TabIndex = 7;
      // 
      // pnlAngle
      // 
      this.pnlAngle.BackColor = System.Drawing.SystemColors.Control;
      this.pnlAngle.Controls.Add(this.label7);
      this.pnlAngle.Controls.Add(this.label8);
      this.pnlAngle.Controls.Add(this.label9);
      this.pnlAngle.Controls.Add(this.label10);
      this.pnlAngle.Controls.Add(this.label3);
      this.pnlAngle.Controls.Add(this.label4);
      this.pnlAngle.Controls.Add(this.label5);
      this.pnlAngle.Controls.Add(this.label6);
      this.pnlAngle.Controls.Add(this.label2);
      this.pnlAngle.Controls.Add(this.label1);
      this.pnlAngle.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlAngle.Location = new System.Drawing.Point(0, 32);
      this.pnlAngle.Name = "pnlAngle";
      this.pnlAngle.Size = new System.Drawing.Size(60, 331);
      this.pnlAngle.TabIndex = 8;
      // 
      // label7
      // 
      this.label7.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label7.Location = new System.Drawing.Point(0, -5);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(60, 34);
      this.label7.TabIndex = 20;
      this.label7.Text = "90";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label8
      // 
      this.label8.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label8.Location = new System.Drawing.Point(0, 29);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(60, 34);
      this.label8.TabIndex = 19;
      this.label8.Text = "80";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label9
      // 
      this.label9.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label9.Location = new System.Drawing.Point(0, 63);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(60, 34);
      this.label9.TabIndex = 18;
      this.label9.Text = "70";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label10
      // 
      this.label10.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label10.Location = new System.Drawing.Point(0, 97);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(60, 34);
      this.label10.TabIndex = 17;
      this.label10.Text = "60";
      this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label3
      // 
      this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label3.Location = new System.Drawing.Point(0, 131);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(60, 34);
      this.label3.TabIndex = 16;
      this.label3.Text = "50";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label4
      // 
      this.label4.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label4.Location = new System.Drawing.Point(0, 165);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(60, 34);
      this.label4.TabIndex = 15;
      this.label4.Text = "40";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label5.Location = new System.Drawing.Point(0, 199);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(60, 34);
      this.label5.TabIndex = 14;
      this.label5.Text = "30";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label6
      // 
      this.label6.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label6.Location = new System.Drawing.Point(0, 233);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(60, 34);
      this.label6.TabIndex = 13;
      this.label6.Text = "20";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label2.Location = new System.Drawing.Point(0, 267);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(60, 34);
      this.label2.TabIndex = 12;
      this.label2.Text = "10";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.label1.Location = new System.Drawing.Point(0, 301);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(60, 30);
      this.label1.TabIndex = 11;
      this.label1.Text = "0";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // trbAngle
      // 
      this.trbAngle.AutoSize = false;
      this.trbAngle.Dock = System.Windows.Forms.DockStyle.Right;
      this.trbAngle.LargeChange = 10;
      this.trbAngle.Location = new System.Drawing.Point(59, 32);
      this.trbAngle.Maximum = 90;
      this.trbAngle.Name = "trbAngle";
      this.trbAngle.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.trbAngle.Size = new System.Drawing.Size(50, 331);
      this.trbAngle.TabIndex = 7;
      this.trbAngle.TickFrequency = 5;
      this.trbAngle.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
      this.trbAngle.ValueChanged += new System.EventHandler(this.trbAngle_ValueChanged);
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.btnStepDown);
      this.panel3.Controls.Add(this.btnStepUp);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel3.Location = new System.Drawing.Point(0, 363);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(109, 61);
      this.panel3.TabIndex = 6;
      // 
      // btnStepDown
      // 
      this.btnStepDown.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btnStepDown.Location = new System.Drawing.Point(0, 30);
      this.btnStepDown.Name = "btnStepDown";
      this.btnStepDown.Size = new System.Drawing.Size(109, 31);
      this.btnStepDown.TabIndex = 6;
      this.btnStepDown.Text = "Step Down";
      this.btnStepDown.UseVisualStyleBackColor = true;
      this.btnStepDown.Click += new System.EventHandler(this.btnStepDown_Click);
      // 
      // btnStepUp
      // 
      this.btnStepUp.Dock = System.Windows.Forms.DockStyle.Top;
      this.btnStepUp.Location = new System.Drawing.Point(0, 0);
      this.btnStepUp.Name = "btnStepUp";
      this.btnStepUp.Size = new System.Drawing.Size(109, 30);
      this.btnStepUp.TabIndex = 5;
      this.btnStepUp.Text = "Step Up";
      this.btnStepUp.UseVisualStyleBackColor = true;
      this.btnStepUp.Click += new System.EventHandler(this.btnStepUp_Click);
      // 
      // panel2
      // 
      this.panel2.BackColor = System.Drawing.SystemColors.Info;
      this.panel2.Controls.Add(this.lblAngle);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel2.ForeColor = System.Drawing.Color.DarkCyan;
      this.panel2.Location = new System.Drawing.Point(0, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(109, 32);
      this.panel2.TabIndex = 5;
      // 
      // lblAngle
      // 
      this.lblAngle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.lblAngle.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lblAngle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblAngle.Location = new System.Drawing.Point(0, 0);
      this.lblAngle.Name = "lblAngle";
      this.lblAngle.Size = new System.Drawing.Size(109, 32);
      this.lblAngle.TabIndex = 1;
      this.lblAngle.Text = "0";
      this.lblAngle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // tmrKeyUp
      // 
      this.tmrKeyUp.Interval = 500;
      this.tmrKeyUp.Tick += new System.EventHandler(this.tmrKeyUp_Tick);
      // 
      // tmrKeyDown
      // 
      this.tmrKeyDown.Interval = 500;
      this.tmrKeyDown.Tick += new System.EventHandler(this.tmrKeyDown_Tick);
      // 
      // CUCAngle90Deg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.panel4);
      this.Name = "CUCAngle90Deg";
      this.Size = new System.Drawing.Size(109, 506);
      this.panel4.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.pnlAngle.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.trbAngle)).EndInit();
      this.panel3.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel4;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel pnlAngle;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TrackBar trbAngle;
    private System.Windows.Forms.Panel panel3;
    private System.Windows.Forms.Button btnStepDown;
    private System.Windows.Forms.Button btnStepUp;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Label lblAngle;
    private System.Windows.Forms.Timer tmrKeyUp;
    private System.Windows.Forms.Timer tmrKeyDown;

  }
}
