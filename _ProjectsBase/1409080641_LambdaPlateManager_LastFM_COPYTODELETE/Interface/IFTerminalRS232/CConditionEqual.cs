﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IFTerminalRS232
{
  public class CCondition //: CStatement
  {
    private Object FOperand0;
    private Object FOperand1;

    public CCondition(Object operand0, Object operand1)
    {
      FOperand0 = operand0;
      FOperand1 = operand1;
    }


    public static Boolean IsEqual(Object operand0, Object operand1)
    {
      return operand0 == operand1;
    }

  }
}
