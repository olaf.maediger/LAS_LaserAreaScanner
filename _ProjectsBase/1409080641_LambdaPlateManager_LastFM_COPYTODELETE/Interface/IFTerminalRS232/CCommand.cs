﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UCNotifier;
using HWComPort;

namespace IFTerminalRS232
{
  //
  //public delegate void DOnStatementExecuteBegin();
  //public delegate void DOnStatementExecuteBusy();
  //public delegate void DOnStatementExecuteResponse(Guid portid, String data);
  //public delegate void DOnStatementExecuteEnd();
  //
  public delegate void DParentOnExecuteEnd(CStatement command);

	public enum EStatementState 
	{ 
		Error = -1,
		Init = 0,
		Begin = 1, 
		Busy = 2, 
    Response = 3,
		Abort = 4,
		End = 5
	};

	public abstract class CStatement //: CStatementlist // local Statementlist
	{
    protected const Int32 INIT_WAITFOR = 1000;

    private CNotifier FNotifier;        // Only Reference!
		protected CNotifier Notifier
		{
			get { return FNotifier; }
		}

		private CStatementlist FStatementlistParent;  // Only Reference (Parent-Statementlist)!
    protected CStatementlist StatementlistParent
    {
			get { return FStatementlistParent; }
		}
    protected CVariablelist VariablelistParent
    {
      get { return FStatementlistParent.Variablelist; }
    }


		private CStatementlist FStatementlistLocal;  // local Statementlist (Block-Statements)
		protected CStatementlist StatementlistLocal
		{
			get { return FStatementlistLocal; }
		}

    private CStatement FParent;           // Only Reference!
    protected void SetParent(CStatement parent)
    {
      FParent = parent;
    }





    private String FName;
    protected String Name
    {
      get { return FName; }
    }

    private String FStatementText;
    protected String StatementText
    {
      get { return FStatementText; }
    }
    public void SetStatementText(String value)
    {
      FStatementText = value;
    }

    private EStatementState FState; // Threadlist-Manager!

		private void SetState(EStatementState value)
		{
			if (value != FState)
			{
				FState = value;
        //
				// Debug 
        String Line = String.Format("Statement[{0}]: State[{1}]", Name, State);
        // Debug 
        Notifier.Write(Line);
      }
		}
		public EStatementState State
		{
			get { return FState; }
			set { SetState(value); }
		}
		protected Thread FThread;
		private CDevice FDevice;
		protected CDevice Device
		{
			get { return FDevice; }
		}

		private CHWComPort FComPort;
		protected CHWComPort HWComPort
		{
			get { return FComPort; }
		}


    //private DOnStatementExecuteBegin FOnStatementExecuteBegin;
    //private DOnStatementExecuteBusy FOnStatementExecuteBusy;
    //private DOnStatementExecuteResponse FOnStatementExecuteResponse;
    //private DOnStatementExecuteEnd FOnStatementExecuteEnd;
    //
    //private DParentOnExecuteEnd FParentOnExecuteEnd;


    private CVariable FVariable;
    protected CVariable Variable
    {
      get { return FVariable;  }
      set { FVariable = value; }
    }



    private CEvent FWaitResponse;
    private Int32 FLineCount;

    public CStatement(CStatementlist commandlistparent,
										String name,
                    String commandtext,
										CNotifier notifier,
									  CDevice device,
										CHWComPort comport)
		{
      FStatementlistParent = commandlistparent;
      FStatementlistLocal = null;
      FName = name;
      FStatementText = commandtext;
      FNotifier = notifier;
			FDevice = device;
			FComPort = comport;
      FThread = null;
      FWaitResponse = new CEvent();
		}

    protected void IncrementLineCount()
    {
      FLineCount++;
    }

    protected void BaseOnLineReceived()
    {
      IncrementLineCount();
      if (1 <= FLineCount)
      {
        FWaitResponse.Set();
      }
    }

		public void Abort()
		{
			State = EStatementState.Abort;
      FThread.Abort();
			AbortExecution();
		}

		public void Join()
		{
			FThread.Join();
		}

    // -> Member!!!public abstract void SelfOnLineReceived(Guid comportid,
    //                                        String line);


    /*//
    protected void SetOnStatementExecuteBegin(DOnStatementExecuteBegin value)
    {
      FOnStatementExecuteBegin = value;
    }

    protected void SetOnStatementExecuteBusy(DOnStatementExecuteBusy value)
    {
      FOnStatementExecuteBusy = value;
    }

    protected void SetOnStatementExecuteResponse(DOnStatementExecuteResponse value)
    {
      FOnStatementExecuteResponse = value;
    }

    protected void SetOnStatementExecuteEnd(DOnStatementExecuteEnd value)
    {
      FOnStatementExecuteEnd = value;
    }*/




    public Boolean StartExecution(DParentOnExecuteEnd value)
		{ // debug 
      try
      {
        /*
        //FNotifier.Write(String.Format("Statement[{0}].Activate", this.ToString()));
        Console.WriteLine(String.Format("Statement[{0}].Activate", this.ToString()));
        FParentOnExecuteEnd = value;
        if (null == FThread)
        {
          FThread = new Thread(new ThreadStart(ThreadExecute));
          // debug FNotifier.Write(String.Format("Thread[{0}].Start", this.ToString()));
          Console.WriteLine(String.Format("Thread[{0}].Start", this.ToString()));
          FThread.Start();
          return true;
        }*/
        return false;
      }
      catch (Exception)
      {
        FNotifier.Write(" Cannot start Statement-Execution");
        return false;
      }
    }

    public Boolean AbortExecution()
		{
      try
      {
        if (FThread is Thread)
        {
					State = EStatementState.Abort;
          return true;
				}
        return false;
      }
      catch (Exception)
      {
        FNotifier.Write(" Cannot abort Statement-Execution");
        return false;
      }
    }


    //
    //---------------------------------------------------------------------------
    //  Segment - Execution
    //---------------------------------------------------------------------------
    /*/
    protected void SelfExecuteBegin()
    {
      Console.WriteLine(String.Format("Statement[{0}].ExecuteBegin", this.ToString()));
      State = EStatementState.Begin;
      //
      if (FOnStatementExecuteBegin is DOnStatementExecuteBegin)
      {
        FOnStatementExecuteBegin();
        Thread.Sleep(1000);
      }
    }

    protected void SelfExecuteBusy()
    {
      State = EStatementState.Busy;
      Console.WriteLine(String.Format("Statement[{0}].ExecuteBusy", this.ToString()));
      //
      if (FOnStatementExecuteBusy is DOnStatementExecuteBusy)
      {
        FOnStatementExecuteBusy();
        Thread.Sleep(1000);
      }
    }

    protected void SelfExecuteResponse()
    {     
      if (FOnStatementExecuteResponse is DOnStatementExecuteResponse)
      {
        State = EStatementState.Response;
        Console.WriteLine(String.Format("Statement[{0}].ExecuteResponse", this.ToString()));
        //
        FLineCount = 0;
        FWaitResponse.Reset();
        //
        // debug Thread.Sleep(1);
        // not here!!! HWComPort.WriteLine(StatementText);
        if (!FWaitResponse.WaitFor(INIT_WAITFOR))
        {
          State = EStatementState.Error;
          FNotifier.Error(CIFTerminalRS232.HEADER_LIBRARY + " " + FName,
                          1, "No response of hardware");
        }
        else
        {
          FOnStatementExecuteResponse(Guid.Empty, "ABC");
        }
      }
    }

    protected void SelfExecuteEnd()
    {
      State = EStatementState.End;
      Console.WriteLine(String.Format("Statement[{0}].ExecuteEnd", this.ToString()));
      //
      if (FOnStatementExecuteEnd is DOnStatementExecuteEnd)
      {
        FOnStatementExecuteEnd();
        Thread.Sleep(1000);
      }
    }

    protected void ThreadExecute()
    {
      Console.WriteLine(String.Format("Statement[{0}] startet", this.ToString()));
      SelfExecuteBegin();
      SelfExecuteBusy();
      SelfExecuteResponse();
      SelfExecuteEnd();
      //
      FParentOnExecuteEnd(this);
      // debug Thread.Sleep(100);
      FThread = null;
      Console.WriteLine(String.Format("Statement[{0}] finnished", this.ToString()));
      if (FVariable is CVariable)
      {
        VariablelistParent.Add(FVariable);
      }
    }



    public virtual Boolean ExecuteDataReceived(String data)
    {
      return true;
    }*/

  }
}
