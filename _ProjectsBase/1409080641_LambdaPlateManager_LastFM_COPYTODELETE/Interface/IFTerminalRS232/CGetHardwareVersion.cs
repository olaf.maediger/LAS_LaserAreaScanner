﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using HWComPort;

namespace IFTerminalRS232
{
	class CGetHardwareVersion : CStatement
	{
    public const String INIT_NAME = "GetHardwareVersion";
    public const String COMMAND_TEXT = "GHV";

		public CGetHardwareVersion(CStatementlist parent,
															 CNotifier notifier,
															 CDevice device,
															 CHWComPort comport)
      : base(parent, INIT_NAME, COMMAND_TEXT, notifier, device, comport)
    {
      SetParent(this);
      //SetOnStatementExecuteResponse(SelfOnSerialResponseReceived);
    }

    public void SelfOnSerialResponseReceived(Guid comportid,
                                             String line)
    {
      base.BaseOnLineReceived();
      String Line = String.Format("{0}: {1}", Name, line);
			Notifier.Write(CIFTerminalRS232.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
			String[] Tokenlist = line.Split(' ');
			if (2 < Tokenlist.Length)
			{
       //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Statementlist.Library.RefreshHardwareVersion(Tokenlist[2]);
			}
		}

	}
}

