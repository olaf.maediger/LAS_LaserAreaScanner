#
# ------------------------------------------------------------------
#   LAS - PCMqttLaserAreaScanner
# ------------------------------------------------------------------
#   Version: 02V03
#   Date   : 200617
#   Time   : 0820
#   Author : OMDevelop
#
import WindowMainMqtt as WM
#
TITLE_APPLICATION                  = "LTC - (Mqtt)LehmannTrainController"
#
###################################################################
# Main
###################################################################
print("*** " + TITLE_APPLICATION + ": begin")
#
WindowMainMqtt = WM.CWindowMainMqtt()
WindowMainMqtt.Execute()
#
print("*** " + TITLE_APPLICATION + ": end")
#
