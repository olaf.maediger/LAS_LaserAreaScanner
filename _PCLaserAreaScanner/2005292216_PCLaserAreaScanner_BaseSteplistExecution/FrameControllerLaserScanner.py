#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameLaserScanner
# ------------------------------------------------------------------
#   Version: 01V01
#   Date   : 200518
#   Time   : 1143
#   Author : OMDevelop
#
import tkinter as tk
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
WIDTH_BUTTON                    = 24
#
INITDATA_SECTION                = "FrameControllerLaserScanner"
#
NAME_PULSEPERIOD_MS             = "PulsePeriod[ms]"
NAME_PULSECOUNT                 = "PulseCount[1]"
NAME_POSITIONX_STP              = "PositionX[stp]"
NAME_POSITIONY_STP              = "PositionY[stp]"
NAME_DELAYMOTION_MS             = "DelayMotion[ms]"
#
INIT_PULSEPERIOD_MS             = "1000"
INIT_PULSECOUNT                 = "1"
INIT_POSITIONX_STP              = "2000"
INIT_POSITIONY_STP              = "2000"
INIT_DELAYMOTION_MS             = "10"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameControllerLaserScanner(tk.Frame):
#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 880, height = 269, \
                          padx = 6, pady = 6, background = "#ffffee")
        self.grid_propagate(0)
        #--------------------------------------------------------------
        # PulsePeriod
        #--------------------------------------------------------------
        self.btnSetPulsePeriod = tk.Button(self, width = WIDTH_BUTTON, \
                                           text = "Set PulsePeriod", \
                                           command = self.OnSetPulsePeriod)
        self.btnSetPulsePeriod.grid(row = 0, column = 0, \
                                    sticky = tk.W, padx = 2, pady = 2)
        #
        self.btnGetPulsePeriod = tk.Button(self, width = WIDTH_BUTTON, \
                                           text = "Get PulsePeriod", \
                                           command = self.OnGetPulsePeriod)
        self.btnGetPulsePeriod.grid(row = 0, column = 1, \
                                    sticky = tk.W, padx = 2, pady = 2)
        #
        self.lblPulsePeriod = tk.Label(self, text = "PulsePeriod[ms]", width = 14)
        self.lblPulsePeriod.grid(row = 0, column = 2, sticky = "w", padx = 10)
        #
        self.spbPulsePeriod = tk.Spinbox(self, from_ = 1000, to = 100000, increment = 1000, \
                                         width = 8, justify = "center")
        self.spbPulsePeriod.grid(row = 0, column = 3, sticky = "w", padx = 6)
        self.spbPulsePeriod.delete(0, tk.END)
        self.spbPulsePeriod.insert(0, INIT_PULSEPERIOD_MS)
        #--------------------------------------------------------------
        # PulseLaser
        #--------------------------------------------------------------
        self.btnPulseLaserAbort = tk.Button(self, width = WIDTH_BUTTON, \
                                           text = "Pulse Laser Abort", \
                                           command = self.OnPulseLaserAbort)
        self.btnPulseLaserAbort.grid(row = 1, column = 0, \
                                     sticky = tk.W, padx = 2, pady = 2)
        #
        self.btnPulseLaserCount = tk.Button(self, width = WIDTH_BUTTON, \
                                            text = "Pulse Laser Count", \
                                            command = self.OnPulseLaserCount)
        self.btnPulseLaserCount.grid(row = 1, column = 1, \
                                     sticky = tk.W, padx = 2, pady = 2)
        #
        self.lblPulseCount = tk.Label(self, text = "PulseCount[1]", width = 14)
        self.lblPulseCount.grid(row = 1, column = 2, sticky = "w", padx = 10)
        #
        self.spbPulseCount = tk.Spinbox(self, from_ = 1, to = 10000, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPulseCount.grid(row = 1, column = 3, sticky = "w", padx = 6)
        self.spbPulseCount.delete(0, tk.END)
        self.spbPulseCount.insert(0, INIT_PULSECOUNT)
        #--------------------------------------------------------------
        # PulseLaserCount
        #--------------------------------------------------------------
        self.btnMovePositionX = tk.Button(self, width = WIDTH_BUTTON, \
                                          text = "Move PositionX", \
                                          command = self.OnMovePositionX)
        self.btnMovePositionX.grid(row = 2, column = 1, \
                                   sticky = tk.W, padx = 2, pady = 2)
        #
        self.lblPositionX = tk.Label(self, text = "PositionX[stp]", width = 14)
        self.lblPositionX.grid(row = 2, column = 2, sticky = "w", padx = 10)
        #
        self.spbPositionX = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionX.grid(row = 2, column = 3, sticky = "w", padx = 6)
        self.spbPositionX.delete(0, tk.END)
        self.spbPositionX.insert(0, INIT_POSITIONX_STP)
        #
        self.btnMovePositionY = tk.Button(self, width = WIDTH_BUTTON, \
                                          text = "Move PositionY", \
                                          command = self.OnMovePositionY)
        self.btnMovePositionY.grid(row = 3, column = 1, \
                                   sticky = tk.W, padx = 2, pady = 2)
        #
        self.lblPositionY = tk.Label(self, text = "PositionY[stp]", width = 14)
        self.lblPositionY.grid(row = 3, column = 2, sticky = "w", padx = 10)
        #
        self.spbPositionY = tk.Spinbox(self, from_ = 0, to = 4095, increment = 1, \
                                         width = 8, justify = "center")
        self.spbPositionY.grid(row = 3, column = 3, sticky = "w", padx = 6)
        self.spbPositionY.delete(0, tk.END)
        self.spbPositionY.insert(0, INIT_POSITIONY_STP)
        #
        #--------------------------------------------------------------
        # MovePositionPulse
        #--------------------------------------------------------------
        self.btnMovePositionPulse = tk.Button(self, width = WIDTH_BUTTON, \
                                              text = "Move Position Pulse", \
                                          command = self.OnMovePositionPulse)
        self.btnMovePositionPulse.grid(row = 4, column = 1, \
                                       sticky = tk.W, padx = 2, pady = 2)
        #
        self.lblDelayMotion = tk.Label(self, text = "DelayMotion[ms]", width = 14)
        self.lblDelayMotion.grid(row = 4, column = 2, sticky = "w", padx = 10)
        #
        self.spbDelayMotion = tk.Spinbox(self, from_ = 0, to = 9999, increment = 1, \
                                         width = 8, justify = "center")
        self.spbDelayMotion.grid(row = 4, column = 3, sticky = "w", padx = 6)
        self.spbDelayMotion.delete(0, tk.END)
        self.spbDelayMotion.insert(0, INIT_DELAYMOTION_MS)
        #
        self.lblLaserLed = tk.Label(self, text = "---", width = 4)
        self.lblLaserLed.grid(row = 4, column = 4, padx = 3)
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def SetCBOnSetPulsePeriod(self, callback):
        self.CBOnSetPulsePeriod = callback
    #
    def SetCBOnGetPulsePeriod(self, callback):
        self.CBOnGetPulsePeriod = callback
    #
    def SetCBOnPulseLaserAbort(self, callback):
        self.CBOnPulseLaserAbort = callback
    #
    def SetCBOnPulseLaserCount(self, callback):
        self.CBOnPulseLaserCount = callback
    #
    def SetCBOnMovePositionX(self, callback):
        self.CBOnMovePositionX = callback
    #
    def SetCBOnMovePositionY(self, callback):
        self.CBOnMovePositionY = callback
    #
    def SetCBOnMovePositionPulse(self, callback):
        self.CBOnMovePositionPulse = callback
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    def OnSetPulsePeriod(self):
        if (self.CBOnSetPulsePeriod):
            #self.RemoteSwitchOn = True
            PP = self.spbPulsePeriod.get()
            self.CBOnSetPulsePeriod(PP)
    #
    def OnGetPulsePeriod(self):
        if (self.CBOnGetPulsePeriod):
            #self.RemoteSwitchOn = False
            #ST = self.spbPulsePeriod.get()
            self.CBOnGetPulsePeriod()
    #
    def OnPulseLaserAbort(self):
        if (self.CBOnPulseLaserAbort):
            self.CBOnPulseLaserAbort()
    #
    def OnPulseLaserCount(self):
        if (self.CBOnPulseLaserCount):
            PP = self.spbPulsePeriod.get()
            PC = self.spbPulseCount.get()
            self.CBOnPulseLaserCount(PP, PC)
    #
    def OnMovePositionX(self):
        if (self.CBOnMovePositionX):
            PX = self.spbPositionX.get()
            self.CBOnMovePositionX(PX)
    #
    def OnMovePositionY(self):
        if (self.CBOnMovePositionY):
            PY = self.spbPositionY.get()
            self.CBOnMovePositionY(PY)
    #
    def OnMovePositionPulse(self):
        if (self.CBOnMovePositionPulse):
            PP = self.spbPulsePeriod.get()
            PC = self.spbPulseCount.get()
            PX = self.spbPositionX.get()
            PY = self.spbPositionY.get()
            DM = self.spbDelayMotion.get()
            self.CBOnMovePositionPulse(PX, PY, PP, PC, DM)           
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        PP = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_PULSEPERIOD_MS, \
                                        INIT_PULSEPERIOD_MS)
        PC = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_PULSECOUNT, \
                                        INIT_PULSECOUNT)
        PX = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONX_STP, \
                                        INIT_POSITIONX_STP)
        PY = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_POSITIONY_STP, \
                                        INIT_POSITIONY_STP)
        DM = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_DELAYMOTION_MS, \
                                        INIT_DELAYMOTION_MS)
        self.spbPulsePeriod.delete(0, tk.END)
        self.spbPulsePeriod.insert(0, PP)
        self.spbPulseCount.delete(0, tk.END)
        self.spbPulseCount.insert(0, PC)
        self.spbPositionX.delete(0, tk.END)
        self.spbPositionX.insert(0, PX)
        self.spbPositionY.delete(0, tk.END)
        self.spbPositionY.insert(0, PY)
        self.spbDelayMotion.delete(0, tk.END)
        self.spbDelayMotion.insert(0, DM)        
        #
        # self.OnSet...
        #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PULSEPERIOD_MS, \
                                 self.spbPulsePeriod.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PULSECOUNT, \
                                 self.spbPulseCount.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONX_STP, \
                                 self.spbPositionX.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_POSITIONY_STP, \
                                 self.spbPositionY.get())
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_DELAYMOTION_MS, \
                                 self.spbDelayMotion.get())
    #
    #---------------------------------------------------------------
    # Management - Event
    #--------------------------------------------------------------
    def RefreshEvent(self, tokens):
        if (2 < len(tokens)):
            # <!> <MPP> <percent> 
            if ("MPP" == tokens[1]):
                PC = int(tokens[2])
                if (PC < 100):
                    self.lblLaserLed.config(bg = "magenta")
                    self.lblLaserLed.config(bg = "green")
                self.lblLaserLed.config(text = str(PC) + "%")    
    #
    #---------------------------------------------------------------
    # Management - Response
    #--------------------------------------------------------------
    def RefreshResponse(self, tokens):
        if (1 < len(tokens)):
            Command = tokens[1]
            Parameters = []
            for TI in range(2, len(tokens)):
                Parameters.append(tokens[TI])               
            self.RefreshCommandData(Command, Parameters)
    #        
    def RefreshCommandData(self, command, parameters):
        if (0 < len(parameters)):
            # PulsePeriod
            if ("GPP" == command) or ("SPP" == command):
                self.spbPulsePeriod.delete(0, tk.END)
                self.spbPulsePeriod.insert(0, float(parameters[0]))            
                return True
            # MovePositionX
            if ("MPX" == command):
                self.spbPositionX.delete(0, tk.END)
                self.spbPositionX.insert(0, int(parameters[0]))
                return True
            # MovePositionY
            if ("MPY" == command):
                self.spbPositionY.delete(0, tk.END)
                self.spbPositionY.insert(0, int(parameters[0]))
                return True
        if (1 < len(parameters)):
            # PulseLaserCount
            if ("PLC" == command):
                self.spbPulsePeriod.delete(0, tk.END)
                self.spbPulsePeriod.insert(0, float(parameters[0]))            
                self.spbPulseCount.delete(0, tk.END)
                self.spbPulseCount.insert(0, int(parameters[1]))
                return True
        if (4 < len(parameters)):
            # MovePositionPulse
            if ("MPP" == command):
                self.spbPositionX.delete(0, tk.END)
                self.spbPositionX.insert(0, int(parameters[0]))
                self.spbPositionY.delete(0, tk.END)
                self.spbPositionY.insert(0, int(parameters[1]))                
                self.spbPulsePeriod.delete(0, tk.END)
                self.spbPulsePeriod.insert(0, float(parameters[2]))                
                self.spbPulseCount.delete(0, tk.END)
                self.spbPulseCount.insert(0, int(parameters[3]))
                self.spbDelayMotion.delete(0, tk.END)
                self.spbDelayMotion.insert(0, int(parameters[4]))
                return True
        return False
#
# MPP 2000 2000 100.0 10 10        