import paho.mqtt.client as mqtt
#
#--------------------------------------------------------------
### Init Mqtt
#--------------------------------------------------------------
class CMqttClient:
    #    
    def __init__(self):
        self.Client = mqtt.Client()
        self.Client.on_connect = self.OnMqttConnect
        self.Client.on_disconnect = self.OnMqttDisconnect
        self.Client.on_message = self.OnMqttMessage

    def SetCBOnLineReceived(self, cbonlinereceived):
        self.CBOnLineReceived = cbonlinereceived
#
#------------------------------------------------------------------
#   Callback - Mqtt
#------------------------------------------------------------------
    def OnMqttConnect(self, client, userdata, flags, rc):
        print("Mqtt: connected with result code " + str(rc))        
    #
    def OnMqttDisconnect(self, client, userdata, rc):
        print("Mqtt: disconnected")
    #
    def OnMqttMessage(self, client, userdata, msg):
        Line = str(msg.payload.decode('utf-8'))
        self.CBOnLineReceived(Line)
    #
    def Open(self, brokeripaddress, brokerport, keepalive):
        # do not change the following lines!!!
        self.Client.connect(brokeripaddress, brokerport, keepalive)
        # do not change the following lines!!!
        self.Client.loop_start()
    #
    def Close(self):
        # do not change the following lines!!!
        self.Client.disconnect()
        # do not change the following lines!!!
        self.Client.loop_stop()
    #
    def BeginExecution(self):
        self.Client.loop_start()
    #
    def AbortExecution(self):
        self.Client.loop_stop()
    #
    def SubscribeTopic(self, topic):
        self.Client.subscribe(topic)
    #
    def PublishTopic(self, topic, message):
        self.Client.publish(topic, message)
#
        