#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - DeviceResponse
# ------------------------------------------------------------------
#   Version: 02V00
#   Date   : 200303
#   Time   : 0939
#   Author : OMDevelop
#
# Common

TERMINAL_TEXT     = "%"
TERMINAL_EVENT    = "!"
TERMINAL_RESPONSE = "&"
TERMINAL_COMMENT  = "#"
TERMINAL_DEBUG    = "|"
TERMINAL_ERROR    = ":"
TERMINAL_WARNING  = "."


# STATES =   ["Undefined", "Idle", "Welcome", "GetHelp", "GetProgramHeader", \
#             "GetSoftwareVersion", "GetHardwareVersion", "GetProcessCount", \
#             "SetProcessCount", "GetProcessPeriod", "SetProcessPeriod", \
#             "GetProcessWidth", "SetProcessWidth", "StopProcessExecution", \
#             # LedSystem
#             "GetLedSytem", "LedSystemOn", "LedSystemOff", "BlinkLedSystem", \
#             # System
#             "ResetSystem", "PulseWatchDog", \
#             # Serial
#             "WriteLineSerialCommand", "ReadLineSerialCommand", \
#             # I2CDisplay
#             "ClearScreenI2CDisplay", "ShowTextI2CDisplay", \
#             # SDCommand
#             "OpenCommandFile", "WriteCommandFile", "CloseCommandFile", \
#             "ExecuteCommandFile", "AbortCommandFile", \
#             # RgbLedStrip
#             "SetRgbLed"]
#    
class CDeviceResponse():
    """CDeviceResponse..."""

    def __init__(self):
        self.State = "Idle"#self.StateToText(0)
        self.CBOnDateTimeDetected = None
        self.CBOnLocalTimeDetected = None
    #
    def SetCBOnDateTimeDetected(self, callback):
        self.CBOnDateTimeDetected = callback
    #
    def SetCBOnLocalTimeDetected(self, callback):
        self.CBOnLocalTimeDetected = callback
    #
    def SetCBOnTextDetected(self, callback):
        self.CBOnTextDetected = callback
    #
    def SetCBOnEventDetected(self, callback):
        self.CBOnEventDetected = callback
    #
    def SetCBOnResponseDetected(self, callback):
        self.CBOnResponseDetected = callback
    #
    def SetCBOnCommentDetected(self, callback):
        self.CBOnCommentDetected = callback
    #
    def SetCBOnDebugDetected(self, callback):
        self.CBOnDebugDetected = callback
    #
    def SetCBOnWarningDetected(self, callback):
        self.CBOnWarningDetected = callback
    #
    def SetCBOnErrorDetected(self, callback):
        self.CBOnErrorDetected = callback
    #
    #------------------------------------------------------------------------
    # Helper
    #------------------------------------------------------------------------
    # 
    #
    #------------------------------------------------------------------------
    #  Detect - LocalTime / DateTime
    #------------------------------------------------------------------------
    def DetectDateTime(self, text):
        # is "&yy.mm.dd-hh:mm:ss.mmm>"
        if ((18 <= len(text)) and ("." == text[6]) and \
            ("." == text[13]) and (">" == text[17])):
            return True
        return False
    #  
    def DetectLocalTime(self, text): #01234567890123#
        # is "&hh:mm:ss.mmm>[rest]"
        if ((14 <= len(text)) and ("&" == text[0]) and (":" == text[3]) \
            and (":" == text[6]) and ("." == text[9]) and (">" == text[13])):
            return True
        return False
    #
    #---------------------------------------------------------------------------      
    #  Analyse - LocalTime / DateTime
    #---------------------------------------------------------------------------      
    def AnalyseDateTime(self, text):
        #     01234567890123456789012
        # is "&yy.mm.dd-hh:mm:ss.mmm>"        
        text = text.replace('>', '.')
        Tokens = text.split('.')
        Date = Tokens[0]
        Time = Tokens[1]
        Millis = Tokens[2]
        Rest = text[18:]
        # debug print("***DateTime: D[" + Date + "] T[" + Time + "] M[" + Millis + "][" + Rest + "]")
        if (None != self.CBOnDateTimeDetected):
            self.CBOnDateTimeDetected(Date, Time, Millis)
        return Rest

    def AnalyseLocalTime(self, text):  
        #     01234567890123
        # is "&hh:mm:ss.mmm>[rest]"
        Hours = text[1:3]
        Minutes = text[4:6]
        Seconds = text[7:9]
        Millis = text[10:13]
        Rest = text[14:]
        #debug print("...LocalTime: LT[" + Hours + ":" + Minutes + ":" + Seconds + "." + Millis + "]")
        #debugif (0 < len(Rest)): print("Rest[" + Rest + "]")
        if (None != self.CBOnLocalTimeDetected):
            self.CBOnLocalTimeDetected(Hours, Minutes, Seconds, Millis)
        return Rest
    #  
    #---------------------------------------------------------------------------      
    #  Analyse - Event
    #---------------------------------------------------------------------------      
    def AnalyseEvent(self, line):
        Tokens = line.split()
        if (1 < len(Tokens)):
            if ('!' == Tokens[0]):
                if (None != self.CBOnEventDetected):
                    self.CBOnEventDetected(Tokens)
    #  
    #---------------------------------------------------------------------------      
    #  Analyse - Response
    #---------------------------------------------------------------------------      
    def AnalyseResponse(self, line):
        Tokens = line.split()
        if (1 < len(Tokens)):
            if ('&' == Tokens[0]):
                if (None != self.CBOnResponseDetected):
                    self.CBOnResponseDetected(Tokens)
    #  
    #---------------------------------------------------------------------------      
    #  Analyse - Comment
    #---------------------------------------------------------------------------      
    def AnalyseComment(self, line):
        if (2 <= len(line)):
            if ('#' == line[0]):
                if (None != self.CBOnCommentDetected):
                    self.CBOnCommentDetected(line[1:])
    #  
    #---------------------------------------------------------------------------      
    #  Analyse - Error
    #---------------------------------------------------------------------------      
    def AnalyseError(self, line):
        if (':' == line[0]):
            Line = line[2:]
            if (None != self.CBOnErrorDetected):
                self.CBOnErrorDetected(Line)
    #  
    #---------------------------------------------------------------------------      
    #  Analyse - Warning
    #---------------------------------------------------------------------------      
    def AnalyseWarning(self, line):
        if ('.' == line[0]):
            Line = line[2:]
            if (None != self.CBOnWarningDetected):
                self.CBOnWarningDetected(Line)
    #
    ############################################################################
    #---------------------------------------------------------------------------      
    #  Analyse - All
    #---------------------------------------------------------------------------      
    def AnalyseLine(self, line):
        # <datetime> with NTP
        # <localtime> without NTP
        # <%><text>
        # <!><event> ## <state><mainstate><substate>
        # <&><response> <command><value>
        # <#><comment>
        # <|><debug>
        # <:><error>
        # <.><warning>
        #debug print("DeviceResponse:AnalyseLine:[" + line + "]")
        if self.DetectDateTime(line):
            line = self.AnalyseDateTime(line)
            #debug print("REST[" + line + "]")
        else:
            if self.DetectLocalTime(line):
                line = self.AnalyseLocalTime(line)
                #debug print("REST[" + line + "]")
        if (1 < len(line)):       
            CS = line[0]
            # if (TERMINAL_TEXT == CS):
            #     #!!! print("Text[" + CS + "][" + line + "]")
            #     return
            if (TERMINAL_EVENT == CS):
                self.AnalyseEvent(line)
                return
            if (TERMINAL_RESPONSE == CS):
                self.AnalyseResponse(line)
                return
            if (TERMINAL_COMMENT == CS):
                self.AnalyseComment(line)
                return
            # if (TERMINAL_DEBUG == CS):
            #     #!!! print("Debug[" + CS + "][" + line + "]")
            #     return
            # if (TERMINAL_ERROR == CS):
            #     #!!! print("!!!!!!!!!!!!Error[" + CS + "][" + line + "]")
            #     self.AnalyseError(line)
            #     return
            # if (TERMINAL_WARNING == CS):
            #     #!!! print("!!!!!!!!!!!!Warning[" + CS + "][" + line + "]")
            #     return




