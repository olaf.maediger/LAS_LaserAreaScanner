#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameControllerHelp
# ------------------------------------------------------------------
#   Version: 02V03
#   Date   : 200617
#   Time   : 0820
#   Author : OMDevelop
#
import tkinter as tk
from tkinter import font
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION    = "FrameControllerHelp"
#NAME_PERIOD_MS      = "Period[ms]"
#
#INIT_PERIOD_MS      = "1000"

#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameControllerHelp(tk.Frame):
    """CFrameControllerHelp: ..."""

#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        """ : ..."""
        tk.Frame.__init__(self, window, width = 880, height = 136, \
                          padx = 6, pady = 6, background = "#ffeeee")
        self.grid_propagate(0)
        #---------------------------------------------------------------
        self.btnGetHelp = tk.Button(self, width = 24, \
                                    text = "Get Help", \
                                    command = self.OnGetHelp)
        self.btnGetHelp.grid(row = 0, column = 0, \
                             sticky = tk.W, padx = 2, pady = 2)
        #
        self.btnGetProgramHeader = tk.Button(self, width = 24, \
                                             text = "Get Program Header", \
                                             command = self.OnGetProgramHeader)
        self.btnGetProgramHeader.grid(row = 1, column = 0, \
                                      sticky = tk.W, padx = 2, pady = 2)
        #
        self.btnGetSoftwareVersion = tk.Button(self, width = 24, \
                                               text = "Get Software Version", \
                                               command = self.OnGetSoftwareVersion)
        self.btnGetSoftwareVersion.grid(row = 2, column = 0, \
                                        sticky = tk.W, padx = 2, pady = 2)
        #
        self.btnGetHardwareVersion = tk.Button(self, width = 24, \
                                               text = "Get Hardware Version", \
                                               command = self.OnGetHardwareVersion)
        self.btnGetHardwareVersion.grid(row = 3, column = 0, \
                                        sticky = tk.W, padx = 2, pady = 2)
        #
        self.lblDateTime = tk.Label(self, width = 28, anchor = 'w', \
                                    text = "DateTime: hh:mm:ss.mmm")
        self.lblDateTime.grid(row = 0, column = 1, sticky = 'w', padx = 2)
        #
        self.lblEvent = tk.Label(self, width = 28, anchor = 'w', text = "Event: ---")
        self.lblEvent.grid(row = 1, column = 1, sticky = 'W', padx = 2)
        #
        self.lblResponse = tk.Label(self, width = 39, anchor = 'w', \
                                    text = "Response: ---")
        self.lblResponse.grid(row = 0, column = 2, sticky = 'w', columnspan = 4, padx = 2)
        #
        self.lblError = tk.Label(self, width = 39, anchor = 'w', \
                                 text = "Warning/Error: ---")
        self.lblError.grid(row = 1, column = 2, sticky = 'w', columnspan = 4, padx = 2)
        #
        self.scbInfo = tk.Scrollbar(self, orient="vertical")
        self.scbInfo.grid(row = 2, column = 5,  sticky = 'wns', rowspan = 2)
        #
        self.FontInfo = font.Font(family = 'courier new', weight= 'normal', size = 8)
        self.lbxLines = tk.Listbox(self, width = 66, height = 4, font = self.FontInfo)
        self.lbxLines.grid(row = 2, column = 1, rowspan = 2, columnspan = 2, padx = 2)
        self.lbxLines.config(yscrollcommand = self.scbInfo.set)
        self.scbInfo.config(command = self.lbxLines.yview)
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def SetCBOnGetHelp(self, callback):
        """: ..."""
        self.CBOnGetHelp = callback
    #
    def SetCBOnGetProgramHeader(self, callback):
        """: ..."""
        self.CBOnGetProgramHeader = callback
    #
    def SetCBOnGetSoftwareVersion(self, callback):
        """: ..."""
        self.CBOnGetSoftwareVersion = callback
    #
    def SetCBOnGetHardwareVersion(self, callback):
        """: ..."""
        self.CBOnGetHardwareVersion = callback
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    def OnGetHelp(self):
        """: ..."""
        self.lbxLines.delete(0, tk.END)
        if (None != self.CBOnGetHelp):
            self.CBOnGetHelp()
    #
    def OnGetProgramHeader(self):
        """: ..."""
        self.lbxLines.delete(0, tk.END)
        if (None != self.CBOnGetProgramHeader):
            self.CBOnGetProgramHeader()
    #
    def OnGetSoftwareVersion(self):
        """: ..."""
        self.lbxLines.delete(0, tk.END)
        if (None != self.CBOnGetSoftwareVersion):
            self.CBOnGetSoftwareVersion()
    #
    def OnGetHardwareVersion(self):
        """: ..."""
        self.lbxLines.delete(0, tk.END)
        if (None != self.CBOnGetHardwareVersion):
            self.CBOnGetHardwareVersion()
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        """CFrameUartControllerHelp.ReadInitdata: ..."""
        #
    #
    def WriteInitdata(self, writeinitdata):
        """CFrameUartControllerHelp.WriteInitdata: ..."""
        writeinitdata.WriteSection(INITDATA_SECTION)
    #
    #---------------------------------------------------------------------
    # Management
    #---------------------------------------------------------------------
    def AppendComment(self, comment):
        self.lbxLines.insert(tk.END, comment)
    #
    def ClearLines(self):
        self.lbxLines.delete(0, tk.END)
    #
    #---------------------------------------------------------------------
    # Management
    #---------------------------------------------------------------------
    def RefreshDateTime(self, date, time, millis):
        """: ..."""
        self.lblDateTime.config(text = "D[" + date[4:6] + "." + date[2:4] + "." + \
                                date[0:2] + "] T[" + time[0:2] + ":" + time[2:4] + \
                                ":" + time[4:6] + "." + millis + "]")
    def RefreshLocalTime(self, hours, minutes, seconds, millis):
        self.lblDateTime.config(text = "LT[ " + hours + " : " + minutes + " : " + \
                                       seconds + " . " + millis + " ]")
    #
    def RefreshEvent(self, tokens):
        Event = "Event: "
        if 1 < len(tokens):
            Event += tokens[1]
        if 2 < len(tokens):
            Event += " [" + tokens[2] + "]"
        if 3 < len(tokens):
            Event += " <- [" + tokens[3] + "]"
        #debug print(Event)
        self.lblEvent.config(text = Event)
    #
    def RefreshResponse(self, tokens):
        Response = "Response:  "
        # debug print(tokens)
        for Token in tokens[1:]:
            Response += " " + Token
        self.lblResponse.config(text = Response)
    #
    def RefreshWarning(self, swarning):
        """: ..."""
        self.lblError.config(text = swarning)
    #
    def RefreshError(self, serror):
        """: ..."""
        self.lblError.config(text = serror)
    #
