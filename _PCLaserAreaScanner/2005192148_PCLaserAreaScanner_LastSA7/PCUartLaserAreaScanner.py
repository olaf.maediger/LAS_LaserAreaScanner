#
# ------------------------------------------------------------------
#   LAS - PCUartLaserAreaScanner
# ------------------------------------------------------------------
#   Version: 01V04
#   Date   : 200519
#   Time   : 1819
#   Author : OMDevelop
#
import WindowMainUart as WM
#
TITLE_APPLICATION                  = "LAS - (Uart)LaserAreaScanner"
#
###################################################################
# Main
###################################################################
print("*** " + TITLE_APPLICATION + ": begin")
#
WindowMainUart = WM.CWindowMainUart()
WindowMainUart.Execute()
#
print("*** " + TITLE_APPLICATION + ": end")
#



