#
# ------------------------------------------------------------------
#   LAS - PCMqttLaserAreaScanner
# ------------------------------------------------------------------
#   Version: 01V12
#   Date   : 200308
#   Time   : 0920
#   Author : OMDevelop
#
"""PCMqttRemoteWirelessSwitch..."""
#
import WindowMainMqtt as WM
#
TITLE_APPLICATION                  = "LTC - (Mqtt)LehmannTrainController"
#
###################################################################
# Main
###################################################################
print("*** " + TITLE_APPLICATION + ": begin")
#
WindowMainMqtt = WM.CWindowMainMqtt()
WindowMainMqtt.Execute()
#
print("*** " + TITLE_APPLICATION + ": end")
#
