#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameMqttOpenClose
# ------------------------------------------------------------------
#   Version: 02V04
#   Date   : 200625
#   Time   : 0820
#   Author : OMDevelop
#
import tkinter as tk
from tkinter import ttk
import Initdata as ID
#import MqttClient
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION    = "FrameMqttOpenClose"
#
NAME_IPADDRESSX     = "IPAddressX"
NAME_IPADDRESSH     = "IPAddressH"
NAME_IPADDRESSM     = "IPAddressM"
NAME_IPADDRESSL     = "IPAddressL"
NAME_IPPORT         = "IPPort"
NAME_MQTTISOPEN     = "MqttIsOpen"
#
NAME_TOPICCOMMAND   = "TopicCommand"
NAME_TOPICTEXT      = "TopicText"
NAME_TOPICEVENT     = "TopicEvent"
NAME_TOPICRESPONSE  = "TopicResponse"
NAME_TOPICCOMMENT   = "TopicComment"
NAME_TOPICDEBUG     = "TopicDebug"
NAME_TOPICERROR     = "TopicError"
NAME_TOPICWARNING   = "TopicWarning"
#
#
INIT_IPADDRESSX     = "192"
INIT_IPADDRESSH     = "168"
INIT_IPADDRESSM     = "178"
INIT_IPADDRESSL     = "76"
INIT_IPPORT         = "1883"
INIT_MQTTISOPEN     = "False"
#
INIT_TOPICCOMMAND   = "LTC/Command"
INIT_TOPICTEXT      = "LTC/Text"
INIT_TOPICEVENT     = "LTC/Event"
INIT_TOPICRESPONSE  = "LTC/Response"
INIT_TOPICCOMMENT   = "LTC/Comment"
INIT_TOPICDEBUG     = "LTC/Debug"
INIT_TOPICERROR     = "LTC/Error"
INIT_TOPICWARNING   = "LTC/Warning"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
class CFrameMqttOpenClose(tk.Frame):
    """CFrameMqttOpenClose: ..."""

#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        """CFrameMqttOpenClose.__init__: ..."""
        tk.Frame.__init__(self, window, width = 880, height = 520, \
                          padx = 6, pady = 6)#, background = "#77ee77")
        self.grid_propagate(0)
        #
        #-------------------------------------------------------------------
        # IPAddress
        #-------------------------------------------------------------------
        self.lblHeaderIPAddress = tk.Label(self, text = "IPAddress Broker")
        self.lblHeaderIPAddress.grid(row = 0, column = 0, sticky = tk.W, padx = 6, pady = 6)
        #
        self.spbIPAddressX = tk.Spinbox(self, width = 5, justify = "center")
        self.spbIPAddressX.insert(0, INIT_IPADDRESSX)
        self.spbIPAddressX.grid(row = 0, column = 1, sticky = tk.W, padx = 6, pady = 6)
        #
        self.lblHeaderPointH = tk.Label(self, text = ".", width = 0, justify = "center")
        self.lblHeaderPointH.grid(row = 0, column = 2, sticky = tk.W, padx = 6, pady = 6)
        #
        self.spbIPAddressH = tk.Spinbox(self, width = 5, justify = "center")
        self.spbIPAddressH.insert(0, INIT_IPADDRESSH)
        self.spbIPAddressH.grid(row = 0, column = 3, sticky = tk.W, padx = 6, pady = 6)
        #
        self.lblHeaderPointM = tk.Label(self, text = ".", width = 0, justify = "center")
        self.lblHeaderPointM.grid(row = 0, column = 4, sticky = tk.W, padx = 6, pady = 6)
        #
        self.spbIPAddressM = tk.Spinbox(self, width = 5, justify = "center")
        self.spbIPAddressM.insert(0, INIT_IPADDRESSM)
        self.spbIPAddressM.grid(row = 0, column = 5, sticky = tk.W, padx = 6, pady = 6)
        #
        self.lblHeaderPointL = tk.Label(self, text = ".", width = 0, justify = "center")
        self.lblHeaderPointL.grid(row = 0, column = 6, sticky = tk.W, padx = 6, pady = 6)
        #
        self.spbIPAddressL = tk.Spinbox(self, width = 5, justify = "center")
        self.spbIPAddressL.insert(0, INIT_IPADDRESSL)
        self.spbIPAddressL.grid(row = 0, column = 7, sticky = tk.W, padx = 6, pady = 6)
        #
        #-------------------------------------------------------------------
        # Port
        #-------------------------------------------------------------------
        self.lblHeaderIPAddress = tk.Label(self, text = "IPPort Broker")
        self.lblHeaderIPAddress.grid(row = 1, column = 0, sticky = tk.W, padx = 6, pady = 6)
        #
        self.spbIPPort = tk.Spinbox(self, width = 5, justify = "center")
        self.spbIPPort.insert(0, INIT_IPPORT)
        self.spbIPPort.grid(row = 1, column = 1, sticky = tk.W, padx = 6, pady = 6)
        #
        #-------------------------------------------------------------------
        # Topic - Command
        #-------------------------------------------------------------------
        self.lblHeaderTopicCommand = tk.Label(self, text = "Topic Command")
        self.lblHeaderTopicCommand.grid(row = 2, column = 0, sticky = tk.W, padx = 6, pady = 6)
        #
        self.entTopicCommand = tk.Entry(self, width = 39, justify = "center")
        self.entTopicCommand.insert(0, INIT_TOPICCOMMAND)
        self.entTopicCommand.grid(row = 2, column = 1, columnspan = 9, sticky = tk.W, padx = 6, pady = 6)
        #
        #-------------------------------------------------------------------
        # Topic - Text
        #-------------------------------------------------------------------
        self.lblHeaderTopicText = tk.Label(self, text = "Topic Text")
        self.lblHeaderTopicText.grid(row = 3, column = 0, sticky = tk.W, padx = 6, pady = 6)
        #
        self.entTopicText = tk.Entry(self, width = 39, justify = "center")
        self.entTopicText.insert(0, INIT_TOPICTEXT)
        self.entTopicText.grid(row = 3, column = 1, columnspan = 9, sticky = tk.W, padx = 6, pady = 6)
        #
        #-------------------------------------------------------------------
        # Topic - Event
        #-------------------------------------------------------------------
        self.lblHeaderTopicEvent = tk.Label(self, text = "Topic Event")
        self.lblHeaderTopicEvent.grid(row = 4, column = 0, sticky = tk.W, padx = 6, pady = 6)
        #
        self.entTopicEvent = tk.Entry(self, width = 39, justify = "center")
        self.entTopicEvent.insert(0, INIT_TOPICEVENT)
        self.entTopicEvent.grid(row = 4, column = 1, columnspan = 9, sticky = tk.W, padx = 6, pady = 6)
        #
        #-------------------------------------------------------------------
        # Topic - Response
        #-------------------------------------------------------------------
        self.lblHeaderTopicResponse = tk.Label(self, text = "Topic Response")
        self.lblHeaderTopicResponse.grid(row = 5, column = 0, sticky = tk.W, padx = 6, pady = 6)
        #
        self.entTopicResponse = tk.Entry(self, width = 39, justify = "center")
        self.entTopicResponse.insert(0, INIT_TOPICRESPONSE)
        self.entTopicResponse.grid(row = 5, column = 1, columnspan = 9, sticky = tk.W, padx = 6, pady = 6)
        #
        #-------------------------------------------------------------------
        # Topic - Comment
        #-------------------------------------------------------------------
        self.lblHeaderTopicComment = tk.Label(self, text = "Topic Comment")
        self.lblHeaderTopicComment.grid(row = 6, column = 0, sticky = tk.W, padx = 6, pady = 6)
        #
        self.entTopicComment = tk.Entry(self, width = 39, justify = "center")
        self.entTopicComment.insert(0, INIT_TOPICCOMMENT)
        self.entTopicComment.grid(row = 6, column = 1, columnspan = 9, sticky = tk.W, padx = 6, pady = 6)
        #
        #-------------------------------------------------------------------
        # Topic - Debug
        #-------------------------------------------------------------------
        self.lblHeaderTopicDebug = tk.Label(self, text = "Topic Debug")
        self.lblHeaderTopicDebug.grid(row = 7, column = 0, sticky = tk.W, padx = 6, pady = 6)
        #
        self.entTopicDebug = tk.Entry(self, width = 39, justify = "center")
        self.entTopicDebug.insert(0, INIT_TOPICDEBUG)
        self.entTopicDebug.grid(row = 7, column = 1, columnspan = 9, sticky = tk.W, padx = 6, pady = 6)
        #
        #-------------------------------------------------------------------
        # Topic - Error
        #-------------------------------------------------------------------
        self.lblHeaderTopicError = tk.Label(self, text = "Topic Error")
        self.lblHeaderTopicError.grid(row = 8, column = 0, sticky = tk.W, padx = 6, pady = 6)
        #
        self.entTopicError = tk.Entry(self, width = 39, justify = "center")
        self.entTopicError.insert(0, INIT_TOPICERROR)
        self.entTopicError.grid(row = 8, column = 1, columnspan = 9, sticky = tk.W, padx = 6, pady = 6)
        #
        #-------------------------------------------------------------------
        # Topic - Warning
        #-------------------------------------------------------------------
        self.lblHeaderTopicWarning = tk.Label(self, text = "Topic Warning")
        self.lblHeaderTopicWarning.grid(row = 9, column = 0, sticky = tk.W, padx = 6, pady = 6)
        #
        self.entTopicWarning = tk.Entry(self, width = 39, justify = "center")
        self.entTopicWarning.insert(0, INIT_TOPICWARNING)
        self.entTopicWarning.grid(row = 9, column = 1, columnspan = 9, sticky = tk.W, padx = 6, pady = 6)
        #
        #-------------------------------------------------------------------
        # Topic - Button
        #-------------------------------------------------------------------
        self.btnOpenClose = tk.Button(self, text = "Open", width = 32, \
                                      command = self.OnMqttOpenClose)
        self.btnOpenClose.grid(row = 10, column = 0, columnspan = 13, padx = 4, pady = 4)
        #

    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def IsOpen(self):
        return ("Close" == self.btnOpenClose["text"])
    #
    def GetIPAddressBroker(self):
        IPAX = self.spbIPAddressX.get()
        IPAH = self.spbIPAddressH.get()
        IPAM = self.spbIPAddressM.get()
        IPAL = self.spbIPAddressL.get()
        return IPAX + "." + IPAH + "." + IPAM + "." + IPAL

    def GetPortBroker(self):
        IPP = self.spbIPPort.get()
        return IPP
    #
    def GetTopicCommand(self):
        TC = self.entTopicCommand.get()
        return TC
    #
    def SetCBOnMqttOpen(self, cbonmqttopen):
        self.CBOnMqttOpen = cbonmqttopen
    #
    def SetCBOnMqttClose(self, cbonmqttclose):
        self.CBOnMqttClose = cbonmqttclose
    #
    def SetCBOnMqttSubscribe(self, cbonmqttsubscribe):
        self.CBOnMqttSubscribe = cbonmqttsubscribe
    #
    #-----------------------------------------------------------------------------------
    # Helper
    #-----------------------------------------------------------------------------------
    def OpenMqtt(self):
        """OpenMqtt: ..."""
        Result = self.CBOnMqttOpen(self.GetIPAddressBroker(), int(self.GetPortBroker()))
        if (True == Result):
            self.btnOpenClose["text"] = "Close"
            self.CBOnMqttSubscribe(self.entTopicText.get())
            self.CBOnMqttSubscribe(self.entTopicEvent.get())
            self.CBOnMqttSubscribe(self.entTopicResponse.get())
            self.CBOnMqttSubscribe(self.entTopicComment.get())
            self.CBOnMqttSubscribe(self.entTopicDebug.get())
            self.CBOnMqttSubscribe(self.entTopicError.get())
            self.CBOnMqttSubscribe(self.entTopicWarning.get())
        # disable
            self.spbIPAddressX["state"] = tk.DISABLED
            self.spbIPAddressH["state"] = tk.DISABLED
            self.spbIPAddressM["state"] = tk.DISABLED
            self.spbIPAddressL["state"] = tk.DISABLED
            self.spbIPPort["state"] = tk.DISABLED
            self.entTopicCommand["state"] = tk.DISABLED
            self.entTopicText["state"] = tk.DISABLED
            self.entTopicEvent["state"] = tk.DISABLED
            self.entTopicResponse["state"] = tk.DISABLED
            self.entTopicComment["state"] = tk.DISABLED
            self.entTopicDebug["state"] = tk.DISABLED
            self.entTopicError["state"] = tk.DISABLED
            self.entTopicWarning["state"] = tk.DISABLED
            #
    #
    def CloseMqtt(self):
        """CloseMqtt: ..."""
        Result = self.CBOnMqttClose()
        if (True == Result):
            self.btnOpenClose["text"] = "Open"
        # enable
            self.spbIPAddressX["state"] = tk.NORMAL
            self.spbIPAddressH["state"] = tk.NORMAL
            self.spbIPAddressM["state"] = tk.NORMAL
            self.spbIPAddressL["state"] = tk.NORMAL
            self.spbIPPort["state"] = tk.NORMAL
            self.entTopicCommand["state"] = tk.NORMAL
            self.entTopicText["state"] = tk.NORMAL
            self.entTopicEvent["state"] = tk.NORMAL
            self.entTopicResponse["state"] = tk.NORMAL
            self.entTopicComment["state"] = tk.NORMAL
            self.entTopicDebug["state"] = tk.NORMAL
            self.entTopicError["state"] = tk.NORMAL
            self.entTopicWarning["state"] = tk.NORMAL
    #
    #-----------------------------------------------------------------------------------
    # Callback
    #-----------------------------------------------------------------------------------
    def OnMqttOpenClose(self):
        if ("Open" == self.btnOpenClose["text"]):
            self.OpenMqtt()
        else:
            self.CloseMqtt()
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        IPAX = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_IPADDRESSX, INIT_IPADDRESSX)
        IPAH = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_IPADDRESSH, INIT_IPADDRESSH)
        IPAM = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_IPADDRESSM, INIT_IPADDRESSM)
        IPAL = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_IPADDRESSL, INIT_IPADDRESSL)
        IPP = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_IPPORT, INIT_IPPORT)
        MIO = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_MQTTISOPEN, INIT_MQTTISOPEN)
        #
        TCD = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_TOPICCOMMAND, INIT_TOPICCOMMAND)
        TTT = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_TOPICTEXT, INIT_TOPICTEXT)
        TET = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_TOPICEVENT, INIT_TOPICEVENT)
        TRE = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_TOPICRESPONSE, INIT_TOPICRESPONSE)
        TCT = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_TOPICCOMMENT, INIT_TOPICCOMMENT)
        TDG = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_TOPICDEBUG, INIT_TOPICDEBUG)
        TER = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_TOPICERROR, INIT_TOPICERROR)
        TWG = readinitdata.ReadValueInit(INITDATA_SECTION, NAME_TOPICWARNING, INIT_TOPICWARNING)
        #
        self.spbIPAddressX["values"] = IPAX
        self.spbIPAddressH["values"] = IPAH
        self.spbIPAddressM["values"] = IPAM
        self.spbIPAddressL["values"] = IPAL
        self.spbIPPort["values"] = IPP
        if (ID.StringBoolean(MIO)):
            #print("T")
            self.OpenMqtt()
        else:
            #print("F")
            self.CloseMqtt()
    #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_IPADDRESSX, str(self.spbIPAddressX.get()))
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_IPADDRESSH, str(self.spbIPAddressH.get()))
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_IPADDRESSM, str(self.spbIPAddressM.get()))
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_IPADDRESSL, str(self.spbIPAddressL.get()))
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_IPPORT, str(self.spbIPPort.get()))
        #
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_TOPICCOMMAND, self.entTopicCommand.get())
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_TOPICTEXT, self.entTopicText.get())
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_TOPICEVENT, self.entTopicEvent.get())
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_TOPICRESPONSE, self.entTopicResponse.get())
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_TOPICCOMMENT, self.entTopicComment.get())
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_TOPICDEBUG, self.entTopicDebug.get())
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_TOPICERROR, self.entTopicError.get())
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_TOPICWARNING, self.entTopicWarning.get())
        #
        MIO = self.IsOpen()
        writeinitdata.WriteValue(INITDATA_SECTION, NAME_MQTTISOPEN, ID.BooleanString(MIO))
    #
