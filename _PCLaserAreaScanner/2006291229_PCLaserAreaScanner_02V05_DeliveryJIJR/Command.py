#
# ------------------------------------------------------------------
#   LAS - PCUartLaserAreaScanner - Command
# ------------------------------------------------------------------
#   Version: 02V04
#   Date   : 200625
#   Time   : 0820
#   Author : OMDevelop
#
#
class CCommand:
    def __init__(self, text):
        self.FText = text
    #
    def GetText(self):
        return self.FText
#
#
#
class CCommandlist(list):
    def __init__(self):
        self.clear()

