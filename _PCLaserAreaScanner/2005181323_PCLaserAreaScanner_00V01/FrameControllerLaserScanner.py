#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameLaserScanner
# ------------------------------------------------------------------
#   Version: 01V01
#   Date   : 200518
#   Time   : 1143
#   Author : OMDevelop
#
import tkinter as tk
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
WIDTH_BUTTON                    = 24
#
INITDATA_SECTION                = "FrameControllerLaserScanner"
#
NAME_PULSEPERIOD_MS             = "PulsePeriod[ms]"
#
INIT_PULSEPERIOD_MS             = "1000"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameControllerLaserScanner(tk.Frame):
#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 880, height = 269, \
                          padx = 6, pady = 6, background = "#ffffee")
        self.grid_propagate(0)
        #--------------------------------------------------------------
        # Switch
        #--------------------------------------------------------------
        self.btnSetPulsePeriod = tk.Button(self, width = WIDTH_BUTTON, \
                                           text = "Set PulsePeriod", \
                                           command = self.OnSetPulsePeriod)
        self.btnSetPulsePeriod.grid(row = 0, column = 0, \
                                    sticky = tk.W, padx = 2, pady = 2)
        #
        self.btnGetPulsePeriod = tk.Button(self, width = WIDTH_BUTTON, \
                                           text = "Get PulsePeriod", \
                                           command = self.OnGetPulsePeriod)
        self.btnGetPulsePeriod.grid(row = 0, column = 1, \
                                    sticky = tk.W, padx = 2, pady = 2)
        #
        self.lblPulsePeriod = tk.Label(self, text = "PulsePeriod[ms]", width = 14)
        self.lblPulsePeriod.grid(row = 0, column = 2, sticky = "w", padx = 10)
        #
        self.spbPulsePeriod = tk.Spinbox(self, from_ = 1000, to = 100000, increment = 1000, \
                                         width = 8, justify = "center")
        self.spbPulsePeriod.grid(row = 0, column = 3, sticky = "w", padx = 6)
        self.spbPulsePeriod.delete(0, tk.END)
        self.spbPulsePeriod.insert(0, INIT_PULSEPERIOD_MS)
        #
        # self.lblColorLed = tk.Label(self, text = "---", width = 4)
        # self.lblColorLed.grid(row = 0, column = 4, padx = 3)
        # #
        # self.RemoteSwitchOn = False
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def SetCBOnSetPulsePeriod(self, callback):
        self.CBOnSetPulsePeriod = callback
    #
    def SetCBOnGetPulsePeriod(self, callback):
        self.CBOnGetPulsePeriod = callback
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    def OnSetPulsePeriod(self):
        if (self.CBOnSetPulsePeriod):
            #self.RemoteSwitchOn = True
            PP = self.spbPulsePeriod.get()
            self.CBOnSetPulsePeriod(PP)
    #
    def OnGetPulsePeriod(self):
        if (self.CBOnGetPulsePeriod):
            #self.RemoteSwitchOn = False
            #ST = self.spbPulsePeriod.get()
            self.CBOnGetPulsePeriod()
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        ST = readinitdata.ReadValueInit(INITDATA_SECTION, \
                                        NAME_PULSEPERIOD_MS, \
                                        INIT_PULSEPERIOD_MS)
        self.spbPulsePeriod.delete(0, tk.END)
        self.spbPulsePeriod.insert(0, ST)
        #
        # self.OnSetDeltaPositionLow()
        #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        writeinitdata.WriteValue(INITDATA_SECTION, \
                                 NAME_PULSEPERIOD_MS, \
                                 self.spbPulsePeriod.get())
    #
    #---------------------------------------------------------------
    # Management
    #--------------------------------------------------------------
    def RefreshEvent(self, tokens):
        if (3 < len(tokens)):
            print("!!! test : LaserScanner Idle / Busy / ...")
    #         if (self.RemoteSwitchOn):
    #             if ("1" == tokens[3]):
    #                 self.lblColorLed.config(bg = "magenta")
    #                 self.lblColorLed.config(text = "ON")
    #             else:
    #                 self.lblColorLed.config(bg = "grey")
    #                 self.lblColorLed.config(text = " ")
    #         else:
    #             if ("1" == tokens[3]):
    #                 self.lblColorLed.config(bg = "royalblue")
    #                 self.lblColorLed.config(text = "OFF")
    #             else:
    #                 self.lblColorLed.config(bg = "grey")
    #                 self.lblColorLed.config(text = " ")
        #     self.lbl.insert(0, ST)
        #     tokens[1]
        #     Event += " [" + tokens[2] + "]"
        #     Event += " <- [" + tokens[3] + "]"
        #debug 
        #print("++++++ REFRESH EVENT SWITCH[" + Event + "]")
        #self.lblEvent.config(text = Event)
    #
    # def RefreshDateTime(self, date, time, millis):
    #     self.lblDateTime.config(text = "D[" + date[4:6] + "." + date[2:4] + "." + \
    #                             date[0:2] + "] T[" + time[0:2] + ":" + time[2:4] + \
    #                             ":" + time[4:6] + "." + millis + "]")
    # def RefreshLocalTime(self, hours, minutes, seconds, millis):
    #     self.lblDateTime.config(text = "LT[ " + hours + " : " + minutes + " : " + \
    #                                    seconds + " . " + millis + " ]")
    #
    #
    def RefreshResponse(self, tokens):
        if (1 < len(tokens)):
            Command = tokens[1]
            Data = []
            for TI in range(2, len(tokens)):
                Data.append(tokens[TI])               
            self.RefreshCommandData(tokens[1], tokens[2])
    #        
    def RefreshCommandData(self, command, data):
        # SetRemoteSwitchOn
        # if ("SRO" == command) or ("SDL" == command):
        #     self.spbDeltaPositionLow.delete(0, tk.END)
        #     self.spbDeltaPositionLow.insert(0, int(data))
        #     return True
        #        
        # PulsePeriod
        if ("GPP" == command) or ("SPP" == command):
            self.spbPulsePeriod.delete(0, tk.END)
            self.spbPulsePeriod.insert(0, float(data))            
            return True
        return False
        # if ("GDH" == command) or ("SDH" == command):
        #     self.spbDeltaPositionHigh.delete(0, tk.END)
        #     self.spbDeltaPositionHigh.insert(0, int(data))
        #     return True
        # # PWM
        # if ("GWL" == command) or ("SWL" == command):
        #     self.spbPWMLow.delete(0, tk.END)
        #     self.spbPWMLow.insert(0, int(data))
        #     return True
        # if ("GWH" == command) or ("SWH" == command):
        #     self.spbPWMHigh.delete(0, tk.END)
        #     self.spbPWMHigh.insert(0, int(data))
        #     return True
        # # PositionActual
        # if ("GPA" == command) or ("SPA" == command):
        #     self.spbPositionActual.delete(0, tk.END)
        #     self.spbPositionActual.insert(0, int(data))
        #     return True
        # # PositionTarget
        # if ("GPT" == command):
        #     self.lblPositionTarget["text"] = str(data)
        #     return True
        # # VelocityTarget
        # if ("GVT" == command) or ("SVT" == command):
        #     self.spbVelocityTarget.delete(0, tk.END)
        #     self.spbVelocityTarget.insert(0, int(data))
        #     return True
        # # VelocityActual
        # if ("GVA" == command):
        #     self.lblVelocityActual["text"] = str(data)
        #     return True
        # # MoveVelocityPositive
        # if ("MVP" == command):
        #     self.spbVelocityPositive.delete(0, tk.END)
        #     self.spbVelocityPositive.insert(0, int(data))
        #     return True
        # # MoveVelocityNegative
        # if ("MVN" == command):
        #     self.spbVelocityNegative.delete(0, tk.END)
        #     self.spbVelocityNegative.insert(0, int(data))
        #     return True
        # # MovePositionAbsolute
        # if ("MPA" == command):
        #     self.spbPositionAbsolute.delete(0, tk.END)
        #     self.spbPositionAbsolute.insert(0, int(data))
        #     return True
        # # MovePositionRelative
        # if ("MPR" == command):
        #     self.spbPositionRelative.delete(0, tk.END)
        #     self.spbPositionRelative.insert(0, int(data))
        #     return True
        #        
        
        
        
        