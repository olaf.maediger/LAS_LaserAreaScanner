#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - Initdata
# ------------------------------------------------------------------
#   Version: 02V04
#   Date   : 200625
#   Time   : 0820
#   Author : OMDevelop
#
import configparser as CP
#
class CWriteInitdata():
    #
    def __init__(self):
        self.Parser = CP.ConfigParser()
        self.Parser.optionxform = str
    #
    def Open(self, filename):
        self.Filename = filename
    #
    def Close(self):
        self.File = open(self.Filename, 'w')
        self.Parser.write(self.File)
        self.File.close()
        self.Parser = None
    #
    def WriteSection(self, section):
        self.Parser.add_section(section)
        # debug print("WriteSection[" + section + "]")

    def WriteValue(self, section, name, value):
        self.Parser.set(section, name, value)
        # debug print("WriteValue[" + section + "] Name[" + name + "] Value[" + value + "]")
#
#
#
class CReadInitdata():
    #
    def __init__(self):
        self.Parser = CP.ConfigParser()
        self.Parser.optionxform = str
    #
    def Open(self, filename):
        self.Parser.read(filename)
    #
    def Close(self):
        self.Parser = None
    #
    def ReadValue(self, section, name):
        Value = "???"
        try:
            Value = self.Parser.get(section, name)
# debug     print("ReadValue[" + section + "] Name[" + name + "] Value[" + Value + "]")
            return Value
        except:
# debug     print("ReadValue[" + section + "] Name[" + name + "] Value[" + Value + "]")
            return "?"
    #
    def ReadValueInit(self, section, name, init):
        Value = init
        try:
            Value = self.Parser.get(section, name)
            # debug print("ReadValue[" + section + "] Name[" + name + "] Value[" + Value + "][" + init + "]")
            return Value
        except:
            # debug print("ReadValue[" + section + "] Name[" + name + "] Value[" + Value + "][" + init + "]")
            return Value
#
#
#
def BooleanString(bvalue):
    if (True == bvalue):
        return "True"
    return "False"
#
def StringBoolean(text):
    Text = text.upper()
    if ("T" in Text):
        return True
    if ("1" in Text):
        return True
    return False
#
#
#
