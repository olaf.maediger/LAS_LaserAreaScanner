#
# ------------------------------------------------------------------
#   LAS - PCUartLaserAreaScanner
# ------------------------------------------------------------------
#   Version: 02V04
#   Date   : 200625
#   Time   : 0820
#   Author : OMDevelop
#
import WindowMainUart as WM
#
TITLE_APPLICATION                  = "LAS - (Uart)LaserAreaScanner"
#
###################################################################
# Main
###################################################################
print("*** " + TITLE_APPLICATION + ": begin")
#
WindowMainUart = WM.CWindowMainUart()
WindowMainUart.Execute()
#
print("*** " + TITLE_APPLICATION + ": end")
#



