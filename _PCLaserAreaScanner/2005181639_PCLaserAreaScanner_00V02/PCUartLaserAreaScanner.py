#
# ------------------------------------------------------------------
#   LAS - PCUartLaserAreaScanner
# ------------------------------------------------------------------
#   Version: 01V02
#   Date   : 200506
#   Time   : 1555
#   Author : OMDevelop
#
import WindowMainUart as WM
#
TITLE_APPLICATION                  = "LAS - (Uart)LaserAreaScanner"
#
###################################################################
# Main
###################################################################
print("*** " + TITLE_APPLICATION + ": begin")
#
WindowMainUart = WM.CWindowMainUart()
WindowMainUart.Execute()
#
print("*** " + TITLE_APPLICATION + ": end")
#



