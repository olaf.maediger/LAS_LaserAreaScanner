#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - WindowMainUart
# ------------------------------------------------------------------
#   Version: 02V00
#   Date   : 200303
#   Time   : 0941
#   Author : OMDevelop
#
"""project: ..."""
#
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import time
#
import UartClient
import Initdata as ID
from Command import *
from Task import *
import DeviceResponse as DR
import FrameUartOpenClose as FUOC
import FrameUartTerminal as FUT
import FrameController as FC
import FrameControllerLaserScanner as FCLS
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
NAME_INITFILE = "PCUartLaserAreaScanner.ini"
INITDATA_SECTION = "WindowMain"
NAME_X = "X"
NAME_Y = "Y"
NAME_W = "W"
NAME_H = "H"
NAME_SELECTTABINDEX = "SelectTabIndex"
#
INIT_X = "10"
INIT_Y = "10"
INIT_W = "690"
INIT_H = "546"
INIT_SELECTTABINDEX = "0"
#
TITLE_WINDOW = "LAS - PCUartLaserAreaScanner"
#
PAD_X = 1
PAD_Y = 1
FONTSIZE_LISTBOX = 11
COLOR_BACK = "#F8FEEE"
#
INFO_ABOUT = TITLE_WINDOW + "\r\n\r\n" + \
              "Version: 01V03\r\n" + \
              "Date: 200506\r\n" + \
              "Time: 1749\r\n" + \
              "Author: OMDevelop\r\n" + \
              "\r\n" + \
              "Abstract:\r\n" + \
              "- Gui for Esp32LaserAreaScanner\r\n" + \
              "- Controlling over UsbSerial\r\n" + \
              "- not included: Mqtt"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
STATECOMMAND_IDLE       = "Idle"
STATECOMMAND_WAIT       = "Wait"
STATECOMMAND_RESPONSE   = "Response"
STATECOMMAND_BUSY       = "Busy"
#
#------------------------------------------------------------------
#   Global Type - WindowMain
#------------------------------------------------------------------
class CWindowMainUart(tk.Tk):
    #---------------------------------------------------------------------
    # CWindowMain - Constructor
    #---------------------------------------------------------------------
    def __init__(self):
        super().__init__()
        super().protocol("WM_DELETE_WINDOW", self.OnDeleteWindow)
        self.title(TITLE_WINDOW)
        #self.resizable(False, False)
        #
        # Menu
        self.Menu = tk.Menu(self)
        self.config(menu = self.Menu)
        # Menu - System
        self.MenuSystem = tk.Menu(self.Menu)
        self.Menu.add_cascade(label = "System", menu = self.MenuSystem)
        self.MenuSystem.add_command(label = "Exit Application", \
                                    command = self.OnExitApplication)
        # Menu - Device
        self.MenuDevice = tk.Menu(self.Menu)
        self.Menu.add_cascade(label = "Device", menu = self.MenuDevice)
        self.MenuDevice.add_command(label = "Open/Close Uart") #, \
                                    #command = self.OnOpenCloseUart)
        self.MenuDevice.add_command(label = "Save File")#, command = self.OnSaveFile)
        self.MenuDevice.add_separator()
        self.MenuDevice.add_command(label = "Choose Color") #,\
        #                             command = self.OnChooseColor)
        # Menu - Help
        self.MenuHelp = tk.Menu(self.Menu)
        self.Menu.add_cascade(label = "Help", menu = self.MenuHelp)
        self.MenuHelp.add_command(label = "Show About", command = self.OnShowAbout)
        #
        #-------------------------------------------------------------------------------
        # Notebook - All
        #-------------------------------------------------------------------------------
        self.nbkMain = ttk.Notebook(self)
        self.nbkMain.pack(fill = tk.BOTH)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Uart
        #-------------------------------------------------------------------------------
        self.UartClient = UartClient.CUartClient()
        self.UartClient.SetCBOnLineReceived(self.CBOnUartLineReceived); 
        #
        #-------------------------------------------------------------------------------
        # Notebook - Uart - Configuration
        #-------------------------------------------------------------------------------
        self.frmUartConfiguration = tk.Frame(self.nbkMain)
        self.nbkMain.add(self.frmUartConfiguration, text = "Uart-Configuration")
        self.FrameUartOpenClose = FUOC.CFrameUartOpenClose(self.frmUartConfiguration)
        self.FrameUartOpenClose.pack(fill = tk.BOTH);
        self.FrameUartOpenClose.SetCBOnSerialOpen(self.CBOnUartOpen)
        self.FrameUartOpenClose.SetCBOnSerialClose(self.CBOnUartClose)               
        self.FrameUartOpenClose.SetCBOnRefreshSerialPorts(self.CBUartOnRefreshSerialPorts)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Uart - Terminal
        #-------------------------------------------------------------------------------
        self.frmUartTerminal = tk.Frame(self.nbkMain)
        self.nbkMain.add(self.frmUartTerminal, text = "Uart-Terminal")
        self.FrameUartTerminal = FUT.CFrameUartTerminal(self.frmUartTerminal)
        self.FrameUartTerminal.pack(fill = tk.BOTH)
        self.FrameUartTerminal.SetCBOnSerialTransmitLine(self.CBOnUartTerminalTransmitLine)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Uart - Special
        #-------------------------------------------------------------------------------
        self.frmController = tk.Frame(self.nbkMain)
        self.nbkMain.add(self.frmController, text = "Controller")
        self.FrameController = FC.CFrameController(self.frmController)
        self.FrameController.pack(fill = tk.BOTH)
        #
        #-------------------------------------------------------------------------------
        # Notebook - DeviceResponse
        #-------------------------------------------------------------------------------
        self.DeviceResponse = DR.CDeviceResponse()
        self.DeviceResponse.SetCBOnDateTimeDetected(self.CBDeviceOnDateTimeDetected)
        self.DeviceResponse.SetCBOnLocalTimeDetected(self.CBDeviceResponseOnLocalTimeDetected)
        self.DeviceResponse.SetCBOnTextDetected(self.CBDeviceOnTextDetected)
        self.DeviceResponse.SetCBOnEventDetected(self.CBDeviceOnEventDetected)
        self.DeviceResponse.SetCBOnResponseDetected(self.CBDeviceOnResponseDetected)
        self.DeviceResponse.SetCBOnCommentDetected(self.CBDeviceOnCommentDetected)
        self.DeviceResponse.SetCBOnDebugDetected(self.CBDeviceOnDebugDetected)
        self.DeviceResponse.SetCBOnWarningDetected(self.CBDeviceOnWarningDetected)
        self.DeviceResponse.SetCBOnErrorDetected(self.CBDeviceOnErrorDetected)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - Help
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerHelp.\
            SetCBOnGetHelp(self.CBOnGetHelp)
        self.FrameController.FrameControllerHelp.\
            SetCBOnGetProgramHeader(self.CBOnGetProgramHeader)
        self.FrameController.FrameControllerHelp.\
            SetCBOnGetSoftwareVersion(self.CBOnGetSoftwareVersion)
        self.FrameController.FrameControllerHelp.\
            SetCBOnGetHardwareVersion(self.CBOnGetHardwareVersion)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - System
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerSystem.\
            SetCBOnAbortProcessExecution(self.CBOnAbortProcessExecution)
        self.FrameController.FrameControllerSystem.\
            SetCBOnResetCommand(self.CBOnResetCommand)
        self.FrameController.FrameControllerSystem.\
            SetCBOnResetSystem(self.CBOnResetSystem)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - Led
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerLed.\
            SetCBOnGetStateLedSystem(self.CBOnGetStateLedSystem)
        self.FrameController.FrameControllerLed.\
            SetCBOnSwitchLedSystemOn(self.CBOnSwitchLedSystemOn)
        self.FrameController.FrameControllerLed.\
            SetCBOnSwitchLedSystemOff(self.CBOnSwitchLedSystemOff)
        self.FrameController.FrameControllerLed.\
            SetCBOnBlinkLedSystem(self.CBOnBlinkLedSystem)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - LaserScanner
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerLaserScanner.\
            SetCBOnSetPulsePeriod(self.CBOnSetPulsePeriod)
        self.FrameController.FrameControllerLaserScanner.\
            SetCBOnGetPulsePeriod(self.CBOnGetPulsePeriod)
        self.FrameController.FrameControllerLaserScanner.\
            SetCBOnPulseLaserAbort(self.CBOnPulseLaserAbort)
        self.FrameController.FrameControllerLaserScanner.\
            SetCBOnPulseLaserCount(self.CBOnPulseLaserCount)
        self.FrameController.FrameControllerLaserScanner.\
            SetCBOnMovePositionX(self.CBOnMovePositionX)
        self.FrameController.FrameControllerLaserScanner.\
            SetCBOnMovePositionY(self.CBOnMovePositionY)
        self.FrameController.FrameControllerLaserScanner.\
            SetCBOnMovePositionPulse(self.CBOnMovePositionPulse)
        #-------------------------------------------------------------------------------
        #
        #-------------------------------------------------------------------------------
        # Init...
        #-------------------------------------------------------------------------------
        self.CBUartOnRefreshSerialPorts();
        #
        self.Commandlist = CCommandlist()
        self.Task = CTask()
        self.Task.SetCBOnExecute(self.TaskOnExecute)
        #
        self.StateCommand = STATECOMMAND_IDLE
        self.SetStateCommand(STATECOMMAND_IDLE)
        #
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #debug self.Commandlist.append(CCommand("H"))
        #
        self.ReadInitdata(NAME_INITFILE)
    #---------------------------------------------------------------------
    # CWindowMain - Callback
    #---------------------------------------------------------------------
    def OnDeleteWindow(self):
        #
        #self.UartClient.WriteLine("AAM")
        self.UartClient.Close()
        #
        self.WriteInitdata(NAME_INITFILE)
        #
        super().destroy()
    #
    #---------------------------------------------------------------------
    # CApplication - Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, filename):
        print("*** ReadInitdata: start")
        RID = ID.CReadInitdata()
        RID.Open(filename)
        X = RID.ReadValueInit(INITDATA_SECTION, NAME_X, INIT_X)
        Y = RID.ReadValueInit(INITDATA_SECTION, NAME_Y, INIT_Y)
        W = RID.ReadValueInit(INITDATA_SECTION, NAME_W, INIT_W)
        H = RID.ReadValueInit(INITDATA_SECTION, NAME_H, INIT_H)
        self.geometry('%dx%d+%d+%d' % (int(W), int(H), int(X), int(Y)))
        #
        self.FrameUartOpenClose.ReadInitdata(RID)
        self.FrameUartTerminal.ReadInitdata(RID)
        self.FrameController.ReadInitdata(RID)
        #
        STI = RID.ReadValueInit(INITDATA_SECTION, NAME_SELECTTABINDEX, \
                                INIT_SELECTTABINDEX)
        self.nbkMain.select(int(STI))
        #
        RID.Close()
        #
        self.Task.Start()
        
        print("*** ReadInitdata: end")
    #
    def WriteInitdata(self, filename):
        print("*** WriteInitdata: begin")
        X = self.winfo_x()
        Y = self.winfo_y()
        W = self.winfo_width()
        H = self.winfo_height()
        WID = ID.CWriteInitdata()
        WID.Open(filename)
        WID.WriteSection(INITDATA_SECTION)
        WID.WriteValue(INITDATA_SECTION, NAME_X, str(X))
        WID.WriteValue(INITDATA_SECTION, NAME_Y, str(Y))
        WID.WriteValue(INITDATA_SECTION, NAME_W, str(W))
        WID.WriteValue(INITDATA_SECTION, NAME_H, str(H))
        #
        self.FrameUartOpenClose.WriteInitdata(WID)
        self.FrameUartTerminal.WriteInitdata(WID)
        self.FrameController.WriteInitdata(WID)
        #
        STI = self.nbkMain.index('current')
        WID.WriteValue(INITDATA_SECTION, NAME_SELECTTABINDEX, str(STI))
        #
        WID.Close()
        print("*** WriteInitdata: end")

    #
    #---------------------------------------------------------------------
    # CWindowMain - Property
    #---------------------------------------------------------------------
    
    #
    #---------------------------------------------------------------------
    # CWindowMain - Event
    #---------------------------------------------------------------------
    #
    def OnShowAbout(self):
        messagebox.showinfo("About", INFO_ABOUT)
    #
    def OnExitApplication(self):
        self.destroy()
    #
    #---------------------------------------------------------------------
    # CWindowMain - Handler
    #---------------------------------------------------------------------
    def Execute(self):
        self.mainloop()
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - Uart
    #------------------------------------------------------------------
    def CBOnUartOpen(self, comport, baudrate, parity, databits, stopbits, handshake):
        return self.UartClient.Open(comport, baudrate, parity, databits, stopbits, handshake)
    #
    def CBOnUartClose(self, comport):
        return self.UartClient.Close(comport)
    #
    def CBUartOnRefreshSerialPorts(self):
        ListSerialPorts = self.UartClient.GetListSerialPorts()
        self.FrameUartOpenClose.SetListSerialPorts(ListSerialPorts)
    #
    def CBOnUartWriteLine(self, line):
        self.UartClient.WriteLine(line)
    #
    def CBOnUartLineReceived(self, line): 
        #debug print("CWindowMain.CBMqttOnLineReceived:<" + line + ">")
        print("Rxd[Uart][" + line + "]")
        self.FrameUartTerminal.AppendLineReceived(line)
        self.DeviceResponse.AnalyseLine(line)
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - Terminal
    #------------------------------------------------------------------
    #
    def CBOnUartTerminalTransmitLine(self, line):
        self.UartClient.WriteLine(line)
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - DeviceResponse
    #------------------------------------------------------------------
    def CBDeviceOnDateTimeDetected(self, date, time, millis):
        print("OnDateTimeDetected[" + date + "][" + time + "][" + millis + "]")
        #self.FrameController.FrameControllerHelp.RefreshDateTime(date, time, millis)
    #
    def CBDeviceResponseOnLocalTimeDetected(self, hours, minutes, seconds, millis):
        #print("OnLocalTimeDetected[" + hours + "][" + minutes + "][" + seconds + "][" + millis + "]")
        self.FrameController.FrameControllerHelp.RefreshLocalTime(hours, minutes, seconds, millis)
    #
    def CBDeviceOnTextDetected(self, tokens, state):
        print("CBDeviceResponseOnTextDetected - missing!")
        #print(tokens)
    #    
    # CS: Idle -> Wait -> Busy -> Response -> Idle
    #
    def AnalyseEventStateCommand(self, tokens):
        if (2 < len(tokens)): 
            # <!> <CS> <SA> <SP>
            if (DR.TERMINAL_EVENT == tokens[0]):
                if ("CS" == tokens[1]):
                    if (STATECOMMAND_WAIT == self.GetStateCommand()):
                        if (STATECOMMAND_BUSY == tokens[2]):
                            self.SetStateCommand(STATECOMMAND_BUSY)
                            return                       
                        if (STATECOMMAND_IDLE == tokens[2]):
                            self.SetStateCommand(STATECOMMAND_IDLE)
                            return
                    if (STATECOMMAND_RESPONSE == self.GetStateCommand()):
                        if (STATECOMMAND_BUSY == tokens[2]):
                            self.SetStateCommand(STATECOMMAND_BUSY)
                            return                       
                        if (STATECOMMAND_IDLE == tokens[2]):
                            self.SetStateCommand(STATECOMMAND_IDLE)
                            return
                    if (STATECOMMAND_BUSY == self.GetStateCommand()):                       
                        if (STATECOMMAND_IDLE == tokens[2]):
                            self.SetStateCommand(STATECOMMAND_IDLE)
                            return
                        if (STATECOMMAND_IDLE == tokens[2]):
                            self.SetStateCommand(STATECOMMAND_IDLE)
                            return
                    self.FrameController.FrameControllerHelp.RefreshEvent(tokens)
                    self.FrameController.FrameControllerLed.RefreshEvent(tokens)
                    self.FrameController.FrameControllerLaserScanner.RefreshEvent(tokens)
    #    
    def AnalyseResponseStateCommand(self, tokens):
        if (1 < len(tokens)): 
            # <&> <CR> {<PI>}
            if (DR.TERMINAL_RESPONSE == tokens[0]):
                if (STATECOMMAND_WAIT == self.GetStateCommand()):                    
                    self.SetStateCommand(STATECOMMAND_RESPONSE)
                else:    
                    if (STATECOMMAND_BUSY == self.GetStateCommand()):                    
                        self.SetStateCommand(STATECOMMAND_RESPONSE)
                #
                self.FrameController.FrameControllerHelp.RefreshResponse(tokens)
                self.FrameController.FrameControllerLaserScanner.RefreshResponse(tokens)
    #        
    def CBDeviceOnEventDetected(self, tokens):        
        # Software-Handshake (uP -> PC) when Command finnished:
        self.AnalyseEventStateCommand(tokens)
    #
    def CBDeviceOnResponseDetected(self, tokens):        
        # Software-Handshake (uP -> PC) when Command responsed:
        self.AnalyseResponseStateCommand(tokens)
    #
    def CBDeviceOnCommentDetected(self, comment):
        #print("CBDeviceResponseOnCommentDetected")
        if (not("###" in comment)):
            self.FrameController.FrameControllerHelp.AppendComment(comment)
    #
    def CBDeviceOnDebugDetected(self, tokens, state):        
        print("CBDeviceResponseOnDebugDetected - missing!")
        #print(tokens)
    #
    def CBDeviceOnErrorDetected(self, error):
        #print("CBUartOnErrorDetected")
        self.FrameController.FrameControllerHelp.ClearLines()
        self.FrameController.FrameControllerHelp.AppendComment(error)
        self.FrameController.FrameControllerHelp.RefreshError(error)
    #        
    def CBDeviceOnWarningDetected(self, warning):
        #print("CBUartOnWarningDetected[")
        self.FrameController.FrameControllerHelp.ClearLines()
        self.FrameController.FrameControllerHelp.AppendComment(warning)
        self.FrameController.FrameControllerHelp.RefreshWarning(warning)
    #    
    #------------------------------------------------------------------
    #   CWindowMain - Callback - Help
    #------------------------------------------------------------------
    def CBOnGetHelp(self):
        self.Commandlist.append(CCommand("H"))
    #
    def CBOnGetProgramHeader(self):
        self.Commandlist.append(CCommand("GPH"))
    #    
    def CBOnGetSoftwareVersion(self):
        self.Commandlist.append(CCommand("GSV"))
    #    
    def CBOnGetHardwareVersion(self):
        self.Commandlist.append(CCommand("GHV"))
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - System
    #------------------------------------------------------------------
    def CBOnAbortProcessExecution(self):
        self.Commandlist.append(CCommand("A"))
    #
    def CBOnResetCommand(self):
        # NO COMMAND!!!
        if (0 < len(self.Commandlist)):
            self.Commandlist.pop(0)
            # vv self.Commandlist.clear()
        self.SetStateCommand('scIdle')
    #
    def CBOnResetSystem(self):
        self.Commandlist.append(CCommand("RSS"))
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - Led
    #------------------------------------------------------------------
    def CBOnGetStateLedSystem(self):
        self.Commandlist.append(CCommand("GLS"))
    #
    def CBOnSwitchLedSystemOn(self):
        self.Commandlist.append(CCommand("LSH"))
    #
    def CBOnSwitchLedSystemOff(self):
        self.Commandlist.append(CCommand("LSL"))
    #
    def CBOnBlinkLedSystem(self, count, period, width):
        self.Commandlist.append(CCommand("BLS " + count + " " + period + " " + width))
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - Scanner
    #------------------------------------------------------------------
    def CBOnSetPulsePeriod(self, period):
        self.Commandlist.append(CCommand("SPP " + period))
    #
    def CBOnGetPulsePeriod(self):
        self.Commandlist.append(CCommand("GPP"))
    #
    def CBOnPulseLaserAbort(self):
        self.Commandlist.append(CCommand("PLA"))
    #
    def CBOnPulseLaserCount(self, period, count):
        self.Commandlist.append(CCommand("PLC " + period + " " + count))
    #
    def CBOnMovePositionX(self, position):
        self.Commandlist.append(CCommand("MPX " + " " + position))
    #
    def CBOnMovePositionY(self, position):
        self.Commandlist.append(CCommand("MPY " + " " + position))
    #
    def CBOnMovePositionPulse(self, px, py, pp, pc, dm):
        self.Commandlist.append(CCommand("MPP " + " " + px + " " + py + " " + \
                                         pp + " " + pc + " " + dm))
    #
    #------------------------------------------------------------------
    #   Callback - Task - Commandlist
    #------------------------------------------------------------------
    def GetStateCommand(self):
        return self.StateCommand
    #
    def SetStateCommand(self, value):
        # debug !!! print(">>>StateCommand[" + self.StateCommand + "]>>>[" + value + "]")
        self.StateCommand = value
    #
    # CS: Idle -> Wait -> Busy -> Response -> Idle
    def TaskOnExecute(self):
        if ((STATECOMMAND_IDLE == self.GetStateCommand()) and (0 < len(self.Commandlist))):
            self.SetStateCommand(STATECOMMAND_WAIT)
            #time.sleep(0.050)
            Command = self.Commandlist[0]
            # Command over Terminal to Uart:
            self.FrameUartTerminal.AppendTransmitLine(Command.GetText())            
            self.Commandlist.pop(0)
        else:      
            time.sleep(0.001)
#
