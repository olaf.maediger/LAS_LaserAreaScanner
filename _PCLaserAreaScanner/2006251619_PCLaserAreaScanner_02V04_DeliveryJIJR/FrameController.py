#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameUartController
# ------------------------------------------------------------------
#   Version: 02V04
#   Date   : 200625
#   Time   : 0820
#   Author : OMDevelop
#
import tkinter as tk
#
import Initdata as ID
import FrameControllerSystem as FCS
import FrameControllerHelp as FCH
import FrameControllerLed as FCL
import FrameControllerLaserScanner as FCLS
import FrameLaserMatrix as FLM
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
#
INITDATA_SECTION    = "FrameController"
#NAME_PERIOD_MS      = "Period[ms]"
#
#INIT_PERIOD_MS      = "1000"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameController(tk.Frame):

#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        """CFrameController.__init__: ..."""
        tk.Frame.__init__(self, window, width = 680, height = 520, \
                          padx = 0, pady = 0, background = "#777777")
        self.grid_propagate(0)
        #---------------------------------------------------------------
        self.FrameControllerHelp = FCH.CFrameControllerHelp(self)
        self.FrameControllerHelp.grid(row = 0, column = 0, sticky = "w")
        #---------------------------------------------------------------
        self.FrameControllerSystem = FCS.CFrameControllerSystem(self)
        self.FrameControllerSystem.grid(row = 1, column = 0, sticky = "w")
        #---------------------------------------------------------------
        self.FrameControllerLed = FCL.CFrameControllerLed(self)
        self.FrameControllerLed.grid(row = 2, column = 0, sticky = "w")
        #---------------------------------------------------------------
        self.FrameControllerLaserScanner = FCLS.CFrameControllerLaserScanner(self)
        self.FrameControllerLaserScanner.grid(row = 3, column = 0, sticky = "w")
        #---------------------------------------------------------------
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    #
    #-----------------------------------------------------------------------------------
    # Helper
    #-----------------------------------------------------------------------------------
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        self.FrameControllerHelp.ReadInitdata(readinitdata)
        self.FrameControllerSystem.ReadInitdata(readinitdata)
        self.FrameControllerLed.ReadInitdata(readinitdata)
        self.FrameControllerLaserScanner.ReadInitdata(readinitdata)
    #
    def WriteInitdata(self, writeinitdata):
        self.FrameControllerHelp.WriteInitdata(writeinitdata)
        self.FrameControllerSystem.WriteInitdata(writeinitdata)
        self.FrameControllerLed.WriteInitdata(writeinitdata)
        self.FrameControllerLaserScanner.WriteInitdata(writeinitdata)
    #