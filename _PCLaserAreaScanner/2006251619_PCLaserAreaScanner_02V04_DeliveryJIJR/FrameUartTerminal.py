#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - FrameUartTerminal
# ------------------------------------------------------------------
#   Version: 02V04
#   Date   : 200625
#   Time   : 0820
#   Author : OMDevelop
#
import tkinter as tk
from tkinter import ttk
from tkinter import font
import UartClient
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
INITDATA_SECTION            = "FrameUartTerminal"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameUartTerminal(tk.Frame):
    #---------------------------------------------------------------------
    # Constructor
    #---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 880, height = 520, \
                          padx = 2, pady = 4)#, background = "#77ee77")
        self.grid_propagate(0)
        #
        self.scrollbar = tk.Scrollbar(self, orient="vertical")
        self.scrollbar.grid(row = 0, column = 2, sticky = 'NSW')
        #
        self.small_font = font.Font(family = 'courier new', weight= 'normal', size = 10)
        self.lbxLines = tk.Listbox(self, width = 82, height = 28, font = self.small_font)
        self.lbxLines.grid(row = 0, column = 0, columnspan = 2, padx = 2)
        self.lbxLines.config(yscrollcommand = self.scrollbar.set)
        self.scrollbar.config(command = self.lbxLines.yview)
        #
        self.btnWriteLine = tk.Button(self, text = "WriteLine", width = 12, \
                                      command = self.OnTransmitLine)
        self.btnWriteLine.grid(row = 1, column = 0, padx = 2, pady = 4, sticky = "W")
        #
        self.entTxData = tk.Entry(self, width = 60)
        self.entTxData.grid(row = 1, column = 0,  columnspan = 1, padx = 3, sticky = "E")
        self.entTxData.insert(0, "H")
        self.entTxData.bind("<Return>", \
                            (lambda event: self.AppendTransmitLine(self.entTxData.get())))
        #
        self.btnClearLines = tk.Button(self, text = "ClearLines", width = 12, \
                                       command = self.OnClearLines)
        self.btnClearLines.grid(row = 1, column = 1, padx = 2, pady = 4, sticky = "E")
    #
    #-----------------------------------------------------------------------------------
    # Property
    #-----------------------------------------------------------------------------------
    def SetCBOnSerialTransmitLine(self, cbonserialtransmitline):
        self.CBOnSerialTransmitLine = cbonserialtransmitline
    #
    #-----------------------------------------------------------------------------------
    # Event
    #-----------------------------------------------------------------------------------
    def OnTransmitLine(self):
        TxLine = self.entTxData.get()
        self.lbxLines.insert('end', 'Txd>' + TxLine)
        self.lbxLines.itemconfig('end', bg = "#ddffdd", fg = 'black')
        #self.lbxLines.selection_set("end")
        self.lbxLines.see("end")
        if (None != self.CBOnSerialTransmitLine):
            self.CBOnSerialTransmitLine(TxLine)
    #
    def OnClearLines(self):
        self.lbxLines.delete(0, tk.END)
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        return
    #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
    #
    #-----------------------------------------------------------------------------------
    # Management
    #-----------------------------------------------------------------------------------
    def AppendTransmitLine(self, line):
        self.entTxData.delete(0, tk.END)
        self.entTxData.insert(0, line)
        self.OnTransmitLine()
    #
    def AppendLineReceived(self, line):
        RxLine = "RxD>" + line
        self.lbxLines.insert('end', RxLine)
        self.lbxLines.itemconfig('end', bg = '#eeeeff', fg = 'black')
        #self.lbxLines.selection_set("end")
        self.lbxLines.see("end")
    #
