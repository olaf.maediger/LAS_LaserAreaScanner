#
# ------------------------------------------------------------------
#   LAS - PCLaserAreaScanner - Steplist
# ------------------------------------------------------------------
#   Version: 02V04
#   Date   : 200625
#   Time   : 0820
#   Author : OMDevelop
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
#

#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CSteplist:
    #
    def __init__(self):
        self.Steplist = []
        self.HeaderRow = ['StepIndex[1]', 'PositionX', 'PositionY', 'PulseCount[1]', \
                          'PulsePeriod[ms]', 'DelayMotion[ms]']
        self.StepIndex = -1
#
#------------------------------------------------------------------
#   Property
#------------------------------------------------------------------
    def Clear(self):
        self.Steplist = []
    #
    def GetStepCount(self):
        return len(self.Steplist)
    #
    def GetStep(self):
        if (self.StepIndex <= len(self.Steplist) - 1):
            Step = self.Steplist[self.StepIndex]
            self.StepIndex += 1
            return Step
        return None
    #
    def GetStepIndexCommandParameter(self):
        if (self.StepIndex <= len(self.Steplist) - 1):
            self.StepIndex += 1
            return self.StepIndex - 1, self.Steplist[self.StepIndex - 1]
        return -1, None
    #
    def GetIndexStep(self, stepindex):
        return self.Steplist[stepindex]
    #
    def AddStep(self, step):
        self.Steplist.append(step)
    #
    def ResetStep(self):
        self.StepIndex = 0
    #
    def NextStepIndex(self):
        self.StepIndex += 1
        if (self.StepIndex <= len(self.Steplist) - 1):
            return self.StepIndex
        return -1
    #
    # def NextStep(self):
    #     Step = self.Steplist[self.StepIndex]
    #     if (self.StepIndex <= len(self.Steplist) - 1):
    #         return self.StepIndex
    #     return -1
    #
#------------------------------------------------------------------
#   Handler
#------------------------------------------------------------------
    def InitSimple(self, stepcount = 5):
        self.Steplist = []
        for SI in range(0, stepcount):
            Step = [1800 + SI, 1800 + SI, 123, 10, 1.234]
            self.AddStep(Step)
    #
    def Debug(self, headerbegin, headerend):
        SC = len(self.Steplist)
        print(headerbegin + "[{0}]:".format(SC))
        print(self.HeaderRow)
        for SI in range(0, SC):
            print(self.Steplist[SI])
        print(headerend)

#
#------------------------------------------------------------------
#
#------------------------------------------------------------------
#
