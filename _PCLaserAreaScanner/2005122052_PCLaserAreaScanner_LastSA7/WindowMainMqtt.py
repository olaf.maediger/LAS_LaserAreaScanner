#
# ------------------------------------------------------------------
#   LTM - PCRemoteWirelessSwitch - WindowMainMqtt
# ------------------------------------------------------------------
#   Version: 02V00
#   Date   : 200303
#   Time   : 0941
#   Author : OMDevelop
#
"""project: ..."""
#
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import time
#
import MqttClient
import Initdata as ID
from Command import *
from Task import *
import DeviceResponse as DR
import FrameMqttOpenClose as FMOC
import FrameMqttTerminal as FMT
import FrameController as FC
import FrameControllerMotor as FCM
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
NAME_INITFILE = "PCMqttRemoteWirelessSwitch.ini"
INITDATA_SECTION = "WindowMain"
NAME_X = "X"
NAME_Y = "Y"
NAME_W = "W"
NAME_H = "H"
NAME_SELECTTABINDEX = "SelectTabIndex"
#
INIT_X = "10"
INIT_Y = "10"
INIT_W = "640"
INIT_H = "480"
INIT_SELECTTABINDEX = "0"
#
TITLE_WINDOW = "LTC - (Mqtt)LehmannTrainController"
#
PAD_X = 1
PAD_Y = 1
FONTSIZE_LISTBOX = 11
COLOR_BACK = "#F8FEEE"
#
INFO_ABOUT = "LTM - PCMqttRemoteWirelessSwitch\r\n" + \
             "\r\n" + \
             "Version: 01V12\r\n" + \
             "Date: 200308\r\n" + \
             "Time: 2225\r\n" + \
             "Author: OMDevelop\r\n" + \
             "\r\n" + \
             "Abstract:\r\n" + \
             "- Gui for Esp32LehmannTrainController\r\n" + \
             "- Controlling over Mqtt\r\n" + \
             "- not included: UsbSerial"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
EStateCommand = ['scIdle', 'scBusy']
#
#------------------------------------------------------------------
#   Global Type - WindowMain
#------------------------------------------------------------------
class CWindowMainMqtt(tk.Tk):
    #---------------------------------------------------------------------
    # CWindowMain - Constructor
    #---------------------------------------------------------------------
    def __init__(self):
        super().__init__()
        super().protocol("WM_DELETE_WINDOW", self.OnDeleteWindow)
        self.title(TITLE_WINDOW)
        #self.resizable(False, False)
        #
        # Menu
        self.Menu = tk.Menu(self)
        self.config(menu = self.Menu)
        # Menu - System
        self.MenuSystem = tk.Menu(self.Menu)
        self.Menu.add_cascade(label = "System", menu = self.MenuSystem)
        self.MenuSystem.add_command(label = "Exit Application", \
                                    command = self.OnExitApplication)
        # Menu - Device
        self.MenuDevice = tk.Menu(self.Menu)
        self.Menu.add_cascade(label = "Device", menu = self.MenuDevice)
        self.MenuDevice.add_command(label = "Open/Close Uart") #, \
                                    #command = self.OnOpenCloseUart)
        self.MenuDevice.add_command(label = "Save File")#, command = self.OnSaveFile)
        self.MenuDevice.add_separator()
        self.MenuDevice.add_command(label = "Choose Color") #,\
        #                             command = self.OnChooseColor)
        # Menu - Help
        self.MenuHelp = tk.Menu(self.Menu)
        self.Menu.add_cascade(label = "Help", menu = self.MenuHelp)
        self.MenuHelp.add_command(label = "Show About", command = self.OnShowAbout)
        #
        #-------------------------------------------------------------------------------
        # Notebook - All
        #-------------------------------------------------------------------------------
        self.nbkMain = ttk.Notebook(self)
        self.nbkMain.pack(fill = tk.BOTH)
        #
        #-------------------------------------------------------------------------------
        # MqttClient
        #-------------------------------------------------------------------------------
        self.MqttClient = MqttClient.CMqttClient()
        self.MqttClient.SetCBOnLineReceived(self.CBOnMqttLineReceived)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Mqtt - Configuration
        #-------------------------------------------------------------------------------
        self.frmMqttConfiguration = tk.Frame(self.nbkMain)
        self.nbkMain.add(self.frmMqttConfiguration, text = "Mqtt-Configuration")
        self.FrameMqttOpenClose = FMOC.CFrameMqttOpenClose(self.frmMqttConfiguration)
        self.FrameMqttOpenClose.pack(fill = tk.BOTH);
        self.FrameMqttOpenClose.SetCBOnMqttOpen(self.CBOnMqttOpen)
        self.FrameMqttOpenClose.SetCBOnMqttClose(self.CBOnMqttClose)        
        self.FrameMqttOpenClose.SetCBOnMqttSubscribe(self.CBOnMqttSubscribe)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Mqtt - Terminal
        #-------------------------------------------------------------------------------
        self.frmMqttTerminal = tk.Frame(self.nbkMain)
        self.nbkMain.add(self.frmMqttTerminal, text = "Mqtt-Terminal")
        self.FrameMqttTerminal = FMT.CFrameMqttTerminal(self.frmMqttTerminal)
        self.FrameMqttTerminal.pack(fill = tk.BOTH)
        self.FrameMqttTerminal.SetCBOnMqttTransmitLine(self.CBOnMqttTerminalTransmitLine)
        #        
        #-------------------------------------------------------------------------------
        # Notebook - Controller
        #-------------------------------------------------------------------------------
        self.frmController = tk.Frame(self.nbkMain)
        self.nbkMain.add(self.frmController, text = "Controller")
        self.FrameController = FC.CFrameController(self.frmController)
        self.FrameController.pack(fill = tk.BOTH)
        #
        #-------------------------------------------------------------------------------
        # Notebook - DeviceResponse
        #-------------------------------------------------------------------------------
        self.DeviceResponse = DR.CDeviceResponse()
        self.DeviceResponse.SetCBOnDateTimeDetected(self.CBDeviceResponseOnDateTimeDetected)
        self.DeviceResponse.SetCBOnLocalTimeDetected(self.CBDeviceResponseOnLocalTimeDetected)
        self.DeviceResponse.SetCBOnTextDetected(self.CBDeviceResponseOnTextDetected)
        self.DeviceResponse.SetCBOnEventDetected(self.CBDeviceResponseOnEventDetected)
        self.DeviceResponse.SetCBOnResponseDetected(self.CBDeviceResponseOnResponseDetected)
        self.DeviceResponse.SetCBOnCommentDetected(self.CBDeviceResponseOnCommentDetected)
        self.DeviceResponse.SetCBOnDebugDetected(self.CBDeviceResponseOnDebugDetected)
        self.DeviceResponse.SetCBOnWarningDetected(self.CBDeviceResponseOnWarningDetected)
        self.DeviceResponse.SetCBOnErrorDetected(self.CBDeviceResponseOnErrorDetected)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - Help
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerHelp.\
            SetCBOnGetHelp(self.CBOnGetHelp)
        self.FrameController.FrameControllerHelp.\
            SetCBOnGetProgramHeader(self.CBOnGetProgramHeader)
        self.FrameController.FrameControllerHelp.\
            SetCBOnGetSoftwareVersion(self.CBOnGetSoftwareVersion)
        self.FrameController.FrameControllerHelp.\
            SetCBOnGetHardwareVersion(self.CBOnGetHardwareVersion)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - System
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerSystem.\
            SetCBOnGetProcessCount(self.CBOnGetProcessCount)
        self.FrameController.FrameControllerSystem.\
            SetCBOnSetProcessCount(self.CBOnSetProcessCount)
        self.FrameController.FrameControllerSystem.\
            SetCBOnGetProcessPeriod(self.CBOnGetProcessPeriod)
        self.FrameController.FrameControllerSystem.\
            SetCBOnSetProcessPeriod(self.CBOnSetProcessPeriod)
        self.FrameController.FrameControllerSystem.\
            SetCBOnAbortProcessExecution(self.CBOnAbortProcessExecution)
        self.FrameController.FrameControllerSystem.\
            SetCBOnResetCommand(self.CBOnResetCommand)
        self.FrameController.FrameControllerSystem.\
            SetCBOnResetSystem(self.CBOnResetSystem)
        self.FrameController.FrameControllerSystem.\
            SetCBOnGetProcessWidth(self.CBOnGetProcessWidth)
        self.FrameController.FrameControllerSystem.\
            SetCBOnSetProcessWidth(self.CBOnSetProcessWidth)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - Led
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerLed.\
            SetCBOnGetStateLedSystem(self.CBOnGetStateLedSystem)
        self.FrameController.FrameControllerLed.\
            SetCBOnSwitchLedSystemOn(self.CBOnSwitchLedSystemOn)
        self.FrameController.FrameControllerLed.\
            SetCBOnSwitchLedSystemOff(self.CBOnSwitchLedSystemOff)
        self.FrameController.FrameControllerLed.\
            SetCBOnBlinkLedSystem(self.CBOnBlinkLedSystem)
        #
        #-------------------------------------------------------------------------------
        # Notebook - Frame - Motor
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnGetDeltaPositionLow(self.CBOnGetDeltaPositionLow)
        self.FrameController.FrameControllerMotor.\
            SetCBOnSetDeltaPositionLow(self.CBOnSetDeltaPositionLow)
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnGetDeltaPositionHigh(self.CBOnGetDeltaPositionHigh)
        self.FrameController.FrameControllerMotor.\
            SetCBOnSetDeltaPositionHigh(self.CBOnSetDeltaPositionHigh)
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnGetPWMLow(self.CBOnGetPWMLow)
        self.FrameController.FrameControllerMotor.\
            SetCBOnSetPWMLow(self.CBOnSetPWMLow)
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnGetPWMHigh(self.CBOnGetPWMHigh)
        self.FrameController.FrameControllerMotor.\
            SetCBOnSetPWMHigh(self.CBOnSetPWMHigh)
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnGetPositionTarget(self.CBOnGetPositionTarget)
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnSetPositionActual(self.CBOnSetPositionActual)
        self.FrameController.FrameControllerMotor.\
            SetCBOnGetPositionActual(self.CBOnGetPositionActual)
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnSetVelocityTarget(self.CBOnSetVelocityTarget)
        self.FrameController.FrameControllerMotor.\
            SetCBOnGetVelocityTarget(self.CBOnGetVelocityTarget)
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnGetVelocityActual(self.CBOnGetVelocityActual)
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnMoveVelocityPositive(self.CBOnMoveVelocityPositive)
        self.FrameController.FrameControllerMotor.\
            SetCBOnMoveVelocityNegative(self.CBOnMoveVelocityNegative)
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnMoveAbsolute(self.CBOnMoveAbsolute)
        self.FrameController.FrameControllerMotor.\
            SetCBOnMoveRelative(self.CBOnMoveRelative)
        #-------------------------------------------------------------------------------
        self.FrameController.FrameControllerMotor.\
            SetCBOnAbortMotion(self.CBOnAbortMotion)
        #
        #-------------------------------------------------------------------------------
        # Init...
        #-------------------------------------------------------------------------------
        # UART !!!! self.CBUartOnRefreshSerialPorts();
        #
        self.Commandlist = CCommandlist()
        self.Task = CTask()
        self.Task.SetCBOnExecute(self.CBOnTaskExecute)
        #
        self.StateCommand = 'scIdle'
        self.SetStateCommand('scIdle')
        #
        #debug self.Commandlist.append(CCommand("H"))
        #
        self.ReadInitdata(NAME_INITFILE)
    #---------------------------------------------------------------------
    # CWindowMain - Callback
    #---------------------------------------------------------------------
    def OnDeleteWindow(self):
        # stop MQTT:
        self.MqttClient.Close();
        #
        self.WriteInitdata(NAME_INITFILE)
        #
        super().destroy()
    #
    #---------------------------------------------------------------------
    # CApplication - Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, filename):
        print("*** ReadInitdata: start")
        RID = ID.CReadInitdata()
        RID.Open(filename)
        X = RID.ReadValueInit(INITDATA_SECTION, NAME_X, INIT_X)
        Y = RID.ReadValueInit(INITDATA_SECTION, NAME_Y, INIT_Y)
        W = RID.ReadValueInit(INITDATA_SECTION, NAME_W, INIT_W)
        H = RID.ReadValueInit(INITDATA_SECTION, NAME_H, INIT_H)
        self.geometry('%dx%d+%d+%d' % (int(W), int(H), int(X), int(Y)))
        #
        self.FrameMqttOpenClose.ReadInitdata(RID)
        self.FrameMqttTerminal.ReadInitdata(RID)
        self.FrameController.ReadInitdata(RID)
        #
        STI = RID.ReadValueInit(INITDATA_SECTION, NAME_SELECTTABINDEX, \
                                INIT_SELECTTABINDEX)
        self.nbkMain.select(int(STI))
        #
        RID.Close()
        #        
        self.Task.Start()
        #
        print("*** ReadInitdata: end")
    #
    def WriteInitdata(self, filename):
        print("*** WriteInitdata: begin")
        X = self.winfo_x()
        Y = self.winfo_y()
        W = self.winfo_width()
        H = self.winfo_height()
        WID = ID.CWriteInitdata()
        WID.Open(filename)
        WID.WriteSection(INITDATA_SECTION)
        WID.WriteValue(INITDATA_SECTION, NAME_X, str(X))
        WID.WriteValue(INITDATA_SECTION, NAME_Y, str(Y))
        WID.WriteValue(INITDATA_SECTION, NAME_W, str(W))
        WID.WriteValue(INITDATA_SECTION, NAME_H, str(H))
        #
        self.FrameMqttOpenClose.WriteInitdata(WID)
        self.FrameMqttTerminal.WriteInitdata(WID)
        self.FrameController.WriteInitdata(WID)
        #
        STI = self.nbkMain.index('current')
        WID.WriteValue(INITDATA_SECTION, NAME_SELECTTABINDEX, str(STI))
        #
        WID.Close()
        #
        print("*** WriteInitdata: end")

    #
    #---------------------------------------------------------------------
    # CWindowMain - Property
    #---------------------------------------------------------------------

    #
    #---------------------------------------------------------------------
    # CWindowMain - Event
    #---------------------------------------------------------------------
    #
    def OnShowAbout(self):
        """CWindowMain.OnMenuAbout ..."""
        messagebox.showinfo("About", INFO_ABOUT)
    #
    def OnExitApplication(self):
        """CWindowMain.OnMenuExit: ..."""
        self.destroy()
    #
    #---------------------------------------------------------------------
    # CWindowMain - Handler
    #---------------------------------------------------------------------
    def Execute(self):
        """CWindowMain.Execute: ..."""
        self.mainloop()
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - Mqtt
    #------------------------------------------------------------------
    def CBOnMqttOpen(self, ipaddressbroker, portbroker):
        self.MqttClient.Open(ipaddressbroker, portbroker, 60)
        return True
    #        
    def CBOnMqttClose(self):
        self.MqttClient.Close()
        return True
    #
    def CBOnMqttSubscribe(self, topic):
        self.MqttClient.SubscribeTopic(topic)
        return True
    #
    def CBOnMqttWriteLine(self, topic, message):
        self.MqttClient.PublishTopic(topic, message)
    #
    def CBOnMqttLineReceived(self, line):
        #debug print("CWindowMain.CBMqttOnLineReceived:<" + line + ">")
        print("Rxd[Mqtt][" + line + "]")
        self.FrameMqttTerminal.AppendLineReceived(line)
        self.DeviceResponse.AnalyseLine(line)    
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - Terminal
    #------------------------------------------------------------------
    def CBOnMqttTerminalTransmitLine(self, line):
        # ONLY here MqttClient.publish!!!
        TopicCommand = self.FrameMqttOpenClose.GetTopicCommand()
        self.MqttClient.PublishTopic(TopicCommand, line)
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - DeviceResponse
    #------------------------------------------------------------------
    def CBDeviceResponseOnDateTimeDetected(self, date, time, millis):
        #print("OnDateTimeDetected[" + date + "][" + time + "][" + millis + "]")
        self.FrameController.FrameControllerHelp.RefreshDateTime(date, time, millis)
    #
    def CBDeviceResponseOnLocalTimeDetected(self, hours, minutes, seconds, millis):
        #print("OnLocalTimeDetected[" + hours + "][" + minutes + "][" + seconds + "][" + millis + "]")
        self.FrameController.FrameControllerHelp.RefreshLocalTime(hours, minutes, seconds, millis)
    #
    def CBDeviceResponseOnTextDetected(self, tokens, state):
        print("CBDeviceResponseOnTextDetected - missing!")
        #print(tokens)
    #
    def CBDeviceResponseOnEventDetected(self, tokens, state):
        #print("CBDeviceResponseOnEventDetected")
        self.FrameController.FrameControllerHelp.RefreshEvent(tokens, state)
    #
    def CBDeviceResponseOnResponseDetected(self, tokens):
        #print("CBDeviceResponseOnResponseDetected")
        self.FrameController.FrameControllerHelp.RefreshResponse(tokens)
        if ('scBusy' == self.GetStateCommand()):
            self.SetStateCommand('scIdle')
    #
    def CBDeviceResponseOnCommentDetected(self, comment):
        #print("CBDeviceResponseOnCommentDetected")
        if (not("###" in comment)):
            self.FrameController.FrameControllerHelp.AppendComment(comment)
    #
    def CBDeviceResponseOnDebugDetected(self, tokens, state):        
        print("CBDeviceResponseOnDebugDetected - missing!")
        #print(tokens)
    #
    def CBDeviceResponseOnErrorDetected(self, error):
        #print("CBDeviceResponseOnErrorDetected")
        self.FrameController.FrameControllerHelp.ClearLines()
        self.FrameController.FrameControllerHelp.AppendComment(error)
        self.FrameController.FrameControllerHelp.RefreshError(error)
    #        
    def CBDeviceResponseOnWarningDetected(self, warning):
        #print("CBDeviceResponseOnWarningDetected")
        self.FrameController.FrameControllerHelp.ClearLines()
        self.FrameController.FrameControllerHelp.AppendComment(warning)
        self.FrameController.FrameControllerHelp.RefreshWarning(warning)
    #    
    #------------------------------------------------------------------
    #   CWindowMain - Callback - Help
    #------------------------------------------------------------------
    def CBOnGetHelp(self):
        self.Commandlist.append(CCommand("H"))
    #
    def CBOnGetProgramHeader(self):
        self.Commandlist.append(CCommand("GPH"))
    #    
    def CBOnGetSoftwareVersion(self):
        self.Commandlist.append(CCommand("GSV"))
    #    
    def CBOnGetHardwareVersion(self):
        self.Commandlist.append(CCommand("GHV"))
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - System
    #------------------------------------------------------------------
    def CBOnGetProcessCount(self):
        self.Commandlist.append(CCommand("GPC"))
    #
    def CBOnSetProcessCount(self, value):
        self.Commandlist.append(CCommand("SPC " + str(value)))
    #
    def CBOnGetProcessPeriod(self):
        self.Commandlist.append(CCommand("GPP"))
    #
    def CBOnSetProcessPeriod(self, value):
        self.Commandlist.append(CCommand("SPP " + str(value)))
    #
    def CBOnAbortProcessExecution(self):
        self.Commandlist.append(CCommand("A"))
    #
    def CBOnResetCommand(self):
        # NO COMMAND!!!
        if (0 < len(self.Commandlist)):
            self.Commandlist.pop(0)
            # vv self.Commandlist.clear()
        self.SetStateCommand('scIdle')
    #
    def CBOnResetSystem(self):
        self.Commandlist.append(CCommand("RSS"))
    #
    def CBOnGetProcessWidth(self):
        self.Commandlist.append(CCommand("GPW"))
    #
    def CBOnSetProcessWidth(self, value):
        self.Commandlist.append(CCommand("SPW " + str(value)))
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - Led
    #------------------------------------------------------------------
    def CBOnGetStateLedSystem(self):
        self.Commandlist.append(CCommand("GLS"))
    #
    def CBOnSwitchLedSystemOn(self):
        self.Commandlist.append(CCommand("LSH"))
    #
    def CBOnSwitchLedSystemOff(self):
        self.Commandlist.append(CCommand("LSL"))
    #
    def CBOnBlinkLedSystem(self, period, count):
        self.Commandlist.append(CCommand("BLS " + period + " " + count))
    #
    #------------------------------------------------------------------
    #   CWindowMain - Callback - Motor
    #------------------------------------------------------------------
    def CBOnGetDeltaPositionLow(self):
        self.Commandlist.append(CCommand("GDL"))
    #
    def CBOnSetDeltaPositionLow(self, value):
        self.Commandlist.append(CCommand("SDL " + str(value)))
    #------------------------------------------------------------------
    def CBOnGetDeltaPositionHigh(self):
        self.Commandlist.append(CCommand("GDH"))
    #
    def CBOnSetDeltaPositionHigh(self, value):
        self.Commandlist.append(CCommand("SDH " + str(value)))
    #------------------------------------------------------------------
    def CBOnGetPWMLow(self):
        self.Commandlist.append(CCommand("GWL"))
    #
    def CBOnSetPWMLow(self, value):
        self.Commandlist.append(CCommand("SWL " + str(value)))
    #------------------------------------------------------------------
    def CBOnGetPWMHigh(self):
        self.Commandlist.append(CCommand("GWH"))
    #
    def CBOnSetPWMHigh(self, value):
        self.Commandlist.append(CCommand("SWH " + str(value)))
    #------------------------------------------------------------------
    def CBOnGetDeltaPositionLow(self):
        self.Commandlist.append(CCommand("GDL"))
    #
    def CBOnSetDeltaPositionLow(self, value):
        self.Commandlist.append(CCommand("SDL " + str(value)))
    #
    def CBOnGetDeltaPositionHigh(self):
        self.Commandlist.append(CCommand("GDH"))
    #
    def CBOnSetDeltaPositionHigh(self, value):
        self.Commandlist.append(CCommand("SDH " + str(value)))
    #------------------------------------------------------------------
    def CBOnGetPWMLow(self):
        self.Commandlist.append(CCommand("GWL"))
    #
    def CBOnSetPWMLow(self, value):
        self.Commandlist.append(CCommand("SWL " + str(value)))
    #
    def CBOnGetPWMHigh(self):
        self.Commandlist.append(CCommand("GWH"))
    #
    def CBOnSetPWMHigh(self, value):
        self.Commandlist.append(CCommand("SWH " + str(value)))
    #------------------------------------------------------------------
    def CBOnGetPositionTarget(self):
        self.Commandlist.append(CCommand("GPT"))
    #
    def CBOnGetPositionActual(self):
        self.Commandlist.append(CCommand("GPA"))
    #
    def CBOnSetPositionActual(self, value):
        self.Commandlist.append(CCommand("SPA " + str(value)))
    #------------------------------------------------------------------
    def CBOnGetVelocityTarget(self):
        self.Commandlist.append(CCommand("GVT"))
    #
    def CBOnSetVelocityTarget(self, value):
        self.Commandlist.append(CCommand("SVT " + str(value)))
    #
    def CBOnGetVelocityActual(self):
        self.Commandlist.append(CCommand("GVA"))
    #------------------------------------------------------------------
    def CBOnMoveVelocityPositive(self, value):
        self.Commandlist.append(CCommand("MVP " + str(value)))        
    #
    def CBOnMoveVelocityNegative(self, value):
        self.Commandlist.append(CCommand("MVN " + str(value)))
    #
    def CBOnMoveAbsolute(self, value):
        self.Commandlist.append(CCommand("MPA " + str(value)))
    #
    def CBOnMoveRelative(self, value):
        self.Commandlist.append(CCommand("MPR " + str(value)))
    #
    def CBOnAbortMotion(self):
        self.Commandlist.append(CCommand("AAM"))
    #
    #------------------------------------------------------------------
    #   Callback - Task - Commandlist
    #------------------------------------------------------------------
    def GetStateCommand(self):
        return self.StateCommand
    #
    def SetStateCommand(self, value):
        # debug print(">>>StateCommand[" + self.StateCommand + "]>>>[" + value + "]")
        self.StateCommand = value
    #
    def CBOnTaskExecute(self):
        if (('scIdle' == self.GetStateCommand()) and (0 < len(self.Commandlist))):
            self.SetStateCommand('scBusy')
            #time.sleep(0.050)
            Command = self.Commandlist[0]
            # command over Terminal to Mqtt:
            self.FrameMqttTerminal.AppendTransmitLine(Command.GetText())            
            self.Commandlist.pop(0)
        else:      
            time.sleep(0.001)
#
            