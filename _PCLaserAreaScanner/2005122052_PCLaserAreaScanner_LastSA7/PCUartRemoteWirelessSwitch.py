#
# ------------------------------------------------------------------
#   RWS - PCUartRemoteWirelessSwitch
# ------------------------------------------------------------------
#   Version: 01V02
#   Date   : 200506
#   Time   : 1555
#   Author : OMDevelop
#
import WindowMainUart as WM
#
TITLE_APPLICATION                  = "RWS - (Uart)RemoteWirelessSwitch"
#
###################################################################
# Main
###################################################################
print("*** " + TITLE_APPLICATION + ": begin")
#
WindowMainUart = WM.CWindowMainUart()
WindowMainUart.Execute()
#
print("*** " + TITLE_APPLICATION + ": end")
#



