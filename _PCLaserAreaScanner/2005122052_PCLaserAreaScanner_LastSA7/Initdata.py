#
# ------------------------------------------------------------------
#   LTM - PCRemoteWirelessSwitch - Initdata
# ------------------------------------------------------------------
#   Version: 01V02
#   Date   : 200207
#   Time   : 1943
#   Author : OMDevelop
#
"""Read/WriteInitdata..."""
import configparser as CP
#
class CWriteInitdata():
    """WriteInitdata..."""

    def __init__(self):
        self.Parser = CP.ConfigParser()
        self.Parser.optionxform = str
    #
    def Open(self, filename):
        """Open..."""
        self.Filename = filename
    #
    def Close(self):
        """Close..."""
        self.File = open(self.Filename, 'w')
        self.Parser.write(self.File)
        self.File.close()
        self.Parser = None
    #
    def WriteSection(self, section):
        """WriteSection..."""
        self.Parser.add_section(section)
# debug print("WriteSection[" + section + "]")

    def WriteValue(self, section, name, value):
        """Value..."""
        self.Parser.set(section, name, value)
        print("WriteValue[" + section + "] Name[" + name + "] Value[" + value + "]")
#
#
#
class CReadInitdata():
    """ReadInitdata..."""

    def __init__(self):
        self.Parser = CP.ConfigParser()
        self.Parser.optionxform = str
    #
    def Open(self, filename):
        """Open..."""
        self.Parser.read(filename)
    #
    def Close(self):
        """Close..."""
        self.Parser = None
    #
    def ReadValue(self, section, name):
        """ReadValue..."""
        Value = "???"
        try:
            Value = self.Parser.get(section, name)
# debug     print("ReadValue[" + section + "] Name[" + name + "] Value[" + Value + "]")
            return Value
        except:
# debug     print("ReadValue[" + section + "] Name[" + name + "] Value[" + Value + "]")
            return "?"
    #
    def ReadValueInit(self, section, name, init):
        """ReadValue..."""
        Value = init
        try:
            Value = self.Parser.get(section, name)
            print("ReadValue[" + section + "] Name[" + name + "] Value[" + Value + "][" + init + "]")
            return Value
        except:
            print("ReadValue[" + section + "] Name[" + name + "] Value[" + Value + "][" + init + "]")
            return Value
#
#
#
def BooleanString(bvalue):
    if (True == bvalue):
        return "True"
    return "False"
#
def StringBoolean(text):
    Text = text.upper()
    if ("T" in Text):
        return True
    if ("1" in Text):
        return True
    return False
#
#
#
            