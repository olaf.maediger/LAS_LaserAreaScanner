#
# ------------------------------------------------------------------
#   LTM - PCRemoteWirelessSwitch - FrameUartControllerSystem
# ------------------------------------------------------------------
#   Version: 01V02
#   Date   : 200207
#   Time   : 1943
#   Author : OMDevelop
#
import tkinter as tk
#
#------------------------------------------------------------------
#   Global Constant
#------------------------------------------------------------------
WIDTH_BUTTON = 6
#
INITDATA_SECTION    = "FrameControllerSystem"
#NAME_PERIOD_MS      = "Period[ms]"
#
#INIT_PERIOD_MS      = "1000"
#
#------------------------------------------------------------------
#   Global Type
#------------------------------------------------------------------
#
class CFrameControllerSystem(tk.Frame):
#---------------------------------------------------------------------
# Constructor
#---------------------------------------------------------------
    def __init__(self, window):
        tk.Frame.__init__(self, window, width = 880, height = 42, \
                          padx = 6, pady = 6, background = "#eeffee")
        self.grid_propagate(0)
        #--------------------------------------------------------------
        self.btnAbortProcessExecution = tk.Button(self, width = 24, \
                                                  text = "Abort Process Execution", \
                                                  command = self.OnAbortProcessExecution)
        self.btnAbortProcessExecution.grid(row = 0, column = 0, \
                                            sticky = tk.W, padx = 2, pady = 2)
        #
        self.btnResetCommand = tk.Button(self, width = 24, \
                                         text = "Reset Command", \
                                         command = self.OnResetCommand)
        self.btnResetCommand.grid(row = 0, column = 2, \
                                  sticky = tk.W, padx = 2, pady = 2)
        #
        self.btnResetSystem = tk.Button(self, width = 24, \
                                        text = "Reset System", \
                                        command = self.OnResetSystem)
        self.btnResetSystem.grid(row = 0, column = 4, \
                                  sticky = tk.W, padx = 2, pady = 2)        
        #--------------------------------------------------------------
    #
    #---------------------------------------------------------------------
    # Property
    #---------------------------------------------------------------------
    def SetCBOnAbortProcessExecution(self, callback):
        self.CBOnAbortProcessExecution = callback
    #
    def SetCBOnResetCommand(self, callback):
        self.CBOnResetCommand = callback
    #
    def SetCBOnResetSystem(self, callback):
        self.CBOnResetSystem = callback
    #
    #---------------------------------------------------------------------
    # Event
    #---------------------------------------------------------------------
    def OnGetProcessCount(self):
        if self.CBOnGetProcessCount:
            self.CBOnGetProcessCount()
    #
    def OnSetProcessCount(self):
        if self.CBOnSetProcessCount:
            Value = self.spbProcessCount.get()
            self.CBOnSetProcessCount(Value)
    #
    def OnGetProcessPeriod(self):
        if self.CBOnGetProcessPeriod:
            self.CBOnGetProcessPeriod()
    #
    def OnSetProcessPeriod(self):
        if self.CBOnSetProcessPeriod:
            Value = self.spbProcessPeriod.get()
            self.CBOnSetProcessPeriod(Value)
    #
    def OnAbortProcessExecution(self):
        if self.CBOnAbortProcessExecution:
            self.CBOnAbortProcessExecution()
    #
    def OnResetCommand(self):
        if self.CBOnResetCommand:
            self.CBOnResetCommand()
    #
    def OnResetSystem(self):
        if self.CBOnResetSystem:
            self.CBOnResetSystem()
    #
    def OnGetProcessWidth(self):
        if self.CBOnGetProcessWidth:
            self.CBOnGetProcessWidth()
    #
    def OnSetProcessWidth(self):
        if self.CBOnSetProcessWidth:
            Value = self.spbProcessWidth.get()
            self.CBOnSetProcessWidth(Value)
    #
    #---------------------------------------------------------------------
    # Initdata
    #---------------------------------------------------------------------
    def ReadInitdata(self, readinitdata):
        print()
        # BP = readinitdata.ReadValueInit(INITDATA_SECTION, \
        #                                 NAME_PERIOD_MS, \
        #                                 INIT_PERIOD_MS)
        # self.spbPeriod.delete(0, tk.END)
        # self.spbPeriod.insert(0, BP)
    #
    def WriteInitdata(self, writeinitdata):
        writeinitdata.WriteSection(INITDATA_SECTION)
        # writeinitdata.WriteValue(INITDATA_SECTION, \
        #                          NAME_PERIOD_MS, \
        #                          self.spbPeriod.get())
    #
    #---------------------------------------------------------------
    # Management
    #--------------------------------------------------------------
    def RefreshCommand(self, command):
        i = 5
        #
    #
    def RefreshCommandData(self, command, data):
        i = 5
        #
    #
