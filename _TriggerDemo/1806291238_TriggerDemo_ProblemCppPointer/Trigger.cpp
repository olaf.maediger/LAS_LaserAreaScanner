#include "Trigger.h"

extern CTrigger TriggerLaser;

CTrigger::CTrigger(byte timerindexperiod, byte timerindexwidth)
{
  FPIrqTimerPeriod = new CIrqTimer(timerindexperiod);
  FPIrqTimerWidth = new CIrqTimer(timerindexwidth);
}


void CTrigger::IrqHandlerPeriod(void)
{
  if (0 < FCounterActual)
  {
    FCounterActual--;
//!!!!!!!!!!!!!!    FPIrqHandlerPeriod(); // extern!
    FPIrqTimerWidth->Start(500000);    
  }
  else
  {
    Stop();
  }
}
void CTrigger::IrqHandlerWidth(void)
{
  FPIrqTimerWidth->Stop();
}
void CTrigger::SetInterruptHandlerPeriod(PCBIrqHandler pirqhandler)
{
  FPIrqHandlerPeriod = pirqhandler;
  FPIrqTimerPeriod->SetInterruptHandler(CTrigger::IrqHandlerPeriod);
}  

void CTrigger::SetInterruptHandlerWidth(PCBIrqHandler pirqhandler)
{
  FPIrqHandlerWidth = pirqhandler;
  FPIrqTimerWidth->SetInterruptHandler(CTrigger::IrqHandlerWidth);
}  

void CTrigger::StartPulseFrequencyHz(double frequency, 
                                     long unsigned counterpreset)
{
  FCounterPreset = counterpreset;
  FPIrqTimerPeriod->Start(frequency);
}
void CTrigger::StartPulsePeriodus(long unsigned period, 
                                  long unsigned counterpreset)
{
  FCounterPreset = counterpreset;
  FPIrqTimerPeriod->SetPeriodus(period);
  FPIrqTimerPeriod->Start();  
}

void CTrigger::StartPulseWidthus(long unsigned period)
{
  FPIrqTimerWidth->SetPeriodus(period);
  FPIrqTimerWidth->Start();  
}

void CTrigger::Stop(void)
{
  FPIrqTimerPeriod->Stop();
}

