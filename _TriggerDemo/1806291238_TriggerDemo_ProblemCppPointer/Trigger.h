#ifndef Trigger_h
#define Trigger_h
//
#include "IrqTimer.h"
//
enum ETriggerState
{
  tsIdle = 0,
  tsActive = 1,
  tsPassive = 2
};
//
class CTrigger
{
  private:
  CIrqTimer* FPIrqTimerPeriod;
  CIrqTimer* FPIrqTimerWidth;
  ETriggerState FState;
  long int FCounterPreset;
  long int FCounterActual;
  //
  PCBIrqHandler FPIrqHandlerPeriod;
  void IrqHandlerPeriod(void);
  PCBIrqHandler FPIrqHandlerWidth;
  void IrqHandlerWidth(void);
  //
  protected:
  //
  public:
	CTrigger(byte timerindexperiod, byte timerindexwidth);
  void SetInterruptHandlerPeriod(PCBIrqHandler pirqhandler);
  void SetInterruptHandlerWidth(PCBIrqHandler pirqhandler);
  void StartPulseFrequencyHz(double frequency, 
                                   long unsigned counterpreset);
  void StartPulsePeriodus(long unsigned period, 
                                long unsigned counterpreset);
  void StartPulseWidthus(long unsigned period);
	void Stop(void);
  void CallInterruptHandlerPeriod();
  void CallInterruptHandlerWidth();
};
//
#endif // Trigger_h
