#include "IrqTimer.h"
//
#include "Trigger.h"
//
const int PIN_LEDSYSTEM = 13;
//
const int INDEX_TIMERPERIOD = 3;
const int INDEX_TIMERWIDTH  = 4;
//
CTrigger TriggerLaser(INDEX_TIMERPERIOD, INDEX_TIMERWIDTH);
//
void IrqHandlerPulsePeriod(void)
{
  digitalWrite(PIN_LEDSYSTEM, HIGH);
}
void IrqHandlerPulseWidth(void)
{
  digitalWrite(PIN_LEDSYSTEM, LOW);
}
//
void setup() 
{ 
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  digitalWrite(PIN_LEDSYSTEM, LOW);
  //
  TriggerLaser.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
  TriggerLaser.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
}

void loop() 
{ 
  TriggerLaser.StartPulseFrequencyHz(1, 3);
  delay(6000);
}



