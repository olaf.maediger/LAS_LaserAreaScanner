#ifndef IrqTimer_h
#define IrqTimer_h
//
#include "Arduino.h"
#include <inttypes.h>
//
#ifdef _SAM3XA_
  #warning  ArduinoDue correct detected!
#else
  #error    This Module only works with ArduinoDue!!!
#endif
//
#if defined TC2
#define NUM_TIMERS  9
#else
#define NUM_TIMERS  6
#endif
// C-Function Callback Type
typedef void (*PCBIrqHandler)(void);
// Cpp-Class Member Callback Type
class CTrigger;
typedef void (CTrigger::*PCCBIrqHandler)(void);
//
class CIrqTimer
{
  protected:
	const unsigned short FIndex; // 0..5/8
	static double FFrequency[NUM_TIMERS];
	// Picks the best clock to lower the error
	static uint8_t OptimizeClock(double frequency, uint32_t& retRC);
  // Make Interrupt handlers friends, so they can use callbacks
  friend void TC0_Handler(void);
  friend void TC1_Handler(void);
  friend void TC2_Handler(void);
  friend void TC3_Handler(void);
  friend void TC4_Handler(void);
  friend void TC5_Handler(void);
#if NUM_TIMERS > 6
  friend void TC6_Handler(void);
  friend void TC7_Handler(void);
  friend void TC8_Handler(void);
#endif
	static void (*FCallbacks[NUM_TIMERS])();
  //
	struct STimer
	{
		Tc *tc;
		uint32_t channel;
		IRQn_Type irq;
	};
  //
	// Store timer configuration (static, as it's fixed for every object)
	static const STimer Timers[NUM_TIMERS];
  //
  public:
	static CIrqTimer GetAvailable(void);
	CIrqTimer(unsigned short _timer);
  CIrqTimer& SetInterruptHandler(PCBIrqHandler irqhandler);
  CIrqTimer& SetInterruptHandler(PCCBIrqHandler irqhandler);
	CIrqTimer& DetachInterrupt(void);
	CIrqTimer& Start(double microseconds = -1);
	CIrqTimer& Stop(void);
	CIrqTimer& SetFrequencyHz(double frequency);
	CIrqTimer& SetPeriodus(double microseconds);
  //
	double GetFrequencyHz(void) const;
	double GetPeriodus(void) const;
  //
  inline __attribute__((always_inline)) bool operator== (const CIrqTimer& rhs) const
    {return FIndex == rhs.FIndex; };
  inline __attribute__((always_inline)) bool operator!= (const CIrqTimer& rhs) const
    {return FIndex != rhs.FIndex; };
};
// Just to call Timer.getAvailable instead of Timer::getAvailable() :
//extern CIrqTimer Timer;

//extern CIrqTimer Timer1;
//// Fix for compatibility with Servo library
//#ifndef USING_SERVO_LIB
//	extern CIrqTimer Timer0;
//	extern CIrqTimer Timer2;
//	extern CIrqTimer Timer3;
//	extern CIrqTimer Timer4;
//	extern CIrqTimer Timer5;
//#endif
//#if NUM_TIMERS > 6
//extern CIrqTimer Timer6;
//extern CIrqTimer Timer7;
//extern CIrqTimer Timer8;
//#endif


#endif // InterruptTimer_h
