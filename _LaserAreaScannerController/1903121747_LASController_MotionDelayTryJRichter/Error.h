//
//--------------------------------
//  Library Error
//--------------------------------
//
#ifndef Error_h
#define Error_h
//
#include "Defines.h"
#include "Serial.h"
//
//--------------------------------
//  Section - Definition
//--------------------------------
//
enum EErrorCode
{
  ecNone = (int)0,  
  ecUnknown = (int)1,
  ecInvalidCommand = (int)2,
  ecToManyParameters = (int)3,
  ecMissingTargetParameter = (int)4,
  ecFailMountSDCard = (int)5,
  ecFailOpenCommandFile = (int)6,
  ecFailWriteCommandFile = (int)7,
  ecFailCloseCommandFile = (int)8,
  ecFailUnmountSDCard = (int)9,
  ecCommandFileNotOpened = (int)10
};
//
class CError
{
  private:
  enum EErrorCode FErrorCode;  
  //
  public:
  CError();
  //
  EErrorCode GetCode();
  void SetCode(EErrorCode errorcode);
  //
  Boolean Open();
  Boolean Close();
  //
  Boolean Handle(CSerial &serial);
};
//
#endif // Error_h



