#include "Led.h"
#include "Process.h"
#include "Command.h"
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  #include "DacInternal.h"
#endif
#if (defined(DAC_MCP4725_X) || defined(DAC_MCP4725_Y)) 
  #include "DacMcp4725.h"
#endif
#include "Pulse.h"
//
extern CLed LedSystem;
extern CLed LedLaser;
extern CSerial SerialCommand;
extern CProcess LASProcess;
extern CCommand LASCommand;
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
extern CDacInternal DacInternal_XY;
#endif
#ifdef DAC_MCP4725_X
extern CDacMcp4725 DacMcp4725_X;
#endif
#ifdef DAC_MCP4725_Y 
extern CDacMcp4725 DacMcp4725_Y;
#endif
extern CPulse PulseTrigger;
//
//---------------------------------------------------
// Segment - IrqCallback - PulseTimer
//---------------------------------------------------
//
// IrqHandler - Process
//
void IrqHandlerPulsePeriod(void)
{ // LedLaser/System - On
  LedSystem.On();
  LedLaser.On(); 
  // PulseTrigger - PulseWidth - Start
  PulseTrigger.StartWidth();
}
//
void IrqHandlerPulseWidth(void)
{ // LedLaser/System - Off
  LedSystem.Off();
  LedLaser.Off();
  // PulseTrigger - PulseWidth - Stop
  PulseTrigger.StopWidth();
  PulseTrigger.IncrementStopPulseStep();
}
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CProcess::CProcess()
{
  FState = spUndefined;
  FStateTick = 0;
  FTimeStamp = millis();
  FTimeMark = FTimeStamp;
  //
  FProcessCount  = INIT_PROCESSCOUNT;
  FProcessPeriod = INIT_PROCESSPERIOD_MS;
  FProcessStep   = 0;
  //
  FXPositionMinimum = INIT_XPOSITION_MINIMUM;
  FXPositionMaximum = INIT_XPOSITION_MAXIMUM;
  FXPositionDelta   = INIT_XPOSITION_DELTA;
  FXPositionActual  = INIT_XPOSITION_ACTUAL;
  //
  FYPositionMinimum = INIT_YPOSITION_MINIMUM;
  FYPositionMaximum = INIT_YPOSITION_MAXIMUM;
  FYPositionDelta   = INIT_YPOSITION_DELTA;
  FYPositionActual  = INIT_YPOSITION_ACTUAL;
  //
  FDelayMotion  = INIT_DELAY_MOTION_MS;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateProcess CProcess::GetState()
{
  return FState;
}

void CProcess::SetState(EStateProcess state)
{
  FStateTick = 0;
  FState = state;
  FSubstate = 0;
  // Message to Manager(PC):
  LASCommand.CallbackStateProcess(SerialCommand, FState, FSubstate);
}

void CProcess::SetState(EStateProcess state, UInt8 substate)
{
  if ((state != FState) || (substate != FSubstate))
  {     
    if (state != FState)
    {
      FStateTick = 0;
    }
    FState = state;
    FSubstate = substate;
    switch (FState)
    {
      case spStopProcessExecution:
        break;
    }
    // Message to Manager(PC):
    LASCommand.CallbackStateProcess(SerialCommand, FState, substate);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CProcess::Open()
{
  SetState(spIdle);
  return true;
}

Boolean CProcess::Close()
{
  SetState(spUndefined);
  return true;
}

//
//----------------------------------------------------------
//  Segment - Common
//----------------------------------------------------------
//
void CProcess::HandleUndefined(CSerial &serial)
{
  delay(1000);
}

void CProcess::HandleIdle(CSerial &serial)
{ // do nothing
  SetTimeStamp();
}

void CProcess::HandleWelcome(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:        
      SetTimeStamp();
      FStateTick++;
      break;
    case 1: case 3: case 5: 
      // if (330000 < GetTimeSpan())
      if (330 < GetTimeSpan())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 2: case 4: case 6:  
      // if (330000 < GetTimeSpan())
      if (330 < GetTimeSpan())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 7:  
      FStateTick++;      
      SetTimeStamp();      
      break;
    case 8:   
      // if (1000000 < GetTimeSpan())
      if (1000 < GetTimeSpan())
      {   
        FStateTick++;
        SetTimeStamp();
      }
      break;
    case 9:   
      // if (1000000 < GetTimeSpan())
      if (1000 < GetTimeSpan())
      {   
        FStateTick++;
        
        SetState(spIdle);
// alternative : 
//        UInt8 CL = 0;
//        UInt8 CH = 7;
//        LASProcess.SetChannelLow(CL);
//        LASProcess.SetChannelHigh(CH);
//        LASProcess.SetState(spRepeatAllTemperatures);
//        LASProcess.SetProcessIndex(0);
//        SetState(spRepeatAllTemperatures);
        SetTimeStamp();
      }
      break;
  }
}

void CProcess::HandleGetHelp(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProgramHeader(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSoftwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetHardwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessCount(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessCount(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessPeriod(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessPeriod(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleStopProcessExecution(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - LedSystem
//----------------------------------------------------------
//
void CProcess::LedSystemState()
{
  switch (LedSystem.GetState())
  {
    case slOn:
      break;
    case slOff:
      break;
    default:
      break;
  }
  SetState(spIdle, LedSystem.GetState());
}

void CProcess::HandleGetLedSystem(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOn(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOff(CSerial &serial)
{
  LedSystemState();
}

// 1807190809 - FSM corrected
void CProcess::HandleBlinkLedSystem(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0: // PulseH
      SetTimeStamp();
      LedSystem.On();
      SetState(spBlinkLedSystem, 1);
      FStateTick = 1;
      break;
    case 1: // PulseWidth
      if (GetPulseWidthus() < GetTimeSpanus())
      {
        LedSystem.Off();
        SetState(spBlinkLedSystem, 0);
        FStateTick = 2; // PulsePeriod
      }
      break;
    case 2: // PulsePeriod
      if (GetProcessPeriod() < GetTimeSpan())
      {
        FProcessStep++;
        if (FProcessStep < FProcessCount)
        {
          FStateTick = 0; // PulseH
        }
        else
        {
          SetState(spIdle);
        }
      }
      break;
  } 
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserPosition
//----------------------------------------------------------
// Command: PulseLaserPosition PLP <x> <y> <p> <c> : Pulse Laser <x><y> <p>eriod <c>ount"
// Command: Definition of XPositionActual, YPositionActual, ProcessPeriod, ProcessCount
// 1805231334 - DelayMotion
// 1807191143 - IrqTimer -> PulsePeriod & PulseWidt
void CProcess::HandlePulseLaserPosition(CSerial &serial)
{ 
  const int STATE_POSITIONXY    = 0;
  const int STATE_DELAYMOTION   = 1;
  const int STATE_STARTTRIGGER  = 2;
  const int STATE_PULSETRIGGER  = 3;
  const int STATE_END           = 4;
  //
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_POSITIONXY:
      SetTimeStamp();
      LedSystem.Off();
      LedLaser.Off();
      SetProcessStep(0);   
#ifdef DAC_INTERNAL_X
      DacInternal_XY.SetValue(DAC_CHANNEL0, GetXPositionActual());
#endif
#ifdef DAC_INTERNAL_Y
      DacInternal_XY.SetValue(DAC_CHANNEL1, GetYPositionActual());
#endif
#ifdef DAC_MCP4725_X
      DacMcp4725_X.SetValue(GetXPositionActual());
#endif
#ifdef DAC_MCP4725_Y 
      DacMcp4725_Y.SetValue(GetYPositionActual());
#endif
      FStateTick = STATE_DELAYMOTION;
      break;
    case STATE_DELAYMOTION:
      if (GetDelayMotion() <= GetTimeSpan())
      {
        SetTimeStamp();
        FStateTick = STATE_STARTTRIGGER; 
      }                 
      break;  
    case STATE_STARTTRIGGER: 
      PulseTrigger.Stop();
      PulseTrigger.SetPulsePeriodus(1000 * GetProcessPeriod()); // us ???????????
      PulseTrigger.SetPulseWidthus(GetPulseWidthus());
      PulseTrigger.SetPulseCount(GetProcessCount());
      PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
      PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
      // start Timer for second, .. Pulses:
      PulseTrigger.Start();
      // First Pulse starts from here:
      IrqHandlerPulsePeriod();
      FStateTick = STATE_PULSETRIGGER;
      break;
    case STATE_PULSETRIGGER: 
      if (GetProcessCount() <= GetProcessStep())
      {
        FStateTick = STATE_END; 
      }
      break;
    case STATE_END:
      SetTimeStamp();
      LedSystem.Off();
      LedLaser.Off();
      SetState(spIdle);
      break;
  }
}

void CProcess::HandleAbortLaserPosition(CSerial &serial)
{  
  PulseTrigger.Stop();
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserRange
//----------------------------------------------------------
//
void CProcess::HandleGetPositionX(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetPositionX(CSerial &serial)
{
  UInt32 PX = GetXPositionActual();
#ifdef DAC_INTERNAL_X
  DacInternal_XY.SetValue(DAC_CHANNEL0, PX);
#endif
#ifdef DAC_MCP4725_X
  DacMcp4725_X.SetValue(PX);
#endif
  SetState(spIdle);
}

void CProcess::HandleGetPositionY(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetPositionY(CSerial &serial)
{
  UInt32 PY = GetYPositionActual();
#ifdef DAC_INTERNAL_Y
  DacInternal_XY.SetValue(DAC_CHANNEL1, PY);
#endif
#ifdef DAC_MCP4725_Y 
  DacMcp4725_Y.SetValue(PY);
#endif
  SetState(spIdle);
}

void CProcess::HandleGetRangeX(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetRangeX(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetRangeY(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetRangeY(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetDelayMotion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetDelayMotion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetPulseWidthus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetPulseWidthus(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserLine
//----------------------------------------------------------
// 
void CProcess::HandlePulseLaserRow(CSerial &serial)
{ 
  const int STATE_INITMOTION    = 0;
  const int STATE_POSITIONY     = 1;
  const int STATE_DELAYMOTION   = 2;
  const int STATE_STARTTRIGGER  = 3;
  const int STATE_PULSETRIGGER  = 4;
  const int STATE_NEXTPOSITION  = 5;
  const int STATE_END           = 6;
  //  
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INITMOTION: 
      SetYPositionActual(GetYPositionMinimum());
      LedSystem.Off();
      LedLaser.Off();
      FStateTick = STATE_POSITIONY;
      SetTimeStamp();
      break;
    case STATE_POSITIONY: 
#ifdef DAC_INTERNAL_Y
      DacInternal_XY.SetValue(DAC_CHANNEL1, GetYPositionActual());
#endif
#ifdef DAC_MCP4725_Y 
      DacMcp4725_Y.SetValue(GetYPositionActual());
#endif
      PulseTrigger.SetPulseStep(0);
      //
      if (PulseTrigger.GetPulseStep() < PulseTrigger.GetPulseCount())
      {
        FStateTick = STATE_DELAYMOTION;
      }
      else
      {
        FStateTick = STATE_NEXTPOSITION;
      }
      SetTimeStamp();
      break;
    case STATE_DELAYMOTION: 
      if (GetDelayMotion() <= GetTimeSpan())
      {
        FStateTick = STATE_STARTTRIGGER; 
        SetTimeStamp();
      }            
      break;
    case STATE_STARTTRIGGER: 
      // LedLaser/System - On
//      LedSystem.On();
//      LedLaser.On(); 
      PulseTrigger.SetInterruptHandlerPeriod(IrqHandlerPulsePeriod);
      PulseTrigger.SetInterruptHandlerWidth(IrqHandlerPulseWidth);
      // PulseTrigger - PulseWidth - First Pulse starts from here:
//      PulseTrigger.StartWidth();
      // start Timer for second, .. Pulses:
//      PulseTrigger.SetPulseCount(1 + PulseTrigger.GetPulseCount());
      PulseTrigger.Start();
      FStateTick = STATE_PULSETRIGGER;     
      break;
    case STATE_PULSETRIGGER: 
      if (PulseTrigger.GetPulseCount() <= PulseTrigger.GetPulseStep())
      {
//        PulseTrigger.SetPulseCount(PulseTrigger.GetPulseCount() - 1);
        FStateTick = STATE_NEXTPOSITION;
      }
      break;
    case STATE_NEXTPOSITION: 
      SetTimeStamp();
      FStateTick = STATE_POSITIONY; 
      FYPositionActual += FYPositionDelta; 
      if (GetYPositionMaximum() < GetYPositionActual())
      { 
        FStateTick = STATE_END; 
      }     
      break;
    case STATE_END: 
      SetTimeStamp();
      LedSystem.Off();
      LedLaser.Off();
      SetState(spIdle);      
      break;      
  }
}

void CProcess::HandleAbortLaserRow(CSerial &serial)
{
  LedSystem.Off();
  LedLaser.Off();
  SetState(spIdle);
  SetTimeStamp();
}

void CProcess::HandlePulseLaserCol(CSerial &serial)
{ 
  const int STATE_INITMOTION    = 0;
  const int STATE_POSITIONX     = 1;
  const int STATE_DELAYMOTION   = 2;
  const int STATE_PULSEHIGH     = 3;
  const int STATE_PULSELOW      = 4;
  const int STATE_NEXTPOSITION  = 5;
  const int STATE_END           = 6;
  //  
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INITMOTION: 
      SetXPositionActual(GetXPositionMinimum());
      FStateTick = STATE_POSITIONX;
      LedSystem.Off();
      LedLaser.Off();
      SetProcessStep(0);
      SetTimeStamp();
      break;
    case STATE_POSITIONX: 
#ifdef DAC_INTERNAL_X
      DacInternal_XY.SetValue(DAC_CHANNEL0, GetXPositionActual());
#endif
#ifdef DAC_MCP4725_X
      DacMcp4725_X.SetValue(GetXPositionActual());
#endif
      if (GetProcessStep() < GetProcessCount())
      {
        FStateTick = STATE_DELAYMOTION;
      }
      else
      {
        FStateTick = STATE_PULSELOW;
      }
      SetTimeStamp();
      break;
    case STATE_DELAYMOTION: 
      if (GetDelayMotion() <= GetTimeSpan())
      {
        FStateTick = STATE_PULSEHIGH; 
        SetTimeStamp();
      }            
      break;
    case STATE_PULSEHIGH: 
      if (slOn != LedLaser.GetState()) 
      { 
        LedSystem.On();
        LedLaser.On();
        IncrementProcessStep();
      }      
      if (GetPulseWidthus() <= GetTimeSpanus())      
      { 
        FStateTick = STATE_PULSELOW;
      }
      break;
    case STATE_PULSELOW: // more Pulses?
      if (slOff != LedLaser.GetState()) 
      { 
        LedSystem.Off();
        LedLaser.Off();
      }
      if (GetProcessPeriod() <= GetTimeSpan())
      { 
        if (GetProcessCount() <= GetProcessStep())
        { 
          FStateTick = STATE_NEXTPOSITION;
        }
        else
        { // more Pulses
          FStateTick = STATE_PULSEHIGH; 
        }
        SetTimeStamp(); 
      }      
      break;
    case STATE_NEXTPOSITION: 
      SetProcessStep(0);
      FXPositionActual += FXPositionDelta;
      FStateTick = STATE_POSITIONX; 
      if (FXPositionMaximum < FXPositionActual) 
      {        
        SetProcessStep(0);
        FStateTick = STATE_END;
        SetTimeStamp(); 
      }     
      break;
    case STATE_END: 
      LedSystem.Off();
      LedLaser.Off();
      SetState(spIdle);      
      SetTimeStamp(); 
      break;      
  }
}

void CProcess::HandleAbortLaserCol(CSerial &serial)
{
  LedSystem.Off();
  LedLaser.Off();
  SetState(spIdle);
  SetTimeStamp(); 
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserMatrix
//----------------------------------------------------------
// 1806271138 - FSM
void CProcess::HandlePulseLaserMatrix(CSerial &serial)
{ 
  const int STATE_INITMOTION    = 0;
  const int STATE_POSITIONXY    = 1;
  const int STATE_DELAYMOTION   = 2;
  const int STATE_PULSEHIGH     = 3;
  const int STATE_PULSELOW      = 4;
  const int STATE_NEXTPOSITION  = 5;
  const int STATE_END           = 6;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INITMOTION: // Init Motion
      SetXPositionActual(GetXPositionMinimum());
      SetYPositionActual(GetYPositionMinimum());
      FStateTick = 1;
      LedSystem.Off();
      LedLaser.Off();
      SetProcessStep(0);
      SetTimeStamp();
      break;
    case STATE_POSITIONXY: // PositionXY
#ifdef DAC_INTERNAL_X
      DacInternal_XY.SetValue(DAC_CHANNEL0, GetXPositionActual());
#endif
#ifdef DAC_INTERNAL_Y
      DacInternal_XY.SetValue(DAC_CHANNEL1, GetYPositionActual());
#endif
#ifdef DAC_MCP4725_X
      DacMcp4725_X.SetValue(GetXPositionActual());
#endif
#ifdef DAC_MCP4725_Y 
      DacMcp4725_Y.SetValue(GetYPositionActual());
#endif
      if (GetProcessStep() < GetProcessCount())
      {
        FStateTick = STATE_DELAYMOTION; 
        SetTimeStamp();
      }
      else
      {
        FStateTick = STATE_PULSELOW; 
        SetTimeStamp();
      }
      break;
    case STATE_DELAYMOTION: 
      if (GetDelayMotion() <= GetTimeSpan())
      {
        FStateTick = STATE_PULSEHIGH; 
        SetTimeStamp();
      }            
      break;
    case STATE_PULSEHIGH: 
      if (slOn != LedLaser.GetState()) 
      { 
        LedSystem.On();
        LedLaser.On();
        IncrementProcessStep();
      }      
      if (GetPulseWidthus() <= GetTimeSpanus())      
      { 
        FStateTick = STATE_PULSELOW;
      }
      break;
    case STATE_PULSELOW: // more Pulses?
      if (slOff != LedLaser.GetState()) 
      { 
        LedSystem.Off();
        LedLaser.Off();
      }
      if (GetProcessPeriod() <= GetTimeSpan())
      { 
        if (GetProcessCount() <= GetProcessStep())
        { // next Position
          FStateTick = STATE_NEXTPOSITION; 
        }
        else
        { // more Pulses
          FStateTick = STATE_PULSEHIGH; 
        }
        SetTimeStamp(); 
      }      
      break;
    case STATE_NEXTPOSITION: 
      SetProcessStep(0);
      FXPositionActual += FXPositionDelta;
      FStateTick = STATE_POSITIONXY;
      if (FXPositionMaximum < FXPositionActual) 
      {        
        FYPositionActual += FYPositionDelta; 
        SetXPositionActual(GetXPositionMinimum());          
        SetProcessStep(0);
        if (GetYPositionMaximum() < GetYPositionActual())
        { 
          FStateTick = STATE_END;
        }
      }     
      SetTimeStamp(); 
      break;
    case STATE_END: 
      LedSystem.Off();
      LedLaser.Off();
      SetState(spIdle);      
      SetTimeStamp(); 
      break;      
  }
}

void CProcess::HandleAbortLaserMatrix(CSerial &serial)
{
  LedSystem.Off();
  LedLaser.Off();
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserImage
//----------------------------------------------------------
// 1806271414 - FSM
void CProcess::HandlePulseLaserImage(CSerial &serial)
{
  const int STATE_INITMOTION  = 0;
  const int STATE_POSITIONXY  = 1;
  const int STATE_DELAYMOTION = 2;
  const int STATE_PULSEHIGH   = 3;
  const int STATE_PULSELOW    = 4;
  const int STATE_WAIT        = 5;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INITMOTION:
// debug Serial.println("0: Init");
      SetTimeStamp();
      LedSystem.Off();
      LedLaser.Off();
      SetProcessStep(0);
// debug Serial.print("PS:"); Serial.print(GetProcessStep()); Serial.print(" PC:"); Serial.println(GetProcessCount());
      FStateTick = STATE_POSITIONXY;
      break;
    case STATE_POSITIONXY:
// debug Serial.println("1: Position"); Serial.print("PS:"); Serial.print(GetProcessStep()); Serial.print(" PC:"); Serial.println(GetProcessCount());
#ifdef DAC_INTERNAL_X
      DacInternal_XY.SetValue(DAC_CHANNEL0, GetXPositionActual());
#endif
#ifdef DAC_INTERNAL_Y
      DacInternal_XY.SetValue(DAC_CHANNEL1, GetYPositionActual());
#endif
#ifdef DAC_MCP4725_X
      DacMcp4725_X.SetValue(GetXPositionActual());
#endif
#ifdef DAC_MCP4725_Y 
      DacMcp4725_Y.SetValue(GetYPositionActual());
#endif
      if (GetProcessStep() < GetProcessCount())
      {
        FStateTick = STATE_DELAYMOTION;
        SetTimeStamp();
      }
      else
      {
        FStateTick = STATE_WAIT;
      }
      break;
    case STATE_DELAYMOTION:
      if (GetDelayMotion() <= GetTimeSpan())
      {
        FStateTick = STATE_PULSEHIGH;
        SetTimeStamp();
      }      
      break;
    case 3: // PulseH 
      if (slOn != LedLaser.GetState()) 
      { // debug Serial.println("3: LedLaser: ON");
        LedSystem.On();
        LedLaser.On();
        IncrementProcessStep();
// debug Serial.print("Incremented PS:"); Serial.print(GetProcessStep()); Serial.print(" PC:"); Serial.println(GetProcessCount());
      }      
      if (GetPulseWidthus() <= GetTimeSpanus())      
      { 
        FStateTick = STATE_PULSELOW;
      }
      break;
    case STATE_PULSELOW: // more Pulses? 
      if (slOff != LedLaser.GetState()) 
      { // debug Serial.println("3: LedLaser: OFF");
        LedSystem.Off();
        LedLaser.Off();
      }
      if (GetProcessPeriod() <= GetTimeSpan())
      { 
        SetTimeStamp(); 
        if (GetProcessCount() <= GetProcessStep())
        { // wait for next Position          
          FStateTick = STATE_WAIT;
        }
        else
        { // more Pulses -> back to PulseH
          FStateTick = STATE_PULSEHIGH; 
        }
      }      
      break;
    case STATE_WAIT: // wait for next PLM- or ALM-Command ...
// debug Serial.println("4: Wait");
      LedSystem.Off();
      LedLaser.Off();
      SetState(spIdle);      
      break;
  }
}

void CProcess::HandleAbortLaserImage(CSerial &serial)
{
  SetState(spIdle); 
}
//
//----------------------------------------------------------
//  Segment - Collector
//----------------------------------------------------------
//
void CProcess::Handle(CSerial &serial)
{
  switch (LASProcess.GetState())
  { // Common
    case spIdle:
      HandleIdle(serial);
      break;
    case spWelcome:
      HandleWelcome(serial);
      break;
    case spGetHelp:
      HandleGetHelp(serial);
      break;
    case spGetProgramHeader:
      HandleGetProgramHeader(serial);
      break;
    case spGetSoftwareVersion:
      HandleGetSoftwareVersion(serial);
      break;
    case spGetHardwareVersion:
      HandleGetHardwareVersion(serial);
      break;
    case spGetProcessCount:
      HandleGetProcessCount(serial);
      break;
    case spSetProcessCount:
      HandleSetProcessCount(serial);
      break;
    case spGetProcessPeriod:
      HandleGetProcessPeriod(serial);
      break;
    case spSetProcessPeriod:
      HandleSetProcessPeriod(serial);
      break;
    case spStopProcessExecution:
      HandleStopProcessExecution(serial);
      break;
    // LedSystem
    case spGetLedSystem:
      HandleGetLedSystem(serial);
      break;
    case spLedSystemOn:
      HandleLedSystemOn(serial);
      break;
    case spLedSystemOff:
      HandleLedSystemOff(serial);
      break;
    case spBlinkLedSystem:
      HandleBlinkLedSystem(serial);
      break;
    // Measurement - LaserParameter
    case spGetPositionX:
      HandleGetPositionX(serial);
      break;
    case spSetPositionX:
      HandleSetPositionX(serial);
      break;
    case spGetPositionY:
      HandleGetPositionY(serial);
      break;
    case spSetPositionY:
      HandleSetPositionY(serial);
      break;      
    case spGetRangeX:
      HandleGetRangeX(serial);
      break;
    case spSetRangeX:
      HandleSetRangeX(serial);
      break;
    case spGetRangeY:
      HandleGetRangeY(serial);
      break;
    case spSetRangeY:
      HandleSetRangeY(serial);
      break;
    case spGetDelayMotion:
      HandleGetDelayMotion(serial);
      break;
    case spSetDelayMotion:
      HandleSetDelayMotion(serial);
      break;
    case spGetPulseWidth:
      HandleGetPulseWidthus(serial);
      break;
    case spSetPulseWidth:
      HandleSetPulseWidthus(serial);
      break;
    // Measurement - LaserPosition
    case spPulseLaserPosition:
      HandlePulseLaserPosition(serial);
      break;
    case spAbortLaserPosition:
      HandleAbortLaserPosition(serial);
      break;
    // Measurement - LaserLine
    case spPulseLaserRow:
      HandlePulseLaserRow(serial);
      break;
    case spAbortLaserRow:
      HandleAbortLaserRow(serial);
      break;
    case spPulseLaserCol:
      HandlePulseLaserCol(serial);
      break;
    case spAbortLaserCol:
      HandleAbortLaserCol(serial);
      break;
    // Measurement - LaserMatrix
    case spPulseLaserMatrix:
      HandlePulseLaserMatrix(serial);
      break;
    case spAbortLaserMatrix:
      HandleAbortLaserMatrix(serial);
      break;
    // Measurement - LaserImage
    case spPulseLaserImage:
      HandlePulseLaserImage(serial);
      break;
    case spAbortLaserImage:
      HandleAbortLaserImage(serial);
      break;
    default: // spUndefined
      HandleUndefined(serial);
      break;
  }  
}
