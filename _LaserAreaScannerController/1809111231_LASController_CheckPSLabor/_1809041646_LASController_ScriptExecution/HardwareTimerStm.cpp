#include "Defines.h"
#if defined(PROCESSOR_STM32F103C8)
//
#include "HardwareTimerStm.h"
//
CHardwareTimerStm::CHardwareTimerStm(UInt8 timerid) 
  : HardwareTimer(timerid)
{
  FTimerID = timerid;
  HardwareTimer::setChannelMode(FTimerID, TIMER_OUTPUTCOMPARE);
}

void CHardwareTimerStm::SetInterruptHandler(void (*irqhandler)(void))
{
	FIrqHandler = irqhandler;
  HardwareTimer::attachInterrupt(FTimerID, FIrqHandler);
}

void CHardwareTimerStm::ResetInterruptHandler(void)
{
	Stop();
  HardwareTimer::detachInterrupt(FTimerID);
	FIrqHandler = NULL;
}

void CHardwareTimerStm::SetPeriodus(UInt32 periodus)
{    
  FPeriodus = periodus;
  HardwareTimer::setPeriod(periodus);
  HardwareTimer::setPrescaleFactor(0xFFFF);
  HardwareTimer::setCompare1(0x0000);
  HardwareTimer::setCompare2(0x0000);
  HardwareTimer::setCompare3(0x0000);
  HardwareTimer::setCompare4(0x0000);
}

void CHardwareTimerStm::Start(void)
{
  HardwareTimer::resume();
}

void CHardwareTimerStm::Stop(void)
{
  HardwareTimer::pause();
}
//
UInt32 CHardwareTimerStm::GetPeriodus(void)
{
	return FPeriodus;
}
//
#endif // defined(PROCESSOR_STM32F103C8)

