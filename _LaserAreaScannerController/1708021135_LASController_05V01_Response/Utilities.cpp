#include "Utilities.h"
#include "LedLine.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
//
//
//-------------------------------------------
//  Segment - Global Variable
//-------------------------------------------
//  
extern enum EErrorCode GlobalErrorCode;
//
extern EStateAutomation StateAutomation;
extern UInt32 CountRepetition; // [1]
extern UInt32 CountLaserPulses; // [1]
extern UInt32 DelayMotion; // [us]
extern UInt32 DelayPulse; // [us]
//
extern UInt16 XPositionActual; // [stp]
extern UInt16 XPositionDelta; // [stp]
extern UInt16 XPositionMinimum; // [stp]
extern UInt16 XPositionMaximum; // [stp]
//
extern UInt16 YPositionActual; // [stp]
extern UInt16 YPositionDelta; // [stp]
extern UInt16 YPositionMinimum; // [stp]
extern UInt16 YPositionMaximum; // [stp]
//
//-------------------------------------------
//  Segment - Global Class
//-------------------------------------------
//  
extern CLedLine LedLine;
//
// NC extern CAdc AnalogDigitalConverter;
//
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
extern CDac DigitalAnalogConverter;
#endif
//
// NC extern CPwm PulseWidthModulator;
//
extern CSerialCommand SerialCommand;
//
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_VERSION);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_SOFTWAREVERSION);
  serial.WriteNewLine();
}

void WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_HARDWAREVERSION);
  serial.WriteNewLine();
}

void WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.WriteLine(HELP_H);
  serial.WriteAnswer(); serial.WriteLine(HELP_GPH);
  serial.WriteAnswer(); serial.WriteLine(HELP_GSV);
  serial.WriteAnswer(); serial.WriteLine(HELP_GHV);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LED);
  serial.WriteAnswer(); serial.WriteLine(HELP_GLD);
  serial.WriteAnswer(); serial.WriteLine(HELP_SLD);
  serial.WriteAnswer(); serial.WriteLine(HELP_CLD);
  serial.WriteAnswer(); serial.WriteLine(HELP_TLD);
  serial.WriteAnswer(); serial.WriteLine(HELP_PLD);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_DAC);
  serial.WriteAnswer(); serial.WriteLine(HELP_GDV);
  serial.WriteAnswer(); serial.WriteLine(HELP_SDV);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_AUTOMATION);
  serial.WriteAnswer(); serial.WriteLine(HELP_SMX);
  serial.WriteAnswer(); serial.WriteLine(HELP_SMY);
  serial.WriteAnswer(); serial.WriteLine(HELP_SDM);
  serial.WriteAnswer(); serial.WriteLine(HELP_SDP);
  serial.WriteAnswer(); serial.WriteLine(HELP_STT);
  serial.WriteAnswer(); serial.WriteLine(HELP_ABT);  
}
//
//#########################################################
//  Segment - Execution - Common
//#########################################################
//
void ExecuteGetHelp(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %i", 
          SerialCommand.GetPRxdCommand(), COUNT_HELPLINES);
  serial.WriteLine(SerialCommand.GetTxdBuffer());         
  WriteHelp(serial);
  serial.WritePrompt();
}

void ExecuteGetProgramHeader(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %i", 
          SerialCommand.GetPRxdCommand(), COUNT_HEADERLINES);
  serial.WriteLine(SerialCommand.GetTxdBuffer());   
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void ExecuteGetSoftwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %i", 
          SerialCommand.GetPRxdCommand(), COUNT_ANSWERLINE);
  serial.WriteLine(SerialCommand.GetTxdBuffer());   
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void ExecuteGetHardwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %i", 
          SerialCommand.GetPRxdCommand(), COUNT_ANSWERLINE);
  serial.WriteLine(SerialCommand.GetTxdBuffer());   
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Led
//#########################################################
//
void ExecuteGetLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  int State = LedLine.GetState(Index);
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %s %i", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0), State);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteClearLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  byte Mask = 0x01 << Index;
  // Execute:
  if (Index <= 7)
  {
    LedLine.Off(Mask);
  }
  else
  {
    LedLine.Off();    
  }
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %s", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0));
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSetLed(CSerial &serial)
{ // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  byte Mask = 0x01 << Index;
  // Execute:
  if (Index <= 7)
  {
    LedLine.On(Mask);
  }
  else
  {
    LedLine.On();
  }
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %s", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0));
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteToggleLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  if (Index <= 7)
  {
    LedLine.Toggle(0x01 << Index);
  }
  else
  {
    LedLine.Toggle();
  }
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %u", 
          SerialCommand.GetPRxdCommand(), Index);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecutePulseLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  UInt32 DelayMillis = atol(SerialCommand.GetPRxdParameters(1));
  UInt32 Count = atol(SerialCommand.GetPRxdParameters(2));
  // Execute:
  if (Index <= 7)
  {
    LedLine.Pulse(0x01 << Index, DelayMillis, Count);
  }
  else
  {
    LedLine.Pulse(DelayMillis, Count);
  }
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %u %lu %lu", 
          SerialCommand.GetPRxdCommand(), Index, DelayMillis, Count);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

//
//#########################################################
//  Segment - Execution - Dac
//#########################################################
//
void ExecuteGetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  UInt16 Value = 0x0000;
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  Value = DigitalAnalogConverter.GetValue(Channel);
#endif  
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %u %u", 
          SerialCommand.GetPRxdCommand(), Channel, Value);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
  UInt32 Value = atol(SerialCommand.GetPRxdParameters(1));
  // Execute:
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.SetValue(Channel, Value);
  Value = DigitalAnalogConverter.GetValue(Channel);
#endif  
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %u %lu", 
          SerialCommand.GetPRxdCommand(), Channel, Value);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Helper
//#########################################################
//
void SetPosition()
{
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.SetValue(DAC_MIRRORX, XPositionActual);
  DigitalAnalogConverter.SetValue(DAC_MIRRORY, YPositionActual);  
#endif
}

void SetTrigger()
{
  LedLine.On(0x01 << LED_TRIGGER);
  delayMicroseconds(DelayPulse / 2);
  LedLine.Off(0x01 << LED_MOTIONY);
  LedLine.Off(0x01 << LED_MOTIONX);
  delayMicroseconds(DelayPulse / 2);
  LedLine.Off(0x01 << LED_TRIGGER);
}

void TriggerLaserPulses()
{ // Pulsedelay fixed to 1000us!
  LedLine.Off(0x01 << LED_MOTIONY);
  LedLine.Off(0x01 << LED_MOTIONX);
  // wait for motion stops...
  delayMicroseconds(DelayMotion);
  //
  UInt32 TI;
  UInt32 DL = DelayPulse / 10;
  UInt32 DH = 9* DL;
  for (TI = 0; TI < CountLaserPulses; TI++)
  {    
    LedLine.On(0x01 << LED_TRIGGER);
    delayMicroseconds(DH);
    LedLine.Off(0x01 << LED_TRIGGER);
    delayMicroseconds(DL);
  }
}
//
//#########################################################
//  Segment - Execution - Automation
//#########################################################
//
void ExecuteSetMotionX(CSerial &serial)
{ // Analyse parameters:
  XPositionMinimum = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  XPositionMaximum = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  XPositionDelta = abs(atoi(SerialCommand.GetPRxdParameters(2)));
  // Execute -
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %lu %lu %lu", 
          SerialCommand.GetPRxdCommand(), XPositionMinimum, XPositionMaximum, XPositionDelta);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

void ExecuteSetMotionY(CSerial &serial)
{ // Analyse parameters:
  YPositionMinimum = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  YPositionMaximum = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  YPositionDelta = abs(atoi(SerialCommand.GetPRxdParameters(2)));
  // Execute -
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %lu %lu %lu", 
          SerialCommand.GetPRxdCommand(), YPositionMinimum, YPositionMaximum, YPositionDelta);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

void ExecuteSetDelayMotion(CSerial &serial)
{ // Analyse parameters:
  DelayMotion = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  // Execute -
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %lu", 
          SerialCommand.GetPRxdCommand(), DelayMotion);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

void ExecuteSetDelayPulse(CSerial &serial)
{ // Analyse parameters:
  DelayPulse = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  // Execute -
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %lu", 
          SerialCommand.GetPRxdCommand(), DelayPulse);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

bool AbortTrigger()
{
  StateAutomation = saIdle;
  LedLine.Off(0x01 << LED_MOTIONY);
  LedLine.Off(0x01 << LED_MOTIONX);
  LedLine.Off(0x01 << LED_BUSY);
  return true;
}

void ExecuteAbortTrigger(CSerial &serial)
{ // Analyse parameters -
  // Execute:
  AbortTrigger();
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":ABT");
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

Boolean StartTrigger()
{
  if (0 < CountRepetition)
  {
    LedLine.On(0x01 << LED_MOTIONY);
    LedLine.On(0x01 << LED_MOTIONX);
    LedLine.On(0x01 << LED_BUSY);
    XPositionActual = XPositionMinimum;
    YPositionActual = YPositionMinimum;
    StateAutomation = saTriggerRowColLeft;
    return true;
  }
  return false;
}

void ExecuteStartTrigger(CSerial &serial)
{ // Analyse parameters:
  CountLaserPulses = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  CountRepetition = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  // Execute:
  StartTrigger();
  // Response:
  sprintf(SerialCommand.GetTxdBuffer(), ":%s %lu %lu", 
          SerialCommand.GetPRxdCommand(), CountLaserPulses, CountRepetition);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//#########################################################
//  Segment - Handler - Error
//#########################################################
//
void HandleError(CSerial &serial)
{
  if (ecNone != GlobalErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(GlobalErrorCode, SerialCommand.GetTxdBuffer()); 
    serial.WriteText(SerialCommand.GetTxdBuffer());
    serial.WriteText("]: ");
    switch (GlobalErrorCode)
    {
      case ecNone:
        serial.WriteText("No Error");
        break;
      case ecUnknown:
        serial.WriteText("Unknown");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
      case ecToManyParameters:
        serial.WriteText("To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.WriteText("Missing Target Parameter");
        break;              
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    GlobalErrorCode = ecNone;
  }
}
//
//#########################################################
//  Segment - Automation
//#########################################################
//
void HandleAutomationIdle(CSerial &serial)
{  
  // nothing to do
}

void HandleAutomationMotionRowColLeft(CSerial &serial)
{
  LedLine.On(0x01 << LED_MOTIONX);
  LedLine.On(0x01 << LED_MOTIONY);
  //
  SetPosition();
  SetTrigger();
  //
  XPositionActual += XPositionDelta;
  if (XPositionMaximum < XPositionActual)
  {
    XPositionActual = XPositionMinimum;
    YPositionActual += YPositionDelta;
    if (YPositionMaximum < YPositionActual)
    { // Finish Area scanning
      XPositionActual = XPositionMinimum;
      YPositionActual = YPositionMinimum;
      CountRepetition--;
      LedLine.Toggle(0x01 << LED_BUSY);
      if (0 == CountRepetition)
      { // ends...
        // SetPosition();
        ExecuteAbortTrigger(serial);
        return;
      }
    } 
  } 
}

void HandleAutomationTriggerRowColLeft(CSerial &serial)
{
  
  LedLine.On(0x01 << LED_MOTIONX);
  LedLine.On(0x01 << LED_MOTIONY);
  //
  SetPosition();
  TriggerLaserPulses();
  //
  XPositionActual += XPositionDelta;
  if (XPositionMaximum < XPositionActual)
  {
    XPositionActual = XPositionMinimum;
    YPositionActual += YPositionDelta;
    if (YPositionMaximum < YPositionActual)
    { // Finish Area scanning
      XPositionActual = XPositionMinimum;
      YPositionActual = YPositionMinimum;
      CountRepetition--;      
      LedLine.Toggle(0x01 << LED_BUSY);
      if (0 == CountRepetition)
      { // ends...
        // SetPosition();
        ExecuteAbortTrigger(serial);
        return;
      }
    } 
  } 
}

void HandleAutomation(CSerial &serial)
{
  switch (StateAutomation)
  {
    case saIdle:
      HandleAutomationIdle(serial);
      break;
    case saMotionRowColLeft:
      HandleAutomationMotionRowColLeft(serial);
      break;
    case saMotionRowColBoth:
      break;
    case saTriggerRowColLeft:
      HandleAutomationTriggerRowColLeft(serial);
      break;
    case saTriggerRowColBoth:
      break;
  }
}







Boolean ExecuteRxdCommand(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand:%s", PRxdCommand);
// debug serialtarget.WriteLine(TxdBuffer);
  if (!strcmp("H", SerialCommand.GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp("GPH", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp("GSV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp("GHV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  // ----------------------------------
  // Led
  // ----------------------------------
  if (!strcmp("GLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetLed(serial);
    return true;
  } else 
  if (!strcmp("SLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetLed(serial);
    return true;
  } else   
  if (!strcmp("CLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteClearLed(serial);
    return true;
  } else   
  if (!strcmp("TLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteToggleLed(serial);
    return true;
  } else   
  if (!strcmp("PLD", SerialCommand.GetPRxdCommand()))
  {
    ExecutePulseLed(serial);
    return true;
  } else   
  // ----------------------------------
  // Dac
  // ----------------------------------
  if (!strcmp("GDV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetDacValue(serial);
    return true;
  } else   
  if (!strcmp("SDV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDacValue(serial);
    return true;
  } else   
  // ----------------------------------
  // Automation
  // ----------------------------------
  if (!strcmp("SMX", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetMotionX(serial);
    return true;
  } else   
  if (!strcmp("SMY", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetMotionY(serial);
    return true;
  } else   
  if (!strcmp("SDM", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDelayMotion(serial);
    return true;
  } else   
  if (!strcmp("SDP", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDelayPulse(serial);
    return true;
  } else   
  if (!strcmp("STT", SerialCommand.GetPRxdCommand()))
  {
    ExecuteStartTrigger(serial);
    return true;
  } else   
  if (!strcmp("ABT", SerialCommand.GetPRxdCommand()))
  {
    ExecuteAbortTrigger(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    GlobalErrorCode = ecInvalidCommand;
  }
  return false;  
}



void HandleSerialCommands(CSerial &serial)
{
  if (SerialCommand.AnalyseRxdCommand(serial))
  {
    ExecuteRxdCommand(serial);
  }  
}




