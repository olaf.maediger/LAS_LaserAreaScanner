//
//##############################################
//
//----------------------------------------------
//  Segment - Global Definition
//----------------------------------------------
//
#define BASE_10 10
//
#define BAUDRATE_SERIAL 115200
//
#define PIN_LEDSYSTEM 13
// 
#define INIT_STATEMOTION 0
#define INIT_REPETITIONCOUNT 100
//
//----------------------------------------------
//  Segment - Global Variable
//----------------------------------------------
//
// Global Variable - Serial
String FRxdBuffer;
String FRxdHeader;
String FRxdCommand;
String FRxdParameter1;
String FRxdParameter2;
char TxdBuffer[14];
//
// Global Variable - LedSystem
bool FStateLedSystem = 0;
//
// Global Variable - Area
int FStateMotion = 0;
int FRepetitionCount = INIT_REPETITIONCOUNT;
int FPixelSizeX = 4096;
int FPixelSizeY = 4096;
int FPixelIndexX = 0;
int FPixelIndexY = 0;
int FStepWidthX = 128;
int FStepWidthY = 128;
int FDelayMicroSeconds = 0;
// NC bool FInfoPositionTrigger = false;
// NC int FChange = 1;
//
//##############################################
//
//----------------------------------------------
//  Segment - Execution - LedSystem
//----------------------------------------------
//
bool SetSystemLed(String state)
{
  FStateLedSystem = abs(state.toInt());
  if (0 == FStateLedSystem)
  {
    digitalWrite(PIN_LEDSYSTEM, LOW);
    return true;
  }
  else
    if (1 == FStateLedSystem)
    {
      digitalWrite(PIN_LEDSYSTEM, HIGH);
      return true;
    }
  return false;
}

void HandleSetLedSystem(String parameter1)
{
  if (SetSystemLed(parameter1))
  {
    itoa(FStateLedSystem, TxdBuffer, BASE_10);
    Comment();
    Serial1.print("SSL ");
    Serial1.print(TxdBuffer);
    Comment();
    return;
  }
  Error("Invalid Parameter");
}
//
//----------------------------------------------
//  Segment - Execution - Area
//----------------------------------------------
//
bool SetAreaSize(String pixelsizex, String pixelsizey)
{
  FPixelSizeX = abs(pixelsizex.toInt());
  FPixelSizeY = abs(pixelsizey.toInt());
  if (0 < FPixelSizeX)
  {
    if (0 < FPixelSizeY)
    {
      return true;
    }
  }
  return false;
}

void HandleSetAreaSize(String parameter1, String parameter2)
{
  if (SetAreaSize(parameter1, parameter2))
  {
    Comment();
    Serial1.print("SAS ");
    itoa(FPixelSizeX, TxdBuffer, BASE_10);
    Serial1.print(TxdBuffer);
    Serial1.print(" ");
    itoa(FPixelSizeY, TxdBuffer, BASE_10);
    Serial1.print(TxdBuffer);
    Comment();
    return;
  }
  Error("Invalid Parameter");
}


bool SetStepWidth(String stepwidthx, String stepwidthy)
{
  FStepWidthX = abs(stepwidthx.toInt());
  FStepWidthY = abs(stepwidthy.toInt());
  if (0 < FPixelSizeX)
  {
    if (0 < FPixelSizeY)
    {
      return true;
    }
  }
  return false;
}

void HandleSetStepWidth(String parameter1, String parameter2)
{
  if (SetStepWidth(parameter1, parameter2))
  {
    Comment();
    Serial1.print("SSW ");
    itoa(FStepWidthX, TxdBuffer, BASE_10);
    Serial1.print(TxdBuffer);
    Serial1.print(" ");
    itoa(FStepWidthY, TxdBuffer, BASE_10);
    Serial1.print(TxdBuffer);
    Comment();
    return;
  }
  Error("Invalid Parameter");
}



bool StartMotion(String delaymicroseconds, String repetitioncount)
{
  FDelayMicroSeconds = abs(delaymicroseconds.toInt());
  if (FDelayMicroSeconds < 500)
  {
    FDelayMicroSeconds = 500;
  }
  FRepetitionCount = abs(repetitioncount.toInt());
  if (FRepetitionCount < 1)
  {
    FRepetitionCount = 1;
  }
  FPixelIndexX = 0;
  FPixelIndexY = 0;
  if (0 < FPixelSizeX)
  {
    if (0 < FPixelSizeY)
    {
      FStateMotion = FRepetitionCount;
      return true;
    }
  }
  FStateMotion = 0;
  return false;
}

void HandleStartMotion(String parameter1, String parameter2)
{
  if (StartMotion(parameter1, parameter2))
  {
    Comment();
    Serial1.print("STM ");
    itoa(FDelayMicroSeconds, TxdBuffer, BASE_10);
    Serial1.print(TxdBuffer);
    Serial1.print(" ");
    itoa(FRepetitionCount, TxdBuffer, BASE_10);
    Serial1.print(TxdBuffer);
    Comment();
    return;
  }
  Error("Invalid Parameter");
}

bool AbortMotion()
{
  FStateMotion = false;
  return true;
}

void HandleAbortMotion()
{
  AbortMotion();
  Comment();
  Serial1.print("ABM ");
  Comment();
}

void SetPosition()
{
//  if (FInfoPositionTrigger)
//  {
//    Comment();
//    Serial1.print("X[");
//    itoa(FPixelIndexX, TxdBuffer, BASE_10);
//    Serial1.print(TxdBuffer);
//    Serial1.print("] Y[");
//    itoa(FPixelIndexY, TxdBuffer, BASE_10);
//    Serial1.print(TxdBuffer);
//    Serial1.print("]");
//  }
  analogWrite(DAC0, FPixelIndexX);
  analogWrite(DAC1, FPixelIndexY);  
}

void SetTrigger()
{
//  if (FInfoPositionTrigger)
//  {
//    //digitalWrite(PIN_LEDSYSTEM, HIGH);
//    // delay(100);
//    //digitalWrite(PIN_LEDSYSTEM, LOW);
//  }
  digitalWrite(PIN_LEDSYSTEM, HIGH);
  delayMicroseconds(FDelayMicroSeconds);
  digitalWrite(PIN_LEDSYSTEM, LOW);
}
//
//##############################################
//
//----------------------------------------------
//  Segment - Serial 
//----------------------------------------------
//
void Prompt()
{
  Serial1.print("\r\n>");
}

void Comment()
{
  Serial1.print("\r\n#");
}

void Error(String text)
{
  Prompt();
  Serial1.print("Error: " + text + "!");
}

void HandleHeader()
{
  Serial1.print("\r\n");
  Comment();
  Serial1.print("Project..: LAS - LaserAreaScanner");
  Comment();
  Serial1.print("Version..: 1.6");
  Comment();
  Serial1.print("Date.....: 161115");
  Comment();
  Serial1.print("Time.....: 1825");
  Comment();
  Serial1.print("Author...: OM");
  Prompt();  
}

void HandleHelp()
{
  Comment();
  Serial1.print(" H : This Help");
  Comment();
  Serial1.print(" SSL <0..1> : Set SystemLed off/on");
  Comment();
  Serial1.print(" SAS <1..4095> <1..4095> : Set Area size <width> <height>");
  Comment();
  Serial1.print(" SSW <1..4095> <1..4095> : Set Stepwidth <stepwidthx> <stepwidthy>");
  Comment();
  Serial1.print(" STM <delayus> <count>: Start Motion with <count>-Repetitions and <delayus> for each pixel");
  Comment();
  Serial1.print(" ABM : Abort Motion");
  Comment();
}

bool AnalyseRxdCommandParameter()
{
  int L = FRxdBuffer.length();
  if (0 < L)
  {
    String Buffer = "";
    FRxdCommand = "";
    FRxdParameter1 = "";
    FRxdParameter2 = "";
    for (int CI = 0; CI < L; CI++)
    {
      char C = FRxdBuffer[CI];
      switch (C)
      {
        case ' ':
          if (FRxdCommand == "")
          {
            Buffer.toUpperCase();
            FRxdCommand = Buffer;
            Buffer = "";
          } else
            if (FRxdParameter1 == "")
            {
              FRxdParameter1 = Buffer;
              Buffer = "";
            } else
              if (FRxdParameter2 == "")
              {
                FRxdParameter2 = Buffer;
                Buffer = "";
              }
          break;
        default:
          Buffer += C;
      }
    }
    if (0 < Buffer.length())
    {
      if (FRxdCommand == "")
      {
        Buffer.toUpperCase();
        FRxdCommand = Buffer;
      } else
        if (FRxdParameter1 == "")
        {
          FRxdParameter1 = Buffer;
        } else
          if (FRxdParameter2 == "")
          {
            FRxdParameter2 = Buffer;
          }
    }
    return true;
  }
  return false;
}

void CommandDispatcher(String command, String parameter1, String parameter2)
{
  if (command == "H")
  {
    HandleHelp();
    return;
  }
  // Led
  if (command == "SSL")
  {
    HandleSetLedSystem(parameter1);
    return;
  }
  // Area
  if (command == "SAS")
  {
    HandleSetAreaSize(parameter1, parameter2);
    return;
  }
  if (command == "SSW")
  {
    HandleSetStepWidth(parameter1, parameter2);
    return;
  }
  if (command == "STM")
  {
    HandleStartMotion(parameter1, parameter2); 
    return;
  }
  if (command == "ABM")
  {
    HandleAbortMotion(); 
    return;
  }
  Error("Invalid Command");  
}
//
//##############################################
//
//----------------------------------------------
//  Segment - Main - CommandLoop
//----------------------------------------------
//
void CommandLoop()
{
  String TxdLine;  
  if (Serial1.available())
  {
    char C = Serial1.read();
    switch (C)
    {
      case '\n': // nothing
        break;
      case '\r': // Command  + Parameter
        if (AnalyseRxdCommandParameter())
        {
          if (0 < FRxdCommand.length())
          {
            // debug Comment();
            // debug TxdLine = "Command[" + FRxdCommand + "]";
            if (0 < FRxdParameter1.length())
            {
              // debug TxdLine += " Parameter1[" + FRxdParameter1 + "]";
            }
            if (0 < FRxdParameter2.length())
            {
              // debug TxdLine += " Parameter2[" + FRxdParameter2 + "]";
            }
            // debug Serial1.print(TxdLine);
            // Command-Action...
            CommandDispatcher(FRxdCommand, FRxdParameter1, FRxdParameter2);
            // Reinit
            FRxdCommand = "";
            FRxdParameter1 = "";
            FRxdParameter2 = "";            
          }
        } else
        {
          Error("Invalid Command");
        }
        Prompt();
        FRxdBuffer = "";
        break;
      default:
        FRxdBuffer += C;
        // Echo:
        Serial1.write(C);
        break;
    }
  }
}
//
//----------------------------------------------
//  Segment - Main - ExecutionLoop
//----------------------------------------------
//
void ExecutionLoop()
{
  if (0 < FStateMotion)
  {
    SetPosition();
//    if (1 == FChange)
//    {
      SetTrigger();
      //FChange = 0;
//    //}
//    else
//    {
//      FChange = 1;
//    }
    //
    FPixelIndexX += FStepWidthX;
    if (FPixelSizeX <= FPixelIndexX)
    {
      FPixelIndexX = 0;
      FPixelIndexY += FStepWidthY;
      if (FPixelSizeY <= FPixelIndexY)
      { // Finish Area scanning
        FPixelIndexY = 0;
        //
        FStateMotion--;
        if (FStateMotion <= 0)
        {
          HandleAbortMotion();
          Prompt();
          return;
        }
      } 
    }
  }
}
//
//##############################################
//
//----------------------------------------------
//  Segment - Setup 
//----------------------------------------------
//
void setup() 
{ // LedSystem
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  int I;
  for (I = 0; I < 6; I++)
  {
    digitalWrite(PIN_LEDSYSTEM, HIGH);
    delay(200);
    digitalWrite(PIN_LEDSYSTEM, LOW);
    delay(200);
  }
  // Serial
  Serial1.begin(BAUDRATE_SERIAL);
  //
  HandleHeader();
  HandleHelp();
  Prompt();  
  FStateMotion = false;  
// NC FInfoPositionTrigger = true;
  //
  pinMode(DAC0, OUTPUT);
  pinMode(DAC1, OUTPUT);
  analogWriteResolution(12);
  analogWrite(DAC0, 0x0000);
  analogWrite(DAC1, 0x0000);
  //
  FStateMotion = 0;
  FRepetitionCount = INIT_REPETITIONCOUNT;
  FPixelSizeX = 4096;
  FPixelSizeY = 4096;
  FPixelIndexX = 0;
  FPixelIndexY = 0;
  FStepWidthX = 128;
  FStepWidthY = 128;
  FDelayMicroSeconds = 0;
  // NC FInfoPositionTrigger = false;
  
}
//
//----------------------------------------------
//  Segment - Setup / Main
//----------------------------------------------
//
void loop() 
{
  CommandLoop();
  ExecutionLoop();
}



