#include "Defines.h"
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Serial.h"
#include "Led.h"
#include "LedLine.h"
#include "Dac.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
// PLI 2000 2000 1000000 3
//
CError LASError;
CProcess LASProcess;
CCommand LASCommand;
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED);
CLedLine LedLine(61, 60, PIN_LEDMOTIONY, PIN_LEDMOTIONX,  // 7..4
                 57, PIN_LEDBUSY, 55, PIN_LEDERROR);      // 3..0
CLed LedLaser(PIN_LEDLASER, LEDLASER_INVERTED);                 
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Dac
//---------------------------------------------------
//
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
CDac DigitalAnalogConverter(true, true);
#endif
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort und CommandPort!
// NC - CSerial SerialDebug(Serial);
// normal CSerial SerialCommand(Serial1);
CSerial SerialCommand(Serial);
// NC - CSerial SerialPrinter(Serial2);
// NC - Serial3
//
//
//---------------------------------------------------
// Segment - Forward-Declaration
//---------------------------------------------------
//

//
//---------------------------------------------------
// Segment - Setup
//---------------------------------------------------
//
void setup() 
{ //-------------------------------------------
  // Device - Led - LedLaser
  //-------------------------------------------
  LedLaser.Open();
  LedLaser.Off();
  //-------------------------------------------
  // Device - Led - LedSystem
  //-------------------------------------------
  LedSystem.Open();
  LedLine.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.Off();
    LedLine.On();
    delay(40);
    LedSystem.On();
    LedLine.Off();
    delay(40);
  }
  LedSystem.Off();
  //-------------------------------------------
  // Device - Dac
  //-------------------------------------------
    DigitalAnalogConverter.Open();
  //-------------------------------------------
  // Device - Serial
  //-------------------------------------------
  // 
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_OFF);
  //-------------------------------------------
  // Device - Command
  //-------------------------------------------
  LASCommand.WriteProgramHeader(SerialCommand);
  LASCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //-------------------------------------------
  // Device - Process 
  //-------------------------------------------
  LASProcess.Open();
  LASProcess.SetState(spWelcome);
}
//
//---------------------------------------------------
// Segment - Loop
//---------------------------------------------------
//
void loop() 
{ 
  LASError.Handle(SerialCommand);    
  LASCommand.Handle(SerialCommand);
  LASProcess.Handle(SerialCommand);  
}




