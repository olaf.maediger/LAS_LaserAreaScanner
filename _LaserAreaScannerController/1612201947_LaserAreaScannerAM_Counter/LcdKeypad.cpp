//
//--------------------------------
//  Library LcdKeypad
//--------------------------------
//
#include "LcdKeypad.h"
//
CLcdKeypad::CLcdKeypad()
{
  FPDisplay = new LiquidCrystal(8, 9, 4, 5, 6, 7);
}

Boolean CLcdKeypad::Open()
{
  FPDisplay->begin(COUNT_COL, COUNT_ROW);
  FPDisplay->clear();
  FPDisplay->display();
  FCursorCol = 0;
  FCursorRow = 0;
  return true;
}

Boolean CLcdKeypad::Close()
{
  FPDisplay->noDisplay();
  return true;
}


void CLcdKeypad::ClearScreen()
{
  FCursorCol = 0;
  FCursorRow = 0;
  FPDisplay->clear();
}

void CLcdKeypad::IncrementCursor()
{
  FCursorCol++;
  if (COUNT_COL <= FCursorCol)
  {
    FCursorCol = 0;
    FCursorRow++;
    if (COUNT_ROW <= FCursorRow)
    {
      FCursorRow = 0;
    }
  }
  FPDisplay->setCursor(FCursorCol, FCursorRow);
}


void CLcdKeypad::WriteText(Byte col, Byte row, PCharacter ptext)
{
  SetCursor(col, row);
  FPDisplay->write(ptext);
}

void CLcdKeypad::WriteCharacter(Character character)
{
  FPDisplay->write(character);
  IncrementCursor();  
}

void CLcdKeypad::SetCursor(Byte col, Byte row)
{
  FCursorCol = col;
  FCursorRow = row;
  FPDisplay->setCursor(FCursorCol, FCursorRow);  
}

void CLcdKeypad::WriteInt16(Byte col, Byte row, Int16 value)
{
  SetCursor(col, row);
  FPDisplay->print(value);
}



//
//EStateLed CLed::GetState()
//{
//  return FState;
//}
//
//void CLed::SetPeriodBlink(long unsigned period)
//{
//  FPeriodBlink = period;
//}
//long unsigned CLed::GetPeriodBlink()
//{
//  return FPeriodBlink;
//}
//long unsigned CLed::GetPeriodBlinkHalf()
//{
//  return FPeriodBlink / 2;
//}
//
//void CLed::SetCountPreset(long unsigned count)
//{
//  FCountPreset = count;
//}
//long unsigned CLed::GetCountPreset()
//{
//  return FCountPreset;
//}
//
//void CLed::SetCountActual(long unsigned count)
//{
//  FCountActual = count;
//}
//long unsigned CLed::GetCountActual()
//{
//  return FCountActual;
//}
//Boolean CLed::CountActualIncrement()
//{
//  if (FCountActual < FCountPreset)
//  {
//    FCountActual++;
//  }
//  // Target reached?
//  return (FCountActual == FCountPreset);
//}
//
//Boolean CLed::On()
//{
//  digitalWrite(FPin, HIGH);
//  FState = slOn;
//  return true;
//}
//
//Boolean CLed::Off()
//{
//  digitalWrite(FPin, LOW);
//  FState = slOff;
//  return true;
//}
//
//Boolean CLed::Toggle()
//{
//  if (slOff == FState)
//  {
//    digitalWrite(FPin, HIGH);
//    FState = slOn;
//    return true;
//  } 
//  if (slOn == FState)
//  {
//    digitalWrite(FPin, LOW);
//    FState = slOff;
//  } 
//  return false;
//}
//
//Boolean CLed::Pulse(UInt32 microseconds)
//{
//  digitalWrite(FPin, HIGH);
//  FState = slOn;
//  delayMicroseconds(microseconds);  
//  digitalWrite(FPin, LOW);
//  FState = slOff;
//}


