//
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include "Arduino.h"
//
//--------------------------------
//  Section - Necessary !!!!!!!!!!
//--------------------------------
// !!!!!!!!!!!!!!!!!!!!
#define ARDUINOMEGA2560
// !!!!!!!!!!!!!!!!!!!! #define ARDUINODUE
//
#define INIT_LOCALECHO true
//
#define INIT_GLOBALERRORCODE ecNone
//
#define INIT_COMMANDSWITCHSLAVE false
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define UInt8 byte
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 unsigned long long int
#define Float float
#define Double double
//
#define true 1
#define false 0
//
#define CARRIAGE_RETURN 0x0D
#define LINE_FEED 0x0A
#define ZERO 0x00
//
#define PROMPT_NEWLINE "\r\n"
#define PROMPT_INPUT ">"
#define PROMPT_ANSWER "#"
//
//-------------------------------------------
//	Global Types - Error
//-------------------------------------------
//
enum EErrorCode
{
  ecNone = (int)0,	
  ecUnknown = (int)1,
  ecInvalidCommand = (int)2,
  ecToManyParameters = (int)3,
  ecMissingTargetParameter = (int)4
};
//
// Enumeration of all Leds
#define LED_SYSTEM 0
//
#define SIZE_TXDBUFFER 64
#define SIZE_RXDBUFFER 64
#define SIZE_RXDPARAMETER 8
#define COUNT_RXDPARAMETERS 3
//
#define PIN_LEDSYSTEM   13
#define PIN_LEDERROR    12
#define PIN_LEDBUSY     11
#define PIN_LEDMOTIONX  10
#define PIN_LEDMOTIONY   9
#define PIN_LEDTRIGGER   8
//
#define PIN_ADC0  54
#define PIN_ADC1  55
#define PIN_ADC2  56
#define PIN_ADC3  57
#define PIN_ADC4  58
#define PIN_ADC5  59
#define PIN_ADC6  60
#define PIN_ADC7  61
#define PIN_ADC8  62
#define PIN_ADC9  63
#define PIN_ADC10 64
#define PIN_ADC11 65
//
#define PIN_DAC0  66
#define PIN_DAC1  67
//
#define PIN_PWM0  2
#define PIN_PWM1  3
#define PIN_PWM2  4 
#define PIN_PWM3  5
#define PIN_PWM4  6 
#define PIN_PWM5  7
#define PIN_PWM6  8
#define PIN_PWM7  9
#define PIN_PWM8  10 
#define PIN_PWM9  11
#define PIN_PWM10 12
#define PIN_PWM11 13
//
#define PIN_ENCODERA 35
#define PIN_ENCODERB 33
#define PIN_SWITCH 31
//
// #define LCD-Pins
// #define KeyPad-Pins
//
#endif 

