//
//--------------------------------
//  Library LcdKeypad
//--------------------------------
//
#ifndef LcdKeypad_h
#define LcdKeypad_h
//
#include "Arduino.h"
#include <LiquidCrystal.h>
#include "Defines.h"
//
//    LiquidCrystal()
//    begin()
//    clear()
//    home()
//    setCursor()
//    write()
//    print()
//    cursor()
//    noCursor()
//    blink()
//    noBlink()
//    display()
//    noDisplay()
//    scrollDisplayLeft()
//    scrollDisplayRight()
//    autoscroll()
//    noAutoscroll()
//    leftToRight()
//    rightToLeft()
//    createChar() 
//
//--------------------------------
//  Section - Constant
//--------------------------------
// 
#define COUNT_COL 16
#define COUNT_ROW 2
//#define INIT_LEDPERIODBLINK ((long unsigned)1000)
//#define INIT_COUNTPRESET ((long unsigned)1)
//#define INIT_COUNTACTUAL ((long unsigned)0)
//
//enum EStateLed
//{
//  slUndefined = -1,
//  slOff = 0,
//  slOn = 1
//};
//
class CLcdKeypad
{
  private:
  LiquidCrystal* FPDisplay;
  int FCursorCol, FCursorRow;
 
  public:
  CLcdKeypad();
  Boolean Open();
  Boolean Close();
  //
  void ClearScreen();
  void IncrementCursor();
  void SetCursor(Byte col, Byte row);
  //
  void WriteText(Byte col, Byte row, PCharacter ptext);
  void WriteCharacter(Character character);
  void WriteInt16(Byte col, Byte row, Int16 value);
//  EStateLed GetState();
//  Boolean On();
//  Boolean Off();
//  Boolean Toggle();
//  Boolean Pulse(UInt32 microseconds);
//  void SetPeriodBlink(long unsigned period);
//  long unsigned GetPeriodBlink();
//  long unsigned GetPeriodBlinkHalf();
//  void SetCountPreset(long unsigned count);
//  long unsigned GetCountPreset();
//  void SetCountActual(long unsigned count);
//  long unsigned GetCountActual();
//  Boolean CountActualIncrement();
};
//
#endif
