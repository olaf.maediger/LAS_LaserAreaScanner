#include "Dispatcher.h"
//
//-------------------------------------------
//  Segment - Global Variable
//-------------------------------------------
//  
extern enum EErrorCode GlobalErrorCode;
extern enum EStateAutomation StateAutomation;
//
//-------------------------------------------
//  Segment - Global Class
//-------------------------------------------
//  
extern CLed LedSystem;
extern CCommand Command;
extern Boolean CommandSwitchSlave;
extern CSerial SerialSlave;
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("-----------------------------------------");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Project:   SAM - SerialArduinoMega    -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Version:   01V02                      -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Hardware:  ArduinoMega2560            -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Date:      161214                     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Time:      1526                       -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Author:    OM                         -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Port:      SerialPC (SerialUSB)       -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Parameter: 115200, N, 8, 1            -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("-----------------------------------------");
  serial.WriteNewLine();	
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Software-Version SAM01V02");
  serial.WriteNewLine();
}

void WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Hardware-Version ArduinoMega2560");
  serial.WriteNewLine();
}

void WriteHelp(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Help (Common):");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" H   : This Help");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GPH : Get Program Header");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GSV : Get Software-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GHV : Get Hardware-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" CSS : Toggle Command Switch to Slave");
  // NC serial.WriteText(" CSS <s>: Command Switch to Slave [0, 1]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Led):");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GLD <i> : Get State Led with <i>ndex[0..4]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" SLD <i> : Set Led with <i>ndex[0..4]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" CLD <i> : Clear Led with <i>ndex[0..4]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Automation):");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" TLS <n> <p>: Toggle LedSystem <n>times with <p>eriod{ms}");
  serial.WriteNewLine();
  serial.WriteAnswer();
//  serial.WriteText(" STT <n> <r> : Start Trigger <n>Pulses with <r>epetitions");
//  serial.WriteNewLine();
//  serial.WriteAnswer();    
//  serial.WriteText(" ABM : Abort Motion");
  //
  serial.WriteNewLine();
}
//
//#########################################################
//  Segment - Execution - Common
//#########################################################
//
void ExecuteGetHelp(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHelp(serial);
  serial.WritePrompt();
}

void ExecuteGetProgramHeader(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void ExecuteGetSoftwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void ExecuteGetHardwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}

void ExecuteCommandSwitchSlave(CSerial &serial)
{  // Analyse parameters:
  CommandSwitchSlave = !CommandSwitchSlave;
  // NC CommandSwitchSlave = atoi(Command.GetPRxdParameters(0));
  // Execute -
  // Answer:
  sprintf(Command.GetTxdBuffer(), "# %s %u", 
          Command.GetPRxdCommand(), CommandSwitchSlave);
  serial.WriteLine(Command.GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Led
//#########################################################
//
void ExecuteGetLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(Command.GetPRxdParameters(0));
  int State;
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      State = LedSystem.GetState();
      break;
  }
  // Answer:
  sprintf(Command.GetTxdBuffer(), "# %s %s %i", 
          Command.GetPRxdCommand(), Command.GetPRxdParameters(0), State);
  serial.WriteLine(Command.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteClearLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(Command.GetPRxdParameters(0));
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.Off();
      break;
  }
  // Answer:
  sprintf(Command.GetTxdBuffer(), "# %s %s", 
          Command.GetPRxdCommand(), Command.GetPRxdParameters(0));
  serial.WriteLine(Command.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSetLed(CSerial &serial)
{	// Analyse parameters:
  int Index = atoi(Command.GetPRxdParameters(0));
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.On();
      break;
  }
  // Answer:
  sprintf(Command.GetTxdBuffer(), "# %s %s", 
          Command.GetPRxdCommand(), Command.GetPRxdParameters(0));
  serial.WriteLine(Command.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteToggleLedSystem(CSerial &serial)
{  // Analyse parameters:
  long unsigned CountPreset = atoi(Command.GetPRxdParameters(0));
  LedSystem.SetCountPreset(CountPreset);
  unsigned long PeriodBlink = atol(Command.GetPRxdParameters(1));
  LedSystem.SetPeriodBlink(PeriodBlink);
  // Execute -> Automation
  LedSystem.Off();
  LedSystem.SetCountActual(0);
  StateAutomation = saBlink;
  // Answer:
  sprintf(Command.GetTxdBuffer(), "# %s %lu %lu", 
          Command.GetPRxdCommand(), LedSystem.GetCountPreset(), LedSystem.GetPeriodBlink());
  serial.WriteLine(Command.GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Helper
//#########################################################
//

//
//#########################################################
//  Segment - Execution - Automation
//#########################################################
//
//bool AbortMotion()
//{
//  StateAutomation = saIdle;
//  LedMotionY.Off();
//  LedMotionX.Off();
//  LedBusy.Off();
//  return true;
//}
//
//void ExecuteAbortMotion(CSerial &serial)
//{ // Analyse parameters -
//  // Execute:
//  AbortMotion();
//  // Answer:
//  sprintf(SerialCommand.GetTxdBuffer(), "# ABM");
//  serial.WriteLine(SerialCommand.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//Boolean StartMotion()
//{
//  if (0 < CountRepetition)
//  {
//    LedMotionY.On();
//    LedMotionX.On();
//    LedBusy.On();
//    XPositionActual = XPositionMinimum;
//    YPositionActual = YPositionMinimum;
//    StateAutomation = saMotionRowColLeft;
//    return true;
//  }
//  return false;
//}
//
//void ExecuteStartMotion(CSerial &serial)
//{ // Analyse parameters:
//  DelayPulse = abs(atoi(SerialCommand.GetPRxdParameters(0)));
//  CountRepetition = abs(atoi(SerialCommand.GetPRxdParameters(1)));
//  // Execute:
//  StartMotion();
//  // Answer:
//  sprintf(Command.GetTxdBuffer(), "# %s %lu %lu", 
//          Command.GetPRxdCommand(), DelayPulse, CountRepetition);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//#########################################################
//  Segment - Handler - Error
//#########################################################
//
void HandleError(CSerial &serial)
{
  if (ecNone != GlobalErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(GlobalErrorCode, Command.GetTxdBuffer());	
    serial.WriteText(Command.GetTxdBuffer());
    serial.WriteText("]: ");
    switch (GlobalErrorCode)
    {
      case ecNone:
        serial.WriteText("No Error");
        break;
      case ecUnknown:
        serial.WriteText("Unknown");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
      case ecToManyParameters:
        serial.WriteText("To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.WriteText("Missing Target Parameter");
        break;              
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    GlobalErrorCode = ecNone;
  }
}

void ExecuteTransferCommandToTarget(CSerial &serialtarget)
{ // Analyse parameters -
  // Execute:
  switch (Command.GetRxdParameterCount())
  {
    case 0:
      serialtarget.WriteLine(Command.GetPRxdCommand());
      break;
    case 1:
      serialtarget.WriteText(Command.GetPRxdCommand());
      serialtarget.WriteCharacter(' ');
      serialtarget.WriteLine(Command.GetPRxdParameters(0));      
      break;
    case 2:
      serialtarget.WriteText(Command.GetPRxdCommand());
      serialtarget.WriteCharacter(' ');
      serialtarget.WriteText(Command.GetPRxdParameters(0));
      serialtarget.WriteCharacter(' ');
      serialtarget.WriteLine(Command.GetPRxdParameters(1));      
      break;
    case 3:
      serialtarget.WriteText(Command.GetPRxdCommand());
      serialtarget.WriteCharacter(' ');
      serialtarget.WriteText(Command.GetPRxdParameters(0));
      serialtarget.WriteCharacter(' ');
      serialtarget.WriteText(Command.GetPRxdParameters(1));
      serialtarget.WriteCharacter(' ');
      serialtarget.WriteLine(Command.GetPRxdParameters(2));      
      break;
  }
  // Answer -
}

Boolean ExecuteRxdCommand(CSerial &serialcommand, CSerial &serialslave)
{ 
  if (!strcmp("CSS", Command.GetPRxdCommand()))
  {
    // NOT OK serialslave.SetLocalEcho(!CommandSwitchSlave);
    ExecuteCommandSwitchSlave(serialcommand);
    return true;
  } else 
  if (CommandSwitchSlave)
  {
    ExecuteTransferCommandToTarget(serialslave);
    return true;
  } else
  if (!strcmp("H", Command.GetPRxdCommand()))
  { 
    ExecuteGetHelp(serialcommand);
    return true;
  } else 
  if (!strcmp("GPH", Command.GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serialcommand);
    return true;
  } else 
  if (!strcmp("GSV", Command.GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serialcommand);
    return true;
  } else 
  if (!strcmp("GHV", Command.GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serialcommand);
    return true;
  } else 
  // ----------------------------------
  // Led
  // ----------------------------------
  if (!strcmp("GLD", Command.GetPRxdCommand()))
  {
    ExecuteGetLed(serialcommand);
    return true;
  } else 
  if (!strcmp("SLD", Command.GetPRxdCommand()))
  {
    ExecuteSetLed(serialcommand);
    return true;
  } else   
  if (!strcmp("CLD", Command.GetPRxdCommand()))
  {
    ExecuteClearLed(serialcommand);
    return true;
  } else   
  if (!strcmp("TLS", Command.GetPRxdCommand()))
  {
    ExecuteToggleLedSystem(serialcommand);
    return true;
  } else   
  //
//  // ----------------------------------
//  // Automation
//  // ----------------------------------
//  if (!strcmp("STM", Command.GetPRxdCommand()))
//  {
//    ExecuteStartMotion(serial);
//    return true;
//  } else   
//  if (!strcmp("ABM", Command.GetPRxdCommand()))
//  {
//    ExecuteAbortMotion(serial);
//    return true;
//  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    GlobalErrorCode = ecInvalidCommand;
  }
  return false;  
}

void HandleCommand(CSerial &serialcommand, CSerial &serialslave)
{
  if (Command.AnalyseRxdCommand(serialcommand))
  {
    ExecuteRxdCommand(serialcommand, serialslave);
  }  
}

void DirectTransferResponse(CSerial &serialsource, CSerial &serialtarget) // SerialSlave -> SerialPC
{
  while (0 < serialsource.GetRxdByteCount())
  {
    Byte B = serialsource.ReadCharacter();
    switch (B)
    {
      case 0x0A:
        serialtarget.WriteCharacter(0x0A);
        break;
      case 0x0D:
        serialtarget.WriteCharacter(0x0D);
        break;
      default:
        serialtarget.WriteCharacter(B);
        break;
    }
  }
}



