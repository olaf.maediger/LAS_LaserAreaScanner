//###################################################
// Project:  LASAM - LaserAreaScannerArduinoMega
// Hardware: Arduno Mega 2560
// Function now: Serial-Command-Dispatcher
// - ArduinoMega gives Commands to ArduinoDue and gets Responses / Events
// Function later: User-Input-Output-Processor
// Parts:
// - ArduinoMega2560 16MHz 5V0 8bit Atmel ATmega2560
//   - Serial1 : CommandChannel to PC (here: ArduinoDue 3V3)
//     - Levelshifter 5V0 - 3V3: Txd, Rxd
//   - Serial2 : Usb-FtdiFT232RL-Serial for Debugging
//   - LedSystem : System-State debugging
//   - LCD Keypad Shield Sainsmart
//     - 16x2 LCDisplay
//     - 6 Keys (Select, Left, Up, Down, Right, Reset)
// later - RotaryEncoder (Hardware Rotation->ABPulses)
// later   - adapted to CRotaryDecoder (Software ABPulses->Position)
//###################################################
//
#include "Defines.h"
#include "Helper.h"
#include "Utilities.h"
#include "Led.h"
#include "Serial.h"
#include "Command.h"
#include "Dispatcher.h"
#include "Automation.h"
#include "LcdKeypad.h"
// later #include "RotaryDecoder.h"
// later #include ".h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables
//---------------------------------------------------
//
enum EErrorCode GlobalErrorCode;
EStateAutomation StateAutomation;
CCommand Command;
Boolean CommandSwitchSlave;

int Counter = 0;
//
//---------------------------------------------------
// Segment - Global Variables - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM);
//
//---------------------------------------------------
// Segment - Global Variables - LcdKeypad
//---------------------------------------------------
//
CLcdKeypad LcdKeypad;
//
//---------------------------------------------------
// Segment - Global Variables - Serial
//---------------------------------------------------
//
CSerial SerialPC(&Serial1);
CSerial SerialSlave(&Serial2);
//#ifdef ARDUINODUE    
////CSerial SerialUsb(&SerialUSB);
//#endif   
//
void setup() 
{ 
  GlobalErrorCode = INIT_GLOBALERRORCODE;
  StateAutomation = INIT_STATEAUTOMATION;
  CommandSwitchSlave = INIT_COMMANDSWITCHSLAVE;
  //
  LedSystem.Open();
  LcdKeypad.Open();
  //
  SerialPC.Open(115200);
  SerialSlave.Open(115200);
  //
  WriteProgramHeader(SerialPC);
  WriteHelp(SerialPC);
  SerialPC.WritePrompt();
  //
  LcdKeypad.ClearScreen();
  LcdKeypad.WriteText(0, 0, "LaserAreaScanner");
  LcdKeypad.WriteText(0, 1, "Counter:");
}

void loop() 
{
  HandleError(SerialPC);  
  HandleCommand(SerialPC, SerialSlave);
  DirectTransferResponse(SerialSlave, SerialPC);
  HandleAutomation(SerialPC);
  //
  LcdKeypad.WriteInt16(9, 1, Counter);
  Counter++;
  delay(100);
//  //
//  LedSystem.On();
//  delay(400);
//  LedSystem.Off();
//  delay(400);
//  if (0 < SerialPC.GetRxdByteCount())
//  {
//    Character C = SerialPC.ReadCharacter();
//    SerialPC.WriteCharacter(C);
//  }
}



