#ifndef Automation_h
#define Automation_h
//
#include "Defines.h"
#include "Helper.h"
#include "Utilities.h"
#include "Serial.h"
#include "Led.h"
//
enum EStateAutomation
{
  saIdle = 0,
  saBlink = 1,
  saBusy = 2
};
//
#define INIT_STATEAUTOMATION saIdle
//
void HandleAutomation(CSerial &serial);
//
#endif // Automation_h
