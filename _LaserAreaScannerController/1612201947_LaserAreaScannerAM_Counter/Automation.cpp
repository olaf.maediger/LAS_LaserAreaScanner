#include "Automation.h"

//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
extern enum EErrorCode GlobalErrorCode;
extern EStateAutomation StateAutomation;
//
extern CLed LedSystem;
//
long unsigned TimeSystemStamp;
//
//#########################################################
//  Segment - Automation
//#########################################################
//
void HandleAutomationIdle(CSerial &serial)
{  
//  if (RotaryDecoder.Refresh())
//  {
//    if (RotaryDecoder.GetStateSwitch())
//    {
//      RotaryDecoder.SetPositionActual(0);
//    }
//    serial.WriteNewLine();
//    serial.WriteText("# Switch<"); 
//    serial.WriteInt32(RotaryDecoder.GetStateSwitch());
//    serial.WriteText("> Position<"); 
//    serial.WriteInt32(RotaryDecoder.GetPositionActual());
//    serial.WriteText(">");
//  }
//  // delayMicroseconds(1000000);
}

void HandleAutomationBlink(CSerial &serial)
{
  long unsigned TimeSystemActual = millis();
  if (LedSystem.GetPeriodBlinkHalf() < (TimeSystemActual - TimeSystemStamp))
  {   
    TimeSystemStamp = TimeSystemActual;
    LedSystem.Toggle();
    if (slOff == LedSystem.GetState())
    {
      if (LedSystem.CountActualIncrement())
      {
        StateAutomation = saIdle;
      }
    }
  }
}

void HandleAutomationBusy(CSerial &serial)
{
//  LedMotionX.On();
//  LedMotionY.On();
//  //
//  SetPosition();
//  TriggerLaserPulses();
//  //
//  XPositionActual += XPositionDelta;
//  if (XPositionMaximum < XPositionActual)
//  {
//    XPositionActual = XPositionMinimum;
//    YPositionActual += YPositionDelta;
//    if (YPositionMaximum < YPositionActual)
//    { // Finish Area scanning
//      XPositionActual = XPositionMinimum;
//      YPositionActual = YPositionMinimum;
//      CountRepetition--;
//      LedBusy.Toggle();
//      if (0 == CountRepetition)
//      { // ends...
//        // SetPosition();
//        ExecuteAbortMotion(serial);
//        return;
//      }
//    } 
//  } 
}

void HandleAutomation(CSerial &serial)
{
  switch (StateAutomation)
  {
    case saIdle:
      HandleAutomationIdle(serial);
      break;
    case saBlink:
      HandleAutomationBlink(serial);
      break;
    case saBusy:
      HandleAutomationBusy(serial);
      break;
  }
}
