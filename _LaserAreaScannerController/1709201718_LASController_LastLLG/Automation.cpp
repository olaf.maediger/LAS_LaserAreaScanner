#include "Automation.h"
//
extern CLed LedLaser;
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  FTimeStart = millis();
  FTimeMarker = FTimeStart;
  FCountEvent = 0;
}

void CAutomation::SetState(EStateAutomation state)
{
  if (state != FState)
  {
    FState = state;
  }
}

void CAutomation::SetTimeMarker()
{
  FTimeMarker = millis();
}



Boolean CAutomation::Open()
{
  FState = saIdle;
  return true;
}

Boolean CAutomation::Close()
{
  FState = saUndefined;
  return true;
}


void CAutomation::HandleIdle(CSerial &serial)
{
  // do nothing
}

void CAutomation::HandleLedLaserPulseBegin(CSerial &serial)
{
  FTimeMarker = millis();  
  LedLaser.On();
  FCountEvent = 1;
  SetState(saLedLaserPulseBusy);
}
void CAutomation::HandleLedLaserPulseBusy(CSerial &serial)
{  
  UInt32 PeriodHalf = LedLaser.GetPeriod() / 2;  
  if (PeriodHalf <= FTimeStamp - FTimeMarker)
  {
    FTimeMarker = FTimeStamp;       
    if (slOff == LedLaser.GetState())
    {
      LedLaser.On();
    }
    else
      if (slOn == LedLaser.GetState())
      {
        LedLaser.Off();
        if (FCountEvent < LedLaser.GetPulseCount())
        {
          FCountEvent++;      
        }
        else
          {
            SetState(saLedLaserPulseEnd);
          }
      }
  }
}
void CAutomation::HandleLedLaserPulseEnd(CSerial &serial)
{  
  SetState(saIdle);  
}

void CAutomation::Handle(CSerial &serial)
{
  FTimeStamp = millis();
  switch (FState)
  {
    case saIdle:
      HandleIdle(serial);
      break;
    case saLedLaserPulseBegin:
      HandleLedLaserPulseBegin(serial);
      break;
    case saLedLaserPulseBusy:
      HandleLedLaserPulseBusy(serial);
      break;
    case saLedLaserPulseEnd:
      HandleLedLaserPulseEnd(serial);
      break;
  }  
}
//      
////    case saMotionRowColLeft:
////      HandleAutomationMotionRowColLeft(serial);
////      break;
////    case saMotionRowColBoth:
////      break;
////    case saTriggerRowColLeft:
////      HandleAutomationTriggerRowColLeft(serial);
////      break;
////    case saTriggerRowColBoth:
////      break;
//  }

