#ifndef Automation_h
#define Automation_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
#include "Led.h"
//
//
enum EStateAutomation
{
  saUndefined = -1,
  saIdle = 0,
  // LedLaser
  saLedLaserPulseBegin = 1,
  saLedLaserPulseBusy = 2,
  saLedLaserPulseEnd = 3
};
  
//  saMotionRowColLeft = 1,
//  saMotionRowColBoth = 2,
//  saTriggerRowColLeft = 3,
//  saTriggerRowColBoth = 4
//
class CAutomation
{
  private:
  EStateAutomation FState;
  UInt32 FTimeStart;
  UInt32 FTimeMarker;
  UInt32 FTimeStamp;
  UInt32 FCountEvent;
  //
  public:
  CAutomation();
  //
  void SetState(EStateAutomation state);
  void SetTimeMarker();
  //
  Boolean Open();
  Boolean Close();
  //
  void HandleIdle(CSerial &serial);
  void HandleLedLaserPulseBegin(CSerial &serial);
  void HandleLedLaserPulseBusy(CSerial &serial);
  void HandleLedLaserPulseEnd(CSerial &serial);
  void Handle(CSerial &serial);
};
//
#endif // Automation_h
