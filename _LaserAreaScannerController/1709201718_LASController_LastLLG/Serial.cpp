#include "Serial.h"
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
void CSerialBase::WriteNewLine()
{
  WriteText(PROMPT_NEWLINE);  
}
  
void CSerialBase::WritePrompt()
{  
//  long unsigned SystemTime = millis();
//  unsigned Millis = SystemTime % 1000;
//  long unsigned RTS = SystemTime / 1000;
//  unsigned Hours = RTS / 3600;
//  RTS = RTS - 3600 * Hours;
//  unsigned Minutes = RTS / 60;
//  unsigned Seconds = RTS - 60 * Minutes;
//  char Buffer[16];
//  sprintf(Buffer, "%2.2d:%2.2d:%2.2d.%3.3d", Hours, Minutes, Seconds, Millis); 
//  WriteText(Buffer);
  WriteText(BuildTime(millis()));
  WriteText(PROMPT_INPUT);
}
  
void CSerialBase::WriteAnswer()
{
  WriteText(PROMPT_ANSWER);
}

String CSerialBase::BuildTime(long unsigned milliseconds)
{
  long unsigned SystemTime = milliseconds;
  unsigned Millis = SystemTime % 1000;
  long unsigned RTS = SystemTime / 1000;
  unsigned Hours = RTS / 3600;
  RTS = RTS - 3600 * Hours;
  unsigned Minutes = RTS / 60;
  unsigned Seconds = RTS - 60 * Minutes;
  char Buffer[16];
  sprintf(Buffer, "%2.2d:%2.2d:%2.2d.%3.3d", Hours, Minutes, Seconds, Millis); 
  return Buffer;
}
//
//----------------------------------------------------
//  Segment - CSerialH 
//----------------------------------------------------
//
#if ((defined PROCESSOR_STM32F103C8) || (defined PROCESSOR_TEENSY36) || (defined PROCESSOR_ARDUINODUE))
bool CSerialH::Open(int baudrate, bool rxdecho)
{
  FRxdEcho = rxdecho;
  FPSerial->begin(baudrate);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialH::Close()
{
  FRxdEcho = false;
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}
  
void CSerialH::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialH::WriteText(char *text)
{
  FPSerial->write(text);
}  
void CSerialH::WriteText(String text)
{
  FPSerial->write(text.c_str());
}  

void CSerialH::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  
void CSerialH::WriteLine(String text)
{
  FPSerial->write(text.c_str());
  WriteNewLine();
}  

void CSerialH::WriteTime()
{
  FPSerial->print(millis());
}

int CSerialH::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialH::ReadCharacter()
{
  char C = (char)FPSerial->read();
  if (FRxdEcho && (isalnum(C) || (' ' == C)))
  {
    FPSerial->write(C);
  }
}
#endif  
//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//

#if ((defined PROCESSOR_STM32F103C8) || (defined PROCESSOR_TEENSY36) || (defined PROCESSOR_ARDUINODUE))
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif

bool CSerial::Open(int baudrate, bool rxdecho)
{
  return FPSerial->Open(baudrate, rxdecho);
}

bool CSerial::Close()
{
  return FPSerial->Close();
}

void CSerial::SetRxdEcho(bool rxdecho)
{
  FPSerial->SetRxdEcho(rxdecho);
}

void CSerial::WriteCharacter(char character)
{
  FPSerial->WriteCharacter(character);
}

void CSerial::WriteText(char *text)
{
  FPSerial->WriteText(text);
}
void CSerial::WriteText(String text)
{
  FPSerial->WriteText(text);
}

void CSerial::WriteLine(char *text)
{
  FPSerial->WriteLine(text);
}
void CSerial::WriteLine(String text)
{
  FPSerial->WriteLine(text);
}

void CSerial::WriteNewLine()
{
  FPSerial->WriteNewLine();
}

void CSerial::WriteAnswer()
{
  FPSerial->WriteAnswer();
}

void CSerial::WritePrompt()
{
  FPSerial->WritePrompt();
}

void CSerial::WriteTime()
{
  FPSerial->WriteTime();
}

int CSerial::GetRxdByteCount()
{
  FPSerial->GetRxdByteCount();
}

char CSerial::ReadCharacter()
{
  char C = (char)FPSerial->ReadCharacter();
  return C;
}




















