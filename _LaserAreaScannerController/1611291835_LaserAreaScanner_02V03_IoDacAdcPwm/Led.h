//
//--------------------------------
//  Library Led
//--------------------------------
//
#ifndef Led_h
#define Led_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
enum EStateLed
{
  slUndefined = -1,
  slOff = 0,
  slOn = 1
};
//
class CLed
{
  private:
  int FPin;
  EStateLed FState;
  
  public:
  CLed(int pin);
  Boolean Open();
  Boolean Close();
  EStateLed GetState();
  Boolean On();
  Boolean Off();
  Boolean Toggle();
};
//
#endif
