#ifndef Utilities_h
#define Utilities_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
//
void WriteProgramHeader(CSerial &serialcommand);
void WriteHelp(CSerial &serialcommand);
//  Handler - Error
void HandleError(CSerial &serial);
//  Handler - SerialPC Communication
void HandleSerialCommands(CSerial &serialcommand);//, CSerial &serialtarget);
//
#endif
