#include "Utilities.h"
#include "Led.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
//
//
//-------------------------------------------
//  Global Variables 
//-------------------------------------------
//	
enum EErrorCode ErrorCode;
//
Character TxdBuffer[SIZE_TXDBUFFER];
Character RxdBuffer[SIZE_RXDBUFFER];
Int16 RxdBufferIndex = 0;
Character RxdCommandLine[SIZE_RXDBUFFER];
PCharacter PRxdCommand;
PCharacter PRxdParameters[COUNT_RXDPARAMETERS];
Int16 RxdParameterCount = 0;
//
extern CLed LedSystem;
extern CLed LedError;
extern CLed LedBusy;
extern CLed LedMotionX;
extern CLed LedMotionY;
extern CLed LedTrigger;
//
extern CAdc AnalogDigitalConverter;
//
extern CDac DigitalAnalogConverter;
//
extern CPwm PulseWidthModulator;
//
//--------------------------------------------------
void ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    RxdBuffer[CI] = 0x00;
  }
  RxdBufferIndex = 0;
}

void ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    TxdBuffer[CI] = 0x00;
  }
}

void ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    RxdCommandLine[CI] = 0x00;
  }
}


void WriteProgramHeader(CSerial &serial)
{  
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
  //
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("---------------------------------------");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Project:   LAS - LaserAreaScanner   -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Version:   02V03                    -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Hardware:  ArduinoDue               -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Date:      161129                   -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Time:      1753                     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Author:    OM                       -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Port:      SerialPC (SerialUSB)     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Parameter: 115200, N, 8, 1          -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("---------------------------------------");
  serial.WriteNewLine();	
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Software-Version LAS02V03");
  serial.WriteNewLine();
}

void WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Hardware-Version ArduinoDue3");
  serial.WriteNewLine();
}

void WriteHelp(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Help (Common):");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" H   : This Help");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GPH <fdl> : Get Program Header");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GSV <fdl> : Get Software-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GHV <fdl> : Get Hardware-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Led):");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GLD <i> : Get State of Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" SLD <i> : Set Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" CLD <i> : Clear Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" TLD <i> <n> <d>: Toggle Led with Index<0..3> <n>-times with <d>-delay");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Dac):");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" GDV <i> : Get Value from DAConverter with Index<0..1>");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SDV <i> <v>: Set Value<0..4095> to DAConverter with Index<0..1>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Adc):");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" GAV <i> : Get Value from ADConverter with Index<0..11>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Pwm):");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" GPV <i> : Get Value from PWModulator with Index<0..11>");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SPV <i> <v>: Set Value<0..4095> to DAConverter with Index<0..1>");
  serial.WriteNewLine();
}


//
//-------------------------------------------
//	*** Common ***
//-------------------------------------------
//
void ExecuteGetHelp(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHelp(serial);
  serial.WritePrompt();
}

void ExecuteGetProgramHeader(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void ExecuteGetSoftwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void ExecuteGetHardwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}
//
//-------------------------------------------
//	*** Led ***
//-------------------------------------------
//
/* -> Fdl...
void GetLed(Boolean response, UInt8 ledindex)
{ // Action
  Boolean State = false; // Inverted!
  switch (ledindex)
  {
    case LEDINDEX_BUSY:
			State = LEDBUSY_State();
			break;
		case LEDINDEX_ERROR:
			State = LEDERROR_State();
			break;
	}
	// Response
	if (response)
	{
		WriteAnswer();
		UARTBASE_CPutString(" GLD ");
		Int16ToAsciiDecimal(ledindex, TXDBuffer);
		UARTBASE_PutString(TXDBuffer);
		UARTBASE_CPutString(" ");
		// State inverted!
		if (0 < State)
		{ 
			UARTBASE_CPutString("0");
		} else
		{
			UARTBASE_CPutString("1");
		}
		WriteNewLine();
		WritePrompt();
	}
}

void ClearLed(Boolean response,	
			  UInt8 ledindex)
{	// Action
	switch (ledindex)
	{
		case LEDINDEX_BUSY:
			LEDBUSY_Clear();		
			break;
		case LEDINDEX_ERROR:
			LEDERROR_Clear();		
			break;
	}
	// Response
	if (response)
	{
		WriteAnswer();
		UARTBASE_CPutString(" CLD ");
		Int16ToAsciiDecimal(ledindex, TXDBuffer);
		UARTBASE_PutString(TXDBuffer);
		WriteNewLine();
		WritePrompt();
	}
}

void SetLed(Boolean response,	
		  	UInt8 ledindex)
{	// Action
	switch (ledindex)
	{
		case LEDINDEX_BUSY:
			LEDBUSY_Set();		
			break;
		case LEDINDEX_ERROR:
			LEDERROR_Set();		
			break;
	}
	// Response
	if (response)
	{
		WriteAnswer();
		UARTBASE_CPutString(" SLD ");
		Int16ToAsciiDecimal(ledindex, TXDBuffer);
		UARTBASE_PutString(TXDBuffer);
		WriteNewLine();
		WritePrompt();
	}
}
*/
//
//----------------------------------------------------------
//  Segment - Execute - Led
//----------------------------------------------------------
//
void ExecuteGetLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(PRxdParameters[0]);
  int State;
  // Execute:
  switch (Index)
  {
    case INDEX_LEDSYSTEM:
      State = LedSystem.GetState();
      break;
    case INDEX_LEDERROR:
      State = LedError.GetState();
      break;
    case INDEX_LEDBUSY:
      State = LedBusy.GetState();
      break;
    case INDEX_LEDMOTIONX:
      State = LedMotionX.GetState();
      break;
    case INDEX_LEDMOTIONY:
      State = LedMotionY.GetState();
      break;
    case INDEX_LEDTRIGGER:
      State = LedTrigger.GetState();
      break;
  }
  // Answer:
  sprintf(TxdBuffer, "# %s %s %i", PRxdCommand, PRxdParameters[0], State);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}

void ExecuteClearLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(PRxdParameters[0]);
  // Execute:
  switch (Index)
  {
    case INDEX_LEDSYSTEM:
      LedSystem.Off();
      break;
    case INDEX_LEDERROR:
      LedError.Off();
      break;
    case INDEX_LEDBUSY:
      LedBusy.Off();
      break;
    case INDEX_LEDMOTIONX:
      LedMotionX.Off();
      break;
    case INDEX_LEDMOTIONY:
      LedMotionY.Off();
      break;
    case INDEX_LEDTRIGGER:
      LedTrigger.Off();
      break;
  }
  // Answer:
  sprintf(TxdBuffer, "# %s %s", PRxdCommand, PRxdParameters[0]);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}

void ExecuteSetLed(CSerial &serial)
{	// Analyse parameters:
  int Index = atoi(PRxdParameters[0]);
  // Execute:
  switch (Index)
  {
    case INDEX_LEDSYSTEM:
      LedSystem.On();
      break;
    case INDEX_LEDERROR:
      LedError.On();
      break;
    case INDEX_LEDBUSY:
      LedBusy.On();
      break;
    case INDEX_LEDMOTIONX:
      LedMotionX.On();
      break;
    case INDEX_LEDMOTIONY:
      LedMotionY.On();
      break;
    case INDEX_LEDTRIGGER:
      LedTrigger.On();
      break;
  }
  // Answer:
  sprintf(TxdBuffer, "# %s %s", PRxdCommand, PRxdParameters[0]);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}

void ExecuteToggleLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(PRxdParameters[0]);
  int Delay = atoi(PRxdParameters[1]);
  int Count = atoi(PRxdParameters[2]);
  // Answer:
  sprintf(TxdBuffer, "# %s %s", PRxdCommand, PRxdParameters[0]);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
  // Execute:
  int I;
  for (I = 0; I < Count - 1; I++)
  {
    switch (Index)
    {
      case INDEX_LEDSYSTEM:
        LedSystem.Toggle();
        delay(Delay);
        break;
      case INDEX_LEDERROR:
        LedError.Toggle();
        delay(Delay);
        break;
      case INDEX_LEDBUSY:
        LedBusy.Toggle();
        delay(Delay);
        break;
      case INDEX_LEDMOTIONX:
        LedMotionX.Toggle();
        delay(Delay);
        break;
      case INDEX_LEDMOTIONY:
        LedMotionY.Toggle();
        delay(Delay);
        break;
      case INDEX_LEDTRIGGER:
        LedTrigger.Toggle();
        delay(Delay);
        break;
    }
  }
  switch (Index)
  {
    case INDEX_LEDSYSTEM:
      LedSystem.Toggle();
      break;
    case INDEX_LEDERROR:
      LedError.Toggle();
      break;
    case INDEX_LEDBUSY:
      LedBusy.Toggle();
      break;
    case INDEX_LEDMOTIONX:
      LedMotionX.Toggle();
      break;
    case INDEX_LEDMOTIONY:
      LedMotionY.Toggle();
      break;
    case INDEX_LEDTRIGGER:
      LedTrigger.Toggle();
      break;
  }
}
//
//----------------------------------------------------------
//  Segment - Execute - Adc
//----------------------------------------------------------
//
void ExecuteGetAdcValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(PRxdParameters[0]);
  // Execute:
  UInt16 Value = AnalogDigitalConverter.GetValue(Channel);
  // Answer:
  sprintf(TxdBuffer, "# %s %u %u", PRxdCommand, Channel, Value);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}
//
//----------------------------------------------------------
//  Segment - Execute - Dac
//----------------------------------------------------------
//
void ExecuteGetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(PRxdParameters[0]);
  // Execute:
  UInt16 Value = DigitalAnalogConverter.GetValue(Channel);
  // Answer:
  sprintf(TxdBuffer, "# %s %u %u", PRxdCommand, Channel, Value);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}

void ExecuteSetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(PRxdParameters[0]);
  UInt16 Value = atoi(PRxdParameters[1]);
  // Execute:
  DigitalAnalogConverter.SetValue(Channel, Value);
  Value = DigitalAnalogConverter.GetValue(Channel);
  // Answer:
  sprintf(TxdBuffer, "# %s %u %u", PRxdCommand, Channel, Value);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}
//
//----------------------------------------------------------
//  Segment - Execute - Pwm
//----------------------------------------------------------
//
void ExecuteGetPwmValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(PRxdParameters[0]);
  // Execute:
  UInt16 Value = PulseWidthModulator.GetValue(Channel);
  // Answer:
  sprintf(TxdBuffer, "# %s %u %u", PRxdCommand, Channel, Value);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}

void ExecuteSetPwmValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(PRxdParameters[0]);
  UInt16 Value = atoi(PRxdParameters[1]);
  // Execute:
  PulseWidthModulator.SetValue(Channel, Value);
  Value = PulseWidthModulator.GetValue(Channel);
  // Answer:
  sprintf(TxdBuffer, "# %s %u %u", PRxdCommand, Channel, Value);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}
//
//
//----------------------------------------------
//  Segment - Handler - Error
//----------------------------------------------
//
void HandleError(CSerial &serial)
{
  if (ecNone != ErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(ErrorCode, TxdBuffer);	
    serial.WriteText(TxdBuffer);
    serial.WriteText("]: ");
    switch (ErrorCode)
    {
      case ecNone:
        serial.WriteText("No Error");
        break;
      case ecUnknown:
        serial.WriteText("Unknown");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
      case ecToManyParameters:
        serial.WriteText("To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.WriteText("Missing Target Parameter");
        break;
        
        
  /*  case ecInvalid...:
        SERIAL_PC.write("Invalid ...");
        break; */
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    ErrorCode = ecNone;
  }
}
//
//----------------------------------------------
//  Segment - Handler - SerialPC Communication
//----------------------------------------------
//
Boolean DetectRxdCommandLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CARRIAGE_RETURN:
        RxdBuffer[RxdBufferIndex] = ZERO;
        RxdBufferIndex = 0; // restart
        strupr(RxdBuffer);
        strcpy(RxdCommandLine, RxdBuffer);
        // debug serial.WriteText("<CR>true");
        // debug delay(1000);
        return true;
      case LINE_FEED: // ignore
        // debug serial.WriteText("<LF>");
        // debug delay(1000);
        break;
      default: 
        // debug serial.WriteText("<");
        // debug serial.WriteCharacter(C);
        // debug serial.WriteText(">");
        RxdBuffer[RxdBufferIndex] = C;
        RxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean AnalyseRxdCommand(CSerial &serial)
{
  if (DetectRxdCommandLine(serial))
  {
    char *PTerminal = " \t\r\n";
    char *PRxdCommandLine = RxdCommandLine;
    PRxdCommand = strtok(RxdCommandLine, PTerminal);
    if (PRxdCommand)
    {
      RxdParameterCount = 0;
      char *PRxdParameter;
      while (PRxdParameter = strtok(0, PTerminal))
      {
        PRxdParameters[RxdParameterCount] = PRxdParameter;
        RxdParameterCount++;
        if (COUNT_RXDPARAMETERS < RxdParameterCount)
        {
          ErrorCode = ecToManyParameters;
          ZeroRxdBuffer();
          // debug serialtarget.WriteLine("error[ecToManyParameters]");
          serial.WriteNewLine();
          serial.WritePrompt();
          return false;
        }
      }  
//      debug sprintf(TxdBuffer, "\n\r# RxdCommand<%s>", PRxdCommand);
//      debug serial.WriteText(TxdBuffer);
//      if (0 < RxdParameterCount)
//      {
//        int II;
//        for (II = 0; II < RxdParameterCount; II++)
//        {
//          sprintf(TxdBuffer, " RxdParameter[%i]<%s>", II, PRxdParameters[II]);
//          serial.WriteText(TxdBuffer);
//        }
//      }
      //
      ZeroRxdBuffer();
      serial.WriteNewLine();
      return true;
    }
    ZeroRxdBuffer();
    serial.WriteNewLine();
    serial.WritePrompt();
  }
  return false;
}

Boolean ExecuteRxdCommand(CSerial &serial)
{ 
//  sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
//  serialtarget.WriteLine(TxdBuffer);
  if (!strcmp("H", PRxdCommand))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp("GPH", PRxdCommand))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp("GSV", PRxdCommand))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp("GHV", PRxdCommand))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  // ----------------------------------
  // Led
  // ----------------------------------
  if (!strcmp("GLD", PRxdCommand))
  {
    ExecuteGetLed(serial);
    return true;
  } else 	
  if (!strcmp("SLD", PRxdCommand))
  {
    ExecuteSetLed(serial);
    return true;
  } else 	
  if (!strcmp("CLD", PRxdCommand))
  {
    ExecuteClearLed(serial);
    return true;
  } else   
  if (!strcmp("TLD", PRxdCommand))
  {
    ExecuteToggleLed(serial);
    return true;
  } else   
  // ----------------------------------
  // Adc
  // ----------------------------------
  if (!strcmp("GAV", PRxdCommand))
  {
    ExecuteGetAdcValue(serial);
    return true;
  } else   
  // ----------------------------------
  // Dac
  // ----------------------------------
  if (!strcmp("GDV", PRxdCommand))
  {
    ExecuteGetDacValue(serial);
    return true;
  } else   
  if (!strcmp("SDV", PRxdCommand))
  {
    ExecuteSetDacValue(serial);
    return true;
  } else   
  // ----------------------------------
  // Pwm
  // ----------------------------------
  if (!strcmp("GPV", PRxdCommand))
  {
    ExecuteGetPwmValue(serial);
    return true;
  } else   
  if (!strcmp("SPV", PRxdCommand))
  {
    ExecuteSetPwmValue(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    ErrorCode = ecInvalidCommand;
  }
  return false;  
}

void HandleSerialCommands(CSerial &serial)
{
  if (AnalyseRxdCommand(serial))
  {
    ExecuteRxdCommand(serial);
  }  
}







