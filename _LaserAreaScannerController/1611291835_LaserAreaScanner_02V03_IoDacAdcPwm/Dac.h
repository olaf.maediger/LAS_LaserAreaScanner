//
//--------------------------------
//  Library Dac
//--------------------------------
//
#ifndef Dac_h
#define Dac_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Dac
//--------------------------------
//
class CDac
{
  private:
  bool FChannel[2];
  UInt16 FValue[2];
  
  public:
  CDac(bool channel0, bool channel1);
  Boolean Open();
  Boolean Close();
  UInt16 GetValue(UInt8 channel);
  void SetValue(UInt8 channel, UInt16 value);
};
//
#endif
