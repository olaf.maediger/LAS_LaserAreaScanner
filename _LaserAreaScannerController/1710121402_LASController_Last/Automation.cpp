#include "Automation.h"
#include "Led.h"
#include "Dac.h"
#include "Process.h"
#include "Command.h"
//
extern CLed LedLaser;
extern CDac DigitalAnalogConverter;
extern CSerial SerialCommand;
extern CProcess LASProcess;
extern CCommand LASCommand;
//
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  //FTimeStart = millis();
  FTimeStamp = millis();
  FTimeMarker = FTimeStamp;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  if (state != FState)
  {
//    Serial.print("S0[");
//    Serial.print(FState);
//    Serial.print("] -> S1[");
//    Serial.print(state);
//    Serial.println("]");
    // Message to Manager(PC):
    LASCommand.CallbackStateAutomation(SerialCommand, FState);
    //
    FState = state;
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CAutomation::Open()
{
  FState = saIdle;
  return true;
}

Boolean CAutomation::Close()
{
  FState = saUndefined;
  return true;
}
//
//----------------------------------------------------------
//  Segment - Automation - Idle
//----------------------------------------------------------
//
void CAutomation::HandleIdle(CSerial &serial)
{
  // do nothing
}
//
//----------------------------------------------------------
//  Segment - Automation - LedLaser
//----------------------------------------------------------
//
void CAutomation::HandleLedLaserPulseBegin(CSerial &serial)
{
  SetTimeStamp();  
  LedLaser.On();
  SetState(saLedLaserPulseBusy);
}
void CAutomation::HandleLedLaserPulseBusy(CSerial &serial)
{  
  SetTimeMarker();  
  if (LedLaser.GetPulsePeriodHalf() <= GetTimePeriod())
  {
    SetTimeStamp();
    if (slOn == LedLaser.GetState())
    {
      LedLaser.Off();
      LASProcess.SetLaserPulseCountActual(LedLaser.GetPulseCountActual());
      if (LedLaser.IsPulseCountReached())
      {
        SetState(saLedLaserPulseEnd);
      }
    }
    else
      if (slOff == LedLaser.GetState())
      {
        LedLaser.On();
      }
  }
}
void CAutomation::HandleLedLaserPulseEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - PositionLaser
//----------------------------------------------------------
//
void CAutomation::HandlePulsePositionLaserBegin(CSerial &serial)
{ // -> Busy : wait for PVL x y p c ( -> Busy) OR : AVL -> Abort -> End -> Idle
  SetState(saPulsePositionLaserBusy);
}

void CAutomation::HandlePulsePositionLaserBusy(CSerial &serial)
{ // -> Busy : wait for PVL x y p c ( -> Busy) OR : AVL -> Abort -> End -> Idle
  SetTimeMarker();
  if (LedLaser.GetPulsePeriodHalf() <= GetTimePeriod())
  {
    SetTimeStamp();
    if (slOff == LedLaser.GetState())
    {
      LedLaser.On();
    }
    else
      if (slOn == LedLaser.GetState())
      {
        LedLaser.Off();
        LASProcess.SetLaserPulseCountActual(LedLaser.GetPulseCountActual());
        if (LedLaser.IsPulseCountReached())
        {
          SetState(saPulsePositionLaserEnd);
        }
      }
  }
}

void CAutomation::HandlePulsePositionLaserEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - MatrixLaser
//----------------------------------------------------------
//
void CAutomation::HandlePulseMatrixLaserBegin(CSerial &serial)
{
  SetTimeStamp();
  UInt32 XPA = LASProcess.GetXPositionMinimum();
  LASProcess.SetXPositionActual(XPA);
  UInt32 YPA = LASProcess.GetYPositionMinimum();
  LASProcess.SetYPositionActual(YPA);
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
  DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());  
#endif
  LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(), 
                               LASProcess.GetLaserPulseCountPreset());
  LASProcess.SetLaserPulseCountActual(0);
  LedLaser.On();
  SetState(saPulseMatrixLaserBusy);
}

void CAutomation::HandlePulseMatrixLaserBusy(CSerial &serial)
{  
  SetTimeMarker();
  if (LedLaser.GetPulsePeriodHalf() <= GetTimePeriod())
  {
    SetTimeStamp();
    if (slOff == LedLaser.GetState())
    {
      LedLaser.On();
    }
    else
      if (slOn == LedLaser.GetState())
      {
        LedLaser.Off();
        LASProcess.SetLaserPulseCountActual(LedLaser.GetPulseCountActual());
        if (LedLaser.IsPulseCountReached())
        { // All Pulses for one Location pulsed -> next Position
          UInt32 XPA = LASProcess.IncrementXPosition();
          if (LASProcess.GetXPositionMaximum() < XPA)
          { // X-Line finished -> next Y-Line
            UInt32 YPA = LASProcess.IncrementYPosition();
            if (LASProcess.GetYPositionMaximum() < YPA)
            { // Y-Line (Matrix) finished
              SetState(saPulseMatrixLaserEnd);
              return;
            }
            else
            { // next Y-Line und X-Restart
              XPA = LASProcess.GetXPositionMinimum();
              LASProcess.SetXPositionActual(XPA);
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
              DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
              DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());  
#endif
            }
          }
          else
          { // next X-Position
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
            DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
#endif           
          }
        }
      }
  }
}

void CAutomation::HandlePulseMatrixLaserAbort(CSerial &serial)
{  
  LedLaser.Off();
  SetState(saIdle);  
}

void CAutomation::HandlePulseMatrixLaserEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - VariableLaser
//----------------------------------------------------------
// SVL -> Begin -> Busy (wait for PVL or AVL)
void CAutomation::HandlePulseVariableLaserBegin(CSerial &serial)
{
  LASProcess.SetLaserPulsePeriod(0);
  LASProcess.SetLaserPulseCountPreset(0);
  LASProcess.SetLaserPulseCountActual(0);
  LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(),
                               LASProcess.GetLaserPulseCountPreset());
  SetState(saPulseVariableLaserWait); 
}

// Wait -> Busy -> Wait -> Busy .... -> Abort
void CAutomation::HandlePulseVariableLaserWait(CSerial &serial)
{ // check if new PVL-Command is received:
  if (0 < LedLaser.GetPulseCountPreset())
  {
    SetState(saPulseVariableLaserBusy); 
    SetTimeStamp();
  }
}

// PVL -> 0 < LaserCountPulsesPreset, 0 < LaserCountPulseActual
void CAutomation::HandlePulseVariableLaserBusy(CSerial &serial)
{ 
  SetTimeMarker();
  if (LedLaser.GetPulsePeriodHalf() <= GetTimePeriod())
  { // Serial.print("DT");
    if (slOff == LedLaser.GetState())
    {
      Serial.println("Laser On");
      LedLaser.On();        
      SetTimeStamp();
    }
    else
    if (slOn == LedLaser.GetState())
    {
      Serial.println("Laser Off");
      LASProcess.SetLaserPulseCountActual(LedLaser.GetPulseCountActual());
      LedLaser.Off();
      SetTimeStamp();      
      if ((0 < LedLaser.GetPulseCountPreset()) &&
          (LedLaser.GetPulseCountPreset() <= LedLaser.GetPulseCountActual()))
      {
        LASProcess.SetLaserPulsePeriod(0);
        LASProcess.SetLaserPulseCountPreset(0);
        LASProcess.SetLaserPulseCountActual(0);
        LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(),
                                     LASProcess.GetLaserPulseCountPreset());
        SetState(saPulseVariableLaserWait);       
      }
    }
  }
}
//  FTimeMarker = millis();
//  Serial.print("TM[");
//  Serial.print(FTimeMarker);
//  Serial.print("] TS[");
//  Serial.print(FTimeStamp);
//  Serial.print("] DT[");
//  Serial.print(FTimeMarker - FTimeStamp);
//  Serial.println("]");
//  delay(1000);
//
//  if (0 == LedLaser.GetPulseCountPreset())
//  {
//    SetState(saPulseVariableLaserWait);
//    return;
//  }
//  if (LedLaser.IsPulseCountReached())
//  {
//    if (slOn == LedLaser.GetState())
//    {
//      Serial.println("Laser Off");
//      LedLaser.Off();
//    }
//    SetState(saPulseVariableLaserWait);
//    return;    
//  }
//  else
//  { // (0 < LaserCountPulsesPreset) and (LaserCountPulseActual < LaserCountPulsePreset)

void CAutomation::HandlePulseVariableLaserAbort(CSerial &serial)
{  
  LedLaser.Off();
  SetState(saPulseVariableLaserEnd);  
}

void CAutomation::HandlePulseVariableLaserEnd(CSerial &serial)
{  
  LedLaser.Off();
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - Collector
//----------------------------------------------------------
//
void CAutomation::Handle(CSerial &serial)
{
  switch (FState)
  { // Idle
    case saIdle:
      HandleIdle(serial);
      break;
    // LedLaser
    case saLedLaserPulseBegin:
      HandleLedLaserPulseBegin(serial);
      break;
    case saLedLaserPulseBusy:
      HandleLedLaserPulseBusy(serial);
      break;
    case saLedLaserPulseEnd:
      HandleLedLaserPulseEnd(serial);
      break;
    // PositionLaser
    case saPulsePositionLaserBegin:
      HandlePulsePositionLaserBegin(serial);
      break;
    case saPulsePositionLaserBusy:
      HandlePulsePositionLaserBusy(serial);
      break;
    case saPulsePositionLaserEnd:
      HandlePulsePositionLaserEnd(serial);
      break;
    // MatrixLaser
    case saPulseMatrixLaserBegin:
      HandlePulseMatrixLaserBegin(serial);
      break;
    case saPulseMatrixLaserBusy:
      HandlePulseMatrixLaserBusy(serial);
      break;
    case saPulseMatrixLaserAbort:
      HandlePulseMatrixLaserEnd(serial);
      break;
    case saPulseMatrixLaserEnd:
      HandlePulseMatrixLaserEnd(serial);
      break;
    // VariableLaser
    case saPulseVariableLaserBegin:
      HandlePulseVariableLaserBegin(serial);
      break;
    case saPulseVariableLaserWait:
      HandlePulseVariableLaserWait(serial);
      break;
    case saPulseVariableLaserBusy:
      HandlePulseVariableLaserBusy(serial);
      break;
    case saPulseVariableLaserAbort:
      HandlePulseVariableLaserEnd(serial);
      break;
    case saPulseVariableLaserEnd:
      HandlePulseVariableLaserEnd(serial);
      break;
  }  
}

