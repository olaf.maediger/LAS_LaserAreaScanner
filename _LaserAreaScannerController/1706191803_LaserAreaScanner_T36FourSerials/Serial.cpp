#include "Serial.h"
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
void CSerialBase::WriteNewLine()
{
  WriteText(PROMPT_NEWLINE);  
}
  
void CSerialBase::WritePrompt()
{
  WriteText(PROMPT_INPUT);
}
  
void CSerialBase::WriteAnswer()
{
  WriteText(PROMPT_ANSWER);
}
//
//----------------------------------------------------
//  Segment - CSerialZ
//----------------------------------------------------
//
#if (defined PROCESSOR_ARDUINODUE)
bool CSerialZ::Open()
{
  FPSerial->begin(115200);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialZ::Close()
{
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}

void CSerialZ::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialZ::WriteText(char *text)
{
  FPSerial->write(text);
}  

void CSerialZ::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  

int CSerialZ::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialZ::ReadCharacter()
{
  return (char)FPSerial->read();
}
#elif (defined PROCESSOR_TEENSY36)
#else
#error Undefined CSerialZ for ProcessorType!
#endif  
//
//----------------------------------------------------
//  Segment - CSerialS
//----------------------------------------------------
//
#if (defined PROCESSOR_ARDUINODUE)
bool CSerialS::Open()
{
  FPSerial->begin(115200);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialS::Close()
{
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}
  
void CSerialS::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialS::WriteText(char *text)
{
  FPSerial->write(text);
}  

void CSerialS::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  

int CSerialS::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialS::ReadCharacter()
{
  return (char)FPSerial->read();
}
#elif (defined PROCESSOR_TEENSY36)
#else
#error Undefined CSerialS for ProcessorType!
#endif  
//
//----------------------------------------------------
//  Segment - CSerialU
//----------------------------------------------------
//
#if (defined PROCESSOR_ARDUINODUE)
bool CSerialU::Open()
{
  FPSerial->begin(115200);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialU::Close()
{
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}
  
void CSerialU::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialU::WriteText(char *text)
{
  FPSerial->write(text);
}  

void CSerialU::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  

int CSerialU::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialU::ReadCharacter()
{
  return (char)FPSerial->read();
}
#elif (defined PROCESSOR_TEENSY36)
#else
#error Undefined CSerialU for ProcessorType!
#endif  
//
//----------------------------------------------------
//  Segment - CSerialH 
//----------------------------------------------------
//
#if ((defined PROCESSOR_STM32F103C8) || (defined PROCESSOR_TEENSY36))
bool CSerialH::Open()
{
  FPSerial->begin(115200);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialH::Close()
{
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}
  
void CSerialH::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialH::WriteText(char *text)
{
  FPSerial->write(text);
}  

void CSerialH::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  

int CSerialH::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialH::ReadCharacter()
{
  return (char)FPSerial->read();
}
#elif (defined PROCESSOR_ARDUINOMEGA)
#elif (defined PROCESSOR_ARDUINODUE)
#else
#error Undefined CSerialH for ProcessorType!
#endif  
//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
#if ((defined PROCESSOR_ARDUINOMEGA) || (defined PROCESSOR_ARDUINODUE))

CSerial::CSerial(UARTClass *serial)
{
  CSerialZ *PSerialZ = (CSerialZ*)new CSerialZ(serial);
  FPSerial = (CSerialBase*)PSerialZ;
}

CSerial::CSerial(UARTClass &serial)
{
  CSerialZ *PSerialZ = (CSerialZ*)new CSerialZ(&serial);
  FPSerial = (CSerialBase*)PSerialZ;
}

CSerial::CSerial(USARTClass *serial)
{
  CSerialS *PSerialS = (CSerialS*)new CSerialS(serial);
  FPSerial = (CSerialBase*)PSerialS;
}

CSerial::CSerial(USARTClass &serial)
{
  CSerialS *PSerialS = (CSerialS*)new CSerialS(&serial);
  FPSerial = (CSerialBase*)PSerialS;
}

CSerial::CSerial(Serial_ *serial)
{ 
  CSerialU *PSerialU = (CSerialU*)new CSerialU(serial);
  FPSerial = (CSerialBase*)PSerialU;
}

CSerial::CSerial(Serial_ &serial)
{ 
  CSerialU *PSerialU = (CSerialU*)new CSerialU(&serial);
  FPSerial = (CSerialBase*)PSerialU;
}

#elif ((defined PROCESSOR_STM32F103C8) || (defined PROCESSOR_TEENSY36))

CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif

bool CSerial::Open()
{
  return FPSerial->Open();
}

bool CSerial::Close()
{
  return FPSerial->Close();
}

void CSerial::WriteCharacter(char character)
{
  FPSerial->WriteCharacter(character);
}

void CSerial::WriteText(char *text)
{
  FPSerial->WriteText(text);
}

void CSerial::WriteLine(char *text)
{
  FPSerial->WriteLine(text);
}

void CSerial::WriteNewLine()
{
  FPSerial->WriteNewLine();
}

void CSerial::WriteAnswer()
{
  FPSerial->WriteAnswer();
}

void CSerial::WritePrompt()
{
  FPSerial->WritePrompt();
}

int CSerial::GetRxdByteCount()
{
  FPSerial->GetRxdByteCount();
}

char CSerial::ReadCharacter()
{
  return (char)FPSerial->ReadCharacter();
}


