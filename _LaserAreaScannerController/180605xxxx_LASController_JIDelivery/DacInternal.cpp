//
//--------------------------------
//  Library Dac
//--------------------------------
//
#include "DacInternal.h"
//
CDacInternal::CDacInternal(bool channel0, bool channel1)
{
  FChannel[0] = channel0;
  FChannel[1] = channel1;
  FValue[0] = 0;
  FValue[1] = 0;
}

Boolean CDacInternal::Open()
{
  analogWriteResolution(12);
  SetValue(DAC_CHANNEL0, 0x0000);  
  SetValue(DAC_CHANNEL1, 0x0000);  
  return true;
}

Boolean CDacInternal::Close()
{
  return true;
}

UInt32 CDacInternal::GetValue(UInt8 channel)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
      case 0:
        return FValue[0];
      case 1:
        return FValue[1];
    }    
  }
  return 0;
}

void CDacInternal::SetValue(UInt8 channel, UInt32 value)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
      case 0:
        FValue[0] = (0x0FFF & value);
        analogWriteResolution(12);
        analogWrite(PIN_DAC_CHANNEL0, FValue[0]);
        break;
      case 1:
        FValue[1] = (0x0FFF & value);
        analogWriteResolution(12);
        analogWrite(PIN_DAC_CHANNEL1, FValue[1]);
        break;
    }    
  }
}

void CDacInternal::SetValues(UInt32 value0, UInt32 value1)
{
  FValue[0] = (0x0FFF & value0);
  analogWriteResolution(12);
  analogWrite(PIN_DAC_CHANNEL0, FValue[0]);
  FValue[1] = (0x0FFF & value1);
  analogWriteResolution(12);
  analogWrite(PIN_DAC_CHANNEL1, FValue[1]);
}

