//
//------------------------------------------------------------------
//  Project:    LAS - LaserAreaScanner
//  Processor:  ArduinoDue
//------------------------------------------------------------------
//
#define PIN_LEDSYSTEM 13

#define PIN_DAC0 DAC0
#define PIN_DAC1 DAC1

#define PIN_ADC0 A0
#define PIN_ADC1 A1
#define PIN_ADC2 A2
#define PIN_ADC3 A3
#define PIN_ADC4 A4
#define PIN_ADC5 A5
#define PIN_ADC6 A6
#define PIN_ADC7 A7
#define PIN_ADC8 A8
#define PIN_ADC9 A9
/* #define PIN_ADC10
#define PIN_ADC11
#define PIN_ADC12
#define PIN_ADC13
 */

#define PIN_PWM1 PWM1 
