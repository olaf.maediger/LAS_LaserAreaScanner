#ifndef SERIAL_H
#define SERIAL_H

#include "Arduino.h"
#include "Serial.h"
#include "Common.h"

class CSerialBase
{
  protected:
  
  public:
  CSerialBase();
  ~CSerialBase();
  //
  virtual bool Init(int baudrate) = 0;
};
//
//--------------------------------------------------------------
//  Section - Serial
//--------------------------------------------------------------
//
class CSerial0 : public CSerialBase
{
  protected:
  
  public:
  CSerial0();
  ~CSerial0();
  //
  bool Init(int baudrate);
};
//
//--------------------------------------------------------------
//  Section - Serial
//--------------------------------------------------------------
//
class CSerial1 : public CSerialBase
{
  protected:
  
  public:
  CSerial1();
  ~CSerial1();
  //
  bool Init(int baudrate);
};
//
//--------------------------------------------------------------
//  Section - Serial
//--------------------------------------------------------------
//
class CSerial2 : public CSerialBase
{
  protected:
  
  public:
  CSerial2();
  ~CSerial2();
  //
  bool Init(int baudrate);
};
//
//--------------------------------------------------------------
//  Section - Serial
//--------------------------------------------------------------
//
class CSerial3 : public CSerialBase
{
  protected:
  
  public:
  CSerial3();
  ~CSerial3();
  //
 bool Init(int baudrate);
};
//
//--------------------------------------------------------------
//  Section - Serial
//--------------------------------------------------------------
//
class CSerial4 : public CSerialBase
{
  protected:
  
  public:
  CSerial4();
  ~CSerial4();
  //
  bool Init(int baudrate);
};
//
#endif // SERIAL_H
