#include "Serial.h"

CSerialBase::CSerialBase()
{
}

CSerialBase::~CSerialBase()
{
}





//
//--------------------------------------------------------------
//  Section - Serial0
//--------------------------------------------------------------
//
CSerial0::CSerial0()
{
}

CSerial0::~CSerial0()
{
}

bool CSerial0::Init(int baudrate)
{
  Serial.begin(baudrate);
  return Serial.available();
}
//
//--------------------------------------------------------------
//  Section - Serial1
//--------------------------------------------------------------
//
CSerial1::CSerial1()
{
}

CSerial1::~CSerial1()
{
}

bool CSerial1::Init(int baudrate)
{
  Serial1.begin(baudrate);
  return Serial1.available();
}
//
//--------------------------------------------------------------
//  Section - Serial2
//--------------------------------------------------------------
//
CSerial2::CSerial2()
{
}

CSerial2::~CSerial2()
{
}

bool CSerial2::Init(int baudrate)
{
  Serial2.begin(baudrate);
  return Serial2.available();
}
//
//--------------------------------------------------------------
//  Section - Serial3
//--------------------------------------------------------------
//
CSerial3::CSerial3()
{
}

CSerial3::~CSerial3()
{
}

bool CSerial3::Init(int baudrate)
{
  Serial3.begin(baudrate);
  return Serial3.available();
}
//
//--------------------------------------------------------------
//  Section - Serial4
//--------------------------------------------------------------
//
CSerial4::CSerial4()
{
}

CSerial4::~CSerial4()
{
}

bool CSerial4::Init(int baudrate)
{
  SerialUSB.begin(baudrate);
  return SerialUSB.available();
}



