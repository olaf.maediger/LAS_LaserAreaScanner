#include "Arduino.h"
#include "Common.h"
#include "Led.h"
#include "Serial.h"

  
CLed FLedSystem(PIN_LEDSYSTEM);
CSerial0 FSerial0();
CSerial1 FSerial1();
CSerial2 FSerial2();
CSerial3 FSerial3();
CSerial4 FSerial4();

void setup()
{
  FLedSystem.Init();
  FSerial0.Init(115200);
  FSerial1.Init(115200);
  FSerial2.Init(115200);
  FSerial3.Init(115200);
  FSerial4.Init(115200);
}

void loop() 
{
  FLedSystem.PulseOn(1000);
//  FSerial0.Write("H");
//  FSerial1.Write("H");
//  FSerial2.Write("H");
//  FSerial3.Write("H");
//  FSerial4.Write("H");
}
