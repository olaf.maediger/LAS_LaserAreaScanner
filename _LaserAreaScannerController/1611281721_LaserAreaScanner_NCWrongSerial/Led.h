#ifndef LED_H
#define LED_H

#include "Arduino.h"
#include "Common.h"

class CLed
{
  protected:
  int FPin;
  
  public:
  CLed(int pin);
  ~CLed();
  //
  bool Init();
  void On();
  void Off();
  void PulseOn(int period);
  void PulseOff(int period);
};

#endif // LED_H
