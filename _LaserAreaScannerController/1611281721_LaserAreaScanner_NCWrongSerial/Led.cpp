#include "Led.h"

CLed::CLed(int pin)
{
  FPin = pin;
}

CLed::~CLed()
{
}

bool CLed::Init()
{
  pinMode(FPin, OUTPUT);
}

void CLed::On()
{
  digitalWrite(FPin, HIGH);
}

void CLed::Off()
{
  digitalWrite(FPin, LOW); 
}

void CLed::PulseOn(int period)
{
  digitalWrite(FPin, HIGH); 
  delay(period);  
  digitalWrite(FPin, LOW); 
  delay(period);  
}

void CLed::PulseOff(int period)
{ 
  digitalWrite(FPin, LOW); 
  delay(period);  
  digitalWrite(FPin, HIGH); 
  delay(period);  
}


