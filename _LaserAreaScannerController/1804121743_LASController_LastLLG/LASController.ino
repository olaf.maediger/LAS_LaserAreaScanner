#include "Defines.h"
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Serial.h"
#include "Led.h"
#include "LedLine.h"
#include "Dac.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
CError LASError;
CProcess LASProcess;
CCommand LASCommand;
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED);
CLedLine LedLine(61, 60, PIN_LEDMOTIONY, PIN_LEDMOTIONX,  // 7..4
                 57, PIN_LEDBUSY, 55, PIN_LEDERROR);      // 3..0
CLed LedLaser(PIN_LEDLASER);                 
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Dac
//---------------------------------------------------
//
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
CDac DigitalAnalogConverter(true, true);
#endif
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort und CommandPort!
// NC - CSerial SerialDebug(Serial);
// normal CSerial SerialCommand(Serial1);
CSerial SerialCommand(Serial);
// NC - CSerial SerialPrinter(Serial2);
// NC - Serial3
//
//
//---------------------------------------------------
// Segment - Forward-Declaration
//---------------------------------------------------
//

//
//---------------------------------------------------
// Segment - Setup
//---------------------------------------------------
//
void setup() 
{ //-------------------------------------------
  // Device - Led 
  //-------------------------------------------
  LedSystem.Open();
  LedLine.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.Off();
    LedLine.On();
    delay(40);
    LedSystem.On();
    LedLine.Off();
    delay(40);
  }
  LedSystem.Off();
  //
  LedLaser.Open();
  LedLaser.Off();
  //-------------------------------------------
  // Device - Serial
  //-------------------------------------------
//  SerialDebug.Open(115200);
//  SerialDebug.SetRxdEcho(RXDECHO_ON);
  // 
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_OFF);
  //
  DigitalAnalogConverter.Open();
  //-------------------------------------------
  // Device - Command
  //-------------------------------------------
  LASCommand.WriteProgramHeader(SerialCommand);
  LASCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //-------------------------------------------
  // Device - Process 
  //-------------------------------------------
  LASProcess.Open();
  LASProcess.SetState(spWelcome);
}
//
//---------------------------------------------------
// Segment - Loop
//---------------------------------------------------
//
void loop() 
{ 
  LASError.Handle(SerialCommand);    
  LASCommand.Handle(SerialCommand);
  LASProcess.Handle(SerialCommand);  
}




