#include "IrqTimer.h"

const CIrqTimer::STimer CIrqTimer::Timers[NUM_TIMERS] = 
{
	{TC0,0,TC0_IRQn},
	{TC0,1,TC1_IRQn},
	{TC0,2,TC2_IRQn},
	{TC1,0,TC3_IRQn},
	{TC1,1,TC4_IRQn},
	{TC1,2,TC5_IRQn},
#if NUM_TIMERS > 6
	{TC2,0,TC6_IRQn},
	{TC2,1,TC7_IRQn},
	{TC2,2,TC8_IRQn},
#endif
};

void (*CIrqTimer::callbacks[NUM_TIMERS])() = {};
		
#if NUM_TIMERS > 6
double CIrqTimer::_frequency[NUM_TIMERS] = {-1,-1,-1,-1,-1,-1,-1,-1,-1};
#else
double CIrqTimer::_frequency[NUM_TIMERS] = {-1,-1,-1,-1,-1,-1};
#endif

CIrqTimer Timer(0);
//CIrqTimer Timer1(1);
//CIrqTimer Timer0(0);
//CIrqTimer Timer2(2);
//CIrqTimer Timer3(3);
//CIrqTimer Timer4(4);
//CIrqTimer Timer5(5);
//#if NUM_TIMERS > 6
//CIrqTimer Timer6(6);
//CIrqTimer Timer7(7);
//CIrqTimer Timer8(8);
//#endif

CIrqTimer::CIrqTimer(unsigned short _timer) : timer(_timer)
{
}

CIrqTimer CIrqTimer::getAvailable(void)
{
	for (int i = 0; i < NUM_TIMERS; i++)
	{
		if (!callbacks[i])
			return CIrqTimer(i);
	}
	return CIrqTimer(0);
}

CIrqTimer& CIrqTimer::attachInterrupt(void (*isr)())
{
	callbacks[timer] = isr;
	return *this;
}

CIrqTimer& CIrqTimer::detachInterrupt(void)
{
	stop(); // Stop the currently running timer
	callbacks[timer] = NULL;
	return *this;
}

CIrqTimer& CIrqTimer::start(double microseconds)
{
  if(microseconds > 0)
		SetPeriodus(microseconds);	
	if(_frequency[timer] <= 0)
		SetFrequencyHz(1);
	NVIC_ClearPendingIRQ(Timers[timer].irq);
	NVIC_EnableIRQ(Timers[timer].irq);	
	TC_Start(Timers[timer].tc, Timers[timer].channel);
	return *this;
}

CIrqTimer& CIrqTimer::stop(void)
{
	NVIC_DisableIRQ(Timers[timer].irq);
	TC_Stop(Timers[timer].tc, Timers[timer].channel);
	return *this;
}

uint8_t CIrqTimer::bestClock(double frequency, uint32_t& retRC)
{
	/*
		Pick the best Clock, thanks to Ogle Basil Hall!

		Timer		Definition
		TIMER_CLOCK1	MCK /  2
		TIMER_CLOCK2	MCK /  8
		TIMER_CLOCK3	MCK / 32
		TIMER_CLOCK4	MCK /128
	*/
	const struct 
	{
		uint8_t flag;
		uint8_t divisor;
	} clockConfig[] = 
	{
		{ TC_CMR_TCCLKS_TIMER_CLOCK1,   2 },
		{ TC_CMR_TCCLKS_TIMER_CLOCK2,   8 },
		{ TC_CMR_TCCLKS_TIMER_CLOCK3,  32 },
		{ TC_CMR_TCCLKS_TIMER_CLOCK4, 128 }
	};
	float ticks;
	float error;
	int clkId = 3;
	int bestClock = 3;
	float bestError = 9.999e99;
	do
	{
		ticks = (float) SystemCoreClock / frequency / (float) clockConfig[clkId].divisor;
		// error = abs(ticks - round(ticks));
		error = clockConfig[clkId].divisor * abs(ticks - round(ticks));	// Error comparison needs scaling
		if (error < bestError)
		{
			bestClock = clkId;
			bestError = error;
		}
	} while (clkId-- > 0);
	ticks = (float) SystemCoreClock / frequency / (float) clockConfig[bestClock].divisor;
	retRC = (uint32_t) round(ticks);
	return clockConfig[bestClock].flag;
}


CIrqTimer& CIrqTimer::SetFrequencyHz(double frequency)
{
	if(frequency <= 0) { frequency = 1; }
	// Remember the frequency — see below how the exact frequency is reported instead
	//_frequency[timer] = frequency;
	// Get current timer configuration
	STimer t = Timers[timer];
	uint32_t rc = 0;
	uint8_t clock;
	// Tell the Power Management Controller to disable 
	// the write protection of the (Timer/Counter) registers:
	pmc_set_writeprotect(false);
	// Enable clock for the timer
	pmc_enable_periph_clk((uint32_t)t.irq);
	// Find the best clock for the wanted frequency
	clock = bestClock(frequency, rc);
	switch (clock) 
	{
	  case TC_CMR_TCCLKS_TIMER_CLOCK1:
	    _frequency[timer] = (double)SystemCoreClock / 2.0 / (double)rc;
	    break;
	  case TC_CMR_TCCLKS_TIMER_CLOCK2:
	    _frequency[timer] = (double)SystemCoreClock / 8.0 / (double)rc;
	    break;
	  case TC_CMR_TCCLKS_TIMER_CLOCK3:
	    _frequency[timer] = (double)SystemCoreClock / 32.0 / (double)rc;
	    break;
	  default: // TC_CMR_TCCLKS_TIMER_CLOCK4
	    _frequency[timer] = (double)SystemCoreClock / 128.0 / (double)rc;
	    break;
	}
	// Set up the Timer in waveform mode which creates a PWM
	// in UP mode with automatic trigger on RC Compare
	// and sets it up with the determined internal clock as clock input.
	TC_Configure(t.tc, t.channel, TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC | clock);
	// Reset counter and fire interrupt when RC value is matched:
	TC_SetRC(t.tc, t.channel, rc);
	// Enable the RC Compare Interrupt...
	t.tc->TC_CHANNEL[t.channel].TC_IER=TC_IER_CPCS;
	// ... and disable all others.
	t.tc->TC_CHANNEL[t.channel].TC_IDR=~TC_IER_CPCS;
	return *this;
}

CIrqTimer& CIrqTimer::SetPeriodus(double microseconds)
{ // Convert period in microseconds to frequency in Hz
	double frequency = 1000000.0 / microseconds;	
	SetFrequencyHz(frequency);
	return *this;
}

double CIrqTimer::GetFrequencyHz(void) const 
{
	return _frequency[timer];
}

double CIrqTimer::GetPeriodus(void) const 
{
	return 1.0 / GetFrequencyHz() * 1000000;
}

/*
	Implementation of the timer callbacks defined in 
	arduino-1.5.2/hardware/arduino/sam/system/CMSIS/Device/ATMEL/sam3xa/include/sam3x8e.h
*/
// Fix for compatibility with Servo library
#ifndef USING_SERVO_LIB
void TC0_Handler(void)
{
	TC_GetStatus(TC0, 0);
	CIrqTimer::callbacks[0]();
}
#endif
void TC1_Handler(void)
{
	TC_GetStatus(TC0, 1);
	CIrqTimer::callbacks[1]();
}
// Fix for compatibility with Servo library
#ifndef USING_SERVO_LIB
void TC2_Handler(void)
{
	TC_GetStatus(TC0, 2);
	CIrqTimer::callbacks[2]();
}
void TC3_Handler(void)
{
	TC_GetStatus(TC1, 0);
	CIrqTimer::callbacks[3]();
}
void TC4_Handler(void)
{
	TC_GetStatus(TC1, 1);
	CIrqTimer::callbacks[4]();
}
void TC5_Handler(void)
{
	TC_GetStatus(TC1, 2);
	CIrqTimer::callbacks[5]();
}
#endif

#if NUM_TIMERS > 6
void TC6_Handler(void)
{
	TC_GetStatus(TC2, 0);
	CIrqTimer::callbacks[6]();
}
void TC7_Handler(void)
{
	TC_GetStatus(TC2, 1);
	CIrqTimer::callbacks[7]();
}
void TC8_Handler(void)
{
	TC_GetStatus(TC2, 2);
	CIrqTimer::callbacks[8]();
}
#endif
