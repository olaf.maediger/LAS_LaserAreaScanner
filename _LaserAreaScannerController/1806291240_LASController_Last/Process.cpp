           #include "Led.h"
#include "Process.h"
#include "Command.h"
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  #include "DacInternal.h"
#endif
#if (defined(DAC_MCP4725_X) || defined(DAC_MCP4725_Y)) 
  #include "DacMcp4725.h"
#endif
//
extern CLed LedSystem;
extern CLed LedLaser;
extern CSerial SerialCommand;
extern CProcess LASProcess;
extern CCommand LASCommand;
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
extern CDacInternal DacInternal_XY;
#endif
#ifdef DAC_MCP4725_X
extern CDacMcp4725 DacMcp4725_X;
#endif
#ifdef DAC_MCP4725_Y 
extern CDacMcp4725 DacMcp4725_Y;
#endif
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CProcess::CProcess()
{
  FState = spUndefined;
  FStateTick = 0;
  FTimeStampms = millis();
  FTimeMarkms = FTimeStampms;
  FTimeStampus = micros();
  FTimeMarkus = FTimeStampus;
  FProcessCount = INIT_PROCESSCOUNT;
  FProcessPeriod = INIT_PROCESSPERIOD_MS;
  FProcessStep = 0;
  FXPositionMinimum = INIT_XPOSITION_MINIMUM;
  FXPositionMaximum = INIT_XPOSITION_MAXIMUM;
  FXPositionDelta = INIT_XPOSITION_DELTA;
  FXPositionActual = INIT_XPOSITION_ACTUAL;
  FYPositionMinimum = INIT_YPOSITION_MINIMUM;
  FYPositionMaximum = INIT_YPOSITION_MAXIMUM;
  FYPositionDelta = INIT_YPOSITION_DELTA;
  FYPositionActual = INIT_YPOSITION_ACTUAL;
  FDelayMotionms = INIT_DELAY_MOTION_MS;
  FPulseWidthus = INIT_PULSEWIDTH_US;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateProcess CProcess::GetState()
{
  return FState;
}

void CProcess::SetState(EStateProcess state)
{
  FStateTick = 0;
  FState = state;
  FSubstate = 0;
  // Message to Manager(PC):
  LASCommand.CallbackStateProcess(SerialCommand, FState, FSubstate);
}

void CProcess::SetState(EStateProcess state, UInt8 substate)
{
  if ((state != FState) || (substate != FSubstate))
  {     
    if (state != FState)
    {
      FStateTick = 0;
    }
    FState = state;
    FSubstate = substate;
    switch (FState)
    {
      case spStopProcessExecution:
        break;
    }
    // Message to Manager(PC):
    LASCommand.CallbackStateProcess(SerialCommand, FState, substate);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CProcess::Open()
{
  SetState(spIdle);
  return true;
}

Boolean CProcess::Close()
{
  SetState(spUndefined);
  return true;
}

//
//----------------------------------------------------------
//  Segment - Common
//----------------------------------------------------------
//
void CProcess::HandleUndefined(CSerial &serial)
{
  delay(1000);
}

void CProcess::HandleIdle(CSerial &serial)
{ // do nothing
  SetTimeStamp();
}

void CProcess::HandleWelcome(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:        
      SetTimeStamp();
      FStateTick++;
      break;
    case 1: case 3: case 5: 
      // if (330000 < GetTimeSpan())
      if (330 < GetTimeSpanms())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 2: case 4: case 6:  
      // if (330000 < GetTimeSpan())
      if (330 < GetTimeSpanms())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 7:  
      FStateTick++;      
      SetTimeStamp();      
      break;
    case 8:   
      // if (1000000 < GetTimeSpanms())
      if (1000 < GetTimeSpanms())
      {   
        FStateTick++;
        SetTimeStamp();
      }
      break;
    case 9:   
      // if (1000000 < GetTimeSpan())
      if (1000 < GetTimeSpanms())
      {   
        FStateTick++;        
        SetState(spIdle);
// alternative : 
//        UInt8 CL = 0;
//        UInt8 CH = 7;
//        LASProcess.SetChannelLow(CL);
//        LASProcess.SetChannelHigh(CH);
//        LASProcess.SetState(spRepeatAllTemperatures);
//        LASProcess.SetProcessIndex(0);
//        SetState(spRepeatAllTemperatures);
        SetTimeStamp();
      }
      break;
  }
}

void CProcess::HandleGetHelp(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProgramHeader(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSoftwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetHardwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessCount(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessCount(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessPeriod(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessPeriod(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleStopProcessExecution(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - LedSystem
//----------------------------------------------------------
//
void CProcess::LedSystemState()
{
  switch (LedSystem.GetState())
  {
    case slOn:
      break;
    case slOff:
      break;
    default:
      break;
  }
  SetState(spIdle, LedSystem.GetState());
}

void CProcess::HandleGetLedSystem(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOn(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOff(CSerial &serial)
{
  LedSystemState();
}

// 1805231334 - No DelayMotion
void CProcess::HandleBlinkLedSystem(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0: // Init              
      FStateTick = 1;
      SetTimeStamp();
      break;
    case 1: // PulseH
      if (GetPulseWidthus() < GetTimeSpanus())
      {
        LedSystem.On();
        SetState(spBlinkLedSystem, 1);
        FStateTick = 2; // PulseL
        // no more !!! SetTimeStamp();
      }
      break;
    case 2: // PulseL
      if (GetProcessPeriod() < GetTimeSpanms())
      {
        LedSystem.Off();
        SetState(spBlinkLedSystem, 0);
        FProcessStep++;
        if (FProcessStep < FProcessCount)
        {
          FStateTick = 1; // PulseH
        }
        else
        {
          SetState(spIdle);
        }
        SetTimeStamp(); 
      }
      break;
  } 
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserPosition
//----------------------------------------------------------
// Command: PulseLaserPosition PLP <x> <y> <p> <c> : Pulse Laser <x><y> <p>eriod <c>ount"
// Command: Definition of XPositionActual, YPositionActual, ProcessPeriod, ProcessCount
// 1805231334 - DelayMotion
void CProcess::HandlePulseLaserPosition(CSerial &serial)
{ 
  SetTimeMark();
  switch (FStateTick)
  {
    case 0: // PositionXY
      SetTimeStamp();
      LedSystem.Off();
      LedLaser.Off();
      SetProcessStep(0);   
#ifdef DAC_INTERNAL_X
      DacInternal_XY.SetValue(DAC_CHANNEL0, GetXPositionActual());
#endif
#ifdef DAC_INTERNAL_Y
      DacInternal_XY.SetValue(DAC_CHANNEL1, GetYPositionActual());
#endif
#ifdef DAC_MCP4725_X
      DacMcp4725_X.SetValue(GetXPositionActual());
#endif
#ifdef DAC_MCP4725_Y 
      DacMcp4725_Y.SetValue(GetYPositionActual());
#endif
      if (GetProcessStep() < GetProcessCount())
      {
        FStateTick = 1; // DelayMotion
      }
      else
      {
        FStateTick = 4; // End
      }
      break;
    case 1: // DelayMotion
      if (GetDelayMotionms() <= GetTimeSpanms())
      {
        SetTimeStamp();
        FStateTick = 2; // PulseH
      }            
      break;
    case 2: // PulseH
      if (slOn != LedLaser.GetState()) 
      {
        LedSystem.On();
        LedLaser.On();
        IncrementProcessStep();
      }
      if (GetPulseWidthus() <= GetTimeSpanus())
      {
        FStateTick = 3; // PulseL
      }
      break;
    case 3: // PulseL
      if (slOff != LedLaser.GetState()) 
      {
        LedSystem.Off();
        LedLaser.Off();
      }
      if (GetProcessPeriod() <= GetTimeSpanms())
      {
        SetTimeStamp();         
        if (GetProcessCount() <= GetProcessStep())
        { // End          
          FStateTick = 4; // End
        }
        else
        { // More Pulses
          FStateTick = 2; // PulseH
        }     
      }
      break;
    case 4: // End
      SetTimeStamp();
      LedSystem.Off();
      LedLaser.Off();
      FStateTick = 1;
      FProcessStep++;
      if (FProcessCount <= FProcessStep)
      {
        SetState(spIdle);
      }
      break;
  }
}

void CProcess::HandleAbortLaserPosition(CSerial &serial)
{  
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserRange
//----------------------------------------------------------
//
void CProcess::HandleGetPositionX(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetPositionX(CSerial &serial)
{
  UInt32 PX = GetXPositionActual();
#ifdef DAC_INTERNAL_X
  DacInternal_XY.SetValue(DAC_CHANNEL0, PX);
#endif
#ifdef DAC_MCP4725_X
  DacMcp4725_X.SetValue(PX);
#endif
  SetState(spIdle);
}

void CProcess::HandleGetPositionY(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetPositionY(CSerial &serial)
{
  UInt32 PY = GetYPositionActual();
#ifdef DAC_INTERNAL_Y
  DacInternal_XY.SetValue(DAC_CHANNEL1, PY);
#endif
#ifdef DAC_MCP4725_Y 
  DacMcp4725_Y.SetValue(PY);
#endif
  SetState(spIdle);
}

void CProcess::HandleGetRangeX(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetRangeX(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetRangeY(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetRangeY(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetDelayMotion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetDelayMotion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetPulseWidthus(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetPulseWidthus(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserLine
//----------------------------------------------------------
// 1806111050
void CProcess::HandlePulseLaserRow(CSerial &serial)
{ 
  const int STATE_INITMOTION    = 0;
  const int STATE_POSITIONY     = 1;
  const int STATE_DELAYMOTION   = 2;
  const int STATE_PULSEHIGH     = 3;
  const int STATE_PULSELOW      = 4;
  const int STATE_NEXTPOSITION  = 5;
  const int STATE_END           = 6;
  //  
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INITMOTION: // Init Motion
      SetYPositionActual(GetYPositionMinimum());
      FStateTick = STATE_POSITIONY;
      LedSystem.Off();
      LedLaser.Off();
      SetProcessStep(0);
      SetDelayMotionms(GetDelayMotionms());
      SetTimeStamp();
      break;
    case STATE_POSITIONY: 
#ifdef DAC_INTERNAL_Y
      DacInternal_XY.SetValue(DAC_CHANNEL1, GetYPositionActual());
#endif
#ifdef DAC_MCP4725_Y 
      DacMcp4725_Y.SetValue(GetYPositionActual());
#endif
      if (GetProcessStep() < GetProcessCount())
      {
        if (0 < GetDelayMotionms())
        {
          SetDelayMotionms(GetDelayMotionms());
          FStateTick = STATE_DELAYMOTION;
        }
        else
        {
          FStateTick = STATE_PULSEHIGH;
        }
      }
      else
      {
        FStateTick = STATE_NEXTPOSITION;
      }      
      SetTimeStamp();
      break;
    case STATE_DELAYMOTION: 
      if (GetDelayMotionms() <= GetTimeSpanms())
      {
        FStateTick = STATE_PULSEHIGH;
        SetTimeStamp();
      }            
      break;
    case STATE_PULSEHIGH: 
      if (slOn != LedLaser.GetState()) 
      { 
        LedSystem.On();
        LedLaser.On();
        IncrementProcessStep();  
      }      
      if (GetPulseWidthus() <= GetTimeSpanus())      
      { 
        FStateTick = STATE_PULSELOW;
      }
      break;
    case STATE_PULSELOW: // more Pulses?
      if (slOff != LedLaser.GetState()) 
      { 
        LedSystem.Off();
        LedLaser.Off();
      }
      if (GetProcessPeriod() <= GetTimeSpanms())
      { 
        if (GetProcessCount() <= GetProcessStep())
        { // next Position
          FStateTick = STATE_NEXTPOSITION;
        }
        else
        { // more Pulses
          FStateTick = STATE_PULSEHIGH; 
        }
        SetTimeStamp();
      }      
      break;
    case STATE_NEXTPOSITION: 
      SetProcessStep(0);
      FStateTick = STATE_POSITIONY;
      FYPositionActual += FYPositionDelta; 
      if (GetYPositionMaximum() < GetYPositionActual())
      { 
        FStateTick = STATE_END; 
      }     
      SetTimeStamp();
      break;
    case STATE_END: 
      LedSystem.Off();
      LedLaser.Off();
      SetState(spIdle);      
      SetTimeStamp();
      break;      
  }
}

void CProcess::HandleAbortLaserRow(CSerial &serial)
{
  LedSystem.Off();
  LedLaser.Off();
  SetState(spIdle);
  SetTimeStamp();
}

void CProcess::HandlePulseLaserCol(CSerial &serial)
{ 
  const int STATE_INITMOTION    = 0;
  const int STATE_POSITIONX     = 1;
  const int STATE_DELAYMOTION   = 2;
  const int STATE_PULSEHIGH     = 3;
  const int STATE_PULSELOW      = 4;
  const int STATE_NEXTPOSITION  = 5;
  const int STATE_END           = 6;
  //  
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INITMOTION: 
      SetXPositionActual(GetXPositionMinimum());
      FStateTick = STATE_POSITIONX;
      LedSystem.Off();
      LedLaser.Off();
      SetProcessStep(0);
      SetTimeStamp();
      break;
    case STATE_POSITIONX: 
#ifdef DAC_INTERNAL_X
      DacInternal_XY.SetValue(DAC_CHANNEL0, GetXPositionActual());
#endif
#ifdef DAC_MCP4725_X
      DacMcp4725_X.SetValue(GetXPositionActual());
#endif
      if (GetProcessStep() < GetProcessCount())
      {
        FStateTick = STATE_DELAYMOTION;
      }
      else
      {
        FStateTick = STATE_PULSELOW;
      }
      SetTimeStamp();
      break;
    case STATE_DELAYMOTION: 
      if (GetDelayMotionms() <= GetTimeSpanms())
      {
        FStateTick = STATE_PULSEHIGH; 
        SetTimeStamp();
      }            
      break;
    case STATE_PULSEHIGH: 
      if (slOn != LedLaser.GetState()) 
      { 
        LedSystem.On();
        LedLaser.On();
        IncrementProcessStep();
      }      
      if (GetPulseWidthus() <= GetTimeSpanus())      
      { 
        FStateTick = STATE_PULSELOW;
      }
      break;
    case STATE_PULSELOW: // more Pulses?
      if (slOff != LedLaser.GetState()) 
      { 
        LedSystem.Off();
        LedLaser.Off();
      }
      if (GetProcessPeriod() <= GetTimeSpanms())
      { 
        if (GetProcessCount() <= GetProcessStep())
        { 
          FStateTick = STATE_NEXTPOSITION;
        }
        else
        { // more Pulses
          FStateTick = STATE_PULSEHIGH; 
        }
        SetTimeStamp(); 
      }      
      break;
    case STATE_NEXTPOSITION: 
      SetProcessStep(0);
      FXPositionActual += FXPositionDelta;
      FStateTick = STATE_POSITIONX; 
      if (FXPositionMaximum < FXPositionActual) 
      {        
        SetProcessStep(0);
        FStateTick = STATE_END;
        SetTimeStamp(); 
      }     
      break;
    case STATE_END: 
      LedSystem.Off();
      LedLaser.Off();
      SetState(spIdle);      
      SetTimeStamp(); 
      break;      
  }
}

void CProcess::HandleAbortLaserCol(CSerial &serial)
{
  LedSystem.Off();
  LedLaser.Off();
  SetState(spIdle);
  SetTimeStamp(); 
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserMatrix
//----------------------------------------------------------
// 1806271138 - FSM
void CProcess::HandlePulseLaserMatrix(CSerial &serial)
{ 
  const int STATE_INITMOTION    = 0;
  const int STATE_POSITIONXY    = 1;
  const int STATE_DELAYMOTION   = 2;
  const int STATE_PULSEHIGH     = 3;
  const int STATE_PULSELOW      = 4;
  const int STATE_NEXTPOSITION  = 5;
  const int STATE_END           = 6;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INITMOTION: // Init Motion
      SetXPositionActual(GetXPositionMinimum());
      SetYPositionActual(GetYPositionMinimum());
      FStateTick = STATE_POSITIONXY;
      LedSystem.Off();
      LedLaser.Off();
      SetProcessStep(0);
      SetTimeStamp();
      break;
    case STATE_POSITIONXY: 
#ifdef DAC_INTERNAL_X
      DacInternal_XY.SetValue(DAC_CHANNEL0, GetXPositionActual());
#endif
#ifdef DAC_INTERNAL_Y
      DacInternal_XY.SetValue(DAC_CHANNEL1, GetYPositionActual());
#endif
#ifdef DAC_MCP4725_X
      DacMcp4725_X.SetValue(GetXPositionActual());
#endif
#ifdef DAC_MCP4725_Y 
      DacMcp4725_Y.SetValue(GetYPositionActual());
#endif
      if (GetProcessStep() < GetProcessCount())
      {
        FStateTick = STATE_DELAYMOTION; 
        SetTimeStamp();
      }
      else
      {
        FStateTick = STATE_PULSELOW; 
        SetTimeStamp();
      }
      break;
    case STATE_DELAYMOTION: 
      if (GetDelayMotionms() <= GetTimeSpanms())
      {
        FStateTick = STATE_PULSEHIGH; 
        SetTimeStamp();
      }            
      break;
    case STATE_PULSEHIGH: 
      if (slOn != LedLaser.GetState()) 
      { 
        LedSystem.On();
        LedLaser.On();
        IncrementProcessStep();
      }      
      if (GetPulseWidthus() <= GetTimeSpanus())      
      { 
        FStateTick = STATE_PULSELOW;
      }
      break;
    case STATE_PULSELOW: // more Pulses?
      if (slOff != LedLaser.GetState()) 
      { 
        LedSystem.Off();
        LedLaser.Off();
      }
      if (GetProcessPeriod() <= GetTimeSpanms())
      { 
        if (GetProcessCount() <= GetProcessStep())
        { // next Position
          FStateTick = STATE_NEXTPOSITION; 
        }
        else
        { // more Pulses
          FStateTick = STATE_PULSEHIGH; 
        }
        SetTimeStamp(); 
      }      
      break;
    case STATE_NEXTPOSITION: 
      SetProcessStep(0);
      FXPositionActual += FXPositionDelta;
      FStateTick = STATE_POSITIONXY;
      if (FXPositionMaximum < FXPositionActual) 
      {        
        FYPositionActual += FYPositionDelta; 
        SetXPositionActual(GetXPositionMinimum());          
        SetProcessStep(0);
        if (GetYPositionMaximum() < GetYPositionActual())
        { 
          FStateTick = STATE_END;
        }
      }     
      SetTimeStamp(); 
      break;
    case STATE_END: 
      LedSystem.Off();
      LedLaser.Off();
      SetState(spIdle);      
      SetTimeStamp(); 
      break;      
  }
}

void CProcess::HandleAbortLaserMatrix(CSerial &serial)
{
  LedSystem.Off();
  LedLaser.Off();
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserImage
//----------------------------------------------------------
// 1806271414 - FSM
void CProcess::HandlePulseLaserImage(CSerial &serial)
{
  const int STATE_INITMOTION  = 0;
  const int STATE_POSITIONXY  = 1;
  const int STATE_DELAYMOTION = 2;
  const int STATE_PULSEHIGH   = 3;
  const int STATE_PULSELOW    = 4;
  const int STATE_WAIT        = 5;
  // 
  SetTimeMark();
  switch (FStateTick)
  {
    case STATE_INITMOTION:
// debug Serial.println("0: Init");
      SetTimeStamp();
      LedSystem.Off();
      LedLaser.Off();
      SetProcessStep(0);
// debug Serial.print("PS:"); Serial.print(GetProcessStep()); Serial.print(" PC:"); Serial.println(GetProcessCount());
      FStateTick = STATE_POSITIONXY;
      break;
    case STATE_POSITIONXY:
// debug Serial.println("1: Position"); Serial.print("PS:"); Serial.print(GetProcessStep()); Serial.print(" PC:"); Serial.println(GetProcessCount());
#ifdef DAC_INTERNAL_X
      DacInternal_XY.SetValue(DAC_CHANNEL0, GetXPositionActual());
#endif
#ifdef DAC_INTERNAL_Y
      DacInternal_XY.SetValue(DAC_CHANNEL1, GetYPositionActual());
#endif
#ifdef DAC_MCP4725_X
      DacMcp4725_X.SetValue(GetXPositionActual());
#endif
#ifdef DAC_MCP4725_Y 
      DacMcp4725_Y.SetValue(GetYPositionActual());
#endif
      if (GetProcessStep() < GetProcessCount())
      {
        FStateTick = STATE_DELAYMOTION;
        SetTimeStamp();
      }
      else
      {
        FStateTick = STATE_WAIT;
      }
      break;
    case STATE_DELAYMOTION:
      if (GetDelayMotionms() <= GetTimeSpanms())
      {
        FStateTick = STATE_PULSEHIGH;
        SetTimeStamp();
      }      
      break;
    case STATE_PULSEHIGH: 
      if (slOn != LedLaser.GetState()) 
      { // debug Serial.println("3: LedLaser: ON");
        LedSystem.On();
        LedLaser.On();
        IncrementProcessStep();
// debug Serial.print("Incremented PS:"); Serial.print(GetProcessStep()); Serial.print(" PC:"); Serial.println(GetProcessCount());
      }      
      if (GetPulseWidthus() <= GetTimeSpanus())      
      { 
        FStateTick = STATE_PULSELOW;
      }
      break;
    case STATE_PULSELOW: // more Pulses? 
      if (slOff != LedLaser.GetState()) 
      { // debug Serial.println("3: LedLaser: OFF");
        LedSystem.Off();
        LedLaser.Off();
      }
      if (GetProcessPeriod() <= GetTimeSpanms())
      { 
        SetTimeStamp(); 
        if (GetProcessCount() <= GetProcessStep())
        { // wait for next Position          
          FStateTick = STATE_WAIT;
        }
        else
        { // more Pulses -> back to PulseH
          FStateTick = STATE_PULSEHIGH; 
        }
      }      
      break;
    case STATE_WAIT: // wait for next PLM- or ALM-Command ...
// debug Serial.println("4: Wait");
      LedSystem.Off();
      LedLaser.Off();
      SetState(spIdle);      
      break;
  }
}

void CProcess::HandleAbortLaserImage(CSerial &serial)
{
  SetState(spIdle); 
}
//
//----------------------------------------------------------
//  Segment - Collector
//----------------------------------------------------------
//
void CProcess::Handle(CSerial &serial)
{
  switch (LASProcess.GetState())
  { // Common
    case spIdle:
      HandleIdle(serial);
      break;
    case spWelcome:
      HandleWelcome(serial);
      break;
    case spGetHelp:
      HandleGetHelp(serial);
      break;
    case spGetProgramHeader:
      HandleGetProgramHeader(serial);
      break;
    case spGetSoftwareVersion:
      HandleGetSoftwareVersion(serial);
      break;
    case spGetHardwareVersion:
      HandleGetHardwareVersion(serial);
      break;
    case spGetProcessCount:
      HandleGetProcessCount(serial);
      break;
    case spSetProcessCount:
      HandleSetProcessCount(serial);
      break;
    case spGetProcessPeriod:
      HandleGetProcessPeriod(serial);
      break;
    case spSetProcessPeriod:
      HandleSetProcessPeriod(serial);
      break;
    case spStopProcessExecution:
      HandleStopProcessExecution(serial);
      break;
    // LedSystem
    case spGetLedSystem:
      HandleGetLedSystem(serial);
      break;
    case spLedSystemOn:
      HandleLedSystemOn(serial);
      break;
    case spLedSystemOff:
      HandleLedSystemOff(serial);
      break;
    case spBlinkLedSystem:
      HandleBlinkLedSystem(serial);
      break;
    // Measurement - LaserPosition
    case spPulseLaserPosition:
      HandlePulseLaserPosition(serial);
      break;
    case spAbortLaserPosition:
      HandleAbortLaserPosition(serial);
      break;
    // Measurement - LaserParameter
    case spGetPositionX:
      HandleGetPositionX(serial);
      break;
    case spSetPositionX:
      HandleSetPositionX(serial);
      break;
    case spGetPositionY:
      HandleGetPositionY(serial);
      break;
    case spSetPositionY:
      HandleSetPositionY(serial);
      break;      
    case spGetRangeX:
      HandleGetRangeX(serial);
      break;
    case spSetRangeX:
      HandleSetRangeX(serial);
      break;
    case spGetRangeY:
      HandleGetRangeY(serial);
      break;
    case spSetRangeY:
      HandleSetRangeY(serial);
      break;
    case spGetDelayMotion:
      HandleGetDelayMotion(serial);
      break;
    case spSetDelayMotion:
      HandleSetDelayMotion(serial);
      break;
    case spGetPulseWidth:
      HandleGetPulseWidthus(serial);
      break;
    case spSetPulseWidth:
      HandleSetPulseWidthus(serial);
      break;
    // Measurement - LaserLine
    case spPulseLaserRow:
      HandlePulseLaserRow(serial);
      break;
    case spAbortLaserRow:
      HandleAbortLaserRow(serial);
      break;
    case spPulseLaserCol:
      HandlePulseLaserCol(serial);
      break;
    case spAbortLaserCol:
      HandleAbortLaserCol(serial);
      break;
    // Measurement - LaserMatrix
    case spPulseLaserMatrix:
      HandlePulseLaserMatrix(serial);
      break;
    case spAbortLaserMatrix:
      HandleAbortLaserMatrix(serial);
      break;
    // Measurement - LaserImage
    case spPulseLaserImage:
      HandlePulseLaserImage(serial);
      break;
    case spAbortLaserImage:
      HandleAbortLaserImage(serial);
      break;
    default: // spUndefined
      HandleUndefined(serial);
      break;
  }  
}
