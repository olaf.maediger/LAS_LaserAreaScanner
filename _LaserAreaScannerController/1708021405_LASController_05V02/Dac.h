//
//--------------------------------
//  Library Dac
//--------------------------------
//
#ifndef Dac_h
#define Dac_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Dac
//--------------------------------
//
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE) 
class CDac
{
  private:
  bool FChannel[2];
  UInt32 FValue[2];
  
  public:
  CDac(bool channel0, bool channel1);
  Boolean Open();
  Boolean Close();
  UInt32 GetValue(UInt8 channel);
  void SetValue(UInt8 channel, UInt32 value);
};
#endif
//
#endif
