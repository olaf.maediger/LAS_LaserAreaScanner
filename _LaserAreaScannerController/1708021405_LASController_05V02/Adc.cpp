//
//--------------------------------
//  Library Adc
//--------------------------------
//
#include "Adc.h"
//
CAdc::CAdc(bool channel0, bool channel1, bool channel2, bool channel3)
{
  FChannel[0] = channel0;
  FChannel[1] = channel1;
  FChannel[2] = channel2;
  FChannel[3] = channel3;
}
CAdc::CAdc(bool channel0, bool channel1, bool channel2, bool channel3,
           bool channel4, bool channel5, bool channel6, bool channel7)
{
  FChannel[0] = channel0;
  FChannel[1] = channel1;
  FChannel[2] = channel2;
  FChannel[3] = channel3;
  FChannel[4] = channel0;
  FChannel[5] = channel1;
  FChannel[6] = channel2;
  FChannel[7] = channel3;
}
CAdc::CAdc(bool channel0, bool channel1, bool channel2, bool channel3,
           bool channel4, bool channel5, bool channel6, bool channel7,
           bool channel8, bool channel9, bool channel10, bool channel11)
{
  FChannel[0] = channel0;
  FChannel[1] = channel1;
  FChannel[2] = channel2;
  FChannel[3] = channel3;
  FChannel[4] = channel4;
  FChannel[5] = channel5;
  FChannel[6] = channel6;
  FChannel[7] = channel7;
  FChannel[8] = channel8;
  FChannel[9] = channel0;
  FChannel[10] = channel10;
  FChannel[11] = channel11;
}

Boolean CAdc::Open()
{
  // ??? analogReadResolution(12);
  return true;
}

Boolean CAdc::Close()
{
  return true;
}

UInt16 CAdc::GetValue(UInt8 channel)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
//      case 0:
//        return analogRead(PIN_ADC0);
//      case 1:
//        return analogRead(PIN_ADC1);
//      case 2:
//        return analogRead(PIN_ADC2);
//      case 3:
//        return analogRead(PIN_ADC3);
//      case 4:
//        return analogRead(PIN_ADC4);
//      case 5:
//        return analogRead(PIN_ADC5);
//      case 6:
//        return analogRead(PIN_ADC6);
//      case 7:
//        return analogRead(PIN_ADC7);
      case 8:
        return analogRead(PIN_ADC8);
      case 9:
        return analogRead(PIN_ADC9);
      case 10:
        return analogRead(PIN_ADC10);
      case 11:
        return analogRead(PIN_ADC11);
    }    
  }
  return 0;
}


