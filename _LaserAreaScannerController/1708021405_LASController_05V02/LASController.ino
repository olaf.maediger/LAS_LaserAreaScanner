#include "Defines.h"
#include "Helper.h"
#include "LedLine.h"
#include "Serial.h"
#include "Dac.h"
#include "SerialCommand.h"
#include "Utilities.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables - Common
//---------------------------------------------------
//
enum EErrorCode GlobalErrorCode;
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
EStateAutomation StateAutomation;
UInt32 CountRepetition; // [1]
UInt32 CountLaserPulses; // [1]
UInt32 DelayMotion; // [us]
UInt32 DelayPulse; // [us]
//
UInt16 XPositionActual; // [stp]
UInt16 XPositionDelta; // [stp]
UInt16 XPositionMinimum; // [stp]
UInt16 XPositionMaximum; // [stp]
//
UInt16 YPositionActual; // [stp]
UInt16 YPositionDelta; // [stp]
UInt16 YPositionMinimum; // [stp]
UInt16 YPositionMaximum; // [stp]
//
CSerialCommand SerialCommand;
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM);
CLedLine LedLine(PIN_LEDTRIGGER, 60, PIN_LEDMOTIONY, PIN_LEDMOTIONX, // 7..4
                 57, PIN_LEDBUSY, 55, PIN_LEDERROR);                 // 3..0
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Dac
//---------------------------------------------------
//
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
CDac DigitalAnalogConverter(true, true);
#endif
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort!
CSerial SerialLan(Serial1, RXDECHO_OFF);
// Serial2 - NC
// Serial3 - NC
//
void setup() 
{ // Led
  LedSystem.Open();
  LedLine.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.Off();
    LedLine.On();
    delay(40);
    LedSystem.On();
    LedLine.Off();
    delay(40);
  }
  LedSystem.Off();
  //
  SerialLan.Open(115200);
  SerialLan.SetRxdEcho(false);
  //
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.Open();
#endif  
  //
  GlobalErrorCode = INIT_GLOBALERRORCODE;
  //
  StateAutomation = INIT_STATEAUTOMATION;
  CountRepetition = INIT_COUNTREPETITION;
  CountLaserPulses = INIT_COUNTLASERPULSES;
  DelayMotion = INIT_DELAYMOTION;
  DelayPulse = INIT_DELAYPULSE;
  //
  XPositionActual = INIT_XPOSITIONACTUAL;
  XPositionDelta = INIT_XPOSITIONDELTA;
  XPositionMinimum = INIT_XPOSITIONMINIMUM;
  XPositionMaximum = INIT_XPOSITIONMAXIMUM;
  //
  YPositionActual = INIT_YPOSITIONACTUAL;
  YPositionDelta = INIT_YPOSITIONDELTA;
  YPositionMinimum = INIT_YPOSITIONMINIMUM;
  YPositionMaximum = INIT_YPOSITIONMAXIMUM;    
  //
  WriteProgramHeader(SerialLan);
  WriteHelp(SerialLan);
  SerialLan.WritePrompt();  
}

void loop() 
{
  HandleError(SerialLan);  
  HandleSerialCommands(SerialLan);
  HandleAutomation(SerialLan);  
  LedSystem.On();
  delay(100);
  LedSystem.Off();
  delay(100);
}


