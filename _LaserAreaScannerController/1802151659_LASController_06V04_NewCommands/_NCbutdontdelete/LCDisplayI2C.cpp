#include "LCDisplayI2C.h"
//
CLCDisplayI2C::CLCDisplayI2C(UInt8 addressi2c, UInt8 colcount, UInt8 rowcount)
{
  FAddressI2C = addressi2c;
  FRowCount = rowcount;
  FColCount = colcount;
  FPLCDisplay = new CDriverLCDisplayI2C(FAddressI2C, FColCount, FRowCount, LCD_5x8DOTS);
}

Boolean CLCDisplayI2C::Open()
{
  FPLCDisplay->Open();
  FPLCDisplay->Clear();
  FPLCDisplay->SetBacklightOn();
  return true;
}

Boolean CLCDisplayI2C::Close()
{
  return true;
}

// ERROR!
//Boolean CLCDisplayI2C::FindAddressI2C()
//{
//  Serial.println("Find I2C-Address:");
//  FAddressI2C = 0x00;
//  for (byte AI = 1; AI < 120; AI++)
//  {
//    delay(100);
//    Wire.beginTransmission(AI);
//    if (0 == Wire.endTransmission())
//    {
//      FAddressI2C = AI;
//      Serial.print("Existing I2C-Address: 0x");
//      Serial.print(DISPLAY_I2CADDRESS, HEX);
//      Serial.print(" 0d");
//      Serial.println(DISPLAY_I2CADDRESS, DEC);
//      delay(100);
//    }
//  }
//  Serial.println("Scanning I2C-Addresses done.");
//}

Byte CLCDisplayI2C::GetAddressI2C()
{
  return FAddressI2C;
}

void CLCDisplayI2C::SetBacklightOn()
{
  FPLCDisplay->SetBacklightOn();
}

void CLCDisplayI2C::SetBacklightOff()
{
  FPLCDisplay->SetBacklightOff();  
}

void CLCDisplayI2C::SetHomePosition()
{
  FPLCDisplay->Home();
}
  
void CLCDisplayI2C::ClearScreen()
{
  FPLCDisplay->Clear();
}

void CLCDisplayI2C::SetCursorPosition(Byte row, Byte col)
{
  FPLCDisplay->SetCursorPosition(col, row);
}

void CLCDisplayI2C::Write(String text)
{
  FPLCDisplay->print(text);
}

void CLCDisplayI2C::WriteByte(Byte value)
{
  FPLCDisplay->print(value, HEX);
}

void CLCDisplayI2C::WriteUInt8(UInt8 value)
{
  FPLCDisplay->print(value, DEC);
}

void CLCDisplayI2C::WriteInt16(Int16 value)
{
  FPLCDisplay->print(value, DEC);  
}



