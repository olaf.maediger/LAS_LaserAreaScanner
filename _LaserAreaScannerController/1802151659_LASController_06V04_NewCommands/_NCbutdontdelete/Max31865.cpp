//
//--------------------------------
//  Library Max31865
//--------------------------------
//
#include "Max31865.h"
//
CMax31865::CMax31865(Byte pinspics)
{
  FPinSpiCS = pinspics;
  FPMax31865 = new CDriverMAX31865(pinspics);
}

Boolean CMax31865::Open(EWireCount wirecount)
{
  FPMax31865->Open(wirecount);
  return true;
}

Boolean CMax31865::Close()
{
  return true;
}

UInt16 CMax31865::ReadRTD()
{
  UInt16 RTD = FPMax31865->ReadRTD();
  return RTD;
}

Double CMax31865::ReadRatio()
{
  Double Ratio = ReadRTD() / 32768.0;
  return Ratio;
}

Double CMax31865::ReadResistanceOhm()
{
  Double Resistance = RREFERENCE * ReadRatio();
  return Resistance;
}

Double CMax31865::ReadTemperatureCelsius()
{
  Double TC = FPMax31865->ReadTemperatureCelsius(RTEMPERATUREZERO, RREFERENCE);
  return TC;
}

Byte CMax31865::ReadFault()
{
  Byte Fault = FPMax31865->ReadFault();
  FPMax31865->ClearFault();
  return Fault;
}



