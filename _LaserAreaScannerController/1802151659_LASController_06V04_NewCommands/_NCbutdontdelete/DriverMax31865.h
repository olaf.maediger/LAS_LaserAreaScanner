#ifndef DriverMAX31865_h
#define DriverMAX31865_h
//
#include <stdlib.h>
#include <SPI.h>
#include "Defines.h"
//
//---------------------------------------------
//  Segment - Constant
//---------------------------------------------
//
#define MAX31856_CONFIG_REG            0x00
#define MAX31856_CONFIG_BIAS           0x80
#define MAX31856_CONFIG_MODEAUTO       0x40
#define MAX31856_CONFIG_MODEOFF        0x00
#define MAX31856_CONFIG_1SHOT          0x20
#define MAX31856_CONFIG_3WIRE          0x10
#define MAX31856_CONFIG_24WIRE         0x00
#define MAX31856_CONFIG_FAULTSTAT      0x02
#define MAX31856_CONFIG_FILT50HZ       0x01
#define MAX31856_CONFIG_FILT60HZ       0x00
//
#define MAX31856_RTDMSB_REG           0x01
#define MAX31856_RTDLSB_REG           0x02
#define MAX31856_HFAULTMSB_REG        0x03
#define MAX31856_HFAULTLSB_REG        0x04
#define MAX31856_LFAULTMSB_REG        0x05
#define MAX31856_LFAULTLSB_REG        0x06
#define MAX31856_FAULTSTAT_REG        0x07
//
#define MAX31865_FAULT_HIGHTHRESH     0x80
#define MAX31865_FAULT_LOWTHRESH      0x40
#define MAX31865_FAULT_REFINLOW       0x20
#define MAX31865_FAULT_REFINHIGH      0x10
#define MAX31865_FAULT_RTDINLOW       0x08
#define MAX31865_FAULT_OVUV           0x04
//
#define RTD_A 3.9083e-3
#define RTD_B -5.775e-7
//
//---------------------------------------------
//  Segment - Type
//---------------------------------------------
//
typedef enum  
{ 
  MAX31865_2WIRE = 0,
  MAX31865_3WIRE = 1,
  MAX31865_4WIRE = 0
} EWireCount;
//
class CDriverMAX31865 
{
  private:
  Int8 FPin_SPI_CSS, FPin_SPI_SCK, FPin_SPI_MISO, FPin_SPI_MOSI;
  //
  UInt8  ReadRegister8(UInt8 address);
  UInt16 ReadRegister16(UInt8 address);
  void ReadRegisterN(UInt8 address, UInt8 data[], UInt8 count);
  void WriteRegister8(UInt8 address, UInt8 data);
  UInt8 SpiTransfer(UInt8 address);
  //
  public:
  // Software SPI
  CDriverMAX31865(Int8 pintspicss, Int8 pinspisck, Int8 pinspimosi, Int8 pinspimiso);
  // Hardware SPI
  CDriverMAX31865(Int8 pinspicss);
  //
  Boolean Open(EWireCount wirecount);
  //
  void SetWires(EWireCount wirecount);
  void SetAutoConvert(Boolean enable);
  void EnableBias(Boolean enable);
  //
  UInt8 ReadFault(void);
  void ClearFault(void);
  UInt16 ReadRTD();
  Float ReadTemperatureCelsius(Float rtdnominal, Float rreference);
};
//
#endif // DriverMAX31865_h
