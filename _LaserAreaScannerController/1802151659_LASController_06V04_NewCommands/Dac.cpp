//
//--------------------------------
//  Library Dac
//--------------------------------
//
#include "Dac.h"
//
CDac::CDac(bool channel0, bool channel1)
{
  FChannel[0] = channel0;
  FChannel[1] = channel1;
  FValue[0] = 0;
  FValue[1] = 0;
}

Boolean CDac::Open()
{
#if defined(PROZESSOR_ARDUINOMEGA) 
  analogWriteResolution(12);
#endif
  return true;
}

Boolean CDac::Close()
{
  return true;
}

UInt32 CDac::GetValue(UInt8 channel)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
      case 0:
        return FValue[0];
      case 1:
        return FValue[1];
    }    
  }
  return 0;
}

void CDac::SetValue(UInt8 channel, UInt32 value)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
      case 0:
        FValue[0] = (0x0FFF & value);
#if defined(PROZESSOR_ARDUINOMEGA)
        analogWriteResolution(12);
#endif
#if (defined(PROZESSOR_ARDUINOMEGA) || defined(PROZESSOR_ARDUINODUE))
        analogWrite(PIN_DAC_CHANNEL0, FValue[0]);
#endif
        break;
      case 1:
        FValue[1] = (0x0FFF & value);
#if defined(PROZESSOR_ARDUINOMEGA)
        analogWriteResolution(12);
#endif
#if (defined(PROZESSOR_ARDUINOMEGA) || defined(PROZESSOR_ARDUINODUE))
        analogWrite(PIN_DAC_CHANNEL1, FValue[1]);
#endif
        break;
    }    
  }
}

void CDac::SetValues(UInt32 value0, UInt32 value1)
{
  FValue[0] = (0x0FFF & value0);
#if defined(PROZESSOR_ARDUINOMEGA)
  analogWriteResolution(12);
#endif
#if (defined(PROZESSOR_ARDUINOMEGA) || defined(PROZESSOR_ARDUINODUE))
  analogWrite(PIN_DAC_CHANNEL0, FValue[0]);
#endif
  FValue[1] = (0x0FFF & value1);
#if defined(PROZESSOR_ARDUINOMEGA)
  analogWriteResolution(12);
#endif
#if (defined(PROZESSOR_ARDUINOMEGA) || defined(PROZESSOR_ARDUINODUE))
  analogWrite(PIN_DAC_CHANNEL1, FValue[1]);
#endif
}

