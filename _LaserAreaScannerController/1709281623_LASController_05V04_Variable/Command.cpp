//
#include "Command.h"
//
//-------------------------------------------
//  External Global Variables 
//-------------------------------------------
//  
extern CLed LedLaser;
extern CDac DigitalAnalogConverter;
extern CError LASError;
extern CProcess LASProcess;
extern CAutomation LASAutomation;
//
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand()
{
  Init();
}

CCommand::~CCommand()
{
  Init();
}
//
//#########################################################
//  Segment - Helper
//#########################################################
//
void CCommand::ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}

void CCommand::ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}

void CCommand::ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdCommandLine[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Management
//#########################################################
//
void CCommand::Init()
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
}

Boolean CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CR:
        FRxdBuffer[FRxdBufferIndex] = ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FRxdCommandLine, FRxdBuffer);
        // debug serial.WriteText("<CR>true");
        // debug delay(1000);
        return true;
      case LF: // ignore
        // debug serial.WriteText("<LF>");
        // debug delay(1000);
        break;
      default: 
        // debug serial.WriteText("<");
        // debug serial.WriteCharacter(C);
        // debug serial.WriteText(">");
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean CCommand::Analyse(CSerial &serial)
{
  if (DetectRxdLine(serial))
  {
    char *PTerminal = " \t\r\n";
    char *PRxdCommandLine = FRxdCommandLine;
    FPRxdCommand = strtok(FRxdCommandLine, PTerminal);
    if (FPRxdCommand)
    {
      FRxdParameterCount = 0;
      char *PRxdParameter;
      while (PRxdParameter = strtok(0, PTerminal))
      {
        FPRxdParameters[FRxdParameterCount] = PRxdParameter;
        FRxdParameterCount++;
        if (COUNT_RXDPARAMETERS < FRxdParameterCount)
        {
          LASError.SetCode(ecToManyParameters);
          ZeroRxdBuffer();
          // debug serialtarget.WriteLine("error[ecToManyParameters]");
          serial.WriteNewLine();
          serial.WritePrompt();
          return false;
        }
      }  
//      debug sprintf(TxdBuffer, "\n\r# RxdCommand<%s>", PRxdCommand);
//      debug serial.WriteText(TxdBuffer);
//      if (0 < RxdParameterCount)
//      {
//        int II;
//        for (II = 0; II < RxdParameterCount; II++)
//        {
//          sprintf(TxdBuffer, " RxdParameter[%i]<%s>", II, PRxdParameters[II]);
//          serial.WriteText(TxdBuffer);
//        }
//      }
      //
      ZeroRxdBuffer();
      serial.WriteNewLine();
      return true;
    }
    ZeroRxdBuffer();
    serial.WriteNewLine();
    serial.WritePrompt();
  }
  return false;
}
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_VERSION);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void CCommand::WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_SOFTWAREVERSION);
  serial.WriteNewLine();
}

void CCommand::WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_HARDWAREVERSION);
  serial.WriteNewLine();
}

void CCommand::WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.WriteLine(HELP_H);
  serial.WriteAnswer(); serial.WriteLine(HELP_GPH);
  serial.WriteAnswer(); serial.WriteLine(HELP_GSV);
  serial.WriteAnswer(); serial.WriteLine(HELP_GHV);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LEDLASER);
  serial.WriteAnswer(); serial.WriteLine(HELP_GLL);
  serial.WriteAnswer(); serial.WriteLine(HELP_LLH);
  serial.WriteAnswer(); serial.WriteLine(HELP_LLL);
  serial.WriteAnswer(); serial.WriteLine(HELP_PLL);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_POS);
  serial.WriteAnswer(); serial.WriteLine(HELP_GPX);
  serial.WriteAnswer(); serial.WriteLine(HELP_SPX);
  serial.WriteAnswer(); serial.WriteLine(HELP_GPY);
  serial.WriteAnswer(); serial.WriteLine(HELP_SPY);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_POSITIONLASER);
  serial.WriteAnswer(); serial.WriteLine(HELP_PPL);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_MACHININGPARAMETER);
  serial.WriteAnswer(); serial.WriteLine(HELP_SRX);
  serial.WriteAnswer(); serial.WriteLine(HELP_SRY);
  serial.WriteAnswer(); serial.WriteLine(HELP_SDM);
  serial.WriteAnswer(); serial.WriteLine(HELP_SDP);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_MATRIXLASER);
  serial.WriteAnswer(); serial.WriteLine(HELP_SML);
  serial.WriteAnswer(); serial.WriteLine(HELP_AML);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_VARIABLELASER);
  serial.WriteAnswer(); serial.WriteLine(HELP_SVL);
  serial.WriteAnswer(); serial.WriteLine(HELP_PVL);
  serial.WriteAnswer(); serial.WriteLine(HELP_AVL);
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
void CCommand::ExecuteGetHelp(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(GetTxdBuffer(), ": %s %i", 
          GetPRxdCommand(), COUNT_HELPLINES);
  serial.WriteLine(GetTxdBuffer());         
  WriteHelp(serial);
  serial.WritePrompt();
}

void CCommand::ExecuteGetProgramHeader(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(GetTxdBuffer(), ": %s %i", 
          GetPRxdCommand(), COUNT_HEADERLINES);
  serial.WriteLine(GetTxdBuffer());   
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void CCommand::ExecuteGetSoftwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(GetTxdBuffer(), ": %s %i", 
          GetPRxdCommand(), COUNT_ANSWERLINE);
  serial.WriteLine(GetTxdBuffer());   
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  sprintf(GetTxdBuffer(), ": %s %i", 
          GetPRxdCommand(), COUNT_ANSWERLINE);
  serial.WriteLine(GetTxdBuffer());   
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LedLaser
//#########################################################
//
void CCommand::ExecuteGetLedLaser(CSerial &serial)
{ // Analyse parameters:
  // Execute:
  int State = LedLaser.GetState();
  // Response:
  sprintf(GetTxdBuffer(), ": %s %i", 
          GetPRxdCommand(), State);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteSwitchLedLaserOn(CSerial &serial)
{  // Analyse parameters:
  // Execute:
  LedLaser.On();
  // Response:
  sprintf(GetTxdBuffer(), ": %s", GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteSwitchLedLaserOff(CSerial &serial)
{ // Analyse parameters:
  // Execute:
  LedLaser.Off();
  // Response:
  sprintf(GetTxdBuffer(), ": %s", GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecutePulseLedLaser(CSerial &serial)
{  // Analyse parameters:
  UInt32 Period = atol(GetPRxdParameters(0));
  UInt32 Count = atol(GetPRxdParameters(1));
  // Execute:
  LASProcess.SetLaserPulsePeriod(Period);
  LASProcess.SetLaserPulseCountActual(0);
  LASProcess.SetLaserPulseCountPreset(Count);
  //
  LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(), 
                               LASProcess.GetLaserPulseCountPreset());
  // Switch ON Automation
  LASAutomation.SetState(saLedLaserPulseBegin);
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu %lu", 
          GetPRxdCommand(), Period, Count);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Position
//#########################################################
//
void CCommand::ExecuteGetPositionX(CSerial &serial)
{ // Analyse parameters: -
  // Execute:
  UInt16 PositionX = 0x0000;
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  PositionX = DigitalAnalogConverter.GetValue(DAC_CHANNELX);
#endif  
  // Response:
  sprintf(GetTxdBuffer(), ": %s %u", 
          GetPRxdCommand(), PositionX);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
void CCommand::ExecuteSetPositionX(CSerial &serial)
{ // Analyse parameters:
  UInt32 PositionX = atol(GetPRxdParameters(0));
  // Execute:
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.SetValue(DAC_CHANNELX, PositionX);
  PositionX = DigitalAnalogConverter.GetValue(DAC_CHANNELX);
#endif  
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu", 
          GetPRxdCommand(), PositionX);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteGetPositionY(CSerial &serial)
{ // Analyse parameters: -
  // Execute:
  UInt16 PositionY = 0x0000;
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  PositionY = DigitalAnalogConverter.GetValue(DAC_CHANNELY);
#endif  
  // Response:
  sprintf(GetTxdBuffer(), ": %s %u", 
          GetPRxdCommand(), PositionY);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
void CCommand::ExecuteSetPositionY(CSerial &serial)
{ // Analyse parameters:
  UInt32 PositionY = atol(GetPRxdParameters(0));
  // Execute:
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.SetValue(DAC_CHANNELY, PositionY);
  PositionY = DigitalAnalogConverter.GetValue(DAC_CHANNELY);
#endif  
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu", 
          GetPRxdCommand(), PositionY);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - PositionLaser
//#########################################################
//
void CCommand::ExecutePulsePositionLaser(CSerial &serial)
{ // Analyse parameters:
  UInt32 PositionX = atol(GetPRxdParameters(0));
  UInt32 PositionY = atol(GetPRxdParameters(1));
  UInt32 PulsePeriod = atol(GetPRxdParameters(2));
  UInt32 PulseCount = atol(GetPRxdParameters(3));
  // Execute:
  LASProcess.SetXPositionActual(PositionX);
  LASProcess.SetYPositionActual(PositionY);
  LASProcess.SetLaserPulsePeriod(PulsePeriod);
  LASProcess.SetLaserPulseCountPreset(PulseCount);  
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
  DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());
#endif
  LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(), 
                               LASProcess.GetLaserPulseCountPreset());
  // Back
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  PositionX = DigitalAnalogConverter.GetValue(DAC_CHANNELX);
  PositionY = DigitalAnalogConverter.GetValue(DAC_CHANNELY);
#endif
  PulsePeriod = LedLaser.GetPulsePeriod();
  PulseCount = LedLaser.GetPulseCountPreset();
  // Switch ON Automation
  LASAutomation.SetState(saPulsePositionLaserBegin);
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu %lu %lu %lu", 
          GetPRxdCommand(), PositionX, PositionY, PulsePeriod, PulseCount);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Machining Parameter
//#########################################################
//
void CCommand::ExecuteSetRangeParameterX(CSerial &serial)
{ // Analyse parameters:
  UInt32 XPL = atol(GetPRxdParameters(0));
  UInt32 XPH = atol(GetPRxdParameters(1));
  UInt32 XPD = atol(GetPRxdParameters(2));
  // Execute:
  LASProcess.SetXPositionMinimum(XPL);
  LASProcess.SetXPositionMaximum(XPH);
  LASProcess.SetXPositionDelta(XPD);
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu %lu %lu", 
          GetPRxdCommand(), XPL, XPH, XPD);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteSetRangeParameterY(CSerial &serial)
{ // Analyse parameters:
  UInt32 YPL = atol(GetPRxdParameters(0));
  UInt32 YPH = atol(GetPRxdParameters(1));
  UInt32 YPD = atol(GetPRxdParameters(2));
  // Execute:
  LASProcess.SetYPositionMinimum(YPL);
  LASProcess.SetYPositionMaximum(YPH);
  LASProcess.SetYPositionDelta(YPD);
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu %lu %lu", 
          GetPRxdCommand(), YPL, YPH, YPD);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteSetRangeDelayMotion(CSerial &serial)
{ // Analyse parameters:
  UInt32 DM = atol(GetPRxdParameters(0));
  // Execute:
  LASProcess.SetRangeDelayMotion(DM);
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu", GetPRxdCommand(), DM);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteSetRangeDelayPulse(CSerial &serial)
{ // Analyse parameters:
  UInt32 DP = atol(GetPRxdParameters(0));
  // Execute:
  LASProcess.SetRangeDelayPulse(DP);
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu", GetPRxdCommand(), DP);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - MatrixLaser
//#########################################################
//
void CCommand::ExecuteStartMatrixLaser(CSerial &serial)
{ // Analyse parameters:
  UInt32 PulsePeriod = atol(GetPRxdParameters(0));
  UInt32 PulseCount = atol(GetPRxdParameters(1));
  UInt32 Repetitions = atol(GetPRxdParameters(2));
  // Execute:
  LASProcess.SetLaserPulsePeriod(PulsePeriod);
  LASProcess.SetLaserPulseCountPreset(PulseCount);
  LASProcess.SetLaserPulseCountActual(0);
  LASProcess.SetMatrixCountRepetitions(Repetitions);
  // 
//#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
//  DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionMinimum());
//  DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionMinimum());
//#endif
//  LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(), 
//                               LASProcess.GetLaserPulseCountPreset());
  // Switch ON Automation
  LASAutomation.SetState(saPulseMatrixLaserBegin);
  // Back
//  PulsePeriod = LedLaser.GetPulsePeriod();
//  PulseCount = LedLaser.GetPulseCountPreset();
//  Repetitions = LASProcess.GetMatrixCountRepetitions();
  PulsePeriod = LASProcess.GetLaserPulsePeriod();
  PulseCount = LASProcess.GetLaserPulseCountPreset();
  Repetitions = LASProcess.GetMatrixCountRepetitions();
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu %lu %lu", 
          GetPRxdCommand(), PulsePeriod, PulseCount, Repetitions);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteAbortMatrixLaser(CSerial &serial)
{ // Analyse parameters: -
  // Execute:
  // Switch ON Automation
  LASAutomation.SetState(saPulseMatrixLaserAbort);
  // Response:
  sprintf(GetTxdBuffer(), ": %s", GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - VariableLaser
//#########################################################
// SVL -> Begin -> Busy
void CCommand::ExecuteStartVariableLaser(CSerial &serial)
{ // Analyse parameters: -
  // Execute: -
  // Switch ON Automation
  LASAutomation.SetState(saPulseVariableLaserBegin);
  // Response:
  sprintf(GetTxdBuffer(), ": %s", GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

// Busy -> PVL x y p c -> Busy
void CCommand::ExecutePulseVariableLaser(CSerial &serial)
{ // Analyse parameters:
  UInt32 PositionX = atol(GetPRxdParameters(0));
  UInt32 PositionY = atol(GetPRxdParameters(1));
  UInt32 PulsePeriod = atol(GetPRxdParameters(2));
  UInt32 PulseCount = atol(GetPRxdParameters(3));
  // -> Process (secure data of this LaserLocation!)
  LASProcess.SetXPositionActual(PositionX);
  LASProcess.SetYPositionActual(PositionY);
  LASProcess.SetLaserPulsePeriod(PulsePeriod);
  LASProcess.SetLaserPulseCountPreset(PulseCount);
  // Execute:
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
  DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());
#endif  
  LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(),
                               LASProcess.GetLaserPulseCountPreset());
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  PositionX = DigitalAnalogConverter.GetValue(DAC_CHANNELX);
  PositionY = DigitalAnalogConverter.GetValue(DAC_CHANNELY);
#endif  
  PulsePeriod = LedLaser.GetPulsePeriod();
  PulseCount = LedLaser.GetPulseCountPreset();
  // Switch ON Automation
  LedLaser.Off();
  //!!!!!!!!!!!!!!!!!!!!LASAutomation.SetTimeStamp();
  LASAutomation.SetState(saPulseVariableLaserWait);
  // Response:
  sprintf(GetTxdBuffer(), ": %s %lu %lu %lu %lu", 
          GetPRxdCommand(), PositionX, PositionY, PulsePeriod, PulseCount);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

// Busy -> AVL -> Abort -> End -> Idle
void CCommand::ExecuteAbortVariableLaser(CSerial &serial)
{ // Analyse parameters: -
  // Execute:
  LedLaser.Off();
  // Switch ON Automation
  LASAutomation.SetState(saPulseVariableLaserAbort);
  // Response:
  sprintf(GetTxdBuffer(), ": %s", GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
Boolean CCommand::Execute(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
// debug serialtarget.WriteLine(TxdBuffer);
  if (!strcmp("H", GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp("GPH", GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp("GSV", GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp("GHV", GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  // ----------------------------------
  // LedLaser
  // ---------------------------------- 
  if (!strcmp("GLL", GetPRxdCommand()))
  {
    ExecuteGetLedLaser(serial);
    return true;
  } else 
  if (!strcmp("LLH", GetPRxdCommand()))
  {
    ExecuteSwitchLedLaserOn(serial);
    return true;
  } else   
  if (!strcmp("LLL", GetPRxdCommand()))
  {
    ExecuteSwitchLedLaserOff(serial);
    return true;
  } else   
  if (!strcmp("PLL", GetPRxdCommand()))
  {
    ExecutePulseLedLaser(serial);
    return true;
  } else   
  // ----------------------------------
  // Position
  // ----------------------------------
  if (!strcmp("GPX", GetPRxdCommand()))
  {
    ExecuteGetPositionX(serial);
    return true;
  } else   
  if (!strcmp("SPX", GetPRxdCommand()))
  {
    ExecuteSetPositionX(serial);
    return true;
  } else   
  if (!strcmp("GPY", GetPRxdCommand()))
  {
    ExecuteGetPositionY(serial);
    return true;
  } else   
  if (!strcmp("SPY", GetPRxdCommand()))
  {
    ExecuteSetPositionY(serial);
    return true;
  } else   
  // ----------------------------------
  // PositionLaser
  // ----------------------------------
  if (!strcmp("PPL", GetPRxdCommand()))
  {
    ExecutePulsePositionLaser(serial);
    return true;
  } else   
  // ----------------------------------
  // Machining Parameter
  // ----------------------------------
  if (!strcmp("SRX", GetPRxdCommand()))
  {
    ExecuteSetRangeParameterX(serial);
    return true;
  } else   
  if (!strcmp("SRY", GetPRxdCommand()))
  {
    ExecuteSetRangeParameterY(serial);
    return true;
  } else   
  if (!strcmp("SDM", GetPRxdCommand()))
  {
    ExecuteSetRangeDelayMotion(serial);
    return true;
  } else   
  if (!strcmp("SDP", GetPRxdCommand()))
  {
    ExecuteSetRangeDelayPulse(serial);
    return true;
  } else   
  // ----------------------------------
  // MatrixLaser
  // ----------------------------------  
  if (!strcmp("SML", GetPRxdCommand()))
  {
    ExecuteStartMatrixLaser(serial);
    return true;
  } else   
  if (!strcmp("AML", GetPRxdCommand()))
  {
    ExecuteAbortMatrixLaser(serial);
    return true;
  } else     
  // ----------------------------------
  // VariableLaser
  // ----------------------------------
  if (!strcmp("SVL", GetPRxdCommand()))
  {
    ExecuteStartVariableLaser(serial);
    return true;
  } else   
  if (!strcmp("PVL", GetPRxdCommand()))
  {
    ExecutePulseVariableLaser(serial);
    return true;
  } else   
  if (!strcmp("AVL", GetPRxdCommand()))
  {
    ExecuteAbortVariableLaser(serial);
    return true;
  } else     
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    LASError.SetCode(ecInvalidCommand);
  }
  return false;  
}

Boolean CCommand::Handle(CSerial &serial)
{
  if (Analyse(serial))
  {
    return Execute(serial);
  }  
  return false;
}

















//
//#########################################################
//  Segment - Execution - Helper
//#########################################################
//
//void CCommand::SetPosition()
//{
//#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
//  DigitalAnalogConverter.SetValue(DAC_MIRRORX, XPositionActual);
//  DigitalAnalogConverter.SetValue(DAC_MIRRORY, YPositionActual);  
//#endif
//}
//
//void SetTrigger()
//{
//  LedLine.On(0x01 << LED_TRIGGER);
//  delayMicroseconds(DelayPulse / 2);
//  LedLine.Off(0x01 << LED_MOTIONY);
//  LedLine.Off(0x01 << LED_MOTIONX);
//  delayMicroseconds(DelayPulse / 2);
//  LedLine.Off(0x01 << LED_TRIGGER);
//}
//
//void TriggerLaserPulses()
//{ // Pulsedelay fixed to 1000us!
//  LedLine.Off(0x01 << LED_MOTIONY);
//  LedLine.Off(0x01 << LED_MOTIONX);
//  // wait for motion stops...
//  delayMicroseconds(DelayMotion);
//  //
//  UInt32 TI;
//  UInt32 DL = DelayPulse / 10;
//  UInt32 DH = 9* DL;
//  for (TI = 0; TI < CountLaserPulses; TI++)
//  {    
//    LedLine.On(0x01 << LED_TRIGGER);
//    delayMicroseconds(DH);
//    LedLine.Off(0x01 << LED_TRIGGER);
//    delayMicroseconds(DL);
//  }
//}
//
//#########################################################
//  Segment - Execution - Automation
//#########################################################
//
//void ExecuteSetMotionX(CSerial &serial)
//{ // Analyse parameters:
//  XPositionMinimum = abs(atoi(Command.GetPRxdParameters(0)));
//  XPositionMaximum = abs(atoi(Command.GetPRxdParameters(1)));
//  XPositionDelta = abs(atoi(Command.GetPRxdParameters(2)));
//  // Execute -
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %lu %lu %lu", 
//          Command.GetPRxdCommand(), XPositionMinimum, XPositionMaximum, XPositionDelta);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//void ExecuteSetMotionY(CSerial &serial)
//{ // Analyse parameters:
//  YPositionMinimum = abs(atoi(Command.GetPRxdParameters(0)));
//  YPositionMaximum = abs(atoi(Command.GetPRxdParameters(1)));
//  YPositionDelta = abs(atoi(Command.GetPRxdParameters(2)));
//  // Execute -
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %lu %lu %lu", 
//          Command.GetPRxdCommand(), YPositionMinimum, YPositionMaximum, YPositionDelta);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//void ExecuteSetDelayMotion(CSerial &serial)
//{ // Analyse parameters:
//  DelayMotion = abs(atoi(Command.GetPRxdParameters(0)));
//  // Execute -
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %lu", 
//          Command.GetPRxdCommand(), DelayMotion);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//void ExecuteSetDelayPulse(CSerial &serial)
//{ // Analyse parameters:
//  DelayPulse = abs(atoi(Command.GetPRxdParameters(0)));
//  // Execute -
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %lu", 
//          Command.GetPRxdCommand(), DelayPulse);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//bool AbortTrigger()
//{
//  StateAutomation = saIdle;
//  LedLine.Off(0x01 << LED_MOTIONY);
//  LedLine.Off(0x01 << LED_MOTIONX);
//  LedLine.Off(0x01 << LED_BUSY);
//  return true;
//}
//
//void ExecuteAbortTrigger(CSerial &serial)
//{ // Analyse parameters -
//  // Execute:
//  AbortTrigger();
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ":ABT");
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//Boolean StartTrigger()
//{
//  if (0 < CountRepetition)
//  {
//    LedLine.On(0x01 << LED_MOTIONY);
//    LedLine.On(0x01 << LED_MOTIONX);
//    LedLine.On(0x01 << LED_BUSY);
//    XPositionActual = XPositionMinimum;
//    YPositionActual = YPositionMinimum;
//    StateAutomation = saTriggerRowColLeft;
//    return true;
//  }
//  return false;
//}
//
//void CCommand::ExecuteStartTrigger(CSerial &serial)
//{ // Analyse parameters:
//  CountLaserPulses = abs(atoi(Command.GetPRxdParameters(0)));
//  CountRepetition = abs(atoi(Command.GetPRxdParameters(1)));
//  // Execute:
//  StartTrigger();
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %lu %lu", 
//          Command.GetPRxdCommand(), CountLaserPulses, CountRepetition);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}



