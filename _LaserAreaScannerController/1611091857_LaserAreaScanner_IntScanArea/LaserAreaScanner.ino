//
#define BASE_10 10
//
#define BAUDRATE_SERIAL 115200
//
#define PIN_LEDSYSTEM 13
//
// Global Variable - Serial
String FRxdBuffer;
String FRxdHeader;
String FRxdCommand;
String FRxdParameter1;
String FRxdParameter2;
char TxdBuffer[14];
//
// Global Variable - LedSystem
bool FStateLedSystem = 0;
//
// Global Variable - Area
bool FStateMotion = false;
int FPixelWidth = 10;
int FPixelHeight = 10;
int FPixelX = 0;
int FPixelY = 0;
int FPixelDelayMs = 0;
//
// Global Variable - 
// NC byte FLoopStates[] = { 0xAA, 0x55 };
// NC int FLoopStateIndex = 0;
// NC int FLoopDelay = 1000;
// NC int FLoopCountPreset = 0;
// NC int FLoopCountActual = 0;
// NC int FLoopActive = false;
//
//----------------------------------------------
//  Segment - LedSystem
//----------------------------------------------
//
bool SetSystemLed(String state)
{
  FStateLedSystem = abs(state.toInt());
  if (0 == FStateLedSystem)
  {
    digitalWrite(PIN_LEDSYSTEM, LOW);
    return true;
  }
  else
    if (1 == FStateLedSystem)
    {
      digitalWrite(PIN_LEDSYSTEM, HIGH);
      return true;
    }
  return false;
}

void HandleSetLedSystem(String parameter1)
{
  if (SetSystemLed(parameter1))
  {
    itoa(FStateLedSystem, TxdBuffer, BASE_10);
    Comment();
    SerialUSB.print("SSL ");
    SerialUSB.print(TxdBuffer);
    return;
  }
  Error("Invalid Parameter");
}
//
//----------------------------------------------
//  Segment - Area
//----------------------------------------------
//
bool SetAreaSize(String pixelwidth, String pixelheight)
{
  FPixelWidth = abs(pixelwidth.toInt());
  FPixelHeight = abs(pixelheight.toInt());
  if (0 < FPixelWidth)
  {
    if (0 < FPixelHeight)
    {
      return true;
    }
  }
  return false;
}

void HandleSetAreaSize(String parameter1, String parameter2)
{
  if (SetAreaSize(parameter1, parameter2))
  {
    Comment();
    SerialUSB.print("SSL ");
    itoa(FPixelWidth, TxdBuffer, BASE_10);
    SerialUSB.print(TxdBuffer);
    SerialUSB.print(" ");
    itoa(FPixelHeight, TxdBuffer, BASE_10);
    SerialUSB.print(TxdBuffer);
    return;
  }
  Error("Invalid Parameter");
}

bool StartMotion(String pixeldelayms)
{
  FPixelDelayMs = abs(pixeldelayms.toInt());
  FPixelX = 0;
  FPixelY = 0;
  if (0 < FPixelWidth)
  {
    if (0 < FPixelHeight)
    {
      FStateMotion = true;
      return true;
    }
  }
  FStateMotion = false;
  return false;
}

void HandleStartMotion(String parameter1)
{
  if (StartMotion(parameter1))
  {
    Comment();
    SerialUSB.print("STM ");
    itoa(FPixelDelayMs, TxdBuffer, BASE_10);
    SerialUSB.print(TxdBuffer);
    return;
  }
  Error("Invalid Parameter");
}

bool AbortMotion()
{
  FStateMotion = false;
  return true;
}

void HandleAbortMotion()
{
  AbortMotion();
  Comment();
  SerialUSB.print("ABM ");
}



void SetPosition()
{
}

void SetTrigger()
{
  digitalWrite(PIN_LEDSYSTEM, HIGH);
  delay(100);
  digitalWrite(PIN_LEDSYSTEM, LOW);
  delay(1);
}


//
//----------------------------------------------
//  Segment - Serial Command
//----------------------------------------------
//
void Prompt()
{
  SerialUSB.print("\r\n>");
}

void Comment()
{
  SerialUSB.print("\r\n#");
}

void Error(String text)
{
  Prompt();
  SerialUSB.print("Error: " + text + "!");
}

void HandleHeader()
{
  SerialUSB.print("\r\n");
  Comment();
  SerialUSB.print("Project..: LAS - LaserAreaScanner");
  Comment();
  SerialUSB.print("Version..: 1.1");
  Comment();
  SerialUSB.print("Date.....: 161108");
  Comment();
  SerialUSB.print("Time.....: 1840");
  Comment();
  SerialUSB.print("Author...: OM");
  Prompt();  
}

void HandleHelp()
{
  Comment();
  SerialUSB.print(" H : This Help");
  Comment();
  SerialUSB.print(" SSL <0..1> : Set SystemLed off/on");
  Comment();
  SerialUSB.print(" SAS <1..200> <1..200> : Set Area size <width> <height>");
  Comment();
  SerialUSB.print(" STM <pixeldelay>: Start Motion with <pixeldelay> for each pixel");
  Comment();
  SerialUSB.print(" ABM : Abort Motion");
  Comment();
}

bool AnalyseRxdCommandParameter()
{
  int L = FRxdBuffer.length();
  if (0 < L)
  {
    String Buffer = "";
    FRxdCommand = "";
    FRxdParameter1 = "";
    FRxdParameter2 = "";
    for (int CI = 0; CI < L; CI++)
    {
      char C = FRxdBuffer[CI];
      switch (C)
      {
        case ' ':
          if (FRxdCommand == "")
          {
            Buffer.toUpperCase();
            FRxdCommand = Buffer;
            Buffer = "";
          } else
            if (FRxdParameter1 == "")
            {
              FRxdParameter1 = Buffer;
              Buffer = "";
            } else
              if (FRxdParameter2 == "")
              {
                FRxdParameter2 = Buffer;
                Buffer = "";
              }
          break;
        default:
          Buffer += C;
      }
    }
    if (0 < Buffer.length())
    {
      if (FRxdCommand == "")
      {
        Buffer.toUpperCase();
        FRxdCommand = Buffer;
      } else
        if (FRxdParameter1 == "")
        {
          FRxdParameter1 = Buffer;
        } else
          if (FRxdParameter2 == "")
          {
            FRxdParameter2 = Buffer;
          }
    }
    return true;
  }
  return false;
}

void CommandDispatcher(String command, String parameter1, String parameter2)
{
  if (command == "H")
  {
    HandleHelp();
    return;
  }
  // Led
  if (command == "SSL")
  {
    HandleSetLedSystem(parameter1);
    return;
  }
  // Area
  if (command == "SAS")
  {
    HandleSetAreaSize(parameter1, parameter2);
    return;
  }
  if (command == "STM")
  {
    HandleStartMotion(parameter1); 
    return;
  }
  if (command == "ABM")
  {
    HandleAbortMotion(); 
    return;
  }
  Error("Invalid Command");  
}

//
//----------------------------------------------
//  Segment - Main Loop
//----------------------------------------------
//
void CommandLoop()
{
  String TxdLine;  
  if (SerialUSB.available())
  {
    char C = SerialUSB.read();
    switch (C)
    {
      case '\n': // nothing
        break;
      case '\r': // Command  + Parameter
        if (AnalyseRxdCommandParameter())
        {
          if (0 < FRxdCommand.length())
          {
            Comment();
            TxdLine = "Command[" + FRxdCommand + "]";
            if (0 < FRxdParameter1.length())
            {
              TxdLine += " Parameter1[" + FRxdParameter1 + "]";
            }
            if (0 < FRxdParameter2.length())
            {
              TxdLine += " Parameter2[" + FRxdParameter2 + "]";
            }
            SerialUSB.print(TxdLine);
            // Command-Action...
            CommandDispatcher(FRxdCommand, FRxdParameter1, FRxdParameter2);
            // Reinit
            FRxdCommand = "";
            FRxdParameter1 = "";
            FRxdParameter2 = "";            
          }
        } else
        {
          Error("Invalid Command");
        }
        Prompt();
        FRxdBuffer = "";
        break;
      default:
        FRxdBuffer += C;
        // Echo:
        SerialUSB.write(C);
        break;
    }
  }
}

void ExecutionLoop()
{
  if (FStateMotion)
  {
    if (FPixelX < FPixelWidth - 1)
    {
      FPixelX++;
    }
    else
    {
      FPixelX = 0;
    }
    if (FPixelY < FPixelHeight - 1)
    {
      FPixelY++;
    }
    else
    {
      FPixelY = 0;
    } 
    //
    SetPosition();
    //
    SetTrigger();
    //
    delay(FPixelDelayMs);
  }
}

void setup() 
{ // LedSystem
  pinMode(PIN_LEDSYSTEM, OUTPUT);
  int I;
  for (I = 0; I < 6; I++)
  {
    digitalWrite(PIN_LEDSYSTEM, HIGH);
    delay(200);
    digitalWrite(PIN_LEDSYSTEM, LOW);
    delay(200);
  }
  // Serial
  SerialUSB.begin(BAUDRATE_SERIAL);
  //
  HandleHeader();
  HandleHelp();
  Prompt();  
  FStateMotion = false;  
}

void loop() 
{
  CommandLoop();
  ExecutionLoop();
}



