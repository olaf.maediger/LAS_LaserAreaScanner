#include "Utilities.h"
#include "Led.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
//
//
//-------------------------------------------
//  Segment - Global Variable
//-------------------------------------------
//  
extern enum EErrorCode GlobalErrorCode;
//
extern EStateAutomation StateAutomation;
extern UInt32 CountRepetition; // [1]
extern UInt32 CountLaserPulses; // [1]
extern UInt32 DelayMotion; // [us]
extern UInt32 DelayPulse; // [us]
//
extern UInt16 XPositionActual; // [stp]
extern UInt16 XPositionDelta; // [stp]
extern UInt16 XPositionMinimum; // [stp]
extern UInt16 XPositionMaximum; // [stp]
//
extern UInt16 YPositionActual; // [stp]
extern UInt16 YPositionDelta; // [stp]
extern UInt16 YPositionMinimum; // [stp]
extern UInt16 YPositionMaximum; // [stp]
//
//-------------------------------------------
//  Segment - Global Class
//-------------------------------------------
//  
extern CLed LedSystem;
extern CLed LedError;
extern CLed LedBusy;
extern CLed LedMotionX;
extern CLed LedMotionY;
extern CLed LedTrigger;
//
// NC extern CAdc AnalogDigitalConverter;
//
extern CDac DigitalAnalogConverter;
//
// NC extern CPwm PulseWidthModulator;
//
extern CSerialCommand SerialCommand;
//
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_VERSION);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();	
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_SOFTWAREVERSION);
  serial.WriteNewLine();
}

void WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_HARDWAREVERSION);
  serial.WriteNewLine();
}

void WriteHelp(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_COMMON);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_H);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_GPH);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_GSV);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_GHV);
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(HELP_LED);
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(HELP_GLD);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_SLD);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_CLD);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_TLD);
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(HELP_DAC);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_GDV);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SDV);
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(HELP_AUTOMATION);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SMX);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SMY);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SDM);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SDP);
  serial.WriteNewLine();
  serial.WriteAnswer();    
//  serial.WriteText(HELP_STM);
//  serial.WriteNewLine();
//  serial.WriteAnswer();    
  serial.WriteText(HELP_STT);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_ABM" ABM : Abort Motion");
  serial.WriteNewLine();
}
//
//#########################################################
//  Segment - Execution - Common
//#########################################################
//
void ExecuteGetHelp(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHelp(serial);
  serial.WritePrompt();
}

void ExecuteGetProgramHeader(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void ExecuteGetSoftwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void ExecuteGetHardwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Led
//#########################################################
//
void ExecuteGetLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  int State;
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      State = LedSystem.GetState();
      break;
    case LED_ERROR:
      State = LedError.GetState();
      break;
    case LED_BUSY:
      State = LedBusy.GetState();
      break;
    case LED_MOTIONX:
      State = LedMotionX.GetState();
      break;
    case LED_MOTIONY:
      State = LedMotionY.GetState();
      break;
    case LED_TRIGGER:
      State = LedTrigger.GetState();
      break;
  }
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s %i", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0), State);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteClearLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.Off();
      break;
    case LED_ERROR:
      LedError.Off();
      break;
    case LED_BUSY:
      LedBusy.Off();
      break;
    case LED_MOTIONX:
      LedMotionX.Off();
      break;
    case LED_MOTIONY:
      LedMotionY.Off();
      break;
    case LED_TRIGGER:
      LedTrigger.Off();
      break;
  }
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0));
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSetLed(CSerial &serial)
{	// Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.On();
      break;
    case LED_ERROR:
      LedError.On();
      break;
    case LED_BUSY:
      LedBusy.On();
      break;
    case LED_MOTIONX:
      LedMotionX.On();
      break;
    case LED_MOTIONY:
      LedMotionY.On();
      break;
    case LED_TRIGGER:
      LedTrigger.On();
      break;
  }
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0));
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteToggleLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  int Delay = atoi(SerialCommand.GetPRxdParameters(1));
  int Count = atoi(SerialCommand.GetPRxdParameters(2));
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0));
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
  // Execute:
  int I;
  for (I = 0; I < Count - 1; I++)
  {
    switch (Index)
    {
      case LED_SYSTEM:
        LedSystem.Toggle();
        delay(Delay);
        break;
      case LED_ERROR:
        LedError.Toggle();
        delay(Delay);
        break;
      case LED_BUSY:
        LedBusy.Toggle();
        delay(Delay);
        break;
      case LED_MOTIONX:
        LedMotionX.Toggle();
        delay(Delay);
        break;
      case LED_MOTIONY:
        LedMotionY.Toggle();
        delay(Delay);
        break;
      case LED_TRIGGER:
        LedTrigger.Toggle();
        delay(Delay);
        break;
    }
  }
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.Toggle();
      break;
    case LED_ERROR:
      LedError.Toggle();
      break;
    case LED_BUSY:
      LedBusy.Toggle();
      break;
    case LED_MOTIONX:
      LedMotionX.Toggle();
      break;
    case LED_MOTIONY:
      LedMotionY.Toggle();
      break;
    case LED_TRIGGER:
      LedTrigger.Toggle();
      break;
  }
}
////
////#########################################################
////  Segment - Execution - Adc
////#########################################################
////
//void ExecuteGetAdcValue(CSerial &serial)
//{  // Analyse parameters:
//  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
//  // Execute:
//  UInt16 Value = AnalogDigitalConverter.GetValue(Channel);
//  // Answer:
//  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %u", 
//            SerialCommand.GetPRxdCommand(), Channel, Value);
//    serial.WriteLine(SerialCommand.GetTxdBuffer());
//  serial.WritePrompt();
//}
//
//#########################################################
//  Segment - Execution - Dac
//#########################################################
//
void ExecuteGetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  UInt16 Value = DigitalAnalogConverter.GetValue(Channel);
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %u", 
          SerialCommand.GetPRxdCommand(), Channel, Value);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
  UInt16 Value = atoi(SerialCommand.GetPRxdParameters(1));
  // Execute:
  DigitalAnalogConverter.SetValue(Channel, Value);
  Value = DigitalAnalogConverter.GetValue(Channel);
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %u", 
          SerialCommand.GetPRxdCommand(), Channel, Value);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}
////
////#########################################################
////  Segment - Execution - Pwm
////#########################################################
////
//void ExecuteGetPwmValue(CSerial &serial)
//{  // Analyse parameters:
//  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
//  // Execute:
//  UInt16 Value = PulseWidthModulator.GetValue(Channel);
//  // Answer:
//  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %u", PRxdCommand, Channel, Value);
//    serial.WriteLine(SerialCommand.GetTxdBuffer());
//  serial.WritePrompt();
//}
//
//void ExecuteSetPwmValue(CSerial &serial)
//{  // Analyse parameters:
//  UInt8 Channel = atoi(PRxdParameters[0]);
//  UInt16 Value = atoi(PRxdParameters[1]);
//  // Execute:
//  PulseWidthModulator.SetValue(Channel, Value);
//  Value = PulseWidthModulator.GetValue(Channel);
//  // Answer:
//  sprintf(SerialCommand.GetTxdBuffer()r, "# %s %u %u", PRxdCommand, Channel, Value);
//  serial.WriteLine(SerialCommand.GetTxdBuffer());
//  serial.WritePrompt();
//}
//
//#########################################################
//  Segment - Execution - Helper
//#########################################################
//
void SetPosition()
{
  DigitalAnalogConverter.SetValue(DAC_MIRRORX, XPositionActual);
  DigitalAnalogConverter.SetValue(DAC_MIRRORY, YPositionActual);  
}

void SetTrigger()
{
  LedTrigger.On();
  delayMicroseconds(DelayPulse / 2);
  LedMotionY.Off();
  LedMotionX.Off();
  delayMicroseconds(DelayPulse / 2);
  LedTrigger.Off();
}

void TriggerLaserPulses()
{ // Pulsedelay fixed to 1000us!
  LedMotionY.Off();
  LedMotionX.Off();
  // wait for motion stops...
  delayMicroseconds(DelayMotion);
  //
  UInt32 TI;
  UInt32 DL = DelayPulse / 10;
  UInt32 DH = 9* DL;
  for (TI = 0; TI < CountLaserPulses; TI++)
  {    
    LedTrigger.On();
    delayMicroseconds(DH);
    LedTrigger.Off();
    delayMicroseconds(DL);
  }
}
//
//#########################################################
//  Segment - Execution - Automation
//#########################################################
//
void ExecuteSetMotionX(CSerial &serial)
{ // Analyse parameters:
  XPositionMinimum = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  XPositionMaximum = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  XPositionDelta = abs(atoi(SerialCommand.GetPRxdParameters(2)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu %lu", 
          SerialCommand.GetPRxdCommand(), XPositionMinimum, XPositionMaximum, XPositionDelta);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

void ExecuteSetMotionY(CSerial &serial)
{ // Analyse parameters:
  YPositionMinimum = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  YPositionMaximum = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  YPositionDelta = abs(atoi(SerialCommand.GetPRxdParameters(2)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu %lu", 
          SerialCommand.GetPRxdCommand(), YPositionMinimum, YPositionMaximum, YPositionDelta);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

void ExecuteSetDelayMotion(CSerial &serial)
{ // Analyse parameters:
  DelayMotion = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu", 
          SerialCommand.GetPRxdCommand(), DelayMotion);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

void ExecuteSetDelayPulse(CSerial &serial)
{ // Analyse parameters:
  DelayPulse = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu", 
          SerialCommand.GetPRxdCommand(), DelayPulse);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

bool AbortMotion()
{
  StateAutomation = saIdle;
  LedMotionY.Off();
  LedMotionX.Off();
  LedBusy.Off();
  return true;
}

void ExecuteAbortMotion(CSerial &serial)
{ // Analyse parameters -
  // Execute:
  AbortMotion();
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# ABM");
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

Boolean StartMotion()
{
  if (0 < CountRepetition)
  {
    LedMotionY.On();
    LedMotionX.On();
    LedBusy.On();
    XPositionActual = XPositionMinimum;
    YPositionActual = YPositionMinimum;
    StateAutomation = saMotionRowColLeft;
    return true;
  }
  return false;
}

void ExecuteStartMotion(CSerial &serial)
{ // Analyse parameters:
  DelayPulse = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  CountRepetition = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  // Execute:
  StartMotion();
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu", 
          SerialCommand.GetPRxdCommand(), DelayPulse, CountRepetition);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

Boolean StartTrigger()
{
  if (0 < CountRepetition)
  {
    LedMotionY.On();
    LedMotionX.On();
    LedBusy.On();
    XPositionActual = XPositionMinimum;
    YPositionActual = YPositionMinimum;
    StateAutomation = saTriggerRowColLeft;
    return true;
  }
  return false;
}

void ExecuteStartTrigger(CSerial &serial)
{ // Analyse parameters:
  CountLaserPulses = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  CountRepetition = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  // Execute:
  StartTrigger();
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu", 
          SerialCommand.GetPRxdCommand(), CountLaserPulses, CountRepetition);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//#########################################################
//  Segment - Handler - Error
//#########################################################
//
void HandleError(CSerial &serial)
{
  if (ecNone != GlobalErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(GlobalErrorCode, SerialCommand.GetTxdBuffer());	
    serial.WriteText(SerialCommand.GetTxdBuffer());
    serial.WriteText("]: ");
    switch (GlobalErrorCode)
    {
      case ecNone:
        serial.WriteText("No Error");
        break;
      case ecUnknown:
        serial.WriteText("Unknown");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
      case ecToManyParameters:
        serial.WriteText("To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.WriteText("Missing Target Parameter");
        break;              
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    GlobalErrorCode = ecNone;
  }
}
//
//#########################################################
//  Segment - Automation
//#########################################################
//
void HandleAutomationIdle(CSerial &serial)
{  
  // nothing to do
}

void HandleAutomationMotionRowColLeft(CSerial &serial)
{
  LedMotionX.On();
  LedMotionY.On();
  //
  SetPosition();
  SetTrigger();
  //
  XPositionActual += XPositionDelta;
  if (XPositionMaximum < XPositionActual)
  {
    XPositionActual = XPositionMinimum;
    YPositionActual += YPositionDelta;
    if (YPositionMaximum < YPositionActual)
    { // Finish Area scanning
      XPositionActual = XPositionMinimum;
      YPositionActual = YPositionMinimum;
      CountRepetition--;
      LedBusy.Toggle();
      if (0 == CountRepetition)
      { // ends...
        // SetPosition();
        ExecuteAbortMotion(serial);
        return;
      }
    } 
  } 
}

void HandleAutomationTriggerRowColLeft(CSerial &serial)
{
  LedMotionX.On();
  LedMotionY.On();
  //
  SetPosition();
  TriggerLaserPulses();
  //
  XPositionActual += XPositionDelta;
  if (XPositionMaximum < XPositionActual)
  {
    XPositionActual = XPositionMinimum;
    YPositionActual += YPositionDelta;
    if (YPositionMaximum < YPositionActual)
    { // Finish Area scanning
      XPositionActual = XPositionMinimum;
      YPositionActual = YPositionMinimum;
      CountRepetition--;
      LedBusy.Toggle();
      if (0 == CountRepetition)
      { // ends...
        // SetPosition();
        ExecuteAbortMotion(serial);
        return;
      }
    } 
  } 
}

void HandleAutomation(CSerial &serial)
{
  switch (StateAutomation)
  {
    case saIdle:
      HandleAutomationIdle(serial);
      break;
    case saMotionRowColLeft:
      HandleAutomationMotionRowColLeft(serial);
      break;
    case saMotionRowColBoth:
      break;
    case saTriggerRowColLeft:
      HandleAutomationTriggerRowColLeft(serial);
      break;
    case saTriggerRowColBoth:
      break;
  }
}







Boolean ExecuteRxdCommand(CSerial &serial)
{ 
//  sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
//  serialtarget.WriteLine(TxdBuffer);
  if (!strcmp("H", SerialCommand.GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp("GPH", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp("GSV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp("GHV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  // ----------------------------------
  // Led
  // ----------------------------------
  if (!strcmp("GLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetLed(serial);
    return true;
  } else 
  if (!strcmp("SLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetLed(serial);
    return true;
  } else   
  if (!strcmp("CLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteClearLed(serial);
    return true;
  } else   
  if (!strcmp("TLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteToggleLed(serial);
    return true;
  } else   
//  // ----------------------------------
//  // Adc
//  // ----------------------------------
//  if (!strcmp("GAV", SerialCommand.GetPRxdCommand()))
//  {
//    ExecuteGetAdcValue(serial);
//    return true;
//  } else   
  // ----------------------------------
  // Dac
  // ----------------------------------
  if (!strcmp("GDV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetDacValue(serial);
    return true;
  } else   
  if (!strcmp("SDV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDacValue(serial);
    return true;
  } else   
//  // ----------------------------------
//  // Pwm
//  // ----------------------------------
//  if (!strcmp("GPV", SerialCommand.GetPRxdCommand()))
//  {
//    ExecuteGetPwmValue(serial);
//    return true;
//  } else   
//  if (!strcmp("SPV", SerialCommand.GetPRxdCommand()))
//  {
//    ExecuteSetPwmValue(serial);
//    return true;
//  } else   
  // ----------------------------------
  // LaserPulse
  // ----------------------------------
  if (!strcmp("TLP", SerialCommand.GetPRxdCommand()))
  {
//!!!!!!!!!!!!!!!!!!!    ExecuteTriggerLaserPulse(serial);
    return true;
  } else   
  // ----------------------------------
  // Automation
  // ----------------------------------
  if (!strcmp("SMX", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetMotionX(serial);
    return true;
  } else   
  if (!strcmp("SMY", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetMotionY(serial);
    return true;
  } else   
  if (!strcmp("SDM", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDelayMotion(serial);
    return true;
  } else   
  if (!strcmp("SDP", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDelayPulse(serial);
    return true;
  } else   
//  if (!strcmp("STM", SerialCommand.GetPRxdCommand()))
//  {
//    ExecuteStartMotion(serial);
//    return true;
//  } else   
  if (!strcmp("STT", SerialCommand.GetPRxdCommand()))
  {
    ExecuteStartTrigger(serial);
    return true;
  } else   
  if (!strcmp("ABM", SerialCommand.GetPRxdCommand()))
  {
    ExecuteAbortMotion(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    GlobalErrorCode = ecInvalidCommand;
  }
  return false;  
}

void HandleSerialCommands(CSerial &serial)
{
  if (SerialCommand.AnalyseRxdCommand(serial))
  {
    ExecuteRxdCommand(serial);
  }  
}



