//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "LedLine.h"
//
CLedLine::CLedLine(vector<int> pins)
{
  int PC = pins.size();  
  FLeds.reserve(PC);
  for (int I = 0; I < PC; I++)
  {
    FLeds[I] = CLed(pins[I]);
  }
}


int CLedLine::GetLedCount()
{
  return FLeds.size();
}

EStateLed CLedLine::GetState(int ledindex)
{
  CLed Led = FLeds[ledindex];
  return Led.GetState();
}



Boolean CLedLine::Open()
{
  int LC = GetLedCount();
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    Led.Open();    
  }
  return true;
}

Boolean CLedLine::Close()
{
  int LC = GetLedCount();
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    Led.Close();
  }
  return true;
}



void CLedLine::On()
{
  int LC = GetLedCount();
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    Led.On();
  }   
}

void CLedLine::Off()
{
  int LC = GetLedCount();
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    Led.Off();
  }   
}

void CLedLine::Toggle()
{
  int LC = GetLedCount();
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    Led.Toggle();
  }   
}

void CLedLine::Pulse(UInt32 microseconds)
{
  int LC = GetLedCount();
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    Led.On();
  }   
  delayMicroseconds(microseconds);  
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    Led.Off();
  }   
}

void CLedLine::On(byte line)
{
  int LC = GetLedCount();
  byte Mask = 0x01;
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    if (Mask && line)
    {
      Led.On();
    }
    Mask << 1;
  }   
}

void CLedLine::Off(byte line)
{
  int LC = GetLedCount();
  byte Mask = 0x01;
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    if (Mask && line)
    {
      Led.Off();
    }
    Mask << 1;
  }   
}

void CLedLine::Toggle(byte line)
{
  int LC = GetLedCount();
  byte Mask = 0x01;
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    if (Mask && line)
    {
      if (Led.GetState())
      {
        Led.Off();
      }
      else
      {
        Led.On();      
      }
    }
    Mask << 1;
  }   
}

void CLedLine::Pulse(byte line, UInt32 microseconds)
{
  int LC = GetLedCount();
  byte Mask = 0x01;
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    if (Mask && line)
    {
      Led.On();
    }
    Mask << 1;
  }   
  delayMicroseconds(microseconds);  
  for (int LI = 0; LI < LC; LI++)
  {
    CLed Led = FLeds[LI];
    if (Mask && line)
    {
      Led.On();
    }
    Mask << 1;
  }   
}


