#ifndef SerialCommand_h
#define SerialCommand_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
//
class CSerialCommand
{
  private:
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Character FRxdCommandLine[SIZE_RXDBUFFER];
  PCharacter FPRxdCommand;
  PCharacter FPRxdParameters[COUNT_RXDPARAMETERS];
  Int16 FRxdParameterCount = 0;
  Int16 FRxdBufferIndex = 0;

  public:
  CSerialCommand();
  ~CSerialCommand();
  //
  PCharacter GetPRxdParameters(UInt8 index)
  {
    return FPRxdParameters[index];
  }
  PCharacter GetTxdBuffer()
  {
    return FTxdBuffer;
  }
  PCharacter GetPRxdCommand()
  {
    return FPRxdCommand;
  }
  //
  void ZeroRxdBuffer();
  void ZeroTxdBuffer();
  void ZeroRxdCommandLine();
  //
  void InitSerialCommand();
  Boolean DetectRxdCommandLine(CSerial &serial);
  Boolean AnalyseRxdCommand(CSerial &serial);
};
//
#endif // SerialCommand_h
