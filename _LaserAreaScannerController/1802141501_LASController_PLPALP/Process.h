#ifndef Process_h
#define Process_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
//
const UInt32 INIT_PROCESSCOUNT = 1000;    // []
const UInt32 INIT_PROCESSPERIOD_US = 1000;   // [us]
//
#define MASK_STATEPROCESS "%s STP %i %i"
//
//
enum EStateProcess
{ // Common
  spUndefined = -1,
  spIdle = 0,
  spWelcome = 1,
  spGetHelp = 2,
  spGetProgramHeader = 3,
  spGetSoftwareVersion = 4,
  spGetHardwareVersion = 5,
  spSetProcessCount = 6,
  spSetProcessPeriod = 7,
  spStopProcessExecution = 8,
  // LedSystem
  spGetLedSystem = 9,  
  spLedSystemOn = 10,  
  spLedSystemOff = 11,  
  spBlinkLedSystem = 12,  
  // LaserPosition
  spLaserPositionPulse = 13,
  spLaserPositionAbort = 14,
  // LaserParameter
  spSetMotionRangeX = 15,
  spSetMotionRangeY = 16,
  spSetDelayMotion = 17,
  spSetDelayPulse = 18,
  // LaserMatrix
  spLaserMatrixBegin = 19,
  spLaserMatrixBusy = 20,
  spLaserMatrixAbort = 21,
  spLaserMatrixEnd = 22,
  // LaserImage
  spLaserImageBegin = 23,
  spLaserImageWait = 24,
  spLaserImageBusy = 25,
  spLaserImageAbort = 26,
  spLaserImageEnd = 27
  //
};
//
class CProcess
{ //
  //  Segment - Field
  //
  private:
  // Common
  EStateProcess FState;
  UInt8 FSubstate;
  UInt32 FStateTick;          // [1]
  UInt32 FTimeStamp;          // [us]
  UInt32 FTimeMark;           // [us]
  UInt32 FProcessCount;       // [1]
  UInt32 FProcessStep;        // [1]
  UInt32 FProcessPeriod;      // [us]
  //  
//  UInt32 FLaserPulsePeriod;           // [us]
//  UInt32 FTotalPulseCountPreset;      // [1]  Total Number of LaserPulses Preset
//  UInt32 FTotalPulseCountActual;      // [1]  Total Number of LaserPulses Actual
//  UInt32 FPositionPulseCountPreset;   // [1]  Number of LaserPulses at one Position Preset
//  UInt32 FPositionPulseCountActual;   // [1]  Number of LaserPulses at one Position Actual
  UInt32 FRangeDelayMotion;           // [us]
  UInt32 FRangeDelayPulse;            // [us]
  //
  UInt16 FXPositionActual;            // [stp]
  UInt16 FXPositionDelta;             // [stp]
  UInt16 FXPositionMinimum;           // [stp]
  UInt16 FXPositionMaximum;           // [stp]
  //
  UInt16 FYPositionActual;            // [stp]
  UInt16 FYPositionDelta;             // [stp]
  UInt16 FYPositionMinimum;           // [stp]
  UInt16 FYPositionMaximum;           // [stp]
  //
  void LedSystemState();
  //
  public:
  CProcess();
  // Property
  EStateProcess GetState();
  void SetState(EStateProcess stateprocess, UInt8 substate = 0);
  // Measurement - Time
  // First: TimeStamp!!!
  inline void SetTimeStamp()
  {
    FTimeStamp = micros();
    FTimeMark = FTimeStamp;
  }
  // Later: TimeMarker!!!
  inline void SetTimeMark()
  {
    FTimeMark = micros();
  }
  inline UInt32 GetTimeSpan()
  {
    return FTimeMark - FTimeStamp;
  }

  inline void SetProcessCount(UInt32 count)
  {
    FProcessStep = 0;
    FProcessCount = count;
  }
  inline UInt32 GetProcessCount(void)
  {
    return FProcessCount;
  }

  inline void SetProcessStep(UInt32 stepindex)
  {
    FProcessStep = stepindex;
  }
  inline UInt32 GetProcessStep(void)
  {
    return FProcessStep;
  }

  inline Boolean IncrementProcessStep()
  {
    FProcessStep++;
    return (FProcessCount <= FProcessStep);
  }

  inline void SetProcessPeriodQuarter(UInt32 periodquarter)
  {
    FProcessPeriod = periodquarter << 2;
  }
  inline void SetProcessPeriodHalf(UInt32 periodhalf)
  {
    FProcessPeriod = periodhalf << 1;
  }
  inline void SetProcessPeriod(UInt32 period)
  {
    FProcessPeriod = period;
  }
  inline UInt32 GetProcessPeriod(void)
  {
    return FProcessPeriod;
  }
  inline UInt32 GetProcessPeriodHalf(void)
  {
    return FProcessPeriod >> 1;
  }
  inline UInt32 GetProcessPeriodQuarter(void)
  {
    return FProcessPeriod >> 2;
  }
  //
  //
  //  Laser
  //
//  inline UInt32 GetLaserPulsePeriod()
//  {
//    return FLaserPulsePeriod;
//  }
//  inline void SetLaserPulsePeriod(UInt32 value)
//  {
//    FLaserPulsePeriod = value;
//  }
//
//  inline UInt32 GetTotalPulseCountPreset()
//  {
//    return FTotalPulseCountPreset;
//  }
//  inline void SetTotalPulseCountPreset(UInt32 value)
//  {
//    FTotalPulseCountPreset = value;
//  }
//  inline UInt32 GetTotalPulseCountActual()
//  {
//    return FTotalPulseCountActual;
//  }
//  inline void SetTotalPulseCountActual(UInt32 value)
//  {
//    FTotalPulseCountActual = value;
//  }
//  inline void IncrementTotalPulseCountActual()
//  {
//    FTotalPulseCountActual++;
//  }
//  inline Boolean IsTotalPulseCountReached()
//  {
//    return (FTotalPulseCountPreset <= FTotalPulseCountActual);
//  }
//
//  inline UInt32 GetPositionPulseCountPreset()
//  {
//    return FPositionPulseCountPreset;
//  }
//  inline void SetPositionPulseCountPreset(UInt32 value)
//  {
//    FPositionPulseCountPreset = value;
//  }
//  inline UInt32 GetPositionPulseCountActual()
//  {
//    return FPositionPulseCountActual;
//  }
//  inline void SetPositionPulseCountActual(UInt32 value)
//  {
//    FPositionPulseCountActual = value;
//  }
//  inline void IncrementPositionPulseCountActual()
//  {
//    FPositionPulseCountActual++;
//  }
//  inline Boolean IsPositionPulseCountReached()
//  {
//    return (FPositionPulseCountPreset <= FPositionPulseCountActual);
//  }
  //
  //  Range - Delay
  //
  inline UInt32 GetRangeDelayMotion()
  {
    return FRangeDelayMotion;
  }
  inline void SetRangeDelayMotion(UInt32 value)
  {
    FRangeDelayMotion = value;
  }
  
  inline UInt32 GetRangeDelayPulse()
  {
    return FRangeDelayPulse;
  }
  inline void SetRangeDelayPulse(UInt32 value)
  {
    FRangeDelayPulse = value;
  }
  //
  //  X-Position
  //
  inline UInt32 GetXPositionActual()
  {
    return FXPositionActual;
  }
  inline void SetXPositionActual(UInt32 value)
  {
    FXPositionActual = value;
  }
    
  inline UInt32 GetXPositionDelta()
  {
    return FXPositionDelta;
  }
  inline void SetXPositionDelta(UInt32 value)
  {
    FXPositionDelta = value;
  }
  
  inline UInt32 GetXPositionMinimum()
  {
    return FXPositionMinimum;
  }
  inline void SetXPositionMinimum(UInt32 value)
  {
    FXPositionMinimum = value;
  }
  
  inline UInt32 GetXPositionMaximum()
  {
    return FXPositionMaximum;
  }
  inline void SetXPositionMaximum(UInt32 value)
  {
    FXPositionMaximum = value;
  }
  //
  //  Y-Position
  //
  inline UInt32 GetYPositionActual()
  {
    return FYPositionActual;
  }
  inline void SetYPositionActual(UInt32 value)
  {
    FYPositionActual = value;
  }
    
  inline UInt32 GetYPositionDelta()
  {
    return FYPositionDelta;
  }
  inline void SetYPositionDelta(UInt32 value)
  {
    FYPositionDelta = value;
  }
  
  inline UInt32 GetYPositionMinimum()
  {
    return FYPositionMinimum;
  }
  inline void SetYPositionMinimum(UInt32 value)
  {
    FYPositionMinimum = value;
  }
  
  inline UInt32 GetYPositionMaximum()
  {
    return FYPositionMaximum;
  }
  inline void SetYPositionMaximum(UInt32 value)
  {
    FYPositionMaximum = value;
  }

  inline UInt32 IncrementXPosition()
  {
    FXPositionActual += FXPositionDelta;
    return FXPositionActual;
  }
  inline UInt32 DecrementXPosition()
  {
    FXPositionActual -= FXPositionDelta;
    return FXPositionActual;
  }
  
  inline UInt32 IncrementYPosition()
  {
    FYPositionActual += FYPositionDelta;
    return FYPositionActual;
  }
  inline UInt32 DecrementYPosition()
  {
    FYPositionActual -= FYPositionDelta;
    return FYPositionActual;
  }
  // Management
  Boolean Open();
  Boolean Close();
  private:
  // System
  void HandleUndefined(CSerial &serial);
  void HandleIdle(CSerial &serial);
  void HandleWelcome(CSerial &serial);
  void HandleGetHelp(CSerial &serial);
  void HandleGetProgramHeader(CSerial &serial);
  void HandleGetSoftwareVersion(CSerial &serial);
  void HandleGetHardwareVersion(CSerial &serial);
  void HandleSetProcessCount(CSerial &serial);
  void HandleSetProcessPeriod(CSerial &serial);
  void HandleStopProcessExecution(CSerial &serial);
  // LedSystem
  void HandleGetLedSystem(CSerial &serial);
  void HandleLedSystemOn(CSerial &serial);
  void HandleLedSystemOff(CSerial &serial);
  void HandleBlinkLedSystem(CSerial &serial);
  // Measurement - LaserPosition
  void HandleLaserPositionPulse(CSerial &serial);
  void HandleLaserPositionAbort(CSerial &serial);
  // Measurement - LaserParameter
  void HandleSetMotionRangeX(CSerial &serial);      
  void HandleSetMotionRangeY(CSerial &serial);      
  void HandleSetDelayMotion(CSerial &serial); 
  void HandleSetDelayPulse(CSerial &serial);  
  // Measurement - LaserMatrix
  void HandleLaserMatrixBegin(CSerial &serial);
  void HandleLaserMatrixBusy(CSerial &serial);
  void HandleLaserMatrixAbort(CSerial &serial);
  void HandleLaserMatrixEnd(CSerial &serial);
  // Measurement - LaserImage
  void HandleLaserImageBegin(CSerial &serial);
  void HandleLaserImageBusy(CSerial &serial);
  void HandleLaserImageAbort(CSerial &serial);
  void HandleLaserImageEnd(CSerial &serial);
  //
  public:
  // Collector
  void Handle(CSerial &serial);
};
//
#endif // Process_h
