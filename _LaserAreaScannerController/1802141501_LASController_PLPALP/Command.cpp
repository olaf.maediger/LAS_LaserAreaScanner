//
#include "Command.h"
#include "Led.h"
#include "Dac.h"
#include "Error.h"
#include "Process.h"
//
//-------------------------------------------
//  External Global Variables 
//-------------------------------------------
//  
extern CLed LedSystem;
extern CDac DigitalAnalogConverter;
extern CSerial SerialCommand;
extern CError LASError;
extern CProcess LASProcess;
extern CCommand LASCommand;
//
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand()
{
  Init();
}

CCommand::~CCommand()
{
  Init();
}
//
//#########################################################
//  Segment - Helper
//#########################################################
//
void CCommand::ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}

void CCommand::ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}

void CCommand::ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdCommandLine[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Management
//#########################################################
//
void CCommand::Init()
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
}

Boolean CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CR:
        FRxdBuffer[FRxdBufferIndex] = ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FRxdCommandLine, FRxdBuffer);
        return true;
      case LF: // ignore
        break;
      default: 
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean CCommand::Analyse(CSerial &serial)
{
  if (DetectRxdLine(serial))
  {
    char *PTerminal = (char*)" \t\r\n";
    char *PRxdCommandLine = FRxdCommandLine;
    FPRxdCommand = strtok(FRxdCommandLine, PTerminal);
    if (FPRxdCommand)
    {
      FRxdParameterCount = 0;
      char *PRxdParameter;
      while (PRxdParameter = strtok(0, PTerminal))
      {
        FPRxdParameters[FRxdParameterCount] = PRxdParameter;
        FRxdParameterCount++;
        if (COUNT_RXDPARAMETERS < FRxdParameterCount)
        {
          LASError.SetCode(ecToManyParameters);
          ZeroRxdBuffer();
          // debug serialtarget.WriteLine("error[ecToManyParameters]");
          serial.WriteNewLine();
          serial.WritePrompt();
          return false;
        }
      }  
//      debug sprintf(TxdBuffer, "\n\r# RxdCommand<%s>", PRxdCommand);
//      debug serial.Write(TxdBuffer);
//      if (0 < RxdParameterCount)
//      {
//        int II;
//        for (II = 0; II < RxdParameterCount; II++)
//        {
//          sprintf(TxdBuffer, " RxdParameter[%i]<%s>", II, PRxdParameters[II]);
//          serial.Write(TxdBuffer);
//        }
//      }
      //
      ZeroRxdBuffer();
      serial.WriteNewLine();
      return true;
    }
    ZeroRxdBuffer();
    serial.WriteNewLine();
    serial.WritePrompt();
  }
  return false;
}
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PROJECT, ARGUMENT_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_HARDWARE, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_DATE, ARGUMENT_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_TIME, ARGUMENT_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_AUTHOR, ARGUMENT_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PORT, ARGUMENT_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PARAMETER, ARGUMENT_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void CCommand::WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
}

void CCommand::WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
}

void CCommand::WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.WriteLine(MASK_H, SHORT_H);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPH, SHORT_GPH);
  serial.WriteAnswer(); serial.WriteLine(MASK_GSV, SHORT_GSV);
  serial.WriteAnswer(); serial.WriteLine(MASK_GHV, SHORT_GHV);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPC, SHORT_SPC);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPP, SHORT_SPP);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPE, SHORT_SPE);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LEDSYSTEM);
  serial.WriteAnswer(); serial.WriteLine(MASK_GLS, SHORT_GLS);
  serial.WriteAnswer(); serial.WriteLine(MASK_LSH, SHORT_LSH);
  serial.WriteAnswer(); serial.WriteLine(MASK_LSL, SHORT_LSL);
  serial.WriteAnswer(); serial.WriteLine(MASK_BLS, SHORT_BLS);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERPOSITION);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLP, SHORT_PLP);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALP, SHORT_ALP);
//
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERPARAMETER);
  serial.WriteAnswer(); serial.WriteLine(MASK_SRX, SHORT_SRX);
  serial.WriteAnswer(); serial.WriteLine(MASK_SRY, SHORT_SRY);
  serial.WriteAnswer(); serial.WriteLine(MASK_SDM, SHORT_SDM);
  serial.WriteAnswer(); serial.WriteLine(MASK_SDP, SHORT_SDP);
//
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERMATRIX);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLM, SHORT_PLM);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALM, SHORT_ALM);
//
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERIMAGE);
  serial.WriteAnswer(); serial.WriteLine(MASK_SLI, SHORT_SLI);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLI, SHORT_PLI);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALI, SHORT_ALI);
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
void CCommand::ExecuteGetHelp(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetHelp);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HELP);
    serial.WriteLine(GetTxdBuffer());         
    WriteHelp(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetProgramHeader(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetProgramHeader);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HEADER);
    serial.WriteLine(GetTxdBuffer());   
    WriteProgramHeader(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetSoftwareVersion(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetSoftwareVersion);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_SOFTWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteSoftwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetHardwareVersion);
    // Analyse parameters: -
    // Response
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HARDWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteHardwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetProcessCount(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetProcessCount);
    // Analyse parameters:
    UInt32 PC = atol(GetPRxdParameters(0));
    // Execute:
    LASProcess.SetProcessCount(PC);
    PC = LASProcess.GetProcessCount();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetProcessPeriod(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetProcessPeriod);
    // Analyse parameters:
    UInt32 PP = atol(GetPRxdParameters(0));
    // Execute:
    LASProcess.SetProcessPeriod(PP);
    PP = LASProcess.GetProcessPeriod();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteStopProcessExecution(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spStopProcessExecution);
    // Analyse parameters:
    // Execute:
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - LedSystem
//#########################################################
//
void CCommand::ExecuteGetLedSystem(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetLedSystem);
    // Analyse parameters:
    // Execute:
    int State = LedSystem.GetState();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), State);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteLedSystemOn(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spLedSystemOn);
    // Analyse parameters:
    // Execute:
    LedSystem.On();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteLedSystemOff(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spLedSystemOff);
    // Analyse parameters:
    // Execute:
    LedSystem.Off();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteBlinkLedSystem(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spBlinkLedSystem);
    // Analyse parameters:
    UInt32 Period = atol(GetPRxdParameters(0)); // [us]
    UInt32 Count = atol(GetPRxdParameters(1));  // [1] 
    // Execute:
    LASProcess.SetProcessPeriod(Period);
    LASProcess.SetProcessCount(Count);    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), Period, Count);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - LaserPosition
//#########################################################
//
// PulseLaserPosition PLP <x> <y> <p> <c> : Pulse Laser <x><y> <p>eriod <c>ount"
// Definition of XPositionActual, YPositionActual, ProcessPeriod, ProcessCount
void CCommand::ExecutePulseLaserPosition(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spLaserPositionPulse);
    // Analyse parameters ( PLP <x> <y> <p> <c> ):
    UInt32 PositionX = atol(GetPRxdParameters(0));
    UInt32 PositionY = atol(GetPRxdParameters(1));
    UInt32 PulsePeriod = atol(GetPRxdParameters(2));
    UInt32 PulseCount = atol(GetPRxdParameters(3));
    // Execute:
    LASProcess.SetXPositionActual(PositionX);
    LASProcess.SetYPositionActual(PositionY);
    LASProcess.SetProcessPeriod(PulsePeriod);
    LASProcess.SetProcessCount(PulseCount);  
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu %lu", PROMPT_RESPONSE, 
            GetPRxdCommand(), PositionX, PositionY, PulsePeriod, PulseCount);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteAbortLaserPosition(CSerial &serial)
{ 
  if (spLaserPositionPulse == LASProcess.GetState())
  {
    LASProcess.SetState(spLaserPositionAbort);
    // Analyse parameters : -
    // Execute : -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - LaserParameter
//#########################################################
//
void CCommand::ExecuteSetMotionRangeX(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
//    LASProcess.SetState(spSetMotionRangeX);
//    // Analyse parameters ( SRX <xmin> <xmax> <dx> ):
//    UInt32 PL = atol(GetPRxdParameters(0));
//    UInt32 PH = atol(GetPRxdParameters(1));
//    UInt32 PD = atol(GetPRxdParameters(2));
//    // Execute:
//    LASProcess.SetXPositionMinimum(PL);
//    LASProcess.SetXPositionMaximum(PH);
//    LASProcess.SetXPositionDelta(PD);
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu", PROMPT_RESPONSE, 
//            GetPRxdCommand(), PL, PH, PD);
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetMotionRangeY(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
//    LASProcess.SetState(spSetMotionRangeY);
//    // Analyse parameters ( SRY <ymin> <ymax> <dy> ):
//    UInt32 PL = atol(GetPRxdParameters(0));
//    UInt32 PH = atol(GetPRxdParameters(1));
//    UInt32 PD = atol(GetPRxdParameters(2));
//    // Execute:
//    LASProcess.SetYPositionMinimum(PL);
//    LASProcess.SetYPositionMaximum(PH);
//    LASProcess.SetYPositionDelta(PD);   
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu", 
//            PROMPT_RESPONSE, GetPRxdCommand(), PL, PH, PD);
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetDelayMotion(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
//    LASProcess.SetState(spSetDelayMotion);
//    // Analyse parameters ( SDM <delay> ):
//    UInt32 D = atol(GetPRxdParameters(0));    
//    LASProcess.SetDelayMotion(D);
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %lu", 
//            PROMPT_RESPONSE, GetPRxdCommand(), D);
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetDelayPulse(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
//    LASProcess.SetState(spSetDelayPulse);
//    // Analyse parameters ( SDP <delay> ):
//    UInt32 D = atol(GetPRxdParameters(0));        
//    LASProcess.SetDelayPulse(D);
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %lu", 
//            PROMPT_RESPONSE, GetPRxdCommand(), D);
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - LaserMatrix
//#########################################################
//
void CCommand::ExecutePulseLaserMatrix(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
//    LASProcess.SetState(spLaserMatrixBegin);
//    // Analyse parameters ( PLM <p> <n> ):
//    UInt32 P = atol(GetPRxdParameters(0));
//    UInt32 N = atol(GetPRxdParameters(1));
//    LASProcess.SetLaserPulsePeriod(P);
//    LASProcess.SetLaserPulseCountPreset(N);
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
//            PROMPT_RESPONSE, GetPRxdCommand(), P, N);
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteAbortLaserMatrix(CSerial &serial)
{
  if (spLaserMatrixBusy == LASProcess.GetState())
  {
//    LASProcess.SetState(spLaserMatrixAbort);
//    // Analyse parameters ( ALM ):
//    // -    
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s", 
//            PROMPT_RESPONSE, GetPRxdCommand());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - LaserImage
//#########################################################
//
void CCommand::ExecuteStartLaserImage(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
//    LASProcess.SetState(spLaserImageBegin);
//    LASProcess.SetState(spLaserImageBusy);
//    // Analyse parameters ( SLI ):
//    // -
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s", 
//            PROMPT_RESPONSE, GetPRxdCommand());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}  

void CCommand::ExecutePulseLaserImage(CSerial &serial)
{
  if (spLaserImageBusy == LASProcess.GetState())
  {
//    // NC LASProcess.SetState(spLaserImageBusy);
//    // Analyse parameters ( PLI <x> <y> <p> <c> ):
//    UInt32 X = atol(GetPRxdParameters(0));
//    UInt32 Y = atol(GetPRxdParameters(1));
//    UInt32 P = atol(GetPRxdParameters(2));
//    UInt32 C = atol(GetPRxdParameters(3));
//    SetLaserPositionX(X);
//    SetLaserPositionY(Y);
//    SetLaserPulsePeriod(P);
//    SetLaserPulseCountPreset(C);
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu %lu", 
//            PROMPT_RESPONSE, GetPRxdCommand(), X, Y, P, C);
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteAbortLaserImage(CSerial &serial)
{
  if (spLaserImageBusy == LASProcess.GetState())
  {
//    SetState(spLaserImageAbort);
//    // Analyse parameters ( ALI ):
//    // -
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s", 
//            PROMPT_RESPONSE, GetPRxdCommand());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
void CCommand::CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate)
{
//  switch (stateprocess)
//  { // Base
//    case spUndefined:
//      break; 
//    case spIdle:
//      break; 
//    default:
//      break;
//  }  
//  serial.WriteNewLine();
//  serial.WritePrompt();
  sprintf(LASCommand.GetTxdBuffer(), MASK_STATEPROCESS, PROMPT_EVENT, stateprocess, substate);
  serial.WriteLine(LASCommand.GetTxdBuffer());
  serial.WritePrompt();
}

Boolean CCommand::Execute(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
// debug Serial.WriteLine(TxdBuffer);
  if (!strcmp(SHORT_H, GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPH, GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GSV, GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GHV, GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPC, GetPRxdCommand()))
  {
    ExecuteSetProcessCount(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPP, GetPRxdCommand()))
  {
    ExecuteSetProcessPeriod(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPE, GetPRxdCommand()))
  {
    ExecuteStopProcessExecution(serial);
    return true;
  } else 
  // ----------------------------------
  // LedSystem
  // ---------------------------------- 
  if (!strcmp(SHORT_GLS, GetPRxdCommand()))
  {
    ExecuteGetLedSystem(serial);
    return true;
  } else 
  if (!strcmp(SHORT_LSH, GetPRxdCommand()))
  {
    ExecuteLedSystemOn(serial);
    return true;
  } else   
  if (!strcmp(SHORT_LSL, GetPRxdCommand()))
  {
    ExecuteLedSystemOff(serial);
    return true;
  } else   
  if (!strcmp(SHORT_BLS, GetPRxdCommand()))
  {
    ExecuteBlinkLedSystem(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - LaserPosition
  // ----------------------------------
  if (!strcmp(SHORT_PLP, GetPRxdCommand()))
  {
    ExecutePulseLaserPosition(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALP, GetPRxdCommand()))
  {
    ExecuteAbortLaserPosition(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - LaserParameter
  // ----------------------------------
  if (!strcmp(SHORT_SRX, GetPRxdCommand()))
  {
    ExecuteSetMotionRangeX(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SRY, GetPRxdCommand()))
  {
    ExecuteSetMotionRangeY(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SDM, GetPRxdCommand()))
  {
    ExecuteSetDelayMotion(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SDP, GetPRxdCommand()))
  {
    ExecuteSetDelayPulse(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - LaserMatrix
  // ----------------------------------
  if (!strcmp(SHORT_PLM, GetPRxdCommand()))
  {
    ExecutePulseLaserMatrix(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALM, GetPRxdCommand()))
  {
    ExecuteAbortLaserMatrix(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - LaserImage
  // ----------------------------------
  if (!strcmp(SHORT_SLI, GetPRxdCommand()))
  {
    ExecuteStartLaserImage(serial);
    return true;
  } else   
  if (!strcmp(SHORT_PLI, GetPRxdCommand()))
  {
    ExecutePulseLaserImage(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALI, GetPRxdCommand()))
  {
    ExecuteAbortLaserImage(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    LASError.SetCode(ecInvalidCommand);
  }
  return false;  
}

Boolean CCommand::Handle(CSerial &serial)
{
  if (Analyse(serial))
  {
    return Execute(serial);
  }  
  return false;
}



