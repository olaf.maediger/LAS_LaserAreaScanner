#ifndef Command_h
#define Command_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
#include "Process.h"
//
#define ARGUMENT_PROJECT    "LAS - LaserAreaScanner-Controller"
#define ARGUMENT_SOFTWARE   "06V02"
#define ARGUMENT_DATE       "180214"
#define ARGUMENT_TIME       "0835"
#define ARGUMENT_AUTHOR     "OM"
#define ARGUMENT_PORT       "SerialCommand (Serial-USB)"
#define ARGUMENT_PARAMETER  "115200, N, 8, 1"
//
#ifdef PROCESSOR_ARDUINOUNO
#define ARGUMENT_HARDWARE "ArduinoUnoR3"
#endif
#ifdef PROCESSOR_ARDUINOMEGA 
#define ARGUMENT_HARDWARE "ArduinoMega328"
#endif
#ifdef PROCESSOR_ARDUINODUE
#define ARGUMENT_HARDWARE "ArduinoDue3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
// 
// HELP_COMMON
#define SHORT_H     "H"
#define SHORT_GPH   "GPH"
#define SHORT_GSV   "GSV"
#define SHORT_GHV   "GHV"
#define SHORT_SPC   "SPC"
#define SHORT_SPP   "SPP"
#define SHORT_SPE   "SPE"
//
// HELP_LEDSYSTEM
#define SHORT_GLS   "GLS"
#define SHORT_LSH   "LSH"
#define SHORT_LSL   "LSL"
#define SHORT_BLS   "BLS"
//
// HELP_LASERPOSITION
#define SHORT_PLP   "PLP"
#define SHORT_ALP   "ALP"
//
// HELP_LASERPARAMETER
#define SHORT_SRX   "SRX"
#define SHORT_SRY   "SRY"
#define SHORT_SDM   "SDM"
#define SHORT_SDP   "SDP"
//
// HELP_LASERMATRIX
#define SHORT_PLM   "PLM"
#define SHORT_ALM   "ALM"
//
// HELP_LASERIMAGE
#define SHORT_SLI   "SLI"
#define SHORT_PLI   "PLI"
#define SHORT_ALI   "ALI"
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HEADER = 12;
//
#define TITLE_LINE            "-----------------------------------------------"
#define MASK_PROJECT          "- Project:   %-32s -"
#define MASK_SOFTWARE         "- Version:   %-32s -"
#define MASK_HARDWARE         "- Hardware:  %-32s -"
#define MASK_DATE             "- Date:      %-32s -"
#define MASK_TIME             "- Time:      %-32s -"
#define MASK_AUTHOR           "- Author:    %-32s -"
#define MASK_PORT             "- Port:      %-32s -"
#define MASK_PARAMETER        "- Parameter: %-32s -"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HELP = 28;
//
#define HELP_COMMON               " Help (Common):"
#define MASK_H                    " %-3s : This Help"
#define MASK_GPH                  " %-3s : Get Program Header"
#define MASK_GSV                  " %-3s : Get Software-Version"
#define MASK_GHV                  " %-3s : Get Hardware-Version"
#define MASK_SPC                  " %-3s <pc> : Set Process Count"
#define MASK_SPP                  " %-3s <pp> : Set Process Period [ms]"
#define MASK_SPE                  " %-3s : Stop Process Execution"
// 8
#define HELP_LEDSYSTEM            " Help (LedSystem):"
#define MASK_GLS                  " %-3s : Get State LedSystem"
#define MASK_LSH                  " %-3s : Switch LedSystem On"
#define MASK_LSL                  " %-3s : Switch LedSystem Off"
#define MASK_BLS                  " %-3s <p> <n> : Blink LedSystem with <p>eriod{ms} <n>times"
// 13
#define HELP_LASERPOSITION        " Help (LaserPosition):"
#define MASK_PLP                  " %-3s <x> <y> <p> <c> : Pulse Laser Position<x><y> <p>eriod <c>ount"
#define MASK_ALP                  " %-3s : Abort Laser Position"
// 16
#define HELP_LASERPARAMETER       " Help (LaserParameter):"
#define MASK_SRX                  " %-3s <xmin> <xmax> <dx> : Set Range ParameterX[0..4095]"
#define MASK_SRY                  " %-3s <ymin> <ymax> <dy> : Set Range ParameterY[0..4095]"
#define MASK_SDM                  " %-3s <delay> : Set Delay Motion{us}"
#define MASK_SDP                  " %-3s <delay> : Set Delay Pulse{us}"
// 21
#define HELP_LASERMATRIX          " Help (LaserMatrix):"
#define MASK_PLM                  " %-3s <p> <n>: Laser Matrix <p>eriod <n>Pulses"
#define MASK_ALM                  " %-3s : Abort Laser Matrix"
// 24
#define HELP_LASERIMAGE           " Help (LaserImage):"
#define MASK_SLI                  " %-3s : Start Laser Image"
#define MASK_PLI                  " %-3s <x> <y> <p> <c> : Laser Pixel <x><y> <p>eriod <c>ount"
#define MASK_ALI                  " %-3s : Abort Laser Image"
// 28
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_SOFTWAREVERSION = 1;
//
#define MASK_SOFTWAREVERSION      " Software-Version LAS%s"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HARDWAREVERSION = 1;
//
#define MASK_HARDWAREVERSION      " Hardware-Version %s"
//
//-----------------------------------------------------------------------------------------
//
// Command-Parameter
#define COUNT_RXDPARAMETERS 4
#define SIZE_RXDPARAMETER   8
//
//-----------------------------------------------------------------------------------------
//
class CCommand
{
  private:
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Character FRxdCommandLine[SIZE_RXDBUFFER];
  PCharacter FPRxdCommand;
  PCharacter FPRxdParameters[COUNT_RXDPARAMETERS];
  Int16 FRxdParameterCount = 0;
  Int16 FRxdBufferIndex = 0;

  public:
  CCommand();
  ~CCommand();
  //
  PCharacter GetPRxdParameters(UInt8 index)
  {
    return FPRxdParameters[index];
  }
  PCharacter GetTxdBuffer()
  {
    return FTxdBuffer;
  }
  PCharacter GetPRxdCommand()
  {
    return FPRxdCommand;
  }
  //
  void ZeroRxdBuffer();
  void ZeroTxdBuffer();
  void ZeroRxdCommandLine();
  //
  //  Segment - Execution - Basic Output
  void WriteProgramHeader(CSerial &serial);
  void WriteSoftwareVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
  void WriteHelp(CSerial &serial);
  //
  //  Segment - Execution - Common
  void ExecuteGetHelp(CSerial &serial);
  void ExecuteGetProgramHeader(CSerial &serial);
  void ExecuteGetSoftwareVersion(CSerial &serial);
  void ExecuteGetHardwareVersion(CSerial &serial);
  void ExecuteSetProcessCount(CSerial &serial);
  void ExecuteSetProcessPeriod(CSerial &serial);
  void ExecuteStopProcessExecution(CSerial &serial);
  //
  //  Segment - Execution - LedSystem
  void ExecuteGetLedSystem(CSerial &serial);
  void ExecuteLedSystemOn(CSerial &serial);
  void ExecuteLedSystemOff(CSerial &serial);
  void ExecuteBlinkLedSystem(CSerial &serial);
  //
  //  Segment - Execution - LaserPosition
  void ExecutePulseLaserPosition(CSerial &serial);
  void ExecuteAbortLaserPosition(CSerial &serial);
  //
  //  Segment - Execution - LaserParameter
  void ExecuteSetMotionRangeX(CSerial &serial);      // SRX
  void ExecuteSetMotionRangeY(CSerial &serial);      // SRY
  void ExecuteSetDelayMotion(CSerial &serial); // SDM
  void ExecuteSetDelayPulse(CSerial &serial);  // SDP
  //
  //  Segment - Execution - LaserMatrix
  void ExecutePulseLaserMatrix(CSerial &serial);
  void ExecuteAbortLaserMatrix(CSerial &serial);
  //
  //  Segment - Execution - LaserImage
  void ExecuteStartLaserImage(CSerial &serial);
  void ExecutePulseLaserImage(CSerial &serial);
  void ExecuteAbortLaserImage(CSerial &serial);
  //  
  // Main
  void Init();
  Boolean DetectRxdLine(CSerial &serial);
  Boolean Analyse(CSerial &serial);
  void CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate);
  Boolean Execute(CSerial &serial);
  Boolean Handle(CSerial &serial);
};
//
#endif // Command_h
