#include "Led.h"
#include "Process.h"
#include "Command.h"
#include "Dac.h"
//
extern CLed LedSystem;
extern CSerial SerialCommand;
extern CProcess LASProcess;
extern CCommand LASCommand;
extern CDac DigitalAnalogConverter;
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CProcess::CProcess()
{
  FState = spUndefined;
  FStateTick = 0;
  FTimeStamp = micros();
  FTimeMark = FTimeStamp;
  FProcessCount = INIT_PROCESSCOUNT;
  FProcessPeriod = INIT_PROCESSPERIOD_US;
  FProcessStep = 0;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateProcess CProcess::GetState()
{
  return FState;
}
void CProcess::SetState(EStateProcess state, UInt8 substate)
{
  if ((state != FState) || (substate != FSubstate))
  {     
    if (state != FState)
    {
      FStateTick = 0;
    }
    FState = state;
    FSubstate = substate;
    switch (FState)
    {
      case spStopProcessExecution:
        break;
    }
    // Message to Manager(PC):
    LASCommand.CallbackStateProcess(SerialCommand, FState, substate);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CProcess::Open()
{
  SetState(spIdle);
  return true;
}

Boolean CProcess::Close()
{
  SetState(spUndefined);
  return true;
}

//
//----------------------------------------------------------
//  Segment - Common
//----------------------------------------------------------
//
void CProcess::HandleUndefined(CSerial &serial)
{
  delay(1000);
}

void CProcess::HandleIdle(CSerial &serial)
{ // do nothing
  SetTimeStamp();
}

void CProcess::HandleWelcome(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:        
      SetTimeStamp();
      FStateTick++;
      break;
    case 1: case 3: case 5: 
      if (330000 < GetTimeSpan())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 2: case 4: case 6:  
      if (330000 < GetTimeSpan())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 7:  
      FStateTick++;      
      SetTimeStamp();      
      break;
    case 8:   
      if (1000000 < GetTimeSpan())
      {   
        FStateTick++;
        SetTimeStamp();
      }
      break;
    case 9:   
      if (1000000 < GetTimeSpan())
      {   
        FStateTick++;
        
        SetState(spIdle);
// alternative : 
//        UInt8 CL = 0;
//        UInt8 CH = 7;
//        LASProcess.SetChannelLow(CL);
//        LASProcess.SetChannelHigh(CH);
//        LASProcess.SetState(spRepeatAllTemperatures);
//        LASProcess.SetProcessIndex(0);
//        SetState(spRepeatAllTemperatures);
        SetTimeStamp();
      }
      break;
  }
}

void CProcess::HandleGetHelp(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProgramHeader(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSoftwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetHardwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessCount(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessPeriod(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleStopProcessExecution(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - LedSystem
//----------------------------------------------------------
//
void CProcess::LedSystemState()
{
  switch (LedSystem.GetState())
  {
    case slOn:
      break;
    case slOff:
      break;
    default:
      break;
  }
  SetState(spIdle, LedSystem.GetState());
}

void CProcess::HandleGetLedSystem(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOn(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOff(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleBlinkLedSystem(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:              
      FStateTick++;
      SetTimeStamp();
      break;
    case 1: 
      if (GetProcessPeriodHalf() < GetTimeSpan())
      {
        LedSystem.On();
        SetState(spBlinkLedSystem, 1);
        FStateTick = 2;
        SetTimeStamp();
      }
      break;
    case 2: 
      if (GetProcessPeriodHalf() < GetTimeSpan())
      {
        LedSystem.Off();
        SetState(spBlinkLedSystem, 0);
        FProcessStep++;
        if (FProcessStep < FProcessCount)
        {
          FStateTick = 1;          
        }
        else
        {
          SetState(spIdle);
        }
        SetTimeStamp(); 
      }
      break;
  } 
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserPosition
//----------------------------------------------------------
// Command: PulseLaserPosition PLP <x> <y> <p> <c> : Pulse Laser <x><y> <p>eriod <c>ount"
// Command: Definition of XPositionActual, YPositionActual, ProcessPeriod, ProcessCount
// 
void CProcess::HandleLaserPositionPulse(CSerial &serial)
{ 
  SetTimeMark();
  switch (FStateTick)
  {
    case 0: // XY
      SetTimeStamp();
      if (0 < FProcessCount)
      {
        DigitalAnalogConverter.SetValue(DAC_CHANNELX, GetXPositionActual());
        DigitalAnalogConverter.SetValue(DAC_CHANNELY, GetYPositionActual());
        FStateTick = 1;
      }
      else
      { // no pulses -> Idle
        FStateTick = 3;
      }
      break;
    case 1:       
      if (slOn != LedSystem.GetState()) LedSystem.On();
      if (GetProcessPeriodHalf() <= GetTimeSpan())
      {
        FStateTick = 2;
      }
      break;
    case 2: 
      if (slOff != LedSystem.GetState()) LedSystem.Off();
      if (GetProcessPeriod() <= GetTimeSpan())
      {
        FStateTick = 3;
      }
      break;
    case 3: 
      SetTimeStamp();
      LedSystem.Off();
      FStateTick = 1;
      FProcessStep++;
      if (FProcessCount <= FProcessStep)
      {
        SetState(spIdle);
      }
      break;
  }
}

void CProcess::HandleLaserPositionAbort(CSerial &serial)
{  
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserParameter
//----------------------------------------------------------
//
//      DigitalAnalogConverter.
//      if (GetProcessPeriod() < GetTimeSpan())
//      { // Position XY
//        
//        FStateTick = 2;
//        SetTimeStamp();
//      }
//      break;
//
//
//
//        
//        LedSystem.On();
//        SetState(spBlinkLedSystem, 1);
//        FStateTick = 2;
//        SetTimeStamp();
//      }
//      break;
//    case 2: 
//      if (GetProcessPeriodHalf() < GetTimeSpan())
//      {
//        LedSystem.Off();
//        SetState(spBlinkLedSystem, 0);
//        FProcessIndex++;
//        if (FProcessIndex < FProcessCount)
//        {
//          FStateTick = 1;          
//        }
//        else
//        {
//          
//        }
//        SetTimeStamp(); 
//      }
//      break;

// before: Definition of XMinimum
//void CProcess::HandleLaserPositionBusy(CSerial &serial)
//{ 
//  SetTimeMark();
//  switch (FStateTick)
//  {
//    case 0:              
//      FStateTick = 1;
//      SetTimeStamp();
//      break;
//    case 1: // Position XY
//      SetXPositionActual(GetXPositionMinimum());
//      SetYPositionActual(GetYPositionMinimum());
//      DigitalAnalogConverter.SetValue(DAC_CHANNELX, GetXPositionActual());
//      DigitalAnalogConverter.SetValue(DAC_CHANNELY, GetYPositionActual());
//      FStateTick = 2;
//      SetTimeStamp();
//      break;
//    case 2: 


//      DigitalAnalogConverter.
//      if (GetProcessPeriod() < GetTimeSpan())
//      { // Position XY
//        
//        FStateTick = 2;
//        SetTimeStamp();
//      }
//      break;
//
//
//
//        
//        LedSystem.On();
//        SetState(spBlinkLedSystem, 1);
//        FStateTick = 2;
//        SetTimeStamp();
//      }
//      break;
//    case 2: 
//      if (GetProcessPeriodHalf() < GetTimeSpan())
//      {
//        LedSystem.Off();
//        SetState(spBlinkLedSystem, 0);
//        FProcessIndex++;
//        if (FProcessIndex < FProcessCount)
//        {
//          FStateTick = 1;          
//        }
//        else
//        {
//          SetState(spIdle);
//        }
//        SetTimeStamp(); 
//      }
//      break;
//  }  

void CProcess::HandleSetMotionRangeX(CSerial &serial)
{
  
}

void CProcess::HandleSetMotionRangeY(CSerial &serial)
{
  
}

void CProcess::HandleSetDelayMotion(CSerial &serial)
{
  
}

void CProcess::HandleSetDelayPulse(CSerial &serial)
{
  
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserMatrix
//----------------------------------------------------------
//
void CProcess::HandleLaserMatrixBegin(CSerial &serial)
{ 
//  UInt32 XPA = GetXPositionMinimum();
//  SetXPositionActual(XPA);
//  UInt32 YPA = GetYPositionMinimum();
//  SetYPositionActual(YPA);
//#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
//  DigitalAnalogConverter.SetValue(DAC_CHANNELX, GetXPositionActual());
//  DigitalAnalogConverter.SetValue(DAC_CHANNELY, GetYPositionActual());  
//#endif
//  LedLaser.SetPulsePeriodCount(GetLaserPulsePeriod(), 
//                               GetPositionPulseCountPreset());
//  SetTotalPulseCountActual(0);
//  SetPositionPulseCountActual(0);
//  SetTimeStamp();
//  LedLaser.On();
//  SetState(saPulseMatrixLaserBusy);
}

void CProcess::HandleLaserMatrixBusy(CSerial &serial)
{

}
//  SetTimeMarker();
//  if (LedLaser.GetPulsePeriodHalf() <= GetTimePeriod())
//  {
//    if (slOff == LedLaser.GetState())
//    { // Off -> On
//      SetTimeStamp();
//      LedLaser.On();
//    }
//    else
//      if (slOn == LedLaser.GetState())
//      { // On -> Off
//        SetTimeStamp();
//        LedLaser.Off();
//        IncrementTotalPulseCountActual();
//        IncrementPositionPulseCountActual();        
//        if (LASProcess.IsPositionPulseCountReached())
//        { // All Pulses for one Location pulsed -> zero PulseCount, next X/Y-Position, new X-Col
//          SetPositionPulseCountActual(0);
//          IncrementXPosition();
//          if (LASProcess.GetXPositionMaximum() < LASProcess.GetXPositionActual())
//          { // new Y-Row
//            SetXPositionActual(GetXPositionMinimum());            
//            IncrementYPosition();
//            if (GetYPositionMaximum() < LASProcess.GetYPositionActual())
//            { 
//              LedLaser.Off();
//              SetState(saPulseMatrixLaserEnd);  
//              return;
//            }
//          }
//        }
//#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
//        DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
//        DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());  
//#endif
//      }
//  }
//}

void CProcess::HandleLaserMatrixAbort(CSerial &serial)
{
//  LedLaser.Off();
  SetState(spLaserMatrixEnd);    
}

void CProcess::HandleLaserMatrixEnd(CSerial &serial)
{
//  LedLaser.Off();
  SetState(spIdle);    
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserImage
//----------------------------------------------------------
//
void CProcess::HandleLaserImageBegin(CSerial &serial)
{
  SetState(spIdle); 
}

void CProcess::HandleLaserImageBusy(CSerial &serial)
{
  SetState(spIdle); 
}

void CProcess::HandleLaserImageAbort(CSerial &serial)
{
  SetState(spIdle); 
}

void CProcess::HandleLaserImageEnd(CSerial &serial)
{
  SetState(spIdle); 
}
//
//----------------------------------------------------------
//  Segment - Collector
//----------------------------------------------------------
//
void CProcess::Handle(CSerial &serial)
{
  switch (LASProcess.GetState())
  { // Common
    case spIdle:
      HandleIdle(serial);
      break;
    case spWelcome:
      HandleWelcome(serial);
      break;
    case spGetHelp:
      HandleGetHelp(serial);
      break;
    case spGetProgramHeader:
      HandleGetProgramHeader(serial);
      break;
    case spGetSoftwareVersion:
      HandleGetSoftwareVersion(serial);
      break;
    case spGetHardwareVersion:
      HandleGetHardwareVersion(serial);
      break;
    case spSetProcessCount:
      HandleSetProcessCount(serial);
      break;
    case spSetProcessPeriod:
      HandleSetProcessPeriod(serial);
      break;
    case spStopProcessExecution:
      HandleStopProcessExecution(serial);
      break;
    // LedSystem
    case spGetLedSystem:
      HandleGetLedSystem(serial);
      break;
    case spLedSystemOn:
      HandleLedSystemOn(serial);
      break;
    case spLedSystemOff:
      HandleLedSystemOff(serial);
      break;
    case spBlinkLedSystem:
      HandleBlinkLedSystem(serial);
      break;
    // Measurement - LaserPosition
    case spLaserPositionPulse:
      HandleLaserPositionPulse(serial);
      break;
    case spLaserPositionAbort:
      HandleLaserPositionAbort(serial);
      break;
    // Measurement - LaserParameter
    case spSetMotionRangeX:
      HandleSetMotionRangeX(serial);
      break;
    case spSetMotionRangeY:
      HandleSetMotionRangeY(serial);
      break;
    case spSetDelayMotion:
      HandleSetDelayMotion(serial);
      break;
    case spSetDelayPulse:
      HandleSetDelayPulse(serial);
      break;
    // Measurement - LaserMatrix
    case spLaserMatrixBegin:
      HandleLaserMatrixBegin(serial);
      break;
    case spLaserMatrixBusy:
      HandleLaserMatrixBusy(serial);
      break;
    case spLaserMatrixAbort:
      HandleLaserMatrixAbort(serial);
      break;
    case spLaserMatrixEnd:
      HandleLaserMatrixEnd(serial);
      break;
//    // Measurement - LaserImage
//    case spStartLaserImage:
//      HandleStartLaserImage(serial);
//      break;
//    case spPulseLaserImage:
//      HandlePulseLaserImage(serial);
//      break;
//    case spAbortLaserImage:
//      HandleAbortLaserImage(serial);
//      break;
    default: // spUndefined
      HandleUndefined(serial);
      break;
  }  
}
