#include "Led.h"
#include "Process.h"
#include "Command.h"
#include "Dac.h"
//
extern CLed LedSystem;
extern CSerial SerialCommand;
extern CProcess LASProcess;
extern CCommand LASCommand;
extern CDac DigitalAnalogConverter;
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CProcess::CProcess()
{
  FState = spUndefined;
  FStateTick = 0;
  FTimeStamp = micros();
  FTimeMark = FTimeStamp;
  FProcessCount = INIT_PROCESSCOUNT;
  FProcessPeriod = INIT_PROCESSPERIOD_US;
  FProcessStep = 0;
  FDelayPulse = INIT_DELAY_PULSE_US;
  FDelayMotion = INIT_DELAY_MOTION_US;
  FXPositionMinimum = INIT_XPOSITION_MINIMUM;
  FXPositionMaximum = INIT_XPOSITION_MAXIMUM;
  FXPositionDelta = INIT_XPOSITION_DELTA;
  FXPositionActual = INIT_XPOSITION_ACTUAL;
  FYPositionMinimum = INIT_YPOSITION_MINIMUM;
  FYPositionMaximum = INIT_YPOSITION_MAXIMUM;
  FYPositionDelta = INIT_YPOSITION_DELTA;
  FYPositionActual = INIT_YPOSITION_ACTUAL;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateProcess CProcess::GetState()
{
  return FState;
}
void CProcess::SetState(EStateProcess state, UInt8 substate)
{
  if ((state != FState) || (substate != FSubstate))
  {     
    if (state != FState)
    {
      FStateTick = 0;
    }
    FState = state;
    FSubstate = substate;
    switch (FState)
    {
      case spStopProcessExecution:
        break;
    }
    // Message to Manager(PC):
    LASCommand.CallbackStateProcess(SerialCommand, FState, substate);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CProcess::Open()
{
  SetState(spIdle);
  return true;
}

Boolean CProcess::Close()
{
  SetState(spUndefined);
  return true;
}

//
//----------------------------------------------------------
//  Segment - Common
//----------------------------------------------------------
//
void CProcess::HandleUndefined(CSerial &serial)
{
  delay(1000);
}

void CProcess::HandleIdle(CSerial &serial)
{ // do nothing
  SetTimeStamp();
}

void CProcess::HandleWelcome(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:        
      SetTimeStamp();
      FStateTick++;
      break;
    case 1: case 3: case 5: 
      if (330000 < GetTimeSpan())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 2: case 4: case 6:  
      if (330000 < GetTimeSpan())
      {
        SetTimeStamp();
        FStateTick++;        
      }
      break;
    case 7:  
      FStateTick++;      
      SetTimeStamp();      
      break;
    case 8:   
      if (1000000 < GetTimeSpan())
      {   
        FStateTick++;
        SetTimeStamp();
      }
      break;
    case 9:   
      if (1000000 < GetTimeSpan())
      {   
        FStateTick++;
        
        SetState(spIdle);
// alternative : 
//        UInt8 CL = 0;
//        UInt8 CH = 7;
//        LASProcess.SetChannelLow(CL);
//        LASProcess.SetChannelHigh(CH);
//        LASProcess.SetState(spRepeatAllTemperatures);
//        LASProcess.SetProcessIndex(0);
//        SetState(spRepeatAllTemperatures);
        SetTimeStamp();
      }
      break;
  }
}

void CProcess::HandleGetHelp(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProgramHeader(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetSoftwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetHardwareVersion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessCount(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessCount(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetProcessPeriod(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetProcessPeriod(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleStopProcessExecution(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - LedSystem
//----------------------------------------------------------
//
void CProcess::LedSystemState()
{
  switch (LedSystem.GetState())
  {
    case slOn:
      break;
    case slOff:
      break;
    default:
      break;
  }
  SetState(spIdle, LedSystem.GetState());
}

void CProcess::HandleGetLedSystem(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOn(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleLedSystemOff(CSerial &serial)
{
  LedSystemState();
}

void CProcess::HandleBlinkLedSystem(CSerial &serial)
{
  SetTimeMark();
  switch (FStateTick)
  {
    case 0:              
      FStateTick++;
      SetTimeStamp();
      break;
    case 1: 
      if (GetProcessPeriodHalf() < GetTimeSpan())
      {
        LedSystem.On();
        SetState(spBlinkLedSystem, 1);
        FStateTick = 2;
        SetTimeStamp();
      }
      break;
    case 2: 
      if (GetProcessPeriodHalf() < GetTimeSpan())
      {
        LedSystem.Off();
        SetState(spBlinkLedSystem, 0);
        FProcessStep++;
        if (FProcessStep < FProcessCount)
        {
          FStateTick = 1;          
        }
        else
        {
          SetState(spIdle);
        }
        SetTimeStamp(); 
      }
      break;
  } 
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserPosition
//----------------------------------------------------------
// Command: PulseLaserPosition PLP <x> <y> <p> <c> : Pulse Laser <x><y> <p>eriod <c>ount"
// Command: Definition of XPositionActual, YPositionActual, ProcessPeriod, ProcessCount
// 
void CProcess::HandlePulseLaserPosition(CSerial &serial)
{ 
  SetTimeMark();
  switch (FStateTick)
  {
    case 0: // XY
      SetTimeStamp();
      if (0 < FProcessCount)
      {
        DigitalAnalogConverter.SetValue(DAC_CHANNELX, GetXPositionActual());
        DigitalAnalogConverter.SetValue(DAC_CHANNELY, GetYPositionActual());
        FStateTick = 1;
      }
      else
      { // no pulses -> Idle
        FStateTick = 3;
      }
      break;
    case 1:       
      if (slOn != LedSystem.GetState()) LedSystem.On();
      if (GetProcessPeriodHalf() <= GetTimeSpan())
      {
        FStateTick = 2;
      }
      break;
    case 2: 
      if (slOff != LedSystem.GetState()) LedSystem.Off();
      if (GetProcessPeriod() <= GetTimeSpan())
      {
        FStateTick = 3;
      }
      break;
    case 3: 
      SetTimeStamp();
      LedSystem.Off();
      FStateTick = 1;
      FProcessStep++;
      if (FProcessCount <= FProcessStep)
      {
        SetState(spIdle);
      }
      break;
  }
}

void CProcess::HandleAbortLaserPosition(CSerial &serial)
{  
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserRange
//----------------------------------------------------------
//
void CProcess::HandleGetPositionX(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetRangeX(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetRangeX(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetPositionY(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetRangeY(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetRangeY(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetDelayMotion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetDelayMotion(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleGetDelayPulse(CSerial &serial)
{
  SetState(spIdle);
}

void CProcess::HandleSetDelayPulse(CSerial &serial)
{
  SetState(spIdle);
}
//
//----------------------------------------------------------
// Segment - Measurement - LaserMatrix
//----------------------------------------------------------
//
void CProcess::HandlePulseLaserMatrix(CSerial &serial)
{ 
  SetTimeMark();
  switch (FStateTick)
  {
    case 0: // Init Motion
// debug Serial.print("1: Init Motion PPQ["); Serial.print(GetProcessPeriodQuarter());
// debug Serial.print(" PPH["); Serial.print(GetProcessPeriodHalf());
// debug Serial.print(" PPF["); Serial.print(GetProcessPeriod());
// debug Serial.println("]");
      SetTimeStamp();
      if (0 < GetProcessCount())//GetLaserPulseCountPreset())
      {
        SetXPositionActual(GetXPositionMinimum());
        SetYPositionActual(GetYPositionMinimum());
        FStateTick = 1;
      }
      else
      { // no pulses -> Idle
        SetState(spIdle);
      }
      break;
    case 1: // PositionXY
      DigitalAnalogConverter.SetValue(DAC_CHANNELY, GetYPositionActual());
      DigitalAnalogConverter.SetValue(DAC_CHANNELX, GetXPositionActual());
// debug Serial.print("1: XPA["); Serial.print(FXPositionActual);  
// debug Serial.print("] YPA["); Serial.print(FYPositionActual); 
// debug Serial.println("]");      
      FStateTick = 2;
      break;
    case 2: // PulseH
//Serial.println("2: PulseH");
      if (slOn != LedSystem.GetState()) 
      { // debug Serial.println("2: LedSystem: ON");
        LedSystem.On();
      }      
      if (GetProcessPeriodHalf() <= GetTimeSpan())      
      { //Serial.print("[H]");        
        FStateTick = 3;
      }
      break;
    case 3: // PulseL, more Pulses?
// Serial.println("3: PulseL");    
      if (slOff != LedSystem.GetState()) 
      { // debug Serial.println("3: LedSystem: OFF");
        LedSystem.Off();
      }
      if (GetProcessPeriod() <= GetTimeSpan())
      {
        SetTimeStamp(); ///!!!!!
        IncrementProcessStep();//FLaserPulseCountActual++;    
// debug Serial.print("Puls["); Serial.print(GetProcessStep()); //Serial.print(FLaserPulseCountActual);
// debug Serial.print("/"); Serial.print(GetProcessCount()); //Serial.print(FLaserPulseCountPreset);
// debug Serial.println("]");
        if (GetProcessCount() <= GetProcessStep())//(FLaserPulseCountPreset <= FLaserPulseCountActual)//IncrementLaserPulseCountActual())
        { // next Position
          SetProcessStep(0);//FLaserPulseCountActual = 0;
          FStateTick = 4;
        }
        else
        { // more Pulses
          FStateTick = 2;
        }
      }      
      break;
    case 4: // Next Position
      FXPositionActual += FXPositionDelta; // IncrementXPositionActual(GetXPositionDelta());
      if (FXPositionMaximum < FXPositionActual) // GetXPositionMaximum() < GetXPositionActual())
      {        
        FYPositionActual += FYPositionDelta; // IncrementYPositionActual(GetYPositionDelta());
        if (FYPositionMaximum < FYPositionActual) // (GetYPositionMaximum() < GetYPositionActual())
        { // debug Serial.println("4: End");
          SetState(spIdle);
        }
        else
        { // debug Serial.println("4: Next Position");
          SetXPositionActual(GetXPositionMinimum());          
        }
      }
      FStateTick = 1;
      break;
  }
}


  
//  UInt32 XPA = GetXPositionMinimum();
//  SetXPositionActual(XPA);
//  UInt32 YPA = GetYPositionMinimum();
//  SetYPositionActual(YPA);
//#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
//  DigitalAnalogConverter.SetValue(DAC_CHANNELX, GetXPositionActual());
//  DigitalAnalogConverter.SetValue(DAC_CHANNELY, GetYPositionActual());  
//#endif
//  LedLaser.SetPulsePeriodCount(GetLaserPulsePeriod(), 
//                               GetPositionPulseCountPreset());
//  SetTotalPulseCountActual(0);
//  SetPositionPulseCountActual(0);
//  SetTimeStamp();
//  LedLaser.On();
//  SetState(saPulseMatrixLaserBusy);
//}

void CProcess::HandleAbortLaserMatrix(CSerial &serial)
{
  LedSystem.Off();
  SetState(spIdle);
}



//void CProcess::HandleLaserMatrixBusy(CSerial &serial)
//{
//
//}
//  SetTimeMarker();
//  if (LedLaser.GetPulsePeriodHalf() <= GetTimePeriod())
//  {
//    if (slOff == LedLaser.GetState())
//    { // Off -> On
//      SetTimeStamp();
//      LedLaser.On();
//    }
//    else
//      if (slOn == LedLaser.GetState())
//      { // On -> Off
//        SetTimeStamp();
//        LedLaser.Off();
//        IncrementTotalPulseCountActual();
//        IncrementPositionPulseCountActual();        
//        if (LASProcess.IsPositionPulseCountReached())
//        { // All Pulses for one Location pulsed -> zero PulseCount, next X/Y-Position, new X-Col
//          SetPositionPulseCountActual(0);
//          IncrementXPosition();
//          if (LASProcess.GetXPositionMaximum() < LASProcess.GetXPositionActual())
//          { // new Y-Row
//            SetXPositionActual(GetXPositionMinimum());            
//            IncrementYPosition();
//            if (GetYPositionMaximum() < LASProcess.GetYPositionActual())
//            { 
//              LedLaser.Off();
//              SetState(saPulseMatrixLaserEnd);  
//              return;
//            }
//          }
//        }
//#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
//        DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
//        DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());  
//#endif
//      }
//  }
//}
//
//void CProcess::HandleLaserMatrixEnd(CSerial &serial)
//{
////  LedLaser.Off();
//  SetState(spIdle);    
//}
//
//----------------------------------------------------------
// Segment - Measurement - LaserImage
//----------------------------------------------------------
//
void CProcess::HandleLaserImageBegin(CSerial &serial)
{
  SetState(spIdle); 
}

void CProcess::HandleLaserImageBusy(CSerial &serial)
{
  SetState(spIdle); 
}

void CProcess::HandleLaserImageAbort(CSerial &serial)
{
  SetState(spIdle); 
}

void CProcess::HandleLaserImageEnd(CSerial &serial)
{
  SetState(spIdle); 
}
//
//----------------------------------------------------------
//  Segment - Collector
//----------------------------------------------------------
//
void CProcess::Handle(CSerial &serial)
{
  switch (LASProcess.GetState())
  { // Common
    case spIdle:
      HandleIdle(serial);
      break;
    case spWelcome:
      HandleWelcome(serial);
      break;
    case spGetHelp:
      HandleGetHelp(serial);
      break;
    case spGetProgramHeader:
      HandleGetProgramHeader(serial);
      break;
    case spGetSoftwareVersion:
      HandleGetSoftwareVersion(serial);
      break;
    case spGetHardwareVersion:
      HandleGetHardwareVersion(serial);
      break;
    case spGetProcessCount:
      HandleGetProcessCount(serial);
      break;
    case spSetProcessCount:
      HandleSetProcessCount(serial);
      break;
    case spGetProcessPeriod:
      HandleGetProcessPeriod(serial);
      break;
    case spSetProcessPeriod:
      HandleSetProcessPeriod(serial);
      break;
    case spStopProcessExecution:
      HandleStopProcessExecution(serial);
      break;
    // LedSystem
    case spGetLedSystem:
      HandleGetLedSystem(serial);
      break;
    case spLedSystemOn:
      HandleLedSystemOn(serial);
      break;
    case spLedSystemOff:
      HandleLedSystemOff(serial);
      break;
    case spBlinkLedSystem:
      HandleBlinkLedSystem(serial);
      break;
    // Measurement - LaserPosition
    case spPulseLaserPosition:
      HandlePulseLaserPosition(serial);
      break;
    case spAbortLaserPosition:
      HandleAbortLaserPosition(serial);
      break;
    // Measurement - LaserParameter
    case spGetPositionX:
      HandleGetPositionX(serial);
      break;
    case spGetRangeX:
      HandleGetRangeX(serial);
      break;
    case spSetRangeX:
      HandleSetRangeX(serial);
      break;
    case spGetPositionY:
      HandleGetPositionY(serial);
      break;
    case spGetRangeY:
      HandleGetRangeY(serial);
      break;
    case spSetRangeY:
      HandleSetRangeY(serial);
      break;
    case spGetDelayMotion:
      HandleGetDelayMotion(serial);
      break;
    case spSetDelayMotion:
      HandleSetDelayMotion(serial);
      break;
    case spGetDelayPulse:
      HandleGetDelayPulse(serial);
      break;
    case spSetDelayPulse:
      HandleSetDelayPulse(serial);
      break;
    // Measurement - LaserMatrix
    case spPulseLaserMatrix:
      HandlePulseLaserMatrix(serial);
      break;
    case spAbortLaserMatrix:
      HandleAbortLaserMatrix(serial);
      break;
//    // Measurement - LaserImage
//    case spStartLaserImage:
//      HandleStartLaserImage(serial);
//      break;
//    case spPulseLaserImage:
//      HandlePulseLaserImage(serial);
//      break;
//    case spAbortLaserImage:
//      HandleAbortLaserImage(serial);
//      break;
    default: // spUndefined
      HandleUndefined(serial);
      break;
  }  
}
