//
//--------------------------------
//  Library LCDisplayI2C
//--------------------------------
//
#ifndef LCDisplayI2C_h
#define LCDisplayI2C_h
//
#include <Wire.h>
#include "DriverLCDisplayI2C.h"
//
#include "Defines.h"
//
const Byte LCD_ADDRESSI2C = 0x27;
const Byte LCD_COLCOUNT = 20; 
const Byte LCD_ROWCOUNT = 4;
//
//--------------------------------
//  Section - CLCDisplayI2C
//--------------------------------
//
class CLCDisplayI2C
{
  private:
  CDriverLCDisplayI2C* FPLCDisplay; 
  UInt8 FAddressI2C;
  UInt8 FRowCount, FColCount;
  
  public:
  CLCDisplayI2C(UInt8 addressi2c = LCD_ADDRESSI2C, UInt8 colcount = LCD_COLCOUNT, UInt8 rowcount = LCD_ROWCOUNT);
  //
  Boolean Open();
  Boolean Close();
  // ERROR! Boolean FindAddressI2C();
  Byte GetAddressI2C();
  //
  void SetBacklightOn();
  void SetBacklightOff();
  void SetHomePosition();
  void ClearScreen();
  void SetCursorPosition(Byte row, Byte col);
  void Write(String text);
  void WriteByte(Byte value);
  void WriteUInt8(UInt8 value);
  void WriteInt16(Int16 value);
};
//
#endif // LCDisplayI2C_h
