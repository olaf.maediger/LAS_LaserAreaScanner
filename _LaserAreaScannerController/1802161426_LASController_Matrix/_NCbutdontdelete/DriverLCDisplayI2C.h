#ifndef DriverLCDisplayI2C_H
#define DriverLCDisplayI2C_H
//
#include <Arduino.h>
#include <inttypes.h>
#include <Print.h>
#include <Wire.h>
#include "Defines.h"
//
// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00

// flags for backlight control
#define LCD_BACKLIGHT 0x08
#define LCD_NOBACKLIGHT 0x00

#define En B00000100  // Enable bit
#define Rw B00000010  // Read/Write bit
#define Rs B00000001  // Register select bit
//
class CDriverLCDisplayI2C : public Print 
{
  private:
  UInt8 FAddressI2C;
  UInt8 FDisplayFunction;
  UInt8 FDisplayControl;
  UInt8 FDisplayMode;
  UInt8 FColCount;
  UInt8 FRowCount;
  UInt8 FCharacterSize;
  UInt8 FBacklightValue;
  //
  void Send(UInt8, UInt8);
  void Write4Bits(UInt8);
  void ExpanderWrite(UInt8);
  void PulseEnable(UInt8);
  //
  public:
	/**
	 * Constructor
	 *
	 * @param lcd_addr	I2C slave address of the LCD display. Most likely printed on the
	 *					LCD circuit board, or look in the supplied LCD documentation.
	 * @param lcd_cols	Number of columns your LCD display has.
	 * @param lcd_rows	Number of rows your LCD display has.
	 * @param charsize	The size in dots that the display has, use LCD_5x10DOTS or LCD_5x8DOTS.
	 */
	CDriverLCDisplayI2C(UInt8 addressi2c, UInt8 colcount, UInt8 rowcount, UInt8 charactersize = LCD_5x8DOTS);

	/**
	 * Set the LCD display in the correct begin state, must be called before anything else is done.
	 */
	void Open();

	 /**
	  * Remove all the characters currently shown. Next print/write operation will start
	  * from the first position on LCD display.
	  */
	void Clear();

	/**
	 * Next print/write operation will will start from the first position on the LCD display.
	 */
	void Home();

	 /**
	  * Do not show any characters on the LCD display. Backlight state will remain unchanged.
	  * Also all characters written on the display will return, when the display in enabled again.
	  */
	void HideDisplay();

	/**
	 * Show the characters on the LCD display, this is the normal behaviour. This method should
	 * only be used after noDisplay() has been used.
	 */
	void ShowDisplay();

	/**
	 * Do not blink the cursor indicator.
	 */
	void DisableCursorBlinking();

	/**
	 * Start blinking the cursor indicator.
	 */
	void EnableCursorBlinking();

	/**
	 * Do not show a cursor indicator.
	 */
  void ShowCursor();//UInt8 col, UInt8 row);
	void HideCursor();

	/**
 	 * Show a cursor indicator, cursor can blink on not blink. Use the
	 * methods blink() and noBlink() for changing cursor blink.
	 */
	void GetCursorBlinking();

	void ScrollDisplayLeft();
	void ScrollDisplayRight();
	void PrintLeft();
	void PrintRight();
	void LeftToRight();
	void RightToLeft();
	void ShiftIncrement();
	void ShiftDecrement();
	void SetBacklightOff();
	void SetBacklightOn();
	bool GetBacklight();
	void EnableAutoScrolling();
	void DisableAutoScrolling();
	void CreateCharacter(UInt8, UInt8[]);
	void SetCursorPosition(UInt8, UInt8);
	virtual size_t write(UInt8);
	void Command(UInt8);
//
//	inline void BlinkOn() { EnableCursorBlinking(); }
//	inline void BlinkOff() { DisableCursorBlinking(); }
//	inline void CursorOn() { ShowCursor(); }
//	inline void CursorOff() { HideCursor(); }

// Compatibility API function aliases
	void SetBacklight(UInt8 new_val);				
	void LoadCustomCharacter(UInt8 char_num, UInt8 *rows);
	void PrintString(const char[]);
};
//
#endif // DriverLCDisplayI2C_H
