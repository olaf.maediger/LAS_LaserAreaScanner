//
//--------------------------------
//  Library LedLine
//--------------------------------
//
#ifndef LedLine_h
#define LedLine_h
//
#include <vector>
#include "Arduino.h"
#include "Defines.h"
#include "Led.h"
//
using namespace std;
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
class CLedLine
{
  private:
  std::vector<CLed> FLeds;
  
  public:
  CLedLine(vector<int> pins);
  int GetLedCount();
  EStateLed GetState(int ledindex);
  Boolean Open();
  Boolean Close();
  void On();
  void Off();
  void Toggle();  
  void Pulse(UInt32 microseconds);
  void On(byte line);
  void Off(byte line);
  void Toggle(byte line);
  void Pulse(byte line, UInt32 microseconds);
};
//
#endif
