#include "Utilities.h"
#include "Led.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
//
//
//-------------------------------------------
//  Segment - Global Variable
//-------------------------------------------
//  
extern enum EErrorCode GlobalErrorCode;
//
extern EStateAutomation StateAutomation;
extern UInt32 CountRepetition; // [1]
extern UInt32 CountLaserPulses; // [1]
extern UInt32 DelayMotion; // [us]
extern UInt32 DelayPulse; // [us]
//
extern UInt16 XPositionActual; // [stp]
extern UInt16 XPositionDelta; // [stp]
extern UInt16 XPositionMinimum; // [stp]
extern UInt16 XPositionMaximum; // [stp]
//
extern UInt16 YPositionActual; // [stp]
extern UInt16 YPositionDelta; // [stp]
extern UInt16 YPositionMinimum; // [stp]
extern UInt16 YPositionMaximum; // [stp]
//
//-------------------------------------------
//  Segment - Global Class
//-------------------------------------------
//  
extern CLed LedSystem;
extern CLed LedError;
extern CLed LedBusy;
extern CLed LedMotionX;
extern CLed LedMotionY;
extern CLed LedTrigger;
//
// NC extern CAdc AnalogDigitalConverter;
//
extern CDac DigitalAnalogConverter;
//
// NC extern CPwm PulseWidthModulator;
//
extern CSerialCommand SerialCommand;
//
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("---------------------------------------");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Project:   LAS - LaserAreaScanner   -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Version:   02V06                    -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Hardware:  ArduinoDue               -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Date:      161206                   -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Time:      0929                     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Author:    OM                       -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Port:      SerialPC (SerialUSB)     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Parameter: 115200, N, 8, 1          -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("---------------------------------------");
  serial.WriteNewLine();	
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Software-Version LAS02V06");
  serial.WriteNewLine();
}

void WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Hardware-Version ArduinoDue3");
  serial.WriteNewLine();
}

void WriteHelp(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Help (Common):");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" H   : This Help");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GPH <fdl> : Get Program Header");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GSV <fdl> : Get Software-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GHV <fdl> : Get Hardware-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Led):");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GLD <i> : Get State Led with <i>ndex[0..4]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" SLD <i> : Set Led with <i>ndex[0..4]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" CLD <i> : Clear Led with <i>ndex[0..4]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" TLD <i> <n> <d>: Toggle Led <i>ndex[0..4] <n>times with <d>elay{us}");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Dac):");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" GDV <i> : Get Value from DAConverter with Index<0..1>");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SDV <i> <v>: Set Value<0..4095> to DAConverter with Index[0..1]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
//  serial.WriteText(" Help (Adc):");
//  serial.WriteNewLine();
//  serial.WriteAnswer();    
//  serial.WriteText(" GAV <i> : Get Value from ADConverter with <i>ndex[0..11]");
//  serial.WriteNewLine();
//  serial.WriteAnswer();
  //
//  serial.WriteText(" Help (Pwm):");
//  serial.WriteNewLine();
//  serial.WriteAnswer();    
//  serial.WriteText(" GPV <i> : Get Value from PWModulator with <i>ndex[0..11]");
//  serial.WriteNewLine();
//  serial.WriteAnswer();    
//  serial.WriteText(" SPV <i> <v>: Set <v>alue[0..1023] to PWModulator <i>ndex[0..11]");
//  serial.WriteNewLine();
  //
  serial.WriteText(" Help (Automation):");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SMX <xmin> <xmax> <dx>: Set Motion-ParameterX[0..4095]");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SMY <ymin> <ymax> <dy>: Set Motion-ParameterY[0..4095]");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SDM <delay>: Set Delay Motion{us}");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SDP <delay>: Set Delay Pulse{us}");
  serial.WriteNewLine();
  serial.WriteAnswer();    
//  serial.WriteText(" STM <d> <r> : Start Motion <d>elaytrigger{us} with <r>epetitions");
//  serial.WriteNewLine();
//  serial.WriteAnswer();    
  serial.WriteText(" STT <n> <r> : Start Trigger <n>Pulses with <r>epetitions");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" ABM : Abort Motion");
  serial.WriteNewLine();
}
//
//#########################################################
//  Segment - Execution - Common
//#########################################################
//
void ExecuteGetHelp(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHelp(serial);
  serial.WritePrompt();
}

void ExecuteGetProgramHeader(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void ExecuteGetSoftwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void ExecuteGetHardwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Led
//#########################################################
//
void ExecuteGetLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  int State;
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      State = LedSystem.GetState();
      break;
    case LED_ERROR:
      State = LedError.GetState();
      break;
    case LED_BUSY:
      State = LedBusy.GetState();
      break;
    case LED_MOTIONX:
      State = LedMotionX.GetState();
      break;
    case LED_MOTIONY:
      State = LedMotionY.GetState();
      break;
    case LED_TRIGGER:
      State = LedTrigger.GetState();
      break;
  }
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s %i", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0), State);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteClearLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.Off();
      break;
    case LED_ERROR:
      LedError.Off();
      break;
    case LED_BUSY:
      LedBusy.Off();
      break;
    case LED_MOTIONX:
      LedMotionX.Off();
      break;
    case LED_MOTIONY:
      LedMotionY.Off();
      break;
    case LED_TRIGGER:
      LedTrigger.Off();
      break;
  }
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0));
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSetLed(CSerial &serial)
{	// Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.On();
      break;
    case LED_ERROR:
      LedError.On();
      break;
    case LED_BUSY:
      LedBusy.On();
      break;
    case LED_MOTIONX:
      LedMotionX.On();
      break;
    case LED_MOTIONY:
      LedMotionY.On();
      break;
    case LED_TRIGGER:
      LedTrigger.On();
      break;
  }
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0));
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteToggleLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  int Delay = atoi(SerialCommand.GetPRxdParameters(1));
  int Count = atoi(SerialCommand.GetPRxdParameters(2));
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0));
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
  // Execute:
  int I;
  for (I = 0; I < Count - 1; I++)
  {
    switch (Index)
    {
      case LED_SYSTEM:
        LedSystem.Toggle();
        delay(Delay);
        break;
      case LED_ERROR:
        LedError.Toggle();
        delay(Delay);
        break;
      case LED_BUSY:
        LedBusy.Toggle();
        delay(Delay);
        break;
      case LED_MOTIONX:
        LedMotionX.Toggle();
        delay(Delay);
        break;
      case LED_MOTIONY:
        LedMotionY.Toggle();
        delay(Delay);
        break;
      case LED_TRIGGER:
        LedTrigger.Toggle();
        delay(Delay);
        break;
    }
  }
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.Toggle();
      break;
    case LED_ERROR:
      LedError.Toggle();
      break;
    case LED_BUSY:
      LedBusy.Toggle();
      break;
    case LED_MOTIONX:
      LedMotionX.Toggle();
      break;
    case LED_MOTIONY:
      LedMotionY.Toggle();
      break;
    case LED_TRIGGER:
      LedTrigger.Toggle();
      break;
  }
}
////
////#########################################################
////  Segment - Execution - Adc
////#########################################################
////
//void ExecuteGetAdcValue(CSerial &serial)
//{  // Analyse parameters:
//  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
//  // Execute:
//  UInt16 Value = AnalogDigitalConverter.GetValue(Channel);
//  // Answer:
//  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %u", 
//            SerialCommand.GetPRxdCommand(), Channel, Value);
//    serial.WriteLine(SerialCommand.GetTxdBuffer());
//  serial.WritePrompt();
//}
//
//#########################################################
//  Segment - Execution - Dac
//#########################################################
//
void ExecuteGetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  UInt16 Value = DigitalAnalogConverter.GetValue(Channel);
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %u", 
          SerialCommand.GetPRxdCommand(), Channel, Value);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
  UInt16 Value = atoi(SerialCommand.GetPRxdParameters(1));
  // Execute:
  DigitalAnalogConverter.SetValue(Channel, Value);
  Value = DigitalAnalogConverter.GetValue(Channel);
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %u", 
          SerialCommand.GetPRxdCommand(), Channel, Value);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}
////
////#########################################################
////  Segment - Execution - Pwm
////#########################################################
////
//void ExecuteGetPwmValue(CSerial &serial)
//{  // Analyse parameters:
//  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
//  // Execute:
//  UInt16 Value = PulseWidthModulator.GetValue(Channel);
//  // Answer:
//  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %u", PRxdCommand, Channel, Value);
//    serial.WriteLine(SerialCommand.GetTxdBuffer());
//  serial.WritePrompt();
//}
//
//void ExecuteSetPwmValue(CSerial &serial)
//{  // Analyse parameters:
//  UInt8 Channel = atoi(PRxdParameters[0]);
//  UInt16 Value = atoi(PRxdParameters[1]);
//  // Execute:
//  PulseWidthModulator.SetValue(Channel, Value);
//  Value = PulseWidthModulator.GetValue(Channel);
//  // Answer:
//  sprintf(SerialCommand.GetTxdBuffer()r, "# %s %u %u", PRxdCommand, Channel, Value);
//  serial.WriteLine(SerialCommand.GetTxdBuffer());
//  serial.WritePrompt();
//}
//
//#########################################################
//  Segment - Execution - Helper
//#########################################################
//
void SetPosition()
{
  DigitalAnalogConverter.SetValue(DAC_MIRRORX, XPositionActual);
  DigitalAnalogConverter.SetValue(DAC_MIRRORY, YPositionActual);  
}

void SetTrigger()
{
  LedTrigger.On();
  delayMicroseconds(DelayPulse / 2);
  LedMotionY.Off();
  LedMotionX.Off();
  delayMicroseconds(DelayPulse / 2);
  LedTrigger.Off();
}

void TriggerLaserPulses()
{ // Pulsedelay fixed to 1000us!
  LedMotionY.Off();
  LedMotionX.Off();
  // wait for motion stops...
  delayMicroseconds(DelayMotion);
  //
  UInt32 TI;
  UInt32 DL = DelayPulse / 10;
  UInt32 DH = 9* DL;
  for (TI = 0; TI < CountLaserPulses; TI++)
  {    
    LedTrigger.On();
    delayMicroseconds(DH);
    LedTrigger.Off();
    delayMicroseconds(DL);
  }
}
//
//#########################################################
//  Segment - Execution - Automation
//#########################################################
//
void ExecuteSetMotionX(CSerial &serial)
{ // Analyse parameters:
  XPositionMinimum = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  XPositionMaximum = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  XPositionDelta = abs(atoi(SerialCommand.GetPRxdParameters(2)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu %lu", 
          SerialCommand.GetPRxdCommand(), XPositionMinimum, XPositionMaximum, XPositionDelta);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

void ExecuteSetMotionY(CSerial &serial)
{ // Analyse parameters:
  YPositionMinimum = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  YPositionMaximum = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  YPositionDelta = abs(atoi(SerialCommand.GetPRxdParameters(2)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu %lu", 
          SerialCommand.GetPRxdCommand(), YPositionMinimum, YPositionMaximum, YPositionDelta);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

void ExecuteSetDelayMotion(CSerial &serial)
{ // Analyse parameters:
  DelayMotion = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu", 
          SerialCommand.GetPRxdCommand(), DelayMotion);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

void ExecuteSetDelayPulse(CSerial &serial)
{ // Analyse parameters:
  DelayPulse = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu", 
          SerialCommand.GetPRxdCommand(), DelayPulse);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

bool AbortMotion()
{
  StateAutomation = saIdle;
  LedMotionY.Off();
  LedMotionX.Off();
  LedBusy.Off();
  return true;
}

void ExecuteAbortMotion(CSerial &serial)
{ // Analyse parameters -
  // Execute:
  AbortMotion();
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# ABM");
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

Boolean StartMotion()
{
  if (0 < CountRepetition)
  {
    LedMotionY.On();
    LedMotionX.On();
    LedBusy.On();
    XPositionActual = XPositionMinimum;
    YPositionActual = YPositionMinimum;
    StateAutomation = saMotionRowColLeft;
    return true;
  }
  return false;
}

void ExecuteStartMotion(CSerial &serial)
{ // Analyse parameters:
  DelayPulse = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  CountRepetition = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  // Execute:
  StartMotion();
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu", 
          SerialCommand.GetPRxdCommand(), DelayPulse, CountRepetition);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}

Boolean StartTrigger()
{
  if (0 < CountRepetition)
  {
    LedMotionY.On();
    LedMotionX.On();
    LedBusy.On();
    XPositionActual = XPositionMinimum;
    YPositionActual = YPositionMinimum;
    StateAutomation = saTriggerRowColLeft;
    return true;
  }
  return false;
}

void ExecuteStartTrigger(CSerial &serial)
{ // Analyse parameters:
  CountLaserPulses = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  CountRepetition = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  // Execute:
  StartTrigger();
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu", 
          SerialCommand.GetPRxdCommand(), CountLaserPulses, CountRepetition);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//#########################################################
//  Segment - Handler - Error
//#########################################################
//
void HandleError(CSerial &serial)
{
  if (ecNone != GlobalErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(GlobalErrorCode, SerialCommand.GetTxdBuffer());	
    serial.WriteText(SerialCommand.GetTxdBuffer());
    serial.WriteText("]: ");
    switch (GlobalErrorCode)
    {
      case ecNone:
        serial.WriteText("No Error");
        break;
      case ecUnknown:
        serial.WriteText("Unknown");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
      case ecToManyParameters:
        serial.WriteText("To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.WriteText("Missing Target Parameter");
        break;              
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    GlobalErrorCode = ecNone;
  }
}
//
//#########################################################
//  Segment - Automation
//#########################################################
//
void HandleAutomationIdle(CSerial &serial)
{  
  // nothing to do
}

void HandleAutomationMotionRowColLeft(CSerial &serial)
{
  LedMotionX.On();
  LedMotionY.On();
  //
  SetPosition();
  SetTrigger();
  //
  XPositionActual += XPositionDelta;
  if (XPositionMaximum < XPositionActual)
  {
    XPositionActual = XPositionMinimum;
    YPositionActual += YPositionDelta;
    if (YPositionMaximum < YPositionActual)
    { // Finish Area scanning
      XPositionActual = XPositionMinimum;
      YPositionActual = YPositionMinimum;
      CountRepetition--;
      LedBusy.Toggle();
      if (0 == CountRepetition)
      { // ends...
        // SetPosition();
        ExecuteAbortMotion(serial);
        return;
      }
    } 
  } 
}

void HandleAutomationTriggerRowColLeft(CSerial &serial)
{
  LedMotionX.On();
  LedMotionY.On();
  //
  SetPosition();
  TriggerLaserPulses();
  //
  XPositionActual += XPositionDelta;
  if (XPositionMaximum < XPositionActual)
  {
    XPositionActual = XPositionMinimum;
    YPositionActual += YPositionDelta;
    if (YPositionMaximum < YPositionActual)
    { // Finish Area scanning
      XPositionActual = XPositionMinimum;
      YPositionActual = YPositionMinimum;
      CountRepetition--;
      LedBusy.Toggle();
      if (0 == CountRepetition)
      { // ends...
        // SetPosition();
        ExecuteAbortMotion(serial);
        return;
      }
    } 
  } 
}

void HandleAutomation(CSerial &serial)
{
  switch (StateAutomation)
  {
    case saIdle:
      HandleAutomationIdle(serial);
      break;
    case saMotionRowColLeft:
      HandleAutomationMotionRowColLeft(serial);
      break;
    case saMotionRowColBoth:
      break;
    case saTriggerRowColLeft:
      HandleAutomationTriggerRowColLeft(serial);
      break;
    case saTriggerRowColBoth:
      break;
  }
}







Boolean ExecuteRxdCommand(CSerial &serial)
{ 
//  sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
//  serialtarget.WriteLine(TxdBuffer);
  if (!strcmp("H", SerialCommand.GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp("GPH", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp("GSV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp("GHV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  // ----------------------------------
  // Led
  // ----------------------------------
  if (!strcmp("GLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetLed(serial);
    return true;
  } else 
  if (!strcmp("SLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetLed(serial);
    return true;
  } else   
  if (!strcmp("CLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteClearLed(serial);
    return true;
  } else   
  if (!strcmp("TLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteToggleLed(serial);
    return true;
  } else   
//  // ----------------------------------
//  // Adc
//  // ----------------------------------
//  if (!strcmp("GAV", SerialCommand.GetPRxdCommand()))
//  {
//    ExecuteGetAdcValue(serial);
//    return true;
//  } else   
  // ----------------------------------
  // Dac
  // ----------------------------------
  if (!strcmp("GDV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetDacValue(serial);
    return true;
  } else   
  if (!strcmp("SDV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDacValue(serial);
    return true;
  } else   
//  // ----------------------------------
//  // Pwm
//  // ----------------------------------
//  if (!strcmp("GPV", SerialCommand.GetPRxdCommand()))
//  {
//    ExecuteGetPwmValue(serial);
//    return true;
//  } else   
//  if (!strcmp("SPV", SerialCommand.GetPRxdCommand()))
//  {
//    ExecuteSetPwmValue(serial);
//    return true;
//  } else   
  // ----------------------------------
  // LaserPulse
  // ----------------------------------
  if (!strcmp("TLP", SerialCommand.GetPRxdCommand()))
  {
//!!!!!!!!!!!!!!!!!!!    ExecuteTriggerLaserPulse(serial);
    return true;
  } else   
  // ----------------------------------
  // Automation
  // ----------------------------------
  if (!strcmp("SMX", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetMotionX(serial);
    return true;
  } else   
  if (!strcmp("SMY", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetMotionY(serial);
    return true;
  } else   
  if (!strcmp("SDM", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDelayMotion(serial);
    return true;
  } else   
  if (!strcmp("SDP", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDelayPulse(serial);
    return true;
  } else   
//  if (!strcmp("STM", SerialCommand.GetPRxdCommand()))
//  {
//    ExecuteStartMotion(serial);
//    return true;
//  } else   
  if (!strcmp("STT", SerialCommand.GetPRxdCommand()))
  {
    ExecuteStartTrigger(serial);
    return true;
  } else   
  if (!strcmp("ABM", SerialCommand.GetPRxdCommand()))
  {
    ExecuteAbortMotion(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    GlobalErrorCode = ecInvalidCommand;
  }
  return false;  
}

void HandleSerialCommands(CSerial &serial)
{
  if (SerialCommand.AnalyseRxdCommand(serial))
  {
    ExecuteRxdCommand(serial);
  }  
}



