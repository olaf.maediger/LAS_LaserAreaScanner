//
//--------------------------------
//  Library Adc
//--------------------------------
//
#ifndef Adc_h
#define Adc_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Adc
//--------------------------------
//
class CAdc
{
  private:
  bool FChannel[12];
  
  public:
  CAdc(bool channel0, bool channel1, bool channel2, bool channel3);
  CAdc(bool channel0, bool channel1, bool channel2, bool channel3,
       bool channel4, bool channel5, bool channel6, bool channel7);
  CAdc(bool channel0, bool channel1, bool channel2, bool channel3,
       bool channel4, bool channel5, bool channel6, bool channel7,
       bool channel8, bool channel9, bool channel10, bool channel11);
  Boolean Open();
  Boolean Close();
  UInt16 GetValue(UInt8 channel);
};
//
#endif
