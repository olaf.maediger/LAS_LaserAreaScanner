#ifndef Process_h
#define Process_h
//
#include "Defines.h"
//
class CProcess
{
  private:
  UInt32 FCountRepetition; // [1]
  UInt32 FCountLaserPulses; // [1]
  UInt32 FDelayMotion; // [us]
  UInt32 FDelayPulse; // [us]
  //
  UInt16 FXPositionActual; // [stp]
  UInt16 FXPositionDelta; // [stp]
  UInt16 FXPositionMinimum; // [stp]
  UInt16 FXPositionMaximum; // [stp]
  //
  UInt16 FYPositionActual; // [stp]
  UInt16 FYPositionDelta; // [stp]
  UInt16 FYPositionMinimum; // [stp]
  UInt16 FYPositionMaximum; // [stp]
  //
  public:
  CProcess();
  //
  //  Count
  //
  inline UInt32 GetCountRepetition()
  {
    return FCountRepetition;
  }
  inline void SetCountRepetition(UInt32 value)
  {
    FCountRepetition = value;
  }
  
  inline UInt32 GetCountLaserPulses()
  {
    return FCountLaserPulses;
  }
  inline void SetCountLaserPulses(UInt32 value)
  {
    FCountLaserPulses = value;
  }
  //
  //  Delay
  //
  inline UInt32 GetDelayMotion()
  {
    return FDelayMotion;
  }
  inline void SetDelayMotion(UInt32 value)
  {
    FDelayMotion = value;
  }
  
  inline UInt32 GetDelayPulse()
  {
    return FDelayPulse;
  }
  inline void SetDelayPulse(UInt32 value)
  {
    FDelayPulse = value;
  }
  //
  //  X-Position
  //
  inline UInt32 GetXPositionActual()
  {
    return FXPositionActual;
  }
  inline void SetXPositionActual(UInt32 value)
  {
    FXPositionActual = value;
  }
    
  inline UInt32 GetXPositionDelta()
  {
    return FXPositionDelta;
  }
  inline void SetXPositionDelta(UInt32 value)
  {
    FXPositionDelta = value;
  }
  
  inline UInt32 GetXPositionMinimum()
  {
    return FXPositionMinimum;
  }
  inline void SetXPositionMinimum(UInt32 value)
  {
    FXPositionMinimum = value;
  }
  
  inline UInt32 GetXPositionMaximum()
  {
    return FXPositionMaximum;
  }
  inline void SetXPositionMaximum(UInt32 value)
  {
    FXPositionMaximum = value;
  }
  //
  //  Y-Position
  //
  inline UInt32 GetYPositionActual()
  {
    return FYPositionActual;
  }
  inline void SetYPositionActual(UInt32 value)
  {
    FYPositionActual = value;
  }
    
  inline UInt32 GetYPositionDelta()
  {
    return FYPositionDelta;
  }
  inline void SetYPositionDelta(UInt32 value)
  {
    FYPositionDelta = value;
  }
  
  inline UInt32 GetYPositionMinimum()
  {
    return FYPositionMinimum;
  }
  inline void SetYPositionMinimum(UInt32 value)
  {
    FYPositionMinimum = value;
  }
  
  inline UInt32 GetYPositionMaximum()
  {
    return FYPositionMaximum;
  }
  inline void SetYPositionMaximum(UInt32 value)
  {
    FYPositionMaximum = value;
  }
};
//
#endif // Process_h
