#include "Automation.h"
//
extern CLed LedLaser;
//
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  FTimeStart = millis();
  FTimeMarker = FTimeStart;
  FCountEvent = 0;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
void CAutomation::SetState(EStateAutomation state)
{
  if (state != FState)
  {
    FState = state;
  }
}

void CAutomation::SetTimeMarker()
{
  FTimeMarker = millis();
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CAutomation::Open()
{
  FState = saIdle;
  return true;
}

Boolean CAutomation::Close()
{
  FState = saUndefined;
  return true;
}
//
//----------------------------------------------------------
//  Segment - Automation - Idle
//----------------------------------------------------------
//
void CAutomation::HandleIdle(CSerial &serial)
{
  // do nothing
}
//
//----------------------------------------------------------
//  Segment - Automation - LedLaser
//----------------------------------------------------------
//
void CAutomation::HandleLedLaserPulseBegin(CSerial &serial)
{
  FTimeMarker = millis();  
  LedLaser.On();
  FCountEvent = 1;
  SetState(saLedLaserPulseBusy);
}
void CAutomation::HandleLedLaserPulseBusy(CSerial &serial)
{  
  UInt32 PeriodHalf = LedLaser.GetPeriod() / 2;  
  if (PeriodHalf <= FTimeStamp - FTimeMarker)
  {
    FTimeMarker = FTimeStamp;       
    if (slOff == LedLaser.GetState())
    {
      LedLaser.On();
    }
    else
      if (slOn == LedLaser.GetState())
      {
        LedLaser.Off();
        if (FCountEvent < LedLaser.GetCount())
        {
          FCountEvent++;      
        }
        else
          {
            SetState(saLedLaserPulseEnd);
          }
      }
  }
}
void CAutomation::HandleLedLaserPulseEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - PositionLaser
//----------------------------------------------------------
//
void CAutomation::HandlePulsePositionLaserBegin(CSerial &serial)
{
  FTimeMarker = millis(); 
  LedLaser.On();
  FCountEvent = 1;
  SetState(saPulsePositionLaserBusy);
}
void CAutomation::HandlePulsePositionLaserBusy(CSerial &serial)
{  
  UInt32 PeriodHalf = LedLaser.GetPeriod() / 2;  
  if (PeriodHalf <= FTimeStamp - FTimeMarker)
  {
    FTimeMarker = FTimeStamp;       
    if (slOff == LedLaser.GetState())
    {
      LedLaser.On();
    }
    else
      if (slOn == LedLaser.GetState())
      {
        LedLaser.Off();
        if (FCountEvent < LedLaser.GetCount())
        {
          FCountEvent++;      
        }
        else
          {
            SetState(saPulsePositionLaserEnd);
          }
      }
  }
}
void CAutomation::HandlePulsePositionLaserEnd(CSerial &serial)
{  
  SetState(saIdle);  
}

//
//----------------------------------------------------------
//  Segment - Automation - Collector
//----------------------------------------------------------
//
void CAutomation::Handle(CSerial &serial)
{
  FTimeStamp = millis();
  switch (FState)
  { // Idle
    case saIdle:
      HandleIdle(serial);
      break;
    // LedLaser
    case saLedLaserPulseBegin:
      HandleLedLaserPulseBegin(serial);
      break;
    case saLedLaserPulseBusy:
      HandleLedLaserPulseBusy(serial);
      break;
    case saLedLaserPulseEnd:
      HandleLedLaserPulseEnd(serial);
      break;
    // PositionLaser
    case saPulsePositionLaserBegin:
      HandlePulsePositionLaserBegin(serial);
      break;
    case saPulsePositionLaserBusy:
      HandlePulsePositionLaserBusy(serial);
      break;
    case saPulsePositionLaserEnd:
      HandlePulsePositionLaserEnd(serial);
      break;
  }  
}

