#include "Process.h"
//
CProcess::CProcess()
{
  FCountRepetition = INIT_COUNTREPETITION;
  FCountLaserPulses = INIT_COUNTLASERPULSES;
  FDelayMotion = INIT_DELAYMOTION;
  FDelayPulse = INIT_DELAYPULSE;
  //
  FXPositionActual = INIT_XPOSITIONACTUAL;
  FXPositionDelta = INIT_XPOSITIONDELTA;
  FXPositionMinimum = INIT_XPOSITIONMINIMUM;
  FXPositionMaximum = INIT_XPOSITIONMAXIMUM;
  //
  FYPositionActual = INIT_YPOSITIONACTUAL;
  FYPositionDelta = INIT_YPOSITIONDELTA;
  FYPositionMinimum = INIT_YPOSITIONMINIMUM;
  FYPositionMaximum = INIT_YPOSITIONMAXIMUM;    
}

