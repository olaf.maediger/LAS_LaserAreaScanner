#ifndef Process_h
#define Process_h
//
#include "Defines.h"
//
class CProcess
{
  private:
  UInt32 FLaserPulsePeriod; // [us]
  UInt32 FTotalPulseCountPreset; // [1]     // Total Number of LaserPulses Preset
  UInt32 FTotalPulseCountActual; // [1]     // Total Number of LaserPulses Actual
  UInt32 FPositionPulseCountPreset; // [1]  // Number of LaserPulses at one Position Preset
  UInt32 FPositionPulseCountActual; // [1]  // Number of LaserPulses at one Position Actual
  UInt32 FRangeDelayMotion; // [us]
  UInt32 FRangeDelayPulse; // [us]
  //
  UInt16 FXPositionActual; // [stp]
  UInt16 FXPositionDelta; // [stp]
  UInt16 FXPositionMinimum; // [stp]
  UInt16 FXPositionMaximum; // [stp]
  //
  UInt16 FYPositionActual; // [stp]
  UInt16 FYPositionDelta; // [stp]
  UInt16 FYPositionMinimum; // [stp]
  UInt16 FYPositionMaximum; // [stp]
  //
  public:
  CProcess();
  //
  //  Laser
  //
  inline UInt32 GetLaserPulsePeriod()
  {
    return FLaserPulsePeriod;
  }
  inline void SetLaserPulsePeriod(UInt32 value)
  {
    FLaserPulsePeriod = value;
  }

  inline UInt32 GetTotalPulseCountPreset()
  {
    return FTotalPulseCountPreset;
  }
  inline void SetTotalPulseCountPreset(UInt32 value)
  {
    FTotalPulseCountPreset = value;
  }
  inline UInt32 GetTotalPulseCountActual()
  {
    return FTotalPulseCountActual;
  }
  inline void SetTotalPulseCountActual(UInt32 value)
  {
    FTotalPulseCountActual = value;
  }
  inline void IncrementTotalPulseCountActual()
  {
    FTotalPulseCountActual++;
  }
  inline Boolean IsTotalPulseCountReached()
  {
    return (FTotalPulseCountPreset <= FTotalPulseCountActual);
  }

  inline UInt32 GetPositionPulseCountPreset()
  {
    return FPositionPulseCountPreset;
  }
  inline void SetPositionPulseCountPreset(UInt32 value)
  {
    FPositionPulseCountPreset = value;
  }
  inline UInt32 GetPositionPulseCountActual()
  {
    return FPositionPulseCountActual;
  }
  inline void SetPositionPulseCountActual(UInt32 value)
  {
    FPositionPulseCountActual = value;
  }
  inline void IncrementPositionPulseCountActual()
  {
    FPositionPulseCountActual++;
  }
  inline Boolean IsPositionPulseCountReached()
  {
    return (FPositionPulseCountPreset <= FPositionPulseCountActual);
  }
  //
  //  Range - Delay
  //
  inline UInt32 GetRangeDelayMotion()
  {
    return FRangeDelayMotion;
  }
  inline void SetRangeDelayMotion(UInt32 value)
  {
    FRangeDelayMotion = value;
  }
  
  inline UInt32 GetRangeDelayPulse()
  {
    return FRangeDelayPulse;
  }
  inline void SetRangeDelayPulse(UInt32 value)
  {
    FRangeDelayPulse = value;
  }
  //
  //  X-Position
  //
  inline UInt32 GetXPositionActual()
  {
    return FXPositionActual;
  }
  inline void SetXPositionActual(UInt32 value)
  {
    FXPositionActual = value;
  }
    
  inline UInt32 GetXPositionDelta()
  {
    return FXPositionDelta;
  }
  inline void SetXPositionDelta(UInt32 value)
  {
    FXPositionDelta = value;
  }
  
  inline UInt32 GetXPositionMinimum()
  {
    return FXPositionMinimum;
  }
  inline void SetXPositionMinimum(UInt32 value)
  {
    FXPositionMinimum = value;
  }
  
  inline UInt32 GetXPositionMaximum()
  {
    return FXPositionMaximum;
  }
  inline void SetXPositionMaximum(UInt32 value)
  {
    FXPositionMaximum = value;
  }
  //
  //  Y-Position
  //
  inline UInt32 GetYPositionActual()
  {
    return FYPositionActual;
  }
  inline void SetYPositionActual(UInt32 value)
  {
    FYPositionActual = value;
  }
    
  inline UInt32 GetYPositionDelta()
  {
    return FYPositionDelta;
  }
  inline void SetYPositionDelta(UInt32 value)
  {
    FYPositionDelta = value;
  }
  
  inline UInt32 GetYPositionMinimum()
  {
    return FYPositionMinimum;
  }
  inline void SetYPositionMinimum(UInt32 value)
  {
    FYPositionMinimum = value;
  }
  
  inline UInt32 GetYPositionMaximum()
  {
    return FYPositionMaximum;
  }
  inline void SetYPositionMaximum(UInt32 value)
  {
    FYPositionMaximum = value;
  }

  inline UInt32 IncrementXPosition()
  {
    FXPositionActual += FXPositionDelta;
    return FXPositionActual;
  }
  inline UInt32 DecrementXPosition()
  {
    FXPositionActual -= FXPositionDelta;
    return FXPositionActual;
  }
  
  inline UInt32 IncrementYPosition()
  {
    FYPositionActual += FYPositionDelta;
    return FYPositionActual;
  }
  inline UInt32 DecrementYPosition()
  {
    FYPositionActual -= FYPositionDelta;
    return FYPositionActual;
  }
};
//
#endif // Process_h
