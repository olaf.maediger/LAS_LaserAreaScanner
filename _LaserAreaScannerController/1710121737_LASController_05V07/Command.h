#ifndef Command_h
#define Command_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
#include "Automation.h"
//
// Command-Parameter
#define SIZE_RXDPARAMETER   8
#define COUNT_RXDPARAMETERS 4
//
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
#define HELP_SOFTWAREVERSION  " Software-Version LAS05V06"
//
const int COUNT_ANSWERLINE = 1;
//
const int COUNT_HEADERLINES = 10;
//
#define TITEL_LINE            "------------------------------------------"
#define TITEL_PROJECT         "- Project:   LaserAreaScanner-Controller -"
#define TITEL_VERSION         "- Version:   05V07                       -"
#ifdef PROCESSOR_ARDUINOUNO
#define TITEL_HARDWARE        "- Hardware:  ArduinoUnoR3                -"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoUnoR3"
#endif
#ifdef PROCESSOR_ARDUINOMEGA 
#define TITEL_HARDWARE        "- Hardware:  ArduinoMega328              -"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoMega328"
#endif
#ifdef PROCESSOR_ARDUINODUE
#define TITEL_HARDWARE        "- Hardware:  ArduinoDue3                 -"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoDue3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define TITEL_HARDWARE        "- Hardware:  Stm32F103C8                 -"
#define HELP_HARDWAREVERSION  " Hardware-Version Stm32F103C8"
#endif
#ifdef PROCESSOR_TEENSY36 
#define TITEL_HARDWARE        "- Hardware:  Teensy36                    -"
#define HELP_HARDWAREVERSION  " Hardware-Version Teensy36"
#endif
#define TITEL_DATE            "- Date:      171011                      -"
#define TITEL_TIME            "- Time:      1620                        -"
#define TITEL_AUTHOR          "- Author:    OM                          -"
#define TITEL_PORT            "- Port:      SerialCommand (Serial1-USB) -"
#define TITEL_PARAMETER       "- Parameter: 115200, N, 8, 1             -"
//
const int COUNT_HELPLINES = 29;
//
#define HELP_COMMON           " Help (Common):"
//
#define HELP_H                " H   : This Help"
#define HELP_GPH              " GPH <fdl> : Get Program Header"
#define HELP_GSV              " GSV <fdl> : Get Software-Version"
#define HELP_GHV              " GHV <fdl> : Get Hardware-Version"
//
#define HELP_LEDLASER         " Help (LedLaser):"
#define HELP_GLL              " GLL : Get State LedLaser"
#define HELP_LLH              " LLH : Switch LedLaser On"
#define HELP_LLL              " LLL : Switch LedLaser Off"
#define HELP_PLL              " PLL <p> <n> : Pulse LedLaser with <p>eriod{ms} <n>times"
//
#define HELP_POS              " Help (Position):"
#define HELP_GPX              " GPX : Get PositionX from DAConverter"
#define HELP_SPX              " SPX <p> : Set PositionX<0..4095> to DAConverter"
#define HELP_GPY              " GPY : Get PositionY from DAConverter"
#define HELP_SPY              " SPY <p> : Set PositionY<0..4095> to DAConverter"
//
#define HELP_POSITIONLASER    " Help (PositionLaser):"
#define HELP_PPL              " PPL <x> <y> <p> <c> : Laser at Position<x><y> with <p>eriod <c>ount Pulses"
//
#define HELP_MACHININGPARAMETER  " Help (MachiningParameter):"
#define HELP_SRX              " SRX <xmin> <xmax> <dx>: Set Range ParameterX[0..4095]"
#define HELP_SRY              " SRY <ymin> <ymax> <dy>: Set Range ParameterY[0..4095]"
#define HELP_SDM              " SDM <delay>: Set Delay Motion{us}"
#define HELP_SDP              " SDP <delay>: Set Delay Pulse{us}"
//
#define HELP_MATRIXLASER      " Help (MatrixLaser):"
#define HELP_PML              " PML <p> <n> : Pulse Matrix Laser <p>eriod <n>Pulses"
#define HELP_AML              " AML : Abort Matrix Laser"
//
#define HELP_VARIABLELASER    " Help (VariableLaser):"
#define HELP_EVL              " EVL : Enter Variable Laser"
#define HELP_PVL              " PVL <x> <y> <p> <c> : Laser at Position<x><y> with <p>eriod <c>ount Pulses"
#define HELP_AVL              " AVL : Abort Variable Laser"
//
class CCommand
{
  private:
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Character FRxdCommandLine[SIZE_RXDBUFFER];
  PCharacter FPRxdCommand;
  PCharacter FPRxdParameters[COUNT_RXDPARAMETERS];
  Int16 FRxdParameterCount = 0;
  Int16 FRxdBufferIndex = 0;

  public:
  CCommand();
  ~CCommand();
  //
  PCharacter GetPRxdParameters(UInt8 index)
  {
    return FPRxdParameters[index];
  }
  PCharacter GetTxdBuffer()
  {
    return FTxdBuffer;
  }
  PCharacter GetPRxdCommand()
  {
    return FPRxdCommand;
  }
  //
  void ZeroRxdBuffer();
  void ZeroTxdBuffer();
  void ZeroRxdCommandLine();
  //
  //  Segment - Execution - Basic Output
  void WriteProgramHeader(CSerial &serial);
  void WriteSoftwareVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
  void WriteHelp(CSerial &serial);
  //
  //  Segment - Execution - Help
  void ExecuteGetHelp(CSerial &serial);
  void ExecuteGetProgramHeader(CSerial &serial);
  void ExecuteGetSoftwareVersion(CSerial &serial);
  void ExecuteGetHardwareVersion(CSerial &serial);
  //
  //  Segment - Execution - LedLaser
  void ExecuteGetLedLaser(CSerial &serial);
  void ExecuteSwitchLedLaserOn(CSerial &serial);
  void ExecuteSwitchLedLaserOff(CSerial &serial);
  void ExecutePulseLedLaser(CSerial &serial);
  //
  //  Segment - Execution - Position
  void ExecuteGetPositionX(CSerial &serial);
  void ExecuteSetPositionX(CSerial &serial);
  void ExecuteGetPositionY(CSerial &serial);
  void ExecuteSetPositionY(CSerial &serial);
  //
  //  Segment - Execution - PositionLaser
  void ExecutePulsePositionLaser(CSerial &serial);
  //
  //  Segment - Execution - MachiningParameter
  void ExecuteSetMachiningRangeX(CSerial &serial);      // SRX
  void ExecuteSetMachiningRangeY(CSerial &serial);      // SRY
  void ExecuteSetMachiningDelayMotion(CSerial &serial); // SDM
  void ExecuteSetMachiningDelayPulse(CSerial &serial);  // SDP
  //
  //  Segment - Execution - MatrixLaser
  void ExecutePulseMatrixLaser(CSerial &serial);
  void ExecuteAbortMatrixLaser(CSerial &serial);
  //
  //  Segment - Execution - VariableLaser
  void ExecuteEnterVariableLaser(CSerial &serial);
  void ExecutePulseVariableLaser(CSerial &serial);
  void ExecuteAbortVariableLaser(CSerial &serial);
  //  
  void Init();
  Boolean DetectRxdLine(CSerial &serial);
  Boolean Analyse(CSerial &serial);
  void CallbackStateAutomation(CSerial &serial, EStateAutomation stateautomation);
  Boolean Execute(CSerial &serial);
  Boolean Handle(CSerial &serial);
};
//
#endif // Command_h
