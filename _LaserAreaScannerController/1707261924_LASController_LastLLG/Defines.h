//
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define UInt8 byte
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Float float
#define Double double
//
#define true 1
#define false 0
//
//----------------------------------
// ARDUINOMEGA :
// #define PROCESSOR_ARDUINOMEGA
//
//----------------------------------
// ARDUINODUE :
// #define PROCESSOR_ARDUINODUE
//
//----------------------------------
// STM32F103C8 : no DACs!
#define PROCESSOR_STM32F103C8
//
//----------------------------------
// TEENSY36 :
// #define PROCESSOR_TEENSY36
//
//-------------------------------------------
//	Global Types - Error
//-------------------------------------------
enum EErrorCode
{
  ecNone = (int)0,	
  ecUnknown = (int)1,
  ecInvalidCommand = (int)2,
  ecToManyParameters = (int)3,
  ecMissingTargetParameter = (int)4
};
//
#define SIZE_TXDBUFFER 64
#define SIZE_RXDBUFFER 64
#define SIZE_RXDPARAMETER 8
#define COUNT_RXDPARAMETERS 3
// Single Led System (fixed)
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
const int PIN_LEDSYSTEM   = 13;
#endif
#if defined(PROCESSOR_STM32F103C8)
const int PIN_LEDSYSTEM   = PC13;
#endif
//
// LedLine
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
const int PIN_LEDERROR    = 54;
//                          55
const int PIN_LEDBUSY =     56;
//                          57
const int PIN_LEDMOTIONX  = 58;
const int PIN_LEDMOTIONY  = 59;
//                          60
const int PIN_LEDTRIGGER  = 61;
#endif
#if defined(PROCESSOR_STM32F103C8)
const int PIN_LEDERROR    = PC14;
//                          
const int PIN_LEDBUSY     = PC15;
//                          
const int PIN_LEDMOTIONX  = PB10;
const int PIN_LEDMOTIONY  = PB11;
//                          
const int PIN_LEDTRIGGER  = PB1;
#endif
//
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
//#define PIN_ADC0  54
//#define PIN_ADC1  55
//#define PIN_ADC2  56
//#define PIN_ADC3  57
//#define PIN_ADC4  58
//#define PIN_ADC5  59
//#define PIN_ADC6  60
//#define PIN_ADC7  61
const int PIN_ADC8  
const int PIN_ADC9  
const int PIN_ADC10 
const int PIN_ADC11 
//
#define PIN_DAC0  66
#define PIN_DAC1  67
//
#define PIN_PWM2  2
#define PIN_PWM3  3
#define PIN_PWM4  4 
#define PIN_PWM5  5
#define PIN_PWM6  6 
#define PIN_PWM7  7
#define PIN_PWM8  8
#define PIN_PWM9  9
#define PIN_PWM10 10 
#define PIN_PWM11 11
#define PIN_PWM12 12
// System Led 
//#define PIN_PWM13 13
#endif
//
#endif
#if defined(PROCESSOR_STM32F103C8)
#define PIN_ADC8  PA0
#define PIN_ADC9  PA1
#define PIN_ADC10 PA2
#define PIN_ADC11 PA3
//
#define PIN_PWM1  PA8
#define PIN_PWM2  PA9
#define PIN_PWM3  PA10
#define PIN_PWM4  PA11
#define PIN_PWM5  PB13
#define PIN_PWM6  PB14
#define PIN_PWM7  PB15
#define PIN_PWM8  PA15
#define PIN_PWM9  PB3
#define PIN_PWM10 PB10
#define PIN_PWM11 PB11
#define PIN_PWM12 PA6
#define PIN_PWM13 PA7
//
#endif

