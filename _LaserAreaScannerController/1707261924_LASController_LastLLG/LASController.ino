#include "Defines.h"
#include "Helper.h"
#include "LedLine.h"
#include "Serial.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
#include "SerialCommand.h"
#include "Utilities.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables - Common
//---------------------------------------------------
//
enum EErrorCode GlobalErrorCode;
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
EStateAutomation StateAutomation;
UInt32 CountRepetition; // [1]
UInt32 CountLaserPulses; // [1]
UInt32 DelayMotion; // [us]
UInt32 DelayPulse; // [us]
//
UInt16 XPositionActual; // [stp]
UInt16 XPositionDelta; // [stp]
UInt16 XPositionMinimum; // [stp]
UInt16 XPositionMaximum; // [stp]
//
UInt16 YPositionActual; // [stp]
UInt16 YPositionDelta; // [stp]
UInt16 YPositionMinimum; // [stp]
UInt16 YPositionMaximum; // [stp]
//
CSerialCommand SerialCommand;
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM);
CLedLine LedLine(PIN_LEDTRIGGER, 60, PIN_LEDMOTIONY, PIN_LEDMOTIONX, // 7..4
                 57, PIN_LEDBUSY, 55, PIN_LEDERROR);                 // 3..0
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
// ... CSerial SerialDebug(Serial, RXDECHO_ON);
CSerial SerialLan(Serial1, RXDECHO_OFF);
//
void setup() 
{
  LedSystem.Open(); 
  // ... SerialDebug.Open(115200);
  SerialLan.Open(115200);
  // ... WriteProgramHeader(SerialLan);
  // ... WriteHelp(SerialLan);
  SerialLan.WritePrompt();  
}

void loop() 
{
  HandleError(SerialLan);  
  HandleSerialCommands(SerialLan);
  HandleAutomation(SerialLan);  
  LedSystem.On();
  delay(1);
  LedSystem.Off();
  delay(1);
}


