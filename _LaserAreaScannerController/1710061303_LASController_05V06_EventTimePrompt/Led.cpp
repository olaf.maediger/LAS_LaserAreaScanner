//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "Led.h"
//
CLed::CLed(int pin)
{
  FPin = pin;
  FState = slUndefined;
  FPulsePeriod = 1000;
  FPulseCountPreset = 0;
  FPulseCountActual = 0;
}

EStateLed CLed::GetState()
{
  return FState;
}

UInt32 CLed::GetPulseCountPreset()
{
  return FPulseCountPreset;  
}

UInt32 CLed::GetPulseCountActual()
{
  return FPulseCountActual;  
}

UInt32 CLed::GetPulsePeriod()
{
  return FPulsePeriod;
}

Boolean CLed::Open()
{
  pinMode(FPin, OUTPUT);
  digitalWrite(FPin, LOW);
  FState = slOff;
  return true;
}

Boolean CLed::Close()
{
  pinMode(FPin, INPUT);
  FState = slUndefined;
  return true;
}

void CLed::On()
{
  digitalWrite(FPin, HIGH);
  FState = slOn;
  FPulseCountActual++;
}

void CLed::Off()
{
  digitalWrite(FPin, LOW);
  FState = slOff;
}

void CLed::SetPulsePeriodCount(UInt32 pulseperiod, UInt32 pulsecountpreset)
{
  FPulsePeriod = pulseperiod;
  FPulseCountPreset = pulsecountpreset;
  FPulseCountActual = 0;
}


