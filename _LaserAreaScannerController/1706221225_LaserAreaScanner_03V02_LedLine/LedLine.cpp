//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "LedLine.h"
//
CLedLine::CLedLine(int pin7, int pin6, int pin5, int pin4, 
                   int pin3, int pin2, int pin1, int pin0)
{
  FPins[0] = pin0;
  FPins[1] = pin1;
  FPins[2] = pin2;
  FPins[3] = pin3;
  FPins[4] = pin4;
  FPins[5] = pin5;
  FPins[6] = pin6;
  FPins[7] = pin7;
  for (int LI = 0; LI < 8; LI++)
  {
    FStates[LI] = slUndefined;
  }  
}



EStateLed CLedLine::GetState(int ledindex)
{
  return FStates[ledindex];
}



Boolean CLedLine::Open()
{
  for (int LI = 0; LI < 8; LI++)
  {
    if (PIN_NC != FPins[LI])
    {
      pinMode(FPins[LI], OUTPUT);
      digitalWrite(FPins[LI], LOW);
      FStates[LI] = slOff;
    }
  }
  return true;
}

Boolean CLedLine::Close()
{
  for (int LI = 0; LI < 8; LI++)
  {
    if (PIN_NC != FPins[LI])
    {
      digitalWrite(FPins[LI], LOW);
      pinMode(FPins[LI], INPUT);
      FStates[LI] = slUndefined;
    }
  }
  return true;
}



void CLedLine::On()
{
  for (int LI = 0; LI < 8; LI++)
  {
//    if (PIN_NC != FPins[LI])
    {
      digitalWrite(FPins[LI], HIGH);
      FStates[LI] = slOn;
    }
  }   
}

void CLedLine::On(byte mask)
{
  byte Mask = 0x01;
  for (int IP = 0; IP < 8; IP++)
  {
    if (0 < (mask & Mask))
    {
      //if (PIN_NC != FPins[LI])
      {
        digitalWrite(FPins[IP], HIGH);
        FStates[IP] = slOn;
      }
    }
    Mask <<= 1;
  }   
}

void CLedLine::Off()
{
  for (int LI = 0; LI < 8; LI++)
  {
//    if (PIN_NC != FPins[LI])
    {
      digitalWrite(FPins[LI], LOW);
      FStates[LI] = slOff;
    }
  }   
}

void CLedLine::Off(byte mask)
{
  byte Mask = 0x01;
  for (int LI = 0; LI < 8; LI++)
  {
    if (0 < (mask & Mask))
    {
      if (PIN_NC != FPins[LI])
      {
        digitalWrite(FPins[LI], LOW);
        FStates[LI] = slOff;
      }
    }
    Mask <<= 1;    
  }   
}


void CLedLine::Toggle()
{
  for (int LI = 0; LI < 8; LI++)
  {
    if (slOff == FStates[LI])
    {
      if (PIN_NC != FPins[LI])
      {
        digitalWrite(FPins[LI], HIGH);
        FStates[LI] = slOn;
      }
    } 
    if (slOn == FStates[LI])
    {
      if (PIN_NC != FPins[LI])
      {
        digitalWrite(FPins[LI], LOW);
        FStates[LI] = slOff;
      }
    } 
  }   
}

void CLedLine::Toggle(byte mask)
{
  byte Mask = 0x01;
  for (int LI = 0; LI < 8; LI++)
  {
    if (mask & Mask)
    {
      if (slOff == FStates[LI])
      {
        if (PIN_NC != FPins[LI])
        {
          digitalWrite(FPins[LI], HIGH);
          FStates[LI] = slOn;
        }
      } 
      if (slOn == FStates[LI])
      {
        if (PIN_NC != FPins[LI])
        {
          digitalWrite(FPins[LI], LOW);
          FStates[LI] = slOff;
        }
      }
    }
    Mask <<= 1;    
  }   
}

void CLedLine::Pulse(UInt32 microseconds)
{
  for (int LI = 0; LI < 8; LI++)
  {
    if (PIN_NC != FPins[LI])
    {
      digitalWrite(FPins[LI], HIGH);
      FStates[LI] = slOn;
    }
  }   
  delayMicroseconds(microseconds);  
  for (int LI = 0; LI < 8; LI++)
  {
    if (PIN_NC != FPins[LI])
    {
      digitalWrite(FPins[LI], LOW);
      FStates[LI] = slOff;
    }
  }   
}

void CLedLine::Pulse(byte mask, UInt32 microseconds)
{
  byte Mask = 0x01;
  for (int LI = 0; LI < 8; LI++)
  {
    if (Mask && mask)
    {
      if (PIN_NC != FPins[LI])
      {
        digitalWrite(FPins[LI], HIGH);
        FStates[LI] = slOn;
      }
    }
    Mask << 1;
  }   
  delayMicroseconds(microseconds);  
  for (int LI = 0; LI < 8; LI++)
  {
    if (Mask && mask)
    {
      if (PIN_NC != FPins[LI])
      {
        digitalWrite(FPins[LI], LOW);
        FStates[LI] = slOff;
      }
    }
    Mask << 1;
  }   
}



