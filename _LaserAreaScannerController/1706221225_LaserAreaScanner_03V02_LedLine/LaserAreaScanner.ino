#include "Defines.h"
#include "Helper.h"
#include "LedLine.h"
#include "Serial.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
#include "SerialCommand.h"
#include "Utilities.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables - Common
//---------------------------------------------------
//
enum EErrorCode GlobalErrorCode;
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
EStateAutomation StateAutomation;
UInt32 CountRepetition; // [1]
UInt32 CountLaserPulses; // [1]
UInt32 DelayMotion; // [us]
UInt32 DelayPulse; // [us]
//
UInt16 XPositionActual; // [stp]
UInt16 XPositionDelta; // [stp]
UInt16 XPositionMinimum; // [stp]
UInt16 XPositionMaximum; // [stp]
//
UInt16 YPositionActual; // [stp]
UInt16 YPositionDelta; // [stp]
UInt16 YPositionMinimum; // [stp]
UInt16 YPositionMaximum; // [stp]
//
CSerialCommand SerialCommand;
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
//CLed LedSystem(PIN_LEDSYSTEM);
//CLed LedError(PIN_LEDERROR);
//CLed LedBusy(PIN_LEDBUSY);
//CLed LedMotionX(PIN_LEDMOTIONX);
//CLed LedMotionY(PIN_LEDMOTIONY);
//CLed LedTrigger(PIN_LEDTRIGGER);
//CLedLine LedLine(PIN_LEDTRIGGER, PIN_NC, PIN_LEDMOTIONY, PIN_LEDMOTIONX, // 7..4
//                 PIN_NC, PIN_LEDBUSY, PIN_NC, PIN_LEDERROR);             // 3..0
CLedLine LedLine(PIN_LEDTRIGGER, 60, PIN_LEDMOTIONY, PIN_LEDMOTIONX, // 7..4
                 57, PIN_LEDBUSY, 55, PIN_LEDERROR);             // 3..0
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
// CSerial SerialProgram(Serial);
//CSerial SerialDebug(Serial);
CSerial SerialPC(Serial1, RXDECHO_ON);
//CSerial SerialYYY(Serial3);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Dac
//---------------------------------------------------
//
CDac DigitalAnalogConverter(true, true);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Adc
//---------------------------------------------------
//
CAdc AnalogDigitalConverter(true, true, true, true);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Pwm
//---------------------------------------------------
//
CPwm PulseWidthModulator(true, true, true, true);
//
void setup() 
{
//  LedSystem.Open();
//  LedSystem.Off();  
//  LedError.Open();
//  LedBusy.Open();
//  LedMotionX.Open();
//  LedMotionY.Open();
//  LedTrigger.Open();
  LedLine.Open();
  for (int BI = 0; BI < 10; BI++)
  {
    LedLine.On();
    delay(100);
    LedLine.Off();
    delay(100);
  }
  //
  SerialPC.Open(115200);
  SerialPC.SetRxdEcho(true);
  //
  DigitalAnalogConverter.Open();
  //
  AnalogDigitalConverter.Open();
  //
  PulseWidthModulator.Open();
  //
  WriteProgramHeader(SerialPC);
  WriteHelp(SerialPC);
  SerialPC.WritePrompt();
  //
  GlobalErrorCode = INIT_GLOBALERRORCODE;
  //
  StateAutomation = INIT_STATEAUTOMATION;
  CountRepetition = INIT_COUNTREPETITION;
  CountLaserPulses = INIT_COUNTLASERPULSES;
  DelayMotion = INIT_DELAYMOTION;
  DelayPulse = INIT_DELAYPULSE;
  //
  XPositionActual = INIT_XPOSITIONACTUAL;
  XPositionDelta = INIT_XPOSITIONDELTA;
  XPositionMinimum = INIT_XPOSITIONMINIMUM;
  XPositionMaximum = INIT_XPOSITIONMAXIMUM;
  //
  YPositionActual = INIT_YPOSITIONACTUAL;
  YPositionDelta = INIT_YPOSITIONDELTA;
  YPositionMinimum = INIT_YPOSITIONMINIMUM;
  YPositionMaximum = INIT_YPOSITIONMAXIMUM;
  for (int BI = 0; BI < 10; BI++)
  {
    LedLine.On();
    delay(20);
    LedLine.Off();
    delay(20);
  }
}

void loop() 
{
  HandleError(SerialPC);  
  HandleSerialCommands(SerialPC);
  HandleAutomation(SerialPC);
  // 
  LedLine.On(0x55);
  delay(100);
  LedLine.Off(0x55);
  delay(500);
  LedLine.On(0xAA);
  delay(100);
  LedLine.Off(0xAA);
  delay(500);
  
}
