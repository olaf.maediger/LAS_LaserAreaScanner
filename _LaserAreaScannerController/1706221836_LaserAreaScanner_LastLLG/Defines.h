//
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define UInt8 byte
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Float float
#define Double double
//
#define true 1
#define false 0
//
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// not used 
// #define PROCESSOR_ARDUINOMEGA
// 
#define PROCESSOR_ARDUINODUE
// #define PROCESSOR_STM32F103C8 : KEINE DACs!
// #define PROCESSOR_TEENSY36
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//-------------------------------------------
//	Global Types - Error
//-------------------------------------------
enum EErrorCode
{
  ecNone = (int)0,	
  ecUnknown = (int)1,
  ecInvalidCommand = (int)2,
  ecToManyParameters = (int)3,
  ecMissingTargetParameter = (int)4
};
//
#define SIZE_TXDBUFFER 64
#define SIZE_RXDBUFFER 64
#define SIZE_RXDPARAMETER 8
#define COUNT_RXDPARAMETERS 3
// Single Led System (fixed)
#define PIN_LEDSYSTEM   13
// LedLine
#define PIN_LEDERROR    54
//                      55
#define PIN_LEDBUSY     56
//                      57
#define PIN_LEDMOTIONX  58
#define PIN_LEDMOTIONY  59
//                      60
#define PIN_LEDTRIGGER  61
//
//#define PIN_ADC0  54
//#define PIN_ADC1  55
//#define PIN_ADC2  56
//#define PIN_ADC3  57
//#define PIN_ADC4  58
//#define PIN_ADC5  59
//#define PIN_ADC6  60
//#define PIN_ADC7  61
#define PIN_ADC8  62
#define PIN_ADC9  63
#define PIN_ADC10 64
#define PIN_ADC11 65
//
#define PIN_DAC0  66
#define PIN_DAC1  67
//
#define PIN_PWM2  2
#define PIN_PWM3  3
#define PIN_PWM4  4 
#define PIN_PWM5  5
#define PIN_PWM6  6 
#define PIN_PWM7  7
#define PIN_PWM8  8
#define PIN_PWM9  9
#define PIN_PWM10 10 
#define PIN_PWM11 11
#define PIN_PWM12 12
// System Led 
#define PIN_PWM13 13
//
//
#endif 

