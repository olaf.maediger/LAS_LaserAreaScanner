#ifndef Command_h
#define Command_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
#include "Process.h"
//
#define ARGUMENT_PROJECT    "LAS - LaserAreaScan-Controller"
#define ARGUMENT_SOFTWARE   "06V14"
#define ARGUMENT_DATE       "180523"
#define ARGUMENT_TIME       "1055"
#define ARGUMENT_AUTHOR     "OM"
#define ARGUMENT_PORT       "SerialCommand (Serial-USB)"
#define ARGUMENT_PARAMETER  "115200, N, 8, 1"
//
#ifdef PROCESSOR_ARDUINOUNO
#define ARGUMENT_HARDWARE "ArduinoUnoR3"
#endif
#ifdef PROCESSOR_ARDUINOMEGA 
#define ARGUMENT_HARDWARE "ArduinoMega328"
#endif
#ifdef PROCESSOR_ARDUINODUE
#define ARGUMENT_HARDWARE "ArduinoDue3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
// 
// HELP_COMMON
#define SHORT_H     "H"
#define SHORT_GPH   "GPH"
#define SHORT_GSV   "GSV"
#define SHORT_GHV   "GHV"
#define SHORT_GPC   "GPC"
#define SHORT_SPC   "SPC"
#define SHORT_GPP   "GPP"
#define SHORT_SPP   "SPP"
#define SHORT_SPE   "SPE"
#define SHORT_A     "A"
//
// HELP_LEDSYSTEM
#define SHORT_GLS   "GLS"
#define SHORT_LSH   "LSH"
#define SHORT_LSL   "LSL"
#define SHORT_BLS   "BLS"
//
// HELP_LASERRANGE
#define SHORT_GPX   "GPX"
#define SHORT_SPX   "SPX"
#define SHORT_GRX   "GRX"
#define SHORT_SRX   "SRX"
#define SHORT_GPY   "GPY"
#define SHORT_SPY   "SPY"
#define SHORT_GRY   "GRY"
#define SHORT_SRY   "SRY"
#define SHORT_GDM   "GDM"
#define SHORT_SDM   "SDM"
//
// HELP_LASERPOSITION
#define SHORT_PLP   "PLP"
#define SHORT_ALP   "ALP"
//
// HELP_LASERMATRIX
#define SHORT_PLM   "PLM"
#define SHORT_ALM   "ALM"
//
// HELP_LASERIMAGE
#define SHORT_PLI   "PLI"
#define SHORT_ALI   "ALI"
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HEADER = 12;
//
#define TITLE_LINE            "-----------------------------------------------"
#define MASK_PROJECT          "- Project:   %-32s -"
#define MASK_SOFTWARE         "- Version:   %-32s -"
#define MASK_HARDWARE         "- Hardware:  %-32s -"
#define MASK_DATE             "- Date:      %-32s -"
#define MASK_TIME             "- Time:      %-32s -"
#define MASK_AUTHOR           "- Author:    %-32s -"
#define MASK_PORT             "- Port:      %-32s -"
#define MASK_PARAMETER        "- Parameter: %-32s -"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HELP = 36;
//
#define HELP_COMMON               " Help (Common):"
#define MASK_H                    " %-3s : This Help"
#define MASK_GPH                  " %-3s : Get Program Header"
#define MASK_GSV                  " %-3s : Get Software-Version"
#define MASK_GHV                  " %-3s : Get Hardware-Version"
#define MASK_GPC                  " %-3s : Get Process Count"
#define MASK_SPC                  " %-3s <pc> : Set Process Count"
#define MASK_GPP                  " %-3s : Get Process Period{us}"
#define MASK_SPP                  " %-3s <pp> : Set Process Period{us}"
#define MASK_A                    " %-1s : Abort Process Execution"
// 11
#define HELP_LEDSYSTEM            " Help (LedSystem):"
#define MASK_GLS                  " %-3s : Get State LedSystem"
#define MASK_LSH                  " %-3s : Switch LedSystem On"
#define MASK_LSL                  " %-3s : Switch LedSystem Off"
#define MASK_BLS                  " %-3s <p> <n> : Blink LedSystem with <p>eriod{ms} <n>times"
// 16
#define HELP_LASERRANGE           " Help (LaserRange):"
#define MASK_GPX                  " %-3s : Get Position X"
#define MASK_SPX                  " %-3s <x> : Set Position <x>"
#define MASK_GPY                  " %-3s : Get Position Y"
#define MASK_SPY                  " %-3s <y> : Set Position <y>"
#define MASK_GRX                  " %-3s : Get Range X"
#define MASK_SRX                  " %-3s <xmin> <xmax> <dx> : Set Range X <xmin>..<xmax> <dx>{0..4095}"
#define MASK_GRY                  " %-3s : Get Range Y"
#define MASK_SRY                  " %-3s <ymin> <ymax> <dy> : Set Range Y <ymin>..<ymax> <dy>{0..4095}"
#define MASK_GDM                  " %-3s : Get Delay Motion{us}"
#define MASK_SDM                  " %-3s <delay> : Set Delay Motion{us}"
// 27
#define HELP_LASERPOSITION        " Help (LaserPosition):"
#define MASK_PLP                  " %-3s <x> <y> <p> <c> : Pulse Laser Position<x><y> <p>eriod{us} <c>ount"
#define MASK_ALP                  " %-3s : Abort Laser Position"
// 30
#define HELP_LASERMATRIX          " Help (LaserMatrix):"
#define MASK_PLM                  " %-3s <p> <n>: Pulse Laser Matrix <p>eriod{us} <n>Pulses"
#define MASK_ALM                  " %-3s : Abort Laser Matrix"
// 33
#define HELP_LASERIMAGE           " Help (LaserImage):"
#define MASK_PLI                  " %-3s <x> <y> <p> <c> : Laser Pixel <x><y> <p>eriod{us} <c>ount"
#define MASK_ALI                  " %-3s : Abort Laser Image"
// 36
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_SOFTWAREVERSION = 1;
//
#define MASK_SOFTWAREVERSION      " Software-Version LAS%s"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HARDWAREVERSION = 1;
//
#define MASK_HARDWAREVERSION      " Hardware-Version %s"
//
//-----------------------------------------------------------------------------------------
//
// Command-Parameter
#define COUNT_RXDPARAMETERS 4
#define SIZE_RXDPARAMETER   8
//
//-----------------------------------------------------------------------------------------
//
class CCommand
{
  private:
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Character FRxdCommandLine[SIZE_RXDBUFFER];
  PCharacter FPRxdCommand;
  PCharacter FPRxdParameters[COUNT_RXDPARAMETERS];
  Int16 FRxdParameterCount = 0;
  Int16 FRxdBufferIndex = 0;

  public:
  CCommand();
  ~CCommand();
  //
  PCharacter GetPRxdParameters(UInt8 index)
  {
    return FPRxdParameters[index];
  }
  PCharacter GetTxdBuffer()
  {
    return FTxdBuffer;
  }
  PCharacter GetPRxdCommand()
  {
    return FPRxdCommand;
  }
  //
  void ZeroRxdBuffer();
  void ZeroTxdBuffer();
  void ZeroRxdCommandLine();
  //
  //  Segment - Execution - Basic Output
  void WriteProgramHeader(CSerial &serial);
  void WriteSoftwareVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
  void WriteHelp(CSerial &serial);
  //
  //  Segment - Execution - Common
  void ExecuteGetHelp(CSerial &serial);
  void ExecuteGetProgramHeader(CSerial &serial);
  void ExecuteGetSoftwareVersion(CSerial &serial);
  void ExecuteGetHardwareVersion(CSerial &serial);
  void ExecuteGetProcessCount(CSerial &serial);
  void ExecuteSetProcessCount(CSerial &serial);
  void ExecuteGetProcessPeriod(CSerial &serial);
  void ExecuteSetProcessPeriod(CSerial &serial);
  void ExecuteAbortAll(CSerial &serial);
  //
  //  Segment - Execution - LedSystem
  void ExecuteGetLedSystem(CSerial &serial);
  void ExecuteLedSystemOn(CSerial &serial);
  void ExecuteLedSystemOff(CSerial &serial);
  void ExecuteBlinkLedSystem(CSerial &serial);
  //
  //  Segment - Execution - LaserRange
  void ExecuteGetPositionX(CSerial &serial);    // GPX
  void ExecuteSetPositionX(CSerial &serial);    // SPX
  void ExecuteGetPositionY(CSerial &serial);    // GPY
  void ExecuteSetPositionY(CSerial &serial);    // SPY
  void ExecuteGetRangeX(CSerial &serial);       // GRX
  void ExecuteSetRangeX(CSerial &serial);       // SRX
  void ExecuteGetRangeY(CSerial &serial);       // GRY
  void ExecuteSetRangeY(CSerial &serial);       // SRY
  void ExecuteGetDelayMotion(CSerial &serial);  // GDM
  void ExecuteSetDelayMotion(CSerial &serial);  // SDM
  //
  //  Segment - Execution - LaserPosition
  void ExecutePulseLaserPosition(CSerial &serial);
  void ExecuteAbortLaserPosition(CSerial &serial);
  //
  //  Segment - Execution - LaserMatrix
  void ExecutePulseLaserMatrix(CSerial &serial);
  void ExecuteAbortLaserMatrix(CSerial &serial);
  //
  //  Segment - Execution - LaserImage
  void ExecutePulseLaserImage(CSerial &serial);
  void ExecuteAbortLaserImage(CSerial &serial);
  //  
  // Main
  void Init();
  Boolean DetectRxdLine(CSerial &serial);
  Boolean Analyse(CSerial &serial);
  void CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate);
  Boolean Execute(CSerial &serial);
  Boolean Handle(CSerial &serial);
};
//
#endif // Command_h
