#include "Process.h"
//
CProcess::CProcess()
{
  FLaserPulsePeriod = INIT_LASERPULSEPERIOD;
  FTotalPulseCountPreset = INIT_TOTALPULSECOUNTPRESET;
  FTotalPulseCountActual = INIT_TOTALPULSECOUNTACTUAL;
  FPositionPulseCountPreset = INIT_POSITIONPULSECOUNTPRESET;
  FPositionPulseCountActual = INIT_POSITIONPULSECOUNTACTUAL;
  FRangeDelayMotion = INIT_RANGEDELAYMOTION;
  FRangeDelayPulse = INIT_RANGEDELAYPULSE;
  //
  FXPositionActual = INIT_XPOSITIONACTUAL;
  FXPositionDelta = INIT_XPOSITIONDELTA;
  FXPositionMinimum = INIT_XPOSITIONMINIMUM;
  FXPositionMaximum = INIT_XPOSITIONMAXIMUM;
  //
  FYPositionActual = INIT_YPOSITIONACTUAL;
  FYPositionDelta = INIT_YPOSITIONDELTA;
  FYPositionMinimum = INIT_YPOSITIONMINIMUM;
  FYPositionMaximum = INIT_YPOSITIONMAXIMUM;    
}

