#include "Error.h"
#include "Command.h"
//
extern CCommand LASCommand;
//
CError::CError()
{
  SetCode(INIT_ERRORCODE);
}

EErrorCode CError::GetCode()
{
  return FErrorCode;  
}
void CError::SetCode(EErrorCode errorcode)
{
  FErrorCode = errorcode;
}
  
Boolean CError::Open()
{
  return false; 
}
  
Boolean CError::Close()
{
  return false; 
}

Boolean CError::Handle(CSerial &serial)
{
  if (ecNone != FErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(GetCode(), LASCommand.GetTxdBuffer()); 
    serial.WriteText(LASCommand.GetTxdBuffer());
    serial.WriteText("]: ");
    switch (GetCode())
    {
      case ecNone:
        serial.WriteText("No Error");
        break;
      case ecUnknown:
        serial.WriteText("Unknown");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
      case ecToManyParameters:
        serial.WriteText("To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.WriteText("Missing Target Parameter");
        break;              
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    FErrorCode = ecNone;
    return true;
  }
  return false;
}
