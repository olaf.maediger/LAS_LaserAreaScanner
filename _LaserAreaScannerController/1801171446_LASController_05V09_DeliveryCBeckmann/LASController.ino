#include "Defines.h"
#include "LedLine.h"
#include "Serial.h"
#include "Dac.h"
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Automation.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
CError LASError;
CProcess LASProcess;
CCommand LASCommand;
CAutomation LASAutomation;
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM);
CLedLine LedLine(61, 60, PIN_LEDMOTIONY, PIN_LEDMOTIONX,  // 7..4
                 57, PIN_LEDBUSY, 55, PIN_LEDERROR);      // 3..0
CLed LedLaser(PIN_LEDLASER);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Dac
//---------------------------------------------------
//
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
CDac DigitalAnalogConverter(true, true);
#endif
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort!
CSerial SerialDebug(Serial);
CSerial SerialCommand(Serial1);
CSerial SerialPrinter(Serial2);
// Serial3 - NC
//
void setup() 
{ // Led 
  LedSystem.Open();
  LedLine.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.Off();
    LedLine.On();
    delay(40);
    LedSystem.On();
    LedLine.Off();
    delay(40);
  }
  LedSystem.Off();
  //
  LedLaser.Open();
  LedLaser.Off();
  //
  SerialDebug.Open(115200, RXDECHO_ON);
  // LATER!!! 
  SerialCommand.Open(115200, RXDECHO_OFF);
  // debug:  SerialCommand.Open(115200, RXDECHO_ON);
  //
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.Open();
#endif  
  //
  LASCommand.WriteProgramHeader(SerialCommand);
  LASCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt();  
  LASAutomation.Open();
}

void loop() 
{ 
  LedSystem.On();
  LASError.Handle(SerialCommand);    
  LASCommand.Handle(SerialCommand);
  LASAutomation.Handle(SerialCommand);  
  LedSystem.Off();
}

