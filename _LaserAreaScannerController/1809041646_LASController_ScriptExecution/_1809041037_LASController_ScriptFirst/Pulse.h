#ifndef Pulse_h
#define Pulse_h
//
#include "Defines.h"
#include "IrqTimer.h"
//
const long unsigned WIDTH_PULSECOUNT = 1;
//
class CPulse
{
  protected:
  // Period
  int FChannelPeriod;
  CIrqTimer* FPTimerPulsePeriod;
  void (*FPIrqHandlerPeriod)(void);
  long unsigned FPulsePeriodus;  
  long unsigned FPulseCount;
  long unsigned FPulseStep;
  // Width
  int FChannelWidth;
  CIrqTimer* FPTimerPulseWidth;
  void (*FPIrqHandlerWidth)(void);
  long unsigned FPulseWidthus;
  //
  public:
  CPulse(int channelperiod, int channelwidth);
  //  Period
  void SetInterruptHandlerPeriod(void (*pirqhandlerperiod)(void));
  void SetPulsePeriodus(long unsigned pulseperiodus);
  void Start();
  void Stop();
  long unsigned GetPulseStep();
  void SetPulseStep(long unsigned pulsestep);
  long unsigned GetPulseCount();
  void SetPulseCount(long unsigned pulsecount);
  long unsigned IncrementStopPulseStep();
  //  Width
  void SetInterruptHandlerWidth(void (*pirqhandlerwidth)(void));
  long unsigned GetPulseWidthus();
  void SetPulseWidthus(long unsigned pulsewidthus);
  void StartWidth();
  void StopWidth();
};
//
#endif  // Pulse_h
