//
//--------------------------------
//  Library LedLine
//--------------------------------
//
#ifndef LedLine_h
#define LedLine_h
//
#include <vector>
#include "Defines.h"
#include "Led.h"
//
using namespace std;
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define PIN_NC 0
//
//--------------------------------
//  Section - CLedLine
//--------------------------------
//
class CLedLine
{
  private:
  int FPins[8];
  EStateLed FStates[8];
  
  public:
  CLedLine(int pin7, int pin6, int pin5, int pin4, 
           int pin3, int pin2, int pin1, int pin0);
  EStateLed GetState(int ledindex);
  Boolean Open();
  Boolean Close();
  void On();
  void On(byte mask);
  void Off();
  void Off(byte mask);
  void Toggle();
  void Toggle(byte mask);
  void Pulse(UInt32 delaymillis, UInt32 count);
  void Pulse(byte mask, UInt32 delaymillis, UInt32 count);
};
//
#endif
