#include "Automation.h"
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
extern enum EErrorCode GlobalErrorCode;
extern EStateAutomation StateAutomation;
//
// extern CLed LedSystem;
extern CLcdKey LcdKey;
extern CRotaryDecoder RotaryDecoder;
//
long unsigned TimeSystemStamp;
//
//#########################################################
//  Segment - Automation
//#########################################################
//
void HandleAutomationInitWelcome(CSerial &serial)
{  
  TimeSystemStamp = millis();
  LcdKey.ClearScreen();
  LcdKey.WriteText(0, 0, "LaserAreaScanner");  
  LcdKey.WriteText(0, 1, "01V01 - LLG 2016");
  StateAutomation = saShowWelcome;
}

void HandleAutomationShowWelcome(CSerial &serial)
{  
  long unsigned TimeSystemActual = millis();
  if (TIME_SHOWWELCOME < (TimeSystemActual - TimeSystemStamp))
  {   
    TimeSystemStamp = TimeSystemActual;
    StateAutomation = saInitOffset;
  }
}

void HandleAutomationInitOffset(CSerial &serial)
{  
  TimeSystemStamp = millis();
  LcdKey.ClearScreen();
  LcdKey.WriteText(0, 0, "Adjust Offset");  
  LcdKey.WriteText(0, 1, "X:.... Y:....");
  StateAutomation = saAdjustOffset;
}

void HandleAutomationAdjustOffset(CSerial &serial)
{  
  if (RotaryDecoder.Refresh())
  {
    if (RotaryDecoder.GetStateSwitch())
    {
      if (1 == RotaryDecoder.GetPositionDelta())
      {
        RotaryDecoder.SetPositionDelta(10);
      } 
      else
      {
        RotaryDecoder.SetPositionDelta(1);        
      }
    }
    LcdKey.WriteInt16(2, 1, 4, RotaryDecoder.GetPositionActual());
  }  
}

void HandleAutomationScanMatrixInit(CSerial &serial)
{  
}

void HandleAutomationScanMatrixBusy(CSerial &serial)
{  
}

void HandleAutomationScanMatrixEnd(CSerial &serial)
{  
}


void HandleAutomation(CSerial &serial)
{
  switch (StateAutomation)
  {
    case saInitWelcome:
      HandleAutomationInitWelcome(serial);
      break;
    case saShowWelcome:
      HandleAutomationShowWelcome(serial);
      break;
    case saInitOffset:
      HandleAutomationInitOffset(serial);
      break;
    case saAdjustOffset:
      HandleAutomationAdjustOffset(serial);
      break;
    case saScanAreaInit:
      HandleAutomationScanMatrixInit(serial);
      break;
    case saScanAreaBusy:
      HandleAutomationScanMatrixBusy(serial);
      break;
    case saScanAreaEnd:
      HandleAutomationScanMatrixEnd(serial);
      break;
  }
}















//void HandleAutomationBlink(CSerial &serial)
//{
//  long unsigned TimeSystemActual = millis();
//  if (LedSystem.GetPeriodBlinkHalf() < (TimeSystemActual - TimeSystemStamp))
//  {   
//    TimeSystemStamp = TimeSystemActual;
//    LedSystem.Toggle();
//    if (slOff == LedSystem.GetState())
//    {
//      if (LedSystem.CountActualIncrement())
//      {
//        StateAutomation = saIdle;
//      }
//    }
//  }
//}



//  if (RotaryDecoder.Refresh())
//  {
//    if (RotaryDecoder.GetStateSwitch())
//    {
//      RotaryDecoder.SetPositionActual(0);
//    }
//    serial.WriteNewLine();
//    serial.WriteText("# Switch<"); 
//    serial.WriteInt32(RotaryDecoder.GetStateSwitch());
//    serial.WriteText("> Position<"); 
//    serial.WriteInt32(RotaryDecoder.GetPositionActual());
//    serial.WriteText(">");
//  }
//  // delayMicroseconds(1000000);
//void HandleAutomationBusy(CSerial &serial)
//{
////  LedMotionX.On();
////  LedMotionY.On();
////  //
////  SetPosition();
////  TriggerLaserPulses();
////  //
////  XPositionActual += XPositionDelta;
////  if (XPositionMaximum < XPositionActual)
////  {
////    XPositionActual = XPositionMinimum;
////    YPositionActual += YPositionDelta;
////    if (YPositionMaximum < YPositionActual)
////    { // Finish Area scanning
////      XPositionActual = XPositionMinimum;
////      YPositionActual = YPositionMinimum;
////      CountRepetition--;
////      LedBusy.Toggle();
////      if (0 == CountRepetition)
////      { // ends...
////        // SetPosition();
////        ExecuteAbortMotion(serial);
////        return;
////      }
////    } 
////  } 
//}

