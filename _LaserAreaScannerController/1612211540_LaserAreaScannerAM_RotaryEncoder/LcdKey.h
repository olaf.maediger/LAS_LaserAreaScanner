//
//--------------------------------
//  Library LcdKeypad
//--------------------------------
//
#ifndef LcdKey_h
#define LcdKey_h
//
#include "Arduino.h"
#include <LiquidCrystal.h>
#include "Defines.h"
//
//    LiquidCrystal()
//    begin()
//    clear()
//    home()
//    setCursor()
//    write()
//    print()
//    cursor()
//    noCursor()
//    blink()
//    noBlink()
//    display()
//    noDisplay()
//    scrollDisplayLeft()
//    scrollDisplayRight()
//    autoscroll()
//    noAutoscroll()
//    leftToRight()
//    rightToLeft()
//    createChar() 
//
//--------------------------------
//  Section - Constant
//--------------------------------
// 
#define LCDKEY_COUNTCOL 16
#define LCDKEY_COUNTROW 2
//
class CLcdKey
{
  private:
  LiquidCrystal* FPDisplay;
  int FCursorCol, FCursorRow;
 
  public:
  CLcdKey();
  Boolean Open();
  Boolean Close();
  //
  void ClearScreen();
  void IncrementCursor(const Byte count = 1);
  void SetCursor(Byte col, Byte row);
  //
  void WriteText(PCharacter ptext);
  void WriteCharacter(Character character);
  void WriteInt16(Int16 value);
  //
  void WriteText(Byte col, Byte row, PCharacter ptext);
  void WriteCharacter(Byte col, Byte row, Character character);
  void WriteInt16(Byte col, Byte row, Byte digitcount, Int16 value);
};
//
#endif // LcdKey_h
