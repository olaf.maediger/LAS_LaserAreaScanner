//
//--------------------------------
//  Library RotaryDecoder
//--------------------------------
//
#ifndef RotaryDecoder_h
#define RotaryDecoder_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Type
//--------------------------------
//
// ### Rotation CW
// EncoderA     ----    ----    ----    ----
//          ----+   ----+   ----+   ----+   ----+
// EncoderB   ----    ----    ----    ----    ----
//          --+   ----+   ----+   ----+   ----+
// ### Rotation CCW
// EncoderA     ----    ----    ----    ----
//          ----+   ----+   ----+   ----+   ----+
// EncoderB --    ----    ----    ----    ----
//            ----+   ----+   ----+   ----+   ----+
//
// ### Rotation CW:  ALH-BH -> BHL-AH -> AHL-BL -> BLH-AL -> ...
//     State:        srdALH_BH srdBHL_AH srdAHL_BL srdBLH_AL  ...
// ### Rotation CCW: ALH-BL -> BLH-AH -> AHL-BH -> BHL-AL -> ...
//     State:        srdALH_BL srdBLH_AH srdAHL_BH srdBHL_AL ...
//
enum EStateRotaryDecoder
{
  srdUndefined = -1,
  // Rotation CW
  srdALH_BH = 0,
  srdBHL_AH = 1,
  srdAHL_BL = 2,
  srdBLH_AL = 3,
  // Rotation CCW
  srdALH_BL = 4,
  srdBLH_AH = 5,
  srdAHL_BH = 6,
  srdBHL_AL = 7
};
//
enum EStateRotation
{
  srUndefined = -1,
  srClockwise = 0,
  srCounterClockwise = 1,
};

enum EEdgeResolution
{
  erDouble = 0,
  erQuad = 1
};
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define INIT_STATEROTARYDECODER srdUndefined
#define INIT_STATEROTATION srUndefined
#define INIT_EDGERESOLUTION erDouble
#define INIT_POSITIONMINIMUM 0
#define INIT_POSITIONMAXIMUM 100
#define INIT_POSITIONACTUAL 0
#define INIT_POSITIONDELTA 1
#define INIT_ENCODERA 0
#define INIT_ENCODERB 0
#define INIT_SWITCH 0
//
class CRotaryDecoder
{
  private:
  int FPinEncoderA;
  int FPinEncoderB;
  int FPinSwitch;
  Boolean FEncoderA;
  Boolean FEncoderB;
  Boolean FSwitch;
  EStateRotaryDecoder FStateRotaryDecoder;
  EStateRotation FStateRotation;
  EEdgeResolution FEdgeResolution;
  Int32 FPositionMinimum;
  Int32 FPositionMaximum;
  Int32 FPositionActual;
  Int32 FPositionDelta;
  //
  public:
  CRotaryDecoder(int pinencodera, int pinencoderb, int pinswitch);
  Boolean Open();
  Boolean Close();
  Boolean Refresh(); // cyclic refresh to detect states, positions,...
  EStateRotaryDecoder GetStateRotaryDecoder();
  EStateRotation GetStateRotation();
  void SetStateRotation(EStateRotation value);
  void SetEdgeResolution(EEdgeResolution edgeresolution);
  Int32 GetPositionMinimum();
  void SetPositionMinimum(Int32 positionminimum);
  Int32 GetPositionMaximum();
  void SetPositionMaximum(Int32 positionmaximum);
  Int32 GetPositionActual();
  void SetPositionActual(Int32 positionactual);
  Int32 GetPositionDelta();
  void SetPositionDelta(Int32 positiondelta);
  Boolean GetStateSwitch();
  void IncrementPositionActual();
  void DecrementPositionActual();
};
//
#endif // RotaryDecoder_h
