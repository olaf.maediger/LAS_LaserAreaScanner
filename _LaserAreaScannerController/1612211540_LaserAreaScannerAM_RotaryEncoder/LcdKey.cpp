//
//--------------------------------
//  Library LcdKey
//--------------------------------
//
#include "LcdKey.h"
//
CLcdKey::CLcdKey()
{
  FPDisplay = new LiquidCrystal(8, 9, 4, 5, 6, 7);
}

Boolean CLcdKey::Open()
{
  FPDisplay->begin(LCDKEY_COUNTCOL, LCDKEY_COUNTROW);
  FPDisplay->clear();
  FPDisplay->display();
  FCursorCol = 0;
  FCursorRow = 0;
  return true;
}

Boolean CLcdKey::Close()
{
  FPDisplay->noDisplay();
  return true;
}


void CLcdKey::ClearScreen()
{
  FCursorCol = 0;
  FCursorRow = 0;
  FPDisplay->clear();
}

void CLcdKey::IncrementCursor(const Byte count)
{
  Byte Count, Index;
  for (Index = 0; Index < Count; Index++)
  {
    FCursorCol++;
    if (LCDKEY_COUNTCOL <= FCursorCol)
    {
      FCursorCol = 0;
      FCursorRow++;
      if (LCDKEY_COUNTROW <= FCursorRow)
      {
        FCursorRow = 0;
      }
    }    
  }
  FPDisplay->setCursor(FCursorCol, FCursorRow);
}

void CLcdKey::SetCursor(Byte col, Byte row)
{
  FCursorCol = col;
  FCursorRow = row;
  FPDisplay->setCursor(FCursorCol, FCursorRow);  
}



void CLcdKey::WriteCharacter(Character character)
{
  FPDisplay->write(character);
  IncrementCursor(1);  
}

void CLcdKey::WriteText(PCharacter ptext)
{
  FPDisplay->write(ptext);
  IncrementCursor(strlen(ptext));
}

void CLcdKey::WriteInt16(Int16 value)
{
  char Buffer[10];
  sprintf(Buffer, "%d", value);
  FPDisplay->write(Buffer);
  IncrementCursor(strlen(Buffer));
}



void CLcdKey::WriteCharacter(Byte col, Byte row, Character character)
{
  SetCursor(col, row);
  FPDisplay->write(character);
  IncrementCursor(1);  
}

void CLcdKey::WriteText(Byte col, Byte row, PCharacter ptext)
{
  SetCursor(col, row);
  FPDisplay->write(ptext);
  IncrementCursor(strlen(ptext));
}

void CLcdKey::WriteInt16(Byte col, Byte row, Byte digitcount, Int16 value)
{
  SetCursor(col, row);
  char Buffer[10];
  char Format[4];
  sprintf(Format, " %d ", digitcount);
  Format[0] = '%';
  Format[1] = 0x30 + digitcount;
  Format[2] = 'd';
  Format[3] = 0x00;
  sprintf(Buffer, Format, value);
  FPDisplay->write(Buffer);
  IncrementCursor(strlen(Buffer));
}







