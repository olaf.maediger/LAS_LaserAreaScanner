#ifndef Automation_h
#define Automation_h
//
#include "Defines.h"
#include "Helper.h"
#include "Utilities.h"
#include "Serial.h"
#include "Led.h"
#include "LcdKey.h"
#include "RotaryDecoder.h"
//
enum EStateAutomation
{
  saInitWelcome = 0,
  saShowWelcome = 1,
  saInitOffset = 2,
  saAdjustOffset = 3,
  saScanAreaInit = 4,
  saScanAreaBusy = 5,
  saScanAreaEnd = 6
};
//
#define INIT_STATEAUTOMATION saInitWelcome
//
#define TIME_SHOWWELCOME 5000
//
void HandleAutomation(CSerial &serial);
//
#endif // Automation_h
