//
//--------------------------------
//  Library RotaryDecoder
//--------------------------------
//
#include "RotaryDecoder.h"
//
CRotaryDecoder::CRotaryDecoder(int pinencodera, 
                               int pinencoderb, 
                               int pinswitch)
{
  FPinEncoderA = pinencodera;
  FPinEncoderB = pinencoderb;
  FPinSwitch = pinswitch;
  FStateRotaryDecoder = INIT_STATEROTARYDECODER;
  FStateRotation = INIT_STATEROTATION;
  FEdgeResolution = INIT_EDGERESOLUTION;
  FPositionMaximum = INIT_POSITIONMAXIMUM;
  FPositionMinimum = INIT_POSITIONMINIMUM;
  FPositionActual = INIT_POSITIONACTUAL;
  FEncoderA = INIT_ENCODERA;
  FEncoderB = INIT_ENCODERB;
  FSwitch = INIT_SWITCH;
}

EStateRotaryDecoder CRotaryDecoder::GetStateRotaryDecoder()
{
  return FStateRotaryDecoder;
}

EStateRotation CRotaryDecoder::GetStateRotation()
{
  return FStateRotation; 
}

void CRotaryDecoder::SetEdgeResolution(EEdgeResolution edgeresolution)
{
  FEdgeResolution = edgeresolution;
}

Int32 CRotaryDecoder::GetPositionMinimum()
{
  return FPositionMinimum;
}
void CRotaryDecoder::SetPositionMinimum(Int32 positionminimum)
{
  FPositionMinimum = positionminimum;
}

Int32 CRotaryDecoder::GetPositionMaximum()
{
  return FPositionMaximum;
}
void CRotaryDecoder::SetPositionMaximum(Int32 positionmaximum)
{
  FPositionMaximum = positionmaximum;
}

Int32 CRotaryDecoder::GetPositionActual()
{
  return FPositionActual;  
}
void CRotaryDecoder::SetPositionActual(Int32 positionactual)
{
  FPositionActual = positionactual;  
}

Boolean CRotaryDecoder::GetStateSwitch()
{
  return !FSwitch;
}

Boolean CRotaryDecoder::Open()
{
  pinMode(FPinEncoderA, INPUT);
  pinMode(FPinEncoderB, INPUT);
  pinMode(FPinSwitch, INPUT);
  FStateRotaryDecoder = INIT_STATEROTARYDECODER;
  FStateRotation = INIT_STATEROTATION;
  FPositionMaximum = INIT_POSITIONMAXIMUM;
  FPositionMinimum = INIT_POSITIONMINIMUM;
  FPositionActual = INIT_POSITIONACTUAL;
  FEdgeResolution = INIT_EDGERESOLUTION;
  return true;
}

Boolean CRotaryDecoder::Close()
{
  FStateRotaryDecoder = INIT_STATEROTARYDECODER;
  FStateRotation = INIT_STATEROTATION;
  FPositionMaximum = INIT_POSITIONMAXIMUM;
  FPositionMinimum = INIT_POSITIONMINIMUM;
  FPositionActual = INIT_POSITIONACTUAL;
  FEdgeResolution = INIT_EDGERESOLUTION;
  return true;
}

void CRotaryDecoder::SetStateRotation(EStateRotation value)
{
//  if (value != FStateRotation)
//  { // Reset of dynamic Increment
//    FPositionDelta = 1;
//  }
  FStateRotation = value;
}

Int32 CRotaryDecoder::GetPositionDelta()
{
  return FPositionDelta;
}
void CRotaryDecoder::SetPositionDelta(Int32 positiondelta)
{
  FPositionDelta = positiondelta;
}


void CRotaryDecoder::DecrementPositionActual()
{
  if (FPositionMinimum + FPositionDelta < FPositionActual)
  {
    FPositionActual -= FPositionDelta;
  }
  else
  {
    FPositionActual = FPositionMinimum;
  }
}

void CRotaryDecoder::IncrementPositionActual()
{
  if (FPositionActual < FPositionMaximum - FPositionDelta)
  {
    FPositionActual += FPositionDelta;
  }
  else
  {
    FPositionActual = FPositionMaximum;
  }  
}



// cyclic refresh to detect states, positions,...
Boolean CRotaryDecoder::Refresh()
{
  Boolean EncoderA = digitalRead(FPinEncoderA);
  Boolean EncoderB = digitalRead(FPinEncoderB);
  Boolean Switch = digitalRead(FPinSwitch);
  if ((erDouble == FEdgeResolution) || (erQuad == FEdgeResolution))
  { // A Edge Detection
    switch (EncoderB)
    {
      case true: // EncoderB == H
        if ((!FEncoderA) && (EncoderA))
        { // FEncoderA == L -> EncoderA == H
          FStateRotaryDecoder = srdALH_BH;
          SetStateRotation(srClockwise);
          DecrementPositionActual();
          FEncoderA = EncoderA;
          FEncoderB = EncoderB;
          FSwitch = Switch;
          return true;
        }
        else
          if ((FEncoderA) && (!EncoderA))
          { // FEncoderA == H -> EncoderA == L
            FStateRotaryDecoder = srdAHL_BH;          
            SetStateRotation(srCounterClockwise);
            IncrementPositionActual();
            FEncoderA = EncoderA;
            FEncoderB = EncoderB;
            FSwitch = Switch;
            return true;
          }
        break;
      case false: // EncoderB == L
        if ((!FEncoderA) && (EncoderA))
        { // FEncoderA == L -> EncoderA == H
          FStateRotaryDecoder = srdALH_BL;
          SetStateRotation(srCounterClockwise);
          IncrementPositionActual();
          FEncoderA = EncoderA;
          FEncoderB = EncoderB;
          FSwitch = Switch;
          return true;
        }
        else
          if ((FEncoderA) && (!EncoderA))
          { // FEncoderA == H -> EncoderA == L
            FStateRotaryDecoder = srdAHL_BL;   
            SetStateRotation(srClockwise);
            DecrementPositionActual();
            FEncoderA = EncoderA;
            FEncoderB = EncoderB;
            FSwitch = Switch;
            return true;
          }
        break;
    }
  }
  if (erQuad == FEdgeResolution)
  { // B Edge Detection
    switch (EncoderA)
    {
      case true: // EncoderA == H
        if ((!FEncoderB) && (EncoderB))
        { // FEncoderB == L -> EncoderB == H
          FStateRotaryDecoder = srdBLH_AH;
          SetStateRotation(srCounterClockwise);
          IncrementPositionActual();
          FEncoderA = EncoderA;
          FEncoderB = EncoderB;
          FSwitch = Switch;
          return true;
        }
        else
          if ((FEncoderB) && (!EncoderB))
          { // FEncoderB == H -> EncoderB == L
            FStateRotaryDecoder = srdBHL_AH;          
            SetStateRotation(srClockwise);
            DecrementPositionActual();
            FEncoderA = EncoderA;
            FEncoderB = EncoderB;
            FSwitch = Switch;
            return true;
          }
        break;
      case false: // EncoderA == L
        if ((!FEncoderB) && (EncoderB))
        { // FEncoderB == L -> EncoderB == H
          FStateRotaryDecoder = srdBLH_AL;
          SetStateRotation(srClockwise);
          DecrementPositionActual();
          FEncoderA = EncoderA;
          FEncoderB = EncoderB;
          FSwitch = Switch;
          return true;
        }
        else
          if ((FEncoderB) && (!EncoderB))
          { // FEncoderB == H -> EncoderB == L
            FStateRotaryDecoder = srdBHL_AL;   
            SetStateRotation(srCounterClockwise);
            IncrementPositionActual();
            FEncoderA = EncoderA;
            FEncoderB = EncoderB;
            FSwitch = Switch;
            return true;
          }
        break;
    }
  }
  // no changes on AB, test if switch changes:
  if (Switch != FSwitch)
  {
    FSwitch = Switch;
    return true;
  }
  //
  return false;
}


