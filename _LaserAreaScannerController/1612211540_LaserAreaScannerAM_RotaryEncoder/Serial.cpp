#include "Serial.h"
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
CSerialBase::CSerialBase()
{
  FLocalEcho = INIT_LOCALECHO;
}

Boolean CSerialBase::GetLocalEcho()
{
  return FLocalEcho;
}
void CSerialBase::SetLocalEcho(Boolean localecho)
{
  FLocalEcho = localecho;
}


void CSerialBase::WriteNewLine()
{
  WriteText(PROMPT_NEWLINE);  
}
  
void CSerialBase::WritePrompt()
{
  WriteText(PROMPT_INPUT);
}
  
void CSerialBase::WriteAnswer()
{
  WriteText(PROMPT_ANSWER);
}
//
//----------------------------------------------------
//  Segment - CSerialN
//----------------------------------------------------
//
CSerialN::CSerialN(HardwareSerial *pserial)
{
  FPSerial = pserial;
}

Boolean CSerialN::Open(long unsigned baudrate)
{
  FPSerial->begin(baudrate);
  return IsOpen();
}
Boolean CSerialN::Close()
{
  FPSerial->end();
  return !IsOpen();
}

Boolean CSerialN::IsOpen()
{
  return (NULL != FPSerial);
}
  
void CSerialN::WriteCharacter(Character character)
{
  FPSerial->write(character);
}

void CSerialN::WriteText(PCharacter ptext)
{
  FPSerial->write(ptext);
}  

void CSerialN::WriteLine(PCharacter ptext)
{
  FPSerial->write(ptext);
  WriteNewLine();
}  

Int16 CSerialN::GetRxdByteCount()
{
  return FPSerial->available();
}

Character CSerialN::ReadCharacter()
{
  return (char)FPSerial->read();
}
#ifdef ARDUINODUE  
//
//----------------------------------------------------
//  Segment - CSerialU
//----------------------------------------------------
//
CSerialU::CSerialU(Serial_ *pserial)
{
  FPSerial = pserial;
}

Boolean CSerialU::Open(long unsigned baudrate)
{
  FPSerial->begin(baudrate);
  return IsOpen();
}

Boolean CSerialU::Close()
{
  FPSerial->end();
  return !IsOpen();
}

Boolean CSerialU::IsOpen()
{
  return FPSerial->IsOpen();
}
  
void CSerialU::WriteCharacter(Character character)
{
  FPSerial->write(character);
}

void CSerialU::WriteText(PCharacter ptext)
{
  FPSerial->write(ptext);
}  

void CSerialU::WriteLine(PCharacter ptext)
{
  FPSerial->write(ptext);
  WriteNewLine();
}  

Int16 CSerialU::GetRxdByteCount()
{
  return FPSerial->available();
}

Character CSerialU::ReadCharacter()
{
  return (char)FPSerial->read();
}
#endif
//
//----------------------------------------------------
//  Segment - CSerial - Global Access for ALL Serials
//----------------------------------------------------
//
CSerial::CSerial(HardwareSerial *pserial)
{  
  FPSerial = (CSerialN*)(new CSerialN(pserial));
}

#ifdef ARDUINODUE  
CSerial::CSerial(Serial_ *pserial)
{
  FPSerial = (CSerialU*)(new CSerialU(pserial));
}
#endif

Boolean CSerial::Open(long unsigned baudrate)
{
  return FPSerial->Open(baudrate);
}

Boolean CSerial::Close()
{
  return FPSerial->Close();
}

Boolean CSerial::IsOpen()
{
  return FPSerial->IsOpen();
}

Boolean CSerial::GetLocalEcho()
{
  return FPSerial->GetLocalEcho();
}
void CSerial::SetLocalEcho(Boolean localecho)
{
  FPSerial->SetLocalEcho(localecho);
}

void CSerial::WriteCharacter(Character character)
{
  FPSerial->WriteCharacter(character);
}

void CSerial::WriteText(PCharacter ptext)
{
  FPSerial->WriteText(ptext);
}

void CSerial::WriteLine(PCharacter ptext)
{
  FPSerial->WriteLine(ptext);
}

void CSerial::WriteNewLine()
{
  FPSerial->WriteNewLine();
}

void CSerial::WriteAnswer()
{
  FPSerial->WriteAnswer();
}

void CSerial::WritePrompt()
{
  FPSerial->WritePrompt();
}

void CSerial::WriteInt32(Int32 value)
{  
  char TxdBuffer[16];
  sprintf(TxdBuffer, "%li", value);
  WriteText(TxdBuffer);
}
void CSerial::WriteDouble(Double value)
{  
  char TxdBuffer[16];
  sprintf(TxdBuffer, "%d", value);
  WriteText(TxdBuffer);
}

Int16 CSerial::GetRxdByteCount()
{
  return FPSerial->GetRxdByteCount();
}

Character CSerial::ReadCharacter()
{
  return (char)FPSerial->ReadCharacter();
}



