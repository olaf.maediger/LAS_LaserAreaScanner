#ifndef Dispatcher_h
#define Dispatcher_h
//
#include "Defines.h"
#include "Helper.h"
#include "Utilities.h"
#include "Serial.h"
#include "Command.h"
#include "Led.h"
#include "Automation.h"
//
void WriteProgramHeader(CSerial &serialcommand);
void WriteHelp(CSerial &serialcommand);
//  Handler - Error
void HandleError(CSerial &serial);
//  Handler - SerialPC Communication
void HandleCommand(CSerial &serialcommand, CSerial &serialslave); // SerialPC
//  DirectTransfer -  SerialSlave -> SerialPC
void DirectTransferResponse(CSerial &serialsource, CSerial &serialtarget); 
//
#endif // Dispatcher_h

