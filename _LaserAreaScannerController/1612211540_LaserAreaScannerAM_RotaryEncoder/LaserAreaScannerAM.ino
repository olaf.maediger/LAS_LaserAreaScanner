//###################################################
// Project:  LASAM - LaserAreaScannerArduinoMega
// Hardware: Arduno Mega 2560
// Function now: Serial-Command-Dispatcher
// - ArduinoMega gives Commands to ArduinoDue and gets Responses / Events
// Function later: User-Input-Output-Processor
// Parts:
// - ArduinoMega2560 16MHz 5V0 8bit Atmel ATmega2560
//   - Serial1 : CommandChannel to PC (here: ArduinoDue 3V3)
//     - Levelshifter 5V0 - 3V3: Txd, Rxd
//   - Serial2 : Usb-FtdiFT232RL-Serial for Debugging
//   - LedSystem : System-State debugging
//   - LCD Keypad Shield Sainsmart
//     - 16x2 LCDisplay
//     - 6 Keys (Select, Left, Up, Down, Right, Reset)
// later - RotaryEncoder (Hardware Rotation->ABPulses)
// later   - adapted to CRotaryDecoder (Software ABPulses->Position)
//###################################################
//
#include "Defines.h"
#include "Helper.h"
#include "Utilities.h"
#include "Led.h"
#include "Serial.h"
#include "Command.h"
#include "Dispatcher.h"
#include "Automation.h"
#include "LcdKey.h"
// later #include "RotaryDecoder.h"
// later #include ".h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables
//---------------------------------------------------
//
enum EErrorCode GlobalErrorCode;
EStateAutomation StateAutomation;
CCommand Command;
Boolean CommandSwitchSlave;
//
//---------------------------------------------------
// Segment - Global - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM);
//
//---------------------------------------------------
// Segment - Global - LcdKey
//---------------------------------------------------
//
CLcdKey LcdKey;
//
//---------------------------------------------------
// Segment - Global - LcdKey
//---------------------------------------------------
//
CRotaryDecoder RotaryDecoder(PIN_ENCODERA, PIN_ENCODERB, PIN_SWITCH);
//
//---------------------------------------------------
// Segment - Global - Serial
//---------------------------------------------------
//
CSerial SerialPC(&Serial1);
CSerial SerialSlave(&Serial2);
//#ifdef ARDUINODUE
////CSerial SerialUsb(&SerialUSB);
//#endif   
//
void setup() 
{ 
  GlobalErrorCode = INIT_GLOBALERRORCODE;
  StateAutomation = INIT_STATEAUTOMATION;
  CommandSwitchSlave = INIT_COMMANDSWITCHSLAVE;
  //
  LedSystem.Open();
  LcdKey.Open();
  //
  RotaryDecoder.Open();
  RotaryDecoder.SetPositionMinimum(POSITIONENCODER_MINIMUM);
  RotaryDecoder.SetPositionMaximum(POSITIONENCODER_MAXIMUM);
  RotaryDecoder.SetPositionActual(POSITIONENCODER_INIT);
  //
  SerialPC.Open(115200);
  SerialSlave.Open(115200);
  //
//  WriteProgramHeader(SerialPC);
//  WriteHelp(SerialPC);
//  SerialPC.WritePrompt();
}

void loop() 
{
  //HandleError(SerialPC);  
  //HandleCommand(SerialPC, SerialSlave);
  //DirectTransferResponse(SerialSlave, SerialPC);
  HandleAutomation(SerialPC);
  //
}



