#include "Utilities.h"
#include "LedLine.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
#include "Automation.h"
//
//
//-------------------------------------------
//  Segment - Global Variable
//-------------------------------------------
//  
extern enum EErrorCode GlobalErrorCode;
//
extern EStateAutomation StateAutomation;
extern UInt32 CountRepetition; // [1]
extern UInt32 CountLaserPulses; // [1]
extern UInt32 DelayMotion; // [us]
extern UInt32 DelayPulse; // [us]
//
extern UInt16 XPositionActual; // [stp]
extern UInt16 XPositionDelta; // [stp]
extern UInt16 XPositionMinimum; // [stp]
extern UInt16 XPositionMaximum; // [stp]
//
extern UInt16 YPositionActual; // [stp]
extern UInt16 YPositionDelta; // [stp]
extern UInt16 YPositionMinimum; // [stp]
extern UInt16 YPositionMaximum; // [stp]
//
//-------------------------------------------
//  Segment - Global Class
//-------------------------------------------
//  
extern CLedLine LedLine;
//
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
extern CDac DigitalAnalogConverter;
extern CLed LedLaser;
#endif
//
extern CCommand Command;
extern CAutomation Automation;
//
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_VERSION);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_SOFTWAREVERSION);
  serial.WriteNewLine();
}

void WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_HARDWAREVERSION);
  serial.WriteNewLine();
}

void WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.WriteLine(HELP_H);
  serial.WriteAnswer(); serial.WriteLine(HELP_GPH);
  serial.WriteAnswer(); serial.WriteLine(HELP_GSV);
  serial.WriteAnswer(); serial.WriteLine(HELP_GHV);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LEDLASER);
  serial.WriteAnswer(); serial.WriteLine(HELP_GLL);
  serial.WriteAnswer(); serial.WriteLine(HELP_LLH);
  serial.WriteAnswer(); serial.WriteLine(HELP_LLL);
  serial.WriteAnswer(); serial.WriteLine(HELP_PLL);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_DAC);
  serial.WriteAnswer(); serial.WriteLine(HELP_GDV);
  serial.WriteAnswer(); serial.WriteLine(HELP_SDV);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_AUTOMATION);
  serial.WriteAnswer(); serial.WriteLine(HELP_SMX);
  serial.WriteAnswer(); serial.WriteLine(HELP_SMY);
  serial.WriteAnswer(); serial.WriteLine(HELP_SDM);
  serial.WriteAnswer(); serial.WriteLine(HELP_SDP);
  serial.WriteAnswer(); serial.WriteLine(HELP_STT);
  serial.WriteAnswer(); serial.WriteLine(HELP_ABT);  
}
//
//#########################################################
//  Segment - Execution - Common
//#########################################################
//
void ExecuteGetHelp(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(Command.GetTxdBuffer(), ": %s %i", 
          Command.GetPRxdCommand(), COUNT_HELPLINES);
  serial.WriteLine(Command.GetTxdBuffer());         
  WriteHelp(serial);
  serial.WritePrompt();
}

void ExecuteGetProgramHeader(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(Command.GetTxdBuffer(), ": %s %i", 
          Command.GetPRxdCommand(), COUNT_HEADERLINES);
  serial.WriteLine(Command.GetTxdBuffer());   
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void ExecuteGetSoftwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response:
  sprintf(Command.GetTxdBuffer(), ": %s %i", 
          Command.GetPRxdCommand(), COUNT_ANSWERLINE);
  serial.WriteLine(Command.GetTxdBuffer());   
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void ExecuteGetHardwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  sprintf(Command.GetTxdBuffer(), ": %s %i", 
          Command.GetPRxdCommand(), COUNT_ANSWERLINE);
  serial.WriteLine(Command.GetTxdBuffer());   
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LedLaser
//#########################################################
//
void ExecuteGetLedLaser(CSerial &serial)
{ // Analyse parameters:
  // Execute:
  int State = LedLaser.GetState();
  // Response:
  sprintf(Command.GetTxdBuffer(), ": %s %i", 
          Command.GetPRxdCommand(), State);
  serial.WriteLine(Command.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSwitchLedLaserOn(CSerial &serial)
{  // Analyse parameters:
  // Execute:
  LedLaser.On();
  // Response:
  sprintf(Command.GetTxdBuffer(), ": %s", 
          Command.GetPRxdCommand());
  serial.WriteLine(Command.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSwitchLedLaserOff(CSerial &serial)
{ // Analyse parameters:
  // Execute:
  LedLaser.Off();
  // Response:
  sprintf(Command.GetTxdBuffer(), ": %s", 
          Command.GetPRxdCommand());
  serial.WriteLine(Command.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecutePulseLedLaser(CSerial &serial)
{  // Analyse parameters:
  UInt32 DelayMillis = atol(Command.GetPRxdParameters(0));
  UInt32 Count = atol(Command.GetPRxdParameters(1));
  // Execute:
  LedLaser.Pulse(DelayMillis, Count);
  Automation.SetState(saLedLaserPulseBegin);
  // Response:
  sprintf(Command.GetTxdBuffer(), ": %s %lu %lu", 
          Command.GetPRxdCommand(), DelayMillis, Count);
  serial.WriteLine(Command.GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Dac
//#########################################################
//
//void ExecuteGetDacValue(CSerial &serial)
//{  // Analyse parameters:
//  UInt8 Channel = atoi(Command.GetPRxdParameters(0));
//  // Execute:
//  UInt16 Value = 0x0000;
//#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
//  Value = DigitalAnalogConverter.GetValue(Channel);
//#endif  
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %u %u", 
//          Command.GetPRxdCommand(), Channel, Value);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();
//}
//
//void ExecuteSetDacValue(CSerial &serial)
//{  // Analyse parameters:
//  UInt8 Channel = atoi(Command.GetPRxdParameters(0));
//  UInt32 Value = atol(Command.GetPRxdParameters(1));
//  // Execute:
//#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
//  DigitalAnalogConverter.SetValue(Channel, Value);
//  Value = DigitalAnalogConverter.GetValue(Channel);
//#endif  
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %u %lu", 
//          Command.GetPRxdCommand(), Channel, Value);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();
//}
//
//#########################################################
//  Segment - Execution - Helper
//#########################################################
//
//void SetPosition()
//{
//#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
//  DigitalAnalogConverter.SetValue(DAC_MIRRORX, XPositionActual);
//  DigitalAnalogConverter.SetValue(DAC_MIRRORY, YPositionActual);  
//#endif
//}
//
//void SetTrigger()
//{
//  LedLine.On(0x01 << LED_TRIGGER);
//  delayMicroseconds(DelayPulse / 2);
//  LedLine.Off(0x01 << LED_MOTIONY);
//  LedLine.Off(0x01 << LED_MOTIONX);
//  delayMicroseconds(DelayPulse / 2);
//  LedLine.Off(0x01 << LED_TRIGGER);
//}
//
//void TriggerLaserPulses()
//{ // Pulsedelay fixed to 1000us!
//  LedLine.Off(0x01 << LED_MOTIONY);
//  LedLine.Off(0x01 << LED_MOTIONX);
//  // wait for motion stops...
//  delayMicroseconds(DelayMotion);
//  //
//  UInt32 TI;
//  UInt32 DL = DelayPulse / 10;
//  UInt32 DH = 9* DL;
//  for (TI = 0; TI < CountLaserPulses; TI++)
//  {    
//    LedLine.On(0x01 << LED_TRIGGER);
//    delayMicroseconds(DH);
//    LedLine.Off(0x01 << LED_TRIGGER);
//    delayMicroseconds(DL);
//  }
//}
//
//#########################################################
//  Segment - Execution - Automation
//#########################################################
//
//void ExecuteSetMotionX(CSerial &serial)
//{ // Analyse parameters:
//  XPositionMinimum = abs(atoi(Command.GetPRxdParameters(0)));
//  XPositionMaximum = abs(atoi(Command.GetPRxdParameters(1)));
//  XPositionDelta = abs(atoi(Command.GetPRxdParameters(2)));
//  // Execute -
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %lu %lu %lu", 
//          Command.GetPRxdCommand(), XPositionMinimum, XPositionMaximum, XPositionDelta);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//void ExecuteSetMotionY(CSerial &serial)
//{ // Analyse parameters:
//  YPositionMinimum = abs(atoi(Command.GetPRxdParameters(0)));
//  YPositionMaximum = abs(atoi(Command.GetPRxdParameters(1)));
//  YPositionDelta = abs(atoi(Command.GetPRxdParameters(2)));
//  // Execute -
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %lu %lu %lu", 
//          Command.GetPRxdCommand(), YPositionMinimum, YPositionMaximum, YPositionDelta);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//void ExecuteSetDelayMotion(CSerial &serial)
//{ // Analyse parameters:
//  DelayMotion = abs(atoi(Command.GetPRxdParameters(0)));
//  // Execute -
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %lu", 
//          Command.GetPRxdCommand(), DelayMotion);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//void ExecuteSetDelayPulse(CSerial &serial)
//{ // Analyse parameters:
//  DelayPulse = abs(atoi(Command.GetPRxdParameters(0)));
//  // Execute -
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %lu", 
//          Command.GetPRxdCommand(), DelayPulse);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//bool AbortTrigger()
//{
//  StateAutomation = saIdle;
//  LedLine.Off(0x01 << LED_MOTIONY);
//  LedLine.Off(0x01 << LED_MOTIONX);
//  LedLine.Off(0x01 << LED_BUSY);
//  return true;
//}
//
//void ExecuteAbortTrigger(CSerial &serial)
//{ // Analyse parameters -
//  // Execute:
//  AbortTrigger();
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ":ABT");
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//Boolean StartTrigger()
//{
//  if (0 < CountRepetition)
//  {
//    LedLine.On(0x01 << LED_MOTIONY);
//    LedLine.On(0x01 << LED_MOTIONX);
//    LedLine.On(0x01 << LED_BUSY);
//    XPositionActual = XPositionMinimum;
//    YPositionActual = YPositionMinimum;
//    StateAutomation = saTriggerRowColLeft;
//    return true;
//  }
//  return false;
//}
//
//void ExecuteStartTrigger(CSerial &serial)
//{ // Analyse parameters:
//  CountLaserPulses = abs(atoi(Command.GetPRxdParameters(0)));
//  CountRepetition = abs(atoi(Command.GetPRxdParameters(1)));
//  // Execute:
//  StartTrigger();
//  // Response:
//  sprintf(Command.GetTxdBuffer(), ": %s %lu %lu", 
//          Command.GetPRxdCommand(), CountLaserPulses, CountRepetition);
//  serial.WriteLine(Command.GetTxdBuffer());
//  serial.WritePrompt();  
//}
//
//#########################################################
//  Segment - Handler - Error
//#########################################################
//
void HandleError(CSerial &serial)
{
  if (ecNone != GlobalErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(GlobalErrorCode, Command.GetTxdBuffer()); 
    serial.WriteText(Command.GetTxdBuffer());
    serial.WriteText("]: ");
    switch (GlobalErrorCode)
    {
      case ecNone:
        serial.WriteText("No Error");
        break;
      case ecUnknown:
        serial.WriteText("Unknown");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
      case ecToManyParameters:
        serial.WriteText("To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.WriteText("Missing Target Parameter");
        break;              
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    GlobalErrorCode = ecNone;
  }
}
//
//#########################################################
//  Segment - Automation
//#########################################################
//
//void HandleAutomationIdle(CSerial &serial)
//{  
//  // nothing to do
//}
//
//void HandleAutomationPulseLedLaser(CSerial &serial)
//{
//  if (lsOn == LedLaser.GetState())
//  {
//    LedLaser.Off();
//  }
//  else
//    if (lsOff == LedLaser.GetState())
//    {
//    
//    }
//    else
//    {
//      
//    }
//}

//void HandleAutomationMotionRowColLeft(CSerial &serial)
//{
//  LedLine.On(0x01 << LED_MOTIONX);
//  LedLine.On(0x01 << LED_MOTIONY);
//  //
//  SetPosition();
//  SetTrigger();
//  //
//  XPositionActual += XPositionDelta;
//  if (XPositionMaximum < XPositionActual)
//  {
//    XPositionActual = XPositionMinimum;
//    YPositionActual += YPositionDelta;
//    if (YPositionMaximum < YPositionActual)
//    { // Finish Area scanning
//      XPositionActual = XPositionMinimum;
//      YPositionActual = YPositionMinimum;
//      CountRepetition--;
//      LedLine.Toggle(0x01 << LED_BUSY);
//      if (0 == CountRepetition)
//      { // ends...
//        // SetPosition();
//        ExecuteAbortTrigger(serial);
//        return;
//      }
//    } 
//  } 
//}

//void HandleAutomationTriggerRowColLeft(CSerial &serial)
//{
//  
//  LedLine.On(0x01 << LED_MOTIONX);
//  LedLine.On(0x01 << LED_MOTIONY);
//  //
//  SetPosition();
//  TriggerLaserPulses();
//  //
//  XPositionActual += XPositionDelta;
//  if (XPositionMaximum < XPositionActual)
//  {
//    XPositionActual = XPositionMinimum;
//    YPositionActual += YPositionDelta;
//    if (YPositionMaximum < YPositionActual)
//    { // Finish Area scanning
//      XPositionActual = XPositionMinimum;
//      YPositionActual = YPositionMinimum;
//      CountRepetition--;      
//      LedLine.Toggle(0x01 << LED_BUSY);
//      if (0 == CountRepetition)
//      { // ends...
//        // SetPosition();
//        ExecuteAbortTrigger(serial);
//        return;
//      }
//    } 
//  } 
//}
//
//void HandleAutomation(CSerial &serial)
//{
//  switch (StateAutomation)
//  {
//    case saIdle:
//      HandleAutomationIdle(serial);
//      break;
//    case saIdle:
//      HandleAutomationPulseLedLaser(serial);
//      break;
////    case saMotionRowColLeft:
////      HandleAutomationMotionRowColLeft(serial);
////      break;
////    case saMotionRowColBoth:
////      break;
////    case saTriggerRowColLeft:
////      HandleAutomationTriggerRowColLeft(serial);
////      break;
////    case saTriggerRowColBoth:
////      break;
//  }
//}







Boolean ExecuteRxdCommand(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
// debug serialtarget.WriteLine(TxdBuffer);
  if (!strcmp("H", Command.GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp("GPH", Command.GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp("GSV", Command.GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp("GHV", Command.GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  // ----------------------------------
  // LedLaser
  // ---------------------------------- 
  if (!strcmp("GLL", Command.GetPRxdCommand()))
  {
    ExecuteGetLedLaser(serial);
    return true;
  } else 
  if (!strcmp("LLH", Command.GetPRxdCommand()))
  {
    ExecuteSwitchLedLaserOn(serial);
    return true;
  } else   
  if (!strcmp("LLL", Command.GetPRxdCommand()))
  {
    ExecuteSwitchLedLaserOff(serial);
    return true;
  } else   
  if (!strcmp("PLL", Command.GetPRxdCommand()))
  {
    ExecutePulseLedLaser(serial);
    return true;
  } else   
//  // ----------------------------------
//  // Dac
//  // ----------------------------------
//  if (!strcmp("GDV", Command.GetPRxdCommand()))
//  {
//    ExecuteGetDacValue(serial);
//    return true;
//  } else   
//  if (!strcmp("SDV", Command.GetPRxdCommand()))
//  {
//    ExecuteSetDacValue(serial);
//    return true;
//  } else   
//  // ----------------------------------
//  // Automation
//  // ----------------------------------
//  if (!strcmp("SMX", Command.GetPRxdCommand()))
//  {
//    ExecuteSetMotionX(serial);
//    return true;
//  } else   
//  if (!strcmp("SMY", Command.GetPRxdCommand()))
//  {
//    ExecuteSetMotionY(serial);
//    return true;
//  } else   
//  if (!strcmp("SDM", Command.GetPRxdCommand()))
//  {
//    ExecuteSetDelayMotion(serial);
//    return true;
//  } else   
//  if (!strcmp("SDP", Command.GetPRxdCommand()))
//  {
//    ExecuteSetDelayPulse(serial);
//    return true;
//  } else   
//  if (!strcmp("STT", Command.GetPRxdCommand()))
//  {
//    ExecuteStartTrigger(serial);
//    return true;
//  } else   
//  if (!strcmp("ABT", Command.GetPRxdCommand()))
//  {
//    ExecuteAbortTrigger(serial);
//    return true;
//  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    GlobalErrorCode = ecInvalidCommand;
  }
  return false;  
}



void HandleCommands(CSerial &serial)
{
  if (Command.AnalyseRxdCommand(serial))
  {
    ExecuteRxdCommand(serial);
  }  
}




