#ifndef Serial_h
#define Serial_h
//
#include "Arduino.h"
#include "Defines.h"
#include "HardwareSerial.h"
//#include "SerialUsbApi.h"
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//
#define RXDECHO_ON  true
#define RXDECHO_OFF false
//
#define CR   0x0D
#define LF   0x0A
#define ZERO 0x00
//
#define PROMPT_NEWLINE "\r\n"
#define PROMPT_INPUT   ">"
#define PROMPT_ANSWER  "#"
//
#define SIZE_TXDBUFFER      64
#define SIZE_RXDBUFFER      64
#define SIZE_RXDPARAMETER   8
#define COUNT_RXDPARAMETERS 3
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
class CSerialBase
{
  protected:
  bool FIsOpen = false;
  bool FRxdEcho = false;
  
  public:
  virtual bool Open(int baudrate, bool rxdecho) = 0;
  virtual bool Close() = 0;
  void SetRxdEcho(bool rxdecho);
  virtual void WriteCharacter(char character) = 0;
  virtual void WriteText(char *text) = 0;
  virtual void WriteText(String text) = 0;
  virtual void WriteLine(char *text) = 0;
  virtual void WriteLine(String text) = 0;
  virtual int GetRxdByteCount() = 0;
  virtual char ReadCharacter() = 0;
  virtual void WriteTime() = 0;
  //
  void WriteNewLine();
  void WritePrompt();
  void WriteAnswer();
  //
  String BuildTime(long unsigned milliseconds);
};
//
//----------------------------------------------------
//  Segment - CSerialH (^=HardwareSerial)
//----------------------------------------------------
//
#if ((defined PROCESSOR_STM32F103C8) || (defined PROCESSOR_TEENSY36) || (defined PROCESSOR_ARDUINODUE))
class CSerialH : CSerialBase
{
  protected:
  HardwareSerial* FPSerial;
  
  public:
  CSerialH(HardwareSerial *serial)
  {
    FPSerial = serial;
  } 

  bool Open(int baudrate, bool rxdecho);
  bool Close();
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteText(String text);
  void WriteLine(char *text);
  void WriteLine(String text);
  void WriteTime();
  int GetRxdByteCount();
  char ReadCharacter();
};
#endif
//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
class CSerial
{
  protected:
  CSerialBase* FPSerial;
  
  public:
#if (defined PROCESSOR_ARDUINOUNO)
#elif (defined PROCESSOR_ARDUINOMEGA)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_ARDUINODUE)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_STM32F103C8)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_TEENSY36)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#else
#error Undefined Processor-Type!
#endif
  bool Open(int baudrate, bool rxdecho);
  bool Close();
  void SetRxdEcho(bool rxdecho);
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteText(String text);
  void WriteLine(char *text);
  void WriteLine(String text);
  void WriteNewLine();
  void WriteAnswer();
  void WritePrompt();
  void WriteTime();
  int GetRxdByteCount();
  char ReadCharacter();
};
//
#endif
