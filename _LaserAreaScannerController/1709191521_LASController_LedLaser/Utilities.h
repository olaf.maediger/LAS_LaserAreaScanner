#ifndef Utilities_h
#define Utilities_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
#include "Command.h"
//
//--------------------------------
//  Section - Constant - Hardware
//--------------------------------
//
// Single LED #define LED_SYSTEM 
// LedLine
#define LED_ERROR   0
#define LED_NC1     1
#define LED_BUSY    2
#define LED_NC3     3
#define LED_MOTIONX 4
#define LED_MOTIONY 5
#define LED_NC6     6
#define LED_TRIGGER 7
//
#define ADC_POSITIONX 0
#define ADC_POSITIONY 1
//
#define DAC_MIRRORX 0
#define DAC_MIRRORY 1
//
#define PWM_FOCUSZ 0
//
#define XPOSITION_CENTER (UInt16)2048
#define YPOSITION_CENTER (UInt16)2048
//
#define DELAY_TRIGGER_1MS (UInt32)1000
//
// Init-Values for Globals:
#define INIT_GLOBALERRORCODE ecNone
#define INIT_STATEAUTOMATION saIdle
#define INIT_COUNTREPETITION 1
#define INIT_COUNTLASERPULSES 1000
// Stabilization of Position after Moving [us]
#define INIT_DELAYMOTION 500
// Laser-Pulse-Period [us] 1ms -> 1KHz
#define INIT_DELAYPULSE 1000
// Default 6 Positions in X
#define INIT_XPOSITIONACTUAL 2048
#define INIT_XPOSITIONDELTA 1023
#define INIT_XPOSITIONMINIMUM 0
#define INIT_XPOSITIONMAXIMUM 4095
// Default 6 Positions in Y (total 36 Positions)
#define INIT_YPOSITIONACTUAL 2048
#define INIT_YPOSITIONDELTA 1023
#define INIT_YPOSITIONMINIMUM 0
#define INIT_YPOSITIONMAXIMUM 4095
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
#define HELP_SOFTWAREVERSION  " Software-Version LAS05V02"
//
const int COUNT_ANSWERLINE = 1;
//
const int COUNT_HEADERLINES = 12;
//
#define TITEL_LINE            "------------------------------------------"
#define TITEL_PROJECT         "- Project:   LaserAreaScanner-Controller -"
#define TITEL_VERSION         "- Version:   05V01                       -"
#ifdef PROCESSOR_ARDUINOUNO
#define TITEL_HARDWARE        "- Hardware:  ArduinoUnoR3                -"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoUnoR3"
#endif
#ifdef PROCESSOR_ARDUINOMEGA 
#define TITEL_HARDWARE        "- Hardware:  ArduinoMega328              -"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoMega328"
#endif
#ifdef PROCESSOR_ARDUINODUE
#define TITEL_HARDWARE        "- Hardware:  ArduinoDue3                 -"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoDue3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define TITEL_HARDWARE        "- Hardware:  Stm32F103C8                 -"
#define HELP_HARDWAREVERSION  " Hardware-Version Stm32F103C8"
#endif
#ifdef PROCESSOR_TEENSY36 
#define TITEL_HARDWARE        "- Hardware:  Teensy36                    -"
#define HELP_HARDWAREVERSION  " Hardware-Version Teensy36"
#endif
#define TITEL_DATE            "- Date:      170915                      -"
#define TITEL_TIME            "- Time:      1242                        -"
#define TITEL_AUTHOR          "- Author:    OM                          -"
#define TITEL_PORT            "- Port:      SerialCommand (Serial1-USB) -"
#define TITEL_PARAMETER       "- Parameter: 115200, N, 8, 1             -"
//
const int COUNT_HELPLINES = 21;
//
#define HELP_COMMON           " Help (Common):"
//
#define HELP_H                " H   : This Help"
#define HELP_GPH              " GPH <fdl> : Get Program Header"
#define HELP_GSV              " GSV <fdl> : Get Software-Version"
#define HELP_GHV              " GHV <fdl> : Get Hardware-Version"
//
#define HELP_LEDLASER         " Help (LedLaser):"
#define HELP_GLL              " GLL : Get State LedLaser"
#define HELP_LLH              " LLH : Switch LedLaser On"
#define HELP_LLL              " LLL : Switch LedLaser Off"
#define HELP_PLL              " PLL <p> <n> : Pulse LedLaser with <p>eriod{ms} <n>times"
//
#define HELP_DAC              " Help (Dac):"
#define HELP_GDV              " GDV <i> : Get Value from DAConverter with Index<0..1>"
#define HELP_SDV              " SDV <i> <v>: Set Value<0..4095> to DAConverter with Index[0..1]"
//
#define HELP_AUTOMATION       " Help (Automation):"
#define HELP_SMX              " SMX <xmin> <xmax> <dx>: Set Motion-ParameterX[0..4095]"
#define HELP_SMY              " SMY <ymin> <ymax> <dy>: Set Motion-ParameterY[0..4095]"
#define HELP_SDM              " SDM <delay>: Set Delay Motion{us}"
#define HELP_SDP              " SDP <delay>: Set Delay Pulse{us}"
#define HELP_STT              " STT <n> <r> : Start Trigger <n>Pulses with <r>epetitions"
#define HELP_ABT              " ABT : Abort Trigger"
//#define HELP_STM              " STM <d> <r> : Start Motion <d>elaytrigger{us} with <r>epetitions"
//
void WriteProgramHeader(CSerial &serialcommand);
void WriteHelp(CSerial &serialcommand);
//  Handler - Error
void HandleError(CSerial &serial);
//  Handler - SerialPC Communication
void HandleCommands(CSerial &serial);
void HandleAutomation(CSerial &serial);
//
#endif
