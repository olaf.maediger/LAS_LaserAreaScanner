//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "Led.h"
//
CLed::CLed(int pin)
{
  FPin = pin;
  FState = slUndefined;
}

EStateLed CLed::GetState()
{
  return FState;
}

UInt32 CLed::GetPulseCount()
{
  return FPulseCount;  
}

UInt32 CLed::GetPeriod()
{
  return FPeriod;
}

Boolean CLed::Open()
{
  pinMode(FPin, OUTPUT);
  digitalWrite(FPin, LOW);
  FState = slOff;
  return true;
}

Boolean CLed::Close()
{
  pinMode(FPin, INPUT);
  FState = slUndefined;
  return true;
}

void CLed::On()
{
  digitalWrite(FPin, HIGH);
  FState = slOn;
}

void CLed::Off()
{
  digitalWrite(FPin, LOW);
  FState = slOff;
}

void CLed::Pulse(UInt32 period, UInt32 pulsecount)
{
  FPeriod = period;
  FPulseCount = pulsecount;
}


