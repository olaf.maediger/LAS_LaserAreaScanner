#ifndef Utilities_h
#define Utilities_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
//
#define SIZE_PARAMETER 8
#define SIZE_TXDBUFFER 64
#define SIZE_RXDBUFFER 64

#define COUNT_PARAMETER 4
//
void WriteProgramHeader(CSerial &serialcommand);
void WriteHelp(CSerial &serialcommand);
//  Handler - Error
void HandleError(CSerial &serial);
//  Handler - SerialPC Communication
void HandleSerialCommands(CSerial &serialcommand, CSerial &serialtarget);
//
#endif
