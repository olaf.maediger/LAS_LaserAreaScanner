#include "Utilities.h"
#include "Led.h"
//
//
//-------------------------------------------
//  Global Variables 
//-------------------------------------------
//	
enum EErrorCode ErrorCode;
//
Character TxdBuffer[SIZE_TXDBUFFER];
Character RxdBuffer[SIZE_RXDBUFFER];
Int16 RxdBufferIndex = 0;
Character RxdCommandLine[SIZE_RXDBUFFER];
PCharacter PRxdCommand;
PCharacter PRxdTarget;
PCharacter PRxdParameters[SIZE_PARAMETER];
Int16 RxdParameterCount = 0;
//
extern CLed LedSystem;
//
void ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    RxdBuffer[CI] = 0x00;
  }
  RxdBufferIndex = 0;
}

void ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    TxdBuffer[CI] = 0x00;
  }
}

void ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    RxdCommandLine[CI] = 0x00;
  }
}


void WriteProgramHeader(CSerial &serial)
{  
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
  //
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("---------------------------------------");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Project:   DBB - DelayBaseBoard     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Version:   01V01                    -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Hardware:  DBB01V01                 -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Date:      150528                   -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Time:      1555                     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Author:    OM                       -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Port:      SerialPC (SerialUSB)     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Parameter: 115200, N, 8, 1          -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("---------------------------------------");
  serial.WriteNewLine();	
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Software-Version DBB01V01");
  serial.WriteNewLine();
}

void WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Hardware-Version DBB01V02");
  serial.WriteNewLine();
}

void WriteHelp(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Help (Common):");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" H   : This Help");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GPH <fdl> : Get Program Header");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GSV <fdl> : Get Software-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GHV <fdl> : Get Hardware-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Led):");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GLD <fdl> <i> : Get State of Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" SLD <fdl> <i> : Set Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" CLD <fdl> <i> : Clear Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (ChannelA):");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GDA <fdl>         : Get Delay-Data [10ps]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" SDA <fdl> <d>     : Set Delay-Data [10ps]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  /*
  serial.WriteText(" GSA <fdl> <i>     : Get Sweep-Parameter");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" SSA <fdl> <i> <v> : Set Sweep-Parameter");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GWA <fdl>         : Get Sweep-State");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" SWA <fdl> <v>     : Set Sweep-State");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  */
  //
  serial.WriteText(" Help (ChannelB):");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GDB <fdl>         : Get Delay-Data [10ps]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" SDB <fdl> <d>     : Set Delay-Data [10ps]");
  serial.WriteNewLine();
  serial.WriteAnswer();
  /*
  serial.WriteText(" GSB <fdl> <i>     : Get Sweep-Parameter");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" SSB <fdl> <i> <v> : Set Sweep-Parameter");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GWB <fdl>         : Get Sweep-State");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" SWB <fdl> <v>     : Set Sweep-State");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  */
  //
  serial.WriteNewLine();
}


//
//-------------------------------------------
//	*** Common ***
//-------------------------------------------
//
void ExecuteGetHelp(CSerial &serialcommand)
{ // Analyse parameters: -
  // Response
  WriteHelp(serialcommand);
  serialcommand.WritePrompt();
}

void ExecuteGetProgramHeader(CSerial &serialcommand)
{ // Analyse parameters: -
  // Response
  WriteProgramHeader(serialcommand);
  serialcommand.WritePrompt();
}

void ExecuteGetSoftwareVersion(CSerial &serialcommand)
{ // Analyse parameters: -
  // Response
  WriteSoftwareVersion(serialcommand);
  serialcommand.WritePrompt();
}

void ExecuteGetHardwareVersion(CSerial &serialcommand)
{ // Analyse parameters: -
  // Response
  WriteHardwareVersion(serialcommand);
  serialcommand.WritePrompt();
}
//
//-------------------------------------------
//	*** Led ***
//-------------------------------------------
//
/* -> Fdl...
void GetLed(Boolean response, UInt8 ledindex)
{ // Action
  Boolean State = false; // Inverted!
  switch (ledindex)
  {
    case LEDINDEX_BUSY:
			State = LEDBUSY_State();
			break;
		case LEDINDEX_ERROR:
			State = LEDERROR_State();
			break;
	}
	// Response
	if (response)
	{
		WriteAnswer();
		UARTBASE_CPutString(" GLD ");
		Int16ToAsciiDecimal(ledindex, TXDBuffer);
		UARTBASE_PutString(TXDBuffer);
		UARTBASE_CPutString(" ");
		// State inverted!
		if (0 < State)
		{ 
			UARTBASE_CPutString("0");
		} else
		{
			UARTBASE_CPutString("1");
		}
		WriteNewLine();
		WritePrompt();
	}
}

void ClearLed(Boolean response,	
			  UInt8 ledindex)
{	// Action
	switch (ledindex)
	{
		case LEDINDEX_BUSY:
			LEDBUSY_Clear();		
			break;
		case LEDINDEX_ERROR:
			LEDERROR_Clear();		
			break;
	}
	// Response
	if (response)
	{
		WriteAnswer();
		UARTBASE_CPutString(" CLD ");
		Int16ToAsciiDecimal(ledindex, TXDBuffer);
		UARTBASE_PutString(TXDBuffer);
		WriteNewLine();
		WritePrompt();
	}
}

void SetLed(Boolean response,	
		  	UInt8 ledindex)
{	// Action
	switch (ledindex)
	{
		case LEDINDEX_BUSY:
			LEDBUSY_Set();		
			break;
		case LEDINDEX_ERROR:
			LEDERROR_Set();		
			break;
	}
	// Response
	if (response)
	{
		WriteAnswer();
		UARTBASE_CPutString(" SLD ");
		Int16ToAsciiDecimal(ledindex, TXDBuffer);
		UARTBASE_PutString(TXDBuffer);
		WriteNewLine();
		WritePrompt();
	}
}
*/
void ExecuteGetLed(CSerial &serialcommand, CSerial &serialtarget)
{	
  //GetLed(true, atoi(RXDParameter[0]));
}

void ExecuteClearLed(CSerial &serialcommand, CSerial &serialtarget)
{	
  //ClearLed(true, atoi(RXDParameter[0]));
}

void ExecuteSetLed(CSerial &serialcommand, CSerial &serialtarget)
{	
  //SetLed(true, atoi(RXDParameter[0]));
  sprintf(TxdBuffer, "%s %s %s", PRxdCommand, PRxdParameters[0], PRxdParameters[1]);
  serialtarget.WriteLine(TxdBuffer);
}
//
//
//----------------------------------------------
//  Segment - Handler - Error
//----------------------------------------------
//
void HandleError(CSerial &serial)
{
  if (ecNone != ErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(ErrorCode, TxdBuffer);	
    serial.WriteText(TxdBuffer);
    serial.WriteText("]: ");
    switch (ErrorCode)
    {
      case ecNone:
        serial.WriteText("No Error");
        break;
      case ecUnknown:
        serial.WriteText("Unknown");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
      case ecToManyParameters:
        serial.WriteText("To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.WriteText("Missing Target Parameter");
        break;
        
        
  /*  case ecInvalid...:
        SERIAL_PC.write("Invalid ...");
        break; */
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    ErrorCode = ecNone;
  }
}
//
//----------------------------------------------
//  Segment - Handler - SerialPC Communication
//----------------------------------------------
//
Boolean DetectRxdCommandLine(CSerial &serialcommand)
{
  while (0 < serialcommand.GetRxdByteCount())
  {
    Character C = serialcommand.ReadCharacter();
    // debug serial.WriteText(".");
    // debug serial.WriteCharacter(C);
    // debug serial.WriteText(".");
    // debug delay(1000);
    switch (C)
    {
      case CARRIAGE_RETURN:
        RxdBuffer[RxdBufferIndex] = ZERO;
        RxdBufferIndex = 0; // restart
        strupr(RxdBuffer);
        strcpy(RxdCommandLine, RxdBuffer);
        // debug serial.WriteText("<CR>true");
        // debug delay(1000);
        return true;
      case LINE_FEED: // ignore
        // debug serial.WriteText("<LF>");
        // debug delay(1000);
        break;
      default: 
        // debug serial.WriteText("<");
        // debug serial.WriteCharacter(C);
        // debug serial.WriteText(">");
        RxdBuffer[RxdBufferIndex] = C;
        RxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean AnalyseRxdCommand(CSerial &serialcommand, CSerial &serialtarget)
{
  if (DetectRxdCommandLine(serialcommand))
  {
    // debug serialtarget.WriteLine("AnalyseRxdCommand:");
    // debug serialtarget.WriteText("RxdCommandLine[");
    // debug serialtarget.WriteText(RxdCommandLine);
   // debug serialtarget.WriteLine("]");    
    
    char *PTerminal = " \t\r\n";
    char *PRxdCommandLine = RxdCommandLine;
    PRxdCommand = strtok(RxdCommandLine, PTerminal);
    if (PRxdCommand)
    {
      // debug sprintf(TxdBuffer, "PRxdCommand[%s]", PRxdCommand);
      // debug serialtarget.WriteLine(TxdBuffer);

      PRxdTarget = strtok(0, PTerminal);
      if (PRxdTarget)
      {      
        // debug sprintf(TxdBuffer, "PRxdTarget[%s]", PRxdTarget);
        // debug serialtarget.WriteLine(TxdBuffer);

        RxdParameterCount = 0;
        char *PRxdParameter;
        while (PRxdParameter = strtok(0, PTerminal))
        {
          // debug sprintf(TxdBuffer, "RxdParameter[%i, %s]", RxdParameterCount, PRxdParameter);
          // debug serialtarget.WriteLine(TxdBuffer);
          
          PRxdParameters[RxdParameterCount] = PRxdParameter;
          RxdParameterCount++;
          if (COUNT_PARAMETER <= RxdParameterCount)
          {
            ErrorCode = ecToManyParameters;
            ZeroRxdBuffer();
            // debug serialtarget.WriteLine("error[ecToManyParameters]");
            serialcommand.WriteNewLine();
            serialcommand.WritePrompt();
            return false;
          }
        }  
        // debug       
        sprintf(TxdBuffer, "\n\r# RxdCommand=%s RxdTarget=%s", PRxdCommand, PRxdTarget);
        serialtarget.WriteText(TxdBuffer);
        int II;
        for (II = 0; II < RxdParameterCount; II++)
        {
          sprintf(TxdBuffer, " RxdParameters[%i]=%s", II, PRxdParameters[II]);
          serialtarget.WriteText(TxdBuffer);
        }
        serialtarget.WriteLine("");
        //
        ZeroRxdBuffer();
        serialcommand.WriteNewLine();
        return true;
      }
      if (!strcmp("H", PRxdCommand))
      {
        ZeroRxdBuffer();
        serialcommand.WriteNewLine();
        return true;
      }
      ErrorCode = ecMissingTargetParameter;
      // debug serialtarget.WriteLine("error[ecMissingTargetParameter]");
    }
    ZeroRxdBuffer();
    serialcommand.WriteNewLine();
    serialcommand.WritePrompt();
  }
  return false;
}

Boolean ExecuteRxdCommand(CSerial &serialcommand, CSerial &serialtarget)
{ 
  sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
  serialtarget.WriteLine(TxdBuffer);
  if (!strcmp("H", PRxdCommand))
  { // debug
    LedSystem.Toggle();
    ExecuteGetHelp(serialcommand);
    return true;
  } else 
  if (!strcmp("GPH", PRxdCommand))
  {
    ExecuteGetProgramHeader(serialcommand);
    return true;
  } else 
  // ----------------------------------
  // Led
  // ----------------------------------
  if (!strcmp("GLD", PRxdCommand))
  {
    ExecuteGetLed(serialcommand, serialtarget);
    return true;
  } else 	
  if (!strcmp("SLD", PRxdCommand))
  {
    ExecuteSetLed(serialcommand, serialtarget);
    return true;
  } else 	
  if (!strcmp("CLD", PRxdCommand))
  {
    ExecuteClearLed(serialcommand, serialtarget);
    return true;
  } else 	
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    ErrorCode = ecInvalidCommand;
  }
  return false;  
}

void HandleSerialCommands(CSerial &serialcommand, CSerial &serialtarget)
{
  if (AnalyseRxdCommand(serialcommand, serialtarget))
  {
    ExecuteRxdCommand(serialcommand, serialtarget);
  }  
}







  /*
  if (UARTBASE_bCmdCheck())
  {	
    RXDCommand = UARTBASE_szGetParam();
    if (0 < strlen(RXDCommand))
    {
      strupr(RXDCommand);
      RXDParameterCount = 0;
      while (RXDParameter[RXDParameterCount] = UARTBASE_szGetParam())
      {
        RXDParameterCount++;
      }							
      return TRUE;
    }
    return FALSE;
    }
  return FALSE;*/
  /* / ----------------------------------
  // Help (Common)
  // ----------------------------------
  if (0 == RXDCommand)
  {
    UARTBASE_CmdReset();
    WritePrompt();
    return FALSE;
  }
  //
  if (!cstrcmp("H", RXDCommand))
  {
    ExecuteGetHelp();
    UARTBASE_CmdReset();
    return TRUE;
  } else 
  if (!cstrcmp("GPH", RXDCommand))
  {
    ExecuteGetProgramHeader();
    UARTBASE_CmdReset();
    return TRUE;
  } else 
  if (!cstrcmp("GSV", RXDCommand))
  {
    ExecuteGetSoftwareVersion();
    UARTBASE_CmdReset();
    return TRUE;
  }*/ /* else 
	if (!cstrcmp("GHV", RXDCommand))
	{
		ExecuteGetHardwareVersion();
		UARTBASE_CmdReset();
		return TRUE;
	} else 
	// ----------------------------------
	// Led
	// ----------------------------------
	if (!cstrcmp("GLD", RXDCommand))
	{
		ExecuteGetLed();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SLD", RXDCommand))
	{
		ExecuteSetLed();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("CLD", RXDCommand))
	{
		ExecuteClearLed();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	// ----------------------------------
	// DelayChannelA
	// ----------------------------------
	if (!cstrcmp("GDA", RXDCommand))
	{
		ExecuteGetDelayChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SDA", RXDCommand))
	{
		ExecuteSetDelayChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("GSA", RXDCommand))
	{
		ExecuteGetSweepParameterChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SSA", RXDCommand))
	{
		ExecuteSetSweepParameterChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("GWA", RXDCommand))
	{
		ExecuteGetSweepStateChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SWA", RXDCommand))
	{
		ExecuteSetSweepStateChannelA();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	// ----------------------------------
	// DelayChannelB
	// ----------------------------------
	if (!cstrcmp("GDB", RXDCommand))
	{
		ExecuteGetDelayChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SDB", RXDCommand))
	{
		ExecuteSetDelayChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("GSB", RXDCommand))
	{
		ExecuteGetSweepParameterChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SSB", RXDCommand))
	{
		ExecuteSetSweepParameterChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("GWB", RXDCommand))
	{
		ExecuteGetSweepStateChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	if (!cstrcmp("SWB", RXDCommand))
	{
		ExecuteSetSweepStateChannelB();
		UARTBASE_CmdReset();
		return TRUE;
	} else 	
	// ----------------------------------
	// Error-Handler
	// ----------------------------------
	{
		GlobalErrorCode = ecInvalidCommand;
	}
	UARTBASE_CmdReset();
	return FALSE;  
  }

  return false;
}*/

