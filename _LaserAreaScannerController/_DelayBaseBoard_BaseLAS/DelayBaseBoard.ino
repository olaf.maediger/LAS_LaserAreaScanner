#include "Defines.h"
#include "Helper.h"
#include "Led.h"
#include "Serial.h"
#include "Utilities.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
CSerial SerialPC(Serial1);
//CSerial SerialXXX(Serial2);
//CSerial SerialYYY(Serial3);
//CSerial SerialVVV(SerialUSB);
//CSerial SerialUUU(Serial);
//
void setup() 
{
  LedSystem.Open();
  SerialPC.Open();
  //
  int I = 0;
  for (I = 0; I < 20; I++)
  {
    LedSystem.Toggle();
    delay(300);
  }
  LedSystem.Off();
  //
  WriteProgramHeader(SerialPC);
  WriteHelp(SerialPC);
  SerialPC.WritePrompt();
  //
  // debug GlobalErrorCode = ecInvalidCommand;
}

void loop() 
{
  //
 //!!!! HandleSerialCommands(SerialPC, SerialFDL1);
//!!!!  HandleError(SerialPC);  
  //
  LedSystem.Toggle();
  delay(1000);
  // debug 
  //Serial.WriteText("A");
}
