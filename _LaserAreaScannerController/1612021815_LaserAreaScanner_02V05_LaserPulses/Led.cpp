//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "Led.h"
//
CLed::CLed(int pin)
{
  FPin = pin;
  FState = slUndefined;
}

Boolean CLed::Open()
{
  pinMode(FPin, OUTPUT);
  digitalWrite(FPin, LOW);
  FState = slOff;
  return true;
}

Boolean CLed::Close()
{
  pinMode(FPin, INPUT);
  FState = slUndefined;
  return true;
}

EStateLed CLed::GetState()
{
  return FState;
}

Boolean CLed::On()
{
  digitalWrite(FPin, HIGH);
  FState = slOn;
  return true;
}

Boolean CLed::Off()
{
  digitalWrite(FPin, LOW);
  FState = slOff;
  return true;
}

Boolean CLed::Toggle()
{
  if (slOff == FState)
  {
    digitalWrite(FPin, HIGH);
    FState = slOn;
    return true;
  } 
  if (slOn == FState)
  {
    digitalWrite(FPin, LOW);
    FState = slOff;
  } 
  return false;
}

Boolean CLed::Pulse(UInt32 microseconds)
{
  digitalWrite(FPin, HIGH);
  FState = slOn;
  delayMicroseconds(microseconds);  
  digitalWrite(FPin, LOW);
  FState = slOff;
}


