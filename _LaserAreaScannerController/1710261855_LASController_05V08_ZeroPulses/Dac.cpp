//
//--------------------------------
//  Library Dac
//--------------------------------
//
#include "Dac.h"
//
//########################################################
#if defined(PROCESSOR_ARDUINOMEGA)
//
CDac::CDac(bool channel0, bool channel1)
{
  FChannel[0] = channel0;
  FChannel[1] = channel1;
  FValue[0] = 0;
  FValue[1] = 0;
}

Boolean CDac::Open()
{
  analogWriteResolution(12);
  return true;
}

Boolean CDac::Close()
{
  return true;
}

UInt32 CDac::GetValue(UInt8 channel)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
      case 0:
        return FValue[0];
      case 1:
        return FValue[1];
    }    
  }
  return 0;
}

void CDac::SetValue(UInt8 channel, UInt32 value)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
      case 0:
        FValue[0] = (0x0FFF & value);
        analogWrite(PIN_DAC0, value);
        break;
      case 1:
        FValue[1] = (0x0FFF & value);
        analogWrite(PIN_DAC1, value);
        break;
    }    
  }
}
//
#endif // PROCESSOR_ARDUINOMEGA
//
//########################################################
#if defined(PROCESSOR_ARDUINODUE)
//
CDac::CDac(bool channel0, bool channel1)
{
  FChannel[0] = channel0;
  FChannel[1] = channel1;
  FValue[0] = 0;
  FValue[1] = 0;
}

Boolean CDac::Open()
{
  //!!!!!!!!!!!!!!!!!analogWriteResolution(12);
  return true;
}

Boolean CDac::Close()
{
  return true;
}

UInt32 CDac::GetValue(UInt8 channel)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
      case 0:
        return FValue[0];
      case 1:
        return FValue[1];
    }    
  }
  return 0;
}

void CDac::SetValue(UInt8 channel, UInt32 value)
{
  if (FChannel[channel])
  {
    switch (channel)
    {
      case 0:
        FValue[0] = (0x0FFF & value);
        analogWrite(PIN_DAC0, value);
        break;
      case 1:
        FValue[1] = (0x0FFF & value);
        analogWrite(PIN_DAC1, value);
        break;
    }    
  }
}
//
#endif // PROCESSOR_ARDUINODUE
