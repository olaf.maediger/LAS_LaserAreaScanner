#ifndef Automation_h
#define Automation_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
//
enum EStateAutomation
{ // Base
  saUndefined = -1,
  saIdle = 0,
  // LedLaser
  saLedLaserPulseBegin = 1,
  saLedLaserPulseBusy = 2,
  saLedLaserPulseEnd = 3,
  // PositionLaser
  saPulsePositionLaserBegin = 4,
  saPulsePositionLaserBusy = 5,
  saPulsePositionLaserEnd = 6,
  // MatrixLaser
  saPulseMatrixLaserBegin = 7,
  saPulseMatrixLaserBusy = 8,
  saPulseMatrixLaserAbort = 9,
  saPulseMatrixLaserEnd = 10,
  // VariableLaser
  saPulseVariableLaserInit = 11,
  saPulseVariableLaserWait = 12,
  saPulseVariableLaserBegin = 13,
  saPulseVariableLaserBusy = 14,
  saPulseVariableLaserAbortEnd = 15
};
//
class CAutomation
{ // Field:
  private:
  EStateAutomation FState;
  UInt32 FTimeStamp;
  UInt32 FTimeMarker;
  //
  public:
  // Constructor
  CAutomation();
  // Property
  EStateAutomation GetState();
  void SetState(EStateAutomation state);
  // First: TimeStamp!!!
  inline void SetTimeStamp()
  {
    FTimeStamp = millis();
    FTimeMarker = FTimeStamp;
  }
  // Later: TimeMarker!!!
  inline void SetTimeMarker()
  {
    FTimeMarker = millis();
  }
  inline UInt32 GetTimePeriod()
  {
    return FTimeMarker - FTimeStamp;
  }
  // Management
  Boolean Open();
  Boolean Close();
  // Idle
  void HandleIdle(CSerial &serial);
  // LedLaser
  void HandleLedLaserPulseBegin(CSerial &serial);
  void HandleLedLaserPulseBusy(CSerial &serial);
  void HandleLedLaserPulseEnd(CSerial &serial);
  // PositionLaser
  void HandlePulsePositionLaserBegin(CSerial &serial);
  void HandlePulsePositionLaserBusy(CSerial &serial);
  void HandlePulsePositionLaserEnd(CSerial &serial);
  // MatrixLaser
  void HandlePulseMatrixLaserBegin(CSerial &serial);
  void HandlePulseMatrixLaserBusy(CSerial &serial);
  void HandlePulseMatrixLaserAbort(CSerial &serial);
  void HandlePulseMatrixLaserEnd(CSerial &serial);
  // VariableLaser
  void HandlePulseVariableLaserInit(CSerial &serial);
  void HandlePulseVariableLaserWait(CSerial &serial);
  void HandlePulseVariableLaserBegin(CSerial &serial);
  void HandlePulseVariableLaserBusy(CSerial &serial);
  void HandlePulseVariableLaserAbortEnd(CSerial &serial);
  // Collector
  void Handle(CSerial &serial);
};
//
#endif // Automation_h
