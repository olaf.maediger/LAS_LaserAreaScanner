//
//--------------------------------
//  Library Pwm
//--------------------------------
//
#ifndef Pwm_h
#define Pwm_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Pwm
//--------------------------------
//
class CPwm
{
  private:
  bool FChannel[12];
  UInt16 FValue[12];
  
  public:
  CPwm(bool channel0, bool channel1, bool channel2, bool channel3);
  CPwm(bool channel0, bool channel1, bool channel2, bool channel3,
       bool channel4, bool channel5, bool channel6, bool channel7);
  CPwm(bool channel0, bool channel1, bool channel2, bool channel3,
       bool channel4, bool channel5, bool channel6, bool channel7,
       bool channel8, bool channel9, bool channel10, bool channel11);
  Boolean Open();
  Boolean Close();
  UInt16 GetValue(UInt8 channel);
  void SetValue(UInt8 channel, UInt16 value);
};
//
#endif
