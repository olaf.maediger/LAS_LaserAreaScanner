//
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Led.h"
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  #include "DacInternal.h"
#endif
#if (defined(DAC_MCP4725_X) || defined(DAC_MCP4725_Y)) 
  #include "DacMcp4725.h"
#endif
#include "Trigger.h"
//
//-------------------------------------------
//  External Global Variables 
//-------------------------------------------
//  
extern CSerial SerialCommand;
extern CError LASError;
extern CProcess LASProcess;
extern CCommand LASCommand;
extern CLed LedSystem;
extern CLed LedLaser;
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  extern CDacInternal DacInternal_XY;
#endif  
#ifdef DAC_MCP4725_X
extern CDacMcp4725 DacMcp4725_X;
#endif  
#ifdef DAC_MCP4725_Y 
extern CDacMcp4725 DacMcp4725_Y;
#endif  
extern CTrigger TriggerIn;
extern CTrigger TriggerOut;
//
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand()
{
  Init();
}

CCommand::~CCommand()
{
  Init();
}
//
//#########################################################
//  Segment - Helper
//#########################################################
//
void CCommand::ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}

void CCommand::ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}

void CCommand::ZeroCommandText()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FCommandText[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Management
//#########################################################
//
void CCommand::CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate)
{
//  switch (stateprocess)
//  { // Base
//    case spUndefined:
//      break; 
//    case spIdle:
//      break; 
//    default:
//      break;
//  }  
//  serial.WriteNewLine();
//  serial.WritePrompt();
  sprintf(LASCommand.GetTxdBuffer(), MASK_STATEPROCESS, PROMPT_EVENT, stateprocess, substate);
  serial.WriteLine(LASCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::Init()
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroCommandText();
}

Boolean CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CR:
        FRxdBuffer[FRxdBufferIndex] = ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FCommandText, FRxdBuffer);
        return true;
      case LF: // ignore
        break;
      default: 
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean CCommand::DetectSDCardLine(CSDCard &sdcard)
{
  if (spIdle == LASProcess.GetState())
  {
    if (LASProcess.IsExecutingCommandFile())
    {
      if (sdcard.IsOpenForReading())
      {
        String Line = sdcard.ReadTextLine();
        if (0 < strlen(FCommandText))
        {
          strcpy(FCommandText, Line.c_str()); 
          return true;      
        }
        else
        { 
          LASProcess.StopExecutionCommandFile();
        }
      }
    }
  }
  return false;
}

Boolean CCommand::AnalyseCommandText(CSerial &serial)
{
  char *PTerminal = (char*)" \t\r\n";
  char *PCommandText = FCommandText;
  FPCommand = strtok(FCommandText, PTerminal);
  if (FPCommand)
  {
    FParameterCount = 0;
    char *PParameter;
    while (PParameter = strtok(0, PTerminal))
    {
      FPParameters[FParameterCount] = PParameter;
      FParameterCount++;
      if (COUNT_TEXTPARAMETERS < FParameterCount)
      {
        LASError.SetCode(ecToManyParameters);
        ZeroRxdBuffer();
        serial.WriteNewLine();
        serial.WritePrompt();
        return false;
      }
    }  
    ZeroRxdBuffer();
    serial.WriteNewLine();
    return true;
  }
  ZeroRxdBuffer();
  serial.WriteNewLine();
  serial.WritePrompt();    
  return false;
}

Boolean CCommand::Handle(CSerial &serial, CSDCard &sdcard)
{
  if (DetectRxdLine(serial))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);     
    }
  }
  if (DetectSDCardLine(sdcard))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);
    }
  }
  return false;
}
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PROJECT, ARGUMENT_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_HARDWARE, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_DATE, ARGUMENT_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_TIME, ARGUMENT_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_AUTHOR, ARGUMENT_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PORT, ARGUMENT_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PARAMETER, ARGUMENT_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void CCommand::WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
}

void CCommand::WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
}

void CCommand::WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.WriteLine(MASK_H, SHORT_H);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPH, SHORT_GPH);
  serial.WriteAnswer(); serial.WriteLine(MASK_GSV, SHORT_GSV);
  serial.WriteAnswer(); serial.WriteLine(MASK_GHV, SHORT_GHV);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPC, SHORT_GPC);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPC, SHORT_SPC);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPP, SHORT_GPP);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPP, SHORT_SPP);
  serial.WriteAnswer(); serial.WriteLine(MASK_A, SHORT_A);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LEDSYSTEM);
  serial.WriteAnswer(); serial.WriteLine(MASK_GLS, SHORT_GLS);
  serial.WriteAnswer(); serial.WriteLine(MASK_LSH, SHORT_LSH);
  serial.WriteAnswer(); serial.WriteLine(MASK_LSL, SHORT_LSL);
  serial.WriteAnswer(); serial.WriteLine(MASK_BLS, SHORT_BLS);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERRANGE);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPX, SHORT_GPX);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPX, SHORT_SPX);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPY, SHORT_GPY);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPY, SHORT_SPY);
  serial.WriteAnswer(); serial.WriteLine(MASK_GRX, SHORT_GRX);
  serial.WriteAnswer(); serial.WriteLine(MASK_SRX, SHORT_SRX);
  serial.WriteAnswer(); serial.WriteLine(MASK_GRY, SHORT_GRY);
  serial.WriteAnswer(); serial.WriteLine(MASK_SRY, SHORT_SRY);
  serial.WriteAnswer(); serial.WriteLine(MASK_GDM, SHORT_GDM);
  serial.WriteAnswer(); serial.WriteLine(MASK_SDM, SHORT_SDM);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPW, SHORT_GPW);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPW, SHORT_SPW);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERPOSITION);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLP, SHORT_PLP);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALP, SHORT_ALP);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERLINE);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLR, SHORT_PLR);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALR, SHORT_ALR);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLC, SHORT_PLC);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALC, SHORT_ALC);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERMATRIX);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLM, SHORT_PLM);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALM, SHORT_ALM);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERIMAGE);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLI, SHORT_PLI);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALI, SHORT_ALI);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_TRIGGER);
  serial.WriteAnswer(); serial.WriteLine(MASK_STA, SHORT_STA);
  serial.WriteAnswer(); serial.WriteLine(MASK_STP, SHORT_STP);
  serial.WriteAnswer(); serial.WriteLine(MASK_GTL, SHORT_GTL);
  serial.WriteAnswer(); serial.WriteLine(MASK_WTA, SHORT_WTA);
  serial.WriteAnswer(); serial.WriteLine(MASK_WTP, SHORT_WTP);
  serial.WriteAnswer(); serial.WriteLine(MASK_BFT, SHORT_BFT);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_SDCOMMAND);
  serial.WriteAnswer(); serial.WriteLine(MASK_OCF, SHORT_OCF);
  serial.WriteAnswer(); serial.WriteLine(MASK_WCF, SHORT_WCF);
  serial.WriteAnswer(); serial.WriteLine(MASK_CCF, SHORT_CCF);
  serial.WriteAnswer(); serial.WriteLine(MASK_ECF, SHORT_ECF);
  serial.WriteAnswer(); serial.WriteLine(MASK_ACF, SHORT_ACF);
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
void CCommand::ExecuteGetHelp(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetHelp);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), COUNT_HELP);
    serial.WriteLine(GetTxdBuffer());         
    WriteHelp(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetProgramHeader(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetProgramHeader);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), COUNT_HEADER);
    serial.WriteLine(GetTxdBuffer());   
    WriteProgramHeader(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetSoftwareVersion(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetSoftwareVersion);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), COUNT_SOFTWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteSoftwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetHardwareVersion);
    // Analyse parameters: -
    // Response
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), COUNT_HARDWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteHardwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetProcessCount(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetProcessCount);
    // Analyse parameters: -
    // Execute:
    UInt32 PC = LASProcess.GetProcessCount();    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetProcessCount(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetProcessCount);
    // Analyse parameters:
    UInt32 PC = atol(GetPParameters(0));
    // Execute:
    LASProcess.SetProcessCount(PC);
    PC = LASProcess.GetProcessCount();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetProcessPeriod(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetProcessPeriod);
    // Analyse parameters: -
    // Execute:
    UInt32 PP = LASProcess.GetProcessPeriodms();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetProcessPeriod(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetProcessPeriod);
    // Analyse parameters:
    UInt32 PP = atol(GetPParameters(0));
    // Execute:
    LASProcess.SetProcessPeriodms(PP);
    PP = LASProcess.GetProcessPeriodms();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteAbortAll(CSerial &serial)
{ 
  LASProcess.SetProcessStep(0);
  LASProcess.SetProcessCount(0);
  LASProcess.SetState(spIdle);
  // Analyse parameters:
  // Execute:
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LedSystem
//#########################################################
//
void CCommand::ExecuteGetLedSystem(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetLedSystem);
    // Analyse parameters:
    // Execute:
    int State = LedSystem.GetState();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), State);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteLedSystemOn(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spLedSystemOn);
    // Analyse parameters:
    // Execute:
    LedSystem.On();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteLedSystemOff(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spLedSystemOff);
    // Analyse parameters:
    // Execute:
    LedSystem.Off();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteBlinkLedSystem(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spBlinkLedSystem);
    // Analyse parameters:
    UInt32 Period = atol(GetPParameters(0)); // [us]!!!!!!!!
    UInt32 Count = atol(GetPParameters(1));  // [1] 
    // Execute:
    LASProcess.SetProcessPeriodms(Period);
    LASProcess.SetProcessCount(Count);    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
            PROMPT_RESPONSE, GetPCommand(), Period, Count);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - LaserRange
//#########################################################
//
void CCommand::ExecuteGetPositionX(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetPositionX);
    // Analyse parameters: -
    // Execute:
    UInt32 PX = LASProcess.GetXPositionActual();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", PROMPT_RESPONSE, 
            GetPCommand(), PX);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetPositionX(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetPositionX);
    // Analyse parameters ( SPX <x> ):
    UInt32 PX = atol(GetPParameters(0));
    // Execute:
    LASProcess.SetXPositionActual(PX);    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", PROMPT_RESPONSE, 
            GetPCommand(), PX);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetPositionY(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetPositionY);
    // Analyse parameters: -
    // Execute:
    UInt32 PY = LASProcess.GetYPositionActual();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PY);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetPositionY(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetPositionY);
    // Analyse parameters ( SPY <y> ):
    UInt32 PY = atol(GetPParameters(0));
    // Execute:
    LASProcess.SetYPositionActual(PY);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", PROMPT_RESPONSE, 
            GetPCommand(), PY);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetRangeX(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetRangeX);
    // Analyse parameters: -
    // Execute:
    UInt32 PL = LASProcess.GetXPositionMinimum();
    UInt32 PH = LASProcess.GetXPositionMaximum();
    UInt32 PD = LASProcess.GetXPositionDelta();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu", PROMPT_RESPONSE, 
            GetPCommand(), PL, PH, PD);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetRangeX(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetRangeX);
    // Analyse parameters ( SRX <xmin> <xmax> <dx> ):
    UInt32 PL = atol(GetPParameters(0));
    UInt32 PH = atol(GetPParameters(1));
    UInt32 PD = atol(GetPParameters(2));
    // Execute:
    LASProcess.SetXPositionMinimum(PL);
    LASProcess.SetXPositionMaximum(PH);
    LASProcess.SetXPositionDelta(PD);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu", PROMPT_RESPONSE, 
            GetPCommand(), PL, PH, PD);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetRangeY(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetRangeY);
    // Analyse parameters: -
    // Execute:
    UInt32 PL = LASProcess.GetYPositionMinimum();
    UInt32 PH = LASProcess.GetYPositionMaximum();
    UInt32 PD = LASProcess.GetYPositionDelta();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu", 
            PROMPT_RESPONSE, GetPCommand(), PL, PH, PD);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetRangeY(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetRangeY);
    // Analyse parameters ( SRY <ymin> <ymax> <dy> ):
    UInt32 PL = atol(GetPParameters(0));
    UInt32 PH = atol(GetPParameters(1));
    UInt32 PD = atol(GetPParameters(2));
    // Execute:
    LASProcess.SetYPositionMinimum(PL);
    LASProcess.SetYPositionMaximum(PH);
    LASProcess.SetYPositionDelta(PD);   
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu", 
            PROMPT_RESPONSE, GetPCommand(), PL, PH, PD);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetDelayMotionus(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetDelayMotion);
    // Analyse parameters: -
    // Execute:
    UInt32 DM = LASProcess.GetDelayMotionus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), DM);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetDelayMotionus(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetDelayMotion);
    // Analyse parameters ( SDM <delayus> ):
    UInt32 DM = atol(GetPParameters(0));    
    // Execute:
    LASProcess.SetDelayMotionus(DM);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), DM);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetPulseWidthus(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetPulseWidth);
    // Analyse parameters: - ( GPW )
    // Execute:
    UInt32 PW = LASProcess.GetPulseWidthus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PW);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetPulseWidthus(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetPulseWidth);
    // Analyse parameters ( SPW <width> ):
    UInt32 PW = atol(GetPParameters(0));
    // Execute:
    LASProcess.SetPulseWidthus(PW);
    PW = LASProcess.GetPulseWidthus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PW);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - LaserPosition
//#########################################################
//
// PulseLaserPosition PLP <x> <y> <p> <c> : Pulse Laser <x><y> <p>eriod <c>ount"
// Definition of XPositionActual, YPositionActual, ProcessPeriod, ProcessCount
void CCommand::ExecutePulseLaserPosition(CSerial &serial)
{ 
//  if (spIdle == LASProcess.GetState())
//  {
    LASProcess.SetState(spPulseLaserPosition);
    // Analyse parameters ( PLP <x> <y> <p> <c> ):
    UInt32 PositionX = atol(GetPParameters(0));
    UInt32 PositionY = atol(GetPParameters(1));
    UInt32 PulsePeriod = atol(GetPParameters(2));
    UInt32 PulseCount = atol(GetPParameters(3));
    // Execute:
    LASProcess.SetXPositionActual(PositionX);
    LASProcess.SetYPositionActual(PositionY);
    LASProcess.SetProcessPeriodms(PulsePeriod);
    LASProcess.SetProcessCount(PulseCount);  
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu %lu", PROMPT_RESPONSE, 
            GetPCommand(), PositionX, PositionY, PulsePeriod, PulseCount);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
//  }
//  else
//  {
//    // Error - Command Timing
//  }
}

void CCommand::ExecuteAbortLaserPosition(CSerial &serial)
{ 
    LASProcess.SetState(spAbortLaserPosition);
    LASProcess.SetProcessStep(0);
    LASProcess.SetProcessCount(0);
    LASProcess.SetState(spIdle);
    // Analyse parameters : -
    // Execute : -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LaserLine
//#########################################################
//
void CCommand::ExecutePulseLaserRow(CSerial &serial)
{
//  if (spIdle == LASProcess.GetState())
//  {
    LASProcess.SetState(spPulseLaserRow);
    // Analyse parameters ( PLR <p> <n> ):
    UInt32 PP = atol(GetPParameters(0));
    UInt32 PC = atol(GetPParameters(1));
    LASProcess.SetProcessPeriodms(PP);
    LASProcess.SetProcessCount(PC);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
            PROMPT_RESPONSE, GetPCommand(), PP, PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
//  }
//  else
//  {
//    // Error - Command Timing
//  }
}

void CCommand::ExecuteAbortLaserRow(CSerial &serial)
{
    LASProcess.SetState(spAbortLaserRow);
    LASProcess.SetProcessStep(0);
    LASProcess.SetProcessCount(0);
    LASProcess.SetState(spIdle);
    // Analyse parameters : -    
    // Execute : -    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
}

void CCommand::ExecutePulseLaserCol(CSerial &serial)
{
//  if (spIdle == LASProcess.GetState())
//  {
    LASProcess.SetState(spPulseLaserCol);
    // Analyse parameters ( PLC <p> <n> ):
    UInt32 PP = atol(GetPParameters(0));
    UInt32 PC = atol(GetPParameters(1));
    LASProcess.SetProcessPeriodms(PP);
    LASProcess.SetProcessCount(PC);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
            PROMPT_RESPONSE, GetPCommand(), PP, PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
//  }
//  else
//  {
//    // Error - Command Timing
//  }
}

void CCommand::ExecuteAbortLaserCol(CSerial &serial)
{
    LASProcess.SetState(spAbortLaserCol);
    LASProcess.SetProcessStep(0);
    LASProcess.SetProcessCount(0);
    LASProcess.SetState(spIdle);
    // Analyse parameters : -    
    // Execute : -    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LaserMatrix
//#########################################################
//
void CCommand::ExecutePulseLaserMatrix(CSerial &serial)
{
//  if (spIdle == LASProcess.GetState())
//  {
    LASProcess.SetState(spPulseLaserMatrix);
    // Analyse parameters ( PLM <p> <n> ):
    UInt32 PP = atol(GetPParameters(0));
    UInt32 PC = atol(GetPParameters(1));
    LASProcess.SetProcessPeriodms(PP);
    LASProcess.SetProcessCount(PC);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
            PROMPT_RESPONSE, GetPCommand(), PP, PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
//  }
//  else
//  {
//    // Error - Command Timing
//  }
}

void CCommand::ExecuteAbortLaserMatrix(CSerial &serial)
{
    LASProcess.SetState(spAbortLaserMatrix);
    LASProcess.SetProcessStep(0);
    LASProcess.SetProcessCount(0);
    LASProcess.SetState(spIdle);
    // Analyse parameters : -    
    // Execute : -    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LaserImage
//#########################################################
//
void CCommand::ExecutePulseLaserImage(CSerial &serial)
{
  LASProcess.SetState(spPulseLaserImage);
  // Analyse parameters ( PLI <x> <y> <p> <c> <dus> ):
  UInt32 XP = atol(GetPParameters(0));
  UInt32 YP = atol(GetPParameters(1));
  UInt32 PP = atol(GetPParameters(2));
  UInt32 PC = atol(GetPParameters(3));
  UInt32 DMUS = atol(GetPParameters(4));
  LASProcess.SetXPositionActual(XP);
  LASProcess.SetYPositionActual(YP);
  LASProcess.SetProcessPeriodms(PP);
  LASProcess.SetProcessCount(PC);
  LASProcess.SetDelayMotionus(DMUS);
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu %lu %lu", 
          PROMPT_RESPONSE, GetPCommand(), XP, YP, PP, PC, DMUS);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteAbortLaserImage(CSerial &serial)
{
    LASProcess.SetState(spAbortLaserImage);
    LASProcess.SetProcessStep(0);
    LASProcess.SetProcessCount(0);
    LASProcess.SetState(spIdle);
    // Analyse parameters : -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", 
            PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Trigger
//#########################################################
//
void CCommand::ExecuteSetTriggerActive(CSerial &serial)
{
  LASProcess.SetState(spSetTriggerActive);
  // Analyse parameters ( STA - ):
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", 
          PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteSetTriggerPassive(CSerial &serial)
{
  LASProcess.SetState(spSetTriggerPassive);
  // Analyse parameters ( STP - ):
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", 
          PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteGetTriggerLevel(CSerial &serial)
{
  LASProcess.SetState(spGetTriggerLevel);
  // Analyse parameters ( GTL - ):
  // Execute:
  ETriggerLevel TL = TriggerIn.GetLevel();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %i", 
          PROMPT_RESPONSE, GetPCommand(), (int)TL);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteWaitTriggerActive(CSerial &serial)
{
  LASProcess.SetState(spWaitTriggerActive);
  // Analyse parameters ( WTA - ):
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", 
          PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteWaitTriggerPassive(CSerial &serial)
{
  LASProcess.SetState(spWaitTriggerPassive);
  // Analyse parameters ( WTP - ):
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", 
          PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteBreakForTimems(CSerial &serial)
{
  LASProcess.SetState(spBreakForTime);
  // Analyse parameters ( BFT <timems> ):
  UInt32 BT = atol(GetPParameters(0));
  LASProcess.SetBreakTimems(BT);
  BT = LASProcess.GetBreakTimems();
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %lu", 
          PROMPT_RESPONSE, GetPCommand(), BT);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - SDCommand
//#########################################################
//
void CCommand::ExecuteOpenCommandFile(CSerial &serial)
{
  LASProcess.SetState(spOpenCommandFile);
  // Analyse parameters ( OCF <f> ):
  LASProcess.SetFileName(GetPParameters(0));
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %s", 
          PROMPT_RESPONSE, GetPCommand(), GetPParameters(0));
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteWriteCommandFile(CSerial &serial)
{
  LASProcess.SetState(spWriteCommandFile);
  // Analyse parameters ( WCF <c> <p> ):
  // Execution: -
  // Response:
  serial.Write(PROMPT_RESPONSE);
  serial.Write(' ');
  serial.Write(GetPCommand());
  serial.Write(' ');
  int PC = GetParameterCount();    
  for (int II = 0; II < PC; II++)
  {
    serial.Write(GetPParameters(II));
    serial.Write(' ');
  }
  serial.WriteLine("");
  serial.WritePrompt();
}

void CCommand::ExecuteCloseCommandFile(CSerial &serial)
{
  LASProcess.SetState(spCloseCommandFile);
  // Analyse parameters ( CCF - ):
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", 
          PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteExecuteCommandFile(CSerial &serial)
{ // ecf a.cmd
  LASProcess.SetState(spExecuteCommandFile);
  // Analyse parameters ( ECF <f> ):
  String CF = GetPParameters(0);
  LASProcess.SetFileName(GetPParameters(0));
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", 
          PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteAbortCommandFile(CSerial &serial)
{
  LASProcess.SetState(spAbortCommandFile);
  // Analyse parameters ( ACF - ):
  // Execution:
//  Boolean Result = SDCard.CloseReadFile();
//  if (Result)
//  {
//    //... abort execution...
//  }
//  // Response:
//  sprintf(GetTxdBuffer(), "%s %s %u", 
//          PROMPT_RESPONSE, GetPCommand(), Result);
//  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
Boolean CCommand::Execute(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
// debug Serial.WriteLine(TxdBuffer);
  //  Common
  if (!strcmp(SHORT_H, GetPCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPH, GetPCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GSV, GetPCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GHV, GetPCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPC, GetPCommand()))
  {
    ExecuteGetProcessCount(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPC, GetPCommand()))
  {
    ExecuteSetProcessCount(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPP, GetPCommand()))
  {
    ExecuteGetProcessPeriod(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPP, GetPCommand()))
  {
    ExecuteSetProcessPeriod(serial);
    return true;
  } else 
  if (!strcmp(SHORT_A, GetPCommand()))
  {
    ExecuteAbortAll(serial);
    return true;
  } else 
  // ----------------------------------
  // LedSystem
  // ---------------------------------- 
  if (!strcmp(SHORT_GLS, GetPCommand()))
  {
    ExecuteGetLedSystem(serial);
    return true;
  } else 
  if (!strcmp(SHORT_LSH, GetPCommand()))
  {
    ExecuteLedSystemOn(serial);
    return true;
  } else   
  if (!strcmp(SHORT_LSL, GetPCommand()))
  {
    ExecuteLedSystemOff(serial);
    return true;
  } else   
  if (!strcmp(SHORT_BLS, GetPCommand()))
  {
    ExecuteBlinkLedSystem(serial);
    return true;
  } else   
  // ----------------------------------
  // LaserRange
  // ----------------------------------
  if (!strcmp(SHORT_GPX, GetPCommand()))
  {
    ExecuteGetPositionX(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPX, GetPCommand()))
  {
    ExecuteSetPositionX(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GPY, GetPCommand()))
  {
    ExecuteGetPositionY(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPY, GetPCommand()))
  {
    ExecuteSetPositionY(serial);
    return true;
  } else    
  if (!strcmp(SHORT_GRX, GetPCommand()))
  {
    ExecuteGetRangeX(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SRX, GetPCommand()))
  {
    ExecuteSetRangeX(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GRY, GetPCommand()))
  {
    ExecuteGetRangeY(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SRY, GetPCommand()))
  {
    ExecuteSetRangeY(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GDM, GetPCommand()))
  {
    ExecuteGetDelayMotionus(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SDM, GetPCommand()))
  {
    ExecuteSetDelayMotionus(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GPW, GetPCommand()))
  {
    ExecuteGetPulseWidthus(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPW, GetPCommand()))
  {
    ExecuteSetPulseWidthus(serial);
    return true;
  } else   
  // ----------------------------------
  // LaserPosition
  // ----------------------------------
  if (!strcmp(SHORT_PLP, GetPCommand()))
  {
    ExecutePulseLaserPosition(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALP, GetPCommand()))
  {
    ExecuteAbortLaserPosition(serial);
    return true;
  } else   
  // ----------------------------------
  // LaserLine
  // ----------------------------------
  if (!strcmp(SHORT_PLR, GetPCommand()))
  {
    ExecutePulseLaserRow(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALR, GetPCommand()))
  {
    ExecuteAbortLaserRow(serial);
    return true;
  } else   
  if (!strcmp(SHORT_PLC, GetPCommand()))
  {
    ExecutePulseLaserCol(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALC, GetPCommand()))
  {
    ExecuteAbortLaserCol(serial);
    return true;
  } else   
  // ----------------------------------
  // LaserMatrix
  // ----------------------------------
  if (!strcmp(SHORT_PLM, GetPCommand()))
  {
    ExecutePulseLaserMatrix(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALM, GetPCommand()))
  {
    ExecuteAbortLaserMatrix(serial);
    return true;
  } else   
  // ----------------------------------
  // LaserImage
  // ----------------------------------
  if (!strcmp(SHORT_PLI, GetPCommand()))
  {
    ExecutePulseLaserImage(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALI, GetPCommand()))
  {
    ExecuteAbortLaserImage(serial);
    return true;
  } else   
  // ----------------------------------
  // Trigger
  // ----------------------------------
  if (!strcmp(SHORT_STA, GetPCommand()))
  {
    ExecuteSetTriggerActive(serial);
    return true;
  } else   
  if (!strcmp(SHORT_STP, GetPCommand()))
  {
    ExecuteSetTriggerPassive(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GTL, GetPCommand()))
  {
    ExecuteGetTriggerLevel(serial);
    return true;
  } else   
  if (!strcmp(SHORT_WTA, GetPCommand()))
  {
    ExecuteWaitTriggerActive(serial);
    return true;
  } else   
  if (!strcmp(SHORT_WTP, GetPCommand()))
  {
    ExecuteWaitTriggerPassive(serial);
    return true;
  } else   
  if (!strcmp(SHORT_BFT, GetPCommand()))
  {
    ExecuteBreakForTimems(serial);
    return true;
  } else   
  // ----------------------------------
  // SDCommand
  // ----------------------------------
  if (!strcmp(SHORT_OCF, GetPCommand()))
  {
    ExecuteOpenCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_WCF, GetPCommand()))
  {
    ExecuteWriteCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_CCF, GetPCommand()))
  {
    ExecuteCloseCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ECF, GetPCommand()))
  {
    ExecuteExecuteCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ACF, GetPCommand()))
  {
    ExecuteAbortCommandFile(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    LASError.SetCode(ecInvalidCommand);
  }
  return false;  
}




