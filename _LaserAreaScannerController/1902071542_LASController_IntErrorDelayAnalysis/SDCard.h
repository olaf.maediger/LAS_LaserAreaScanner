//
//--------------------------------
//  Library SDCard
//--------------------------------
//
#include "Defines.h"
//
#ifndef SDCard_h
#define SDCard_h
//
#include <Arduino.h>
#include <String.h>
#include <SPI.h>
#include <SDCard.h>
// 
// Maximal Size of SDHCard: 32GB !!!
//
// Connection
//  ArduinoDue    SDCardModule
//    5V0(!!!)  -   3V3
//    GND       -   GND
//    D4        -   CS/
//    MOSI      -   MOSI
//    MISO      -   MISO
//
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Exchange _Exchange_With_Original_In_Arduino_Lib__SD.h
// or put the following end() function to .../Arduino/libraries/SD/src/SD.h :
//
// boolean begin(uint8_t csPin = SD_CHIP_SELECT_PIN);
// boolean begin(uint32_t clock, uint8_t csPin);
// insert this:
// boolean end() { root.close(); }
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
//--------------------------------
//  Section - SDCard
//--------------------------------
//
class CSDCard 
{
  private:
  int FPinCS;
  SDClass* FPSDClass;
  File FFileWrite;
  File FFileRead;
  Boolean FIsOpenForWriting;
  Boolean FIsOpenForReading;
  //  
  public:
  CSDCard(int pincs);
  //
  Boolean IsMounted();
  Boolean Mount();    // == Open()
  Boolean Unmount();  // == Close()
  //
  Boolean CreateDirectory(String directoryname);
  Boolean RemoveDirectory(String directoryname);
  Boolean GetFileExists(String filename);
  long unsigned GetFileSize(String filename);
  Boolean RemoveFile(String filename);
  //
  Boolean IsOpenForWriting();
  Boolean OpenWriteFile(String filename);
  Boolean WriteSingleCharacter(char character);
  Boolean WriteSingleByte(unsigned char value);
  Boolean WriteTextBlock(String text);
  Boolean WriteTextBlock(UInt32 size, String text);
  Boolean WriteTextLine(String line);
  Boolean WriteNumberText(long unsigned number);
  Boolean CloseWriteFile();
  //
  Boolean IsOpenForReading();
  Boolean OpenReadFile(String filename);
  Boolean ReadAvailable();
  char ReadSingleCharacter();
  String ReadTextBlock(unsigned charactercount);
  String ReadTextLine();
  Boolean CloseReadFile();
};
//
#endif // SDCard_h

