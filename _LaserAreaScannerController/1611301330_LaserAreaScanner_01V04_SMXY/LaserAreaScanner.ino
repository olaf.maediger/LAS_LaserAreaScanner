#include "Defines.h"
#include "Helper.h"
#include "Led.h"
#include "Serial.h"
#include "Utilities.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM);
CLed LedError(PIN_LEDERROR);
CLed LedBusy(PIN_LEDBUSY);
CLed LedMotionX(PIN_LEDMOTIONX);
CLed LedMotionY(PIN_LEDMOTIONY);
CLed LedTrigger(PIN_LEDTRIGGER);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
CSerial SerialPC(Serial1);
//CSerial SerialXXX(Serial2);
//CSerial SerialYYY(Serial3);
//CSerial SerialVVV(SerialUSB);
//CSerial SerialUUU(Serial);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Dac
//---------------------------------------------------
//
CDac DigitalAnalogConverter(true, true);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Adc
//---------------------------------------------------
//
CAdc AnalogDigitalConverter(true, true, true, true);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Pwm
//---------------------------------------------------
//
CPwm PulseWidthModulator(true, true, true, true);
//
//---------------------------------------------------
// Segment - Global Variables - Automation
//---------------------------------------------------
//
EStateAutomation StateAutomation;
Int32 CountRepetition; // [1]
//
UInt16 XPositionActual; // [stp]
UInt16 XPositionDelta; // [stp]
UInt16 XPositionMinimum; // [stp]
UInt16 XPositionMaximum; // [stp]
//
UInt16 YPositionActual; // [stp]
UInt16 YPositionDelta; // [stp]
UInt16 YPositionMinimum; // [stp]
UInt16 YPositionMaximum; // [stp]
//
UInt32 DelayTrigger; // [us]
//
void setup() 
{
  LedSystem.Open();
  LedSystem.Off();  
  LedError.Open();
  LedBusy.Open();
  LedMotionX.Open();
  LedMotionY.Open();
  LedTrigger.Open();
  //
  SerialPC.Open();
  //
  DigitalAnalogConverter.Open();
  //
  AnalogDigitalConverter.Open();
  //
  PulseWidthModulator.Open();
  //
  WriteProgramHeader(SerialPC);
  WriteHelp(SerialPC);
  SerialPC.WritePrompt();
  //
  StateAutomation = saIdle;
  CountRepetition = INIT_COUNTREPETITION;
  //
  XPositionActual = INIT_XPOSITIONACTUAL;
  XPositionDelta = INIT_XPOSITIONDELTA;
  XPositionMinimum = INIT_XPOSITIONMINIMUM;
  XPositionMaximum = INIT_XPOSITIONMAXIMUM;
  //
  YPositionActual = INIT_YPOSITIONACTUAL;
  YPositionDelta = INIT_YPOSITIONDELTA;
  YPositionMinimum = INIT_YPOSITIONMINIMUM;
  YPositionMaximum = INIT_YPOSITIONMAXIMUM;
  //
  DelayTrigger = INIT_DELAYTRIGGER;
  //
  // debug GlobalErrorCode = ecInvalidCommand;
}

void loop() 
{
  HandleError(SerialPC);  
  HandleSerialCommands(SerialPC);
  HandleAutomation(SerialPC);
}
