#ifndef Utilities_h
#define Utilities_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
//
#define LED_SYSTEM  0
#define LED_ERROR   1
#define LED_BUSY    2
#define LED_MOTIONX 3
#define LED_MOTIONY 4
#define LED_TRIGGER 5
//
#define ADC_POSITIONX 0
#define ADC_POSITIONY 1
//
#define DAC_MIRRORX 0
#define DAC_MIRRORY 1
//
#define PWM_FOCUSZ 0
//
#define XPOSITION_CENTER (UInt16)2048
#define YPOSITION_CENTER (UInt16)2048
//
#define DELAY_TRIGGER_1MS (UInt32)1000
//
// Init-Values for Globals:
#define INIT_COUNTREPETITION 1
//
#define INIT_XPOSITIONACTUAL 2048
#define INIT_XPOSITIONDELTA 1023
#define INIT_XPOSITIONMINIMUM 0
#define INIT_XPOSITIONMAXIMUM 4095
//
#define INIT_YPOSITIONACTUAL 2048
#define INIT_YPOSITIONDELTA 1023
#define INIT_YPOSITIONMINIMUM 0
#define INIT_YPOSITIONMAXIMUM 4095
//
#define INIT_DELAYTRIGGER 1000
//
enum EStateAutomation
{
  saIdle = 0,
  saRowColLeft = 1,
  saRowColBoth = 2
};
//
void WriteProgramHeader(CSerial &serialcommand);
void WriteHelp(CSerial &serialcommand);
//  Handler - Error
void HandleError(CSerial &serial);
//  Handler - SerialPC Communication
void HandleSerialCommands(CSerial &serialcommand);
void HandleAutomation(CSerial &serial);
//
#endif
