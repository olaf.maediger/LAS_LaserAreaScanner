#include "Utilities.h"
#include "Led.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
//
//
//-------------------------------------------
//  Global Variables 
//-------------------------------------------
//	
enum EErrorCode ErrorCode;
//
Character TxdBuffer[SIZE_TXDBUFFER];
Character RxdBuffer[SIZE_RXDBUFFER];
Int16 RxdBufferIndex = 0;
Character RxdCommandLine[SIZE_RXDBUFFER];
PCharacter PRxdCommand;
PCharacter PRxdParameters[COUNT_RXDPARAMETERS];
Int16 RxdParameterCount = 0;
//
extern CLed LedSystem;
extern CLed LedError;
extern CLed LedBusy;
extern CLed LedMotionX;
extern CLed LedMotionY;
extern CLed LedTrigger;
//
extern CAdc AnalogDigitalConverter;
//
extern CDac DigitalAnalogConverter;
//
extern CPwm PulseWidthModulator;
//
extern EStateAutomation StateAutomation;
extern Int32 CountRepetition; // [1]
//
extern UInt16 XPositionActual; // [stp]
extern UInt16 XPositionDelta; // [stp]
extern UInt16 XPositionMinimum; // [stp]
extern UInt16 XPositionMaximum; // [stp]
//
extern UInt16 YPositionActual; // [stp]
extern UInt16 YPositionDelta; // [stp]
extern UInt16 YPositionMinimum; // [stp]
extern UInt16 YPositionMaximum; // [stp]
//
extern UInt32 DelayTrigger; // [us]
//
//
//#########################################################
//  Segment - Helper
//#########################################################
//
void ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    RxdBuffer[CI] = 0x00;
  }
  RxdBufferIndex = 0;
}

void ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    TxdBuffer[CI] = 0x00;
  }
}

void ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    RxdCommandLine[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void WriteProgramHeader(CSerial &serial)
{  
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
  //
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("---------------------------------------");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Project:   LAS - LaserAreaScanner   -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Version:   02V04                    -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Hardware:  ArduinoDue               -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Date:      161130                   -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Time:      1335                     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Author:    OM                       -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Port:      SerialPC (SerialUSB)     -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("- Parameter: 115200, N, 8, 1          -");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText("---------------------------------------");
  serial.WriteNewLine();	
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Software-Version LAS02V04");
  serial.WriteNewLine();
}

void WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Hardware-Version ArduinoDue3");
  serial.WriteNewLine();
}

void WriteHelp(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(" Help (Common):");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" H   : This Help");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GPH <fdl> : Get Program Header");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GSV <fdl> : Get Software-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" GHV <fdl> : Get Hardware-Version");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Led):");
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(" GLD <i> : Get State of Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" SLD <i> : Set Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" CLD <i> : Clear Led with Index<0..3>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(" TLD <i> <n> <d>: Toggle Led with Index<0..3> <n>-times with <d>-delay");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Dac):");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" GDV <i> : Get Value from DAConverter with Index<0..1>");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SDV <i> <v>: Set Value<0..4095> to DAConverter with Index<0..1>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Adc):");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" GAV <i> : Get Value from ADConverter with Index<0..11>");
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(" Help (Pwm):");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" GPV <i> : Get Value from PWModulator with Index<0..11>");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SPV <i> <v>: Set Value<0..4095> to DAConverter with Index<0..1>");
  serial.WriteNewLine();
  //
  serial.WriteText(" Help (Automation):");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SMX <xmin> <xmax> <dx>: Set Motion-ParameterX [0..4095]");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" SMY <ymin> <ymax> <dy>: Set Motion-ParameterY [0..4095]");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" STM <delay> <repetitions> : Start Motion (Trigger<delay>) with <repetitions> ");
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(" ABM : Abort Motion");
  serial.WriteNewLine();
}
//
//#########################################################
//  Segment - Execution - Common
//#########################################################
//
void ExecuteGetHelp(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHelp(serial);
  serial.WritePrompt();
}

void ExecuteGetProgramHeader(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void ExecuteGetSoftwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void ExecuteGetHardwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Led
//#########################################################
//
void ExecuteGetLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(PRxdParameters[0]);
  int State;
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      State = LedSystem.GetState();
      break;
    case LED_ERROR:
      State = LedError.GetState();
      break;
    case LED_BUSY:
      State = LedBusy.GetState();
      break;
    case LED_MOTIONX:
      State = LedMotionX.GetState();
      break;
    case LED_MOTIONY:
      State = LedMotionY.GetState();
      break;
    case LED_TRIGGER:
      State = LedTrigger.GetState();
      break;
  }
  // Answer:
  sprintf(TxdBuffer, "# %s %s %i", PRxdCommand, PRxdParameters[0], State);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}

void ExecuteClearLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(PRxdParameters[0]);
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.Off();
      break;
    case LED_ERROR:
      LedError.Off();
      break;
    case LED_BUSY:
      LedBusy.Off();
      break;
    case LED_MOTIONX:
      LedMotionX.Off();
      break;
    case LED_MOTIONY:
      LedMotionY.Off();
      break;
    case LED_TRIGGER:
      LedTrigger.Off();
      break;
  }
  // Answer:
  sprintf(TxdBuffer, "# %s %s", PRxdCommand, PRxdParameters[0]);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}

void ExecuteSetLed(CSerial &serial)
{	// Analyse parameters:
  int Index = atoi(PRxdParameters[0]);
  // Execute:
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.On();
      break;
    case LED_ERROR:
      LedError.On();
      break;
    case LED_BUSY:
      LedBusy.On();
      break;
    case LED_MOTIONX:
      LedMotionX.On();
      break;
    case LED_MOTIONY:
      LedMotionY.On();
      break;
    case LED_TRIGGER:
      LedTrigger.On();
      break;
  }
  // Answer:
  sprintf(TxdBuffer, "# %s %s", PRxdCommand, PRxdParameters[0]);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}

void ExecuteToggleLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(PRxdParameters[0]);
  int Delay = atoi(PRxdParameters[1]);
  int Count = atoi(PRxdParameters[2]);
  // Answer:
  sprintf(TxdBuffer, "# %s %s", PRxdCommand, PRxdParameters[0]);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
  // Execute:
  int I;
  for (I = 0; I < Count - 1; I++)
  {
    switch (Index)
    {
      case LED_SYSTEM:
        LedSystem.Toggle();
        delay(Delay);
        break;
      case LED_ERROR:
        LedError.Toggle();
        delay(Delay);
        break;
      case LED_BUSY:
        LedBusy.Toggle();
        delay(Delay);
        break;
      case LED_MOTIONX:
        LedMotionX.Toggle();
        delay(Delay);
        break;
      case LED_MOTIONY:
        LedMotionY.Toggle();
        delay(Delay);
        break;
      case LED_TRIGGER:
        LedTrigger.Toggle();
        delay(Delay);
        break;
    }
  }
  switch (Index)
  {
    case LED_SYSTEM:
      LedSystem.Toggle();
      break;
    case LED_ERROR:
      LedError.Toggle();
      break;
    case LED_BUSY:
      LedBusy.Toggle();
      break;
    case LED_MOTIONX:
      LedMotionX.Toggle();
      break;
    case LED_MOTIONY:
      LedMotionY.Toggle();
      break;
    case LED_TRIGGER:
      LedTrigger.Toggle();
      break;
  }
}
//
//#########################################################
//  Segment - Execution - Adc
//#########################################################
//
void ExecuteGetAdcValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(PRxdParameters[0]);
  // Execute:
  UInt16 Value = AnalogDigitalConverter.GetValue(Channel);
  // Answer:
  sprintf(TxdBuffer, "# %s %u %u", PRxdCommand, Channel, Value);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Dac
//#########################################################
//
void ExecuteGetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(PRxdParameters[0]);
  // Execute:
  UInt16 Value = DigitalAnalogConverter.GetValue(Channel);
  // Answer:
  sprintf(TxdBuffer, "# %s %u %u", PRxdCommand, Channel, Value);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}

void ExecuteSetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(PRxdParameters[0]);
  UInt16 Value = atoi(PRxdParameters[1]);
  // Execute:
  DigitalAnalogConverter.SetValue(Channel, Value);
  Value = DigitalAnalogConverter.GetValue(Channel);
  // Answer:
  sprintf(TxdBuffer, "# %s %u %u", PRxdCommand, Channel, Value);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Pwm
//#########################################################
//
void ExecuteGetPwmValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(PRxdParameters[0]);
  // Execute:
  UInt16 Value = PulseWidthModulator.GetValue(Channel);
  // Answer:
  sprintf(TxdBuffer, "# %s %u %u", PRxdCommand, Channel, Value);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}

void ExecuteSetPwmValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(PRxdParameters[0]);
  UInt16 Value = atoi(PRxdParameters[1]);
  // Execute:
  PulseWidthModulator.SetValue(Channel, Value);
  Value = PulseWidthModulator.GetValue(Channel);
  // Answer:
  sprintf(TxdBuffer, "# %s %u %u", PRxdCommand, Channel, Value);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Helper
//#########################################################
//
void SetPosition()
{
  DigitalAnalogConverter.SetValue(DAC_MIRRORX, XPositionActual);
  DigitalAnalogConverter.SetValue(DAC_MIRRORY, YPositionActual);  
}

void SetTrigger()
{
  LedTrigger.On();
  delayMicroseconds(DelayTrigger / 2);
  LedMotionY.Off();
  LedMotionX.Off();
  delayMicroseconds(DelayTrigger / 2);
  LedTrigger.Off();
}
//
//#########################################################
//  Segment - Execution - Automation
//#########################################################
//
void ExecuteSetMotionX(CSerial &serial)
{ // Analyse parameters:
  XPositionMinimum = abs(atoi(PRxdParameters[0]));
  XPositionMaximum = abs(atoi(PRxdParameters[1]));
  XPositionDelta = abs(atoi(PRxdParameters[2]));
  // Execute -
  // Answer:
  sprintf(TxdBuffer, "# %s %lu %lu %lu", PRxdCommand, XPositionMinimum, XPositionMaximum, XPositionDelta);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();  
}

void ExecuteSetMotionY(CSerial &serial)
{ // Analyse parameters:
  YPositionMinimum = abs(atoi(PRxdParameters[0]));
  YPositionMaximum = abs(atoi(PRxdParameters[1]));
  YPositionDelta = abs(atoi(PRxdParameters[2]));
  // Execute -
  // Answer:
  sprintf(TxdBuffer, "# %s %lu %lu %lu", PRxdCommand, YPositionMinimum, YPositionMaximum, YPositionDelta);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();  
}

bool AbortMotion()
{
  StateAutomation = saIdle;
  LedMotionY.Off();
  LedMotionX.Off();
  LedBusy.Off();
  return true;
}

void ExecuteAbortMotion(CSerial &serial)
{ // Analyse parameters -
  // Execute:
  AbortMotion();
  // Answer:
  sprintf(TxdBuffer, "# ABM");
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();  
}

Boolean StartMotion()
{
  if (0 < CountRepetition)
  {
    LedMotionY.On();
    LedMotionX.On();
    LedBusy.On();
    XPositionActual = XPositionMinimum;
    YPositionActual = YPositionMinimum;
    StateAutomation = saRowColLeft;
    return true;
  }
  return false;
}

void ExecuteStartMotion(CSerial &serial)
{ // Analyse parameters:
  DelayTrigger = abs(atoi(PRxdParameters[0]));
  CountRepetition = abs(atoi(PRxdParameters[1]));
  // Execute:
  StartMotion();
  // Answer:
  sprintf(TxdBuffer, "# %s %lu %lu", PRxdCommand, DelayTrigger, CountRepetition);
  serial.WriteLine(TxdBuffer);
  serial.WritePrompt();  
}
//
//#########################################################
//  Segment - Handler - Error
//#########################################################
//
void HandleError(CSerial &serial)
{
  if (ecNone != ErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(ErrorCode, TxdBuffer);	
    serial.WriteText(TxdBuffer);
    serial.WriteText("]: ");
    switch (ErrorCode)
    {
      case ecNone:
        serial.WriteText("No Error");
        break;
      case ecUnknown:
        serial.WriteText("Unknown");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
      case ecToManyParameters:
        serial.WriteText("To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.WriteText("Missing Target Parameter");
        break;              
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    ErrorCode = ecNone;
  }
}
//
//#########################################################
//  Segment - Handler - Command - Helper
//#########################################################
//
Boolean DetectRxdCommandLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CARRIAGE_RETURN:
        RxdBuffer[RxdBufferIndex] = ZERO;
        RxdBufferIndex = 0; // restart
        strupr(RxdBuffer);
        strcpy(RxdCommandLine, RxdBuffer);
        // debug serial.WriteText("<CR>true");
        // debug delay(1000);
        return true;
      case LINE_FEED: // ignore
        // debug serial.WriteText("<LF>");
        // debug delay(1000);
        break;
      default: 
        // debug serial.WriteText("<");
        // debug serial.WriteCharacter(C);
        // debug serial.WriteText(">");
        RxdBuffer[RxdBufferIndex] = C;
        RxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean AnalyseRxdCommand(CSerial &serial)
{
  if (DetectRxdCommandLine(serial))
  {
    char *PTerminal = " \t\r\n";
    char *PRxdCommandLine = RxdCommandLine;
    PRxdCommand = strtok(RxdCommandLine, PTerminal);
    if (PRxdCommand)
    {
      RxdParameterCount = 0;
      char *PRxdParameter;
      while (PRxdParameter = strtok(0, PTerminal))
      {
        PRxdParameters[RxdParameterCount] = PRxdParameter;
        RxdParameterCount++;
        if (COUNT_RXDPARAMETERS < RxdParameterCount)
        {
          ErrorCode = ecToManyParameters;
          ZeroRxdBuffer();
          // debug serialtarget.WriteLine("error[ecToManyParameters]");
          serial.WriteNewLine();
          serial.WritePrompt();
          return false;
        }
      }  
//      debug sprintf(TxdBuffer, "\n\r# RxdCommand<%s>", PRxdCommand);
//      debug serial.WriteText(TxdBuffer);
//      if (0 < RxdParameterCount)
//      {
//        int II;
//        for (II = 0; II < RxdParameterCount; II++)
//        {
//          sprintf(TxdBuffer, " RxdParameter[%i]<%s>", II, PRxdParameters[II]);
//          serial.WriteText(TxdBuffer);
//        }
//      }
      //
      ZeroRxdBuffer();
      serial.WriteNewLine();
      return true;
    }
    ZeroRxdBuffer();
    serial.WriteNewLine();
    serial.WritePrompt();
  }
  return false;
}

Boolean ExecuteRxdCommand(CSerial &serial)
{ 
//  sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
//  serialtarget.WriteLine(TxdBuffer);
  if (!strcmp("H", PRxdCommand))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp("GPH", PRxdCommand))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp("GSV", PRxdCommand))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp("GHV", PRxdCommand))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  // ----------------------------------
  // Led
  // ----------------------------------
  if (!strcmp("GLD", PRxdCommand))
  {
    ExecuteGetLed(serial);
    return true;
  } else 	
  if (!strcmp("SLD", PRxdCommand))
  {
    ExecuteSetLed(serial);
    return true;
  } else 	
  if (!strcmp("CLD", PRxdCommand))
  {
    ExecuteClearLed(serial);
    return true;
  } else   
  if (!strcmp("TLD", PRxdCommand))
  {
    ExecuteToggleLed(serial);
    return true;
  } else   
  // ----------------------------------
  // Adc
  // ----------------------------------
  if (!strcmp("GAV", PRxdCommand))
  {
    ExecuteGetAdcValue(serial);
    return true;
  } else   
  // ----------------------------------
  // Dac
  // ----------------------------------
  if (!strcmp("GDV", PRxdCommand))
  {
    ExecuteGetDacValue(serial);
    return true;
  } else   
  if (!strcmp("SDV", PRxdCommand))
  {
    ExecuteSetDacValue(serial);
    return true;
  } else   
  // ----------------------------------
  // Pwm
  // ----------------------------------
  if (!strcmp("GPV", PRxdCommand))
  {
    ExecuteGetPwmValue(serial);
    return true;
  } else   
  if (!strcmp("SPV", PRxdCommand))
  {
    ExecuteSetPwmValue(serial);
    return true;
  } else   
  // ----------------------------------
  // Automation
  // ----------------------------------
  if (!strcmp("STM", PRxdCommand))
  {
    ExecuteStartMotion(serial);
    return true;
  } else   
  if (!strcmp("ABM", PRxdCommand))
  {
    ExecuteAbortMotion(serial);
    return true;
  } else   
  if (!strcmp("SMX", PRxdCommand))
  {
    ExecuteSetMotionX(serial);
    return true;
  } else   
  if (!strcmp("SMY", PRxdCommand))
  {
    ExecuteSetMotionY(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    ErrorCode = ecInvalidCommand;
  }
  return false;  
}

void HandleSerialCommands(CSerial &serial)
{
  if (AnalyseRxdCommand(serial))
  {
    ExecuteRxdCommand(serial);
  }  
}


//
//#########################################################
//  Segment - Automation
//#########################################################
//
void HandleAutomationIdle(CSerial &serial)
{  
  // nothing to do
}

void HandleAutomationRowColLeft(CSerial &serial)
{
  LedMotionX.On();
  LedMotionY.On();
  //
  SetPosition();
  SetTrigger();
  //
  XPositionActual += XPositionDelta;
  if (XPositionMaximum < XPositionActual)
  {
    XPositionActual = XPositionMinimum;
    YPositionActual += YPositionDelta;
    if (YPositionMaximum < YPositionActual)
    { // Finish Area scanning
      XPositionActual = XPositionMinimum;
      YPositionActual = YPositionMinimum;
      CountRepetition--;
      LedBusy.Toggle();
      if (0 == CountRepetition)
      { // ends...
        // SetPosition();
        ExecuteAbortMotion(serial);
        return;
      }
    } 
  } 
}

void HandleAutomation(CSerial &serial)
{
  switch (StateAutomation)
  {
    case saIdle:
      HandleAutomationIdle(serial);
      break;
    case saRowColLeft:
      HandleAutomationRowColLeft(serial);
      break;
    case saRowColBoth:
      break;
  }
}



