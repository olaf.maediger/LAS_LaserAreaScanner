#include "DriverMAX31865.h"
//
static SPISettings max31865_spisettings = SPISettings(500000, MSBFIRST, SPI_MODE1);
//
// Software SPI
CDriverMAX31865::CDriverMAX31865(Int8 pintspicss, Int8 pinspisck, Int8 pinspimosi, Int8 pinspimiso)
{
  FPin_SPI_CSS = pintspicss;
  FPin_SPI_SCK = pinspisck;
  FPin_SPI_MOSI = pinspimosi;
  FPin_SPI_MISO = pinspimiso;
}
// Hardware SPI 
CDriverMAX31865::CDriverMAX31865(Int8 pinspicss) 
{
  FPin_SPI_CSS = pinspicss;
  FPin_SPI_SCK = 0;
  FPin_SPI_MOSI = 0;
  FPin_SPI_MISO = 0;
}



UInt8 CDriverMAX31865::ReadRegister8(UInt8 address) 
{
  UInt8 Result = 0;
  ReadRegisterN(address, &Result, 1);
  return Result;
}

UInt16 CDriverMAX31865::ReadRegister16(UInt8 address) 
{
  UInt8 Buffer[2] = {0, 0};
  ReadRegisterN(address, Buffer, 2);
  UInt16 Result = Buffer[0];
  Result <<= 8;
  Result |=  Buffer[1]; 
  return Result;
}


void CDriverMAX31865::ReadRegisterN(UInt8 address, UInt8 data[], UInt8 count)
{
  address &= 0x7F;
  if (0 < FPin_SPI_SCK)
  { // Software SPI
    digitalWrite(FPin_SPI_SCK, LOW);
  }
  else
  { // Hardware SPI
    SPI.beginTransaction(max31865_spisettings);
  }
  digitalWrite(FPin_SPI_CSS, LOW);
  SpiTransfer(address);
  while (count--) 
  {
    data[0] = SpiTransfer(0xFF);
    data++;
  }
  if (0 < FPin_SPI_SCK)
  { // Software SPI    
  }
  else
  { // Hardware SPI
    SPI.endTransaction();
  }
  digitalWrite(FPin_SPI_CSS, HIGH);
}

void CDriverMAX31865::WriteRegister8(UInt8 address, UInt8 data)
{
  if (0 < FPin_SPI_SCK)
  { // Software SPI    
  }
  else
  { // Hardware SPI
    SPI.beginTransaction(max31865_spisettings);
  }
  digitalWrite(FPin_SPI_SCK, LOW);
  digitalWrite(FPin_SPI_CSS, LOW);
  SpiTransfer(address | 0x80);   
  SpiTransfer(data);
  if (0 < FPin_SPI_SCK)
  { // Software SPI    
  }
  else
  { // Hardware SPI
    SPI.endTransaction();
  }
  digitalWrite(FPin_SPI_CSS, HIGH);
}

UInt8 CDriverMAX31865::SpiTransfer(UInt8 data)
{
  if (0 < FPin_SPI_SCK)
  { // Software SPI
    UInt8 Reply = 0;
    for (int BI = 7; 0 <= BI; BI--) 
    {
      Reply <<= 1;
      digitalWrite(FPin_SPI_SCK, HIGH);
      digitalWrite(FPin_SPI_MOSI, data & (1 << BI));
      digitalWrite(FPin_SPI_SCK, LOW);
      if (digitalRead(FPin_SPI_MISO))
        Reply |= 1;
    }
    return Reply;
  }
  // hardware SPI
  return SPI.transfer(data);
}

Boolean CDriverMAX31865::Open(EWireCount wirecount) 
{
  pinMode(FPin_SPI_CSS, OUTPUT);
  digitalWrite(FPin_SPI_CSS, HIGH);
  if (0 < FPin_SPI_SCK) 
  { // Software SPI
    pinMode(FPin_SPI_SCK, OUTPUT); 
    digitalWrite(FPin_SPI_SCK, LOW);
    pinMode(FPin_SPI_MOSI, OUTPUT); 
    pinMode(FPin_SPI_MISO, INPUT);
  } 
  else 
  { // Hardware SPI
    SPI.begin();
  }  
  // dummy read all register
  for (Byte RI = 0; RI < 16; RI++) 
  {
    // readRegister8(i);
  }
  SetWires(wirecount);
  EnableBias(false);
  SetAutoConvert(false);
  ClearFault();
  return true;
}

void CDriverMAX31865::SetWires(EWireCount wirecount) 
{
  Byte RV = ReadRegister8(MAX31856_CONFIG_REG);
  if (MAX31865_3WIRE == wirecount) 
  { // 3wire
    RV |= MAX31856_CONFIG_3WIRE;
  } 
  else 
  { //2/4wire
    RV &= ~MAX31856_CONFIG_3WIRE;
  }
  WriteRegister8(MAX31856_CONFIG_REG, RV);
}

void CDriverMAX31865::SetAutoConvert(Boolean enable) 
{
  UInt8 RV = ReadRegister8(MAX31856_CONFIG_REG);
  if (enable) 
  { // enable autoconvert
    RV |= MAX31856_CONFIG_MODEAUTO;       
  } 
  else 
  { // disable autoconvert
    RV &= ~MAX31856_CONFIG_MODEAUTO;       
  }
  WriteRegister8(MAX31856_CONFIG_REG, RV);
}

void CDriverMAX31865::EnableBias(Boolean enable) 
{
  UInt8 RV = ReadRegister8(MAX31856_CONFIG_REG);
  if (enable) 
  { // enable bias
    RV |= MAX31856_CONFIG_BIAS;       
  } 
  else 
  { // disable bias
    RV &= ~MAX31856_CONFIG_BIAS;       
  }
  WriteRegister8(MAX31856_CONFIG_REG, RV);
}

UInt8 CDriverMAX31865::ReadFault(void) 
{
  return ReadRegister8(MAX31856_FAULTSTAT_REG);
}

void CDriverMAX31865::ClearFault(void) 
{
  UInt8 RV = ReadRegister8(MAX31856_CONFIG_REG);
  RV &= ~0x2C;
  RV |= MAX31856_CONFIG_FAULTSTAT;
  WriteRegister8(MAX31856_CONFIG_REG, RV);
}

UInt16 CDriverMAX31865::ReadRTD(void) 
{
  ClearFault();
  EnableBias(true);
  delay(10);
  UInt8 RV = ReadRegister8(MAX31856_CONFIG_REG);
  RV |= MAX31856_CONFIG_1SHOT;      
  WriteRegister8(MAX31856_CONFIG_REG, RV);
  delay(65);
  UInt16 RTD = ReadRegister16(MAX31856_RTDMSB_REG);
  // remove fault
  RTD >>= 1;
  return RTD;
}

Float CDriverMAX31865::ReadTemperatureCelsius(Float rtdnominal, Float rreference) 
{
  Float Z1, Z2, Z3, Z4, RTD, PValue;
  RTD = ReadRTD();
  RTD /= 32768;
  RTD *= rreference;
  Z1 = -RTD_A;
  Z2 = RTD_A * RTD_A - (4 * RTD_B);
  Z3 = (4 * RTD_B) / rtdnominal;
  Z4 = 2 * RTD_B;
  PValue = Z2 + (Z3 * RTD);
  PValue = (sqrt(PValue) + Z1) / Z4;
  if (0 <= PValue) return PValue;
  Float RP = RTD;
  PValue = -242.02;
  PValue += 2.2228 * RP;
  RP *= RTD;  // square
  PValue += 2.5859e-3 * RP;
  RP *= RTD;  // ^3
  PValue -= 4.8260e-6 * RP;
  RP *= RTD;  // ^4
  PValue -= 2.8183e-8 * RP;
  RP *= RTD;  // ^5
  PValue += 1.5243e-10 * RP;
  return PValue;
}

