#include "Automation.h"
#include "Led.h"
#include "Dac.h"
#include "Process.h"
#include "Command.h"
//
extern CLed LedLaser;
extern CDac DigitalAnalogConverter;
extern CSerial SerialCommand;
extern CProcess LASProcess;
extern CCommand LASCommand;
//
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  //FTimeStart = millis();
  FTimeStamp = millis();
  FTimeMarker = FTimeStamp;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  if (state != FState)
  { // Message to Manager(PC):
    FState = state;
    LASCommand.CallbackStateAutomation(SerialCommand, FState);
  }
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CAutomation::Open()
{
  FState = saIdle;
  return true;
}

Boolean CAutomation::Close()
{
  FState = saUndefined;
  return true;
}
//
//----------------------------------------------------------
//  Segment - Automation - Idle
//----------------------------------------------------------
//
void CAutomation::HandleIdle(CSerial &serial)
{
  // do nothing
}
//
//----------------------------------------------------------
//  Segment - Automation - LedLaser
//----------------------------------------------------------
//
void CAutomation::HandleLedLaserPulseBegin(CSerial &serial)
{
  LASProcess.SetPositionPulseCountActual(0);
  LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(), 
                               LASProcess.GetPositionPulseCountPreset());
  SetTimeStamp();  
  LedLaser.On(); 
  SetState(saLedLaserPulseBusy);
}
void CAutomation::HandleLedLaserPulseBusy(CSerial &serial)
{  
  SetTimeMarker();  
  if (LedLaser.GetPulsePeriodHalf() <= GetTimePeriod())
  {
    SetTimeStamp();
    if (slOn == LedLaser.GetState())
    {
      LedLaser.Off();
      LASProcess.IncrementPositionPulseCountActual();
      if (LASProcess.IsPositionPulseCountReached())
      {
        SetState(saLedLaserPulseEnd);
      }
    }
    else
      if (slOff == LedLaser.GetState())
      {
        LedLaser.On();
      }
  }
}
void CAutomation::HandleLedLaserPulseEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - PositionLaser
//----------------------------------------------------------
//
void CAutomation::HandlePulsePositionLaserBegin(CSerial &serial)
{ // -> Busy : wait for PVL x y p c ( -> Busy) OR : AVL -> Abort -> End -> Idle
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
  DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());
#endif
  LASProcess.SetPositionPulseCountActual(0);
  LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(), 
                               LASProcess.GetPositionPulseCountPreset());
  SetTimeStamp();
  LedLaser.On();
  SetState(saPulsePositionLaserBusy);
}

void CAutomation::HandlePulsePositionLaserBusy(CSerial &serial)
{ // -> Busy : wait for PVL x y p c ( -> Busy) OR : AVL -> Abort -> End -> Idle
  SetTimeMarker();
  if (LedLaser.GetPulsePeriodHalf() <= GetTimePeriod())
  {
    if (slOff == LedLaser.GetState())
    {
      SetTimeStamp();
      LedLaser.On();
    }
    else
      if (slOn == LedLaser.GetState())
      {
        SetTimeStamp();
        LedLaser.Off();
        LASProcess.IncrementPositionPulseCountActual();
        if (LASProcess.IsPositionPulseCountReached())
        {
          SetState(saPulsePositionLaserEnd);
        }
      }
  }
}

void CAutomation::HandlePulsePositionLaserEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - MatrixLaser
//----------------------------------------------------------
//
void CAutomation::HandlePulseMatrixLaserBegin(CSerial &serial)
{
  UInt32 XPA = LASProcess.GetXPositionMinimum();
  LASProcess.SetXPositionActual(XPA);
  UInt32 YPA = LASProcess.GetYPositionMinimum();
  LASProcess.SetYPositionActual(YPA);
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
  DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());  
#endif
  LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(), 
                               LASProcess.GetPositionPulseCountPreset());
  LASProcess.SetTotalPulseCountActual(0);
  LASProcess.SetPositionPulseCountActual(0);
  SetTimeStamp();
  LedLaser.On();
  SetState(saPulseMatrixLaserBusy);
}

void CAutomation::HandlePulseMatrixLaserBusy(CSerial &serial)
{  
  SetTimeMarker();
  if (LedLaser.GetPulsePeriodHalf() <= GetTimePeriod())
  {
    if (slOff == LedLaser.GetState())
    { // Off -> On
      SetTimeStamp();
      LedLaser.On();
    }
    else
      if (slOn == LedLaser.GetState())
      { // On -> Off
        SetTimeStamp();
        LedLaser.Off();
        LASProcess.IncrementTotalPulseCountActual();
        LASProcess.IncrementPositionPulseCountActual();        
        if (LASProcess.IsPositionPulseCountReached())
        { // All Pulses for one Location pulsed -> zero PulseCount, next X/Y-Position, new X-Col
          LASProcess.SetPositionPulseCountActual(0);
          LASProcess.IncrementXPosition();
          if (LASProcess.GetXPositionMaximum() < LASProcess.GetXPositionActual())
          { // new Y-Row
            LASProcess.SetXPositionActual(LASProcess.GetXPositionMinimum());            
            LASProcess.IncrementYPosition();
            if (LASProcess.GetYPositionMaximum() < LASProcess.GetYPositionActual())
            { 
              LedLaser.Off();
              SetState(saPulseMatrixLaserEnd);  
              return;
            }
          }
        }
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
        DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
        DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());  
#endif
      }
  }
}

void CAutomation::HandlePulseMatrixLaserAbort(CSerial &serial)
{  
  LedLaser.Off();
  SetState(saPulseMatrixLaserEnd);  
}

void CAutomation::HandlePulseMatrixLaserEnd(CSerial &serial)
{  
  LedLaser.Off();
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - VariableLaser
//----------------------------------------------------------
// SVL -> Begin -> Busy (wait for PVL or AVL)
void CAutomation::HandlePulseVariableLaserInit(CSerial &serial)
{
  LASProcess.SetTotalPulseCountPreset(0);
  LASProcess.SetTotalPulseCountActual(0);
  LASProcess.SetPositionPulseCountPreset(0);
  LASProcess.SetPositionPulseCountActual(0);
  SetState(saPulseVariableLaserWait); 
}

void CAutomation::HandlePulseVariableLaserWait(CSerial &serial)
{ // check if new PVL-Command is received (Wait -(PVL)-> Busy!):
}

void CAutomation::HandlePulseVariableLaserBegin(CSerial &serial)
{ // check if new PVL-Command is received (Wait -> Busy!):
#if defined(PROCESSOR_ARDUINOMEGA) || defined(PROCESSOR_ARDUINODUE)
  DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
  DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());
#endif  
  LedLaser.SetPulsePeriodCount(LASProcess.GetLaserPulsePeriod(),
                               LASProcess.GetPositionPulseCountPreset());
  LASProcess.SetPositionPulseCountActual(0);
  SetTimeStamp();
  LedLaser.On();
  SetState(saPulseVariableLaserBusy);  
}

void CAutomation::HandlePulseVariableLaserBusy(CSerial &serial)
{ 
  SetTimeMarker();
  if (LedLaser.GetPulsePeriodHalf() <= GetTimePeriod())
  { // Serial.print("DT");
    if (slOff == LedLaser.GetState())
    { // debug Serial.println("Laser On");
      LedLaser.On();        
      SetTimeStamp();
    }
    else
    if (slOn == LedLaser.GetState())
    { // Serial.println("Laser Off");
      LedLaser.Off();
      SetTimeStamp();      
      LASProcess.IncrementTotalPulseCountActual();
      LASProcess.IncrementPositionPulseCountActual();
      if (LASProcess.IsPositionPulseCountReached())
      {
        SetState(saPulseVariableLaserWait);       
      }
    }
  }
}

void CAutomation::HandlePulseVariableLaserAbort(CSerial &serial)
{  
  LedLaser.Off();
  SetState(saPulseVariableLaserEnd);  
}

void CAutomation::HandlePulseVariableLaserEnd(CSerial &serial)
{  
  LedLaser.Off();
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - Collector
//----------------------------------------------------------
//
void CAutomation::Handle(CSerial &serial)
{
  switch (FState)
  { // Idle
    case saIdle:
      HandleIdle(serial);
      break;
    // LedLaser
    case saLedLaserPulseBegin:
      HandleLedLaserPulseBegin(serial);
      break;
    case saLedLaserPulseBusy:
      HandleLedLaserPulseBusy(serial);
      break;
    case saLedLaserPulseEnd:
      HandleLedLaserPulseEnd(serial);
      break;
    // PositionLaser (PulsePositionLaser)
    case saPulsePositionLaserBegin:
      HandlePulsePositionLaserBegin(serial);
      break;
    case saPulsePositionLaserBusy:
      HandlePulsePositionLaserBusy(serial);
      break;
    case saPulsePositionLaserEnd:
      HandlePulsePositionLaserEnd(serial);
      break;
    // MatrixLaser
    case saPulseMatrixLaserBegin:
      HandlePulseMatrixLaserBegin(serial);
      break;
    case saPulseMatrixLaserBusy:
      HandlePulseMatrixLaserBusy(serial);
      break;
    case saPulseMatrixLaserAbort:
      HandlePulseMatrixLaserEnd(serial);
      break;
    case saPulseMatrixLaserEnd:
      HandlePulseMatrixLaserEnd(serial);
      break;
    // VariableLaser
    case saPulseVariableLaserBegin:
      HandlePulseVariableLaserBegin(serial);
      break;
    case saPulseVariableLaserWait:
      HandlePulseVariableLaserWait(serial);
      break;
    case saPulseVariableLaserBusy:
      HandlePulseVariableLaserBusy(serial);
      break;
    case saPulseVariableLaserAbort:
      HandlePulseVariableLaserEnd(serial);
      break;
    case saPulseVariableLaserEnd:
      HandlePulseVariableLaserEnd(serial);
      break;
  }  
}

