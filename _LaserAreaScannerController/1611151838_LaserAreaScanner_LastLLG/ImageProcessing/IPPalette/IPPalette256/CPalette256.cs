﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPPaletteBase;
//
namespace IPPalette256
{
  public abstract class CPalette256 : CPaletteBase
  { //
    //------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------
    //
    //static public Int32 COUNT_COLORENTRIES = 256;
    //static public Int32 INDEX_COLORLOW = 0;
    //static public Int32 INDEX_COLORHIGH = COUNT_COLORENTRIES - 1;
    //static public Color INIT_COLOR_BLACK = Color.FromArgb(0);
    //static public Color INIT_COLOR_WHITE = Color.FromArgb(0xFF, 0xFF, 0xFF);
    //static public EPaletteKind INIT_PALETTEKIND = EPaletteKind.GreyScale;
    //
    //public static String[] PALETTEKINDS =
    //{
    //  "GreyScale",
    //  "GreyScaleInverse",
    //  "RedSection",
    //  "RedSectionBright",
    //  "GreenSection",
    //  "GreenSectionBright",
    //  "BlueSection",
    //  "BlueSectionBright",
    //  "RGBSection",
    //  "RGBSectionBright",
    //  "Spectrum",
    //  "SpectrumBright",
    //  "Rainbow",
    //  "RainbowLow",
    //  "RainbowHigh",
    //  "RainbowHot"
    //};
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    //
    protected EPaletteKind FPalettekind;
    protected String FName;
    protected Color[] FColors;
    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CPalette256(String name, EPaletteKind palettekind)
    {
      FName = name;
      FPalettekind = palettekind;
      FColors = new Color[COUNT_COLORENTRIES];
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //
    private Color GetColor(Int32 index)
    {
      try
      {
        if ((INDEX_COLORLOW <= index) && (index <= INDEX_COLORHIGH))
        {
          return FColors[(Byte)index];
        }
        return INIT_COLOR_BLACK;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return Color.White;
      }
    }
    private void SetColor(Int32 index, Color color)
    {
      try
      {
        if ((INDEX_COLORLOW <= index) && (index <= INDEX_COLORHIGH))
        {
          FColors[index] = color;
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }
    }
    public Color this[Int32 index]
    {
      get
      {
        return GetColor(index);
      }
      set
      {
        SetColor(index, value);
      }
    }
    //
    //------------------------------------------------------------
    //  Segment - Static - Property
    //------------------------------------------------------------
    //
 
    //
    //------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------
    //
    protected abstract Boolean BuildPaletteColors();

    protected Boolean BuildPaletteColorsSection(Byte colorred,
                                                Byte colorgreen,
                                                Byte colorblue,
                                                Byte coloroffset,
                                                Int32 sectioncount)
    {
      Byte ColorRed = colorred;
      Byte ColorGreen = colorgreen;
      Byte ColorBlue = colorblue;
      Byte ColorOffset = coloroffset;
      Int32 SectionCount = Math.Max(2, Math.Min(COUNT_COLORENTRIES / 2, sectioncount));
      Byte ColorSection = (Byte)(COUNT_COLORENTRIES / SectionCount);
      for (Int32 BI = 0; BI < SectionCount; BI++)
      {
        Int32 Value = (Byte)(ColorSection / 2 - 1);
        Value += ColorOffset + BI * ColorSection * (COUNT_COLORENTRIES - ColorOffset) / COUNT_COLORENTRIES;
        Byte Red = (Byte)(Value * ColorRed / COUNT_COLORENTRIES);
        Byte Green = (Byte)(Value * ColorGreen / COUNT_COLORENTRIES);
        Byte Blue = (Byte)(Value * ColorBlue / COUNT_COLORENTRIES);
        for (Int32 CI = BI * ColorSection; CI < (1 + BI) * ColorSection; CI++)
        {
          FColors[CI] = Color.FromArgb(0xFF, Red, Green, Blue);
        }
      }
      return true;
    }

    protected Boolean BuildPaletteColorsRGB(Byte coloroffset,
                                            Int32 sectioncount)
    {
      Byte ColorOffset = coloroffset;
      Int32 SectionCount = Math.Max(2, Math.Min(COUNT_COLORENTRIES / 2, sectioncount));
      Byte ColorSection = (Byte)(COUNT_COLORENTRIES / SectionCount);
      Int32 ColorSelector = 0;
      for (Int32 BI = 0; BI < SectionCount; BI++)
      {
        Int32 Value = (Byte)(ColorSection / 2 - 1);
        Value += ColorOffset + BI * ColorSection * (COUNT_COLORENTRIES - ColorOffset) / COUNT_COLORENTRIES;
        Byte Red = 0;
        Byte Green = 0;
        Byte Blue = 0;
        switch (ColorSelector)
        {
          case 0:
            {
              Red = (Byte)Value;
              ColorSelector++;
              break;
            }
          case 1:
            {
              Green = (Byte)Value;
              ColorSelector++;
              break;
            }
          case 2:
            {
              Blue = (Byte)Value;
              ColorSelector = 0;
              break;
            }
        }
        for (Int32 CI = BI * ColorSection; CI < (1 + BI) * ColorSection; CI++)
        {
          FColors[CI] = Color.FromArgb(0xFF, Red, Green, Blue);
        }
      }
      return true;
    }


    public static CPalette256 CreatePalette256(EPaletteKind palettekind)
    {
      switch (palettekind)
      {
        case EPaletteKind.GreyScale:
          return new CGreyScale();
        case EPaletteKind.GreyScaleInverse:
          return new CGreyScaleInverse();
        case EPaletteKind.RedSection:
          return new CRedSection();
        case EPaletteKind.RedSectionBright:
          return new CRedSectionBright();
        case EPaletteKind.GreenSection:
          return new CGreenSection();
        case EPaletteKind.GreenSectionBright:
          return new CGreenSectionBright();
        case EPaletteKind.BlueSection:
          return new CBlueSection();
        case EPaletteKind.BlueSectionBright:
          return new CBlueSectionBright();
        case EPaletteKind.RGBSection:
          return new CRGBSection();
        case EPaletteKind.RGBSectionBright:
          return new CRGBSectionBright();
        case EPaletteKind.Spectrum:
          return new CSpectrum();
        case EPaletteKind.SpectrumBright:
          return new CSpectrumBright();
        case EPaletteKind.Rainbow:
          return new CRainbow();
        case EPaletteKind.RainbowLow:
          return new CRainbowLow();
        case EPaletteKind.RainbowHigh:
          return new CRainbowHigh();
        case EPaletteKind.RainbowHot:
          return new CRainbowHot();
        default:
          return new CGreyScale();
      }
    }



  }
}
