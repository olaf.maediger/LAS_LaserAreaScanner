﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPPaletteBase;
//
namespace IPPalette256
{
  public class CGreenSectionBright : CPalette256
  {
    //
    //------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------
    //
    protected static String NAME_PALETTE = CPalette256.PALETTEKINDS[(int)EPaletteKind.GreenSectionBright];
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CGreenSectionBright()
      : base(NAME_PALETTE, EPaletteKind.GreenSectionBright)
    {
      BuildPaletteColors();
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------
    //
    protected override Boolean BuildPaletteColors()
    {
      return BuildPaletteColorsSection(0, 255, 0, 128, 8);
    }

  }
}
