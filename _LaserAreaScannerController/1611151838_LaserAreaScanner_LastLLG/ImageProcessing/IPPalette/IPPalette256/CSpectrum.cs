﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPPaletteBase;
//
namespace IPPalette256
{
  public class CSpectrum : CPalette256
  {
    //
    //------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------
    //
    protected static String NAME_PALETTE = CPalette256.PALETTEKINDS[(int)EPaletteKind.Spectrum];
    //
    //------------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------
    //
    public CSpectrum()
      : base(NAME_PALETTE, EPaletteKind.Spectrum)
    {
      BuildPaletteColors();
    }
    //
    //------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------
    //

    //
    //------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------
    //
    protected override Boolean BuildPaletteColors()
    {
      const Int32 PRESET_COUNT = 8;
      Int32 COLOR_SECTION = COUNT_COLORENTRIES / PRESET_COUNT;
      Byte Offset = (Byte)(COLOR_SECTION); // !!!!!!!!!!!!!!!
      Color[] Presets = new Color[PRESET_COUNT];
      for (Int32 CI = 0; CI < Presets.Length; CI++)
      {
        Byte Red = (Byte)(Offset + COLOR_SECTION * (Presets.Length - 1 - CI));
        Byte Green = (Byte)(Offset + COLOR_SECTION * CI);
        Byte Blue = (Byte)(Offset + COLOR_SECTION * (Presets.Length - 1 - CI));
        Presets[CI] = Color.FromArgb(0xFF, Red, Green, Blue);
      }
      //
      Byte ColorOffset = 0;
      Byte ColorSection = (Byte)(COUNT_COLORENTRIES / PRESET_COUNT);
      for (Int32 BI = 0; BI < PRESET_COUNT; BI++)
      {
        Int32 Value = (Byte)(ColorSection / 2 - 1);
        Value += ColorOffset + BI * ColorSection * (COUNT_COLORENTRIES - ColorOffset) / COUNT_COLORENTRIES;
        Value = ColorOffset; // Müll!!!
        Byte Red = (Byte)(Value + Presets[BI].R);
        Byte Green = (Byte)(Value + Presets[BI].G);
        Byte Blue = (Byte)(Value + Presets[BI].B);
        for (Int32 CI = BI * ColorSection; CI < (1 + BI) * ColorSection; CI++)
        {
          FColors[CI] = Color.FromArgb(0xFF, Red, Green, Blue);
        }
      }
      return true;
    }

  }
}
