﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPPaletteBase
{ //
  //------------------------------------------------------------
  //  Segment - Type
  //------------------------------------------------------------
  //
  public enum EPaletteKind
  {
    GreyScale = 0,
    GreyScaleInverse = 1,
    RedSection = 2,
    RedSectionBright = 3,
    GreenSection = 4,
    GreenSectionBright = 5,
    BlueSection = 6,
    BlueSectionBright = 7,
    RGBSection = 8,
    RGBSectionBright = 9,
    Spectrum = 10,
    SpectrumBright = 11,
    Rainbow = 12,
    RainbowLow = 13,
    RainbowHigh = 14,
    RainbowHot = 15
  };
  //
  public class CPaletteBase
  { //
    //------------------------------------------------------------
    //  Segment - Constant
    //------------------------------------------------------------
    //
    static public Int32 COUNT_COLORENTRIES = 256;
    static public Int32 INDEX_COLORLOW = 0;
    static public Int32 INDEX_COLORHIGH = COUNT_COLORENTRIES - 1;
    static public Color INIT_COLOR_BLACK = Color.FromArgb(0);
    static public Color INIT_COLOR_WHITE = Color.FromArgb(0xFF, 0xFF, 0xFF);
    static public EPaletteKind INIT_PALETTEKIND = EPaletteKind.GreyScale;
    //
    public static String[] PALETTEKINDS =
		{
			"GreyScale",
			"GreyScaleInverse",
			"RedSection",
			"RedSectionBright",
			"GreenSection",
			"GreenSectionBright",
			"BlueSection",
			"BlueSectionBright",
			"RGBSection",
			"RGBSectionBright",
			"Spectrum",
			"SpectrumBright",
      "Rainbow",
      "RainbowLow",
      "RainbowHigh",
      "RainbowHot"
		};
    //
    //------------------------------------------------------------
    //  Segment - Static - Property
    //------------------------------------------------------------
    //
    public static EPaletteKind TextToPaletteKind(String palettekind)
    {
      for (EPaletteKind PK = EPaletteKind.GreyScale; PK <= EPaletteKind.RainbowHot; PK++)
      {
        if (palettekind == PK.ToString())
        {
          return PK;
        }
      }
      return INIT_PALETTEKIND;
    }

    public static String PaletteKindToText(EPaletteKind palettekind)
    {
      if ((EPaletteKind.GreyScale <= palettekind) && (palettekind <= EPaletteKind.RainbowHot))
      {
        return PALETTEKINDS[(Int32)palettekind];
      }
      return PALETTEKINDS[(Int32)EPaletteKind.GreyScale];
    }

  }
}
