﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
//
using IPMatrix;
using IPPaletteBase;
using IPPalette256;
//
namespace IPBitmap
{
  public class CBVectorConversion
  {
    //
    //#####################################################################
    //###   VectorByte (no Palette) -> Bitmap... 
    //#####################################################################
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) -> BitmapRgb
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToBitmapRgb(Int32 colcount, Int32 rowcount,
                                                       Byte[] vectorsource,
                                                       out CBitmapRgb bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmapRgb(BW, BH);
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 0);
              VI++;
                // B - 08Bit
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) -> BitmapArgb
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToBitmapArgb(Int32 colcount, Int32 rowcount,
                                                        Byte[] vectorsource,
                                                        out CBitmapArgb bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmapArgb(BW, BH);
        PixelFormat PF = bitmaptarget.PixelFormat;
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 0);
              VI++;
              // B - 08Bit
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) + Palette256 -> Bitmap256 
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToBitmap256(Int32 colcount, Int32 rowcount,
                                                       CPalette256 palette256,
                                                       Byte[] vectorsource,
                                                       out CBitmap256 bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmap256(BW, BH, palette256);
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 0);
              Color C = palette256[BV];
              VI++;
              // B - 08Bit
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#####################################################################
    //###   VectorUInt10 (no Palette) -> Bitmap... 
    //#####################################################################
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) -> BitmapRgb
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToBitmapRgb(Int32 colcount, Int32 rowcount,
                                                         UInt16[] vectorsource,
                                                         out CBitmapRgb bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmapRgb(BW, BH);
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 2);
              VI++;
              // B - 08Bit
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) -> BitmapArgb
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToBitmapArgb(Int32 colcount, Int32 rowcount,
                                                          UInt16[] vectorsource,
                                                          out CBitmapArgb bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmapArgb(BW, BH);
        PixelFormat PF = bitmaptarget.PixelFormat;
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 2);
              VI++;
              // B - 08Bit
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) + Palette -> Bitmap256 
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToBitmap256(Int32 colcount, Int32 rowcount,
                                                         UInt16[] vectorsource,
                                                         CPalette256 palette256,
                                                         out CBitmap256 bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmap256(BW, BH, palette256);
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 2);
              Color C = palette256[BV];
              VI++;
              // B - 08Bit
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#####################################################################
    //###   VectorUInt12 (no Palette) -> Bitmap... 
    //#####################################################################
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 (no Palette) -> BitmapRgb
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12Palette256ToBitmapRgb(Int32 colcount, Int32 rowcount,
                                                                   UInt16[] vectorsource,
                                                                   out CBitmapRgb bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmapRgb(BW, BH);
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 4);
              VI++;
              // B - 08Bit
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 (no Palette) -> BitmapArgb
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12Palette256ToBitmapArgb(Int32 colcount, Int32 rowcount,
                                                                    UInt16[] vectorsource,
                                                                    out CBitmapArgb bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmapArgb(BW, BH);
        PixelFormat PF = bitmaptarget.PixelFormat;
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 4);
              VI++;
              // B - 08Bit
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 (no Palette) + Palette -> Bitmap256
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12ToBitmap256(Int32 colcount, Int32 rowcount,
                                                         UInt16[] vectorsource,
                                                         CPalette256 palette256,
                                                         out CBitmap256 bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmap256(BW, BH, palette256);
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 4);
              Color C = palette256[BV];
              VI++;
              // B - 08Bit
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#####################################################################
    //###   VectorUInt16 (no Palette) -> Bitmap... 
    //#####################################################################
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) -> BitmapRgb
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToBitmapRgb(Int32 colcount, Int32 rowcount,
                                                         UInt16[] vectorsource,
                                                         out CBitmapRgb bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmapRgb(BW, BH);
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 8);
              VI++;
              // B - 08Bit
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) -> BitmapArgb
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToBitmapArgb(Int32 colcount, Int32 rowcount,
                                                          UInt16[] vectorsource,
                                                          out CBitmapArgb bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmapArgb(BW, BH);
        PixelFormat PF = bitmaptarget.PixelFormat;
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 8);
              VI++;
              // B - 08Bit
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) + Palette -> Bitmap256
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToBitmap256(Int32 colcount, Int32 rowcount,
                                                         UInt16[] vectorsource,
                                                         CPalette256 palette256,
                                                         out CBitmap256 bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmaptarget = new CBitmap256(BW, BH, palette256);
        BitmapData BD = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vectorsource[VI] >> 8);
              Color C = palette256[BV];
              VI++;
              // B - 08Bit
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmaptarget.Bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Public Method - Conversion VectorUInt16 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToBitmap24bppRgb(Int32 colcount,
                                                              Int32 rowcount,
                                                              UInt16[] vector,
                                                              out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = colcount;
        Int32 RC = rowcount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          Int32 VI = 0;
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            {
              Byte BV = (Byte)(vector[VI] >> 8);
              VI++;
              // B - 08Bit
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
  }
}
