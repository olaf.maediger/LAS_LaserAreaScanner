﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace IPBitmap
{
  public delegate void DOnBitmapRead(Bitmap bitmap);
  public delegate void DOnBitmapRgbRead(CBitmapRgb bitmaprgb);
  public delegate void DOnBitmapArgbRead(CBitmapArgb bitmapargb);
  public delegate void DOnBitmap256Read(CBitmap256 bitmap256);
  //
  public delegate void DOnBitmapWrite(Bitmap bitmap);
  public delegate void DOnBitmapRgbWrite(CBitmapRgb bitmaprgb);
  public delegate void DOnBitmapArgbWrite(CBitmapArgb bitmapargb);
  public delegate void DOnBitmap256Write(CBitmap256 bitmap256);
}
