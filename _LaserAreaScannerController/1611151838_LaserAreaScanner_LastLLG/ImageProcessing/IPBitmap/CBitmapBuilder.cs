﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPMatrix;
using IPPaletteBase;
using IPPalette256;
//
namespace IPBitmap
{
  public class CBitmapBuilder
  {
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constant
    //-----------------------------------------------------------------------------
    //
    public static Boolean CreateBitmap08BitConstant(Int32 width, Int32 height,
                                                    Int32 constant,
                                                    out CMatrix08Bit matrix,
                                                    out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixConstant(width, height, constant, out matrix);
      matrix.Name = "Matrix08BitConstant";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }

    public static Boolean CreateBitmap10BitConstant(Int32 width, Int32 height,
                                                    Int32 constant,
                                                    out CMatrix10Bit matrix,
                                                    out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixConstant(width, height, constant, out matrix);
      matrix.Name = "Matrix10BitConstant";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }

    public static Boolean CreateBitmap12BitConstant(Int32 width, Int32 height,
                                                    Int32 constant,
                                                    out CMatrix12Bit matrix,
                                                    out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixConstant(width, height, constant, out matrix);
      matrix.Name = "Matrix12BitConstant";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Linear
    //-----------------------------------------------------------------------------
    //
    public static Boolean CreateBitmap08BitLinear(Int32 width, Int32 height,
                                                  Int32 minimumx, Int32 maximumx,
                                                  Int32 minimumy, Int32 maximumy,
                                                  out CMatrix08Bit matrix,
                                                  out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixLinear(width, height, minimumx, maximumx, minimumy, maximumy, out matrix);
      matrix.Name = "Matrix08BitLinear";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }

    public static Boolean CreateBitmap10BitLinear(Int32 width, Int32 height,
                                                  Int32 minimumx, Int32 maximumx,
                                                  Int32 minimumy, Int32 maximumy,
                                                  out CMatrix10Bit matrix,
                                                  out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixLinear(width, height,  minimumx, maximumx, minimumy, maximumy, out matrix);
      matrix.Name = "Matrix10BitLinear";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }

    public static Boolean CreateBitmap12BitLinear(Int32 width, Int32 height,
                                                  Int32 minimumx, Int32 maximumx,
                                                  Int32 minimumy, Int32 maximumy,
                                                  out CMatrix12Bit matrix,
                                                  out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixLinear(width, height, minimumx, maximumx, minimumy, maximumy, out matrix);
      matrix.Name = "Matrix12BitLinear";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Circular
    //-----------------------------------------------------------------------------
    //
    public static Boolean CreateBitmap08BitCircular(Int32 width, Int32 height,
                                                    Int32 minimum, Int32 maximum,
                                                    out CMatrix08Bit matrix,
                                                    out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixCircular(width, height, minimum, maximum, out matrix);
      matrix.Name = "Matrix08BitCircular";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }

    public static Boolean CreateBitmap10BitCircular(Int32 width, Int32 height,
                                                    Int32 minimum, Int32 maximum,
                                                    out CMatrix10Bit matrix,
                                                    out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixCircular(width, height, minimum, maximum, out matrix);
      matrix.Name = "Matrix10BitCircular";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }

    public static Boolean CreateBitmap12BitCircular(Int32 width, Int32 height,
                                                    Int32 minimum, Int32 maximum,
                                                    out CMatrix12Bit matrix,
                                                    out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixCircular(width, height, minimum, maximum, out matrix);
      matrix.Name = "Matrix12BitCircular";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Noise
    //-----------------------------------------------------------------------------
    //
    public static Boolean CreateBitmap08BitNoise(Int32 width, Int32 height,
                                                 Int32 minimum, Int32 maximum,
                                                 out CMatrix08Bit matrix,
                                                 out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixNoise(width, height, minimum, maximum, out matrix);
      matrix.Name = "Matrix08BitNoise";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }

    public static Boolean CreateBitmap10BitNoise(Int32 width, Int32 height,
                                                 Int32 minimum, Int32 maximum,
                                                 out CMatrix10Bit matrix,
                                                 out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixNoise(width, height, minimum, maximum, out matrix);
      matrix.Name = "Matrix10BitNoise";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }

    public static Boolean CreateBitmap12BitNoise(Int32 width, Int32 height,
                                                 Int32 minimum, Int32 maximum,
                                                 out CMatrix12Bit matrix,
                                                 out CBitmap256 bitmap)
    {
      CMatrixBuilder.CreateMatrixNoise(width, height, minimum, maximum, out matrix);
      matrix.Name = "Matrix12BitNoise";
      bitmap = new CBitmap256(EPaletteKind.RGBSection, matrix);
      return true;
    }

  
  }
}
