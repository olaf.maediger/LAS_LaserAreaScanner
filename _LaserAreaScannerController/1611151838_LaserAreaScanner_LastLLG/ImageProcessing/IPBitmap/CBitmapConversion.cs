﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
//
using IPMatrix;
using IPPaletteBase;
using IPPalette256;
//
namespace IPBitmap
{
//
//#####################################################################
//### Matrix... -> Bitmap... NO Palette
//#####################################################################
//
  public class CBitmapConversion
  { //
    //---------------------------------------------------------------------
    //  Section - Public Method - Conversion Matrix08Bit -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrix08BitToBitmap24bppRgb(CMatrix08Bit matrix,
                                                             out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix08BitToBitmap32bppArgb(CMatrix08Bit matrix,
                                                              out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Public Method - Conversion Matrix10Bit -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrix10BitToBitmap24bppRgb(CMatrix10Bit matrix,
                                                             out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix10BitToBitmap32bppArgb(CMatrix10Bit matrix,
                                                              out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Public Method - Conversion Matrix12Bit -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrix12BitToBitmap24bppRgb(CMatrix12Bit matrix,
                                                             out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix12BitToBitmap32bppArgb(CMatrix12Bit matrix,
                                                              out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Public Method - Conversion Matrix16Bit -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrix16BitToBitmap24bppRgb(CMatrix16Bit matrix,
                                                             out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix16BitToBitmap32bppArgb(CMatrix16Bit matrix,
                                                              out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Public Method - Conversion MatrixByte -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrixByteToBitmap24bppRgb(Byte[,] matrix,
                                                            out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 0);
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixByteToBitmap32bppArgb(Byte[,] matrix,
                                                             out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 0);
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Public Method - Conversion MatrixUInt10 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrixUInt10ToBitmap24bppRgb(UInt16[,] matrix,
                                                              out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 2);
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixUInt10ToBitmap32bppArgb(UInt16[,] matrix,
                                                               out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 2);
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Public Method - Conversion MatrixUInt12 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrixUInt12ToBitmap24bppRgb(UInt16[,] matrix,
                                                              out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 4);
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixUInt12ToBitmap32bppArgb(UInt16[,] matrix,
                                                               out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 4);
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Public Method - Conversion MatrixUInt16 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrixUInt16ToBitmap24bppRgb(UInt16[,] matrix,
                                                              out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 8);
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixUInt16ToBitmap32bppArgb(UInt16[,] matrix,
                                                               out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.ReadOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 8);
              *PB = BV;
              PB++;
              // G - 16Bit
              *PB = BV;
              PB++;
              // R - 24Bit
              *PB = BV;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#####################################################################
    //### Bitmap... -> Matrix...
    //#####################################################################
    //
    //------------------------------------------------------------------------------
    //  Section - Public Method - Conversion Bitmap... -> Matrix08Bit/10Bit/12Bit
    //------------------------------------------------------------------------------
    //
    public static Boolean ConvertBitmapToMatrix08Bit(Bitmap bitmap, out CMatrix08Bit matrix)
    {
      matrix = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 08Bit
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new CMatrix08Bit(WT, HT);
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix.SetValue08Bit(CI, RI, II);
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 24bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 08Bit
            WT = WS;
            HT = HS;
            matrix = new CMatrix08Bit(WT, HT);
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix.SetValue08Bit(CI, RI, II);
                  PBS++;
                  // A - 32Bit
                  // ignore value!
                  PBS++; 
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmapToMatrix10Bit(Bitmap bitmap, out CMatrix10Bit matrix)
    {
      matrix = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 08Bit
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new CMatrix10Bit(WT, HT);
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix.SetValue08Bit(CI, RI, II);
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 24bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 08Bit
            WT = WS;
            HT = HS;
            matrix = new CMatrix10Bit(WT, HT);
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix.SetValue08Bit(CI, RI, II);
                  PBS++;
                  // A - 32Bit
                  // ignore value!
                  PBS++;
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmapToMatrix12Bit(Bitmap bitmap, out CMatrix12Bit matrix)
    {
      matrix = null;
      try
      {
        float FR = CMatrixBase.FACTOR_RED;
        float FG = CMatrixBase.FACTOR_GREEN;
        float FB = CMatrixBase.FACTOR_BLUE;
        switch (bitmap.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 12Bit
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new CMatrix12Bit(WT, HT);
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix.SetValue08Bit(CI, RI, II);
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 24bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 12Bit
            WT = WS;
            HT = HS;
            matrix = new CMatrix12Bit(WT, HT);
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix.SetValue08Bit(CI, RI, II);
                  PBS++;
                  // A - 32Bit
                  // ignore value!
                  PBS++;
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmapToMatrix16Bit(Bitmap bitmap, out CMatrix16Bit matrix)
    {
      matrix = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 16Bit
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new CMatrix16Bit(WT, HT);
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix.SetValue08Bit(CI, RI, II);
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 24bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 16Bit
            WT = WS;
            HT = HS;
            matrix = new CMatrix16Bit(WT, HT);
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte Grey = (Byte)(FR * R + 0.587 * G + 0.114 * B);
                  matrix.SetValue08Bit(CI, RI, Grey);
                  PBS++;
                  // A - 32Bit
                  // ignore value!
                  PBS++;
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmapToMatrixByte(Bitmap bitmap, out Byte[,] matrix)
    {
      matrix = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target Byte
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new Byte[WT, HT];
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix[CI, RI] = (Byte)(II << 0);
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 24bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 16Bit
            WT = WS;
            HT = HS;
            matrix = new Byte[WT, HT];
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix[CI, RI] = (Byte)(II << 0);
                  PBS++;
                  // A - 32Bit
                  // ignore value!
                  PBS++;
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmapToMatrixUInt10(Bitmap bitmap, out UInt16[,] matrix)
    {
      matrix = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target Byte
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix[CI, RI] = (Byte)(II << 2);
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 24bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 16Bit
            WT = WS;
            HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix[CI, RI] = (Byte)(II << 2);
                  PBS++;
                  // A - 32Bit
                  // ignore value!
                  PBS++;
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmapToMatrixUInt12(Bitmap bitmap, out UInt16[,] matrix)
    {
      matrix = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target Byte
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix[CI, RI] = (Byte)(II << 4);
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 24bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 16Bit
            WT = WS;
            HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix[CI, RI] = (Byte)(II << 4);
                  PBS++;
                  // A - 32Bit
                  // ignore value!
                  PBS++;
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmapToMatrixUInt16(Bitmap bitmap, out UInt16[,] matrix)
    {
      matrix = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target Byte
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix[CI, RI] = (Byte)(II << 8);
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 24bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 16Bit
            WT = WS;
            HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  float B = (float)(*PBS);
                  PBS++;
                  // G - 16Bit
                  float G = (float)(*PBS);
                  PBS++;
                  // R - 24Bit
                  float R = (float)(*PBS);
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  matrix[CI, RI] = (Byte)(II << 8);
                  PBS++;
                  // A - 32Bit
                  // ignore value!
                  PBS++;
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#####################################################################
    //### Bitmap... -> Bitmap... NO Palette
    //#####################################################################
    //
    //-----------------------------------------------------------
    //  Section - Conversion Bitmap -> Bitmap24bppRgb/32bppArgb
    //-----------------------------------------------------------
    //
    public static Boolean ConvertBitmapToBitmap24bppRgb(Bitmap bitmapsource,
                                                         out Bitmap bitmaptarget)
    {
      bitmaptarget = null;
      try
      {
        switch (bitmapsource.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmapsource.Width;
            Int32 HS = bitmapsource.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 24bpp
            Int32 WT = WS;
            Int32 HT = HS;
            PixelFormat PFT = PixelFormat.Format24bppRgb;
            bitmaptarget = new Bitmap(WT, HT, PFT);
            //
            BitmapData BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BitmapData BDT = bitmaptarget.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.ReadOnly, PFT);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // G - 16Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // R - 24Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // A - 32Bit
                  // NC !!! 24bit *PBT = 0xFF;
                  // NC !!! 24bit PBS++; 
                  // NC !!! 24bit PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.UnlockBits(BDT);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmapsource.Width;
            HS = bitmapsource.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 24bpp
            WT = WS;
            HT = HS;
            PFT = PixelFormat.Format24bppRgb;
            bitmaptarget = new Bitmap(WT, HT, PFT);
            //
            BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BDT = bitmaptarget.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.ReadOnly, PFT);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // G - 16Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // R - 24Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // A - 32Bit
                  // NC *PBT = 0xFF;
                  PBS++;
                  // NC PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.UnlockBits(BDT);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmapToBitmap32bppArgb(Bitmap bitmapsource,
                                                         out Bitmap bitmaptarget)
    {
      bitmaptarget = null;
      try
      {
        switch (bitmapsource.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmapsource.Width;
            Int32 HS = bitmapsource.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 32bpp
            Int32 WT = WS;
            Int32 HT = HS;
            PixelFormat PFT = PixelFormat.Format32bppRgb;
            bitmaptarget = new Bitmap(WT, HT, PFT);
            //
            BitmapData BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BitmapData BDT = bitmaptarget.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.ReadOnly, PFT);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // G - 16Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // R - 24Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // A - 32Bit
                  *PBT = 0xFF;
                  PBT++;
                  // NC !!! 24bit PBS++; 
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.UnlockBits(BDT);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmapsource.Width;
            HS = bitmapsource.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 32bpp
            WT = WS;
            HT = HS;
            PFT = PixelFormat.Format32bppRgb;
            bitmaptarget = new Bitmap(WT, HT, PFT);
            //
            BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BDT = bitmaptarget.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.ReadOnly, PFT);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // G - 16Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // R - 24Bit
                  *PBT = *PBS;
                  PBS++; PBT++;
                  // A - 32Bit
                  *PBT = 0xFF;
                  PBS++;
                  PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.UnlockBits(BDT);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }




    //
    //------------------------------------------------------------------------------
    //  Section - Public Method - Conversion Bitmap256 -> Bitmap24bppRgb/32bppArgb
    //------------------------------------------------------------------------------
    //
    public static Boolean ConvertBitmap256ToBitmap24bppRgb(CBitmap256 bitmapsource,
                                                           out Bitmap bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source 32bpp
        Int32 WS = bitmapsource.Width;
        Int32 HS = bitmapsource.Height;
        PixelFormat PFS = PixelFormat.Format32bppRgb;
        Bitmap BS = bitmapsource.Bitmap;
        // Target 24bpp
        Int32 WT = WS;
        Int32 HT = HS;
        PixelFormat PFT = PixelFormat.Format24bppRgb;
        bitmaptarget = new Bitmap(WT, HT, PFT);
        //
        BitmapData BDS = BS.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
        BitmapData BDT = bitmaptarget.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.ReadOnly, PFT);
        //
        Int32 CIL = 0;
        Int32 CIH = WS - 1;
        Int32 RIL = 0;
        Int32 RIH = HS - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
            Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              *PBT = *PBS;
              PBS++; PBT++;
              // G - 16Bit
              *PBT = *PBS;
              PBS++; PBT++;
              // R - 24Bit
              *PBT = *PBS;
              PBS++; PBT++;
              // A - 32Bit
              // 24bit! *PBT = 0xFF; 
              PBS++;
              // PBT++;
            }
          }
        }
        BS.UnlockBits(BDS);
        bitmaptarget.UnlockBits(BDT);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmap256ToBitmap32bppArgb(CBitmap256 bitmapsource,
                                                            out Bitmap bitmaptarget)
    {
      bitmaptarget = null;
      try
      { // Source 32bpp
        Int32 WS = bitmapsource.Width;
        Int32 HS = bitmapsource.Height;
        PixelFormat PFS = PixelFormat.Format32bppRgb;
        Bitmap BS = bitmapsource.Bitmap;
        // Target 32bpp
        Int32 WT = WS;
        Int32 HT = HS;
        PixelFormat PFT = PixelFormat.Format32bppArgb;
        bitmaptarget = new Bitmap(WT, HT, PFT);
        //
        BitmapData BDS = BS.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
        BitmapData BDT = bitmaptarget.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.ReadOnly, PFT);
        //
        Int32 CIL = 0;
        Int32 CIH = WS - 1;
        Int32 RIL = 0;
        Int32 RIH = HS - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
            Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              *PBT = *PBS;
              PBS++; PBT++;
              // G - 16Bit
              *PBT = *PBS;
              PBS++; PBT++;
              // R - 24Bit
              *PBT = *PBS;
              PBS++; PBT++;
              // A - 32Bit
              *PBT = 0xFF; // *PBS
              PBS++;
              PBT++;
            }
          }
        }
        BS.UnlockBits(BDS);
        bitmaptarget.UnlockBits(BDT);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }  





  }
}
