﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
//
using IPMatrix;
using IPPaletteBase;
using IPPalette256;
//
namespace IPBitmap
{
  public class CBitmapConversionPalette
  { //
    //#####################################################################
    //###   Matrix... -> Bitmap... WITH Palette
    //#####################################################################
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion Matrix08Bit + Palette256 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrix08BitPalette256ToBitmap24bppRgb(CMatrix08Bit matrix,
                                                                       CPalette256 palette256,
                                                                       out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix08BitPalette256ToBitmap32bppArgb(CMatrix08Bit matrix,
                                                                        CPalette256 palette256,
                                                                        out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Conversion Matrix10Bit + Palette256 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrix10BitPalette256ToBitmap24bppRgb(CMatrix10Bit matrix,
                                                                       CPalette256 palette256,
                                                                       out Bitmap bitmap)
    {
      bitmap = null;
      try
      {
        // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix10BitPalette256ToBitmap32bppArgb(CMatrix10Bit matrix,
                                                                        CPalette256 palette256,
                                                                        out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Conversion Matrix12Bit + Palette256 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrix12BitPalette256ToBitmap24bppRgb(CMatrix12Bit matrix,
                                                                       CPalette256 palette256,
                                                                       out Bitmap bitmap)
    {
      bitmap = null;
      try
      {
        // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix12BitPalette256ToBitmap32bppArgb(CMatrix12Bit matrix,
                                                                        CPalette256 palette256,
                                                                        out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Section - Conversion Matrix16Bit + Palette256 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrix16BitPalette256ToBitmap24bppRgb(CMatrix16Bit matrix,
                                                                       CPalette256 palette256,
                                                                       out Bitmap bitmap)
    {
      bitmap = null;
      try
      {
        // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrix16BitPalette256ToBitmap32bppArgb(CMatrix16Bit matrix,
                                                                        CPalette256 palette256,
                                                                        out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix.GetValue08Bit(CI, RI));
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion MatrixByte + Palette256 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrixBytePalette256ToBitmap24bppRgb(Byte[,] matrix,
                                                                      CPalette256 palette256,
                                                                      out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 0);
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixBytePalette256ToBitmap32bppArgb(Byte[,] matrix,
                                                                       CPalette256 palette256,
                                                                       out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 0);
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion MatrixUInt10 + Palette256 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrixUInt10Palette256ToBitmap24bppRgb(UInt16[,] matrix,
                                                                        CPalette256 palette256,
                                                                        out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 2);
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixUInt10Palette256ToBitmap32bppArgb(UInt16[,] matrix,
                                                                         CPalette256 palette256,
                                                                         out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 2);
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion MatrixUInt12 + Palette256 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrixUInt12Palette256ToBitmap24bppRgb(UInt16[,] matrix,
                                                                        CPalette256 palette256,
                                                                        out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 4);
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixUInt12Palette256ToBitmap32bppArgb(UInt16[,] matrix,
                                                                         CPalette256 palette256,
                                                                         out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 4);
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion MatrixUInt16 + Palette256 -> Bitmap...
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertMatrixUInt16Palette256ToBitmap24bppRgb(UInt16[,] matrix,
                                                                        CPalette256 palette256,
                                                                        out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format24bppRgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 8);
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // NC A - 32Bit
              //*PB = 0xFF;
              //PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertMatrixUInt16Palette256ToBitmap32bppArgb(UInt16[,] matrix,
                                                                         CPalette256 palette256,
                                                                         out Bitmap bitmap)
    {
      bitmap = null;
      try
      { // Source
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        // Destination
        PixelFormat PF = PixelFormat.Format32bppArgb;
        Int32 BW = CC;
        Int32 BH = RC;
        bitmap = new Bitmap(BW, BH, PF);
        BitmapData BD = bitmap.LockBits(new Rectangle(0, 0, BW, BH), ImageLockMode.WriteOnly, PF);
        //
        Int32 CIL = 0;
        Int32 CIH = BW - 1;
        Int32 RIL = 0;
        Int32 RIH = BH - 1;
        unsafe
        {
          for (Int32 RI = RIL; RI <= RIH; RI++)
          {
            Byte* PB = (Byte*)(BD.Scan0 + RI * BD.Stride);
            for (Int32 CI = CIL; CI <= CIH; CI++)
            { // B - 08Bit
              Byte BV = (Byte)(matrix[CI, RI] >> 8);
              Color C = palette256[BV];
              *PB = C.B;
              PB++;
              // G - 16Bit
              *PB = C.G;
              PB++;
              // R - 24Bit
              *PB = C.R;
              PB++;
              // A - 32Bit
              *PB = 0xFF;
              PB++;
            }
          }
        }
        bitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#####################################################################
    //###   Bitmap + Palette256 -> Matrix... 
    //#####################################################################
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion Bitmap + Palette256 -> Matrix08Bit
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertBitmapPalette256ToMatrix08Bit(Bitmap bitmap,
                                                               CPalette256 palette256,
                                                               out CMatrix08Bit matrix)
    {
      matrix = null;
      const float FR = CMatrixBase.FACTOR_RED;
      const float FG = CMatrixBase.FACTOR_GREEN;
      const float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 08Bit
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new CMatrix08Bit(WT, HT);
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix.SetValue08Bit(CI, RI, II);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 32bpp
            WT = WS;
            HT = HS;
            matrix = new CMatrix08Bit(WT, HT);
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix.SetValue08Bit(CI, RI, II);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;    
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion Bitmap + Palette256 -> Matrix10Bit
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertBitmapPalette256ToMatrix10Bit(Bitmap bitmap,
                                                               CPalette256 palette256,
                                                               out CMatrix10Bit matrix)
    {
      matrix = null;
      const float FR = CMatrixBase.FACTOR_RED;
      const float FG = CMatrixBase.FACTOR_GREEN;
      const float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 08Bit
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new CMatrix10Bit(WT, HT);
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix.SetValue08Bit(CI, RI, II);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 32bpp
            WT = WS;
            HT = HS;
            matrix = new CMatrix10Bit(WT, HT);
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix.SetValue08Bit(CI, RI, II);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion Bitmap + Palette256 -> Matrix12Bit
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertBitmapPalette256ToMatrix12Bit(Bitmap bitmap,
                                                               CPalette256 palette256,
                                                               out CMatrix12Bit matrix)
    {
      matrix = null;
      const float FR = CMatrixBase.FACTOR_RED;
      const float FG = CMatrixBase.FACTOR_GREEN;
      const float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 08Bit
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new CMatrix12Bit(WT, HT);
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix.SetValue08Bit(CI, RI, II);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 32bpp
            WT = WS;
            HT = HS;
            matrix = new CMatrix12Bit(WT, HT);
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix.SetValue08Bit(CI, RI, II);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion Bitmap + Palette256 -> Matrix16Bit
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertBitmapPalette256ToMatrix16Bit(Bitmap bitmap,
                                                               CPalette256 palette256,
                                                               out CMatrix16Bit matrix)
    {
      matrix = null;
      const float FR = CMatrixBase.FACTOR_RED;
      const float FG = CMatrixBase.FACTOR_GREEN;
      const float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 08Bit
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new CMatrix16Bit(WT, HT);
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix.SetValue08Bit(CI, RI, II);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 32bpp
            WT = WS;
            HT = HS;
            matrix = new CMatrix16Bit(WT, HT);
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix.SetValue08Bit(CI, RI, II);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion Bitmap + Palette256 -> MatrixByte
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertBitmapPalette256ToMatrixByte(Bitmap bitmap,
                                                              CPalette256 palette256,
                                                              out Byte[,] matrix)
    {
      matrix = null;
      const float FR = CMatrixBase.FACTOR_RED;
      const float FG = CMatrixBase.FACTOR_GREEN;
      const float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target Byte
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new Byte[WT, HT];
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix[CI, RI] = (Byte)(II << 0);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target Byte
            WT = WS;
            HT = HS;
            matrix = new Byte[WT, HT];
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix[CI, RI] = (Byte)(II << 0);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion Bitmap + Palette256 -> MatrixUInt10
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertBitmapPalette256ToMatrixUInt10(Bitmap bitmap,
                                                                CPalette256 palette256,
                                                                out UInt16[,] matrix)
    {
      matrix = null;
      const float FR = CMatrixBase.FACTOR_RED;
      const float FG = CMatrixBase.FACTOR_GREEN;
      const float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target UInt10
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix[CI, RI] = (UInt16)(II << 2);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target UInt10
            WT = WS;
            HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix[CI, RI] = (UInt16)(II << 2);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion Bitmap + Palette256 -> MatrixUInt12
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertBitmapPalette256ToMatrixUInt12(Bitmap bitmap,
                                                                CPalette256 palette256,
                                                                out UInt16[,] matrix)
    {
      matrix = null;
      const float FR = CMatrixBase.FACTOR_RED;
      const float FG = CMatrixBase.FACTOR_GREEN;
      const float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target UInt10
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix[CI, RI] = (UInt16)(II << 4);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target UInt10
            WT = WS;
            HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix[CI, RI] = (UInt16)(II << 4);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //    
    //---------------------------------------------------------------------
    //  Section - Conversion Bitmap + Palette256 -> MatrixUInt16
    //---------------------------------------------------------------------
    //
    public static Boolean ConvertBitmapPalette256ToMatrixUInt16(Bitmap bitmap,
                                                                CPalette256 palette256,
                                                                out UInt16[,] matrix)
    {
      matrix = null;
      const float FR = CMatrixBase.FACTOR_RED;
      const float FG = CMatrixBase.FACTOR_GREEN;
      const float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmap.Width;
            Int32 HS = bitmap.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target UInt10
            Int32 WT = WS;
            Int32 HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BitmapData BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix[CI, RI] = (UInt16)(II << 8);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
          case PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmap.Width;
            HS = bitmap.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target UInt10
            WT = WS;
            HT = HS;
            matrix = new UInt16[WT, HT];
            //
            BDS = bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  II = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
                  matrix[CI, RI] = (UInt16)(II << 8);
                }
              }
            }
            bitmap.UnlockBits(BDS);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //#####################################################################
    //###   Bitmap -> Bitmap... WITH Palette
    //#####################################################################
    //    
    public static Boolean ConvertBitmapPalette256ToBitmap24bppRgb(Bitmap bitmapsource,
                                                                  CPalette256 palette256,
                                                                  out Bitmap bitmaptarget)
    {
      bitmaptarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmapsource.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmapsource.Width;
            Int32 HS = bitmapsource.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 32bpp
            Int32 WT = WS;
            Int32 HT = HS;
            PixelFormat PFT = PixelFormat.Format32bppArgb;
            bitmaptarget = new Bitmap(WT, HT, PFT);
            //
            BitmapData BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BitmapData BDT = bitmaptarget.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.WriteOnly, PFT);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  // B - 08Bit
                  *PBT = CV.B;
                  PBT++;
                  // G - 16Bit
                  *PBT = CV.G;
                  PBT++;
                  // R - 24Bit
                  *PBT = CV.R;
                  PBT++;
                  // A - 32Bit
                  // NC *PBT = 0xFF;
                  // NC PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.UnlockBits(BDT);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmapsource.Width;
            HS = bitmapsource.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 32bpp
            WT = WS;
            HT = HS;
            PFT = PixelFormat.Format32bppArgb;
            bitmaptarget = new Bitmap(WT, HT, PFT);
            //
            BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BDT = bitmaptarget.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.WriteOnly, PFT);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  // B - 08Bit
                  *PBT = CV.B;
                  PBT++;
                  // G - 16Bit
                  *PBT = CV.G;
                  PBT++;
                  // R - 24Bit
                  *PBT = CV.R;
                  PBT++;
                  // A - 32Bit
                  // NC *PBT = 0xFF;
                  // NC PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.UnlockBits(BDT);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmapPalette256ToBitmap32bppArgb(Bitmap bitmapsource,
                                                                    CPalette256 palette256,
                                                                    out Bitmap bitmaptarget)
    {
      bitmaptarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmapsource.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmapsource.Width;
            Int32 HS = bitmapsource.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 32bpp
            Int32 WT = WS;
            Int32 HT = HS;
            PixelFormat PFT = PixelFormat.Format32bppArgb;
            bitmaptarget = new Bitmap(WT, HT, PFT);
            //
            BitmapData BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BitmapData BDT = bitmaptarget.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.WriteOnly, PFT);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  // B - 08Bit
                  *PBT = CV.B;
                  PBT++;
                  // G - 16Bit
                  *PBT = CV.G;
                  PBT++;
                  // R - 24Bit
                  *PBT = CV.R;
                  PBT++;
                  // A - 32Bit
                  *PBT = 0xFF;
                  PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.UnlockBits(BDT);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmapsource.Width;
            HS = bitmapsource.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 32bpp
            WT = WS;
            HT = HS;
            PFT = PixelFormat.Format32bppArgb;
            bitmaptarget = new Bitmap(WT, HT, PFT);
            //
            BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BDT = bitmaptarget.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.WriteOnly, PFT);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  // B - 08Bit
                  *PBT = CV.B;
                  PBT++;
                  // G - 16Bit
                  *PBT = CV.G;
                  PBT++;
                  // R - 24Bit
                  *PBT = CV.R;
                  PBT++;
                  // A - 32Bit
                  *PBT = 0xFF;
                  PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.UnlockBits(BDT);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    
    public static Boolean ConvertBitmapPalette256ToBitmapRgb(Bitmap bitmapsource,
                                                             CPalette256 palette256,
                                                             out CBitmapRgb bitmaptarget)
    {
      bitmaptarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmapsource.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmapsource.Width;
            Int32 HS = bitmapsource.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 32bpp
            Int32 WT = WS;
            Int32 HT = HS;
            PixelFormat PFT = PixelFormat.Format24bppRgb;
            bitmaptarget = new CBitmapRgb(WT, HT);
            //
            BitmapData BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BitmapData BDT = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.WriteOnly, PFT);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  // B - 08Bit
                  *PBT = CV.B;
                  PBT++;
                  // G - 16Bit
                  *PBT = CV.G;
                  PBT++;
                  // R - 24Bit
                  *PBT = CV.R;
                  PBT++;
                  // A - 32Bit
                  // NC *PBT = 0xFF;
                  // NC PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.Bitmap.UnlockBits(BDT);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmapsource.Width;
            HS = bitmapsource.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 32bpp
            WT = WS;
            HT = HS;
            PFT = PixelFormat.Format32bppArgb;
            bitmaptarget = new CBitmapRgb(WT, HT);
            //
            BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BDT = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.WriteOnly, PFT);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++; // NC from Source
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  // B - 08Bit
                  *PBT = CV.B;
                  PBT++;
                  // G - 16Bit
                  *PBT = CV.G;
                  PBT++;
                  // R - 24Bit
                  *PBT = CV.R;
                  PBT++;
                  // A - 32Bit
                  // NC *PBT = 0xFF;
                  // NC PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.Bitmap.UnlockBits(BDT);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }
    
    public static Boolean ConvertBitmapPalette256ToBitmapArgb(Bitmap bitmapsource,
                                                              CPalette256 palette256,
                                                              out CBitmapArgb bitmaptarget)
    {
      bitmaptarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmapsource.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmapsource.Width;
            Int32 HS = bitmapsource.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 32bpp
            Int32 WT = WS;
            Int32 HT = HS;
            bitmaptarget = new CBitmapArgb(WT, HT);
            PixelFormat PFT = bitmaptarget.PixelFormat; // 32bppArgb
            //
            BitmapData BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BitmapData BDT = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.WriteOnly, PFT);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  // B - 08Bit
                  *PBT = CV.B;
                  PBT++;
                  // G - 16Bit
                  *PBT = CV.G;
                  PBT++;
                  // R - 24Bit
                  *PBT = CV.R;
                  PBT++;
                  // A - 32Bit
                  *PBT = 0xFF;
                  PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.Bitmap.UnlockBits(BDT);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmapsource.Width;
            HS = bitmapsource.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 32bpp
            WT = WS;
            HT = HS;
            bitmaptarget = new CBitmapArgb(WT, HT);
            PFT = bitmaptarget.PixelFormat; // 32bppArgb
            //
            BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BDT = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.WriteOnly, PFT);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++;
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  // B - 08Bit
                  *PBT = CV.B;
                  PBT++;
                  // G - 16Bit
                  *PBT = CV.G;
                  PBT++;
                  // R - 24Bit
                  *PBT = CV.R;
                  PBT++;
                  // A - 32Bit
                  *PBT = 0xFF;
                  PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.Bitmap.UnlockBits(BDT);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public static Boolean ConvertBitmapPalette256ToBitmap256(Bitmap bitmapsource,
                                                             CPalette256 palette256,
                                                             out CBitmap256 bitmaptarget)
    {
      bitmaptarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        switch (bitmapsource.PixelFormat)
        {
          case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
            // Source 24bpp
            Int32 WS = bitmapsource.Width;
            Int32 HS = bitmapsource.Height;
            PixelFormat PFS = PixelFormat.Format24bppRgb;
            // Target 32bpp
            Int32 WT = WS;
            Int32 HT = HS;
            PixelFormat PFT = PixelFormat.Format24bppRgb;
            bitmaptarget = new CBitmap256(WT, HT, palette256);
            //
            BitmapData BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BitmapData BDT = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.WriteOnly, PFT);
            //
            Int32 CIL = 0;
            Int32 CIH = WS - 1;
            Int32 RIL = 0;
            Int32 RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                {
                  // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  // NC !!! 24bit PBS++; 
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  // B - 08Bit
                  *PBT = CV.B;
                  PBT++;
                  // G - 16Bit
                  *PBT = CV.G;
                  PBT++;
                  // R - 24Bit
                  *PBT = CV.R;
                  PBT++;
                  // A - 32Bit
                  *PBT = 0xFF;
                  PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.Bitmap.UnlockBits(BDT);
            return true;
          case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
            // Source 32bpp
            WS = bitmapsource.Width;
            HS = bitmapsource.Height;
            PFS = PixelFormat.Format32bppRgb;
            // Target 32bpp
            WT = WS;
            HT = HS;
            PFT = PixelFormat.Format32bppArgb;
            bitmaptarget = new CBitmap256(WT, HT, palette256);
            //
            BDS = bitmapsource.LockBits(new Rectangle(0, 0, WS, HS), ImageLockMode.ReadOnly, PFS);
            BDT = bitmaptarget.Bitmap.LockBits(new Rectangle(0, 0, WT, HT), ImageLockMode.WriteOnly, PFT);
            //
            CIL = 0;
            CIH = WS - 1;
            RIL = 0;
            RIH = HS - 1;
            unsafe
            {
              for (Int32 RI = RIL; RI <= RIH; RI++)
              {
                Byte* PBS = (Byte*)(BDS.Scan0 + RI * BDS.Stride);
                Byte* PBT = (Byte*)(BDT.Scan0 + RI * BDT.Stride);
                for (Int32 CI = CIL; CI <= CIH; CI++)
                { // B - 08Bit
                  Byte B = *PBS;
                  PBS++;
                  // G - 16Bit
                  Byte G = *PBS;
                  PBS++;
                  // R - 24Bit
                  Byte R = *PBS;
                  PBS++;
                  // A - 32Bit
                  PBS++; // NC from Source
                  Byte II = (Byte)(FR * (float)R + FG * (float)G + FB * (float)B);
                  Color CV = palette256[II];
                  // B - 08Bit
                  *PBT = CV.B;
                  PBT++;
                  // G - 16Bit
                  *PBT = CV.G;
                  PBT++;
                  // R - 24Bit
                  *PBT = CV.R;
                  PBT++;
                  // A - 32Bit
                  *PBT = 0xFF;
                  PBT++;
                }
              }
            }
            bitmapsource.UnlockBits(BDS);
            bitmaptarget.Bitmap.UnlockBits(BDT);
            return true;
        }
        return false;
      }
      catch (Exception)
      {
        return false;
      }
    }







  }
}
