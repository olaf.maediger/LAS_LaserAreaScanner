﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMemory;
//
namespace IPMatrix
{
  public class CMatrixInt32 : CMatrixBase
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------------------
    //  
    public const UInt32 MASK_RANGE = 0x0000FFFF;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private Int32[,] FMatrix;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrixInt32(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrix = new Int32[FColCount, FRowCount];
    }

    //public CMatrixInt32(CMatrix08Bit matrix)
    //  : base(matrix.ColCount, matrix.RowCount)
    //{
    //  ConvertArray08BitToInt32(matrix);
    //}
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    ////  
    //protected override Int32[,] GetInt32Values()
    //{
    //  return FMatrix;
    //}
    //
    //-------------------------------------------------------------------------------
    //
    public override Byte GetValue08Bit(Int32 indexcol, Int32 indexrow)
    { // 08Bit <- Int32
      return (Byte)(0x000000FF & FMatrix[indexcol, indexrow]);
    }
    public override void SetValue08Bit(Int32 indexcol, Int32 indexrow, Byte value)
    { // Int32 <- 08Bit
      FMatrix[indexcol, indexrow] = (Int32)value;
    }

    public override UInt16 GetValue10Bit(Int32 indexcol, Int32 indexrow)
    { // 10Bit <- Int32
      return (UInt16)(0x000003FF & FMatrix[indexcol, indexrow]);
    }
    public override void SetValue10Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // Int32 <- 10Bit
      FMatrix[indexcol, indexrow] = (Int32)value;
    }

    public override UInt16 GetValue12Bit(Int32 indexcol, Int32 indexrow)
    { // 12Bit <- Int32
      return (UInt16)(0x00000FFF & FMatrix[indexcol, indexrow]);
    }
    public override void SetValue12Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // Int32 <- 12Bit
      FMatrix[indexcol, indexrow] = (Int32)value;
    }

    public override UInt16 GetValue16Bit(Int32 indexcol, Int32 indexrow)
    { // 16Bit <- Int32
      return (UInt16)(0x0000FFFF & FMatrix[indexcol, indexrow]);
    }
    public override void SetValue16Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // Int32 <- 16Bit
      FMatrix[indexcol, indexrow] = (Int32)value;
    }
    //
    //-------------------------------------------------------------------------------
    //
    public override Int32 GetValueInt32(Int32 indexcol, Int32 indexrow)
    { // Int32 <- Int32
      return (Int32)FMatrix[indexcol, indexrow];
    }
    public override void SetValueInt32(Int32 indexcol, Int32 indexrow, Int32 value)
    { // Int32 <- Int32
      FMatrix[indexcol, indexrow] = (Int32)value;
    }

    public override Double GetValueDouble(Int32 indexcol, Int32 indexrow)
    { // Double <- Int32
      return (Double)FMatrix[indexcol, indexrow];
    }
    public override void SetValueDouble(Int32 indexcol, Int32 indexrow, Double value)
    { // Int32 <- Double
      FMatrix[indexcol, indexrow] = (Int32)value;
    }  
    //  
    //----------------------------------------------------------------------------
    //  Segment - Helper
    //----------------------------------------------------------------------------
    //  
    //private Boolean ConvertArray08BitToInt32(CMatrix08Bit matrix)
    //{
    //  try
    //  {
    //    Int32 CC = matrix.ColCount;
    //    Int32 RC = matrix.RowCount;
    //    FMatrix = new Int32[CC, RC];
    //    for (Int32 RI = 0; RI < RC; RI++)
    //    {
    //      for (Int32 CI = 0; CI < CC; CI++)
    //      {
    //        FMatrix[CI, RI] = matrix.GetValue08Bit(CI, RI);
    //      }
    //    }
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

  }
}

