﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMemory;
//
namespace IPMatrix
{
  public class CMatrix08Bit : CMatrixBase
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------------------
    //  
    public const UInt32 MASK_RANGE = 0x000000FF;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private Byte[,] FMatrix;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrix08Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrix = new Byte[FColCount, FRowCount];
    }

    public CMatrix08Bit(Byte[,] matrix)
      : base(matrix.GetLength(0), matrix.GetLength(1))
    {
      FMatrix = matrix;
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    ////  
    //protected override Byte[,] GetByteValues()
    //{
    //  return FMatrix;
    //}
    //
    //-------------------------------------------------------------------------------
    //
    public override Byte GetValue08Bit(Int32 indexcol, Int32 indexrow)
    { // 08Bit <- 08Bit
      return (Byte)FMatrix[indexcol, indexrow];
    }
    public override void SetValue08Bit(Int32 indexcol, Int32 indexrow, Byte value)
    { // 08Bit <- 08Bit
      FMatrix[indexcol, indexrow] = (Byte)value;
    }
    
    public override UInt16 GetValue10Bit(Int32 indexcol, Int32 indexrow)
    { // 10Bit <- 08Bit
      return (UInt16)(FMatrix[indexcol, indexrow] << 2);
    }
    public override void SetValue10Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 08Bit <- 10Bit
      FMatrix[indexcol, indexrow] = (Byte)(0x000000FF & (value >> 2));
    }
    
    public override UInt16 GetValue12Bit(Int32 indexcol, Int32 indexrow)
    { // 12Bit <- 08Bit
      return (UInt16)(FMatrix[indexcol, indexrow] << 4);
    }
    public override void SetValue12Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 08Bit <- 12Bit
      FMatrix[indexcol, indexrow] = (Byte)(0x000000FF & (value >> 4));
    }

    public override UInt16 GetValue16Bit(Int32 indexcol, Int32 indexrow)
    { // 16Bit <- 08Bit
      return (UInt16)(FMatrix[indexcol, indexrow] << 8);
    }
    public override void SetValue16Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 08Bit <- 16Bit
      FMatrix[indexcol, indexrow] = (Byte)(0x000000FF & (value >> 8));
    }
    //
    //-------------------------------------------------------------------------------
    //
    public override Int32 GetValueInt32(Int32 indexcol, Int32 indexrow)
    { // Int32 <- 08Bit
      return (Int32)FMatrix[indexcol, indexrow];
    }
    public override void SetValueInt32(Int32 indexcol, Int32 indexrow, Int32 value)
    { // 08Bit <- Int32
      FMatrix[indexcol, indexrow] = (Byte)value;
    }
    
    public override Double GetValueDouble(Int32 indexcol, Int32 indexrow)
    { // Double <- 08Bit
      return (Double)FMatrix[indexcol, indexrow];
    }
    public override void SetValueDouble(Int32 indexcol, Int32 indexrow, Double value)
    { // 08Bit <- Double
      FMatrix[indexcol, indexrow] = (Byte)value;
    }
    //
    //-----------------------------------------------------------
    //  Section - Public Method
    //-----------------------------------------------------------
    //


  }
}
