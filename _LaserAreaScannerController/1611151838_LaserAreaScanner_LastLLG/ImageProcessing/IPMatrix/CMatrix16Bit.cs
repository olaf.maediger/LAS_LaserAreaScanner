﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMemory;
//
namespace IPMatrix
{
  public class CMatrix16Bit : CMatrixBase
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------------------
    //  
    public const UInt32 MASK_RANGE = 0x0000FFFF;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private UInt16[,] FMatrix;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrix16Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrix = new UInt16[FColCount, FRowCount];
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    //  
    //-------------------------------------------------------------------------------
    //
    public override Byte GetValue08Bit(Int32 indexcol, Int32 indexrow)
    { // 08Bit <- 16Bit
      return (Byte)(FMatrix[indexcol, indexrow] >> 8);
    }
    public override void SetValue08Bit(Int32 indexcol, Int32 indexrow, Byte value)
    { // 16Bit <- 08Bit
      FMatrix[indexcol, indexrow] = (UInt16)(0x0000FFFF & (value << 8));
    }

    public override UInt16 GetValue10Bit(Int32 indexcol, Int32 indexrow)
    { // 10Bit <- 16Bit
      return (UInt16)(FMatrix[indexcol, indexrow] << 6);
    }
    public override void SetValue10Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 16Bit <- 10Bit
      FMatrix[indexcol, indexrow] = (UInt16)(0x0000FFFF & (value >> 6));
    }

    public override UInt16 GetValue12Bit(Int32 indexcol, Int32 indexrow)
    { // 12Bit <- 16Bit
      return (UInt16)(FMatrix[indexcol, indexrow] << 4);
    }
    public override void SetValue12Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 16Bit <- 12Bit
      FMatrix[indexcol, indexrow] = (UInt16)(0x0000FFFF & (value >> 4));
    }

    public override UInt16 GetValue16Bit(Int32 indexcol, Int32 indexrow)
    { // 16Bit <- 16Bit
      return (UInt16)(FMatrix[indexcol, indexrow] << 0);
    }
    public override void SetValue16Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 16Bit <- 16Bit
      FMatrix[indexcol, indexrow] = (UInt16)(0x0000FFFF & (value >> 0));
    }
    //
    //-------------------------------------------------------------------------------
    //
    public override Int32 GetValueInt32(Int32 indexcol, Int32 indexrow)
    { // Int32 <- 16Bit
      return (Int32)FMatrix[indexcol, indexrow];
    }
    public override void SetValueInt32(Int32 indexcol, Int32 indexrow, Int32 value)
    { // 16Bit <- Int32
      FMatrix[indexcol, indexrow] = (UInt16)value;
    }
    //
    //-------------------------------------------------------------------------------
    //
    public override Double GetValueDouble(Int32 indexcol, Int32 indexrow)
    { // Double <- 16Bit
      return (Double)FMatrix[indexcol, indexrow];
    }
    public override void SetValueDouble(Int32 indexcol, Int32 indexrow, Double value)
    { // 16Bit <- Double
      FMatrix[indexcol, indexrow] = (UInt16)(0x0000FFFF & (UInt16)value);
    }  

  }
}
