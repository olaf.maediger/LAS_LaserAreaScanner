﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
//
namespace IPMatrix
{
  //
  //--------------------------------------------------
  //	Types
  //--------------------------------------------------
  //
  public struct RTextFileData
  {
    public String Entry;
    public String Drive;
    public String Path;
    public String Name;
    public String Extension;
    public String FormatString;
    public String FormatCharacter;
    public String FormatInt16;
    public String FormatInt32;
    public String FormatByte;
    public String FormatUInt10;
    public String FormatUInt12;
    public String FormatUInt16;
    public String FormatUInt32;
    public String FormatDecimal;
    public String FormatSingle;
    public String FormatDouble;
    public String[] TokenDelimiters;
    public String[] LineDelimiters;
    //
    public DOnMatrix08BitRead OnMatrix08BitRead;
    public DOnMatrix10BitRead OnMatrix10BitRead;
    public DOnMatrix12BitRead OnMatrix12BitRead;
    public DOnMatrix16BitRead OnMatrix16BitRead;
    public DOnMatrixByteRead OnMatrixByteRead;
    public DOnMatrixUInt10Read OnMatrixUInt10Read;
    public DOnMatrixUInt12Read OnMatrixUInt12Read;
    public DOnMatrixUInt16Read OnMatrixUInt16Read;
    //
    public DOnMatrix08BitWrite OnMatrix08BitWrite;
    public DOnMatrix10BitWrite OnMatrix10BitWrite;
    public DOnMatrix12BitWrite OnMatrix12BitWrite;
    public DOnMatrix16BitWrite OnMatrix16BitWrite;
    public DOnMatrixByteWrite OnMatrixByteWrite;
    public DOnMatrixUInt10Write OnMatrixUInt10Write;
    public DOnMatrixUInt12Write OnMatrixUInt12Write;
    public DOnMatrixUInt16Write OnMatrixUInt16Write;
    //
    public RTextFileData(Int32 init)
    {      
      Entry = CMatrixFileText.INIT_ENTRY;
      Drive = CMatrixFileText.INIT_DRIVE;
      Path = CMatrixFileText.INIT_PATH;
      Name = CMatrixFileText.INIT_HEADER;
      Extension = CMatrixFileText.INIT_EXTENSION;
      FormatString = CMatrixFileText.INIT_FORMATSTRING;
      FormatCharacter = CMatrixFileText.INIT_FORMATCHARACTER;
      FormatInt16 = CMatrixFileText.INIT_FORMATINT16;
      FormatInt32 = CMatrixFileText.INIT_FORMATINT32;
      FormatByte = CMatrixFileText.INIT_FORMATBYTE;
      FormatUInt10 = CMatrixFileText.INIT_FORMATUINT16;
      FormatUInt12 = CMatrixFileText.INIT_FORMATUINT16;
      FormatUInt16 = CMatrixFileText.INIT_FORMATUINT16;
      FormatUInt32 = CMatrixFileText.INIT_FORMATUINT32;
      FormatDecimal = CMatrixFileText.INIT_FORMATDECIMAL;
      FormatSingle = CMatrixFileText.INIT_FORMATSINGLE;
      FormatDouble = CMatrixFileText.INIT_FORMATDOUBLE;
      TokenDelimiters = new String[] { "\t", " " };
      LineDelimiters = new String[] { "\r\n", "\r", "\n" };
      //
      OnMatrix08BitRead = null;
      OnMatrix10BitRead = null;
      OnMatrix12BitRead = null;
      OnMatrix16BitRead = null;
      OnMatrixByteRead = null;
      OnMatrixUInt10Read = null;
      OnMatrixUInt12Read = null;
      OnMatrixUInt16Read = null;
      //
      OnMatrix08BitWrite = null;
      OnMatrix10BitWrite = null;
      OnMatrix12BitWrite = null;
      OnMatrix16BitWrite = null;
      OnMatrixByteWrite = null;
      OnMatrixUInt10Write = null;
      OnMatrixUInt12Write = null;
      OnMatrixUInt16Write = null;
    }
  };
  //
  public partial class CMatrixFileText
  {
    //	
    //--------------------------------------------------
    //	Section - Constant
    //--------------------------------------------------
    //
    public static String HEADER_LIBRARY = "TextFile";
    public static String SECTION_LIBRARY = HEADER_LIBRARY;
    //
    public const String INIT_DRIVESEPARATOR = ":";
    public const String INIT_HEADERSEPARATOR = ".";
    public const String INIT_DRIVE = "C";
    public const String INIT_PATH = INIT_HEADERSEPARATOR;
    public const String INIT_HEADER = "filename";
    public const String INIT_EXTENSION = "txt";
    public const String INIT_ENTRY = INIT_HEADER + INIT_HEADERSEPARATOR + INIT_EXTENSION;
    public const String INIT_FORMATSTRING = "{0}";
    public const String INIT_FORMATCHARACTER = "{0}";
    public const String INIT_FORMATINT16 = "{0}";
    public const String INIT_FORMATINT32 = "{0}";
    public const String INIT_FORMATBYTE = "{0}";
    public const String INIT_FORMATUINT10 = "{0}";
    public const String INIT_FORMATUINT12 = "{0}";
    public const String INIT_FORMATUINT16 = "{0}";
    public const String INIT_FORMATUINT32 = "{0}";
    public const String INIT_FORMATDECIMAL = "{0}";
    public const String INIT_FORMATSINGLE = "{0}";
    public const String INIT_FORMATDOUBLE = "{0:0.000000000}";
    // 130412 
    public readonly String[] INIT_TOKENDELIMITERS = new String[] { "\t", " " };
    public readonly String[] INIT_LINEDELIMITERS = new String[] { "\r\n", "\r", "\n" };
    //
    public const String INIT_STRING = "";
    public const Char INIT_CHAR = (Char)0x00;
    public const Int16 INIT_INT16 = 0;
    public const Int32 INIT_INT32 = 0;
    public const Byte INIT_BYTE = 0x00;
    public const UInt16 INIT_UINT10 = 0;
    public const UInt16 INIT_UINT12 = 0;
    public const UInt16 INIT_UINT16 = 0;
    public const UInt32 INIT_UINT32 = 0;
    public const Single INIT_SINGLE = 0;
    public const Double INIT_DOUBLE = 0;
    public const Decimal INIT_DECIMAL = 0;
    public DateTime INIT_DATETIME = DateTime.MinValue;
    public TimeSpan INIT_TIMESPAN = TimeSpan.MinValue;
    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //
    private RTextFileData FData;
    private StreamWriter FStreamWriter;
    private StreamReader FStreamReader;
    private String[][] FTokenArray;	// Row, Col
    private Int32 FRowIndex;
    private Int32 FColIndex;
    //
    //--------------------------------------------------
    //	Section - Constructor
    //--------------------------------------------------
    //
    public CMatrixFileText()
    {
      FData = new RTextFileData(0);
    }
    //
    //--------------------------------------------------
    //	Section - Property
    //--------------------------------------------------
    //
    public Int32 RowCount
    {
      get { return FTokenArray.Length; }
    }

    public Int32 ColCount(Int32 rowindex)
    {
      return FTokenArray[rowindex].Length;
    }

    public Int32 RowIndex
    {
      get { return FRowIndex; }
    }

    public Int32 ColIndex
    {
      get { return FColIndex; }
    }

    public Boolean IsEndOfFile()
    {
      return !IsTokenAvailable();
    }

    private Int32 RIH()
    {
      return FTokenArray.Length - 1;
    }

    private Int32 CIH(Int32 rowindex)
    {
      return FTokenArray[rowindex].Length - 1;
    }
    //
    //--------------------------------------------------
    //	Section - Get/SetData
    //--------------------------------------------------
    //
    public Boolean GetData(out RTextFileData data)
    {
      data = FData;
      return true;
    }

    public Boolean SetData(RTextFileData data)
    {
      FData = data;
      return true;
    }
    //
    //--------------------------------------------------
    //	Section - Token-Management
    //--------------------------------------------------
    //
    protected Boolean TokenizeText(String text)
    {
      String ReadBuffer = text;
      String[] RowLines = ReadBuffer.Split(FData.LineDelimiters, StringSplitOptions.RemoveEmptyEntries);
      FTokenArray = new String[RowLines.Length][];
      for (Int32 RI = 0; RI < RowLines.Length; RI++)
      {
        String RowLine = RowLines[RI];
        String[] Tokens = RowLine.Split(FData.TokenDelimiters, StringSplitOptions.RemoveEmptyEntries);
        FTokenArray[RI] = new String[Tokens.Length];
        for (Int32 CI = 0; CI < Tokens.Length; CI++)
        {
          FTokenArray[RI][CI] = Tokens[CI];
        }
      }
      FRowIndex = 0;
      FColIndex = 0;
      return (0 < RowCount);
    }

    protected Boolean IsTokenAvailable()
    {	// Endposition reached?
      if ((FRowIndex < 0) || (FColIndex < 0))
      { // no more Token available!
        return false;
      }
      //
      Int32 RowIndexHigh = RIH();
      Int32 ColIndexHigh = CIH(FRowIndex);
      return ((FRowIndex <= RowIndexHigh) && (FColIndex <= ColIndexHigh));
    }

    private void IncrementTokenIndices()
    {
      if (FColIndex < CIH(FRowIndex))
      {
        FColIndex++;
        return;
      }
      if (FRowIndex < RIH())
      {
        FColIndex = 0;
        FRowIndex++;
        return;
      }
      FColIndex = -1;
      FRowIndex = -1;
    }
    //
    //--------------------------------------------------
    //	Section - File-Management
    //--------------------------------------------------
    //
    public Boolean Close()
    {
      if (FStreamWriter is StreamWriter)
      {
        FStreamWriter.Close();
        FStreamWriter = null;
      }
      if (FStreamReader is StreamReader)
      {
        FStreamReader.Close();
        FStreamReader = null;
      }
      return true;
    }

  }
}
