﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using IPMemory;
//
namespace IPMatrix
{
  public class CMatrix10Bit : CMatrixBase
  {
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constant
    //----------------------------------------------------------------------------
    //  
    public const UInt32 MASK_RANGE = 0x000003FF; 
    //  
    //----------------------------------------------------------------------------
    //  Segment - Field
    //----------------------------------------------------------------------------
    //  
    private UInt16[,] FMatrix;
    //  
    //----------------------------------------------------------------------------
    //  Segment - Constructor
    //----------------------------------------------------------------------------
    //  
    public CMatrix10Bit(Int32 colcount, Int32 rowcount)
      : base(colcount, rowcount)
    {
      FMatrix = new UInt16[FColCount, FRowCount];
    }
    //  
    //----------------------------------------------------------------------------
    //  Segment - Property
    //----------------------------------------------------------------------------
    ////  
    //protected override UInt16[,] GetUInt16Values()
    //{
    //  return FMatrix;
    //}
    //
    //-------------------------------------------------------------------------------
    //
    public override Byte GetValue08Bit(Int32 indexcol, Int32 indexrow)
    { // 08Bit <- 10Bit
      return (Byte)(FMatrix[indexcol, indexrow] >> 2);
    }
    public override void SetValue08Bit(Int32 indexcol, Int32 indexrow, Byte value)
    { // 10Bit <- 08Bit
      FMatrix[indexcol, indexrow] = (UInt16)(MASK_RANGE & (value << 2));
    }

    public override UInt16 GetValue10Bit(Int32 indexcol, Int32 indexrow)
    { // 10Bit <- 10Bit
      return (UInt16)(FMatrix[indexcol, indexrow] << 0);
    }
    public override void SetValue10Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 10Bit <- 10Bit
      FMatrix[indexcol, indexrow] = (UInt16)(MASK_RANGE & (value >> 0));
    }

    public override UInt16 GetValue12Bit(Int32 indexcol, Int32 indexrow)
    { // 12Bit <- 10Bit
      return (UInt16)(FMatrix[indexcol, indexrow] << 2);
    }
    public override void SetValue12Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 10Bit <- 12Bit
      FMatrix[indexcol, indexrow] = (UInt16)(MASK_RANGE & (value >> 2));
    }

    public override UInt16 GetValue16Bit(Int32 indexcol, Int32 indexrow)
    { // 16Bit <- 10Bit
      return (UInt16)(FMatrix[indexcol, indexrow] << 6);
    }
    public override void SetValue16Bit(Int32 indexcol, Int32 indexrow, UInt16 value)
    { // 10Bit <- 16Bit
      FMatrix[indexcol, indexrow] = (UInt16)(MASK_RANGE & (value >> 6));
    }
    //
    //-------------------------------------------------------------------------------
    //
    public override Int32 GetValueInt32(Int32 indexcol, Int32 indexrow)
    { // Int32 <- 10Bit
      return (Int32)FMatrix[indexcol, indexrow];
    }
    public override void SetValueInt32(Int32 indexcol, Int32 indexrow, Int32 value)
    { // 10Bit <- Int32
      FMatrix[indexcol, indexrow] = (UInt16)(MASK_RANGE & value);
    }

    public override Double GetValueDouble(Int32 indexcol, Int32 indexrow)
    { // Double <- 10Bit
      return (Double)FMatrix[indexcol, indexrow];
    }
    public override void SetValueDouble(Int32 indexcol, Int32 indexrow, Double value)
    { // 10Bit <- Double
      FMatrix[indexcol, indexrow] = (UInt16)(MASK_RANGE & (UInt16)value);
    }  

  }
}
