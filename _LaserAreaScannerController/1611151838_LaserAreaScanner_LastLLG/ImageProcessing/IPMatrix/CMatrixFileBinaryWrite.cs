﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
//
namespace IPMatrix
{
  public partial class CMatrixFileBinary
  {
    //
    //--------------------------------------------------
    //	Section - File-Management
    //--------------------------------------------------
    //
    public Boolean OpenWrite(String fileentry)
    {
      if (!(FBinaryWriter is BinaryWriter))
      {
        FileStream FS = new FileStream(fileentry, FileMode.Create);
        if (FS is FileStream)
        {
          FBinaryWriter = new BinaryWriter(FS);
          return (FBinaryWriter is BinaryWriter);
        }
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Section - LowLevel - Write
    //--------------------------------------------------
    //
    protected Boolean Write(String value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Write(value);
        return true;
      }
      return false;
    }

    protected Boolean Write(Char value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Write(value);
        return true;
      }
      return false;
    }

    protected Boolean Write(Byte value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Write(value);
        return true;
      }
      return false;
    }

    protected Boolean Write(Int16 value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Write(value);
        return true;
      }
      return false;
    }

    protected Boolean Write(UInt16 value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Write(value);
        return true;
      }
      return false;
    }

    protected Boolean Write(Int32 value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Write(value);
        return true;
      }
      return false;
    }

    protected Boolean Write(UInt32 value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Write(value);
        return true;
      }
      return false;
    }

    protected Boolean Write(Single value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Write(value);
        return true;
      }
      return false;
    }

    protected Boolean Write(Double value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Write(value);
        return true;
      }
      return false;
    }

    protected Boolean Write(Decimal value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        FBinaryWriter.Write(value);
        return true;
      }
      return false;
    }

    protected Boolean Write(DateTime value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        String Buffer = value.ToString();
        FBinaryWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(TimeSpan value)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        String Buffer = value.ToString();
        FBinaryWriter.Write(Buffer);
        return true;
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Segment - HighLevel - Write - Class-specific
    //--------------------------------------------------
    //
    public Boolean WriteMatrix08Bit(CMatrix08Bit matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("08Bit");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix.GetValue08Bit(CI, RI));
          }
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrix10Bit(CMatrix10Bit matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("10Bit");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix.GetValue10Bit(CI, RI));
          }
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrix12Bit(CMatrix12Bit matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("12Bit");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix.GetValue12Bit(CI, RI));
          }
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrix16Bit(CMatrix16Bit matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("16Bit");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix.GetValue16Bit(CI, RI));
          }
        }
        return true;
      }
      return false;
    }
    //
    //------------------------------------------------------------
    //
    public Boolean WriteMatrixByte(Byte[,] matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("Byte");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix[CI, RI]);
          }
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrixUInt10(UInt16[,] matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("UInt10");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix[CI, RI]);
          }
        }
        return true;
      }
      return false;
    }

    public Boolean WriteMatrixUInt12(UInt16[,] matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("UInt12");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix[CI, RI]);
          }
        }
        return true;
      }
      return false;
    }
    public Boolean WriteMatrixUInt16(UInt16[,] matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("UInt16");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix[CI, RI]);
          }
        }
        return true;
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Segment - HighLevel - Write - Class-independent
    //--------------------------------------------------
    //
    public Boolean Write(CMatrix08Bit matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("08Bit");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix.GetValue08Bit(CI, RI));
          }
        }
        return true;
      }
      return false;
    }

    public Boolean Write(CMatrix10Bit matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("10Bit");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix.GetValue10Bit(CI, RI));
          }
        }
        return true;
      }
      return false;
    }

    public Boolean Write(CMatrix12Bit matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 CC = matrix.ColCount;
        Int32 RC = matrix.RowCount;
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("12Bit");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix.GetValue12Bit(CI, RI));
          }
        }
        return true;
      }
      return false;
    }
    //
    //------------------------------------------------------------
    //
    public Boolean Write(Byte[,] matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 CC = matrix.GetLength(0);
        Int32 RC = matrix.GetLength(1);
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("Byte");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix[CI, RI]);
          }
        }
        return true;
      }
      return false;
    }

    public Boolean Write(Int32[,] matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("Int32");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix[CI, RI]);
          }
        }
        return true;
      }
      return false;
    }

    public Boolean Write(Double[,] matrix)
    {
      if (FBinaryWriter is BinaryWriter)
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        FBinaryWriter.Write(CC);
        FBinaryWriter.Write(RC);
        FBinaryWriter.Write("Double");
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            FBinaryWriter.Write(matrix[CI, RI]);
          }
        }
        return true;
      }
      return false;
    }

  }
}
