﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPPaletteBase;
using IPPalette256;
//
namespace IPMatrix
{
  public class CMVectorConversion
  { //
    //-----------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Callback
    //-----------------------------------------------------------
    //





    //
    //###################################################################################
    //  Segment - VectorByte (no Palette) -> Matrix...
    //###################################################################################
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) -> Matrix08Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToMatrix08Bit(Int32 colcount, Int32 rowcount,
                                                         Byte[] vectorsource,
                                                         out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            matrixtarget.SetValue08Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) -> Matrix10Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToMatrix10Bit(Int32 colcount, Int32 rowcount,
                                                         Byte[] vectorsource,
                                                         out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            matrixtarget.SetValue08Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) -> Matrix12Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToMatrix12Bit(Int32 colcount, Int32 rowcount,
                                                         Byte[] vectorsource,
                                                         out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            matrixtarget.SetValue08Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) -> Matrix16Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToMatrix16Bit(Int32 colcount, Int32 rowcount,
                                                         Byte[] vectorsource,
                                                         out CMatrix16Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix16Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            matrixtarget.SetValue08Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) -> MatrixByte
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToMatrixByte(Int32 colcount, Int32 rowcount,
                                                        Byte[] vectorsource,
                                                        out Byte[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new Byte[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            matrixtarget[CI, RI] = CE;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) -> MatrixUInt10
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToMatrixUInt10(Int32 colcount, Int32 rowcount,
                                                          Byte[] vectorsource,
                                                          out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            matrixtarget[CI, RI] = (UInt16)(CE << 2);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) -> MatrixUInt12
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToMatrixUInt12(Int32 colcount, Int32 rowcount,
                                                          Byte[] vectorsource,
                                                          out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            matrixtarget[CI, RI] = (UInt16)(CE << 4);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte (no Palette) -> MatrixUInt16
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorByteToMatrixUInt16(Int32 colcount, Int32 rowcount,
                                                          Byte[] vectorsource,
                                                          out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            matrixtarget[CI, RI] = (UInt16)(CE << 8);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //###################################################################################
    //  Segment - VectorUInt10 (no Palette) -> Matrix...
    //###################################################################################
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) -> Matrix08Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToMatrix08Bit(Int32 colcount, Int32 rowcount,
                                                           UInt16[] vectorsource,
                                                           out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 2);
            VI++;
            matrixtarget.SetValue08Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) -> Matrix10Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToMatrix10Bit(Int32 colcount, Int32 rowcount,
                                                           UInt16[] vectorsource,
                                                           out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            UInt16 CE = (UInt16)(vectorsource[VI] >> 0);
            VI++;
            matrixtarget.SetValue10Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) -> Matrix12Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToMatrix12Bit(Int32 colcount, Int32 rowcount,
                                                           UInt16[] vectorsource,
                                                           out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            UInt16 CE = (UInt16)(vectorsource[VI] >> 2);
            VI++;
            matrixtarget.SetValue12Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) -> Matrix16Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToMatrix16Bit(Int32 colcount, Int32 rowcount,
                                                           UInt16[] vectorsource,
                                                           out CMatrix16Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix16Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            UInt16 CE = (UInt16)(vectorsource[VI] >> 6);
            VI++;
            matrixtarget.SetValue16Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) -> MatrixByte
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToMatrixByte(Int32 colcount, Int32 rowcount,
                                                          UInt16[] vectorsource,
                                                          out Byte[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new Byte[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 2);
            VI++;
            matrixtarget[CI, RI] = CE;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) -> MatrixUInt10
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToMatrixUInt10(Int32 colcount, Int32 rowcount,
                                                            UInt16[] vectorsource,
                                                            out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            UInt16 CE = (UInt16)vectorsource[VI];
            VI++;
            matrixtarget[CI, RI] = (UInt16)CE;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) -> MatrixUInt12
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToMatrixUInt12(Int32 colcount, Int32 rowcount,
                                                            UInt16[] vectorsource,
                                                            out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            UInt16 CE = (Byte)vectorsource[VI];
            VI++;
            matrixtarget[CI, RI] = (UInt16)(CE << 2);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 (no Palette) -> MatrixUInt16
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10ToMatrixUInt16(Int32 colcount, Int32 rowcount,
                                                            UInt16[] vectorsource,
                                                            out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            UInt16 CE = (UInt16)vectorsource[VI];
            VI++;
            matrixtarget[CI, RI] = (UInt16)(CE << 6);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //###################################################################################
    //  Segment - VectorUInt16 (no Palette) -> Matrix...
    //###################################################################################
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) -> Matrix08Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToMatrix08Bit(Int32 colcount, Int32 rowcount,
                                                           UInt16[] vectorsource,
                                                           out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            matrixtarget.SetValue08Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) -> Matrix10Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToMatrix10Bit(Int32 colcount, Int32 rowcount,
                                                           UInt16[] vectorsource,
                                                           out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            matrixtarget.SetValue08Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) -> Matrix12Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToMatrix12Bit(Int32 colcount, Int32 rowcount,
                                                           UInt16[] vectorsource,
                                                           out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            matrixtarget.SetValue08Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) -> Matrix16Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToMatrix16Bit(Int32 colcount, Int32 rowcount,
                                                           UInt16[] vectorsource,
                                                           out CMatrix16Bit matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix16Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            matrixtarget.SetValue08Bit(CI, RI, CE);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) -> MatrixByte
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToMatrixByte(Int32 colcount, Int32 rowcount,
                                                          UInt16[] vectorsource,
                                                          out Byte[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new Byte[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            matrixtarget[CI, RI] = CE;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) -> MatrixUInt10
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToMatrixUInt10(Int32 colcount, Int32 rowcount,
                                                            UInt16[] vectorsource,
                                                            out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            UInt16 CE = (UInt16)(vectorsource[VI] >> 8);
            VI++;
            matrixtarget[CI, RI] = (UInt16)CE;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) -> MatrixUInt12
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToMatrixUInt12(Int32 colcount, Int32 rowcount,
                                                            UInt16[] vectorsource,
                                                            out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            UInt16 CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            matrixtarget[CI, RI] = (UInt16)(CE << 2);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 (no Palette) -> MatrixUInt16
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16ToMatrixUInt16(Int32 colcount, Int32 rowcount,
                                                            UInt16[] vectorsource,
                                                            out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            UInt16 CE = (UInt16)(vectorsource[VI] >> 8);
            VI++;
            matrixtarget[CI, RI] = (UInt16)(CE << 8);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }
}
