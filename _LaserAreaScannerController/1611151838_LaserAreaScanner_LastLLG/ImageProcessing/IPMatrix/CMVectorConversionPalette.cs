﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
using IPPaletteBase;
using IPPalette256;
//
namespace IPMatrix
{
  public class CMVectorConversionPalette
  { //
    //-----------------------------------------------------------
    //  Section - Constant
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Field
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Constructor
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Property
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Helper
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Event
    //-----------------------------------------------------------
    //

    //
    //-----------------------------------------------------------
    //  Section - Callback
    //-----------------------------------------------------------
    //





    //
    //###################################################################################
    //  Segment - VectorByte + Palette256 -> Matrix...
    //###################################################################################
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte + Palette256 -> Matrix08Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorBytePalette256ToMatrix08Bit(Int32 colcount, Int32 rowcount,
                                                                   Byte[] vectorsource,
                                                                   CPalette256 palette256,
                                                                   out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte + Palette256 -> Matrix10Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorBytePalette256ToMatrix10Bit(Int32 colcount, Int32 rowcount,
                                                                   CPalette256 palette256,
                                                                   Byte[] vectorsource,
                                                                   out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte + Palette256 -> Matrix12Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorBytePalette256ToMatrix12Bit(Int32 colcount, Int32 rowcount,
                                                                   Byte[] vectorsource,
                                                                   CPalette256 palette256,
                                                                   out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte + Palette256 -> Matrix16Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorBytePalette256ToMatrix16Bit(Int32 colcount, Int32 rowcount,
                                                                   Byte[] vectorsource,
                                                                   CPalette256 palette256,
                                                                   out CMatrix16Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix16Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte + Palette256 -> MatrixByte
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorBytePalette256ToMatrixByte(Int32 colcount, Int32 rowcount,
                                                                  Byte[] vectorsource,
                                                                  CPalette256 palette256,
                                                                  out Byte[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new Byte[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = GV;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte + Palette256 -> MatrixUInt10
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorBytePalette256ToMatrixUInt10(Int32 colcount, Int32 rowcount,
                                                                    Byte[] vectorsource,
                                                                    CPalette256 palette256,
                                                                    out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 2);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte + Palette256 -> MatrixUInt12
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorBytePalette256ToMatrixUInt12(Int32 colcount, Int32 rowcount,
                                                                    Byte[] vectorsource,
                                                                    CPalette256 palette256,
                                                                    out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 4);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorByte + Palette256 -> MatrixUInt16
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorBytePalette256ToMatrixUInt16(Int32 colcount, Int32 rowcount,
                                                                    Byte[] vectorsource,
                                                                    CPalette256 palette256,
                                                                    out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)vectorsource[VI];
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 8);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //###################################################################################
    //  Segment - VectorUInt10 + Palette256 -> Matrix...
    //###################################################################################
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 + Palette256 -> Matrix08Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10Palette256ToMatrix08Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 2);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 + Palette256 -> Matrix10Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10Palette256ToMatrix10Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 2);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 + Palette256 -> Matrix12Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10Palette256ToMatrix12Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 2);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 + Palette256 -> Matrix16Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10Palette256ToMatrix16Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix16Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix16Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 2);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 + Palette256 -> MatrixByte
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10Palette256ToMatrixByte(Int32 colcount, Int32 rowcount,
                                                                    UInt16[] vectorsource,
                                                                    CPalette256 palette256,
                                                                    out Byte[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new Byte[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 2);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = GV;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 + Palette256 -> MatrixUInt10
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10Palette256ToMatrixUInt10(Int32 colcount, Int32 rowcount,
                                                                      UInt16[] vectorsource,
                                                                      CPalette256 palette256,
                                                                      out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 2);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 2);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 + Palette256 -> MatrixUInt12
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10Palette256ToMatrixUInt12(Int32 colcount, Int32 rowcount,
                                                                      UInt16[] vectorsource,
                                                                      CPalette256 palette256,
                                                                      out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 2);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 4);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt10 + Palette256 -> MatrixUInt16
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt10Palette256ToMatrixUInt16(Int32 colcount, Int32 rowcount,
                                                                      UInt16[] vectorsource,
                                                                      CPalette256 palette256,
                                                                      out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 2);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 8);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //###################################################################################
    //  Segment - VectorUInt12 + Palette256 -> Matrix...
    //###################################################################################
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 + Palette256 -> Matrix08Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12Palette256ToMatrix08Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 4);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 + Palette256 -> Matrix10Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12Palette256ToMatrix10Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 4);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 + Palette256 -> Matrix12Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12Palette256ToMatrix12Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 4);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 + Palette256 -> Matrix16Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12Palette256ToMatrix16Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix16Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix16Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 4);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 + Palette256 -> MatrixByte
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12Palette256ToMatrixByte(Int32 colcount, Int32 rowcount,
                                                                    UInt16[] vectorsource,
                                                                    CPalette256 palette256,
                                                                    out Byte[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new Byte[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 4);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = GV;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 + Palette256 -> MatrixUInt10
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12Palette256ToMatrixUInt10(Int32 colcount, Int32 rowcount,
                                                                      UInt16[] vectorsource,
                                                                      CPalette256 palette256,
                                                                      out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 4);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 2);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 + Palette256 -> MatrixUInt12
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12Palette256ToMatrixUInt12(Int32 colcount, Int32 rowcount,
                                                                      UInt16[] vectorsource,
                                                                      CPalette256 palette256,
                                                                      out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 4);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 4);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt12 + Palette256 -> MatrixUInt16
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt12Palette256ToMatrixUInt16(Int32 colcount, Int32 rowcount,
                                                                      UInt16[] vectorsource,
                                                                      CPalette256 palette256,
                                                                      out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 4);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 8);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //###################################################################################
    //  Segment - VectorUInt16 + Palette256 -> Matrix...
    //###################################################################################
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 + Palette256 -> Matrix08Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16Palette256ToMatrix08Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix08Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix08Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 + Palette256 -> Matrix10Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16Palette256ToMatrix10Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix10Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix10Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 + Palette256 -> Matrix12Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16Palette256ToMatrix12Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix12Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix12Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 + Palette256 -> Matrix16Bit
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16Palette256ToMatrix16Bit(Int32 colcount, Int32 rowcount,
                                                                     UInt16[] vectorsource,
                                                                     CPalette256 palette256,
                                                                     out CMatrix16Bit matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new CMatrix16Bit(CC, RC);
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget.SetValue08Bit(CI, RI, GV);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 + Palette256 -> MatrixByte
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16Palette256ToMatrixByte(Int32 colcount, Int32 rowcount,
                                                                    UInt16[] vectorsource,
                                                                    CPalette256 palette256,
                                                                    out Byte[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new Byte[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = GV;
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 + Palette256 -> MatrixUInt10
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16Palette256ToMatrixUInt10(Int32 colcount, Int32 rowcount,
                                                                      UInt16[] vectorsource,
                                                                      CPalette256 palette256,
                                                                      out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 2);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 + Palette256 -> MatrixUInt12
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16Palette256ToMatrixUInt12(Int32 colcount, Int32 rowcount,
                                                                      UInt16[] vectorsource,
                                                                      CPalette256 palette256,
                                                                      out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 4);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------------
    //  Section - Conversion VectorUInt16 + Palette256 -> MatrixUInt16
    //-----------------------------------------------------------------------------------
    //
    public static Boolean ConvertVectorUInt16Palette256ToMatrixUInt16(Int32 colcount, Int32 rowcount,
                                                                      UInt16[] vectorsource,
                                                                      CPalette256 palette256,
                                                                      out UInt16[,] matrixtarget)
    {
      matrixtarget = null;
      float FR = CMatrixBase.FACTOR_RED;
      float FG = CMatrixBase.FACTOR_GREEN;
      float FB = CMatrixBase.FACTOR_BLUE;
      try
      {
        Int32 CC = colcount;
        Int32 RC = rowcount;
        Int32 VI = 0;
        matrixtarget = new UInt16[CC, RC];
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Byte CE = (Byte)(vectorsource[VI] >> 8);
            VI++;
            Color CV = palette256[CE];
            Byte GV = (Byte)(FR * (float)CV.R + FG * (float)CV.G + FB * (float)CV.B);
            matrixtarget[CI, RI] = (UInt16)(GV << 8);
          }
        }
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }








  }
}
