﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace IFCommand
{
  public enum EVariableKind
  { // Scalar
    vkByte,
    vkUInt32,
    vkInt32,
    vkDouble,
    vkText,
    // Vector
    vkInt32Vector,
    vkDoubleVector,
    vkTextVector
  }

  public class CVariable
  {
    private CNotifier FNotifier;
    protected EVariableKind FKind;
    protected String FName;
    protected String FTime;

    public CVariable(EVariableKind kind)
    {
      FKind = kind;
    }

    public CVariable(EVariableKind kind,
                     String name)
    {
      FKind = kind;
      FName = name;
    }


    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public String Name
    {
      get { return FName; }
      set { FName = value; }
    }

    public String Time
    {
      get { return FTime; }
      set { FTime = value; }
    }

    public virtual void Debug()
    {
      FNotifier.Write(String.Format(" Variable[{0}]:", Name));
      FNotifier.Write(String.Format("  Time = {0}:", Time));
    }


  }
}
