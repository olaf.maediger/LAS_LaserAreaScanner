﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;

namespace IFCommand
{
	public enum EDeviceState
	{
		Idle = 0,
    IOPRead = 1,
    IOPWrite = 2,
    ADCRead = 3,
    ADCWrite = 4,
    DACRead = 5,
    DACWrite = 6,
    Undefined = 7
	};

  public static class CDEVICESTATES 
  {
    public const String Idle = "Idle";
    public const String IOPRead = "IOPRead";
    public const String IOPWrite = "IOPWrite";
    public const String ADCRead = "ADCRead";
    public const String ADCWrite = "ADCWrite";
    public const String DACRead = "DACRead";
    public const String DACWrite = "DACWrite";
    public const String Undefined = "Undefined";
  };

  public delegate void DOnDeviceStateChanged(EDeviceState value);

	public class CDevice
	{
		public const EDeviceState INIT_DEVICESTATE = EDeviceState.Undefined;

		private CNotifier FNotifier;
		public void SetNotifier(CNotifier notifier)
		{
			FNotifier = notifier;
		}

		private EDeviceState FDeviceState;

		private DOnDeviceStateChanged FOnDeviceStateChanged;
		public void SetOnDeviceStateChanged(DOnDeviceStateChanged value)
		{
			FOnDeviceStateChanged = value;
		}

		public CDevice()
		{
			FDeviceState = INIT_DEVICESTATE;
		}

		public EDeviceState DeviceState
		{
			get { return FDeviceState; }
			set 
			{
				if (value != DeviceState)
				{
          if (FNotifier is CNotifier)
          {
            FNotifier.Write(CIFCommand.HEADER_LIBRARY,
                            String.Format("DeviceState : {0} -> {1}", DeviceState, value));
          }
					FDeviceState = value;
					if (FOnDeviceStateChanged is DOnDeviceStateChanged)
					{
						FOnDeviceStateChanged(FDeviceState);
					}
				}
			}
		}


	}
}
