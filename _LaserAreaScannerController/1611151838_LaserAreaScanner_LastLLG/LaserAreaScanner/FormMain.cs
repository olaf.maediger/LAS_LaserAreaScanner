﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using HWComPort;
using IFCommand;
//
namespace LaserAreaScanner
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormMain : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //		
    private const String INIT_DEVICENAME = "NetworkDeviceObserver"; // CLIENT!!!
    private const String INIT_DEVICETYPE = "NDO";
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = false;//true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    //		
    private CComPort FComPort;
    private CCommandList FCommandList;
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //
      FCommandList = new CCommandList();
      FCommandList.SetNotifier(FUCNotifier);
      //
      // ??? FUCComPort.SetOnAddStatement(UCComPortOnAddStatement);
      FUCComPort.RefreshComPortControls();
      //
      FUCTerminal.SetNotifier(FUCNotifier);
      FUCTerminal.SetOnSendData(UCTerminalOnSendData);
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      //
      FCommandList.AbortExecution();
      FCommandList.Clear();
      //
      if (FComPort is CComPort)
      {
        FComPort.Close();
      }
      FComPort = null;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      //Int32 IValue = INIT_AUTOINTERVAL;
      //Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      //Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxAutomate.Checked)
      {
        FStartupState = 0;
        tmrStartup.Enabled = true;
      }
      else
      {
        tmrStartup.Enabled = false;
        FCommandList.AbortExecution();
        FCommandList.Clear();
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper - 
    //------------------------------------------------------------------------
    //
 
    //
    //########################################################################
    //
    //------------------------------------------------------------------------
    //  Section - Callback - FUCTerminal
    //------------------------------------------------------------------------
    // 
    private void UCTerminalOnSendData(String txdtext,
                                      Int32 delaycharacter,
                                      Int32 delaycr,
                                      Int32 delaylf)
    {
      FCommandList.ComPort = FUCComPort.ComPort;
      String[] Arguments = new String[4];
      Arguments[0] = txdtext;
      Arguments[1] = delaycharacter.ToString();
      Arguments[2] = delaycr.ToString();
      Arguments[3] = delaylf.ToString();
      CTransmitTextDelay Command = new CTransmitTextDelay(FCommandList, Arguments);
      FCommandList.Add(Command);
      FCommandList.StartExecution();
      // NC FUCComPort.AddCommandTransmitTextDelay(Arguments);
    }

 
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    //
    private Boolean OnStartupTick(Int32 startupstate)
    {
      String[] Arguments;
      switch (FStartupState)
      {
        case 0:
          FCommandList.AbortExecution();
          if (FComPort is CComPort)
          {
            FComPort.Close();
            FComPort = null;
          }
          FComPort = new CComPort();
          FCommandList.SetComPort(FComPort);
          FCommandList.SetNotifier(FUCNotifier);
          return true;
        case 1:
          RComPortData CPD;
          FComPort.GetComPortData(out CPD);
          CPD.ComPort = EComPort.cp1;
          CPD.Baudrate = EBaudrate.br115200;
          CPD.Parity = EParity.paNone;
          CPD.Databits = EDatabits.db8;
          CPD.Stopbits = EStopbits.sb1;
          CPD.Handshake = EHandshake.hsNone;
          return FComPort.Open(CPD);
          //return true;
        case 2:
          Arguments = new String[1];
          Arguments[0] = "1";
          FCommandList.Add(new CSetLed(FCommandList, Arguments));
          return true;
        case 3:
          FCommandList.StartExecution();
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          return true;
        case 7:
          return true;
        case 8:
          return true;
        case 9:
          return true;
        case 10:
          return true;
        case 11:
          //cbxAutomate.Checked = false;
          //return false;
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          cbxAutomate.Checked = false;
          return false;
      }
    }

  }
}
