﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
//
using UCNotifier;
//
namespace Xml
{
  //
  //------------------------------------------
  //	Segment - Definition
  //------------------------------------------
  //
  public abstract class CXmlBase
  {
    //
    //------------------------------------------
    //	Segment - Constant
    //------------------------------------------
    //
    public const Boolean INIT_PROTOCOLENABLED = false;
    public const Int32 INIT_TABULATOR = 0;
    //
    //------------------------------------------
    //	Segment - Field
    //------------------------------------------
    //
    protected CNotifier FNotifier;
    protected Int32 FTabulator;
    protected Boolean FProtocolEnabled;
    //
    //------------------------------------------
    //	Segment - Constructor
    //------------------------------------------
    //
    public CXmlBase()
    {
      FProtocolEnabled = INIT_PROTOCOLENABLED;
      FTabulator = INIT_TABULATOR;
    }
    //
    //------------------------------------------
    //	Segment - Property
    //------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void EnableProtocol(Boolean enable)
    {
      FProtocolEnabled = enable;
    }
    //
    //------------------------------------------
    //	Segment - Helper
    //------------------------------------------
    //
    public void WriteIncrement()
    {
      FTabulator++;
    }
    public void WriteDecrement()
    {
      if (0 < FTabulator)
        FTabulator--;
    }

    protected void CountIncrement(Int32 count)
    {
      for (Int32 CI = 0; CI < count; CI++)
      {
        WriteIncrement();
      }
    }
    protected void CountDecrement(Int32 count)
    {
      for (Int32 CI = 0; CI < count; CI++)
      {
        WriteDecrement();
      }
    }

    public void Write(String line)
    {
      //if (FNotifier is CNotifier)
      //{
      //  if (FProtocolEnabled)
      //  {
      //    String Header = "";
      //    for (Int32 CI = 0; CI < FTabulator; CI++)
      //    {
      //      Header += "  ";
      //    }
      //    FNotifier.Write(Header + line);
      //  }
      //}
    }






  }
}
