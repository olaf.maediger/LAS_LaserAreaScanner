﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
//
namespace Xml
{
  public struct RXmlHeader
  {
    public const String INIT_PROJECT = "project";
    public const String INIT_VERSION = "01.01.01";
    public const String INIT_AUTHOR = "author";
    //
    public String Project;
    public String Version;
    public Int32 Year;
    public Int32 Month;
    public Int32 Day;
    public Int32 Hour;
    public Int32 Minute;
    public Int32 Second;
    public String Author;
    //
    public RXmlHeader(Int32 init)
    {
      Project = INIT_PROJECT;
      Version = INIT_VERSION;
      DateTime DT = DateTime.Now;
      Year = DT.Year;
      Month = DT.Month;
      Day = DT.Day;
      Hour = DT.Hour;
      Minute = DT.Minute;
      Second = DT.Second;
      Author = INIT_AUTHOR;
    }
  }

  public class CXmlHeader
  {
    //
    //------------------------------------------
    //	Constant
    //------------------------------------------
    //
    public const String TOKEN_COMMENT = "#comment";
    //
    public const String TITLEVERSION = "1.0";
    public const String TITLEENCODING = "utf-8";
    public const String TITLESTANDALONE = "yes";
    //
    public const String STRING = "String";
    public const String BYTE = "Byte";
    public const String INT32 = "Int32";
    public const String BOOLEAN = "Boolean";
    public const String DOUBLE = "Double";
    //
    //	Header
    //
    public const String MAIN = "Main";
    public const String COMMENT = "Comment";
    public const String HEADER = "Header";
    public const String PROJECT = "Project";
    public const String VERSION = "Version";
    public const String YEAR = "Year";
    public const String MONTH = "Month";
    public const String DAY = "Day";
    public const String HOUR = "Hour";
    public const String MINUTE = "Minute";
    public const String SECOND = "Second";
    public const String AUTHOR = "Author";
    //
    //public const String VISIBLE = "Visible";
    //public const String TEXT = "Text";
    //public const String POSITION = "Position";
    //public const String FONT = "Font";
    ////
    //public const String LEFT = "Left";
    //public const String TOP = "Top";
    //public const String WIDTH = "Width";
    //public const String HEIGHT = "Height";
    //public const String OFFSETX = "OffsetX";
    //public const String OFFSETY = "OffsetY";
    ////
    //public const String OFFSETLEFT = "OffsetLeft";
    //public const String OFFSETTOP = "OffsetTop";
    //public const String OFFSETRIGHT = "OffsetRight";
    //public const String OFFSETBOTTOM = "OffsetBottom";
    ////
    //public const String TITLE = "Title";
    //public const String FORMAT = "Format";
    ////
    //public const String DELTA = "Delta";
    ////
    //public const String LABEL = "Label";
    ////
    //public const String GUID = "Guid";
    //
  }
}
