﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using ComPort;
//
namespace HWDcMotorController
{
  public class CSetSweepStateChannelA : CCommand
  {
    public const String INIT_NAME = "SetSweepStateChannelA";
    public const String COMMAND_TEXT = "SWA";

    private ESweepState FSweepState;

    public CSetSweepStateChannelA(CHardwareDevice hardwaredevice,
                                  ESweepState value)
      : base(hardwaredevice, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, (Int32)value))
    {
      SetParent(this);
      FSweepState = value;
    }

    public override void OnSerialDataReceived(String data)
    {
      base.OnSerialDataReceived(data);
      String Line = String.Format("{0}: {1}", Name, data);
      Notifier.Write(CHWDcMotorController.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = data.Split(' ');
      if (2 < Tokenlist.Length)
      {
        ESweepState SS = CHWDcMotorController.StringIndexToSweepState(Tokenlist[2]);
        // !!!!!!!!!!!! Commandlist.Library.RefreshSweepStateChannelA(SS);
      }
    }
  }
}
