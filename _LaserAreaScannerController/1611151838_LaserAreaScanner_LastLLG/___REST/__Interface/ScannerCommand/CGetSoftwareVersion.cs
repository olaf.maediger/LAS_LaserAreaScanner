﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCNotifier;
using ComPort;

namespace HWDcMotorController
{
	public class CGetSoftwareVersion : CCommand
	{
    public const String INIT_NAME = "GetSoftwareVersion";
    public const String COMMAND_TEXT = "GSV";

    public CGetSoftwareVersion(CHardwareDevice hardwaredevice)
      : base(hardwaredevice, INIT_NAME, COMMAND_TEXT)
    {
      SetParent(this);
    }

    public override void OnSerialDataReceived(String data)
    {
      base.OnSerialDataReceived(data);
      String Line = String.Format("{0}: {1}", Name, data);
			Notifier.Write(CHWDcMotorController.HEADER_LIBRARY, Line);
			// Analyse Response to refresh Gui
      String[] Tokenlist = data.Split(' ');
			if (2 < Tokenlist.Length)
			{
        // !!!!!!!!!!!! Commandlist.RefreshSoftwareVersion(Tokenlist[2]);
			}
		}

	}
}
