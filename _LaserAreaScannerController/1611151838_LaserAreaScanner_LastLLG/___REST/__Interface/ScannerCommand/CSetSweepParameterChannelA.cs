﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using ComPort;
//
namespace HWDcMotorController
{
  public class CSetSweepParameterChannelA : CCommand
  {
    public const String INIT_NAME = "SetSweepParameterChannelA";
    public const String COMMAND_TEXT = "SSA";

    private Int32 FIndex;
    private Int32 FValue;

    public CSetSweepParameterChannelA(CHardwareDevice hardwaredevice,
                                      Int32 index,
                                      Int32 value)
      : base(hardwaredevice, INIT_NAME,
             String.Format("{0} {1} {2}", COMMAND_TEXT, index, value))
    {
      SetParent(this);
      FIndex = index;
      FValue = value;
    }

    public override void OnSerialDataReceived(String data)
    {
      base.OnSerialDataReceived(data);
      String Line = String.Format("{0}: {1}", Name, data);
      Notifier.Write(CHWDcMotorController.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = data.Split(' ');
      if (3 < Tokenlist.Length)
      {
        if (FIndex == Int32.Parse(Tokenlist[2]))
        {
          if (FValue == Int32.Parse(Tokenlist[3]))
          {
            // !!!!!!!!!!!! Commandlist.Library.RefreshSweepParameterChannelA(FIndex, FValue);
          }
        }
      }
    }
  }
}
