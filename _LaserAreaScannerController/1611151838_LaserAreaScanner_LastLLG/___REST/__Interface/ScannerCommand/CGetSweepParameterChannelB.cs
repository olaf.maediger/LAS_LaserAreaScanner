﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using ComPort;
//
namespace HWDcMotorController
{
  public class CGetSweepParameterChannelB : CCommand// CCommandSweepBase
  {
    public const String INIT_NAME = "GetSweepParameterChannelB";
    public const String COMMAND_TEXT = "GSB";

    private Int32 FIndex;

    public CGetSweepParameterChannelB(CHardwareDevice hardwaredevice,
                                      Int32 index)
      : base(hardwaredevice, INIT_NAME,
             String.Format("{0} {1}", COMMAND_TEXT, index))
    {
      SetParent(this);
      FIndex = index;
    }

    public override void OnSerialDataReceived(String data)
    {
      base.OnSerialDataReceived(data);
      String Line = String.Format("{0}: {1}", Name, data);
      Notifier.Write(CHWDcMotorController.HEADER_LIBRARY, Line);
      // Analyse Response to refresh Gui
      String[] Tokenlist = data.Split(' ');
      if (3 < Tokenlist.Length)
      {
        if (FIndex == Int32.Parse(Tokenlist[2]))
        {
          Int32 Value = Int32.Parse(Tokenlist[3]);
          // !!!!!!!!!!!! Commandlist.Library.RefreshSweepParameterChannelB(FIndex, Value);
        }
      }
    }
  }
}
