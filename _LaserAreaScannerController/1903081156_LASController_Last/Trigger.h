//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#ifndef Trigger_h
#define Trigger_h
//
#include "Defines.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
enum ETriggerDirection
{
  tdInput = 0,
  tdOutput = 1
};
enum ETriggerLevel
{
  tlUndefined = -1,
  tlPassive = 0,
  tlActive = 1
};
//
class CTrigger
{
  private:
  int FPin;
  bool FInverted;
  ETriggerDirection FDirection;
  ETriggerLevel FLevel;
  //  
  public:
  CTrigger(int pin, ETriggerDirection triggerdirection);
  CTrigger(int pin, ETriggerDirection triggerdirection, bool inverted);
  //
  ETriggerLevel GetLevel();
  //
  Boolean Open();
  Boolean Close();
  void SetActive();
  void SetPassive();
};
//
#endif // Trigger_h
