#include "Defines.h"
#if defined(PROCESSOR_STM32F103C8)
//
#ifndef IrqTimerStm_h
#define IrqTimerStm_h
//
#include "Arduino.h"
#include <HardwareTimer.h>
//
class CHardwareTimerStm : public HardwareTimer
{
  protected:
	UInt8 FTimerID;
  UInt32 FPeriodus;
  void (*FIrqHandler)(void);
  //
  public:
	CHardwareTimerStm(UInt8 timerid);
	void SetInterruptHandler(void (*irqhandler)());
	void ResetInterruptHandler(void);
	void Start(void);
	void Stop(void);
	void SetPeriodus(UInt32 microseconds);
	UInt32 GetPeriodus(void);
};
//
#endif // IrqTimerStm_h
//
#endif // defined(PROCESSOR_STM32F103C8)

