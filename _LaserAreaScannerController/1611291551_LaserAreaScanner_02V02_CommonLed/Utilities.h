#ifndef Utilities_h
#define Utilities_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
//
#define SIZE_TXDBUFFER 64
#define SIZE_RXDBUFFER 64
#define SIZE_RXDPARAMETER 8
#define COUNT_RXDPARAMETERS 3
//
#define INDEX_LEDSYSTEM  0
#define INDEX_LEDERROR   1
#define INDEX_LEDBUSY    2
#define INDEX_LEDMOTIONX 3
#define INDEX_LEDMOTIONY 4
#define INDEX_LEDTRIGGER 5
//
#define PIN_LEDSYSTEM   13
#define PIN_LEDERROR    12
#define PIN_LEDBUSY     11
#define PIN_LEDMOTIONX  10
#define PIN_LEDMOTIONY   9
#define PIN_LEDTRIGGER   8
//
void WriteProgramHeader(CSerial &serialcommand);
void WriteHelp(CSerial &serialcommand);
//  Handler - Error
void HandleError(CSerial &serial);
//  Handler - SerialPC Communication
void HandleSerialCommands(CSerial &serialcommand);//, CSerial &serialtarget);
//
#endif
