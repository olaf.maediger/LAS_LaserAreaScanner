#ifndef Command_h
#define Command_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
//
class CCommand
{
  private:
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Character FRxdCommandLine[SIZE_RXDBUFFER];
  PCharacter FPRxdCommand;
  PCharacter FPRxdParameters[COUNT_RXDPARAMETERS];
  Int16 FRxdParameterCount = 0;
  Int16 FRxdBufferIndex = 0;

  public:
  CCommand();
  ~CCommand();
  //
  Int16 GetRxdParameterCount()
  {
    return FRxdParameterCount;
  }
  PCharacter GetPRxdParameters(UInt8 index)
  {
    return FPRxdParameters[index];
  }
  PCharacter GetTxdBuffer()
  {
    return FTxdBuffer;
  }
  PCharacter GetPRxdCommand()
  {
    return FPRxdCommand;
  }
  //
  void ZeroRxdBuffer();
  void ZeroTxdBuffer();
  void ZeroRxdCommandLine();
  //
  void InitSerialCommand();
  Boolean DetectRxdCommandLine(CSerial &serial);
  Boolean AnalyseRxdCommand(CSerial &serial);
};
//
#endif // Command_h
