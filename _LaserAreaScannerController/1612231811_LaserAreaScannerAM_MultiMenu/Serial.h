#ifndef Serial_h
#define Serial_h
//
#include "Arduino.h"
#include "Defines.h"
#include "HardwareSerial.h"
#ifdef ARDUINODUE
#include "SerialUsbApi.h"
#endif
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//


//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
class CSerialBase
{
  protected:
  Boolean FLocalEcho;
  public:
  CSerialBase();
  //
  virtual Boolean Open(long unsigned baudrate) = 0;
  virtual Boolean Close() = 0;
  virtual Boolean IsOpen() = 0;
  virtual void WriteCharacter(Character character) = 0;
  virtual void WriteText(PCharacter ptext) = 0;
  virtual void WriteLine(PCharacter ptext) = 0;
  virtual Int16 GetRxdByteCount() = 0;
  virtual Character ReadCharacter() = 0;
  //
  Boolean GetLocalEcho();
  void SetLocalEcho(Boolean localecho);
  void WriteNewLine();
  void WritePrompt();
  void WriteAnswer();
};
//
//----------------------------------------------------
//  Segment - CSerialN - Normal Serial, Serial1/2/3
//----------------------------------------------------
//
class CSerialN : public CSerialBase
{
  private:
  HardwareSerial *FPSerial;
  public:
  CSerialN(HardwareSerial *pserial);
  //
  Boolean Open(long unsigned baudrate);
  Boolean Close();
  Boolean IsOpen();
  void WriteCharacter(Character character);
  void WriteText(PCharacter ptext);
  void WriteLine(PCharacter ptext);
  Int16 GetRxdByteCount();
  Character ReadCharacter();
};
//
#ifdef ARDUINODUE
//
//----------------------------------------------------
//  Segment - CSerialU - SerialUSB
//----------------------------------------------------
//
class CSerialU : public CSerialBase
{
  private:
  Serial_ *FPSerial;
  public:
  CSerialU(Serial_ *pserial); 
  //
  Boolean Open(long unsigned baudrate);
  Boolean Close();
  Boolean IsOpen();
  void WriteCharacter(Character character);
  void WriteText(PCharacter ptext);
  void WriteLine(PCharacter ptext);
  Int16 GetRxdByteCount();
  Character ReadCharacter();
};
#endif
//
//----------------------------------------------------
//  Segment - CSerial - Global Access for ALL Serials
//----------------------------------------------------
//
class CSerial 
{
  private:
  CSerialBase *FPSerial;
  public:
  CSerial(HardwareSerial *pserial);
#ifdef ARDUINODUE  
  CSerial(Serial_ *pserial);
#endif  
  //
  Boolean Open(long unsigned baudrate);
  Boolean Close();
  Boolean IsOpen();
  //
  Boolean GetLocalEcho();  
  void SetLocalEcho(Boolean localecho);
  //
  void WriteCharacter(Character character);
  void WriteText(PCharacter ptext);
  void WriteLine(PCharacter ptext);
  void WriteNewLine();
  void WriteAnswer();
  void WritePrompt();
  void WriteInt32(Int32 value);
  void WriteDouble(Double value);
  //
  Int16 GetRxdByteCount();
  Character ReadCharacter();
};


#endif // Serial_h
