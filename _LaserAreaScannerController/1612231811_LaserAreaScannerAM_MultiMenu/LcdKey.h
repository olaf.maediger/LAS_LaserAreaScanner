//
//--------------------------------
//  Library LcdKeypad
//--------------------------------
//
#ifndef LcdKey_h
#define LcdKey_h
//
#include "Arduino.h"
#include <LiquidCrystal.h>
#include "Defines.h"
// imported commands from default LCD-library;
//    LiquidCrystal()
//    begin()
//    clear()
//    home()
//    setCursor()
//    write()
//    print()
//    cursor()
//    noCursor()
//    blink()
//    noBlink()
//    display()
//    noDisplay()
//    scrollDisplayLeft()
//    scrollDisplayRight()
//    autoscroll()
//    noAutoscroll()
//    leftToRight()
//    rightToLeft()
//    createChar() 
//
//--------------------------------
//  Section - Constant
//--------------------------------
// 
#define LCDKEY_COUNTCOL 16
#define LCDKEY_COUNTROW 2
//
//#define ANALOG_KEY_NONE   1023
//#define ANALOG_KEY_SELECT 741
//#define ANALOG_KEY_LEFT   503
//#define ANALOG_KEY_DOWN   326
//#define ANALOG_KEY_UP     142
//#define ANALOG_KEY_RIGHT  0
//
#define ANALOG_KEY_NONE   1023
#define ANALOG_KEY_SELECT 800
#define ANALOG_KEY_LEFT   600
#define ANALOG_KEY_DOWN   400
#define ANALOG_KEY_UP     200
#define ANALOG_KEY_RIGHT  60
//
enum EKeyCode
{
  kcNone = 0,
  kcSelectLH = 1,
  kcSelectHL = 2,
  kcLeftLH = 3,
  kcLeftHL = 4,
  kcDownLH = 5,
  kcDownHL = 6,
  kcUpLH = 7,
  kcUpHL = 8,
  kcRightLH = 9,
  kcRightHL = 10
};
//
class CLcdKey
{
  private:
  LiquidCrystal* FPDisplay;
  int FCursorCol, FCursorRow;
  EKeyCode FKeyCode;
 
  public:
  CLcdKey();
  Boolean Open();
  Boolean Close();
  //
  void ClearScreen();
  void IncrementCursor(const Byte count = 1);
  void SetCursor(Byte col, Byte row);
  //
  void WriteText(PCharacter ptext);
  void WriteCharacter(Character character);
  void WriteInt16(Int16 value);
  //
  void WriteText(Byte col, Byte row, PCharacter ptext);
  void WriteCharacter(Byte col, Byte row, Character character);
  void WriteInt16(Byte col, Byte row, Byte digitcount, Int16 value);
  //
  Boolean RefreshKey();
  EKeyCode GetKeyCode();
  void SetKeyCode(EKeyCode value);
};
//
#endif // LcdKey_h
