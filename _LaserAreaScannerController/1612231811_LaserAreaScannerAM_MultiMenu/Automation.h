#ifndef Automation_h
#define Automation_h
//
#include "Defines.h"
#include "Helper.h"
#include "Utilities.h"
#include "Serial.h"
#include "Led.h"
#include "LcdKey.h"
#include "RotaryDecoder.h"
//
enum EStateAutomation
{ // Welcome 0
  saInitWelcome = 0,  // 0.0 -[]>0.1
  saShowWelcome = 1,  // 0.1 -[1s]>1.0
  // Idle 1
  saInitIdle = 2,     // 1.0 -[]>1.1
  saSystemIdle = 3,   // 1.1 -[S]>2.0
  // Offset 2
  saInitOffset = 4,   // 2.0 -[]>2.1
  saAdjustOffset = 5, // 2.1 -[S]>3.0
  // ScanArea 3
  saScanAreaInit = 6, // 3.0 -[]>3.1 -[S]>1.0
  saScanAreaWait = 7, // 3.1 -[U]>3.1 -[S]>1.0
  saScanAreaBusy = 8  // 3.2 -[End]>1.0 -[S]>1.0
};
//
#define INIT_STATEAUTOMATION saInitWelcome
//
#define TIME_SHOWWELCOME 1000
//
void HandleAutomation(CSerial &serial);
//
#endif // Automation_h
