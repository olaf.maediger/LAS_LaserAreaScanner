//###################################################
// Project:  LASAM - LaserAreaScannerArduinoMega
// Hardware: Arduno Mega 2560
// Function now: Serial-Command-Dispatcher
// - ArduinoMega gives Commands to ArduinoDue and gets Responses / Events
// Function later: User-Input-Output-Processor
// Parts:
// - ArduinoMega2560 16MHz 5V0 8bit Atmel ATmega2560
//   - Serial1 : CommandChannel to PC (here: ArduinoDue 3V3)
//     - Levelshifter 5V0 - 3V3: Txd, Rxd
//   - Serial2 : Usb-FtdiFT232RL-Serial for Debugging
//   - LedSystem : System-State debugging
//   - LCD Keypad Shield Sainsmart
//     - 16x2 LCDisplay
//     - 6 Keys (Select, Left, Up, Down, Right, Reset)
// later - RotaryEncoder (Hardware Rotation->ABPulses)
// later   - adapted to CRotaryDecoder (Software ABPulses->Position)
//###################################################
//
#include "Defines.h"
#include "Helper.h"
#include "Utilities.h"
#include "Led.h"
#include "Serial.h"
#include "Command.h"
#include "Dispatcher.h"
#include "Automation.h"
#include "LcdKey.h"
#include "RotaryDecoder.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables
//---------------------------------------------------
//
enum EErrorCode GlobalErrorCode;
EStateAutomation StateAutomation;
CCommand Command;
Boolean CommandSwitchSlave;
//
UInt32 PositionOffsetX, PositionOffsetY;
UInt32 PositionMirrorX, PositionMirrorY;
//
//---------------------------------------------------
// Segment - Global - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM);
//
//---------------------------------------------------
// Segment - Global - LcdKey
//---------------------------------------------------
//
CLcdKey LcdKey;
//
//---------------------------------------------------
// Segment - Global - LcdKey
//---------------------------------------------------
//
CRotaryDecoder RotaryDecoderX(PIN_ENCODERA_X, PIN_ENCODERB_X, PIN_SWITCH_X);
CRotaryDecoder RotaryDecoderY(PIN_ENCODERA_Y, PIN_ENCODERB_Y, PIN_SWITCH_Y);
//
//---------------------------------------------------
// Segment - Global - Serial
//---------------------------------------------------
//
CSerial SerialPC(&Serial1);
CSerial SerialSlave(&Serial2);
//#ifdef ARDUINODUE
////CSerial SerialUsb(&SerialUSB);
//#endif   
//
void setup() 
{ 
  GlobalErrorCode = INIT_GLOBALERRORCODE;
  StateAutomation = INIT_STATEAUTOMATION;
  CommandSwitchSlave = INIT_COMMANDSWITCHSLAVE;
  //
  PositionOffsetX = INIT_POSITIONOFFSET_X;
  PositionMirrorX = PositionOffsetX;
  PositionOffsetY = INIT_POSITIONOFFSET_Y;
  PositionMirrorY = PositionOffsetY;
  //
  LedSystem.Open();
  //
  LcdKey.Open();
  //
  RotaryDecoderX.Open();
  RotaryDecoderX.SetPositionMinimum(POSITIONENCODER_MINIMUM);
  RotaryDecoderX.SetPositionMaximum(POSITIONENCODER_MAXIMUM);
  RotaryDecoderX.SetPositionActual(POSITIONENCODER_INIT);
  //
  RotaryDecoderY.Open();
  RotaryDecoderY.SetPositionMinimum(POSITIONENCODER_MINIMUM);
  RotaryDecoderY.SetPositionMaximum(POSITIONENCODER_MAXIMUM);
  RotaryDecoderY.SetPositionActual(POSITIONENCODER_INIT);
  //
  SerialPC.Open(115200);
  SerialSlave.Open(115200);
  //
//  WriteProgramHeader(SerialPC);
//  WriteHelp(SerialPC);
//  SerialPC.WritePrompt();
}

void loop() 
{
  //HandleError(SerialPC);  
  //HandleCommand(SerialPC, SerialSlave);
  //DirectTransferResponse(SerialSlave, SerialPC);
  HandleAutomation(SerialPC);
}



