//
//--------------------------------
//  Library Led
//--------------------------------
//
#ifndef Led_h
#define Led_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
// [ms]
#define INIT_LEDPERIODBLINK ((long unsigned)1000)
#define INIT_COUNTPRESET ((long unsigned)1)
#define INIT_COUNTACTUAL ((long unsigned)0)
//
enum EStateLed
{
  slUndefined = -1,
  slOff = 0,
  slOn = 1
};
//
class CLed
{
  private:
  int FPin;
  EStateLed FState;
  long unsigned FPeriodBlink; 
  long unsigned FCountPreset;
  long unsigned FCountActual;
 
  public:
  CLed(int pin);
  Boolean Open();
  Boolean Close();
  EStateLed GetState();
  Boolean On();
  Boolean Off();
  Boolean Toggle();
  Boolean Pulse(UInt32 microseconds);
  void SetPeriodBlink(long unsigned period);
  long unsigned GetPeriodBlink();
  long unsigned GetPeriodBlinkHalf();
  void SetCountPreset(long unsigned count);
  long unsigned GetCountPreset();
  void SetCountActual(long unsigned count);
  long unsigned GetCountActual();
  Boolean CountActualIncrement();
};
//
#endif
