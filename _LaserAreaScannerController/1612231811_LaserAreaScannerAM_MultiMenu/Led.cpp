//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "Led.h"
//
CLed::CLed(int pin)
{
  FPin = pin;
  FState = slUndefined;
  FPeriodBlink = INIT_LEDPERIODBLINK;
  FCountPreset = INIT_COUNTPRESET;
  FCountActual = INIT_COUNTACTUAL;
}

Boolean CLed::Open()
{
  pinMode(FPin, OUTPUT);
  digitalWrite(FPin, LOW);
  FState = slOff;
  return true;
}

Boolean CLed::Close()
{
  pinMode(FPin, INPUT);
  FState = slUndefined;
  return true;
}

EStateLed CLed::GetState()
{
  return FState;
}

void CLed::SetPeriodBlink(long unsigned period)
{
  FPeriodBlink = period;
}
long unsigned CLed::GetPeriodBlink()
{
  return FPeriodBlink;
}
long unsigned CLed::GetPeriodBlinkHalf()
{
  return FPeriodBlink / 2;
}

void CLed::SetCountPreset(long unsigned count)
{
  FCountPreset = count;
}
long unsigned CLed::GetCountPreset()
{
  return FCountPreset;
}

void CLed::SetCountActual(long unsigned count)
{
  FCountActual = count;
}
long unsigned CLed::GetCountActual()
{
  return FCountActual;
}
Boolean CLed::CountActualIncrement()
{
  if (FCountActual < FCountPreset)
  {
    FCountActual++;
  }
  // Target reached?
  return (FCountActual == FCountPreset);
}

Boolean CLed::On()
{
  digitalWrite(FPin, HIGH);
  FState = slOn;
  return true;
}

Boolean CLed::Off()
{
  digitalWrite(FPin, LOW);
  FState = slOff;
  return true;
}

Boolean CLed::Toggle()
{
  if (slOff == FState)
  {
    digitalWrite(FPin, HIGH);
    FState = slOn;
    return true;
  } 
  if (slOn == FState)
  {
    digitalWrite(FPin, LOW);
    FState = slOff;
  } 
  return false;
}

Boolean CLed::Pulse(UInt32 microseconds)
{
  digitalWrite(FPin, HIGH);
  FState = slOn;
  delayMicroseconds(microseconds);  
  digitalWrite(FPin, LOW);
  FState = slOff;
}


