//
//--------------------------------
//  Library LcdKey
//--------------------------------
//
#include "LcdKey.h"
//
CLcdKey::CLcdKey()
{
  FPDisplay = new LiquidCrystal(8, 9, 4, 5, 6, 7);
}

Boolean CLcdKey::Open()
{
  FPDisplay->begin(LCDKEY_COUNTCOL, LCDKEY_COUNTROW);
  FPDisplay->clear();
  FPDisplay->display();
  FCursorCol = 0;
  FCursorRow = 0;
  analogReference(DEFAULT);
  pinMode(A0, INPUT);
  FKeyCode = kcNone;
  return true;
}

Boolean CLcdKey::Close()
{
  FPDisplay->noDisplay();
  return true;
}


void CLcdKey::ClearScreen()
{
  FCursorCol = 0;
  FCursorRow = 0;
  FPDisplay->clear();
}

void CLcdKey::IncrementCursor(const Byte count)
{
  Byte Count, Index;
  for (Index = 0; Index < Count; Index++)
  {
    FCursorCol++;
    if (LCDKEY_COUNTCOL <= FCursorCol)
    {
      FCursorCol = 0;
      FCursorRow++;
      if (LCDKEY_COUNTROW <= FCursorRow)
      {
        FCursorRow = 0;
      }
    }    
  }
  FPDisplay->setCursor(FCursorCol, FCursorRow);
}

void CLcdKey::SetCursor(Byte col, Byte row)
{
  FCursorCol = col;
  FCursorRow = row;
  FPDisplay->setCursor(FCursorCol, FCursorRow);  
}



void CLcdKey::WriteCharacter(Character character)
{
  FPDisplay->write(character);
  IncrementCursor(1);  
}

void CLcdKey::WriteText(PCharacter ptext)
{
  FPDisplay->write(ptext);
  IncrementCursor(strlen(ptext));
}

void CLcdKey::WriteInt16(Int16 value)
{
  char Buffer[10];
  sprintf(Buffer, "%d", value);
  FPDisplay->write(Buffer);
  IncrementCursor(strlen(Buffer));
}



void CLcdKey::WriteCharacter(Byte col, Byte row, Character character)
{
  SetCursor(col, row);
  FPDisplay->write(character);
  IncrementCursor(1);  
}

void CLcdKey::WriteText(Byte col, Byte row, PCharacter ptext)
{
  SetCursor(col, row);
  FPDisplay->write(ptext);
  IncrementCursor(strlen(ptext));
}

void CLcdKey::WriteInt16(Byte col, Byte row, Byte digitcount, Int16 value)
{
  SetCursor(col, row);
  char Buffer[10];
  char Format[4];
  sprintf(Format, " %d ", digitcount);
  Format[0] = '%';
  Format[1] = 0x30 + digitcount;
  Format[2] = 'd';
  Format[3] = 0x00;
  sprintf(Buffer, Format, value);
  FPDisplay->write(Buffer);
  IncrementCursor(strlen(Buffer));
}




//Boolean CLcdKey::RefreshKey()
//{
//  int AKC = analogRead(A0);
////  if (AKC < 100)
////  { // WriteText(0, 0, "R");
////    if (kcNone == FKeyCode)
////    { // first time -> rising edge
////      SetKeyCode(kcRightLH);
////    }
////    return true;
////  } 
////  if (AKC < 200)
////  { // WriteText(0, 0, "U");
////    if (kcNone == FKeyCode)
////    { // first time -> rising edge
////      SetKeyCode(kcUpLH);
////    }
////    return true;
////  } 
////  if (AKC < 400)
////  { // WriteText(0, 0, "D");
////    if (kcNone == FKeyCode)
////    { // first time -> rising edge
////      SetKeyCode(kcDownLH);
////    }
////    return true;
////  } 
////  if (AKC < 600)
////  { // WriteText(0, 0, "L");
////    if (kcNone == FKeyCode)
////    { // first time -> rising edge
////      SetKeyCode(kcLeftLH);
////    }
////    return true;
////  } 
//  if (AKC < 800)
//  { // 
//    WriteText(0, 0, "SLH");
//    if (kcNone == FKeyCode)
//    { // first time -> rising edge
//      SetKeyCode(kcSelectLH);
//    }
//    return true;
//  } 
//  // no Key pressed
//  switch (FKeyCode)
//  { // first time -> falling edge
//    kcSelectLH:
//      WriteText(0, 0, "SHL");
//      SetKeyCode(kcSelectHL);
//      return true;
////    kcLeftLH:
////      SetKeyCode(kcLeftHL);
////      return true;
////    kcDownLH:
////      SetKeyCode(kcDownHL);
////      return true;
////    kcUpLH:
////      SetKeyCode(kcUpHL);
////      return true;
////    kcRightLH:
////      SetKeyCode(kcRightHL);
////      return true;
//  }
//  return false;
//}

//void CLcdKey::SetKeyCode(EKeyCode value)
//{
//  //if (value != FKeyCode)
//  {
//    FKeyCode = value;
//    WriteInt16(13, 0, 1, FKeyCode);
//    delay(1000);
//  }
//}
//
//EKeyCode CLcdKey::GetKeyCode()
//{ // handshake - gets and reset Keycode
//  EKeyCode KC = FKeyCode;
//  switch (KC)
//  { // first time -> falling edge
//    kcSelectHL:
//      SetKeyCode(kcNone);
//      break;
//    kcLeftHL:
//      SetKeyCode(kcNone);
//      break;
//    kcDownHL:
//      SetKeyCode(kcNone);
//      break;
//    kcUpHL:
//      SetKeyCode(kcNone);
//      break;
//    kcRightHL:
//      SetKeyCode(kcNone);
//      break;
//  }
//  return KC;
//}



Boolean CLcdKey::RefreshKey()
{
  int AKC = analogRead(A0);
  if (AKC < 100)
  { 
    if (kcNone == FKeyCode)
    {
      SetKeyCode(kcRightLH);
      return true;
    }
  } 
  if (AKC < 200)
  { 
    if (kcNone == FKeyCode)
    {
      SetKeyCode(kcUpLH);
      return true;
    }
  } 
  if (AKC < 400)
  { 
    if (kcNone == FKeyCode)
    {
      SetKeyCode(kcDownLH);
      return true;
    }
  } 
  if (AKC < 600)
  { 
    if (kcNone == FKeyCode)
    {
      SetKeyCode(kcLeftLH);
      return true;
    }
  } 
  if (AKC < 800)
  { 
    if (kcNone == FKeyCode)
    {
      SetKeyCode(kcSelectLH);
      return true;
    }
  } 
  // no key pressed:
  if (kcRightLH == FKeyCode)
  {
    SetKeyCode(kcRightHL);   
    return true;
  }
  if (kcUpLH == FKeyCode)
  {
    SetKeyCode(kcUpHL);   
    return true;
  }
  if (kcDownLH == FKeyCode)
  {
    SetKeyCode(kcDownHL);   
    return true;
  }
  if (kcLeftLH == FKeyCode)
  {
    SetKeyCode(kcLeftHL);   
    return true;
  }
  if (kcSelectLH == FKeyCode)
  {
    SetKeyCode(kcSelectHL);   
    return true;
  }
  return false;
}

void CLcdKey::SetKeyCode(EKeyCode value)
{
  FKeyCode = value;
  delay(100);
}

EKeyCode CLcdKey::GetKeyCode()
{ 
  EKeyCode KC = FKeyCode;
  if (kcSelectHL == FKeyCode)
  {
    SetKeyCode(kcNone);   
  }
  if (kcLeftHL == FKeyCode)
  {
    SetKeyCode(kcNone);   
  }
  if (kcDownHL == FKeyCode)
  {
    SetKeyCode(kcNone);   
  }
  if (kcUpHL == FKeyCode)
  {
    SetKeyCode(kcNone);   
  }
  if (kcRightHL == FKeyCode)
  {
    SetKeyCode(kcNone);   
  }
  return KC;
}





