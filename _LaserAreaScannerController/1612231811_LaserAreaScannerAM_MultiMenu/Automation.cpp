#include "Automation.h"
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
extern enum EErrorCode GlobalErrorCode;
extern EStateAutomation StateAutomation;
//
// extern CLed LedSystem;
extern CLcdKey LcdKey;
extern CRotaryDecoder RotaryDecoderX;
extern CRotaryDecoder RotaryDecoderY;
//
long unsigned TimeSystemStamp;
//
extern UInt32 PositionOffsetX, PositionOffsetY;
extern UInt32 PositionMirrorX, PositionMirrorY;
//
//#########################################################
//  Segment - Automation
//#########################################################
//
//---------------------------------------------------------
// State - Welcome - 0
//---------------------------------------------------------
// 
void HandleAutomationInitWelcome(CSerial &serial)
{ // 0.0 -[]>0.1
  TimeSystemStamp = millis();
  LcdKey.ClearScreen();
  LcdKey.WriteText(0, 0, "LaserAreaScanner");  
  LcdKey.WriteText(0, 1, "01V01 - LLG 2016");
  StateAutomation = saShowWelcome;
}

void HandleAutomationShowWelcome(CSerial &serial)
{ // 0.1 -[1s]>1.0
  long unsigned TimeSystemActual = millis();
  if (TIME_SHOWWELCOME < (TimeSystemActual - TimeSystemStamp))
  {   
    TimeSystemStamp = TimeSystemActual;
    StateAutomation = saInitIdle;
  }
}
//
//---------------------------------------------------------
// State - Idle - 1
//---------------------------------------------------------
//
void HandleAutomationInitIdle(CSerial &serial)
{ // 1.0 -[]>1.1
  TimeSystemStamp = millis();
  LcdKey.ClearScreen();
  LcdKey.WriteText(0, 0, "LAS - Idle");  
  LcdKey.WriteText(0, 1, "MX:     MY:    ");
  LcdKey.WriteInt16(3, 1, 4, PositionMirrorX);
  LcdKey.WriteInt16(11, 1, 4, PositionMirrorY);
  StateAutomation = saSystemIdle;
}

void HandleAutomationSystemIdle(CSerial &serial)
{ // 1.1 -[S]>2.0
  // UserInput - Key
  LcdKey.RefreshKey();  
  EKeyCode KC = LcdKey.GetKeyCode();
  if (kcSelectHL == KC)
  {
    StateAutomation = saInitOffset;
  }  
  else
  {
    //.. actualise System-Parameter
  }
}
//
//---------------------------------------------------------
// State - Offset - 2
//---------------------------------------------------------
//
void HandleAutomationInitOffset(CSerial &serial)
{ // 2.0 -[]>2.1
  TimeSystemStamp = millis();
  RotaryDecoderX.SetPositionActual(PositionOffsetX);
  RotaryDecoderY.SetPositionActual(PositionOffsetY);
  LcdKey.ClearScreen();
  LcdKey.WriteText(0, 0, "LAS - Offset");  
  LcdKey.WriteText(0, 1, "OX:     OY:    ");
  LcdKey.WriteInt16(3, 1, 4, PositionOffsetX);
  LcdKey.WriteInt16(11, 1, 4, PositionOffsetY);
  StateAutomation = saAdjustOffset;
  delay(1000);
}

void HandleAutomationAdjustOffset(CSerial &serial)
{ // 2.1 -[S]>3.0
  // UserInput - Key
  LcdKey.RefreshKey();
  if (kcSelectHL == LcdKey.GetKeyCode())
  {
    StateAutomation = saScanAreaInit;
  }
  else
  { // UserInput - RotaryDecoderX
    if (RotaryDecoderX.Refresh())
    {
      if (RotaryDecoderX.GetStateSwitch())
      {
        if (INIT_POSITIONDELTA_LOW == RotaryDecoderX.GetPositionDelta())
        {
          RotaryDecoderX.SetPositionDelta(INIT_POSITIONDELTA_HIGH);
        } 
        else
        {
          RotaryDecoderX.SetPositionDelta(INIT_POSITIONDELTA_LOW);        
        }
      }
      PositionOffsetX = RotaryDecoderX.GetPositionActual();
      LcdKey.WriteInt16(3, 1, 4, PositionOffsetX);
    }  
    // UserInput - RotaryDecoderY
    if (RotaryDecoderY.Refresh())
    {
      if (RotaryDecoderY.GetStateSwitch())
      {
        if (INIT_POSITIONDELTA_LOW == RotaryDecoderY.GetPositionDelta())
        {
          RotaryDecoderY.SetPositionDelta(INIT_POSITIONDELTA_HIGH);
        } 
        else
        {
          RotaryDecoderY.SetPositionDelta(INIT_POSITIONDELTA_LOW);
        }
      }
      PositionOffsetY = RotaryDecoderY.GetPositionActual();
      LcdKey.WriteInt16(11, 1, 4, PositionOffsetY);
    }  
  }
}
//
//---------------------------------------------------------
// State - ScanArea - 3
//---------------------------------------------------------
//
void HandleAutomationScanAreaInit(CSerial &serial)
{ // 3.0 -[]>3.1 -[S]>1.0
  // UserInput - Key
  LcdKey.RefreshKey();
  if (kcSelectHL == LcdKey.GetKeyCode())
  {
    StateAutomation = saInitIdle;
  }
  else
  {
    TimeSystemStamp = millis();
    LcdKey.ClearScreen();
    LcdKey.WriteText(0, 0, "Scan Area Init");  
    LcdKey.WriteText(0, 1, "MX:     MY:");
    PositionMirrorX = PositionOffsetX;
    PositionMirrorY = PositionOffsetY;
    LcdKey.WriteInt16(3, 1, 4, PositionOffsetX);
    LcdKey.WriteInt16(11, 1, 4, PositionOffsetY);
    StateAutomation = saScanAreaWait;
  }
}

void HandleAutomationScanAreaWait(CSerial &serial)
{ // 3.1 -[U]>3.1 -[S]>1.0
  // UserInput - Key
  LcdKey.RefreshKey();
  EKeyCode KC = LcdKey.GetKeyCode();
  if (kcSelectHL == KC)
  {
    StateAutomation = saInitIdle;
  }
  if (kcUpHL == KC)
  {
    TimeSystemStamp = millis();
    LcdKey.ClearScreen();
    LcdKey.WriteText(0, 0, "Scan Area Busy");  
    LcdKey.WriteText(0, 1, "MX:     MY:");
    PositionMirrorX = PositionOffsetX;
    PositionMirrorY = PositionOffsetY;
    LcdKey.WriteInt16(3, 1, 4, PositionOffsetX);
    LcdKey.WriteInt16(11, 1, 4, PositionOffsetY);
    StateAutomation = saScanAreaBusy;
  }  
}

void HandleAutomationScanAreaBusy(CSerial &serial)
{ // 3.2 -[End]>1.0 -[S]>1.0
  // UserInput - Key
  LcdKey.RefreshKey();
  if (kcSelectHL == LcdKey.GetKeyCode())
  {
    StateAutomation = saInitIdle;
  }
  else
  {
    // display progress of processing...
    // wait for End - Message from LASAD -> saInitIdle
    // Simulation:
    Boolean ScanEnded = false;
    PositionMirrorX++;
    if (64 + PositionOffsetX < PositionMirrorX)
    {
      PositionMirrorX = PositionOffsetX;
      PositionMirrorY++;
      if (48 + PositionOffsetY < PositionMirrorY)
      {
        PositionMirrorY = PositionOffsetY;
        ScanEnded = true;
      }
    }     
    LcdKey.WriteInt16(3, 1, 4, PositionMirrorX);
    LcdKey.WriteInt16(11, 1, 4, PositionMirrorY);
    // delay(1);
    if (ScanEnded)
    {
      StateAutomation = saInitIdle;     
    }
  }
}
//
//#####################################################
//
void HandleAutomation(CSerial &serial)
{
  switch (StateAutomation)
  {
    case saInitWelcome:   // 0.0 -[]>0.1
      HandleAutomationInitWelcome(serial);
      break;
    case saShowWelcome:   // 0.1 -[sec]>1.0
      HandleAutomationShowWelcome(serial);
      break;
    case saInitIdle:      // 1.0 -[]>1.1
      HandleAutomationInitIdle(serial);
      break;
    case saSystemIdle:    // 1.1 -[S]>2.0
      HandleAutomationSystemIdle(serial);
      break;
    case saInitOffset:    // 2.0 -[]>2.1 -[S]>3.0
      HandleAutomationInitOffset(serial);
      break;
    case saAdjustOffset:  // 2.1 -[S]>3.0
      HandleAutomationAdjustOffset(serial);
      break;
    case saScanAreaInit:  // 3.0 -[]>3.1 -[S]>1.0
      HandleAutomationScanAreaInit(serial);
      break;
    case saScanAreaWait:  // 3.1 -[U]>3.2 -[S]>1.0
      HandleAutomationScanAreaWait(serial);
      break;
    case saScanAreaBusy:  // 3.2 -[End]>1.0 -[S]>1.0
      HandleAutomationScanAreaBusy(serial);
      break;
  }
}




