#ifndef Utilities_h
#define Utilities_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
#include "SerialCommand.h"
//
//--------------------------------
//  Section - Constant - Hardware
//--------------------------------
//
#define LED_SYSTEM  0
#define LED_ERROR   1
#define LED_BUSY    2
#define LED_MOTIONX 3
#define LED_MOTIONY 4
#define LED_TRIGGER 5
//
#define ADC_POSITIONX 0
#define ADC_POSITIONY 1
//
#define DAC_MIRRORX 0
#define DAC_MIRRORY 1
//
#define PWM_FOCUSZ 0
//
#define XPOSITION_CENTER (UInt16)2048
#define YPOSITION_CENTER (UInt16)2048
//
#define DELAY_TRIGGER_1MS (UInt32)1000
//
// Init-Values for Globals:
#define INIT_GLOBALERRORCODE ecNone
#define INIT_STATEAUTOMATION saIdle
#define INIT_COUNTREPETITION 1
#define INIT_COUNTLASERPULSES 1
#define INIT_DELAYMOTION 500
#define INIT_DELAYPULSE 1000
//
#define INIT_XPOSITIONACTUAL 2048
#define INIT_XPOSITIONDELTA 1023
#define INIT_XPOSITIONMINIMUM 0
#define INIT_XPOSITIONMAXIMUM 4095
//
#define INIT_YPOSITIONACTUAL 2048
#define INIT_YPOSITIONDELTA 1023
#define INIT_YPOSITIONMINIMUM 0
#define INIT_YPOSITIONMAXIMUM 4095
//
#define INIT_DELAYTRIGGER 1000
//
enum EStateAutomation
{
  saIdle = 0,
  saMotionRowColLeft = 1,
  saMotionRowColBoth = 2,
  saTriggerRowColLeft = 3,
  saTriggerRowColBoth = 4
};
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
#define TITEL_LINE            "---------------------------------------"
#define TITEL_PROJECT         "- Project:   LAS - LaserAreaScanner   -"
#define TITEL_VERSION         "- Version:   03V01                    -"
#define TITEL_HARDWARE        "- Hardware:  ArduinoDue               -"
#define TITEL_DATE            "- Date:      170621                   -"
#define TITEL_TIME            "- Time:      1903                     -"
#define TITEL_AUTHOR          "- Author:    OM                       -"
#define TITEL_PORT            "- Port:      SerialPC (SerialUSB)     -"
#define TITEL_PARAMETER       "- Parameter: 115200, N, 8, 1          -"
//
#define HELP_SOFTWAREVERSION  " Software-Version LAS03V01"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoDue3"
//
#define HELP_COMMON           " Help (Common):"
#define HELP_H                " H   : This Help"
#define HELP_GPH              " GPH <fdl> : Get Program Header"
#define HELP_GSV              " GSV <fdl> : Get Software-Version"
#define HELP_GHV              " GHV <fdl> : Get Hardware-Version"
#define HELP_LED              " Help (Led):"
#define HELP_GLD              " GLD <i> : Get State Led with <i>ndex[0..4]"
#define HELP_SLD              " SLD <i> : Set Led with <i>ndex[0..4]"
#define HELP_CLD              " CLD <i> : Clear Led with <i>ndex[0..4]"
#define HELP_TLD              " TLD <i> <n> <d>: Toggle Led <i>ndex[0..4] <n>times with <d>elay{us}"
#define HELP_DAC              " Help (Dac):"
#define HELP_GDV              " GDV <i> : Get Value from DAConverter with Index<0..1>"
#define HELP_SDV              " SDV <i> <v>: Set Value<0..4095> to DAConverter with Index[0..1]"
#define HELP_AUTOMATION       " Help (Automation):"
#define HELP_SMX              " SMX <xmin> <xmax> <dx>: Set Motion-ParameterX[0..4095]"
#define HELP_SMY              " SMY <ymin> <ymax> <dy>: Set Motion-ParameterY[0..4095]"
#define HELP_SDM              " SDM <delay>: Set Delay Motion{us}"
#define HELP_SDP              " SDP <delay>: Set Delay Pulse{us}"
//// #define HELP_STM              " STM <d> <r> : Start Motion <d>elaytrigger{us} with <r>epetitions"
#define HELP_STT              " STT <n> <r> : Start Trigger <n>Pulses with <r>epetitions"
#define HELP_ABM              " ABM : Abort Motion"
//
void WriteProgramHeader(CSerial &serialcommand);
void WriteHelp(CSerial &serialcommand);
//  Handler - Error
void HandleError(CSerial &serial);
//  Handler - SerialPC Communication
void HandleSerialCommands(CSerial &serialcommand);
void HandleAutomation(CSerial &serial);
//
#endif
