//
//--------------------------------
//  Library Definitions
//--------------------------------
//
#ifndef Defines_h
#define Defines_h
//
#include <stdlib.h>
#include "Arduino.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
//
#define TRUE 1
#define FALSE 0
//
// Init-Values - Global
#define INIT_ERRORCODE ecNone
#define INIT_RXDECHO false
//
//----------------------------------
// ARDUINOUNO :
// #define PROCESSOR_ARDUINOUNO
//
//----------------------------------
// Arduino Mega 2560
// #define PROCESSOR_ARDUINOMEGA
//
//----------------------------------
// Arduino Due
// 
#define PROCESSOR_ARDUINODUE
//
//----------------------------------
// STM32F103C8 : no DACs!
// #define PROCESSOR_STM32F103C8
//
//----------------------------------
// Teensy 3.2
// #define PROCESSOR_TEENSY32
//
//----------------------------------
// Teensy 3.6
// #define PROCESSOR_TEENSY36
//
//----------------------------------
// Arduino Uno R3
// #define PROCESSOR_ARDUINOUNO
//
//##########################################
//  PROCESSOR_ARDUINOUNO
//##########################################
#if defined(PROCESSOR_ARDUINOUNO)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
// SPI
const int PIN_SPI_CS_TEMPERATURE0 = 10;
const int PIN_SPI_CS_TEMPERATURE1 =  9;
const int PIN_SPI_CS_TEMPERATURE2 =  8;
const int PIN_SPI_CS_TEMPERATURE3 =  7;
const int PIN_SPI_CS_TEMPERATURE4 =  6;
const int PIN_SPI_CS_TEMPERATURE5 =  5;
const int PIN_SPI_CS_TEMPERATURE6 =  4;
const int PIN_SPI_CS_TEMPERATURE7 =  3;
// I2C
const int PIN_I2C_SCL = 19;
const int PIN_I2C_SDA = 18;
// System Led 
// used from SPI const int PIN_LEDSYSTEM = 13;
// const bool LEDSYSTEM_INVERTED = false;
//
#endif // PROCESSOR_ARDUINOUNO
////
////##########################################
////  PROCESSOR_ARDUINOMEGA
////##########################################
//#if defined(PROCESSOR_ARDUINOMEGA)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 int 
#define UInt16 unsigned int
#define Int32 long int
#define UInt32 long unsigned int
#define Int64 long long int
#define UInt64 long long unsigned int
#define Float float
#define Double double
//
//// System Led 
//const int PIN_LEDSYSTEM         = 13;
//#endif // PROCESSOR_ARDUINOMEGA
////
////##########################################
////  PROCESSOR_ARDUINODUE
////##########################################
#if defined(PROCESSOR_ARDUINODUE)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
// System Led 
const int PIN_LEDSYSTEM           = 13;
const bool LEDSYSTEM_INVERTED     = false;
// System LedLine
const int PIN_LEDERROR            = 54;
//                                  55
const int PIN_LEDBUSY             = 56;
//                                  57
const int PIN_LEDMOTIONX          = 58;
const int PIN_LEDMOTIONY          = 59;
//                                  60
const int PIN_LEDLASER            = 50;
// SPI
const int PIN_SPI_CS_TEMPERATURE0 = 12;
const int PIN_SPI_CS_TEMPERATURE1 = 11;
const int PIN_SPI_CS_TEMPERATURE2 = 10;
const int PIN_SPI_CS_TEMPERATURE3 = 9;
const int PIN_SPI_CS_TEMPERATURE4 = 8;
const int PIN_SPI_CS_TEMPERATURE5 = 7;
const int PIN_SPI_CS_TEMPERATURE6 = 6;
const int PIN_SPI_CS_TEMPERATURE7 = 5;
// I2C
const int PIN_I2C_SCL             = 21;
const int PIN_I2C_SDA             = 20;
// Dac
const int PIN_DAC_CHANNEL0        = 66;
const int PIN_DAC_CHANNEL1        = 67;
#endif // PROCESSOR_ARDUINODUE
//
//##########################################
//  PROCESSOR_STM32F103C8
//##########################################
#if defined(PROCESSOR_STM32F103C8)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
// SPI
const int PIN_SPI_CS_TEMPERATURE0 = PB12;
const int PIN_SPI_CS_TEMPERATURE1 = PB13;
const int PIN_SPI_CS_TEMPERATURE2 = PB14;
const int PIN_SPI_CS_TEMPERATURE3 = PB5;
const int PIN_SPI_CS_TEMPERATURE4 = PB4;
const int PIN_SPI_CS_TEMPERATURE5 = PB3;
const int PIN_SPI_CS_TEMPERATURE6 = PA15;
const int PIN_SPI_CS_TEMPERATURE7 = PA12;
// I2C
const int PIN_I2C_SCL = PB6;
const int PIN_I2C_SDA = PB7;
// System Led 
const int PIN_LEDSYSTEM = PC13;
const bool LEDSYSTEM_INVERTED = true;
//
#endif // PROCESSOR_STM32F103C8
//
////
////##########################################
////  PROCESSOR_TEENSY32
////##########################################
#if defined(PROCESSOR_TEENSY32)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
// Single Led System (fixed)
const int PIN_LEDSYSTEM   = 13;
const bool LEDSYSTEM_INVERTED = false;
//
#endif // PROCESSOR_TEENSY32
//
////##########################################
////  PROCESSOR_TEENSY36
////##########################################
//#if defined(PROCESSOR_TEENSY36)
//
#define Boolean bool
#define Character char
#define PCharacter char*
#define Byte byte
#define Int8 signed char
#define UInt8 unsigned char
#define Int16 short int 
#define UInt16 short unsigned int
#define Int32 int
#define UInt32 unsigned int
#define Int64 long int
#define UInt64 long unsigned int
#define Float float
#define Double double
//
//// Single Led System (fixed)
//const int PIN_LEDSYSTEM   = 13;
////
//#endif // PROCESSOR_TEENSY36
//
//--------------------------------
//  Section - Constant - Error
//--------------------------------
//
//--------------------------------
//  Section - Constant - Process
//--------------------------------
// Init-Values - PulsePosition
//const UInt32 INIT_LASERPULSEPERIOD = 1000000; // [us]
//
#endif // Defines_h





