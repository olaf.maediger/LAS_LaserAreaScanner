//
//--------------------------------
//  Library Dac
//--------------------------------
//
#ifndef Dac_h
#define Dac_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Dac
//--------------------------------
//
const Byte DAC_CHANNELX = 0x00;
const Byte DAC_CHANNELY = 0x01;
//
class CDac
{
  private:
  bool FChannel[2];
  UInt32 FValue[2];
  
  public:
  CDac(bool channel0, bool channel1);
  Boolean Open();
  Boolean Close();
  UInt32 GetValue(UInt8 channel);
  void SetValue(UInt8 channel, UInt32 value);
  void SetValues(UInt32 value0, UInt32 value1);
};
//
#endif // Dac_h
