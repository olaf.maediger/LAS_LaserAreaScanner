//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "Led.h"
//
CLed::CLed(int pin)
{
  FPin = pin;
  FState = slUndefined;
}

Boolean CLed::Open()
{
  pinMode(FPin, OUTPUT);
  digitalWrite(FPin, LOW);
  FState = slOff;
  return true;
}

Boolean CLed::Close()
{
  pinMode(FPin, INPUT);
  FState = slUndefined;
  return true;
}

EStateLed CLed::GetState()
{
  return FState;
}

void CLed::On()
{
  digitalWrite(FPin, HIGH);
  FState = slOn;
}

void CLed::Off()
{
  digitalWrite(FPin, LOW);
  FState = slOff;
}

void CLed::Toggle()
{
  if (slOff == FState)
  {
    digitalWrite(FPin, HIGH);
    FState = slOn;
  } 
  if (slOn == FState)
  {
    digitalWrite(FPin, LOW);
  } 
}

void CLed::Pulse(UInt32 microseconds)
{
  digitalWrite(FPin, HIGH);
  FState = slOn;
  delayMicroseconds(microseconds);  
  digitalWrite(FPin, LOW);
  FState = slOff;
}


