#ifndef Serial_h
#define Serial_h
//
#include "Arduino.h"
#include "Defines.h"
#include "HardwareSerial.h"
#include "SerialUsbApi.h"
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//
#define RXDECHO_ON  true
#define RXDECHO_OFF false
//
#define CR   0x0D
#define LF   0x0A
#define ZERO 0x00
//
#define PROMPT_NEWLINE "\r\n"
#define PROMPT_INPUT   ">"
#define PROMPT_ANSWER  "#"
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
class CSerialBase
{
  protected:
  bool FIsOpen = false;
  
  public:
  virtual bool Open(int baudrate) = 0;
  virtual bool Close() = 0;
  virtual void WriteCharacter(char character) = 0;
  virtual void WriteText(char *text) = 0;
  virtual void WriteLine(char *text) = 0;
  virtual int GetRxdByteCount() = 0;
  virtual char ReadCharacter() = 0;

  void WriteNewLine();
  void WritePrompt();
  void WriteAnswer();
};
//
//----------------------------------------------------
//  Segment - CSerialZ (^= Serial)
//----------------------------------------------------
//
#ifdef PROCESSOR_ARDUINODUE
class CSerialZ : CSerialBase
{
  protected:
  UARTClass* FPSerial;
 
  public:
  CSerialZ(UARTClass *serial)
  {
    FPSerial = serial;
  }
  
  bool Open(int baudrate);
  bool Close();
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteLine(char *text);
  int GetRxdByteCount();
  char ReadCharacter();
};
#endif
//
//----------------------------------------------------
//  Segment - CSerialS (^= Serial1 / Serial2 / Serial3)
//----------------------------------------------------
//
#ifdef PROCESSOR_ARDUINODUE
class CSerialS : CSerialBase
{
  protected:
  USARTClass* FPSerial; 
  
  public:
  CSerialS(USARTClass *serial)
  {
    FPSerial = serial;
  }
  
  bool Open(int baudrate);
  bool Close();
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteLine(char *text);
  int GetRxdByteCount();
  char ReadCharacter();
};
#endif
//
//----------------------------------------------------
//  Segment - CSerialU (^=SerialUSB)
//----------------------------------------------------
//
#ifdef PROCESSOR_ARDUINODUE
class CSerialU : CSerialBase
{
  protected:
  Serial_* FPSerial;
  
  public:
  CSerialU(Serial_ *serial)
  {
    FPSerial = serial;
  } 

  bool Open(int baudrate);
  bool Close();
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteLine(char *text);
  int GetRxdByteCount();
  char ReadCharacter();
};
#endif


//
//----------------------------------------------------
//  Segment - CSerialH (^=HardwareSerial)
//----------------------------------------------------
//
#if ((defined PROCESSOR_STM32F103C8) || (defined PROCESSOR_TEENSY36))
class CSerialH : CSerialBase
{
  protected:
  HardwareSerial* FPSerial;
  
  public:
  CSerialH(HardwareSerial *serial)
  {
    FPSerial = serial;
  } 

  bool Open(int baudrate);
  bool Close();
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteLine(char *text);
  int GetRxdByteCount();
  char ReadCharacter();
};
#endif

//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
class CSerial
{
  protected:
  CSerialBase* FPSerial;
  bool FRxdEcho;
  
  public:
#if (defined PROCESSOR_ARDUINODUE )
  CSerial(UARTClass *serial, bool rxdecho);
  CSerial(UARTClass &serial, bool rxdecho);
  CSerial(USARTClass *serial, bool rxdecho);
  CSerial(USARTClass &serial, bool rxdecho);
  CSerial(Serial_ *serial, bool rxdecho);
  CSerial(Serial_ &serial, bool rxdecho);
#elif (defined PROCESSOR_ARDUINOMEGA)
  CSerial(HardwareSerial *serial, bool rxdecho);
  CSerial(HardwareSerial &serial, bool rxdecho);
#elif (defined PROCESSOR_STM32F103C8)
  CSerial(HardwareSerial *serial, bool rxdecho);
  CSerial(HardwareSerial &serial, bool rxdecho);
#elif (defined PROCESSOR_TEENSY36)
  CSerial(HardwareSerial *serial, bool rxdecho);
  CSerial(HardwareSerial &serial, bool rxdecho);
#else
#error Undefined Processor-Type!
#endif
  bool Open(int baudrate);
  bool Close();
  void SetRxdEcho(bool rxdecho);
  void WriteCharacter(char character);
  void WriteText(char *text);
  void WriteLine(char *text);
  void WriteNewLine();
  void WriteAnswer();
  void WritePrompt();
  int GetRxdByteCount();
  char ReadCharacter();
};
//
#endif
