#include "Utilities.h"
#include "LedLine.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
//
//
//-------------------------------------------
//  Segment - Global Variable
//-------------------------------------------
//  
extern enum EErrorCode GlobalErrorCode;
//
extern EStateAutomation StateAutomation;
extern UInt32 CountRepetition; // [1]
extern UInt32 CountLaserPulses; // [1]
extern UInt32 DelayMotion; // [us]
extern UInt32 DelayPulse; // [us]
//
extern UInt16 XPositionActual; // [stp]
extern UInt16 XPositionDelta; // [stp]
extern UInt16 XPositionMinimum; // [stp]
extern UInt16 XPositionMaximum; // [stp]
extern UInt16 XPositionHome; // [stp]
//
extern UInt16 YPositionActual; // [stp]
extern UInt16 YPositionDelta; // [stp]
extern UInt16 YPositionMinimum; // [stp]
extern UInt16 YPositionMaximum; // [stp]
extern UInt16 YPositionHome; // [stp]
//
//-------------------------------------------
//  Segment - Global Class
//-------------------------------------------
//  
extern CLedLine LedLine;
//
// NC extern CAdc AnalogDigitalConverter;
//
extern CDac DigitalAnalogConverter;
//
// NC extern CPwm PulseWidthModulator;
//
extern CSerialCommand SerialCommand;
//
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_VERSION);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(TITEL_LINE);
  serial.WriteNewLine();	
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_SOFTWAREVERSION);
  serial.WriteNewLine();
}

void WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_HARDWAREVERSION);
  serial.WriteNewLine();
}

void WriteHelp(CSerial &serial)
{
  serial.WriteAnswer();
  serial.WriteText(HELP_COMMON);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_H);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_GPH);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_GSV);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_GHV);
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(HELP_LED);
  serial.WriteNewLine();
  serial.WriteAnswer();		
  serial.WriteText(HELP_GLD);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_SLD);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_CLD);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_TLD);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.WriteText(HELP_PLD);
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(HELP_DAC);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_GDV);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SDV);
  serial.WriteNewLine();
  serial.WriteAnswer();
  //
  serial.WriteText(HELP_AUTOMATION);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SMX);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SMY);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SDM);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SDP);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_SPH);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_STH);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_STM);
  serial.WriteNewLine();
  serial.WriteAnswer();    
  serial.WriteText(HELP_ABM);
  serial.WriteNewLine();
}
//
//#########################################################
//  Segment - Execution - Common
//#########################################################
//
void ExecuteGetHelp(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHelp(serial);
  serial.WritePrompt();
}

void ExecuteGetProgramHeader(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteProgramHeader(serial);
  serial.WritePrompt();
}

void ExecuteGetSoftwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteSoftwareVersion(serial);
  serial.WritePrompt();
}

void ExecuteGetHardwareVersion(CSerial &serial)
{ // Analyse parameters: -
  // Response
  WriteHardwareVersion(serial);
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Led
//#########################################################
//
void ExecuteGetLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  int State = LedLine.GetState(Index);
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s %i", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0), State);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteClearLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  byte Mask = 0x01 << Index;
  // Execute:
  if (Index <= 7)
  {
    LedLine.Off(Mask);
  }
  else
  {
    LedLine.Off();    
  }
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0));
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSetLed(CSerial &serial)
{	// Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  byte Mask = 0x01 << Index;
  // Execute:
  if (Index <= 7)
  {
    LedLine.On(Mask);
  }
  else
  {
    LedLine.On();
  }
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %s", 
          SerialCommand.GetPRxdCommand(), SerialCommand.GetPRxdParameters(0));
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteToggleLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  if (Index <= 7)
  {
    LedLine.Toggle(0x01 << Index);
  }
  else
  {
    LedLine.Toggle();
  }
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u", 
          SerialCommand.GetPRxdCommand(), Index);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecutePulseLed(CSerial &serial)
{  // Analyse parameters:
  int Index = atoi(SerialCommand.GetPRxdParameters(0));
  UInt32 DelayMillis = atol(SerialCommand.GetPRxdParameters(1));
  UInt32 Count = atol(SerialCommand.GetPRxdParameters(2));
  // Execute:
  if (Index <= 7)
  {
    LedLine.Pulse(0x01 << Index, DelayMillis, Count);
  }
  else
  {
    LedLine.Pulse(DelayMillis, Count);
  }
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %lu %lu", 
          SerialCommand.GetPRxdCommand(), Index, DelayMillis, Count);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Dac
//#########################################################
//
void ExecuteGetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
  // Execute:
  UInt16 Value = DigitalAnalogConverter.GetValue(Channel);
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %u", 
          SerialCommand.GetPRxdCommand(), Channel, Value);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void ExecuteSetDacValue(CSerial &serial)
{  // Analyse parameters:
  UInt8 Channel = atoi(SerialCommand.GetPRxdParameters(0));
  UInt32 Value = atol(SerialCommand.GetPRxdParameters(1));
  // Execute:
  DigitalAnalogConverter.SetValue(Channel, Value);
  Value = DigitalAnalogConverter.GetValue(Channel);
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %u %lu", 
          SerialCommand.GetPRxdCommand(), Channel, Value);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - Helper
//#########################################################
//
void ActivatePositionActual()
{
  DigitalAnalogConverter.SetValue(DAC_MIRRORX, XPositionActual);
  DigitalAnalogConverter.SetValue(DAC_MIRRORY, YPositionActual);  
}

void ActivatePositionHome()
{
  DigitalAnalogConverter.SetValue(DAC_MIRRORX, XPositionHome);
  DigitalAnalogConverter.SetValue(DAC_MIRRORY, YPositionHome);
}

void ActivateTrigger()
{
  LedLine.On(0x01 << LED_TRIGGER);
  delayMicroseconds(DelayPulse / 2);
  LedLine.Off(0x01 << LED_MOTIONY);
  LedLine.Off(0x01 << LED_MOTIONX);
  delayMicroseconds(DelayPulse / 2);
  LedLine.Off(0x01 << LED_TRIGGER);
}

void TriggerLaserPulses()
{ // Pulsedelay fixed to 1000us!
  LedLine.Off(0x01 << LED_MOTIONY);
  LedLine.Off(0x01 << LED_MOTIONX);
  // wait for motion stops...
  delayMicroseconds(DelayMotion);
  //
  UInt32 TI;
  UInt32 DL = DelayPulse / 10;
  UInt32 DH = 9* DL;
  for (TI = 0; TI < CountLaserPulses; TI++)
  {    
    LedLine.On(0x01 << LED_TRIGGER);
    delayMicroseconds(DH);
    LedLine.Off(0x01 << LED_TRIGGER);
    delayMicroseconds(DL);
  }
}
//
//#########################################################
//  Segment - Execution - Automation
//#########################################################
//
void ExecuteSetMotionX(CSerial &serial)
{ // Analyse parameters:
  XPositionMinimum = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  XPositionMaximum = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  XPositionDelta = abs(atoi(SerialCommand.GetPRxdParameters(2)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu %lu", 
          SerialCommand.GetPRxdCommand(), XPositionMinimum, XPositionMaximum, XPositionDelta);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//##################################################
//
void ExecuteSetMotionY(CSerial &serial)
{ // Analyse parameters:
  YPositionMinimum = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  YPositionMaximum = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  YPositionDelta = abs(atoi(SerialCommand.GetPRxdParameters(2)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu %lu", 
          SerialCommand.GetPRxdCommand(), YPositionMinimum, YPositionMaximum, YPositionDelta);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//##################################################
//
void ExecuteSetDelayMotion(CSerial &serial)
{ // Analyse parameters:
  DelayMotion = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu", 
          SerialCommand.GetPRxdCommand(), DelayMotion);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//##################################################
//
void ExecuteSetDelayPulse(CSerial &serial)
{ // Analyse parameters:
  DelayPulse = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu", 
          SerialCommand.GetPRxdCommand(), DelayPulse);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//##################################################
//
void ExecuteSetPositionHome(CSerial &serial)
{ // Analyse parameters:
  XPositionHome = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  YPositionHome = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  // Execute -
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu", 
          SerialCommand.GetPRxdCommand(), XPositionHome, YPositionHome);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//##################################################
//
bool AbortMotion()
{
  StateAutomation = saIdle;
  LedLine.Off(0x01 << LED_MOTIONY);
  LedLine.Off(0x01 << LED_MOTIONX);
  LedLine.Off(0x01 << LED_BUSY);
  return true;
}

void ExecuteAbortMotion(CSerial &serial)
{ // Analyse parameters -
  // Execute:
  AbortMotion();
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# ABM");
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//##################################################
//
Boolean StartTriggerHome()
{
  if (0 < CountRepetition)
  {
    LedLine.Off(0x01 << LED_MOTIONY);
    LedLine.Off(0x01 << LED_MOTIONX);
    LedLine.On(0x01 << LED_BUSY);
    XPositionActual = XPositionHome;
    YPositionActual = YPositionHome;
    ActivatePositionActual();
    StateAutomation = saTriggerHome;
    return true;
  }
  return false;
}

void ExecuteStartTriggerHome(CSerial &serial)
{ // Analyse parameters:
  CountLaserPulses = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  DelayPulse = abs(atoi(SerialCommand.GetPRxdParameters(1)));  
  // Execute:
  StartTriggerHome();
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu", 
          SerialCommand.GetPRxdCommand(), CountLaserPulses, DelayPulse);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//##################################################
//
Boolean StartTriggerMotion()
{
  if (0 < CountRepetition)
  {
    LedLine.On(0x01 << LED_MOTIONY);
    LedLine.On(0x01 << LED_MOTIONX);
    LedLine.On(0x01 << LED_BUSY);
    XPositionActual = XPositionMinimum;
    YPositionActual = YPositionMinimum;
    ActivatePositionActual();
    StateAutomation = saTriggerMotion;
    return true;
  }
  return false;
}

void ExecuteStartTriggerMotion(CSerial &serial)
{ // Analyse parameters:
  CountLaserPulses = abs(atoi(SerialCommand.GetPRxdParameters(0)));
  CountRepetition = abs(atoi(SerialCommand.GetPRxdParameters(1)));
  // Execute:
  StartTriggerMotion();
  // Answer:
  sprintf(SerialCommand.GetTxdBuffer(), "# %s %lu %lu", 
          SerialCommand.GetPRxdCommand(), CountLaserPulses, CountRepetition);
  serial.WriteLine(SerialCommand.GetTxdBuffer());
  serial.WritePrompt();  
}
//
//#########################################################
//  Segment - Handler - Error
//#########################################################
//
void HandleError(CSerial &serial)
{
  if (ecNone != GlobalErrorCode)
  {
    serial.WriteAnswer();
    serial.WriteText(" Error[");
    Int16ToAsciiDecimal(GlobalErrorCode, SerialCommand.GetTxdBuffer());	
    serial.WriteText(SerialCommand.GetTxdBuffer());
    serial.WriteText("]: ");
    switch (GlobalErrorCode)
    {
      case ecNone:
        serial.WriteText("No Error");
        break;
      case ecUnknown:
        serial.WriteText("Unknown");
        break;
      case ecInvalidCommand:
        serial.WriteText("Invalid Command");
        break;
      case ecToManyParameters:
        serial.WriteText("To many Parameters");
        break;     
      case ecMissingTargetParameter:
        serial.WriteText("Missing Target Parameter");
        break;              
      default:
        serial.WriteText("Unknown Error");
        break;
    }
    serial.WriteText("!");
    serial.WriteNewLine();
    serial.WritePrompt();
    GlobalErrorCode = ecNone;
  }
}
//
//#########################################################
//  Segment - Automation
//#########################################################
//
void HandleAutomationIdle(CSerial &serial)
{  
  // nothing to do
}

void HandleAutomationTriggerHome(CSerial &serial)
{
  LedLine.On(0x01 << LED_MOTIONX);
  LedLine.On(0x01 << LED_MOTIONY);
  //
  XPositionActual = XPositionHome;
  YPositionActual = YPositionHome;
  //
  ActivatePositionActual();
  ActivateTrigger();
  //
  LedLine.Off(0x01 << LED_MOTIONX);
  LedLine.Off(0x01 << LED_MOTIONY);
  //  
  ExecuteAbortMotion(serial);
}

void HandleAutomationTriggerMotion(CSerial &serial)
{
  LedLine.On(0x01 << LED_MOTIONX);
  LedLine.On(0x01 << LED_MOTIONY);
  //
  ActivatePositionActual();
  ActivateTrigger();
  //
  XPositionActual += XPositionDelta;
  if (XPositionMaximum < XPositionActual)
  {
    XPositionActual = XPositionMinimum;
    YPositionActual += YPositionDelta;
    if (YPositionMaximum < YPositionActual)
    { // Finish Area scanning
      XPositionActual = XPositionMinimum;
      YPositionActual = YPositionMinimum;
      CountRepetition--;
      LedLine.Toggle(0x01 << LED_BUSY);
      if (0 == CountRepetition)
      { // ends...
        // SetPosition();
        ExecuteAbortMotion(serial);
        return;
      }
    } 
  } 
}

void HandleAutomation(CSerial &serial)
{
  switch (StateAutomation)
  {
    case saIdle:
      HandleAutomationIdle(serial);
      break;
    case saTriggerHome:
      HandleAutomationTriggerHome(serial);
      break;
    case saTriggerMotion:
      HandleAutomationTriggerMotion(serial);
      break;
  }
}

Boolean ExecuteRxdCommand(CSerial &serial)
{ 
//  sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
//  serialtarget.WriteLine(TxdBuffer);
  if (!strcmp("H", SerialCommand.GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp("GPH", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp("GSV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp("GHV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  // ----------------------------------
  // Led
  // ----------------------------------
  if (!strcmp("GLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetLed(serial);
    return true;
  } else 
  if (!strcmp("SLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetLed(serial);
    return true;
  } else   
  if (!strcmp("CLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteClearLed(serial);
    return true;
  } else   
  if (!strcmp("TLD", SerialCommand.GetPRxdCommand()))
  {
    ExecuteToggleLed(serial);
    return true;
  } else   
  if (!strcmp("PLD", SerialCommand.GetPRxdCommand()))
  {
    ExecutePulseLed(serial);
    return true;
  } else   
//  // ----------------------------------
//  // Adc
//  // ----------------------------------
//  if (!strcmp("GAV", SerialCommand.GetPRxdCommand()))
//  {
//    ExecuteGetAdcValue(serial);
//    return true;
//  } else   
  // ----------------------------------
  // Dac
  // ----------------------------------
  if (!strcmp("GDV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteGetDacValue(serial);
    return true;
  } else   
  if (!strcmp("SDV", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDacValue(serial);
    return true;
  } else   
  // ----------------------------------
  // LaserPulse
  // ----------------------------------
  if (!strcmp("TLP", SerialCommand.GetPRxdCommand()))
  {
//!!!!!!!!!!!!!!!!!!!    ExecuteTriggerLaserPulse(serial);
    return true;
  } else   
  // ----------------------------------
  // Automation
  // ----------------------------------
  if (!strcmp("SMX", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetMotionX(serial);
    return true;
  } else   
  if (!strcmp("SMY", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetMotionY(serial);
    return true;
  } else   
  if (!strcmp("SDM", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDelayMotion(serial);
    return true;
  } else   
  if (!strcmp("SDP", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetDelayPulse(serial);
    return true;
  } else   
  if (!strcmp("SPH", SerialCommand.GetPRxdCommand()))
  {
    ExecuteSetPositionHome(serial);
    return true;
  } else   
  if (!strcmp("STH", SerialCommand.GetPRxdCommand()))
  {
    ExecuteStartTriggerHome(serial);
    return true;
  } else   
  if (!strcmp("STM", SerialCommand.GetPRxdCommand()))
  {
    ExecuteStartTriggerMotion(serial);
    return true;
  } else   
  if (!strcmp("ABM", SerialCommand.GetPRxdCommand()))
  {
    ExecuteAbortMotion(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    GlobalErrorCode = ecInvalidCommand;
  }
  return false;  
}

void HandleSerialCommands(CSerial &serial)
{
  if (SerialCommand.AnalyseRxdCommand(serial))
  {
    ExecuteRxdCommand(serial);
  }  
}



