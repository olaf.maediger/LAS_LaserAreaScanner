#include "Serial.h"
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
void CSerialBase::WriteNewLine()
{
  WriteText(PROMPT_NEWLINE);  
}
  
void CSerialBase::WritePrompt()
{  
//  long unsigned SystemTime = millis();
//  unsigned Millis = SystemTime % 1000;
//  long unsigned RTS = SystemTime / 1000;
//  unsigned Hours = RTS / 3600;
//  RTS = RTS - 3600 * Hours;
//  unsigned Minutes = RTS / 60;
//  unsigned Seconds = RTS - 60 * Minutes;
//  char Buffer[16];
//  sprintf(Buffer, "%2.2d:%2.2d:%2.2d.%3.3d", Hours, Minutes, Seconds, Millis); 
//  WriteText(Buffer);
  WriteText(BuildTime(millis()));
  WriteText(PROMPT_INPUT);
}
  
void CSerialBase::WriteAnswer()
{
  WriteText(PROMPT_ANSWER);
}

String CSerialBase::BuildTime(long unsigned milliseconds)
{
  long unsigned SystemTime = milliseconds;
  unsigned Millis = SystemTime % 1000;
  long unsigned RTS = SystemTime / 1000;
  unsigned Hours = RTS / 3600;
  RTS = RTS - 3600 * Hours;
  unsigned Minutes = RTS / 60;
  unsigned Seconds = RTS - 60 * Minutes;
  char Buffer[16];
  sprintf(Buffer, "%2.2d:%2.2d:%2.2d.%3.3d", Hours, Minutes, Seconds, Millis); 
  return Buffer;
}
//
//----------------------------------------------------
//  Segment - CSerialZ
//----------------------------------------------------
//
#if (defined PROCESSOR_ARDUINODUE)
bool CSerialZ::Open(int baudrate)
{
  FPSerial->begin(baudrate);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialZ::Close()
{
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}

void CSerialZ::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialZ::WriteText(char *text)
{
  FPSerial->write(text);
}  

void CSerialZ::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  

int CSerialZ::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialZ::ReadCharacter()
{
  return (char)FPSerial->read();
}

void CSerialZ::WriteTime()
{
  FPSerial->print(millis());
}

#elif (defined PROCESSOR_TEENSY36)
#else
#error Undefined CSerialZ for ProcessorType!
#endif  
//
//----------------------------------------------------
//  Segment - CSerialS
//----------------------------------------------------
//
#if (defined PROCESSOR_ARDUINODUE)
bool CSerialS::Open(int baudrate)
{
  FPSerial->begin(baudrate);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialS::Close()
{
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}
  
void CSerialS::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialS::WriteText(char *text)
{
  FPSerial->write(text);
}  
void CSerialS::WriteText(String text)
{
  FPSerial->write(text.c_str());
}  

void CSerialS::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  
void CSerialS::WriteLine(String text)
{
  FPSerial->write(text.c_str());
  WriteNewLine();
}  

int CSerialS::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialS::ReadCharacter()
{
  return (char)FPSerial->read();
}

void CSerialS::WriteTime()
{
  FPSerial->print(millis());
}
#elif (defined PROCESSOR_TEENSY36)
#else
#error Undefined CSerialS for ProcessorType!
#endif  
//
//----------------------------------------------------
//  Segment - CSerialU
//----------------------------------------------------
//
#if (defined PROCESSOR_ARDUINODUE)
bool CSerialU::Open(int baudrate)
{
  FPSerial->begin(baudrate);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialU::Close()
{
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}
  
void CSerialU::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialU::WriteText(char *text)
{
  FPSerial->write(text);
}  

void CSerialU::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  

int CSerialU::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialU::ReadCharacter()
{
  return (char)FPSerial->read();
}

void CSerialU::WriteTime()
{
  FPSerial->print(millis());
}
#elif (defined PROCESSOR_TEENSY36)
#else
#error Undefined CSerialU for ProcessorType!
#endif  
//
//----------------------------------------------------
//  Segment - CSerialH 
//----------------------------------------------------
//
#if ((defined PROCESSOR_STM32F103C8) || (defined PROCESSOR_TEENSY36))
bool CSerialH::Open(int baudrate)
{
  FPSerial->begin(baudrate);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialH::Close()
{
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}
  
void CSerialH::WriteCharacter(char character)
{
  FPSerial->write(character);
}

void CSerialH::WriteText(char *text)
{
  FPSerial->write(text);
}  

void CSerialH::WriteLine(char *text)
{
  FPSerial->write(text);
  WriteNewLine();
}  

int CSerialH::GetRxdByteCount()
{
  FPSerial->available();
}

char CSerialH::ReadCharacter()
{
  return (char)FPSerial->read();
}
#elif (defined PROCESSOR_ARDUINOMEGA)
#elif (defined PROCESSOR_ARDUINODUE)
#else
#error Undefined CSerialH for ProcessorType!
#endif  
//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
#if ((defined PROCESSOR_ARDUINOMEGA) || (defined PROCESSOR_ARDUINODUE))

CSerial::CSerial(UARTClass *serial, bool rxdecho)
{
  CSerialZ *PSerialZ = (CSerialZ*)new CSerialZ(serial);
  FPSerial = (CSerialBase*)PSerialZ;
  FRxdEcho = rxdecho;
}

CSerial::CSerial(UARTClass &serial, bool rxdecho)
{
  CSerialZ *PSerialZ = (CSerialZ*)new CSerialZ(&serial);
  FPSerial = (CSerialBase*)PSerialZ;
  FRxdEcho = rxdecho;
}

CSerial::CSerial(USARTClass *serial, bool rxdecho)
{
  CSerialS *PSerialS = (CSerialS*)new CSerialS(serial);
  FPSerial = (CSerialBase*)PSerialS;
  FRxdEcho = rxdecho;
}

CSerial::CSerial(USARTClass &serial, bool rxdecho)
{
  CSerialS *PSerialS = (CSerialS*)new CSerialS(&serial);
  FPSerial = (CSerialBase*)PSerialS;
  FRxdEcho = rxdecho;
}

CSerial::CSerial(Serial_ *serial, bool rxdecho)
{ 
  CSerialU *PSerialU = (CSerialU*)new CSerialU(serial);
  FPSerial = (CSerialBase*)PSerialU;
  FRxdEcho = rxdecho;
}

CSerial::CSerial(Serial_ &serial, bool rxdecho)
{ 
  CSerialU *PSerialU = (CSerialU*)new CSerialU(&serial);
  FPSerial = (CSerialBase*)PSerialU;
  FRxdEcho = rxdecho;
}

#elif ((defined PROCESSOR_STM32F103C8) || (defined PROCESSOR_TEENSY36))

CSerial::CSerial(HardwareSerial *serial, bool rxdecho)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
  FRxdEcho = rxdecho;
}

CSerial::CSerial(HardwareSerial &serial, bool rxdecho)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
  FRxdEcho = rxdecho;
}
#endif

bool CSerial::Open(int baudrate)
{
  return FPSerial->Open(baudrate);
}

bool CSerial::Close()
{
  return FPSerial->Close();
}

void CSerial::SetRxdEcho(bool rxdecho)
{
  FRxdEcho = rxdecho;
}

void CSerial::WriteCharacter(char character)
{
  FPSerial->WriteCharacter(character);
}

void CSerial::WriteText(char *text)
{
  FPSerial->WriteText(text);
}
void CSerial::WriteText(String text)
{
  FPSerial->WriteText(text);
}

void CSerial::WriteLine(char *text)
{
  FPSerial->WriteLine(text);
}
void CSerial::WriteLine(String text)
{
  FPSerial->WriteLine(text);
}

void CSerial::WriteNewLine()
{
  FPSerial->WriteNewLine();
}

void CSerial::WriteAnswer()
{
  FPSerial->WriteAnswer();
}

void CSerial::WritePrompt()
{
  FPSerial->WritePrompt();
}

void CSerial::WriteTime()
{
  FPSerial->WriteTime();
}

int CSerial::GetRxdByteCount()
{
  FPSerial->GetRxdByteCount();
}

char CSerial::ReadCharacter()
{
  char C = (char)FPSerial->ReadCharacter();
  if (FRxdEcho && (isalnum(C) || (' ' == C)))
  {
    FPSerial->WriteCharacter(C);
  }
  return C;
}


