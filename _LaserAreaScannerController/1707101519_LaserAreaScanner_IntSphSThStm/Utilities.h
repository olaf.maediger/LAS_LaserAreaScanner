#ifndef Utilities_h
#define Utilities_h
//
#include "Defines.h"
#include "Helper.h"
#include "Serial.h"
#include "Adc.h"
#include "Dac.h"
#include "Pwm.h"
#include "SerialCommand.h"
//
//--------------------------------
//  Section - Constant - Hardware
//--------------------------------
//
// Single LED #define LED_SYSTEM 
// LedLine
#define LED_ERROR   0
#define LED_NC1     1
#define LED_BUSY    2
#define LED_NC3     3
#define LED_MOTIONX 4
#define LED_MOTIONY 5
#define LED_NC6     6
#define LED_TRIGGER 7
//
#define ADC_POSITIONX 0
#define ADC_POSITIONY 1
//
#define DAC_MIRRORX 0
#define DAC_MIRRORY 1
//
#define PWM_FOCUSZ 0
//
#define XPOSITION_CENTER (UInt16)2048
#define YPOSITION_CENTER (UInt16)2048
//
#define DELAY_TRIGGER_1MS (UInt32)1000
//
// Init-Values for Globals:
#define INIT_GLOBALERRORCODE ecNone
#define INIT_STATEAUTOMATION saIdle
#define INIT_COUNTREPETITION 1
#define INIT_COUNTLASERPULSES 1000
// Stabilization of Position after Moving [us]
#define INIT_DELAYMOTION 500
// Laser-Pulse-Period [us] 100ms -> 10Hz
#define INIT_DELAYPULSE 100000
// Laser-Pulse-Period [us] 10ms -> 100Hz //#define INIT_DELAYPULSE 10000
//#define INIT_DELAYPULSE 1000 // Laser-Pulse-Period [us] 1ms -> 1KHz
//
// Default 5 Positions in X
#define INIT_XPOSITIONACTUAL 2000
#define INIT_XPOSITIONDELTA 1000
#define INIT_XPOSITIONMINIMUM 0
#define INIT_XPOSITIONMAXIMUM 4000
#define INIT_XPOSITIONHOME 2048
// Default 5 Positions in Y (total 36 Positions)
#define INIT_YPOSITIONACTUAL 2000
#define INIT_YPOSITIONDELTA 1000
#define INIT_YPOSITIONMINIMUM 0
#define INIT_YPOSITIONMAXIMUM 4000
#define INIT_YPOSITIONHOME 2000
//
enum EStateAutomation
{
  saIdle = 0,
  saTriggerHome = 1,
  saTriggerMotion = 2,
};
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
#define TITEL_LINE            "---------------------------------------"
#define TITEL_PROJECT         "- Project:   LAS - LaserAreaScanner   -"
#define TITEL_VERSION         "- Version:   03V04                    -"
#define TITEL_HARDWARE        "- Hardware:  ArduinoDue               -"
#define TITEL_DATE            "- Date:      170710                   -"
#define TITEL_TIME            "- Time:      1328                     -"
#define TITEL_AUTHOR          "- Author:    OM                       -"
#define TITEL_PORT            "- Port:      SerialPC (SerialUSB)     -"
#define TITEL_PARAMETER       "- Parameter: 115200, N, 8, 1          -"
//
#define HELP_SOFTWAREVERSION  " Software-Version LAS03V04"
#define HELP_HARDWAREVERSION  " Hardware-Version ArduinoDue3"
//
#define HELP_COMMON           " Help (Common):"
#define HELP_H                " H   : This Help"
#define HELP_GPH              " GPH <fdl> : Get Program Header"
#define HELP_GSV              " GSV <fdl> : Get Software-Version"
#define HELP_GHV              " GHV <fdl> : Get Hardware-Version"
#define HELP_LED              " Help (Led):"
#define HELP_GLD              " GLD <i> : Get State Led with <i>ndex[0..4]"
#define HELP_SLD              " SLD <i> : Set Led with <i>ndex[0..4]"
#define HELP_CLD              " CLD <i> : Clear Led with <i>ndex[0..4]"
#define HELP_TLD              " TLD <i> : Toggle Led <i>ndex[0..4]"
#define HELP_PLD              " PLD <i> <d> <n> : Toggle Led <i>ndex[0..4] with <d>elay{ms} <n>times"
#define HELP_DAC              " Help (Dac):"
#define HELP_GDV              " GDV <i> : Get Value from DAConverter with Index<0..1>"
#define HELP_SDV              " SDV <i> <v>: Set Value<0..4095> to DAConverter with Index[0..1]"
#define HELP_AUTOMATION       " Help (Automation):"
#define HELP_SMX              " SMX <xmin> <xmax> <dx>: Set Motion-ParameterX[0..4095]"
#define HELP_SMY              " SMY <ymin> <ymax> <dy>: Set Motion-ParameterY[0..4095]"
#define HELP_SDM              " SDM <delay>: Set Delay Motion{us}"
#define HELP_SDP              " SDP <delay>: Set Delay Pulse{us}"
#define HELP_SPH              " SPH <x> <y>: Set Position Home {us}"
#define HELP_STH              " STH <n> <d> : Start Trigger Home <n>Pulses with Pulse<d>elay"
#define HELP_STM              " STM <n> <r> : Start Trigger Motion <n>Pulses with <r>epetitions"
#define HELP_ABM              " ABM : Abort Motion"
//
void WriteProgramHeader(CSerial &serialcommand);
void WriteHelp(CSerial &serialcommand);
//  Handler - Error
void HandleError(CSerial &serial);
//  Handler - SerialPC Communication
void HandleSerialCommands(CSerial &serialcommand);
void HandleAutomation(CSerial &serial);
//
void ActivatePositionHome();
void ActivatePositionActual();
void ActivateTrigger();
//
#endif
