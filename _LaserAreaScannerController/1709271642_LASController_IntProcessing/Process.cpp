#include "Process.h"
//
CProcess::CProcess()
{
  FLaserPulsePeriod = INIT_LASERPULSEPERIOD;
  FLaserPulseCountPreset = INIT_LASERPULSECOUNTPRESET;
  FLaserPulseCountActual = INIT_LASERPULSECOUNTACTUAL;
  FMatrixCountRepetitions = INIT_MATRIXCOUNTREPETITIONS;
  FRangeDelayMotion = INIT_RANGEDELAYMOTION;
  FRangeDelayPulse = INIT_RANGEDELAYPULSE;
  //
  FXPositionActual = INIT_XPOSITIONACTUAL;
  FXPositionDelta = INIT_XPOSITIONDELTA;
  FXPositionMinimum = INIT_XPOSITIONMINIMUM;
  FXPositionMaximum = INIT_XPOSITIONMAXIMUM;
  //
  FYPositionActual = INIT_YPOSITIONACTUAL;
  FYPositionDelta = INIT_YPOSITIONDELTA;
  FYPositionMinimum = INIT_YPOSITIONMINIMUM;
  FYPositionMaximum = INIT_YPOSITIONMAXIMUM;    
}

