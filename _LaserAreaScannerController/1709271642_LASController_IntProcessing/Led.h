//
//--------------------------------
//  Library Led
//--------------------------------
//
#ifndef Led_h
#define Led_h
//
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
enum EStateLed
{
  slUndefined = -1,
  slOff = 0,
  slOn = 1
};
//
class CLed
{
  private:
  int FPin;
  EStateLed FState;
  UInt32 FPeriod;
  UInt32 FCount;
  
  public:
  CLed(int pin);
  //
  EStateLed GetState();
  UInt32 GetCount();
  UInt32 GetPeriod();
  inline UInt32 GetPeriodHalf()
  {
    return FPeriod / 2;  
  }
  //
  Boolean Open();
  Boolean Close();
  void On();
  void Off();
  void Pulse(UInt32 period, UInt32 count);
};
//
#endif
