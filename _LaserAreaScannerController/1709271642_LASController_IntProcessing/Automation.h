#ifndef Automation_h
#define Automation_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
#include "Led.h"
#include "Dac.h"
#include "Process.h"
//
//
enum EStateAutomation
{
  saUndefined = -1,
  saIdle = 0,
  // LedLaser
  saLedLaserPulseBegin = 1,
  saLedLaserPulseBusy = 2,
  saLedLaserPulseEnd = 3,
  // PositionLaser
  saPulsePositionLaserBegin = 4,
  saPulsePositionLaserBusy = 5,
  saPulsePositionLaserEnd = 6,
  // MatrixLaser
  saPulseMatrixLaserBegin = 7,
  saPulseMatrixLaserBusy = 8,
  saPulseMatrixLaserAbort = 9,
  saPulseMatrixLaserEnd = 10,
  // VariableLaser
  saPulseVariableLaserBegin = 11,
  saPulseVariableLaserBusy = 12,
  saPulseVariableLaserAbort = 13,
  saPulseVariableLaserEnd = 14
};
//
class CAutomation
{ // Field:
  private:
  EStateAutomation FState;
  UInt32 FTimeStart;
  UInt32 FTimeMarker;
  UInt32 FTimeStamp;
  // -> Process UInt32 FCountEvent;
  //
  public:
  // Constructor
  CAutomation();
  // Property
  EStateAutomation GetState();
  void SetState(EStateAutomation state);
  void SetTimeMarker();
  // Management
  Boolean Open();
  Boolean Close();
  // Idle
  void HandleIdle(CSerial &serial);
  // LedLaser
  void HandleLedLaserPulseBegin(CSerial &serial);
  void HandleLedLaserPulseBusy(CSerial &serial);
  void HandleLedLaserPulseEnd(CSerial &serial);
  // PositionLaser
  void HandlePulsePositionLaserBegin(CSerial &serial);
  void HandlePulsePositionLaserBusy(CSerial &serial);
  void HandlePulsePositionLaserEnd(CSerial &serial);
  // MatrixLaser
  void HandlePulseMatrixLaserBegin(CSerial &serial);
  void HandlePulseMatrixLaserBusy(CSerial &serial);
  void HandlePulseMatrixLaserAbort(CSerial &serial);
  void HandlePulseMatrixLaserEnd(CSerial &serial);
  // VariableLaser
  void HandlePulseVariableLaserBegin(CSerial &serial);
  void HandlePulseVariableLaserBusy(CSerial &serial);
  void HandlePulseVariableLaserAbort(CSerial &serial);
  void HandlePulseVariableLaserEnd(CSerial &serial);
  // Collector
  void Handle(CSerial &serial);
};
//
#endif // Automation_h
