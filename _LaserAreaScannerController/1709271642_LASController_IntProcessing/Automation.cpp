#include "Automation.h"
//
extern CLed LedLaser;
extern CDac DigitalAnalogConverter;
extern CProcess LASProcess;
//
//
//----------------------------------------------------------
//  Segment - Constructor
//----------------------------------------------------------
//
CAutomation::CAutomation()
{
  FState = saUndefined;
  FTimeStart = millis();
  FTimeMarker = FTimeStart;
}
//
//----------------------------------------------------------
//  Segment - Property
//----------------------------------------------------------
//
EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  if (state != FState)
  {
    FState = state;
  }
}

void CAutomation::SetTimeMarker()
{
  FTimeMarker = millis();
}
//
//----------------------------------------------------------
//  Segment - Management
//----------------------------------------------------------
//
Boolean CAutomation::Open()
{
  FState = saIdle;
  return true;
}

Boolean CAutomation::Close()
{
  FState = saUndefined;
  return true;
}
//
//----------------------------------------------------------
//  Segment - Automation - Idle
//----------------------------------------------------------
//
void CAutomation::HandleIdle(CSerial &serial)
{
  // do nothing
}
//
//----------------------------------------------------------
//  Segment - Automation - LedLaser
//----------------------------------------------------------
//
void CAutomation::HandleLedLaserPulseBegin(CSerial &serial)
{
  FTimeMarker = millis();  
  LedLaser.On();
  SetState(saLedLaserPulseBusy);
}
void CAutomation::HandleLedLaserPulseBusy(CSerial &serial)
{  
  if (LedLaser.GetPeriodHalf() <= FTimeStamp - FTimeMarker)
  {
    FTimeMarker = FTimeStamp;       
    if (slOff == LedLaser.GetState())
    {
      LedLaser.On();
    }
    else
      if (slOn == LedLaser.GetState())
      {
        LedLaser.Off();
        LASProcess.SetLaserPulseCountActual(1 + LedLaser.GetCount());
        if (LASProcess.IsLaserPulseCountFinished())
        {
          SetState(saLedLaserPulseEnd);
        }
      }
  }
}
void CAutomation::HandleLedLaserPulseEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - PositionLaser
//----------------------------------------------------------
//
void CAutomation::HandlePulsePositionLaserBegin(CSerial &serial)
{ // -> Busy : wait for PVL x y p c ( -> Busy) OR : AVL -> Abort -> End -> Idle
  SetState(saPulsePositionLaserBusy);
}

void CAutomation::HandlePulsePositionLaserBusy(CSerial &serial)
{ // -> Busy : wait for PVL x y p c ( -> Busy) OR : AVL -> Abort -> End -> Idle
  if (LedLaser.GetPeriodHalf() <= FTimeStamp - FTimeMarker)
  {
    FTimeMarker = FTimeStamp;       
    if (slOff == LedLaser.GetState())
    {
      LedLaser.On();
    }
    else
      if (slOn == LedLaser.GetState())
      {
        LedLaser.Off();
        LASProcess.SetLaserPulseCountActual(1 + LedLaser.GetCount());
        if (LASProcess.IsLaserPulseCountFinished())
        {
          SetState(saPulsePositionLaserEnd);
        }
      }
  }
}

void CAutomation::HandlePulsePositionLaserEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - MatrixLaser
//----------------------------------------------------------
//
void CAutomation::HandlePulseMatrixLaserBegin(CSerial &serial)
{
  FTimeMarker = millis(); 
  UInt32 XPA = LASProcess.GetXPositionMinimum();
  LASProcess.SetXPositionActual(XPA);
  UInt32 YPA = LASProcess.GetYPositionMinimum();
  LASProcess.SetYPositionActual(YPA);
  DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
  DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());  
  LASProcess.SetLaserPulseCountActual(1);  
  LedLaser.On();
  SetState(saPulseMatrixLaserBusy);
}

void CAutomation::HandlePulseMatrixLaserBusy(CSerial &serial)
{  
  if (LedLaser.GetPeriodHalf() <= FTimeStamp - FTimeMarker)
  {
    FTimeMarker = FTimeStamp;       
    if (slOff == LedLaser.GetState())
    {
      LedLaser.On();
    }
    else
      if (slOn == LedLaser.GetState())
      {
        LedLaser.Off();
        LASProcess.SetLaserPulseCountActual(1 + LedLaser.GetCount());
        if (LASProcess.IsLaserPulseCountFinished())
        { // All Pulses for one Location pulsed -> next Position
          UInt32 XPA = LASProcess.IncrementXPosition();
          if (XPA <= LASProcess.GetXPositionMaximum())
          { // next X-Position
            DigitalAnalogConverter.SetValue(DAC_CHANNELX, LASProcess.GetXPositionActual());
            DigitalAnalogConverter.SetValue(DAC_CHANNELY, LASProcess.GetYPositionActual());                
          }
          else
          { // X-Line finished
            UInt32 YPA = LASProcess.IncrementYPosition();
            if (YPA <= LASProcess.GetYPositionMaximum())
            { // next Y-Line
            }
            else
            { // Y-Line (Matrix) finished
              SetState(saPulseMatrixLaserEnd);
            }
          }
        }
      }
  }
}

void CAutomation::HandlePulseMatrixLaserAbort(CSerial &serial)
{  
  LedLaser.Off();
  SetState(saIdle);  
}

void CAutomation::HandlePulseMatrixLaserEnd(CSerial &serial)
{  
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - VariableLaser
//----------------------------------------------------------
// SVL -> Begin -> Busy (wait for PVL or AVL)
void CAutomation::HandlePulseVariableLaserBegin(CSerial &serial)
{
  LASProcess.SetLaserPulseCountPreset(0);
  LASProcess.SetLaserPulseCountActual(0);
  LASProcess.SetLaserPulsePeriod(0);
  SetState(saPulseVariableLaserBusy); // -> Busy
}

// PVL -> 0 < LaserCountPulses, 0 < LaserPulsePeriod, X, Y
void CAutomation::HandlePulseVariableLaserBusy(CSerial &serial)
{  
  if (!LASProcess.IsLaserPulseCountFinished())
  {  
    if (LedLaser.GetPeriodHalf() <= FTimeStamp - FTimeMarker)
    {
      FTimeMarker = FTimeStamp;       
      if (slOff == LedLaser.GetState())
      {
        LedLaser.On();
      }
      else
        if (slOn == LedLaser.GetState())
        {
          LedLaser.Off();
          LASProcess.SetLaserPulseCountActual(LedLaser.GetCount());
          if (LASProcess.IsLaserPulseCountFinished())
          {
          }
        }
    }
  }
}

void CAutomation::HandlePulseVariableLaserAbort(CSerial &serial)
{  
  LedLaser.Off();
  SetState(saPulseVariableLaserEnd);  
}

void CAutomation::HandlePulseVariableLaserEnd(CSerial &serial)
{  
  LedLaser.Off();
  SetState(saIdle);  
}
//
//----------------------------------------------------------
//  Segment - Automation - Collector
//----------------------------------------------------------
//
void CAutomation::Handle(CSerial &serial)
{
  FTimeStamp = millis();
  switch (FState)
  { // Idle
    case saIdle:
      HandleIdle(serial);
      break;
    // LedLaser
    case saLedLaserPulseBegin:
      HandleLedLaserPulseBegin(serial);
      break;
    case saLedLaserPulseBusy:
      HandleLedLaserPulseBusy(serial);
      break;
    case saLedLaserPulseEnd:
      HandleLedLaserPulseEnd(serial);
      break;
    // PositionLaser
    case saPulsePositionLaserBegin:
      HandlePulsePositionLaserBegin(serial);
      break;
    case saPulsePositionLaserBusy:
      HandlePulsePositionLaserBusy(serial);
      break;
    case saPulsePositionLaserEnd:
      HandlePulsePositionLaserEnd(serial);
      break;
    // MatrixLaser
    case saPulseMatrixLaserBegin:
      HandlePulseMatrixLaserBegin(serial);
      break;
    case saPulseMatrixLaserBusy:
      HandlePulseMatrixLaserBusy(serial);
      break;
    case saPulseMatrixLaserAbort:
      HandlePulseMatrixLaserEnd(serial);
      break;
    case saPulseMatrixLaserEnd:
      HandlePulseMatrixLaserEnd(serial);
      break;
    // VariableLaser
    case saPulseVariableLaserBegin:
      HandlePulseVariableLaserBegin(serial);
      break;
    case saPulseVariableLaserBusy:
      HandlePulseVariableLaserBusy(serial);
      break;
    case saPulseVariableLaserAbort:
      HandlePulseVariableLaserEnd(serial);
      break;
    case saPulseVariableLaserEnd:
      HandlePulseVariableLaserEnd(serial);
      break;
  }  
}

